package com.tsfyun.scm.system.entity;

import lombok.Data;

import javax.persistence.Id;


/**
 * <p>
 * 币制实体类
 * </p>
 *

 * @since 2020-03-18
 */
@Data
public class Currency  {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    /**
     * 禁用状态
     */

    private Boolean disabled;

    /**
     * 中国银行币制索引
     */

    private String markCode;

    /**
     * 币制名称
     */

    private String name;

    /**=
     * 排序
     */
    private Integer sort;


}
