package com.tsfyun.scm.system.util;

import cn.hutool.http.HttpRequest;
import com.tsfyun.common.base.util.DateUtils;
import com.tsfyun.common.base.util.LocalDateTimeUtils;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.system.entity.BankChinaRate;
import lombok.extern.slf4j.Slf4j;
import com.tsfyun.scm.system.entity.Currency;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class RateUtil {

    //农历节假日(春节，端午,中秋)
    private static Map<String, List<String>> holidayMap = new HashMap<String, List<String>>(){{
        put("2019", Arrays.asList("02-05","02-06","02-07","06-07","09-13"));
        put("2020", Arrays.asList("01-25","01-26","01-27","06-25","10-01"));
    }};


    public static Date calculationRateDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.MONTH, -1);
        while (Boolean.TRUE){
            if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY){
                cal.add(Calendar.DATE, 14);
                break;
            }
            cal.add(Calendar.DATE, 1);
        }
        Date grabDate = cal.getTime();
        List<String> holidays = holidayMap.get(DateUtils.format(grabDate,"yyyy"));
        //法定节假日顺延1周
        if(holidays!=null&&holidays.contains(DateUtils.format(grabDate,"MM-dd"))){
            cal.add(Calendar.DATE, 7);
            grabDate = cal.getTime();
        }
        return grabDate;
    }


    public static void startGrab(String date, Currency currency, Integer page, List<BankChinaRate> bankChinaRates, List<String> ids){
        log.info(currency.getName()+"：开始抓取第["+page+"]页数据");
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("erectDate",date);
        params.put("nothing",date);
        params.put("pjname",currency.getMarkCode());
        params.put("page",page.toString());
        Map<String,String> headers = new LinkedHashMap<>();
        headers.put("Content-Type","application/x-www-form-urlencoded");
        String response = null;
        try {
            response = HttpRequest.post("https://srh.bankofchina.com/search/whpj/search_cn.jsp").header("Content-Type", "application/x-www-form-urlencoded")
                    .form(params).timeout(5000).execute().body();
        } catch (Exception e) {
            log.error("中行抓取汇率失败",e);
        }
        if(StringUtils.isEmpty(response)){return;}
        Document doc = Jsoup.parse(response);
        Elements trs = doc.select(".BOC_main").select("tr");
        if(trs!=null&&trs.size()>1) {
            for(int i=1;i<trs.size();i++){
                Elements tds = trs.get(i).select("td");
                if (tds != null && tds.size() == 7){
                    String currencyName = StringUtils.removeSpecialSymbol(tds.get(0).text());
                    if(currency.getMarkCode().equals(currencyName)){
                        Date publishDate = DateUtils.parseLong(StringUtils.removeSpecialSymbol(tds.get(6).text().replace(".","-")));
                        //检查公布时间在上一次公布之前则退出
//                        if(publishDate.before(lastPublishDate)){return;}
                        //检查是否已经存在
                        String id = currency.getId()+DateUtils.fromatShortNoSign(publishDate);
                        if(!ids.contains(id)){
                            ids.add(id);
                        }else{
                            continue;
                        }
                        BankChinaRate bcr = new BankChinaRate();
                        bcr.setId(id);
                        bcr.setDateCreated(LocalDateTime.now());
                        bcr.setCurrencyId(currency.getId());
                        bcr.setCurrencyName(currency.getName());
                        bcr.setPublishDate(LocalDateTimeUtils.convertDateToLDT(publishDate));
                        //现汇买入价
                        String buyingRate = StringUtils.removeSpecialSymbol(tds.get(1).text());
                        bcr.setBuyingRate(StringUtils.isNotEmpty(buyingRate) ? new BigDecimal(buyingRate) : null);
                        //现钞买入价
                        String cashPurchase = StringUtils.removeSpecialSymbol(tds.get(2).text());
                        bcr.setCashPurchase(StringUtils.isNotEmpty(cashPurchase) ? new BigDecimal(cashPurchase) : null);
                        //现汇卖出价
                        String spotSelling = StringUtils.removeSpecialSymbol(tds.get(3).text());
                        bcr.setSpotSelling(StringUtils.isNotEmpty(spotSelling) ? new BigDecimal(spotSelling) : null);
                        //现钞卖出价
                        String cashSelling = StringUtils.removeSpecialSymbol(tds.get(4).text());
                        bcr.setCashSelling(StringUtils.isNotEmpty(cashSelling) ? new BigDecimal(cashSelling) : null);
                        //中行折算价
                        String bocConversion = StringUtils.removeSpecialSymbol(tds.get(5).text());
                        bcr.setBocConversion(StringUtils.isNotEmpty(bocConversion) ? new BigDecimal(bocConversion) : null);
                        bankChinaRates.add(bcr);
                    }
                }
            }
            //是否存在下一页
            BigDecimal pageSize = BigDecimal.valueOf(20);//默认为20
            BigDecimal recordCount = BigDecimal.ZERO;
            //正则取总记录数和分页数
            String regex1 = "var m_nRecordCount = ([^;]*)";
            Pattern p1 = Pattern.compile(regex1);
            Matcher m1 = p1.matcher(response);
            if (m1.find()) {
                recordCount = new BigDecimal(m1.group(1));
            }
            String regex2 = "var m_nPageSize = ([^;]*)";
            Pattern p2 = Pattern.compile(regex2);
            Matcher m2 = p2.matcher(response);
            if (m2.find()) {
                pageSize = new BigDecimal(m2.group(1));
            }
            //总页数
            BigDecimal totalPage = recordCount.divide(pageSize).setScale(0,BigDecimal.ROUND_UP);
            log.info("当前页数："+page+" 总页数："+totalPage);
            if(totalPage.compareTo(BigDecimal.valueOf(page)) == 1){
                //抓取下一页
                startGrab(date,currency,page+1,bankChinaRates,ids);
            }
        }
    }



}
