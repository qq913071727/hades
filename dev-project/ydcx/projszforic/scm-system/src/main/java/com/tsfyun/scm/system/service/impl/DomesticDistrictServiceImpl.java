package com.tsfyun.scm.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.tsfyun.common.base.constant.CacheConstant;
import com.tsfyun.scm.system.entity.DomesticDistrict;
import com.tsfyun.scm.system.mapper.DomesticDistrictMapper;
import com.tsfyun.scm.system.service.IDomesticDistrictService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.system.vo.DomesticDistrictVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 国内地区 服务实现类
 * </p>
 *

 * @since 2020-03-20
 */
@Slf4j
@Service
public class DomesticDistrictServiceImpl extends ServiceImpl<DomesticDistrict> implements IDomesticDistrictService {


    @Autowired
    private DomesticDistrictMapper domesticDistrictMapper;

    //@Cacheable(value = CacheConstant.DOMESTIC_DISTRICT_LIST,key = "#keyword")
    @Override
    public List<DomesticDistrictVO> list(String keyword) {
        List<DomesticDistrict> list = domesticDistrictMapper.list(keyword);
        return beanMapper.mapAsList(list,DomesticDistrictVO.class);
    }

    @CacheEvict(value = CacheConstant.DOMESTIC_DISTRICT_LIST,allEntries = true,beforeInvocation = true)
    @Override
    public void clear() {
        log.info("清除国内行政区划缓存成功");
    }
}
