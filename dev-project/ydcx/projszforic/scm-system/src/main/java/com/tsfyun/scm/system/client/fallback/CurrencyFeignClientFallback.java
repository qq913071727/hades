package com.tsfyun.scm.system.client.fallback;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.vo.CurrencyVO;
import com.tsfyun.scm.system.client.CurrencyFeignClient;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class CurrencyFeignClientFallback implements FallbackFactory<CurrencyFeignClient> {
    @Override
    public CurrencyFeignClient create(Throwable throwable) {
        return new CurrencyFeignClient() {

            @Override
            public Result<List<CurrencyVO>> select() {
                log.error("调用saas服务获取币制列表异常");
                return null;
            }
        };
    }
}
