package com.tsfyun.scm.system.service;

import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.entity.TaxCodeBase;
import com.tsfyun.scm.system.vo.TaxCodeBaseVO;

import java.util.List;

/**
 * <p>
 * 税收分类编码 服务类
 * </p>
 *

 * @since 2020-05-15
 */
public interface ITaxCodeBaseService extends IService<TaxCodeBase> {

    /**
     * 根据税收分类编码获取数据
     * @param taxCode
     * @return
     */
    TaxCodeBaseVO findByCode(String taxCode);

    /**
     * 获取所有的税收分类编码
     * @return
     */
    List<TaxCodeBaseVO> select();

    /**
     * 清除缓存
     */
    public void clear( );

}
