package com.tsfyun.scm.system.controller;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.scm.system.service.ISinglewindowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/singlewindow")
public class SinglewindowController extends BaseController {

    @Autowired
    private ISinglewindowService singlewindowService;

    //刷新单一海关编码
    @GetMapping(value = "refreshAllHsCode")
    public String refreshAllHsCode(){
        singlewindowService.refreshAllHsCode();
        return "SUCCESS";
    }

    //刷新单一申报要素
    @GetMapping(value = "refreshElements")
    public String refreshElements(){
        singlewindowService.refreshElements();
        return "SUCCESS";
    }
}
