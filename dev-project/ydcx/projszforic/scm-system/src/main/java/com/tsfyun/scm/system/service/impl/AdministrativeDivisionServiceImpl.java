package com.tsfyun.scm.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.constant.CacheConstant;
import com.tsfyun.scm.system.entity.AdministrativeDivision;
import com.tsfyun.scm.system.mapper.AdministrativeDivisionMapper;
import com.tsfyun.scm.system.service.IAdministrativeDivisionService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.system.vo.AdministrativeDivisionVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 行政区划表 服务实现类
 * </p>
 *

 * @since 2020-03-20
 */
@Slf4j
@Service
public class AdministrativeDivisionServiceImpl extends ServiceImpl<AdministrativeDivision> implements IAdministrativeDivisionService {

    @Autowired
    private OrikaBeanMapper beanMapper;

    @Autowired
    private AdministrativeDivisionMapper administrativeDivisionMapper;

    //@Cacheable(value = CacheConstant.ADMINISTRATIVE_DIVISION_LIST,key = "#keyword")
    @Override
    public List<AdministrativeDivisionVO> list(String keyword) {
        List<AdministrativeDivision> list = administrativeDivisionMapper.list(keyword);
        return beanMapper.mapAsList(list,AdministrativeDivisionVO.class);
    }

    @CacheEvict(value = CacheConstant.ADMINISTRATIVE_DIVISION_LIST,allEntries = true,beforeInvocation = true)
    @Override
    public void clear() {
        log.info("清除行政区划缓存成功");
    }
}
