package com.tsfyun.scm.system.entity;

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;


/**
 * <p>
 * 海关基础数据
 * </p>
 *

 * @since 2020-03-20
 */
@Data
public class CustomsData  implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    /**
     * 类别
     */
    private String category;

    /**
     * 海关编码
     */
    private String code;

    /**
     * 中文名称
     */
    private String name;

    /**
     * 原海关编码
     */
    private String cusCode;

    /**
     * 商检编码
     */
    private String ciqCode;


}
