package com.tsfyun.scm.system.mapper;

import com.tsfyun.scm.system.entity.CustomsElements;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-19
 */
@Repository
public interface CustomsElementsMapper extends Mapper<CustomsElements> {

    int deleteByCustomsCode(@Param(value = "hsCode")String hsCode);

    List<CustomsElements> findByName(@Param(value = "name")String name);

    //根据海关编码查询申报要素
    List<CustomsElements> findByHsCode(@Param(value = "hsCode")String hsCode);
}
