package com.tsfyun.scm.system.mapper;

import com.tsfyun.scm.system.entity.Unit;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-19
 */
@Repository
public interface UnitMapper extends Mapper<Unit> {

}
