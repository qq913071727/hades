package com.tsfyun.scm.system.dto;

import com.tsfyun.common.base.dto.PaginationDto;
import lombok.Data;

@Data
public class HsCodeDTO extends PaginationDto {

    private String hsCode;//海关编码(模糊查询)
    private String name;//编码名称(模糊查询)
    private String iaqr;//检验检疫监管条件(模糊查询)
    private String csc;//监管条件(模糊查询)

    private String keyword;//关键字查询

    private String eqMemo;//备注(精确查询)

    private Boolean memoNotEmpty;//备注不为空
}
