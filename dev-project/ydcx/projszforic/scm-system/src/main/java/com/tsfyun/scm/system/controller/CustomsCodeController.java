package com.tsfyun.scm.system.controller;


import cn.hutool.core.collection.CollUtil;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.system.dto.CustomsCodeDTO;
import com.tsfyun.scm.system.dto.CustomsQTO;
import com.tsfyun.scm.system.dto.SelectCustomsCodeVO;
import com.tsfyun.scm.system.entity.CustomsCode;
import com.tsfyun.scm.system.service.ICiqCodesService;
import com.tsfyun.scm.system.service.ICustomsCodeService;
import com.tsfyun.scm.system.service.ICustomsElementsService;
import com.tsfyun.scm.system.vo.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-03-19
 */
@RestController
@RequestMapping("/customsCode")
public class CustomsCodeController extends BaseController {

    @Autowired
    private ICustomsCodeService customsCodeService;
    @Autowired
    private ICustomsElementsService customsElementsService;
    @Autowired
    private ICiqCodesService ciqCodesService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 分页查询
     * @param dto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<CustomsCodeVO>> pageList(@ModelAttribute CustomsQTO dto) {
        PageInfo<CustomsCode> page = customsCodeService.list(dto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),CustomsCodeVO.class));
    }

    /**
     * 明细
     * @param id
     * @return
     */
    @RequestMapping(value = "detail")
    public Result<CustomsCodeVO> detail(@RequestParam(value = "id")String id) {
        return success(customsCodeService.detail(id));
    }

    @PostMapping("details")
    public Result<Map<String,CustomsCodeVO>> details(@RequestBody @Validated Set<String> ids){
        Map<String,CustomsCodeVO> result = new LinkedHashMap<>();
        for(String id : ids){
            CustomsCodeVO vo = customsCodeService.detail(id);
            if(Objects.nonNull(vo)){
                result.put(id,vo);
            }
        }
        return success(result);
    }

    /**
     * 无需登录
     * @param ids
     * @return
     */
    @PostMapping("innerDetails")
    public Result<Map<String,CustomsCodeVO>> innerDetails(@RequestBody @Validated Set<String> ids){
        Map<String,CustomsCodeVO> result = new LinkedHashMap<>();
        for(String id : ids){
            CustomsCodeVO vo = customsCodeService.detail(id);
            if(Objects.nonNull(vo)){
                result.put(id,vo);
            }
        }
        return success(result);
    }

    /**
     * 获取所有的最惠国税率大于0的海关编码
     * @return
     */
    @PostMapping(value = "tariffCustoms")
    public Result<List<TariffCustomsCodeVO>> tariffCustoms( ) {
        return success(customsCodeService.tariffCustoms( ));
    }

    @PostMapping(value = "/selectAll")
    public Result<List<SelectCustomsCodeVO>> selectAll(){
        return Result.success(customsCodeService.selectAll());
    }


    //根据海关编码获取详情和申报要素(REDIS 数据缓存)
    @GetMapping(value = "/codeDetail/{hsCode}")
    public Result<HsCodeAndElementsVO> codeDetail(@PathVariable(value = "hsCode")String hsCode){
        CustomsCodeVO customsCode = customsCodeService.detail(hsCode);
        if(Objects.nonNull(customsCode)){
            HsCodeAndElementsVO vo = new HsCodeAndElementsVO();
            vo.setHsCode(beanMapper.map(customsCode, HsCodeSimpleVO.class));
            //获取申报要素
            List<CustomsElementsVO> customsElements = customsElementsService.findByHsCode(hsCode);
            vo.setElements(customsElements);
            //获取CIQ编码
            List<CiqCodeVo> codeVoList = ciqCodesService.findByHsCode(hsCode);
            if(CollUtil.isNotEmpty(codeVoList)){
                vo.setCiqCodes(beanMapper.mapAsList(codeVoList,CiqCodeSimpleVo.class));
            }else{
                vo.setCiqCodes(Lists.newArrayList());
            }
            return Result.success(vo);
        }
        return Result.success(null);
    }

    @ApiOperation(value = "新增海关编码信息")
    @PostMapping(value = "/addCustomsCode")
    public Result<Void> addCustomsCode(@ModelAttribute @Validated CustomsCodeDTO dto){
        customsCodeService.add(dto);
        return Result.success();
    }

    @ApiOperation(value = "修改海关编码信息")
    @PostMapping(value = "/editCustomsCode")
    public Result<Void> editCustomsCode(@ModelAttribute @Validated(UpdateGroup.class) CustomsCodeDTO dto){
        customsCodeService.edit(dto);
        return Result.success();
    }

    @ApiOperation(value = "获取海关编码数据")
    @GetMapping(value = "/customsCodeDetail")
    public Result<CustomsCodeDetailVO> customsCodeDetail(@RequestParam(value="id") String id){
        return Result.success(customsCodeService.info(id));
    }

    @ApiOperation(value = "删除海关编码数据")
    @PostMapping(value = "/delete")
    public Result<Void> delete(@RequestParam(value="id") String id){
        customsCodeService.delete(id);
        return Result.success( );
    }


}

