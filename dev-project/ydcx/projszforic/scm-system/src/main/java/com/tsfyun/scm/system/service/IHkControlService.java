package com.tsfyun.scm.system.service;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.system.dto.HkControlDTO;
import com.tsfyun.scm.system.entity.HkControl;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.vo.HkControlVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-19
 */
public interface IHkControlService extends IService<HkControl> {

    Result<List<HkControlVO>> list(HkControlDTO dto);

    void importData(MultipartFile importFile);
}
