package com.tsfyun.scm.system.entity;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;


/**
 * <p>
 * 港口代码
 * </p>
 *

 * @since 2020-03-20
 */
@Data
public class Port implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    /**
     * 编码
     */

    private String code;

    /**
     * 中文名称
     */

    private String name;

    /**
     * 英文名称
     */

    private String nameEn;

    /**
     * 是否禁用，1-是;0-否
     */

    private Boolean disabled;


}
