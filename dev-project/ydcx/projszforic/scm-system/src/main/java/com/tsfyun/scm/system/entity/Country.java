package com.tsfyun.scm.system.entity;

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;


/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-19
 */
@Data
public class Country implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    private Boolean disabled;


    private Boolean disinfectCheck;


    private Boolean isDiscount;


    private String name;


    private String namee;


    private Integer sort;


}
