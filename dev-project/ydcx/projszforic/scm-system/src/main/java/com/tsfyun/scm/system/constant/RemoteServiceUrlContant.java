package com.tsfyun.scm.system.constant;

/**
 * 远程调用请求地址
 */
public interface RemoteServiceUrlContant {

    //海关编码分页列表
    String CUSTOMS_CODE_LIST = "base/customsCode/queryList";

    //海关编码详细信息
    String CUSTOMS_CODE_DETAIL = "base/customsCode/getCustomsCodeById";

    //香港管制物品
    String HK_CONTROL_LIST = "inquiry/hkcontrol/list";

}
