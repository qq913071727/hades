package com.tsfyun.scm.system.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class HSCodeVO implements Serializable {

    private String id;//海关编码
    private String name;//海关编码名称
    private String csc;//监管条件
    private String iaqr;//检验检疫监管条件
    private String unitName;//法一单位
    private String sunitName;//法二单位

    private Boolean disabled;
}