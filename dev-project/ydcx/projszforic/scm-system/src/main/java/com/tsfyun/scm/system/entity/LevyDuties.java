package com.tsfyun.scm.system.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Data;

import javax.persistence.Id;


/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-20
 */
@Data
public class LevyDuties  implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    /**
     * 国家代码
     */

    private String countryCode;

    /**
     * 国家名称
     */

    private String countryName;

    /**
     * 海关编码
     */

    private String hsCode;

    /**
     * 是否启用
     */

    private Boolean isStart;

    /**
     * 加征比例
     */

    private BigDecimal levyVal;

    private LocalDateTime dateCreated;


}
