package com.tsfyun.scm.system.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@Data
public class CustomsRateVO implements Serializable {

    private String id;

    /**
     * 币制ID
     */
    @ApiModelProperty(value = "币制编码")
    private String currencyId;

    /**
     * 币制名称
     */
    @ApiModelProperty(value = "币制名称")
    private String currencyName;

    /**
     * 公布时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "公布时间")
    private LocalDateTime publishDate;

    /**
     * 汇率日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "汇率日期")
    private Date rateDate;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime dateCreated;

    /**
     * 汇率
     */
    @ApiModelProperty(value = "汇率")
    private BigDecimal rate;

}
