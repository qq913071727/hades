package com.tsfyun.scm.system.service;

import com.tsfyun.scm.system.entity.CustomsData;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.vo.CustomerDataS1VO;
import com.tsfyun.scm.system.vo.CustomerDataS2VO;
import com.tsfyun.scm.system.vo.CustomsDataVO;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 海关基础数据 服务类
 * </p>
 *

 * @since 2020-03-20
 */
public interface ICustomsDataService extends IService<CustomsData> {

    //数据同步
    void synchroData();

    //根据类型删除数据
    void deleteByType(String category);
    //根据类型查询数据格式化
    List<CustomerDataS1VO> formatData1ByCategory(String category);
    List<CustomerDataS2VO> formatData2ByCategory(String category);
    /**
     * 清楚缓存
     */
    public void clear( );
}
