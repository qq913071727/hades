package com.tsfyun.scm.system.mapper;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.system.entity.BaseData;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 基础数据配置 Mapper 接口
 * </p>
 *

 * @since 2020-04-08
 */
@Repository
public interface BaseDataMapper extends Mapper<BaseData> {

}
