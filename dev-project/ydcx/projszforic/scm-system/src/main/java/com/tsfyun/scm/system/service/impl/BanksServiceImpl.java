package com.tsfyun.scm.system.service.impl;

import com.tsfyun.scm.system.entity.Banks;
import com.tsfyun.scm.system.mapper.BanksMapper;
import com.tsfyun.scm.system.service.IBanksService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.system.vo.BanksVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *

 * @since 2020-03-20
 */
@Service
public class BanksServiceImpl extends ServiceImpl<Banks> implements IBanksService {

    @Autowired
    private BanksMapper banksMapper;

    @Override
    public List<BanksVO> findByName(String name) {
        return banksMapper.findByName(name);
    }
}
