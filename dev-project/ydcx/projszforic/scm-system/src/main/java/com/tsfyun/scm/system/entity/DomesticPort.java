package com.tsfyun.scm.system.entity;

import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;


/**
 * <p>
 * 国内港口
 * </p>
 *

 * @since 2020-03-20
 */
@Data
public class DomesticPort  implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    private String code;


    private String name;


    private String nameEn;


}
