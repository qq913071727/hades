package com.tsfyun.scm.system.service.impl;

import com.tsfyun.common.base.constant.CacheConstant;
import com.tsfyun.scm.system.entity.Port;
import com.tsfyun.scm.system.mapper.PortMapper;
import com.tsfyun.scm.system.service.IPortService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.system.vo.PortVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 港口代码 服务实现类
 * </p>
 *

 * @since 2020-03-20
 */
@Slf4j
@Service
public class PortServiceImpl extends ServiceImpl<Port> implements IPortService {

    @Autowired
    private PortMapper portMapper;

    //@Cacheable(value = CacheConstant.PORT_LIST,key = "#keyword")
    @Override
    public List<PortVO> list(String keyword) {
        List<Port> list = portMapper.list(keyword);
        return beanMapper.mapAsList(list,PortVO.class);
    }

    @CacheEvict(value = CacheConstant.PORT_LIST,allEntries = true,beforeInvocation = true)
    @Override
    public void clear() {
        log.info("清除港口缓存数据成功");
    }
}
