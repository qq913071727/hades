package com.tsfyun.scm.system.mapper;

import com.tsfyun.scm.system.entity.CustomsData;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.system.vo.CustomerDataS1VO;
import com.tsfyun.scm.system.vo.CustomerDataS2VO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 海关基础数据 Mapper 接口
 * </p>
 *

 * @since 2020-03-20
 */
@Repository
public interface CustomsDataMapper extends Mapper<CustomsData> {

    void deleteByType(@Param(value = "category") String category);
    Integer saveBatch(@Param(value = "customsDataList") List<CustomsData> customsDataList);

    List<CustomerDataS1VO> formatData1ByCategory(@Param(value = "category")String category);
    List<CustomerDataS2VO> formatData2ByCategory(@Param(value = "category")String category);
}
