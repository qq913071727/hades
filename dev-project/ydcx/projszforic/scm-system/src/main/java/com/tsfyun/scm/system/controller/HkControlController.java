package com.tsfyun.scm.system.controller;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.system.dto.HkControlDTO;
import com.tsfyun.scm.system.service.IHkControlService;
import com.tsfyun.scm.system.vo.HkControlVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 *  香港管制物品前端控制器
 * </p>
 *

 * @since 2020-03-19
 */
@RestController
@RequestMapping("/hkControl")
public class HkControlController extends BaseController {

    @Autowired
    private IHkControlService controlService;

    @PostMapping(value = "/list")
    public Result<List<HkControlVO>> list(HkControlDTO dto){
        return controlService.list(dto);
    }

    //数据导入
    @PostMapping(value = "/importData")
    public Result<Void> importData(HttpServletRequest request){
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile importFile = multipartRequest.getFile("file");
        controlService.importData(importFile);
        return success();
    }
}

