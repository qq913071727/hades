package com.tsfyun.scm.system.service;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.dto.CustomsRateDTO;
import com.tsfyun.scm.system.entity.Currency;
import com.tsfyun.scm.system.entity.CustomsRate;
import com.tsfyun.scm.system.vo.CustomsRateVO;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 海关汇率服务接口类
 */
public interface ICustomsRateService extends IService<CustomsRate> {

    //分页列表
    PageInfo<CustomsRate> page(CustomsRateDTO dto);

    //新增
    void add(CustomsRateDTO dto);

    //修改
    void edit(CustomsRateDTO dto);

    //删除
    void delete(String id);

    //详情
    CustomsRateVO detail(String id);
    //获取海关汇率
    BigDecimal obtain(String currencyId, Date date);
    //获取海关汇率
    BigDecimal obtain(String currencyId);

    /**
     * 抓取汇率
     * @param date
     * @param currency
     */
    void grabRate(Date date, Currency currency);

    /**
     * 获取当月所有币制的汇率
     * @return
     */
    List<CustomsRate> getNowAllCurrencyRate();

    /**
     * 客户端海关汇率显示
     * @return
     */
    List<CustomsRateVO> clientCustomsRate(String queryMonth);

    /**
     * 手动触发抓取
     */
    void grab();

}
