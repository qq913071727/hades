package com.tsfyun.scm.system.service;

import com.tsfyun.scm.system.entity.AdministrativeDivision;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.vo.AdministrativeDivisionVO;

import java.util.List;

/**
 * <p>
 * 行政区划表 服务类
 * </p>
 *

 * @since 2020-03-20
 */
public interface IAdministrativeDivisionService extends IService<AdministrativeDivision> {

    /**
     * 按照编码或者名称模糊搜索
     * @param keyword
     * @return
     */
    List<AdministrativeDivisionVO> list(String keyword);

    /**
     * 清除缓存
     */
    void clear();

}
