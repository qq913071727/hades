package com.tsfyun.scm.system.controller;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.system.service.IAdministrativeDivisionService;
import com.tsfyun.scm.system.vo.AdministrativeDivisionVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 行政区划表 前端控制器
 * </p>
 *

 * @since 2020-03-20
 */
@RestController
@RequestMapping("/administrativeDivision")
public class AdministrativeDivisionController extends BaseController {

    @Autowired
    private IAdministrativeDivisionService administrativeDivisionService;

    @ApiOperation(value = "列表数据")
    @PostMapping(value = "select")
    public Result<List<AdministrativeDivisionVO>> list(@RequestParam(value = "keyword",required = false) String keyword){
        return success(administrativeDivisionService.list(StringUtils.null2EmptyWithTrim(keyword)));
    }

    /**
     * 暂不缓存
     */
    @ApiOperation(value = "清除缓存")
    @GetMapping(value = "clear")
    public Result<Void> clear( ){
        administrativeDivisionService.clear( );
        return success();
    }

}

