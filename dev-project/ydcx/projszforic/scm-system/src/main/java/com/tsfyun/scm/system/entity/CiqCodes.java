package com.tsfyun.scm.system.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-19
 */
@Data
public class CiqCodes implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    /**
     * 商检编码名称
     */

    private String name;

    /**
     * 海关编码
     */

    private String hsCode;

    /**
     * 商检编码
     */

    private String code;

    /**
     * 类型名称
     */

    private String typeName;

    /**
     * 是否有效
     */

    private Boolean effective;

    /**=
     * 更新时间
     */
    private LocalDateTime dateCreated;


}
