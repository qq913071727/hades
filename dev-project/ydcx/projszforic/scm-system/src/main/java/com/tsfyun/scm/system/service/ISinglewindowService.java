package com.tsfyun.scm.system.service;

public interface ISinglewindowService {

    // 刷新单一海关编码
    void refreshAllHsCode();
    // 刷新单一申报要素
    void refreshElements();
}
