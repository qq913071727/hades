package com.tsfyun.scm.system.service;

import com.tsfyun.scm.system.dto.DistrictNameDto;
import com.tsfyun.scm.system.vo.SimpleDistrictVO;

import java.util.List;

public interface IDistrictService {
    //获取所有省数据
    List<DistrictNameDto> findAllProvince();
    //根据省查询市信息
    List<DistrictNameDto> findCityByProvince(String pname);
    //根据省市查询区信息
    List<DistrictNameDto> findAreaByProvinceAndCity(String pname, String cname);
    //获取所有省市区数据
    List<SimpleDistrictVO> findAll();
}
