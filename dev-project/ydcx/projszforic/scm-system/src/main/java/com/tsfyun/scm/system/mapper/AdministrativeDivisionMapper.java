package com.tsfyun.scm.system.mapper;

import com.tsfyun.scm.system.entity.AdministrativeDivision;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 行政区划表 Mapper 接口
 * </p>
 *

 * @since 2020-03-20
 */
@Repository
public interface AdministrativeDivisionMapper extends Mapper<AdministrativeDivision> {

    List<AdministrativeDivision> list(@Param(value = "keyword") String keyword);

}
