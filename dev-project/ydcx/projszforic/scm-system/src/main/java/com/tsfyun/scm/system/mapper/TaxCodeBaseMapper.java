package com.tsfyun.scm.system.mapper;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.system.entity.TaxCodeBase;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 税收分类编码 Mapper 接口
 * </p>
 *

 * @since 2020-05-15
 */
@Repository
public interface TaxCodeBaseMapper extends Mapper<TaxCodeBase> {

}
