package com.tsfyun.scm.system.controller;


import cn.hutool.core.collection.CollUtil;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.system.service.ITaxCodeBaseService;
import com.tsfyun.scm.system.vo.TaxCodeBaseVO;
import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 税收分类编码 前端控制器
 * </p>
 *

 * @since 2020-05-15
 */
@RestController
@RequestMapping("/taxCodeBase")
public class TaxCodeBaseController extends BaseController {

    @Autowired
    private ITaxCodeBaseService taxCodeBaseService;

    /**
     * 获取所有的税收分类编码
     * @return
     */
    @GetMapping(value = "select")
    public Result<List<TaxCodeBaseVO>> select() {
        return success(taxCodeBaseService.select());
    }

    /**
     * 明细
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    public Result<TaxCodeBaseVO> detail(@RequestParam(value = "id")String id) {
        return success(taxCodeBaseService.findByCode(id));
    }

    /**
     * 模糊搜索税收分类编码，取前20条
     * @return
     */
    @GetMapping(value = "search")
    public Result<List<TaxCodeBaseVO>> search(@RequestParam(value = "code")String code) {
        List<TaxCodeBaseVO> all = taxCodeBaseService.select();
        List<List<TaxCodeBaseVO>> lists = Lists.partition(Lists.newArrayList(Iterables.filter(all, taxCodeBaseVO -> taxCodeBaseVO.getId().indexOf(code) != -1)),20);
        return success(CollUtil.isNotEmpty(lists) ? lists.get(0) : null);
    }

}

