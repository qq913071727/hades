package com.tsfyun.scm.system.mapper;

import com.tsfyun.scm.system.entity.DomesticDistrict;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 国内地区 Mapper 接口
 * </p>
 *

 * @since 2020-03-20
 */
@Repository
public interface DomesticDistrictMapper extends Mapper<DomesticDistrict> {

    List<DomesticDistrict> list(@Param(value = "keyword") String keyword);

}
