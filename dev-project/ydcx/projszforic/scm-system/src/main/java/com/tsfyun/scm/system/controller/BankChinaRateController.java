package com.tsfyun.scm.system.controller;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.util.DateUtils;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.system.dto.BankChinaRateDTO;
import com.tsfyun.scm.system.entity.BankChinaRate;
import com.tsfyun.scm.system.entity.Currency;
import com.tsfyun.scm.system.service.IBankChinaRateService;
import com.tsfyun.scm.system.service.ICurrencyService;
import com.tsfyun.scm.system.vo.BankChinaRateVO;
import com.tsfyun.scm.system.vo.IndexHalfMonthRateVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  中行汇率前端控制器
 * </p>
 *

 * @since 2020-03-17
 */
@Slf4j
@RestController
@RequestMapping("/bankChinaRate")
@Api(tags = "中行汇率")
public class BankChinaRateController extends BaseController {

    @Autowired
    private IBankChinaRateService bankChinaRateService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    @Autowired
    private ICurrencyService currencyService;


    /**
     * 分页查询
     * @param dto
     * @return
     */
    @ApiIgnore
    @PostMapping(value = "list")
    public Result<List<BankChinaRateVO>> pageList(@ModelAttribute BankChinaRateDTO dto) {
        PageInfo<BankChinaRate> page = bankChinaRateService.page(dto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),BankChinaRateVO.class));
    }

    /**
     * 新增
     */
    @ApiIgnore
    @PostMapping("/add")
    public Result<Void> add(@ModelAttribute @Valid BankChinaRateDTO dto){
        bankChinaRateService.add(dto);
        return success();
    }

    /**
     * 修改
     */
    @ApiIgnore
    @PostMapping("/edit")
    public Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) BankChinaRateDTO dto){
        bankChinaRateService.edit(dto);
        return success();
    }

    /**
     * 删除（真删）
     */
    @ApiIgnore
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam(value = "id") String id){
        bankChinaRateService.delete(id);
        return success();
    }

    /**
     * 详情
     */
    @ApiIgnore
    @GetMapping("/detail")
    public Result<BankChinaRateVO> detail(@RequestParam(value = "id")String id){
        return success(bankChinaRateService.detail(id));
    }

    /**=
     * 获取汇率
     * @return
     */
    @ApiOperation(value = "根据币制、汇率时间类型、汇率时间获取汇率")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currencyId", value = "币制", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "rateType", value = "汇率时间类型", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "date", value = "汇率时间", required = true, paramType = "query", dataType = "String")
    })
    @PostMapping("/obtain")
    public Result<BigDecimal> obtain(@RequestParam(value = "currencyId")String currencyId,@RequestParam(value = "rateType")String rateType,@RequestParam(value = "date") String date){
        return success(bankChinaRateService.obtain(currencyId,rateType,date));
    }

    /**
     * 最近十五天的中行汇率（不传参取9点30）
     */
    @ApiIgnore
    @GetMapping("/recentlyHalfMonthBankRate")
    public Result<List<IndexHalfMonthRateVO>> recentlyHalfMonthBankRate(@RequestParam(value = "rateTime",required = false)String rateTime){
        return success(bankChinaRateService.recentlyHalfMonthBankRate(rateTime));
    }

    /**
     * 当日中行汇率（不传参取取当日9点30）
     */
    @ApiOperation(value = "当日中行汇率",notes="不传参数取当日9点30")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "rateTime", value = "汇率时间类型", required = false, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "rateDate", value = "汇率时间", required = false, paramType = "query", dataType = "String")
    })
    @GetMapping("/currentBankRate")
    public Result<List<IndexHalfMonthRateVO>> currentBankRate(@RequestParam(value = "rateTime",required = false)String rateTime,
                                                              @RequestParam(value = "rateDate",required = false)String rateDate){
        return success(bankChinaRateService.currentBankRate(rateTime,rateDate));
    }

    /**
     * 手动抓取汇率
     */
    @ApiIgnore
    @GetMapping("/grab")
    public Result<Void> grab( ){
        new Thread(()->{
            bankChinaRateService.grab();
        }).start();
        return success();
    }

}

