package com.tsfyun.scm.system.service;

import com.tsfyun.common.base.extension.IService;
import com.tsfyun.common.base.vo.DecDataVO;
import com.tsfyun.scm.system.entity.BaseData;
import com.tsfyun.scm.system.vo.BaseDataVO;
import org.springframework.lang.NonNull;

import java.util.List;

/**
 * <p>
 * 基础数据配置 服务类
 * </p>
 *

 * @since 2020-04-08
 */
public interface IBaseDataService extends IService<BaseData> {

    /**
     * 根据类型获取基础数据
     * @param type
     * @return
     */
    List<BaseDataVO> getByType(@NonNull String type);

    /**=
     * 同步海关数据
     */
    void synchronizeCustomsData();

    /**
     * 清除缓存
     */
    void clear();
}
