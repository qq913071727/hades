package com.tsfyun.scm.system.service;

import com.tsfyun.scm.system.entity.DomesticDistrict;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.vo.DomesticDistrictVO;

import java.util.List;

/**
 * <p>
 * 国内地区 服务类
 * </p>
 *

 * @since 2020-03-20
 */
public interface IDomesticDistrictService extends IService<DomesticDistrict> {


    List<DomesticDistrictVO> list(String keyword);

    void clear();

}
