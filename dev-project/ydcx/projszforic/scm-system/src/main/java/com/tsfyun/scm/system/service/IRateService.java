package com.tsfyun.scm.system.service;

import com.tsfyun.scm.system.vo.IndexCustomsRatePlusVO;
import com.tsfyun.scm.system.vo.RateSimpleVO;

import java.util.List;
import java.util.Map;

/**
 * @Description: 汇率服务接口
 * @CreateDate: Created in 2020/5/31 16:16
 */
public interface IRateService {

    Map<String, List<RateSimpleVO>> currentRate(String rateTime);

    /**
     * 首页海关汇率
     * @param rateMonth
     * @return
     */
    IndexCustomsRatePlusVO customsExchangeRate(String rateMonth);

    /**
     * 首页中行汇率
     * @param spotSelling
     * @param obcRateTime
     * @param rateDate（如果为空会取当前日期）
     * @return
     */
    List<RateSimpleVO> obcRate(String spotSelling,String obcRateTime,String rateDate);
}
