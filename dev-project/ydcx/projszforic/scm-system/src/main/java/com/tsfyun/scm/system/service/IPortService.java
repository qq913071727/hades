package com.tsfyun.scm.system.service;

import com.tsfyun.scm.system.entity.Port;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.vo.PortVO;

import java.util.List;

/**
 * <p>
 * 港口代码 服务类
 * </p>
 *

 * @since 2020-03-20
 */
public interface IPortService extends IService<Port> {

    /**
     * 获取下拉，一次性获取20条
     * @param keyword
     * @return
     */
    List<PortVO> list(String keyword);

    /**
     * 清除缓存
     */
    void clear();


}
