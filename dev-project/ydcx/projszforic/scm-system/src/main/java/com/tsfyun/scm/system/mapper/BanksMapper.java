package com.tsfyun.scm.system.mapper;

import com.tsfyun.scm.system.entity.Banks;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.system.vo.BanksVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-20
 */
@Repository
public interface BanksMapper extends Mapper<Banks> {

    List<BanksVO> findByName(@Param(value = "name") String name);

}
