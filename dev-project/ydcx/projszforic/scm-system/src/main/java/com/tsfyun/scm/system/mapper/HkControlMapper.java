package com.tsfyun.scm.system.mapper;

import com.tsfyun.scm.system.dto.HkControlDTO;
import com.tsfyun.scm.system.entity.HkControl;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.system.vo.HkControlVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-19
 */
@Repository
public interface HkControlMapper extends Mapper<HkControl> {

    //列表查询
    List<HkControlVO> list(HkControlDTO dto);

    Integer count(HkControlDTO dto);

    //批量保存或更新
    int saveOrUpdateList(@Param(value = "hkControls")List<HkControl> hkControls);
}
