package com.tsfyun.scm.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.constant.CacheConstant;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.extension.OrderItem;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.system.dto.CountryDTO;
import com.tsfyun.scm.system.entity.Country;
import com.tsfyun.scm.system.mapper.CountryMapper;
import com.tsfyun.scm.system.service.ICountryService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.system.util.TsfWeekendSqls;
import com.tsfyun.scm.system.vo.CountryVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *

 * @since 2020-03-19
 */
@Slf4j
@Service
public class CountryServiceImpl extends ServiceImpl<Country> implements ICountryService {

    @Autowired
    private OrikaBeanMapper beanMapper;

    @Autowired
    private CountryMapper countryMapper;

    @Cacheable(value = CacheConstant.COUNTRY_LIST,key = "'select'")
    @Override
    public List<CountryVO> select( ) {
        Country condition = new Country();
        condition.setDisabled(Boolean.FALSE);
        List<Country> list = super.list(condition);
        if(CollectionUtil.isNotEmpty(list)) {
            list = list.stream().sorted(Comparator.comparing(Country::getSort).reversed()).collect(Collectors.toList());
            return beanMapper.mapAsList(list,CountryVO.class);
        }
        return Lists.newArrayList();
    }

    @CacheEvict(value = CacheConstant.COUNTRY_LIST,allEntries = true,beforeInvocation = true)
    @Override
    public void clear() {
        log.info("清除国家缓存成功");
    }

    @Override
    public PageInfo<Country> page(CountryDTO dto) {
        Example example = new Example(Country.class);
        if (StringUtils.isNotEmpty(dto.getKeyword())) {
            Example.Criteria c = example.createCriteria();
            c.orLike("id", StrUtil.format("%{}%",dto.getKeyword())).orLike("name", StrUtil.format("%{}%",dto.getKeyword()));
        }
        if(Objects.nonNull(dto.getDisabled())) {
            example.and(example.createCriteria().andEqualTo("disabled",dto.getDisabled()));
        }
        PageHelper.startPage(dto.getPage(),dto.getLimit(),"sort desc");
        List<Country> countries = countryMapper.selectByExample(example);
        return new PageInfo<>(countries);
    }

    @Override
    public CountryVO detail(String id) {
        Country country = super.getById(id);
        TsfPreconditions.checkArgument(Objects.nonNull(country),new ServiceException("国家信息信息不存在"));
        return beanMapper.map(country,CountryVO.class);
    }
}
