package com.tsfyun.scm.system.controller;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.system.service.IRateService;
import com.tsfyun.scm.system.vo.IndexCustomsRatePlusVO;
import com.tsfyun.scm.system.vo.RateSimpleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @CreateDate: Created in 2020/6/1 10:11
 */
@RestController
@RequestMapping(value = "/rate")
public class RateController extends BaseController {

    @Autowired
    private IRateService rateService;


    /**
     * 首页海关汇率和中行汇率
     * @param rateTime
     * @return
     */
    @GetMapping("/index")
    public Result<Map<String, List<RateSimpleVO>>> indexCustomsAndBankRate(@RequestParam(value = "rateTime",required = false)String rateTime){
        return success(rateService.currentRate(rateTime));
    }

    /**
     * 首页海关汇率
     * @param rateMonth
     * @return
     */
    @GetMapping("/customsExchangeRate")
    public Result<IndexCustomsRatePlusVO> customsExchangeRate(@RequestParam(value = "rateMonth",required = false)String rateMonth){
        return success(rateService.customsExchangeRate(rateMonth));
    }

    /**
     * 首页中行汇率
     * @param obcRateType
     * @param obcRateTime
     * @return
     */
    @GetMapping("/obcRate")
    public Result<List<RateSimpleVO>> obcRate(
            @RequestParam(value = "obcRateType",required = false,defaultValue = "spotSelling")String obcRateType,
            @RequestParam(value = "obcRateTime",required = false,defaultValue = "09:30")String obcRateTime,
            @RequestParam(value = "rateDate",required = false)String rateDate
    ){
        return success(rateService.obcRate(obcRateType,obcRateTime,rateDate));
    }
}
