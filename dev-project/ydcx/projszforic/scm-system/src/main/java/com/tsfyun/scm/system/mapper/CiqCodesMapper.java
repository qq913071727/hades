package com.tsfyun.scm.system.mapper;

import com.tsfyun.scm.system.dto.CiqCodesDTO;
import com.tsfyun.scm.system.entity.CiqCodes;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.system.vo.CiqCodeVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-19
 */
@Repository
public interface CiqCodesMapper extends Mapper<CiqCodes> {

    List<CiqCodes> list(CiqCodesDTO dto);

    List<CiqCodeVo> findByHsCode(@Param(value = "hsCode") String hsCode);

}
