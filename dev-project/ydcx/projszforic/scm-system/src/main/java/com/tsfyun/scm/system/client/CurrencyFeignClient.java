package com.tsfyun.scm.system.client;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.vo.CurrencyVO;
import com.tsfyun.scm.system.client.fallback.CurrencyFeignClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;


@Component
@FeignClient(name = "scm-saas",path = "currency",fallbackFactory = CurrencyFeignClientFallback.class)
public interface CurrencyFeignClient {

    /**
     * 获取所有启用币制（所有的币制，启用的，不分页）
     * @return
     */
    @GetMapping(value = "select")
    Result<List<CurrencyVO>> select( );

}
