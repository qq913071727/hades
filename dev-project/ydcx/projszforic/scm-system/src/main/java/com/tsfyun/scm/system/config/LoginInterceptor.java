//package com.tsfyun.scm.system.config;
//
//import cn.hutool.core.util.ArrayUtil;
//import com.alibaba.fastjson.JSONObject;
//import com.tsfyun.common.base.constant.LoginConstant;
//import com.tsfyun.common.base.dto.Result;
//import com.tsfyun.common.base.util.ResultUtil;
//import com.tsfyun.common.base.util.StringUtils;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.context.config.annotation.RefreshScope;
//import org.springframework.stereotype.Component;
//import org.springframework.util.AntPathMatcher;
//import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// * 登录拦截器
// */
//@RefreshScope
//@Component
//@Slf4j
//public class LoginInterceptor extends HandlerInterceptorAdapter {
//
//    @Autowired
//    private PermitUrlProperties permitUrlProperties;
//
//    @Autowired
//    private StringRedisUtils stringRedisUtils;
//
//    private AntPathMatcher pathMatcher = new AntPathMatcher();
//
//    private static final String[] outsideIgnoreUrls = {"/v2/api-docs","/configuration/ui","/swagger-resources","/configuration/security",
//        "/swagger-ui.html","/webjars/**","/doc.html","/static/images/**"};
//
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        String[] ignores = permitUrlProperties.getIgnored();
//        String[] ignoreUrls = ArrayUtil.addAll(ignores,outsideIgnoreUrls);
//        String token = request.getHeader("token");
//        String url = request.getRequestURI();
//        boolean isLoginFlag = true;
//        boolean noNeedLogin = false;
//        String errmsg = "请登录";
//        if(ArrayUtil.isNotEmpty(ignoreUrls)) {
//            for (String ignore : ignoreUrls) {
//                //通配符比较
//                if(pathMatcher.match(ignore,url)) {
//                    noNeedLogin = true;
//                    break;
//                }
//            }
//        }
//        //无需登录直接放行
//        if(noNeedLogin) {
//            return true;
//        }
//        //需要登录但是未携带token
//        if( StringUtils.isEmpty(token)) {
//            log.error("未携带token");
//            isLoginFlag = false;
//        } else {
//            String tokenInfo =  stringRedisUtils.getToString(LoginConstant.TOKEN  + token);
//            if(StringUtils.isEmpty(tokenInfo)) {
//                log.error("携带的token：{}未查询到登录信息",token);
//                isLoginFlag = false;
//            } else {
//                //已经改为对象了
//                JSONObject tokenJsonObject =JSONObject.parseObject(tokenInfo);
//                Long originPersonId = tokenJsonObject.getLong("originPersonId");
//                Long personId = tokenJsonObject.getLong("personId");
//                String authPerson = stringRedisUtils.getToString(LoginConstant.AUTH + personId);
//                if(StringUtils.isEmpty(authPerson)) {
//                    log.error("携带的token：{}对应的人员id信息未查询到人员信息",token);
//                    isLoginFlag = false;
//                } else {
//
//                }
//            }
//        }
//        if(!isLoginFlag) {
//            Result result = Result.error(HttpServletResponse.SC_UNAUTHORIZED + "",errmsg);
//            ResultUtil.errorBack(response,result);
//            return false;
//        }
//        return true;
//    }
//
//
//
//}
