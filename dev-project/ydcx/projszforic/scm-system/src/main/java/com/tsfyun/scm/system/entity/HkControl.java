package com.tsfyun.scm.system.entity;

import java.time.LocalDateTime;
import lombok.Data;

import javax.persistence.Id;


/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-19
 */
@Data
public class HkControl {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    /**
     * 品牌
     */

    private String brand;

    /**
     * 产品说明描述
     */

    private String description;

    /**
     * 是否管制
     */

    private Boolean isControl;

    /**
     * 型号
     */

    private String model;

    /**
     * 同步时间
     */

    private LocalDateTime synchroDate;

    /**
     * 管制类别
     */

    private String type;


}
