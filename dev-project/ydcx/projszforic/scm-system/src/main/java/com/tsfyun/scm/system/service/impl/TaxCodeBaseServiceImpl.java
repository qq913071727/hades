package com.tsfyun.scm.system.service.impl;

import com.tsfyun.common.base.constant.CacheConstant;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.system.entity.TaxCodeBase;
import com.tsfyun.scm.system.service.ITaxCodeBaseService;
import com.tsfyun.scm.system.vo.TaxCodeBaseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 税收分类编码 服务实现类
 * </p>
 *

 * @since 2020-05-15
 */
@Slf4j
@Service
public class TaxCodeBaseServiceImpl extends ServiceImpl<TaxCodeBase> implements ITaxCodeBaseService {

    @Override
    public TaxCodeBaseVO findByCode(String taxCode) {
        TaxCodeBase taxCodeBase = super.getById(taxCode);
        Optional.ofNullable(taxCodeBase).orElseThrow(()->new ServiceException("税收分类编码不存在"));
        return beanMapper.map(taxCodeBase,TaxCodeBaseVO.class);
    }



    @Cacheable(value = CacheConstant.TAX_CODE_LIST,key = "'select'")
    @Override
    public List<TaxCodeBaseVO> select() {
        List<TaxCodeBase> list = super.list();
        return beanMapper.mapAsList(list,TaxCodeBaseVO.class);
    }

    @CacheEvict(value = CacheConstant.TAX_CODE_LIST,allEntries = true,beforeInvocation = true)
    @Override
    public void clear() {
        log.info("清除税收分类编码缓存成功");
    }

}
