package com.tsfyun.scm.system.controller;
import com.tsfyun.common.base.constant.CacheConstant;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.system.config.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Objects;

//数据版本，控制前端数据缓存
@RestController
@Slf4j
public class VersionController {

    @Autowired
    private RedisUtils redisUtils;

    @GetMapping(value = "version")
    public Result<String> newVersion(){
        String version = new Date().getTime()+"";
        try{
            Object obj = redisUtils.get(CacheConstant.VERSION);
            if(Objects.nonNull(obj)){
                version = obj.toString();
            }else{
                redisUtils.set(CacheConstant.VERSION,version);
            }
        }catch (Exception e){
            log.error("获取基础数据版本失败：",e);
        }
        return Result.success(version);
    }
}
