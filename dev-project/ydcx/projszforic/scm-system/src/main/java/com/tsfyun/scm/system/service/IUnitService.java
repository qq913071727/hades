package com.tsfyun.scm.system.service;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.system.dto.UnitDTO;
import com.tsfyun.scm.system.entity.Unit;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.vo.UnitVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-19
 */
public interface IUnitService extends IService<Unit> {

    /**
     * 分页获取数据
     * @param dto
     * @return
     */
    PageInfo<Unit> list(UnitDTO dto);

    /**
     * 明细数据
     * @param id
     * @return
     */
    UnitVO detail(String id);

    /**
     * 获取单位列表
     * @return
     */
    public List<UnitVO> select ( );

    /**
     * 清楚缓存
     */
    public void clear( );


}
