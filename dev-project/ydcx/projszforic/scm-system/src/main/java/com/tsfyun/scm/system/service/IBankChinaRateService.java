package com.tsfyun.scm.system.service;

import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.dto.BankChinaRateDTO;
import com.tsfyun.scm.system.entity.BankChinaRate;
import com.tsfyun.scm.system.entity.Currency;
import com.tsfyun.scm.system.vo.BankChinaRateVO;
import com.tsfyun.scm.system.vo.IndexHalfMonthRateVO;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-17
 */
public interface IBankChinaRateService extends IService<BankChinaRate> {

    //分页列表
    PageInfo<BankChinaRate> page(BankChinaRateDTO dto);

    //新增
    void add(BankChinaRateDTO dto);

    //修改
    void edit(BankChinaRateDTO dto);

    //删除
    void delete(String id);

    //详情
    BankChinaRateVO detail(String id);

    //根据币制和日期获取当日最晚的一条汇率
    BankChinaRate findLastOne(String currencyCode, String date);

    //根据日期和币制抓取汇率
    void grabRate(String date, Currency currency);
    //获取汇率
    BigDecimal obtain(String currency, String reteType, String data);

    List<IndexHalfMonthRateVO> recentlyHalfMonthBankRate(String rateTime);

    List<IndexHalfMonthRateVO> currentBankRate(String rateTime,String rateDate);

    void grab();
}
