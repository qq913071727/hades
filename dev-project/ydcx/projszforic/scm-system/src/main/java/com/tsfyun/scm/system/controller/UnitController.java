package com.tsfyun.scm.system.controller;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.system.dto.UnitDTO;
import com.tsfyun.scm.system.entity.Unit;
import com.tsfyun.scm.system.service.IUnitService;
import com.tsfyun.scm.system.vo.UnitVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *

 * @since 2020-03-19
 */
@RestController
@RequestMapping("/unit")
public class UnitController extends BaseController {

    @Autowired
    private IUnitService unitService;

    private OrikaBeanMapper beanMapper;

    /**
     * 分页查询
     * @param dto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<UnitVO>> pageList(@ModelAttribute UnitDTO dto) {
        PageInfo<Unit> page = unitService.list(dto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),UnitVO.class));
    }

    /**
     * 详情
     */
    @GetMapping("/detail")
    public Result<UnitVO> detail(@RequestParam(value = "id")String id){
        return success(unitService.detail(id));
    }

    /**
     * 获取单位下拉列表
     * @return
     */
    @GetMapping(value = "select")
    public Result<List<UnitVO>> select() {
        return success(unitService.select());
    }


}

