package com.tsfyun.scm.system.job;

import com.tsfyun.scm.system.service.ICustomsRateService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**=
 * 抓取海关汇率定时器
 */
@JobHandler(value="customsJobHandler")
@Component
@Slf4j
public class CustomsJobHandler extends IJobHandler {

    @Autowired
    private ICustomsRateService customsRateService;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        customsRateService.grab();
        return SUCCESS;
    }
}
