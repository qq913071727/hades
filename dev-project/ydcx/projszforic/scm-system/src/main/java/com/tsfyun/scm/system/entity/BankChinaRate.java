package com.tsfyun.scm.system.entity;

import com.tsfyun.common.base.extension.annotation.InsertFill;
import lombok.Data;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;


/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-17
 */
@Data
public class BankChinaRate {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    /**
     * 币制ID
     */

    private String currencyId;

    /**
     * 币制名称
     */

    private String currencyName;

    /**
     * 公布时间
     */

    private LocalDateTime publishDate;

    /**
     * 现汇买入价
     */

    private BigDecimal buyingRate;

    /**
     * 现钞买入价
     */

    private BigDecimal cashPurchase;

    /**
     * 现汇卖出价
     */

    private BigDecimal spotSelling;

    /**
     * 现钞卖出价
     */

    private BigDecimal cashSelling;

    /**
     * 中行折算价
     */

    private BigDecimal bocConversion;


    @InsertFill
    private LocalDateTime dateCreated;


}
