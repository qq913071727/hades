package com.tsfyun.scm.system.mapper;

import com.tsfyun.scm.system.dto.DistrictNameDto;
import com.tsfyun.scm.system.entity.District;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * DistrictMapper继承基类
 */
@Repository
public interface DistrictMapper {

        //所有省信息
        List<DistrictNameDto> findAllProvince();
        //根据省查询市信息
        List<DistrictNameDto> findCityByProvince(@Param(value = "pname") String pname);
        //根据省市查询区信息
        List<DistrictNameDto> findAreaByProvinceAndCity(@Param(value = "pname") String pname, @Param(value = "cname") String cname);
        //验证省市区是否存在
        int findByAllName(@Param(value = "pname") String pname, @Param(value = "cname") String cname, @Param(value = "aname") String aname);

        List<District> findAll();


}