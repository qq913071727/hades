package com.tsfyun.scm.system.service;

import com.tsfyun.scm.system.entity.Banks;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.vo.BanksVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-20
 */
public interface IBanksService extends IService<Banks> {

    /**
     * 根据名称模糊搜索前20条
     * @param name
     * @return
     */
    List<BanksVO> findByName(String name);

}
