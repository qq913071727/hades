package com.tsfyun.scm.system.job;

import com.tsfyun.common.base.util.DateUtils;
import com.tsfyun.scm.system.entity.Currency;
import com.tsfyun.scm.system.service.IBankChinaRateService;
import com.tsfyun.scm.system.service.ICurrencyService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.List;
import java.util.Objects;

/***
 * 抓取中行汇率任务
 *
 * @date 2019/10/22
 */
@JobHandler(value="exchangeJobHandler")
@Component
@Slf4j
public class ExchangeJobHandler extends IJobHandler {

    @Autowired
    private IBankChinaRateService bankChinaRateService;

    @Autowired
    private ICurrencyService currencyService;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        log.info("定时扫描中行汇率数据开始......");
        try{
            bankChinaRateService.grab();
        } catch (Exception e) {
            log.error("定时扫描中行汇率数据失败",e);
        }
        return SUCCESS;
    }
}