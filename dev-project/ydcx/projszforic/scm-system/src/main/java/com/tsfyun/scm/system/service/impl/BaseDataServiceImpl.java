package com.tsfyun.scm.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.file.FileReader;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.tsfyun.common.base.constant.CacheConstant;
import com.tsfyun.common.base.enums.BaseDataTypeEnum;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.vo.BaseDataSimple;
import com.tsfyun.common.base.vo.DecDataVO;
import com.tsfyun.scm.system.entity.BaseData;
import com.tsfyun.scm.system.mapper.BaseDataMapper;
import com.tsfyun.scm.system.service.IBaseDataService;
import com.tsfyun.scm.system.vo.BaseDataVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 基础数据配置 服务实现类
 * </p>
 *

 * @since 2020-04-08
 */
@Slf4j
@Service
public class BaseDataServiceImpl extends ServiceImpl<BaseData> implements IBaseDataService {


    @Autowired
    private BaseDataMapper baseDataMapper;

    @Cacheable(value = CacheConstant.BASE_DATA_LIST+"|-1",key = "#type")
    @Override
    public List<BaseDataVO> getByType(@NonNull String type) {
        BaseData condition = new BaseData();
        condition.setBaseDataType(type);
        condition.setDisabled(Boolean.FALSE);
        List<BaseData> list = super.list(condition);
        if(CollUtil.isNotEmpty(list)) {
            //按行号排序
            return beanMapper.mapAsList(list.stream().sorted(Comparator.comparing(BaseData::getRowNo)).collect(Collectors.toList()),BaseDataVO.class);
        }
        return new ArrayList<>();
    }

    @Override
    public void synchronizeCustomsData() {
        FileReader fileReader = new FileReader("D:\\java_work\\scm-cloud\\scm-system\\src\\main\\resources\\file\\autocompAllGbSource");
        String text = fileReader.readString();
        List<Map<String,Object>> list = JSONObject.parseObject(text, List.class);
        for(Map<String,Object> map : list){
            analysisData("1",map);
        }

        fileReader = new FileReader("D:\\java_work\\scm-cloud\\scm-system\\src\\main\\resources\\file\\autocompAllLocalSource1");
        text = fileReader.readString();
        list = JSONObject.parseObject(text, List.class);
        for(Map<String,Object> map : list){
            analysisData("2",map);
        }

        fileReader = new FileReader("D:\\java_work\\scm-cloud\\scm-system\\src\\main\\resources\\file\\autocompAllLocalSource2");
        text = fileReader.readString();
        list = JSONObject.parseObject(text, List.class);
        for(Map<String,Object> map : list){
            analysisData("2",map);
        }

    }

    @CacheEvict(value = CacheConstant.BASE_DATA_LIST,allEntries = true,beforeInvocation = true)
    @Override
    public void clear() {
        log.info("基础配置缓存清除成功");
    }

    @Transactional(rollbackFor = Exception.class)
    public void analysisData(String type,Map<String,Object> map){
        BaseDataTypeEnum baseDataTypeEnum = BaseDataTypeEnum.of(map.get("paraName").toString());
        if(Objects.nonNull(baseDataTypeEnum)){
            deleteByType(baseDataTypeEnum);

            List<Map<String,String>> datas = (List<Map<String,String>>)map.get("paraData");
            List<BaseData> baseDataList = Lists.newArrayList();
            for(int i=0;i<datas.size();i++){
                Map<String,String> data = datas.get(i);
                System.out.println(JSONObject.toJSONString(data));
                BaseData baseData = new BaseData();
                baseData.setId(StringUtils.UUID());
                baseData.setBaseDataType(baseDataTypeEnum.getCode());
                if("1".equals(type)){
                    baseData.setDecCode(data.get("gbCode"));
                    baseData.setName(data.get("codeName"));
                }else if("2".equals(type)){
                    baseData.setDecCode(data.get("codeValue"));
                    baseData.setName(data.get("codeName"));
                }
                baseData.setCode(baseData.getBaseDataType().concat("_").concat(baseData.getDecCode()));
                baseData.setDisabled(Boolean.FALSE);
                baseData.setRowNo(i);
                baseDataList.add(baseData);
            }
            super.savaBatch(baseDataList);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteByType(BaseDataTypeEnum baseDataType){
        BaseData delete = new BaseData();
        delete.setBaseDataType(baseDataType.getCode());
        baseDataMapper.delete(delete);
    }

}
