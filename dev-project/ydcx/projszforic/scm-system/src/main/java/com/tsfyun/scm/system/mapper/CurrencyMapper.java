package com.tsfyun.scm.system.mapper;

import com.tsfyun.common.base.vo.CurrencyVO;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.system.entity.Currency;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  币制Mapper 接口
 * </p>
 *

 * @since 2020-03-18
 */
@Repository
public interface CurrencyMapper extends Mapper<Currency> {

    List<CurrencyVO> findAll();
}
