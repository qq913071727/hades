package com.tsfyun.scm.system.controller;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.system.dto.LevyDutiesDTO;
import com.tsfyun.scm.system.entity.LevyDuties;
import com.tsfyun.scm.system.service.ILevyDutiesService;
import com.tsfyun.scm.system.vo.LevyDutiesVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  加征关税前端控制器
 * </p>
 *

 * @since 2020-03-20
 */
@RestController
@RequestMapping("/levyDuties")
public class LevyDutiesController extends BaseController {

    @Autowired
    private ILevyDutiesService levyDutiesService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 分页查询
     * @param dto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<LevyDutiesVO>> pageList(@ModelAttribute LevyDutiesDTO dto) {
        PageInfo<LevyDuties> page = levyDutiesService.page(dto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),LevyDutiesVO.class));
    }

    /**
     * 根据海关编码获取加征关税信息(批量)
     * @param hsCodes
     * @return
     */
    @PostMapping(value = "getByHsCodes")
    public Result<Map<String, List<LevyDutiesVO>>> getByHsCodes(@RequestParam(value = "hsCodes") List<String> hsCodes) {
        return success(levyDutiesService.listByHsCodes(hsCodes));
    }

}

