package com.tsfyun.scm.system.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SimpleDistrictVO implements Serializable {

    private String value;
    private String label;
    private List<SimpleDistrictVO> children;
}
