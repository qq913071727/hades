package com.tsfyun.scm.system.receiver;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.stereotype.Component;

/**
 * @Description: 消息输入通道定义
 * @Project:
 * @CreateDate: Created in 2019/12/16 15:43
 */
@Component
public interface Sink {

    String IN_SCM_DEMO = "scm-demo-input";

    @Input(IN_SCM_DEMO)
    SubscribableChannel testDemo();





}
