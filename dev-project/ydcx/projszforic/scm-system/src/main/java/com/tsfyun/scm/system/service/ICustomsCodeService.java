package com.tsfyun.scm.system.service;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.system.dto.CustomsCodeDTO;
import com.tsfyun.scm.system.dto.CustomsQTO;
import com.tsfyun.scm.system.dto.SelectCustomsCodeVO;
import com.tsfyun.scm.system.entity.CustomsCode;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.vo.CustomsCodeDetailVO;
import com.tsfyun.scm.system.vo.CustomsCodeVO;
import com.tsfyun.scm.system.vo.TariffCustomsCodeVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-19
 */
public interface ICustomsCodeService extends IService<CustomsCode> {

    /**
     * 分页获取海关编码数据
     * @param dto
     * @return
     */
    PageInfo<CustomsCode> list(CustomsQTO dto);

    /**
     * 明细数据
     * @param id
     * @return
     */
    CustomsCodeVO detail(String id);

    /**
     * 获取所有的最惠国税率大于0的海关编码
     * @return
     */
    List<TariffCustomsCodeVO> tariffCustoms();

    /**
     * 获取所有海关编码
     * @return
     */
    List<SelectCustomsCodeVO> selectAll();

    /**
     * 新增海关编码
     * @param dto
     */
    void add(CustomsCodeDTO dto);

    /**
     * 修改海关编码
     * @param dto
     */
    void edit(CustomsCodeDTO dto);

    /**
     * 查询
     * @param id
     * @return
     */
    CustomsCodeDetailVO info(String id);

    /**
     * 删除
     * @param id
     */
    void delete(String id);

    /**
     * 清楚缓存
     */
    public void clear( );

}
