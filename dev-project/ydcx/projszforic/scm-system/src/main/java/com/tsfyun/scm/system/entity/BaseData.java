package com.tsfyun.scm.system.entity;

import lombok.Data;

import javax.persistence.Id;


/**
 * <p>
 * 基础数据配置
 * </p>
 *

 * @since 2020-04-08
 */
@Data
public class BaseData  {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    /**
     * 基础数据类型
     */

    private String baseDataType;

    /**
     * 编码
     */

    private String code;

    /**
     * 上级编码
     */

    private String pcode;

    /**
     * 报关编码
     */

    private String decCode;

    /**
     * 名称
     */

    private String name;

    /**
     * 是否禁用
     */

    private Boolean disabled;

    /**
     * 行号
     */

    private Integer rowNo;


}
