package com.tsfyun.scm.system.service.impl;

import com.tsfyun.common.base.constant.CacheConstant;
import com.tsfyun.scm.system.entity.DomesticPort;
import com.tsfyun.scm.system.mapper.DomesticPortMapper;
import com.tsfyun.scm.system.service.IDomesticPortService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.system.vo.DomesticPortVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 国内港口 服务实现类
 * </p>
 *

 * @since 2020-03-20
 */
@Slf4j
@Service
public class DomesticPortServiceImpl extends ServiceImpl<DomesticPort> implements IDomesticPortService {

    @Autowired
    private DomesticPortMapper domesticPortMapper;

    //@Cacheable(value = CacheConstant.DOMESTIC_PORT_LIST,key = "#keyword")
    @Override
    public List<DomesticPortVO> list(String keyword) {
        List<DomesticPort> list = domesticPortMapper.list(keyword);
        return beanMapper.mapAsList(list,DomesticPortVO.class);
    }

    @CacheEvict(value = CacheConstant.DOMESTIC_PORT_LIST,allEntries = true,beforeInvocation = true)
    @Override
    public void clear() {
        log.info("清除国内港口缓存数据成功");
    }
}
