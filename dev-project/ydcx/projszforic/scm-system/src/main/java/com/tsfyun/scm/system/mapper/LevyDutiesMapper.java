package com.tsfyun.scm.system.mapper;

import com.tsfyun.scm.system.entity.LevyDuties;
import com.tsfyun.common.base.extension.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-20
 */
@Repository
public interface LevyDutiesMapper extends Mapper<LevyDuties> {

}
