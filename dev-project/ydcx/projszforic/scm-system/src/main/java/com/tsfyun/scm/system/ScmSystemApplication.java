package com.tsfyun.scm.system;

import com.tsfyun.scm.log.annotation.EnableLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import tk.mybatis.spring.annotation.MapperScan;

import java.net.InetAddress;

@SpringBootApplication(exclude = {})
@ComponentScan("com.tsfyun")
@MapperScan(basePackages = "com.tsfyun.scm.system.mapper")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.tsfyun.scm.system.client")
@EnableCircuitBreaker
@EnableLog
@Slf4j
public class ScmSystemApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(ScmSystemApplication.class, args);
        Environment env = run.getEnvironment();
        String host = "127.0.0.1";
        try {
            host = InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e) {}
        log.info("\n----------------------------------------------------------\n\t" +
                        "应用 '{}' 运行成功!\n\t" +
                        "Swagger文档: \t\thttp://{}:{}{}{}/doc.html\n\t" +
                        "----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                host,
                env.getProperty("server.port"),
                env.getProperty("server.servlet.context-path", ""),
                env.getProperty("spring.mvc.servlet.path", "")
        );
    }

}
