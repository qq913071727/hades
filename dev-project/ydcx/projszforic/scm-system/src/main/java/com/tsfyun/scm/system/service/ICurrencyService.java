package com.tsfyun.scm.system.service;

import com.tsfyun.common.base.extension.IService;
import com.tsfyun.common.base.vo.CurrencyVO;
import com.tsfyun.scm.system.entity.Currency;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  币制服务类
 * </p>
 *

 * @since 2020-03-18
 */
public interface ICurrencyService extends IService<Currency> {

    /**
     * 获取所有的启用的币制度=下拉框
     * @return
     */
    List<CurrencyVO> select();


    Currency findById(String id);

    Currency findByName(String name);

}
