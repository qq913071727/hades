package com.tsfyun.scm.system.entity;

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;


/**
 * <p>
 * 国内地区
 * </p>
 *

 * @since 2020-03-20
 */
@Data
public class DomesticDistrict  implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    /**
     * 编码
     */

    private String code;

    /**
     * 中文名称
     */

    private String name;

    /**
     * 英文名称
     */

    private String shortName;

    /**
     * 是否禁用，1-是;0-否
     */

    private Boolean disabled;

    /**
     * 国内地区性质标记
     */

    private String propertyFlag;


}
