package com.tsfyun.scm.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.enums.ResultCodeEnum;
import com.tsfyun.common.base.exception.ClientException;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.system.dto.HkControlDTO;
import com.tsfyun.scm.system.entity.HkControl;
import com.tsfyun.scm.system.mapper.HkControlMapper;
import com.tsfyun.scm.system.service.IHkControlService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.system.vo.HkControlVO;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *

 * @since 2020-03-19
 */
@Service
@Slf4j
public class HkControlServiceImpl extends ServiceImpl<HkControl> implements IHkControlService {

    @Autowired
    private HkControlMapper hkControlMapper;

    @Override
    public Result<List<HkControlVO>> list(HkControlDTO dto) {
        Result result = Result.success();
        List<HkControlVO> list  = hkControlMapper.list(dto);
        result.setData(list);
        if(CollUtil.isNotEmpty(list)) {
            Integer count = hkControlMapper.count(dto);
            result.setCount(count);
        }
        return result;
    }

    @Override
    public void importData(MultipartFile importFile) {
        try{
            InputStream inp = importFile.getInputStream();
            Workbook wb = Workbook.getWorkbook(inp);
            Sheet[] sheets = wb.getSheets();
            for(Sheet sheet : sheets){
                saveImportSheet(sheet);
                log.info(String.format("[%s]处理完成",sheet.getName()));
            }
        }catch (Exception e){
            log.error("导入失败：",e);
            throw new ClientException("输入导入失败，请检查格式是否正确");
        }
    }

    public void saveImportSheet(Sheet sheet){
        int totalRows = sheet.getRows();
        List<HkControl> hkControls = new ArrayList<>();
        if(totalRows>4){
            for (int i = 4; i < totalRows; i++) {
                Cell[] cells = sheet.getRow(i);
                HkControl hc = new HkControl();
                hc.setBrand(StringUtils.removeSpecialSymbol(cells[0].getContents()));
                hc.setModel(StringUtils.removeSpecialSymbol(cells[1].getContents()));
                hc.setDescription(StringUtils.removeSpecialSymbol(cells[2].getContents()));
                hc.setType(StringUtils.removeSpecialSymbol(cells[3].getContents()));
                hc.setIsControl(!"NLR".equals(hc.getType().toUpperCase()));
                hc.setSynchroDate(LocalDateTime.now());
                hc.setId(StringUtils.UUID());
                hkControls.add(hc);
            }
        }
        if(CollectionUtil.isNotEmpty(hkControls)){
            hkControlMapper.saveOrUpdateList(hkControls);
        }
    }
}
