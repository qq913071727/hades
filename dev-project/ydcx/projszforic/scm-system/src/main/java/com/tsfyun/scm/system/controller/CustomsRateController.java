package com.tsfyun.scm.system.controller;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.validator.group.UpdateGroup;
import com.tsfyun.scm.system.dto.CustomsRateDTO;
import com.tsfyun.scm.system.entity.CustomsRate;
import com.tsfyun.scm.system.service.ICustomsRateService;
import com.tsfyun.scm.system.vo.CustomsRateVO;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  海关汇率前端控制器
 * </p>
 *

 * @since 2020-03-17
 */
@RestController
@RequestMapping("/customsRate")
@Api(tags = "海关汇率")
public class CustomsRateController extends BaseController {

    @Autowired
    private ICustomsRateService customsRateService;

    @Autowired
    private OrikaBeanMapper beanMapper;


    /**
     * 分页查询
     * @param dto
     * @return
     */
    @ApiIgnore
    @PostMapping(value = "list")
    public Result<List<CustomsRateVO>> pageList(@ModelAttribute CustomsRateDTO dto) {
        PageInfo<CustomsRate> page = customsRateService.page(dto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),CustomsRateVO.class));
    }

    /**
     * 新增
     */
    @ApiIgnore
    @PostMapping("/add")
    public Result<Void> add(@ModelAttribute @Valid CustomsRateDTO dto){
        customsRateService.add(dto);
        return success();
    }

    /**
     * 修改
     */
    @ApiIgnore
    @PostMapping("/edit")
    public Result<Void> edit(@ModelAttribute @Validated(UpdateGroup.class) CustomsRateDTO dto){
        customsRateService.edit(dto);
        return success();
    }

    /**
     * 删除（真删）
     */
    @ApiIgnore
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam(value = "id") String id){
        customsRateService.delete(id);
        return success();
    }

    /**
     * 详情
     */
    @ApiIgnore
    @GetMapping("/detail")
    public Result<CustomsRateVO> detail(@RequestParam(value = "id")String id){
        return success(customsRateService.detail(id));
    }

    /**=
     * 根据币制获取本月海关汇率
     * @param currencyId
     * @return
     */
    @ApiOperation(value = "根据币制获取本月海关汇率")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currencyId", value = "币制", required = true, paramType = "query", dataType = "String")
    })
    @PostMapping("/thisMonth")
    public Result<BigDecimal> thisMonth(@RequestParam(value = "currencyId")String currencyId){
        return success(customsRateService.obtain(currencyId));
    }

    /**=
     * 根据币制和日期获取汇率
     * @param currencyId
     * @param date
     * @return
     */
    @ApiOperation(value = "根据币制和日期获取汇率")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currencyId", value = "币制", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "date", value = "日期", required = true, paramType = "query", dataType = "String")
    })
    @PostMapping("/obtain")
    public Result<BigDecimal> obtain(@RequestParam(value = "currencyId")String currencyId,@RequestParam(value = "date") Date date){
        return success(customsRateService.obtain(currencyId,date));
    }

    /**=
     * 根据月份获取海关汇率
     * @param date
     * @return
     */
    @ApiOperation(value = "根据月份获取海关汇率集合")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "date", value = "月份", required = false, paramType = "query", dataType = "String",example = "2020-12")
    })
    @PostMapping("/obtainByDate")
    public Result<List<CustomsRateVO>> obtainByDate(@RequestParam(value = "date",required = false) String date){
        return success(customsRateService.clientCustomsRate(date));
    }

    /**
     * 手动抓取汇率
     */
    @ApiIgnore
    @GetMapping("/grab")
    public Result<Void> grab( ){
        new Thread(()->{
            customsRateService.grab();
        }).start();
        return success();
    }

}

