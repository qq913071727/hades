package com.tsfyun.scm.system.mapper;

import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.system.dto.CustomsRateDTO;
import com.tsfyun.scm.system.entity.CustomsRate;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  海关汇率Mapper 接口
 * </p>
 *

 * @since 2020-03-17
 */
@Repository
public interface CustomsRateMapper extends Mapper<CustomsRate> {

    /**
     * 修改
     * @param dto
     */
    void update(@Param("dto") CustomsRateDTO dto);

}
