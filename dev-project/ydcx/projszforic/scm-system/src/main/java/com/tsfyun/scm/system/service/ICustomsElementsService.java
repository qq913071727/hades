package com.tsfyun.scm.system.service;

import com.tsfyun.scm.system.entity.CustomsCode;
import com.tsfyun.scm.system.entity.CustomsElements;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.vo.CustomsElementsVO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-19
 */
public interface ICustomsElementsService extends IService<CustomsElements> {

    List<CustomsElementsVO> findByHsCode(String hsCode);

    /**
     * 删除并新增海关编码要素信息
     * @param oldHsCode
     * @param nowHsCode
     * @param isNeeds
     */
    void deleteAndSaveElementsByHsCode(String oldHsCode,String nowHsCode, List<String> elementsName, List<String> isNeeds);

    List<Map<String,String>> findByName(String name);

    /**
     * 根据海关编码删除要素
     * @param hsCode
     */
    void deleteByCustomsCode(String hsCode);

    /**
     * 清楚缓存
     */
    public void clear( );
}
