package com.tsfyun.scm.system.entity;

import com.tsfyun.common.base.extension.annotation.InsertFill;
import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 海关汇率
 */
@Data
public class CustomsRate implements Serializable {

    @Id
    private String id;

    /**
     * 币制ID
     */
    private String currencyId;

    /**
     * 币制名称
     */
    private String currencyName;

    /**
     * 公布时间
     */
    private LocalDateTime publishDate;

    /**
     * 汇率日期
     */
    private Date rateDate;

    /**
     * 创建时间
     */
    @InsertFill
    private LocalDateTime dateCreated;

    /**
     * 汇率
     */
    private BigDecimal rate;

}