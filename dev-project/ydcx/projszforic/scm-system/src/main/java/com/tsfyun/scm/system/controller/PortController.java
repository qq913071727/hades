package com.tsfyun.scm.system.controller;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.system.service.IPortService;
import com.tsfyun.scm.system.vo.PortVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 * 港口代码 前端控制器
 * </p>
 *

 * @since 2020-03-20
 */
@RestController
@RequestMapping("/port")
public class PortController extends BaseController {

    @Autowired
    private IPortService portService;

    @ApiOperation(value = "港口列表数据")
    @PostMapping(value = "select")
    public Result<List<PortVO>> list(@RequestParam(value = "keyword",required = false) String keyword){
        return success(portService.list(StringUtils.null2EmptyWithTrim(keyword)));
    }

    /**
     * 暂不使用缓存
     */
    @ApiOperation(value = "清除缓存")
    @GetMapping(value = "clear")
    public Result<Void> clear( ){
        portService.clear( );
        return success();
    }

}

