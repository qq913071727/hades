package com.tsfyun.scm.system.entity;

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;


/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-19
 */
@Data
public class CustomsElements implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
     private String id;

    private String hsCode;


    private Boolean isNeed;


    private String name;


    private Integer sort;


}
