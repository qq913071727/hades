package com.tsfyun.scm.system.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class IndexCustomsRatePlusVO implements Serializable {
    // 汇率月份集合
    private List<String> rateMonths;
    // 汇率月份
    private String rateMonth;
    // 汇率
    List<CustomsRateVO> customsRates;
}
