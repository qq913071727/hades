package com.tsfyun.scm.system.mapper;

import com.tsfyun.scm.system.entity.Port;
import com.tsfyun.common.base.extension.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 港口代码 Mapper 接口
 * </p>
 *

 * @since 2020-03-20
 */
@Repository
public interface PortMapper extends Mapper<Port> {

    List<Port> list(@Param(value = "keyword") String keyword);

}
