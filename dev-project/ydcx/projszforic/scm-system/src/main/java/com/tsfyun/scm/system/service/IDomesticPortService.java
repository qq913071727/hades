package com.tsfyun.scm.system.service;

import com.tsfyun.scm.system.entity.DomesticPort;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.vo.DomesticPortVO;

import java.util.List;

/**
 * <p>
 * 国内港口 服务类
 * </p>
 *

 * @since 2020-03-20
 */
public interface IDomesticPortService extends IService<DomesticPort> {

    /**
     * 获取数据，只显示20条
     * @param keyword
     * @return
     */
    List<DomesticPortVO> list(String keyword);

    void clear();

}
