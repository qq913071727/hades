package com.tsfyun.scm.system.entity;

import com.tsfyun.common.base.extension.BaseEntity;
import com.tsfyun.common.base.extension.IdWorker;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.io.Serializable;


/**
 * <p>
 * 税收分类编码
 * </p>
 *

 * @since 2020-05-15
 */
@Data
public class TaxCodeBase implements Serializable {

     private static final long serialVersionUID=1L;

    @Id
    @KeySql(genId = IdWorker.class)
    private Long id;

    /**
     * 货物和劳务名称
     */

    private String name;

    /**
     * 商品和服务分类简称
     */

    private String shortName;


    private String memo;


}
