package com.tsfyun.scm.system.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.constant.CacheConstant;
import com.tsfyun.common.base.exception.ServiceException;
import com.tsfyun.common.base.util.TsfPreconditions;
import com.tsfyun.scm.system.dto.CiqCodesDTO;
import com.tsfyun.scm.system.entity.CiqCodes;
import com.tsfyun.scm.system.mapper.CiqCodesMapper;
import com.tsfyun.scm.system.service.ICiqCodesService;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.scm.system.vo.CiqCodeVo;
import com.tsfyun.scm.system.vo.CiqCodesVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *

 * @since 2020-03-19
 */
@Service
public class CiqCodesServiceImpl extends ServiceImpl<CiqCodes> implements ICiqCodesService {

    @Autowired
    private CiqCodesMapper ciqCodesMapper;

    @Autowired
    private OrikaBeanMapper beanMapper;

    @Override
    public PageInfo<CiqCodes> page(CiqCodesDTO dto) {
        PageHelper.startPage(dto.getPage(),dto.getLimit());
        List<CiqCodes> ciqCodes = ciqCodesMapper.list(dto);
        return new PageInfo<>(ciqCodes);
    }

    @Override
    public CiqCodesVO detail(String id) {
        CiqCodes ciqCodes = super.getById(id);
        TsfPreconditions.checkArgument(Objects.nonNull(ciqCodes),new ServiceException("数据不存在"));
        return beanMapper.map(ciqCodes,CiqCodesVO.class);
    }

    @Override
    @Cacheable(value = CacheConstant.CUSTOMS_CIQ_LIST+"|-1",key = "#hsCode",unless = "#result.size() == 0")
    public List<CiqCodeVo> findByHsCode(String hsCode) {
        return ciqCodesMapper.findByHsCode(hsCode);
    }
}
