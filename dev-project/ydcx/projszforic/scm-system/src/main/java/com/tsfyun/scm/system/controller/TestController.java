package com.tsfyun.scm.system.controller;

import com.tsfyun.common.base.controller.BaseController;
import com.tsfyun.common.base.dto.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "test")
public class TestController extends BaseController {

    @GetMapping(value = "testLogin")
    public Result<String> testLogin() {
        return success("testLogin");
    }

    @GetMapping(value = "testNoLogin")
    public Result<String> testNoLogin() {
        return success("testNoLogin");
    }

}
