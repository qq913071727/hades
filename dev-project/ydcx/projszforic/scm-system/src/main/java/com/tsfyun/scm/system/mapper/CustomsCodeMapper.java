package com.tsfyun.scm.system.mapper;

import com.tsfyun.scm.system.dto.CustomsCodeDTO;
import com.tsfyun.scm.system.dto.HsCodeDTO;
import com.tsfyun.scm.system.dto.SelectCustomsCodeVO;
import com.tsfyun.scm.system.entity.CustomsCode;
import com.tsfyun.common.base.extension.Mapper;
import com.tsfyun.scm.system.vo.CustomsCodeVO;
import com.tsfyun.scm.system.vo.HSCodeVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *

 * @since 2020-03-19
 */
@Repository
public interface CustomsCodeMapper extends Mapper<CustomsCode> {

    CustomsCodeVO findById(String id);

    List<SelectCustomsCodeVO> findAllShort();

    void updateCustomsCode(@Param(value = "customsCode") CustomsCode customsCode,@Param(value = "preId") String preId);

    //查询服务查询
    List<HSCodeVO> inquiryQuery(HsCodeDTO hsCodeDTO);

    //修改海关编码备注
    void updateCustomsCodeMemo(@Param(value = "hsCode") String hsCode,@Param(value = "memo") String memo);
}
