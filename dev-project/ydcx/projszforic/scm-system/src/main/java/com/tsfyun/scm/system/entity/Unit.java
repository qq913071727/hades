package com.tsfyun.scm.system.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import javax.persistence.Id;


/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-19
 */
@Data
public class Unit implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    /**
     * 单位名称
     */

    private String name;

    /**
     * 对应统计计量单位代码
     */

    private String sid;

    /**
     * 转换率
     */

    private BigDecimal convertRate;


    private Integer sort;


}
