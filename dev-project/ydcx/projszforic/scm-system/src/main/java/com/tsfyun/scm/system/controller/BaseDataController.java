package com.tsfyun.scm.system.controller;


import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.system.service.IBaseDataService;
import com.tsfyun.scm.system.vo.BaseDataVO;
import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.tsfyun.common.base.controller.BaseController;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 基础数据配置 前端控制器
 * </p>
 *

 * @since 2020-04-08
 */
@RestController
@RequestMapping("/baseData")
public class BaseDataController extends BaseController {

    @Autowired
    private IBaseDataService baseDataService;

    @Autowired
    public OrikaBeanMapper beanMapper;
//
//    private static final List<String> types = new ArrayList<String>() {{
//        add(BaseDataTypeEnum.CUS_CUSTOMS.getCode());
//        add(BaseDataTypeEnum.CUS_TRANSF.getCode());
//        add(BaseDataTypeEnum.CUS_MAPPING_TRADE_CODE_V.getCode());
//        add(BaseDataTypeEnum.CUS_LEVYTYPE.getCode());
//        add(BaseDataTypeEnum.CUS_MAPPING_PORT_CODE_V.getCode());
//        add(BaseDataTypeEnum.CIQ_PORT_CN.getCode());
//        add(BaseDataTypeEnum.CUS_MAPPING_PACKAGEKIND_CODE_V.getCode());
//        add(BaseDataTypeEnum.CUS_DISTRICT.getCode());
//        add(BaseDataTypeEnum.CIQ_CITY_CN.getCode());
//        add(BaseDataTypeEnum.CUS_LEVYMODE.getCode());
//        add(BaseDataTypeEnum.CUS_LEVYMODE.getCode());
//        add(BaseDataTypeEnum.CUS_MARK.getCode());
//    }};
    /**
     * 根据类型获取基础配置数据
     * @param type
     * @return
     */
    @GetMapping(value = "getByType")
    Result<List<BaseDataVO>> getByType(@RequestParam(value = "type")String type) {
       return success(baseDataService.getByType(type));
    }

    /**
     * 根据ID获取数据
     * @param typeId
     * @return
     */
    @GetMapping(value = "getByTypeId")
    Result<BaseDataVO> getByTypeId(@RequestParam(value = "typeId")String typeId) {
        BaseDataVO dataVO = null;
        String[] strs = typeId.split("-");
        if(strs.length==2){
            List<BaseDataVO> baseDataVOS = baseDataService.getByType(strs[0]);
            dataVO = baseDataVOS.stream().filter(bd ->typeId.equals(bd.getId())).collect(Collectors.toList()).stream().collect(Collectors.toMap(BaseDataVO::getId, Function.identity())).getOrDefault(typeId,null);
        }
        return success(dataVO);
    }

    @GetMapping(value = "getByTypes")
    Result<Map<String,List<BaseDataVO>>> getByTypes(@RequestParam(value = "types")String types) {
        Map<String,List<BaseDataVO>> maps = new LinkedMap<>();
        Arrays.stream(types.split(",")).forEach(type ->{
            maps.put(type,baseDataService.getByType(type));
        });
        return success(maps);
    }


//    @PostMapping(value = "decDatas")
//    Result<List<DecDataVO>> decDatas(){
//        List<DecDataVO> decDataVOS = Lists.newArrayList();
//        types.stream().forEach(type ->{
//            List<BaseDataVO> baseDataVOS = baseDataService.getByType(type);
//            decDataVOS.add(new DecDataVO(type,beanMapper.mapAsList(baseDataVOS, BaseDataSimple.class)));
//        });
//        return success(decDataVOS);
//    }

//    /**
//     * 海关基础数据（按类型）
//     * @return
//     */
//    @PostMapping(value = "decDatasMap")
//    Result<Map<String,List<BaseDataSimple>>> decDatasMap(){
//        Map<String,List<BaseDataSimple>> decDatasMap = Maps.newHashMap();
//        types.stream().forEach(type ->{
//            List<BaseDataVO> baseDataVOS = baseDataService.getByType(type);
//            decDatasMap.put(type,beanMapper.mapAsList(baseDataVOS, BaseDataSimple.class));
//        });
//        return success(decDatasMap);
//    }

    //数据同步
    @GetMapping(value = "test")
    Result<Void> test(){
        baseDataService.synchronizeCustomsData();
        return success();
    }

}

