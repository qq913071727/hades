package com.tsfyun.scm.system.receiver;

import com.tsfyun.common.base.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


/**
 * @Description:
 * @since Created in 2019/12/16 18:55
 */
@EnableBinding(value = {Sink.class})
@Component
@Slf4j
public class SystemReceiver {
    /**
     * 测试接收数据
     * @param payload
     */
    @StreamListener(value = Sink.IN_SCM_DEMO)
    @Transactional(rollbackFor = Exception.class)
    public void receive(@Payload Message<String> payload) {
        String msgId = payload.getHeaders().get(AmqpHeaders.MESSAGE_ID).toString();
        log.info("监听到测试信息,消息id:{}，消息：{}",  msgId,payload.getPayload());
        if(StringUtils.isEmpty(payload.getPayload())) {
            log.error("空消息，消息id:{},不处理",msgId);
            return;
        }
        log.info("消息处理成功,消息id:{}",msgId);
    }

}
