package com.tsfyun.scm.system.controller;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.scm.system.dto.CiqCodesDTO;
import com.tsfyun.scm.system.entity.CiqCodes;
import com.tsfyun.scm.system.service.ICiqCodesService;
import com.tsfyun.scm.system.vo.CiqCodeVo;
import com.tsfyun.scm.system.vo.CiqCodesVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *

 * @since 2020-03-19
 */
@RestController
@RequestMapping("/ciqCodes")
public class CiqCodesController extends BaseController {

    @Autowired
    private ICiqCodesService ciqCodesService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 分页查询
     * @param dto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<CiqCodesVO>> pageList(@ModelAttribute CiqCodesDTO dto) {
        PageInfo<CiqCodes> page = ciqCodesService.page(dto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),CiqCodesVO.class));
    }

    /**
     * 明细
     * @param id
     * @return
     */
    @GetMapping(value = "detail")
    public Result<CiqCodesVO> detail(@RequestParam(value = "id")String id) {
        return success(ciqCodesService.detail(id));
    }


    //根据海关编码查询CIQ编码
    @PostMapping(value = "findByHsCode")
    public Result<List<CiqCodeVo>> findByHsCode(@RequestParam(name = "hsCode",required = false,defaultValue = "")String hsCode){
        if(StringUtils.isNotEmpty(hsCode)&&hsCode.length()==10){
            List<CiqCodeVo> codeVoList = ciqCodesService.findByHsCode(hsCode);
            return Result.success(codeVoList);
        }else{
            return Result.success(new ArrayList<>());
        }
    }
}

