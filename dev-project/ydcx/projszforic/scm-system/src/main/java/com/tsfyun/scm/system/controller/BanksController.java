package com.tsfyun.scm.system.controller;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.system.service.IBanksService;
import com.tsfyun.scm.system.vo.BanksVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 *  全国银行基础数据
 * </p>
 *

 * @since 2020-03-20
 */
@RestController
@RequestMapping("/banks")
public class BanksController extends BaseController {

    @Autowired
    private IBanksService banksService;

    /**
     * 明细
     * @param name
     * @return
     */
    @PostMapping(value = "select")
    public Result<List<BanksVO>> select(@RequestParam(value = "name",required = false)String name) {
        return success(banksService.findByName(name));
    }

}

