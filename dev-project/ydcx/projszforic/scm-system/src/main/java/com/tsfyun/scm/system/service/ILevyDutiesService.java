package com.tsfyun.scm.system.service;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.system.dto.LevyDutiesDTO;
import com.tsfyun.scm.system.entity.LevyDuties;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.vo.LevyDutiesVO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  加征关税服务类
 * </p>
 *

 * @since 2020-03-20
 */
public interface ILevyDutiesService extends IService<LevyDuties> {

    /**
     * 分页查询加征关税
     * @param dto
     * @return
     */
    PageInfo<LevyDuties> page(LevyDutiesDTO dto);

    /**
     * 批量根据海关编码获取加征关税信息
     * @param hsCodes
     * @return
     */
    Map<String,List<LevyDutiesVO>> listByHsCodes(List<String> hsCodes);

    /**=
     * 根据海关编码查询对美加征关税
     * @param hsCode
     * @return
     */
    BigDecimal taxByHsCode(String hsCode);

}
