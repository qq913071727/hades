package com.tsfyun.scm.system.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import com.tsfyun.common.base.extension.BaseEntity;
import lombok.Data;

import javax.persistence.Id;


/**
 * <p>
 * 
 * </p>
 *

 * @since 2020-03-19
 */
@Data
public class CustomsCode implements Serializable {

     private static final long serialVersionUID=1L;

     @Id
     private String id;

    /**
     * 消费税率
     */

    private String conrate;

    /**
     * 监管条件
     */

    private String csc;

    /**
     * 是否禁用
     */

    private Boolean disabled;

    /**
     * 出口税率
     */

    private BigDecimal exportRate;

    /**
     * 普通国汇率
     */

    private BigDecimal gtr;

    /**
     * 检验检疫监管条件
     */

    private String iaqr;

    /**
     * 最惠国税率
     */

    private BigDecimal mfntr;

    /**
     * 海关编码名称
     */

    private String name;

    /**
     * 法二单位编码
     */

    private String sunitCode;

    /**
     * 临时出口税率
     */

    private BigDecimal ter;

    /**
     * 进口暂定税率
     */

    private BigDecimal tit;

    /**
     * 出口退税率
     */

    private String trr;

    /**
     * 法一单位编码
     */

    private String unitCode;

    /**
     * 增值税率
     */

    private BigDecimal vatr;

    /**
     * 法二单位名称
     */

    private String sunitName;

    /**
     * 法一单位名称
     */

    private String unitName;

    /**
     * 备注
     */

    private String memo;


}
