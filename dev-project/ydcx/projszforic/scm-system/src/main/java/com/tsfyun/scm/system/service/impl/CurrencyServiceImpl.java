package com.tsfyun.scm.system.service.impl;

import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.constant.CacheConstant;
import com.tsfyun.common.base.util.StringUtils;
import com.tsfyun.common.base.extension.ServiceImpl;
import com.tsfyun.common.base.vo.CurrencyVO;
import com.tsfyun.scm.system.entity.Currency;
import com.tsfyun.scm.system.mapper.CurrencyMapper;
import com.tsfyun.scm.system.service.ICurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  币制服务实现类
 * </p>
 *

 * @since 2020-03-18
 */
@Service
public class CurrencyServiceImpl extends ServiceImpl<Currency> implements ICurrencyService {

    @Autowired
    private OrikaBeanMapper beanMapper;

    @Autowired
    private CurrencyMapper currencyMapper;

    @Cacheable(value = CacheConstant.CURRENCY_LIST,key = "'all'")
    @Override
    public List<CurrencyVO> select() {
        return currencyMapper.findAll();
    }

    @Override
    public Currency findById(String id) {
        if(StringUtils.isEmpty(id)){ return null; }

        return super.getById(id);
    }

    @Override
    public Currency findByName(String name) {
        if(StringUtils.isEmpty(name)){ return null; }

        Currency currency = new Currency();
        currency.setName(name);
        return currencyMapper.selectOne(currency);
    }
}
