package com.tsfyun.scm.system.controller;


import com.github.pagehelper.PageInfo;
import com.tsfyun.common.base.config.OrikaBeanMapper;
import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.system.dto.CountryDTO;
import com.tsfyun.scm.system.entity.Country;
import com.tsfyun.scm.system.service.ICountryService;
import com.tsfyun.scm.system.vo.CountryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsfyun.common.base.controller.BaseController;

import java.util.List;

/**
 * <p>
 *  国家前端控制器
 * </p>
 *

 * @since 2020-03-19
 */
@RestController
@RequestMapping("/country")
public class CountryController extends BaseController {

    @Autowired
    private ICountryService countryService;

    @Autowired
    private OrikaBeanMapper beanMapper;

    /**
     * 获取国家下拉列表
     * @return
     */
    @GetMapping(value = "select")
    public Result<List<CountryVO>> select() {
        return success(countryService.select());
    }

    /**
     * 分页查询
     * @param dto
     * @return
     */
    @PostMapping(value = "list")
    public Result<List<CountryVO>> pageList(@ModelAttribute CountryDTO dto) {
        PageInfo<Country> page = countryService.page(dto);
        return success((int)page.getTotal(),beanMapper.mapAsList(page.getList(),CountryVO.class));
    }

    /**
     * 详情
     */
    @GetMapping("/detail")
    public Result<CountryVO> detail(@RequestParam(value = "id")String id){
        return success(countryService.detail(id));
    }


}

