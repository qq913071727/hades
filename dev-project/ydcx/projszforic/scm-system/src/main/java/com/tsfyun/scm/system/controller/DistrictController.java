package com.tsfyun.scm.system.controller;

import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.system.dto.DistrictNameDto;
import com.tsfyun.scm.system.mapper.DistrictMapper;
import com.tsfyun.scm.system.service.IDistrictService;
import com.tsfyun.scm.system.vo.SimpleDistrictVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**=
 * 行政区域信息
 */
@RestController
@RequestMapping(value = "/district")
@CrossOrigin()
public class DistrictController {

    @Autowired
    private IDistrictService districtService;


    //所有省
    @GetMapping(value = "/provinces")
    public Result provinces(){
        return Result.success(districtService.findAllProvince());
    }
    //根据省查询市
    @GetMapping(value = "/citys")
    public Result citys(@RequestParam(name = "pname",required = true)String pname){
        return Result.success(districtService.findCityByProvince(pname));
    }
    //根据省市查询区
    @GetMapping(value = "/areas")
    public Result areas(@RequestParam(name = "pname",required = true)String pname,@RequestParam(name = "cname",required = true)String cname){
        return Result.success(districtService.findAreaByProvinceAndCity(pname, cname));
    }

    @GetMapping(value = "/alls")
    public Result<List<SimpleDistrictVO>> alls(){
        return Result.success(districtService.findAll());
    }
}
