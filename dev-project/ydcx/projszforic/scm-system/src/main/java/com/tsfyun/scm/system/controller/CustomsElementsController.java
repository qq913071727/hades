package com.tsfyun.scm.system.controller;


import com.tsfyun.common.base.dto.Result;
import com.tsfyun.scm.system.service.ICustomsElementsService;
import com.tsfyun.scm.system.vo.CustomsElementsVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.tsfyun.common.base.controller.BaseController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *

 * @since 2020-03-19
 */
@RestController
@RequestMapping("/customsElements")
public class CustomsElementsController extends BaseController {

    @Autowired
    private ICustomsElementsService customsElementsService;

    /**
     * 根据海关编码获取品名要素
     * @param hsCode
     * @return
     */
    @RequestMapping(value = "getByHsCode")
    public Result<List<CustomsElementsVO>> detail(@RequestParam(value = "hsCode")String hsCode) {
        return success(customsElementsService.findByHsCode(hsCode));
    }

    @ApiOperation(value = "模糊搜索品名要素")
    @GetMapping(value = "/elementsName")
    public Result<List<Map<String, String>>> elementsName(@RequestParam(value="name",required = false) String name){
        return Result.success(customsElementsService.findByName(name));
    }
}

