package com.tsfyun.scm.system.service;

import com.github.pagehelper.PageInfo;
import com.tsfyun.scm.system.dto.CiqCodesDTO;
import com.tsfyun.scm.system.entity.CiqCodes;
import com.tsfyun.common.base.extension.IService;
import com.tsfyun.scm.system.vo.CiqCodeVo;
import com.tsfyun.scm.system.vo.CiqCodesVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *

 * @since 2020-03-19
 */
public interface ICiqCodesService extends IService<CiqCodes> {

    /**
     * 分页查询
     * @param dto
     * @return
     */
    PageInfo<CiqCodes> page(CiqCodesDTO dto);

    /**
     * 获取明细
     * @param id
     * @return
     */
    CiqCodesVO detail(String id);

    //根据海关编码查询CIQ编码
    List<CiqCodeVo> findByHsCode(String hsCode);

}
