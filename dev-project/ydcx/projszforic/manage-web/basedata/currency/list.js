layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('scm/currency/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'id',width:80, title: '货币编号'}
                ,{field:'name',width:140, title: '货币名称'}
                ,{field:'sort',width:80, title: '排序',edit:'text'}
                ,{field:'disabled',width:80, title: '启用状态',align:'form-switch',templet:'#disabledTemp'}
                ,{field:'empty',title: '&nbsp;'}
            ]],
        },
    });

    layui.table.on('edit(table-list)', function (data) {
        var value = data.value;//修改后的数据
        //值必须为0-99
        var data = data.data;//修改的当行数据
        var oldValue =  $(this).prev().text();
        if (!/^[0-9]|[0-9]$/.test(value)) {
            layer.msg('排序值只能为0-99');
            return false;
        }
        var id = data.id;
        $.post("/scm/currency/adjustSort",{id:id,sort:value}, function (res) {
            if(isSuccess(res)) {
                reloadList();
                closeThisWin('调整成功');
            }
        });
    });


    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/currency/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });

});
