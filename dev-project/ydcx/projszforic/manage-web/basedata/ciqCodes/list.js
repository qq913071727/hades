layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('system/ciqCodes/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'hsCode', fixed:true,width:120, title: '海关编码',templet:'#hsCodeTemp'}
                ,{field:'code', width:80,fixed:true, title: '商检编码',templet:'#codeTemp'}
                ,{field:'typeName', width:100, title: '类型名称'}
                ,{field:'name', width:200, title: '编码名称'}
                ,{field:'effective', width:80, title: '是否生效',align:'center',templet:'#effectiveTemp'}
                ,{field:'empty',title: '&nbsp;'}
            ]]
        }
    });
});
function showHsCodeDetail(id){
    openwin(rurl('basedata/customsCode/detail.html?id='+id),'海关编码详情',{area: ['1200px', '620px'],shadeClose:true});
}