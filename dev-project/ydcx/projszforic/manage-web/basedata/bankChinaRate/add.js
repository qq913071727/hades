layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    $("#publishDate").val(getNowFormatTimeMill());
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    form.on('submit(save-btn)', function(data){
        formSub('scm/bankChinaRate/add',function (res) {
            if (isSuccess(res)) {
                parent.reloadList();
                closeThisWin("新增成功");
            }
        });
        return false;
    });
});
