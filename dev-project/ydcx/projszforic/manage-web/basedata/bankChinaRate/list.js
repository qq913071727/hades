layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $("#publishDate").val(getNowFormatDate());
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('system/bankChinaRate/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'currencyName', width:140, title: '币制'}
                ,{field:'buyingRate', width:120, title: '现汇买入价',align:'right'}
                ,{field:'cashPurchase', width:120, title: '现钞买入价',align:'right'}
                ,{field:'spotSelling', width:120, title: '现汇卖出价',align:'right'}
                ,{field:'cashSelling', width:120, title: '现钞卖出价',align:'right'}
                ,{field:'bocConversion', width:120, title: '中行折算价',align:'right'}
                ,{field:'publishDate', title: '公布时间',align:'center'}
            ]]
        }
    });
    
    $(".grab-btn").click(function () {
        $.get('/system/bankChinaRate/grab',function (res) {
            if(isSuccess(res)) {
                show_msg('获取成功，请稍后刷新页面查看');
            }
        });
    });
    
});
