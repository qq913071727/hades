var backUrl = '';
layui.use(['form', 'layer','laydate','table'], function () {
    var layer = layui.layer, form = layui.form, laydate = layui.laydate,table = layui.table;
    $('.sel-load').selLoad(function(){
        form.render('select');
        var id = getUrlParam("id");
        if(isEmpty(id)) {//新增
            $("#pageTitle").html("新增海关编码");
            //默认初始化 品牌类型和出口享惠  2个要素
            addRow("品牌类型",true);
            addRow("出口享惠情况",true);
        } else {//修改
            $("#pageTitle").html("修改海关编码");
            $.get(rurl('system/customsCode/customsCodeDetail'),{'id':getUrlParam('id')},function (res) {
                if(isSuccess(res)){
                    var codeData = res.data.customsCode;
                    var customsElements = res.data.customsElements;
                    form.val('form',codeData);
                    $("#preId").val(codeData.id);
                    $.each(customsElements,function (i,o) {
                        addRow(o.name,o.isNeed);
                    });
                }
            });
        }
        $('.addrow-btn').click(function () {
            addBlankRow();
        });
        form.on('submit(save-btn)', function(data){
            var trs = $('#elements').find('tr');
            if(trs.size()==0){show_msg("请添加编码要素");return false;}
            $('input[name="isNeed"]').remove();
            var $need = $('input[name="need"]');
            $.each($need,function (i,o) {
                $('form').append('<input type="hidden" name="isNeed" value="'+($(o).attr("checked")?true:false)+'"/>')
            });
            let requestUrl = 'system/customsCode/' + (isEmpty(id) ? 'addCustomsCode' : 'editCustomsCode');
            formSub(requestUrl,function (data) {
                parent.reloadList();
                closeThisWin('保存成功');
            });
            return false;
        });
    });
});

function addBlankRow() {//添加空白行
    var html = [];
    html.push('<tr><td>');
    html.push('<a href="javascript:" class="def-a" style="font-size: 13px;" onclick="downElement(this)"><i class="fa fa-arrow-down"></i></a>&nbsp;');
    html.push('<a href="javascript:" class="def-a" style="font-size: 13px;" onclick="upElement(this)"><i class="fa fa-arrow-up"></i></a>&nbsp;');
    html.push('<a href="javascript:" class="def-a" style="font-size: 13px;" onclick="removeElement(this)"><i class="fa fa-trash"></i></a>');
    html.push('</td><td ind></td><td><input type="text" name="elementsName" class="layui-input"/></td>');
    html.push('<td><input type="checkbox" name="need" lay-skin="primary" checked=""></td>');
    html.push('<td>&nbsp;</td></tr>');
    $('#elements').append(html.join(''));
    layui.form.render();
    tableRowSort();
}

function addRow(ename,che) {//添加有数据行
    var html = [];
    if('品牌类型' != ename && '出口享惠情况' != ename) {
        html.push('<tr><td>');
        html.push('<a href="javascript:" class="def-a" style="font-size: 13px;" onclick="downElement(this)"><i class="fa fa-arrow-down"></i></a>&nbsp;');
        html.push('<a href="javascript:" class="def-a" style="font-size: 13px;" onclick="upElement(this)"><i class="fa fa-arrow-up"></i></a>&nbsp;');
        html.push('<a href="javascript:" class="def-a" style="font-size: 13px;" onclick="removeElement(this)"><i class="fa fa-trash"></i></a>');

        html.push('</td><td ind></td><td><input type="text" name="elementsName" class="layui-input" value="'+ename+'"/></td>');
        html.push('<td><input type="checkbox" name="need" lay-skin="primary" '+((che)?'checked=""':'')+'></td>');
        html.push('<td>&nbsp;</td></tr>');
    } else {
        html.push('<tr><td>');
        html.push('</td><td ind></td><td><input type="text" name="elementsName" class="layui-input" readonly value="'+ename+'"/></td>');
        html.push('<td><input type="checkbox" name="need" lay-skin="primary" checked'+'></td>');
        html.push('<td>&nbsp;</td></tr>');
    }
    $('#elements').append(html.join(''));
    layui.form.render();
    tableRowSort();
}

function tableRowSort() {
    var trs = $('#elements').find('tr');
    $.each(trs,function (i,o) {
       $(o).find('td[ind]').html(i+1);
       $(o).attr({"stor":(i+1)});
    });
    $('input[name="elementsName"]').autocomplete({
        source: function (request, response) {
            $.get(rurl('system/customsElements/elementsName'),{name: request.term},function (res) {response($.map(res.data, function (item) {return {label: item.name, value: item.name, data: item}}));});
        }, minLength: 1, delay: 700
    })
}
//删除要素
function removeElement(e) {
    $(e).parent().parent().remove();
    layui.form.render();
    tableRowSort();
}
//下移要素
function downElement(e) {
    var stor = parseInt($(e).parent().parent().attr('stor'));
    var trs = $('#elements').find('tr');
    if(stor<trs.size()){
        var $that = $('tr[stor="'+stor+'"]');
        var $next = $('tr[stor="'+(stor+1)+'"]');
        var thatName = $that.find('input[name="elementsName"]').val();
        var thatNeed = $that.find('input[name="need"]').attr('checked')?true:false;
        var nextName = $next.find('input[name="elementsName"]').val();
        var nextNeed = $next.find('input[name="need"]').attr('checked')?true:false;
        $next.find('input[name="elementsName"]').val(thatName);
        $next.find('input[name="need"]').attr({'checked':thatNeed});
        $that.find('input[name="elementsName"]').val(nextName);
        $that.find('input[name="need"]').attr({'checked':nextNeed});
        layui.form.render();
    }else{
        show_msg("已经是最后一个了哦")
    }
}
//上移要素
function upElement(e) {
    var stor = parseInt($(e).parent().parent().attr('stor'));
    var trs = $('#elements').find('tr');
    //第3个元素不允许上移，因为品牌类型和出口享惠情况是固定不能动的
    if(stor <= 3) {
        show_msg("品牌类型和出口享惠情况后的第一个要素不允许上移");
        return;
    }
    if(stor>1){
        var $that = $('tr[stor="'+stor+'"]');
        var $up = $('tr[stor="'+(stor-1)+'"]');
        var thatName = $that.find('input[name="elementsName"]').val();
        var thatNeed = $that.find('input[name="need"]').attr('checked')?true:false;
        var upName = $up.find('input[name="elementsName"]').val();
        var upNeed = $up.find('input[name="need"]').attr('checked')?true:false;
        $up.find('input[name="elementsName"]').val(thatName);
        $up.find('input[name="need"]').attr({'checked':thatNeed});
        $that.find('input[name="elementsName"]').val(upName);
        $that.find('input[name="need"]').attr({'checked':upNeed});
        layui.form.render();
    }else{
        show_msg("已经是第一个了哦")
    }
}