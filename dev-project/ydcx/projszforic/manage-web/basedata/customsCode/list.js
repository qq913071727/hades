layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('system/customsCode/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'id', fixed:true, width:120, title: '编号',templet:'#codeTemp'}
                ,{field:'name', fixed:true, width:160, title: '名称',templet:'#nameTemp'}
                ,{field:'mfntr', width:110, title: '进口最惠国税率%',align:'right'}
                ,{field:'gtr', width:110, title: '进口普通国税率%',align:'right'}
                ,{field:'tit', width:100, title: '暂定税率%',align:'right'}
                ,{field:'vatr', width:100, title: '增值税率%',align:'right'}
                ,{field:'conrate', width:100, title: '消费税率%',align:'right'}
                ,{field:'csc', width:80, title: '监管条件',templet:'#cscTemp'}
                ,{field:'iaqr', width:80, title: '检验检疫',templet:'#iaqrTemp'}
                ,{field:'ter', width:100, title: '出口临时税率%',align:'right'}
                ,{field:'exportRate', width:100, title: '出口税率%',align:'right'}
                ,{field:'trr', width:100, title: '退税率%',align:'right'}
            ]]
        },listQueryDone:function(){
            formatCsc();formatIaqr();
        }
    });
});
function showDetail(id){
    openwin(rurl('basedata/customsCode/detail.html?id='+id),'编码详情',{area: ['1200px', '620px'],shadeClose:true});
}

function deleteCustomsCode(o) {
    var ids = getIds();
    if(ids.length != 1){show_msg('请单选一条记录');return;}
    layer.confirm('海关编码数据极为重要，删除后将会导致系统<span style="color:red;">部分重要功能无法使用</span>。 </br></br>删除后<span style="color: red">数据无法恢复</span>，一般仅限于您误添加的数据删除。</br></br>三思而后行，您确定要删除该海关编码数据吗?',
        {icon: 3,closeBtn:false,title:'温馨提示',area:['400px','300px']},function(index){
        layer.close(index);
        $.post(rurl('/system/customsCode/delete'),{'id':ids[0]},function (res) {
            if(isSuccessWarn(res)){
                reloadList();
            }
        });
    });
    return false;
}