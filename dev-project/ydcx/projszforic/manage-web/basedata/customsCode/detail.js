layui.use(['layer','form','table'], function() {
       var layer = layui.layer, form = layui.form, table = layui.table;
    var id = getUrlParam("id");
    show_loading();
    $.get(rurl('system/customsCode/detail'),{'id':id},function (res) {
       if(isSuccess(res)){
           var data = res.data;
           form.val('form',data);
           disableForm(form);
       }
    });
    table.render({
        elem: '#table-list',where:{'hsCode':id},response:{msgName:'message',statusCode: '1'}
        ,url:rurl('system/customsElements/getByHsCode')
        , page: false,limit:1000,id:'tab-list'
        ,cols: [[
            {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
            ,{field:'name', width:260, title: '要素名称'}
            ,{field:'isNeed', width:80, title: '是否必填',align:'center',templet:'#isNeedTemp'}
            ,{field:'empty', title: ''}
        ]]
    });
});