layui.use(['form', 'layer','element'], function () {
    var layer = layui.layer,form = layui.form,element = layui.element;
    $('#importFile').fileupload({
        dataType: 'json',
        url:rurl('system/hkControl/importData'),
        add: function(e, data) {
            data.submit();
            show_loading();
        },
        progressall: function (e, data) {},
        done: function (e, data){
            var res = data.result;
            if(isSuccess(res)){
                show_msg("数据导入成功");
            }
        }
    });
});
