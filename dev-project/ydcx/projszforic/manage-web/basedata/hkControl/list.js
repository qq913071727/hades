layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('system/hkControl/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'model', width:200, title: '型号'}
                ,{field:'brand', width:160, title: '品牌'}
                ,{field:'isControl', width:80, title: '是否管制',align:'center',templet: '#isControlTemp'}
                ,{field:'type', width:100, title: '类别代码'}
                ,{field:'description', title: '产品说明描述'}
                ,{field:'synchroDate', width:120, title: '更新时间'}
            ]]
        }
    });
});
