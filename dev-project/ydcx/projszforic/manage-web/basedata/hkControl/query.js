layui.use(['layer','form','table'], function(){
    var layer = layui.layer,form = layui.form,table = layui.table;
    try{$('#model').val(parent.returnModel());}catch (e) {}
    var $listTab = table.render({
        url:rurl('system/hkControl/list'),elem: '#table-list',where:$('.query-form').serializeJson(),method:'post',
        height: 'full-'+($('.tsf-list-top').height()+20),page: true,
        limits: limits,limit:limit,id:'tab-list',response:{msgName:'message',statusCode: '1'}
        , cols: [[
            {type: 'numbers', title: '序号', width: 40}
            ,{field:'model', width:140, title: '型号'}
            ,{field:'brand', width:120, title: '品牌'}
            ,{field:'isControl', width:65, title: '是否管制',align:'center',templet: '#isControlTemp'}
            ,{field:'type', width:90, title: '类别代码'}
            ,{field:'description', title: '产品说明描述'}
            ,{field:'synchroDate', width:120, title: '更新时间'}
        ]]
    });
    $('.search-btn').click(function () {
        $listTab.reload({page:{curr:1},height: 'full-'+($('.tsf-list-top').height()+20),where:$('.query-form').serializeJson()});
    });
});
