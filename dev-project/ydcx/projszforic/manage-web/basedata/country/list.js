layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('system/country/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'id', fixed:true,width:80, title: '国家编号'}
                ,{field:'name', fixed:true,width:140, title: '国家名称'}
                ,{field:'namee', width:200, title: '英文名称'}
                ,{field:'disabled', width:100, title: '启用状态',align:'center',templet:'#disabledTemp'}
                ,{field:'isDiscount', width:100, title: '是否最惠国',align:'center',templet:'#discountTemp'}
                // ,{field:'disinfectCheck', width:100, title: '是否消毒检',align:'right',templet:'#disinfectCheckTemp'}
                ,{field:'empty',title: '&nbsp;'}
            ]]
        }
    });
});
