layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('system/levyDuties/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'countryName', fixed:true,width:80, title: '国家'}
                ,{field:'hsCode', fixed:true,width:120, title: '海关编码',templet:'#hsCodeTemp'}
                ,{field:'levyVal', width:100, title: '加征比例%'}
                ,{field:'isStart', width:80, title: '是否生效',align:'center',templet:'#effectiveTemp'}
                ,{field:'empty',title: '&nbsp;'}
            ]]
        }
    });
});
function showHsCodeDetail(id){
    openwin(rurl('basedata/customsCode/detail.html?id='+id),'海关编码详情',{area: ['1200px', '620px'],shadeClose:true});
}
