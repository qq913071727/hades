layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    $("#publishDate").val(getNowFormatTimeMill());
    form.on('submit(save-btn)', function(data){
        formSub('scm/customsRate/add',function (res) {
            if (isSuccess(res)) {
                parent.reloadList();
                closeThisWin("新增成功");
            }
        });
        return false;
    });
});
