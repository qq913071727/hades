layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    //$("#rateDate").val(getNowFormatDate());
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('system/customsRate/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'currencyName', width:140, title: '币制'}
                ,{field:'rateDate', width:150, title: '汇率日期',align:'right'}
                ,{field:'rate', width:120, title: '汇率',align:'right'}
                ,{field:'publishDate', title: '公布时间',align:'center'}
            ]]
        }
    });

    $(".grab-btn").click(function () {
        $.get('/system/customsRate/grab',function (res) {
            if(isSuccess(res)) {
                show_msg('获取成功，请稍后刷新页面查看');
            }
        });
    });

});
