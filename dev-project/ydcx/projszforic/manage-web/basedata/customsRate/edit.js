layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table,fileupload = layui.fileupload;
    var id = getUrlParam('id');//待修改数据id
    $('.sel-load').selLoad(function () {
        show_loading();
        $.get(rurl("/scm/customsRate/detail"),{"id":id},function (res) {
            if(isSuccess(res)) {
                var data = res.data;
                //旧id，用于后台数据校验存在与否
                data['preId'] = data.id;
                form.val('form',data);
            }
        })
    });

    $('.save-btn').click(function () {
        formSub('scm/customsRate/edit',function (res) {
            if(isSuccess(res)) {
                parent.reloadList();
                closeThisWin('修改成功');
            }
        });
        return false;
    });
});
