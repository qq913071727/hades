(function($){
    var outInd = 0,maxInd = 0,$that = null,callback;
    $.fn.selLoad = function (fn) {callback = fn;$that = $(this);maxInd = $that.length;outInd = 0;singleFull();}
    function singleFull() {
        if(maxInd>outInd){
            var $outSel = $($that[outInd]),cacheName = $outSel.attr('cache');
            var cacheData = [];
            if(!isEmpty(cacheName)){cacheData = JSON.parse(getLocalStorage(cacheName) || "[]");}
            if(cacheData.length==0){
                $.get(rurl($outSel.attr('sel-url')),{},function (res) {
                    cacheData = res.data;
                    if(cacheData&&!isEmpty($outSel.attr('sel-data-name'))){
                        cacheData = res.data[$outSel.attr('sel-data-name')];
                    }
                    if(!isEmpty(cacheName)&&cacheData.length>0){setLocalStorage(cacheName,JSON.stringify(cacheData));}
                    singleFullData($outSel,cacheData);
                });
            }else{
                singleFullData($outSel,cacheData);
            }
        }else{callback();}
    }
    function singleFullData($outSel,data) {
        $outSel.get(0).length = 0;
        if(!isEmpty($outSel.attr('sel-first'))){
            $outSel.append('<option value="">'+$outSel.attr('sel-first')+'</option>');
        }
        var _k = $outSel.attr('sel-key'),_v = $outSel.attr('sel-val'),selectedText = $outSel.attr('selected-text');
        $.each((data || []),function (i,o) {
            $outSel.append('<option value="'+o[_k]+'"'+((!isEmpty(selectedText)&&selectedText==o[_v])?' selected':'')+'>'+o[_v]+'</option>');
        });
        outInd++;singleFull();
    }
})(jQuery);