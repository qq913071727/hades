layui.use(['layer','form'], function(){
    app.refreshSubject();
    var layer = layui.layer,form = layui.form;
    form.val('form',{'accessId':getCookie('$pwd_accessId')});
    form.verify({
        passWord: function (value, item){if(isEmpty(value)||value.length<6){return '新密码不能为空且必须大于6位';}},
        comfirPassWord: function (value, item){if(isEmpty(value)){return '请再次输入新密码';}else{if(value!=$('#passWord').val()){return '两次密码输入不一致';}}}
    });
    form.on('submit(login-btn)', function(data){
        show_loading();try{if(returnCitySN){$('#local').val(returnCitySN.cname);}}catch(e){}
        $.post(rurl('scm/auth/setPassWord'),$('.layui-form').serialize(),function (res) {
            if(isSuccess(res)){
                prompt_success('修改成功，请重新登录',function () {
                    toUrl('login.html');
                });
                clearCookie('$pwd_accessId');
            }
        });
        return false;
    });
});