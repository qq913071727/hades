function loadProvinceDatas(fn) {
    var static_province = getLocalStorage('static_province');
    if(!isEmpty(static_province)){
        fn(JSON.parse(static_province));
    }else{
        $.get('/system/district/provinces',{},function (res) {
            if(isSuccess(res)){
                provinceDatas = res.data;
                setLocalStorage('static_province',JSON.stringify(provinceDatas));
                fn(provinceDatas);
            }
        });
    }
}
$('.tsf-page-content').append('<div class="area-content"><div class="layui-tab layui-tab-card"><ul class="layui-tab-title"><li class="layui-this" atsf-tit="province">省份</li><li atsf-tit="city">城市</li><li atsf-tit="area">区县</li></ul><div class="layui-tab-content"><div class="layui-tab-item layui-show" atsf-con="province"><img src="/images/load.gif"/></div><div class="layui-tab-item" atsf-con="city"></div><div class="layui-tab-item" atsf-con="area"></div></div></div></div>');
var $Areacontent = $('.area-content');
$Areacontent.find('li[atsf-tit]').click(function (event) {
    event.stopPropagation();
    var $that = $(this);
    $Areacontent.find('li[atsf-tit]').removeClass('layui-this');
    $that.addClass('layui-this');
    $Areacontent.find('div[atsf-con]').removeClass('layui-show');
    $Areacontent.find('div[atsf-con="'+$that.attr('atsf-tit')+'"]').addClass('layui-show');
});
var $_province = $Areacontent.find('div[atsf-con="province"]');
var $_city = $Areacontent.find('div[atsf-con="city"]');
var $_area = $Areacontent.find('div[atsf-con="area"]');
var $checkCityObj = null;
var isPullProvince = false;
$("body").click(function(event){
    $('.parent-area').removeClass('layui-form-selected');
    $Areacontent.hide();
});
$Areacontent.click(function(event){
    event.stopPropagation();
});
function initCity(elem) {
    loadProvinceDatas(function (provinceData) {
        var $elem = $(elem);
        if(isPullProvince==false){
            isPullProvince = true;
            var phtml = [];
            $.each(provinceData,function (i,o) {
                phtml.push('<a title="'+o.name+'" onclick="show_city(this)">'+o.name+'</a>');
            });
            $_province.html(phtml.join(''));
        }
        $elem.attr({'readonly':true,'autocomplete':'off'});
        $elem.after('<i class="layui-edge"></i>');
        $elem.parent().addClass('layui-unselect layui-form-select parent-area');
        $elem.click(function (event) {
            event.stopPropagation();
            $checkCityObj = $(this);
            $('.parent-area').removeClass('layui-form-selected');
            $checkCityObj.parent().addClass('layui-form-selected');
            $Areacontent.css({'left': $checkCityObj.offset().left, 'top': ($checkCityObj.offset().top + $('.tsf-page-content').scrollTop() + $checkCityObj.outerHeight() + 4),'display':'block'});
        });
    });
}
function show_city(e) {
    var $that = $(e);
    $Areacontent.find('li[atsf-tit="city"]').click();
    if(!$that.hasClass('active')){
        $_province.find('a').removeClass('active');
        $that.addClass('active');
        //获取市
        $_city.html('<img src="/images/load.gif"/>');
        $.get('/system/district/citys',{'pname':$that.attr('title')},function (res) {
            if (isSuccess(res)) {
                var chtml = [];
                $.each(res.data,function (i,o) {
                    chtml.push('<a title="'+o.name+'" onclick="show_area(this)">'+o.name+'</a>');
                });
                $_city.html(chtml.join(''));
            }
        });
    }
}
function show_area(e){
    var $that = $(e);
    $Areacontent.find('li[atsf-tit="area"]').click();
    if(!$that.hasClass('active')){
        $_city.find('a').removeClass('active');
        $that.addClass('active');
        //获取区
        $_area.html('<img src="/images/load.gif"/>');
        var pname = $_province.find('.active').attr("title");
        $.get('/system/district/areas',{'pname':pname,'cname':$that.attr('title')},function (res) {
            if (isSuccess(res)) {
                var ahtml = [];
                $.each(res.data,function (i,o) {
                    ahtml.push('<a title="'+o.name+'" onclick="assignment_val(this)">'+o.name+'</a>');
                });
                $_area.html(ahtml.join(''));
            }
        });
    }
}
function assignment_val(e) {
    var $that = $(e);
    $_area.find('a').removeClass('active');
    $that.addClass('active');
    $('.parent-area').removeClass('layui-form-selected');
    $Areacontent.hide();
    var vals = [];
    $.each($Areacontent.find('.active'),function (i,o) {
        vals.push($(o).attr('title'));
    });
    $checkCityObj.val(vals.join(" / "));
}