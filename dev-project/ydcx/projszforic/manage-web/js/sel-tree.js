(function ($) {
    function selTree() {
        var ts = new Object();
        ts.$input = null;
        ts.$selInput = null;
        ts.$valInput = null;
        ts.$pDiv = null;
        ts.$selDiv = null;
        ts.$selUl = null;
        ts.$zTree = null;
        ts.loadComplete = false;
        ts.$arg = {
            valueId:new Date().getTime()
        };
        ts.init = function ($element,arg) {
            ts.$input = $element;
            ts.$arg = $.extend(ts.$arg,arg);
            ts.initInput();
            $("body").bind("click", ts.bodyClick);
        };
        ts.initInput = function(){
            ts.$input.hide().attr({'autocomplete':'off'});
            var fhtml = [];
            var verify = ts.$input.attr('lay-verify');
            fhtml.push('<input type="hidden" name="'+ts.$arg.valueId+'" id="val_'+ts.$arg.valueId+'"/>');
            fhtml.push('<div class="layui-unselect layui-form-select" id="div_'+ts.$arg.valueId+'"><div class="layui-select-title">');
            fhtml.push('<input type="text" '+(isEmpty(verify)?'':'lay-verify="'+verify+'"')+' id="input_'+ts.$arg.valueId+'" name="input_'+ts.$arg.valueId+'" placeholder="'+ts.$input.attr('placeholder')+'" value="'+ts.$input.val()+'" readonly="" class="layui-input layui-unselect">');
            fhtml.push('<i class="layui-edge"></i></div></div>');
            ts.$input.after(fhtml.join(''));
            ts.$input.attr({'lay-verify':null});
            ts.$valInput = $('#val_'+ts.$arg.valueId);
            ts.$selInput = $('#input_'+ts.$arg.valueId);
            ts.$pDiv = $('#div_'+ts.$arg.valueId);
            ts.$selInput.bind("focus", function () {
                ts.openTree();
            });
            var input_x = ts.$selInput.offset().left;
            var input_y = ts.$selInput.offset().top;
            var input_w = ts.$selInput.outerWidth();
            var input_h = ts.$selInput.outerHeight();
            var div_w = input_w;
            var html = '<div id="sel_div_' + ts.valueId + '" class="tree-select"><ul id="sel_ul_' + ts.valueId + '" class="ztree"><img src="/images/load.gif"/></ul></div>';
            ts.$selInput.after(html);
            ts.$selDiv = $("#sel_div_" + ts.valueId);
            ts.$selDiv.offset({"left": input_x, "top": input_y + input_h + 4});
            ts.$selDiv.css({"width": div_w, "max-width": div_w});
            ts.$selUl = $("#sel_ul_" + ts.valueId);
            ts.$selUl.css({"margin": 0, "padding": 0});
        };
        ts.initTree = function (data) {
            $.fn.zTree.init(ts.$selUl, {
                callback: {
                    onClick: ts.clickTree
                },
                view: {
                    showLine: true,
                    showTitle: true,
                    selectedMulti: false,
                    expandSpeed: "fast"
                },
                data: {
                    simpleData: {
                        enable: true,
                        pIdKey:'parentId'
                    }
                }
            }, data);
            ts.$zTree = $.fn.zTree.getZTreeObj("sel_ul_" + ts.key).expandAll(true);
        };
        ts.clickTree = function (event, treeId, treeNode) {
            ts.$selInput.val(treeNode["name"]);
            ts.$input.val(treeNode["name"]);
            ts.$valInput.val(treeNode["id"]);
            ts.closeTree();
        };
        ts.bodyClick = function (event) {
            var x1 = ts.$selInput.offset().left;
            var y1 = ts.$selInput.offset().top;
            var width = ts.$selInput.outerWidth();
            var height = ts.$selInput.outerHeight() + ts.$selDiv.outerHeight() + 1;//1px的缝隙
            var x2 = x1 + width;
            var y2 = y1 + height;
            var x = event.clientX;
            var y = event.clientY;
            if (x < x1 || x2 < x || y < y1 || y2 < y) {
                ts.closeTree();
            }else{
                setTimeout(function () {
                    ts.$pDiv.addClass('layui-form-selected');
                },15);
            }
        };
        ts.closeTree = function () {
            ts.$selDiv.hide();
            setTimeout(function () {
                ts.$pDiv.removeClass('layui-form-selected');
            },20);
        };
        ts.openTree = function () {
            ts.$pDiv.addClass('layui-form-selected');
            ts.$selDiv.show();
            if(!ts.loadComplete){
                ts.loadComplete = true;
                $.get(rurl(ts.$arg.url),{},function (res) {
                    if (isSuccess(res)) {
                        ts.initTree(res.data);
                    }else{
                        ts.closeTree();
                    }
                });
            }
        };
        return ts;
    };
    $.fn.treeSelect = function (arg) {
        var ts = new selTree();
        ts.init(this,arg);
        ts.closeTree();
        return ts;
    };
})(jQuery);