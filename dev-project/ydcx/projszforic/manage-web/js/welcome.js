layui.use(['layer','form','laytpl'], function(){
    var layer = layui.layer,form = layui.form,laytpl = layui.laytpl;
    var businessVolumeChart = echarts.init(document.getElementById("monthly-business-volume"));
    var agencyFeeChart = echarts.init(document.getElementById("monthly-agency-fee"));
    var fn = {
        impExpTotalData:function(){
            // 进出口数据统计
            $.get('/scm/welcome/impExpTotalData',function (res) {
                if(isSuccess(res)){
                    laytpl($('#area1-view-temp').html()).render(res.data,function (html) {
                        $('#area1-view').html(html);
                    });
                }
            });
        },
        customsExchangeRate:function(){
            var $rateMonth = $('#customsRateMonth');
            // 获取海关汇率数据
            $.get('/system/rate/customsExchangeRate',{'rateMonth':$rateMonth.val()},function (res) {
                if(isSuccess(res)){
                    var data = res.data;
                    $rateMonth.get(0).length = 0;
                    $.each(data.rateMonths,function (i,o) {
                        $rateMonth.append('<option value="'+o+'" '+(o==data.rateMonth?'selected':'')+'>'+o+'</option>');
                    });
                    form.render('select');
                    laytpl($('#rate-temp').html()).render(data.customsRates,function (html) {
                        $('#customs-exchange-rate-view').html(html);
                    });
                }
            });
        },
        initChart:function(){
            // 指定图表的配置项和数据
            var option = {
                legend: {},
                grid: {
                    left: '6px',
                    right: '6px',
                    bottom: '6px',
                    containLabel: true
                },
                tooltip: {},
                dataset: {
                    source: [
                        ['2021-01', 0, 0],
                        ['2021-02', 0, 0],
                        ['2021-03', 0, 0],
                        ['2021-04', 0, 0],
                        ['2021-05', 0, 0],
                        ['2021-06', 0, 0],
                        ['2021-07', 0, 0],
                        ['2021-08', 0, 0],
                        ['2021-09', 0, 0],
                        ['2021-10', 0, 0],
                        ['2021-11', 0, 0],
                        ['2021-12', 0, 0]
                    ]
                },
                xAxis: { type: 'category' },
                yAxis: {},
                series: [
                    { name: '进口',type: 'bar', color: ['#0ba9f9'],barWidth: 14},
                    { name: '出口',type: 'bar', color: ['#1E9FFF'],barWidth: 14}
                ]
            };
            businessVolumeChart.setOption(option);
            agencyFeeChart.setOption(option);
        },
        fullMonthlyBusinessVolume:function(){
            var year = new Date().getFullYear();
            $.get('/scm/welcome/monthImpExpTotalData',{'year':year},function (res) {
                if(isSuccess(res)){
                    var data = res.data,source = [];
                    for(let key  in data){
                        source.push([year+'-'+(key<10?'0'+key:key),data[key].monthImpAmountMoney,data[key].monthExpAmountMoney]);
                    }
                    businessVolumeChart.setOption({
                        dataset:{
                            source:source
                        }
                    });
                }
            });
        },
        fullMonthImpExpAgencyFee:function () {
            var year = new Date().getFullYear();
            $.get('/scm/welcome/monthImpExpAgencyFee',{'year':year},function (res) {
                if(isSuccess(res)){
                    var data = res.data,source = [];
                    $.each(data,function(i,o){
                        source.push([o.month,o.impCost,o.expCost]);
                    });
                    agencyFeeChart.setOption({
                        dataset:{
                            source:source
                        }
                    });
                }
            });
        },
        init:function () {
            //默认中行汇率为当前日期
            $("#bankRateDate").val(getNowFormatDate());
            fn.impExpTotalData();
            fn.customsExchangeRate();
            obcRate();
            fn.initChart();
            fn.fullMonthlyBusinessVolume();
            fn.fullMonthImpExpAgencyFee();
        }
    };
    fn.init();
    form.on('select(customsRateMonth)',function (data) {
        fn.customsExchangeRate();
    });
    form.on('select(obcRateSelect)',function (data) {
        obcRate();
    });
    window.onresize = function () {
        businessVolumeChart.resize;
        agencyFeeChart.resize;
    };
});

function obcRate() {
    // 获取中行汇率数据
    $.get('/system/rate/obcRate',{'obcRateType':$('#obcRateType').val(),'obcRateTime':$('#obcRateTime').val(),'rateDate':$("#bankRateDate").val()},function (res) {
        if(isSuccess(res)){
            layui.laytpl($('#obc-rate-temp').html()).render(res.data,function (html) {
                $('#obc-rate-view').html(html);
            });
        }
    });
}