layui.use(['layer','form'], function(){
    var layer = layui.layer,form = layui.form;
    app.refreshSubject(function () {
        if(!isEmpty(getLocalStorage('TSF_SCM'))){toUrl('index.html');return;}
    });
    form.val('form',{'deviceId':uuid()});
    form.verify({
        userName: function (value, item){if(isEmpty(value)){return '请输入登录账号';}},
        passWord: function (value, item){if(isEmpty(value)){return '请输入登录密码';}}
    });
    var original = location.origin+'/index.html';if(!isEmpty(getUrlParam('original'))){original = decodeURIComponent(getUrlParam('original'));}
    form.on('submit(login-btn)', function(data){
        show_loading();try{if(returnCitySN){$('#local').val(returnCitySN.cname);}}catch(e){}
        postJSON('scm/auth/login',$('.layui-form').serializeJson(),function (res) {
            cleanLocalData();
            var data = res.data;
            if(data.requireChangedPwd){
                setCookie('$pwd_accessId',data.accessId);
                prompt_warn('为了您的账号安全请修改您的登录密码',function () {
                    toUrl('set-pwd.html');
                });
            }else{
                setLocalStorage('TSF_SCM',res.data.token);
                setLocalStorage('TSF_SCM_U',JSON.stringify(res.data.loginInfo));
                location.href=original;
            }
        });
        return false;
    });
});
function cleanLocalData() {
    cleanLocalStorage('static_province');//省数据
    cleanLocalStorage('static_currency');//币制数据
    cleanLocalStorage('static_unit');//单位
    cleanLocalStorage('static_country');//国家
    cleanLocalStorage('static_domestic_logistics');//国内物流
    cleanLocalStorage('static_transaction_mode');//成交方式
    cleanLocalStorage('static_delivery_mode');//交货方式
    cleanLocalStorage('static_hk_express');//香港快递公司
    cleanLocalStorage('static_receiving_mode');//国内送货方式
    cleanLocalStorage('static_business_type');//业务类型
    cleanLocalStorage('static_bill_type');//单据业务类型
    cleanLocalStorage('static_quote_type');//报价类型
    cleanLocalStorage('static_sale_persons');//销售人员
    cleanLocalStorage('static_business_persons');//商务人员
    cleanLocalStorage('static_decl_datas');//报关参数
    cleanLocalStorage('static_payment_term');//付款方式
    cleanLocalStorage('static_subject_overseas');//卖方
    cleanLocalStorage('static_cus_traf_mode');//运输方式
    
    cleanLocalStorage('static_decl_datas1');//海关基础数据
    cleanLocalStorage('static_decl_datas2');//海关基础数据
    cleanLocalStorage('static_code_datas');//海关基础数据
    cleanLocalStorage('static_prod_traf_data');//海关基础数据
}