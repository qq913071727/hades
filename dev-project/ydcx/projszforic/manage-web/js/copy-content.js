$(document).ready(function(){
    if($('#copyContent').size()==0){
        $('body').append('<input type="text" id="copyContent" value="" style="width: 1px;height: 1px;position: fixed;bottom: 0;left: 0;z-index: 0;background-color: transparent;border: 0px"/>');
    }
    startCopyContent();
});
function startCopyContent(){
    $(".copy").each(function(){
        $(this).parent().append('<a class="copy-btn" title="复制"><i class="icon-copy"></i></a>');
    });
    $(".copy-btn").click(function () {
        var $self = $(this);
        var content = $self.parent().text();
        copyContent(content,$self)
    });
}
function copyContent(no,e){
    var obj = document.getElementById("copyContent");
    obj.value = no.trim();
    obj.select();
    document.execCommand("copy");
    layer.tips('已复制', e.parent(),{tips: [1,'#000'], time: 2000});
}
