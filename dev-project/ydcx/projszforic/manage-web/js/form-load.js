
/**
 * 扩展 form load 方法
 * @param action string
 * @param data object & string
 * @param queryForm object
 * eg
 * $(obj).formLoad("load",{})
 * $(obj).formLoad("load","ajaxurl",queryForm)
 * **/
;(function($) {
    ;function load(_582, data, queryForm) {
        if (typeof data == "string") {
            if (!queryForm) {
                return;
            }
            $.ajax({
                url: data,
                data: queryForm,
                dataType: "json",
                success: function(data) {
                    _584(data);
                }
            });
        } else {
            _584(data);
        }
        function _584(data) {
            var form = $(_582);
            for (var name in data) {
                var val = data[name];
                if (!_585(name, val)) {
                    form.find("input[name=\"" + name + "\"]").val(val);
                    form.find("textarea[name=\"" + name + "\"]").val(val);
                    form.find("select[name=\"" + name + "\"]").val(val);
                    form.find("label[name=\"" + name + "\"]").text(val != null ? val : '');
                    form.find("div[name=\"" + name + "\"]").text(val != null ? val : '');
                    form.find("span[name=\"" + name + "\"]").text(val != null ? val : '');
                }
            }
        }
        ;function _585(name, val) {
            var cc = $(_582).find("input[name=\"" + name + "\"][type=radio], input[name=\"" + name + "\"][type=checkbox]");
            if (cc.length) {
                cc.prop("checked", false);
                cc.each(function() {
                    if (_587($(this).val(), val)) {
                        $(this).prop("checked", true);
                    }
                });
                return true;
            }
            return false;
        }
        ;function _587(v, val) {
            if (v == String(val) || $.inArray(v, $.isArray(val) ? val : [val]) >= 0) {
                return true;
            } else {
                return false;
            }
        }
        ;
    }
    ;$.fn.formLoad = function(action, data, queryForm) {
        if (typeof action == "string") {
            return $.fn.formLoad.methods[action](this, data, queryForm);
        }
    }
    ;$.fn.formLoad.methods = {
        load: function(jq, data, queryForm) {
            return jq.each(function() {
                load(this, data, queryForm);
            });
        },
    }
})(jQuery);
