layui.define([ 'layer','upload'], function(exports){
    var layer = layui.layer,upload = layui.upload
    ,fileupload = {
        config:{
            id:"",
            startQuery:true,
            edit:true,
            uploadArg:{},
            images:['jpg','jpeg','gif','png','bmp','tif'],
            icons:{'pdf':'fa fa-file-pdf-o','xls':'fa fa-file-excel-o','xlsx':'fa fa-file-excel-o','doc':'fa fa-file-word-o','docx':'fa fa-file-word-o','ppt':'fa fa-file-powerpoint-o','pptx':'fa fa-file-powerpoint-o','txt':'fa fa-file-text-o','zip':'fa fa-file-zip-o','rar':'fa fa-file-zip-o','7z':'fa fa-file-zip-o','jpg':'fa fa-file-image-o','jpeg':'fa fa-file-image-o','gif':'fa fa-file-image-o','png':'fa fa-file-image-o','bmp':'fa fa-file-image-o','tif':'fa fa-file-image-o'}
        }
        ,index: layui.fileupload ? (layui.fileupload.index + 10000) : 0
    }
    //当前操作实例
    ,thisUpload = function () {
        var that = this,options = that.config,id = options.id || options.index;
        return {
            config: options
        }
    }    
    ,Class = function(options){
        var that = this;
        that.index = ++fileupload.index;
        that.config = $.extend({}, that.config, fileupload.config, options);
        that.render();
    };

    //文件上传区域渲染
    Class.prototype.render = function(){
        var that = this,options = that.config;
        options.elem = $(options.elem);
        options.id = options.id || options.elem.attr('id') || that.index;
        options.fileId = 'file_'.concat(options.id);
        options.fileDatas = [];
        var areas = [
            '<ul><li class="upload-btn"><a class="upload-a" id="',options.fileId,'"><i class="fa fa-cloud-upload"></i></a></li></ul>'
        ]
        options.elem.html(areas.join(''));
        options.uploadBtnElem = options.elem.find('.upload-btn');
        if(options.edit){
            upload.render($.extend({},{
                url:rurl('scm/file/upload'),size:10240,exts:'pdf|xls|xlsx|doc|docx|ppt|pptx|txt|zip|rar|7z|jpg|jpeg|gif|png|bmp|tif',
                multiple:true
            },options.uploadArg,{
                elem:'#'.concat(options.fileId),
                before:function () {
                    show_loading();
                },done:function (res){
                    if(isSuccess(res)){
                        that.loadItem(res.data);
                    }
                },error:function () {
                    hide_loading();
                }
            }));
        }else{
            options.uploadBtnElem.hide();
        }
        if(options.startQuery){
            that.loadList();
        }
    };
    //单个加载
    Class.prototype.loadItem = function(item){
        var that = this,options = that.config;
        var row = ['<li class="upload-item" id="item_',item.id,'"><div class="file-type">',
            '<i class="preview ',isEmpty(options.icons[item.extension])?'fa fa-file-o':options.icons[item.extension],'"></i></div>',
            options.edit?'<a class="del-file" title="删除"><i class="fa fa-remove"></i></a>':'',
            '<a href="/scm/download/file/',item.id,'?down=true" class="down-file" target="_blank" title="',item.name,'">',item.name,'</a></li>'];
        options.uploadBtnElem.before(row.join(''));
        var $items = $('#item_'+item.id);
        if(options.edit){
            $items.find('.del-file').unbind().bind('click',function () {
                delete_inquiry('是否确定要删除此文件？','scm/file/removes',{'ids':item.id},function () {
                    $items.unbind().remove();
                });
            });
        }
        // if(contain(options.images,item.extension)){//图片
        //     $items.find('.preview').css({'cursor':'pointer'}).unbind().bind('click',function () {
        //         layer.photos({photos: {'data': [{'src': '/scm/download/file/'.concat(item.id)}]},shade : 0.5,anim:0});
        //     });
        // }
    };
    //加载数据列表
    Class.prototype.loadList = function(){
        var that = this,options = that.config;
        $.post(rurl('scm/file/fileList'),options.uploadArg.data,function (res) {
            if(isSuccess(res)){
                options.elem.find('.upload-item').unbind().remove();
                var list = res.data;
                if(list){
                    $.each(list,function (i,o) {
                        that.loadItem(o);
                    });
                }
            }
        });
    };
    //核心入口
    fileupload.render = function(options){
        var inst = new Class(options);
        return thisUpload.call(inst);
    };
    exports('fileupload', fileupload);
});