var app = {url:'/',tsfyun:'https://www.xxxx.com/'};
var limits=[10,20,30,40,50,100],limit=20;
function rurl(url){return [(url.indexOf('/')==0)?url:app.url+url,(url.indexOf('?v=')==-1&&url.indexOf('&v=')==-1)?((url.indexOf('?')==-1)?'?v='+(getLocalStorage('sasa-version')||''):'&v='+(getLocalStorage('sasa-version')||'')):''].join('');}
function initInput(){
    $('input[NUMBER]').bind('keyup',function(e){checkDouble(this);}).bind('blur',function(e){checkDouble(this);if(isEmpty($(this).val())){$(this).val('0.00')}}).bind('focus',function (e){clearNum(this)});
    $('input[NUMBER-NC]').bind('keyup',function(e){checkDouble(this);}).bind('blur',function(e){checkDouble(this);});
    $('input[INT]').bind('keyup',function(e){checkInt(this);}).bind('blur',function(e){checkInt(this);if(isEmpty($(this).val())){$(this).val('0')}}).bind('focus',function (e){clearNum(this)});
    $('input[INT-NC]').bind('keyup',function(e){checkInt(this);}).bind('blur',function(e){checkInt(this);});
    $('input[NUMBER2]').bind('keyup',function(e){checkDouble2(this);}).bind('blur',function(e){checkDouble2(this);if(isEmpty($(this).val())){$(this).val('0.00')}}).bind('focus',function (e){clearNum(this)});
    $('input[NUMBER3]').bind('keyup',function(e){checkDouble3(this);}).bind('blur',function(e){checkDouble3(this);if(isEmpty($(this).val())){$(this).val('0.000')}}).bind('focus',function (e){clearNum(this)});
    $('input[NUMBER4]').bind('keyup',function(e){checkDouble4(this);}).bind('blur',function(e){checkDouble4(this);if(isEmpty($(this).val())){$(this).val('0.0000')}}).bind('focus',function (e){clearNum(this)});
    $('input[NUMBER6]').bind('keyup',function(e){checkDouble6(this);}).bind('blur',function(e){checkDouble6(this);if(isEmpty($(this).val())){$(this).val('0.000000')}}).bind('focus',function (e){clearNum(this)});
}
String.prototype.replaceAll = function(s1,s2){return this.replace(new RegExp(s1,"gm"),s2);}
function checkDouble(th){var $that = $(th);$that.val(trim($that.val()).replace(',',''));if (!$that.val().match(/^[\+\-]?\d*?\.?\d{0,2}?$/)){$that.val($that.attr('t_value'));}else{$that.attr({'t_value':$that.val()});}if ($that.val().match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/)){$that.attr({'o_value':$that.val()});}}
function checkInt(th) {var $that = $(th);$that.val($that.val().replace(/\D/g, ""));}
function clearNum(th){var $that = $(th);if(!isEmpty($that.val())){if(parseFloat($that.val())==0.0){$that.val('')}}}
function checkDouble2(th){var $that = $(th);$that.val(trim($that.val()).replace(',',''));if (!$that.val().match(/^[\+\-]?\d*?\.?\d{0,2}?$/)){$that.val($that.attr('t_value'));}else{$that.attr({'t_value':$that.val()});}if ($that.val().match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/)){$that.attr({'o_value':$that.val()});}}
function checkDouble3(th){var $that = $(th);$that.val(trim($that.val()).replace(',',''));if (!$that.val().match(/^[\+\-]?\d*?\.?\d{0,3}?$/)){$that.val($that.attr('t_value'));}else{$that.attr({'t_value':$that.val()});}if ($that.val().match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/)){$that.attr({'o_value':$that.val()});}}
function checkDouble4(th){var $that = $(th);$that.val(trim($that.val()).replace(',',''));if (!$that.val().match(/^[\+\-]?\d*?\.?\d{0,4}?$/)){$that.val($that.attr('t_value'));}else{$that.attr({'t_value':$that.val()});}if ($that.val().match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/)){$that.attr({'o_value':$that.val()});}}
function checkDouble6(th){var $that = $(th);$that.val(trim($that.val()).replace(',',''));if (!$that.val().match(/^[\+\-]?\d*?\.?\d{0,6}?$/)){$that.val($that.attr('t_value'));}else{$that.attr({'t_value':$that.val()});}if ($that.val().match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/)){$that.attr({'o_value':$that.val()});}}
$('body').append('<div class="spinner_shade lodding"></div><div class="spinner lodding"><div class="lodding_ico"></div><div>正在加载中...</div></div>');
$.ajaxSetup(
    {timeout:-1,beforeSend:function(xhr){
        xhr.setRequestHeader('token',getLocalStorage('TSF_SCM'));
        xhr.setRequestHeader('device','pc');
        },complete:function (xhr,status) {
            if(xhr.status==200){
                var res = {};try{ res = JSON.parse(xhr.responseText);}catch (e){}
                switch (res.code) {
                    case '401':
                        setLocalStorage('TSF_SCM','');setLocalStorage('TSF_SCM_U','');prompt_warn((res.message || '请登录'),function () {parent.parent.location.href = rurl('login.html?original=')+encodeURIComponent(parent.location.href);});
                        break;
                }
            }
        },error: function (XMLHttpRequest){
            hide_loading();parent.hide_loading();
            switch (XMLHttpRequest.status){
                case(401):
                    setLocalStorage('TSF_SCM','');setLocalStorage('TSF_SCM_U','');prompt_warn((res.message || '请登录'),function () {parent.parent.location.href = rurl('login.html?original=')+encodeURIComponent(parent.location.href);});
                    break;
                case(403):show_msg('您无此访问权限,请联系贵司管理员授权');break;case(404):show_msg('请求的资源暂不可用');break;case(405):show_msg('请求不被允许');break;case(413):show_msg('请求数据过大');break;case(415):show_msg('请求参数错误');break;case(408):show_msg('请求超时，请稍后重试');break;case(502):show_msg('网络异常，请稍后重试');break;case(504):show_msg('服务响应时间过长，请稍后查看');break;default:show_msg('网络异常，请稍后重试');break;
            }
        }
    });
app.obtainSubject = function(fn){var subjectObj = {},localSub = getLocalStorage('TSF_SUB');if(isEmpty(localSub)){$.get(rurl('scm/common/subject'),{},function (res) {if(res.code=='1'){subjectObj = res.data;setLocalStorage('TSF_SUB',JSON.stringify(subjectObj));if(fn){fn(subjectObj);}}});}else{subjectObj = JSON.parse(localSub);if(fn){fn(subjectObj);}}};
app.initPageSubject = function(){var localSub = getLocalStorage('TSF_SUB');if(!isEmpty(localSub)){var o = JSON.parse(localSub);$('.sub-name').html(o.name+'&nbsp;&nbsp;');$('title').html(o.browserTitle);$('.company_logo').attr({'src':rurl(o.logo),'title':o.name});$('.user-login-bg').css({'background-image':'url('+rurl(o.loginBg)+')'})}else{app.refreshSubject();}};
app.refreshSubject = function(fn){cleanLocalStorage('TSF_SUB');app.obtainSubject(function (o) {app.initPageSubject();if(fn){fn(o);}});};
app.obtainUser = function(){var user = getLocalStorage('TSF_SCM_U');if(!isEmpty(user)){return JSON.parse(user);}return {};};
function applyClear(){
    var $clear = $('input[clear-val]');
    $clear.parent().addClass('clear-input');
    $clear.after('<i class="fa fa-remove clear-val"></i>');
    $clear.attr({'clear-val':null});
    $('.clear-val').click(function(){
        $(this).parent().find('input').not(':input[disabled]').val('').select();
    });
}
$(function () {
    app.initPageSubject();
    $('.required').bind('blur',function(){var $that = $(this);if($that.hasClass('layui-form-danger')&&!isEmpty($that.val())){$that.removeClass('layui-form-danger');}});
    applyClear();
});
$.fn.serializeJson=function(){var serializeObj={},$this=$(this), array=this.serializeArray();$(array).each(function(){if(typeof(serializeObj[this.name])!="undefined"){if($.isArray(serializeObj[this.name])){serializeObj[this.name].push(this.value);}else{serializeObj[this.name]=[serializeObj[this.name],this.value];}}else{var isList = (typeof($this.find('[name="'+[this.name]+'"]').attr("LIST"))=="undefined");serializeObj[this.name]= (isList)?this.value:[this.value];}});return serializeObj;};
$.fn.extend({enter:function (fn,thisObj) {var obj = thisObj || this;return this.each(function () {$(this).keyup(function(event){if(event.keyCode == 13){fn.call(obj,event);}});});}});
function jsonList(json){var list = [],keys=[];for(var key in json){keys.push(key);}$.each(json[keys[0]] ||[],function (i,o) {var item = {};$.each(keys,function (j,k) {item[k] = json[k][i];});list.push(item);});return list;}
$('.close-window').click(function () {closeThisWin();});
$('.refresh-btn').click(function () {refPage();});
$('.goback').click(function () {toUrl($(this).attr('url'))});
initInput();
$(".picker-month").attr({"autocomplete":"off","readonly":true}).focus(function(){WdatePicker({dateFmt:"yyyy-MM"})}).click(function(){WdatePicker({dateFmt:"yyyy-MM"})});
$('.picker-date').attr({"autocomplete":"off","readonly":true}).focus(function(){WdatePicker()}).click(function(){WdatePicker()});
$('.picker-time').attr({"autocomplete":"off","readonly":true}).focus(function(){WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})}).click(function(){WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})});
$('.picker-time-start').attr({"autocomplete":"off","readonly":true}).focus(function(){WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',startDate:'%y-%M-%d 00:00:00'})}).click(function(){WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})});
$('.picker-time-end').attr({"autocomplete":"off","readonly":true}).focus(function(){WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',startDate:'%y-%M-%d 23:59:59'})}).click(function(){WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})});
var DEFAULTS = {shade:0.2, anim:1, area: ['500px', '320px']};
(function(global,factory){typeof exports==="object"&&typeof module!=="undefined"?module.exports=factory(global):typeof define==="function"&&define.amd?define(factory):factory(global)})(typeof self!=="undefined"?self:typeof window!=="undefined"?window:typeof global!=="undefined"?global:this,function(global){"use strict";global=global||{};var _Base64=global.Base64;var version="2.5.1";var buffer;if(typeof module!=="undefined"&&module.exports){try{buffer=eval("require('buffer').Buffer")}catch(err){buffer=undefined}}var b64chars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";var b64tab=function(bin){var t={};for(var i=0,l=bin.length;i<l;i++)t[bin.charAt(i)]=i;return t}(b64chars);var fromCharCode=String.fromCharCode;var cb_utob=function(c){if(c.length<2){var cc=c.charCodeAt(0);return cc<128?c:cc<2048?fromCharCode(192|cc>>>6)+fromCharCode(128|cc&63):fromCharCode(224|cc>>>12&15)+fromCharCode(128|cc>>>6&63)+fromCharCode(128|cc&63)}else{var cc=65536+(c.charCodeAt(0)-55296)*1024+(c.charCodeAt(1)-56320);return fromCharCode(240|cc>>>18&7)+fromCharCode(128|cc>>>12&63)+fromCharCode(128|cc>>>6&63)+fromCharCode(128|cc&63)}};var re_utob=/[\uD800-\uDBFF][\uDC00-\uDFFFF]|[^\x00-\x7F]/g;var utob=function(u){return u.replace(re_utob,cb_utob)};var cb_encode=function(ccc){var padlen=[0,2,1][ccc.length%3],ord=ccc.charCodeAt(0)<<16|(ccc.length>1?ccc.charCodeAt(1):0)<<8|(ccc.length>2?ccc.charCodeAt(2):0),chars=[b64chars.charAt(ord>>>18),b64chars.charAt(ord>>>12&63),padlen>=2?"=":b64chars.charAt(ord>>>6&63),padlen>=1?"=":b64chars.charAt(ord&63)];return chars.join("")};var btoa=global.btoa?function(b){return global.btoa(b)}:function(b){return b.replace(/[\s\S]{1,3}/g,cb_encode)};var _encode=buffer?buffer.from&&Uint8Array&&buffer.from!==Uint8Array.from?function(u){return(u.constructor===buffer.constructor?u:buffer.from(u)).toString("base64")}:function(u){return(u.constructor===buffer.constructor?u:new buffer(u)).toString("base64")}:function(u){return btoa(utob(u))};var encode=function(u,urisafe){return!urisafe?_encode(String(u)):_encode(String(u)).replace(/[+\/]/g,function(m0){return m0=="+"?"-":"_"}).replace(/=/g,"")};var encodeURI=function(u){return encode(u,true)};var re_btou=new RegExp(["[À-ß][-¿]","[à-ï][-¿]{2}","[ð-÷][-¿]{3}"].join("|"),"g");var cb_btou=function(cccc){switch(cccc.length){case 4:var cp=(7&cccc.charCodeAt(0))<<18|(63&cccc.charCodeAt(1))<<12|(63&cccc.charCodeAt(2))<<6|63&cccc.charCodeAt(3),offset=cp-65536;return fromCharCode((offset>>>10)+55296)+fromCharCode((offset&1023)+56320);case 3:return fromCharCode((15&cccc.charCodeAt(0))<<12|(63&cccc.charCodeAt(1))<<6|63&cccc.charCodeAt(2));default:return fromCharCode((31&cccc.charCodeAt(0))<<6|63&cccc.charCodeAt(1))}};var btou=function(b){return b.replace(re_btou,cb_btou)};var cb_decode=function(cccc){var len=cccc.length,padlen=len%4,n=(len>0?b64tab[cccc.charAt(0)]<<18:0)|(len>1?b64tab[cccc.charAt(1)]<<12:0)|(len>2?b64tab[cccc.charAt(2)]<<6:0)|(len>3?b64tab[cccc.charAt(3)]:0),chars=[fromCharCode(n>>>16),fromCharCode(n>>>8&255),fromCharCode(n&255)];chars.length-=[0,0,2,1][padlen];return chars.join("")};var _atob=global.atob?function(a){return global.atob(a)}:function(a){return a.replace(/\S{1,4}/g,cb_decode)};var atob=function(a){return _atob(String(a).replace(/[^A-Za-z0-9\+\/]/g,""))};var _decode=buffer?buffer.from&&Uint8Array&&buffer.from!==Uint8Array.from?function(a){return(a.constructor===buffer.constructor?a:buffer.from(a,"base64")).toString()}:function(a){return(a.constructor===buffer.constructor?a:new buffer(a,"base64")).toString()}:function(a){return btou(_atob(a))};var decode=function(a){return _decode(String(a).replace(/[-_]/g,function(m0){return m0=="-"?"+":"/"}).replace(/[^A-Za-z0-9\+\/]/g,""))};var noConflict=function(){var Base64=global.Base64;global.Base64=_Base64;return Base64};global.Base64={VERSION:version,atob:atob,btoa:btoa,fromBase64:decode,toBase64:encode,utob:utob,encode:encode,encodeURI:encodeURI,btou:btou,decode:decode,noConflict:noConflict,__buffer__:buffer};if(typeof Object.defineProperty==="function"){var noEnum=function(v){return{value:v,enumerable:false,writable:true,configurable:true}};global.Base64.extendString=function(){Object.defineProperty(String.prototype,"fromBase64",noEnum(function(){return decode(this)}));Object.defineProperty(String.prototype,"toBase64",noEnum(function(urisafe){return encode(this,urisafe)}));Object.defineProperty(String.prototype,"toBase64URI",noEnum(function(){return encode(this,true)}))}}if(global["Meteor"]){Base64=global.Base64}if(typeof module!=="undefined"&&module.exports){module.exports.Base64=global.Base64}else if(typeof define==="function"&&define.amd){define([],function(){return global.Base64})}return{Base64:global.Base64}});
function openwin(url,title,arg){
    var merge = $.extend({},DEFAULTS,arg),w=parseInt(merge.area[0].replace('px','')),h=parseInt(merge.area[1].replace('px','')), ww=$(window).width(), wh=$(window).height(),wfull=((w==0||w>ww)&&(h==0||h>wh));
    if(w==0||w>ww){w=ww;}if(h==0||h>wh){h=wh;}var winf = layer.open($.extend(merge,{content:rurl(url),area: [w+'px', h+'px'],title:title,type: 2,scrollbar:false,zIndex: layer.zIndex}));
    if(wfull){layer.full(winf);}return winf;
}
function show_loading(){$('.lodding').fadeIn();}
function hide_loading(){$('.lodding').fadeOut(200);}
function setLocalStorage(key,val){localStorage.setItem(key,Base64.encode(val));}
function cleanLocalStorage(key){setLocalStorage(key,'');}
function getLocalStorage(key){var val = localStorage.getItem(key);if(isEmpty(val)){return val;}return Base64.decode(val);}
function setCookie(name,value,seconds){try{seconds = seconds || 0;var expires = "";if(seconds != 0) {var date = new Date();date.setTime(date.getTime() + (seconds * 1000));expires = "; expires=" + date.toGMTString();}document.cookie = name + "=" + Base64.encode(value) + expires + "; path=/";}catch (e){}}
function getCookie(name){try{var nameEQ = name + "=";var ca = document.cookie.split(';');for(var i = 0; i < ca.length; i++) {var c = ca[i];while(c.charAt(0) == ' ') {c = c.substring(1, c.length);}if(c.indexOf(nameEQ) == 0) {return Base64.decode(c.substring(nameEQ.length, c.length));}}}catch (e){}return '';}
function clearCookie(name){setCookie(name, "", -1);}
function show_error(message){layer.msg(message,{time: 4000,shift:6,zIndex: layer.zIndex});}
function show_success(message){layer.msg(message,{icon: 1,time: 3000,anim:1,zIndex: layer.zIndex});}
function show_msg(message){layer.msg(message,{time: 3000,zIndex: layer.zIndex});}
function isSuccess(res){hide_loading();if(res.code != "1"){show_error(res.message);return false}return true;}
function delayedUrl(url,time){setTimeout(function(){toUrl(url)},time);}
function isSuccessNoTip(res){hide_loading();return res.code == "1";}
function isSuccessWarn(res){hide_loading();if(res.code != "1"){prompt_warn(res.message);return false}return true;}
function isSuccessWarnClose(res){hide_loading();if(res.code != "1"){prompt_warn(res.message,function (){closeThisWin();try{parent.reloadList();}catch (e) {}});return false}return true;}
function closeThisWin(msg) {parent.layer.close(parent.layer.getFrameIndex(window.name));if(msg){parent.show_success(msg)}}
function browserInfo(){var brow=$.browser;if(brow.chrome){bInfo="Chrome："+brow.version;}else if(brow.msie){bInfo="Microsoft Internet Explorer："+brow.version;}else if(brow.mozilla){bInfo="Mozilla Firefox："+brow.version;}else if(brow.safari){bInfo="Apple Safari："+brow.version;}else if(brow.opera){bInfo="Opera："+brow.version;}else if(brow.opera){bInfo="Opera："+brow.version;}else{bInfo="Other："+brow.version;}return bInfo;}
function refPage(){window.location.reload();}
function getTabData(){var tdata = [];var datas = layui.table.checkStatus('tab-list');$.each(datas.data,function (i,o) {tdata.push(o);});return tdata;}
function getUrlParam(name){var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");if(location.href.indexOf('?')!=-1){var r = location.href.substr(location.href.indexOf('?')+1).match(reg);if (r != null) return unescape(r[2]);}return null;}
function trim(va){va=va+"";return va.replace(/(^\s*)|(\s*$)/g, "");}
function isEmpty(va){if(!va){return true;}if(trim(va).length==0){return true;}return false}
function isNotEmpty(va){return !isEmpty(va);}
function removeNull(va){if(isEmpty(va)){return '';}else{return va!='null'?va:''}}
function checkedForm(form){
    var forms = form.find('.required').filter(':visible').not('.disabled').not(':input[disabled]');
    for(var i=0;i<forms.size();i++){
        var $form = $(forms[i]);
        if(isEmpty($form.val())){
            $form.addClass('layui-form-danger').select();
            show_error('必填项不能为空');
            return false;
        }
    }
    return true;
}
function formSub(url,completeFN) {show_loading();$.post(rurl(url),$('.layui-form').serialize(),function (res) {if(isSuccess(res)){var fn = eval(completeFN);fn(res);}});}
function formSubJson(url,completeFN){postJSON(url,$('.layui-form').serializeJson(),completeFN);}
function postJSON(url,json,completeFN){show_loading();$.ajax({url: rurl(url), data: JSON.stringify(json), type: 'post', dataType: 'json', contentType: 'application/json', success: function (res){if(isSuccess(res)){var fn = eval(completeFN);fn(res);}}});}
function prompt_warn(title,fn){var al = layer.alert(title,{icon: 0,closeBtn:false,title:'温馨提示'},function () {if(fn){var _FN = eval(fn);_FN();}layer.close(al);})}
function prompt_success(title,fn){var al = layer.alert(title,{icon: 1,closeBtn:false,title:'温馨提示'},function () {if(fn){var _FN = eval(fn);_FN();}layer.close(al);})}
function prompt_error(title,fn){var al = layer.alert(title,{icon: 2,closeBtn:false,title:'温馨提示'},function () {if(fn){var _FN = eval(fn);_FN();}layer.close(al);})}
function prompt_inquiry(title,fn){var al = layer.alert(title,{icon: 3,closeBtn:false,title:'温馨提示'},function () {if(fn){var _FN = eval(fn);_FN();}layer.close(al);})}
function confirm_inquiry(title,fn1,fn2){var al = layer.confirm(title,{icon: 3,closeBtn:false,title:'温馨提示'},function (){if(fn1){var _FN = eval(fn1);_FN();}layer.close(al);},function (){if(fn2){var _FN = eval(fn2);_FN();}layer.close(al);});}
function delete_inquiry(title,rul,arg,done){confirm_inquiry(title,function () {show_loading();$.post(rurl(rul),arg,function (res) {if(isSuccess(res)){if(done){var _FN = eval(done);_FN(res);}}})});}
function warmReminder(id,msg,wmsg){if(isEmpty(getLocalStorage('WR_'+id))){layer.alert(wmsg,{icon: 1,closeBtn:false,title:'温馨提醒',success:function (layero, index){$(layero).find('.layui-layer-btn a').before('<div class="tps-displayed"><label><input type="checkbox" id="CK_'+id+'"/> 不再提醒</label></div>');$('#CK_'+id).click(function(){setLocalStorage('WR_'+id,isEmpty($(this).attr('checked'))?'':'1');});}});}else{if(!isEmpty(msg)){show_success(msg);}}}
function showPhotos(e){$(e).click(function (){openPhotos(e)});}
function openPhotos(e){layui.layer.photos({photos: {"data": [{"src": $(e).attr('src')}]},anim: 5});}
function toUrl(url){location.href = rurl(url);}
function formatTableData(){var formatMoneyObjs = $('td[align="money"] div');$.each(formatMoneyObjs,function (i,o) {$(o).html(formatCurrency($(o).text()));});}
function formatCurrency(num){if(num==''||num==null||num=='null'){return '0.00';}num = num.toString().replace(/\$|\,/g,'');if(isNaN(num)){num = "0";}sign = (num == (num = Math.abs(num)));num = Math.floor(num*100+0.50000000001);cents = num%100;num = Math.floor(num/100).toString();if(cents<10){cents = "0" + cents;}for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++){num = num.substring(0,num.length-(4*i+3))+','+ num.substring(num.length-(4*i+3));}return (((sign)?'':'-') + num + '.' + cents);}
function contain(array,val) {for (var i = 0; i < array.length; i++) {if (array[i] == val) {return true;}}return false;}
var filewin = null,outFileObject = null;
function initFileSize(fileObject){var $fileSize = fileObject || $('.file-size');$fileSize.click(function (){layer.close(filewin);var $that = $(this);if(!isEmpty($that.attr('docId'))){outFileObject = $that;var furl = rurl('uploader/upload-list.html?docId='+$that.attr('docId')+'&docType='+$that.attr('docType')+'&memo='+escape($that.attr('memo'))+'&edit='+$that.attr('edit'));filewin = layer.open({area: ['640px', '300px'],shade :0,anim:1,content:furl,title:"附件资料",resize:true,type: 2,zIndex: layer.zIndex});}});obtainFileSize($fileSize);}
function refOutFileSize(){var $fileSize = outFileObject || $('.file-size');obtainFileSize($fileSize);}
function obtainFileSize(fileObject) {var $fileSize = fileObject || $('.file-size');var params = [];$.each($fileSize,function (i,o) {var $that = $(o);params.push({"docId":$that.attr('docId'),"docType":$that.attr('docType')});});if(params.length==0){return;}$fileSize.find('span[fs]').html('<img src="'+rurl('images/indicator.gif')+'"/>');$.ajax({url: rurl('file/upload/obtainFileSize'), data: JSON.stringify(params), type: 'post', dataType: 'json', contentType: 'application/json', success: function (res) {$fileSize.find('span[fs]').html('0');$.each(res.data, function (i, o) {var showObj = $('a[docId="' + o.docId + '"][docType="' + o.docType + '"]').find('span[fs]');showObj.html(o.count);});}});}
var statuswin=null;function initDocumentStatus(){$('.document-status').click(function () {layer.close(statuswin);var furl = rurl('template/document-status-history.html?documentId='+$(this).attr('documentId'));statuswin = layer.open({area: ['600px', '280px'],shade :0,anim:1,content:furl,title:"历史操作",resize:true,type: 2,zIndex: layer.zIndex});});}
function tableInitAll(){hide_loading();startCopyContent();refTask();initFileSize();initDocumentStatus();formatTableData();}
function getNowFormatDate(){var date = new Date();var seperator1 = "-";var year = date.getFullYear();var month = date.getMonth() + 1;var strDate = date.getDate();if (month >= 1 && month <= 9) {month = "0" + month;}if (strDate >= 0 && strDate <= 9) {strDate = "0" + strDate;}var currentdate = year + seperator1 + month + seperator1 + strDate;return currentdate;}
function getNowFormatTime(){var date = new Date(),seperator1 = "-",year = date.getFullYear(),month = date.getMonth() + 1,strDate = date.getDate(),hours = date.getHours(),minute = date.getMinutes();if (month >= 1 && month <= 9) {month = "0" + month;}if (strDate >= 0 && strDate <= 9) {strDate = "0" + strDate;}if(hours >= 0 && hours <= 9) {hours = "0" + hours;}if(minute >= 0 && minute <= 9) {minute = "0" + minute;}var currentdate = year + seperator1 + month + seperator1 + strDate +" "+hours+":"+minute;return currentdate;}
function getNowFormatTimeMill(){var date = new Date(),seperator1 = "-",year = date.getFullYear(),month = date.getMonth() + 1,strDate = date.getDate(),hours = date.getHours(),minute = date.getMinutes(),second = date.getSeconds();if (month >= 1 && month <= 9) {month = "0" + month;}if (strDate >= 0 && strDate <= 9) {strDate = "0" + strDate;}if(hours >= 0 && hours <= 9) {hours = "0" + hours;}if(minute >= 0 && minute <= 9) {minute = "0" + minute;}if(second >= 0 && second <= 9) {second = "0" + second;}var currentdate = year + seperator1 + month + seperator1 + strDate +" "+hours+":"+minute+":"+second;return currentdate;}
function exportFile(id){toUrl('scm/export/down?id='+id);}
function exportExcel (o){show_loading();$.post(rurl(o.url),$('#queryForm').serialize(),function (res) {if(isSuccess(res)){exportFile(res.data.fileId);}})}
function verifyEmptyf(ele,msg) {var $obj = $(ele);if(isEmpty($obj.val())){show_error(msg || '必填项不能为空');$obj.focus();return true;}return false;}
function uuid(){var d = new Date().getTime();if (window.performance && typeof window.performance.now === "function") {d += performance.now();}var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {var r = (d + Math.random() * 16) % 16 | 0;d = Math.floor(d / 16);return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);});return uuid;}
function disableForm(form,parelement){$((parelement||'body')).find('input,textarea,select').attr({'disabled':true,'lay-verify':null,'placeholder':null}).addClass('layui-disabled').removeClass('required');form.render('select');$((parelement||'body')).find('.layui-edge').remove();}
function isSj(csc,iaqr) {if(!isEmpty(csc)&&csc.indexOf('A')!=-1){return true;}if(!isEmpty(iaqr)&&(iaqr.indexOf('M')!=-1 || iaqr.indexOf('L')!=-1)){return true;}return false;}
function isOz(csc){if(!isEmpty(csc)&&csc.indexOf('O')!=-1){return true;}return false;}
function tipsMessage(innerText,ele,flag){
    if(flag){
        setTimeout(function(){
            layer.tips(innerText,ele,{tips: [2,'#ff0000'], tipsMore: true, time:2000});
        },500);
    }else{
        layer.tips(innerText,ele,{tips: [2,'#ff0000'], tipsMore: true, time:2000});
    }
}
function verificationTips($inp,msg,esize) {tipsMessage(msg,$inp);$inp.addClass('layui-form-danger').one('blur',function (){$inp.removeClass('layui-form-danger');});if(esize==0){$inp.select();}}
function verificationEmpty($inp,msg,esize) {
    if(isEmpty($inp.val())){
        verificationTips($inp,msg,esize);return 1;
    }
    return 0;
}