$(function () {
    autoCustomer();
    autoSupplier();
});
function autoCustomer() {
    $('.auto-customer').autocomplete({
        source: function (request, response) {
            $.post(rurl('scm/customer/select'),{name: trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: item.name+'('+item.code+')', value: item.name, data: item}}));
            });
        }, minLength: 0,autoFocus:true, delay: 300,
        select: function( event, ui ) {
            try{var data = ui.item.data;fullCustomerData(data);}catch (e) {}
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    }).blur(function () {
        if(isEmpty($(this).val())){
            try{fullCustomerData({id:'',name:''});}catch (e) {}
        }
    });
}
function autoSupplier() {
    $('.auto-supplier').autocomplete({
        source: function (request, response) {
            $.post(rurl('scm/supplier/vague'),{customerName:$('#customerName').val(),name: trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: item.name, value: item.name, data: item}}));
            });
        }, minLength: 0,autoFocus:true, delay: 300,
        select: function( event, ui ) {
            try{var data = ui.item.data;fullSupplierData(data);}catch (e) {}
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
}
function autoCurrency() {
    var currencystr = getLocalStorage('static_currency');
    if(isEmpty(currencystr)){
        $.get(rurl('scm/currency/select'),{},function (res) {
           if(isSuccess(res)){
               var currencyData = [];
               $.each(res.data,function (i,o) {
                   currencyData.push({name:o.name,code:o.id});
               });
               if(currencyData.length>0){
                   setLocalStorage('static_currency',JSON.stringify(currencyData));
               }
               bindCurrency(currencyData);
           }
        });
    }else{
        var currencyData = JSON.parse(currencystr);
        bindCurrency(currencyData);
    }
}
function bindCurrency(datas) {
    var fdatas = [];
    $.each(datas,function (i,o) {
        fdatas.push({value:o.name,label:o.name+'('+o.code+')',code:o.code});
    });
    $('.auto-currency').autocomplete({
        source:fdatas, minLength: 0,autoFocus:true,selectFirst:true,
        select: function( event, ui ) {
            try{var data = ui.item;fullCurrencyData(data);}catch (e) {}
        }
    }).focus(function (){
        $(this).select().autocomplete("search");
    });
}
function autoUnit() {
    var unitstr = getLocalStorage('static_unit');
    if(isEmpty(unitstr)){
        $.get(rurl('system/unit/select'),{},function (res) {
            if(isSuccess(res)){
                var unitData = [];
                $.each(res.data,function (i,o) {
                    unitData.push({value:o.name,label:o.name+'('+o.id+')',code:o.id});
                });
                if(unitData.length>0){
                    setLocalStorage('static_unit',JSON.stringify(unitData));
                }
                bindUnit(unitData);
            }
        });
    }else{
        var unitData = JSON.parse(unitstr);
        bindUnit(unitData);
    }
}
function bindUnit(datas) {
    $('.auto-unit').autocomplete({
        source:datas, minLength: 0,autoFocus:true,
        select: function( event, ui ) {
            try{var data = ui.item;fullUnitData(data);}catch (e) {}
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
}
function autoCountry() {
    var countrystr = getLocalStorage('static_country');
    if(isEmpty(countrystr)){
        $.get(rurl('system/country/select'),{},function (res) {
            if(isSuccess(res)){
                var countryData = [];
                $.each(res.data,function (i,o) {
                    countryData.push({code:o.id,name:o.name});
                });
                if(countryData.length>0){
                    setLocalStorage('static_country',JSON.stringify(countryData));
                    bindCountry(countryData);
                }
            }
        });
    }else{
        var countryData = JSON.parse(countrystr);
        bindCountry(countryData);
    }
}
function bindCountry(datas){
    var fdatas = [];
    $.each(datas,function (i,o) {
        fdatas.push({value:o.name,label:o.name+'('+o.code+')',code:o.code});
    });
    $('.auto-country').autocomplete({
        source:fdatas, minLength: 0,autoFocus:true,
        select: function( event, ui ) {
            try{var data = ui.item;fullCountryData(data);}catch (e) {}
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
}
function autoBaseBank() {
    $('.auto-base-bank').autocomplete({
        source: function (request, response) {
            $.post(rurl('system/banks/select'),{name: trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: item.name, value: item.name, data: item}}));
            });
        }, minLength: 2,autoFocus:true, delay: 500
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
}
function autoAllModel() {
    $('.auto-customer-model').autocomplete({
        source: function (request, response) {
            $.post(rurl('scm/materiel/vague'),{customerName:'',model: trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: '型号：'+item.model+' 品牌：'+item.brand+' 品名：'+item.name, value: item.model, data: item}}));
            });
        }, minLength: 0,autoFocus:true, delay: 500,
        select: function( event, ui){
            try{var data = ui.item.data;fullCustomerModelData(this,data);}catch (e) {}
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
}

/**
 * 出口按型号模糊搜索
 */
function autoAllModelExp() {
    $('.auto-customer-model-exp').autocomplete({
        source: function (request, response) {
            if (isNotEmpty($("#customerName").val())) {
                $.post(rurl('scm/materielExp/vague'), {
                    customerName: $("#customerName").val(),
                    model: trim(request.term)
                }, function (res) {
                    response($.map(res.data, function (item) {
                        return {
                            label: '型号：' + item.model + ' 品牌：' + item.brand + ' 品名：' + item.name + ' 规格型号：' + item.spec,
                            value: item.model,
                            data: item
                        }
                    }));
                });
            }
        }, minLength: 0, autoFocus: true, delay: 500,
        select: function (event, ui) {
            try {
                var data = ui.item.data;
                fullCustomerModelDataExp(this, data);
            } catch (e) {
            }
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
}

/**
 * 出口按名称模糊搜索
 */
function autoAllNameExp() {
    $('.auto-customer-name-exp').autocomplete({
        source: function (request, response) {
            if(isNotEmpty($("#customerName").val())) {
                $.post(rurl('scm/materielExp/vague'),{customerName:$("#customerName").val(),name: trim(request.term)},function (res) {
                    response($.map(res.data, function (item) {return {label: '品名：' + item.name + ' 型号：'+item.model+' 品牌：'+item.brand+' 规格型号：'+item.spec, value: item.name, data: item}}));
                });
            }
        }, minLength: 0,autoFocus:true, delay: 500,
        select: function( event, ui){
            try{var data = ui.item.data;fullCustomerNameDataExp(this,data);}catch (e) {}
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
}

/**
 * 出口按品牌模糊搜索
 */
function autoAllBrandExp() {
    $('.auto-customer-brand-exp').autocomplete({
        source: function (request, response) {
            if(isNotEmpty($("#customerName").val())) {
                $.post(rurl('scm/materielExp/vague'),{customerName:$("#customerName").val(),brand: trim(request.term)},function (res) {
                    response($.map(res.data, function (item) {return {label: '品牌：' + item.brand + ' 型号：'+item.model+' 品名：'+item.name+' 规格型号：'+item.spec, value: item.brand, data: item}}));
                });
            }
        }, minLength: 0,autoFocus:true, delay: 500,
        select: function( event, ui){
            try{var data = ui.item.data;fullCustomerBrandDataExp(this,data);}catch (e) {}
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
}