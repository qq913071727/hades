$(function () {
    autoCustomer();
});
function autoCustomer() {
    $('.auto-customer').autocompleter({
        cache:false,subKey:'name',delay:300,
        source:rurl('scm/customer/select'),
        formatData:function (res) {
            var list = [];
            if(isSuccess(res)){
                $.each((res.data || []),function (i,o) {
                    list.push({id:o.id,value:o.name,label:o.name.concat(' (').concat(o.code).concat(')')});
                });
            }
            return list;
        }
    });
}