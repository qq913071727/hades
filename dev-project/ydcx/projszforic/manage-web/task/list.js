layui.use(['layer','table'], function() {
    var layer = layui.layer, table = layui.table,
    taskTable = table.render({
        elem: '#table-list',url:rurl('scm/taskNotice/list'),method:'post',height:'full-0',
        page: true,limits: limits,limit:limit,id:'tab-list',response:{msgName:'message',statusCode: '1'},
        text:{none:'暂无需要处理的任务'}
        ,cols: [[
            {type:'operate',title:'操作',align:'center',width:50,toolbar:'#operateTemp'}
            ,{field:'operationName',title:'任务',width:140,toolbar:'#nameTemp'}
            ,{field:'content',title:'任务描述'}
            ,{field:'dateCreated',title:'时间',align:'center',width:116}
        ]]
    });
    table.on('tool(table-list)', function(item){
        var rowData = item.data;
        if(item.event === 'operateOperation'){
            var actionParams = JSON.parse(rowData['actionParams']),taskArg={"documentId":rowData.documentId,"documentClass":rowData.documentType,"operation":rowData.operationCode};
            show_loading();
            $.post(rurl('scm/task/check'),taskArg,function (res) {
                hide_loading();
                if(res.code != "1"){
                    show_error('当前任务已被执行');
                    $.post(rurl('scm/task/deleteTaskNotice'),taskArg,function (dre) {
                        taskTable.reload({page:{curr:1},height:'full-0'});
                        parent.newTask();
                    });
                }else{
                    closeThisWin();
                    setLocalStorage('TASK_'+actionParams['pmid'],rowData['queryParams']);
                    if("-1"!=rowData.documentId){
                        setLocalStorage('AUTO_TASK',JSON.stringify(taskArg));
                    }
                    parent.refreshTab(actionParams['pmid']);
                }
                if(rowData.isExeRemove){
                    $.post(rurl('scm/task/deleteTaskNotice'),taskArg,function (dre) {});
                }
            });
        }else if(item.event === 'ignoreOperation'){
            delete_inquiry('您是否确定不再接收【'+rowData.operationName+'】的任务通知？','scm/taskNotice/dislikeTaskNotice',{'operationCode':rowData.operationCode},function (res) {
                show_success('设置成功，您后续将不再收到【' + rowData.operationName + "】的任务通知");
                taskTable.reload({page:{curr:1},height:'full-0'});
                parent.newTask();
            });
        }
    });
});
