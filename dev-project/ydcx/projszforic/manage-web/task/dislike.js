layui.use(['layer','table'], function() {
    var layer = layui.layer, table = layui.table;
    loadDislikes();
});

function like(e) {
    const operationCode = $(e).attr("operationCode");
    const operationName =  $(e).attr("operationName");
    $.post(rurl('scm/taskNotice/likeTaskNotice'),{'operationCode':operationCode},function (res) {
        if(isSuccess(res)){
            show_success('设置成功，您后续将可以收到【' + operationName + "】的任务通知");
            //重新渲染按钮集合
            $(e).remove();
        }
    });
}

function loadDislikes() {
    $.get(rurl('scm/taskNotice/dislikeTaskNoticeList'),{},function (res) {
        var data = res.data,html = [];
        $.each(data,function (i,o) {
            html.push('<button type="button" class="layui-btn layui-btn-primary layui-btn-sm" operationCode="' + o.operationCode + '" operationName=' + o.operationCodeDesc + ' onclick="like(this)">'+o.operationCodeDesc+'</button>');
        });
        $('#taskGroups').html(html.join(''));
    });
}