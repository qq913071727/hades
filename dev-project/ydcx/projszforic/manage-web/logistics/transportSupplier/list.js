layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listQueryArg:{
            url:rurl('scm/transportSupplier/page')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'name', width:300, title: '供应商名称',templet:'#nameTemp'}
                ,{field:'linkPerson', width:150, title: '联系人'}
                ,{field:'tel', width:150, title: '电话'}
                ,{field:'fax', width:150, title: '传真'}
                ,{field:'disabled', width:100,align:'form-switch',title: '状态',templet:'#disabledTemp'}
                ,{field:'address', title: '地址'}
                ,{field:'dateCreated', width:135, title: '登记日期'}
                ,{field:'memo', title: '备注'}
            ]]
        }
    });
    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/transportSupplier/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });
});
//运输供应商详情信息
function showDetail(id) {
    openwin(rurl('logistics/transportSupplier/detail.html?id='+id),'运输供应商详情',{area: ['0', '0'],shadeClose:true});
}