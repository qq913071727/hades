layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table,fileupload = layui.fileupload;
    var id = getUrlParam("id");
    show_loading();
    $.get(rurl("/scm/transportSupplier/info"),{"id":id},function (res) {
        if(isSuccess(res)) {
            var data = res.data;
            form.val("baseForm",data.transportSupplier);
            var conveyances = data.conveyances || [];
            conveyanceList(conveyances);
            disableForm(form);
        }
    });
});

function conveyanceList(data) {
    layui.table.render({
        elem: '#conveyance-list',limit:1000,id:'conveyance-list'
        ,cols: [[
            {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
            ,{field: 'conveyanceNo', title: '车牌1', width: 120}
            ,{field: 'conveyanceNo2', title: '车牌2', width: 120}
            ,{field: 'customsRegNo', title: '海关注册编号', width: 140}
            ,{field: 'driver', title: '司机', width: 140}
            ,{field: 'driverNo', title: '司机身份证号码', width: 160}
            ,{field: 'driverTel', title: '司机电话', width: 120}
            ,{field: 'head', title: '牌头', width: 150}
            ,{field: 'headTel', title: '企业电话', width: 140}
            ,{field: 'headFax', title: '企业传真', width: 140}
            ,{field: 'headNo', title: '企业代码', width: 140}
            ,{field: 'disabled', title: '状态', width: 80,align:'center',templet:'#disabledTemp'}
            ,{field: 'isDefault', title: '默认', width: 65,align:'center',templet:'#isDefaultTemp'}
        ]]
        ,data: data
    });
}
