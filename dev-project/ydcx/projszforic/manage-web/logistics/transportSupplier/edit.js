layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table,fileupload = layui.fileupload;
    var id = getUrlParam("id");
    show_loading();
    $.get(rurl("/scm/transportSupplier/info"),{"id":id},function (res) {
        if(isSuccess(res)) {
            var data = res.data;
            form.val("baseForm",data.transportSupplier);

            var conveyances = data.conveyances || [];
            conveyanceList(conveyances);
        }
    });

    table.on('tool(conveyance-list)', function(obj){
        var data = obj.data;
        if(obj.event === 'deDel'){
            layer.confirm('您确定要删除此运输工具信息？', function(index){
                obj.del();
                conveyanceList(obtainConveyanceData());
                layer.close(index);
            });
        }
    });
    $('.save-btn').click(function () {
        if(checkedForm($('.tsf-page-content'))){
            show_loading();
            var transportSupplier = $('#baseForm').serializeJson(),conveyances = obtainConveyanceData();
            postJSON('scm/transportSupplier/edit',{"transportSupplier":transportSupplier,"conveyances":conveyances},function (res) {
                parent.reloadList();
                closeThisWin('添加成功');
            });
        }
    });
});

function conveyanceList(data) {
    layui.table.render({
        elem: '#conveyance-list',limit:1000,id:'conveyance-list',text:{none:'<a href="javascript:" class="def-a" onclick="addConveyance()"><i class="fa fa-plus-circle"></i>添加一行</a>'}
        ,cols: [[
            {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
            ,{field: 'conveyanceNo', title: '车牌1', width: 120,align:'form',templet:'#conveyanceNoTemp'}
            ,{field: 'conveyanceNo2', title: '车牌2', width: 120,align:'form',templet:'#conveyanceNo2Temp'}
            ,{field: 'customsRegNo', title: '海关注册编号', width: 140,align:'form',templet:'#customsRegNoTemp'}
            ,{field: 'driver', title: '司机', width: 140,align:'form',templet:'#driverTemp'}
            ,{field: 'driverNo', title: '司机身份证号码', width: 160,align:'form',templet:'#driverNoTemp'}
            ,{field: 'driverTel', title: '司机电话', width: 120,align:'form',templet:'#driverTelTemp'}
            ,{field: 'head', title: '牌头', width: 150,align:'form',templet:'#headTemp'}
            ,{field: 'headTel', title: '企业电话', width: 140,align:'form',templet:'#headTelTemp'}
            ,{field: 'headFax', title: '企业传真', width: 140,align:'form',templet:'#headFaxTemp'}
            ,{field: 'headNo', title: '企业代码', width: 140,align:'form',templet:'#headNoTemp'}
            ,{field: 'disabled', title: '状态', width: 80,align:'center',templet:'#disabledTemp'}
            ,{field: 'isDefault', title: '默认', width: 65,align:'center',templet:'#isDefaultTemp'}
            ,{field: 'oper', title: '操作', width: 65,toolbar: '#deOperaBar',fixed: 'right',align:'center'}
        ]]
        ,data: data
    });
}

function obtainConveyanceData() {
    var data = []
        ,$conveyanceForm = $('#conveyanceForm')
        ,$ids = $conveyanceForm.find('input[name="id"]')
        ,$conveyanceNos = $conveyanceForm.find('input[name="conveyanceNo"]')
        ,$conveyanceNo2s = $conveyanceForm.find('input[name="conveyanceNo2"]')
        ,$customsRegNos = $conveyanceForm.find('input[name="customsRegNo"]')
        ,$drivers = $conveyanceForm.find('input[name="driver"]')
        ,$driverNos = $conveyanceForm.find('input[name="driverNo"]')
        ,$driverTels = $conveyanceForm.find('input[name="driverTel"]')
        ,$heads = $conveyanceForm.find('input[name="head"]')
        ,$headTels = $conveyanceForm.find('input[name="headTel"]')
        ,$headFaxs = $conveyanceForm.find('input[name="headFax"]')
        ,$headNos = $conveyanceForm.find('input[name="headNo"]')
        ,$disableds = $conveyanceForm.find('input[name="disabled"]')
        ,$isDefaults = $conveyanceForm.find('input[name="isDefault"]');
    for(var i=0;i<$ids.size();i++){
        var item = {
            id:$($ids[i]).val(),
            conveyanceNo:$($conveyanceNos[i]).val(),
            conveyanceNo2:$($conveyanceNo2s[i]).val(),
            customsRegNo:$($customsRegNos[i]).val(),
            driver:$($drivers[i]).val(),
            driverNo:$($driverNos[i]).val(),
            driverTel:$($driverTels[i]).val(),
            head:$($heads[i]).val(),
            headTel:$($headTels[i]).val(),
            headFax:$($headFaxs[i]).val(),
            headNo:$($headNos[i]).val(),
            disabled:($($disableds[i]).parent().find('.layui-form-onswitch').size()>0)?false:true,
            isDefault:($($isDefaults[i]).parent().find('.layui-form-radioed').size()>0)?true:false
        }
        data.push(item);
    }
    return data;
}
function addConveyance() {
    var data = obtainConveyanceData();
    data.push({id:'', conveyanceNo:'', conveyanceNo2:'', customsRegNo:'',driver:'',driverNo:'', driverTel:'',head:'', headTel:'', headFax:'',headNo:'',   disabled:false, isDefault:(data.length==0)?true:false});
    conveyanceList(data);
}
