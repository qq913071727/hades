layui.config({base: '/logistics/crossBorderWaybill/'}).extend({detailTemp: 'detail-main-temp'}).use(['layer','detailTemp','table'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,table=layui.table;
    var id = getUrlParam("id");
    $('#waybillDetail').load(rurl('logistics/crossBorderWaybill/detail-main-temp.html'),function () {
        detailTemp.render(id,'waybill_bind');
        var taskArg = {'operation':'waybill_bind','documentId':id,'documentClass':'CrossBorderWaybill','newStatusCode':''};
        $('#backUpdate').click(function () {
            taskArg['newStatusCode'] = 'waitConfirm';
            taskArg['operation'] = 'waybill_back';
            delete_inquiry('退回修改将会解除所有已经绑定订单是否继续？','scm/crossBorderWaybill/backUpdate',taskArg,function (res) {
                parent.reloadList();
                closeThisWin('操作成功');
            });
        });
        $('#bindComplete').click(function () {
            taskArg['newStatusCode'] = 'waitReview';
            taskArg['operation'] = 'waybill_bind';
            show_loading();
            $.post(rurl('scm/crossBorderWaybill/bindComplete'),taskArg,function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            })
        });
    });
    //渲染已绑订单
    table.render({
        elem: '#binded-list',page:false,limit:1000,id:'binded-list',url:rurl('scm/crossBorderWaybill/binded?id='+id)
        ,response:{msgName:'message',statusCode: '1'},text:{none:'还未绑定任何订单数据'},height:'300',totalRow: true
        ,cols: [[
            {field: 'operation',width: 60,title: '操作',templet: '#boundTemp',align: 'center', totalRowText: '合计'}
            ,{type: 'numbers',width: 45,title: '序号',align: 'center'}
            ,{field: 'customerName',width: 260,title: '客户名称'}
            ,{field: 'docNo',width: 120,title: '订单号'}
            ,{field: 'orderDate',width: 90,title: '订单日期'}
            ,{field: 'statusDesc',width: 80,title: '订单状态',align: 'center',templet:'#orderStatusTemp'}
            ,{field: 'contrNo',width: 120,title: '合同号'}
            ,{field: 'declareStatusDesc',width: 80,title: '报关单状态',align: 'center',templet:'#decStatusTemp'}
            ,{field: 'packNo',width: 65,title: '总件数',align: 'right', totalRow: true}
            ,{field: 'receivingModeName',width: 90,title: '国内送货方式'}
            ,{field: 'deliveryCompanyName',width: 220,title: '收货公司'}
            ,{field: 'deliveryAddress',title: '收货地址'}
        ]]
    });
    //渲染可绑订单
    table.render({
        elem: '#canbind-list',page:false,limit:1000,id:'canbind-list',url:rurl('scm/crossBorderWaybill/canBind?id='+id)
        ,response:{msgName:'message',statusCode: '1'},text:{none:'暂无可绑订单数据'},height:'400',totalRow: true
        ,cols: [[
            {field: 'operation',width: 60,title: '操作',templet: '#bindTemp',align: 'center', totalRowText: '合计'}
            ,{type: 'numbers',width: 45,title: '序号',align: 'center'}
            ,{field: 'customerName',width: 260,title: '客户名称'}
            ,{field: 'docNo',width: 120,title: '订单号'}
            ,{field: 'orderDate',width: 90,title: '订单日期'}
            ,{field: 'statusDesc',width: 80,title: '订单状态',align: 'center',templet:'#orderStatusTemp'}
            ,{field: 'contrNo',width: 120,title: '合同号'}
            ,{field: 'declareStatusDesc',width: 80,title: '报关单状态',align: 'center',templet:'#decStatusTemp'}
            ,{field: 'iePortName',width: 120,title: '进境关别'}
            ,{field: 'packNo',width: 65,title: '总件数',align: 'right', totalRow: true}
            ,{field: 'receivingModeName',width: 90,title: '国内送货方式'}
            ,{field: 'deliveryCompanyName',width: 220,title: '收货公司'}
            ,{field: 'deliveryAddress',title: '收货地址'}
        ]]
    });
    table.on('tool(binded-list)', function(item){
        var rowData = item.data;
        if(item.event === 'unBindOrder'){
            show_loading();
            $.post(rurl('scm/crossBorderWaybill/unbindOrder'),{"id":id,"orderId":rowData.id},function (res) {
                if(isSuccess(res)){
                    show_msg("解绑成功");
                    table.reload('binded-list',{height: '300'});
                    table.reload('canbind-list',{height: '400'});
                }
            });
        }
    });
    table.on('tool(canbind-list)', function(item){
        var rowData = item.data;
        if(item.event === 'bindOrder'){
            show_loading();
            $.post(rurl('scm/crossBorderWaybill/bindOrder'),{"id":id,"orderId":rowData.id},function (res) {
                if(isSuccess(res)){
                    show_msg("绑定成功");
                    table.reload('binded-list',{height: '300'});
                    table.reload('canbind-list',{height: '400'});
                }
            });
        }
    });
});
