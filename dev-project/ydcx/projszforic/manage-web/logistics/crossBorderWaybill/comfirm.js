layui.config({base: '/logistics/crossBorderWaybill/'}).extend({detailTemp: 'detail-main-temp'}).use(['layer','detailTemp','laytpl'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,laytpl=layui.laytpl;
    var id = getUrlParam("id");
    $('#waybillDetail').load(rurl('logistics/crossBorderWaybill/detail-main-temp.html'),function () {
        detailTemp.render(id,'waybill_confirm');

        var $operationTaskBtn = $('.operation-task-btn'),
            taskArg = {'operation':'waybill_confirm','documentId':id,'documentClass':'CrossBorderWaybill','newStatusCode':''};
        $operationTaskBtn.click(function () {
            var $that = $(this),needMemo = $that.attr('need-memo');
            if(!isEmpty(needMemo)){
                var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
                taskArg['memo'] = $memo.val();
            }else{
                taskArg['memo'] = $('#taskMemo').val();
            }
            taskArg['newStatusCode'] = $that.attr('status-code');

            if(taskArg['newStatusCode']=="remove"){
                delete_inquiry('您确定要删除此操作记录？','scm/crossBorderWaybill/delete',{'id':id},function () {
                    parent.reloadList();
                    closeThisWin('操作成功');
                });
            }else{
                show_loading();
                $.post(rurl('scm/crossBorderWaybill/confirm'),taskArg,function (res) {
                    if(isSuccess(res)){
                        parent.reloadList();
                        closeThisWin('操作成功');
                    }
                });
            }
        });
    });
    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
    $('#toUpdatePage').click(function () {
        closeThisWin();parent.editWaybill();
    });
});