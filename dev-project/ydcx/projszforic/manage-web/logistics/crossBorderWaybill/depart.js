layui.config({base: '/logistics/crossBorderWaybill/'}).extend({detailTemp: 'detail-main-temp'}).use(['layer','detailTemp','table'], function() {
    var layer = layui.layer, detailTemp = layui.detailTemp, table = layui.table;
    var id = getUrlParam("id");
    $('#waybillDetail').load(rurl('logistics/crossBorderWaybill/detail-main-temp.html'),function () {
        detailTemp.render(id, 'waybill_depart');
        $('#departureDate').val(getNowFormatTime());
        $('#confirmDeparture').click(function () {
            show_loading();
            var params = $('#operationForm').serializeJson();
            params['id'] = id;
            $.post(rurl('scm/crossBorderWaybill/depart'),params,function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
        $('#backReview').click(function () {
            layer.prompt({formType:2,maxlength: 500,title:'请填写退回原因'},function(val, index){
                var taskArg = {'operation':'waybill_depart','documentId':id,'documentClass':'CrossBorderWaybill','newStatusCode':'waitReview',memo:val};
                show_loading();
                $.post(rurl('scm/crossBorderWaybill/processJump'),taskArg,function (res) {
                    if(isSuccess(res)){
                        parent.reloadList();
                        closeThisWin('操作成功');
                    }
                });
            });

        });
    });
    //渲染已绑订单
    table.render({
        elem: '#binded-list',page:false,limit:1000,id:'binded-list',url:rurl('scm/crossBorderWaybill/binded?id='+id)
        ,response:{msgName:'message',statusCode: '1'},text:{none:'还未绑定任何订单数据'},totalRow: true
        ,cols: [[
            {type: 'numbers',width: 45,title: '序号',align: 'center'}
            ,{field: 'customerName',width: 260,title: '客户名称'}
            ,{field: 'docNo',width: 120,title: '订单号'}
            ,{field: 'orderDate',width: 90,title: '订单日期'}
            ,{field: 'statusDesc',width: 80,title: '订单状态',align: 'center',templet:'#orderStatusTemp'}
            ,{field: 'contrNo',width: 120,title: '合同号'}
            ,{field: 'declareStatusDesc',width: 80,title: '报关单状态',align: 'center',templet:'#decStatusTemp'}
            ,{field: 'packNo',width: 65,title: '总件数',align: 'right', totalRow: true}
            ,{field: 'receivingModeName',width: 90,title: '国内送货方式'}
            ,{field: 'deliveryCompanyName',width: 220,title: '收货公司'}
            ,{field: 'deliveryAddress',title: '收货地址'}
        ]]
    });
});