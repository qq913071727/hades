layui.define(['layer','laytpl','element','table'], function(exports){
    var layer = layui.layer,laytpl=layui.laytpl,element=layui.element,table=layui.table,
    detailTemp = {
        render:function (id,operation) {
            show_loading();
            $.get(rurl('/scm/overseasDeliveryNote/detail'),{'id':id,'operation':operation},function (res) {
                if(isSuccessWarnClose(res)){
                    detailTemp.renderData(res.data);
                }
            });
        },
        renderData(data){
            var deliveryNote = data.overseasDeliveryNote,members=data.members||[];
            laytpl($('#mainViewTemp').html()).render(deliveryNote,function (html) {
                $('#mainView').html(html);
            });
            table.render({
                elem: '#deliveryMemberTable',limit:5000,height:'full-208',totalRow: true,page:false
                ,cols: [[
                    {type:'numbers',title:'序号',width:45}
                    ,{field:'expOrderNo',title:'订单号',width:120,totalRowText: '合计'}
                    ,{field:'model',title:'型号',width:200}
                    ,{field:'brand',title:'品牌',width:120}
                    ,{field:'name',title:'名称',width:120}
                    ,{field:'quantity',title:'派送数量',width:90,align:'right',totalRow: true}
                    ,{field:'cartonNum',title:'件数',width:90,align:'right',totalRow: true}
                    ,{field:'grossWeight',title:'毛重',width:90,align:'right',totalRow: true,totalRowType: 'num4'}
                    ,{field:'netWeight',title:'净重',width:90,align:'right',totalRow: true,totalRowType: 'num4'}
                    ,{field:'unitName',title:'单位',width:50,align:'center'}
                ]]
                , data: members
            });
            formatTableData();
        }
    };
    exports('detailTemp', detailTemp);
});