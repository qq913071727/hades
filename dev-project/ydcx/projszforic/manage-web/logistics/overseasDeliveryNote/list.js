layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');

        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('scm/overseasDeliveryNote/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'statusDesc', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                    ,{field:'docNo', width:126, title: '系统单号',templet:'#docNoTemp', fixed: true}
                    ,{field:'expOrderNos', width:180, title: '订单编号'}
                    ,{field:'deliveryDate', width:120, title: '送货时间'}
                    ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                    ,{field:'receivingNo', width:120, title: '入仓号'}
                    ,{field:'receivingWarehouse', width:180, title: '收货仓库'}
                    ,{field:'consignee', width:180, title: '收货单位'}
                    ,{field:'consigneeAddress', width:180, title: '收货地址'}
                    ,{field:'memo', title: '备注'}
                ]]
            },listQueryDone:function () {

            }
        });
    });

});
function fullCustomerData(data) {
    $('#customerId').val(data.id);
}
function editWaybill(){$('button[operation-code="overseas_delivery_note_edit"]').click();}
function showDetail(id) {
    openwin(rurl('logistics/overseasDeliveryNote/show-detail.html?id='+id),'境外送货单详情',{area: ['0', '0'],shadeClose:true});
}
function removeOverseasDeliveryNote(o) {
    let data = getCheckDatas();
    if(data.length==1){
        let item = data[0];
        delete_inquiry('当前单据状态<span class="f16b '+item.statusId+'">'+item.statusDesc+'</span>，是否确定要删除？',o.url,{'id':item.id},function (res) {
            show_success('删除成功');
            reloadList();
        });
    }else{
        show_error('请选择一条您要删除的送货单信息');
    }
}