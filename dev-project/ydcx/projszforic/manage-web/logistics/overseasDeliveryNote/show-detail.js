layui.config({base: '/logistics/overseasDeliveryNote/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#showDetail').load(rurl('logistics/overseasDeliveryNote/detail-temp.html'),function () {
        detailTemp.render(id,'');
    });
});