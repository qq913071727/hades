layui.use(['layer','form','table'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table;
    var overseasDeliveryNoteId = getUrlParam('overseasDeliveryNoteId');
    var customerId = getUrlParam('customerId');
    if(isEmpty(overseasDeliveryNoteId)){
        closeThisWin('请先保存境外派送单');
    }
    $('#customerId').val(customerId);
    table.render({
        elem:'#table-list',id:'table-list',limits: [100,200,500],limit:100,page:true,height: 'full-106',method:'post',where:$('.query-form').serializeJson(),
        url:'/scm/overseasDeliveryNote/listBindMembers',response:{msgName:'message',statusCode: '1'}
        ,cols: [[
            {checkbox: true, fixed: true}
            ,{field:'orderDate', width:80, title: '订单时间'}
            ,{field:'expOrderNo', width:110, title: '订单号'}
            ,{field:'model', width:160, title: '型号'}
            ,{field:'brand', width:120, title: '品牌'}
            ,{field:'name', width:120, title: '名称'}
            ,{field:'unitName', width:70, title: '单位'}
            ,{field:'quantity', width:100, title: '总数量',align:'right'}
            ,{field:'stockQuantity', width:100, title: '可派送数量',align:'right'}
            ,{field:'deliveryQuantity', width:100, title: '本次派送数量',templet:'#deliveryQuantityTemp',fixed:'right'}
            ,{field:'cartonNum', width:100, title: '总件数',align:'right'}
            ,{field:'stockCartonNum', width:100, title: '可派送件数',align:'right'}
            ,{field:'cartonNumber', width:100, title: '本次派送件数',templet:'#cartonNumberTemp',fixed:'right'}
        ]],done:function (){
            initInput();
        }
    });
    $('.search-btn').click(function (){
        table.reload('table-list',{page:{curr:1},where:$('.query-form').serializeJson()});
    });
    $('.save-btn').click(function () {
        var data = table.checkStatus('table-list').data;
        if(data.length==0){
            show_error('请勾选需要派送的订单明细');return;
        }
        var members = [];
        for(var i=0;i<data.length;i++){
            var item = data[i],$outQuantity = $('#qty'+item.id),$carton = $('#carton'+item.id);
            if(isEmpty($outQuantity.val())){
                show_error('订单：' + item.expOrderNo + '，型号：'+item.model+'请输入本次派送数量');return;
            }
            if(isEmpty($carton.val())){
                show_error('订单：' + item.expOrderNo + '，型号：'+item.model+'请输入本次派送件数');return;
            }
            var outQuantity = Math.round(parseFloat($outQuantity.val())*100)/100;
            if(outQuantity <= 0 || outQuantity > item.stockQuantity){
                show_error('订单：' + item.expOrderNo + '，型号：'+item.model+'派送数量必须大于零且小于当前可派送数量');return;
            }
            var outCartonNum = $carton.val();
            if(outCartonNum < 0 || outCartonNum > item.stockCartonNum) {
                show_error('订单：' + item.expOrderNo + '，型号：'+item.model+'派送件数必须大于等于零且小于当前可派送件数');return;
            }
            debugger;
            members.push({
                id:item.id,
                quantity:outQuantity,
                cartonNum:outCartonNum
            })
        }
        postJSON('/scm/overseasDeliveryNote/bindOrderMembers',{
            overseasDeliveryNoteId:overseasDeliveryNoteId,
            members:members
        },function (res){
            show_success('添加成功');
            $('.search-btn').click();
            parent.obtainDetail(overseasDeliveryNoteId);
        });

    });
});
