layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer, laytpl = layui.laytpl, form = layui.form;
    var id = getUrlParam('id'); show_loading();
    $.get(rurl('scm/overseasDeliveryNote/detail'),{'id':id,'operation':''},function (res) {
        if(isSuccessWarnClose(res)){
            var data = res.data;
            var total = {'quantity':0.0,'palletNumber':0,'grossWeight':0.0000,'netWeight':0.0000};
            $.each(data.members || [],function (i,o) {
                total.quantity = Math.round((total.quantity + o.quantity)*100)/100;
                total.palletNumber = total.palletNumber + (null == o.palletNumber ? 0 : o.palletNumber);
                total.grossWeight = Math.round((total.grossWeight + o.grossWeight)*10000)/10000;
                total.netWeight = Math.round((total.netWeight + o.netWeight)*10000)/10000;
            });
            data['total'] = total;
            app.obtainSubject(function (subject) {
                data['subject'] = subject;
                laytpl($('#conTemp').html()).render(data,function (html) {
                    $('#conTempView').html(html);
                });
            });

        }
    });
});