layui.config({base: '/logistics/overseasDeliveryNote/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','laytpl'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,laytpl=layui.laytpl;
    var id = getUrlParam("id");
    $('#waybillDetail').load(rurl('logistics/overseasDeliveryNote/detail-temp.html'),function () {
        detailTemp.render(id,'overseas_delivery_note_confirm');

        var $operationTaskBtn = $('.operation-task-btn'),
            taskArg = {'operation':'overseas_delivery_note_confirm','documentId':id,'documentClass':'OverseasDeliveryNote','newStatusCode':''};
        $operationTaskBtn.click(function () {
            var $that = $(this),needMemo = $that.attr('need-memo');
            if(!isEmpty(needMemo)){
                var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
                taskArg['memo'] = $memo.val();
            }else{
                taskArg['memo'] = $('#taskMemo').val();
            }
            taskArg['newStatusCode'] = $that.attr('status-code');

            if(taskArg['newStatusCode']=="remove"){
                delete_inquiry('您确定要删除此该派送单？','scm/overseasDeliveryNote/remove',{'id':id},function () {
                    parent.reloadList();
                    closeThisWin('操作成功');
                });
            }else{
                show_loading();
                $.post(rurl('scm/overseasDeliveryNote/confirm'),taskArg,function (res) {
                    if(isSuccess(res)){
                        parent.reloadList();
                        closeThisWin('操作成功');
                    }
                });
            }
        });
    });
    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
    $('#toUpdatePage').click(function () {
        closeThisWin();parent.editWaybill();
    });
});