layui.config({base: '/logistics/deliveryWaybill/'}).extend({detailTemp: 'detail-temp'}).use(['layer','form','detailTemp'], function(){
    var layer = layui.layer,form=layui.form,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#detail').load(rurl('logistics/deliveryWaybill/detail-temp.html'),function () {
        detailTemp.render(id,'delivery_way_sign',function(data) {
            var delivery=data.delivery;
            if(delivery.receivingMode=='HkThrough'){
                prompt_warn('港车直送需要在中港运单签收',function (){closeThisWin();parent.reloadList();});
                return;
            }
            form.val('operationForm',{'id':delivery.id,'reachDate':getNowFormatTime()});
        });
        $('#deliWaybillReceipt').attr({'doc-id':id});
        initFileSize();
    });

    $('#confirmSign').click(function () {
        show_loading();
        $.post(rurl('scm/deliveryWaybill/confirmSign'),$('#operationForm').serialize(),function (res) {
            if(isSuccess(res)){
                parent.reloadList();
                closeThisWin('操作成功');
            }
        });
    });
});