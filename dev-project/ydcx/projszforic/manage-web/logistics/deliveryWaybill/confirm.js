layui.config({base: '/logistics/deliveryWaybill/'}).extend({detailTemp: 'detail-temp'}).use(['layer','form','detailTemp'], function(){
    var layer = layui.layer,form=layui.form,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#detail').load(rurl('logistics/deliveryWaybill/detail-temp.html'),function () {
        detailTemp.render(id,'delivery_way_confirm',function(data) {
            var delivery=data.delivery,logistics=data.logistics;
            var $area = $('#'+delivery.receivingMode);
            if($area.length==0){
                prompt_warn('当前配送方式不需要确认',function (){closeThisWin();parent.reloadList();});
                return;
            }
            $area.show();
            form.val('operationForm',{'id':delivery.id,'logisticsCompanyName':logistics.logisticsCompanyName,'experssNo':delivery.experssNo});
        });
    });
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    //根据运输供应商加载运输工具
    form.on("select(transportSupplierId)",function (data) {
        var selectedTransportSupplierId = data.value;
        if(!isEmpty(selectedTransportSupplierId)) {
            $("#conveyanceId").attr("sel-url","scm/conveyance/select?transportSupplierId=" + selectedTransportSupplierId)
                .selLoad(function () {
                    form.render('select');
                });
        }else{
            $("#conveyanceId").get(0).length = 0;
            form.render('select');
        }
    });

    $('#confirm').click(function () {
       show_loading();
       $.post(rurl('scm/deliveryWaybill/confirm'),$('#operationForm').serialize(),function (res) {
          if(isSuccess(res)){
              parent.reloadList();
              closeThisWin('操作成功');
          }
       });
    });
});