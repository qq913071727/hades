layui.define(['layer','laytpl','element','table'], function(exports){
    var layer = layui.layer,laytpl=layui.laytpl,element=layui.element,table=layui.table,
    detailTemp = {
        render:function (id,operation,fn) {
            $.post(rurl('scm/deliveryWaybill/detail'),{'id':id,'operation':operation},function (res) {
                if(isSuccessWarnClose(res)){
                    detailTemp.renderData(res.data);
                    if(fn){
                        fn(res.data);
                    }
                }
            });
        },
        renderData:function(data){
            var delivery=data.delivery,logistics=data.logistics,members=data.members||[];
            laytpl($('#deliveryViewTemp').html()).render(delivery,function (html) {
                $('#deliveryView').html(html);
            });
            laytpl($('#logistiscsViewTemp').html()).render(logistics,function (html) {
                $('#logistiscsView').html(html);
            });
            table.render({
                elem: '#memberTab' ,totalRow: true,limit:5000,page:false
                , cols: [[
                    {type:'numbers',title:'序号',width:45}
                    ,{field:'model',title:'型号',width:180, totalRowText: '合计'}
                    ,{field:'name',title:'名称',width:180}
                    ,{field:'brand',title:'品牌',width:180}
                    ,{field:'goodsCode',title:'客户料号',width:180}
                    ,{field:'quantity',title:'数量',width:90,align:'money', totalRow: true}
                    ,{field:'cartonNum',title:'箱数',width:90,align:'right', totalRow: true}
                    ,{field:'grossWeight',title:'毛重',width:90,align:'money', totalRow: true}
                ]], data: members
            });
            formatTableData();
        }
    };
    exports('detailTemp', detailTemp);
});