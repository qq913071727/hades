layui.config({base: '/logistics/deliveryWaybill/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#detail').load(rurl('logistics/deliveryWaybill/detail-temp.html'),function () {
        detailTemp.render(id,'');
    });
});