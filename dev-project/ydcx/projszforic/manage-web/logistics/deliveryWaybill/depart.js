layui.config({base: '/logistics/deliveryWaybill/'}).extend({detailTemp: 'detail-temp'}).use(['layer','form','detailTemp'], function(){
    var layer = layui.layer,form=layui.form,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#detail').load(rurl('logistics/deliveryWaybill/detail-temp.html'),function () {
        detailTemp.render(id,'delivery_way_depart',function(data) {
            var delivery=data.delivery,logistics=data.logistics;
            var $area = $('#'+delivery.receivingMode);
            if($area.length==0){
                prompt_warn('当前配送方式不需要确认',function (){closeThisWin();parent.reloadList();});
                return;
            }
            $area.show();
            form.val('operationForm',{'id':delivery.id,'departureDate':getNowFormatTime(),'logisticsCompanyName':logistics.logisticsCompanyName,'experssNo':delivery.experssNo,'sealNo':delivery.sealNo});
        });
    });

    $('#confirmDeparture').click(function () {
        show_loading();
        $.post(rurl('scm/deliveryWaybill/confirmDeparture'),$('#operationForm').serialize(),function (res) {
            if(isSuccess(res)){
                parent.reloadList();
                closeThisWin('操作成功');
            }
        });
    });
});