layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer, laytpl = layui.laytpl, form = layui.form;
    var id = getUrlParam('id'); show_loading();
    $.post(rurl('scm/deliveryWaybill/detail'),{'id':id,'operation':''},function (res) {
        if(isSuccessWarnClose(res)){
            var data = res.data;
            var total = {'quantity':0.0,'cartonNum':0,'grossWeight':0.0};
            $.each(data.members || [],function (i,o) {
                total.quantity = Math.round((total.quantity + o.quantity)*100)/100;
                total.cartonNum = Math.round((total.cartonNum + o.cartonNum)*100)/100;
                total.grossWeight = Math.round((total.grossWeight + o.grossWeight)*100)/100;
            });
            data['total'] = total;
            app.obtainSubject(function (subject) {
                data['subject'] = subject;
                laytpl($('#conTemp').html()).render(data,function (html) {
                    $('#conTempView').html(html);
                });
            });

        }
    });
});