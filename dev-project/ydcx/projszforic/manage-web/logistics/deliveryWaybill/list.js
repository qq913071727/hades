layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/deliveryWaybill/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'task', width:70, title: '任务',align:'center',toolbar:'#taskTemp', fixed: true}
                ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                ,{field:'docNo', width:126, title: '系统单号',templet:'#docNoTemp', fixed: true}
                ,{field:'impOrderNo', width:126, title: '订单号',templet:'#orderNoTemp'}
                ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                ,{field:'transDate', width:90, title: '运输日期'}
                ,{field:'receivingModeName', width:100, title: '送货方式'}
                ,{field:'departureDate', width:110, title: '发车时间'}
                ,{field:'reachDate', width:110, title: '签收时间'}
                ,{field:'logisticsCompanyName', width:140, title: '物流公司'}
                ,{field:'experssNo', width:110, title: '物流单号'}
                ,{field:'deliWaybillReceipt', width:100, title: '附件管理',templet:'#deliWaybillReceiptTemp',align:'center'}
                ,{field:'deliveryCompanyName', width:200, title: '收货公司'}
                ,{field:'deliveryAddress', width:320, title: '收货地址'}
            ]]
        },listQueryDone:function(){
            initFileSize();
        }
    });
});
function showDetail(id) {
    openwin(rurl('logistics/deliveryWaybill/detail.html?id='+id),'送货单详情',{area: ['0', '0'],shadeClose:true});
}
function removeDeliveryWaybill (o) {
    let data = getCheckDatas();
    if(data.length==1){
        let item = data[0];
        delete_inquiry('当前单据状态<span class="f16b '+item.statusId+'">'+item.statusName+'</span>，是否确定要删除？',o.url,{'id':item.id},function (res) {
            show_success('删除成功');
            reloadList();
        });
    }else{
        show_error('请选择一条您要删除的送货单信息');
    }
}