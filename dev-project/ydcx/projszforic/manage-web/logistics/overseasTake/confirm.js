layui.config({}).extend({}).use(['layer','form'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,form=layui.form;
    var id = getUrlParam("id");
    $("#id").val(id);
    $("#takeTime").val(getNowFormatTime());
        fn = {
        save:function () {
            show_loading();
            var postData = $('#operationForm').serialize();
            $.post('/scm/impOrderLogistics/takeConfirm',postData,function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        }
    };
    $('#confirmDomesticTake').click(function () {
        fn.save();
    });
});
