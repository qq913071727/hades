layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/impOrderLogistics/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'overseasTakeFlag', width:80, title: '状态',align:'center',templet:'#overseasTakeFlagTemp', fixed: true}
                ,{field:'orderNo', width:126, title: '订单号',templet:'#orderNoTemp', fixed: true}
                ,{field:'customerOrderNo', width:126, title: '客户单号'}
                ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                ,{field:'orderDate', width:100, title: '订单日期'}
                ,{field:'overseasTakeTime', width:110, title: '提货时间'}
                ,{field:'takeLinkCompany', width:180, title: '提货公司'}
                ,{field:'takeLinkPerson', width:120, title: '提货联系人'}
                ,{field:'takeLinkTel', width:150, title: '提货联系电话'}
                ,{field:'takeAddress', title: '提货地址'}
            ]]
        },listQueryDone:function(){
            initFileSize();
        }
    });
});

function takeConfirm() {
    
}