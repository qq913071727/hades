layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    //运输日期默认当前日期
    $("#transDate").val(getNowFormatDate());
    show_loading();
    $('.sel-load').selLoad(function () {
        hide_loading();
        form.render('select');
        //根据运输供应商加载运输工具
        form.on("select(transportSupplierId)",function (data) {
            var selectedTransportSupplierId = data.value;
            if(!isEmpty(selectedTransportSupplierId)) {
                $("#conveyanceId").attr("sel-url","scm/conveyance/select?transportSupplierId=" + selectedTransportSupplierId)
                    .selLoad(function () {
                        //重新渲染下拉
                        form.render('select');
                    });
            }
        });
        //根据选择送货目地显示收货信息
        form.on("select(transportDestination)",function (data) {
            showDelivery($(data.elem));
        });
        //加载默认值
        form.val('form',JSON.parse(getLocalStorage('def_crossBorderWaybillExp_add') || "{}"));
    });
    $('.save-btn').click(function () {
        if(checkedForm($('.tsf-page-content'))){
            show_loading();
            $.post("/scm/crossBorderWaybill/add",$('.layui-form').serialize(), function (res) {
                if(isSuccess(res)) {
                    setDefaultArg();
                    parent.reloadList();
                    closeThisWin('保存成功');
                }
            });
        }
    });
});
function showDelivery(e) {
    $.each(e,function (i,o) {
        var $that = $(o);
        $('[tsf-mark="' + $that.attr('id') + '"]').hide();
        $('[tsf-mark="'+$that.attr('id')+'"][scm-key="'+$that.val()+'"]').show();
    })
}
function setDefaultArg() {
    var data = $('.layui-form').serializeJson();
    setLocalStorage('def_crossBorderWaybillExp_add',JSON.stringify({
        'customsCode':data['customsCode']
        ,'customsName':data['customsName']
        ,'shippingWareId':data['shippingWareId']
        ,'deliveryWareId':data['deliveryWareId']
    }));
}