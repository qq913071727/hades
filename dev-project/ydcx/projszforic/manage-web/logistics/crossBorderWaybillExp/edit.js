layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    var id = getUrlParam("id");
    show_loading();
    $('.sel-load').selLoad(function () {
        form.render('select');
        $.get(rurl('scm/crossBorderWaybillExp/detail'),{'id':id,'operation':'exp_waybill_edit'},function (res) {
            if (isSuccess(res)) {
                var data = res.data;
                form.val("form",data);
                //初始化
                showDelivery($("#transportDestination"));
                var selectedTransportSupplierId = data.transportSupplierId;
                $("#conveyanceId").attr("sel-url","scm/conveyance/select?transportSupplierId=" + selectedTransportSupplierId)
                    .selLoad(function () {
                        //重新渲染下拉
                        form.render('select');
                        //待下拉框加载完成后需要重新赋值运输工具
                        form.val("form",{'conveyanceId':data.conveyanceId});
                    });
            }
        });
        //根据运输供应商加载运输工具
        form.on("select(transportSupplierId)",function (data) {
            var selectedTransportSupplierId = data.value;
            if(!isEmpty(selectedTransportSupplierId)) {
                $("#conveyanceId").attr("sel-url","scm/conveyance/select?transportSupplierId=" + selectedTransportSupplierId)
                    .selLoad(function () {
                        //重新渲染下拉
                        form.render('select');
                    });
            }
        });
        //根据选择送货目地显示收货信息
        form.on("select(transportDestination)",function (data) {
            showDelivery($(data.elem));
        });
    });

    $('.save-btn').click(function () {
        if(checkedForm($('.tsf-page-content'))){
            show_loading();
            $.post("/scm/crossBorderWaybillExp/edit",$('.layui-form').serialize(), function (res) {
                if(isSuccess(res)) {
                    parent.reloadList();
                    closeThisWin('保存成功');
                }
            });
        }
    });
});

function showDelivery(e) {
    $.each(e,function (i,o) {
        var $that = $(o);
        $('[tsf-mark="' + $that.attr('id') + '"]').hide();
        $('[tsf-mark="'+$that.attr('id')+'"][scm-key="'+$that.val()+'"]').show();
    })
}
