layui.config({base: '/logistics/crossBorderWaybillExp/'}).extend({detailTemp: 'detail-main-temp'}).use(['layer','detailTemp','table'], function() {
    var layer = layui.layer, detailTemp = layui.detailTemp, table = layui.table;
    var id = getUrlParam("id");
    $('#waybillDetail').load(rurl('logistics/crossBorderWaybillExp/detail-main-temp.html'),function () {
        detailTemp.render(id, 'exp_waybill_review');

        var $operationTaskBtn = $('.operation-task-btn'),
            taskArg = {'operation':'exp_waybill_review','documentId':id,'documentClass':'CrossBorderWaybill','newStatusCode':''};
        $operationTaskBtn.click(function () {
            var $that = $(this),needMemo = $that.attr('need-memo');
            if(!isEmpty(needMemo)){
                var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
                taskArg['memo'] = $memo.val();
            }else{
                taskArg['memo'] = $('#taskMemo').val();
            }
            taskArg['newStatusCode'] = $that.attr('status-code');

            show_loading();
            $.post(rurl('scm/crossBorderWaybillExp/review'),taskArg,function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
    });
    //渲染已绑订单
    table.render({
        elem: '#binded-list',page:false,limit:1000,id:'binded-list',url:rurl('scm/crossBorderWaybillExp/binded?id='+id)
        ,response:{msgName:'message',statusCode: '1'},text:{none:'还未绑定任何订单数据'},totalRow: true
        ,cols: [[
            {type: 'numbers',width: 45,title: '序号',align: 'center'}
            ,{field: 'customerName',width: 260,title: '客户名称'}
            ,{field: 'docNo',width: 120,title: '订单号'}
            ,{field: 'orderDate',width: 90,title: '订单日期'}
            ,{field: 'statusDesc',width: 80,title: '订单状态',align: 'center',templet:'#orderStatusTemp'}
            ,{field: 'contrNo',width: 120,title: '合同号'}
            ,{field: 'declareStatusDesc',width: 80,title: '报关单状态',align: 'center',templet:'#decStatusTemp'}
            ,{field: 'iePortName',width: 120,title: '进境关别'}
            ,{field: 'packNo',width: 65,title: '总件数',align: 'right', totalRow: true}
            ,{field: 'deliveryDestinationDesc',width: 90,title: '香港交货方式'}
            ,{field: 'deliveryCompanyName',width: 220,title: '收货公司'}
            ,{field: 'deliveryAddress',title: '收货地址'}
        ]]
    });
    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
});