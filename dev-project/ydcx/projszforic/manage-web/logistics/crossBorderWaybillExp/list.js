layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/crossBorderWaybillExp/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'task', width:70, title: '任务',align:'center',toolbar:'#taskTemp', fixed: true}
                ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                ,{field:'docNo', width:126, title: '系统单号',templet:'#docNoTemp', fixed: true}
                ,{field:'transDate', width:85, title: '运输日期'}
                ,{field:'conveyanceNo', width:140, title: '车牌',templet:'#conveyanceTemp'}
                ,{field:'transportDestinationDesc', width:100, title: '送货目地'}
                ,{field:'departureDate', width:120, title: '发车时间'}
                ,{field:'reachDate', width:120, title: '签收时间'}
                ,{field:'waybillNo', width:120, title: '运输单号'}
                ,{field:'sealNo', width:100, title: '封条号'}
                ,{field:'orderNos', width:150, title: '订单号'}
                ,{field:'waybillReceipt', width:100, title: '附件管理',templet:'#waybillReceiptTemp',align:'center'}
                ,{field:'deliveryCompany', width:200, title: '收货公司'}
                ,{field:'deliveryAddress', title: '收货地址'}
            ]]
        },listQueryDone:function(){
            initFileSize();
        }
    });
});
function editWaybill(){$('button[operation-code="exp_waybill_edit"]').click();}
function showDetail(id) {
    openwin(rurl('logistics/crossBorderWaybillExp/detail.html?id='+id),'运单详情',{area: ['0', '0'],shadeClose:true});
}