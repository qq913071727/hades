function autoDelivery() {
    $('.auto-delivery').autocomplete({
        source: function (request, response) {
            $.get(rurl('scm/crossBorderWaybill/getRecentDeliveryInfo'),{name: trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {
                    label: item.deliveryCompanyName  +'   '+item.deliveryAddress, value: item.deliveryCompanyName, data: item
                }}));
            });
        }, minLength: 0,autoFocus:true, delay: 500,
        select: function( event, ui){
            try{
                var data = ui.item.data;
                $("#deliveryCompany").val(data.deliveryCompanyName);
                $("#deliveryLinkPerson").val(data.deliveryLinkPerson);
                $("#deliveryLinkTel").val(data.deliveryLinkTel);
                $("#deliveryAddress").val(data.deliveryAddress);
            }catch (e) {

            }
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
}
function autoDeliveryExp() {
    $('.auto-delivery-exp').autocomplete({
        source: function (request, response) {
            $.get(rurl('scm/crossBorderWaybillExp/getRecentDeliveryInfo'),{name: trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {
                    label: item.deliveryCompanyName  +'   '+item.deliveryAddress, value: item.deliveryCompanyName, data: item
                }}));
            });
        }, minLength: 0,autoFocus:true, delay: 500,
        select: function( event, ui){
            try{
                var data = ui.item.data;
                $("#deliveryCompany").val(data.deliveryCompanyName);
                $("#deliveryLinkPerson").val(data.deliveryLinkPerson);
                $("#deliveryLinkTel").val(data.deliveryLinkTel);
                $("#deliveryAddress").val(data.deliveryAddress);
            }catch (e) {

            }
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
}
$(function () {
    autoDelivery();
    autoDeliveryExp();
})