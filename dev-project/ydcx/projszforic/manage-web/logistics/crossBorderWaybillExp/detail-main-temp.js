layui.define(['layer','laytpl'], function(exports){
    var layer = layui.layer,laytpl=layui.laytpl,
        detailTemp = {
            render:function (id,operation) {
                $.get(rurl('scm/crossBorderWaybill/detail'),{'id':id,'operation':operation},function (res) {
                    if(isSuccessWarnClose(res)){
                        detailTemp.renderData(res.data);
                    }
                });
            },
            renderData(data){
                laytpl($('#mainViewTemp').html()).render(data,function (html) {
                    $('#mainView').html(html);
                });
            }
        };
    exports('detailTemp', detailTemp);
});