layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form,code=getUrlParam('code'),serviceRate=parseFloat(getUrlParam('serviceRate'));
    $('#serviceRate').html(serviceRate+"%");
    let $date = $('#date');
    $date.attr({"autocomplete":"off","readonly":true}).click(function(){WdatePicker({
        disabledDays:[0,6],
        onpicked:function(dp){
            show_loading();
            $.get('/fund/netWorthMember/findByCodeAndDate',{code:code,date:$date.val()},function (res) {
               if(isSuccess(res)){
                   let data = res.data;
                   if(isEmpty(data)){
                       show_error('未找到有效净值');
                       $date.val('');
                   }else{
                       $('#unitNet').val(data.unit);
                   }
               }
            });
        }});
    });
    $('#accountValue').blur(function () {
        let commission = Math.round((parseFloat($(this).val()) * serviceRate / 100)*100)/100;
        if(serviceRate>0&&commission<0.01){
            commission = 0.01;
        }
        $('#commission').val(commission);
    });
    form.on('submit(save-btn)', function(data){
        $('#code').val(code);
        formSub('/fund/transactionRecord/buy',function (res) {
            parent.refList();
            closeThisWin();
        });
        return false;
    });
});