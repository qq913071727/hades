layui.define(['layer','table'], function(exports) {
    var layer = layui.layer,table=layui.table,fn = {
        init:function(){
            table.render({
                elem:'#table-list',id:'table-list',limit:10000,page:false,height:'494',
                url:'/fund/main/list',response:{msgName:'message',statusCode: '1'}
                ,cols: [[
                    {checkbox: true, fixed: true},
                    {type:'numbers',title: '序号'},
                    {field:'id',title: '代码',width:'60',align:'center'},
                    {field:'name',title: '名称',width:'160',templet:'#tpl_name'},
                    {field:'estimateRise',title: '估算涨幅',width:'85',align:'center',sort:true,templet:'#tpl_estimate_rise'},
                    {field:'dailyGrowth',title: '日增长率',width:'85',align:'center',sort:true,templet:'#tpl_daily_growth'},
                    {field:'upAndDown',title: '最近连续涨跌幅',width:'120',align:'center',sort:true,templet:'#tpl_up_and_down'}
                ]]
            });
        },
        query:function () {
            table.reload('table-list',{page:{curr:1}});
        }
    };
    
    $('#code').autocomplete({
        source: function (request, response) {
            $.get('/fund/main/fundSearch',{keyword: trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: item.NAME+'('+item.CODE+')', value: item.NAME, data: item}}));
            });
        }, minLength: 1,autoFocus:true, delay: 400,
        select: function( event, ui ) {
            var data = ui.item.data;
            show_loading();
            $.get('/fund/main/sysByCode',{code:data.CODE},function (res) {
                if(isSuccess(res)){
                    show_success('【'+data.NAME+'】添加成功');
                    fn.query();
                }
            });
        }
    });
    fn.init();
    
    $('#btn_buy').click(function () {
        let data = getDatas();
        if(data.length == 1){
            openwin('/fund/buy.html?code='+data[0].id+'&serviceRate='+data[0].serviceRate,'交易：'+data[0].name,{area:['600','260']});
        }else{
            show_error('请选择一条记录');
        }
    });
    exports('index', fn);
});
function getDatas(elem){return layui.table.checkStatus((elem || 'table-list')).data;}
function refList() {
    layui.index.query();
}