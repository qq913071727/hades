layui.use(['layer','form'], function(){
    var layer = layui.layer,form = layui.form;
    form.verify({
        passWord: function (value, item){if(isEmpty(value)){return '请输入原始密码';}},
        newPassWord: function (value, item){if(isEmpty(value)||value.length<6){return '新密码不能为空且必须大于6位';}},
        confirmPwd: function (value, item){if(isEmpty(value)){return '请再次输入新密码';}else{if(value!=$('#newPassWord').val()){return '两次密码输入不一致';}}}
    });
    form.on('submit(save-btn)', function(data){
        show_loading();
        $.post(rurl('scm/auth/modifyPassWord'),$('.layui-form').serialize(),function (res) {
            if(isSuccess(res)){
                prompt_success('修改成功，请登录',function () {
                    parent.toUrl('login.html');
                });
            }
        });
        return false;
    });
});