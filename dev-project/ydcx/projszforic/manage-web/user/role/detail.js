layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    initArea();$(window).bind('resize',function(){initArea();});
    var setting = {view: {}, check: {enable: false}, data: {simpleData: {enable: true,pIdKey:'parentId'}}, edit: {enable: false}};
    var $menuTree = $("#menuTree");
    var id = getUrlParam("id");
    var fn = {
        roleDetail:function () {
            show_loading();
            $.get(rurl('scm/role/detail'),{'id':id},function (res) {
                if(isSuccess(res)){
                    var data = res.data;
                    data['switchDisabled'] = !data.disabled;
                    form.val('form',data);
                    var menus = data.menus;
                    if(menus&&menus.length>0){
                        $.fn.zTree.init($menuTree, setting, menus).expandAll(true);
                        $('#openAll').show().click(function (){var treeObj = $.fn.zTree.getZTreeObj("menuTree");treeObj.expandAll(true);});
                        $('#closeAll').show().click(function (){var treeObj = $.fn.zTree.getZTreeObj("menuTree");treeObj.expandAll(false);});
                    }else{
                        $menuTree.html('<div class="text-center">无任何操作权限</div>')
                    }
                    disableForm(form);
                }
            });
        }
    }
    fn.roleDetail();
});

function initArea() {
    $('.role-area').css({'height':($(window).height()-119)+'px'});
}