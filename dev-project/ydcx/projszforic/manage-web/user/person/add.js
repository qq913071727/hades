layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    form.on('switch(switchIncumbency)', function(data){
        $('#incumbency').val(this.checked);
    });
    form.on('submit(save-btn)', function(data){
        formSub('scm/person/add',function (res) {
            parent.reloadList();
            parent.warmReminder('add-person-success','添加成功','员工添加成功，请记得给员工授权角色哦');
            closeThisWin();
        });
        return false;
    });
    $('#departmentName').treeSelect({
        valueId:'departmentId',
        url:'scm/department/list'
    });
});