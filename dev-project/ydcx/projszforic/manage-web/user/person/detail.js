layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    var id = getUrlParam('id');
    show_loading();
    $.get(rurl('scm/person/detail'),{'id':id},function (res) {
        if(isSuccess(res)){
            var data = res.data;
            data['switchIncumbency'] = data.incumbency;
            form.val('form',data);
            disableForm(form);
        }
    });
});