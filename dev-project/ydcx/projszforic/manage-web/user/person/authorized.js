layui.use(['layer','transfer'], function() {
    var layer = layui.layer, transfer = layui.transfer;
    var personId = getUrlParam('id');
    var fn = {
        roleDatas:{},
        initTransfer:function () {
            //获取所有角色
            show_loading();
            $.post(rurl('scm/role/allList'),{'disabled':false},function (res) {
                if(res.code == "1"){
                    fn.roleDatas = res.data;
                    transfer.render({
                        elem: '#personRole',id: 'person-role',showSearch:true,title: ['所有角色', '已授权角色'],data: {},height: 400
                        ,parseData: function(res){
                            return {
                                "value": res.id
                                ,"title": res.name
                                ,"disabled": res.disabled
                            }
                        },onchange: function(data, index){
                            fn.bindRole();
                        }
                    });
                    fn.userRoles();
                }else{
                    isSuccess(res);
                }
            });
        },
        userRoles:function () {
            show_loading();
            $.post(rurl('scm/role/userRoles'),{'personId':personId},function (res) {
                if(isSuccess(res)){
                    var userRoleIds = [];
                    $.each(res.data,function (i,o) {
                        userRoleIds.push(o.id);
                    });
                    transfer.reload('person-role', {
                        data: fn.roleDatas
                        ,value:userRoleIds
                    });
                }
            });
        },bindRole:function () {
            var getData = transfer.getData('person-role');
            var roleIds = [];
            $.each(getData,function (i,o) {
                roleIds.push(o.value);
            });
            show_loading();
            $.post(rurl('scm/role/bindRole'),{'personId':personId,'roleIds':roleIds.join(',')},function (res) {
                fn.userRoles();
                if(isSuccess(res)){
                    show_success('授权成功');
                    parent.reloadList();
                }
            });
        }
    }
    fn.initTransfer();
});