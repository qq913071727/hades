layui.use(['layer','form'], function(){
    var layer = layui.layer,form = layui.form;
    var id = getUrlParam('id');
    form.verify({
        newPassWord: function (value, item){if(isEmpty(value)||value.length<6){return '新密码不能为空且必须大于6位';}},
        confirmPwd: function (value, item){if(isEmpty(value)){return '请再次输入新密码';}else{if(value!=$('#newPassWord').val()){return '两次密码输入不一致';}}}
    });
    form.on('submit(save-btn)', function(data){
        show_loading();
        $.post(rurl('scm/auth/resetPassWord'),{
            "newPassWord":$("#newPassWord").val(),"personId":id
        },function (res) {
            if(isSuccess(res)){
                parent.reloadList();
                closeThisWin('修改成功，员工修改密码后可以登录本系统');
            }
        });
        return false;
    });
});