layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    var id = getUrlParam('id');
    show_loading();
    $('.sel-load').selLoad(function () {
        form.render('select');
        $('#departmentName').treeSelect({
            valueId:'departmentId',
            url:'scm/department/list'
        });
        $.get(rurl('scm/person/detail'),{'id':id},function (res) {
            if(isSuccess(res)){
                var data = res.data;
                data['input_departmentId'] = data.departmentName;
                data['switchIncumbency'] = data.incumbency;
                form.val('form',data);
            }
        });
    });
    form.on('submit(save-btn)', function(data){
        formSub('scm/person/edit',function (res) {
            parent.reloadList();
            closeThisWin('修改成功');
        });
        return false;
    });
    form.on('switch(switchIncumbency)', function(data){
        $('#incumbency').val(this.checked);
    });
});