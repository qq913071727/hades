layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('scm/person/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'code', width:120, title: '工号',templet:'#codeTemp'}
                ,{field:'name', width:100, title: '名称'}
                ,{field:'genderName', width:70, title: '性别',align:'center',templet:'#genderTemp'}
                ,{field:'departmentName', width:120, title: '部门'}
                ,{field:'roleNames', width:220, title: '所属角色'}
                ,{field:'positionName', width:70, title: '职位'}
                ,{field:'incumbency', width:100,align:'form-switch',title: '在职状态',templet:'#incumbencyTemp'}
                ,{field:'isDelete', width:100,title: '是否删除',templet:'#deleteTemp'}
                ,{field:'mail', width:160, title: '邮箱'}
                ,{field:'phone', width:100, title: '手机号码'}
                ,{field:'dateCreated', width:130,align:'center',title: '创建时间'}
                ,{field:'customerName', title: '客户名称',width:180}
                ,{field:'memo', title: '备注'}
            ]]
        }
    });

    $("#customerName").hide();

    form.on('switch(switchIncumbency)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/person/updateIncumbency'),{'id':data.value,'incumbency':$that.checked},function (res) {
            if(isSuccess(res)){
                if(!$that.checked){
                    layer.tips('离职后此员工将无法登录系统','#in'+data.value, {tips: [3, '#333'], time: 3000});
                }
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });
    form.on('select(channel)',function (data) {
        if(data.value == 'client') {
            $("#customerName").show();
        } else {
            $("#customerName").hide();
        }
    });
});
function showDetail(id) {
    openwin(rurl('user/person/detail.html?id='+id),'员工详情',{area: ['1000px', '420px'],shadeClose:true});
}

function importSuccess(msg){prompt_success(msg,function (){show_loading();setTimeout(function () {hide_loading();reloadList();},1000)});}

/**
 * 删除员工
 * @param o
 */
function deletePerson(o) {
    var ids = getIds();
    if(ids.length != 1){show_msg('请单选一条记录');return;}
    confirm_inquiry('三思而后行，删除该员工后将无法恢复，且该员工无法登录系统，无法被选择使用，您确定要删除该员工吗？',function () {
        $.post(rurl('/scm/person/delete'),{'id':ids[0]},function (res) {
            if(isSuccessWarn(res)){
                reloadList();
            }
        });
    });
    return false;
}