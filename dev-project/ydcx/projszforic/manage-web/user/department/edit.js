layui.use(['form','layer'], function () {
    var layer = layui.layer, form = layui.form;
    var id = getUrlParam("id");
    show_loading()
    $.get(rurl('scm/department/detail'),{'id':id},function (res) {
       if(isSuccess(res)){
            form.val('form',res.data);
       }
    });
    form.on('submit(save-btn)', function(data){
        formSub('scm/department/edit',function (res) {
            parent.obtainDepart();
            closeThisWin('修改成功');
        });
        return false;
    });
});