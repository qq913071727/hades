var setting = {view:{autoCancelSelected: false, selectedMulti: false, showLine: false, txtSelectedEnable: true, addHoverDom: addHoverDom, removeHoverDom: removeHoverDom},callback: {onClick: zTreeOnClick},check:{enable: false},data:{simpleData:{enable: true,pIdKey:'parentId'}},edit:{enable: false}};
layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation'], function(){
    var layer = layui.layer,operation = layui.operation,table = layui.table;
    initArea();$(window).bind('resize',function(){initArea();});
    operation.init({
        listQuery:false,
        elem:'.hide-btn-group',
        done:function (ops) {
            obtainDepart();
        }
    });
});

function obtainDepart() {
    var $departTree = $("#departTree");
    $departTree.html('<img src="/images/load.gif"/>');
    $.get(rurl('scm/department/list'),{},function (res) {
        if(isSuccess(res)){
            var data = res.data,nodes=[];
            app.obtainSubject(function (sub) {
                nodes.push({id:'0', parentId:'-1', name:sub.name, open:true});
                $.each(data,function (i,o) {
                    o['parentId']=isEmpty(o.parentId)?'0':o.parentId;
                    o['open']=true;
                    nodes.push(o);
                });
                $.fn.zTree.init($departTree, setting, nodes).expandAll(true);

                obtainPerson('');
            });
        }
    });
}
function zTreeOnClick(event, treeId, treeNode) {
    var departmentId = (treeNode.id!='0')?treeNode.id:'';
    obtainPerson(departmentId);
}
function obtainPerson(departmentId) {
    layui.table.render({
        elem: '#table-list',where:{'departmentId':departmentId,'isDelete':0},response:{msgName:'message',statusCode: '1'}
        ,url:rurl('scm/person/list'),height: 'full-78',method:'post'
        , page: true, limits: limits,limit:limit,id:'tab-list'
        ,cols: [[
            {type:'numbers',width:50, fixed: true}
            ,{field:'code', width:140, title: '工号'}
            ,{field:'name', width:220, title: '名称'}
            ,{field:'genderName', width:70, title: '性别',align:'center',templet:'#genderTemp'}
            ,{field:'departmentName', width:220, title: '部门'}
            ,{field:'positionName', width:70, title: '职位'}
            ,{field:'empty', title: ''}
        ]]
    });
}
function addHoverDom(treeId, treeNode) {
    if($("#diySpan_"+treeNode.id).length==0){
        var aObj = $("#" + treeNode.tId + '_a');
        var btnHtml = [];
        btnHtml.push('<span id="diySpan_'+treeNode.id+'">');
        btnHtml.push('<a href="javascript:" onclick="addDepart(\''+treeNode.id+'\',\''+escape(treeNode.name)+'\')" class="green-a" title="新增"><i class="fa fa-plus-circle"></i></a>');
        if(treeNode.id!='0'){
            btnHtml.push('<a href="javascript:" onclick="editDepart(\''+treeNode.id+'\')" class="blue-a" title="修改"><i class="fa fa-edit"></i></a>');
            btnHtml.push('<a href="javascript:" onclick="delDepart(\''+treeNode.id+'\')" class="red-a" title="删除"><i class="fa fa-trash-o"></i></a>');
        }
        btnHtml.push('</span>');
        aObj.append(btnHtml.join(''));
    }
}
function removeHoverDom(treeId, treeNode) {
    var $diySpan = $("#diySpan_"+treeNode.id);
    $diySpan.find("a").unbind().remove();
    $diySpan.unbind().remove();
}
function initArea() {
    $('#departmentArea').css({'height':($(window).height()-78)+'px'});
}
function addDepart(parentId,parentName) {
    openwin('user/department/add.html?parentId='+parentId+'&parentName='+parentName,'添加部门',{area: ['500px', '240px']});
}
function editDepart(parentId) {
    openwin('user/department/edit.html?id='+parentId,'修改部门',{area: ['500px', '200px']});
}
function delDepart(parentId) {
    delete_inquiry('您确定要删除此部门信息？','scm/department/delete',{'id':parentId},function (res) {
       show_success("删除成功");obtainDepart();
    });
}