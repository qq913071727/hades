layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','fileupload'], function() {
    var layer = layui.layer,fileupload = layui.fileupload;
    var docId=getUrlParam('docId'),showFile=getUrlParam('showFile'),edit=getUrlParam('edit')=='true';
    var fileObj = {docId:docId, docType:_filecf[showFile].docType};
    $.each(_filecf[showFile].businessTypes,function (i,o) {
        var areas = [];
        areas.push('<fieldset class="layui-elem-field">');
        areas.push('<legend>'+_filebt[o]+'</legend>');
        areas.push('<div class="layui-field-box">');
        areas.push('<div class="upload-area" id="'+o+'"></div>');
        areas.push('</div></fieldset>');
        $('#file-area').append(areas.join(''));
        fileupload.render({elem:'#'+o,startQuery:true,edit:edit,uploadArg:{data:$.extend({},fileObj,{businessType:o})}});
    });
});
