var _filebt = {
    'agr_evidence':'佐证资料',
    'agr_original':'协议原件',
    'imp_order_information':'境外提货委托书',
    'exp_order_information':'国内提货委托书',
    'imp_order_self':'境内自提委托书',
    'imp_order_pi':'装箱单/发票',
    'imp_order_other':'其它资料',
    'imp_order_entrust':'委托单盖章件',
    'receiving_note_enc':'附件资料',
    'accessories':'附件资料',
    'waybill_receipt':'签收单',
    'delivery_waybill_receipt':'签收单',
    'pay_memo':'付汇水单',
    'payment_memo':'付款水单',
    'pay_entrust':'付汇委托书',
    'refund_receipt':'退款水单',
    'service_greement_original':'服务协议正本',
    'exp_order_other':'附件资料',
    'decl_original':'报关单原件',
    'decl_release':'放行通知书',
    'other_file':'其它资料',
    'collect_invoice':'收票资料',
    'contract_seal':'合同盖章件',
}
var _filecf = {
    //协议原件
    agrOriginal:{'docType':'agreement','businessTypes':['agr_original']},
    //提货委托书(境外提货)
    impOrderInformation:{'docType':'imp_order','businessTypes':['imp_order_information']},
    //提货委托书(国内提货)
    expOrderInformation:{'docType':'exp_order','businessTypes':['exp_order_information']},
    //提货盖章委托书(国内自提)
    impOrderSelf:{'docType':'imp_order','businessTypes':['imp_order_self']},
    //装箱单/发票(PACKING-LIST/INVOICE)
    impOrderPI:{'docType':'imp_order','businessTypes':['imp_order_pi']},
    //进口订单其它资料
    impOrderOther:{'docType':'imp_order','businessTypes':['imp_order_other']},
    //出口订单其它资料
    expOrderOther:{'docType':'exp_order','businessTypes':['exp_order_other']},
    //进口订单所有资料
    impOrderAll:{'docType':'imp_order','businessTypes':['imp_order_information','imp_order_self','imp_order_pi','imp_order_other','imp_order_entrust']},
    //入库单列表附件
    receivingNoteList:{'docType':'receiving_note','businessTypes':['receiving_note_enc']},
    //跨境运输单签收
    waybillReceipt:{'docType':'cross_border_waybill','businessTypes':['waybill_receipt']},
    //进口委托单盖章件
    orderEntrust:{'docType':'imp_order','businessTypes':['imp_order_entrust']},
    //付汇附件管理
    impPayFile:{'docType':'imp_payment','businessTypes':['pay_memo','pay_entrust']},
    //付汇水单
    payMemo:{'docType':'imp_payment','businessTypes':['pay_memo']},
    //境外付款水单
    paymentMemo:{'docType':'overseas_receiving_account','businessTypes':['payment_memo']},
    //国内送货单签收单
    deliWaybillReceipt:{'docType':'delivery_waybill','businessTypes':['delivery_waybill_receipt']},
    //退款水单
    refundReceipt:{'docType':'refund_account','businessTypes':['refund_receipt']},
    //进口服务协议原件
    impAgreementOriginal:{'docType':'agreement','businessTypes':['agr_original','agr_evidence']},
    // 仓库管理入库单列表
    wmsReceivingNoteList:{'docType':'wms_receiving_note','businessTypes':['accessories']},
    // 仓库管理出库单列表
    wmsDeliveryNoteList:{'docType':'wms_delivery_note','businessTypes':['accessories']},
    // 报关单文件
    declarationAll:{'docType':'scm_declaration','businessTypes':['decl_original','decl_release']},
    //出口订单所有资料
    expOrderAll:{'docType':'exp_order','businessTypes':['exp_order_information','exp_order_other']},
    //出口服务协议原件
    expAgreementOriginal:{'docType':'exp_agreement','businessTypes':['agr_original','agr_evidence']},
    //采购合同
    purchaseFile:{'docType':'purchase','businessTypes':['collect_invoice','contract_seal','other_file']},
    //出口付款
    expPaymentAccount:{'docType':'exp_payment_account','businessTypes':['payment_memo']},
    //出口退税
    drawback:{'docType':'drawback','businessTypes':['payment_memo']},
};