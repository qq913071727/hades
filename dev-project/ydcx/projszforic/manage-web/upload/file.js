var filewin=null;
function initFileSize(){
    var $fileSize = $('.file-size');
    $fileSize.unbind().click(function (){
        layer.close(filewin);
        var $that = $(this);
        if(!isEmpty($that.attr('doc-id'))){
            filewin = layer.open({area: ['760px', '330px'],shade :0,anim:1,content: rurl(['upload/upload-list.html?docId=',$that.attr('doc-id'),'&showFile=',$that.attr('show-file'),'&edit=',$that.attr('edit')].join('')),title:"附件资料",resize:true,type: 2,zIndex: layer.zIndex,cancel:function () {obtainFileSize();}});
        }
    });
    obtainFileSize();
}
function obtainFileSize() {
    var $fileSize = $('.file-size'),docIds = [],docType = '';
    $.each($fileSize,function (i,o) {
        var $that = $(o);docId=$that.attr('doc-id');
        if(!contain(docIds,docId)){
            docIds.push(docId);
            if(isEmpty(docType)){
                var filecfObj = _filecf[$that.attr('show-file')];
                docType = filecfObj.docType;
            }
        }
    });
    if(docIds.length==0){return;}
    $fileSize.html('<img src="'+rurl('images/indicator.gif')+'"/>');
    $.post(rurl('scm/file/fileSizeGroup'),{'docId':docIds.join(","),'docType':docType},function (res) {
        $fileSize.html('<span class="layui-badge">查看附件(<qty>0</qty>)</span>');
        $.each((res.data||[]),function (i,o) {
            var $item = $('a[doc-id="'+o.docId+'"]');
            $.each($item,function (j,k) {
                var $self = $(k);
                if(contain(_filecf[$self.attr('show-file')].businessTypes,o.businessType)){
                    var $qty = $self.find('qty');
                    $qty.html(parseInt($qty.text())+o.quantity);
                }
            });
        })
    });
}