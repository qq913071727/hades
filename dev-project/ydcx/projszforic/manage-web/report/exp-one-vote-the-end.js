layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('scm/expReport/oneVoteTheEnd')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'idx', width:45, title: '序号',align:'center', fixed: true}
                    ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                    ,{field:'orderNo', width:126, title: '订单编号', fixed: true}
                    ,{field:'clientOrderNo', width:120, title: '客户单号'}
                    ,{field:'orderDate', width:80, title: '订单日期'}
                    ,{field:'customerName', width:260, title: '客户名称'}
                    ,{field:'name', width:100, title: '品名'}
                    ,{field:'brand', width:100, title: '品牌'}
                    ,{field:'model', width:120, title: '型号'}
                    ,{field:'hsCode', width:90, title: '海关编码',align:'center'}
                    ,{field:'trr', width:50, title: '退税率',align:'right'}
                    ,{field:'unitPrice', width:65, title: '单价',align:'right'}
                    ,{field:'quantity', width:75, title: '数量',align:'right'}
                    ,{field:'totalPrice', width:75, title: '出口金额',align:'right'}
                    ,{field:'currencyName', width:60, title: '订单币制',align:'center'}
                    ,{field:'quoteDescribe', width:290, title: '代理协议'}
                    ,{field:'agencyFee', width:60, title: '代理费率',align:'right'}
                    ,{field:'minAgencyFee', width:75, title: '最低收费',align:'right'}
                    ,{field:'agencyFeeCost', width:80, title: '应收代理费',align:'right'}
                    ,{field:'agentName', width:220, title: '申报单位'}
                    ,{field:'decDate', width:80, title: '通关日期'}
                    ,{field:'declarationNo', width:120, title: '报关单号'}
                    ,{field:'ciqEntyPortName', width:80, title: '通关口岸'}
                    ,{field:'cartonNum', width:60, title: '箱数',align:'right'}
                    ,{field:'plateNum', width:60, title: '板数',align:'right'}
                    ,{field:'cusVoyageNo', width:100, title: '国际运单号'}
                    ,{field:'cusTrafModeName', width:80, title: '运输方式'}
                    ,{field:'conveyanceType', width:60, title: '订单车型',align:'center'}
                    ,{field:'conveyanceNo', width:80, title: '大陆车牌'}
                    ,{field:'conveyanceNo2', width:70, title: '香港车牌'}
                    ,{field:'transportSupplierName', width:120, title: '承运公司'}
                    ,{field:'shippingCompany', width:200, title: '发货单位'}
                    ,{field:'shippingAddress', width:260, title: '发货地址'}
                    ,{field:'shippingLinkPerson', width:85, title: '发货联系人'}
                    ,{field:'shippingLinkTel', width:100, title: '发货联系电话'}
                    ,{field:'deliveryCompany', width:200, title: '收货单位'}
                    ,{field:'deliveryAddress', width:260, title: '收货地址'}
                    ,{field:'deliveryLinkPerson', width:85, title: '收货联系人'}
                    ,{field:'deliveryLinkTel', width:100, title: '收货联系电话'}

                    ,{field:'receivingNo', width:100, title: '境外收款单号',align:'center'}
                    ,{field:'receivingDate', width:80, title: '收款日期'}
                    ,{field:'amountReceived', width:85, title: '到账金额',align:'right'}
                    ,{field:'receivingCurrency', width:60, title: '收款币制',align:'center'}
                    ,{field:'receivingFee', width:75, title: '手续费',align:'right'}
                    ,{field:'orderClaimValue', width:85, title: '本单认领金额',align:'right'}

                    ,{field:'paymentNo', width:100, title: '付款单号',align:'center'}
                    ,{field:'paymentDate', width:110, title: '付款时间'}
                    ,{field:'settlementRate', width:70, title: '结汇汇率',align:'right'}
                    ,{field:'paymentValue', width:130, title: '已付结汇金额CNY',align:'right'}

                    ,{field:'noSettlementValue', width:80, title: '未结汇金额',align:'right'}
                    ,{field:'currencyName', width:80, title: '未结汇币制',align:'center'}

                    ,{field:'settlementNo', width:100, title: '结汇单号',align:'center'}

                    ,{field:'purchaseValue', width:90, title: '采购合同金额',align:'right'}
                    ,{field:'actualPurchaseValue', width:90, title: '应开发票金额',align:'right'}
                    ,{field:'differenceVal', width:80, title: '产生票差',align:'right'}
                    ,{field:'adjustmentVal', width:80, title: '调整票差',align:'right'}
                    ,{field:'invoicingDate', width:80, title: '开票日期'}
                    ,{field:'invoiceDate', width:80, title: '收票日期'}
                    ,{field:'invoiceNo', width:100, title: '发票号码'}
                    ,{field:'invoiceVal', width:80, title: '收票金额',align:'right'}
                    ,{field:'vatInvVal', width:80, title: '增票金额',align:'right'}
                    ,{field:'taxInvVal', width:80, title: '增票税额',align:'right'}

                    ,{field:'drawbackNo', width:120, title: '退税付款单号'}
                    ,{field:'actualDrawbackValue', width:80, title: '应付退税款',align:'right'}
                    ,{field:'alreadyDrawbackValue', width:80, title: '已付退税款',align:'right'}
                    ,{field:'unpaidDrawbackValue', width:80, title: '未付退税款',align:'right'}

                    ,{field:'taxDeclareTime', width:90, title: '退税申报日期',align:'center'}
                    ,{field:'taxDeclareAmount', width:90, title: '退税申报金额',align:'right'}
                    ,{field:'taxRefundTime', width:90, title: '税局退税日期',align:'center'}
                    ,{field:'taxRefundAmount', width:90, title: '税局退税金额',align:'right'}
                    ,{field:'isCorrespondence', width:80, title: '是否函调',align:'center',templet:'#isCorrespondenceTemp'}
                    ,{field:'replyStatus', width:80, title: '是否回函',align:'center'}
                    ,{field:'replyDate', width:80, title: '回函日期',align:'center'}

                ]]
            },listQueryDone:function () {
                formatTableData();
            }
        });
    });
});

function exportExcel (o) {
    show_loading();
    $.post(rurl(o.url),$('.query-form').serialize(),function (res)
    {if(isSuccessWarn(res)){
        exportFile(res.data);
    }});
}