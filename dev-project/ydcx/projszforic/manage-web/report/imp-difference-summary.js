layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation'], function(){
    var layer = layui.layer,operation = layui.operation;
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/impReport/impDifferenceMemberSummary')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'customerName',width:260, title: '客户名称',templet:'#customerNameTemp'}
                ,{field:'differenceVal',width:100, title: '总票差金额',align:'money'}

            ]]
        },listQueryDone:function (da) {
            formatTableData();
        }
    });
});
function fullCustomerData(d) {
    $('#customerId').val(d.id);
    $('#customerName').val(d.name);
}
function exportExcel (o) {
    show_loading();
    $.post(rurl(o.url),$('.query-form').serialize(),function (res)
    {if(isSuccessWarn(res)){
        exportFile(res.data);
    }});
}