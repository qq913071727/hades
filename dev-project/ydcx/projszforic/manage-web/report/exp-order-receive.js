layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation'], function(){
    var layer = layui.layer,operation = layui.operation;
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/expReport/orderReceivable')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'orderNo', fixed:true,width:120, title: '订单号',align:'center',fixed:true,templet: '#orderNoTemp'}
                ,{field:'customerOrderNo', fixed:true,width:120, title: '客户合同号',align:'center',fixed:true}
                ,{field:'orderDate', width:90, title: '订单日期',align:'center', fixed:true}
                ,{field:'customerName',width:260, title: '客户名称',templet:'#customerNameTemp'}
                ,{field:'salePersonName', width:70, title: '销售人员'}
                ,{field:'totalPrice',width:80, title: '出口金额',align:'money'}
                ,{field:'currencyName', width:50, title: '币制',align:'center'}
                ,{field:'receAmount',width:80, title: '应收金额',align:'money'}
                ,{field:'acceAmount',width:80, title: '已收金额',align:'money'}

            ]]
        },listQueryDone:function (da) {
            formatTableData();
            $('#total-area').remove();
            if(da.count > 0){
                var $ares = $('.choice-btn');
                $.post('/scm/expReport/orderReceivableTotal',$('.query-form').serialize(),function (res) {
                    if(isSuccess(res)){
                        var data = res.data;
                        $ares.before('<div id="total-area">总单数：<span>'+data.totalSingle+'</span>总应收：<span>'+formatCurrency(data.totalReceAmount)+'</span>总已收：<span>'+formatCurrency(data.totalAcceAmount)+'</span>总订单金额：<span>'+formatCurrency(data.totalPrice)+'</span></div>');
                    }
                });
            }
        }
    });
});
function fullCustomerData(d) {
    $('#customerId').val(d.id);
    $('#customerName').val(d.name);
}
function exportExcel (o) {
    show_loading();
    $.post(rurl(o.url),$('.query-form').serialize(),function (res)
    {if(isSuccessWarn(res)){
        exportFile(res.data);
    }});
}