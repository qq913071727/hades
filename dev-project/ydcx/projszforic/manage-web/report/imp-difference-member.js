layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation'], function(){
    var layer = layui.layer,operation = layui.operation;
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/impReport/impDifferenceMember')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'docNo', fixed:true,width:120, title: '订单编号',templet:'#docNoTemp'}
                ,{field:'orderDate', width:90, title: '订单日期',align:'center', fixed:true}
                ,{field:'customerName',width:260, title: '客户名称',templet:'#customerNameTemp'}
                ,{field:'salesDocNo', width:100, title: '采购合同号'}
                ,{field:'accountValueCny',width:100, title: '付汇金额(CNY)',align:'money'}
                ,{field:'goodsVal', width:120, title: '销售合同货款金额',align:'money'}
                ,{field:'balance',width:100, title: '票差金额',align:'money'}
                ,{field:'differenceVal',width:100, title: '票差调整金额',align:'money'}
                ,{field:'totalPrice',width:100, title: '销售合同总价',align:'money'}
            ]]
        },listQueryDone:function (da) {
            formatTableData();
        }
    });
});
function fullCustomerData(d) {
    $('#customerId').val(d.id);
    $('#customerName').val(d.name);
}
function exportExcel (o) {
    show_loading();
    $.post(rurl(o.url),$('.query-form').serialize(),function (res)
    {if(isSuccessWarn(res)){
        exportFile(res.data);
    }});
}