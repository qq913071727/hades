layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation'], function(){
    var layer = layui.layer,operation = layui.operation;
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/impReport/orderReceivable')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'docNo', fixed:true,width:100, title: '订单号'}
                ,{field:'customerOrderNo', fixed:true,width:100, title: '客户合同号'}
                ,{field:'orderDate', width:90, title: '订单日期',align:'center', fixed:true}
                ,{field:'customerName',width:260, title: '客户名称',templet:'#customerNameTemp'}
                ,{field:'salePersonName', width:70, title: '销售人员'}
                ,{field:'totalPrice',width:80, title: '委托金额',align:'money'}
                ,{field:'currencyName', width:50, title: '币制',align:'center'}
                ,{field:'quoteTypeName', width:80, title: '报价类型',align:'center'}
                ,{field:'totalCost',width:80, title: '应收金额',align:'money'}
                ,{field:'writeValue',width:80, title: '已收金额',align:'money'}
                ,{field:'arrearsValue',width:80, title: '欠款金额',align:'money'}
                ,{field:'periodDate', width:90, title: '账期日期',align:'center'}
                ,{field:'overdueDaysStr', width:60, title: '逾期天数',align:'center'}
                ,{field:'huokuan',width:80, title: '货款',align:'money'}
                ,{field:'shouxufei',width:80, title: '手续费',align:'money'}
                ,{field:'dailifei',width:80, title: '代理费',align:'money'}
                ,{field:'zenzhishui',width:80, title: '增值税',align:'money'}
                ,{field:'guanshui',width:80, title: '关税',align:'money'}
                ,{field:'xiaofeishui',width:80, title: '消费税',align:'money'}
                ,{field:'mian3c',width:80, title: '免3C认证费',align:'money'}
                ,{field:'guoleisonhuofei',width:80, title: '国内送货费',align:'money'}
                ,{field:'shangjianfei',width:80, title: '商检服务费',align:'money'}

            ]]
        },listQueryDone:function (da) {
            formatTableData();
            $('#total-area').remove();
            if(da.count > 0){
                var $ares = $('.choice-btn');
                $.post('/scm/impReport/orderReceivableTotal',$('.query-form').serialize(),function (res) {
                    if(isSuccess(res)){
                        var data = res.data;
                        $ares.before('<div id="total-area">总单数：<span>'+data.totalSingle+'</span>总应收：<span>'+formatCurrency(data.totalReceAmount)+'</span>总已收：<span>'+formatCurrency(data.totalAcceAmount)+'</span>总欠款：<span>'+formatCurrency(data.totalArrears)+'</span>总逾期：<span>'+formatCurrency(data.totalOverdue)+'</span>总订单金额：<span>'+formatCurrency(data.totalPrice)+'</span></div>');
                    }
                });
            }
        }
    });
});
function fullCustomerData(d) {
    $('#customerId').val(d.id);
    $('#customerName').val(d.name);
}
function exportExcel (o) {
    show_loading();
    $.post(rurl(o.url),$('.query-form').serialize(),function (res)
    {if(isSuccessWarn(res)){
        exportFile(res.data);
    }});
}