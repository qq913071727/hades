layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation'], function(){
    var layer = layui.layer,operation = layui.operation;
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/expReport/expDifferenceMember')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'docNo', fixed:true,width:120, title: '订单编号',templet:'#docNoTemp'}
                ,{field:'orderDate', width:90, title: '订单日期',align:'center', fixed:true}
                ,{field:'customerName',width:260, title: '客户名称',templet:'#customerNameTemp'}
                ,{field:'purchaseContractNo', width:100, title: '采购合同号'}
                ,{field:'purchaseCny',width:100, title: '采购合同总价',align:'money'}
                ,{field:'actualPurchaseCny', width:100, title: '实际采购金额',align:'money'}
                ,{field:'adjustmentVal',width:100, title: '票差调整金额',align:'money'}
                ,{field:'differenceVal',width:100, title: '票差金额',align:'money'}
            ]]
        },listQueryDone:function (da) {
            formatTableData();
        }
    });
});
function fullCustomerData(d) {
    $('#customerId').val(d.id);
    $('#customerName').val(d.name);
}
function exportExcel (o) {
    show_loading();
    $.post(rurl(o.url),$('.query-form').serialize(),function (res)
    {if(isSuccessWarn(res)){
        exportFile(res.data);
    }});
}