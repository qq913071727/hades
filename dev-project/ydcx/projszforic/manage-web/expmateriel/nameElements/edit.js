layui.use(['layer','form','table','element'], function() {
    var layer = layui.layer, form = layui.form, table = layui.table,element = layui.element;
    var id = getUrlParam('id');
    $.get(rurl('scm/nameElements/detail'),{'id':id},function (res) {
        if(isSuccess(res)){
            var nameElements = res.data.nameElements,elementValues = res.data.elementValues;customsElements = res.data.customsElements;
            form.val('form',nameElements);
            //品名要素
            var elements = [];
            $.each(customsElements, function (i, o) {
                var eValue = "", readonly = "";
                elements.push('<tr><td style="text-align: center">' + (i + 1) + '</td><td>&nbsp;&nbsp;' + o.name + '</td><td>');
                if (o.name == '品牌类型') {
                    elements.push(brandType(elementValues[i]));
                } else if (o.name == '出口享惠情况') {
                    elements.push(expBenefit(elementValues[i]));
                } else {
                    elements.push('<input name="elementsVal" class="layui-input ' + (isEmpty(readonly) ? "" : "layui-disabled") + '" value="' + elementValues[i] + '" ' + readonly + '/>');
                }
                elements.push('</td></tr>');
            });
            $('#elements').html(elements.join(''));
            form.render();
        }
    });
    $('#hsCode').autocomplete({
        source: function (request, response) {
            $.post(rurl('system/customsCode/list'),{'id': trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: item.id+'('+item.name+')', value: item.id, data: item}}));

                //如果没有查询到海关编码数据，则清空申报要素
                if(res.data == null || res.data.length < 1) {
                    $('#elements').html([].join(''));
                    form.val('form',{'hsCodeName':''});
                    layui.form.render('select');
                }

            });
        }, minLength: 0, delay: 300,
        select: function( event, ui ) {
            var data = ui.item.data;
            form.val('form',{'hsCode':data.id,'hsCodeName':data.name});
            obtainElements();
        }
    }).focus(function (){
        $(this).autocomplete("search");
    });

    form.on('submit(save-btn)', function(data){
        formSub('scm/nameElements/edit',function (res) {
            parent.reloadList();
            closeThisWin('修改成功');
        });
        return false;
    });

});

function obtainElements() {
    show_loading();
    var elements = [];
    $.get(rurl('system/customsElements/getByHsCode'),{'hsCode':$('#hsCode').val()},function (res) {
        if(isSuccess(res)) {
            $.each(res.data, function (i, o) {
                var eValue = "", readonly = "";
                elements.push('<tr><td style="text-align: center">' + (i + 1) + '</td><td>&nbsp;&nbsp;' + o.name + '</td><td>');
                if (o.name == '品牌类型') {
                    elements.push(brandType(''));
                } else if (o.name == '出口享惠情况') {
                    elements.push(expBenefit(''));
                } else {
                    elements.push('<input name="elementsVal" class="layui-input ' + (isEmpty(readonly) ? "" : "layui-disabled") + '" value="' + eValue + '" ' + readonly + '/>');
                }
                elements.push('</td></tr>');
            });
        }
        $('#elements').html(elements.join(''));
        layui.form.render('select');
    });
}

function brandType(defVal) {
    return ['<select name="elementsVal" id="brandType" lay-search="">','<option value="">品牌类型</option>','<option value="0" ',((defVal=='0')?'selected':''),'>0：无品牌</option>','<option value="1" ',((defVal=='1')?'selected':''),'>1：境内自主品牌</option>','<option value="2" ',((defVal=='2')?'selected':''),'>2：境内收购品牌</option>','<option value="3" ',((defVal=='3')?'selected':''),'>3：境外品牌(贴牌生产)</option>','<option value="4" ',((defVal=='4')?'selected':''),'>4：境外品牌(其他)</option>','</select>'].join('');
}
function expBenefit(defVal) {
    return ['<select name="elementsVal" id="expBenefit" lay-search="">','<option value="">出口享惠情况</option>','<option value="0" ',((defVal=='0')?'selected':''),'>0：出口货物在最终目的国(地区)不享受优惠关税</option>','<option value="1" ',((defVal=='1')?'selected':''),'>1：出口货物在最终目的国(地区)享受优惠关税</option>','<option value="2" ',((defVal=='2')?'selected':''),'>2：出口货物不能确定在最终目的国(地区)享受优惠关税</option>','<option value="3" ',((defVal=='3')?'selected':''),'>3：不适用于进口报关单</option>','</select>'].join('');
}

