layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    //类型下拉框
    var typeObj = $('#type');
    typeObj.append("<option value=''>全部</option>");
    $.get(rurl('scm/common/bizSelect'),{type:'MaterielBrandType'},function (da){
        $.each(da.data['MaterielBrandType'],function (i,o) {
            typeObj.append('<option value="'+o.value+'">' + o.label+'</option>');
        });
        form.render('select');
    });
    form.on('submit(save-btn)', function(data){
        formSub('scm/materielBrand/add',function (res) {
            parent.reloadList();
            closeThisWin('添加成功');
        });
        return false;
    });
});
