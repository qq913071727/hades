layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    var id = getUrlParam('id');
    show_loading();
    //类型下拉框
    var typeObj = $('#type');
    typeObj.append("<option value=''>全部</option>");
    $.get(rurl('scm/common/bizSelect'),{type:'MaterielBrandType'},function (da){
        $.each(da.data['MaterielBrandType'],function (i,o) {
            typeObj.append('<option value="'+o.value+'">' + o.label+'</option>');
        });
        form.render('select');
        $.get(rurl('scm/materielBrand/detail'),{'id':id},function (res) {
            if(isSuccess(res)){
                var data = res.data;
                form.val('form',data);
            }
        });
    });
    form.on('submit(save-btn)', function(data){
        formSub('scm/materielBrand/edit',function (res) {
            parent.reloadList();
            closeThisWin('修改成功');
        });
        return false;
    });
});
