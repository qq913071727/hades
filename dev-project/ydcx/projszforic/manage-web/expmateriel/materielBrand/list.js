layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form','table'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form,table = layui.table;

    //类型下拉框
    var typeObj = $('#type');
    typeObj.append("<option value=''>全部</option>");
    $.get(rurl('scm/common/bizSelect'),{type:'MaterielBrandType'},function (da){
        $.each(da.data['MaterielBrandType'],function (i,o) {
            typeObj.append('<option value="'+o.value+'">' + o.label+'</option>');
        });
        form.render('select');
    });
    operation.init({
        listQuery:true,
        winArg:{shadeClose:true},
        listQueryArg:{
            url:rurl('scm/materielBrand/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'name', width:300, title: '英文品牌'}
                ,{field:'memo', width:350, title: '中文品牌'}
                ,{field:'typeDesc',width:200, title: '品牌类型'}
            ]]
        },listQueryDone:function(){

        }
    });


});
