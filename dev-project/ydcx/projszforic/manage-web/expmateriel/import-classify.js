layui.use(['layer','upload'], function() {
    var layer = layui.layer, upload = layui.upload;
    upload.render({
        elem: '#importExcel',url:rurl('scm/materielExp/importClassify'),size:10240,exts:'xls|xlsx'
        ,acceptMime:'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ,before:function (res) {
            show_loading();
        } ,done: function(res){
            if(isSuccessWarn(res)){
                var data = res.data;
                closeThisWin();
                parent.importSuccess(['导入条数：',data.successCount,' ',data.message].join(''));
            }
        },error:function (index, upload) {
            hide_loading();
        }
    });
});