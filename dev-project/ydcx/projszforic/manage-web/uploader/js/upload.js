$(function () {
    $('.uploadFile').fileupload({
        dataType: 'json',
        url:rurl('upload/index'),
        formData:{'domainId':'','domainType':'','memo':''},
        add: function(e, data) {
            try{if(data.originalFiles[0]['size'] > 5120000){show_error('单个文件不能大于5M');return;}}catch (e) {}
            data.paramName = "file";
            data.submit();
            show_loading();
        },
        progressall: function (e, data) {},
        done: function (e, data){
            var $this = $(this),res = data.result;
            if(isSuccess(res)){
                obtainFiles($this.parent().parent())
            }
        }
    }).bind('fileuploadsubmit', function (e, data){
        var $that = $(this);
        data.formData = {'domainId':$that.attr('domainId'),'domainType':$that.attr('domainType'),'memo':$that.attr('memo')}
    });
});

function obtainFiles($that) {
    var $fileList = $that.find('.file-list');
    $fileList.html('<img src="'+rurl('images/indicator.gif')+'"/>');
    $.get(rurl('upload/fileList'),{'domainId':$that.attr('domainId'),'domainType':$that.attr('domainType'),'memo':$that.attr('memo')},function (res) {
        var _files = [];
        $.each(res.data,function (i,o) {
            _files.push('<div class="file-list-item"><a href="'+rurl('download/index?id='+o.id)+'" target="_blank">'+(i+1)+'：'+o.name+'</a><a href="javascript:" fid="'+o.id+'" onclick="removeFile(this)" class="del-file">X</a></div>');
        });
        $fileList.html(_files.join(''));
    });
}
function removeFile(e) {
    var $that = $(e);
    confirm_inquiry('您确定要删除此文件？',function () {
       show_loading();
       $.get(rurl('upload/removeFile'),{"id":$that.attr('fid')},function (res) {
           if(isSuccess(res)){
               show_msg("删除成功");
               obtainFiles($that.parent().parent().parent())
           }
       })
    });
}