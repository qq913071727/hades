var edit = false;
layui.use(['table','form','upload', 'layer'], function () {
    var table = layui.table,upload = layui.upload,layer = layui.layer,form = layui.form;
    show_loading();
    edit = getUrlParam('edit') == "true"
    var docId = getUrlParam('docId');
    var docType = getUrlParam('docType');
    var memo = getUrlParam('memo');
    var fileArg = {'docId':docId,'docType':docType};
    form.val('form',fileArg);
    var rowArg = [
        {field:'ind',type:'numbers',width:45,align:'center', title: '序号'}
        ,{field:'name', title: '文件名称',templet:'#name'}
        ,{field:'dateCreated', width:116,align:'center', title: '上传时间'}
        ,{field:'uname', width:80, title: '上传人'}
    ]
    if(edit==true){
        $('.file-range').show();
        rowArg.push({field:'opertaion',width:45,align:'center', title: '操作',templet:'#opertaion'});
        upload.render({
            elem: '#uploadFile',size:5120,multiple:true,accept: 'file',data:{'docType':docType, 'docId':docId, 'memo':memo},url: rurl('file/upload/file')
            ,before:function (res) {
                show_loading();
            },done: function(res){
                if(isSuccess(res)){refTable();parent.refOutFileSize();}
            },error:function (res) {
                hide_loading();
            }
        });
    }
    table.render({
        elem: '#table-list',where:$('.layui-form').serializeJson()
        ,url:rurl('file/upload/fileList'),height: ('full-'+(edit?30:0))
        , page: false,id:'tab-list'
        ,cols: [rowArg],done: function (data) {hide_loading();}
    });
});
function refTable(){layui.table.reload('tab-list',{page:{curr:1},height: ('full-'+(edit?30:0)),where:$('.layui-form').serializeJson(),done:function (data) {hide_loading();}});}
function removeFile(id) {
    confirm_inquiry('您确定要删除此文件？',function () {
        show_loading();
        $.post(rurl('file/upload/deleteFile/'+id),{},function (res) {
            if(isSuccess(res)){
                show_msg("删除成功");refTable();parent.refOutFileSize();
            }
        });
    });
}
