layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form,
        fn = {
            initData:function(){
                $('#name').focus();
                var id = getUrlParam('id'),copy=isNotEmpty(getUrlParam('copy'));
                if(isNotEmpty(id)){
                    show_loading();
                    $.get('/declare/vehicleHead/detail',{id:id},function (res) {
                        if(isSuccessWarnClose(res)){
                            var data = res.data;
                            if(copy){
                                data.id = '';
                            }
                            form.val('pageForm',data);
                            feeMarkSelect();
                            insurMarkSelect();
                        }
                    });
                }
            },
            saveVehicle:function () {
                if(checkedForm($('#pageForm'))){
                    postJSON('/declare/vehicleHead/save',$('#pageForm').serializeJson(),function (res) {
                        parent.reloadList();
                        closeThisWin('保存成功');
                    });
                }
            }
        };
    fn.initData();
    $('#memo').enter(function(){fn.saveVehicle()});
    $('#saveVehicle').click(function(){fn.saveVehicle()});
});
