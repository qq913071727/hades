layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form,
        fn = {
            initData:function(){
                var id = getUrlParam('id')
                show_loading();
                $.get('/declare/vehicleHead/detail',{id:id},function (res) {
                    if(isSuccessWarnClose(res)){
                        var data = res.data;
                        form.val('pageForm',data);
                        $('#pageForm').find('input').attr("disabled", "disabled").addClass("disabled");
                    }
                });
            }
        };
    fn.initData();
});
