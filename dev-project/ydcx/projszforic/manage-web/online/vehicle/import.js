layui.use(['layer','form','upload'], function () {
    var layer = layui.layer, form = layui.form, upload = layui.upload,
        fn = {
            //初始化文件上传
            initUpload:function (){
                upload.render({
                    elem: '#importVehicle'
                    ,url: '/declare/vehicleDeclare/vehicleImport',size:10240
                    ,exts:'xls|xlsx',acceptMime:'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                    ,data: {
                        customerId: function(){
                            return $('#customerId').val();
                        },
                        vehicleHeadId: function () {
                            return $('#vehicleHeadId').val();
                        },
                        billNo: function () {
                            return $('#billNo').val();
                        },
                        carInfoId: function () {
                            return $('#carInfoId').val();
                        }
                    }
                    ,before:function () {
                        show_loading();
                    }
                    ,done: function(res){
                        if(isSuccessWarn(res)){
                            //跳转到详情页面
                            closeThisWin();
                            parent.openwin('online/declare/page.html?id='+res.data,'整车申报',{area:['0','0']});
                        }
                    },error:function () {
                        show_msg('不支持的文件，请按照模板正确填写');
                        hide_loading();
                    }
                })
            }
        };

    $('#vehicleHeadName').autocomplete({
        source: function (request, response) {
            $.post(rurl('declare/vehicleHead/pageList'),{name: trim(request.term),disabled:false},function (res) {
                response($.map(res.data, function (item) {return {label: item.name, value: item.name, data: item}}));
            });
        }, minLength: 0,autoFocus:true, delay: 300,
        select: function( event, ui ) {
            try{var data = ui.item.data;fullVehicleHeadData(data);}catch (e) {}
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    }).blur(function () {
        if(isEmpty($(this).val())){
            try{fullVehicleHeadData({id:'',name:''});}catch (e) {}
        }
    });
    $('#trafName').autocomplete({
        source: function (request, response) {
            $.post(rurl('declare/carInfo/list'),{innerCarNum: trim(request.term),disabled:false},function (res) {
                response($.map(res.data, function (item) {return {label: item.innerCarNum, value: item.innerCarNum, data: item}}));
            });
        }, minLength: 0,autoFocus:true, delay: 300,
        select: function( event, ui ) {
            try{var data = ui.item.data;fullTrafNameData(data);}catch (e) {}
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    }).blur(function () {
        if(isEmpty($(this).val())){
            try{fullTrafNameData({id:'',innerCarNum:''});}catch (e) {}
        }
    });

    fn.initUpload();

});

function fullCustomerData(data) {
    $('#customerId').val(data.id);
}
function fullVehicleHeadData(data) {
    $('#vehicleHeadId').val(data.id);
}
function fullTrafNameData(data) {
    $('#carInfoId').val(data.id);
}