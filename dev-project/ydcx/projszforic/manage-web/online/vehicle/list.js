layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('declare/vehicleHead/pageList')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'name', width:240, title: '表头名',templet:'#nameTemp', fixed: true}
                    ,{field:'logisticsName', width:260, title: '物流企业名称',templet:'#logisticsNameTemp'}
                    ,{field:'ebcName', width:260, title: '电商企业名称',templet:'#ebcNameTemp'}
                    ,{field:'disabled', width:100,align:'form-switch',title: '状态',templet:'#disabledTemp'}
                    ,{field:'tradeModeName', width:140, title: '贸易方式'}
                    ,{field:'customMasterName', width:100, title: '申报地海关'}
                    ,{field:'iePortName', width:100, title: '出口口岸'}
                    ,{field:'distinatePortName', width:120, title: '指运港'}
                    ,{field:'cusTrafModeName', width:100, title: '运输方式'}
                    ,{field:'statisticsFlagName', width:100, title: '申报业务类型'}
                    ,{field:'currencyName', width:100, title: '商品币制'}

                ]]
            },listQueryDone:function () {
                formatTableData();
            }
        });
    });
    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('declare/vehicleHead/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });
});

function showDetail(id) {
    openwin(rurl('online/vehicle/detail.html?id='+id),'整车头详情',{area: ['900px', '400px'],shadeClose:true});
}