layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('declare/vehicleSummary/pageList')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'customerName', width:180, title: '客户',align:'center', fixed: true}
                    ,{field:'sumNo', width:180, title: '汇总编号',align:'center', fixed: true}
                    ,{field:'copNo', width:180, title: '企业唯一编码',fixed: true}
                    ,{field:'ebcName', width:160, title: '电商企业',templet:'#ebcNameTemp'}
                    ,{field:'agentName', width:160, title: '申报单位',templet:'#agentNameTemp'}
                    ,{field:'declAgentName', width:160, title: '报关单位',templet:'#declAgentNameTemp'}
                    ,{field:'summaryTime', width:250, title: '汇总时间段',templet:'#summaryTimeTemp',align:'center'}
                    ,{field:'msgCount', width:70, title: '报文总数',align:'center'}
                    ,{field:'msgSeqNo', width:80, title: '报文序号',align:'center'}
                    ,{field:'statusDesc', width:100, title: '业务状态',align:'right',templet:'#statusTemp'}
                    ,{field:'customsCode', width:90, title: '申报地海关',align:'center'}
                    ,{field:'dateCreated', width:150, title: '申报时间',align:'center'}
                    ,{field:'opView', title: '查看',align:'center',templet:'#opViewTemp'}
                ]]
            },listQueryDone:function () {
                formatTableData();
            }
        });
    });
});

function goVehicleDeclare(ebcName,dateStart,dateEnd) {
    var key = CryptoJS.enc.Utf8.parse("1234567890123456");
    var encryptedData = CryptoJS.AES.encrypt(ebcName, key, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    });
    encryptedData = encryptedData.ciphertext.toString();
    let ebcNameEncrypt = encryptedData;
    openwin(rurl('online/declare/vehicle_list.html?mid=5430&isFromSummary=1&ebcName=' + ebcNameEncrypt +"&dateStart=" + dateStart + "&dateEnd="+dateEnd),'整车申报',{area:['0','0']});
}