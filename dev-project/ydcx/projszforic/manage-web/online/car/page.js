layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form,
        fn = {
            initData:function(){
                $('#innerCarNum').focus();
                $('.sel-load').selLoad(function () {
                    form.render('select');
                    var id = getUrlParam('id');
                    if(isNotEmpty(id)){
                        show_loading();
                        $.get('/declare/carInfo/detail',{id:id},function (res) {
                            if(isSuccessWarnClose(res)){
                                form.val('pageForm',res.data);
                            }
                        });
                    }
                });
            },
            saveCar:function () {
                if(checkedForm($('#pageForm'))){
                    var params = $('#pageForm').serializeJson();
                    postJSON('/declare/carInfo/'.concat(isEmpty(params.id)?'add':'edit'),params,function (res) {
                        parent.reloadList();
                        closeThisWin('保存成功');
                    });
                }
            }
        };
    fn.initData();
    $('#address').enter(function(){fn.saveCar()});
    $('#saveCar').click(function(){fn.saveCar()});
});
