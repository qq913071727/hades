layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('declare/carInfo/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'innerCarNum', width:120, title: '大陆车牌号',align:'center',fixed: true}
                    ,{field:'hkCarNum', width:120, title: '香港车牌号',fixed: true}
                    ,{field:'customsCode', width:150, title: '车辆海关编号/目的码头'}
                    ,{field:'driverCode', width:150, title: '司机海关编号/最终目的地'}
                    ,{field:'driverName', width:100, title: '司机姓名'}
                    ,{field:'transCode', width:100, title: '承运公司编码'}
                    ,{field:'transCompany', width:150, title: '承运公司名称'}
                    ,{field:'containerTypeName', width:120, title: '集装箱类型',align:'center'}
                    ,{field:'containerCode', width:120, title: '集装箱编号',align:'center'}
                    ,{field:'dateCreated', width:120, title: '创建时间',align:'center'}
                ]]
            },listQueryDone:function () {

            }
        });
    });
});