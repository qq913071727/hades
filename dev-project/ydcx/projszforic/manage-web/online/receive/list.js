layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('declare/receiveInfo/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'linkMan', width:180, title: '联系人'}
                    ,{field:'linkPhone', width:100, title: '联系电话'}
                    ,{field:'linkAddress', width:200, title: '联系地址'}
                    ,{field:'destinationCountry', width:160, title: '海关收货人所在国',templet:'#destinationCountryTemp'}
                    ,{field:'inspectionCountry', width:160,title: '国检收货人所在国',templet:'#inspectionCountryTemp'}
                    ,{field:'dateCreated', width:130,title: '创建时间',align:'center'}

                ]]
            },listQueryDone:function () {
                formatTableData();
            }
        });
    });
});
