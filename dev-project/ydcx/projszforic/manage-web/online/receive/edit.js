layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form,
        fn = {
            init:function () {
               let id = getUrlParam("id");
                if(isNotEmpty(id)){
                    show_loading();
                    $.get('/declare/receiveInfo/detail',{id:id},function (res) {
                        if(isSuccessWarnClose(res)){
                            var data = res.data;
                            form.val('pageForm',data);
                            form.render('select');
                            fn.initData();
                            form.render('select');
                        }
                    });
                }
            },
            initData:function() {
                let destinationCountryCode = $("#destinationCountryCode").val();
                let inspectionCountryCode = $("#inspectionCountryCode").val();
                if(isEmpty(destinationCountryCode)) {
                    $("#destinationCountryCode").val("HKG");
                    $("#destinationCountryName").val("中国香港");
                }
                if(isEmpty(inspectionCountryCode)) {
                    $("#inspectionCountryCode").val("HKG");
                    $("#inspectionCountryName").val("中国香港");
                }
            },
            save:function () {
                if(checkedForm($('#pageForm'))){
                    fn.initData();
                    formSub('/declare/receiveInfo/edit',function (res) {
                        parent.reloadList();
                        closeThisWin('保存成功');
                    });
                }
            }
        };
    fn.init();
    $('#memo').enter(function(){fn.save()});
    $('.save-btn').click(function(){fn.save()});
});