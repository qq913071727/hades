layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form,
        fn = {
            save:function () {
                if(checkedForm($('#pageForm'))){
                    formSub('/declare/nameReplace/add',function (res) {
                        parent.reloadList();
                        closeThisWin('保存成功');
                    });
                }
            }
        };
    $('#memo').enter(function(){fn.save()});
    $('.save-btn').click(function(){fn.save()});
});

function test(){
    var sensitiveName = $("#sensitiveName").val();
    var testKeyword = $("#testKeyword").val();
    if(isEmpty(testKeyword)) {
        $("#testResult").val('');
    }
    if(isNotEmpty(sensitiveName) && isNotEmpty(testKeyword)) {
        //改从后台测试
        $.post(rurl('declare/common/checkReg'),{'regex':sensitiveName,'content':testKeyword},function (res) {
            var match = res.data;
            if(match) {
                $("#testResult").val('匹配成功');
                $("#testResult")[0].style.color="green";
            } else {
                $("#testResult").val('无法匹配');
                $("#testResult")[0].style.color="red";
            }
        });
    }
}