layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form,
        fn = {
            init:function(){
                $('.sel-load').selLoad(function () {
                    form.render('select');
                    let id = getUrlParam("id");
                    if(isNotEmpty(id)){
                        show_loading();
                        $.get('/declare/nameReplace/detail',{id:id},function (res) {
                            if(isSuccessWarnClose(res)){
                                var data = res.data;
                                form.val('pageForm',data);
                                form.render('select');
                            }
                        });
                    }
                });
            },
            save:function () {
                if(checkedForm($('#pageForm'))){
                    formSub('/declare/nameReplace/edit',function (res) {
                        parent.reloadList();
                        closeThisWin('保存成功');
                    });
                }
            }
        };
    fn.init();
    $('#memo').enter(function(){fn.save()});
    $('.save-btn').click(function(){fn.save()});
});

function test(){
    var sensitiveName = $("#sensitiveName").val();
    var testKeyword = $("#testKeyword").val();
    if(isEmpty(testKeyword)) {
        $("#testResult").val('');
    }
    if(isNotEmpty(sensitiveName) && isNotEmpty(testKeyword)) {
        //改从后台测试
        $.post(rurl('declare/common/checkReg'),{'regex':sensitiveName,'content':testKeyword},function (res) {
            var match = res.data;
            if(match) {
                $("#testResult").val('匹配成功');
                $("#testResult")[0].style.color="green";
            } else {
                $("#testResult").val('无法匹配');
                $("#testResult")[0].style.color="red";
            }
        });
    }
}
