layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('declare/nameReplace/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'sensitiveName', width:180, title: '敏感品名',templet:'#sensitiveNameTemp'}
                    ,{field:'validName', width:180, title: '合法品名',templet:'#validNameTemp'}
                    ,{field:'explain', width:320, title: '提示说明',templet:'#explainTemp'}
                    ,{field:'disabled', width:100,align:'form-switch',title: '启用状态',templet:'#disabledTemp'}
                    ,{field:'dateCreated', width:130,title: '创建时间',align:'center'}
                    ,{field:'memo', width:300, title: '备注'}

                ]]
            },listQueryDone:function () {
                formatTableData();
            }
        });
    });
    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('declare/nameReplace/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });
});
