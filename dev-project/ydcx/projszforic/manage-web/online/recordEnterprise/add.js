layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form,
        fn = {
            save:function () {
                if(checkedForm($('#pageForm'))){
                    formSub('/declare/recordEnterprises/add',function (res) {
                        parent.reloadList();
                        closeThisWin('保存成功');
                    });
                }
            }
        };

    form.render("select");
    $('.save-btn').click(function(){fn.save()});
});

function fullCustomerData(data) {
    $('#customerId').val(data.id);
}