layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('declare/recordEnterprises/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'customerName', width:180, title: '客户名称'}
                    ,{field:'name', width:180, title: '企业名称'}
                    ,{field:'code', width:100, title: '企业海关代码'}
                    ,{field:'shortName', width:100, title: '企业简称'}
                    ,{field:'dxpId', width:150, title: 'DXP ID'}
                    ,{field:'disabled', width:100,align:'form-switch',title: '启用状态',templet:'#disabledTemp'}
                    ,{field:'memo', title: '备注'}
                ]]
            },listQueryDone:function () {

            }
        });

        form.on('switch(switchDisabled)', function(data){
            show_loading();
            var $that = this;
            $.post(rurl('declare/recordEnterprises/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
                if(isSuccess(res)){
                    if(!$that.checked == true){
                        layer.tips('禁用后此电商企业将无法通过深关通发送报文','#in'+data.value, {tips: [3, '#333'], time: 3000});
                    }
                    show_success('修改成功');
                }else{
                    $(data.elem).attr({'checked':(!this.checked)});
                    form.render('checkbox');
                }
            });
        });

    });
});
