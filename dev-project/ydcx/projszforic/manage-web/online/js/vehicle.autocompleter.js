var auto1 = {
    "customMasterName": {hid: 'customMaster',type:'CUS_CUSTOMS'},
    "iePortName": {hid: 'iePort',type:'CUS_CUSTOMS'},
    "payWayName": {hid: 'payWay',type:'VEH_PAY_WAY'},
    "orderTypeName": {hid: 'orderType',type:'VEH_ORDER_TYPE'},
    "orderSendUnitName": {hid: 'orderSendUnit',type:'VEH_ORDER_SEND'},
    "invtSendUnitName": {hid: 'invtSendUnit',type:'VEH_INVT_SEND'},
    "statisticsFlagName": {hid: 'statisticsFlag',type:'VEH_DEC_FLAG'},
    "wrapTypeName": {hid: 'wrapType',type:'VEH_PACKAGING',dropUp:true},
    "feeMarkName": {hid: 'feeMark',type:'DEC_FEE_MARK',callback:feeMarkSelect},
    "insurMarkName": {hid: 'insurMark',type:'DEC_INSUR_MARK',callback:insurMarkSelect},
    "gunitName": {hid: 'gunit',type:'CUS_UNIT',dropUp:true,callback:sysQty},
    "unit1Name": {hid: 'unit1',type:'CUS_UNIT',dropUp:true,callback:sysQty},
    "unit2Name": {hid: 'unit2',type:'CUS_UNIT',dropUp:true,callback:sysQty}
},auto2 = {
    "cusTrafModeName": {hid: 'cusTrafMode',type:'CUS_MAPPING_TRANSPORT_CODE_V'},
    "distinatePortName": {hid: 'distinatePort',type:'CUS_MAPPING_PORT_CODE_V'},
    "tradeModeName": {hid: 'tradeMode',type:'CUS_MAPPING_TRADE_CODE_V',dropUp:true},
    "feeCurrName": {hid: 'feeCurr',type:'CUS_MAPPING_CURRENCY_CODE_V',dropUp:true},
    "insurCurrName": {hid: 'insurCurr',type:'CUS_MAPPING_CURRENCY_CODE_V',dropUp:true},
    "currencyName": {hid: 'currencyCode',type:'CUS_MAPPING_CURRENCY_CODE_V',dropUp:true},
    "tradeCurrName": {hid: 'tradeCurr',type:'CUS_MAPPING_CURRENCY_CODE_V',dropUp:true},
    "destinationCountryName": {hid: 'destinationCountryCode',type:'CUS_MAPPING_COUNTRY_CODE_V'},
    "inspectionCountryName": {hid: 'inspectionCountryCode',type:'CUS_MAPPING_COUNTRY_CODE_V'},
};
//修改成交数量同步法一发二数量
function sysQty() {
    if($('#gunit').val()==$('#unit1').val()){$('#qty1').val($('#gqty').val());}
    if($('#gunit').val()==$('#unit2').val() && isNotEmpty($('#unit2').val())){$('#qty2').val($('#gqty').val());}
}
function initAuto1(data){
    $.each(auto1,function (key,val) {
        var $input = $('#'+key);
        if($input.length>0){
            var source = [];
            $.each(data[val.type]||[],function (i,o) {
                source.push({'label':o.c+'-'+o.n,'value':o.n});
            });
            if(source.length>0){
                $input.autocompleter({
                    source:source,
                    style:removeNull(val.style),
                    limit:20,
                    focusOpen:(val.focusOpen),
                    hiddenId:val.hid,
                    dropUp:val.dropUp,
                    callback:val.callback
                });
            }
        }
    });
}
function initAuto2(data){
    $.each(auto2,function (key,val) {
        var $input = $('#'+key);
        if($input.length>0){
            var source = [];
            $.each(data[val.type]||[],function (i,o) {
                if(key!='tradeModeName' || (key=='tradeModeName' && contain(['9610','9710','9810','1210','1239'],o.c))){
                    o['label'] = o.c+'_'+o.n+'_'+o.o+'_'+o.q;
                    source.push(o);
                }
            });
            if(source.length>0){
                $input.autocompleter({
                    source:source,
                    style:removeNull(val.style),
                    limit:20,
                    focusOpen:(val.focusOpen),
                    hiddenId:val.hid,
                    dropUp:val.dropUp,
                    callback:val.callback,
                    template:'<div>{{c}}-{{n}} <span class="fr">{{o}}&nbsp;&nbsp;{{q}}</span></div>'
                });
            }
        }
    });
}
function initAutoHsCode(data) {
    var source = [];
    $.each(data||[],function (i,o) {
        o['label'] = o.c;
        source.push(o);
    });
    if(source.length>0){
        $('#s_hsCode').autocompleter({
            source:source,
            style:'min-width:400px',
            limit:5,
            focusOpen:false,
            minLength:2,
            template:'<div>{{c}} <span class="fr">{{n}}</span></div>'
        });
    }
}
function initBaseData(){
    var declDatas1 = localStorage.getItem("static_decl_datas1");
    if(isEmpty(declDatas1)){
        $.post(rurl('system/customsData/formatData1'),{},function (res) {
            if(isSuccessWarn(res)){
                localStorage.setItem("static_decl_datas1",JSON.stringify(res.data));
                initAuto1(res.data);
            }
        });
    }else{
        initAuto1(JSON.parse(declDatas1));
    }
    var declDatas2 = localStorage.getItem("static_decl_datas2");
    if(isEmpty(declDatas2)){
        $.post(rurl('system/customsData/formatData2'),{},function (res) {
            if(isSuccessWarn(res)){
                localStorage.setItem("static_decl_datas2",JSON.stringify(res.data));
                initAuto2(res.data);
            }
        });
    }else{
        initAuto2(JSON.parse(declDatas2));
    }
    var hsCodeDatas = localStorage.getItem("static_code_datas");
    if(isEmpty(hsCodeDatas)){
        $.post(rurl('system/customsCode/selectAll'),{},function (res) {
            if(isSuccessWarn(res)){
                localStorage.setItem("static_code_datas",JSON.stringify(res.data));
                initAutoHsCode(res.data);
            }
        });
    }else{
        initAutoHsCode(JSON.parse(hsCodeDatas));
    }
}
$(function () {
    $.get(rurl('system/version'),{},function (res) {
        if(isSuccess(res)){
            if(res.data != localStorage.getItem('static_version')){
                localStorage.setItem("static_code_datas","");
                localStorage.setItem("static_decl_datas1","");
                localStorage.setItem("static_decl_datas2","");
                localStorage.setItem("static_prod_traf_data","");
                localStorage.setItem("static_version",res.data);
            }
            initBaseData();
        }
    });
});

//运保杂费标志为率时币制灰掉
function markSelect(mark, code, id) {
    if ($("#" + mark).val() == "1") {
        $("#" + code).val("");
        $("#" + id).val("");
        $("#"+id).attr("disabled", "disabled").addClass("disabled");
    }else {
        $("#" + id).removeAttr("disabled", "disabled").removeClass("disabled");
    }
}
function feeMarkSelect() {
    markSelect('feeMark','feeCurr','feeCurrName');
}
function insurMarkSelect() {
    markSelect('insurMark','insurCurr','insurCurrName');
}
