$('body').on('keydown', 'textarea', function(e) {
    var self = $(this),eCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
    if (eCode == 13) {
        e.preventDefault();
    }
});
$('body').on('keyup','input, select, textarea',function(e){
    var self = $(this), form = self.parents('form:eq(0)'), focusable, next, prev,eCode = e.keyCode ? e.keyCode : e.which ? e.which: e.charCode;
    if (e.shiftKey) {
        if (e.keyCode == 13) {
            focusable = form.find('input,a,select,textarea').filter(':visible').not('.disabled').not(':input[disabled]');
            prev = focusable.eq(focusable.index(this) - 1);
            if (prev.length){if($(this).attr("shiftEnter")=="no"){return false;}else{prev.select();}}
        }
    } else if (e.ctrlKey && eCode == 13 && this.localName == "textarea") {
        var myValue = "\n",$t = $(this)[0];
        if (document.selection){
            this.focus();
            var sel = document.selection.createRange();
            sel.text = myValue;
            this.focus();
            sel.moveStart('character', -l);
            var wee = sel.text.length;
        }else if ($t.selectionStart || $t.selectionStart == '0') {
            var startPos = $t.selectionStart;
            var endPos = $t.selectionEnd;
            var scrollTop = $t.scrollTop;
            $t.value = $t.value.substring(0, startPos) + myValue + $t.value.substring(endPos,$t.value.length);
            this.focus();
            $t.selectionStart = startPos + myValue.length;
            $t.selectionEnd = startPos + myValue.length;
            $t.scrollTop = scrollTop;

        } else {
            this.value += myValue;
            this.focus();
        }
    } else  if (eCode == 13) {
        if (this.localName == "textarea") {
            e.preventDefault();
            e.stopPropagation();
        }
        focusable = form.find('input,select,textarea').filter(':visible').not('.disabled').not(':input[disabled]');
        next = focusable.eq(focusable.index(this) + 1);
        if (next.length) {
            if($(this).attr("enter")=="no"){
                return false;
            }else{
                next.select();
            }
        }
        return false;
    }
});
function searchCss(queryFlag,enterCode,customsCode,name){
    if(isEmpty(enterCode)){return;}
    switch (queryFlag) {
        case '1'://社会信用代码
            if(enterCode.length!=18){setTimeout(function(){verificationTips($(scc),'社会信用代码必须18位',-1);$(scc).val('');},100);return;}
            break;
        case '0'://海关代码
            if(enterCode.length!=9&&enterCode.length!=10){setTimeout(function(){verificationTips($(customsCode),'海关代码格式错误',-1);$(customsCode).val('');},100);return;}
            break;
    }
    $.ajax({url: rurl('declare/cssInfo/search'), data: JSON.stringify({'queryFlag':queryFlag,'enterCode':trim(enterCode)}), type: 'post', dataType: 'json', contentType: 'application/json', success: function (res){
            if(isSuccessNoTip(res)&&res.data!=null){
                $(name).val(res.data.name).removeClass('layui-form-danger');
                $(customsCode).val(res.data.customsCode).removeClass('layui-form-danger');
            }}
    });
}
$('#agentCode').enter(function(){searchCss('0',$(this).val(),'#agentCode','#agentName')});
$('#agentName').enter(function(){searchCss('3',$(this).val(),'#agentCode','#agentName')});
$('#logisticsCode').enter(function(){searchCss('0',$(this).val(),'#logisticsCode','#logisticsName')});
$('#logisticsName').enter(function(){searchCss('3',$(this).val(),'#logisticsCode','#logisticsName')});
$('#ebcCode').enter(function(){searchCss('0',$(this).val(),'#ebcCode','#ebcName')});
$('#ebcName').enter(function(){searchCss('3',$(this).val(),'#ebcCode','#ebcName')});
$('#ebpCode').enter(function(){searchCss('0',$(this).val(),'#ebpCode','#ebpName')});
$('#ebpName').enter(function(){searchCss('3',$(this).val(),'#ebpCode','#ebpName')});
$('#ownerCode').enter(function(){searchCss('0',$(this).val(),'#ownerCode','#ownerName')});
$('#ownerName').enter(function(){searchCss('3',$(this).val(),'#ownerCode','#ownerName')});
$('#payCode').enter(function(){searchCss('0',$(this).val(),'#payCode','#payName')});
$('#payName').enter(function(){searchCss('3',$(this).val(),'#payCode','#payName')});
$('#declagAgentCode').enter(function(){searchCss('0',$(this).val(),'#declagAgentCode','#declAgentName')});