layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('declare/disableProduct/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'name', width:180, title: '品名'}
                    ,{field:'matchTypeDesc', width:100, title: '匹配类型'}
                    ,{field:'dateCreated', width:130,title: '创建时间',align:'center'}

                ]]
            },listQueryDone:function () {

            }
        });
    });
});
