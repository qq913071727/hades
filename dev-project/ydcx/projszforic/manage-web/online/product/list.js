layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('declare/productName/pageList')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'name', width:180, title: '商品名称', fixed: true}
                    ,{field:'hsCode', width:90, title: '海关编码', fixed: true}
                    ,{field:'element', width:280, title: '申报要素'}
                    ,{field:'alias', width:100, title: '别名'}
                    ,{field:'disabled', width:100,align:'form-switch',title: '状态',templet:'#disabledTemp'}
                    ,{field:'unitName', width:70, title: '法一单位',align:'center'}
                    ,{field:'sunitName', width:70, title: '法二单位',align:'center'}
                    ,{field:'unitPrice', width:70, title: '单价',align:'right'}
                    ,{field:'netWeight', width:70, title: '单个净重',align:'right'}
                    ,{field:'quantityNw', width:70, title: '数量/KG',align:'right'}
                    ,{field:'totalPriceNw', width:70, title: '金额/KG',align:'right'}
                    ,{field:'totalPriceGe', width:70, title: '金额/个',align:'right'}
                    ,{field:'exportRate', width:70, title: '出口税率',align:'right'}
                    ,{field:'trr', width:70, title: '退税率',align:'right'}
                    ,{field:'csc', width:70, title: '监管条件',templet:'#cscTemp'}
                    ,{field:'iaqr', width:70, title: '检验检疫',templet:'#iaqrTemp'}
                ]]
            },listQueryDone:function () {
                formatCsc();formatIaqr();
                formatTableData();
            }
        });
    });
    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('declare/productName/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });
});

function showDetail(id) {
    openwin(rurl('online/vehicle/detail.html?id='+id),'整车头详情',{area: ['900px', '400px'],shadeClose:true});
}