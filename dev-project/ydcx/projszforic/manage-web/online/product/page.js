layui.use(['layer','form','laytpl'], function() {
    var layer = layui.layer, form = layui.form,laytpl = layui.laytpl,
        fn = {
            ohsCode:'',
            initData:function(){
                $('#name').focus();
                var id = getUrlParam('id'),copy=isNotEmpty(getUrlParam('copy'));
                if(isNotEmpty(id)){
                    show_loading();
                    $.get('/declare/productName/detail',{id:id},function (res) {
                        if(isSuccessWarnClose(res)){
                            var data = res.data;
                            if(copy){
                                data.id = '';
                            }
                            form.val('pageForm',data);
                            fn.obtainHsCodeDetail('true');
                        }
                    });
                }
            },
            obtainHsCodeDetail:function(mark){
                var $hsCode = $('#s_hsCode'),codeTs=trim($hsCode.val());
                if(fn.ohsCode==codeTs){
                    if(isEmpty(mark)){
                        $('input[name="elementVal"]')[0].focus();
                    }
                    return;
                }
                if(isNotEmpty(codeTs)&&codeTs.length==10){
                    show_loading();
                    $.get(rurl('system/customsCode/codeDetail/'+codeTs),{},function (res) {
                        if (isSuccess(res) && res.data != null) {
                            var data = res.data,hsCode=data.hsCode,elements=data.elements || [];
                            fn.ohsCode=codeTs;
                            form.val('pageForm',{hsCodeName:hsCode.name,unit1Name:hsCode.unitName,unit2Name:hsCode.sunitName});
                            var oldSpecs = ($('#element').val() || '0|0').split('|');
                            $.each(elements,function (i,item) {
                                item.val = removeNull(oldSpecs[i]);
                            });
                            laytpl($('#elementTemp').html()).render(elements,function (html) {
                                $('#elementView').html(html);
                            });
                            var elementVals = $('input[name="elementVal"]');
                            if(isEmpty(mark)){
                                $(elementVals[0]).select();
                            }
                            $(elementVals[elementVals.length-1]).enter(function () {
                                fn.saveForm();
                            });
                        }
                    });
                }else{
                    $hsCode.val('').focus();
                }
            },
            fullElements:function (){
                var specs = $('#element').val().split('|');
                $('input[name="elementVal"]').each(function (i,o){
                    $(o).val(removeNull(specs[i]));
                });
            },
            saveForm:function () {
                if(checkedForm($('#pageForm'))){
                    postJSON('/declare/productName/save',$('#pageForm').serializeJson(),function (res) {
                        parent.reloadList();
                        closeThisWin('保存成功');
                    });
                }
            }
        };
    fn.initData();
    $('#saveForm').click(function(){fn.saveForm()});
    $('#s_hsCode').enter(function(){
        fn.obtainHsCodeDetail();
    });
    $('#element').blur(function (){
        fn.fullElements();
    });
});
//申报要素
function assembleEl(that){
    that.value = that.value.replace(new RegExp("\\|", "gm"), "");
    joinElements();
}
function removeLastSplit(val) {
    if(isNotEmpty(val)){
        while (true){
            val = trim(val);
            if('|'==val.substring(val.length-1)){val = val.substring(0,val.length-1);}else{return val;}
        }
    }
    return '';
}
function joinElements() {
    var elementsJson = $('#pageForm').serializeJson();
    var elestr = removeLastSplit(elementsJson.elementVal.join('|'));
    $('#element').val(elestr);
}