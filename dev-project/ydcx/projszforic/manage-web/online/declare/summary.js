layui.use(['layer','form'], function() {
    var layer = layui.layer,
        fn = {
            initData:function(){
                $("#customMaster").val("5349");
                $("#customMasterName").val("前海港区");
            },
            declare:function () {
                if(checkedForm($('#pageForm'))){
                    confirm_inquiry('请确认您填写的数据是否正确？',function () {
                        postJSON('/declare/vehicleSummary/declare',$('#pageForm').serializeJson(),function (res) {
                            parent.reloadList();
                            closeThisWin('提交成功，请耐心等待汇总结果');
                        });
                    });
                }
            }
        };
    fn.initData();
    $('#declare').click(function(){fn.declare()});
});

function fullCustomerData(data) {
    $('#customerId').val(data.id);
}
