layui.use(['layer','form','table','element','laytpl'], function() {
    var layer = layui.layer, form = layui.form, table = layui.table,element = layui.element,
        laytpl = layui.laytpl,
        id = getUrlParam("id"),statusId=getUrlParam("statusId");
        const opData = {'three_order': '三单','inventory':'清单','waybill':'总分单','departure':'离境单'};
    fn = {
             op:'three_order',//申报类型
             threeDeclare(params){ //三单申报
                 //禁用按钮，防止重复点击
                 $(".declare").attr("disabled",true);
                 $.post("/declare/vehicleDeclare/threeDeclare",params, function (res) {
                     if(isSuccess(res)) {
                         closeThisWin('三单申报提交成功，请耐心等待回执结果，并在单一窗口中确认数据是否正确，相关数据不正确请勿重发');
                     } else {
                         $(".declare").removeAttr("disabled");
                     }
                 });
             },
             inventoryDeclare(params){ //清单申报
                 //禁用按钮，防止重复点击
                 $(".declare").attr("disabled",true);
                 $.post("/declare/vehicleDeclare/inventoryDeclare",params, function (res) {
                     if(isSuccess(res)) {
                         closeThisWin('清单申报提交成功，请耐心等待回执结果，并在单一窗口中确认数据是否正确，相关数据不正确请勿重发');
                     } else {
                         $(".declare").removeAttr("disabled");
                     }
                 });
             },
             waybillDeclare(params) { //总分单
                 //禁用按钮，防止重复点击
                 $(".declare").attr("disabled",true);
                 $.post("/declare/vehicleDeclare/waybillDeclare",params, function (res) {
                     if(isSuccess(res)) {
                         closeThisWin('总分单申报提交成功，请耐心等待回执结果，并在单一窗口中确认数据是否正确，相关数据不正确请勿重发');
                     } else {
                         $(".declare").removeAttr("disabled");
                     }
                 });
             },
             departureDeclare() { //离境单
                 //禁用按钮，防止重复点击
                 $(".declare").attr("disabled",true);
                 $.post("/declare/vehicleDeclare/departureDeclare",{'id':id,'leaveTime':$("#leaveTime").val()}, function (res) {
                     if(isSuccess(res)) {
                         closeThisWin('离境单申报提交成功，请耐心等待回执结果，并在单一窗口中确认数据是否正确，相关数据不正确请勿重发');
                     } else {
                         $(".declare").removeAttr("disabled");
                     }
                 });
             },
             declare() {
                 if(fn.op == 'three_order') {
                     if(statusId != 'temporary') {
                         confirm_inquiry('该单之前已经申报过三单，请确认是否重新申报？',function () {
                             fn.threeDeclare({'id':id});
                         });
                     } else {
                         fn.threeDeclare({'id':id});
                     }
                 } else if (fn.op == 'inventory') {
                     if(statusId == 'temporary') {
                         show_error("该单还未申报三单，不允许申报清单");
                         return;
                     }
                     if(statusId != 'three_declare') {
                         confirm_inquiry('该单之前已经申报过清单，请确认是否重新申报？',function () {
                             fn.inventoryDeclare({'id':id});
                         });
                     } else {
                         confirm_inquiry('请确认三单数据是否完整发送，且回执状态也已完全送达更新，如回执还不完整，请耐心等待？',function () {
                             fn.inventoryDeclare({'id':id});
                         });
                     }
                 } else if (fn.op == 'waybill') {
                     if(statusId == 'temporary') {
                         show_error("该单还未申报三单，不允许申报总分单");
                         return;
                     }
                     if(statusId == 'three_declare') {
                         show_error("该单还未申报清单，不允许申报总分单");
                         return;
                     }
                     if(statusId != 'inventory') {
                         confirm_inquiry('该单之前已经申报过总分单，请确认是否重新申报？',function () {
                             fn.waybillDeclare({'id':id});
                         });
                     } else {
                         fn.waybillDeclare({'id':id});
                     }
                 } else if (fn.op == 'departure') {
                     if(statusId == 'temporary') {
                         show_error("该单还未申报三单，不允许申报离境单");
                         return;
                     }
                     if(statusId == 'three_declare') {
                         show_error("该单还未申报清单，不允许申报离境单");
                         return;
                     }
                     if(statusId == 'inventory') {
                         show_error("该单还未申报总分单，不允许申报离境单");
                         return;
                     }
                     if(statusId != 'waybill') {
                         confirm_inquiry('该单之前已经申报过离境单，请确认是否重新申报？',function () {
                             fn.departureDeclare();
                         });
                     } else {
                         fn.departureDeclare();
                     }
                 }
             },
             setDeclareBtnName(ele){
                 let op = ele.attr('lay-id');
                 fn.op = op;
                 $("#btn-text").text(opData[op] + "申报");
                 if(fn.op == 'departure') {
                     $("#vehicle_leave").show();
                 } else {
                     $("#vehicle_leave").hide();
                 }
             },
             init(){
                 if(statusId == 'temporary') {
                     $('li[lay-id="three_order"]').addClass("layui-this");
                 } else if (statusId == 'three_declare') {
                     $('li[lay-id="inventory"]').addClass("layui-this");
                 } else if (statusId == 'inventory') {
                     $('li[lay-id="waybill"]').addClass("layui-this");
                 } else if (statusId == 'waybill') {
                     $('li[lay-id="departure"]').addClass("layui-this");
                 }
                 $('.layui-tab-title li').each(function (index) {
                     if($(this).hasClass('layui-this')) {
                         fn.setDeclareBtnName($(this));
                     }
                 });
             },
             getDeclareSituation(){
                 $.get(rurl('/declare/vehicleDeclare/declareSituation?id=' + id),function (res) {
                     laytpl($('#declareSituationTemp1').html()).render(res.data,function (html) {
                         $('#declareSituationView1').html(html);
                     });
                     if(res.data.declareInfos != null) {
                         laytpl($('#declareSituationTemp2').html()).render(res.data.declareInfos,function (html) {
                             $('#declareSituationView2').html(html);
                         });
                     }
                 });
             },
        };

    element.on('tab(declare)',function (data) {
        fn.setDeclareBtnName($(this));
        fn.getDeclareSituation();
    });
    $('.declare').click(function (){
        fn.declare();
    });
    fn.getDeclareSituation();
    fn.init();
});