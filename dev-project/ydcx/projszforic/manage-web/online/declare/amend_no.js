layui.use(['layer','form','table','element','laytpl'], function() {
    var layer = layui.layer, form = layui.form, table = layui.table,element = layui.element,
        laytpl = layui.laytpl,
        id = getUrlParam("id"),memberIds = getUrlParam("memberIds"),
        statusId = getUrlParam("statusId"),orderStatus = getUrlParam("orderStatus"),waybillOrderNo = getUrlParam("waybillOrderNo"),
        codeTs = getUrlParam("codeTs"),batchDeclareSize = getUrlParam("batchDeclareSize");
    fn = {
             amendNo( ) {
                 var changeTypeArray = [],changeRegularArray = [];
                 $('input[name="changeType"][type=checkbox]:checked').each(function() {
                     changeTypeArray.push($(this).val());
                 });
                 $('input[name="changeRegular"][type=radio]:checked').each(function() {
                     changeRegularArray.push($(this).val());
                 });
                 if(changeTypeArray == null || changeTypeArray.length <=0) {
                     show_error("请选择修改单号类型");
                     return;
                 }
                 if(changeRegularArray == null || changeRegularArray.length <=0) {
                     show_error("请选择修改单号类型");
                     return;
                 }
                 let changeType = "";
                 if(changeTypeArray.indexOf("1") != -1 && changeTypeArray.indexOf("2") != -1) {
                     changeType = "3";
                 } else {
                     changeType = changeTypeArray[0];
                 }
                 if(isEmpty($("#changeNo").val())) {
                     show_error("请输入前缀/后缀/单号");
                     return;
                 }
                 let params = {
                     "id":id,
                     "memberIds":memberIds,
                     "statusId":statusId,
                     "orderStatus":orderStatus,
                     "waybillOrderNo":waybillOrderNo,
                     "codeTs":codeTs,
                     "changeType":changeType,
                     "changeRegular":changeRegularArray[0],
                     "changeNo":$("#changeNo").val()
                 }
                 $.post("/declare/vehicleDeclare/amendNo",params, function (res) {
                     if(isSuccess(res)) {
                         window.parent.location.reload();
                         closeThisWin('修改单号成功，如需重新申报，请操作同样的数据点击‘批量申报’按钮操作');
                     }
                 });
             },
             showLayer( ){
                 if(null == memberIds || typeof memberIds == "undefined") {
                     memberIds = "";
                 }
                 var ids= new Array();
                 ids = memberIds.split(",").filter(str=>{return !!str});
                 if((ids == null || ids.length == 0)
                     && batchDeclareSize <= 0) {
                     show_error("当前没有数据可以修改单号，如需修改单号请勾选数据或者输入条件查询");
                     return;
                 }
                 let title = '修改单号（' + ( (ids != null && ids.length > 0) ?  ('按选中' + ids.length) : ('按筛选结果' + batchDeclareSize) ) +  '条）';
                 parent.$(".layui-layer-title").html(title);
             },

        };

    $('.amendNo').click(function (){
        fn.amendNo();
    });
    fn.showLayer();
});