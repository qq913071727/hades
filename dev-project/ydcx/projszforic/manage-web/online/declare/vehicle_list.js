layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    let ebcNameDecrypt=getUrlParam("ebcName"),dateStart=getUrlParam("dateStart"),dateEnd=getUrlParam("dateEnd");

    if(isNotEmpty(ebcNameDecrypt) && isNotEmpty(dateStart) && isNotEmpty(dateEnd)) {
        var key = CryptoJS.enc.Utf8.parse("1234567890123456");
        var encryptedHexStr = CryptoJS.enc.Hex.parse(ebcNameDecrypt);
        var encryptedBase64Str = CryptoJS.enc.Base64.stringify(encryptedHexStr);
        var decryptedData = CryptoJS.AES.decrypt(encryptedBase64Str, key, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        var ebcName = decryptedData.toString(CryptoJS.enc.Utf8);
        $("#ebcName").val(ebcName);$("#dateStart").val(dateStart);$("#dateEnd").val(dateEnd);
    }

    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('declare/vehicleDeclare/pageList')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'statusName', width:90, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                    ,{field:'subStatusDesc', width:90, title: '进度',align:'center', fixed: true}
                    ,{field:'docNo', width:120, title: '系统单号',templet:'#docNoTemp', fixed: true}
                    ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                    ,{field:'billNo', width:120, title: '提运单号'}
                    ,{field:'trafName', width:85, title: '车牌号'}
                    ,{field:'totalSingle', width:80, title: '总单数',align:'center'}
                    ,{field:'totalNum', width:70, title: '总件数',align:'center'}
                    ,{field:'totalNetWeight', width:80, title: '总净重',align:'right'}
                    ,{field:'totalGrossWeight', width:80, title: '总毛重',align:'right'}
                    ,{field:'customMasterName', width:90, title: '申报海关',align:'center'}
                    ,{field:'tradeModeName', width:170, title: '贸易方式',templet:'#tradeModeNameTemp'}
                    ,{field:'dateCreated', width:120, title: '制单时间',align:'center'}
                    ,{field:'ieDate', width:80, title: '出口日期',align:'center'}
                    ,{field:'createBy', width:80, title: '制单人'}
                ]]
            },listQueryDone:function () {
                formatTableData();
            }
        });
    });
});
function fullCustomerData(data) {
    $('#customerId').val(data.id);
}
function editDeclare(id) {
    openwin('online/declare/page.html?id='.concat(id),'整车申报',{area:['0','0']});
}