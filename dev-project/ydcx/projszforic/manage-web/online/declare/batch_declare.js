layui.use(['layer','form','table','element','laytpl'], function() {
    var layer = layui.layer, form = layui.form, table = layui.table,element = layui.element,
        laytpl = layui.laytpl,
        id = getUrlParam("id"),memberIds = getUrlParam("memberIds"),
        statusId = getUrlParam("statusId"),orderStatus = getUrlParam("orderStatus"),vehicleStatusId = getUrlParam("vehicleStatusId"),
        waybillOrderNo = getUrlParam("waybillOrderNo"),
        codeTs = getUrlParam("codeTs"),batchDeclareSize = getUrlParam("batchDeclareSize");
        const opData = {'order':'订单','receipts':'收款单','logistics':'物流单',
            'three_order': '三单','inventory':'清单','waybill':'总分单','inventoryCancel':'撤销单'};
    fn = {
             op:'order',//申报类型
             orderDeclare(params) {//订单申报
                 $.post("/declare/vehicleDeclare/orderDeclare",params, function (res) {
                     if(isSuccess(res)) {
                         closeThisWin('批量订单申报提交成功，请前往单一窗口查询申报情况，并耐心等待回执结果');
                     }
                 });
             },
            receiptsDeclare(params) {//收款单申报
                $.post("/declare/vehicleDeclare/receiptsDeclare",params, function (res) {
                    if(isSuccess(res)) {
                        closeThisWin('批量收款单申报提交成功，请前往单一窗口查询申报情况，并耐心等待回执结果');
                    }
                });
            },
            logisticsDeclare(params) {//物流单申报
                $.post("/declare/vehicleDeclare/logisticsDeclare",params, function (res) {
                    if(isSuccess(res)) {
                        closeThisWin('批量物流单申报提交成功，请前往单一窗口查询申报情况，并耐心等待回执结果');
                    }
                });
            },
             threeDeclare(params){//三单申报
                 $.post("/declare/vehicleDeclare/batchThreeDeclare",params, function (res) {
                     if(isSuccess(res)) {
                         closeThisWin('批量三单申报提交成功，请前往单一窗口查询申报情况，并耐心等待回执结果');
                     }
                 });
             },
             inventoryDeclare(params){ //清单申报
                 $.post("/declare/vehicleDeclare/batchInventoryDeclare",params, function (res) {
                     if(isSuccess(res)) {
                         closeThisWin('批量清单申报提交成功，请前往单一窗口查询申报情况，并耐心等待回执结果');
                     }
                 });
             },
             waybillDeclare(params) { //重新申报总分单
                 $.post("/declare/vehicleDeclare/againWaybillDeclare",params, function (res) {
                     if(isSuccess(res)) {
                         closeThisWin('重报总分单申报提交成功，请前往单一窗口查询申报情况，并耐心等待回执结果');
                     }
                 });
             },
            inventoryCancelDeclare(params){ //撤销单单申报
                $.post("/declare/vehicleDeclare/batchInventoryCancelDeclare",params, function (res) {
                    if(isSuccess(res)) {
                        closeThisWin('批量撤销单单申报提交成功，请前往单一窗口查询申报情况，并耐心等待回执结果');
                    }
                });
            },
             declare() {
                 let params = {
                     "id":id,
                     "memberIds":memberIds,
                     "statusId":statusId,
                     "orderStatus":orderStatus,
                     "waybillOrderNo":waybillOrderNo,
                     "codeTs":codeTs,
                     "cebType":'SZ'//报关渠道，默认深关通
                 }
                 if(fn.op == 'order') {
                     var cebType = $("#cebType").val();
                     if(isEmpty(cebType)) {
                         show_error("请选择9710渠道");
                         return;
                     }
                     params.cebType = cebType;
                     fn.orderDeclare(params);
                 } else if(fn.op == 'receipts') {
                     fn.receiptsDeclare(params);
                 } else if(fn.op == 'logistics') {
                     fn.logisticsDeclare(params);
                 } else if(fn.op == 'three_order') {
                     fn.threeDeclare(params);
                 } else if (fn.op == 'inventory') {
                     fn.inventoryDeclare(params);
                 } else if (fn.op == 'waybill') {
                     fn.waybillDeclare(params);
                 } else if (fn.op == 'inventoryCancel') {
                     fn.inventoryCancelDeclare(params);
                 }
             },
             setDeclareBtnName(ele){
                 let op = ele.attr('lay-id');
                 fn.op = op;
                 if(fn.op != 'waybill') {
                     $("#btn-text").text(opData[op] + "批量申报");
                 } else {
                     $("#btn-text").text(opData[op] + "重新申报");
                 }

                 if(null == memberIds || typeof memberIds == "undefined") {
                     memberIds = "";
                 }
                 var ids= new Array();
                 ids = memberIds.split(",").filter(str=>{return !!str});
                 if((ids == null || ids.length == 0)
                     && batchDeclareSize <= 0) {
                     show_error("当前没有数据可以批量申报，如需批量申报请勾选数据或者输入条件查询");
                     return;
                 }
                 if(fn.op == 'waybill') {
                     parent.$(".layui-layer-title").html("重新申报总分单");
                 } else {
                     let title = '批量申报（' + ( (ids != null && ids.length > 0) ?  ('按选中' + ids.length) : ('按筛选结果' + batchDeclareSize) ) +  '条，请仔细确认操作的数据）';
                     parent.$(".layui-layer-title").html(title);
                 }
                 if(fn.op == 'order') {
                    $(".declareOrderSituation").show();
                 } else {
                    $(".declareOrderSituation").hide();
                 }
                 if(fn.op == 'inventoryCancel') {
                     $(".declareSituation").show();
                 } else {
                     $(".declareSituation").hide();
                 }
                 if(fn.op == 'inventoryCancel') { //撤销单
                     //无法从上一个页面带过来，从后台查询获取吧
                     let params = {
                         "id":id,
                         "memberIds":memberIds,
                         "statusId":statusId,
                         "orderStatus":orderStatus,
                         "waybillOrderNo":waybillOrderNo,
                         "codeTs":codeTs
                     }
                     $.post("/declare/vehicleDeclare/getInventoryCancelSituation",params, function (res) {
                         laytpl($('#declareSituationTemp').html()).render(res.data,function (html) {
                             $('#declareSituationView').html(html);
                         });
                     });
                 }
             },
             init(){
                 if(vehicleStatusId == 'temporary') {
                     $('li[lay-id="order"]').addClass("layui-this");
                 } else if (vehicleStatusId == 'three_declare') {
                     $('li[lay-id="inventory"]').addClass("layui-this");
                 } else if (vehicleStatusId == 'inventory') {
                     $('li[lay-id="waybill"]').addClass("layui-this");
                 } else if (vehicleStatusId == 'waybill') {
                     $('li[lay-id="departure"]').addClass("layui-this");
                 }
                 $('.layui-tab-title li').each(function (index) {
                     if($(this).hasClass('layui-this')) {
                         fn.setDeclareBtnName($(this));

                     }
                 });
             },
        };

    element.on('tab(declare)',function (data) {
        fn.setDeclareBtnName($(this));
    });
    $('.declare').click(function (){
        fn.declare();
    });
    fn.init();
});