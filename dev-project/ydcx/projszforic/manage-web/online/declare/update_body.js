layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form,
        fn = {
            init:function() {
                let id = getUrlParam("id");
                if(isNotEmpty(id)){
                    show_loading();
                    $.get('/declare/vehicleDeclareGoods/detail',{id:id},function (res) {
                        if(isSuccessWarnClose(res)){
                            var data = res.data;
                            form.val('pageForm',data);
                        }
                    });
                }
            },
            save:function () {
                if(checkedForm($('#pageForm'))){
                    formSub('/declare/vehicleDeclareGoods/updateVehicleBody',function (res) {
                        window.parent.location.reload();
                        closeThisWin('保存成功');
                    });
                }
            },
            saveAndSend:function () {
                if(checkedForm($('#pageForm'))){
                    formSub('/declare/vehicleDeclareGoods/updateVehicleBodyAndChange3Order',function (res) {
                        window.parent.location.reload();
                        closeThisWin('保存成功,请前往单一窗口查询变更情况，并耐心等待回执信息');
                    });
                }
            }
        };
    fn.init();
    $('.save-btn').click(function(){fn.save()});
    $('.save-ceb-btn').click(function(){fn.saveAndSend()});
});

//修改单价计算总价
function tdeclPrice() {
    var gqty = parseFloat($('#gqty').val()||'0'),declPrice = parseFloat($('#declPrice').val()||'0');
    if(gqty>0&&declPrice>0){
        $('#declTotal').val(Math.round(gqty*declPrice*100)/100);
    }
}
//修改总价计算单价
function tdeclTotal() {
    var gqty = parseFloat($('#gqty').val()||'0'),declTotal = parseFloat($('#declTotal').val()||'0');
    if(gqty>0&&declTotal>0){
        $('#declPrice').val(Math.round(declTotal/gqty*10000)/10000);
    }
}