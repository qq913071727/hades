layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form,
        fn = {
            init:function(){
                    let id = getUrlParam("id");
                    if(isNotEmpty(id)){
                        show_loading();
                        $.get('/declare/onlineCustomer/detail',{id:id},function (res) {
                            if(isSuccessWarnClose(res)){
                                var data = res.data;
                                form.val('pageForm',data);
                            }
                        });
                    }
            },
            save:function () {
                if(checkedForm($('#pageForm'))){
                    formSub('/declare/onlineCustomer/edit',function (res) {
                        parent.reloadList();
                        closeThisWin('保存成功');
                    });
                }
            }
        };
    fn.init();
    $('.save-btn').click(function(){fn.save()});
});