layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form,
        fn = {
            save:function () {
                if(checkedForm($('#pageForm'))){
                    formSub('/declare/onlineCustomer/add',function (res) {
                        parent.reloadList();
                        closeThisWin('保存成功');
                    });
                }
            }
        };
    $('.save-btn').click(function(){fn.save()});
});