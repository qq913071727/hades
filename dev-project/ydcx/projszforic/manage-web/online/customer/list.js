layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('declare/onlineCustomer/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'companyName', width:180, title: '公司名称'}
                ,{field:'code', width:80, title: '公司编码'}
                ,{field:'socialNo', width:160, title: '统一社会信用代码'}
                ,{field:'linkPerson', width:100,title: '联系人'}
                ,{field:'linkTel', width:100,title: '联系电话'}
                ,{field:'linkFax', width:100,title: '传真'}
                ,{field:'address', width:180,title: '联系地址'}
                ,{field:'dateCreated', width:130,title: '创建时间',align:'center'}
            ]]
        },listQueryDone:function () {

        }
    });
});
