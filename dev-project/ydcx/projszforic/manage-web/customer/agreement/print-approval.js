layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer, laytpl = layui.laytpl, form = layui.form;
    var id = getUrlParam('id');
    show_loading();
    $.get(rurl('scm/agreement/getPrintApproval'),{'id':id},function (res) {
        if(isSuccessWarnClose(res)){
            var data = res.data;
            console.log(data);
            laytpl($('#conTemp').html()).render(data,function (html) {
                $('#conTempView').html(html);
            });
        }
    });
    
    $("#exportExcel").click(function () {
        toUrl('/scm/agreement/exportExcel?id='+id);
    });
});