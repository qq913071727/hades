layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload','laytpl'], function() {
    var layer = layui.layer, form = layui.form,
        table = layui.table,
        laytpl = layui.laytpl,
        fileupload = layui.fileupload;
    var id = getUrlParam("id");
    show_loading();
    $('.sel-load').selLoad(function () {
        form.render('select');

        $.get(rurl('scm/agreement/info'),{'id':id,'operation':'agreement_edit'},function (res) {
            if(isSuccessWarnClose(res)){
                var data = res.data,agreement=data.agreement,impClause=data.impClause,impQuotes=data.impQuotes;
                form.val('baseForm',agreement);
                form.val('clauseForm',impClause);
                showIsPaidIn();
                $('#advance_charge').find('input[name="quoteType"]').removeAttr("checked");
                $.each(impQuotes,function (i,o) {
                    form.val(o.quoteType+'_form',o);
                    showType(o.quoteType);
                    showType('tax_'+o.quoteType);
                    showType('go_'+o.quoteType);

                    calculationFormula(o.quoteType);
                    var fitObj = fitArg(o);
                    var $form = $('#'+o.quoteType),$taxFit = $form.find('input[name="taxFit"]'),$goodFit = $form.find('input[name="goodFit"]');
                    var $quoteType = $form.find('input[name="quoteType"]');
                    var $agencyFeeMode = $form.find('select[name="agencyFeeMode"]');

                    $taxFit.attr({'checked':fitObj.taxFit?true:null});
                    $goodFit.attr({'checked':fitObj.goodFit?true:null});
                    $quoteType.attr({'checked':true});

                    showOtherArg($taxFit);
                    showOtherArg($goodFit);

                    form.render('select');
                });
                form.render('checkbox');
                var fileObj = {docId:id, docType:'agreement'};
                fileupload.render({elem:'#agr_evidence',startQuery:true,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_evidence'})}});
                fileupload.render({elem:'#agr_original',startQuery:true,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_original'})}});
                $('.save-btn').click(function () {saveAgreement(false,fileObj);});
                $('.examine-btn').click(function () {saveAgreement(true,fileObj);});
                selectDeclareType();
                initPage();
            }
        });
    });

    form.on('select(select-agree-mode)', function(data){
        showType($(data.elem).attr('tsf-mark'))
    });
    // 代理费计费模式选择
    form.on('select(select-agency-fee-mode)', function(data){
        showType($(data.elem).attr('tsf-mark'));
    });
    form.on('select(select-tax-type)', function(data){
        calculationFormula($(data.elem).attr('cal-mark'))
    });
    form.on('select(select-declare-type)', function(data){
        selectDeclareType();
    });
    form.on('checkbox(check-tax-included)', function(data){
        calculationFormula($(data.elem).attr('cal-mark'))
    });
    form.on('checkbox(checkbox-arg)', function(data){
        showOtherArg($(data.elem));
    });
    form.on('checkbox(checkbox-isPaidIn)', function(data){
        showIsPaidIn();
    });
    let tplquotehtml = $('#tpl-standard-quote').html();
    $.each(['advance_charge','pad_tax','pad_good'],function (i,o) {
        $.get('/scm/expressStandardQuote/currentEffective',{quoteType:o},function (res) {
            laytpl(tplquotehtml).render(res.data, function (html) {
                $('#view-'.concat(o)).html(html);
            });
        });
    });
});
function saveAgreement(submitAudit,fileObj) {
    if(checkedForm($('.tsf-page-content'))){
        var impQuotes = obtainQuoraForm();
        if(impQuotes==null){return;}
        show_loading();
        var agreement = $('#baseForm').serializeJson(),impClause = $('#clauseForm').serializeJson();
        impClause['isPaidIn'] = isEmpty($('#isPaidIn').attr("checked"))?false:true;
        if(impClause['isPaidIn']){
            impClause['taxRate'] = 'imp_customs';
            impClause['taxRateTime'] = '';
        }
        agreement['submitAudit'] = submitAudit;

        var agencyFeeMode = $("#agencyFeeMode").val();
        postJSON('scm/agreement/edit',{"agreement":agreement,"impClause":impClause,"impQuotes":impQuotes,"file":fileObj,
                                                'agencyFeeMode':agencyFeeMode},function (res) {
            parent.reloadList();
            closeThisWin('修改成功');
        });
    }
}