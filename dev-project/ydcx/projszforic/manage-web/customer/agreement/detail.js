layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table,fileupload = layui.fileupload;
    var id = getUrlParam("id");
    $('#agreementDetail').load(rurl('customer/agreement/detail-temp.html'),function () {
        form.render();
        show_loading();
        $('.sel-load').selLoad(function () {
            form.render('select');
            initPage();
            $.get(rurl('scm/agreement/info'),{'id':id},function (res) {
                if(isSuccessWarnClose(res)){
                    var data = res.data,agreement=data.agreement,impClause=data.impClause,impQuotes=data.impQuotes;
                    form.val('baseForm',agreement);
                    form.val('clauseForm',impClause);
                    showIsPaidIn();
                    $('#advance_charge').find('input[name="quoteType"]').removeAttr("checked");
                    $.each(impQuotes,function (i,o) {
                        form.val(o.quoteType+'_form',o);
                        showType(o.quoteType);
                        showType('tax_'+o.quoteType);
                        showType('go_'+o.quoteType);
                        calculationFormula(o.quoteType);
                        var fitObj = fitArg(o);
                        var $form = $('#'+o.quoteType),$taxFit = $form.find('input[name="taxFit"]'),$goodFit = $form.find('input[name="goodFit"]');
                        var $quoteType = $form.find('input[name="quoteType"]');
                        $taxFit.attr({'checked':fitObj.taxFit?true:null});
                        $goodFit.attr({'checked':fitObj.goodFit?true:null});
                        $quoteType.attr({'checked':true});

                        showOtherArg($taxFit);
                        showOtherArg($goodFit);
                    });
                    form.render('checkbox');
                    var fileObj = {docId:id, docType:'agreement'};
                    fileupload.render({elem:'#agr_evidence',startQuery:true,edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_evidence'})}});
                    fileupload.render({elem:'#agr_original',startQuery:true,edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_original'})}});
                    selectDeclareType();
                    disableForm(form);
                }
            });
        });
        form.on('select(select-agree-mode)', function(data){
            showType($(data.elem).attr('tsf-mark'))
        });
        // 代理费计费模式选择
        form.on('select(select-agency-fee-mode)', function(data){
            showType($(data.elem).attr('tsf-mark'));
        });
    });
});

function showExpressQuoteHelp(quoteType) {
    let customerName = "";
    var partyaName = $("#partyaName").val();
    if(!isEmpty(partyaName)) {
        //为解决乱码，加密
        var encrypted = CryptoJS.AES.encrypt(partyaName, "123456");
        customerName = encrypted;
    }
    openwin(rurl('customer/agreement/expressQuote.html?customerName='+customerName+'&quoteType='+quoteType),'计费详情',{area: ['620px', '800px']});
}