function initPage() {

    showType('fee_mode_advance_charge');// 预付款代理费计费模式选择
    showType('fee_mode_pad_tax');// 垫税款代理费计费模式选择
    showType('fee_mode_pad_good');// 垫货款代理费计费模式选择

    showType('pad_tax');// 垫税款代杂费账期选择
    showType('pad_good');// 垫货款代杂费账期选择

    showType('tax_pad_tax');//垫税款税款账期选择
    showType('tax_pad_good');//垫货款税款账期选择
    showType('go_pad_good');//垫货款货款账期选择

    calculationFormula('advance_charge');
    calculationFormula('pad_tax');
    calculationFormula('pad_good');
}
function showType(type) {
    var $type = $('div[show-type]'),mark = $('select[tsf-mark="'+type+'"]').val();
    $.each($type,function (i,o) {
        var $that = $(o),type_val = $that.attr('show-type');
        if(type_val.indexOf(type)==0){
            $that.hide();
        }
        if(type_val==type.concat('_').concat(mark)){
            $that.show();
        }
    });
}
function calculationFormula(type) {
    var $arae = $('#'+type),isIncluded=(($arae.find('input[name="taxIncluded"]').attr('checked'))?true:false),
        formula = [
            isIncluded?'(':'',
            $arae.find('input[name="basePrice"]').val(),' + (报关金额 * 海关汇率',
            ('after_tax'==$arae.find('select[name="taxType"]').val())?' + 关税 + 增值税 + 消费税':'',
            ') * ',$arae.find('input[name="serviceRate"]').val(),'%',
            isIncluded?') * (1+税点)':''
        ];

    $arae.find('span[show-formula]').html(formula.join(''));
}
function fullCustomerData(cus) {
    layui.form.val('baseForm',{'partyaLegalPerson':cus.legalPerson,'partyaSocialNo':cus.socialNo,'partyaTel':cus.tel,'partyaFax':cus.fax,'partyaAddress':cus.address});
}
function generateDocNo() {
    show_loading();
    $.post(rurl('scm/serialNumber/generateDocNo'),{'type':'imp_quote'},function (res) {
        if(isSuccess(res)){
            $('#docNo').val(res.data);
        }
    });
}
function obtainQuoraForm() {
    var $quoraForm = $('.quora-form'),impQuotes = [];
    for(var i=0;i<$quoraForm.length;i++){
        var $form = $($quoraForm[i]),quora=$form.serializeJson();
        if(!isEmpty(quora.quoteType)){
            quora['taxIncluded'] = $form.find('input[name="taxIncluded"]').attr("checked")?true:false;
            quora['taxFit'] = $form.find('input[name="taxFit"]').attr("checked")?true:false;
            quora['goodFit'] = $form.find('input[name="goodFit"]').attr("checked")?true:false;
            impQuotes.push(quora);
        }
    }
    if(impQuotes.length==0){
        show_error('请至少选择一种账期报价');
        return null;
    }
    return impQuotes;
}
function selectDeclareType() {
    var $declareTypeDesc = $('#declareTypeDesc');
    switch ($('#declareType').val()){
        case 'single':
            $declareTypeDesc.html('报关单消费使用单位为公司(代理进口不能选择单抬头)');
            break;
        case 'double':
            $declareTypeDesc.html('报关单消费使用单位为客户');
            break;
        default:
            $declareTypeDesc.html('');
            break;
    }
}
function showOtherArg(e) {
    var $that = e;
    var $arg=$($that.attr('arg-type'));
    if($that.attr('checked')){$arg.hide();}else{$arg.show();}
}
function fitArg(o) {
    var period = [o.agreeMode,o.day,o.month,o.monthDay,o.firstMonth,o.firstMonthDay,o.lowerMonth,o.lowerMonthDay,o.week,o.weekDay].join('-')
        ,taxPeriod = [o.taxAgreeMode,o.taxDay,o.taxMonth,o.taxMonthDay,o.taxFirstMonth,o.taxFirstMonthDay,o.taxLowerMonth,o.taxLowerMonthDay,o.taxWeek,o.taxWeekDay].join('-')
        ,goPeriod = [o.goAgreeMode,o.goDay,o.goMonth,o.goMonthDay,o.goFirstMonth,o.goFirstMonthDay,o.goLowerMonth,o.goLowerMonthDay,o.goWeek,o.goWeekDay].join('-');
    return {
        taxFit:period==taxPeriod,
        goodFit:period==goPeriod
    };
}
function showIsPaidIn() {
    var $isPaidIn = $('#isPaidIn'),$taxRate=$('#taxRate'),$taxRateTime=$('#taxRateTime');
    if($isPaidIn.attr('checked')){
        layui.form.val('clauseForm',{'taxRate':'imp_customs','taxRateTime':''});
        $taxRate.attr({'disabled':true}).addClass('layui-disabled');
        $taxRateTime.attr({'disabled':true}).addClass('layui-disabled');
    }else{
        $taxRate.attr({'disabled':null}).removeClass('layui-disabled');
        $taxRateTime.attr({'disabled':null}).removeClass('layui-disabled');
    }
    layui.form.render('select');
}