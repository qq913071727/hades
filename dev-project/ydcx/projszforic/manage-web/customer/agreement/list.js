layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form','table','dropdown'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form,table = layui.table,dropdown = layui.dropdown;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
           url:rurl('scm/agreement/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'task', width:50, title: '任务',align:'center',toolbar:'#taskTemp', fixed: true}
                ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                ,{field:'docNo', fixed:true,width:120, title: '协议编号',templet: '#agreementTemp'}
                ,{field:'businessTypeDesc', width:80, title: '业务类型'}
                ,{field:'declareTypeDesc', width:80, title: '报关类型'}
                ,{field:'partyaName',width:220, title: '客户名称',templet:'#partyaNameTemp'}
                ,{field:'isSignBack',width:100, title: '是否签回正本',templet:'#isSignBackTemp',align:'center'}
                ,{field:'originalAgr', width:100, title: '正本协议',templet:'#originalAgrTemp',align:'center'}
                ,{field:'dateCreated', width:90, title: '创建日期',align:'center'}
                ,{field:'createBy', width:80, title: '创建人'}
                ,{field:'quoteDescribe',title: '报价描述'}
                ,{fixed: 'right', title:'下载模板', toolbar: '#down-dropdown-toolbar', width:90,align:'center'}
            ]]
        },listQueryDone:function () {
            initFileSize();
        },toolClick:function (that,event,data) {
            if(event=='more-down'){
                dropdown.render({
                    elem: that
                    ,show: true
                    ,data: [{
                        id: data.afileId
                        ,title: '服务协议'
                        ,templet: '<a href="/scm/download/file/{{d.id}}" target="_blank"> {{d.title}}</a>'
                    }
                    // , {
                    //     id: data.qfileId
                    //     ,title: '报价单'
                    //     ,templet: '<a href="/scm/download/file/{{d.id}}" target="_blank"> {{d.title}}</a>'
                    // }
                    ]
                });
            }
        }
    });

});
//协议详情
function showDetail(id){
    openwin(rurl('customer/agreement/detail.html?id='+id),'协议详情',{area: ['0', '0'],shadeClose:true});
}
//原件签回
function originalSignBack (o) {
    var ids = getIds();
    if(ids.length!=1){show_error("请选择一条服务协议签回");return;}
    confirm_inquiry("您是否确定当前协议正本已经签回？",function () {
        show_loading();
        $.post(rurl(o.url),{'id':ids[0]},function (res) {if(isSuccess(res)){show_msg("操作成功");reloadList();}});
    })
}

//代客户确认
function substituteSign(o) {
    let data = getCheckDatas();
    if(data.length != 1) {
        show_error('请选择一条数据操作');
    }
    confirm_inquiry('您确认是否已与客户沟通并代替客户确认此进口报价吗？',function () {
        $.post(rurl(o.url),{'agreementId':data[0].id},function (res) {
            if(isSuccessWarn(res)){
                show_success('操作成功');
                reloadList();
            }
        });
    });
}

function exportExcel(o) {
    let ids = getIds();
    if(ids.length != 1) {
        show_error("请单选一条记录");
        return;
    }
    toUrl('/scm/agreement/exportExcel?id='+ids[0]);
}