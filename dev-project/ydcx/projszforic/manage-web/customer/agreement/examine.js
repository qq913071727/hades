layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload','element'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table,fileupload = layui.fileupload,element=layui.element;
    var id = getUrlParam("id"),customerId='';
    $('#agreementDetail').load(rurl('customer/agreement/detail-temp.html'),function () {
        show_loading();
        $('.sel-load').selLoad(function () {
            form.render('select');
            initPage();
            $.get(rurl('scm/agreement/info'),{'id':id},function (res) {
                if(isSuccessWarnClose(res)){
                    var data = res.data,agreement=data.agreement,impClause=data.impClause,impQuotes=data.impQuotes;
                    customerId=agreement.customerId;
                    form.val('baseForm',agreement);
                    form.val('clauseForm',impClause);
                    showIsPaidIn();
                    $.each(impQuotes,function (i,o) {
                        form.val(o.quoteType+'_form',o);
                        showType(o.quoteType);
                        showType('tax_'+o.quoteType);
                        showType('go_'+o.quoteType);
                        calculationFormula(o.quoteType);
                        var fitObj = fitArg(o);
                        var $form = $('#'+o.quoteType),$taxFit = $form.find('input[name="taxFit"]'),$goodFit = $form.find('input[name="goodFit"]');
                        $taxFit.attr({'checked':fitObj.taxFit?true:null});
                        $goodFit.attr({'checked':fitObj.goodFit?true:null});

                        showOtherArg($taxFit);
                        showOtherArg($goodFit);
                    });
                    form.render('checkbox');
                    var fileObj = {docId:id, docType:'agreement'};
                    fileupload.render({elem:'#agr_evidence',startQuery:true,edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_evidence'})}});
                    fileupload.render({elem:'#agr_original',startQuery:true,edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_original'})}});
                    selectDeclareType();
                    disableForm(form,'#agreementDetail');
                }
            });
        });
    });
    var $operationTaskBtn = $('.operation-task-btn'),
    taskArg = {'operation':'agreement_examine','documentId':id,'documentClass':'Agreement','newStatusCode':''};
    $operationTaskBtn.click(function () {
        var $that = $(this),needMemo = $that.attr('need-memo');
        if(!isEmpty(needMemo)){
            var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
            taskArg['memo'] = $memo.val();
        }else{
            taskArg['memo'] = $('#taskMemo').val();
        }
        taskArg['newStatusCode'] = $that.attr('status-code');

        show_loading();
        $.post(rurl('scm/agreement/examine'),taskArg,function (res) {
            if(isSuccess(res)){
                parent.reloadList();
                closeThisWin('操作成功');
            }
        });
    });

    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
    autoDetailContent();
    $(window).resize(function() {
        autoDetailContent();
    });
    var isLoad = false;
    element.on('tab(detailTab)', function(){
        var layId = this.getAttribute('lay-id');
        if(layId=="2"&&isLoad==false){
            isLoad = true;
            $('#customerDetail').attr({'src':rurl('customer/agreement/customer-detail.html?customerId='+customerId)});
        }
    });
});
var $detailContent=$('#detail-content'),$tsfPageDetail = $('.tsf-page-detail');
function autoDetailContent(){
    $detailContent.css({'height':($tsfPageDetail.height()-37)+'px'});
}