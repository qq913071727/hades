layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
           url:rurl('scm/customer/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'task', width:50, title: '任务',align:'center',toolbar:'#taskTemp', fixed: true}
                ,{field:'statusDesc', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                ,{field:'code', width:140, title: '编号'}
                ,{field:'name', width:240, title: '中文名称',templet:'#nameTemp'}
                ,{field:'dateCreated', width:135, title: '登记日期'}
                ,{field:'salePersonName', width:80, title: '跟进销售'}
                ,{field:'busPersonName', width:100, title: '跟进商务',templet:'#busPersonNameTemp'}
                ,{field:'disabled', width:100,align:'form-switch',title: '状态',templet:'#disabledTemp'}
                ,{field:'claimDate', width:90, title: '认领日期',align:'center'}
                // ,{field:'firstOrderDate', width:90, title: '首单日期',align:'center'}
                // ,{field:'endOrderDate', width:90, title: '最近下单日期',align:'center'}
                ,{field:'goodsQuota', width:90, title: '货款额度',align:'money'}
                ,{field:'taxQuota', width:90, title: '税款额度',align:'money'}
                ,{field:'isPhone', width:80, title: '客户端注册',templet:'#clientRegistrationTemp'}
                ,{field:'phoneNo', width:120, title: '注册手机'}
            ]]
        },listQueryDone:function(){
            formatTableData();
        }
    });
    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/customer/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });
});
function showDetail(id) {
    openwin(rurl('customer/detail.html?id='+id),'客户详情',{area: ['0', '0'],shadeClose:true});
}

function importSuccess(msg){prompt_success(msg,function (){show_loading();setTimeout(function () {hide_loading();reloadList();},1000)});}