layui.use(['layer','form','table','element'], function() {
    var element = layui.element;
    var layer = layui.layer, form = layui.form,table = layui.table;
    var id = getUrlParam('id');
    $('.sel-load').selLoad(function () {
        show_loading();
        form.render('select');
        $.get(rurl('scm/customer/getCustomerPerson'),{'id':id},function (res) {
            if(isSuccess(res)){
                var data = res.data;
                form.val('form',{id:data.id,name:data.name,busPersonName:data.busPersonName});

                table.render({
                    elem: '#table-list',limit:5000,id:'table-list',height:'full-100',page:false,
                    url:rurl('/scm/customerPersonChange/history?personType=1&customerId=' + data.id),response:{msgName:'message',statusCode: '1'}
                    ,cols: [[
                        {checkbox: true, fixed: true}
                        ,{field:'dateCreated', width:160, title: '变更时间'}
                        ,{field:'oldPersonName', width:100, title: '原商务'}
                        ,{field:'nowPersonName', width:100, title: '变更后商务'}
                        ,{field:'operator', width:100, title: '操作人'}
                        ,{field:'memo', title: '备注'}
                    ]]
                });
            }
        });
        form.on('submit(save-btn)', function(data){
            formSub('scm/customer/changeBus',function (res) {
                parent.reloadList();
                closeThisWin('变更成功');
            });
            return false;
        });
    });
});