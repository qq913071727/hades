layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload'], function() {
    var layer = layui.layer, form = layui.form, table = layui.table, fileupload = layui.fileupload;
    var id = getUrlParam('id');
    $('#customerDetail').load(rurl('customer/detail-temp.html'),function () {
        show_loading();
        $.get(rurl('scm/customer/info'),{'id':id},function (res) {
            if(isSuccess(res)){
                var data = res.data;
                data.customer.linkAddress = [removeNull(data.customer.invoiceProvince),removeNull(data.customer.invoiceCity),removeNull(data.customer.invoiceArea),removeNull(data.customer.linkAddress)].join(' ');
                form.val('baseForm',data.customer);
                form.render('select');

                var deliverys = data.deliverys || [];
                $.each(deliverys,function (i,o) {
                    o['area'] = '';
                    if(!isEmpty(o.provinceName)&&!isEmpty(o.cityName)&&!isEmpty(o.areaName)){
                        o['area'] = [o.provinceName,o.cityName,o.areaName].join(' / ');
                    }
                });
                deliveryInfoList(deliverys);

                var fileObj = {docId:data.customer.id, docType:'customer'};
                fileupload.render({elem:'#cus_business',edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'cus_business'})}});
                fileupload.render({elem:'#cus_invoice',edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'cus_invoice'})}});
                fileupload.render({elem:'#cus_legal_id',edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'cus_legal_id'})}});
                fileupload.render({elem:'#cus_other',edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'cus_other'})}});
                disableForm(form,'#customerDetail');


                var $operationTaskBtn = $('.operation-task-btn'),
                    taskArg = {'operation':'customer_approve','documentId':id,'documentClass':'Customer','newStatusCode':''};
                $operationTaskBtn.click(function () {
                    var $that = $(this),needMemo = $that.attr('need-memo');
                    if(!isEmpty(needMemo)){
                        var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
                        taskArg['memo'] = $memo.val();
                    }else{
                        taskArg['memo'] = $('#taskMemo').val();
                    }
                    taskArg['newStatusCode'] = $that.attr('status-code');

                    show_loading();
                    $.post(rurl('scm/customer/approving'),taskArg,function (res) {
                        if(isSuccess(res)){
                            parent.reloadList();
                            closeThisWin('操作成功');
                        }
                    });
                });

            }
        });
    });
    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });

    $.get('/scm/statusHistory/recent',{domainId:id,domainType:'com.tsfyun.scm.entity.customer.Customer',nowStatusId:'waitApprove'},function (res) {
       if(isSuccess(res)){
           if(!isEmpty(res.data.memo)){
               $('#top-tips').html(res.data.memo).show();
           }
       }
    });
});
function deliveryInfoList(data) {
    layui.table.render({
        elem: '#delivery-info-list',limit:1000,id:'delivery-info-list'
        ,cols: [[
            {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
            ,{field: 'companyName', title: '收货公司', width: 240}
            ,{field: 'linkPerson', title: '联系人', width: 90}
            ,{field: 'linkTel', title: '联系电话', width: 110}
            ,{field: 'area', title: '收货区域', width: 200}
            ,{field: 'address', title: '详细地址'}
            ,{field: 'disabled', title: '状态', width: 80,align:'form-switch',templet:'#disabledTemp'}
            ,{field: 'isDefault', title: '默认', width: 65,align:'center',templet:'#isDefaultTemp'}
        ]]
        ,data: data
    });
}