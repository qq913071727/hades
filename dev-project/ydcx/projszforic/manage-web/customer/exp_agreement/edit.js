layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload','laytpl'], function() {
    var layer = layui.layer, form = layui.form,
        table = layui.table,
        laytpl = layui.laytpl,
        fileupload = layui.fileupload;
    var id = getUrlParam("id");
    show_loading();
    $('.sel-load').selLoad(function () {
        form.render('select');
        //获取详情
        $.get(rurl('scm/expAgreement/detail'),{'id':id,'operation':'exp_agreement_edit'},function (res) {
            if(isSuccessWarnClose(res)){
                var data = res.data;
                data.isTaxAdvance = data.isTaxAdvance+'';
                form.val('baseForm',data);

                var fileObj = {docId:id, docType:'exp_agreement'};
                fileupload.render({elem:'#agr_evidence',startQuery:true,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_evidence'})}});
                fileupload.render({elem:'#agr_original',startQuery:true,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_original'})}});
                $('.save-btn').click(function () {saveAgreement(false,fileObj);});
                $('.examine-btn').click(function () {saveAgreement(true,fileObj);});
                selectDeclareType();
            }
        });
    });
    form.val('baseForm',{'signingDate':getNowFormatDate(),'effectDate':getNowFormatDate()});
    app.refreshSubject(function (sub) {
        form.val('baseForm',{'partybName':sub.name,'partybLegalPerson':sub.legalPerson,'partybTel':sub.tel,'partybFax':sub.fax,'partybAddress':sub.address});
    });
    form.on('select(select-declare-type)', function(data){
        selectDeclareType();
    });
    form.on('select(select-settlementMode)',function (data) {
        selectAgencyFeeMode();
    });
});
function saveAgreement(submitAudit,fileObj) {
    if(checkedForm($('.tsf-page-content'))){
        show_loading();
        var agreement = $('#baseForm').serializeJson();
        agreement['submitAudit'] = submitAudit;
        postJSON('scm/expAgreement/edit',{"agreement":agreement,"file":fileObj},function (res) {
            parent.reloadList();
            closeThisWin('保存成功');
        });
    }
}
function selectDeclareType() {
    var $declareTypeDesc = $('#declareTypeDesc');
    switch ($('#declareType').val()){
        case 'single':
            $declareTypeDesc.html('报关单生产销售单位为公司');
            break;
        case 'double':
            $declareTypeDesc.html('报关单生产销售单位为客户');
            break;
        default:
            $declareTypeDesc.html('');
            break;
    }
}

function fullCustomerData(cus) {
    layui.form.val('baseForm',{'partyaLegalPerson':cus.legalPerson,'partyaSocialNo':cus.socialNo,'partyaTel':cus.tel,'partyaFax':cus.fax,'partyaAddress':cus.address});
}

function selectAgencyFeeMode() {
    var settlementMode = $("#settlementMode").val();
    if(settlementMode == 'dec_percent') {
        $(".agencyFee-desc").html("代理费率%");
    } else if (settlementMode == 'one_dollar'){
        $(".agencyFee-desc").html("1美金收人民币");
    }
}