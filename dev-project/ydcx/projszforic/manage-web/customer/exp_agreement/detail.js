layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table,fileupload = layui.fileupload;
    var id = getUrlParam("id");
    $('#agreementDetail').load(rurl('customer/exp_agreement/detail-temp.html'),function () {
        form.render();
        show_loading();
        $('.sel-load').selLoad(function () {
            form.render('select');
            $.get(rurl('scm/expAgreement/detail'),{'id':id},function (res) {
                if(isSuccessWarnClose(res)){
                    var data = res.data;
                    form.val('baseForm',data);
                    form.val('clauseForm',data);
                    form.render('checkbox');
                    var fileObj = {docId:id, docType:'exp_agreement'};
                    fileupload.render({elem:'#agr_evidence',startQuery:true,edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_evidence'})}});
                    fileupload.render({elem:'#agr_original',startQuery:true,edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_original'})}});
                    selectDeclareType();
                    selectAgencyFeeMode();
                    disableForm(form);
                }
            });
        });
    });
});

function selectDeclareType() {
    var $declareTypeDesc = $('#declareTypeDesc');
    switch ($('#declareType').val()){
        case 'single':
            $declareTypeDesc.html('报关单生产使用单位为公司');
            break;
        case 'double':
            $declareTypeDesc.html('报关单生产使用单位为客户');
            break;
        default:
            $declareTypeDesc.html('');
            break;
    }
}

function selectAgencyFeeMode() {
    var settlementMode = $("#settlementMode").val();
    if(settlementMode == 'dec_percent') {
        $(".agencyFee-desc").html("代理费率%");
    } else if (settlementMode == 'one_dollar'){
        $(".agencyFee-desc").html("1美金收人民币");
    }
}