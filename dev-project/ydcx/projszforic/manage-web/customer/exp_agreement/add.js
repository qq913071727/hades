layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload','laytpl'], function() {
    var layer = layui.layer, form = layui.form,
        table = layui.table,
        laytpl = layui.laytpl,
        fileupload = layui.fileupload;
    $('.sel-load').selLoad(function () {
        form.render('select');
        generateDocNo();
        selectDeclareType();
    });
    form.val('baseForm',{'signingDate':getNowFormatDate(),'effectDate':getNowFormatDate()});
    app.refreshSubject(function (sub) {
        form.val('baseForm',{'partybName':sub.name,'partybLegalPerson':sub.legalPerson,'partybTel':sub.tel,'partybFax':sub.fax,'partybAddress':sub.address});
    });

    var fileObj = {docId:'temp_'+uuid(), docType:'exp_agreement'};
    fileupload.render({elem:'#agr_evidence',startQuery:false,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_evidence'})}});
    fileupload.render({elem:'#agr_original',startQuery:false,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_original'})}});

    form.on('select(select-declare-type)', function(data){
        selectDeclareType();
    });
    form.on('select(select-settlementMode)',function (data) {
        selectAgencyFeeMode();
    });

    $('.save-btn').click(function () {saveAgreement(false,fileObj);});
    $('.examine-btn').click(function () {saveAgreement(true,fileObj);});
});
function saveAgreement(submitAudit,fileObj){
    if(checkedForm($('.tsf-page-content'))){
        show_loading();
        var agreement = $('#baseForm').serializeJson();
        agreement['submitAudit'] = submitAudit;
        postJSON('scm/expAgreement/add',{"agreement":agreement,"file":fileObj},function (res) {
            parent.reloadList();
            closeThisWin('保存成功');
        });
    }
}
function selectDeclareType() {
    var $declareTypeDesc = $('#declareTypeDesc');
    switch ($('#declareType').val()){
        case 'single':
            $declareTypeDesc.html('报关单生产销售单位为公司');
            break;
        case 'double':
            $declareTypeDesc.html('报关单生产销售单位为客户');
            break;
        default:
            $declareTypeDesc.html('');
            break;
    }
}

function generateDocNo() {
    show_loading();
    $.post(rurl('scm/serialNumber/generateDocNo'),{'type':'exp_agree'},function (res) {
        if(isSuccess(res)){
            $('#docNo').val(res.data);
        }
    });
}

function fullCustomerData(cus) {
    layui.form.val('baseForm',{'partyaLegalPerson':cus.legalPerson,'partyaSocialNo':cus.socialNo,'partyaTel':cus.tel,'partyaFax':cus.fax,'partyaAddress':cus.address});
}

function selectAgencyFeeMode() {
    var settlementMode = $("#settlementMode").val();
    if(settlementMode == 'dec_percent') {
        $(".agencyFee-desc").html("代理费率%");
    } else if (settlementMode == 'one_dollar'){
        $(".agencyFee-desc").html("1美金收人民币");
    }
}