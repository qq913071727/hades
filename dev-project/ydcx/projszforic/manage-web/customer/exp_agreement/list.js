layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
           url:rurl('scm/expAgreement/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'task', width:50, title: '任务',align:'center',toolbar:'#taskTemp', fixed: true}
                ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                ,{field:'docNo', fixed:true,width:120, title: '协议编号',templet: '#agreementTemp'}
                ,{field:'declareTypeDesc', width:80, title: '报关类型'}
                ,{field:'partyaName',width:220, title: '客户名称',templet:'#partyaNameTemp'}
                ,{field:'dateCreated', width:90, title: '创建日期',align:'center'}
                ,{field:'effectDate', width:90, title: '生效日期',align:'center'}
                ,{field:'actualInvalidDate', width:90, title: '实际失效日期'}
                ,{field:'createBy', width:80, title: '创建人'}
                ,{field:'isSignBack',width:100, title: '是否签回正本',templet:'#isSignBackTemp'}
                ,{field:'originalAgr', width:110, title: '正本协议',templet:'#originalAgrTemp'}
                ,{field:'quoteDescribe',title: '报价描述'}
            ]]
        },listQueryDone:function () {
            initFileSize();
        }
    });
});
//协议详情
function showDetail(id){
    openwin(rurl('customer/exp_agreement/detail.html?id='+id),'协议详情',{area: ['0', '0'],shadeClose:true});
}
//原件签回
function originalSignBack (o) {
    var ids = getIds();
    if(ids.length!=1){show_error("请选择一条服务协议签回");return;}
    confirm_inquiry("您是否确定当前出口协议正本已经签回？",function () {
        show_loading();
        $.post(rurl('scm/expAgreement/originSignBack'),{'id':ids[0]},function (res) {if(isSuccess(res)){show_msg("操作成功");reloadList();}});
    })
}