layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table,fileupload = layui.fileupload;
    var id = getUrlParam("id");
    $('#agreementDetail').load(rurl('customer/exp_agreement/detail-temp.html'),function () {
        show_loading();
        $('.sel-load').selLoad(function () {
            form.render('select');
            $.get(rurl('scm/expAgreement/detail'),{'id':id},function (res) {
                if(isSuccessWarnClose(res)){
                    var data = res.data;
                    form.val('baseForm',data);
                    form.val('clauseForm',data);
                    var fileObj = {docId:id, docType:'exp_agreement'};
                    fileupload.render({elem:'#agr_evidence',startQuery:true,edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_evidence'})}});
                    fileupload.render({elem:'#agr_original',startQuery:true,edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_original'})}});
                    disableForm(form,'#agreementDetail');
                }
            });
        });
    });
    var $operationTaskBtn = $('.operation-task-btn'),
    taskArg = {'operation':'exp_agreement_invalid','documentId':id,'documentClass':'ExpAgreement','newStatusCode':''};
    $operationTaskBtn.click(function () {
        var $that = $(this),needMemo = $that.attr('need-memo');
        if(!isEmpty(needMemo)){
            var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
            taskArg['memo'] = $memo.val();
        }else{
            taskArg['memo'] = $('#taskMemo').val();
        }
        taskArg['newStatusCode'] = $that.attr('status-code');
        taskArg['memo'] = $memo.val();
        show_loading();
        $.post(rurl('scm/task/execute'),taskArg,function (res) {
            if(isSuccess(res)){
                parent.reloadList();
                closeThisWin('操作成功');
            }
        });
    });

    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
});