layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload','element'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table,fileupload = layui.fileupload,element=layui.element;
    var id = getUrlParam("id"),customerId='';
    $('#agreementDetail').load(rurl('customer/exp_agreement/detail-temp.html'),function () {
        show_loading();
        $('.sel-load').selLoad(function () {
            form.render('select');
            $.get(rurl('scm/expAgreement/detail'),{'id':id},function (res) {
                if(isSuccessWarnClose(res)){
                    var data = res.data;
                    customerId=data.customerId;
                    form.val('baseForm',data);
                    form.val('clauseForm',data);
                    form.render('checkbox');
                    var fileObj = {docId:id, docType:'exp_agreement'};
                    fileupload.render({elem:'#agr_evidence',startQuery:true,edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_evidence'})}});
                    fileupload.render({elem:'#agr_original',startQuery:true,edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'agr_original'})}});
                    selectDeclareType();
                    disableForm(form,'#agreementDetail');
                }
            });
        });
    });
    var $operationTaskBtn = $('.operation-task-btn'),
    taskArg = {'operation':'exp_agreement_examine','documentId':id,'documentClass':'ExpAgreement','newStatusCode':''};
    $operationTaskBtn.click(function () {
        var $that = $(this),needMemo = $that.attr('need-memo');
        if(!isEmpty(needMemo)){
            var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
            taskArg['memo'] = $memo.val();
        }else{
            taskArg['memo'] = $('#taskMemo').val();
        }
        taskArg['newStatusCode'] = $that.attr('status-code');

        show_loading();
        $.post(rurl('scm/expAgreement/examine'),taskArg,function (res) {
            if(isSuccess(res)){
                parent.reloadList();
                closeThisWin('操作成功');
            }
        });
    });

    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
    autoDetailContent();
    $(window).resize(function() {
        autoDetailContent();
    });
    var isLoad = false;
    element.on('tab(detailTab)', function(){
        var layId = this.getAttribute('lay-id');
        if(layId=="2"&&isLoad==false){
            isLoad = true;
            $('#customerDetail').attr({'src':rurl('customer/agreement/customer-detail.html?customerId='+customerId)});
        }
    });
});
var $detailContent=$('#detail-content'),$tsfPageDetail = $('.tsf-page-detail');
function autoDetailContent(){
    $detailContent.css({'height':($tsfPageDetail.height()-37)+'px'});
}