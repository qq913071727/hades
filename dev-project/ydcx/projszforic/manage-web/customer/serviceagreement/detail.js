layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table,fileupload = layui.fileupload;
    var id = getUrlParam("id");
    $('.sel-load').selLoad(function () {
        form.render('select');
        $.get(rurl('scm/serviceAgreement/info'),{'id':id},function (res) {
            if(isSuccessWarnClose(res)){
                var data = res.data;
                form.val('baseForm',data);
                $("#isEffective").val(data.isEffective);
                var fileObj = {docId:id, docType:'service_agreement'};
                fileupload.render({elem:'#serviceagr_original',startQuery:true,edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'serviceagr_original'})}});
                disableForm(form);
            }
        });
    });
});
