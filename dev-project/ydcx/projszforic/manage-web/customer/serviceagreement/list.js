layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
           url:rurl('scm/serviceAgreement/page')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'docNo', fixed:true,width:120, title: '协议编号',templet: '#agreementTemp'}
                ,{field:'partyaTel', width:120, title: '甲方电话'}
                ,{field:'partyaName',width:220, title: '客户名称',templet:'#partyaNameTemp'}
                ,{field:'isEffective',width:220, title: '是否生效',templet:'#isEffectiveTemp'}
                ,{field:'isSignBack',width:220, title: '是否签回正本',templet:'#isSignBackTemp'}
                ,{field:'originalAgr', width:110, title: '正本协议',templet:'#originalAgrTemp'}
                ,{field:'dateCreated', width:90, title: '创建日期',align:'center'}
                ,{field:'signingDate', width:90, title: '签订日期',align:'center'}
                ,{field:'version', width:80, title: '版本'}
                ,{field:'actualInvalidDate', width:90, title: '实际失效日期'}
                ,{field:'invalidReason', title: '作废原因',align:'center'}
            ]]
        },listQueryDone:function () {
            initFileSize();
        }
    });
});
//协议详情
function showDetail(id){
    openwin(rurl('customer/serviceagreement/detail.html?id='+id),'服务协议详情',{area: ['0', '0'],shadeClose:true});
}
//原件签回
function originalSignBack (o) {
    var ids = getIds();
    if(ids.length!=1){show_error("请选择一条服务协议签回");return;}
    confirm_inquiry("您是否确定当前协议正本已经签回？",function () {
        show_loading();
        $.post(rurl('scm/serviceAgreement/originSignBack'),{'id':ids[0]},function (res) {if(isSuccess(res)){show_msg("操作成功");reloadList();}});
    })
}

/**
 * 作废协议
 */
function invalidServiceAgreement() {
    var ids = getIds();
    if(ids.length!=1){
        show_error("请选择一条服务协议作废");return;
    }
    layer.open({
        title: '三思而后行，请确认您是否要作废协议?',
        area: ['400px', '310px'],
        btnAlign: 'c',
        closeBtn:'1',
        type: 1,
        content: `<div style="padding-left: 10px;">
                    <blockquote class="layui-elem-quote">温馨提示：作废协议后客户将无法正常下单</blockquote>
                    <textarea maxlength="500" placeholder="请输入作废原因" name="invalidReason" id="invalidReason" style="width:350px;height:110px;"></textarea>
                   </div>`,
        btn:['确认','取消'],
        yes: function (indexT, layero) {
            var invalidReason = $('#invalidReason').val();
            if(isEmpty(invalidReason)) {
                layer.msg("请填写作废原因");
                return false;
            }
            $.post(rurl('scm/serviceAgreement/invalidServiceAgreement'),{'id':ids[0],'invalidReason':invalidReason},function (res) {
                if(isSuccess(res)){
                    reloadList();
                    show_success('作废成功');
                    layer.close(indexT);
                }
            });
        },
        no:function(indexT) {
            layer.close(indexT);
            return false;
        }
    });
}