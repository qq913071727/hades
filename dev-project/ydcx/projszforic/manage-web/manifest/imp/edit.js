layui.config({base: '/manifest/js/',version:'202007141822'}).extend({mftUtil: 'mft-util'}).use(['layer','form','table','element','mftUtil'], function() {
    var layer = layui.layer, form = layui.form,table=layui.table,element = layui.element,mftUtil=layui.mftUtil,
        fn = {
            initData:function(){
                $('#voyageNoBtn').attr({'disabled':true}).addClass('layui-btn-disabled');
                show_loading();
                $.get(rurl('declare/manifest/detail'),{'id':getUrlParam('id')},function (res) {
                    if(isSuccess(res)){
                        var data = res.data;
                        form.val('mtfHeadForm',data.manifest);
                        mftUtil.data.mftBills = data.mftBills || [];
                        mftUtil.data.mftGoods = data.mtfGoods || [];
                        mftUtil.data.mftContainers = data.mftContainers || [];
                        mftUtil.init();
                        if(!contain(['temporary','failed','chargeback'],data.manifest.statusId)){
                            $('input').attr({'readonly':true}).addClass('disabled');
                            $('select').attr({'disabled':true}).addClass('disabled');
                            $('.prohibit-btn').attr({'disabled':true}).addClass('disabled').unbind('click');
                            form.render('select');
                        }
                        if(contain(['declared'],data.manifest.statusId)){
                            $('#removeSingle').show().click(function () {
                                show_loading();
                                $.post(rurl('declare/manifest/removeSingle'),{'id':data.manifest.id},function (res) {
                                    if(isSuccess(res)){
                                        prompt_success('发送成功',function () {refPage();});
                                    }
                                });
                            });
                        }
                    }
                });
            }, showCheckMsg:function(data){
                if(!data||data.length==0){return;}
                var mgsList = [];
                $.each(data,function (i,o) {
                    mgsList.push((i+1)+'：'+o.message);
                    if(isNotEmpty(o.field)){
                        verificationTips($(o.field),o.message,-1);
                    }
                });
                prompt_warn(mgsList.join('<br/>'));
            },
            packData:function(){
                var manifest = $('#mtfHeadForm').serializeJson();
                var $billTab = $('#mft-bill-area ').find('.layui-table-main').find('tr');
                var billNos = [];
                for(var i=0;i<mftUtil.data.mftBills.length;i++){
                    var item = mftUtil.data.mftBills[i];
                    if(isEmpty(item.billNo)){
                        var $itemEl = $($billTab[item.listNo-1]).find('td[data-field="billNo"]');
                        $itemEl.addClass('layui-form-danger');
                        tipsMessage('提(运)单号不能为空',$itemEl);
                        return;
                    }
                    if(contain(billNos,item.billNo)){
                        var $itemEl = $($billTab[item.listNo-1]).find('td[data-field="billNo"]');
                        $itemEl.addClass('layui-form-danger');
                        tipsMessage('提(运)单号重复',$itemEl);
                        return;
                    }
                    billNos.push(item.billNo);
                }
                return {'manifest':manifest,'mftBills':mftUtil.data.mftBills,'mtfGoods':mftUtil.data.mftGoods,'mftContainers':mftUtil.data.mftContainers};
            },
            saveMtf:function () {
                var params = fn.packData();
                if(params==null){return null;}
                show_loading();
                postJSON('declare/manifest/save',params,function (res){
                    show_success('暂存成功');
                });
            },sendSingle:function (isAgain) {
                var id = $('#mtfHeadForm #id').val();
                if (isEmpty(id)) {
                    show_msg('请先暂存舱单数据');
                    return;
                }
                var params = fn.packData();
                if(params==null){return null;}
                show_loading();
                postJSON('declare/manifest/saveAndSend',params,function (res){
                    if(res.data&&res.data.length>0){
                        fn.showCheckMsg(res.data);
                    }else{
                        prompt_success('发送成功',function () {refPage();});
                    }
                });
            }
        };
    fn.initData();
    $('#saveMtf').click(function () {
        fn.saveMtf();
    });
    $('#sendSingle').click(function () {
        fn.sendSingle(false);
    });
    $('#copyManifest').click(function () {
        toUrl('manifest/imp/copy.html?id='+getUrlParam('id'));
    });
});
function fullCustomerData(data) {
    $('#customerId').val(data.id);
}