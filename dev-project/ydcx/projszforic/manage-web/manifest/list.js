layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('declare/manifest/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'statusName', width:100, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                    ,{field:'docNo', width:120, title: '系统单号',templet:'#docNoTemp', fixed: true}
                    ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                    ,{field:'typeName', width:90, title: '舱单类型',align:'center'}
                    ,{field:'customsCodeName', width:90, title: '进出境口岸',align:'center'}
                    ,{field:'packNum', width:90, title: '总件数',align:'right'}
                    ,{field:'grossWt', width:90, title: '总毛重',align:'right'}
                    ,{field:'voyageNo', width:120, title: '批次号',align:'center'}
                    ,{field:'billNo', width:140, title: '提运单号'}
                    ,{field:'contaId', width:140, title: '集装箱号'}
                    ,{field:'decDocNo', width:120, title: '报关单系统单号',align:'center'}
                    ,{field:'dateUpdated', width:125, title: '操作日期',align:'center'}
                    ,{field:'loadingDate', width:125, title: '货物装载日期',align:'center'}
                ]]
            },listQueryDone:function () {
                formatTableData();
            }
        });
    });
});
function fullCustomerData(data) {
    $('#customerId').val(data.id);
}

function editManifest(id,typeId) {
    if('MT2401'==typeId){
        openwin('manifest/exp/edit.html?id='.concat(id),'出口预配舱单',{area:['0','0']});
    }else if('MT1401'==typeId){
        openwin('manifest/imp/edit.html?id='.concat(id),'进口原始舱单',{area:['0','0']});
    }else{
        show_error('舱单类型错误');
    }
}