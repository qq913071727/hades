var tipsJson = null;
$(document).ready(function(){
    tipsJson=[
        {"id":"voyageNo","name":"货物运输批次号：请输入大写字母或者数字"},
        {"id":"customsCodeName","name":"进出境口岸海关代码：请在列表中选择"},
        {"id":"carrierCode","name":"承运人代码：由字母、数字或“-”、“/”，“_”、半角空格构成，特殊字符最多出现一次且不能作为开头结尾"},
        {"id":"transAgentCode","name":"运输工具代理企业代码：填写在直属海关备案的运输工具代理企业编码，4位关区号+9位组织机构代码 "},
        {"id":"loadingDate","name":"货物装载时间：时间精确到秒，格式：YYYYMMDDhhmmss"},
        {"id":"loadingLocationCode","name":"装货地代码：填写监管场所海关备案代码或国内公路口岸代码，由大写字母或数字构成"},
        {"id":"arrivalDate","name":"到达卸货地日期：格式：YYYYMMDD"},
        {"id":"customMasterName","name":"传输企业备案关区：填写企业备案关区"},
        {"id":"msgRepName","name":"舱单传输人名称：填写传输企业具体名称或组织机构代码 "},

        {"id":"billNo","name":"提（运）单号：由大写字母、数字或“@”构成，且“@”只能作为开头 "},
        {"id":"conditionCodeName","name":"运输条款：请在列表中选择 "},
        {"id":"paymentType","name":"运费支付方法：请在列表中选择"},
        {"id":"goodsCustomsStat","name":"海关货物通关代码：请在列表中选择"},
        {"id":"packNum","name":"货物总件数：最多可填写8位整数 "},
        {"id":"packType","name":"包装种类：请在列表中选择 "},
        {"id":"cube","name":"货物体积(M3)：最多可填写10位整数，单位立方米 "},
        {"id":"grossWt","name":"货物总毛重(KG)：最多可填写8位整数，3位小数，单位千克"},
        {"id":"goodsValue","name":"货物价值：最多可填写14位整数，2位小数"},
        {"id":"currencyType","name":"金额类型：请在列表中选择 "},
        {"id":"consolidatorId","name":"拼箱人代码：填写组织机构代码，由大写字母或数字构成 "},
        {"id":"routingContryIdText","name":"途径国家代码：请在列表中选择 "},
        {"id":"consigneeId","name":"收货人代码：填写实际收货人组织机构代码；收货人为自然人的填写身份证、护照号等"},
        {"id":"communicationTypeText","name":"收货人通讯方式：请在列表中选择 "},
//	              {"id":"communicationNo","name":"收货具体联系人号码：若填写了具体联系人名称则必填"},
        {"id":"communicationTypeTextj","name":"收货具体联系人通讯方式类别代码：请在列表中选择 "},
        {"id":"consignorId","name":"发货人代码：填写发货人组织机构代码，发货人为自然人的填写身份证、护照号等 "},
        {"id":"communicationTypeTextf","name":"发货人通讯方式类别代码：请在列表中选择 "},
        {"id":"notifyId","name":"通知人代码：填写通知人组织机构代码，发货人为自然人的填写身份证、护照号等"},
        {"id":"notifyName","name":"通知人名称：若有通知人则必填，否则不可填写 "},
        {"id":"notifyAddr","name":"通知人地址（街道,邮箱：若填报了通知人名称则必填 "},
//	              {"id":"communicationNo","name":"通知人联系号码：若填报了通知人名称则必填"},
        {"id":"communicationTypeTextt","name":"通知人通讯方式类别代码：请在列表中选择 "},
//	              {"id":"communicationNo","name":"危险品联系人联系号码：若填写了危险品联系人姓名则必填 "},
        {"id":"communicationTypeTextw","name":"危险品联系人通讯方式类别代码：请在列表中选择 "},

        {"id":"contaId","name":"集装箱(器)编号 ：由大写字母或数字构成，只允许出现一次“-”且不能作为开头和结尾 "},

        {"id":"goodsPackNum","name":"商品项件数：最多可填写8位整数"},
        {"id":"goodsPackType","name":"包装种类：请在列表中选择 "},
        {"id":"goodsGrossWt","name":"商品项毛重(KG)：最多可填写8位整数，3位小数，单位千克"},
        {"id":"goodsBriefDesc","name":"商品项简要描述：填写足以鉴别货物性质的描述信息"},
        {"id":"HSCode","name":"HS编码：至少录入三位数字按Enter键调出列表"},
        {"id":"undgNoName","name":"危险品编号：请录入2位数字调出参数表，或直接录入4位数字"}

    ];
});
//光标进入全选
$('body').on('focus','input,textarea',function(e){
    $(this).select();
    if(document.getElementById("tipsMessagetext")){
        var tipText="";
        for(var o in tipsJson){
            if(tipsJson[o].id==this.id){
                var tipText=tipsJson[o].name;
                break;
            }
        }
        document.getElementById("tipsMessagetext").innerHTML=tipText;
    }
})
/**
 * body里的标签不能拖拽
 */
$('body').delegate('input,textarea','dragstart',"event",function(event){
    event.preventDefault();
    event.stopPropagation();
})