



/**
 * 消息框
 * @param msg 需要提示的消息
 * @param time 消息框出现多久自动关闭
 * @returns
 */
function showLayerMsg(msg, time){
    layer.msg(msg, {
        icon: 1,//成功图标
        time: time || 2000 //2秒关闭（如果不配置，默认是3秒）
    });
}
/**
 * 消息框
 * @author 马青松 2018/5/18
 * @param msg 成功的消息，需要单击确定从能关闭
 * @returns
 */
function showLayerSucessMsg(msg){
    layer.confirm(msg,{icon: 1, title:'提示',//失败图标
        btn : [ '确定' ]//按钮
    },function(index) {
        layer.close(index);
    });
}
/**
 * 消息框
 * @param msg 需要警告的消息
 * @param time 消息框出现多久自动关闭
 * @returns
 */
//function showLayerWarnMsg(msg, time){
//	layer.msg(msg, {
//	  icon: 7,//警告图标
//	  time: time || 2000 //2秒关闭（如果不配置，默认是3秒）
//	});
//}
function showLayerWarnMsg(msg){
    layer.confirm(msg,{icon: 7, title:'提示',//警告图标
        btn : [ '确定' ]//按钮
    },function(index) {
        layer.close(index);
    });

}
/**
 *
 * @param msg 需要显示的tips信息
 * @param elem 定位 tips 出现的位置 dom 节点  如：$("#xxx");
 * @param time tips 出现多久自动关闭
 * @returns
 */
function showLayerTip(msg, elem, time){
    layer.tips(msg, elem,{ // 所需提醒的元素ID
        tips : [ 3, '#DC143C' ], // 在元素的下面出现 1上面，2右边 3下面
        tipsMore : true, // 允许同时存在多个
        time : time || 2000
        // tips自动关闭时间，毫秒
    });
}
/**
 * 消息框
 * @author 马青松 2018/5/18
 * @param msg 失败的消息，需要单击确定从能关闭
 * @returns
 */
function showLayerFailMsg(msg){
    layer.confirm(msg,{icon: 2, title:'提示',//失败图标
        btn : [ '确定' ]//按钮
    },function(index) {
        layer.close(index);
    });
}
/**
 * 弹出框：需要手动关闭
 * @param msg  弹出框的内容
 * @param num 弹出框的内容
 * @returns
 */
function showLayerAlert(msg,num){
    layer.alert(
        msg,
        {icon: num||2}
    );
}
//ajax 返回的错误提示消息
function rebackInfo(json,info){
    var errorcode = json.errorCode;
    if(errorcode){
        showLayerAlert(info+":<br>错误编码:"+errorcode+";<br>"+json.errors.join("<br>"));
    }else{
        showLayerAlert(info+":<br>" +json.errors.join("<br>"));
    }
}


/**
 * 添加jQuery 校验方法
 *
 * @param value 校验字段的值
 * @param element 校验字段dom
 * @param param 需要依赖字段的Id
 * @returns
 */
jQuery.validator.addMethod("dependon", function(value, element, param) {
    var dependEle = $('#' + param).val();
    if(dependEle) return !!value;
    return true;
}, "此项不能为空");

/**
 * 添加jQuery 校验方法
 *
 * @param value 校验字段的值
 * @param element 校验字段dom
 * @param param 正则表达式
 * @returns
 */
jQuery.validator.addMethod("pattern", function( value, element, param ) {
    if(!value){
        return true;
    }else{
        return  new RegExp(param).test(value);
    }

}, "无法匹配");


/**
 *
 * @param formId 需要校验表单的id
 * @param rules 校验表单的规则
 * @param messages 校验表单的提示信息
 * @returns
 */
function ValidateFunc(formId, rules, messages){
    var validateForm = $("#" + formId).validate({ //表单id
        focusInvalid: false, //提交表单后，未通过验证的表单是否获得焦点
        onkeyup: false,		//是否在敲击键盘时验证
        onfocusout: function(element) { if(element.id!="dischargeStartTime"
            &&element.id!="distStartTime"&&element.id!="moveStart"
            &&element.id!="moveEnd"&&element.id!="arrDischargePlaceDate"&&
            element.id!="consLoadingDate"
        ){$(element).valid();}
        }, //在获取焦点时验证
        onclick: function(element) { $(element).valid(); },   //在鼠标点击时验证
        onsubmit: true, //是否提交表单时验证
        ignore: true, //如果要校验下拉框请配置此项
        submitHandler: function(form){   //表单提交句柄,为一回调函数，带一个参数：form
            form.submit();   //提交表单
        },
        rules:rules,
        errorPlacement : function(error, element) {
            // 提示消息回调
            if (element.is(':radio') || element.is(':checkbox')) {
                var objValList = document.getElementsByName(element[0].name);// 为了显示在同一单选/复选框的最后面
                showLayerTip(error[0].innerText, objValList[objValList.length - 1], 3000);
            } else if (element[0].nodeName == "SELECT") { // 伪选择器不支持SELECT,下拉框需要单独定位
                var selectTipsObj = $(element[0]).next('div');
                showLayerTip(error[0].innerText, selectTipsObj, 3000);
            } else {
                showLayerTip(error[0].innerText, element[0].parentElement, 3000);
            }
        },
        messages:messages
    });
    return validateForm;
}

/**
 * 生成序列
 *
 * @param value
 * @param row
 * @param index
 * @returns
 */
function indexFormatter(value, row, index) {
    return index + 1;
}
