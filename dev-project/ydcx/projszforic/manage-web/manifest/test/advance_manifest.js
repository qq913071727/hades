
var swProxyBasePath='../../../../';
var urlParams = getUrlVars();
var msgType, pageType;
msgType = urlParams.msgType;
pageType = urlParams.pageType;

/*//暂存url
const saveHeadUrl = BasePath+"/advanceManifest/save?msgType="+ msgType ;
//申报url
const declaraHeadUrl = BasePath+"/advanceManifest/declare?msgType="+ msgType ;
//删除
const deleteHeadUrl = BasePath+"/advanceManifest/delete?msgType="+ msgType ;
//系统分配批次号
const systemGiveUrl=BasePath+"/manifest/voyageNo?msgType="+ msgType ;
//查询服务
const manifestQueryUrl=BasePath+"/advanceManifest/query?msgType="+ msgType ;*/

//校验开始
/**
 * 舱单表头非空，长度校验
 */

var mftHeadInfoFormValidateRules = {
    voyageNo : {
        /*required : true,*/
        maxlength : 35,
        pattern : /^[A-Z0-9]+$/
    }, //货物运输批次号
    /*customsCodeName : {
        required : pageType == "main"

    }, //进出境口岸海关代码
*/		carrierCode : {
        minlength : 2,
        maxlength : 18,
        pattern:/^[A-Za-z0-9]+[_/ -]{0,1}[A-Za-z0-9]+$/
    },//承运人代码
    transAgentCode : {
        maxlength : 35,
        pattern:/^[A-Z0-9]+[-]{0,1}[A-Z0-9]+$|^$/
    }, //运输工具代理企业代码
    //cusCodeText:{required:msgType=="MT2101" && pageType=="main"}, //运输工具离境地 海关代码
    loadingDate : {
        pattern:/^((((1[6-9]|[2-9]\d)\d{2})(0[13578]|1[02])(0[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})(0[13456789]|1[012])(0[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})02(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))0229))((20|21|22|23|[0-1]\d)[0-5]\d([0-5]\d\d{3}|086|[0-5]\d))$/
    }, //货物装载时间
    loadingLocationCode : {
        /*required : pageType == "main",*/
        maxlength : 17,
        pattern : /^[A-Z0-9]{1,17}$/
    }, //卸货地代码
    /*customMasterName : {
        required :true
    },//传输企业备案关区
*/		/*unitCode : {
			required :pageType == "change"
		},//企业代码
*/
//		msgRepName : {
//			/*required:true, */
//			maxlength:35,
//			pattern:/^([@]{0,1}[A-Za-z0-9]{0,35}[A-Za-z0-9_/ -]{0,35}[A-Za-z0-9]{0,35})|([\u4e00-\u9fa5]+[A-Za-z0-9]{0,35})$/
////			pattern:/^([@]{0,1}[A-Za-z0-9]+[A-Za-z0-9_/ -]+[A-Za-z0-9]+)|([\u4e00-\u9fa5]+[A-Za-z0-9]+)|([\u4e00-\u9fa5]+)$/
//		}, //舱单传输人名称



}
var mftHeadInfoFormValidateMsgs = {
    voyageNo : {
        /*required : "此项为必填项",*/
        maxlength : "最大长度为35位",
        pattern : "由大写字母或数字构成"
    },
    /*customsCodeName : {
        required : "此项为必填项"


    },*/
    carrierCode : {
        minlength : "最小长度为2位，填写承运企业的海关备案号或统一社会信用代码或该企业的国际通用代码",
        maxlength : "最大长度为18位",
        pattern : "由字母、数字或“-”、“/”，“_”、半角空格构成，特殊字符最多出现一次且不能作为开头结尾"
    },
    transAgentCode : {

        maxlength : "最大长度为35位",
        pattern : '填写企业在海关备案的13位数据传输人识别号或18位统一社会信用代码'
    },
    loadingDate : {
        pattern : "时间格式不匹配"
    },
    //cusCodeText:{required : "此项为必填项"},
    loadingLocationCode : {
        /*required : "此项为必填项",*/
        maxlength : "最大长度为17位",
        pattern:'填写监管场所海关备案代码或国内公路口岸代码，由大写字母或数字构成'
    }
    //carrierId:{required : "此项为必填项",maxlength : "最大长度为17位",pattern:'由字母、数字或“-”、“/”，“_”、“ ”构成，特殊字符最多出现一次且不能作为开头结尾'},

    /*customMasterName : {
        required : "此项为必填项"
    },*/
    /*unitCode : {
        required :"此项为必填项"
    },//企业代码
*/
//		msgRepName : {
//			/*required : "此项为必填项",*/
//			maxlength:"最大长度为35位",
//			pattern:'填写传输企业具体名称或组织机构代码或统一社会信用代码'
//		}
//			ciqOrgCodeText:{required:"此项为必填项"},//
//			ciqDeclCopCode:{required:"此项为必填项"},//

}
/**
 * 提运单非空，长度校验
 */

var billInfoFormValidateRules = {
    billNo : {
        /*required : true,*/
        minlength : 3,
        maxlength : 35,
        pattern : /^[@]{0,1}([A-Z0-9]+[_*/]{0,1}[A-Z0-9]+){0,1}([A-Z0-9]+){0,1}$/
    },//总提运单号

    /*chgCodeText : {
        required : pageType=='change'
    },//变更原因代码
*/	/*paymentType : {
		required : pageType=='main'
	},//运费支付方法
*/	/*goodsCustomsStat : {
		required : pageType=='main'
	},//海关货物通关代码
*/	/*transLocationId : {
		required : $("#goodsCustomsStatCode").val=='RD9'||$("#goodsCustomsStatCode").val=='RD16'
	},//跨境指运地
*/	packNum : {
        /*required : pageType=='main',*/
        intLength:8
    },//货物总件数
    /*packType : {
        required : pageType=='main'
    },//包装种类
*/	cube : {

        intLength : 10
    },//货物体积
    grossWt : {
        /*required : pageType=='main'||pageType=='change',*/
        decimalLength:[8,3]
    }, // 货物总毛重(KG)

    goodsValue : {
        /*required : pageType=='main',*/
        decimalLength:[14,2]
    },//货物价值
    /*currencyType : {
        required : pageType=='main'
    },//金额类型
*/	consolidatorId : {

        pattern:/^[A-Z0-9]+$/
    },//拆箱人代码
    consigneeId:{cnLength : 25},//收货人代码
    consigneeName : {
        /*required : pageType=='main',*/
        cnLength : 210
    },//收货人名称

//	consigneeContactName : {
//		cnLength : 105
//	},//收货人具体联系人名称
    //consignorId:{cnLength : 17},//发货人代码
    consignorName : {
        /*required : pageType == "main",*/
        cnLength : 210
    },//发货人名称
//undgContactName:{cnLength : 210},//危险品联系人姓名
    notifyId:{cnLength : 18},//通知人代码
    notifyName:{dependon: 'notifyAddr',cnLength : 210},//通知人名称
    notifyAddr:{dependon: 'notifyName',cnLength : 210},//通知人地址（街道,邮箱）

}
var billInfoFormValidateMsgs = {
    billNo : {
        /*required : "此项为必填项",*/
        minlength : "最小长度为3位",
        maxlength : "最大长度为35位",
        pattern : '由大写字母、数字或“@”构成，且“@”只能作为开头'
    },
    /*chgCodeText : {
        required : "此项为必填项"
    },
    paymentType : {
        required : "此项为必填项"
    },
    goodsCustomsStat : {
        required : "此项为必填项"
    },
    transLocationId : {
        required : "此项为必填项"
    },*/
    packNum : {
        /*required : "此项为必填项",*/
        intLength:'最长8位整数'
    },
    /*packType : {
        required : "此项为必填项"
    },*/
    cube : {

        intLength : "输入格式错误，整数最多10位，小数最多0位"
    },
    grossWt : {
        /*required : "此项为必填项",*/
        decimalLength:'最长8位整数位，并可精确到小数点后3位'
    },
    goodsValue : {
        /*required : "此项为必填项",*/
        decimalLength:'整数部分最大14位，小数部分最大2位'
    },
    /*currencyType : {
        required : "此项为必填项"
    },*/
    consolidatorId : {
        pattern:"填写组织机构代码，由大写字母或数字构成"
    },
    consigneeId:{cnLength:"填写实际收货人组织机构代码；收货人为自然人的填写身份证、护照号等"},//收货人代码

    /*consigneeName : {
        required : "此项为必填项"

    },*///收货人名称
    //consigneeContactName:{required:"此项为必填项"},//具体联系人名称
    //consignorId:{required:"此项为必填项"},//发货人代码
    /*consignorName : {
        required : "此项为必填项"

    },*///发货人名称
    //undgContactName:{required:"此项为必填项"},//危险品联系人姓名
    notifyId:{cnLength : "最大长度为18位,填写通知人组织机构代码或统一社会信用代码或海关备案代码，自然人填写身份证、护照号等"},
    notifyName:{dependon: "通知人地址不为空时必填"},//通知人名称
    notifyAddr:{dependon: "通知人名称不为空时必填"},//通知人地址（街道,邮箱）
}

/**
 * 集装箱非空，长度校验
 */
var contaInfoFormValidateRules = {
    contaId : {
        /*required : pageType=='main'||pageType=="delete",*/
        maxlength : 17,
        pattern : /^[A-Z0-9]+[-]{0,1}[A-Z0-9]+$/
    },
// contaSizeTypeText:{required:pageType=="main"},
//contaSuppIdText:{required:pageType=="other"},
// contaLoadedTypeText:{required:pageType=="main"}
}
var contaInfoFormValidateMsgs = {
    contaId : {
        /*required : "此项为必填项",*/
        maxlength : "最大长度为17",
        pattern : '最少添加2位，由大写字母或数字构成，只允许出现一次“-”且不能作为开头和结尾'
    },
// contaSizeTypeText:{required:"此项为必填项"},
// contaSuppIdText:{required:"此项为必填项"},
// contaLoadedTypeText:{required:"此项为必填项"}
}
/**
 * 商品非空，长度校验
 */
var commodityinfoFormValidateRules = {
//	goodsSeqNo : {
//		required : true,
//		maxlength : 5,
//		pattern : /^[A-Za-z0-9]+$/
//	},//商品项序号，灰调，自增
    /*goodsPackType : {
        required : pageType=='main'

    },*///货物包装种类
    goodsPackNum : {
        /*required : pageType=='main',*/
        intLength : 8
    },//商品项数件
    goodsBriefDesc : {
        /*required : pageType=='main',*/
        cnLength : 768
    },//商品项简要描述
    goodsDetailDesc : {
        /*required : pageType=='main',*/
        cnLength : 1536
    },//补充信息
    goodsGrossWt : {
        /*required : pageType=='main',*/
        decimalLength : [ 8, 3 ]
    },//货物毛重(kg)
//	undgNo : {
//		cnLength : 4
//	},//危险品编号

}
var commodityinfoFormValidateMsgs = {
//	goodsSeqNo : {
//		required : "此项为必填项",
//		maxlength : "最大长度为5",
//		pattern : "只能为字母和数字"
//	},//商品项序号
    /*goodsPackType : {
        required : "此项为必填项"

    },//货物包装种类
    goodsPackNum : {
        required : "此项为必填项"
    },//商品项数件
    goodsBriefDesc : {
        required : "此项为必填项",cnLength:"最大长度为768"
    },*///商品项简要描述
    goodsDetailDesc:{
        /*required : "此项为必填项",*/
        cnLength:"最大长度为1536"},//商品项描述补充信息
    goodsGrossWt : {
        /*required : "此项为必填项",*/
        decimalLength : '整数部分最大8位，小数最大3位'
    },//货物毛重(kg)
//	undgNo : {
//		cnLength : "最大长度为4"
//	},//危险品编号

}
//校验结束

//1211 无纸化变更开关
var changeInformationSwitch;
$(document).ready(function(){
//1.初始化页面
    initPage();
    //2.初始参数下拉框
    initParamSelect();
    //3.绑定事件

    /**
     * 功能： 绑定航班航次编号和运输工具代码回车事件，在运输工具输入框上敲击回车键，
     * 		如果数据库存在数据就反填到表提信息
     */
    //1211 无纸化变更开关
    changeInformationSwitch = switchCheck();
    if(changeInformationSwitch){
        $("#reason").addClass("non-empty");
        $("#contact").addClass("non-empty");
        $("#phone").addClass("non-empty");
    }
    $('#voyageNo').keydown(function(event){
        var code = getKeyCode(event);//兼容IE 火狐 及其他浏览器的公共方法
//	    按Enter键的键值为13
        if (code == 13){
            var voyageNo=$("#voyageNo").val();
            if(voyageNo.length>0&&voyageNo.length<36 ){
                /**
                 * 如果录入框有值，取消系统分配批次号回车事件
                 */
                $('#sysRand').keydown(function(e){
                    if(e.keyCode==13){
                        return false;
                    }
                });
                $("#org_head_save").attr("disabled", false);//激活暂存按钮
                $("#mft_copy").attr("disabled", false);//激活复制按钮
            }
        }else{
        }

    });

    $("#voyageNo").blur(function(){
        var voyageNo=$("#voyageNo").val();

        if(voyageNo.length>0 ){
            if(voyageNo.length>35){
                return;
            }
            queryHead();
            $("#sysRand").attr("disabled",true);//系统分配批次号
            $("#org_head_save").attr("disabled", false);//暂存
        }else{
            $("#customsCodeName").focus();//没查询到数据光标定位到下个录入框
            return;
        }
    });

    $("#mftfresh").click(function(){
        var voyageNo=$("#voyageNo").val();

        if(voyageNo.length>0 ){
            if(voyageNo.length>35){
                return;
            }
            queryHead();
            $("#sysRand").attr("disabled",true);//系统分配批次号
            $("#org_head_save").attr("disabled", false);//暂存
        }else{
            return;
        }


    });
    /**
     * 查询中蒙数据交换项
     */
    $("#customsCodeName").blur(function(){
        if(pageType=="delete"){
            return ;
        }
        //先清空中蒙数据交换项
        $("#dataExchangeIdText").val("");
        $("#dataExchangeId").val("");
        var customsCode=$("#customsCode").val();
        var queryData = {"serviceName":"eport.superpass.roadmft.LoadListDataExangeSwitchQueryService",
            "queryLoadListDataExangeRequest":{
                "customsCode":customsCode,
                "ieflag":"E",
                "switchType":"MN_DATA_EXCH_CHECK"
            }
        }
        $.ajax({
            type : "POST",
            url : BasePath+"/advanceManifest/getZMOnFlag",
            data : JSON.stringify(queryData),
            contentType : "application/json;charset=utf-8",
            dataType : "json",
            success : function(json) {
                if(isNotEmpty(json.querySwitchResponse.onFlag)){
                    if("1"==json.querySwitchResponse.onFlag){
                        $("#dataExchangeIdText").val("参与中蒙数据交换");
                        $("#dataExchangeId").val("MN");
                    }

                }else{
                    //不参与中蒙数据交换还是不显示
                }

            }
        });
    });
    //excel导入提运单
    $("#excelId").click(function(data){
        ExcelInfoFunc();//excel导入功能
    });
    //excel导入商品项
    $("#excelgoodsId").click(function(data){
        ExcelgoodsfoFunc();//excel导入功能
    });
    //excel导入集装箱
    $("#excelcontasId").click(function(data){
        ExcelcontasfoFunc();//excel导入功能
    });
    /**
     * 预配舱单 表头信息 暂存
     */
    $("#org_head_save").click(function() {
        saveMftHeadFunc();
    });
    /**
     * 预配舱单新增事件，清空前先提示是否清空数据
     * */
    $("#mft_main_add").click(function(){
        resetData();
    });



    /**
     *  预配舱单 表头  删除
     * @returns
     */
    $("#delete_mft_head").click(function (){
        //调用删除删除舱单的申请
        delhead();
    });
    /**
     * 预配舱单 提运单信息 暂存
     */
    $("#bill_info_save").click(function(){
        saveBillInfoFunc();
    });
    /**
     * 预配舱单 提运单信息 删除
     * @returns
     */
    $("#delete_mft_bill").click(function (){
        //调用删除提运单信息方法
        delBill();
    });

    /**
     * 预配舱单 提运单信息  刷新
     *
     */
    $("#bill_info_refresh").click(function (){
        clearBillInfo();
        $("#billNo").focus();
    });

    /**
     *  集装箱信息  保存
     */
    $("#mft_container_save").click(function(){
        containerSave();
    });

    /**
     *  集装箱信息  新增
     */
    $("#container_add").click(function(){
        containerSave();
    });

    /**集装箱信息保存**/
    function containerSave(){
        var billNo = $("#billNo").val();
        if(!isNotEmpty(billNo)){
            showLayerWarnMsg("提(运)单号不可为空");
            return;
        }else{
            //非空校验和正则校验通过才能新增
            var contaId=$("#contaId").val();
            if(contaId.length===0){
                showLayerWarnMsg("集装箱（器）编号不可为空");
                return;
            }else{
                if($("#mft_bill_container_form").valid()){
                    addContainerFunc();
                    refreshContainerInfo();
                }

            }

        }
    }


    /**
     *预配舱单  集装箱 删除
     */
    $("#mft_container_delete").click(function(){
        //删除集装箱信息的方法
        delContainer();
    });
    /**
     * 商品项信息 删除
     */
    $("#mft_goods_delete").click(function(){
        //调用删除商品信息的方法
        delGoods();

    });
    /**
     * 商品信息  保存
     */
    $("#commodity_info_save").click(function(){
        commoditySave();
    });

    /**商品项表体新增**/
    $("#commodity_add").click(function(){
        commoditySave();
    });

    /**商品项新增方法**/
    function commoditySave(){
        var billNo = $("#billNo").val();
//		var customsStatCode=$("#goodsCustomsStatCode").val();
//		if(customsStatCode==="RD09"||customsStatCode==="RD16"){
//			if($("#customsTransit").val().length==0){
//				showLayerWarnMsg("转关类型必填");
//				return;
//			}
//		}
        if(!isNotEmpty(billNo)){
            showLayerWarnMsg("提(运)单号不可为空");
            return;
        }else{
            if(goodsInfoNotNull()){
                if($("#mft_goods_info_form").valid()){
                    addGoodsFunc();
                }
            }

        }
    }
    //商品项新增enter新增
    $("#goodsDetailDesc").keydown(function(e){
        if(getKey(e)=="13"){
            var billNo = $("#billNo").val();
//			var customsStatCode = $("#customsStatCode").val();
//	    	if(customsStatCode==="RD09"||customsStatCode==="RD16"){
//				if($("#customsTransit").val().length==0){
//					showLayerWarnMsg("转关类型必填");
//					return;
//				}
//			}
            if(!isNotEmpty(billNo)){
                showLayerWarnMsg("提(运)单号不可为空");
                return;
            }else{
                if(goodsInfoNotNull()){
                    if($("#mft_goods_info_form").valid()){
                        addGoodsFunc();
                        setTimeout(function () {$("#goodsPackNum").focus();}, 100);
                    }
                }
            }

        }
    });


    /*商品项新增时非空判断，true：非空，false:空*/
    function goodsInfoNotNull(){
        var billInfos =$("#mft_goods_info_form").serializeJson();
        if(pageType==="main"){
            if(!isNotEmpty(billInfos.goodsPackNum)){
                showLayerWarnMsg("商品项件数不可为空！");
                return false;
            }
            if(!isNotEmpty(billInfos.goodsPackType)){
                showLayerWarnMsg("包装种类不可为空！");
                return false;
            }
            if(!isNotEmpty(billInfos.goodsGrossWt)){
                showLayerWarnMsg("商品项毛重(KG)不可为空！");
                return false;
            }
            if(!isNotEmpty(billInfos.goodsBriefDesc)){
                showLayerWarnMsg("货物简要描述不可为空！");
                return false;
            }
        }else if(pageType==="change"){
            if(!isNotEmpty(billInfos.goodsPackNum)){
                showLayerWarnMsg("商品项件数不可为空！");
                return false;
            }
            if(!isNotEmpty(billInfos.goodsPackType)){
                showLayerWarnMsg("包装种类不可为空！");
                return false;
            }
        }


        return true;
    }

    /**
     * 在不关闭弹出框的前提下 点击此按钮刷新弹窗，继续开始新增
     */
    $("#bill_info_refresh").click(function(){
        //调用提运单新增时的
        clearBillInfo();
    });


    /**
     * 绑定舱单申报按钮点击事件
     */
    $("#mft_declare").click(function(){
        decMainData();
    });

    /**
     * @Method description 原始舱单 复制
     * @author 习成 2019/6/20
     * @description 原始舱单 复制按钮单击事件
     */
    $("#mft_copy").click(function(){
        decCopyData();
    });

    /**
     * 绑定向质检申报按钮点击事件
     *//*
	msgType == "MT1101" && $("#mft_ciq_declare").click(function(){
		decMainData({isCiq:true});
	});*/

    //4.初始化 table
    /**
     * 提运单列表定义
     */



    //var dataJson = './billOff.json';
    $('#billOfLadingInformationTalbe').bootstrapTable('destroy');
    $('#billOfLadingInformationTalbe').bootstrapTable({
        // url:dataJson,
        escape:true,
        cache : false,
        height : 150,
        striped : true,
        pagination : false,
        pageSize : 5,
        pageNumber : 1,
        pageList : [ 5, 10, 15, 20, 30, 50, 100, 200, 500],
        search : false,
        showRefresh : false,
        showToggle : false,
        showExport : false,
        clickToSelect : false,
        singleSelect : false,
        checkboxHeader : true,
        undefinedText : "",
        ajaxOptions:{async:false},
        exportTypes : [ 'excel' ],
        queryParams : 'queryParams',
        toolbar : '#tableToolbar',
        iconSize : 'outline',
        icons : {
            refresh : 'glyphicon-repeat',
            toggle : 'glyphicon-list-alt',
            columns : 'glyphicon-list'
        },
        onClickRow : function(row,$element) {
            $('#billOfLadingInformationTalbe .success').removeClass('success');//去除之前选中的行的，选中样式
            $element.addClass('success');//添加当前选中的 success样式用于区别
            updateIndex  = $element.data('index');
            openBillFormFunc(updateIndex);
            billSaveFlag = true;
            //console.log(updateIndex);
        },
        onPageChange : function(size, number) {
        },
        formatNoMatches : function() {
            return '无匹配数据';
        }
    });





    /**
     * 集装箱列表信息定义
     */
    // var contaJson = './contaUrl.json';
    $('#containerInformationTalbe').bootstrapTable('destroy');
    $('#containerInformationTalbe').bootstrapTable({
        //  url:contaJson,
        escape:true,
        cache : false,
        height : 150,
        striped : true,
        pagination : false,
        pageSize : 5,
        pageNumber : 1,
        pageList : [ 5, 10, 15, 20, 30, 50, 100, 200, 500],
        search : false,
        showRefresh : false,
        showToggle : false,
        showExport : false,
        undefinedText : "",
        clickToSelect : true,
        singleSelect : false,
        checkboxHeader : true,
        ajaxOptions:{async:false},
        exportTypes : [ 'excel' ],
        queryParams : 'queryParams',
        toolbar : '#tableToolbar',
        iconSize : 'outline',
        icons : {
            refresh : 'glyphicon-repeat',
            toggle : 'glyphicon-list-alt',
            columns : 'glyphicon-list'
        },
        onPageChange : function(size, number) {
        },
        onClickRow : function(row, $element){
            $('#containerInformationTalbe .success').removeClass('success');//去除之前选中的行的，选中样式
            $element.addClass('success');//添加当前选中的 success样式用于区别
            updateContainerIndex = $element.data('index');
            lookContasInfo(updateContainerIndex,$element);
            containerFlag = true;
        },
        formatNoMatches : function() {
            return '无匹配数据';
        }
    });




    /**
     * 商品信息列表定义
     */
    //  var goodsJson = './queryGoodesUrl.json';
    $('#commodityInformationTalbe').bootstrapTable('destroy');
    $('#commodityInformationTalbe').bootstrapTable({
        //  url:goodsJson,
        escape:true,
        cache : false,
        height : 150,
        striped : true,
        pagination : false,
        pageSize : 5,
        pageNumber : 1,
        pageList : [ 5, 10, 15, 20, 30, 50, 100, 200, 500],
        search : false,
        showRefresh : false,
        showToggle : false,
        showExport : false,
        undefinedText : "",
        clickToSelect : true,
        singleSelect : false,
        checkboxHeader : true,
        ajaxOptions:{async:false},
        exportTypes : [ 'excel' ],
        queryParams : 'queryParams',
        toolbar : '#tableToolbar',
        iconSize : 'outline',
        icons : {
            refresh : 'glyphicon-repeat',
            toggle : 'glyphicon-list-alt',
            columns : 'glyphicon-list'
        },
        onPageChange : function(size, number) {
        },
        onClickRow : function(row, $element){
            $('#commodityInformationTalbe .success').removeClass('success');//去除之前选中的行的，选中样式
            $element.addClass('success');//添加当前选中的 success样式用于区别
            updateGoodsIndex = $element.data('index');
            lookGoodsInfo(updateGoodsIndex,$element);
            goodsInfoFlag = true;
        },
        formatNoMatches : function() {
            return '无匹配数据';
        }
    });
    /**
     * @Method description 预配舱单 收货人列表定义
     * @author 马青松 2018/5/17
     * @description bootStrap 收货人列表定义
     */
    $('#consigneeInfoTalbe').bootstrapTable('destroy');
    $('#consigneeInfoTalbe').bootstrapTable({
        //  url:contaJson,
        escape:true,
        cache : false,
        height : 180,
        striped : true,
        pagination : false,
        pageSize : 5,
        pageNumber : 1,
        pageList : [ 5, 10, 15, 20, 30, 50, 100, 200, 500],
        search : false,
        showRefresh : false,
        showToggle : false,
        showExport : false,
        undefinedText : "",
        clickToSelect : false,
        singleSelect : false,
        checkboxHeader : true,
        ajaxOptions:{async:false},
        exportTypes : [ 'excel' ],
        queryParams : 'queryParams',
        toolbar : '#tableToolbar',
        iconSize : 'outline',
        icons : {
            refresh : 'glyphicon-repeat',
            toggle : 'glyphicon-list-alt',
            columns : 'glyphicon-list'
        },
        onPageChange : function(size, number) {
        },
        onClickRow : function(row, $element){
            updateConsigneeIndex = $element.data('index');
            openConsigneeInfo(updateConsigneeIndex);
            consigneeFlag = true;
        },
        formatNoMatches : function() {
            return '无匹配数据';
        }
    });
    /**
     * @Method description 预配舱单 收货具体联系人列表定义
     * @author 马青松 2018/5/17
     * @description bootStrap 收货具体联系人列表定义
     */
    $('#consigneeContactInfoTalbe').bootstrapTable('destroy');
    $('#consigneeContactInfoTalbe').bootstrapTable({
        //  url:contaJson,
        escape:true,
        cache : false,
        height : 180,
        striped : true,
        pagination : false,
        pageSize : 5,
        pageNumber : 1,
        pageList : [ 5, 10, 15, 20, 30, 50, 100, 200, 500],
        search : false,
        showRefresh : false,
        showToggle : false,
        showExport : false,
        undefinedText : "",
        clickToSelect : false,
        singleSelect : false,
        checkboxHeader : true,
        ajaxOptions:{async:false},
        exportTypes : [ 'excel' ],
        queryParams : 'queryParams',
        toolbar : '#tableToolbar',
        iconSize : 'outline',
        icons : {
            refresh : 'glyphicon-repeat',
            toggle : 'glyphicon-list-alt',
            columns : 'glyphicon-list'
        },
        onPageChange : function(size, number) {
        },
        onClickRow : function(row, $element){
            updateconsigneeContactIndex = $element.data('index');
            openConsigneeContactIndexInfo(updateconsigneeContactIndex);
            updateconsigneeContactIndexFlag = true;
        },
        formatNoMatches : function() {
            return '无匹配数据';
        }
    });
    /**
     * @Method description 预配舱单 发货人列表定义
     * @author 马青松 2018/5/17
     * @description bootStrap 发货人列表定义
     */
    $('#consignoInfoTalbe').bootstrapTable('destroy');
    $('#consignoInfoTalbe').bootstrapTable({
        escape:true,
        cache : false,
        height : 180,
        striped : true,
        pagination : false,
        pageSize : 5,
        pageNumber : 1,
        pageList : [ 5, 10, 15, 20, 30, 50, 100, 200, 500],
        search : false,
        showRefresh : false,
        showToggle : false,
        showExport : false,
        undefinedText : "",
        clickToSelect : false,
        singleSelect : false,
        checkboxHeader : true,
        ajaxOptions:{async:false},
        exportTypes : [ 'excel' ],
        queryParams : 'queryParams',
        toolbar : '#tableToolbar',
        iconSize : 'outline',
        icons : {
            refresh : 'glyphicon-repeat',
            toggle : 'glyphicon-list-alt',
            columns : 'glyphicon-list'
        },
        onPageChange : function(size, number) {
        },
        onClickRow : function(row, $element){
            updateConsignorIndex = $element.data('index');
            openConsignorInfo(updateConsignorIndex);
            consignorFlag = true;
        },
        formatNoMatches : function() {
            return '无匹配数据';
        }
    });
    /**
     * @Method description 预配舱单 通知人列表定义
     * @author 马青松 2018/5/17
     * @description bootStrap 通知人列表定义
     */
    $('#notifyInfoTalbe').bootstrapTable('destroy');
    $('#notifyInfoTalbe').bootstrapTable({
        escape:true,
        cache : false,
        height : 180,
        striped : true,
        pagination : false,
        pageSize : 5,
        pageNumber : 1,
        pageList : [ 5, 10, 15, 20, 30, 50, 100, 200, 500],
        search : false,
        showRefresh : false,
        showToggle : false,
        showExport : false,
        undefinedText : "",
        clickToSelect : false,
        singleSelect : false,
        checkboxHeader : true,
        ajaxOptions:{async:false},
        exportTypes : [ 'excel' ],
        queryParams : 'queryParams',
        toolbar : '#tableToolbar',
        iconSize : 'outline',
        icons : {
            refresh : 'glyphicon-repeat',
            toggle : 'glyphicon-list-alt',
            columns : 'glyphicon-list'
        },
        onPageChange : function(size, number) {
        },
        onClickRow : function(row, $element){
            updateNotifyIndex = $element.data('index');
            openNotifyInfo(updateNotifyIndex);
            notifyFlag = true;
        },
        formatNoMatches : function() {
            return '无匹配数据';
        }
    });
    /**
     * @Method description 预配舱单 危险品联系人列表定义
     * @author 马青松 2018/5/17
     * @description bootStrap 危险品联系人列表定义
     */
    $('#undgInfoTalbe').bootstrapTable('destroy');
    $('#undgInfoTalbe').bootstrapTable({
        escape:true,
        cache : false,
        height : 180,
        striped : true,
        pagination : false,
        pageSize : 5,
        pageNumber : 1,
        pageList : [ 5, 10, 15, 20, 30, 50, 100, 200, 500],
        search : false,
        showRefresh : false,
        showToggle : false,
        showExport : false,
        undefinedText : "",
        clickToSelect : false,
        singleSelect : false,
        checkboxHeader : true,
        ajaxOptions:{async:false},
        exportTypes : [ 'excel' ],
        queryParams : 'queryParams',
        toolbar : '#tableToolbar',
        iconSize : 'outline',
        icons : {
            refresh : 'glyphicon-repeat',
            toggle : 'glyphicon-list-alt',
            columns : 'glyphicon-list'
        },
        onPageChange : function(size, number) {
        },
        onClickRow : function(row, $element){
            updateUndgIndex = $element.data('index');
            openUndgInfo(updateUndgIndex);
            undgFlag = true;
        },
        formatNoMatches : function() {
            return '无匹配数据';
        }
    });
    /**
     * @Method description 预配舱单 途径国家列表定义
     * @author 马青松 2018/5/17
     * @description bootStrap 途径国家列表定义
     */
    $('#countryInfoTalbe').bootstrapTable('destroy');
    $('#countryInfoTalbe').bootstrapTable({
        //  url:contaJson,
        escape:true,
        cache : false,
        height : 180,
        striped : true,
        pagination : false,
        pageSize : 5,
        pageNumber : 1,
        pageList : [ 5, 10, 15, 20, 30, 50, 100, 200, 500],
        search : false,
        showRefresh : false,
        showToggle : false,
        showExport : false,
        undefinedText : "",
        clickToSelect : false,
        singleSelect : false,
        checkboxHeader : true,
        ajaxOptions:{async:false},
        exportTypes : [ 'excel' ],
        queryParams : 'queryParams',
        toolbar : '#tableToolbar',
        iconSize : 'outline',
        icons : {
            refresh : 'glyphicon-repeat',
            toggle : 'glyphicon-list-alt',
            columns : 'glyphicon-list'
        },
        onPageChange : function(size, number) {
        },
        onClickRow : function(row, $element){
            updateCountryIndex = $element.data('index');
            openCountryTableInfo(updateCountryIndex);
            countryFlag = true;
        },
        formatNoMatches : function() {
            return '无匹配数据';
        }
    });
    /**
     * @Method description 预配舱单 更改原因列表定义
     * @author 马青松 2018/5/17
     * @description bootStrap 更改原因列表定义
     */
    $('#chgInfoTalbe').bootstrapTable('destroy');
    $('#chgInfoTalbe').bootstrapTable({
        escape:true,
        cache : false,
        height : 180,
        striped : true,
        pagination : false,
        pageSize : 5,
        pageNumber : 1,
        pageList : [ 5, 10, 15, 20, 30, 50, 100, 200, 500],
        search : false,
        showRefresh : false,
        showToggle : false,
        showExport : false,
        undefinedText : "",
        clickToSelect : false,
        singleSelect : false,
        checkboxHeader : true,
        ajaxOptions:{async:false},
        exportTypes : [ 'excel' ],
        queryParams : 'queryParams',
        toolbar : '#tableToolbar',
        iconSize : 'outline',
        icons : {
            refresh : 'glyphicon-repeat',
            toggle : 'glyphicon-list-alt',
            columns : 'glyphicon-list'
        },
        onPageChange : function(size, number) {
        },
        onClickRow : function(row, $element){
            updateChgIndex = $element.data('index');
            openChgTableInfo(updateChgIndex);
            chgFlag = true;
        },
        formatNoMatches : function() {
            return '无匹配数据';
        }
    });
    //hscode列表
    $('#hscodeInfoTalbe').bootstrapTable('destroy');
    $('#hscodeInfoTalbe').bootstrapTable({
        // url:dataJson,
        escape:true,
        cache : false,
        height : 500,
        striped : true,
        pagination : false,
        pageSize : 5,
        pageNumber : 1,
        pageList : [ 5, 10, 15, 20, 30, 50, 100, 200, 500],
        search : false,
        showRefresh : false,
        showToggle : false,
        showExport : false,
        clickToSelect : true,
        singleSelect : true,
        checkboxHeader : true,
        undefinedText : "",
        ajaxOptions:{async:false},
        exportTypes : [ 'excel' ],
        queryParams : 'queryParams',
        toolbar : '#tableToolbar',
        iconSize : 'outline',
        icons : {
            refresh : 'glyphicon-repeat',
            toggle : 'glyphicon-list-alt',
            columns : 'glyphicon-list'
        },
        onClickRow : function(row,$element) {

        },
        onPageChange : function(size, number) {
        },
        formatNoMatches : function() {
            return '无匹配数据';
        }
    });
    //校验
    mftValidateFunc('org_mtf_m_head_form',mftHeadInfoFormValidateRules,mftHeadInfoFormValidateMsgs);
    mftValidateFunc('org_bill_info',billInfoFormValidateRules,billInfoFormValidateMsgs);
    mftValidateFunc('mft_bill_container_form',contaInfoFormValidateRules,contaInfoFormValidateMsgs);
    mftValidateFunc('mft_goods_info_form',commodityinfoFormValidateRules,commodityinfoFormValidateMsgs);
    //回车挂数据
    /*chgBillInfos();
    chgContaInfos();
    chgGoodsInfos();*/
    //回车打开弹窗
    //openButtons();
    // $("#HSCode").bind('input propertychange',function () {//增加HS编码 20181022
    // 	 getGoodsHs();
    //     });
    function getGoodsHs(){//添加HS编码（税号？）20181022
        var HSCode =  $("#HSCode").val();
        if(HSCode.length>2){
            var jsonObj = {};
            jsonObj.tableName = 'CUS_COMPLEX';
            jsonObj.keyValue = HSCode;
            $.ajax({
                url: swProxyBasePath+"/sw/base/para/depParaList",
                async: false,
                data: jsonObj,
                dataType: "json",
                type: "GET",
                contentType: "application/json;charset=utf-8",
                success: function(data) {
                    var compJson = [];
                    var compNoRepeat = {};
//							var hsNum = 0;
                    if(data.length<1){
                        layer.tips('无匹配数据', '#HSCode', {
                            tips: [1, 'red'],
                            time: 2000
                        });
                        return;
                    }
                    for(var i = 0; i < data.length; i++) {
                        if(compJson.length>0&&compNoRepeat.hasOwnProperty(data[i].codeValue)){//判断对象中是否已经存在某个HS码 如果有就跳过
                            continue;//开始json对象中没数据 所以要先保证有一条数据再判断
                        }
                        compNoRepeat[data[i].codeValue]=i;//将查询的HS码作为key放入指定json对象
                        var compJsonI = {
                            value: data[i].codeValue,
                            name: data[i].codeValue,
                            label:data[i].codeValue
                        }
                        compJson.push(compJsonI);//将对象放入数组
                    }
//							$("#HSCode" ).val(data[0].codeValue)
                    $("#HSCode" ).autocompleter({
                        source: compJson,
                        hiddenId: "HSCodeText",
                        callback: function(lable, index, obj) {

                        }
                    })
                }
            })

        }

    }
});
//页面初始化
function initPage() {
    //其他数据申请
    pageType == 'main' && initMainPage();
    //删除申请
    pageType == 'delete' && initDelPage();

    //变更申请
    pageType == 'change' && initChangePage();

    removeMustType();
    var mftId = null;
    setTimeout(100);//先初始化页面再查询，避免按钮激活混乱
    if(getTabParam()){
        mftId=getTabParam().mftNo;
        queryHead(mftId);
    }
//	$("#voyageNo").focus();
    if(pageType==="main"){
        //时间控件
        laydate.render({
            elem: '#loadingDate', //指定元素
            // istime: true,
            type: 'datetime',
            done: function(value, date){
                $("#loadingDate").val(value);
                $("#loadingLocationCode").focus();
            },

            format: 'yyyyMMddHHmmss' ,
            min: '1000-01-11 00:00:00' ,
            max: '2999-12-31 23:59:59'
        });
    }

}
//初始化 main数据页面
function initMainPage() {
    $("#org_head_save").attr("disabled", true);//暂存
    $("#mft_copy").attr("disabled", true);//暂存
    $("#mft_declare").attr("disabled", true);//申报
    $("#delete_mft_head").attr("disabled", true);//删除
    $("#chgCodeAddBtn").attr("disabled", true);//更改原因
    $("#unitCode").attr("readonly",true);//企业代码

    getCompanyInfo(null);
}
//初始化 change数据页面
function initChangePage() {
    //移除不需要的按钮
    var notAllowBtn = ["org_head_save","delete_mft_head","mft_copy","excelcontasId","excelId","excelgoodsId","mftfresh"];

    $("button").each(function(){
        if(($.inArray($(this)[0].id,notAllowBtn)>= 0)){
            $(this).remove();
        }
    });
    $("#container_add").attr("disabled", true);
    $("#mft_container_save").attr("disabled", true);//集装箱新增
    $("#mft_container_delete").attr("disabled", true);//集装箱删除
    $("#sysRand").attr("disabled", true);//系统分配批次
    $("#billNo").removeAttr("readonly");
    $("#goodsCustomsStat").attr("readonly",true);//海关货物通关代码
    /*$("#packNum").attr("readonly",true);//货物总件数
    $("#packType").attr("readonly",true);//包装种类代码
    $("#grossWt").attr("readonly",true);//货物总毛重
*/	/*$("#chgCodeAddBtn").attr("disabled", true);//更改原因
	$("#countryInfoAddBtn").attr("disabled", true);//途径国家
	$("#consigneeInfo").attr("disabled", true);//收货人
	$("#consigorInfo").attr("disabled", true);//发货人
	$("#notifyInfo").attr("disabled", true);//通知人
	$("#undgInfo").attr("disabled", true);//危险品
	$("#commodity_info_save").attr("disabled", true);//商品项新增
	$("#mft_goods_delete").attr("disabled", true);//商品项删除
*/
    /**
     * 2.设置不可编辑的input text 为readonly
     * @param notAllow  不可编辑input text 的ID
     * @returns
     */
        //处理表头输入框  提运单 集装箱 封志
    var notAllow = [];
    //表头
    notAllow.push('carrierCode', 'transAgentCode', 'loadingDate',
        'loadingLocationCode','unitCode');
    /*//提运单
    notAllow.push('billNo', 'conditionCodeName', 'paymentType',
            'goodsCustomsStat', 'packNum', 'packType',
            'cube','grossWt', 'goodsValue', 'currencyType',
            'consolidatorId');*/
    //商品项

    /*notAllow.push('goodsSeqNo','goodsPackNum', 'goodsPackType', 'goodsGrossWt',
                'goodsBriefDesc', "undgNoName", "goodsDetailDesc");*/


    //集装箱
    notAllow.push('contaId');

    $("input").each(function() {
        if (($.inArray($(this)[0].id, notAllow) >= 0)) {
            $(this).attr("readonly", "readonly");
//			$(this).removeAttr("style");
//			$(this).removeAttr("title");
        }
    });

    getCompanyInfo(null);
}
/*删除变更申请*/
function initDelPage(){
    //移除不需要的按钮
    var notAllowBtn = ["org_head_save","delete_mft_head","mft_copy","excelcontasId","excelId","excelgoodsId","mftfresh"];

    $("button").each(function(){
        if(($.inArray($(this)[0].id,notAllowBtn)>= 0)){
            $(this).remove();
        }
    });

    $("#sysRand").attr("disabled", true);//系统分配批次
    //$("#chgCodeAddBtn").attr("disabled", true);//更改原因
//	$("#countryInfoAddBtn").attr("disabled", true);//途径国家
    $("#consigneeInfo").attr("disabled", true);//收货人
    $("#consigorInfo").attr("disabled", true);//发货人
    //$("#notifyInfo").attr("disabled", true);//通知人
    $("#undgInfo").attr("disabled", true);//危险品
    $("#container_add").attr("disabled", true);
    $("#mft_container_save").attr("disabled", true);//集装箱新增
    $("#mft_container_delete").attr("disabled", true);//集装箱删除
    $("#commodity_info_save").attr("disabled", true);//商品项新增
    $("#mft_goods_delete").attr("disabled", true);//商品项删除
    $("#commodity_add").attr("disabled", true);
    $("#billNo").removeAttr("readonly");

    /**
     * 2.设置不可编辑的input text 为readonly
     * @param notAllow  不可编辑input text 的ID
     * @returns
     */
        //处理表头输入框  提运单 集装箱 封志
    var notAllow = [];
    //表头
    notAllow.push('carrierCode', 'transAgentCode', 'loadingDate',
        'loadingLocationCode','unitCode');

    //提运单
    notAllow.push('conditionCodeName', 'paymentType',
        'goodsCustomsStat', 'packNum', 'packType',
        'cube','grossWt', 'goodsValue', 'currencyType',
        'consolidatorId');
    //商品项

    notAllow.push('goodsSeqNo','goodsPackNum', 'goodsPackType', 'goodsGrossWt',
        'goodsBriefDesc', "undgNoName","HSCode", "goodsDetailDesc");


    //集装箱
    notAllow.push('contaId');

    $("input").each(function() {
        if (($.inArray($(this)[0].id, notAllow) >= 0)) {
            $(this).attr("readonly", "readonly");
            //			$(this).removeAttr("style");
            //			$(this).removeAttr("title");
        }
    });

    getCompanyInfo(null);

}

/*$("#customMasterName").blur(function(){
	var code=$("#customMaster").val();
	getCompanyInfo(code);
});*/

//移除必填样式
function removeMustType(){
    if(pageType=="change"){
        //移除必填样式
        var move = [];
        move.push('loadingDate', 'loadingLocationCode', 'paymentType',
            'goodsCustomsStat', 'packNum', 'packType',
            'goodsValue', 'currencyType','consigneeName','consignorName','contaId',
            'goodsPackNum','goodsPackType','goodsGrossWt','goodsBriefDesc','goodsDetailDesc'
        );
        $("input").each(function() {
            if (($.inArray($(this)[0].id, move) >= 0)) {

                $(this).removeClass("non-empty");

            }
        });
    }else if(pageType=="delete"){
        var allow = [];
        //提运单
        allow.push('billNo');

        //移除必填样式
        var move = [];
        move.push('loadingDate', 'loadingLocationCode', 'paymentType',
            'goodsCustomsStat', 'packNum', 'packType',
            'grossWt', 'goodsValue', 'currencyType','consigneeName','consignorName','contaId',
            'goodsPackNum','goodsPackType','goodsGrossWt','goodsBriefDesc','goodsDetailDesc'
        );
        $("input").each(function() {
            if (($.inArray($(this)[0].id, move) >= 0)) {
                $(this).removeClass("non-empty");
            }
        });
    }
}

$("#consigneeName").blur(function(){
    var length=$("#consigneeName").val().length;
    if(length>0){
        $("#consigneeContactName").removeAttr("readonly");

    }else{
        $("#consigneeContactName").attr("readOnly", "readOnly");
        $("#consigneeContactName").val('');
        clearConsigneeContactInfo();//清空表单
        $('#consigneeContactInfoTalbe').bootstrapTable('removeAll');//清空列表
    }
});
//集装箱enter新增
$("#contaId").keydown(function(e){
    if(getKey(e)=="13"){
        var billNo = $("#billNo").val();
        if(billNo==''||billNo== null||billNo== undefined){
            showLayerWarnMsg("提(运)单号不可为空");
            return;
        }else{
            //非空校验和正则校验通过才能新增
            var contaId=$("#contaId").val();
            if(contaId.length===0){
                showLayerWarnMsg("集装箱（器）编号不可为空");
                return;
            }else{
                if($("#mft_bill_container_form").valid()){
                    addContainerFunc();
                    refreshContainerInfo();
                }
            }
        }
    }

});


/**备注enter事件换行***/
$("#additionalInformation").keydown(function(e){
    if(getKey(e)=="13"){
        //延时操作 解决光标跳两格bug
        setTimeout(function () {$("#billNo").focus();}, 100);
    }
});
$("#additionalInformation").keyup(function(e){//按Ctrl换行
    if(getKey(e)=="17"){
        //延时操作 解决光标跳两格bug
        setTimeout(function () {$("#billNo").focus();}, 100);
    }
});

/**按enter事件时判断输入框中长度只有长度大于3才允许新增,大于3返回true，否则返回false**/
function isAdd(target){
    if($(target).val().length>3) return true;
    return false;
}

/*各种联系人enter事件*/

/*变更原因新增*/
$('#chgCodeText').keydown(function(event){
    if(getKey(event)=="13"&&isAdd(this)){
        addOrUpdateChg();//新增或更新变更原因方法
    }
});

$("#communicationTypeText").keydown(function(event){
    if(getKey(event)=="13"&&isAdd(this)){
        addOrUpdateConsignee();//新增或更新收货人信息
    }
});

$("#communicationTypeTextj").keydown(function(){
    if(getKey(event)=="13"&&isAdd(this)){
        addOrUpdateConsigneeContact();//新增或更新收货具体联系人信息
    }
});

$("#communicationTypeTextf").keydown(function(){
    if(getKey(event)=="13"&&isAdd(this)){
        addOrUpdateConsignor();//发货人新增或更新方法
    }
});

$("#communicationTypeTextt").keydown(function(){
    if(getKey(event)=="13"&&isAdd(this)){
        addOrUpdateNotify();//新增或更新通知人信息方法
    }
});

$("#communicationTypeTextw").keydown(function(){//危险品联系人
    if(getKey(event)=="13"&&isAdd(this)){
        addOrUpdateUndg();
    }
});


//下拉选
function initParamSelect(){

}

/**
 * 得到提运单列表查询的参数
 */
function querybillParams(param) {
    var  mftSeqNo= $("#mftSeqNo").val();
    /**
     if(urlParams && urlParams.opType==1){
		return $.extend({},urlParams,param);
	}
     */
    if(mftSeqNo){
        return {
            limit : param.limit,// 页面大小
            offset : param.offset,// 页码
            mftSeqNo : mftSeqNo,
        };
    }else{
        return false;
    }
}
/**
 * 得到集装箱信息列表查询的参数
 */
function queryContsParams(param) {
    var mftSeqNo=$("#mftSeqNo").val();
    var listNo=$("#listNo").val();
    if(mftSeqNo&&listNo){
        return {
            limit : param.limit,// 页面大小
            offset : param.offset,// 页码
            mftSeqNo : mftSeqNo,
            listNo : listNo,
        };
    }else{
        return false;
    }

}
/**
 * 得到商品信息列表查询的参数
 */
function queryGoodsParams(param) {

    var listNo=$("#listNo").val();
    if(mftSeqNo&&listNo){
        return {
            limit : param.limit,// 页面大小
            offset : param.offset,// 页码
        };
    }else{
        return false;
    }
}


$("#loadingLocationCode").focus(function(){
    $("body").click();
});

//点击 添加提运单增加 按钮，弹出提运单信息表单
$("#billOfLadingInformationAddBtn").click(function () {

    billSave();

});

/**提运单保存**/
function billSave(){
    if (!$("#org_bill_info").valid()) {
        return;
    };
    var goodsValue = $("#goodsValue").val();
    var currencyTypeCode = $("#currencyTypeCode").val();
    if((isNotEmpty(goodsValue)&&!isNotEmpty(currencyTypeCode))||(!isNotEmpty(goodsValue)&&isNotEmpty(currencyTypeCode))){//判断货物价值和金额类型必须成对出现
        showLayerWarnMsg("货物价值和金额类型必须成对录入");
        return;
    }
    addBillInfoFunc();

    //重置表单序号
    //billIndex = 1;
    contaIndex = 1;//集装箱列表序号
    commodityIndex = 1;//商品
    countryIndex = 1;//国家
    chgIndex = 1;//变更原因
    consigneeIndex = 1;//收货人
    consigneeContactIndex = 1;//收货具体联系人
    consignorIndex = 1;//发货人
    notifyIndex = 1;//通知人
    undgIndex = 1;//危险品联系人
}

/**
 * 提运单保存**/
$("#bill_add").click(function(){
    billSave();
});


/**
 * 联系人信息 窗口弹出事件
 */
function contactInfo(contactInfo,mftPersonTypeId,id){
    /**
     var  listNo= $("#listNo").val();
     if(listNo==""){
		 layer.msg("请先暂存提单信息", {
			  icon: 2,//失败图标
			  time: 2000 //2秒关闭（如果不配置，默认是3秒）
		});
		 return;
		}
     */
    layer.open({
        type:1,
        shadeClose:true,
        area:['800px','400px'],
        title:contactInfo,
        shade:0.5,
        scrollbar:false,
        btnAlign: 'c',//居中
        end:function(index){
            clearContactInfo();
            $("#mftPersonTypeId").val("");
            $('#contactInfoTalbe').bootstrapTable("removeAll");
        },
        content:$("#contactInfoAddContent")
    })
    $("#mftPersonTypeId").val(mftPersonTypeId);
    //刷新列表
    //$('#contactInfoTalbe').bootstrapTable("refresh");

    $("#"+id).blur();
}

/*************联系人信息*******************************************/
/**
 * @Method description 预配舱单 收货人新增事件
 * @author 马青松 2018/5/17
 * @description 新增或更新收货人信息
 */
$("#addContact").click(function(){//收货人信息界面新增按钮
    addOrUpdateConsignee();//收货人新增或更新方法
});
//
$("#saveContact").click(function(){//收货人信息界面保存按钮
    addOrUpdateConsignee();//收货人新增或更新方法
});
/**
 * @Method description 预配舱单 收货人删除事件
 * @author 马青松 2018/5/17
 * @description 删除收货人信息
 */
$("#delContact").click(function(){
    var selects = $('#consigneeInfoTalbe').bootstrapTable("getSelections");
    if(selects.length<1){
        layer.confirm('请勾选需删除的数据', {icon: 7, title:'提示'}, function(index){ //提示图标
            layer.close(index);
        });
        return;
    }else{
        layer.confirm('是否确认删除该数据？', {icon: 7, title:'提示'}, function(index){ //提示图标
            var listArr  = [];//空数组，盛放要删除的行的代表的fied

            for(var a=0;a<selects.length;a++){	//遍历需要删除的行
                var selectedIndex = selects[a].state;//用state状态去删除，state为true时删除
                var s = [];
                s.push(selectedIndex)//将state为true状态的加入数组
                $('#consigneeInfoTalbe').bootstrapTable('remove',{field: 'state', values: s});//移除指定行
                consigneeIndex--;//代表index的序号 减少1
            }

            layer.close(index);
            showLayerMsg('操作成功！');

        });
    }
    clearContactInfo();//清空表单
    consigneeFlag = false;//重置更新标记
});
/**
 * @Method description 预配舱单 收货人刷新方法
 * @author 马青松 2018/5/17
 * @description 收货人信息Input 清空
 */
function clearContactInfo(){
    $("#consignee_info_form #communicationNo").val("");
    $('#communicationTypeText').val('');
    $('#communicationType').val('');

}

/**
 * @Method description 预配舱单 收货具体联系人新增事件
 * @author 马青松 2018/5/17
 * @description 收货具体联系人信息新增或更新
 */
$("#addConsigneeContact").click(function(){
    addOrUpdateConsigneeContact();//收货具体联系人信息新增或更新方法
});
//保存按钮
$("#saveConsigneeContact").click(function(){
    addOrUpdateConsigneeContact();//收货具体联系人信息新增或更新方法
});
/**
 * @Method description 预配舱单 收货具体联系人删除事件
 * @author 马青松 2018/5/17
 * @description 收货具体联系人信息删除
 */
$("#delConsigneeContact").click(function(){
    var selects = $('#consigneeContactInfoTalbe').bootstrapTable("getSelections");
    if(selects.length<1){
        layer.confirm('请勾选需删除的数据', {icon: 7, title:'提示'}, function(index){ //3提示图标
            layer.close(index);
        });
        return;
    }else{
        layer.confirm('是否确认删除该数据？', {icon: 7, title:'提示'}, function(index){ //3提示图标
            var listArr  = [];
            //取联系人的技术主键放入输入通过ajax 传送
            for(var a=0;a<selects.length;a++){
                var selectedIndex = selects[a].state;//需要删除的fied
                var s = [];
                s.push(selectedIndex)
                $('#consigneeContactInfoTalbe').bootstrapTable('remove',{field: 'state', values: s});
                consigneeContactIndex--;
            }
            layer.close(index);
            showLayerMsg('操作成功！');

        });


    }
    clearConsigneeContactInfo();//清空input 框
    updateconsigneeContactIndexFlag = false;//重置更新标记
});
/**
 * @Method description 预配舱单 收货具体联系人刷新方法
 * @author 马青松 2018/5/17
 * @description 收货具体联系人信息清空
 */
function clearConsigneeContactInfo(){
    $("#consigneeContact_info_form #communicationNo").val("");
    $('#communicationTypeTextj').val('');
    $('#communicationTypej').val('');

}
/**
 * @Method description 预配舱单 发货人新增事件
 * @author 马青松 2018/5/17
 * @description 发货人新增或更新
 */
$("#addconsigno").click(function(){
    addOrUpdateConsignor();	//发货人新增或更新方法
});
//保存按钮
$("#saveconsigno").click(function(){
    addOrUpdateConsignor();	//发货人新增或更新方法
});
/**
 * @Method description 预配舱单 发货人删除事件
 * @author 马青松 2018/5/17
 * @description 发货人删除
 */
$("#delconsigno").click(function(){
    var selects = $('#consignoInfoTalbe').bootstrapTable("getSelections");
    if(selects.length<1){
        layer.confirm('请勾选需删除的数据', {icon: 7, title:'提示'}, function(index){ //3提示图标
            layer.close(index);
        });
        return;
    }else{
        layer.confirm('是否确认删除该数据？', {icon: 7, title:'提示'}, function(index){ //3提示图标
            var listArr  = [];
            //取联系人的技术主键放入输入通过ajax 传送
            for(var a=0;a<selects.length;a++){
                var selectedIndex = selects[a].state;//需要删除的行
                var s = [];
                s.push(selectedIndex)
                $('#consignoInfoTalbe').bootstrapTable('remove',{field: 'state', values: s});//移除相应行
                consignorIndex--;
            }
            layer.close(index);
            showLayerMsg('操作成功！');

        });


    }
    clearconsignorInfo();//清空input框
    consignorFlag = false;//重置更新标记
});
/**
 * @Method description 预配舱单 发货人信息刷新
 * @author 马青松 2018/5/17
 * @description 发货人刷新
 */
function clearconsignorInfo(){
    $("#consignor_info_form #communicationNo").val("");
    $('#communicationTypeTextf').val('');
    $('#communicationTypef').val('');

}
/**
 * @Method description 预配舱单 通知人信息新增
 * @author 马青松 2018/5/17
 * @description 通知人信息新增或更新
 */
$("#addnotify").click(function(){
    addOrUpdateNotify();//通知人信息新增或更新方法
});
//保存按钮
$("#savenotify").click(function(){
    addOrUpdateNotify();//通知人信息新增或更新方法
});
/**
 * @Method description 预配舱单 通知人删除事件
 * @author 马青松 2018/5/17
 * @description 通知人信息删除
 */
$("#delnotify").click(function(){
    var selects = $('#notifyInfoTalbe').bootstrapTable("getSelections");
    if(selects.length<1){
        layer.confirm('请勾选需删除的数据', {icon: 7, title:'提示'}, function(index){ //3提示图标
            layer.close(index);
        });
        return;
    }else{
        layer.confirm('是否确认删除该数据？', {icon: 7, title:'提示'}, function(index){ //3提示图标
            var listArr  = [];
            //取联系人的技术主键放入输入通过ajax 传送
            for(var a=0;a<selects.length;a++){	//遍历需要删除的行
                var selectedIndex = selects[a].state;//选中需要删除的field
                var s = [];
                s.push(selectedIndex);//将需要移除的field标记放入数组
                $('#notifyInfoTalbe').bootstrapTable('remove',{field: 'state', values: s});//移除对应行
                notifyIndex--;
            }
            layer.close(index);
            showLayerMsg('操作成功！');

        });


    }
    clearaddnotifyInfo();//清空input框
    notifyFlag = false;//重置更新标记
});
/**
 * @Method description 预配舱单 通知人刷新方法
 * @author 马青松 2018/5/17
 * @description 通知人信息刷新
 */
function clearaddnotifyInfo(){
    $("#notify_info_form #communicationNo").val("");
    $('#communicationTypeTextt').val('');
    $('#communicationTypet').val('');

}
/**
 * @Method description 预配舱单 危险品联系人新增事件
 * @author 马青松 2018/5/17
 * @description 危险品联系人新增或更新
 */
$("#addundg").click(function(){
    addOrUpdateUndg();//危险品联系人新增或更新方法
});
//保存按钮
$("#saveundg").click(function(){
    addOrUpdateUndg();//危险品联系人新增或更新方法
});
/**
 * @Method description 预配舱单 危险品联系人删除事件
 * @author 马青松 2018/5/17
 * @description 危险品联系人删除
 */
$("#delundg").click(function(){
    var selects = $('#undgInfoTalbe').bootstrapTable("getSelections");
    if(selects.length<1){
        layer.confirm('请勾选需删除的数据', {icon: 7, title:'提示'}, function(index){ //3提示图标
            layer.close(index);
        });
        return;
    }else{
        layer.confirm('是否确认删除该数据？', {icon: 7, title:'提示'}, function(index){ //3提示图标
            var listArr  = [];
            //取联系人的技术主键放入输入通过ajax 传送
            for(var a=0;a<selects.length;a++){//遍历需要删除的行
                var selectedIndex = selects[a].state;//获取需要删除行的标记
                var s = [];
                s.push(selectedIndex)//将需要删除行的标记放入数组
                $('#undgInfoTalbe').bootstrapTable('remove',{field: 'state', values: s});//移除相应行
                undgIndex--;
            }
            layer.close(index);
            showLayerMsg('操作成功！');

        });


    }
    clearaddundgInfo();//刷新input框
    undgFlag = false;//重置更新标记
});
/**
 * @Method description 预配舱单 危险品联系人刷新方法
 * @author 马青松 2018/5/17
 * @description 危险品联系人刷新方法
 */
function clearaddundgInfo(){
    $("#undg_info_form #communicationNo").val("");
    $('#communicationTypeTextw').val('');
    $('#communicationTypew').val('');

}


//------------------------变更原因
//变更原因  窗口打开
$("#chgCodeAddBtn").click(function() {
    /**
     var listNo =$("#listNo").val();
     if(!listNo){
		showLayerWarnMsg('请先保存提运单基本信息');
		return;
	}
     */
    layer.open({
        type : 1,
        shadeClose : false,
        area : ["60%", "80%" ],
        title : "变更原因信息",
        shade : 0.5,
        scrollbar : false,
        btn:['关闭'],
        btn1:function(index, layero){
            layer.close(index);
        },
        btnAlign : 'c',// 居中
        end: function(index) {
//			$("#chgInfoTalbe").bootstrapTable("removeAll");
        },
        content : $("#changeAddContent")
    })
    $("#chgCodeText").focus();
    return false;

});
//新增变更原因
$("#saveChgInfoBtn").click(function(){
    addOrUpdateChg();
});
//新增变更原因
$("#saveAddChgInfoBtn").click(function(){
    addOrUpdateChg();
});
//删除变更原因
$("#deleteChgInfoBtn").click(function(){
    delChgInfo();
});
//

/**
 * 预配舱单 提运单信息  刷新
 *
 */
$("#bill_info_refresh").click(function (){
    clearBillInfo();
    $("#billNo").focus();
});

/**
 * 预配舱单 提运单信息 暂存
 */
//	$("#bill_info_save").click(function(){
//		saveBillInfoFunc();
//	});
/**
 * 提单列表生成超链接方法
 */
function billTableHrefFormatter(value,row,index){
    return '<a href="javascript:void(0)" onclick="openBillFormFunc(' + index + ')">' + value + '</a>';
}
/**
 * 集装箱列表生成超链接方法
 */
function containerTableHrefFormatter(value,row,index){
    return '<a href="javascript:void(0)" onclick="lookContainerInfo(' + index + ')">' + value + '</a>';
}
/**
 * 商品列表生成超链接方法
 */
function goodsTableHrefFormatter(value,row,index){
    return '<a href="javascript:void(0)" onclick="lookGoodsInfo(' + index + ')">' + value + '</a>';
}

/**
 * 提运单复制
 */
$("#bill_info_copy").click(function(data){
    copyBillSaveInfoFunc();
    //$("#bill_info_copy").blur();
});
/**
 * 汇总
 */
$("#goodsTotal").click(function(){
    copyBillInfoFunc();
    //$("#bill_info_copy").blur();
});

/**
 * 系统分配批次号
 */
$("#sysRand").click(function(){
    createBatchNumber('MT2401');
});




//收货人
$("#consigneeInfo").click(function(title,ContactId){
    title='收货人信息';
    ContactId="#consigneeInfoAddContent";
    openDetail(title,ContactId);
    setTimeout(function () {$("#consigneeId").focus();}, 100);//延时，不然会聚焦到第二个input框
//	$("#consigneeId").focus();
});
//发货人
$("#consigorInfo").click(function(title,ContactId){
    title='发货人信息';
    ContactId="#consignorInfoAddContent";
    openDetail(title,ContactId);
    setTimeout(function () {$("#consignorId").focus();}, 100);
//	$("#consignorId").focus();

});
//通知人
$("#notifyInfo").click(function(title,ContactId){
    title='通知人信息';
    ContactId="#notifyInfoAddContent";
    openDetail(title,ContactId);
    setTimeout(function () {$("#notifyId").focus();}, 100);
//	$("#notifyId").focus();
});
//危险品联系人
$("#undgInfo").click(function(title,ContactId){
    title='危险品联系人信息';
    ContactId="#undgInfoAddContent";
    openDetail(title,ContactId);
    setTimeout(function () {$("#undgContactName").focus();}, 100);
//	$("#undgContactName").focus();
});
/**
 * 舱单页面校验公共方法
 * @param formId
 * @param rules
 * @param messages
 * @returns
 */
function mftValidateFunc(formId, rules, messages) {
    var idArr = [ "voyageNo", "customsCode", "loadingDate",
        "loadingLocationCode", "customMaster", "unitCode",
        "msgRepName", "billNo",
        "conditionCode", "paymentTypeCode" ];
    var validateForm = $("#" + formId).validate(
        { //表单id
            focusInvalid : false, //提交表单后，未通过验证的表单是否获得焦点
            onkeyup : false, //是否在敲击键盘时验证
            onfocusout : function(element) {

                $(element).valid();

            },
            onclick : function(element) {
                $(element).valid();
            }, //在鼠标点击时验证
            onsubmit : true, //是否提交表单时验证
            ignore : "", //如果要校验下拉框请配置此项
            submitHandler : function(form) { //表单提交句柄,为一回调函数，带一个参数：form
                form.submit(); //提交表单
            },
            rules : rules,
            errorPlacement : function(error, element) {
                // 提示消息回调
                if (element.is(':radio') || element.is(':checkbox')) {
                    var objValList = document
                        .getElementsByName(element[0].name);// 为了显示在同一单选/复选框的最后面
                    layer.tips(error[0].innerText, // 所需提醒的信息
                        objValList[objValList.length - 1], { // 所需提醒的元素
                            tips : [ 3, '#DC143C' ], // 在元素的下面出现 1上面，2右边 3下面
                            tipsMore : true, // 允许同时存在多个
                            time : 5000
                            // tips自动关闭时间，毫秒
                        });
                } else if (element[0].nodeName == "SELECT") { // 伪选择器不支持SELECT,下拉框需要单独定位
                    var selectTipsObj = $(element[0]).next('div');
                    layer.tips(error[0].innerText, // 所需提醒的信息
                        selectTipsObj, { // 所需提醒的元素
                            tips : [ 3, '#DC143C' ], // 在元素的下面出现 1上面，2右边 3下面
                            tipsMore : true, // 允许同时存在多个
                            time : 5000
                            // tips自动关闭时间，毫秒
                        });
                } else {
                    if(element[0].type == 'hidden'){
                        layer.tips(error[0].innerText,  //所需提醒的信息
                            $("#" + element[0].id+'Name'),{		//所需提醒的元素
                                tips: [2,'#DC143C'], //在元素的下面出现 1上面，2右边 3下面
                                tipsMore: true, //允许同时存在多个
                                time:3000 //tips自动关闭时间，毫秒
                            });
                    }else{
                        layer.tips(error[0].innerText,  //所需提醒的信息
                            element[0],{		//所需提醒的元素
                                tips: [2,'#DC143C'], //在元素的下面出现 1上面，2右边 3下面
                                tipsMore: true, //允许同时存在多个
                                time:3000 //tips自动关闭时间，毫秒
                            });
                    }
//								var errDiv = $('<div class="error-tips" data-error="'
//										+ error[0].innerText
//										+ '" style="top: '
//										+ element.outerHeight(true)
//										+ 'px;"></div>');
//								if ($(element).parent()
//										&& $(element).parent().hasClass(
//												'input-group')) {
//									errDiv.insertAfter($(element).parent());
//								} else {
//									errDiv.insertAfter(element);
//								}
//								setTimeout(function() {
//									errDiv.remove()
//								}, 5000);
                }
            },
            messages : messages
        });
    return validateForm;
}
/**
 * @Method description 预配舱单 非空判断方法
 * @author 马青松 2018/5/17
 * @description 判断传入数据是否为空
 * @return true Or false
 */
function isNotNull(data){
    if (data !== null && data !== undefined && data !== '') {
        return true;
    }else{
        return false;
    }
}

//加载hscode列表 maqingsong 20181220
$("#HSCode").keydown(function(e){
    var hscode=$("#HSCode").val();
    if(getKey(e)=="13" && trim(hscode)!=""){
        getHscodeList();
        openHscodeWindow();//打开弹窗
    }
});
/**
 * 去空格
 * @param s
 */
function trim(s){
    return s.replace(/(^\s*)|(\s*$)/g, "");
}
var isOpenWindow = false;
function openHscodeWindow(){
    isOpenWindow = true;
    layer.open({
        type:1,
        shadeClose:true,
        area:['250px','650px'],
        title:'HS编码列表',
        shade:0.5,
        scrollbar:false,
        btnAlign: 'c',//居中
        success:function(){
            $("#HSCode").focus();
        },
        btn:['关闭'],
        btn1:function(index, layero){
            isOpenWindow = false;
            layer.close(index);
        },
        end:function(index){
            var hscodeInfo=$("#hscodeInfoTalbe").bootstrapTable("getSelections");

            if(isNotNull(hscodeInfo)&&hscodeInfo.length>0){//如果用户做了选中则给input框赋值，并光标聚焦下个input
                $("#HSCode").val(hscodeInfo[0].codeValue);
                $("#goodsDetailDesc").focus();
            }else{//如果用户不做选择，聚焦到hscode框
                $("#HSCode").focus();
            }
            isOpenWindow = false;

        },
        content:$("#hsCodeContent")
    })

}
function getHscodeList(){
    if(isOpenWindow){//如果弹窗打开了，就不再触发
        return;
    }
    var HSCode = $("#HSCode").val();
    if(HSCode.length>=3){
        $("#hscodeInfoTalbe").bootstrapTable("removeAll");//先清空列表

        var jsonObj = {};
        jsonObj.tableName = 'CUS_COMPLEX';
        jsonObj.keyValue = HSCode;
        $.ajax({
            url: swProxyBasePath+"/sw/base/para/depParaList",
            async: false,
            data: jsonObj,
            dataType: "json",
            type: "GET",
            contentType: "application/json;charset=utf-8",
            success: function(data) {
                var compJson = [];
                var compNoRepeat = {};
//							var hsNum = 0;
                if(data.length<1){
                    layer.tips('无匹配数据', '#HSCode', {
                        tips: [1, 'red'],
                        time: 2000
                    });
                    return;
                }
                for(var i = 0; i < data.length; i++) {
                    if(compJson.length>0&&compNoRepeat.hasOwnProperty(data[i].codeValue)){//判断对象中是否已经存在某个HS码 如果有就跳过
                        continue;//开始json对象中没数据 所以要先保证有一条数据再判断
                    }
                    compNoRepeat[data[i].codeValue]=i;//将查询的HS码作为key放入指定json对象
                    var compJsonI = {
                        codeValue: data[i].codeValue,
                        name: data[i].codeValue,
                        label:data[i].codeValue
                    }
                    compJson.push(compJsonI);//将对象放入数组
                    var hscodeArray=$("#hscodeInfoTalbe").bootstrapTable("getData");
                    $('#hscodeInfoTalbe').bootstrapTable('insertRow', {
                        index: hscodeArray.length+1,
                        row: compJsonI
                    });
                }

//
            },
            error:function(){

            }
        })
    }
}
$("#goodsCustomsStat").change(function(){
    changeClass();
});