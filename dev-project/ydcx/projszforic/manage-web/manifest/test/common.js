/**
 * 2020-03-19
 * 原始、预配、空车、空箱生成批次号
 **/
function createBatchNumber(msgType){
    var voyageNo = $("#voyageNo").val();
    if(voyageNo.length>0){
        $("#sysRand").attr("disabled", true);//系统分配批次按钮灰掉
        $("#customsCodeName").focus();
        return;
    }
    var serviceName="eport.superpass.roadmft.GetVoyageNoService";//系统分配批次号服务名
    //var msgType="msgType";//报文类型
    var data={"serviceName":serviceName,
        "getVoyageNoSqeRequest":{"msgType":msgType}}

    $.ajax({
        type : "POST",
        url : BasePath+"/manifest/voyageNo",
        data : JSON.stringify(data),
        contentType : "application/json;charset=utf-8",
        dataType : "json",
        success : function(json) {
            //console.log(json);
            if(json=="fail"){
                return;

            }else{
                $("#voyageNo").val(json);//将返回的货物运输批次号显示在input框上
                $("#sysRand").attr("disabled", true);//系统分配批次按钮灰掉
            }

            //光标聚焦到货物运输批次号
            $("#voyageNo").focus();
        }
    });

}
