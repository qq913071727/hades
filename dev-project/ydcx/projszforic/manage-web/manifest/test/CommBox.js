
//报关下拉框
$(function() {
    //运输方式代码 SW_TRANSF
//	$('#trafMode').autocomp({// 隐藏框
//		tableName:'SW_TRANSF',// 查询的同义词
//		hiddenId:'trafModeCode'// 显示框
//	});
    //进出境口岸海关代码
    $('#customsCodeName').autocomp({// 隐藏框
        tableName:'CUS_CUSTOMS',// 查询的同义词
        hiddenId:'customsCode'// 显示框
    });

    //传输企业备案关区
    $('#customMasterName').autocomp({// 隐藏框
        tableName:'CUS_CUSTOMS',// （备案机关？申报企业关区？）
        hiddenId:'customMaster'// 显示框
    });
    //企业代码 不能选？
//	$('#loadingLocationId').autocomp({// 隐藏框
//		tableName:'MFT8_ENTITY',// 查询的同义词
//		hiddenId:'loadingLocationCode'// 显示框
//	});
    //变更原因代码
    $('#chgCodeText').autocomp({// 隐藏框
        tableName:'CUS_MFT8_CHANGE_REASON',// 查询的同义词
        hiddenId:'amendmentSeq'// 显示框
    });
    //货物海关通关代码
    $('#goodsCustomsStat').autocomp({// 隐藏框
        tableName:'CUS_MFT8_ROAD_CUSTOMS_STATUS',// 查询的同义词
        hiddenId:'goodsCustomsStatCode'// 显示框
    });
    //运输条款
    $('#conditionCodeName').autocomp({// 隐藏框
        tableName:'CUS_MFT8_CONTR_CAR_COND',// 查询的同义词
        hiddenId:'conditionCode'// 显示框
    });
    //运费支付方法
    $('#paymentType').autocomp({// 隐藏框
        tableName:'CUS_MFT8_ROAD_PAYMENT_METHOD',// 查询的同义词
        hiddenId:'paymentTypeCode'// 显示框
    });

    //跨境指运地
    $('#transLocationId').autocomp({// 隐藏框
        tableName:'CUS_CUSTOMS',// 查询的同义词
        hiddenId:'transLocationIdCode'// 显示框
    });
    //包装种类
    $('#packType').autocomp({// 隐藏框
        tableName:'CUS_MFT8_PACKAGING',// 运输工具包装种类？
        hiddenId:'packTypeCode'// 显示框
    });
    //金额类型
    $('#currencyType').autocomp({// 隐藏框
        tableName:'CUS_MFT8_ROAD_CURR',// 查询的同义词
        hiddenId:'currencyTypeCode'// 显示框
    });

    //途径国家代码
    $('#routingContryIdText').autocomp({// 隐藏框
        tableName:'CUS_MFT8_COUNTRY_CODE',// 查询的同义词
        hiddenId:'routingContryIdTextCode'// 显示框
    });
    //通讯方式类别代码-收货人
    $('#communicationTypeText').autocomp({// 显示框
        tableName:'CUS_MFT8_COMMUNI_TYPE',// 查询的同义词
        hiddenId:'communicationType'// 隐藏框
    });
    //通讯方式类别代码-收货具体
    $('#communicationTypeTextj').autocomp({// 显示框
        tableName:'CUS_MFT8_COMMUNI_TYPE',// 查询的同义词
        hiddenId:'communicationTypej'// 隐藏框
    });
    //通讯方式类别代码-通知人
    $('#communicationTypeTextt').autocomp({// 显示框
        tableName:'CUS_MFT8_COMMUNI_TYPE',// 查询的同义词
        hiddenId:'communicationTypet'// 隐藏框
    });
    //通讯方式类别代码-发货人
    $('#communicationTypeTextf').autocomp({// 显示框
        tableName:'CUS_MFT8_COMMUNI_TYPE',// 查询的同义词
        hiddenId:'communicationTypef'// 隐藏框
    });
    //通讯方式类别代码-危险品
    $('#communicationTypeTextw').autocomp({// 显示框
        tableName:'CUS_MFT8_COMMUNI_TYPE',// 查询的同义词
        hiddenId:'communicationTypew'// 隐藏框
    });

    //包装种类
    $('#goodsPackType').autocomp({// 隐藏框
        tableName:'CUS_MFT8_PACKAGING',// 查询的同义词
        hiddenId:'goodsPackTypeCode'// 显示框
    });

    //包装种类_商品项
    $('#goodsPackType').autocomp({// 隐藏框
        tableName:'CUS_MFT8_PACKAGING',// 查询的同义词
        hiddenId:'goodsPackTypeCode'// 显示框
    });
    //危险品编号
    $('#undgNoName').autocomp({// 隐藏框
        tableName:'CUS_MFT8_DAN_GOODS',// 查询的同义词
        hiddenId:'undgNo'// 显示框
    });
//	//转关类型
//	$('#customsTransit').autocomp6({// 显示框
//		tableName:"PARA_CUSTOMS_TRANSIT_TYPE",// 查询的同义词
//		hiddenId:'customsTransitCode',// 隐藏框
//		keyValue:'MT2401'
//	});

//商品编码
    /**
     * 根据包装材料种类id(key)获取 包装种类中文简称
     * @param element
     * @param obj
     */
//function selectPackTypeCodeCallBack(objValue){
//	$.ajax({
//		type : "POST",
//		url : swProxyBasePath+"sw/dec/ciq/getCiqPackTypeMap/"+objValue,
//		dataType : "json",
//		contentType : "application/json; charset=utf-8",
//		success : function(data) {
//			if(data){
//				var itemnameshort = data.itemnameshort;
//				if(itemnameshort){
//					$("#packTypeShort").val(itemnameshort);
//				}
//			}
//		}
//	});
});

