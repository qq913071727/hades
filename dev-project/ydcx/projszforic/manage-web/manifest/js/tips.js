var tipsJson = {},reminderTips=null,$reminder=$('#reminder');
layui.use(['layer'], function() {
    tipsJson={
    "voyageNo":"货物运输批次号：请输入大写字母或者数字",
    "customsCodeName":"进出境口岸海关代码：请在列表中选择",
    "carrierCode":"承运人代码：由字母、数字或“-”、“/”，“_”、半角空格构成，特殊字符最多出现一次且不能作为开头结尾",
    "transAgentCode":"运输工具代理企业代码：填写在直属海关备案的运输工具代理企业编码，4位关区号+9位组织机构代码 ",
    "loadingDate":"货物装载时间：时间精确到秒，格式：YYYYMMDDhhmmss",
    "loadingLocationCode":"装货地代码：填写监管场所海关备案代码或国内公路口岸代码，由大写字母或数字构成",
    "arrivalDate":"到达卸货地日期：格式：YYYYMMDD",
    "customMasterName":"传输企业备案关区：填写企业备案关区",
    "msgRepName":"舱单传输人名称：填写传输企业具体名称或组织机构代码 ",

    "billNo":"提（运）单号：由大写字母、数字或“@”构成，且“@”只能作为开头 ",
    "conditionCodeName":"运输条款：请在列表中选择 ",
    "paymentType":"运费支付方法：请在列表中选择",
    "goodsCustomsStat":"海关货物通关代码：请在列表中选择",
    "packNum":"货物总件数：最多可填写8位整数 ",
    "packType":"包装种类：请在列表中选择 ",
    "cube":"货物体积(M3)：最多可填写10位整数，单位立方米 ",
    "grossWt":"货物总毛重(KG)：最多可填写8位整数，3位小数，单位千克",
    "goodsValue":"货物价值：最多可填写14位整数，2位小数",
    "currencyType":"金额类型：请在列表中选择 ",
    "consolidatorId":"拼箱人代码：填写组织机构代码，由大写字母或数字构成 ",
    "routingContryIdText":"途径国家代码：请在列表中选择 ",
    "consigneeId":"收货人代码：填写实际收货人组织机构代码；收货人为自然人的填写身份证、护照号等",
    "communicationTypeText":"收货人通讯方式：请在列表中选择 ",
    "communicationTypeTextj":"收货具体联系人通讯方式类别代码：请在列表中选择 ",
    "consignorId":"发货人代码：填写发货人组织机构代码，发货人为自然人的填写身份证、护照号等 ",
    "communicationTypeTextf":"发货人通讯方式类别代码：请在列表中选择 ",
    "notifyId":"通知人代码：填写通知人组织机构代码，发货人为自然人的填写身份证、护照号等",
    "notifyName":"通知人名称：若有通知人则必填，否则不可填写 ",
    "notifyAddr":"通知人地址（街道,邮箱：若填报了通知人名称则必填 ",
    "communicationTypeTextt":"通知人通讯方式类别代码：请在列表中选择 ",
    "communicationTypeTextw":"危险品联系人通讯方式类别代码：请在列表中选择 ",

    "contaId":"集装箱(器)编号 ：由大写字母或数字构成，只允许出现一次“-”且不能作为开头和结尾 ",

    "goodsPackNum":"商品项件数：最多可填写8位整数",
    "goodsPackType":"包装种类：请在列表中选择 ",
    "goodsGrossWt":"商品项毛重(KG)：最多可填写8位整数，3位小数，单位千克",
    "goodsBriefDesc":"商品项简要描述：填写足以鉴别货物性质的描述信息",
    "HSCode":"HS编码：至少录入三位数字按Enter键调出列表",
    "undgNoName":"危险品编号：请录入2位数字调出参数表，或直接录入4位数字"
    };
});
$('body').on('focus','input,textarea',function(e){
    var tipText = '';
    try{tipText = tipsJson[this.id];}catch (e){}
    if(isNotEmpty(tipText)){
        $reminder.html(tipText);
        // layer.close(reminderTips);
        // if(isEmpty(this.value)){
        //     var wz = ($(this).hasClass('dropUp')?4:1);
        //     reminderTips = layer.tips(tipText, this,{tips: [wz, '#1E9FFF'], time: 5000,tipsMore: true});
        // }
    }
}).on('blur','input,textarea',function(e){
    // layer.close(reminderTips);
    $reminder.html('');
});
function tipsMessage(innerText,ele,flag){
    if(flag){
        setTimeout(function(){
            layer.tips(innerText,ele,{tips: [2,'#ff0000'], tipsMore: true, time:2000});
        },500);
    }else{
        layer.tips(innerText,ele,{tips: [2,'#ff0000'], tipsMore: true, time:2000});
    }
}