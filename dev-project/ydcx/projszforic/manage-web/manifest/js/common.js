var backUrl = '',NUMREGULAR = {'1':/^\d*?\.?\d{0,1}?$/, '2':/^\d*?\.?\d{0,2}?$/, '3':/^\d*?\.?\d{0,3}?$/, '4':/^\d*?\.?\d{0,4}?$/, '5':/^\d*?\.?\d{0,5}?$/, '6':/^\d*?\.?\d{0,6}?$/};
var comFn = {
    checkNum:function($that){
        $that.val($that.val().replaceAll(',',""));
        if(isNotEmpty($that.attr('NUM'))){
            if (!$that.val().match(NUMREGULAR[$that.attr('NUM')])){
                $that.val($that.attr('t_value'));
            }else{
                $that.attr({'t_value':$that.val()});
            }
            if ($that.val().match(NUMREGULAR[$that.attr('NUM')])){
                $that.attr({'o_value':$that.val()});
            }
        }else{
            $that.val($that.val().replace(/\D/g, ""));
        }
    },
    digitalControl:function () {
        $('input[NUM]').bind('keyup',function(e){
            comFn.checkNum($(this));
        }).bind('blur',function(e){
            comFn.checkNum($(this));
        }).bind('focus',function(e){
            $(this).select();
        });
    }
};
backUrl = $(window.frameElement).attr('src');
$('.goback').attr({'url':backUrl});
comFn.digitalControl();
function verificationTips($inp,msg,esize) {tipsMessage(msg,$inp);$inp.addClass('layui-form-danger').one('blur',function (){$inp.removeClass('layui-form-danger');});if(esize==0){$inp.select();}}

function tabCheckboxChoice(obj,tabId) {
    var inx = obj.tr.attr('data-index'),rchecked=false;
    $.each(layui.table.cache[tabId], function (i, o) {
        var index = o['LAY_TABLE_INDEX'],
            $tr = $('.layui-table-view[lay-id="' + tabId + '"] tr[data-index=' + index + ']');
        var $itemCheckbox = $tr.find('input[type="checkbox"]');
        if(inx==index){
            if(o["LAY_CHECKED"]){
                o["LAY_CHECKED"] = false;
                $itemCheckbox.prop('checked', false);
                $itemCheckbox.next().removeClass('layui-form-checked');
                $tr.removeClass('layui-table-click');
            }else{
                o["LAY_CHECKED"] = true;
                $itemCheckbox.prop('checked', true);
                $itemCheckbox.next().addClass('layui-form-checked');
                $tr.addClass('layui-table-click');
                rchecked = true;
            }
        }
    });
    return rchecked;
}
function selectCustomer(cus){
    $('#customerId').val((cus)?cus.id:"");
}
function verificationTips($inp,msg,esize) {tipsMessage(msg,$inp);$inp.addClass('layui-form-danger').one('blur',function (){$inp.removeClass('layui-form-danger');});if(esize==0){$inp.select();}}
function verificationEmpty($inp,msg,esize) {
    if(isEmpty($inp.val())){
        verificationTips($inp,msg,esize);return 1;
    }
    return 0;
}
//保存数据基础验证
function saveDataVerification() {
    $('.layui-form-danger').removeClass('layui-form-danger');
    var esize = 0;
    esize += verificationEmpty($('#customerName'),'客户名称不能为空',esize);

    return esize == 0;
}