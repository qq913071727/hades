var auto1 = {
    "customsCodeName": {hid: 'customsCode',type:'CUS_CUSTOMS'},
    "customMasterName": {hid: 'customMaster',type:'CUS_CUSTOMS'},
    "transLocationIdName": {hid: 'transLocationIdCode',type:'CUS_CUSTOMS'},
    "conditionCodeName": {hid: 'conditionCode',type:'CUS_MFT8_CONTR_CAR_COND'},
    "paymentType": {hid: 'paymentTypeCode',type:'CUS_MFT8_ROAD_PAYMENT_METHOD'},
    "goodsCustomsStat": {hid: 'goodsCustomsStatCode',type:'CUS_MFT8_ROAD_CUSTOMS_STATUS',callback:transLocationId},
    "packType": {hid: 'packTypeCode',type:'CUS_MFT8_PACKAGING'},
    "goodsPackType": {hid: 'goodsPackTypeCode',type:'CUS_MFT8_PACKAGING'},
    "currencyType": {hid: 'currencyTypeCode',type:'CUS_MFT8_ROAD_CURR'},
    "undgNoName": {hid: 'undgNo',type:'CUS_MFT8_DAN_GOODS'}
};
function transLocationId() {
    var goodsCustomsStatCode = $('#goodsCustomsStatCode').val();
    if(goodsCustomsStatCode=="RD09"||goodsCustomsStatCode=="RD16"){
        $('#transLocationIdName').attr({'readonly':null}).removeClass('disabled');
    }else{
        $('#transLocationIdName').attr({'readonly':true}).addClass('disabled').val('');
        $('#transLocationIdCode').val('');
    }
}
function initAuto1(data){
    $.each(auto1,function (key,val) {
        var $input = $('#'+key);
        if($input.length>0){
            var source = [];
            $.each(data[val.type]||[],function (i,o) {
                source.push({'label':o.c+'-'+o.n,'value':o.n});
            });
            if(source.length>0){
                $input.autocompleter({
                    source:source,
                    style:removeNull(val.style),
                    limit:20,
                    focusOpen:(val.focusOpen),
                    hiddenId:val.hid,
                    dropUp:val.dropUp,
                    callback:val.callback
                });
            }
        }
    });
}
function initAutoHsCode(data) {
    var source = [];
    $.each(data||[],function (i,o) {
        o['label'] = o.c;
        source.push(o);
    });
    if(source.length>0){
        $('#hsCode').autocompleter({
            source:source,
            style:'min-width:400px',
            limit:5,
            focusOpen:false,
            minLength:2,
            template:'<div>{{c}} <span class="fr">{{n}}</span></div>'
        });
    }
}
function initBaseData() {
    var declDatas1 = localStorage.getItem("static_decl_datas1");
    if (isEmpty(declDatas1)) {
        $.post(rurl('system/customsData/formatData1'), {}, function (res) {
            if (isSuccessWarn(res)) {
                localStorage.setItem("static_decl_datas1", JSON.stringify(res.data));
                initAuto1(res.data);
            }
        });
    } else {
        initAuto1(JSON.parse(declDatas1));
    }

    var hsCodeDatas = localStorage.getItem("static_code_datas");
    if(isEmpty(hsCodeDatas)){
        $.post(rurl('system/customsCode/selectAll'),{},function (res) {
            if(isSuccessWarn(res)){
                localStorage.setItem("static_code_datas",JSON.stringify(res.data));
                initAutoHsCode(res.data);
            }
        });
    }else{
        initAutoHsCode(JSON.parse(hsCodeDatas));
    }
}
$(function () {
    $.get(rurl('system/version'),{},function (res) {
        if(isSuccess(res)){
            if(res.data != localStorage.getItem('static_version')){
                localStorage.setItem("static_code_datas","");
                localStorage.setItem("static_decl_datas1","");
                localStorage.setItem("static_decl_datas2","");
                localStorage.setItem("static_prod_traf_data","");
                localStorage.setItem("static_version",res.data);
            }
            initBaseData();
        }
    });
});