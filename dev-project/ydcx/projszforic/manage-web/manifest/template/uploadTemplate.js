layui.use(['layer','form','upload'], function () {
    var layer = layui.layer, form = layui.form, upload = layui.upload;
    form.on('select(templateType)', function(data){
        //变更下载地址（固定为xlsx格式，以_manifest结尾）
        $("#downloadTemplate").attr("href","/importTemp/manifest/" + data.value + "_manifest.xlsx");
    });

    upload.render({
        elem: '#uploadFile',size:2048,accept: 'file',url: rurl('declare/manifest/uploadManifestTemplate'),
        exts: 'pdf|xls|xlsx', acceptMime: 'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        data:{
            'cusIEFlag':$("#cusIEFlag").val(),
            'templateType':function (){return $('#templateType').val();}
        },
        before:function (res){
            if(isEmpty($("#cusIEFlag").val())){
                show_msg("请选择舱单类型");
                return false;
            }
            show_loading();
        },done: function(res){
            if(isSuccessWarn(res)){
                closeThisWin();
                if("I"==$('#cusIEFlag').val()){
                    parent.openwin('manifest/imp/add.html?smartId='+res.data,'进口原始舱单',{area:['0','0']});
                }else{
                    parent.openwin('manifest/exp/add.html?smartId='+res.data,'出口预配舱单',{area:['0','0']});
                }
            }
        },error:function (res) {
            hide_loading();
        }
    });
});