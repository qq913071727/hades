layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table,fileupload = layui.fileupload;

    timelinessList([]);
    table.on('tool(timeliness-list)', function(obj){
        var data = obj.data;
        if(obj.event === 'deDel'){
            layer.confirm('您确定要删除此行数据？', function(index){
                obj.del();
                timelinessList(obtainData());
                layer.close(index);
            });
        }
    });
    $('.save-btn').click(function () {
        if(checkedForm($('.tsf-page-content'))){
            show_loading();
            var base = $('#baseForm').serializeJson();
            var timeliness = obtainData();
            var timelinessArray = [];
            if(null != timeliness && timeliness.length > 0) {
                $.each(timeliness,function (i,o) {
                    timelinessArray.push(o.timeliness);
                });
            }
            base['timeliness'] = timelinessArray;
            postJSON('scm/domesticLogistics/add',base,function (res) {
                parent.reloadList();
                closeThisWin('添加成功');
            });
        }
    });
});

function timelinessList(data) {
    layui.table.render({
        elem: '#timeliness-list',limit:1000,id:'timeliness-list',text:{none:'<a href="javascript:" class="def-a" onclick="addRow()"><i class="fa fa-plus-circle"></i>添加一行</a>'}
        ,cols: [[
            {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
            ,{field: 'timeliness', title: '时效', width: 150,align:'form',templet:'#timeLinessTemp'}
            ,{field: 'oper', title: '操作', width: 65,toolbar: '#deOperaBar',fixed: 'right',align:'center'}
        ]]
        ,data: data
        ,done:function () {

        }
    });
}

function addRow() {
    var data = obtainData();
    data.push({timeliness:''});
    timelinessList(data);
}

function obtainData() {
    var data = []
        ,$timelinessForm = $('#timelinessForm')
        ,$timeliness = $timelinessForm.find('input[name="timeliness"]');
    for(var i=0;i<$timeliness.size();i++){
        var item = {
            timeliness:$($timeliness[i]).val()
        }
        data.push(item);
    }
    return data;
}