layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('scm/domesticLogistics/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'name', width:180, title: '物流名称',templet:'#nameTemp'}
                ,{field:'code', width:100, title: '编码',templet:'#codeTemp'}
                ,{field:'disabled', width:100,align:'form-switch',title: '状态',templet:'#disabledTemp'}
                ,{field:'locking', width:60,align:'center',title: '锁定',templet:'#lockingTemp'}
                ,{field:'timeliness', title: '时效要求'}
                ,{field:'dateUpdated',width:120, title: '修改时间'}
            ]]
        }
    });
    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/domesticLogistics/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });
});
