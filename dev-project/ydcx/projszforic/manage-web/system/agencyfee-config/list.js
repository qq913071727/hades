layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('scm/expressStandardQuote/page')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'name', width:200, title: '报价名称',templet:'#nameTemp'}
                ,{field:'quoteTypeDesc', width:100, title: '报价类型'}
                ,{field:'quoteStartTime', width:130, title: '开始时间'}
                ,{field:'quoteEndTime', width:130, title: '失效时间'}
                ,{field:'capped', width:180,title: '封顶标识',templet:'#cappedTemp'}
                ,{field:'disabled', width:100,align:'form-switch',title: '状态',templet:'#disabledTemp'}
            ]]
        }
    });
    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/expressStandardQuote/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                if(true == !$that.checked) {
                    show_msg('禁用后报价配置为按重量收费时将无法计算代理费');
                } else {
                    show_success('修改成功');
                }
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });
});

function showDetail(id) {
    openwin(rurl('system/agencyfee-config/detail.html?id='+id),'报价单详情',{area: ['1000', '600'],shadeClose:true});
}