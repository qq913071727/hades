layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table;
    var id = getUrlParam("id");
    show_loading();
    $('.sel-load').selLoad(function () {
        form.render('select');
        $.get(rurl("/scm/expressStandardQuote/info"),{"id":id},function (res) {
            if(isSuccess(res)) {
                var data = res.data;
                form.val("quoteForm",data.quote);
                $("#disabled").val(data.quote.disabled);
                $("#capped").val(data.quote.capped);
                var members = data.members || [];
                memberList(members);

                disableForm(form);
            }
        });
    });
});

function memberList(data) {
    layui.table.render({
        elem: '#quote-member-list',limit:1000,id:'quote-member-list',text:{none:'<a href="javascript:" class="def-a" onclick="addRow()"><i class="fa fa-plus-circle"></i>添加一行</a>'}
        ,cols: [[
            {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
            ,{field: 'startWeight', title: '开始重量', width: 150,align:'form',templet:'#startWeightTemp'}
            ,{field: 'endWeight', title: '结束重量', width: 150,align:'form',templet:'#endWeightTemp'}
            ,{field: 'basePrice', title: '基准价', width: 150,align:'form',templet:'#basePriceTemp'}
            ,{field: 'price', title: '单价', width: 150,align:'form',templet:'#priceTemp'}
        ]]
        ,data: data
        ,done:function () {

        }
    });
}

function obtainData() {
    var data = []
        ,$memberForm = $('#memberForm')
        ,$ids = $memberForm.find('input[name="id"]')
        ,$startWeight = $memberForm.find('input[name="startWeight"]')
        ,$endWeight = $memberForm.find('input[name="endWeight"]')
        ,$basePrice = $memberForm.find('input[name="basePrice"]')
        ,$price = $memberForm.find('input[name="price"]');
    for(var i=0;i<$ids.size();i++){
        var item = {
            startWeight:$($startWeight[i]).val(),
            endWeight:$($endWeight[i]).val(),
            basePrice:$($basePrice[i]).val(),
            price:$($price[i]).val()
        }
        data.push(item);
    }
    return data;
}
