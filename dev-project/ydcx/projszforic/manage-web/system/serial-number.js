layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listQueryArg:{
            url:rurl('scm/serialNumber/list'),page: false,limit:1000
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'name', title: '单据类型', width:240}
                ,{field:'prefix', title: '前缀', width:80}
                ,{field:'timeRuleName', title: '日期规则', width:200}
                ,{field:'serialLength', title: '流水号长度', width:80}
                ,{field:'serialClearName', title: '流水号重置方式', width:120}
                ,{field:'nowSerial', title: '当前流水号值', width:100}
                ,{field:'result', title: '规则结果'}
            ]]
        }
    });
});
