layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    form.on('switch(switchDisabled)', function(data){
        $('#disabled').val(!this.checked);
    });
    form.on('submit(save-btn)', function(data){
        formSub('scm/expenseSubject/add',function (res) {
            parent.reloadList();
            closeThisWin('添加成功');
        });
        return false;
    });
});
