layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    var id = getUrlParam('id');
    show_loading();
    $.get(rurl("/scm/expenseSubject/detail"),{"id":id},function (res) {
        if(isSuccess(res)) {
            var data = res.data;
            data['switchDisabled'] = !data.disabled;
            data['preId'] = data.id;
            form.val('form',data);

            $('.save-btn').click(function () {
                formSub('scm/expenseSubject/edit',function (sres) {
                    parent.reloadList();
                    closeThisWin('修改成功');
                });
                return false;
            });
        }
    });
    form.on('switch(switchDisabled)', function(data){
        $('#disabled').val(!this.checked);
    });
});
