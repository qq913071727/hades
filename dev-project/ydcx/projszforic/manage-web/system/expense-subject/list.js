layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('scm/expenseSubject/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'name', width:160, title: '费用科目名称',templet:'#nameTemp'}
                ,{field:'code', width:120, title: '费用科目编码',templet:'#codeTemp'}
                ,{field:'disabled', width:100,align:'form-switch',title: '状态',templet:'#disabledTemp'}
                ,{field:'locking', width:65,align:'center',title: '锁定',templet:'#lockingTemp'}
                ,{field:'memo', title: '备注'}
            ]]
        }
    });
    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/expenseSubject/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });
});
