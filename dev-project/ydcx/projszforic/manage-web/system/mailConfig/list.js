layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('scm/mail/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'mailHost', width:180, title: '邮箱服务器'}
                ,{field:'mailUser', width:150, title: '邮箱用户名'}
                ,{field:'mailPassword', width:150, title: '邮箱密码'}
                ,{field:'mailPort', width:180, title: '邮箱服务器端口'}
                ,{field:'disabled', width:100,align:'form-switch',title: '状态',templet:'#disabledTemp'}
                ,{field:'isDefault', width:100,align:'form-switch',title: '是否默认',templet:'#isDefaultTemp'}
            ]]
        }
    });
    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/mail/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!$that.checked)});
                form.render('checkbox');
            }
        });
    });
    form.on('switch(switchIsDefault)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/mail/setDefault'),{'id':data.value,'isDefault':$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
                reloadList();
            }else{
                $(data.elem).attr({'checked':(!$that.checked)});
                form.render('checkbox');
            }
        });
    });
});
