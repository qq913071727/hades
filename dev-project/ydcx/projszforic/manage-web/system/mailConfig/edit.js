layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    var id = getUrlParam('id');
    show_loading();
    $.get(rurl("/scm/mail/detail"),{"id":id},function (res) {
        if(isSuccess(res)) {
            var data = res.data;
            data['switchIsDefault'] = data.isDefault;
            form.val('form',data);

            $('.save-btn').click(function () {
                formSub('scm/mail/edit',function (sres) {
                    parent.reloadList();
                    closeThisWin('修改成功');
                });
                return false;
            });
        }
    });
    form.on('switch(switchIsDefault)', function(data){
        $('#isDefault').val(this.checked);
    });
    $(".test-btn").click(function () {
        if(checkedForm($('.tsf-page-content'))){
            show_loading();
            $.post(rurl('scm/mail/checkConnect'),{"mailHost":$("#mailHost").val(),"mailUser":$("#mailUser").val(),"mailPassword":$("#mailPassword").val(),"mailPort":$("#mailPort").val()},function (res) {
                if(isSuccess(res)) {
                    if(res.data) {
                        prompt_success("恭喜您，邮箱配置连接正常");
                    } else {
                        prompt_warn("对不起，您的邮箱配置连接有误，本次您依然可以保存，为了后续能够正常发送邮件，请配置成正确的邮箱信息");
                    }
                }
            });
        }
    });
});
