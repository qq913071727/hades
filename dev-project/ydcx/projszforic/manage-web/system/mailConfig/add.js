layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    //邮箱端口默认25
    $("#mailPort").val(25);
    form.on('switch(switchIsDefault)', function(data){
        $('#isDefault').val(this.checked);
    });
    form.on('submit(save-btn)', function(data){
        formSub('scm/mail/add',function (res) {
            parent.reloadList();
            closeThisWin('添加成功');
        });
        return false;
    });
    $(".test-btn").click(function () {
        if(checkedForm($('.tsf-page-content'))){
            show_loading();
            $.post(rurl('scm/mail/checkConnect'),{"mailHost":$("#mailHost").val(),"mailUser":$("#mailUser").val(),"mailPassword":$("#mailPassword").val(),"mailPort":$("#mailPort").val()},function (res) {
                if(isSuccess(res)) {
                    if(res.data) {
                        prompt_success("恭喜您，邮箱配置连接正常");
                    } else {
                        prompt_warn("不好意思，您的邮箱配置连接有误，为了后续能够正常发送邮件，请配置成正确的邮箱信息");
                    }
                }
            });
        }
    });
});
