layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listQueryArg:{
            url:rurl('scm/notice/admin/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'operation', width:60, title: '操作',align:'center',templet: '#operationTemp'}
                ,{field:'publishTime', width:120,align:'center', title: '发布日期'}
                ,{field:'isOpen', width:60,align:'center', title: '是否公开',templet:'#isOpenTemp'}
                ,{field:'isLoginSee', width:60,align:'center', title: '登录查看',templet:'#isLoginSeeTemp'}
                ,{field:'isPopup', width:60,align:'center', title: '弹出显示',templet:'#isPopupTemp'}
                ,{field:'categoryDesc', width:60,title: '分类'}
                ,{field:'title', title: '标题'}
            ]]
        }
    });
});


function remove() {
    let ids = getIds();
    if(ids == null || ids.length > 1) {
        layer.msg("请单选一条记录");
        return;
    }
}