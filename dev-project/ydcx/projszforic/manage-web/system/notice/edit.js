layui.use(['form', 'layer','laydate','table','upload'], function () {
    var layer = layui.layer, form = layui.form, laydate = layui.laydate,table = layui.table,upload = layui.upload;
    backUrl = $(window.frameElement).attr('src');
    window.UEDITOR_CONFIG.serverUrl = "/scm/ueditorHandler/handle";
    var id = getUrlParam("id");
    var ue = UE.getEditor('contentUe');
    show_loading();
    $('.sel-load').selLoad(function(){
        form.render('select');
        if(!isEmpty(id)){
            $.get(rurl('scm/notice/info?id='+id),{},function (res) {
                if(isSuccess(res)){
                    var data = res.data;
                    form.val('form',data);

                    if(!isEmpty(data.banner)) {
                        $("#notice-banner").attr('src',data.banner);
                        $("#notice-banner").show();
                    } else {
                        $("#notice-banner").hide();
                    }
                    ue.ready(function() {
                        ue.setContent(data.content);
                    });

                }
            });
        }else{
            hide_loading();
        }

        form.on('switch(switchIsLoginSee)', function(data){
            $('#isLoginSee').val(this.checked);
        });
        form.on('switch(switchIsPopup)', function(data){
            $('#isPopup').val(this.checked);
        });

    });

    let uploadFileId = id || 'temp_'+uuid();
    let fileObj = {docId:uploadFileId, docType:'notice'};
    upload.render({
        elem: '#upload-banner',size: 2048,exts: 'jpg|png'
        ,acceptMime:'image/jpeg,image/png'
        ,url: '/scm/file/upload'
        ,data:{docId:fileObj.docId,docType:fileObj.docType,businessType:'notice_banner'}
        ,before: function () {
            show_loading();
        }
        ,done: function(res){
            if(isSuccess(res)) {
                $("#notice-banner").show();
                $("#notice-banner").attr('src','/scm/download/file/'+ res.data.id);
                $("#banner").val('/scm/download/file/'+ res.data.id);
            } else {
                $("#notice-banner").hide();
                layer.msg("上传失败，请您稍后再试");
            }
        }
        ,error: function(){
            hide_loading();
        }
    });

    form.on('submit(save-temporary)', function(data){
        $('#isOpen').val(false);
        $('#content').val(ue.getContent());
        saveNotice(id,fileObj);return false;
    });
    form.on('submit(save-publish)', function(data){
        $('#isOpen').val(true);
        $('#content').val(ue.getContent());
        saveNotice(id,fileObj);return false;
    });
    
    $("#delete-banner").click(function () {
        $("#notice-banner").attr('src','');
        $("#notice-banner").hide();
        $("#banner").val('');
    });

});
function saveNotice(id,fileObj){
    let requestUrl = !isEmpty(id) ? 'scm/notice/edit' : 'scm/notice/add';
    let param = $('.layui-form').serializeJson();
    param = Object.assign(param,fileObj);
    postJSON(requestUrl,param,function (res) {
        parent.reloadList();
        closeThisWin('保存成功');
    });
}