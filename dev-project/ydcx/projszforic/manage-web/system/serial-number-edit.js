layui.use(['form','layer'], function () {
    var layer = layui.layer, form = layui.form;
    var id = getUrlParam("id");
    $('.sel-load').selLoad(function () {
        show_loading();
        $.get(rurl('scm/serialNumber/detail'),{'id':id},function (res) {
            if (isSuccess(res)) {
                var data = res.data;
                form.val('form',data);
                form.render('select');
            }
        });
    });
    form.on('submit(save-btn)', function(data){
        formSub('scm/serialNumber/edit',function (res) {
            parent.reloadList();
            closeThisWin('修改成功');
        });
        return false;
    });
});