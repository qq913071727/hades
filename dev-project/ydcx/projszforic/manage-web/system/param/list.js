layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('scm/sysParam/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'id', width:160, title: '参数名',templet:'<div><span title="{{d.idDesc}}";display:block;>{{d.id}}</span></div>'}
                ,{field:'val', width:300, title: '参数值'}
                ,{field:'disabled', width:100,align:'form-switch',title: '状态',templet:'#disabledTemp'}
                ,{field:'locking', width:65,align:'center',title: '锁定',templet:'#lockingTemp'}
                ,{field:'remark', title: '说明'}
            ]]
        }
    });
    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/sysParam/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });
});
function editConfig(o) {
    var datas = getDatas();
    if(datas.length!=1){show_msg('请选择一条您要修改的数据');return;}
    var item = datas[0];
    switch (item.id){
        case 'imp_fob':
            openwin('system/param/imp_fob.html?id='+item.id,o.name,{area:['800px', '440px']});
            break;
        default:
            openwin('system/param/edit.html?id='+item.id,o.name,{area:['800px', '320px']});
            break;
    }
}