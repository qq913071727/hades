layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    var id = getUrlParam('id');
    $('.sel-load').selLoad(function () {
        form.render('select');
        $.get(rurl("/scm/sysParam/detail"),{"id":id},function (res) {
            if(isSuccess(res)) {
                var data = res.data;
                data['switchDisabled'] = !data.disabled;
                data['preId'] = data.id;
                form.val('form',data);

                $("#id").attr("disabled","disabled");
                form.render('select');

                $('.save-btn').click(function () {
                    formSub('scm/sysParam/edit',function (sres) {
                        parent.reloadList();
                        closeThisWin('修改成功');
                    });
                    return false;
                });
            }
        });
    });
    form.on('switch(switchDisabled)', function(data){
        $('#disabled').val(!this.checked);
    });
});
