layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    var id = getUrlParam('id');
    $('.sel-load').selLoad(function () {
        form.render('select');
        $.get(rurl("/scm/sysParam/detail"),{"id":id},function (res) {
            if(isSuccess(res)) {
                var data = res.data;
                data['idName'] = data.id;
                var vals = data.val.split(',');

                var transCosts = vals[0].split('/');
                data.feeMark = transCosts[2];
                data.feeMarkName = markMap[data.feeMark];
                data.feeRate = transCosts[1];
                data.feeCurr = transCosts[0];
                data.feeCurrName = curMap[data.feeCurr];

                var insuranceCosts = vals[1].split('/');
                data.insurMark = insuranceCosts[2];
                data.insurMarkName = markMap[data.insurMark];
                data.insurRate = insuranceCosts[1];
                data.insurCurr = insuranceCosts[0];
                data.insurCurrName = curMap[data.insurCurr];

                var miscCosts = vals[2].split('/');
                data.otherMark = miscCosts[2];
                data.otherMarkName = markMap[data.otherMark];
                data.otherRate = miscCosts[1];
                data.otherCurr = miscCosts[0];
                data.otherCurrName = curMap[data.otherCurr];

                form.val('form',data);
                $("#idName").attr("disabled","disabled");
                form.render('select');

                $('.save-btn').click(function () {

                    var $feeCurrName = $('#feeCurrName');
                    if($('#feeMark').val()=="1"){
                        $('#feeCurr').val('000');
                        $feeCurrName.val('');
                    }else{
                        if(isEmpty($feeCurrName.val())){show_error("必填项不能为空");$feeCurrName.select();return false;}
                    }

                    var $insurMarkName = $('#insurMarkName');if(isEmpty($insurMarkName.val())){show_error("必填项不能为空");$insurMarkName.select();return false;}
                    var $insurRate = $('#insurRate');if(isEmpty($insurRate.val())){show_error("必填项不能为空");$insurRate.select();return false;}
                    var $insurCurrName = $('#insurCurrName');
                    if($('#insurMark').val()=="1"){
                        $('#insurCurr').val('000');
                        $insurCurrName.val('');
                    }else{
                        if(isEmpty($insurCurrName.val())){show_error("必填项不能为空");$insurCurrName.select();return false;}
                    }

                    var $otherMark = $('#otherMark');if(isEmpty($otherMark.val())){$otherMark.val('1');}
                    var $otherRate = $('#otherRate');if(isEmpty($otherRate.val())){$otherRate.val('0.0');}
                    var $otherCurr = $('#otherCurr');if(isEmpty($otherCurr.val())){$otherCurr.val('000');}

                    if($otherMark.val()!='1'){
                        var $otherCurrName = $('#otherCurrName');
                        if(isEmpty($otherCurrName.val())){
                            show_error("必填项不能为空");$otherCurrName.select();return false;
                        }
                    }

                    var nvals = [];
                    nvals.push($('#feeCurr').val()+"/"+$('#feeRate').val()+"/"+$('#feeMark').val());
                    nvals.push($('#insurCurr').val()+"/"+$('#insurRate').val()+"/"+$('#insurMark').val());
                    nvals.push($('#otherCurr').val()+"/"+$('#otherRate').val()+"/"+$('#otherMark').val());
                    $('#val').val(nvals.join(','));
                    formSub('scm/sysParam/edit',function (sres) {
                        parent.reloadList();
                        closeThisWin('修改成功');
                    });
                    return false;
                });
            }
        });
    });
    form.on('switch(switchDisabled)', function(data){
        $('#disabled').val(!this.checked);
    });
});
var curMap = {"000":"", "AUD":"澳大利亚元", "CAD":"加拿大元", "CHF":"瑞士法郎", "CNY":"人民币", "DKK":"丹麦克朗", "EUR":"欧元", "GBP":"英镑", "HKD":"港币", "IDR":"印度尼西亚卢比", "JPY":"日本元", "KRW":"韩国元", "MOP":"澳门元", "MYR":"马来西亚林吉特", "NOK":"挪威克朗", "NZD":"新西兰元", "PHP":"菲律宾比索", "RUB":"俄罗斯卢布", "SEK":"瑞典克朗", "SGD":"新加坡元", "THB":"泰国铢", "TWD":"新台币", "USD":"美元"}
markMap = {"1":"率","2":"单价","3":"总价"};