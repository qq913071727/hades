layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    var id = getUrlParam('id');
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    $.get(rurl("/scm/warehouse/detail"),{"id":id},function (res) {
        if(isSuccess(res)) {
            var data = res.data;
            data['switchIsSelfMemtion'] = data.isSelfMention;
            form.val('form',data);

            form.render('select');
            showSelfMemtion(data.warehouseType);
        }
    });

    form.on('submit(save-btn)', function(data){
        formSub('scm/warehouse/edit',function (res) {
            parent.reloadList();
            closeThisWin('修改成功');
        });
        return false;
    });

    form.on('switch(switchIsSelfMemtion)', function(data){
        $('#isSelfMention').val(this.checked);
    });

    form.on('select(warehouseType)', function(data){
        //只有仓库类型为境内仓库时才能选择国内自提
        showSelfMemtion(data.value);
    });
});

function showSelfMemtion(value) {
    if("domestic" != value) {
        $(".isSelfMentionDiv").attr("style","display:none");
        $("#isSelfMention").val(false);
    } else {
        $(".isSelfMentionDiv").attr("style","display:block");
    }
}
