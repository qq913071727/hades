layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listQueryArg:{
           url:rurl('scm/warehouse/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'warehouseTypeDesc', width:100, title: '仓库类型'}
                ,{field:'code', width:100, title: '仓库编码'}
                ,{field:'name', width:160, title: '仓库名称'}
                ,{field:'companyName', width:180, title: '公司名称'}
                ,{field:'address', width:180, title: '仓库地址'}
                ,{field:'linkPerson', width:100, title: '联系人'}
                ,{field:'linkTel', width:120, title: '联系电话'}
                ,{field:'nameEn', width:180, title: '仓库英文名称'}
                ,{field:'addressEn', width:180, title: '仓库英文地址'}
                ,{field:'disabled', width:100,align:'form-switch',title: '状态',templet:'#disabledTemp'}
                ,{field:'isSelfMemtion', width:100,align:'form-switch',title: '国内自提',templet:'#isSelfMemtionTemp'}
                ,{field:'memo', title: '备注'}
            ]]
        }
    });
    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/warehouse/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });
});
