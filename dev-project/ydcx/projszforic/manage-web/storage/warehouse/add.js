layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    form.on('switch(switchIsSelfMemtion)', function(data){
        $('#isSelfMention').val(this.checked);
    });
    form.on('select(warehouseType)', function(data){
        //只有仓库类型为境内仓库时才能选择国内自提
        showSelfMemtion(data.value);
    });
    form.on('submit(save-btn)', function(data){
        formSub('scm/warehouse/add',function (res) {
            parent.reloadList();
            closeThisWin('添加成功');
        });
        return false;
    });
});


function showSelfMemtion(value) {
    if("domestic" != value) {
        $(".isSelfMentionDiv").attr("style","display:none");
        $("#isSelfMention").val(false);
    } else {
        $(".isSelfMentionDiv").attr("style","display:block");
    }
}