layui.use(['layer','form','table'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table;
    var deliveryNoteId = getUrlParam('deliveryNoteId');
    if(isEmpty(deliveryNoteId)){
        closeThisWin('请先保存出库单');
    }
    $('#warehouseId').val(getUrlParam('warehouseId'));
    $('#customerId').val(getUrlParam('customerId'));
    table.render({
        elem:'#table-list',id:'table-list',limits: [100,200,500],limit:100,page:true,height: 'full-106',method:'post',where:$('.query-form').serializeJson(),
        url:'/wms/stock/list',response:{msgName:'message',statusCode: '1'}
        ,cols: [[
            {checkbox: true, fixed: true}
            ,{field:'receivingDate', width:120, title: '入库时间'}
            ,{field:'docNo', width:110, title: '入库单号'}
            ,{field:'model', width:160, title: '型号'}
            ,{field:'brand', width:120, title: '品牌'}
            ,{field:'name', width:120, title: '名称'}
            ,{field:'batchNo', width:130, title: '批次号'}
            ,{field:'quantity', width:100, title: '入库数量',align:'right'}
            ,{field:'stockQuantity', width:100, title: '库存数量',align:'right'}
            ,{field:'outQuantity', width:100, title: '出库数量',templet:'#outQuantityTemp',fixed:'right'}
        ]],done:function (){
            initInput();
        }
    });
    $('.search-btn').click(function (){
        table.reload('table-list',{page:{curr:1},where:$('.query-form').serializeJson()});
    });
    $('.save-btn').click(function () {
        var data = table.checkStatus('table-list').data;
        if(data.length==0){
            show_error('清选择您要出库的产品');return;
        }
        var members = [];
        for(var i=0;i<data.length;i++){
            var item = data[i],$outQuantity = $('#qty'+item.id);
            if(isEmpty($outQuantity.val())){
                show_error('型号：'+item.model+'请输入出库数量');return;
            }
            var outQuantity = Math.round(parseFloat($outQuantity.val())*100)/100;
            if(outQuantity==0||outQuantity>item.stockQuantity){
                show_error('型号：'+item.model+'出库数量必须大于零且小于当前库存数量');return;
            }
            members.push({
                id:item.id,
                quantity:outQuantity
            })
        }
        postJSON('/wms/deliveryNoteMember/addMembers',{
            deliveryNoteId:deliveryNoteId,
            members:members
        },function (res){
            show_success('添加成功');
            $('.search-btn').click();
            parent.obtainDetail(deliveryNoteId);
        });

    });
});
