layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('wms/stock/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'receivingDate', width:120, title: '入库时间', fixed: true}
                    ,{field:'docNo', width:126, title: '入库单号', fixed: true}
                    ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                    ,{field:'model', width:120, title: '型号'}
                    ,{field:'brand', width:120, title: '品牌'}
                    ,{field:'name', width:120, title: '名称'}
                    ,{field:'batchNo', width:120, title: '批次号'}
                    ,{field:'quantity', width:100, title: '入库数量',align:'right'}
                    ,{field:'stockQuantity', width:100, title: '库存数量',align:'right'}

                ]]
            },listQueryDone:function () {

            }
        });
    });
});
function fullCustomerData(data) {
    $('#customerId').val(data.id);
}