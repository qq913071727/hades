layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    show_loading();
    $('.sel-load').selLoad(function () {
        form.render('select');
        generateDocNo();
        form.val('form',{'receivingDate':getNowFormatTime(),'warehouseId':getLocalStorage('def_abroad_warehouse_id')});
    });
    var $expressInfo = $('#expressInfo');
    form.on('select(deliveryMode)', function(data){
         if('ExpressDelivery'==data.value){
             $expressInfo.show();
         }else{
             $expressInfo.hide();
         }
    });
    form.on('select(warehouseId)', function(data){
         setLocalStorage('def_abroad_warehouse_id',data.value);
    });
    form.on('submit(save-btn)', function(data){
        saveData(false);
        return false;
    });
    form.on('submit(save-print-btn)', function(data){
        saveData(true);
        return false;
    });
    var saveData = function(print){
        formSubJson('scm/receivingNote/impRegister',function (res) {
            parent.reloadList();
            if(print){
                parent.autoPrintLable(res.data);
            }
            closeThisWin('登记成功');
        });
    }
    $('.auto-imp-order').autocomplete({
        source: function (request, response) {
            $.post(rurl('scm/impOrder/selectByOrderNo'),{orderNo: trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: item.orderNo+'('+item.customerName+')', value: item.orderNo, data: item}}));
            });
        }, minLength: 0,autoFocus:true, delay: 500,
        select: function(event, ui){
            $('#customerName').val(ui.item.data.customerName);
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
});
function generateDocNo() {
    show_loading();
    $.post(rurl('scm/serialNumber/generateDocNo'),{'type':'abroad_receiving_note'},function (res) {
        if(isSuccess(res)){
            $('#docNo').val(res.data);
        }
    });
}