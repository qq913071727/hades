layui.config({base: '/wms/receivingNote/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#abroadDetail').load(rurl('wms/receivingNote/detail-temp.html'),function () {
        detailTemp.render(id,'');
    });
});