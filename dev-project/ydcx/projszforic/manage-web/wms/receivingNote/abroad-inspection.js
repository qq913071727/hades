var $quantityTotal=$('#quantityTotal'),$cartonTotal=$('#cartonTotal'),$netWeightTotal=$('#netWeightTotal'),$grossWeightTotal=$('#grossWeightTotal');
layui.use(['layer','laytpl','element','table'], function() {
    var layer = layui.layer, laytpl = layui.laytpl, element = layui.element, table = layui.table;
    var id = getUrlParam("id");
    show_loading();
    $.post(rurl('scm/receivingNote/inspectionDetail'),{'id':id},function (res) {
        if(isSuccessWarnClose(res)){
            var data = res.data,receivingNote=data.receivingNote,members=data.members;

            laytpl($('#mainViewTemp').html()).render(receivingNote,function (html) {
                $('#mainView').html(html);
            });
            createRows(members || []);
            $('.save-btn').click(function () {
                var pmembers = obtainMenberJSON();
                postJSON('scm/receivingNote/add',{"id":receivingNote.id,"members":pmembers},function (ires) {
                    parent.reloadList();
                    closeThisWin('保存成功');
                });
            });
        }
    });
    $('.addrow-btn').click(function () {createRow();});
});
//订单明细
function obtainMenberJSON() {
    var members = [],orderMember = $('#memberForm').serializeJson(),keys=[];
    for(var key in orderMember){keys.push(key);}
    $.each(orderMember[keys[0]] ||[],function (i,o) {
        var item = {};
        $.each(keys,function (j,k) {
            item[k] = orderMember[k][i];
        });
        members.push(item);
    });
    return members;
}
function createRows(members) {
    var mhtml = [],tquantity = 0.0,tcartonNum = 0,tnetWeight = 0.0,tgrossWeight = 0.0;
    $.each(members,function (i,o) {
        mhtml.push(createRowHtml(o.id,o.model,o.name,o.brand,o.unitName,o.quantity,o.bindQuantity,o.cartonNum,o.cartonNo,o.netWeight,o.grossWeight,o.countryName,o.goodsCode,o.orderNo,o.isLock));
        tquantity+=o.quantity;tcartonNum+=o.cartonNum;tnetWeight+=o.netWeight;tgrossWeight+=o.grossWeight;
    });
    $('#materialBody').append(mhtml.join(''));
    $quantityTotal.html(Math.round(tquantity*100)/100);
    $cartonTotal.html(tcartonNum);
    $netWeightTotal.html(Math.round(tnetWeight*10000)/10000);
    $grossWeightTotal.html(Math.round(tgrossWeight*10000)/10000);
    autoAllModel();
    autoUnit();
    autoCountry();
    initInput();
    createIndex();
}
function createRow(mid,goodsModel,goodsName,goodsBrand,unitName,quantity,bindQuantity,cartonNum,cartonNo,netWeight,grossWeight,countryName,goodsCode,orderNo,isLock) {
    $('#materialBody').append(createRowHtml(mid,goodsModel,goodsName,goodsBrand,unitName,quantity,bindQuantity,cartonNum,cartonNo,netWeight,grossWeight,countryName,goodsCode,orderNo,isLock));
    autoAllModel();
    autoUnit();
    autoCountry();
    initInput();
    createIndex();
    $('.tabBody').scrollTop(10000);
}
function createRowHtml(mid,goodsModel,goodsName,goodsBrand,unitName,quantity,bindQuantity,cartonNum,cartonNo,netWeight,grossWeight,countryName,goodsCode,orderNo,isLock) {
    return [
        '<tr><td class="t-a-c mindex"></td>',
        '<td><input type="hidden" name="id" LIST value="'+(isEmpty(mid)?'':mid)+'"/><input type="text" name="model" LIST class="layui-input required auto-customer-model" placeholder="型号" value="'+(isEmpty(goodsModel)?'':goodsModel)+'"/></td>',
        '<td><input type="text" name="name" LIST class="layui-input" value="'+(isEmpty(goodsName)?'':goodsName)+'"/></td>',
        '<td><input type="text" name="brand" LIST class="layui-input required" value="'+(isEmpty(goodsBrand)?'':goodsBrand)+'"/></td>',
        '<td><input type="text" name="unitName" LIST class="layui-input auto-unit required" value="'+(isEmpty(unitName)?'个':unitName)+'"/></td>',
        '<td><input type="text" name="quantity" LIST onblur="calculationAll(this)" NUMBER class="layui-input required" value="'+(isEmpty(quantity)?'':quantity)+'"/></td>',
        '<td class="t-a-r pr5">'+(isEmpty(bindQuantity)?'0.00':bindQuantity)+'</td>'+
        '<td><input type="text" name="cartonNum" LIST onblur="calculationAll()" INT class="layui-input required" value="'+(isEmpty(cartonNum)?'0':cartonNum)+'"/></td>',
        '<td><input type="text" name="cartonNo" LIST  class="layui-input" value="'+(isEmpty(cartonNo)?'':cartonNo)+'"/></td>',
        '<td><input type="text" name="netWeight" LIST NUMBER4 onblur="calculationAll()" class="layui-input required" value="'+(isEmpty(netWeight)?'0.0000':netWeight)+'"/></td>',
        '<td><input type="text" name="grossWeight" LIST NUMBER4 onblur="calculationAll()" class="layui-input required" value="'+(isEmpty(grossWeight)?'0.0000':grossWeight)+'"/></td>',
        '<td><input type="text" name="countryName" LIST class="layui-input auto-country required" value="'+(isEmpty(countryName)?'中国':countryName)+'"/></td>',
        '<td><input type="text" name="goodsCode" LIST class="layui-input" value="'+(isEmpty(goodsCode)?'':goodsCode)+'"/></td>',
        '<td class="pl5">'+removeNull(orderNo)+'</td>',
        '<td>&nbsp;&nbsp;'+(isLock?'':'<a href="javascript:" onclick="deleteRow(this)"><i class="fa fa-trash-o"></i></a>')+'</td></tr>'
    ].join('');
}
function autoAllModel() {
    $('.auto-customer-model').autocomplete({
        source: function (request, response) {
            $.post(rurl('scm/materiel/vague'),{customerName:'',model: trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: '型号：'+item.model+' 品牌：'+item.brand+' 品名：'+item.name, value: item.model, data: item}}));
            });
        }, minLength: 0,autoFocus:true, delay: 500,
        select: function( event, ui){
            try{var data = ui.item.data;fullCustomerModelData(this,data);}catch (e) {}
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
}
function createIndex() {
    var mind = 1;
    $.each($('.mindex'),function (i,o) {
        $(o).html(mind++);
    });
}
function deleteRow(e) {
    var $per = $(e).parent().parent();
    $per.remove();
    createIndex();
    calculationAll();
}
function calculationAll(e) {
    var tquantity = 0.0,tcartonNum = 0,tnetWeight = 0.0,tgrossWeight = 0.0;
    $.each($('#materialBody').find('tr'),function (i,o) {
        var $that = $(o);
        tquantity += Math.round(parseFloat($that.find('input[name="quantity"]').val() || '0.0')*100)/100;
        tcartonNum += parseInt($that.find('input[name="cartonNum"]').val() || '0');
        tnetWeight += Math.round(parseFloat($that.find('input[name="netWeight"]').val() || '0.0')*10000)/10000;
        tgrossWeight += Math.round(parseFloat($that.find('input[name="grossWeight"]').val() || '0.0')*10000)/10000;
    });
    $quantityTotal.html(Math.round(tquantity*100)/100);
    $cartonTotal.html(tcartonNum);
    $netWeightTotal.html(Math.round(tnetWeight*10000)/10000);
    $grossWeightTotal.html(Math.round(tgrossWeight*10000)/10000);
}