layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');

        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('scm/receivingNote/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                    ,{field:'docNo', width:126, title: '入库单号',templet:'#docNoTemp', fixed: true}
                    ,{field:'receivingDate', width:120, title: '入库时间'}
                    ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                    ,{field:'fileList', width:94, title: '附件资料',align:'center',templet:'#fileListTemp'}
                    ,{field:'warehouseName', width:100, title: '仓库'}
                    ,{field:'deliveryModeName', width:80, title: '入库方式'}
                    ,{field:'express', width:150, title: '快递信息',templet:'#expressTemp'}
                    ,{field:'totalCartonNum', width:80, title: '总箱数'}
                    ,{field:'orderNo', width:120, title: '订单号'}
                    ,{field:'supplierName', width:260, title: '境外供应商'}
                    ,{field:'memo',width:260, title: '备注'}
                ]]
            },listQueryDone:function () {
                initFileSize();
            }
        });
    });

});
function showDetail(id) {
    openwin(rurl('wms/receivingNote/abroad-detail.html?id='+id),'入库单详情',{area: ['0', '0'],shadeClose:true});
}
function printLable(o) {
    autoPrintLable(getIds()[0]);
}
function autoPrintLable(id) {
    show_loading();$.post(rurl('scm/receivingNote/printLable'),{'id':id},function (res) {if(isSuccess(res)){exportFile(res.data);}});
}