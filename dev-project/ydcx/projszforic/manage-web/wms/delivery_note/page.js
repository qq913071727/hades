layui.use(['layer','form','table'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table;
    $('.sel-load').selLoad(function () {
        form.render('select');
        var id = getUrlParam('id');
        if(isNotEmpty(id)){
            obtainDetail(id);
        }else{
            //生成单号
            generateDocNo();
            $('#deliveryDate').val(getNowFormatTime());
            fullMember([]);
        }
    });
});
function obtainDetail(id){
    show_loading();
    $.get('/wms/deliveryNote/detail',{id:id},function (res){
        if(isSuccessWarnClose(res)){
            layui.form.val('mainForm',res.data.deliveryNote);
            fullMember(res.data.deliveryNoteMembers);
        }
    });
}
function saveDeliveryNote(isOpen){
    if(!checkedForm($('#mainForm'))){return;}
    var params = $('#mainForm').serializeJson();
    postJSON('wms/deliveryNote/save',params,function (res){
        $('#id').val(res.data);
        if(isOpen){
            openwin('/wms/stock/out-stock.html?deliveryNoteId='+res.data+'&warehouseId='+params.warehouseId+'&customerId'+params.customerId,'库存选择',{area: ['1150px', '450px']});
        }else{
            closeThisWin('保存成功');
        }
    });
}
function generateDocNo() {
    show_loading();
    $.post(rurl('scm/serialNumber/generateDocNo'),{'type':'delivery_note'},function (res) {
        if(isSuccess(res)){
            $('#docNo').val(res.data);
        }
    });
}
function fullCustomerData(data) {
    $('#customerId').val(data.id);
}

function fullMember(members){
    layui.table.render({
        elem: '#deliveryMemberTable',limit:5000,height:'full-340',totalRow: true,page:false,text: {
        none: '<a href="javascript:" class="def-a" onclick="saveDeliveryNote(true)"><i class="layui-icon layui-icon-ok"></i>点击选择出库明细</a>'
    }
        ,cols: [[
            {type:'numbers',title:'序号',width:45}
            ,{field:'receivingNoteNo',title:'入库单号',width:120,totalRowText: '合计'}
            ,{field:'model',title:'型号',width:200}
            ,{field:'brand',title:'品牌',width:120}
            ,{field:'name',title:'名称',width:120}
            ,{field:'batchNo',title:'批次号',width:140}
            ,{field:'quantity',title:'出库数量',width:90,align:'right',totalRow: true}
            ,{field:'unitName',title:'单位',width:50,align:'center'}
            // ,{field:'unitPrice',title:'单价',width:90,align:'right'}
            // ,{field:'totalPrice',title:'总价',width:90,align:'money'}
            // ,{field:'cartonNum',title:'箱数',width:100}
            // ,{field:'cartonNo',title:'箱号',width:100}
            // ,{field:'netWeight',title:'净重',width:100}
            // ,{field:'grossWeight',title:'毛重',width:100}
        ]]
        , data: members
    });
    formatTableData();
}