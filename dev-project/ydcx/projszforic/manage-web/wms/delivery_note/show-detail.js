layui.config({base: '/wms/delivery_note/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#showDetail').load(rurl('wms/delivery_note/detail-temp.html'),function () {
        detailTemp.render(id,'');
    });
});