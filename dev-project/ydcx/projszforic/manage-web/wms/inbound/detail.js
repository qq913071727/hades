layui.config({base: '/wms/inbound/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#abroadDetail').load(rurl('wms/inbound/detail-temp.html'),function () {
        detailTemp.render(id,'');
    });
});