layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');

        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('wms/receivingNote/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'statusDesc', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                    ,{field:'docNo', width:126, title: '入库单号',templet:'#docNoTemp', fixed: true}
                    ,{field:'receivingDate', width:120, title: '入库时间'}
                    ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                    ,{field:'fileList', width:94, title: '附件资料',align:'center',templet:'#fileListTemp'}
                    ,{field:'warehouseName', width:200, title: '仓库'}
                    ,{field:'memo', title: '备注'}
                ]]
            },listQueryDone:function () {
                initFileSize();
            }
        });
    });

});
function fullCustomerData(data) {
    $('#customerId').val(data.id);
}
function showDetail(id) {
    openwin(rurl('wms/inbound/detail.html?id='+id),'入库单详情',{area: ['0', '0'],shadeClose:true});
}