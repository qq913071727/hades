layui.define(['layer','laytpl','element','table'], function(exports){
    var layer = layui.layer,laytpl=layui.laytpl,element=layui.element,table=layui.table,
    detailTemp = {
        render:function (id,operation) {
            show_loading();
            $.post(rurl('wms/receivingNote/detail'),{'id':id,'operation':operation},function (res) {
                if(isSuccessWarnClose(res)){
                    detailTemp.renderData(res.data);
                }
            });
            $('.file-size').attr({'doc-id':id});
        },
        renderData(data){
            var receivingNote = data.receivingNote,members=data.members||[];
            laytpl($('#receivingNoteViewTemp').html()).render(receivingNote,function (html) {
                $('#receivingNoteView').html(html);
            });
            table.render({
                elem: '#memberMaterial',limit:5000,page:false
                ,cols: [[
                    {type:'numbers',title:'序号',width:45}
                    ,{field:'model',title:'型号',width:160}
                    ,{field:'name',title:'名称',width:180}
                    ,{field:'brand',title:'品牌',width:180}
                ]]
                , data: members
            });
            table.render({
                elem: '#memberQty' ,totalRow: true,limit:5000,page:false
                , cols: [[
                    {type:'numbers',title:'序号',width:45}
                    ,{field:'model',title:'型号',width:180, totalRowText: '合计'}
                    ,{field:'quantity',title:'数量',width:90,align:'money', totalRow: true}
                    ,{field:'unitName',title:'单位',width:80,align:'center'}
                    ,{field:'unitPrice',title:'单价',width:80,align:'center'}
                    ,{field:'totalPrice',title:'总价',width:80,align:'money', totalRow: true}
                ]], data: members
            });
            table.render({
                elem: '#memberOther',totalRow: true,limit:5000,page:false
                , cols: [[
                    {type:'numbers',title:'序号',width:45}
                    ,{field:'model',title:'型号',width:180, totalRowText: '合计'}
                    ,{field:'cartonNum',title:'箱数',width:90,align:'right', totalRow: true}
                    ,{field:'cartonNo',title:'箱号',width:120}
                    ,{field:'netWeight',title:'净重',width:90,align:'money', totalRow: true}
                    ,{field:'grossWeight',title:'毛重',width:90,align:'money', totalRow: true}
                ]]
                , data: members
            });
            formatTableData();
        }
    };
    exports('detailTemp', detailTemp);
});