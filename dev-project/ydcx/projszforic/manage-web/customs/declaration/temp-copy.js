layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    var id = getUrlParam("id");
    show_loading();
    $('.sel-load').selLoad(function () {
        form.render('select');
        $.get(rurl('/scm/declarationTemp/detail'),{'id':id},function (res) {
            if(isSuccessWarnClose(res)){
                var data = res.data;
                data['id'] = '';
                data['tempName'] = '';
                form.val('tempForm',data);
                showImpOrExp();
            }
        })
    });
    form.on("select(billType)",function (data){
        showImpOrExp();
    });
    $('.save-btn').click(function () {
        if(checkedForm($('.tsf-page-content'))){
            show_loading();
            $.post("/scm/declarationTemp/add",$('.layui-form').serialize(), function (res) {
                if(isSuccess(res)) {
                    parent.reloadList();
                    closeThisWin('保存成功');
                }
            });
        }
    });
});
function showImpOrExp(){
    $('[mark]').hide();
    $('[mark="'+$('#billType').val()+'"]').show();
}