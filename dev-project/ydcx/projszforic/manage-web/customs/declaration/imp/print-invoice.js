layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer,laytpl=layui.laytpl,form=layui.form
        ,id = getUrlParam("id"),autoChapter = true,$chapterArea=$('.chapterArea');
    show_loading();
    $.post(rurl('scm/declaration/contractData'),{'id':id},function (res) {
        if(isSuccessWarn(res)){
            var data = res.data;
            var tquantity = 0.0,ttotalPrice=0.0;
            $.each(data.members || [],function (i,o) {
                tquantity += o.quantity;
                ttotalPrice += o.totalPrice;
            });
            data['tquantity'] = Math.round(tquantity*100)/100;
            data['ttotalPrice'] = Math.round(ttotalPrice*100)/100;
            laytpl($('#conTemp').html()).render(data,function (html) {
                $('#conTempView').html(html);
                $chapterArea=$('.chapterArea');
            });
        }
    });
    form.on("checkbox(autoChapter)",function (d) {autoChapter = this.checked;if(autoChapter){$chapterArea.show();}else{$chapterArea.hide();}});
    $('#exportPdf').click(function(){
        show_loading();
        $.get(rurl('scm/declarationPdf/invoice'),{'id':id,'autoChapter':autoChapter},function (res) {
            if(isSuccessWarn(res)){
                exportFile(res.data);
            }
        });
    });
    $('#exportPdfBatch').click(function (values) {
        var promise1 = new Promise(function (resolve, reject) {
            $.get(rurl('scm/declarationPdf/contract'),{'id':id,'autoChapter':autoChapter},function (res) {
                if(res.code == '1'){
                    resolve(res.data);
                } else {
                    reject(res.message);
                }
            });
        });
        var promise2 = new Promise(function (resolve, reject) {
            $.get(rurl('scm/declarationPdf/invoice'),{'id':id,'autoChapter':autoChapter},function (res) {
                if(res.code == '1'){
                    resolve(res.data);
                } else {
                    reject(res.message);
                }
            });
        });
        var promise3 = new Promise(function (resolve, reject) {
            $.get(rurl('scm/declarationPdf/packing'),{'id':id,'autoChapter':autoChapter},function (res) {
                if(res.code == '1'){
                    resolve(res.data);
                } else {
                    reject(res.message);
                }
            });
        });
        var p = Promise.all([promise1,promise2,promise3]).then(values => {
            // 依次从返回的数据接口数组中获取不同接口数据
            exportFile(values[0]);
            //间隔导出
            setTimeout(function (){
                exportFile(values[1])
            },100);
            setTimeout(function (){
                exportFile(values[2])
            },2000);
        });
    });
    $('#exportPdfMerge').click(function () {
        show_loading();
        $.get(rurl('scm/declarationPdf/merge'),{'id':id,'autoChapter':autoChapter},function (res) {
            if(isSuccessWarn(res)){
                exportFile(res.data);
            }
        });
    });
});
