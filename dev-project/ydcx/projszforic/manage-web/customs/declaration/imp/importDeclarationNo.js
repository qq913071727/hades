layui.use(['layer','upload'], function() {
    var layer = layui.layer, upload = layui.upload;
    upload.render({
        elem: '#importExcel',url:rurl('scm/declaration/importData'),size:10240,exts:'xls|xlsx'
        ,acceptMime:'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ,before:function (res) {
            show_loading();
        } ,done: function(res){
            if(isSuccessWarn(res)){
                closeThisWin();
                parent.importSuccess('导入成功');
            }
        },error:function (index, upload) {
            hide_loading();
        }
    });
});