layui.define(['layer','form','laytpl','element','table'], function(exports){
    var layer = layui.layer,form=layui.form,laytpl=layui.laytpl,element=layui.element,table=layui.table,
    detailTemp = {
        render:function (id,operation) {
            show_loading();
            $.post(rurl('scm/declaration/detail'),{'id':id,'operation':operation},function (res) {
                if(isSuccessWarnClose(res)){
                    var data=res.data,declaration=data.declaration,members=data.members;
                    form.val('tempForm',declaration);
                    table.render({
                        elem: '#table-list',height:'380',page: false,totalRow: true,limit:1000,id:'tab-list'
                        ,cols: [[
                            {field:'rowNo',title:'序号',width:45,align:'center',fixed: true,totalRowText: '合计'}
                            ,{field:'hsCode',title:'海关编码',width:90,align:'center',fixed: true}
                            ,{field:'ciqNo',title:'CIQ',width:50,align:'center'}
                            ,{field:'model',title:'型号',width:140}
                            ,{field:'name',title:'名称',width:140}
                            ,{field:'brand',title:'品牌',width:100}
                            ,{field:'quantity',title:'成交数量',width:70,align:'money', totalRow: true}
                            ,{field:'unitName',title:'成交单位',width:60,align:'center'}
                            ,{field:'quantity1',title:'法一数量',width:70,align:'money', totalRow: true}
                            ,{field:'unit1Name',title:'法一单位',width:60,align:'center'}
                            ,{field:'quantity2',title:'法二数量',width:70,align:'money', totalRow: true}
                            ,{field:'unit2Name',title:'法二单位',width:60,align:'center'}
                            ,{field:'unitPrice',title:'单价',width:60,align:'right'}
                            ,{field:'totalPrice',title:'总价',width:80,align:'money', totalRow: true}
                            ,{field:'currencyName',title:'币制',width:60,align:'center'}
                            ,{field:'countryName',title:'产地',width:60}
                            ,{field:'destinationCountryName',title:'最终目的国',width:80}
                            ,{field:'dutyModeName',title:'征免方式',width:80}
                            ,{field:'districtName',title:'境内目的地',width:150,templet:'#districtTemp'}
                            ,{field:'declareSpec',title:'报关规格',width:300}
                        ]]
                        , data: members
                        ,done:function () {
                            formatTableData();
                        }
                    });
                }
            });
        }
    };
    exports('detailTemp', detailTemp);
});