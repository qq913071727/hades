layui.config({base: '/customs/declaration/imp/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#decDetail').load(rurl('customs/declaration/imp/detail-temp.html'),function () {
        detailTemp.render(id,'');
    });
});