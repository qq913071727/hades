layui.use(['layer','form','table'], function() {
    var layer = layui.layer, form = layui.form,table=layui.table;
    var id = getUrlParam("id");show_loading();
    $.post(rurl('scm/declaration/mainDetail'),{'id':id,'operation':'dec_confirm'},function (res) {
        if(isSuccessWarnClose(res)){
            var data = res.data;
            form.val('tempForm',data);

            table.render({
                elem: '#table-list',url:rurl('scm/declaration/memberDetail'),height:'380',method:'post',where:{'id':id},
                page: false,totalRow: true,limit:1000,id:'tab-list',response:{msgName:'message',statusCode: '1'}
                ,cols: [[
                    {field:'rowNo',title:'序号',width:45,align:'center',fixed: true,totalRowText: '合计'}
                    ,{type:'operate',title:'操作',align:'center',width:70,toolbar:'#operateTemp',fixed: true}
                    ,{field:'hsCode',title:'海关编码',width:90,align:'center',fixed: true}
                    ,{field:'ciqNo',title:'CIQ',width:50,align:'center'}
                    ,{field:'model',title:'型号',width:140}
                    ,{field:'name',title:'名称',width:140}
                    ,{field:'brand',title:'品牌',width:100}
                    ,{field:'quantity',title:'成交数量',width:70,align:'money', totalRow: true}
                    ,{field:'unitName',title:'成交单位',width:60,align:'center'}
                    ,{field:'quantity1',title:'法一数量',width:70,align:'money', totalRow: true}
                    ,{field:'unit1Name',title:'法一单位',width:60,align:'center'}
                    ,{field:'quantity2',title:'法二数量',width:70,align:'money', totalRow: true}
                    ,{field:'unit2Name',title:'法二单位',width:60,align:'center'}
                    ,{field:'unitPrice',title:'单价',width:60,align:'right'}
                    ,{field:'totalPrice',title:'总价',width:80,align:'money', totalRow: true}
                    ,{field:'currencyName',title:'币制',width:60,align:'center'}
                    ,{field:'countryName',title:'产地',width:60}
                    ,{field:'destinationCountryName',title:'最终目的国',width:80}
                    ,{field:'dutyModeName',title:'征免方式',width:80}
                    ,{field:'districtName',title:'境内目的地',width:150,templet:'#districtTemp'}
                    ,{field:'declareSpec',title:'报关规格',width:300}
                ]],done:function () {
                    formatTableData();
                }
            });
        }
    });
    table.on('tool(table-list)', function(item){
        if(item.event === 'operateOperation'){
            openwin('customs/declaration/imp/perfect_member.html?id='+item.data.id,'修改明细',{area: ['1000px', '600px'],shadeClose:true});
        }
    });

    $('.save-btn').click(function () {
        if(checkedForm($('.layui-form'))){
            formSub('scm/declaration/confirm',function (res) {
                parent.reloadList();
                closeThisWin('操作成功');
            });
        }
    });
    // 退回验货
    $('.back-inspection').click(function () {
        layer.prompt({formType:2,maxlength: 500,title:'请填写退回原因'},function(val, index){
            show_loading();
            $.post(rurl('scm/declaration/backInspection'),{'id':id,'info':val},function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
    });
    $('#agentName').enter(function (){searchCss('3',$(this).val());});
    $('#agentScc').enter(function(){searchCss('1',$(this).val());});

    $('#agentName').autocomplete({
        source: function (request, response) {
            $.post(rurl('scm/declarationBroker/select'),{'name': trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: item.name, value: item.name, data: item}}));
            });
        }, minLength: 0, delay: 300,
        select: function( event, ui ) {
            var data = ui.item.data;
            form.val('tempForm',{
                'agentName':data.name,
                'agentScc':data.socialNo,
                'agentCode':data.customsNo
            });
        }
    }).focus(function (){
        $(this).autocomplete("search");
    });
});
function searchCss(queryFlag,enterCode){
    if(isEmpty(enterCode)){return;}show_loading();$.ajax({url: rurl('scm/css/search'), data: JSON.stringify({'queryFlag':queryFlag,'enterCode':trim(enterCode)}), type: 'post', dataType: 'json', contentType: 'application/json', success: function (res){
    if(isSuccessNoTip(res)&&res.data!=null){$('#agentName').val(res.data.name);$('#agentScc').val(res.data.id);$('#agentCode').val(res.data.customsCode);}}});
}
function reloadList() {
    layui.table.reload('tab-list',{done:function (data){formatTableData();}});
}