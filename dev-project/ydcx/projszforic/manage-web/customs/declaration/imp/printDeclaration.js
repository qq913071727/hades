layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer,laytpl=layui.laytpl,form=layui.form
        ,id = getUrlParam("id");
    show_loading();
    var imp_contrNo = "";
    $.post(rurl('scm/declaration/detail'),{'id':id},function (res) {
        if(isSuccessWarn(res)){
            var data = res.data;

            imp_contrNo = data.declaration.contrNo;

            var tquantity = 0.0,ttotalPrice=0.0,tnetweight=0.0;
            $.each(data.members || [],function (i,o) {
                tquantity += o.quantity;
                ttotalPrice += o.totalPrice;
                tnetweight += o.netWeight;
            });
            data['tquantity'] = Math.round(tquantity*100)/100;
            data['ttotalPrice'] = Math.round(ttotalPrice*100)/100;
            data['tnetweight'] = Math.round(tnetweight*10000)/10000;
            laytpl($('#conTemp').html()).render(data,function (html) {
                $('#conTempView').html(html);
            });
        }
    });

    $("#exportPdf").click(function () {
        window.location.href = '/scm/declaration/exportDraft?id=' + id
    });

    $("#print").click(function () {
        top.document.title = imp_contrNo;
        setTimeout(function () {
            top.document.title = "";
        },2000);
        window.print();
    });

});


