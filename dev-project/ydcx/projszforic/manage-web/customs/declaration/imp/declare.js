layui.config({base: '/customs/declaration/imp/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#decDetail').load(rurl('customs/declaration/imp/detail-temp.html'),function () {
        detailTemp.render(id,'dec_declare');

        var $operationTaskBtn = $('.operation-task-btn'),$backOrderBtn=$('.back-order-btn'),taskArg = {'operation':'dec_declare','documentId':id,'documentClass':'Declaration','newStatusCode':''};
        $operationTaskBtn.click(function (){
            var $that = $(this),needMemo = $that.attr('need-memo');
            if(!isEmpty(needMemo)){
                var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
                taskArg['memo'] = $memo.val();
            }else{
                taskArg['memo'] = $('#taskMemo').val();
            }
            taskArg['newStatusCode'] = $that.attr('status-code');
            show_loading();
            $.post(rurl('scm/declaration/confirmDeclare'),taskArg,function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
    });
    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
});