layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');

        operation.init({
            listQuery:true,
            listQueryArg:{
                url:rurl('scm/declarationTemp/page')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'billTypeName', width:80, title: '业务类型'}
                    ,{field:'tempName', width:200, title: '模板名称',templet:'#nameTemp'}
                    ,{field:'cusTrafModeName', width:100, title: '运输方式'}
                    ,{field:'customMasterName', width:100, title: '申报地海关'}
                    ,{field:'iePortName', width:100, title: '进境关别'}
                    ,{field:'ciqEntyPortName', width:100, title: '入境口岸'}
                    ,{field:'disabled', width:100,align:'form-switch',title: '状态',templet:'#disabledTemp'}
                    ,{field:'isDefault', width:45, title: '默认',align:'center',templet:'#isDefaultTemp'}
                    ,{field:'updateBy', width:80, title: '修改人',templet:'#updateByTemp'}
                    ,{field:'dateUpdated', width:120, title: '修改时间'}
                    ,{field:'empty', title: '&nbsp;'}
                ]]
            }
        });

        form.on('switch(switchDisabled)', function(data){
            show_loading();
            var $that = this;
            $.post(rurl('scm/declarationTemp/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
                if(isSuccess(res)){
                    show_success('修改成功');
                }else{
                    $(data.elem).attr({'checked':(!this.checked)});
                    form.render('checkbox');
                }
            });
        });
    });

});
function showDetail(id) {
    openwin(rurl('customs/declaration/temp-detail.html?id='+id),'报关模板详情',{area: ['0', '0'],shadeClose:true});
}
