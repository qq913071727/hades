var icoObjs = [],idx=0,sendIng=false;
layui.use(['layer','table'], function() {
    var layer = layui.layer,table=layui.table,id = getUrlParam('id');
    table.render({
        elem: '#table-list',limit:5000,id:'table-list',height:'full-42',page:false,method:'post',
        url:rurl('/scm/declaration/idsList'),where:{'ids':id},response:{msgName:'message',statusCode: '1'}
        ,cols: [[
            {field:'ico',width:40,align:'center',title:'',templet:'#icoTemp'}
            ,{field:'declarationStatusDesc', width:80,align:'center', title: '导单状态',templet:'#singleStatusTemp'}
            ,{field:'statusName', width:80, title: '报关状态',align:'center',templet:'#statusTemp'}
            ,{field:'orderDocNo', width:120, title: '系统单号'}
            ,{field:'contrNo', width:126, title: '合同号'}
            ,{field:'importDate', width:80, title: '订单日期'}
            ,{field:'customerName', width:200, title: '客户名称'}
            ,{field:'customMasterName', width:80, title: '申报地海关'}
            ,{field:'message', title: '消息',templet:'#messageTemp'}
        ]],done:function () {
            initImport();
        }
    });
    $('#startSend').click(function (){
        if(sendIng==false){
            sendIng=true;
            $('div[icoId]').html('<img src="'+rurl('images/indicator.gif')+'"/>');
            $('div[messageId]').html('');
            startSend();
        }
    });
});
function initImport() {
    idx = 0;icoObjs = $('div[icoId]');
}
function startSend(){
    if(icoObjs.size()>idx){
        var $id = $(icoObjs[idx]);idx++;
        $.post(rurl('scm/declaration/sendSinglewindow'),{'id':$id.attr('icoId')},function (res) {
            if(res.code=='1'){
                $id.html('<img src="'+rurl('images/success.png')+'"/>');
            }else{
                $id.html('<img src="'+rurl('images/error.png')+'"/>');
                $('div[messageId="'+$id.attr('icoId')+'"]').html(res.message);
            }
            startSend();
        });
    }else{
        sendIng = false;initImport();
        prompt_success("处理完成");
        parent.reloadList();
    }
}