layui.use(['layer','form','table','element'], function() {
    var layer = layui.layer, form = layui.form, table = layui.table, element = layui.element;
    var id = getUrlParam('id');
    show_loading();
    $.post(rurl('scm/declarationMember/obtainDeclarationElements'),{'id':id},function (res) {
        if(isSuccessWarnClose(res)){
            var data = res.data,member=data.member,elements=data.elements;
            if(!isEmpty(member.unit2Name)){
                $('#quantity2').addClass('required');
            }
            form.val('member',member);
            var ehtml = [];
            $.each(elements,function (i,o) {
                var eValue = isEmpty(o.val)?"":o.val,readonly = "";
                ehtml.push('<tr><td style="text-align: center">' + (i + 1) + '</td><td>&nbsp;&nbsp;' + o.name + '</td><td>');
                if ("品牌（中文或外文名称）" == o.name || "型号" == o.name) {
                    readonly = "readonly";
                }
                if(o.name=='品牌类型'){
                    ehtml.push(brandType(eValue));
                }else if(o.name=='出口享惠情况'){
                    ehtml.push(expBenefit(eValue));
                }else {
                    ehtml.push('<input name="elementsVal" class="layui-input ' + (isEmpty(readonly) ? "" : "layui-disabled") + '" value="' + eValue + '" ' + readonly + '/>');
                }
                ehtml.push('</td></tr>');
            });
            $('#elements').html(ehtml.join(''));
            form.render('select');
        }
    });

    $('.save-btn').click(function () {
        if(checkedForm($('.layui-form'))){
            formSub('scm/declarationMember/saveMemberElements',function (res) {
                parent.reloadList();
                closeThisWin('操作成功');
            });
        }
    });
});
function brandType(defVal) {
    defVal = defVal || $('#brand').attr('brand-type');
    return ['<select name="elementsVal" id="brandType" lay-search="">','<option value="">品牌类型</option>','<option value="0" ',((defVal=='0')?'selected':''),'>0：无品牌</option>','<option value="1" ',((defVal=='1')?'selected':''),'>1：境内自主品牌</option>','<option value="2" ',((defVal=='2')?'selected':''),'>2：境内收购品牌</option>','<option value="3" ',((defVal=='3')?'selected':''),'>3：境外品牌(贴牌生产)</option>','<option value="4" ',((defVal=='4')?'selected':''),'>4：境外品牌(其他)</option>','</select>'].join('');
}
function expBenefit(defVal) {
    return ['<select name="elementsVal" id="expBenefit" lay-search="">','<option value="">出口享惠情况</option>','<option value="0" ',((defVal=='0')?'selected':''),'>0：出口货物在最终目的国(地区)不享受优惠关税</option>','<option value="1" ',((defVal=='1')?'selected':''),'>1：出口货物在最终目的国(地区)享受优惠关税</option>','<option value="2" ',((defVal=='2')?'selected':''),'>2：出口货物不能确定在最终目的国(地区)享受优惠关税</option>','<option value="3" ',((defVal=='3')?'selected':''),'>3：不适用于进口报关单</option>','</select>'].join('');
}