layui.use(['layer','element'], function() {
    var layer = layui.layer, element = layui.element;
    var id = getUrlParam("id");
    element.on('tab(detailTab)', function() {
        var layId = this.getAttribute('lay-id'),$iframe = $('#'+layId);
        if(isEmpty($iframe.attr('src'))){
            $iframe.attr({'src':rurl('customs/declaration/exp/overseas-print-'+layId+'.html?id='+id)});
        }
    });
    autoDetailContent();
    $(window).resize(function(){autoDetailContent();});
    element.tabChange('detailTab','contract');
});
var $detailContent=$('#detail-content');
function autoDetailContent(){
    $detailContent.css({'height':($(window).height()-38)+'px'});
}