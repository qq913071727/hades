layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer,laytpl=layui.laytpl,form=layui.form
        ,id = getUrlParam("id");
    show_loading();
    var contrNo = "";
    $.post(rurl('scm/declaration/detail'),{'id':id},function (res) {
        if(isSuccessWarn(res)){
            var data = res.data;
            $("#iePortCode").val(data.declaration.iePortCode);
            $("#iePortName").val(data.declaration.iePortName);
            $("#ciqEntyPortCode").val(data.declaration.ciqEntyPortCode);
            $("#ciqEntyPortName").val(data.declaration.ciqEntyPortName);

            contrNo = data.declaration.contrNo;
            var tquantity = 0.0,ttotalPrice=0.0,tnetweight=0.0;
            $.each(data.members || [],function (i,o) {
                tquantity += o.quantity;
                ttotalPrice += o.totalPrice;
                tnetweight += o.netWeight;
            });
            data['tquantity'] = Math.round(tquantity*100)/100;
            data['ttotalPrice'] = Math.round(ttotalPrice*100)/100;
            data['tnetweight'] = Math.round(tnetweight*10000)/10000;
            laytpl($('#conTemp').html()).render(data,function (html) {
                $('#conTempView').html(html);
            });

            if(data.declaration.cusTradeCountryCode == 'RUS') {
                //默认选中
                var noShowFiles = ['iePort','ciqEntyPort'];
                layui.formSelects.value('notshow-select',noShowFiles);
                for(let notShowField of noShowFiles) {
                    $("." + notShowField + "Code").text("()");
                    $("." + notShowField + "Name").text("");
                }
            }
            
        }
    });

    $("#exportPdf").click(function () {
        var notShowFields = [];
        var notShows = layui.formSelects.value('notshow-select', 'val');
        if(notShows != null && notShows.length > 0) {
            for(let notShow of notShows) {
                notShowFields.push(notShow + "Code");
                notShowFields.push(notShow + "Name");
            }
        }
        window.location.href = '/scm/declaration/exportDraft?id=' + id + "&notShowFields=" + notShowFields.join(",");
    });

    $("#print").click(function () {
        top.document.title = contrNo;
        setTimeout(function () {
            top.document.title = "";
        },2000);
        window.print();
    });

    layui.formSelects.on('notshow-select', function(id, vals, val, isAdd, isDisabled){
        //id:           点击select的id
        //vals:         当前select已选中的值
        //val:          当前select点击的值
        //isAdd:        当前操作选中or取消
        //isDisabled:   当前选项是否是disabled
        //如果return false, 那么将取消本次操作
        //console.log(isAdd);
        //console.log(isDisabled);
        //console.log(val.value);
        let notShowField = val.value;
        if(isAdd) { //勾选
            if(isNotEmpty(notShowField)) {
                $("." + notShowField + "Code").text("()");
                $("." + notShowField + "Name").text("");
            }
        } else {//取消勾选
            let notShowCode = notShowField + "Code";
            let notShowName = notShowField + "Name";
            $("." + notShowCode).text("(" + $("#" + notShowCode).val() + ")");
            $("." + notShowName).text($("#" + notShowName).val());
        }
        return true;
    });

    layui.formSelects.opened('notshow-select', function(id){

    });
    layui.formSelects.closed('notshow-select', function(id){

    });

});


