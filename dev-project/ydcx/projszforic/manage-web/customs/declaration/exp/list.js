layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');

        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('scm/declaration/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'task', width:50, title: '任务',align:'center',toolbar:'#taskTemp', fixed: true}
                    ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                    ,{field:'docNo', width:126, title: '系统单号',templet:'#docNoTemp', fixed: true}
                    ,{field:'contrNo', width:126, title: '合同号', fixed: true}
                    ,{field:'importDate', width:80, title: '订单日期'}
                    ,{field:'decDate', width:80, title: '申报日期'}
                    ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                    ,{field:'packNo', width:65, title: '件数',align:'center'}
                    ,{field:'transactionModeDesc', width:70, title: '成交方式',align:'center'}
                    ,{field:'customMasterName', width:75, title: '申报地海关',align:'center'}
                    ,{field:'iePortName', width:75, title: '进境关别',align:'center'}
                    ,{field:'declarationStatusDesc', width:80,align:'center', title: '导单状态',templet:'#singleStatusTemp'}
                    ,{field:'manifestSwStatusDesc', width:80,align:'center', title: '舱单状态',templet:'#manifestSwStatusTemp'}
                    ,{field:'cusVoyageNo', width:110, title: '六联单号'}
                    ,{field:'licensePlate', width:90, title: '车牌号码',align:'center'}
                    ,{field:'declarationNo', width:140, title: '报关单号'}
                    ,{field:'decFile', width:90, title: '报关单文件',align:'center',fixed:'right',templet:'#decFileTemp'}
                ]]
            },listQueryDone:function () {
                initFileSize();
            }
        });
    });

});
function showDetail(id) {
    openwin(rurl('customs/declaration/exp/detail.html?id='+id),'报关单详情',{area: ['0', '0'],shadeClose:true});
}
function showOrderDetail(id) {
    openwin(rurl('order/exp/detail.html?id='+id),'订单详情',{area: ['0', '0'],shadeClose:true});
}
var declarationStatuswin;
function showDeclarationStatus(docNo){
    layer.close(declarationStatuswin);
    declarationStatuswin = openwin('status/single-list.html?domainType=declare&domainId='+docNo,null,{
        area:['360px','520px'],shadeClose:true
    });
}

var manifestStatuswin;
function showManifestStatus(docNo){
    layer.close(manifestStatuswin);
    manifestStatuswin = openwin('status/single-list.html?domainType=mainfest&domainId='+docNo,null,{
        area:['360px','520px'],shadeClose:true
    });
}

function completed(o) {
    var ids = getIds();
    if(ids.length==1){
        openwin('customs/declaration/exp/completed.html?id='+ids[0],'确定申报完成',{area:['0px','0px']});
    }else if(ids.length>1){
        openwin('customs/declaration/exp/batch-completed.html?id='+ids.join(','),'批量确定申报完成',{area:['1000px','400px']});
    }else{
        show_error('请至少选择一条报关单数据');
    }
}

function removeManifest() {
    var ids = getIds();
    if(ids.length != 1) {
        show_error('请单选一条报关单数据');
    }
    $.post(rurl('scm/declaration/removeManifest'),{'id':ids[0]},function (res) {
        if(isSuccess(res)){
            closeThisWin('操作成功');
            reloadList();
        }
    });
}

function generateSendManifest() {
    var ids = getIds();
    if(ids.length != 1) {
        show_error('请单选一条报关单数据');
    }
    $.post(rurl('scm/declaration/generateSendManifest'),{'id':ids[0]},function (res) {
        if(isSuccess(res)){
            closeThisWin('操作成功');
            reloadList();
        }
    });
}
function sendToScmbot(o) {
    confirm_inquiry('您是否确认要将此单推送至报关行申报？',function () {
        show_loading();
        $.post(rurl(o.url),{'id':getIds()[0]},function (res) {
            if(isSuccess(res)){
                show_success('操作成功，已发送至报关行等待申报');
            }
        });
    });
}

function exportExcel(o) {
    let ids = getIds();
    if(ids.length != 1) {
        show_error("请单选一条记录");
        return;
    }
    toUrl('/scm/declaration/exportDraft?id='+ids[0]);
}