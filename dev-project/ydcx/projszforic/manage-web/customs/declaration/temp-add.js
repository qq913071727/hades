layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
        showImpOrExp();
    });

    form.on("select(billType)",function (data){
        showImpOrExp();
    });
    $('.save-btn').click(function () {
        if(checkedForm($('.tsf-page-content'))){
            show_loading();
            $.post("/scm/declarationTemp/add",$('.layui-form').serialize(), function (res) {
                if(isSuccess(res)) {
                    parent.reloadList();
                    closeThisWin('保存成功');
                }
            });
        }
    });
});
function showImpOrExp(){
    $('[mark]').hide();
    $('[mark="'+$('#billType').val()+'"]').show();
}