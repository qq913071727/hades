layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    var id = getUrlParam("id");
    show_loading();
    $('.sel-load').selLoad(function () {
        form.render('select');

        $.get(rurl('/scm/declarationTemp/detail'),{'id':id},function (res) {
            if(isSuccessWarnClose(res)){
                var data = res.data;
                form.val('tempForm',data);
                showImpOrExp();
                disableForm(form);
            }
        })
    });
});
function showImpOrExp(){
    $('[mark]').hide();
    $('[mark="'+$('#billType').val()+'"]').show();
}