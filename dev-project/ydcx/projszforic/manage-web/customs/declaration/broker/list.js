layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form','table'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form,table = layui.table;
    operation.init({
        listQuery:true,
        winArg:{shadeClose:true},
        listQueryArg:{
            url:rurl('scm/declarationBroker/page')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'name', fixed:true,width:200, title: '企业名称'}
                ,{field:'socialNo',width:200, title: '统一信用代码'}
                ,{field:'customsNo', width:120, title: '海关编码',templet:'#codeTemp'}
                ,{field:'disabled',width:100, title: '启用状态',align:'form-switch',templet:'#disabledTemp'}
                ,{field:'memo',width:300, title: '备注'}
            ]]
        },listQueryDone:function(){
            formatCsc();formatIaqr();
        }
    });

    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/declarationBroker/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });

});