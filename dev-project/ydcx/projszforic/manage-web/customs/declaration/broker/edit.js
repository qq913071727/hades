layui.use(['layer','form','table','element'], function() {
    var layer = layui.layer, form = layui.form, table = layui.table,element = layui.element;
    let id = getUrlParam("id");
    $.get(rurl('scm/declarationBroker/detail'),{'id':id},function (res) {
        form.val('form',res.data);
    });
    form.on('submit(save-btn)', function(data){
        formSub('scm/declarationBroker/edit',function (res) {
            parent.reloadList();
            closeThisWin('修改成功');
        });
        return false;
    });
});