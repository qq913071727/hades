layui.use(['layer','form','table','element'], function() {
    var layer = layui.layer, form = layui.form, table = layui.table,element = layui.element;
    form.on('submit(save-btn)', function(data){
        formSub('scm/declarationBroker/add',function (res) {
            parent.reloadList();
            closeThisWin('新增成功');
        });
        return false;
    });
});