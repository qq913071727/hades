var auto1 = {
    "customMasterName": {hid: 'customMasterCode',type:'CUS_CUSTOMS'},
    "iePortName": {hid: 'iePortCode',type:'CUS_CUSTOMS'},
    "ciqEntyPortName": {hid: 'ciqEntyPortCode',type:'CIQ_PORT_CN'},
    "destPortName": {hid: 'destPortCode',type:'CUS_MAPPING_PORT_CODE'},
    "cutModeName": {hid: 'cutModeCode',type:'CUS_LEVYTYPE'},
    "dutyModeName": {hid: 'dutyMode',type:'CUS_LEVYMODE'},
    "districtName": {hid: 'districtCode',type:'CUS_DISTRICT'},
    "ciqDestName": {hid: 'ciqDestCode',type:'CIQ_CITY_CN'},
    "feeMarkName": {hid: 'feeMark',type:'DEC_FEE_MARK'},
    "insurMarkName": {hid: 'insurMark',type:'DEC_INSUR_MARK'},
    "otherMarkName": {hid: 'otherMark',type:'DEC_OTHER_MARK'},
},auto2 = {
    "cusTradeCountryName": {hid: 'cusTradeCountryCode',type:'CUS_MAPPING_COUNTRY_CODE_V'},
    "cusTradeNationName": {hid: 'cusTradeNationCode',type:'CUS_MAPPING_COUNTRY_CODE_V'},
    "distinatePortName": {hid: 'distinatePortCode',type:'CUS_MAPPING_PORT_CODE_V'},
    "cusTrafModeName": {hid: 'cusTrafModeCode',type:'CUS_MAPPING_TRANSPORT_CODE_V'},
    "supvModeCddeName": {hid: 'supvModeCdde',type:'CUS_MAPPING_TRADE_CODE_V'},
    "wrapTypeName": {hid: 'wrapTypeCode',type:'CUS_MAPPING_PACKAGEKIND_CODE_V'},
    "destinationCountryName": {hid: 'destinationCountry',type:'CUS_MAPPING_COUNTRY_CODE_V'},
    "feeCurrName": {hid: 'feeCurr',type:'CUS_MAPPING_CURRENCY_CODE_V'},
    "insurCurrName": {hid: 'insurCurr',type:'CUS_MAPPING_CURRENCY_CODE_V'},
    "otherCurrName": {hid: 'otherCurr',type:'CUS_MAPPING_CURRENCY_CODE_V'},
};
function initAuto1(data){
    $.each(auto1,function (key,val) {
        var $input = $('#'+key);
        if($input.length>0){
            var source = [];
            $.each(data[val.type]||[],function (i,o) {
                source.push({'label':o.c+'-'+o.n,'value':o.n});
            });
            if(source.length>0){
                $input.autocompleter({
                    source:source,
                    style:removeNull(val.style),
                    limit:20,
                    focusOpen:(val.focusOpen),
                    hiddenId:val.hid,
                    dropUp:val.dropUp,
                    callback:val.callback
                });
            }
        }
    });
}
function initAuto2(data){
    $.each(auto2,function (key,val) {
        var $input = $('#'+key);
        if($input.length>0){
            var source = [];
            $.each(data[val.type]||[],function (i,o) {
                o['label'] = o.c+'_'+o.n+'_'+o.o+'_'+o.q;
                source.push(o);
            });
            if(source.length>0){
                $input.autocompleter({
                    source:source,
                    style:removeNull(val.style),
                    limit:20,
                    focusOpen:(val.focusOpen),
                    hiddenId:val.hid,
                    dropUp:val.dropUp,
                    callback:val.callback,
                    template:'<div>{{c}}-{{n}} <span class="fr">{{o}}&nbsp;&nbsp;{{q}}</span></div>'
                });
            }
        }
    });
}
function initBaseData(){
    var declDatas1 = localStorage.getItem("static_decl_datas1");
    if(isEmpty(declDatas1)){
        $.post(rurl('system/customsData/formatData1'),{},function (res) {
            if(isSuccessWarn(res)){
                localStorage.setItem("static_decl_datas1",JSON.stringify(res.data));
                initAuto1(res.data);
            }
        });
    }else{
        initAuto1(JSON.parse(declDatas1));
    }
    var declDatas2 = localStorage.getItem("static_decl_datas2");
    if(isEmpty(declDatas2)){
        $.post(rurl('system/customsData/formatData2'),{},function (res) {
            if(isSuccessWarn(res)){
                localStorage.setItem("static_decl_datas2",JSON.stringify(res.data));
                initAuto2(res.data);
            }
        });
    }else{
        initAuto2(JSON.parse(declDatas2));
    }
}
$(function () {
    $.get(rurl('system/version'),{},function (res) {
        if(isSuccess(res)){
            if(res.data != localStorage.getItem('static_version')){
                localStorage.setItem("static_code_datas","");
                localStorage.setItem("static_decl_datas1","");
                localStorage.setItem("static_decl_datas2","");
                localStorage.setItem("static_prod_traf_data","");
                localStorage.setItem("static_version",res.data);
            }
            initBaseData();
        }
    });
});