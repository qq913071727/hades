layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form','table'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form,table = layui.table;
    operation.init({
        listQuery:true,
        winArg:{shadeClose:true},
        listQueryArg:{
            url:rurl('scm/nameElements/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'nameMemo', fixed:true,width:200, title: '要素名称'}
                ,{field:'name',width:200, title: '物料名称'}
                ,{field:'hsCode', width:120, title: '海关编码',templet:'#codeTemp'}
                ,{field:'tit',width:110, title: '暂定税率',align:'right'}
                ,{field:'mfntr',width:110, title: '进口最惠国税率',align:'right'}
                ,{field:'gtr',width:110, title: '进口普通国税率',align:'right'}
                ,{field:'vatr',width:90, title: '增值税率',align:'right'}
                ,{field:'trr',width:90, title: '退税率',align:'right'}
                ,{field:'csc', width:80, title: '监管条件',templet:'#cscTemp'}
                ,{field:'iaqr', width:80, title: '检验检疫',templet:'#iaqrTemp'}
                ,{field:'elements',width:300, title: '要素'}
                ,{field:'disabled',width:100, title: '启用状态',align:'form-switch',templet:'#disabledTemp'}
            ]]
        },listQueryDone:function(){
            formatCsc();formatIaqr();
        }
    });

    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/nameElements/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });

});

function showCustomsCodeDetail(id){
    openwin(rurl('basedata/customsCode/detail.html?id='+id),'编码详情',{area: ['1200px', '620px'],shadeClose:true});
}