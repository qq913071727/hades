layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form','table'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form,table = layui.table;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,listCols:true,
        winArg:{shadeClose:true},
        listQueryArg:{
           url:rurl('scm/materiel/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                ,{field:'customerName', width:260, title: '所属客户',templet:'#customerNameTemp', fixed: true}
                ,{field:'model', width:140, title: '型号',templet:'#modelTemp', fixed: true}
                ,{field:'brand', width:100, title: '品牌'}
                ,{field:'name', width:120, title: '名称'}
                ,{field:'hsCode', width:90, title: '海关编码'}
                ,{field:'ciqNo', width:65, title: 'CIQ编码'}
                ,{field:'csc', width:70, title: '监管条件',templet:'#cscTemp'}
                ,{field:'iaqr', width:70, title: '检验检疫',templet:'#iaqrTemp'}
                ,{field:'isSmp', width:70, title: '战略物资',templet:'#isSmpTemp',align:'center'}
                ,{field:'isCapp', width:70, title: '3C证书',templet:'#isCappTemp',align:'center'}
                ,{field:'isCappNo', width:70, title: '3C目录鉴定',templet:'#isCappNoTemp',align:'center'}
                ,{field:'cappNo', width:70, title: '证书编号'}
                ,{field:'tit', width:100, title: '进口暂定税率',align:'right'}
                ,{field:'mfntr', width:100, title: '进口最惠国税率',align:'right'}
                ,{field:'gtr', width:100, title: '进口普通国税率',align:'right'}
                ,{field:'levyTax', width:90, title: '对美加征关税',align:'right'}
                ,{field:'conrate', width:70, title: '消费税率',align:'right'}
                ,{field:'vatr', width:70, title: '增值税率',align:'right'}
                ,{field:'dateCreated', width:120, title: '提交时间'}
                ,{field:'classifyTime', width:120, title: '最后归类时间'}
                ,{field:'classifyPerson', width:75, title: '归类人员'}
                ,{field:'busPersonName', width:75, title: '商务人员'}
                ,{field:'source', width:100, title: '来源'}
                ,{field:'elements', width:360, title: '申报要素'}
            ]]
        },listQueryDone:function(tdata,mdata){
            formatCsc();formatIaqr();
            var operationCode='materiel_classify',isAllow = isAllowOperation(mdata,operationCode);
            if(isAllow){
                $('a[classifyId]').unbind().click(function (){
                    var classifyId = $(this).attr('classifyId');
                    operation.clearCheck(classifyId);
                    $('button[operation-code="'+operationCode+'"]').click();
                });
            }else{
                $('a[classifyId]').addClass('disabled-a');
            }
        }
    });
    //绑定海关编码
    $('#hsCode').autocomplete({
        source: function (request, response) {
            $.post(rurl('system/customsCode/list'),{'id': trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: item.id+'('+item.name+')', value: item.id, data: item}}));
            });
        }, minLength: 0, delay: 500
    }).focus(function (){
        $(this).autocomplete("search");
    });
});
function importSuccess(msg){prompt_success(msg,function (){show_loading();setTimeout(function () {hide_loading();reloadList();},1000)});}