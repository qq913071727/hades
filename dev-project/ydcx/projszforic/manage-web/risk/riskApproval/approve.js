layui.use(['layer','form','laytpl','element'], function() {
    var layer = layui.layer, form = layui.form, laytpl = layui.laytpl,element=layui.element;

    var id = getUrlParam("id"),customerId='';
    show_loading();
    $.post(rurl('scm/riskApproval/detail'),{'id':id,'operation':'risk_approval_audit'},function (res) {
        if(isSuccessWarnClose(res)){
            var data = res.data;customerId=data.customerId;
            laytpl($('#approveTemp').html()).render(data,function (html) {
                $('#approveTempView').html(html);
            });
            switch (data.docType){
                case 'orderAmountLess':
                    layui.config({base: '/order/imp/'}).extend({detailTemp: 'detail-temp'}).use(['detailTemp'],function () {
                        var detailTemp = layui.detailTemp;
                        $.post(rurl('scm/impOrderCost/orderCostInfo'),{'id':data.docId},function (sres) {
                            if(isSuccess(sres)){
                                laytpl($('#'.concat(data.docType)).html()).render(sres.data,function (html) {
                                    $('#docReasonView').html(html);
                                });
                            }
                        });
                        $('#orderDetail').load(rurl('order/imp/detail-temp.html'),function () {
                            detailTemp.render(data.docId,'');
                        });
                    });
                    break;
                case 'paymentAmountLess':
                    layui.config({base: '/finance/paymentAccount/'}).extend({detailTemp: 'detail-temp'}).use(['detailTemp'],function () {
                        var detailTemp = layui.detailTemp;
                        $.post(rurl('scm/impOrderCost/paymentCostInfo'),{'id':data.docId},function (sres) {
                            if(isSuccess(sres)){
                                laytpl($('#'.concat(data.docType)).html()).render(sres.data,function (html) {
                                    $('#docReasonView').html(html);
                                });
                            }
                        });
                        $('#orderDetail').load(rurl('finance/paymentAccount/detail-temp.html'),function () {
                            detailTemp.render(data.docId,'');
                        });
                    });
                    break;
                case 'paymentRateAdjust':
                    layui.config({base: '/finance/paymentAccount/'}).extend({detailTemp: 'detail-temp'}).use(['detailTemp'],function () {
                        var detailTemp = layui.detailTemp;
                        $('#orderDetail').load(rurl('finance/paymentAccount/detail-temp.html'),function () {
                            detailTemp.render(data.docId,'');
                        });
                    });
                    break;
            }
        }
    });

    var $operationTaskBtn = $('.operation-task-btn'),
        taskArg = {'operation':'risk_audit','documentId':id,'documentClass':'RiskApproval','newStatusCode':''};
    $operationTaskBtn.click(function () {
        var $that = $(this),needMemo = $that.attr('need-memo');
        if(!isEmpty(needMemo)){
            var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
            taskArg['memo'] = $memo.val();
        }else{
            taskArg['memo'] = $('#taskMemo').val();
        }
        taskArg['newStatusCode'] = $that.attr('status-code');

        show_loading();
        $.post(rurl('scm/riskApproval/approval'),taskArg,function (res) {
            if(isSuccess(res)){
                parent.reloadList();
                closeThisWin('操作成功');
            }
        });
    });
    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
    autoDetailContent();
    $(window).resize(function() {
        autoDetailContent();
    });
    var isLoad = false;
    element.on('tab(detailTab)', function(){
        var layId = this.getAttribute('lay-id');
        if(layId=="2"&&isLoad==false){
            isLoad = true;
            $('#customerDetail').attr({'src':rurl('customer/agreement/customer-detail.html?customerId='+customerId)});
        }
    });
});
var $detailContent=$('#detail-content'),$tsfPageDetail = $('.tsf-page-detail');
function autoDetailContent(){
    $detailContent.css({'height':($tsfPageDetail.height()-37)+'px'});
}