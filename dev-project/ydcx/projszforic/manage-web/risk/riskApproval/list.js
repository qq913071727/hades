layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');

        operation.init({
            listQuery:true,
            listQueryArg:{
                url:rurl('scm/riskApproval/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'operation', width:60, title: '操作',templet:'#taskTemp',align:'center', fixed: true}
                    ,{field:'statusDesc', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                    ,{field:'docTypeDesc', width:130, title: '审核类型'}
                    ,{field:'docNo', width:126, title: '单据编号',templet:'#docNoTemp'}
                    ,{field:'customerName', width:260, title: '客户',templet:'#customerNameTemp'}
                    ,{field:'approvalReason', width:260, title: '审批原因'}
                    ,{field:'dateCreated', width:120, title: '提交日期'}
                    ,{field:'submitter', width:80, title: '提交人'}
                    ,{field:'approver', width:80, title: '审核人'}
                    ,{field:'approvalInfo', title: '审核意见'}
                ]]
            },listQueryDone:function(){
                formatTableData();
            }
        });
    });

});
