layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
        var docNo = getUrlParam("docNo");
        if(isNotEmpty(docNo)) {
            $("#docNo").val(docNo);
        }
        operation.init({
            listQuery:true,
            listQueryArg:{
                url:rurl('scm/expOrderPrice/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'task', width:50, title: '任务',align:'center',toolbar:'#taskTemp', fixed: true}
                    ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp'}
                    ,{field:'docNo', width:126, title: '订单编号',templet:'#docNoTemp'}
                    ,{field:'submitCount', width:60, title: '提交次数',align:'center'}
                    ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                    // ,{field:'supplierName', width:260, title: '供应商'}
                    ,{field:'dateCreated', width:120, title: '提交时间'}
                    ,{field:'reviewTime', width:120, title: '审核时间'}
                    ,{field:'reviewer', width:70, title: '审核人'}
                    ,{field:'busPersonName', width:70, title: '跟进商务'}
                    ,{field:'empty',title: '&nbsp;'}
                ]]
            }
        });
    });
});
function showDetail(id) {
    openwin(rurl('order/priceexp/detail.html?id='+id),'审价详情',{area: ['0', '0'],shadeClose:true});
}
