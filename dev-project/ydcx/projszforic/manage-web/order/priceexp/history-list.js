layui.use(['layer','table'], function() {
    var layer = layui.layer,table=layui.table,id = getUrlParam('id');
    table.render({
        elem: '#table-list',limit:5000,id:'table-list',height:'full-20',page:false,method:'post',
        url:rurl('/scm/priceFluctuationHistoryExp/list?memberId=' + id),response:{msgName:'message',statusCode: '1'}
        ,cols: [[
            {field:'customerName', width:200, title: '客户名称'}
            ,{field:'orderDate',width:75, title: '出口日期'}
            ,{field:'orderDocNo',width:110, title: '订单号'}
            ,{field:'name',width:110, title: '名称'}
            ,{field:'brand',width:80, title: '品牌'}
            ,{field:'model',width:120, title: '型号'}
            ,{field:'quantity',width:80, title: '数量',align:'money'}
            ,{field:'unitName',width:45, title: '单位'}
            ,{field:'usdUnitPrice',width:85, title: '单价(USD)',align:'right'}
            ,{field:'usdTotalPrice',width:85, title: '总价(USD)',align:'money'}
        ]],done:function () {
            formatTableData();
        }
    });
});