layui.use(['layer','form','table','element'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table,element=layui.element;
    var id = getUrlParam("id"),orderId="";
    show_loading();
    $.post(rurl('scm/expOrderPrice/detail'),{"id":id},function (res) {
        if(isSuccessWarnClose(res)){
            var data = res.data,orderPrice = data.orderPrice,members=data.members;
            orderId=orderPrice.orderId;
            table.render({
                elem: '#orderPriceMember',limit:5000,page:false
                ,cols: [[
                    {type:'numbers',title:'序号',width:45}
                    ,{field:'model',title:'型号',width:160 ,totalRowText: '合计'}
                    ,{field:'name',title:'名称',width:160}
                    ,{field:'brand',title:'品牌',width:160}
                    ,{field:'quantity',title:'数量',width:85,align:'money',totalRow: true}
                    ,{field:'unitName',title:'单位',width:50}
                    ,{field:'unitPrice',title:'单价',width:75,align:'right',templet:'#unitPriceTemp'}
                    ,{field:'totalPrice',title:'总价',width:85,align:'money',totalRow: true}
                    ,{field:'currencyName',title:'币制',width:50}
                    ,{field:'fluctuations',title:'波动率%',width:75,align:'right',templet:'#fluctuationsTemp'}
                    ,{field:'isImp',title:'出口记录',width:70,templet:'#isImpTemp'}
                    ,{field:'isNewGoods',title:'初次提交',width:70,templet:'#isNewGoodsTemp'}
                    ,{field:'spec',title:'规格描述'}
                    ,{field:'processLogs',title:'计算过程',width:160}
                ]]
                , data: members
            });
            formatTableData();
            $.each(members,function (i,o) {
                if(Math.abs(o.fluctuations)>=5){
                    var obj = $('div[fluctuations="'+o.id+'"]').parent().parent().parent();
                    obj.addClass('color-red');
                }
                if(o.isImp==false){
                    var obj = $('div[fluctuations="'+o.id+'"]').parent().parent().parent();
                    obj.addClass('color-orange');
                }
            });
        }
    });

    var isLoad = false;
    element.on('tab(detailTab)', function(){
        var layId = this.getAttribute('lay-id');
        if(layId=="2"&&isLoad==false){
            isLoad = true;
            $('#orderDetail').attr({'src':rurl('order/priceexp/order-detail.html?orderId='+orderId)});
        }
    });
    autoDetailContent();
    $(window).resize(function() {
        autoDetailContent();
    });
});
var $detailContent=$('#detail-content'),$tsfPageDetail = $('.tsf-page-content');
function autoDetailContent(){
    $detailContent.css({'height':($tsfPageDetail.height()-37)+'px'});
}
var historyWin = null;
function showHistory(id) {
    layer.close(historyWin);
    historyWin = openwin(rurl('order/priceexp/history-list.html?id='+id),'历史价格',{shade:0,area: ['1016px', '300px']});
}