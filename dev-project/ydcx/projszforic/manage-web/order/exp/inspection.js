var inspec = {},customerId='',$quantityTotal=$('#quantityTotal'),$totalPriceTotal=$('#totalPrice'),$cartonTotal=$('#cartonTotal'),$netWeightTotal=$('#netWeightTotal'),$grossWeightTotal=$('#grossWeightTotal');
layui.use(['layer','form','laytpl'], function() {
    var layer = layui.layer, form = layui.form,laytpl=layui.laytpl;
    var id = getUrlParam('id'),
        keys = ['model','name','brand','unitName','quantity','unitPrice','totalPrice','cartonNum','cartonNo','netWeight','grossWeight','countryName','goodsCode','spec'];
    inspec = {
        renderData:function (){
            show_loading();
            $.post(rurl('scm/expOrder/detail'),{'id':id,'operation':'exp_order_inspection'},function (res) {
                if(isSuccessWarnClose(res)){
                    var data=res.data,order=data.order,logistiscs=data.logistiscs,members=data.members||[];
                    customerId = order.customerId;
                    laytpl($('#orderViewTemp').html()).render(order,function (html) {
                        $('#orderView').html(html);
                    });
                    laytpl($('#logistiscsViewTemp').html()).render(logistiscs,function (html) {
                        $('#logistiscsView').html(html);
                    });
                    inspec.createRows(members);

                    $('.confirm-inspection').removeClass('layui-btn-disabled').attr({'disabled':null});
                    $('.edit-member').show();
                    $('.save-edit-member').hide();
                }
            });
        },
        createRows:function (members) {
            var mhtml = [],tquantity = 0.0,ttotalPrice = 0.0,tcartonNum = 0,tnetWeight = 0.0,tgrossWeight = 0.0;
            $.each(members,function (i,o) {
                mhtml.push(inspec.createRowHtml(o));
                tquantity+=o.quantity;ttotalPrice+=o.totalPrice;tcartonNum+=o.cartonNum;tnetWeight+=o.netWeight;tgrossWeight+=o.grossWeight;
            });
            $('#materialBody').html(mhtml.join(''));
            inspec.historyMembers();
            $quantityTotal.html(Math.round(tquantity*100)/100);
            $totalPriceTotal.html(formatCurrency(Math.round(ttotalPrice*100)/100));
            $cartonTotal.html(tcartonNum);
            $netWeightTotal.html(Math.round(tnetWeight*100)/100);
            $grossWeightTotal.html(Math.round(tgrossWeight*100)/100);
            autoUnit();
            initInput();
        },
        createRowHtml:function(o){
            return [
                '<tr mid="'+o.id+'"><td class="t-a-c">'+o.rowNo+'</td>',
                '<td class="t-a-c"><input type="hidden" name="id" LIST value="'+o.id+'"/><span class="'+o.materielStatusId+'">'+o.materielStatusName+'</span></td>',
                '<td><div class="m-text" model>'+o.model+'</div><div class="m-input"><input type="text" name="model" LIST class="layui-input required auto-customer-model" placeholder="型号" value="'+(isEmpty(o.model)?'':o.model)+'"/></div></td>',
                '<td><div class="m-text" name>'+o.name+'</div><div class="m-input"><input type="text" name="name" LIST class="layui-input required" value="'+(isEmpty(o.name)?'':o.name)+'"/></div></td>',
                '<td><div class="m-text" brand>'+o.brand+'</div><div class="m-input"><input type="text" name="brand" LIST class="layui-input required" value="'+(isEmpty(o.brand)?'':o.brand)+'"/></div></td>',
                '<td><div class="m-text" unitName>'+o.unitName+'</div><div class="m-input"><input type="text" name="unitName" LIST class="layui-input auto-unit required" value="'+(isEmpty(o.unitName)?'个':o.unitName)+'"/></div></td>',
                '<td><div class="m-text" quantity>'+formatCurrency(o.quantity)+'</div><div class="m-input"><input type="text" name="quantity" LIST onblur="calculationAll(this)" NUMBER class="layui-input required" value="'+(isEmpty(o.quantity)?'':o.quantity)+'"/></div></td>',
                '<td><div class="m-text" unitPrice>'+o.unitPrice+'</div><div class="m-input"><input type="text" name="unitPrice" LIST class="layui-input layui-disabled" readonly value="'+(isEmpty(o.unitPrice)?'0.0000':o.unitPrice)+'"/></div></td>',
                '<td><div class="m-text" totalPrice>'+formatCurrency(o.totalPrice)+'</div><div class="m-input"><input type="text" name="totalPrice" LIST onblur="calculationAll(this)" NUMBER class="layui-input required" value="'+(isEmpty(o.totalPrice)?'':o.totalPrice)+'"/></div></td>',
                '<td><div class="m-text" cartonNum>'+o.cartonNum+'</div><div class="m-input"><input type="text" name="cartonNum" LIST onblur="calculationAll()" INT class="layui-input required" value="'+(isEmpty(o.cartonNum)?'0':o.cartonNum)+'"/></div></td>',
                '<td><div class="m-text" cartonNo>'+removeNull(o.cartonNo)+'</div><div class="m-input"><input type="text" name="cartonNo" LIST class="layui-input" value="'+(isEmpty(o.cartonNo)?'':o.cartonNo)+'"/></div></td>',
                '<td><div class="m-text" netWeight>'+o.netWeight+'</div><div class="m-input"><input type="text" name="netWeight" LIST NUMBER4 onblur="calculationAll()" class="layui-input required" value="'+(isEmpty(o.netWeight)?'0.0000':o.netWeight)+'"/></div></td>',
                '<td><div class="m-text" grossWeight>'+o.grossWeight+'</div><div class="m-input"><input type="text" name="grossWeight" LIST NUMBER4 onblur="calculationAll()" class="layui-input required" value="'+(isEmpty(o.grossWeight)?'0.0000':o.grossWeight)+'"/></div></td>',
                '<td><div class="m-text" goodsCode>'+o.goodsCode+'</div><div class="m-input"><input type="text" name="goodsCode" LIST class="layui-input" value="'+(isEmpty(o.goodsCode)?'':o.goodsCode)+'"/></td>',
                '<td><div class="m-text" spec>'+o.spec+'</div><div class="m-input"><input type="text" name="spec" LIST class="layui-input required" value="'+(isEmpty(o.spec)?'':o.spec)+'"/></td>',
                '</td></tr>'
            ].join('');
        },
        historyMembers:function () {
            $.post(rurl('scm/expOrderMemberHistory/list'),{'orderId':id},function (res) {
                if(isSuccess(res)){
                    $.each(res.data || [],function (i,o) {
                        var midObj = $('tr[mid="'+o.id+'"]');
                        if(midObj.size()>0){
                            $.each(keys,function (j,k) {
                                var keyObj = midObj.find('['+k+']');
                                if(keyObj.size()==1){
                                    var oval = removeNull(o[k]+''),nval = keyObj.html();
                                    if('quantity'==k||'totalPrice'==k){
                                        oval = formatCurrency(o[k]);
                                    }
                                    if(oval!=nval){
                                        keyObj.html('<div class="original-info">'+oval+'</div><div class="new-info">'+nval+'</div>');
                                    }
                                }
                            });
                        }
                    });
                }
            });
        }
    };
    inspec.renderData();
    $('.file-size').attr({'doc-id':id});
    initFileSize();

    $('.share-net-weight').click(function () {
        layer.prompt({id:'snwei',formType:3,maxlength: 12,title:'请填写总净重',success:function () {$('#snwei').find('input').attr({'NUMBER':''});initInput();}},function(val, index){
            show_loading();
            $.post(rurl('scm/expOrder/shareNetWeight'),{'id':id,'netWeight':val},function (res) {
                if(isSuccess(res)){
                    layer.close(index);
                    inspec.renderData();
                    show_success('分摊成功');
                }
            });
        });
    });

    $('.share-gross-weight').click(function () {
        layer.prompt({id:'sgwei',formType:3,maxlength: 12,title:'请填写总毛重',success:function () {$('#sgwei').find('input').attr({'NUMBER':''});initInput();}},function(val, index){
            show_loading();
            $.post(rurl('scm/expOrder/shareGrossWeight'),{'id':id,'grossWeight':val},function (res) {
                if(isSuccess(res)){
                    layer.close(index);
                    inspec.renderData();
                    show_success('分摊成功');
                }
            });
        });
    });
    $('.edit-member').click(function () {
        $('#materialBody').find('tr').addClass('edit');
        $('.confirm-inspection').addClass('layui-btn-disabled').attr({'disabled':true});
        $(this).hide();
        $('.save-edit-member').show();
    });
    $('.print-checklist').click(function () {
        openwin('order/exp/print-checklist.html?id='+id,'核对单打印',{ area: ['1000px', '0px'],shadeClose:true});
    });

    var taskArg = {'operation':'exp_order_inspection','documentId':id,'documentClass':'ExpOrder','newStatusCode':'waitEdit'};
    //退回修改
    $('.back-business').click(function () {
        layer.prompt({formType:2,maxlength: 500,title:'请填写退回原因'},function(val, index){
            show_loading();
            taskArg['memo'] = val;
            $.post(rurl('scm/expOrder/examine'),taskArg,function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
    });
    //保存修改明细
    $('.save-edit-member').click(function () {
        var members = jsonList($('#orderMemberForm').serializeJson());
        postJSON('scm/expOrder/saveInspectionMemberEdit',{"orderId":id,"members":members},function (res) {
            show_success('修改成功');
            inspec.renderData();
        });
    });
    //验货完成
    $('.confirm-inspection').click(function(){
        show_loading();
        $.post(rurl('scm/expOrder/confirmInspection'),{'id':id},function (res) {
            if(isSuccess(res)){
                parent.reloadList();
                closeThisWin('验货完成');
            }
        });
    });
});
function calculationAll(e){
    var tquantity = 0.0,ttotalPrice = 0.0,tcartonNum = 0,tnetWeight = 0.0,tgrossWeight = 0.0;
    if(!isEmpty(e)){
        var $that = $(e),$parent=$that.parent().parent().parent(),$quantity=$parent.find('input[name="quantity"]'),$unitPrice=$parent.find('input[name="unitPrice"]'),$totalPrice=$parent.find('input[name="totalPrice"]');
        switch ($that.attr('name')){
            case 'quantity':
                var quantity = Math.round(parseFloat($quantity.val() || '0.0')*100)/100;
                var totalPrice = Math.round(parseFloat($totalPrice.val() || '0.0')*100)/100;
                $unitPrice.val(Math.round(totalPrice/quantity*10000)/10000);
                break;
            case 'unitPrice':
                var quantity = Math.round(parseFloat($quantity.val() || '0.0')*100)/100;
                var unitPrice = Math.round(parseFloat($unitPrice.val() || '0.0')*10000)/10000;
                $totalPrice.val(Math.round(quantity*unitPrice*100)/100);
                break;
            case 'totalPrice':
                var quantity = Math.round(parseFloat($quantity.val() || '0.0')*100)/100;
                if(quantity==0.0){$unitPrice.val('0.0000');return;}
                var totalPrice = Math.round(parseFloat($totalPrice.val() || '0.0')*100)/100;
                $unitPrice.val(Math.round(totalPrice/quantity*10000)/10000);
                break;
        }
    }
    $.each($('#materialBody').find('tr'),function (i,o) {
        var $that = $(o);
        tquantity += Math.round(parseFloat($that.find('input[name="quantity"]').val() || '0.0')*100)/100;
        ttotalPrice += Math.round(parseFloat($that.find('input[name="totalPrice"]').val() || '0.0')*100)/100;
        tcartonNum += parseInt($that.find('input[name="cartonNum"]').val() || '0');
        tnetWeight += Math.round(parseFloat($that.find('input[name="netWeight"]').val() || '0.0')*10000)/10000;
        tgrossWeight += Math.round(parseFloat($that.find('input[name="grossWeight"]').val() || '0.0')*10000)/10000;
    });
    $quantityTotal.html(Math.round(tquantity*100)/100);
    $totalPriceTotal.html(formatCurrency(Math.round(ttotalPrice*100)/100));
    $cartonTotal.html(tcartonNum);
    $netWeightTotal.html(Math.round(tnetWeight*100)/100);
    $grossWeightTotal.html(Math.round(tgrossWeight*100)/100);
}
function deleteRow(e) {
    var $per = $(e).parent().parent();
    $per.remove();
}
