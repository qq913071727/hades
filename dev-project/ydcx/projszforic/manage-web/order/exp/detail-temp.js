layui.define(['layer','laytpl','element','table'], function(exports){
    var layer = layui.layer,laytpl=layui.laytpl,element=layui.element,table=layui.table,
    detailTemp = {
        render:function (id,operation,loadDeclare,declareEdit) {
            $.post(rurl('scm/expOrder/detail'),{'id':id,'operation':operation},function (res) {
                if(isSuccessWarnClose(res)){
                    detailTemp.renderData(res.data);
                    if(loadDeclare){
                        detailTemp.renderDeclare(res.data,declareEdit);
                    }
                }
            });
            $('.file-size').attr({'doc-id':id});
            initFileSize();
        },
        renderData:function(data){
            var order=data.order,logistiscs=data.logistiscs,members=data.members||[];
            laytpl($('#orderViewTemp').html()).render(order,function (html) {
                $('#orderView').html(html);
            });
            laytpl($('#logistiscsViewTemp').html()).render(logistiscs,function (html) {
                $('#logistiscsView').html(html);
            });
            table.render({
                elem: '#memberMaterial',limit:5000,page:false
                ,cols: [[
                    {type:'numbers',title:'序号',width:45}
                    ,{field:'materielStatusName',title:'物料状态',width:70,templet:'#materielStatusTemp',align:'center'}
                    ,{field:'model',title:'型号',width:160}
                    ,{field:'name',title:'名称',width:180,templet:'#nameTemp'}
                    ,{field:'brand',title:'品牌',width:180,templet:'#brandTemp'}
                    ,{field:'countryName',title:'产地',width:80}
                    ,{field:'goodsCode',title:'客户料号',width:100}
                    ,{field:'spec',title:'规格描述',templet:'#specTemp'}
                    ,{field:'declareSpec',title:'申报要素'}
                ]]
                , data: members
            });
            table.render({
                elem: '#memberQty' ,totalRow: true,limit:5000,page:false
                , cols: [[
                    {type:'numbers',title:'序号',width:45}
                    ,{field:'model',title:'型号',width:180, totalRowText: '合计'}
                    ,{field:'name',title:'名称',width:180,templet:'#nameTemp'}
                    ,{field:'brand',title:'品牌',width:180,templet:'#brandTemp'}
                    ,{field:'quantity',title:'数量',width:90,align:'money', totalRow: true}
                    ,{field:'unitName',title:'单位',width:80,align:'center'}
                    ,{field:'unitPrice',title:'委托单价',width:90,align:'right'}
                    ,{field:'totalPrice',title:'委托总价',width:90,align:'money', totalRow: true}
                    ,{field:'decUnitPrice',title:'报关单价',width:90,align:'right'}
                    ,{field:'decTotalPrice',title:'报关总价',width:90,align:'money', totalRow: true}
                    ,{field:'currencyName',title:'币制',width:80,templet:'#currencyTemp',align:'center'}
                ]], data: members
            });
            table.render({
                elem: '#memberOther',totalRow: true,limit:5000,page:false
                , cols: [[
                    {type:'numbers',title:'序号',width:45}
                    ,{field:'name',title:'名称',width:180,templet:'#nameTemp'}
                    ,{field:'brand',title:'品牌',width:180,templet:'#brandTemp'}
                    ,{field:'model',title:'型号',width:180, totalRowText: '合计'}
                    ,{field:'cartonNum',title:'箱数',width:90,align:'right', totalRow: true}
                    ,{field:'cartonNo',title:'箱号',width:120}
                    ,{field:'netWeight',title:'净重',width:90, totalRow: true,totalRowType: 'num4'}
                    ,{field:'grossWeight',title:'毛重',width:90, totalRow: true,totalRowType: 'num4'}
                ]]
                , data: members
            });
            //totalRowType，num4保留4位小数点，int整数，其他保留2位小数点
            $('.currency-name').html(order.currencyName);
            formatTableData();
        },
        renderDeclare:function (data,declareEdit) {
            if(isEmpty(declareEdit)){
                declareEdit = false;
            }
            var order=data.order;
            var decInfo = {
                declareEdit:declareEdit,
                orderId:order.id,
                transactionModeDesc:order.transactionModeDesc,
                transCosts:order.transCosts,
                insuranceCosts:order.insuranceCosts,
                miscCosts:order.miscCosts,
                districtCodeName:order.districtCodeName,

            }
            var tempUrl = '';
            if(isEmpty(order.declarationTempId)){
                tempUrl = rurl('scm/declarationTemp/expDefault')
            }else{
                tempUrl = rurl('scm/declarationTemp/detail?id='+order.declarationTempId);
            }
            $.get(tempUrl,{},function (res) {
                if(res.data!=null){
                    decInfo =  $.extend({},res.data,decInfo);
                }
                laytpl($('#decViewTemp').html()).render(decInfo,function (html) {
                    $('#decView').html(html);
                });
            });
        }
    };
    exports('detailTemp', detailTemp);
});
function deployDecTemp() {
    var mid=parent.parent.obtainMIdByOperation('dec_temp_list'),taskArg={"documentId":"","documentClass":"","operation":"dec_temp_add"};
    if(isEmpty(mid)){prompt_warn('对不起您无此权限,请联系贵司管理员授权!');return;}
    setLocalStorage('TASK_'+mid,JSON.stringify({"billType":"exp"}));
    setLocalStorage('AUTO_TASK',JSON.stringify(taskArg));
    parent.parent.refreshTab(mid);
}
function editDecInfo(orderId) {
    openwin('order/exp/edit-dec-info.html?id='+orderId,'修改申报信息',{area:['900px','420px']});
}