layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('scm/expOrder/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'task', width:50, title: '任务',align:'center',toolbar:'#taskTemp', fixed: true}
                    ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                    ,{field:'docNo', width:126, title: '订单编号',templet:'#docNoTemp', fixed: true}
                    ,{field:'clientNo', width:120, title: '客户单号'}
                    ,{field:'orderDate', width:80, title: '订单日期'}
                    ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                    ,{field:'busPersonName', width:70, title: '跟进商务'}
                    ,{field:'salePersonName',width:70, title: '销售人员'}
                    ,{field:'cusTrafModeName', width:120, title: '运输方式'}
                    ,{field:'distinatePortName', width:110, title: '指运港'}
                    ,{field:'purchaseContractNo', width:120, title: '采购合同',align:'center'}
                    ,{field:'transactionModeDesc', width:80, title: '成交方式'}
                    ,{field:'declareTypeDesc', width:80, title: '报关类型'}
                    ,{field:'totalPrice', width:90, title: '订单金额',align:'money'}
                    ,{field:'currencyName', width:60, title: '币制',align:'center'}
                    ,{field:'isSettleAccount', width:70, title: '是否结汇',align:'center',templet:'#isSettleAccountTemp'}
                    ,{field:'expEntrust', width:100, title: '附件管理',templet:'#expOrderAllTemp',align:'center'}
                ]]
            },listQueryDone:function (da) {
                initFileSize(); formatTableData();
                $('#total-area').remove();
                if(da.count > 0){
                    var $ares = $('.choice-btn');
                    $.post('/scm/expOrder/summaryOrderAmount',$('.query-form').serialize(),function (res) {
                        if(isSuccess(res)){
                            var data = res.data;
                            $ares.before('<div id="total-area">总订单金额：<span>'+formatCurrency(data)+'</span></div>');
                        }
                    });
                }
            }
        });
    });
});
function showDetail(id) {
    openwin(rurl('order/exp/detail.html?id='+id),'订单详情',{area: ['0', '0'],shadeClose:true});
}
function orderCost(o){
   var datas = getCheckDatas();
   if(datas.length!=1){
       show_msg('请选择您要操作的记录');return;
   }
   openwin(o.url+'?orderNo='+datas[0].docNo,o.name, {area: ['1200px', '600px']});
}

function gotoPrice(orderNo) {
    openwin(rurl('order/priceexp/list.html?mid=5055&docNo='+orderNo),'订单审价',{area: ['1200px', '800px'],shadeClose:true});
}