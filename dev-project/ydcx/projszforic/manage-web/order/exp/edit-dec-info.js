layui.use(['layer','form','laytpl'], function() {
    var layer = layui.layer, form = layui.form,laytpl=layui.laytpl;
    var id = getUrlParam("id");
    var orderDecArg = {};
    $('.sel-load').selLoad(function () {
        form.render('select');
        show_loading();
        $.post(rurl('scm/expOrder/detail'),{'id':id,'operation':''},function (res) {
            if(isSuccessWarnClose(res)){
                var data=res.data,order=data.order;
                orderDecArg['arg1'] = order.cusTradeCountryName;//运抵国(地区)
                orderDecArg['arg2'] = order.cusTradeNationCodeName;//贸易国别(地区)
                orderDecArg['arg3'] = order.distinatePortName;//指运港
                orderDecArg['arg4'] = order.cusTrafModeName;//运输方式
                orderDecArg['arg5'] = order.districtCodeName;//境内货源地
                orderDecArg['arg6'] = order.destinationCountryName;//最终目的国
                var orderInfo = {
                    id:order.id,tempId:order.declarationTempId,
                    transactionModeName:order.transactionModeDesc,
                    feeMark:'', feeMarkName:'', feeRate:'', feeCurr:'', feeCurrName:'',
                    insurMark:'', insurMarkName:'', insurRate:'', insurCurr:'', insurCurrName:'',
                    otherMark:'', otherMarkName:'', otherRate:'', otherCurr:'', otherCurrName:''
                }
                if(!isEmpty(order.transCosts)){
                    var arrays = order.transCosts.split('/');
                    orderInfo.feeMark = arrays[2];
                    orderInfo.feeMarkName = markMap[orderInfo.feeMark];
                    orderInfo.feeRate = arrays[1];
                    orderInfo.feeCurr = arrays[0];
                    orderInfo.feeCurrName = curMap[orderInfo.feeCurr];
                }
                if(!isEmpty(order.insuranceCosts)){
                    var arrays = order.insuranceCosts.split('/');
                    orderInfo.insurMark = arrays[2];
                    orderInfo.insurMarkName = markMap[orderInfo.insurMark];
                    orderInfo.insurRate = arrays[1];
                    orderInfo.insurCurr = arrays[0];
                    orderInfo.insurCurrName = curMap[orderInfo.insurCurr];
                }
                if(!isEmpty(order.miscCosts)){
                    var arrays = order.miscCosts.split('/');
                    orderInfo.otherMark = arrays[2];
                    orderInfo.otherMarkName = markMap[orderInfo.otherMark];
                    orderInfo.otherRate = arrays[1];
                    orderInfo.otherCurr = arrays[0];
                    orderInfo.otherCurrName = curMap[orderInfo.otherCurr];
                }
                form.val('form',orderInfo);
                loadTempInfo($('#tempId').val());
            }
        });
    });
    form.on("select(tempId)",function (data) {
        loadTempInfo(data.value);
    });
    var loadTempInfo = function(tempId){
        if(isEmpty(tempId)){
            prompt_warn("未配置出口默认报关模板，请联相关人员配置！！！");
            return;
        }
        show_loading();
        $.get(rurl('scm/declarationTemp/detail'),{'id':tempId},function (res) {
            if(isSuccessWarn(res)){
                laytpl($('#decViewTemp').html()).render($.extend({},res.data,orderDecArg),function (html) {
                    $('#decView').html(html);
                });
            }
        });
    };

    form.on('submit(save-btn)', function(data){
        if("CIF" == $('#transactionModeName').val()){
            var $feeMarkName = $('#feeMarkName');if(isEmpty($feeMarkName.val())){show_error("必填项不能为空");$feeMarkName.select();return false;}
            var $feeRate = $('#feeRate');if(isEmpty($feeRate.val())){show_error("必填项不能为空");$feeRate.select();return false;}

            var $feeCurrName = $('#feeCurrName');
            if($('#feeMark').val()=="1"){
                $('#feeCurr').val('000');
                $feeCurrName.val('');
            }else{
               if(isEmpty($feeCurrName.val())){show_error("必填项不能为空");$feeCurrName.select();return false;}
            }

            var $insurMarkName = $('#insurMarkName');if(isEmpty($insurMarkName.val())){show_error("必填项不能为空");$insurMarkName.select();return false;}
            var $insurRate = $('#insurRate');if(isEmpty($insurRate.val())){show_error("必填项不能为空");$insurRate.select();return false;}
            var $insurCurrName = $('#insurCurrName');
            if($('#insurMark').val()=="1"){
                $('#insurCurr').val('000');
                $insurCurrName.val('');
            }else{
                if(isEmpty($insurCurrName.val())){show_error("必填项不能为空");$insurCurrName.select();return false;}
            }

            var $otherMark = $('#otherMark');if(isEmpty($otherMark.val())){$otherMark.val('1');}
            var $otherRate = $('#otherRate');if(isEmpty($otherRate.val())){$otherRate.val('0.0');}
            var $otherCurr = $('#otherCurr');if(isEmpty($otherCurr.val())){$otherCurr.val('000');}

            if($otherMark.val()!='1'){
                var $otherCurrName = $('#otherCurrName');
                if(isEmpty($otherCurrName.val())){
                    show_error("必填项不能为空");$otherCurrName.select();return false;
                }
            }
        }
        formSub('scm/expOrder/setupDeclaration',function (res) {
            parent.refPage();
            closeThisWin('设置成功');
        });
        return false;
    });
});
var curMap = {"000":"", "AUD":"澳大利亚元", "CAD":"加拿大元", "CHF":"瑞士法郎", "CNY":"人民币", "DKK":"丹麦克朗", "EUR":"欧元", "GBP":"英镑", "HKD":"港币", "IDR":"印度尼西亚卢比", "JPY":"日本元", "KRW":"韩国元", "MOP":"澳门元", "MYR":"马来西亚林吉特", "NOK":"挪威克朗", "NZD":"新西兰元", "PHP":"菲律宾比索", "RUB":"俄罗斯卢布", "SEK":"瑞典克朗", "SGD":"新加坡元", "THB":"泰国铢", "TWD":"新台币", "USD":"美元"}
markMap = {"1":"率","2":"单价","3":"总价"};