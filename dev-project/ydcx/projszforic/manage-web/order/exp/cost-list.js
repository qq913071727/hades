layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listQueryArg:{
            url:rurl('scm/expOrderCost/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'customerName', width:200, title: '客户名称',templet:'#customerNameTemp'}
                ,{field:'expOrderNo', width:135, title: '订单号',templet:'#docNoTemp'}
                ,{field:'customerOrderNo', width:135, title: '客户单号',templet:'#customerOrderNoTemp'}
                ,{field:'expenseSubjectName', width:110, title: '费用科目'}
                ,{field:'receAmount', width:100, title: '应收金额',align:'money'}
                ,{field:'acceAmount', width:100, title: '已收金额',align:'money'}
                ,{field:'collectionSourceDesc', width:100, title: '支付方式'}
                ,{field:'payAmount', width:100, title: '应付金额',align:'money'}
                ,{field:'payCurrencyName', width:80, title: '应付币制'}
                ,{field:'extend1', width:180, title: '供应商'}
                ,{field:'happenDate', width:95, title: '发生日期'}
                ,{field:'dateCreated', width:95, title: '创建日期'}
                ,{field:'operator', width:80, title: '操作人'}
                ,{field:'memo', title: '备注'}
            ]]
        },listQueryDone:function(){
            formatTableData();
        }
    });
});

function importSuccess(msg){prompt_success(msg,function (){show_loading();setTimeout(function () {hide_loading();reloadList();},1000)});}
