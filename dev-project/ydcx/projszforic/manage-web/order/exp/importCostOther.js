layui.use(['layer','upload'], function() {
    var layer = layui.layer, upload = layui.upload;
    upload.render({
        elem: '#importExcel',url:rurl('scm/costChangeRecordExp/importOtherCost'),size:10240,exts:'xls|xlsx'
        ,acceptMime:'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ,before:function (res) {
            show_loading();
        } ,done: function(res){
            if(isSuccessWarn(res)){
                closeThisWin();
                parent.importSuccess('导入成功，请查看订单费用数据确认');
            }
        },error:function (index, upload) {
            hide_loading();
        }
    });
});