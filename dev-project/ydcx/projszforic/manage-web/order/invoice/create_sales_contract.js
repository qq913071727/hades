var icoObjs = [],idx=0,sendIng=false;
layui.use(['layer','table'], function() {
    var layer = layui.layer,table=layui.table,id = getUrlParam('id');
    table.render({
        elem: '#table-list',limit:5000,id:'table-list',height:'full-42',page:false,method:'post',
        url:rurl('/scm/impOrderInvoice/idsList'),where:{'ids':id},response:{msgName:'message',statusCode: '1'}
        ,cols: [[
            {field:'ico',width:40,align:'center',title:'',templet:'#icoTemp'}
            ,{field:'statusName', width:80, title: '订单状态',align:'center',templet:'#statusTemp'}
            ,{field:'docNo', width:126, title: '订单号'}
            ,{field:'orderDate', width:80, title: '订单日期'}
            ,{field:'customerName', width:200, title: '客户名称'}
            ,{field:'businessTypeDesc', width:70, title: '业务类型'}
            ,{field:'totalDifferenceVal', width:90, title: '客户总票差',align:'right'}
            ,{field:'adjustmentAmount', width:90, title: '票差调整金额',align:"form",templet:'#adjustmentAmountTemp'}
            ,{field:'message', title: '消息',templet:'#messageTemp'}
        ]],done:function () {
            initInput();
            initImport();
        }
    });
    $('#startSend').click(function (){
        if(sendIng==false){
            sendIng=true;
            $('div[icoId]').html('<img src="'+rurl('images/indicator.gif')+'"/>');
            $('div[messageId]').html('');
            startSend();
        }
    });
});
function initImport() {
    idx = 0;icoObjs = $('div[icoId]');
}
var paArg = {id:'',adjustmentAmount:0.0};
function startSend(){
    if(icoObjs.size()>idx){
        var $id = $(icoObjs[idx]);idx++;
        paArg['id'] = $id.attr('icoId');
        paArg['adjustmentAmount'] = $('#ad'+paArg['id']).val();
        $.post(rurl('scm/impOrderSalesContract/generateSalesContract'),paArg,function (res) {
            if(res.code=='1'){
                $id.html('<img src="'+rurl('images/success.png')+'"/>');
            }else{
                $id.html('<img src="'+rurl('images/error.png')+'"/>');
                $('div[messageId="'+$id.attr('icoId')+'"]').html(res.message);
            }
            startSend();
        });
    }else{
        sendIng = false;initImport();
        prompt_success("处理完成");
        parent.reloadList();
    }
}