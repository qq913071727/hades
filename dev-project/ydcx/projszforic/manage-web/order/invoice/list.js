layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form','table'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form,table = layui.table;
    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('scm/impOrderInvoice/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                    ,{field:'docNo', width:126, title: '订单编号',templet:'#docNoTemp', fixed: true}
                    ,{field:'orderDate', width:80, title: '订单日期'}
                    ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                    ,{field:'businessTypeDesc', width:70, title: '业务类型'}
                    ,{field:'declareTypeName', width:70, title: '报关类型'}
                    ,{field:'totalPrice', width:90, title: '订单金额',align:'money'}
                    ,{field:'currencyName', width:60, title: '币制',align:'center'}
                    ,{field:'scDocNo', width:126, title: '销售合同号'}
                    ,{field:'scTotalPrice', width:90, title: '合同金额',align:'money'}
                    ,{field:'invDocNo', width:126, title: '开票申请号'}
                    ,{field:'invoiceVal', width:90, title: '开票金额',align:'money'}
                    ,{field:'taxpayerNo', width:140, title: '客户纳税识别号'}
                    ,{field:'invoiceBankName', width:140, title: '客户开票银行'}
                    ,{field:'invoiceBankAccount', width:140, title: '客户开票银行账号'}
                ]]
            },listQueryDone:function (){
                formatTableData();
            }
        });
    });
});
function showDetail(id) {
    openwin(rurl('order/imp/detail.html?id='+id),'订单详情',{area: ['0', '0'],shadeClose:true});
}
function createSc(o) {
    var datas = getDatas();
    if(datas.length==0){show_msg('请至少选择一条订单信息');return;}
    var ids=[];
    for(var i=0;i<datas.length;i++){
        var item = datas[i];
        if('imp_cooperate'!=item.businessType){
            prompt_warn('订单【'+item.docNo+'】不是自营业务无法创建销售合同！');return;
        }
        if(!isEmpty(item.scDocNo)){
            prompt_warn('订单【'+item.docNo+'】已经生成销售合同，请不要重复申请！');return;
        }
        if(isEmpty(item.taxpayerNo)||isEmpty(item.invoiceBankName)||isEmpty(item.invoiceBankAccount)){
            prompt_warn('客户【'+item.customerName+'】开票信息还未完善！');return;
        }
        ids.push(item.id);
    }
    openwin(o.url+'?id='+ids.join(','),o.name,{area:['1100px','400px']});
}
function createInv (o) {
    var datas = getDatas();
    if(datas.length==0){show_msg('请至少选择一条订单信息');return;}
    var ids=[];
    for(var i=0;i<datas.length;i++){
        var item = datas[i];
        if(!isEmpty(item.invDocNo)){
            prompt_warn('订单【'+item.docNo+'】已经生成发票申请，请不要重复申请！');return;
        }
        if('imp_cooperate'==item.businessType&&isEmpty(item.scDocNo)){
            prompt_warn('订单【'+item.docNo+'】自营业务，请先创建销售合同！');return;
        }
        if(isEmpty(item.taxpayerNo)||isEmpty(item.invoiceBankName)||isEmpty(item.invoiceBankAccount)){
            prompt_warn('客户【'+item.customerName+'】开票信息还未完善！');return;
        }
        ids.push(item.id);
    }
    openwin(o.url+'?id='+ids.join(','),o.name,{area:['1000px','400px']});
}