layui.config({base: '/order/imp/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("orderId");
    $('#orderDetail').load(rurl('order/imp/detail-temp.html'),function () {
        detailTemp.render(id,'');
    });
});