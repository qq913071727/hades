layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listQueryArg:{
            url:rurl('scm/costChangeRecord/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                ,{field:'impOrderNo', width:126, title: '订单号',templet:'#docNoTemp'}
                ,{field:'expenseSubjectName', width:150, title: '费用科目'}
                ,{field:'originCostAmount', width:140, title: '原始金额',align:'money'}
                ,{field:'costAmount', width:140, title: '变更后金额',align:'money'}
                ,{field:'dateUpdated', width:150, title: '操作时间'}
                ,{field:'updateBy', width:150, title: '操作人'}
                ,{field:'memo', title: '备注'}
            ]]
        },listQueryDone:function(){
            formatTableData();
        }
    });
});

function adjust(o) {

}
