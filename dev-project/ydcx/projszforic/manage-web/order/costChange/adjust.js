layui.use(['layer','form','table'], function(){
    var layer = layui.layer,table = layui.table,odocNo='',fn = {
        searchCost:function () {
            var $impOrderNo = $("#impOrderNo");
            if(!isEmpty($impOrderNo.val())) {
                if(odocNo == $impOrderNo.val()){return;}
                show_loading();
                $.get(rurl('scm/impOrderCost/checkGetAdjustCost'),{'impOrderNo':$impOrderNo.val(),'getCost':true},function (res) {
                    if(isSuccess(res)) {
                        fn.costList(res.data);
                    } else {
                        fn.costList([]);
                    }
                });
            } else {
                $impOrderNo.focus();
                show_error('请输入订单号查询');
                fn.costList([]);
            }
            odocNo = $impOrderNo.val();
        },
        costList:function(data) {
            table.render({
                elem: '#table-list',limit:5000,id:'table-list',totalRow: true,height:'full-152'
                ,cols: [[
                    {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
                    ,{field:'operation', width:60, title: '操作',templet:'#operationTemp',align:'center', fixed: true}
                    ,{field: 'impOrderNo', title: '订单号', width: 150}
                    ,{field: 'expenseSubjectName', title: '科目名称', width: 150}
                    ,{field: 'receAmount', title: '费用金额（元）', width: 120,align:'money', totalRow: true}
                    ,{field: 'acceAmount', title: '已收金额（元）', width: 120,align:'money', totalRow: true}
                    ,{field: 'happenDate', title: '发生日', width: 100,align:'center'}
                    ,{field: 'periodDate', title: '账期日', width: 100,align:'center'}
                    ,{field: 'operator', title: '操作人', width: 80}
                    ,{field: 'memo', title: '费用说明'}
                ]]
                ,data: data
            });
            formatTableData();
        }
    };
    $(".search-btn").click(function () {
        odocNo = '';
        fn.searchCost();
    });
    var paramsOrderNo = getUrlParam("orderNo");
    if(isNotEmpty(paramsOrderNo)){
        $('#impOrderNo').val(paramsOrderNo);
        $(".search-btn").click();
    }
    $(".add-cost-page").click(function () {
        var impOrderNo = $("#impOrderNo").val();
        if(isEmpty(impOrderNo)) {
            layer.msg('请先输入正确的订单号查询费用信息');
            return;
        }
        openCost(impOrderNo,'');
    });
    $('.auto-adjust-imp-order').autocomplete({
        source: function (request, response) {
            $.post(rurl('scm/costChangeRecord/selectAdjustOrder'),{impOrderNo: trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: item.impOrderNo+'('+item.customerName+')', value: item.impOrderNo, data: item}}));
            });
        }, minLength: 0,autoFocus:true, delay: 500,
        select: function(event, ui){
            $('#impOrderNo').val(ui.item.data.impOrderNo);
            fn.searchCost();
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    }).change(function () {
        fn.searchCost();
    });
});
function openCost(impOrderNo,impOrderCostId) {
    openwin(rurl('order/costChange/cost-page.html?impOrderNo='+impOrderNo+"&impOrderCostId=" + impOrderCostId),isEmpty(impOrderCostId) ? '新增费用' : '修改费用',{area: ['800px', '360px'],shadeClose:true});
}
function requestOderCost() {
    $(".search-btn").click();
}
