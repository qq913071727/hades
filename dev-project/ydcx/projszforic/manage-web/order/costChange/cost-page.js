layui.use(['layer','form'], function(){
    var layer = layui.layer,form = layui.form;
    var impOrderNo = getUrlParam("impOrderNo");
    $("#impOrderNo").val(impOrderNo);
    $("#impOrderCostId").val(getUrlParam("impOrderCostId"));
    $('.sel-load').selLoad(function () {
        form.render('select');
        if(!isEmpty(getUrlParam("impOrderCostId"))) {
            $.get(rurl('scm/impOrderCost/getSimpleCostInfo'),{'id':getUrlParam("impOrderCostId")},function (res) {
                form.val('costForm',res.data);
                $("#costAmount").val(res.data.receAmount);
                //值被覆盖
                $("#impOrderNo").val(impOrderNo);
                $("#oldCostAmount").val(res.data.receAmount);

                var $expenseSubjectId = $('#expenseSubjectId');
                var subjectId = $expenseSubjectId.val(),subjectName = $expenseSubjectId.find("option:selected").text();
                $expenseSubjectId.get(0).length = 0;
                $expenseSubjectId.append('<option value="'+subjectId+'">'+subjectName+'</option>');
                form.render('select');
            });
        }
    });

    $(".save-btn").click(function () {
        if(checkedForm($('#costForm'))){
            //修改费用应收费用信息必须变化
            if(!isEmpty(getUrlParam("impOrderCostId"))) {
                if($("#oldCostAmount").val() == $("#costAmount").val()) {
                    layer.msg('费用没有变更');
                    return;
                }
            }
            show_loading();
            $.post(rurl('scm/costChangeRecord/adjust'),$('.cost-form').serialize(),function (res) {
                if(isSuccess(res)) {
                    closeThisWin("保存成功");
                    //重新渲染列表
                    window.parent.requestOderCost();
                }
            });
        }
    });
});



