layui.use(['layer','form'], function(){
    var layer = layui.layer,form = layui.form;
    var expOrderNo = getUrlParam("expOrderNo");
    $("#expOrderNo").val(expOrderNo);
    $("#expOrderCostId").val(getUrlParam("expOrderCostId"));
    $('.sel-load').selLoad(function () {
        form.render('select');
        if(!isEmpty(getUrlParam("expOrderCostId"))) {
            $.get(rurl('scm/expOrderCost/getSimpleCostInfo'),{'id':getUrlParam("expOrderCostId")},function (res) {
                form.val('costForm',res.data);
                $("#costAmount").val(res.data.receAmount);
                //值被覆盖
                $("#expOrderNo").val(expOrderNo);
                $("#oldCostAmount").val(res.data.receAmount);
                $("#oldCollectionSource").val(res.data.collectionSource);

                var $expenseSubjectId = $('#expenseSubjectId');
                var subjectId = $expenseSubjectId.val(),subjectName = $expenseSubjectId.find("option:selected").text();
                $expenseSubjectId.get(0).length = 0;
                $expenseSubjectId.append('<option value="'+subjectId+'">'+subjectName+'</option>');
                form.render('select');
            });
        }
    });

    $(".save-btn").click(function () {
        if(checkedForm($('#costForm'))){
            //修改费用应收费用信息必须变化
            if(!isEmpty(getUrlParam("expOrderCostId"))) {
                if($("#oldCollectionSource").val()==$("#collectionSource").val() && $("#oldCostAmount").val() == $("#costAmount").val()) {
                    layer.msg('费用没有变更');
                    return;
                }
            }
            show_loading();
            $.post(rurl('scm/costChangeRecordExp/adjust'),$('.cost-form').serialize(),function (res) {
                if(isSuccess(res)) {
                    closeThisWin("保存成功");
                    //重新渲染列表
                    window.parent.requestOderCost();
                }
            });
        }
    });
});



