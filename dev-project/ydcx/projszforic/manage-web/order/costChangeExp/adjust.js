layui.use(['layer','form','table'], function(){
    var layer = layui.layer,table = layui.table,odocNo='',fn = {
        searchCost:function () {
            var $expOrderNo = $("#expOrderNo");
            if(!isEmpty($expOrderNo.val())) {
                if(odocNo == $expOrderNo.val()){return;}
                show_loading();
                $.get(rurl('scm/expOrderCost/checkGetAdjustCost'),{'expOrderNo':$expOrderNo.val(),'getCost':true},function (res) {
                    if(isSuccess(res)) {
                        fn.costList(res.data);
                    } else {
                        fn.costList([]);
                    }
                });
            } else {
                $expOrderNo.focus();
                show_error('请输入订单号查询');
                fn.costList([]);
            }
            odocNo = $expOrderNo.val();
        },
        costList:function(data) {
            table.render({
                elem: '#table-list',limit:5000,id:'table-list',totalRow: true,height:'full-152'
                ,cols: [[
                    {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
                    ,{field:'operation', width:60, title: '操作',templet:'#operationTemp',align:'center', fixed: true}
                    ,{field: 'expOrderNo', title: '订单号', width: 150}
                    ,{field: 'expenseSubjectName', title: '科目名称', width: 150}
                    ,{field: 'receAmount', title: '费用金额（元）', width: 120,align:'money', totalRow: true}
                    ,{field: 'acceAmount', title: '已收金额（元）', width: 120,align:'money', totalRow: true}
                    ,{field: 'collectionSourceDesc', title: '支付方式', width: 120}
                    // ,{field: 'happenDate', title: '发生日', width: 100,align:'center'}
                    // ,{field: 'periodDate', title: '账期日', width: 100,align:'center'}
                    ,{field: 'operator', title: '操作人', width: 80}
                    ,{field: 'memo', title: '费用说明'}
                ]]
                ,data: data
            });
            formatTableData();
        }
    };
    $(".search-btn").click(function () {
        odocNo = '';
        fn.searchCost();
    });
    var paramsOrderNo = getUrlParam("orderNo");
    if(isNotEmpty(paramsOrderNo)){
        $('#expOrderNo').val(paramsOrderNo);
        $(".search-btn").click();
    }
    $(".add-cost-page").click(function () {
        var expOrderNo = $("#expOrderNo").val();
        if(isEmpty(expOrderNo)) {
            layer.msg('请先输入正确的订单号查询费用信息');
            return;
        }
        openCost(expOrderNo,'');
    });
    $('.auto-adjust-exp-order').autocomplete({
        source: function (request, response) {
            $.post(rurl('scm/costChangeRecordExp/selectAdjustOrder'),{expOrderNo: trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: item.expOrderNo+'('+item.customerName+')', value: item.expOrderNo, data: item}}));
            });
        }, minLength: 0,autoFocus:true, delay: 500,
        select: function(event, ui){
            $('#expOrderNo').val(ui.item.data.expOrderNo);
            fn.searchCost();
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    }).change(function () {
        fn.searchCost();
    });
});
function openCost(expOrderNo,expOrderCostId) {
    openwin(rurl('order/costChangeExp/cost-page.html?expOrderNo='+expOrderNo+"&expOrderCostId=" + expOrderCostId),isEmpty(expOrderCostId) ? '新增费用' : '修改费用',{area: ['800px', '360px'],shadeClose:true});
}
function requestOderCost() {
    $(".search-btn").click();
}
