layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form','table'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form,table = layui.table;
    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('scm/impOrderPayment/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                     ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                     ,{field:'docNo', width:126, title: '订单编号',templet:'#docNoTemp', fixed: true}
                     ,{field:'clientNo', width:126, title: '订单编号',templet:'#clientNoTemp', fixed: true}
                    ,{field:'orderDate', width:80, title: '订单日期'}
                    ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                    ,{field:'supplierName', width:260, title: '供应商'}
                    ,{field:'businessTypeDesc', width:70, title: '业务类型'}
                    ,{field:'declareTypeName', width:70, title: '报关类型'}
                    ,{field:'totalPrice', width:90, title: '订单金额',align:'money'}
                    ,{field:'currencyName', width:60, title: '币制',align:'center'}
                    ,{field:'applyPayVal', width:90, title: '付汇申请金额',align:'money'}
                    ,{field:'payVal', width:90, title: '付汇付出金额',align:'money'}
                    ,{field:'allowApplyVal', width:90, title: '可申请金额',align:'money',templet:'#allowApplyValTemp'}
                    ,{field:'overdueDay', width:90, title: '超期天数',align:'right'}
                    ,{field:'paymentNo', width:200,title: '付汇单号'}
                ]]
            },listQueryDone:function () {
                formatTableData();
            }
        });
    });
});
function showDetail(id) {
    openwin(rurl('order/imp/detail.html?id='+id),'订单详情',{area: ['0', '0'],shadeClose:true});
}
function applyPayment(o) {
    var datas = getDatas();
    if(datas.length==0){show_msg('请至少选择一条订单信息');return;}
    var customerId=null,currencyId = null,ids=[];
    for(var i=0;i<datas.length;i++){
        var item = datas[i];
        if(customerId!=null&&customerId!=item.customerId){
            prompt_warn('订单【'+item.docNo+'】所选客户不一致，无法合并付汇');return;
        }
        if(currencyId!=null&&currencyId!=item.currencyId){
            prompt_warn('订单【'+item.docNo+'】所选币制不一致，无法合并付汇');return;
        }
        if(item.totalPrice<=item.applyPayVal){
            prompt_warn('订单【'+item.docNo+'】可申请金额不足');return;
        }
        customerId = item.customerId;
        currencyId = item.currencyId;
        ids.push(item.id);
    }
    openwin(o.url+'?id='+ids.join(','),o.name,{area: ['0', '0']});
}