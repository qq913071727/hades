layui.use(['layer','form','upload'], function() {
    var layer = layui.layer, form = layui.form,upload = layui.upload;
    var id = getUrlParam("id");
    show_loading();
    var fileObj = {docId:'temp_'+uuid(), docType:'imp_order'};
    $('.file-size').attr({'doc-id':fileObj.docId});
    $('.sel-load').selLoad(function () {
        form.render('select');
        $.post(rurl('scm/impOrder/detail'),{'id':id},function (res) {
            if(isSuccessWarnClose(res)){
                var data = res.data,order=data.order,logistiscs=data.logistiscs,members=data.members||[];
                order['id']='';
                order['docNo']='';
                order['clientNo']='';
                form.val('orderMainForm',order);
                //生成单号
                generateDocNo();
                $('#memo').val(order.memo);
                //根据客户获取有效的协议报价
                oldCustomerId = order.customerId;supplierId=order.supplierId;
                obtainImpQuote(order.customerId,order.businessType,order.impQuoteId,order.transactionMode);
                //订单明细
                $.each(members,function (i,o) {o['id']='';});
                createRows(members);
                // createRows();
                //物流信息
                form.val('orderLogisticsForm',logistiscs);
                //交货方式显示
                showOrHide($('#deliveryMode'));
                //供应商提货地址信息
                takeInformation(order.supplierName,logistiscs.supplierTakeInfoId);
                //获取国内公司物流信息
                domesticLogistics(logistiscs.logisticsCompanyId,logistiscs.prescription);
                //客户收货地址信息
                takeDeliveryInformation(order.customerName,logistiscs.customerDeliveryInfoId);
                //送货方式显示
                showOrHide($('#receivingMode'));
                //自提仓库
                takeSelfWare(logistiscs.selfWareId);
                //币制填充
                autoCurrency();
                //附件资料
                initFileSize();
            }
        });
    });
    //监听业务类型选择
    form.on('select(businessType)',function (obj){fullImpQuote();});
    //监听交货方式
    form.on('select(deliveryMode)', function(da){showOrHide($(da.elem));});
    //监听送货方式
    form.on('select(receivingMode)', function(da){showOrHide($(da.elem));});
    //物流公司选择
    form.on('select(logisticsCompany)', function(da){logisticsTimeliness();});

    $('.save-btn').click(function (){saveImpOrder(false,fileObj);});
    $('.examine-btn').click(function () {saveImpOrder(true,fileObj);});

    upload.render({
        elem: '#importExcel',url:rurl('scm/impOrder/importMember'),size:10240,exts:'xls|xlsx'
        ,acceptMime:'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ,before:function (res) {
            show_loading();
        },done: function(res){
            if(isSuccessWarn(res)){
                var memberData = res.data;
                show_msg("成功解析："+memberData.length+" 条数据");
                $('input[name="model"]').each(function (i,o) {
                    var $model = $(o);if(isEmpty($model.val())){$model.parent().parent().remove();}
                });
                createRows(memberData);
                calculationAll();
            }
        },error:function (index, upload) {
            hide_loading();
        }
    });
});
function saveImpOrder(submitAudit,fileObj) {
    if(!checkedForm($('#orderMainForm'))){return;}
    var order = $('#orderMainForm').serializeJson(),logistiscs =  $('#orderLogisticsForm').serializeJson(),members = obtainMenberJSON();
    order['memo'] = $('#memo').val();//备注
    postJSON('scm/impOrder/add',{"order":order,"logistiscs":logistiscs,"members":members,"submitAudit":submitAudit,"file":fileObj},function (res) {
        parent.reloadList();
        closeThisWin('保存成功');
    });
}
