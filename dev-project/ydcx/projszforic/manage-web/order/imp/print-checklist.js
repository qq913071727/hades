layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer,laytpl=layui.laytpl,form=layui.form
        ,id = getUrlParam("id")
        ,keys = ['model','name','brand','unitName','quantity','unitPrice','totalPrice','cartonNum','cartonNo','netWeight','grossWeight','countryName','goodsCode'];
    show_loading();
    $.post(rurl('scm/impOrder/detail'),{'id':id,'operation':''},function (res) {
        if(isSuccessWarn(res)){
            var data = res.data;
            var total = {tquantity:0.0, ttotalPrice:0.0, tcartonNum:0, tnetWeight:0.0, tgrossWeight:0.0};
            $.each(data.members || [],function (i,o) {
                total.tquantity += o.quantity;
                total.tcartonNum += o.cartonNum;
                total.ttotalPrice += o.totalPrice;
                total.tnetWeight += o.netWeight;
                total.tgrossWeight += o.grossWeight;
            });
            total.tquantity = Math.round(total.tquantity*100)/100;
            total.ttotalPrice = Math.round(total.ttotalPrice*100)/100;
            total.tnetWeight = Math.round(total.tnetWeight*100)/100;
            total.tgrossWeight = Math.round(total.tgrossWeight*100)/100;
            data['total'] = total;
            laytpl($('#conTemp').html()).render(data,function (html) {
                $('#conTempView').html(html);
            });

            $.post(rurl('scm/impOrderMemberHistory/list'),{'orderId':id},function (hres) {
                if(isSuccess(hres)){
                    var htotal = {tquantity:0.0, ttotalPrice:0.0, tcartonNum:0, tnetWeight:0.0, tgrossWeight:0.0};
                    $.each(hres.data || [],function (i,o) {
                        htotal.tquantity += o.quantity;
                        htotal.tcartonNum += o.cartonNum;
                        htotal.ttotalPrice += o.totalPrice;
                        htotal.tnetWeight += o.netWeight;
                        htotal.tgrossWeight += o.grossWeight;
                        var midObj = $('tr[mid="'+o.id+'"]');
                        if(midObj.size()>0){
                            $.each(keys,function (j,k) {
                                var keyObj = midObj.find('['+k+']');
                                if(keyObj.size()==1){
                                    var oval = removeNull(o[k]+''),nval = keyObj.html();
                                    if('quantity'==k||'totalPrice'==k){
                                        oval = formatCurrency(o[k]);
                                    }
                                    if(oval!=nval){
                                        keyObj.html('<div class="original-info">'+oval+'</div><div class="new-info">'+nval+'</div>');
                                    }
                                }
                            });
                        }
                    });
                    htotal.tquantity = Math.round(htotal.tquantity*100)/100;
                    htotal.ttotalPrice = Math.round(htotal.ttotalPrice*100)/100;
                    htotal.tnetWeight = Math.round(htotal.tnetWeight*100)/100;
                    htotal.tgrossWeight = Math.round(htotal.tgrossWeight*100)/100;
                    if(total.tquantity!=htotal.tquantity){
                        $('div[tquantity]').html('<div class="original-info">'+formatCurrency(htotal.tquantity)+'</div><div class="new-info">'+formatCurrency(total.tquantity)+'</div>');
                    }
                    if(total.ttotalPrice!=htotal.ttotalPrice){
                        $('div[ttotalPrice]').html('<div class="original-info">'+formatCurrency(htotal.ttotalPrice)+'</div><div class="new-info">'+formatCurrency(total.ttotalPrice)+'</div>');
                    }
                    if(total.tcartonNum!=htotal.tcartonNum){
                        $('div[tcartonNum]').html('<div class="original-info">'+htotal.tcartonNum+'</div><div class="new-info">'+total.tcartonNum+'</div>');
                    }
                    if(total.tnetWeight!=htotal.tnetWeight){
                        $('div[tnetWeight]').html('<div class="original-info">'+formatCurrency(htotal.tnetWeight)+'</div><div class="new-info">'+formatCurrency(total.tnetWeight)+'</div>');
                    }
                    if(total.tgrossWeight!=htotal.tgrossWeight){
                        $('div[tgrossWeight]').html('<div class="original-info">'+formatCurrency(htotal.tgrossWeight)+'</div><div class="new-info">'+formatCurrency(total.tgrossWeight)+'</div>');
                    }
                }
            })
        }
    });
});
