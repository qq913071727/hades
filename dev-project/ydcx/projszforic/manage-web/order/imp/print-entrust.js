layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer, laytpl = layui.laytpl, form = layui.form;
    var id = getUrlParam('id');
    show_loading();
    $.post(rurl('scm/impOrder/getPrintEntrustData'),{'id':id},function (res) {
        if(isSuccessWarnClose(res)){
            var data = res.data;
            var total = {'tquantity':0.0,'ttotalPrice':0.0,'tnetWeight':0.0,'tgrossWeight':0.0,'tcartonNum':0};
            $.each(data.members || [],function (i,o) {
                total.tquantity = Math.round((total.tquantity + o.quantity)*100)/100;
                total.ttotalPrice = Math.round((total.ttotalPrice + o.totalPrice)*100)/100;
                total.tnetWeight = Math.round((total.tnetWeight + o.netWeight)*10000)/10000;
                total.tgrossWeight = Math.round((total.tgrossWeight + o.grossWeight)*10000)/10000;

                total.tcartonNum = total.tcartonNum + (null == o.cartonNum ? 0 : o.cartonNum);
            });
            data['total'] = total;
            laytpl($('#conTemp').html()).render(data,function (html) {
                $('#conTempView').html(html);
            });
        }
    });

    $("#exportExcel").click(function () {
        show_loading();$.post(rurl('scm/impOrder/exportEntrustExcel'),{'id':id},function (res) {if(isSuccess(res)){exportFile(res.data);}});
    });
});