layui.config({base: '/order/imp/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','laytpl'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,laytpl=layui.laytpl;
    var id = getUrlParam("id");
    $('#orderDetail').load(rurl('order/imp/detail-temp.html'),function () {
        detailTemp.render(id,'order_confirm_imp',true,true);
        var $operationTaskBtn = $('.operation-task-btn'),taskArg = {'operation':'order_confirm_imp','documentId':id,'documentClass':'ImpOrder','newStatusCode':''};
        $operationTaskBtn.click(function (){
            var $that = $(this),needMemo = $that.attr('need-memo');
            if(!isEmpty(needMemo)){
                var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
                taskArg['memo'] = $memo.val();
            }else{
                taskArg['memo'] = $('#taskMemo').val();
            }
            taskArg['newStatusCode'] = $that.attr('status-code');
            show_loading();
            $.post(rurl('scm/impOrder/confirmImp'),taskArg,function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
    });
    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
    $.post(rurl('scm/impOrderCost/orderCostInfo'),{'id':id},function (res) {
       if(isSuccessWarnClose(res)){
           //订单费用信息
           var  data = res.data;
           laytpl($('#costTemp').html()).render(data,function (html) {
               $('#costTempView').html(html);
           });
           if(data.isAdopt==true||(data.isAdopt==false&&data.fkAdopt==true)){
                $('#confirmImp').attr({'disabled':null}).removeClass('layui-btn-primary').removeClass('layui-disabled').addClass('layui-btn-normal');
           }
       }
    });
});