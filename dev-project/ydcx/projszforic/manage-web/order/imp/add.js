layui.use(['layer','form','upload'], function() {
    var layer = layui.layer, form = layui.form,upload = layui.upload;
    var fileObj = {docId:'temp_'+uuid(), docType:'imp_order'};
    $('.file-size').attr({'doc-id':fileObj.docId});
    $('.sel-load').selLoad(function () {
        form.render('select');
        //生成单号
        generateDocNo();
        //币制填充
        autoCurrency();
        //创建一行空的明细
        createRow();
        //获取国内公司物流信息
        domesticLogistics();
        //自提仓库
        takeSelfWare();
        //初始化附件
        initFileSize();
    });
    //监听业务类型选择
    form.on('select(businessType)',function (obj){fullImpQuote();});
    //监听交货方式
    form.on('select(deliveryMode)', function(da){showOrHide($(da.elem));});
    //监听送货方式
    form.on('select(receivingMode)', function(da){showOrHide($(da.elem));});
    //物流公司选择
    form.on('select(logisticsCompany)', function(da){logisticsTimeliness();});

    $('.save-btn').click(function () {saveImpOrder(false,fileObj);});
    $('.examine-btn').click(function () {saveImpOrder(true,fileObj);});

    upload.render({
        elem: '#importExcel',url:rurl('scm/impOrder/importMember'),size:10240,exts:'xls|xlsx'
        ,acceptMime:'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ,before:function (res) {
            show_loading();
        },done: function(res){
            if(isSuccessWarn(res)){
               var memberData = res.data;
                show_msg("成功解析："+memberData.length+" 条数据");
                $('input[name="model"]').each(function (i,o) {
                    var $model = $(o);if(isEmpty($model.val())){$model.parent().parent().remove();}
                });
                createRows(memberData);
                calculationAll();
            }
        },error:function (index, upload) {
            hide_loading();
        }
    });

});
function saveImpOrder(submitAudit,fileObj) {
    if(!checkedForm($('#orderMainForm'))){return;}
    var order = $('#orderMainForm').serializeJson(),logistiscs =  $('#orderLogisticsForm').serializeJson(),members = obtainMenberJSON();
    order['memo'] = $('#memo').val();//备注
    postJSON('scm/impOrder/add',{"order":order,"logistiscs":logistiscs,"members":members,"submitAudit":submitAudit,"file":fileObj},function (res) {
        parent.reloadList();
        closeThisWin('保存成功');
    });
}
