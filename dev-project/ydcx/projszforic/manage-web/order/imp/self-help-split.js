layui.use(['layer','form','element','table'], function() {
    var layer = layui.layer, form = layui.form,element=layui.element,table=layui.table,orderId = getUrlParam('id'),quantity = getUrlParam('quantity'),orderMap=[],fn={
        init:function () {
            show_loading();
            $.post(rurl('scm/impOrder/initSelfHelpSplit'),{orderId:orderId,quantity:quantity},function (res) {
                if(isSuccessWarnClose(res)){
                    let data = res.data;
                    $.each(data,function (i,o) {
                        element.tabAdd('order-tab', {
                            title: '订单【'+(i+1)+'】'
                            ,content: '<div class="layui-row pb10" id="area-'+i+'"><div class="layui-col-md2">总数量：<h2 style="display: inline-block" class="quantityTotal">0.0</h2></div><div class="layui-col-md2">总价：<h2 style="display: inline-block" class="totalPrice">0.0</h2></div><div class="layui-col-md2">总箱数：<h2 style="display: inline-block" class="cartonTotal">0</h2></div><div class="layui-col-md2">总净重：<h2 style="display: inline-block" class="netWeightTotal">0.0000</h2></div><div class="layui-col-md2">总毛重：<h2 style="display: inline-block" class="grossWeightTotal">0.0000</h2></div></div>'+
                            '<form class="layui-form order-memner-form"><table class="layui-hide" id="table-'+i+'" lay-filter="table-'+i+'"></table></form>'
                            ,id: 'tab-'+i
                        });
                        orderMap[i] = o.members;
                        fn.fullTableList(i);
                    });
                    element.tabChange('order-tab','tab-0');
                }
            });

            $('.save-btn').click(function () {
                let $form = $('.order-memner-form'),members=[];
                $.each($form,function (i,o) {
                    members.push($(o).serializeJson())
                });
                postJSON('scm/impOrder/saveSelfHelpSplit',{"orderId":orderId,"members":members},function (res) {
                    closeThisWin('拆单成功');
                });
            });
        },fullTableList(key){
            let data = orderMap[key];
            table.render({
                elem: '#table-'+key,limit:5000,page:false
                ,cols: [[
                    {type:'numbers',title:'序号',width:45}
                    ,{field:'superviseInfo',title:'监管说明',width:240,templet:'#superviseInfoTemp'}
                    ,{field:'model',title:'型号',width:160}
                    ,{field:'name',title:'名称',width:180,}
                    ,{field:'brand',title:'品牌',width:180}
                    ,{field:'quantity',title:'数量',width:90,align:'money', totalRow: true}
                    ,{field:'unitName',title:'单位',width:80,align:'center'}
                    ,{field:'unitPrice',title:'单价',width:90,align:'right'}
                    ,{field:'totalPrice',title:'总价',width:90,align:'money', totalRow: true}
                    ,{field:'cartonNum',title:'箱数',width:90,align:'right', totalRow: true}
                    ,{field:'cartonNo',title:'箱号',width:120}
                    ,{field:'netWeight',title:'净重',width:90,align:'money', totalRow: true}
                    ,{field:'grossWeight',title:'毛重',width:90,align:'money', totalRow: true}
                    ,{field:'countryName',title:'产地',width:80}
                ]]
                , data: data
            });
            formatTableData();
            var tquantity = 0.0,ttotalPrice = 0.0,tcartonNum = 0,tnetWeight = 0.0,tgrossWeight = 0.0;
            $.each(data,function (i,o) {
                tquantity += o.quantity;
                ttotalPrice += o.totalPrice;
                tcartonNum += o.cartonNum;
                tnetWeight += o.netWeight;
                tgrossWeight += o.grossWeight;
            });
            let $area = $('#area-'+key);
            $area.find('.quantityTotal').html(Math.round(tquantity*100)/100);
            $area.find('.totalPrice').html(formatCurrency(Math.round(ttotalPrice*100)/100));
            $area.find('.cartonTotal').html(tcartonNum);
            $area.find('.netWeightTotal').html(Math.round(tnetWeight*100)/100);
            $area.find('.grossWeightTotal').html(Math.round(tgrossWeight*100)/100);
        }
    };
    fn.init();
});
