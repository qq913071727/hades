layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
        operation.init({
            listQuery:true,
            listCols:true,
            listQueryArg:{
                url:rurl('scm/impOrder/list')
                ,cols: [[
                    {checkbox: true, fixed: true}
                    ,{field:'task', width:50, title: '任务',align:'center',toolbar:'#taskTemp', fixed: true}
                    ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                    ,{field:'docNo', width:130, title: '订单编号',templet:'#docNoTemp', fixed: true}
                    ,{field:'clientNo', width:120, title: '客户单号'}
                    ,{field:'orderDate', width:80, title: '订单日期'}
                    ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                    ,{field:'busPersonName', width:70, title: '跟进商务'}
                    ,{field:'salePersonName',width:70, title: '销售人员'}
                    ,{field:'businessTypeDesc', width:70, title: '业务类型'}
                    ,{field:'declareTypeName', width:70, title: '报关类型'}
                    ,{field:'transactionModeDesc', width:70, title: '成交方式'}
                    ,{field:'quoteTypeDesc', width:70, title: '报价类型'}
                    ,{field:'totalPrice', width:90, title: '订单金额',align:'money'}
                    ,{field:'currencyName', width:60, title: '币制',align:'center'}
                    // ,{field:'paymentNo', width:110, title: '付汇单号'}
                    // ,{field:'payVal', width:100, title: '已申请金额',align:'money'}
                    // ,{field:'salesContractNo', width:110, title: '销售合同号'}
                    // ,{field:'invoiceNo', width:110, title: '开票申请号'}
                    ,{field:'impEntrust', width:100, title: '附件管理',templet:'#impOrderAllTemp',align:'center'}
                ]]
            },listQueryDone:function (da) {
                initFileSize(); formatTableData();
                $('#total-area').remove();
                if(da.count > 0){
                    var $ares = $('.choice-btn');
                    $.post('/scm/impOrder/summaryOrderAmount',$('.query-form').serialize(),function (res) {
                        if(isSuccess(res)){
                            var data = res.data;
                            $ares.before('<div id="total-area">总订单金额：<span>'+formatCurrency(data)+'</span></div>');
                        }
                    });
                }
            }
        });
    });
});
function showDetail(id) {
    openwin(rurl('order/imp/detail.html?id='+id),'订单详情',{area: ['0', '0'],shadeClose:true});
}

function invoiceApply(o) {
    $.post(rurl('scm/impOrder/invoiceApply'),{"id":getIds()[0]},function (res) {
        if(isSuccess(res)){
            reloadList();
            closeThisWin('操作成功');
        }
    });
}
function openAuxiliaryInspection(id) {
    openwin('order/imp/auxiliary-inspection.html?id='+id,'辅助验货',{
        area:['0','0'],
        end:function () {
            $('button[operation-code="order_inspection"]').click();
        }
    });
}
function openSelfHelpSplit(id,quantity) {
    openwin('order/imp/self-help-split.html?id='+id+'&quantity='+quantity,'自助拆单',{
        area:['0','0'],
        end:function () {
            $('button[operation-code="order_inspection"]').click();
        }
    });
}
function orderCost(o){
    var datas = getCheckDatas();
    if(datas.length!=1){
        show_msg('请选择您要操作的记录');return;
    }
    openwin(o.url+'?orderNo='+datas[0].docNo,o.name, {area: ['1200px', '600px']});
}