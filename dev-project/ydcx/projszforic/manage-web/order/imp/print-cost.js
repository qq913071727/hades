layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer, laytpl = layui.laytpl, form = layui.form;
    var id = getUrlParam('id');
    show_loading();
    $.post(rurl('scm/impOrderCost/adviceReceipt'),{'id':id},function (res) {
        if(isSuccessWarnClose(res)){
            var data = res.data;
            var total = {'quantity':0.0,'totalPrice':0.0,'tariffRateVal':0.0,'addedTaxTateVal':0.0,'receAmount':0.0,'totalGrossWeight':0.0};
            $.each(data.members || [],function (i,o) {
                total.quantity = Math.round((total.quantity + o.quantity)*100)/100;
                total.totalPrice = Math.round((total.totalPrice + o.totalPrice)*100)/100;
                total.tariffRateVal = Math.round((total.tariffRateVal + o.tariffRateVal)*100)/100;
                total.addedTaxTateVal = Math.round((total.addedTaxTateVal + o.addedTaxTateVal)*100)/100;

                total.totalGrossWeight = Math.round((total.totalGrossWeight + o.grossWeight)*100)/100;
            });
            $.each(data.costs || [],function (i,o) {
                total.receAmount = Math.round((total.receAmount + o.receAmount)*100)/100;
            });
            data['total'] = total;
            laytpl($('#conTemp').html()).render(data,function (html) {
                $('#conTempView').html(html);
            });
        }
    });



    $("#exportExcel").click(function () {
        show_loading();$.get(rurl('scm/impOrderCost/exportAdviceReceiptExcel'),{'id':id},function (res) {if(isSuccess(res)){exportFile(res.data);}});
    });
});