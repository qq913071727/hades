layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table,fileupload = layui.fileupload;
    var id = getUrlParam("id");
    show_loading();
    $.get(rurl("/scm/supplier/info"),{"id":id},function (res) {
        if(isSuccess(res)) {
            var data = res.data;
            form.val("baseForm",data.supplier);

            var takes = data.takes || [];
            var banks = data.banks || [];
            $.each(takes,function (i,o) {
                if(!isEmpty(o.provinceName) && !isEmpty(o.cityName) && !isEmpty(o.areaName)) {
                    o.area = [o.provinceName,o.cityName,o.areaName].join(' / ');
                }
            });
            takeInfoList(takes);
            bankList(banks);

            var fileObj = {docId:data.supplier.id, docType:'supplier'};
            fileupload.render({elem:'#sup_business',uploadArg:{data:$.extend({},fileObj,{businessType:'sup_business'})}});
            fileupload.render({elem:'#sup_address',uploadArg:{data:$.extend({},fileObj,{businessType:'sup_address'})}});
            fileupload.render({elem:'#sup_other',uploadArg:{data:$.extend({},fileObj,{businessType:'sup_other'})}});

            $('.save-btn').click(function () {
                if(checkedForm($('.tsf-page-content'))){
                    show_loading();
                    var supplier = $('#baseForm').serializeJson(),takes = obtainTakeInfoData(),banks = obtainBankData();
                    postJSON('scm/supplier/edit',{"supplier":supplier,"takes":takes,"banks":banks},function () {
                        parent.reloadList();
                        closeThisWin('修改成功');
                    });
                }
            });
        }
    });
    table.on('tool(take-info-list)', function(obj){
        var data = obj.data;
        if(obj.event === 'deDel'){
            layer.confirm('您确定要删除此提货地址？', function(index){
                obj.del();
                takeInfoList(obtainTakeInfoData());
                layer.close(index);
            });
        }
    });
    table.on('tool(bank-list)', function(obj){
        var data = obj.data;
        if(obj.event === 'deDel'){
            layer.confirm('您确定要删除此银行信息？', function(index){
                obj.del();
                bankList(obtainBankData());
                layer.close(index);
            });
        }
    });
});

function bankList(data){
    layui.table.render({
        elem: '#bank-list',limit:1000,id:'bank-list',text:{none:'<a href="javascript:" class="def-a" onclick="addBank()"><i class="fa fa-plus-circle"></i>添加一行</a>'}
        ,cols: [[
            {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
            ,{field: 'name', title: '银行名称', width: 240,align:'form',templet:'#bankNameTemp'}
            ,{field: 'account', title: '账号', width: 160,align:'form',templet:'#bankAccountTemp'}
            ,{field: 'currencyId', title: '币制', width: 100,align:'form',templet:'#currencyIdTemp'}
            ,{field: 'swiftCode', title: 'SWIFT CODE', width: 160,align:'form',templet:'#bankSwiftCodeTemp'}
            ,{field: 'code', title: '银行代码', width: 100,align:'form',templet:'#bankCodeTemp'}
            ,{field: 'address', title: '详细地址',align:'form',templet:'#bankAddressTemp'}
            ,{field: 'disabled', title: '状态', width: 80,align:'form-switch',templet:'#disabledTemp'}
            ,{field: 'isDefault', title: '默认', width: 65,align:'center',templet:'#isDefaultTemp'}
            ,{field: 'oper', title: '操作', width: 65,toolbar: '#deOperaBar',fixed: 'right',align:'center'}
        ]]
        ,data: data
        ,done:function () {
            autoCurrency();
        }
    });
}

function takeInfoList(data) {
    layui.table.render({
        elem: '#take-info-list',limit:1000,id:'take-info-list',text:{none:'<a href="javascript:" class="def-a" onclick="addTakeInfo()"><i class="fa fa-plus-circle"></i>添加一行</a>'}
        ,cols: [[
            {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
            ,{field: 'companyName', title: '提货公司', width: 240,align:'form',templet:'#companyNameTemp'}
            ,{field: 'linkPerson', title: '联系人', width: 120,align:'form',templet:'#linkPersonTemp'}
            ,{field: 'linkTel', title: '联系电话', width: 140,align:'form',templet:'#linkTelTemp'}
            ,{field: 'area', title: '提货区域', width: 200,align:'form',templet:'#areaTemp'}
            ,{field: 'address', title: '详细地址',align:'form',templet:'#addressTemp'}
            ,{field: 'disabled', title: '状态', width: 80,align:'form-switch',templet:'#disabledTemp'}
            ,{field: 'isDefault', title: '默认', width: 65,align:'center',templet:'#isDefaultTemp'}
            ,{field: 'oper', title: '操作', width: 65,toolbar: '#deOperaBar',fixed: 'right',align:'center'}
        ]]
        ,data: data
        ,done:function () {
            initCity('.area');
        }
    });
}

function obtainBankData() {
    var data = []
        ,$bankForm = $('#bankForm')
        ,$ids = $bankForm.find('input[name="id"]')
        ,$names = $bankForm.find('input[name="name"]')
        ,$codes = $bankForm.find('input[name="code"]')
        ,$accounts = $bankForm.find('input[name="account"]')
        ,$currencyNames = $bankForm.find('input[name="currencyName"]')
        ,$swiftCodes = $bankForm.find('input[name="swiftCode"]')
        ,$addresss = $bankForm.find('input[name="address"]')
        ,$disableds = $bankForm.find('input[name="disabled"]')
        ,$isDefaults = $bankForm.find('input[name="isDefault"]');
    for(var i=0;i<$ids.size();i++){
        let disabled = ($($disableds[i]).parent().find('.layui-form-onswitch').size()>0)?false:true;
        var item = {
            id:$($ids[i]).val(),
            name:$($names[i]).val(),
            account:$($accounts[i]).val(),
            currencyName:$($currencyNames[i]).val(),
            swiftCode:$($swiftCodes[i]).val(),
            code:$($codes[i]).val(),
            address:$($addresss[i]).val(),
            disabled:disabled,
            isDefault:($($isDefaults[i]).parent().find('.layui-form-radioed').size()>0) && disabled != true ?true:false
        }
        data.push(item);
    }
    return data;
}
function obtainTakeInfoData() {
    var data = []
        ,$takeInfoForm = $('#takeInfoForm')
        ,$ids = $takeInfoForm.find('input[name="id"]')
        ,$companyNames = $takeInfoForm.find('input[name="companyName"]')
        ,$linkPersons = $takeInfoForm.find('input[name="linkPerson"]')
        ,$linkTels = $takeInfoForm.find('input[name="linkTel"]')
        ,$areas = $takeInfoForm.find('input[name="area"]')
        ,$addresss = $takeInfoForm.find('input[name="address"]')
        ,$disableds = $takeInfoForm.find('input[name="disabled"]')
        ,$isDefaults = $takeInfoForm.find('input[name="isDefault"]');
    for(var i=0;i<$ids.size();i++){
        let disabled = ($($disableds[i]).parent().find('.layui-form-onswitch').size()>0)?false:true;
        var item = {
            id:$($ids[i]).val(),
            companyName:$($companyNames[i]).val(),
            linkPerson:$($linkPersons[i]).val(),
            linkTel:$($linkTels[i]).val(),
            area:$($areas[i]).val(),
            provinceName:'',
            cityName:'',
            areaName:'',
            address:$($addresss[i]).val(),
            disabled:disabled,
            isDefault:($($isDefaults[i]).parent().find('.layui-form-radioed').size()>0) && disabled != true ?true:false
        }
        if(!isEmpty(item.area)){
            var areas = item.area.split(" / ");
            item.provinceName = areas[0];
            item.cityName = areas[1];
            item.areaName = areas[2];
        }
        data.push(item);
    }
    return data;
}
function addBank() {
    var data = obtainBankData();
    data.push({id:'', name:'', account:'', swiftCode:'',currencyName:'美元',code:'', address:'', disabled:false, isDefault:(data.length==0)?true:false});
    bankList(data);
}
function addTakeInfo() {
    var data = obtainTakeInfoData();
    data.push({id:'', companyName:'', linkPerson:'', linkTel:'', area:'', address:'', disabled:false, isDefault:(data.length==0)?true:false});
    takeInfoList(data);
}
