layui.config({base: '/js/lib/'}).extend({fileupload: 'fileupload'}).use(['layer','form','table','fileupload'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table,fileupload = layui.fileupload;
    var id = getUrlParam("id");
    show_loading();
    $.get(rurl("/scm/supplier/info"),{"id":id},function (res) {
        if(isSuccess(res)) {
            var data = res.data;
            form.val("baseForm",data.supplier);

            var takes = data.takes || [];
            var banks = data.banks || [];
            $.each(takes,function (i,o) {
                if(!isEmpty(o.provinceName) && !isEmpty(o.cityName) && !isEmpty(o.areaName)) {
                    o.area = [o.provinceName,o.cityName,o.areaName].join(' / ');
                }
            });
            takeInfoList(takes);
            bankList(banks);

            var fileObj = {docId:data.supplier.id, docType:'supplier'};
            fileupload.render({elem:'#sup_business',edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'sup_business'})}});
            fileupload.render({elem:'#sup_address',edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'sup_address'})}});
            fileupload.render({elem:'#sup_other',edit:false,uploadArg:{data:$.extend({},fileObj,{businessType:'sup_other'})}});
            disableForm(form);
        }
    });
});

function bankList(data){
    layui.table.render({
        elem: '#bank-list',limit:1000,id:'bank-list'
        ,cols: [[
            {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
            ,{field: 'name', title: '银行名称', width: 240}
            ,{field: 'account', title: '账号', width: 160}
            ,{field: 'currencyId', title: '币制', width: 100}
            ,{field: 'swiftCode', title: 'SWIFT CODE', width: 160}
            ,{field: 'code', title: '银行代码', width: 100}
            ,{field: 'address', title: '详细地址'}
            ,{field: 'disabled', title: '状态', width: 80,align:'center',templet:'#disabledTemp'}
            ,{field: 'isDefault', title: '默认', width: 65,align:'center',templet:'#isDefaultTemp'}
        ]]
        ,data: data
    });
}

function takeInfoList(data) {
    layui.table.render({
        elem: '#take-info-list',limit:1000,id:'take-info-list'
        ,cols: [[
            {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
            ,{field: 'companyName', title: '提货公司', width: 240}
            ,{field: 'linkPerson', title: '联系人', width: 120}
            ,{field: 'linkTel', title: '联系电话', width: 140}
            ,{field: 'area', title: '提货区域', width: 200}
            ,{field: 'address', title: '详细地址'}
            ,{field: 'disabled', title: '状态', width: 80,align:'center',templet:'#disabledTemp'}
            ,{field: 'isDefault', title: '默认', width: 65,align:'center',templet:'#isDefaultTemp'}
        ]]
        ,data: data
    });
}
