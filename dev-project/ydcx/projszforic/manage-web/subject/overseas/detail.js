layui.use(['layer','form','table'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table;
    var id = getUrlParam('id');
    show_loading();
    $.get(rurl('scm/subjectOverseas/info'),{'id':id},function (res) {
        if(isSuccess(res)){
            var data = res.data;
            form.val('baseForm',data.overseas);
            bankList((data.banks || []));
            disableForm(form);
            if(!isEmpty(data.overseas.chapter)){$('#chapterImg').attr({'src':rurl(data.overseas.chapter)});$('#chapterArea').show();}
        }
    });
});
function bankList(data){
    layui.table.render({
        elem: '#bank-list',limit:1000,id:'bank-list'
        ,cols: [[
            {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
            ,{field: 'name', title: '银行名称', width: 240}
            ,{field: 'account', title: '账号', width: 160}
            ,{field: 'currencyId', title: '币制', width: 100}
            ,{field: 'swiftCode', title: 'SWIFT CODE', width: 160}
            ,{field: 'code', title: '银行代码', width: 100}
            ,{field: 'address', title: '详细地址'}
            ,{field: 'disabled', title: '状态', width: 80,align:'center',templet:'#disabledTemp'}
            ,{field: 'isDefault', title: '默认', width: 65,align:'center',templet:'#isDefaultTemp'}
        ]]
        ,data: data
        ,done:function () {
            autoCurrency();
        }
    });
}
