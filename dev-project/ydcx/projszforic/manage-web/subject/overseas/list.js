layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listQueryArg:{
            url:rurl('scm/subjectOverseas/list'),page: false,limit:5000
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'name', width:300, title: '英文名称',templet:'#nameTemp'}
                ,{field:'nameCn', width:240, title: '中文名称'}
                ,{field:'dateCreated', width:130, title: '登记日期'}
                ,{field:'tel', width:100, title: '电话'}
                ,{field:'fax', width:100, title: '传真'}
                ,{field:'disabled', width:100,align:'form-switch',title: '状态',templet:'#disabledTemp'}
                ,{field: 'isDefault', title: '默认', width: 65,align:'center',templet:'#isDefaultTemp'}
                ,{field:'address', title: '地址'}
            ]]
        }
    });
    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/subjectOverseas/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });
});
//境外公司详情
function showDetail(id) {
    openwin(rurl('subject/overseas/detail.html?id='+id),'境外公司详情',{area: ['0', '0'],shadeClose:true});
}

