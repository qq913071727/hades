layui.use(['layer','form','table','upload'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table,upload = layui.upload;
    bankList([]);
    $('.save-btn').click(function () {
        if(checkedForm($('.tsf-page-content'))){
            show_loading();
            var overseas = $('#baseForm').serializeJson(),banks = obtainBankData();
            postJSON('scm/subjectOverseas/add',{"overseas":overseas,"banks":banks},function (res) {
                parent.reloadList();
                closeThisWin('添加成功');
            });
        }
    });
    table.on('tool(bank-list)', function(obj){
        var data = obj.data;
        if(obj.event === 'deDel'){
            layer.confirm('您确定要删除此银行信息？', function(index){
                obj.del();
                bankList(obtainBankData());
                layer.close(index);
            });
        }
    });
    var fileId = 'temp_'+uuid();
    $('#fileId').val(fileId);
    //合同章上传
    upload.render({
        elem: '#uploadChapterBtn',accept:'images',acceptMime:'image/*',exts:'png',size:2048,
        url:rurl('scm/file/upload'),data:{docId:fileId,docType:'subject_overseas',businessType:'sub_ove_chapter'},
        before:function () {show_loading();},done:function (ures) {
            if(isSuccess(ures)){
                $('#chapter').val(rurl('/scm/download/img/'+ures.data.id));
                $('#chapterImg').attr({'src':rurl('/scm/download/img/'+ures.data.id)});
                $('#chapterArea').show();
            }
        },error:function () {hide_loading();}
    });

});
function bankList(data){
    layui.table.render({
        elem: '#bank-list',limit:1000,id:'bank-list',text:{none:'<a href="javascript:" class="def-a" onclick="addBank()"><i class="fa fa-plus-circle"></i>添加一行</a>'}
        ,cols: [[
            {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
            ,{field: 'name', title: '银行名称', width: 240,align:'form',templet:'#bankNameTemp'}
            ,{field: 'account', title: '账号', width: 160,align:'form',templet:'#bankAccountTemp'}
            ,{field: 'currencyId', title: '币制', width: 100,align:'form',templet:'#currencyIdTemp'}
            ,{field: 'swiftCode', title: 'SWIFT CODE', width: 160,align:'form',templet:'#bankSwiftCodeTemp'}
            ,{field: 'code', title: '银行代码', width: 100,align:'form',templet:'#bankCodeTemp'}
            ,{field: 'address', title: '详细地址',align:'form',templet:'#bankAddressTemp'}
            ,{field: 'disabled', title: '状态', width: 80,align:'form-switch',templet:'#disabledTemp'}
            ,{field: 'isDefault', title: '默认', width: 65,align:'center',templet:'#isDefaultTemp'}
            ,{field: 'oper', title: '操作', width: 65,toolbar: '#deOperaBar',fixed: 'right',align:'center'}
        ]]
        ,data: data
        ,done:function () {
            autoCurrency();
        }
    });
}

function obtainBankData() {
    var data = []
        ,$bankForm = $('#bankForm')
        ,$ids = $bankForm.find('input[name="id"]')
        ,$names = $bankForm.find('input[name="name"]')
        ,$codes = $bankForm.find('input[name="code"]')
        ,$accounts = $bankForm.find('input[name="account"]')
        ,$currencyNames = $bankForm.find('input[name="currencyName"]')
        ,$swiftCodes = $bankForm.find('input[name="swiftCode"]')
        ,$addresss = $bankForm.find('input[name="address"]')
        ,$disableds = $bankForm.find('input[name="disabled"]')
        ,$isDefaults = $bankForm.find('input[name="isDefault"]');
    for(var i=0;i<$ids.size();i++){
        var item = {
            id:$($ids[i]).val(),
            name:$($names[i]).val(),
            account:$($accounts[i]).val(),
            currencyName:$($currencyNames[i]).val(),
            swiftCode:$($swiftCodes[i]).val(),
            code:$($codes[i]).val(),
            address:$($addresss[i]).val(),
            disabled:($($disableds[i]).parent().find('.layui-form-onswitch').size()>0)?false:true,
            isDefault:($($isDefaults[i]).parent().find('.layui-form-radioed').size()>0)?true:false
        }
        data.push(item);
    }
    return data;
}

function addBank() {
    var data = obtainBankData();
    data.push({id:'', name:'', account:'', swiftCode:'',currencyName:'美元',code:'', address:'', disabled:false, isDefault:(data.length==0)?true:false});
    bankList(data);
}