layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form','upload'], function() {
    var layer = layui.layer, operation = layui.operation, form = layui.form,upload = layui.upload;
    operation.init({
        listQuery:false,
        done:function (mres) {
            var allowEdit = false;
            $.each(mres.data,function (i,o) {
                if("editSubject"==o.action){allowEdit=true;}
            });
            if(allowEdit==false){
                disableForm(form);
            }else{
                $('.edit-operate').show();
            }
            show_loading();
            $.get(rurl('scm/subject/detail'),{},function (res) {
               if(isSuccess(res)){
                   var subject = res.data;
                   if(!isEmpty(subject.loginBg)&&subject.loginBg.indexOf('?v=')!=-1){subject.loginBg = (subject.loginBg.substring(0,subject.loginBg.indexOf('?v=')));}
                   form.val('form',subject);
                   $('#logoImg').attr({'src':rurl(subject.logo)});
                   if(!isEmpty(subject.chapter)){$('#chapterImg').attr({'src':rurl(subject.chapter)});$('#chapterArea').show();}
                   obtainLoginBgFile(subject.id,allowEdit);
                   createBgItem('def_bg_0','/images/login.png','',allowEdit);
                   createBgItem('def_bg_1','/images/login01.jpg','',allowEdit);
                   createBgItem('def_bg_2','/images/login02.jpg','',allowEdit);

                   if(allowEdit){
                       $('.save-btn').click(function () {
                           if(checkedForm($('#form'))){
                               formSub('scm/subject/edit',function (sres) {
                                   //修改本地数据
                                   app.refreshSubject();
                                   closeThisWin('修改成功');
                               });
                           }
                       });
                       //logo上传
                       upload.render({
                           elem: '#uploadLogoBtn',accept:'images',acceptMime:'image/*',exts:'jpg|jpeg|gif|png|bmp|tif',size:10240,
                           url:rurl('scm/file/upload'),data:{docId:subject.id,docType:'subject',businessType:'sub_logo'},
                           before:function () {show_loading();},done:function (ures) {
                               if(isSuccess(ures)){
                                   $('#logo').val(rurl('/scm/download/img/'+ures.data.id));
                                   $('#logoImg').attr({'src':rurl('/scm/download/img/'+ures.data.id)});
                               }
                           },error:function () {hide_loading();}
                       });
                       //合同章上传
                       upload.render({
                           elem: '#uploadChapterBtn',accept:'images',acceptMime:'image/*',exts:'png',size:2048,
                           url:rurl('scm/file/upload'),data:{docId:subject.id,docType:'subject',businessType:'sub_chapter'},
                           before:function () {show_loading();},done:function (ures) {
                               if(isSuccess(ures)){
                                   $('#chapter').val(rurl('/scm/download/img/'+ures.data.id));
                                   $('#chapterImg').attr({'src':rurl('/scm/download/img/'+ures.data.id)});
                                   $('#chapterArea').show();
                               }
                           },error:function () {hide_loading();}
                       });
                       //登录背景
                       upload.render({
                           elem: '#uploadLoginBtn',accept:'images',acceptMime:'image/*',exts:'jpg|jpeg|gif|png|bmp|tif',size:10240,
                           url:rurl('scm/file/upload'),data:{docId:subject.id,docType:'subject',businessType:'sub_login_bg'},
                           before:function () {show_loading();},done:function (ures) {
                               if(isSuccess(ures)){
                                   $('#loginBg').val(rurl('/scm/download/img/'+ures.data.id));
                                   createBgItem(ures.data.id,rurl('/scm/download/img/'+ures.data.id),'own',allowEdit);
                               }
                           },error:function () {hide_loading();}
                       });

                       $('#name').enter(function (){searchCss('3',$(this).val());});
                       $('#socialNo').enter(function(){searchCss('1',$(this).val());});
                   }
               }
            });
        }
    });
});
function searchCss(queryFlag,enterCode){
    if(isEmpty(enterCode)){return;}show_loading();$.ajax({url: rurl('scm/css/search'), data: JSON.stringify({'queryFlag':queryFlag,'enterCode':trim(enterCode)}), type: 'post', dataType: 'json', contentType: 'application/json', success: function (res){
    if(isSuccessNoTip(res)&&res.data!=null){$('#name').val(res.data.name);$('#socialNo').val(res.data.id);$('#taxpayerNo').val(res.data.id);$('#customsCode').val(res.data.customsCode);$('#ciqNo').val(res.data.ciqCode);}}});
}
function obtainLoginBgFile(docId,allowEdit) {
    $.post(rurl('scm/file/fileList'),{docId:docId,docType:'subject',businessType:'sub_login_bg'},function (res) {
        if(isSuccess(res)){
            $.each((res.data || []),function (i,o) {
                createBgItem(o.id,rurl('/scm/download/img/'+o.id),'own',allowEdit);
            });
        }
    });
}
function createBgItem(id,url,own,allowEdit) {
    url = rurl(url);
    if(url.indexOf('?v=')!=-1){url = url.substring(0,url.indexOf('?v='));}
    var ihtml = [
        '<div class="login-bcks ',(allowEdit?'':'tsf-hide '),own,'">','<img src="',url,'" class="login-bcks-img" id="img',id,'"/>',(allowEdit?'<i class="fa fa-remove remove-bck" id="del'+id+'"></i>':''),'</div>'
    ];
    $('#bckArea').append(ihtml.join(''));
    if(allowEdit){
        $('#img'.concat(id)).click(function (event) {
            event.stopPropagation();
            var $that = $(this);
            $('#loginBg').val($that.attr('src'));
            checkBgItem();
        });
        $('#del'.concat(id)).click(function (event) {
            event.stopPropagation();
            var $that = $(this);
            delete_inquiry('是否确定要删除此背景图？','scm/file/removes',{'ids':id},function () {
                $that.parent().remove();
                if(url==$('#loginBg').val()){
                    $('#loginBg').val($($('.login-bcks-img')[0]).attr('src'));
                    checkBgItem();
                }
            });
        });
    }
    checkBgItem();
}
function checkBgItem() {
    $('.select-bck').remove();
    var $loginBcksImg = $('.login-bcks-img[src="'+$('#loginBg').val()+'"]');
    $loginBcksImg.after('<i class="fa fa-check-circle select-bck"></i>');
    $loginBcksImg.parent().show();
}