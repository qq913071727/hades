layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    var id = getUrlParam('id');
    show_loading();
    $.get(rurl('scm/subjectBank/detail'),{'id':id},function (res) {
       if(isSuccess(res)){
           var data = res.data;
           form.val('form',data);
           autoCurrency();
           autoBaseBank();
       }
    });
    form.on('submit(save-btn)', function(data){
        formSub('scm/subjectBank/edit',function (sres) {
            parent.reloadList();
            closeThisWin('修改成功');
        });
        return false;
    });
});
