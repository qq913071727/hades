layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    operation.init({
        listQuery:true,
        listQueryArg:{
            url:rurl('scm/subjectBank/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'name', width:240, title: '银行名称'}
                ,{field:'account', width:150, title: '银行账号'}
                ,{field:'currencyName', width:80, title: '币制'}
                ,{field:'dateCreated', width:130, title: '登记日期'}
                ,{field:'disabled', width:100,align:'form-switch',title: '状态',templet:'#disabledTemp'}
                ,{field: 'isDefault', title: '默认', width: 65,align:'center',templet:'#isDefaultTemp'}
                ,{field: 'receivables', title: '收款行', width: 65,align:'center',templet:'#receivablesTemp'}
                ,{field:'address', title: '地址'}
                ,{field:'memo', title: '备注'}
            ]]
        }
    });
    form.on('switch(switchDisabled)', function(data){
        show_loading();
        var $that = this;
        $.post(rurl('scm/subjectBank/updateDisabled'),{'id':data.value,'disabled':!$that.checked},function (res) {
            if(isSuccess(res)){
                show_success('修改成功');
            }else{
                $(data.elem).attr({'checked':(!this.checked)});
                form.render('checkbox');
            }
        });
    });
});
