layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    autoCurrency();
    autoBaseBank();
    form.on('submit(save-btn)', function(data){
        formSub('scm/subjectBank/add',function (res) {
            parent.reloadList();
            closeThisWin('添加成功');
        });
        return false;
    });
});
