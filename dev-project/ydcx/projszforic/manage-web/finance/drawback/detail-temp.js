layui.define(['layer','laytpl','element','table'], function(exports){
    var layer = layui.layer,laytpl=layui.laytpl,table=layui.table,
        detailTemp = {
            render:function (id,operation,fn) {
                $.post(rurl('scm/drawback/detail'),{'id':id,'operation':operation},function (res) {
                    if(isSuccessWarnClose(res)){
                        detailTemp.renderData(res.data);
                        $('.file-size[show-file="drawback"]').attr({'doc-id':id});
                        initFileSize();
                        if(fn){
                            fn(res.data);
                        }
                    }
                });
            },
            renderData(data){
                let drawback = data.drawback;
                laytpl($('#drawbackViewTemp').html()).render(drawback,function (html) {
                    $('#drawbackView').html(html);
                });
                table.render({
                    elem: '#purchase-contract-list',totalRow: true,limit:5000,page:false
                    , cols: [[
                        {type:'numbers',title:'序号',width:45}
                        ,{field:'orderDate',title:'出口日期',width:100,align:'center'}
                        ,{field:'orderNo',title:'订单号',width:110,align:'center'}
                        ,{field:'orderTotalPrice',title:'订单金额',width:100,align:'money', totalRow: true}
                        ,{field:'orderCurrencyName',title:'币制',width:80,align:'center'}
                        ,{field:'purchaseNo',title:'采购合同号',width:110,align:'center'}
                        ,{field:'purchaseTotalPrice',title:'采购合同金额',width:100,align:'money', totalRow: true}
                        ,{field:'invoiceValue',title:'收票金额',width:100,align:'money', totalRow: true}
                        ,{field:'settlementValue',title:'结汇金额',width:100,align:'money', totalRow: true}
                        ,{field:'estimatedPayment',title:'预估货款',width:100,align:'money', totalRow: true}
                        ,{field:'agencyFee',title:'代理费金额',width:100,align:'money', totalRow: true}
                        ,{field:'deductionValue',title:'代垫费金额',width:100,align:'money', totalRow: true}
                        ,{field:'actualDrawbackValue',title:'应退税金额',width:100,align:'money', totalRow: true}
                        ,{field:'alreadyDrawbackValue',title:'已退税金额',width:100,align:'money', totalRow: true}
                        ,{field:'drawbackValue',title:'本次退税金额',width:100,align:'money', totalRow: true}
                    ]]
                    , data: data.orders
                });
                formatTableData();
            }
        };
    exports('detailTemp', detailTemp);
});