layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer,laytpl=layui.laytpl,form=layui.form
        ,id = getUrlParam("id"),autoChapter = true,$chapterArea=$('.chapterArea');
    show_loading();
    var contrNo = "";
    $.get(rurl('scm/drawback/getPaymentPrintData?id=' + id),function (res) {
        if(isSuccessWarn(res)){
            var data = res.data;
            contrNo = data.clientNo;
            laytpl($('#conTemp').html()).render(data,function (html) {
                $('#conTempView').html(html);
            });
        }
    });

    $("#print").click(function () {
        top.document.title = contrNo + "(出口退税付款申请)";
        setTimeout(function () {
            top.document.title = "";
        },2000);
        window.print();
    });

});
