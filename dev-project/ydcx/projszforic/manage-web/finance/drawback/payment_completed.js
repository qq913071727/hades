layui.config({base: '/finance/drawback/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','form'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,form=layui.form;
    var id = getUrlParam("id");

    $('#paymentDetail').load(rurl('finance/drawback/detail-temp.html'),function(){
        detailTemp.render(id,'drawback_completed');
        $('#id').val(id);
        $('#actualPaymentDate').val(getNowFormatTime());
        $('#completedPaymentMemo').attr({'doc-id':id});
        initFileSize();
    });
    $('#backConfirm').click(function () {
        var taskArg = {'operation':'drawback_completed','documentId':id,'documentClass':'Drawback','newStatusCode':'waitConfirm'};
        layer.prompt({formType:2,maxlength: 500,title:'请填写退回原因'},function(val, index){
            show_loading();
            taskArg['memo'] = val;
            $.post(rurl('scm/drawback/backConfirm'),taskArg,function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
    });
    $('#confirmBank').click(function () {
        var taskArg = {'operation':'drawback_completed','documentId':id,'documentClass':'Drawback','newStatusCode':'confirmBank','memo':'重新确定付款行'};
        show_loading();
        $.post(rurl('scm/drawback/backConfirmBank'),taskArg,function (res) {
            if(isSuccess(res)){
                parent.reloadList();
                closeThisWin('操作成功');
            }
        });
    });
    $('#confirmPaymen').click(function () {
        var $from = $('#operationForm');
        if(checkedForm($from)){
            show_loading();
            $.post(rurl('scm/drawback/paymentCompleted'),$from.serialize(),function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        }
    });
});
