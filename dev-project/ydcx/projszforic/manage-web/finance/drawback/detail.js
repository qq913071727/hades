layui.config({base: '/finance/drawback/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#drawbackDetail').load(rurl('finance/drawback/detail-temp.html'),function () {
        detailTemp.render(id,'');
    });
});