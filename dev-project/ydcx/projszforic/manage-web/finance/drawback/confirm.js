layui.config({base: '/finance/drawback/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','laytpl'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,laytpl=layui.laytpl;
    var id = getUrlParam("id");
    $('#paymentDetail').load(rurl('finance/drawback/detail-temp.html'),function () {
        detailTemp.render(id,'drawback_confirm');

        var taskArg = {'operation':'drawback_confirm','documentId':id,'documentClass':'Drawback','newStatusCode':'confirmBank'};
        $('#confirm').click(function () {
            taskArg['memo'] = $('#taskMemo').val();
            if(isEmpty(taskArg['memo'])){
                show_error('请填写意见');
                $('#taskMemo').focus();
                return;
            }
            show_loading();
            $.post(rurl('scm/drawback/confirm'),taskArg,function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
        // 删除
        $('#toVoid').click(function () {
            delete_inquiry('是否确定要删除退税单信息？','scm/drawback/remove',{'id':id},function () {
                parent.reloadList();
                closeThisWin('操作成功');
            });
        });
    });
    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
});