layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer,laytpl=layui.laytpl,form=layui.form
        ,id = getUrlParam("id"),autoChapter = true,$chapterArea=$('.chapterArea');
    show_loading();
    $.post(rurl('scm/impSalesContract/contractData'),{'id':id},function (res) {
        if(isSuccessWarn(res)){
            var data = res.data;
            var tquantity = 0.0,ttotalPrice=0.0;
            $.each(data.members || [],function (i,o) {
                tquantity += o.quantity;
                ttotalPrice += o.totalPrice;
            });
            data['tquantity'] = Math.round(tquantity*100)/100;
            data['ttotalPrice'] = Math.round(ttotalPrice*100)/100;
            laytpl($('#conTemp').html()).render(data,function (html) {
                $('#conTempView').html(html);
                $chapterArea=$('.chapterArea');
            });
        }
    });
    $('#exportPdf').click(function () {
        show_loading();
        $.get(rurl('scm/impSalesContract/exportPdf'),{'id':id},function (res) {
            if(isSuccessWarn(res)){
                exportFile(res.data);
            }
        });
    });

});
