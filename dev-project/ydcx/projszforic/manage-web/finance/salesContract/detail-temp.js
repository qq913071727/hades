layui.define(['layer','laytpl','element','table'], function(exports){
    var layer = layui.layer,laytpl=layui.laytpl,table=layui.table,
        detailTemp = {
            render:function (id,operation) {
                $.post(rurl('scm/impSalesContract/detail'),{'id':id,'operation':operation},function (res) {
                    if(isSuccessWarnClose(res)){
                        detailTemp.renderData(res.data);
                    }
                });
            },
            renderData(data){
                let mainData = data.salesContract,members = data.members,costMembers = data.costMembers;
                laytpl($('#salesContractViewTemp').html()).render(mainData,function (html) {
                    $('#salesContractView').html(html);
                });

                //合同明细信息
                table.render({
                    elem: '#member-list',limit:5000,page:false,totalRow: true
                    ,cols: [[
                        {type:'numbers',title:'序号',width:45, totalRowText: '合计'}
                        ,{field:'goodsName',title:'物料名称',width:180}
                        ,{field:'goodsModel',title:'物料型号',width:180}
                        ,{field:'goodsBrand',title:'物料品牌',width:150}
                        ,{field:'countryName',title:'产地',width:100}
                        ,{field:'unitName',title:'单位',width:80}
                        ,{field:'quantity',title:'数量',width:100,align:'money', totalRow: true}
                        ,{field:'totalPrice',title:'总价',width:100,align:'money', totalRow: true}
                        ,{field:'currencyName',title:'币制',width:100}
                    ]]
                    , data: members
                });

                //发票费用明细
                table.render({
                    elem: '#cost-list',limit:5000,page:false,totalRow: true
                    ,cols: [[
                        {type:'numbers',title:'序号',width:45, totalRowText: '合计'}
                        ,{field:'expenseSubjectName',title:'费用名称',width:160}
                        ,{field:'costMoney',title:'金额',width:180,align:'money', totalRow: true}
                        ,{field:'orderNo',title:'订单号',width:100}
                        ,{field:'docNo',title:'合同/发票号',width:100}
                        ,{field:'memo',title:'说明'}
                    ]]
                    , data: costMembers
                });

            }
        };
    exports('detailTemp', detailTemp);
});