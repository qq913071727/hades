layui.config({base: '/finance/salesContract/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#salesContractDetail').load(rurl('finance/salesContract/detail-temp.html'),function () {
        detailTemp.render(id,'',true,false);
    });
});