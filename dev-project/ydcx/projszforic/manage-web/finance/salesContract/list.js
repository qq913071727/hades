layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/impSalesContract/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                ,{field:'docNo', width:130, title: '合同编号',templet:'#docNoTemp',fixed: true}
                ,{field:'impOrderNo', width:120, title: '订单编号',templet:'#orderTemp',fixed: true}
                ,{field:'contractDate', width:150, title: '合同日期'}
                ,{field:'buyerName', width:260, title: '买方(客户)',templet:'#customerNameTemp'}
                ,{field:'totalPrice', width:100, title: '合同金额',align:'money'}
                ,{field:'goodsVal', width:100, title: '货款金额',align:'money'}
                ,{field:'differenceVal', width:100, title: '票差调整金额',align:'money'}
                ,{field:'invoiceNo', width:150, title: '发票号'}
                ,{field:'busPersonName', width:70, title: '跟进商务'}
                ,{field:'buyerBankName', width:240, title: '买方银行'}
                ,{field:'buyerBankAccount', width:200, title: '买方银行账号'}
            ]]
        },listQueryDone:function(){
            formatTableData();
        }
    });
});
function showDetail(id) {
    openwin(rurl('finance/salesContract/detail.html?id='+id),'销售合同详情',{area: ['0', '0'],shadeClose:true});
}

function batchDownloadZip (o) {
    let datas = getCheckDatas();
    if(datas.length <= 0){
        show_error('请选择需要下载的合同');
    }else{
        var ids = [];
        for(let data of datas) {
            ids.push(data.id);
        }
        if(ids.length > 10) {
            show_error('一次最多只能选择10个合同下载');
            return;
        }
        show_msg("正在下载，请耐心等待");
        $('button[btn-code=700608]').attr('disabled',true);
        setTimeout(function () {
            $('button[btn-code=700608]').removeAttr('disabled');
        },5000);
        location.href = '/scm/impSalesContract/batchDownloadZip?ids=' + ids.join(",")
    }
}
