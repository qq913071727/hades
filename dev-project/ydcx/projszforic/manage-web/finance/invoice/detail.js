layui.config({base: '/finance/invoice/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#invoiceDetail').load(rurl('finance/invoice/detail-temp.html'),function () {
        detailTemp.render(id,'');
    });
});