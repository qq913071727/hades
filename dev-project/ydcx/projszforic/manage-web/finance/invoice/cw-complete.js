layui.config({base: '/finance/invoice/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#invoiceDetail').load(rurl('finance/invoice/detail-temp.html'),function () {
        detailTemp.render(id,'inv_cw_complete');
        var $operationTaskBtn = $('.operation-task-btn');
        $operationTaskBtn.click(function () {
            var $that = $(this),statusId = $that.attr('status-code'),invoiceNo=$('#invoiceNo').val();
            if('invoiced'==statusId&&isEmpty(invoiceNo)){show_error('请填写发票号');$('#invoiceNo').select();return;}
            show_loading();
            $.post(rurl('scm/invoice/completeInvoice'),{'id':id,'invoiceNo':invoiceNo,'statusId':statusId},function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
    });
});