layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/invoice/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'operation', width:60, title: '操作',templet:'#taskTemp',align:'center', fixed: true}
                ,{field:'statusName', width:120, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                ,{field:'docNo', width:120, title: '系统编号',templet:'#docNoTemp',fixed: true}
                ,{field:'orderNo', width:120, title: '订单编号',templet:'#orderTemp',fixed: true}
                ,{field:'applyDate', width:90, title: '申请日期'}
                ,{field:'confirmDate', width:90, title: '确认日期'}
                ,{field:'invoiceDate', width:90, title: '开票日期'}
                ,{field:'taxTypeDesc', width:150, title: '开票类型'}
                ,{field:'buyerName', width:260, title: '购货单位(客户)',templet:'#customerNameTemp'}
                ,{field:'busPersonName', width:70, title: '跟进商务'}
                ,{field:'invoiceVal', width:100, title: '发票金额',align:'money'}
                ,{field:'saleContractNo', width:120, title: '销售合同号'}
                ,{field:'invoiceNo', width:120, title: '发票号'}
                ,{field:'buyerBankName', width:280, title: '客户银行'}
                ,{field:'buyerBankAccount', width:160, title: '客户银行账号'}
            ]]
        },listQueryDone:function(){
            formatTableData();
        }
    });
});

function showDetail(id) {
    openwin(rurl('finance/invoice/detail.html?id='+id),'发票详情',{area: ['0', '0'],shadeClose:true});
}
function removeInvoice (o) {
    let data = getCheckDatas();
    if(data.length==1){
        let item = data[0];
        delete_inquiry('当前单据状态<span class="f16b '+item.statusId+'">'+item.statusDesc+'</span>，是否确定要删除？',o.url,{'id':item.id},function (res) {
            show_success('删除成功');
            reloadList();
        });
    }else{
        show_error('请选择一条您要删除的发票信息');
    }
}
function exportSpaceFlightExcel(o) {
    var ids = getIds();
    if(ids.length==0){
        show_error('请至少选择一条您要导出的开票资料');return;
    }
    show_loading();
    $.post(o.url,{ids:ids.join(',')},function (res) {
        if(isSuccess(res)){
            exportFile(res.data);
        }
    });
}

function customerConfirmTax(o) {
    let data = getCheckDatas();
    if(data.length==1){
        let id = data[0].id;
        $.post(rurl("/scm/invoice/customerConfirmTax"),{"id":id},function (res) {
            if(isSuccess(res)){
                reloadList();
                closeThisWin('操作成功');
            }
        })
    }else{
        show_error('请选择一条您要代客户确认的发票信息');
    }
}
