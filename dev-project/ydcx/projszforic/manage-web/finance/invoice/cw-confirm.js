layui.use(['layer','form','table','laytpl'], function() {
    var layer = layui.layer, form = layui.form, table = layui.table,laytpl=layui.laytpl;
    var id = getUrlParam('id');show_loading();
    $.post(rurl('scm/invoice/detail'),{'id':id,'operation':'inv_confirm_tax'},function (res) {
        if(isSuccessWarnClose(res)){
            var data=res.data,mainData = data.invoice,members = data.members,costMembers = data.costMembers;
            laytpl($('#invoiceViewTemp').html()).render(mainData,function (html) {
                $('#invoiceView').html(html);
            });
            $('#id').val(mainData.id);
            //发票明细信息
            table.render({
                elem: '#member-list',limit:5000,page:false,totalRow: true
                ,cols: [[
                    {type:'numbers',title:'序号',width:45, totalRowText: '合计'}
                    ,{field:'isFirst',title:'新料',width:60,align:'center',templet:'#isFirstTemp'}
                    ,{field:'taxCode',title:'税收分类编码',width:160,align:'form',templet:'#taxCodeTemp'}
                    ,{field:'taxName',title:'分类简称',width:180,align:'form',templet:'#taxNameTemp'}
                    ,{field:'invoiceName',title:'开票名称',width:180,align:'form',templet:'#invoiceNameTemp'}
                    ,{field:'goodsName',title:'物料名称',width:180,align:'form',templet:'#goodsNameTemp'}
                    ,{field:'goodsModel',title:'物料型号',width:180}
                    ,{field:'goodsBrand',title:'物料品牌',width:150}
                    ,{field:'unitName',title:'单位',width:80}
                    ,{field:'quantity',title:'数量',width:100,align:'money', totalRow: true}
                    ,{field:'totalPrice',title:'总价',width:100,align:'money', totalRow: true}
                ]]
                , data: members
            });
            //发票费用明细
            table.render({
                elem: '#cost-list',limit:5000,page:false,totalRow: true
                ,cols: [[
                    {type:'numbers',title:'序号',width:45, totalRowText: '合计'}
                    ,{field:'expenseSubjectName',title:'费用名称',width:160}
                    ,{field:'costMoney',title:'金额',width:100,align:'money', totalRow: true}
                    ,{field:'orderNo',title:'订单号',width:120}
                    ,{field:'docNo',title:'合同/发票号',width:120}
                    ,{field:'memo',title:'说明'}
                ]]
                , data: costMembers
            });
            formatTableData();

            $('.auto-tax-code').autocomplete({
                source: function (request, response) {
                    $.get(rurl('system/taxCodeBase/search'),{code: trim(request.term)},function (res) {
                        response($.map(res.data, function (item) {return {
                            label: item.id  +'   '+item.shortName, value: item.id, data: item
                        }}));
                    });
                }, minLength: 0,autoFocus:true, delay: 500,
                select: function( event, ui){
                    var data = ui.item.data;
                    $('#taxCodeName'+$(this).attr('mid')).val(data.shortName);
                }
            }).focus(function () {
                $(this).select().autocomplete("search");
            });
        }
    });

    $('.save-btn').click(function () {
        var $mainForm = $('#mainForm');
        if(checkedForm($mainForm)){
            var members = jsonList($mainForm.serializeJson());
            postJSON('scm/invoice/financialTaxCode',{'id':id,'members':members},function (res) {
               if(isSuccess(res)){
                   parent.reloadList();
                   closeThisWin('操作成功');
               }
            });
        }
    });
});