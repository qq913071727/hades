layui.config({base: '/finance/invoice/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#invoiceDetail').load(rurl('finance/invoice/detail-temp.html'),function () {
        detailTemp.render(id,'inv_confirm_sale');

        var $operationTaskBtn = $('.operation-task-btn'),
            taskArg = {'operation':'inv_confirm_sale','documentId':id,'documentClass':'Invoice','newStatusCode':''};
        $operationTaskBtn.click(function () {
            var $that = $(this),needMemo = $that.attr('need-memo');
            if(!isEmpty(needMemo)){
                var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
                taskArg['memo'] = $memo.val();
            }else{
                taskArg['memo'] = $('#taskMemo').val();
            }
            taskArg['newStatusCode'] = $that.attr('status-code');

            show_loading();
            $.post(rurl('scm/invoice/businessConfirm'),taskArg,function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
    });

    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
});