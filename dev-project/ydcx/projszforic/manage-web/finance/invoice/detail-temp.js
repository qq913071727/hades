layui.define(['layer','laytpl','element','table'], function(exports){
    var layer = layui.layer,laytpl=layui.laytpl,table=layui.table,
        detailTemp = {
            render:function (id,operation) {
                $.post(rurl('scm/invoice/detail'),{'id':id,'operation':operation},function (res) {
                    if(isSuccessWarnClose(res)){
                        detailTemp.renderData(res.data);
                        if(!isEmpty(operation)&&operation=='inv_confirm_sale'&&!isEmpty(res.data.invoice.customerReason)&&'OK'!=res.data.invoice.customerReason){
                            layer.alert('此单开票申请，客户对税收分类编码有疑问，请与客户沟通！！！', {icon: 6,title:'温馨提醒',btn:['朕知道了']});
                        }
                    }
                });
            },
            renderData(data){
                let mainData = data.invoice,members = data.members,costMembers = data.costMembers;
                laytpl($('#invoiceViewTemp').html()).render(mainData,function (html) {
                    $('#invoiceView').html(html);
                });

                //发票明细信息
                table.render({
                    elem: '#member-list',limit:5000,page:false,totalRow: true
                    ,cols: [[
                        {type:'numbers',title:'序号',width:45, totalRowText: '合计'}
                        ,{field:'isFirst',title:'新料',width:60,align:'center',templet:'#isFirstTemp'}
                        ,{field:'taxCode',title:'税收分类编码',width:160}
                        ,{field:'taxCodeName',title:'分类简称',width:180}
                        ,{field:'invoiceName',title:'开票名称',width:180}
                        ,{field:'goodsName',title:'物料名称',width:180}
                        ,{field:'goodsModel',title:'物料型号',width:180}
                        ,{field:'goodsBrand',title:'物料品牌',width:150}
                        ,{field:'unitName',title:'单位',width:80}
                        ,{field:'quantity',title:'数量',width:100,align:'money', totalRow: true}
                        ,{field:'totalPrice',title:'总价',width:100,align:'money', totalRow: true}
                    ]]
                    , data: members
                });

                //发票费用明细
                table.render({
                    elem: '#cost-list',limit:5000,page:false,totalRow: true
                    ,cols: [[
                        {type:'numbers',title:'序号',width:45, totalRowText: '合计'}
                        ,{field:'expenseSubjectName',title:'费用名称',width:160}
                        ,{field:'costMoney',title:'金额',width:100,align:'money', totalRow: true}
                        ,{field:'orderNo',title:'订单号',width:120}
                        ,{field:'docNo',title:'合同/发票号',width:120}
                        ,{field:'memo',title:'说明'}
                    ]]
                    , data: costMembers
                });
                formatTableData();

            }
        };
    exports('detailTemp', detailTemp);
});