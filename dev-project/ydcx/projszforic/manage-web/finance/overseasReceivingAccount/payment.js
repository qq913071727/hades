layui.config({base: '/finance/overseasReceivingAccount/'}).extend({detailTemp: 'detail-temp'}).use(['layer','form','detailTemp'], function(){
    var layer = layui.layer,form = layui.form,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#accountDetail').load(rurl('finance/overseasReceivingAccount/detail-temp.html'),function () {
        detailTemp.render(id,'overseas_receiving_account_payment',function () {
            show_loading();
            $.get('/scm/overseasReceivingAccount/initPayment',{id:id},function (res) {
                if(isSuccessWarnClose(res)){
                    form.val('form',res.data);
                }
            });
        });
    });
    form.on('submit(save-btn)', function(data){
        if(checkedForm($('#form'))){
            formSubJson('scm/expPaymentAccount/save',function (res) {
                parent.reloadList();
                closeThisWin('保存成功');
            });
        }
        return false;
    });

    $('.auto-customer-bank').autocomplete({
        source: function (request, response) {
            $.post(rurl('scm/customerBank/page'),{customerName:$('#payeeName').val(),name: trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: item.name+'('+item.account+')', value: item.name, data: item}}));
            });
        }, minLength: 0,autoFocus:true, delay: 300,
        select: function( event, ui ) {
            var data = ui.item.data;
            form.val("form",{'payeeBankNo':data.account});
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
});