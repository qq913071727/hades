layui.define(['layer','laytpl','element','table'], function(exports){
    var layer = layui.layer,laytpl=layui.laytpl,table=layui.table,
        detailTemp = {
            render:function (id,operation,fn) {
                $.get(rurl('scm/overseasReceivingAccount/detail'),{'id':id,'operation':operation},function (res) {
                    if(isSuccessWarnClose(res)){
                        $('.file-size[show-file="paymentMemo"]').attr({'doc-id':id});
                        initFileSize();
                        detailTemp.renderData(res.data);
                        if(fn){
                            fn(res.data);
                        }
                    }
                });
            },
            renderData(data){
                let account = data.account,orders = data.orders || [];
                laytpl($('#accountViewTemp').html()).render(account,function (html) {
                    $('#accountView').html(html);
                });
                table.render({
                    elem: '#order-list',totalRow: true,limit:5000,page:false
                    , cols: [[
                        {type:'numbers',title:'序号',width:45}
                        ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', totalRowText: '合计'}
                        ,{field:'orderNo',title:'订单号',width:120,align:'center'}
                        ,{field:'customerName', width:260, title: '客户名称'}
                        ,{field:'orderDate',title:'出口日期',width:120,align:'center'}
                        ,{field:'clientNo',title:'客户单号',width:120,align:'center'}
                        ,{field:'decTotalPrice',title:'出口金额',width:120,align:'money', totalRow: true}
                        ,{field:'currencyName',title:'币制',width:80,align:'center'}
                        ,{field:'accountValue',title:'认领金额',width:120,align:'money', totalRow: true}
                    ]]
                    , data: orders
                });
                formatTableData();
            }
        };
    exports('detailTemp', detailTemp);
});