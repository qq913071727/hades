layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    var fileId = 'temp_'+uuid();
    var fileObj = {docId:fileId, docType:'overseas_receiving_account'};
    $('.file-size').attr({'doc-id':fileObj.docId});
    fn = {
        initSubject:function (callback) {
            show_loading();
            $.get('/scm/overseasReceivingAccount/subjectSelect',function (res) {
                if(isSuccess(res)){
                    var $subjectId = $('#subjectId');
                    $.each(res.data,function (i,o) {
                        $subjectId.append('<option value="'+o.id+'_'+o.type+'">'+o.name+'</option>');
                    });
                    form.render('select');
                    fn.renderOverseasBank();
                    callback();
                    autoCurrency();
                }
            });
        },
        renderOverseasBank:function () {
            var params = {
                subjectType:$('#subjectId').val().split('_')[1],
                overseasId:$('#subjectId').val().split('_')[0],
                overseasName:$('#subjectId option:selected').text()
            }
            form.val('form',params);
            var bankUrl = '/scm/subjectBank/list';
            if('abroad'==params.subjectType){
                bankUrl = '/scm/subjectOverseasBank/selectByOverseasId'
            }
            show_loading();
            var $overseasBankId=$('#overseasBankId');
            $overseasBankId.get(0).length = 0;
            $.post(bankUrl,{overseasId:params.overseasId},function (res) {
                if(isSuccess(res)){
                    $.each(res.data,function (i,o) {
                        $overseasBankId.append('<option value="'+o.id+'">'+o.name+'（'+o.account+'）</option>');
                    });
                    form.render('select');
                }
            });
        }
    };
    fn.initSubject(function () {
        $('#fileId').val(fileId);
        initFileSize();
    });
    form.on('select(subjectId)',function (data) {
        fn.renderOverseasBank();
    });
    form.on('submit(save-btn)', function(data){
        formSub('scm/overseasReceivingAccount/add',function (res) {
            parent.reloadList();
            closeThisWin('登记成功');
        });
        return false;
    });
    $('.auto-supplier-bank').autocomplete({
        source: function (request, response) {
            $.post(rurl('scm/supplierBank/vague'),{customerId:$('#customerId').val(),supplierName:$('#payer').val(),bankName: trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: item.name, value: item.name, data: item}}));
            });
        }, minLength: 0,autoFocus:true, delay: 300,
        select: function( event, ui ) {
            var data = ui.item.data;
            form.val("form",{'payerBankNo':data.account});
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
});
function fullCustomerData(data) {
    $('#customerId').val(data.id);
}