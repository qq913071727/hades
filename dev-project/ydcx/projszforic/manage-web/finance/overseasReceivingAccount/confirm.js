layui.config({base: '/finance/overseasReceivingAccount/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','laytpl','form'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,laytpl=layui.laytpl,form=layui.form;
    var id = getUrlParam("id"),accountValue = 0.0;
    $('#accountDetail').load(rurl('finance/overseasReceivingAccount/detail-temp.html'),function () {
        detailTemp.render(id,'overseas_receiving_account_confirm',function (data) {
            var account = data.account;
            $('#accountDate').val(getNowFormatTime());
            accountValue = account.accountValue;
            var jsAmountReceived = function () {
                $('#amountReceived').val(formatCurrency(accountValue - parseFloat($('#fee').val())));
            };
            jsAmountReceived();
            $('#fee').blur(function () {
                jsAmountReceived();
            }).keyup(function () {
                jsAmountReceived();
            });
            $('#contractId').val(id);
            $('#confirm').click(function () {
                if(checkedForm($('#operationForm'))){
                    show_loading();
                    $.post('/scm/overseasReceivingAccount/confirm',$('#operationForm').serialize(),function (res) {
                       if(isSuccess(res)){
                           parent.reloadList();
                           closeThisWin('操作成功');
                       }
                    });
                }
            });
            var taskArg = {'operation':'overseas_receiving_account_cancel','documentId':id,'documentClass':'OverseasReceivingAccount','newStatusCode':'invalid'};
            $('#toVoid').click(function () {
                layer.prompt({formType:2,maxlength: 500,title:'请填作废原因'},function(val, index){
                    show_loading();
                    taskArg['memo'] = val;
                    $.post(rurl('scm/overseasReceivingAccount/toVoid'),taskArg,function (res) {
                        if(isSuccess(res)){
                            parent.reloadList();
                            closeThisWin('操作成功');
                        }
                    });
                });
            });
        });
    });


});