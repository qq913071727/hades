layui.config({base: '/finance/overseasReceivingAccount/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#accountDetail').load(rurl('finance/overseasReceivingAccount/detail-temp.html'),function () {
        detailTemp.render(id,'');
    });
});