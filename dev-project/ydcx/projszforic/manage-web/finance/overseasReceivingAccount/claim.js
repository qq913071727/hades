layui.config({base: '/finance/overseasReceivingAccount/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','table'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,table=layui.table;
    var id = getUrlParam("id"),accountValue = 0.0;
    $('#accountDetail').load(rurl('finance/overseasReceivingAccount/detail-temp.html'),function () {
        detailTemp.render(id,'overseas_receiving_account_claim',function (data) {
            var account = data.account;
            $('#c_m1').html(formatCurrency(account.accountValue));
            $('#c_m2').html(formatCurrency(account.kwriteValue - account.writeValue));
            // 根据客户和币制获取待认领订单
            show_loading();
            $.post('/scm/expOrder/collectionClaimedAmount',{customerId:account.customerId,currencyId:account.currencyId},function (res) {
                if(isSuccessWarnClose(res)){
                    var orders = res.data;
                    table.render({
                        elem: '#claimOrderTable',limit:5000,height:'166',page:false
                        ,cols: [[
                            {checkbox: true, fixed: true}
                            ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp'}
                            ,{field:'orderNo',title:'订单号',width:120,align:'center'}
                            ,{field:'customerName', width:260, title: '客户名称'}
                            ,{field:'orderDate',title:'出口日期',width:120,align:'center'}
                            ,{field:'clientNo',title:'客户单号',width:120,align:'center'}
                            ,{field:'decTotalPrice',title:'出口金额',width:120,align:'money'}
                            ,{field:'currencyName',title:'币制',width:80,align:'center'}
                            ,{field:'unclaimedValue',title:'可认领金额',width:120,align:'money'}
                            ,{field:'claimedValue',title:'本次认领金额',width:120,templet:'#claimedValueTemp'}
                        ]]
                        , data: orders
                    });
                    formatTableData();
                    initInput();
                    table.on('checkbox(claimOrderTable)', function(obj){
                        totalVal();
                    });
                    $('.unclaimedValue').blur(function () {
                        totalVal();
                    });
                }
            });
        });
    });
    var totalVal = function () {
        var total = 0.0,tabs = table.checkStatus('claimOrderTable').data;
        $.each(tabs,function (i,o) {
            total += parseFloat($('#'+o.expOrderId).val());
        });
        $('#c_m3').html(formatCurrency(total));
    }
    
    $('#confirm').click(function () {
        var tabs = table.checkStatus('claimOrderTable').data;
        if(tabs.length==0){
            show_error('请至少选择一条订单进行认领');return;
        }
        var orders = [];
        $.each(tabs,function (i,o) {
            orders.push({
                orderId:o.expOrderId,
                unclaimedValue:Math.round(parseFloat($('#'+o.expOrderId).val())*100)/100
            });
        });
        postJSON('/scm/overseasReceivingAccount/claim',{id:id,orders:orders},function (data) {
            parent.reloadList();
            closeThisWin('操作成功');
        });
    });
    $('#clearClaimOrder').click(function () {
        delete_inquiry('您确定要清空当前收款单所有认领订单数据？','scm/overseasReceivingAccount/clearClaimOrder',{'id':id},function () {
            prompt_success('操作成功',function () {
                refPage();
            });
        });
    });
});