layui.config({base: '/finance/overseasReceivingAccount/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','laytpl'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,laytpl=layui.laytpl;
    var id = getUrlParam("id");
    $('#accountDetail').load(rurl('finance/overseasReceivingAccount/detail-temp.html'),function () {
        detailTemp.render(id, 'overseas_receiving_account_cancel');

        var $operationTaskBtn = $('.operation-task-btn'),
            taskArg = {'operation':'overseas_receiving_account_cancel','documentId':id,'documentClass':'OverseasReceivingAccount','newStatusCode':''};
        $operationTaskBtn.click(function () {
            var $that = $(this),needMemo = $that.attr('need-memo');
            if(!isEmpty(needMemo)){
                var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
                taskArg['memo'] = $memo.val();
            }else{
                taskArg['memo'] = $('#taskMemo').val();
            }
            taskArg['newStatusCode'] = $that.attr('status-code');

            show_loading();
            $.post(rurl('scm/overseasReceivingAccount/toVoid'),taskArg,function (bres) {
                if(isSuccess(bres)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
    });

    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
});