layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/overseasReceivingAccount/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'operation', width:60, title: '操作',templet:'#taskTemp',align:'center', fixed: true}
                ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                ,{field:'docNo', width:126, title: '收款单号',templet:'#docNoTemp'}
                ,{field:'orderNo', width:130, title: '订单号'}
                ,{field:'clientNo', width:130, title: '客户单号'}
                ,{field:'dateCreated', width:120, title: '登记时间'}
                ,{field:'accountDate', width:120, title: '到账时间'}
                ,{field:'customerName', width:260, title: '结算单位',templet:'#customerNameTemp'}
                ,{field:'accountValue', width:100, title: '收款金额',align:'money'}
                ,{field:'currencyName', width:70, title: '币制',align:'center'}
                ,{field:'undertakingModeDesc', width:100, title: '手续费承担方式',align:'center'}
                ,{field:'fee', width:80, title: '手续费',align:'money'}
                ,{field:'actualValue', width:100, title: '实际到账金额',align:'money'}
                ,{field:'kwriteValue', width:100, title: '可认领金额',align:'money'}
                ,{field:'writeValue', width:100, title: '已认领金额',align:'money'}
                ,{field:'isSettlement', width:70, title: '是否结汇',align:'center',templet:'#isSettlementTemp'}
                ,{field:'isPayment', width:70, title: '是否付款',align:'center',templet:'#isPaymentTemp'}
                ,{field:'overseasName', width:280, title: '收款主体'}
                ,{field:'receivingBank', width:280, title: '收款行'}
                ,{field:'payer', width:180, title: '付款方'}
                ,{field:'createBy', width:80, title: '制单人',align:'center'}
                ,{field:'paymentMemo', width:110,fixed: 'right', title: '付款水单',templet:'#paymentMemoTemp'}
            ]]
        },listQueryDone:function(){
            formatTableData();
            initFileSize();
        }
    });
});
function showDetail(id) {
    openwin(rurl('finance/overseasReceivingAccount/detail.html?id='+id),'境外收款详情',{area: ['0', '0'],shadeClose:true});
}
