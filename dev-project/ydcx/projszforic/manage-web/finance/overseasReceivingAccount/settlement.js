layui.use(['layer','form','table'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table;
    var accountValue = 0.0,fn = {
        initSubject:function (callback) {
            show_loading();
            $.get('/scm/overseasReceivingAccount/subjectSelect?first=mainland',function (res) {
                if(isSuccess(res)){
                    var $subjectId = $('#subjectStrId');
                    $.each(res.data,function (i,o) {
                        $subjectId.append('<option value="'+o.id+'_'+o.type+'">'+o.name+'</option>');
                    });
                    form.render('select');
                    fn.renderOverseasBank();
                    callback();
                }
            });
        },
        renderOverseasBank:function () {
            var params = {
                subjectType:$('#subjectStrId').val().split('_')[1],
                subjectId:$('#subjectStrId').val().split('_')[0],
                subjectName:$('#subjectStrId option:selected').text()
            };
            $('#subjectType').val(params.subjectType);
            $('#subjectId').val(params.subjectId);
            var bankUrl = '/scm/subjectBank/list';
            if('abroad'==params.subjectType){
                bankUrl = '/scm/subjectOverseasBank/selectByOverseasId'
            }
            show_loading();
            var $overseasBankId=$('#subjectBank');
            $overseasBankId.get(0).length = 0;
            $.post(bankUrl,{overseasId:params.subjectId},function (res) {
                if(isSuccess(res)){
                    $.each(res.data,function (i,o) {
                        $overseasBankId.append('<option value="'+o.id+'">'+o.name+'（'+o.account+'）</option>');
                    });
                    form.render('select');
                }
            });
        },
        initSettlement:function () {
            var id = getUrlParam('id');
            $('#accountId').val(id);
            show_loading();
            $.get('/scm/overseasReceivingAccount/initSettlement',{id:id},function (res) {
                if(isSuccessWarnClose(res)){
                    var data = res.data,settlementAccount = data.settlementAccount,overseasReceivingAccountList = data.overseasReceivingAccountList;
                    accountValue = settlementAccount.accountValue;
                    form.val('form',settlementAccount);
                    fn.initAccountOrder(settlementAccount.accountNo);
                    table.render({
                        elem: '#account-list',totalRow: true,limit:5000,page:false
                        , cols: [[
                            {type:'numbers',title:'序号',width:45}
                            ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp'}
                            ,{field:'docNo', width:116, title: '收款单号'}
                            ,{field:'dateCreated', width:120, title: '登记时间'}
                            ,{field:'accountDate', width:120, title: '到账时间'}
                            ,{field:'customerName', width:220, title: '结算单位'}
                            ,{field:'accountValue', width:90, title: '收款金额',align:'money', totalRow: true}
                            ,{field:'currencyName', width:70, title: '币制',align:'center'}
                            ,{field:'fee', width:80, title: '手续费',align:'money', totalRow: true}
                            ,{field:'actualValue', width:90, title: '到账金额',align:'money', totalRow: true}
                            ,{field:'writeValue', width:90, title: '认领金额',align:'money', totalRow: true}
                            ,{field:'overseasName', width:220, title: '收款主体'}
                            ,{field:'receivingBank', width:200, title: '收款行'}
                        ]]
                        , data: overseasReceivingAccountList
                    });
                    formatTableData();
                }
            });
        },
        initAccountOrder:function (accountNos) {
            $.post('/scm/overseasReceivingAccount/overseasReceivings',{accountNos:accountNos},function (res) {
                if(isSuccess(res)){
                    table.render({
                        elem: '#order-list',totalRow: true,limit:5000,page:false
                        , cols: [[
                            {type:'numbers',title:'序号',width:45}
                            ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', totalRowText: '合计'}
                            ,{field:'orderNo',title:'订单号',width:120,align:'center'}
                            ,{field:'customerName', width:260, title: '客户名称'}
                            ,{field:'orderDate',title:'出口日期',width:120,align:'center'}
                            ,{field:'clientNo',title:'客户单号',width:120,align:'center'}
                            ,{field:'decTotalPrice',title:'出口金额',width:120,align:'money', totalRow: true}
                            ,{field:'currencyName',title:'币制',width:80,align:'center'}
                            ,{field:'accountValue',title:'认领金额',width:120,align:'money', totalRow: true}
                        ]]
                        , data: res.data
                    });
                    formatTableData();
                }
            });
        }
    };
    fn.initSubject(function () {
        fn.initSettlement();
    });
    form.on('select(subjectStrId)',function (data) {
        fn.renderOverseasBank();
    });
    $('#customsRate').blur(function () {
        $('#settlementValue').val(Math.round(accountValue * parseFloat($('#customsRate').val()) * 100)/100);
    });
    form.on('submit(save-btn)', function(data){
        var params = {
            subjectType:$('#subjectStrId').val().split('_')[1],
            subjectId:$('#subjectStrId').val().split('_')[0],
            subjectName:$('#subjectStrId option:selected').text()
        };
        $('#subjectType').val(params.subjectType);
        $('#subjectId').val(params.subjectId);
        formSubJson('scm/settlementAccount/save',function (res) {
            parent.reloadList();
            closeThisWin('保存成功');
        });
        return false;
    });
});
