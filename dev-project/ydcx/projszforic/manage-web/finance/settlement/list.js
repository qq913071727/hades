layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/settlementAccount/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp'}
                ,{field:'docNo', width:120, title: '结汇单号',align:'center',templet:'#docNoTemp'}
                ,{field:'orderNo', width:130, title: '订单号'}
                ,{field:'clientNo', width:130, title: '客户单号'}
                ,{field:'dateCreated', width:120, title: '创建时间',align:'center'}
                ,{field:'settlementDate', width:120, title: '结汇时间',align:'center'}
                ,{field:'accountValue', width:100, title: '原币金额',align:'money'}
                ,{field:'currencyName', width:80, title: '币制',align:'center'}
                ,{field:'customsRate', width:90, title: '结汇汇率',align:'center'}
                ,{field:'settlementValue', width:100, title: '本币金额'}
                ,{field:'subjectName', width:260, title: '结汇主体'}
                ,{field:'subjectBank', width:260, title: '结汇银行'}
                ,{field:'memo', title: '备注'}
            ]]
        },listQueryDone:function(){
            formatTableData();
        }
    });
});
function showDetail(id) {
    openwin(rurl('finance/settlement/detail.html?id='+id),'结汇详情',{area: ['0', '0'],shadeClose:true});
}
