layui.define(['layer','laytpl','element','table'], function(exports){
    var layer = layui.layer,laytpl=layui.laytpl,table=layui.table,
        detailTemp = {
            render:function (id,operation,fn) {
                $.post(rurl('scm/settlementAccount/detail'),{'id':id,'operation':operation},function (res) {
                    if(isSuccessWarnClose(res)){
                        detailTemp.renderData(res.data);
                        if(fn){
                            fn(res.data);
                        }
                    }
                });
            },
            renderData(data){
                let settlementAccount = data.settlementAccount,overseasReceivingAccountList = data.overseasReceivingAccountList;
                laytpl($('#settlementAccountViewTemp').html()).render(settlementAccount,function (html) {
                    $('#settlementAccountView').html(html);
                });
                detailTemp.initAccountOrder(settlementAccount.accountNo);
                table.render({
                    elem: '#account-list',totalRow: true,limit:5000,page:false
                    , cols: [[
                        {type:'numbers',title:'序号',width:45}
                        ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp'}
                        ,{field:'docNo', width:116, title: '收款单号'}
                        ,{field:'dateCreated', width:120, title: '登记时间'}
                        ,{field:'accountDate', width:120, title: '到账时间'}
                        ,{field:'customerName', width:220, title: '结算单位'}
                        ,{field:'accountValue', width:90, title: '收款金额',align:'money', totalRow: true}
                        ,{field:'currencyName', width:70, title: '币制',align:'center'}
                        ,{field:'fee', width:80, title: '手续费',align:'money', totalRow: true}
                        ,{field:'actualValue', width:90, title: '到账金额',align:'money', totalRow: true}
                        ,{field:'writeValue', width:90, title: '认领金额',align:'money', totalRow: true}
                        ,{field:'overseasName', width:220, title: '收款主体'}
                        ,{field:'receivingBank', width:200, title: '收款行'}
                    ]]
                    , data: overseasReceivingAccountList
                });
                formatTableData();
            },
            initAccountOrder:function (accountNos) {
                $.post('/scm/overseasReceivingAccount/overseasReceivings',{accountNos:accountNos},function (res) {
                    if(isSuccess(res)){
                        table.render({
                            elem: '#order-list',totalRow: true,limit:5000,page:false
                            , cols: [[
                                {type:'numbers',title:'序号',width:45}
                                ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', totalRowText: '合计'}
                                ,{field:'orderNo',title:'订单号',width:120,align:'center'}
                                ,{field:'customerName', width:260, title: '客户名称'}
                                ,{field:'orderDate',title:'出口日期',width:120,align:'center'}
                                ,{field:'clientNo',title:'客户单号',width:120,align:'center'}
                                ,{field:'decTotalPrice',title:'出口金额',width:120,align:'money', totalRow: true}
                                ,{field:'currencyName',title:'币制',width:80,align:'center'}
                                ,{field:'accountValue',title:'认领金额',width:120,align:'money', totalRow: true}
                                ,{field:'settlementValue1',title:'人民币金额',width:120,align:'money', totalRow: true}
                            ]]
                            , data: res.data
                        });
                        formatTableData();
                    }
                });
            }
        };
    exports('detailTemp', detailTemp);
});