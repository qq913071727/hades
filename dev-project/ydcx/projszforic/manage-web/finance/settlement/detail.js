layui.config({base: '/finance/settlement/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#settlementAccountDetail').load(rurl('finance/settlement/detail-temp.html'),function () {
        detailTemp.render(id,'');
    });
});