layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/receiptAccount/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'operation', width:60, title: '操作',templet:'#taskTemp',align:'center', fixed: true}
                ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                ,{field:'docNo', width:126, title: '收款单号',templet:'#docNoTemp'}
                ,{field:'accountDate', width:120, title: '收款时间'}
                ,{field:'customerName', width:260, title: '结算单位',templet:'#customerNameTemp'}
                ,{field:'accountValue', width:100, title: '收款金额',align:'money'}
                ,{field:'writeValue', width:100, title: '核销金额',align:'money'}
                ,{field:'receivingModeDesc', width:80, title: '收款方式',align:'center'}
                ,{field:'receivingBank', width:280, title: '收款行'}
                ,{field:'createBy', width:80, title: '制单人',align:'center'}
                ,{field:'memo',title: '备注'}
            ]]
        },listQueryDone:function(){
            formatTableData();
        }
    });
});
function showDetail(id) {
    openwin(rurl('finance/receiptAccount/detail.html?id='+id),'收款详情',{area: ['0', '0'],shadeClose:true});
}
function exportExcel (o) {
    show_loading();
    $.post(rurl(o.url),$('#queryForm').serialize(),function (res)
    {if(isSuccessWarn(res)){
        exportFile(res.data);
    }})
}
function removeReceipt(o) {
    let data = getCheckDatas();
    if(data.length==1){
        let item = data[0];
        delete_inquiry('当前操作不可回退，是否确定要删除？',o.url,{'id':item.id},function (res) {
            show_success('删除成功');
            reloadList();
        });
    }else{
        show_error('请选择一条您要删除的收款单信息');
    }
}