layui.config({base: '/finance/receiptAccount/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#receiptAccountDetail').load(rurl('finance/receiptAccount/detail-temp.html'),function () {
        detailTemp.render(id,'');
    });
});
