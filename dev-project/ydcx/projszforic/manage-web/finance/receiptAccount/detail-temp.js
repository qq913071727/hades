layui.define(['layer','laytpl','table'], function(exports){
    var layer = layui.layer,laytpl=layui.laytpl,table=layui.table,
        detailTemp = {
            render:function (id,operation) {
                $.get(rurl('scm/receiptAccount/info'),{'id':id},function (res) {
                    if(isSuccessWarnClose(res)){
                        detailTemp.renderData(res.data);
                    }
                });
            },
            renderData(data){
                var mainData = data.receiptAccount,members = data.members;
                laytpl($('#receiptAccountViewTemp').html()).render(mainData,function (html) {
                    $('#receiptAccountView').html(html);
                });

                //收款明细信息
                table.render({
                    elem: '#member-list',limit:5000,page:false,totalRow: true
                    ,cols: [[
                        {type:'numbers',title:'序号',width:45, totalRowText: '合计'}
                        ,{field:'docNo',title:'核销单据',width:140}
                        ,{field:'expenseSubjectName',title:'费用科目',width:120}
                        ,{field:'accountValue',title:'核销金额',width:120,align:'money', totalRow: true}
                        ,{field:'writeOffTypeDesc',title:'核销类型',width:140}
                        ,{field:'operation',title:'操作人',width:140}
                        ,{field:'dateCreated',title:'创建时间',width:140}
                    ]]
                    , data: members
                });
                formatTableData();
            }
        };
    exports('detailTemp', detailTemp);
});
