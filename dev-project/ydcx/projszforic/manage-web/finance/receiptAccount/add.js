layui.use(['layer','form'], function() {
    var layer = layui.layer, form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    form.val('form',{'accountDate':getNowFormatTime()});
    form.on('submit(save-btn)', function(data){
        formSub('scm/receiptAccount/register',function (res) {
            parent.reloadList();
            closeThisWin('登记成功');
        });
        return false;
    });
});
function fullCustomerData(cus) {
    $('#payer').val(cus.name);
}