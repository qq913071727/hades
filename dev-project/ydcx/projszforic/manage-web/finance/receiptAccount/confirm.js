layui.use(['layer','form','laytpl'], function() {
    var layer = layui.layer, form = layui.form,laytpl=layui.laytpl;
    var id = getUrlParam("id");
    show_loading();
    $.post(rurl('scm/receiptAccount/detail'),{'id':id,'operation':'receipt_confirm'},function (res) {
       if(isSuccessWarnClose(res)){
           laytpl($('#detailTemp').html()).render(res.data,function (html) {
               $('#detailTempView').html(html);
           });

           var $operationTaskBtn = $('.operation-task-btn'),
               taskArg = {'operation':'receipt_confirm','documentId':id,'documentClass':'ReceiptAccount','newStatusCode':''};
           $operationTaskBtn.click(function () {
               var $that = $(this),needMemo = $that.attr('need-memo');
               if(!isEmpty(needMemo)){
                   var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
                   taskArg['memo'] = $memo.val();
               }else{
                   taskArg['memo'] = $('#taskMemo').val();
               }
               taskArg['newStatusCode'] = $that.attr('status-code');

               show_loading();
               $.post(rurl('scm/receiptAccount/confirm'),taskArg,function (bres) {
                   if(isSuccess(bres)){
                       parent.reloadList();
                       closeThisWin('操作成功');
                   }
               });
           });
       }
    });
    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
});