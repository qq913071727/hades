layui.config({base: '/finance/expPaymentAccount/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','laytpl'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,laytpl=layui.laytpl;
    var id = getUrlParam("id");
    $('#paymentDetail').load(rurl('finance/expPaymentAccount/detail-temp.html'),function () {
        detailTemp.render(id,'exp_payment_account_confirm');

        var taskArg = {'operation':'exp_payment_account_confirm','documentId':id,'documentClass':'ExpPaymentAccount','newStatusCode':'confirmBank'};
        $('#confirm').click(function () {
            taskArg['memo'] = $('#taskMemo').val();
            if(isEmpty(taskArg['memo'])){
                show_error('请填写意见');
                $('#taskMemo').focus();
                return;
            }
            show_loading();
            $.post(rurl('scm/expPaymentAccount/confirm'),taskArg,function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
        // 删除
        $('#toVoid').click(function () {
            delete_inquiry('是否确定要删除付款单信息？','scm/expPaymentAccount/remove',{'id':id},function () {
                parent.reloadList();
                closeThisWin('操作成功');
            });
        });
    });
    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
});