layui.config({base: '/finance/expPaymentAccount/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','form'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,form=layui.form;
    var id = getUrlParam("id"),fn = {
        initSubject:function () {
            show_loading();
            $.get('/scm/overseasReceivingAccount/subjectSelect?first=mainland',function (res) {
                if(isSuccess(res)){
                    var $subjectId = $('#subjectStrId');
                    $.each(res.data,function (i,o) {
                        $subjectId.append('<option value="'+o.id+'_'+o.type+'">'+o.name+'</option>');
                    });
                    form.render('select');
                    fn.renderOverseasBank();
                }
            });
        },
        renderOverseasBank:function () {
            var params = {
                subjectType:$('#subjectStrId').val().split('_')[1],
                subjectId:$('#subjectStrId').val().split('_')[0],
                subjectName:$('#subjectStrId option:selected').text()
            };
            $('#subjectType').val(params.subjectType);
            $('#subjectId').val(params.subjectId);
            var bankUrl = '/scm/subjectBank/list';
            if('abroad'==params.subjectType){
                bankUrl = '/scm/subjectOverseasBank/selectByOverseasId'
            }
            show_loading();
            var $overseasBankId=$('#subjectBank');
            $overseasBankId.get(0).length = 0;
            $.post(bankUrl,{overseasId:params.subjectId},function (res) {
                if(isSuccess(res)){
                    $.each(res.data,function (i,o) {
                        $overseasBankId.append('<option value="'+o.id+'">'+o.name+'（'+o.account+'）</option>');
                    });
                    form.render('select');
                }
            });
        }
    };
    form.on('select(subjectStrId)',function (data) {
        fn.renderOverseasBank();
    });
    $('#paymentDetail').load(rurl('finance/expPaymentAccount/detail-temp.html'),function(){
        detailTemp.render(id,'exp_payment_account_bank');
        $('#id').val(id);
        fn.initSubject();
    });
    $('#backConfirm').click(function () {
        var taskArg = {'operation':'exp_payment_account_bank','documentId':id,'documentClass':'ExpPaymentAccount','newStatusCode':'waitConfirm'};
        layer.prompt({formType:2,maxlength: 500,title:'请填写退回原因'},function(val, index){
            show_loading();
            taskArg['memo'] = val;
            $.post(rurl('scm/expPaymentAccount/backConfirm'),taskArg,function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
    });
    $('#confirmBank').click(function () {
        if(checkedForm($('#operationForm'))){
            show_loading();
            $.post('/scm/expPaymentAccount/confirmBank',$('#operationForm').serialize(),function (res) {
               if(isSuccess(res)){
                   parent.reloadList();
                   closeThisWin('操作成功');
               }
            });
        }
    });
});
