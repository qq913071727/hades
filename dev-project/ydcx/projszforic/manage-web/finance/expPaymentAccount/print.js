layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer,laytpl=layui.laytpl,form=layui.form
        ,id = getUrlParam("id"),autoChapter = true,$chapterArea=$('.chapterArea');
    show_loading();
    $.get(rurl('scm/expPaymentAccount/getPrintExpPaymentAccountData?id=' + id),function (res) {
        if(isSuccessWarn(res)){
            var data = res.data;
            laytpl($('#conTemp').html()).render(data,function (html) {
                $('#conTempView').html(html);
            });
        }
    });
});
