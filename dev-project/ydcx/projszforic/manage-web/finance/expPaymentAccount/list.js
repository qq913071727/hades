layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/expPaymentAccount/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'operation', width:60, title: '操作',templet:'#taskTemp',align:'center', fixed: true}
                ,{field:'statusName', width:100, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                ,{field:'docNo', width:110, title: '付款单号',templet:'#docNoTemp'}
                ,{field:'orderNo', width:130, title: '订单号'}
                ,{field:'clientNo', width:130, title: '客户单号'}
                ,{field:'customerName', width:200, title: '客户名称',templet:'#customerNameTemp'}
                ,{field:'accountValue', width:100, title: '付款金额',align:'money'}
                ,{field:'dateCreated', width:120, title: '申请时间'}
                ,{field:'claimPaymentDate', width:120, title: '要求付款时间'}
                ,{field:'actualPaymentDate', width:120, title: '实际付款时间'}
                ,{field:'payeeName', width:180, title: '收款方'}
                ,{field:'payeeBank', width:200, title: '收款银行'}
                ,{field:'payeeBankNo', width:180, title: '收款账号'}
                ,{field:'subjectName', width:260, title: '付款主体'}
                ,{field:'subjectBank', width:200, title: '付款银行'}
                ,{field:'subjectBankNo', width:180, title: '付款账号'}
                ,{field:'paymentMemo', width:110,fixed: 'right', title: '付款水单',templet:'#paymentMemoTemp'}
            ]]
        },listQueryDone:function(){
            formatTableData();
            initFileSize();
        }
    });
});
function showDetail(id) {
    openwin(rurl('finance/expPaymentAccount/detail.html?id='+id),'出口付款详情',{area: ['0', '0'],shadeClose:true});
}
