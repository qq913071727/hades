layui.config({base: '/finance/expPaymentAccount/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#expPaymentAccountDetail').load(rurl('finance/expPaymentAccount/detail-temp.html'),function () {
        detailTemp.render(id,'');
    });
});