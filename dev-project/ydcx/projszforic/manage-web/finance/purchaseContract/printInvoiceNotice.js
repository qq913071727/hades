layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer,laytpl=layui.laytpl,form=layui.form
        ,expOrderNo = getUrlParam("expOrderNo"),autoChapter = true,$chapterArea=$('.chapterArea');
    show_loading();
    $.get(rurl('scm/purchaseContract/obtainExpInvoiceNotificationData?expOrderNo=' + expOrderNo),function (res) {
        if(isSuccessWarn(res)){
            var data = res.data;
            var tquantity = 0.0,ttotalPrice=0.0,tsettlementAccountCny=0.0,tpurchaseContractAmount=0.0;
            $.each(data.members || [],function (i,o) {
                tquantity += o.quantity;
                ttotalPrice += o.expTotalPrice;
                tsettlementAccountCny += o.settlementAccountCny;
                tpurchaseContractAmount += o.purchaseContractAmount;
            });
            data['tquantity'] = Math.round(tquantity*100)/100;
            data['ttotalPrice'] = Math.round(ttotalPrice*100)/100;
            data['tsettlementAccountCny'] = Math.round(tsettlementAccountCny*100)/100;
            data['tpurchaseContractAmount'] = Math.round(tpurchaseContractAmount*100)/100;
            laytpl($('#conTemp').html()).render(data,function (html) {
                $('#conTempView').html(html);
            });
        }
    });
});
