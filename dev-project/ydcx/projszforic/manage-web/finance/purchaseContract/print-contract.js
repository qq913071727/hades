layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer,laytpl=layui.laytpl,form=layui.form
        ,id = getUrlParam("id"),autoChapter = true,$chapterArea=$('.chapterArea');
    show_loading();
    var contrNo = "";
    $.post(rurl('scm/purchaseContract/detail'),{'id':id},function (res) {
        if(isSuccessWarn(res)){
            var data = res.data;
            contrNo = data.purchaseContract.customerOrderNo;
            var tquantity = 0.0,ttotalPrice=0.0;
            $.each(data.members || [],function (i,o) {
                tquantity += o.quantity;
                ttotalPrice += o.totalPrice;
            });
            data['tquantity'] = Math.round(tquantity*100)/100;
            data['ttotalPrice'] = Math.round(ttotalPrice*100)/100;
            laytpl($('#conTemp').html()).render(data,function (html) {
                $('#conTempView').html(html);
                $chapterArea=$('.chapterArea');
            });
        }
    });

    $("#print").click(function () {
        top.document.title = contrNo + "(采购合同)";
        setTimeout(function () {
            top.document.title = "";
        },2000);
        window.print();
    });

    $('#exportPdf').click(function () {
        show_loading();
        $.get(rurl('scm/purchaseContract/exportPdf'),{'id':id},function (res) {
            if(isSuccessWarn(res)){
                exportFile(res.data);
            }
        });
    });

});
