layui.use(['layer','laytpl','form'], function() {
    var layer = layui.layer,laytpl=layui.laytpl,form=layui.form
        ,id = getUrlParam("id"),autoChapter = true,$chapterArea=$('.chapterArea');
    show_loading();
    var contrNo = "";
    $.post(rurl('scm/purchaseContract/detail'),{'id':id},function (res) {
        if(isSuccessWarn(res)){
            var data = res.data;
            contrNo = data.purchaseContract.customerOrderNo;
            var tquantity = 0.0,ttotalPrice=0.0,tnetWeight = 0.0,tgrossWeight = 0.0,packNo = 0;
            $.each(data.members || [],function (i,o) {
                tquantity += o.quantity;
                ttotalPrice += o.totalPrice;
                tnetWeight += o.netWeight;
                tgrossWeight += o.grossWeight;
                packNo += (null == o.cartonNum ? 0 : o.cartonNum);
            });
            data['tquantity'] = Math.round(tquantity*100)/100;
            data['ttotalPrice'] = Math.round(ttotalPrice*100)/100;
            data['tnetWeight'] = Math.round(tnetWeight*10000)/10000;
            data['tgrossWeight'] = Math.round(tnetWeight*10000)/10000;
            data['packNo'] = packNo;
            laytpl($('#conTemp').html()).render(data,function (html) {
                $('#conTempView').html(html);
                $chapterArea=$('.chapterArea');
            });
        }
    });

    $("#print").click(function () {
        top.document.title = contrNo + "(送货单)";
        setTimeout(function () {
            top.document.title = "";
        },2000);
        window.print();
    });

    $('#exportPdf').click(function () {
        show_loading();
        $.get(rurl('scm/purchaseContract/exportDeliveryPdf'),{'id':id},function (res) {
            if(isSuccessWarn(res)){
                exportFile(res.data);
            }
        });
    });

});
