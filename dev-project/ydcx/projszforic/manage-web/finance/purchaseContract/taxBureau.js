layui.config({base: '/finance/purchaseContract/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','form'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,form=layui.form;
    var id = getUrlParam("id"),fn = {
        showArea:function(){
            var formData = $('#operationForm').serializeJson();
            $('#isCorrespondenceArea,#replyStatusArea').hide();
            if(formData.isCorrespondence == 'true'){
                $('#isCorrespondenceArea').show();
            }
            if(formData.replyStatus == '已回函'){
                $('#replyStatusArea').show();
            }
        },
        save:function () {
            show_loading();
            $.post('/scm/purchaseContract/taxBureauRegister',$('#operationForm').serialize(),function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        }
    };
    form.on('radio(showArea)',function () {
        fn.showArea();
    });
    $('#confirmTaxBureau').click(function () {
        fn.save();
    });
    $('#purchaseContractDetail').load(rurl('finance/purchaseContract/detail-temp.html'),function(){
        detailTemp.render(id,'purchase_tax_bureau',function (data) {
            data.purchaseContract.isCorrespondence = data.purchaseContract.isCorrespondence+'';
            form.val('operationForm',data.purchaseContract);
            fn.showArea();
        });
    });
});
