layui.config({base: '/finance/purchaseContract/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#purchaseContractDetail').load(rurl('finance/purchaseContract/detail-temp.html'),function () {
        detailTemp.render(id,'');
    });
});