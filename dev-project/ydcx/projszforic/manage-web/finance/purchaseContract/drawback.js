layui.use(['layer','form','table'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table;
    var id = getUrlParam('id'),fn = {
        initDrawback: function () {
            show_loading();
            $.get('/scm/purchaseContract/initDrawback',{id:id}, function (res) {
                if (isSuccessWarnClose(res)) {
                    fn.fullData(res.data);
                }
            });
        },
        fullData(data){
            var drawback = data.drawback,orders = data.orders;
            form.val('form',drawback);
            $('#purchaseId').val(id);
            table.render({
                elem: '#purchase-contract-list',totalRow: true,limit:5000,page:false
                , cols: [[
                    {type:'numbers',title:'序号',width:45}
                    ,{field:'orderDate',title:'出口日期',width:100,align:'center'}
                    ,{field:'orderNo',title:'订单号',width:110,align:'center'}
                    ,{field:'orderTotalPrice',title:'订单金额',width:100,align:'money', totalRow: true}
                    ,{field:'orderCurrencyName',title:'币制',width:80,align:'center'}
                    ,{field:'purchaseNo',title:'采购合同号',width:110,align:'center'}
                    ,{field:'purchaseTotalPrice',title:'采购合同金额',width:100,align:'money', totalRow: true}
                    ,{field:'invoiceValue',title:'收票金额',width:100,align:'money', totalRow: true}
                    ,{field:'settlementValue',title:'结汇金额',width:100,align:'money', totalRow: true}
                    ,{field:'estimatedPayment',title:'预估货款',width:100,align:'money', totalRow: true}
                    ,{field:'agencyFee',title:'代理费金额',width:100,align:'money', totalRow: true}
                    ,{field:'deductionValue',title:'代垫费金额',width:100,align:'money', totalRow: true}
                    ,{field:'actualDrawbackValue',title:'应退税金额',width:100,align:'money', totalRow: true}
                    ,{field:'alreadyDrawbackValue',title:'已退税金额',width:100,align:'money', totalRow: true}
                    ,{field:'drawbackValue',title:'本次退税金额',width:100,align:'money', totalRow: true}
                ]]
                , data: orders
            });
            formatTableData();
        }
    };
    fn.initDrawback();
    $('.auto-customer-bank').autocomplete({
        source: function (request, response) {
            $.post(rurl('scm/customerBank/page'),{customerId:$('#customerId').val(),name: trim(request.term)},function (res) {
                response($.map(res.data, function (item) {return {label: item.name+'('+item.account+')', value: item.name, data: item}}));
            });
        }, minLength: 0,autoFocus:true, delay: 300,
        select: function( event, ui ) {
            var data = ui.item.data;
            form.val("form",{'payeeBankNo':data.account});
        }
    }).focus(function () {
        $(this).select().autocomplete("search");
    });
    $('#billie').blur(function () {
        show_loading();
        $.post('/scm/drawback/recalculate',$('#form').serialize(),function (res) {
            if(isSuccessWarn(res)){
                fn.fullData(res.data);
            }
        });
    });
    form.on('submit(save-btn)', function(data){
        if(checkedForm($('#form'))){
            formSubJson('scm/drawback/save',function (res) {
                parent.reloadList();
                closeThisWin('保存成功');
            });
        }
        return false;
    });
});
