layui.define(['layer','laytpl','element','table'], function(exports){
    var layer = layui.layer,laytpl=layui.laytpl,table=layui.table,
        detailTemp = {
            render:function (id,operation,fn) {
                $.post(rurl('scm/purchaseContract/detail'),{'id':id,'operation':operation},function (res) {
                    if(isSuccessWarnClose(res)){
                        detailTemp.renderData(res.data);
                        if(fn){
                            fn(res.data);
                        }
                    }
                });
            },
            renderData(data){
                let mainData = data.purchaseContract,members = data.members,costMembers = data.costMembers;
                laytpl($('#purchaseContractViewTemp').html()).render(mainData,function (html) {
                    $('#purchaseContractView').html(html);
                });

                //合同明细信息
                table.render({
                    elem: '#member-list',limit:5000,page:false,totalRow: true
                    ,cols: [[
                        {type:'numbers',title:'序号',width:45}
                        ,{field:'name',title:'物料名称',width:160, totalRowText: '合计'}
                        ,{field:'model',title:'物料型号',width:160}
                        ,{field:'brand',title:'物料品牌',width:120}
                        ,{field:'quantity',title:'数量',width:90,align:'money', totalRow: true}
                        ,{field:'unitName',title:'单位',width:80,align:'center'}
                        ,{field:'addedTaxRate',title:'增值税率',width:70,align:'right'}
                        ,{field:'taxRebateRate',title:'退税率',width:70,align:'right'}
                        ,{field:'agentFee',title:'代理费',width:90,align:'money', totalRow: true}
                        ,{field:'matFee',title:'代垫费',width:90,align:'money', totalRow: true}
                        ,{field:'differenceVal',title:'票差调整',width:90,align:'money', totalRow: true}
                        ,{field:'unitPrice',title:'采购单价',width:90,align:'right'}
                        ,{field:'totalPrice',title:'采购总价',width:100,align:'money', totalRow: true}
                    ]]
                    , data: members
                });

                //发票费用明细
                table.render({
                    elem: '#cost-list',limit:5000,page:false,totalRow: true
                    ,cols: [[
                        {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
                        ,{field: 'expOrderNo', title: '订单号', width: 150}
                        ,{field: 'expenseSubjectName', title: '科目名称', width: 150}
                        ,{field: 'receAmount', title: '费用金额（元）', width: 120,align:'money', totalRow: true}
                        ,{field: 'acceAmount', title: '已收金额（元）', width: 120,align:'money', totalRow: true}
                        ,{field: 'collectionSourceDesc', title: '支付方式', width: 120, align:'center'}
                        ,{field: 'operator', title: '操作人', width: 80}
                        ,{field: 'memo', title: '费用说明'}
                    ]]
                    , data: costMembers
                });
            }
        };
    exports('detailTemp', detailTemp);
});