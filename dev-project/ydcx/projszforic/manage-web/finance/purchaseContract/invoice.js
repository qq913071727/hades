layui.config({base: '/finance/purchaseContract/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','laytpl'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,laytpl=layui.laytpl;
    var id = getUrlParam("id");
    $('#orderDetail').load(rurl('finance/purchaseContract/detail-temp.html'),function () {
        var totalPrice = 0.0;
        detailTemp.render(id,'purchase_contract_invoice',function (data) {
            var purchaseContract = data.purchaseContract;
            $('#invoiceDate').val(purchaseContract.invoiceDate || getNowFormatDate());
            $('#invoiceVal').val(purchaseContract.invoiceVal || purchaseContract.totalPrice);
            $('#invoiceNo').val(purchaseContract.invoiceNo);
            totalPrice = purchaseContract.totalPrice;
        });
        $('#purchaseFile').attr({'doc-id':id});
        initFileSize();
        $('#contractId').val(id);
        var confirmInvoice = function(){
            show_loading();
            $.post('/scm/purchaseContract/confirmInvoice',$('#operationForm').serialize(),function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        }
        $('#confirmInvoice').click(function () {
            if(checkedForm($('#operationForm'))){
                var invoiceVal = Math.round(parseFloat($('#invoiceVal').val())*100)/100;
                if(totalPrice!=invoiceVal){
                    prompt_inquiry('采购合同总价：'+formatCurrency(totalPrice)+' 收票金额：'+formatCurrency(invoiceVal)+' 差额：'+(Math.round((totalPrice-invoiceVal)*100)/100)+'是否确定收票完成？',function () {
                        confirmInvoice();
                    });
                }else{
                    confirmInvoice();
                }
            }
        });
    });
});