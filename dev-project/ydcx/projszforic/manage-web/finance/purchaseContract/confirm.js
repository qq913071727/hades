layui.config({base: '/finance/purchaseContract/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','laytpl'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,laytpl=layui.laytpl;
    var id = getUrlParam("id");
    $('#orderDetail').load(rurl('finance/purchaseContract/detail-temp.html'),function () {
        detailTemp.render(id,'purchase_contract_confirm');
        var $operationTaskBtn = $('.operation-task-btn'),taskArg = {'operation':'purchase_contract_confirm','documentId':id,'documentClass':'PurchaseContract','newStatusCode':''};
        $operationTaskBtn.click(function (){
            var $that = $(this),needMemo = $that.attr('need-memo');
            if(!isEmpty(needMemo)){
                var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
                taskArg['memo'] = $memo.val();
            }else{
                taskArg['memo'] = $('#taskMemo').val();
            }
            taskArg['newStatusCode'] = $that.attr('status-code');
            show_loading();
            $.post(rurl('scm/purchaseContract/confirm'),taskArg,function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
        $('#deleteContract').click(function () {
            delete_inquiry('您确定要删除此合同信息？','scm/purchaseContract/delete',{'id':id},function () {
                parent.reloadList();
                closeThisWin('操作成功');
            });
        });
    });
    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
});