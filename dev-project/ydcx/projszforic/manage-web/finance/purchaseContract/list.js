layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/purchaseContract/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'task', width:50, title: '任务',align:'center',toolbar:'#taskTemp', fixed: true}
                ,{field:'statusName', width:80, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                ,{field:'docNo', width:130, title: '合同编号',templet:'#docNoTemp',fixed: true}
                ,{field:'orderNo', width:120, title: '订单编号',templet:'#orderTemp',fixed: true}
                ,{field:'customerOrderNo', width:120, title: '客户单号'}
                ,{field:'signingDate', width:90, title: '签订日期',align:'center'}
                ,{field:'dateCreated', width:90, title: '创建日期',align:'center'}
                ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                ,{field:'totalPrice', width:90, title: '合同金额',align:'money'}
                ,{field:'exchangeRate', width:70, title: '汇率',align:'right'}
                ,{field:'differenceVal', width:90, title: '票差调整金额',align:'money'}
                ,{field:'invoiceVal', width:90, title: '收票金额',align:'money'}
                ,{field:'invoiceDate', width:90, title: '收票日期',align:'center'}
                ,{field:'actualDrawbackValue', width:90, title: '应退税金额',align:'money'}
                ,{field:'alreadyDrawbackValue', width:90, title: '已退税金额',align:'money'}
                ,{field:'drawbackStatus', width:90, title: '退税状态',align:'center',templet:'#drawbackStatusTemp'}
                ,{field:'sellerName', width:260, title: '卖方',templet:'#sellerNameTemp'}
                ,{field:'sellerBank', width:240, title: '卖方银行'}
                ,{field:'sellerBankNo', width:200, title: '卖方银行账号'}
                ,{field:'buyerName', width:260, title: '买方',templet:'#buyerNameTemp'}
                ,{field:'buyerBank', width:240, title: '买方银行'}
                ,{field:'buyerBankNo', width:200, title: '买方银行账号'}
                ,{field:'purchaseFile', width:100, title: '附件管理',templet:'#purchaseFileTemp',align:'center',fixed:'right'}
            ]]
        },listQueryDone:function(){
            formatTableData();
            initFileSize();
        }
    });
});
function showDetail(id) {
    openwin(rurl('finance/purchaseContract/detail.html?id='+id),'采购合同详情',{area: ['0', '0'],shadeClose:true});
}

function printInvoiceNotice() {
    let datas = getDatas();
    if(null == datas || datas.length != 1) {show_error("请单选一条采购合同数据");return;}
    openwin('/finance/purchaseContract/printInvoiceNotice.html?expOrderNo='+datas[0].orderNo,'打印开票通知书',{area: ['1000px', '0px']});
}

function sendMail() {
    let datas = getDatas();
    if(null == datas || datas.length != 1) {show_error("请单选一条采购合同数据");return;}
    var id = datas[0].id;
    $.post('/scm/purchaseContract/getDefaultMail',{'customerName':datas[0].customerName},function (res) {
        if(isSuccess(res)){
            showSendMailDialog(id,res.data);
        }
    });


}

function showSendMailDialog(id,mails) {
    layui.layer.open({
        title: '您确认要将此采购合同发送邮件给客户?',
        area: ['400px', '310px'],
        btnAlign: 'c',
        closeBtn:'1',
        type: 1,
        content: '<div style="padding-left: 10px;">' +
            '<blockquote class="layui-elem-quote">温馨提示：邮件发送后将无法撤回；填写的邮箱将自动下次带入</blockquote>' +
            '<textarea maxlength="500" placeholder="请输入客户邮箱，多个请用中英文逗号隔开" name="mails" id="mails" style="width:350px;height:110px;">' + mails + '</textarea>'
            + '</div>',
        btn:['确认','取消'],
        yes: function (indexT, layero) {
            var mails = $('#mails').val();
            if(isEmpty(mails)) {
                layer.msg("请填写客户邮箱，如果有多个邮箱，请用中英文逗号隔开");
                return false;
            }
            $.post(rurl('scm/purchaseContract/sendMail'),{'id':id,'receivers':mails},function (res) {
                if(isSuccess(res)){
                    show_success('发送成功');
                    layer.close(indexT);
                }
            });
        },
        no:function(indexT) {
            layer.close(indexT);
            return false;
        }
    });
}
