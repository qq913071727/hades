layui.use(['layer','form','table'], function() {
    var layer = layui.layer, form = layui.form,table = layui.table;
    var id = getUrlParam('id'),fn = {
        generateDocNo:function(){
            // show_loading();
            // $.post(rurl('scm/serialNumber/generateDocNo'),{'type':'purchase_contract'},function (res) {
            //     if(isSuccess(res)){
            //         $('#docNo').val(res.data);
            //     }
            // });
        },
        obtainCustomerExpDifferenceVal:function (customerId) {
            $.post('/scm/expReport/obtainCustomerExpDifferenceVal',{customerId:customerId},function (res) {
                if(isSuccess(res)){
                    $('#totalDifferenceVal').val(res.data);
                }
            });
        }
        ,
        fullData:function(data){
            var main = data.main,members = data.members,orderCosts = data.orderCosts;
            main.docNo = main.orderNo;
            form.val('baseForm',main);
            fn.obtainCustomerExpDifferenceVal(main.customerId);
            if(main.isSettleAccount == true){
                $('#exchangeRate').hide();
                $('#exchangeRateDisabled').show();
            }
            table.render({
                elem: '#memberTable' ,totalRow: true,limit:5000,page:false
                , cols: [[
                    {type:'numbers',title:'序号',width:45}
                    ,{field:'name',title:'物料名称',width:160, totalRowText: '合计'}
                    ,{field:'model',title:'物料型号',width:160}
                    ,{field:'brand',title:'物料品牌',width:120}
                    ,{field:'quantity',title:'数量',width:90,align:'money', totalRow: true}
                    ,{field:'unitName',title:'单位',width:80,align:'center'}
                    ,{field:'decTotalPrice',title:'报关总价',width:100,align:'money', totalRow: true}
                    ,{field:'addedTaxRate',title:'增值税率',width:70,align:'right'}
                    ,{field:'taxRebateRate',title:'退税率',width:70,align:'right'}
                    ,{field:'agentFee',title:'代理费',width:90,align:'money', totalRow: true}
                    ,{field:'matFee',title:'代垫费',width:90,align:'money', totalRow: true}
                    ,{field:'settlementValueCny',title:'已结汇人民币',width:90,align:'money', totalRow: true}
                    ,{field:'differenceVal',title:'票差调整',width:90,align:'money', totalRow: true}
                    ,{field:'unitPrice',title:'采购单价',width:90,align:'right'}
                    ,{field:'totalPrice',title:'采购总价',width:100,align:'money', totalRow: true}
                ]], data: members
            });
            table.render({
                elem: '#costTable',totalRow: true,limit:5000,page:false
                ,cols: [[
                    {type: 'numbers', title: '序号', width: 50,align:'center',fixed:true}
                    ,{field: 'expOrderNo', title: '订单号', width: 150}
                    ,{field: 'expenseSubjectName', title: '科目名称', width: 150}
                    ,{field: 'receAmount', title: '费用金额（元）', width: 120,align:'money', totalRow: true}
                    ,{field: 'acceAmount', title: '已收金额（元）', width: 120,align:'money', totalRow: true}
                    ,{field: 'collectionSourceDesc', title: '支付方式', width: 120, align:'center'}
                    ,{field: 'operator', title: '操作人', width: 80}
                    ,{field: 'memo', title: '费用说明'}
                ]]
                ,data: orderCosts
            });
            formatTableData();
        }

    };
    show_loading();
    $.post('/scm/purchaseContract/createByOrderId',{id:id},function (res) {
       if(isSuccessWarnClose(res)){
           fn.generateDocNo();
           fn.fullData(res.data);
       }
    });
    $('.save-btn').click(function () {
        if(checkedForm($('#baseForm'))){
            show_loading();
            $.post('/scm/purchaseContract/save',$('#baseForm').serialize(),function (res) {
                if(isSuccessWarn(res)){
                    parent.reloadList();
                    closeThisWin('采购合同生成成功');
                }
            });
        }
    });
    $('#exchangeRate,#differenceVal').blur(function () {
        show_loading();
        $.post('/scm/purchaseContract/recalculate',$('#baseForm').serialize(),function (res) {
            if(isSuccessWarn(res)){
                fn.generateDocNo();
                fn.fullData(res.data);
            }
        });
    });
});
