layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listQuery:true,
        listCols:true,
        listQueryArg:{
            url:rurl('scm/transactionFlow/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                ,{field:'transactionNo', width:126, title: '交易单号',templet:'#docNoTemp'}
                ,{field:'transactionTypeDesc', width:150, title: '交易类型'}
                ,{field:'amount', width:140, title: '交易金额',align:'right',templet:'#amountTemp'}
                ,{field:'balance', width:140, title: '账户余额',align:'right',templet:'#balanceTemp'}
                ,{field:'dateCreated', width:150, title: '交易时间'}
                ,{field:'memo', title: '备注'}
            ]]
        },listQueryDone:function(){
            formatTableData();
        }
    });
});
