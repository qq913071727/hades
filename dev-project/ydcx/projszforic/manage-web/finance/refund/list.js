layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listCols:true,
        listQuery:true,
        listQueryArg:{
            url:rurl('scm/refundAccount/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'operation', width:60, title: '操作',templet:'#taskTemp',align:'center', fixed: true}
                ,{field:'statusName', width:100, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                ,{field:'docNo', width:126, title: '退款单号',templet:'#docNoTemp', fixed: true}
                ,{field:'payeeAccountName', width:260, title: '客户名称',templet:'#payeeAccountNameTemp'}
                ,{field:'dateCreated', width:120, title: '提交时间'}
                ,{field:'accountValue', width:100, title: '退款金额',align:'money'}
                ,{field:'payeeBankName', width:220, title: '收款银行'}
                ,{field:'payeeBankAccountNo', width:160, title: '收款账号'}
                ,{field:'bankReceipt', width:110, title: '退款水单',templet:'#bankReceiptTemp'}
                ,{field:'payerBankName', width:220, title: '付款行'}
                ,{field:'accountDate', width:120, title: '付款时间'}
            ]]
        },listQueryDone:function(){
            formatTableData(); initFileSize();
        }
    });
});
function showDetail(id) {
    openwin(rurl('finance/refund/detail.html?id='+id),'退款详情',{area: ['500', '500'],shadeClose:false});
}
function toVoidRefund() {
    let data = getCheckDatas();
    if(data.length==1){
        let item = data[0];
        layer.prompt({formType:2,maxlength: 500,title:'请填写作废原因'},function(val, index){
            show_loading();
            $.post(rurl('scm/refundAccount/toVoid'),{'documentId':item.id,'documentClass':'RefundAccount','newStatusCode':'notApprove','memo':val,'operation':'refund_to_void'},function (res) {
                if(isSuccess(res)){
                    show_success('作废成功');
                    reloadList();
                    layer.close(index);
                }
            });
        });
    }else{
        show_error('请选择一条您要作废的退款单信息');
    }
}
function removeRefund () {
    let data = getCheckDatas();
    if(data.length==1){
        let item = data[0];
        delete_inquiry('当前操作不可回退，是否确定要删除？','scm/refundAccount/remove',{'id':item.id},function (res) {
            show_success('删除成功');
            reloadList();
        });
    }else{
        show_error('请选择一条您要删除的退款单信息');
    }
}