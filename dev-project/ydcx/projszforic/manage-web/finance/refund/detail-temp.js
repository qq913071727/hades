layui.define(['layer','laytpl','element','table'], function(exports){
    var layer = layui.layer,laytpl=layui.laytpl,element=layui.element,table=layui.table,
        detailTemp = {
            render:function (id,operation,fn) {
                $.post(rurl('scm/refundAccount/detail'),{'id':id,'operation':operation},function (res) {
                   if(isSuccessWarnClose(res)){
                       detailTemp.renderData(res.data);
                       if(fn){
                           fn(res.data);
                       }
                   }
                });
            },
            renderData:function(data){
                laytpl($('#refundViewTemp').html()).render(data,function (html) {
                    $('#refundView').html(html);
                });
            }
        };
    exports('detailTemp', detailTemp);
});