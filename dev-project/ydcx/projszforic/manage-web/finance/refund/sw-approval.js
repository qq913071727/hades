layui.config({base: '/finance/refund/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','laytpl'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,laytpl=layui.laytpl;
    var id = getUrlParam("id");

    $('#refundDetail').load(rurl('finance/refund/detail-temp.html'),function(){
        detailTemp.render(id,'refund_audit');

        var $operationTaskBtn = $('.operation-task-btn'),
            taskArg = {'operation':'refund_audit','documentId':id,'documentClass':'RefundAccount','newStatusCode':''};
        $operationTaskBtn.click(function () {
            var $that = $(this),needMemo = $that.attr('need-memo');
            if(!isEmpty(needMemo)){
                var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
                taskArg['memo'] = $memo.val();
            }else{
                taskArg['memo'] = $('#taskMemo').val();
            }
            taskArg['newStatusCode'] = $that.attr('status-code');
            show_loading();
            $.post(rurl('scm/refundAccount/approve'),taskArg,function (bres) {
                if(isSuccess(bres)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
    });

    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });

});