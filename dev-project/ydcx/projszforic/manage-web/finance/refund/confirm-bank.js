layui.config({base: '/finance/refund/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','laytpl','form'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,laytpl=layui.laytpl,form = layui.form;
    var id = getUrlParam("id");

    $('#refundDetail').load(rurl('finance/refund/detail-temp.html'),function(){
        detailTemp.render(id,'refund_confirm_bank');

        var obtainPayerBank = function () {
            var bankObj = $('#payerBankId');
            bankObj.empty();
            $.get(rurl('scm/subjectBank/selectEnableBanks'),{},function (res) {
                if(res.data != null && res.data.length) {
                    $.each(res.data,function (i,o) {
                        bankObj.append('<option value="'+o.id+'">'+o.name+' ('+o.account+')</option>');
                    });
                }
                form.render('select');
            });
        };

        $('.sel-load').selLoad(function () {
            form.render('select');
            obtainPayerBank();
        });

        var $operationTaskBtn = $('.operation-task-btn'),
            taskArg = {'operation':'refund_confirm_bank','documentId':id,'documentClass':'RefundAccount','newStatusCode':''};
        $operationTaskBtn.click(function () {
            var $that = $(this),needMemo = $that.attr('need-memo');
            if(!isEmpty(needMemo)){
                var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
                taskArg['memo'] = $memo.val();
            }else{
                taskArg['memo'] = $('#taskMemo').val();
            }
            taskArg['newStatusCode'] = $that.attr('status-code');
            taskArg['payerBankId'] = $("#payerBankId").val();
            show_loading();
            $.post(rurl('scm/refundAccount/confirmBank'),taskArg,function (bres) {
                if(isSuccess(bres)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });

    });

    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });

});