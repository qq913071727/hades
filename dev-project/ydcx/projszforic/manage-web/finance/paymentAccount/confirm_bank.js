layui.config({base: '/finance/paymentAccount/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','form'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,form=layui.form;
    var id = getUrlParam("id");

    $('#paymentDetail').load(rurl('finance/paymentAccount/detail-temp.html'),function(){
        detailTemp.render(id,'payment_confirm_bank');
    });

    $('.sel-load').selLoad(function () {
        form.render('select');
        obtainPayerBank();
    });

    form.on("select(payerId)",function (data) {
        obtainPayerBank();
    });

    var obtainPayerBank = function () {
        var bankObj = $('#payerBankId');
        bankObj.empty();
        $.post(rurl('scm/subjectOverseasBank/selectByOverseasId'),{"overseasId":$('#payerId').val()},function (res) {
            $.each(res.data,function (i,o) {
                bankObj.append('<option value="'+o.id+'">'+o.name+' ('+o.account+')</option>');
            });
            form.render('select');
        });
    };
    form.val('operationForm',{'id':id});
    $('#confirmBank').click(function () {
        var $from = $('#operationForm');
        if(checkedForm($from)){
            show_loading();
            $.post(rurl('scm/paymentAccount/saveConfirmBank'),$from.serialize(),function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        }
    });
});
