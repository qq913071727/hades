layui.config({base: '/finance/paymentAccount/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','laytpl'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,laytpl=layui.laytpl;
    var id = getUrlParam("id");

    $('#paymentDetail').load(rurl('finance/paymentAccount/detail-temp.html'),function(){
        detailTemp.render(id,'payment_audit');

        var $operationTaskBtn = $('.operation-task-btn'),
            taskArg = {'operation':'payment_audit','documentId':id,'documentClass':'PaymentAccount','newStatusCode':''};
        $operationTaskBtn.click(function () {
            var $that = $(this),needMemo = $that.attr('need-memo');
            if(!isEmpty(needMemo)){
                var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
                taskArg['memo'] = $memo.val();
            }else{
                taskArg['memo'] = $('#taskMemo').val();
            }
            taskArg['newStatusCode'] = $that.attr('status-code');
            if(taskArg['newStatusCode']=='toVoid'){
                taskArg['operation'] = 'payment_invalid';
            }
            show_loading();
            $.post(rurl('scm/paymentAccount/processJump'),taskArg,function (bres) {
                if(isSuccess(bres)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
    });

    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
    
    $('#adjustRate').click(function () {
        openwin('finance/paymentAccount/adjust-rate.html?id='+id,'调整汇率',{area: ['760px', '330px']})
    });
    $('#adjustBankFee').click(function () {
        openwin('finance/paymentAccount/adjust-bank-fee.html?id='+id,'手续费调整',{area: ['760px', '330px']})
    });

    $.post(rurl('scm/impOrderCost/paymentCostInfo'),{'id':id},function (res) {
        if(isSuccessWarnClose(res)){
            //货款费用信息
            var  data = res.data;
            laytpl($('#costTemp').html()).render(data,function (html) {
                $('#costTempView').html(html);
            });
            if(data.isAdopt==true||(data.isAdopt==false&&data.fkAdopt==true)){
                $('#confirmPay').attr({'disabled':null}).removeClass('layui-btn-primary').removeClass('layui-disabled').addClass('layui-btn-normal');
            }
        }
    });
});
function adjustRateSuccess() {
    prompt_success('汇率调整成功，需要等待审核通过后生效',function () {
        parent.reloadList();
        closeThisWin();
    });
}
function adjustBankFeeSuccess() {
    prompt_success("手续费调整成功",function () {
        location.reload();
    });
}