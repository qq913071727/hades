layui.config({base: '/finance/paymentAccount/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','form'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,form=layui.form;
    var id = getUrlParam("id");

    $('#paymentDetail').load(rurl('finance/paymentAccount/detail-temp.html'),function(){
        detailTemp.render(id,'payment_confirm_pay',function (data) {
            var payment=data.paymentAccount;
            form.val('operationForm',{'id':id,'actualAccountValue':payment.accountValue,'actualCurrencyName':payment.currencyName});
        });

        $('#payMemo').attr({'doc-id':id});
        initFileSize();
    });

    autoCurrency();
    $('#confirmPaymen').click(function () {
        var $from = $('#operationForm');
        if(checkedForm($from)){
            show_loading();
            $.post(rurl('scm/paymentAccount/paymentCompleted'),$from.serialize(),function (res) {
                if(isSuccess(res)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        }
    });
});
