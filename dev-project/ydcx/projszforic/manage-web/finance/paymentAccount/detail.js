layui.config({base: '/finance/paymentAccount/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp;
    var id = getUrlParam("id");
    $('#paymentDetail').load(rurl('finance/paymentAccount/detail-temp.html'),function(){
        detailTemp.render(id,'');
    });
});
