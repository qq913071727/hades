layui.define(['layer','laytpl','element','table'], function(exports){
    var layer = layui.layer,laytpl=layui.laytpl,element=layui.element,table=layui.table,
        detailTemp = {
            render:function (id,operation,fn) {
                $.post(rurl('scm/paymentAccount/detail'),{'id':id,'operation':operation},function (res) {
                   if(isSuccessWarnClose(res)){
                       detailTemp.renderData(res.data);
                       if(fn){
                           fn(res.data);
                       }
                   }
                });
            },
            renderData:function(data){
                var payment=data.paymentAccount,members=data.members;
                laytpl($('#paymentViewTemp').html()).render(payment,function (html) {
                    $('#paymentView').html(html);
                });
                table.render({
                    elem: '#memberTable',totalRow: true,limit:1000,page:false
                    ,cols: [[
                        {type:'numbers',title:'序号',width:70,align:'center', totalRowText: '合计'}
                        ,{field:'statusDesc',title:'订单状态',width:100,align:'center',templet:'#orderStatusTemp'}
                        ,{field:'impOrderNo',title:'订单号',width:120}
                        ,{field:'orderDate',title:'订单日期',width:100,align:'center'}
                        ,{field:'supplierName',title:'供应商',width:260}
                        ,{field:'totalPrice',title:'订单金额',width:120,align:'money', totalRow: true}
                        ,{field:'accountValue',title:'付款金额',width:120,align:'money', totalRow: true}
                        ,{field:'accountValueCny',title:'人民币金额',width:120,align:'money', totalRow: true}
                    ]]
                    , data: members
                });
                formatTableData();
            }
        };
    exports('detailTemp', detailTemp);
});