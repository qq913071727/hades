layui.config({base: '/js/lib/'}).extend({operation: 'operation'}).use(['layer','operation','form'], function(){
    var layer = layui.layer,operation = layui.operation,form = layui.form;
    $('.sel-load').selLoad(function () {
        form.render('select');
    });
    operation.init({
        listCols:true,
        listQuery:true,
        listQueryArg:{
            url:rurl('scm/paymentAccount/list')
            ,cols: [[
                {checkbox: true, fixed: true}
                ,{field:'operation', width:60, title: '操作',templet:'#taskTemp',align:'center', fixed: true}
                ,{field:'statusName', width:100, title: '状态',align:'center',templet:'#statusTemp', fixed: true}
                ,{field:'docNo', width:118, title: '付款单号',templet:'#docNoTemp'}
                ,{field:'accountDate', width:120, title: '申请日期'}
                ,{field:'customerName', width:260, title: '客户名称',templet:'#customerNameTemp'}
                ,{field:'accountValue', width:90, title: '申请金额',align:'money'}
                ,{field:'currencyName', width:50, title: '币制',align:'center'}
                ,{field:'exchangeRate', width:55, title: '汇率',align:'right'}
                ,{field:'accountValueCny', width:95, title: '人民币金额',align:'money'}
                ,{field:'paymentTermDesc', width:60, title: '付款方式',align:'center'}
                ,{field:'counterFeeTypeDesc', width:100, title: '手续费承担方式'}
                ,{field:'bankFee', width:55, title: '手续费',align:'money'}
                ,{field:'payee', width:260, title: '收款人'}
                ,{field:'swiftCode', width:100, title: 'SWIFT CODE'}
                // ,{field:'memo', width:220, title: '付款要求'}
                ,{field:'impPayFile', width:100, title: '附件管理',templet:'#impPayFileTemp',align:'center'}
                ,{field:'payerBankName', width:260, title: '付款行'}
                ,{field:'realAccountDate', width:130, title: '付款时间'}
            ]]
        },listQueryDone:function(){
            formatTableData(); initFileSize();
        }
    });
});
function showDetail(id) {
    openwin(rurl('finance/paymentAccount/detail.html?id='+id),'付汇详情',{area: ['0', '0'],shadeClose:true});
}

/**
 * 导出付汇委托书
 * @param id
 */
function entrustExcel() {
    let ids = getIds();
    if(ids == null || ids.length != 1) {
        layer.msg("请单选一条记录");
        return;
    }
    $.get(rurl('scm/paymentAccount/entrustExcel'),{id:ids[0]},function (res) {
        if(isSuccess(res)){
            window.open('/scm/export/down?id=' + res.data);
        }
    });
}
//删除付款单
function removePayment(o) {
    let data = getCheckDatas();
    if(data.length==1){
        let item = data[0];
        delete_inquiry('当前操作不可回退，是否确定要删除？',o.url,{'id':item.id},function (res) {
            show_success('删除成功');
            reloadList();
        });
    }else{
        show_error('请选择一条您要删除的付款单信息');
    }
}
