layui.use(['layer','form','table'], function() {
    var layer = layui.layer, form = layui.form, table = layui.table;
    var id = getUrlParam('id');
    show_loading();
    $.post(rurl('scm/paymentAccount/detail'),{'id':id,'operation':'rate_adjust'},function (res) {
        if(isSuccessWarnClose(res)){
            var payment=res.data.paymentAccount;
            payment['adjustRate'] = payment.exchangeRate;
            payment['accountValue'] = formatCurrency(payment.accountValue)+' '+payment.currencyName;
            form.val('mainForm',payment);
        }
    });
    $('.save-btn').click(function () {
        if(checkedForm($('#mainForm'))){
            show_loading();
            $.post(rurl('scm/paymentAccount/adjustRate'),$('#mainForm').serialize(),function (res) {
                if(isSuccess(res)){
                    parent.adjustRateSuccess();
                    closeThisWin();
                }
            });
        }
    });
});