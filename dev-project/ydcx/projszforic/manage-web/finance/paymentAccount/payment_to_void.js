layui.config({base: '/finance/paymentAccount/'}).extend({detailTemp: 'detail-temp'}).use(['layer','detailTemp','laytpl'], function(){
    var layer = layui.layer,detailTemp=layui.detailTemp,laytpl=layui.laytpl;
    var id = getUrlParam("id");

    $('#paymentDetail').load(rurl('finance/paymentAccount/detail-temp.html'),function(){
        detailTemp.render(id,'payment_invalid');

        var $operationTaskBtn = $('.operation-task-btn'),
            taskArg = {'operation':'payment_invalid','documentId':id,'documentClass':'PaymentAccount','newStatusCode':'toVoid'};
        $operationTaskBtn.click(function () {
            var $that = $(this),needMemo = $that.attr('need-memo');
            if(!isEmpty(needMemo)){
                var $memo = $(needMemo);if(isEmpty($memo.val())){$memo.select();show_error($memo.attr('placeholder')||'必填项不能为空');return;}
                taskArg['memo'] = $memo.val();
            }else{
                taskArg['memo'] = $('#taskMemo').val();
            }
            show_loading();
            $.post(rurl('scm/paymentAccount/processJump'),taskArg,function (bres) {
                if(isSuccess(bres)){
                    parent.reloadList();
                    closeThisWin('操作成功');
                }
            });
        });
    });

    $('.shortcut-selection').find('.layui-badge').click(function () {
        $('.shortcut-val').val($(this).text());
    });
});