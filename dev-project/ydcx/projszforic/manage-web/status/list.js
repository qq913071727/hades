layui.use(['layer'], function() {
    var layer = layui.layer,docId = getUrlParam('docId');
    var $saObj = $('#statusArea');
    $.post(rurl('scm/statusHistory/list'),{'docId':docId},function (res) {
        $.each(res.data||[],function (i,o) {
            $saObj.append([
                '<li class="layui-timeline-item">','<i class="layui-icon layui-timeline-axis"></i>','<div class="layui-timeline-content layui-text">',
                '<h3 class="layui-timeline-title">【',o.operationName,'】',o.dateCreated,'</h3>',' <span class="',o.originalStatusId,'">',o.originalStatusName,'</span>',
                ((isEmpty(o.originalStatusId))?'':'<i class="fa fa-long-arrow-right"></i>'),'<span class="',o.nowStatusId,'">',o.nowStausName,'</span>','<p>','<span class="oname">',o.createBy,'</span>',
                o.memo,'</p></div></li>'
            ].join(''));
        });
    });
});