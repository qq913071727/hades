layui.use(['layer'], function() {
    var layer = layui.layer,domainId = getUrlParam('domainId'),domainType = getUrlParam('domainType');
    var $saObj = $('#statusArea');
    $.post(rurl('scm/singleWindow/list'),{'domainId':domainId,'domainType':domainType},function (res) {
        $.each(res.data||[],function (i,o) {
            $saObj.append([
                '<li class="layui-timeline-item">','<i class="layui-icon layui-timeline-axis"></i>','<div class="layui-timeline-content layui-text">',
                '<h3 class="layui-timeline-title">【',o.node,'】',o.dateCreated,'</h3>',
                '<p>','<span class="oname">',o.createBy,'</span>',
                o.memo,'</p></div></li>'
            ].join(''));
        });
    });
});