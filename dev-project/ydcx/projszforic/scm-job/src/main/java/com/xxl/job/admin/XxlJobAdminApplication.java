package com.xxl.job.admin;

import com.xxl.job.admin.core.conf.XxlJobAdminConfig;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import javax.annotation.Resource;

/**
 * @author xuxueli 2018-10-28 00:38:13
 */
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = "com.xxl")
public class XxlJobAdminApplication implements CommandLineRunner {

	@Resource
	private XxlJobAdminConfig xxlJobAdminConfig;

	public static void main(String[] args) {
        SpringApplication.run(XxlJobAdminApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//System.out.println("查看bean初始化:" + xxlJobAdminConfig);
	}
}