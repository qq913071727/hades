package com.innovation.ic.b1b.data.center.web.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Strings;
import com.innovation.ic.b1b.data.center.base.pojo.constant.RedisStorage;
import com.innovation.ic.b1b.data.center.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.data.center.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.data.center.base.pojo.variable.inventory.InventoryBlurQueryBzkPartNumberListRespPojo;
import com.innovation.ic.b1b.data.center.base.vo.inventory.InventoryBlurQueryVo;
import com.innovation.ic.b1b.data.center.base.vo.inventory.InventoryDelByBrandPartNumberVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @desc   库存相关数据处理controller
 * @author linuo
 * @time   2023年4月17日15:46:32
 */
@Api(value = "库存相关数据处理controller", tags = "InventoryController")
@RestController
@RequestMapping("/api/v1/inventory")
@Slf4j
public class InventoryController extends AbstractController {
    @ApiOperation("根据型号模糊查询信息")
    @RequestMapping(value = "/blurQueryBzkPartNumberList", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ApiImplicitParam(name = "InventoryBlurQueryVo", value = "根据型号模糊查询信息的Vo类", required = true, dataType = "InventoryBlurQueryVo")
    @ResponseBody
    public ResponseEntity<ApiResult<List<InventoryBlurQueryBzkPartNumberListRespPojo>>> blurQueryBzkPartNumberList(@RequestBody InventoryBlurQueryVo inventoryBlurQueryVo) {
        if (Strings.isNullOrEmpty(inventoryBlurQueryVo.getPartNumber())) {
            String message = "调用接口【/api/v1/inventory/blurQueryBzkPartNumberList】时，参数partNumber不能为空";
            log.warn(message);
            ApiResult<List<InventoryBlurQueryBzkPartNumberListRespPojo>> apiResult = new ApiResult<>(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        List<InventoryBlurQueryBzkPartNumberListRespPojo> respPojos = new ArrayList<>();

        Set<String> strings = redisManager.zRange(RedisStorage.PART_NUMBER_BLUR_QUERY_PREFIX + inventoryBlurQueryVo.getPartNumber(), 0, 10);
        if (strings == null || strings.size() == 0) {
            // 查询数据库数据
            ServiceResult<List<InventoryBlurQueryBzkPartNumberListRespPojo>> result = productsService.blurQueryBzkPartNumberList(inventoryBlurQueryVo.getPartNumber());
            if(result != null && result.getResult() != null){
                List<InventoryBlurQueryBzkPartNumberListRespPojo> result1 = result.getResult();
                for(InventoryBlurQueryBzkPartNumberListRespPojo data : result1){
                    // 保存查询结果数据
                    redisManager.zAdd(RedisStorage.PART_NUMBER_BLUR_QUERY_PREFIX + inventoryBlurQueryVo.getPartNumber(), JSON.toJSONString(data), System.currentTimeMillis());
                    redisManager.expire(RedisStorage.PART_NUMBER_BLUR_QUERY_PREFIX + inventoryBlurQueryVo.getPartNumber(), redisParamConfig.getExpireTimeout());

                    // 保存型号与redis的key关联关系数据
                    redisManager.zAdd(RedisStorage.PART_NUMBER_REDIS_KEY_CORRELATION_PREFIX + data.getPartNumber() + "_" + data.getBrand(), RedisStorage.PART_NUMBER_BLUR_QUERY_PREFIX + inventoryBlurQueryVo.getPartNumber(), System.currentTimeMillis());
                    redisManager.expire(RedisStorage.PART_NUMBER_REDIS_KEY_CORRELATION_PREFIX + data.getPartNumber() + "_" + data.getBrand(), redisParamConfig.getExpireTimeout());
                }
                respPojos = result.getResult();
            }
        }else{
            for(String set : strings){
                InventoryBlurQueryBzkPartNumberListRespPojo inventoryBlurQueryBzkPartNumberListRespPojo = JSON.parseObject(set, InventoryBlurQueryBzkPartNumberListRespPojo.class);
                respPojos.add(inventoryBlurQueryBzkPartNumberListRespPojo);
            }
        }

        ApiResult<List<InventoryBlurQueryBzkPartNumberListRespPojo>> apiResult = new ApiResult<>(HttpStatus.OK.value(), ServiceResult.SELECT_SUCCESS, respPojos, Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @ApiOperation("根据品牌、型号删除redis信息")
    @ApiImplicitParam(name = "InventoryDelByBrandPartNumberVo", value = "根据品牌、型号删除redis信息接口的Vo类", required = true, dataType = "InventoryDelByBrandPartNumberVo")
    @RequestMapping(value = "/delByBrandPartNumber", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> delByBrandPartNumber(@RequestBody InventoryDelByBrandPartNumberVo inventoryDelByBrandPartNumberVo) {
        if (Strings.isNullOrEmpty(inventoryDelByBrandPartNumberVo.getBrand()) || Strings.isNullOrEmpty(inventoryDelByBrandPartNumberVo.getPartNumber())) {
            String message = "调用接口【/api/v1/inventory/delByBrandPartNumber】时，参数brand、partNumber不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>(HttpStatus.BAD_REQUEST.value(), message, null, Boolean.FALSE);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        Boolean result = Boolean.FALSE;
        String message = ServiceResult.DELETE_FAIL;

        String key = RedisStorage.PART_NUMBER_REDIS_KEY_CORRELATION_PREFIX + inventoryDelByBrandPartNumberVo.getPartNumber() + "_" + inventoryDelByBrandPartNumberVo.getBrand();
        Long size = redisManager.zSize(key);
        if(size > 0){
            log.info("key:[{}]的数据数量为[{}]", key, size);
            Set<String> strings = redisManager.zRange(key, 0, size);
            Long del = redisManager.del(strings);
            log.info("删除的数据数量为[{}]", del);
            if(del > 0){
                Boolean del1 = redisManager.del(key);
                if(del1){
                    log.info("key:[{}]删除成功", key);
                    result = Boolean.TRUE;
                    message = ServiceResult.DELETE_SUCCESS;
                }
            }
        }else{
            log.info("key:[{}]的数据数量为0,无需删除任何数据", key);
            result = Boolean.TRUE;
            message = ServiceResult.DELETE_SUCCESS;
        }

        ApiResult<Boolean> apiResult = new ApiResult<>(HttpStatus.OK.value(), message, result, Boolean.TRUE);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}