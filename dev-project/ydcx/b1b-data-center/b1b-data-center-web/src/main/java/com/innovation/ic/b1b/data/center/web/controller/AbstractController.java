package com.innovation.ic.b1b.data.center.web.controller;

import com.innovation.ic.b1b.data.center.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.data.center.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard.BrandsService;
import com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard.PetNamesService;
import com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard.ProductsService;
import com.innovation.ic.b1b.data.center.base.value.RedisParamConfig;
import com.innovation.ic.b1b.framework.manager.RedisManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import javax.annotation.Resource;

/**
 * 抽象controller
 */
public abstract class AbstractController {
    @Resource
    protected RedisManager redisManager;

    @Resource
    protected BrandsService brandsService;

    @Resource
    protected PetNamesService petNamesService;

    @Resource
    protected ProductsService productsService;

    @Resource
    protected RedisParamConfig redisParamConfig;

    /**
     * hystrix默认处理方法
     * 其它应用调用超时时调用此方法进行异常信息抛出，防止出现长时间接口不返回结果的情况出现
     * @return 返回熔断默认结果
     */
    protected ResponseEntity<ApiResult> defaultFallback() {
        ApiResult apiResult = new ApiResult();
        apiResult.setSuccess(Boolean.FALSE);
        apiResult.setCode(HttpStatus.REQUEST_TIMEOUT.value());
        apiResult.setResult(Boolean.FALSE);
        apiResult.setMessage(ServiceResult.TIME_OUT);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}