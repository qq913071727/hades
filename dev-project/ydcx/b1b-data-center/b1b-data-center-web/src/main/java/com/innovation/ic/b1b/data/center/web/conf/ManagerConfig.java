package com.innovation.ic.b1b.data.center.web.conf;

import com.innovation.ic.b1b.data.center.base.value.ThreadPoolConfig;
import com.innovation.ic.b1b.framework.manager.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import javax.annotation.Resource;

@Configuration
public class ManagerConfig {

    /****************************************** redis *********************************************/
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Bean
    public RedisTemplate<String, String> redisTemplate(RedisTemplate redisTemplate) {
        RedisSerializer stringSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringSerializer);
        redisTemplate.setStringSerializer(stringSerializer);
        redisTemplate.setValueSerializer(stringSerializer);
        redisTemplate.setHashKeySerializer(stringSerializer);
        redisTemplate.setHashValueSerializer(stringSerializer);
        return redisTemplate;
    }

    @Bean
    public RedisManager redisManager() {
        return new RedisManager(redisTemplate);
    }


    /**************************************** thread pool *******************************************/
    @Resource
    private ThreadPoolConfig threadPoolConfig;

    @Bean
    public ThreadPoolManager threadPoolManager() {
        return new ThreadPoolManager(threadPoolConfig.getCorePoolSize(),
                threadPoolConfig.getMaximumPoolSize(), threadPoolConfig.getKeepAliveTime(),
                threadPoolConfig.getWorkQueue(), null);
    }

}
