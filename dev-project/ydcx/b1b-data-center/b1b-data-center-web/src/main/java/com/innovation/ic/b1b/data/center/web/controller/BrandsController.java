package com.innovation.ic.b1b.data.center.web.controller;

import com.alibaba.fastjson.JSON;
import com.innovation.ic.b1b.data.center.base.model.PetNames;
import com.innovation.ic.b1b.data.center.base.pojo.BrandAndPetNamePojo;
import com.innovation.ic.b1b.data.center.base.pojo.constant.DatabaseOperationType;
import com.innovation.ic.b1b.data.center.base.pojo.constant.RedisStorage;
import com.innovation.ic.b1b.data.center.base.pojo.variable.ApiResult;
import com.innovation.ic.b1b.data.center.base.vo.BrandsOperateVo;
import com.innovation.ic.b1b.data.center.base.vo.PetNamesOperateVo;
import com.innovation.ic.b1b.framework.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zengqinglong
 * @desc 产品品牌
 * @Date 2023/4/17 14:11
 **/
@Api(value = "产品品牌", tags = "BrandsController")
@RestController
@RequestMapping("/api/v1/brands")
@Slf4j
public class BrandsController extends AbstractController {
    /**
     * @return
     * @description 查询品牌信息
     * @parms
     */
    @ApiOperation("根据 名称、中午名、英文名、简称、别名 查询品牌")
    @RequestMapping(value = "/findByName", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ApiParam(name = "searchName", value = "名称、中午名、英文名、简称、别名", required = false, type = "string")
    public ResponseEntity<ApiResult<String>> findByName(String searchName) {
        if (StringUtils.isEmpty(searchName)) {
            return new ResponseEntity<>(ApiResult.error(ApiResult.INPUT_PARAM_NULL, "searchName 查询参数为空"), HttpStatus.OK);
        }
        String res = (String) redisManager.get(RedisStorage.BRANDS_BY_FIND_PREFIX + searchName.toLowerCase().trim());
        if (StringUtils.isEmpty(res)) {
            return new ResponseEntity<>(ApiResult.ok("查询成功"), HttpStatus.OK);
        }
        return new ResponseEntity<>(ApiResult.ok(res, "查询成功"), HttpStatus.OK);
    }

    /**
     * @return
     * @description 大赢家品牌名称转换
     * @parms
     */
    @ApiOperation("大赢家 品牌名称转换")
    @RequestMapping(value = "/dyjBrandNameConversion", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ApiParam(name = "searchName", value = "别名", required = false, type = "string")
    public ResponseEntity<ApiResult<String>> dyjBrandNameConversion(String searchName) {
        if (StringUtils.isEmpty(searchName)) {
            return new ResponseEntity<>(ApiResult.error(ApiResult.INPUT_PARAM_NULL, "searchName 查询参数为空"), HttpStatus.OK);
        }
        String res = (String) redisManager.get(RedisStorage.DYJ_BRAND_NAME_CONVERSION_PREFIX + searchName.toLowerCase().trim());
        if (StringUtils.isEmpty(res)) {
            return new ResponseEntity<>(ApiResult.ok("查询成功"), HttpStatus.OK);
        }
        BrandAndPetNamePojo brandAndPetNamePojo = JSON.parseObject(res, BrandAndPetNamePojo.class);
        return new ResponseEntity<>(ApiResult.ok(brandAndPetNamePojo.getName(), "查询成功"), HttpStatus.OK);
    }


    /**
     * @return
     * @description 品牌表操作接口
     * @parms
     */
    @ApiOperation(value = "品牌表操作接口：brands表操作")
    @RequestMapping(value = "/brandsTableOperate", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult> brandsTableOperate(@RequestBody BrandsOperateVo brandsOperateVo) {
        if (!StringUtils.validateParameter(brandsOperateVo.getOperate())) {
            String message = "调用接口【/api/v1/brands/brandsTableOperate，参数Operate不能为空";
            log.info(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        //处理brands
        //删除
        if (brandsOperateVo.getOperate().equals(DatabaseOperationType.DELETE)) {
            delRedisBrands(brandsOperateVo);

            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.TRUE);
            apiResult.setCode(HttpStatus.OK.value());
            apiResult.setMessage("接口调用成功");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        //修改
        if (brandsOperateVo.getOperate().equals(DatabaseOperationType.UPDATE)) {
            updateRedisBrands(brandsOperateVo);

            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.TRUE);
            apiResult.setCode(HttpStatus.OK.value());
            apiResult.setMessage("接口调用成功");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        //添加
        if (brandsOperateVo.getOperate().equals(DatabaseOperationType.INSERT)) {
            insertRedisBrands(brandsOperateVo);

            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.TRUE);
            apiResult.setCode(HttpStatus.OK.value());
            apiResult.setMessage("接口调用成功");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }


        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.FALSE);
        apiResult.setCode(HttpStatus.BAD_REQUEST.value());
        apiResult.setMessage("操作类型 未知,operate:" + brandsOperateVo.getOperate());
        return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
    }

    /**
     * @return
     * @description 别名表操作接口
     * @parms
     */
    @ApiOperation(value = "别名表操作接口：petNames表操作")
    @RequestMapping(value = "/petNamesTableOperate", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    public ResponseEntity<ApiResult> petNamesTableOperate(@RequestBody PetNamesOperateVo petNamesOperateVo) {
        if (!StringUtils.validateParameter(petNamesOperateVo.getOperate())) {
            String message = "调用接口【/api/v1/brands/petNamesTableOperate，参数Operate不能为空";
            log.info(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        //处理brands
        //删除
        if (petNamesOperateVo.getOperate().equals(DatabaseOperationType.DELETE)) {
            delRedisPetNames(petNamesOperateVo);

            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.TRUE);
            apiResult.setCode(HttpStatus.OK.value());
            apiResult.setMessage("接口调用成功");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        //修改
        if (petNamesOperateVo.getOperate().equals(DatabaseOperationType.UPDATE)) {
            updateRedisPetNames(petNamesOperateVo);

            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.TRUE);
            apiResult.setCode(HttpStatus.OK.value());
            apiResult.setMessage("接口调用成功");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        //添加
        if (petNamesOperateVo.getOperate().equals(DatabaseOperationType.INSERT)) {
            insertRedisPetNames(petNamesOperateVo);

            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.TRUE);
            apiResult.setCode(HttpStatus.OK.value());
            apiResult.setMessage("接口调用成功");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }


        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.FALSE);
        apiResult.setCode(HttpStatus.BAD_REQUEST.value());
        apiResult.setMessage("操作类型 未知,operate:" + petNamesOperateVo.getOperate());
        return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
    }


    /**
     * 品牌别名删除
     *
     * @param petNamesOperateVo
     */
    public void delRedisPetNames(PetNamesOperateVo petNamesOperateVo) {
        if (StringUtils.isNotEmpty(petNamesOperateVo.getNaming())) {
            redisManager.del(RedisStorage.BRANDS_BY_FIND_PREFIX + petNamesOperateVo.getNaming().toLowerCase().trim());
            redisManager.del(RedisStorage.DYJ_BRAND_NAME_CONVERSION_PREFIX + petNamesOperateVo.getNaming().toLowerCase().trim());
        }
    }

    /**
     * 品牌别名添加
     *
     * @param petNamesOperateVo
     */
    public void insertRedisPetNames(PetNamesOperateVo petNamesOperateVo) {
        BrandAndPetNamePojo brandAndPetNamePojo = brandsService.findBrandsAndPetNamesByPetNamesId(petNamesOperateVo.getId());
        if (brandAndPetNamePojo == null) {
            return;
        }
        //petName表 别名
        if (StringUtils.isNotEmpty(brandAndPetNamePojo.getPetNameNaming())) {
            //更新大赢家数据
            redisManager.set(RedisStorage.DYJ_BRAND_NAME_CONVERSION_PREFIX + brandAndPetNamePojo.getPetNameNaming().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
            redisManager.set(RedisStorage.DYJ_BRAND_NAME_CONVERSION_PREFIX + brandAndPetNamePojo.getName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
            //更新brands表数据
            redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getPetNameNaming().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
            redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
            redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getCnName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
            redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getEnName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
            redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getShortName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
        }
    }

    /**
     * 品牌别名添加
     *
     * @param petNamesOperateVo
     */
    public void updateRedisPetNames(PetNamesOperateVo petNamesOperateVo) {
        insertRedisPetNames(petNamesOperateVo);
    }

    /**
     * 品牌删除
     *
     * @param brandsOperateVo
     */
    public void delRedisBrands(BrandsOperateVo brandsOperateVo) {
        //名称
        if (StringUtils.isNotEmpty(brandsOperateVo.getName())) {
            redisManager.del(RedisStorage.BRANDS_BY_FIND_PREFIX + brandsOperateVo.getName().toLowerCase().trim());
            redisManager.del(RedisStorage.DYJ_BRAND_NAME_CONVERSION_PREFIX + brandsOperateVo.getName().toLowerCase().trim());
        }
        //中文名称
        if (StringUtils.isNotEmpty(brandsOperateVo.getCnName())) {
            redisManager.del(RedisStorage.BRANDS_BY_FIND_PREFIX + brandsOperateVo.getCnName().toLowerCase().trim());
        }
        //英文名称
        if (StringUtils.isNotEmpty(brandsOperateVo.getEnName())) {
            redisManager.del(RedisStorage.BRANDS_BY_FIND_PREFIX + brandsOperateVo.getEnName().toLowerCase().trim());
        }
        //简称
        if (StringUtils.isNotEmpty(brandsOperateVo.getShortName())) {
            redisManager.del(RedisStorage.BRANDS_BY_FIND_PREFIX + brandsOperateVo.getShortName().toLowerCase().trim());
        }
        //删除别名
        PetNames petNames = petNamesService.findByOriginName(brandsOperateVo.getName());
        if (petNames != null && petNames.getNaming() != null) {
            redisManager.del(RedisStorage.BRANDS_BY_FIND_PREFIX + petNames.getNaming().toLowerCase().trim());
            redisManager.del(RedisStorage.DYJ_BRAND_NAME_CONVERSION_PREFIX + petNames.getNaming().toLowerCase().trim());
        }
    }

    /**
     * 品牌添加
     *
     * @param brandsOperateVo
     */
    public void insertRedisBrands(BrandsOperateVo brandsOperateVo) {
        BrandAndPetNamePojo brandAndPetNamePojo = brandsService.findBrandsAndPetNamesByBrandsId(brandsOperateVo.getId());
        //未查询不做任何添加
        if (brandAndPetNamePojo == null) {
            return;
        }
        //处理大赢家转换Redis
        //名称
        if (StringUtils.isNotEmpty(brandAndPetNamePojo.getName())) {
            redisManager.set(RedisStorage.DYJ_BRAND_NAME_CONVERSION_PREFIX + brandAndPetNamePojo.getName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
        }
        //petName表 别名
        if (StringUtils.isNotEmpty(brandAndPetNamePojo.getPetNameNaming())) {
            redisManager.set(RedisStorage.DYJ_BRAND_NAME_CONVERSION_PREFIX + brandAndPetNamePojo.getPetNameNaming().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
        }

        //处理查询品牌redis
        //名称
        if (StringUtils.isNotEmpty(brandAndPetNamePojo.getName())) {
            redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
        }
        //中文名称
        if (StringUtils.isNotEmpty(brandAndPetNamePojo.getCnName())) {
            redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getCnName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
        }
        //英文名称
        if (StringUtils.isNotEmpty(brandAndPetNamePojo.getEnName())) {
            redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getEnName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
        }
        //简称
        if (StringUtils.isNotEmpty(brandAndPetNamePojo.getShortName())) {
            redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getShortName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
        }
        //petName表 别名
        if (StringUtils.isNotEmpty(brandAndPetNamePojo.getPetNameNaming())) {
            redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getPetNameNaming().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
        }
    }

    /**
     * 品牌修改
     *
     * @param brandsOperateVo
     */
    public void updateRedisBrands(BrandsOperateVo brandsOperateVo) {
        //如果修改状态为停用 需要删除redis
        if (brandsOperateVo.getStatus() == 500) {
            delRedisBrands(brandsOperateVo);
            return;
        }
        insertRedisBrands(brandsOperateVo);
    }
}
