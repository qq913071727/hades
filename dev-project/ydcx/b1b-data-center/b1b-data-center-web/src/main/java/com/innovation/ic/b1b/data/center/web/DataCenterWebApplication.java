package com.innovation.ic.b1b.data.center.web;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Slf4j
@MapperScan("com.innovation.ic.b1b.data.center.base.mapper")
@SpringBootApplication(scanBasePackages = {"com.innovation.ic.b1b.data.center"})
public class DataCenterWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataCenterWebApplication.class, args);
        log.info("数据中台系统启动完成");
    }
}