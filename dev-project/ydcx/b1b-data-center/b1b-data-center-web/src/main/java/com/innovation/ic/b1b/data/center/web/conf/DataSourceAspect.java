package com.innovation.ic.b1b.data.center.web.conf;
import com.innovation.ic.b1b.data.center.base.pojo.config.DatabaseGlobal;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value = -100)
@Aspect
public class DataSourceAspect {

    private static final Logger log = LoggerFactory.getLogger(DataSourceAspect.class);

    @Pointcut("execution(* com.innovation.ic.b1b.data.center.base.service.dc.impl..*.*(..))")
    private void b1bDataCenterAspect() {
    }

    @Pointcut("execution(* com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard.impl..*.*(..))")
    private void erp9PveStandardAspect() {
    }

    @Before("b1bDataCenterAspect()")
    public void b1bDataCenter() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.B1B_DATA_CENTER);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.B1B_DATA_CENTER);
    }

    @Before("erp9PveStandardAspect()")
    public void erp9PveStandard() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.ERP9_PVESTANDARD);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.ERP9_PVESTANDARD);
    }

}
