package com.innovation.ic.b1b.data.center.web.schedule;

import com.alibaba.fastjson.JSON;
import com.innovation.ic.b1b.data.center.base.pojo.BrandAndPetNamePojo;
import com.innovation.ic.b1b.data.center.base.pojo.constant.RedisStorage;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 初始化品牌数据到redis中
 * @Date 2023/4/14 15:38
 **/
@Component
@Slf4j
public class ImportBrandsAndPetNamesSchedule extends AbstractSchedule {
    /**
     * 初始化 大赢家品牌名称转换 数据到redis
     */
    @XxlJob("importInitDyjBrandNameConversionToRedis")
    public void importInitDyjBrandNameConversionToRedis() {
        try {
            log.info("初始化 大赢家品牌名称转换 数据到redis  开始");
            // 删除旧的数据
            redisManager.delRedisDataByKeyPrefix(RedisStorage.DYJ_BRAND_NAME_CONVERSION_PREFIX);
            //获取 赢家品牌名称转换 所需参数品牌表和别名表所有
            List<BrandAndPetNamePojo> brandAndPetNamePojoList = brandsService.DyjBrandNameConversionFindAll();
            for (BrandAndPetNamePojo brandAndPetNamePojo : brandAndPetNamePojoList) {
                //名称
                if (addRedis(RedisStorage.DYJ_BRAND_NAME_CONVERSION_PREFIX, brandAndPetNamePojo.getName())) {
                    redisManager.set(RedisStorage.DYJ_BRAND_NAME_CONVERSION_PREFIX + brandAndPetNamePojo.getName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
                }
                //petName表 别名
                if (addRedis(RedisStorage.DYJ_BRAND_NAME_CONVERSION_PREFIX, brandAndPetNamePojo.getPetNameNaming())) {
                    redisManager.set(RedisStorage.DYJ_BRAND_NAME_CONVERSION_PREFIX + brandAndPetNamePojo.getPetNameNaming().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
                }
            }
            log.info("初始化 大赢家品牌名称转换 数据到redis  结束");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("初始化 导入erp9所需的品牌数据:", e);
        }
    }

    /**
     * 初始化 品牌数据到redis中
     */
    @XxlJob("importInitBrandsAndPetNameToRedis")
    public void importInitBrandsAndPetNameToRedis() {
        try {
            log.info("初始化 品牌数据到redis中 开始");
            // 删除旧的数据
            redisManager.delRedisDataByKeyPrefix(RedisStorage.BRANDS_BY_FIND_PREFIX);
            //获取 赢家品牌名称转换 所需参数品牌表和别名表所有
            List<BrandAndPetNamePojo> brandAndPetNamePojoList = brandsService.findBrandsForPetNameAll();
            for (BrandAndPetNamePojo brandAndPetNamePojo : brandAndPetNamePojoList) {
                //名称
                if (addRedis(RedisStorage.BRANDS_BY_FIND_PREFIX, brandAndPetNamePojo.getName())) {
                    redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
                }
                //中文名称
                if (addRedis(RedisStorage.BRANDS_BY_FIND_PREFIX, brandAndPetNamePojo.getCnName())) {
                    redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getCnName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
                }
                //英文名称
                if (addRedis(RedisStorage.BRANDS_BY_FIND_PREFIX, brandAndPetNamePojo.getEnName())) {
                    redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getEnName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
                }
                //简称
                if (addRedis(RedisStorage.BRANDS_BY_FIND_PREFIX, brandAndPetNamePojo.getShortName())) {
                    redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getShortName().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
                }
                //petName表 别名
                if (addRedis(RedisStorage.BRANDS_BY_FIND_PREFIX, brandAndPetNamePojo.getPetNameNaming())) {
                    redisManager.set(RedisStorage.BRANDS_BY_FIND_PREFIX + brandAndPetNamePojo.getPetNameNaming().toLowerCase().trim(), JSON.toJSONString(brandAndPetNamePojo));
                }
            }
            log.info("初始化 品牌数据到redis中 结束");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("初始化 品牌数据到redis中 : 失败", e);
        }
    }

    /**
     * 判断是否需要添加到redis
     */
    public boolean addRedis(String redisKey, String searchName) {
        //为空数据无需做key
        if (StringUtils.isEmpty(searchName)) {
            return false;
        }
        //判断是否在 redis中 在无需添加,数据只能有一条
        return !redisManager.hasKey(redisKey + searchName.toLowerCase().trim());
    }
}
