package com.innovation.ic.b1b.data.center.web.schedule;

import com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard.BrandsService;
import com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard.PetNamesService;
import com.innovation.ic.b1b.framework.manager.RedisManager;

import javax.annotation.Resource;

/**
 * @author zengqinglong
 * @desc 定时任务的抽象类
 * @Date 2023/4/14 15:25
 **/
public abstract class AbstractSchedule {
    @Resource
    protected BrandsService brandsService;
    @Resource
    protected PetNamesService petNamesService;
    @Resource
    protected RedisManager redisManager;
}
