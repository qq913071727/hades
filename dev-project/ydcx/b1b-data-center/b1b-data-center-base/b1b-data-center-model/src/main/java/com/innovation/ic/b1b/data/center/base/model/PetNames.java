package com.innovation.ic.b1b.data.center.base.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 品牌别名表
 * @Date 2023/4/13 17:02
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PetNames", description = "品牌别名表")
@TableName("PetNames")
public class PetNames {
    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "ID")
    private String id;

    @ApiModelProperty(value = "别名类型", dataType = "Integer")
    @TableField(value = "Type")
    private Integer type;

    @ApiModelProperty(value = "名称，和Brands表的Name字段对应", dataType = "String")
    @TableField(value = "OriginName")
    private String originName;

    @ApiModelProperty(value = "别名", dataType = "String")
    @TableField(value = "Naming")
    private String naming;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField("CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField("ModifyDate")
    private Date modifyDate;

    @ApiModelProperty(value = "创建人", dataType = "String")
    @TableField("CreatorID")
    private String creatorId;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField("Classify")
    private String classify;
}