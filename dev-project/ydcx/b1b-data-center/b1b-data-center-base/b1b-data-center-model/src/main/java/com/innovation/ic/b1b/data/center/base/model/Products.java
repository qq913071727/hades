package com.innovation.ic.b1b.data.center.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

/**
 * @desc   产品表实体类
 * @author linuo
 * @time   2023年4月17日16:33:23
 */
@Data
@ApiModel
@TableName("Products")
public class Products implements Serializable {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "型号", dataType = "String")
    @TableField("PartNumber")
    private String partNumber;

    @ApiModelProperty(value = "品牌Id", dataType = "String")
    @TableField("BrandID")
    private String brandId;
}