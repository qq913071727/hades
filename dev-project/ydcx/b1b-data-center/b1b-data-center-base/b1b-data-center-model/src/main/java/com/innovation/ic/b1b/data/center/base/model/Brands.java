package com.innovation.ic.b1b.data.center.base.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * @author zengqinglong
 * @desc 品牌
 * @Date 2023/4/13 17:02
 **/
@Data
@ApiModel
@TableName("Brands")
public class Brands implements Serializable {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "ID")
    private String id;

    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField("Name")
    private String name;

    @ApiModelProperty(value = "品牌名称", dataType = "String")
    @TableField("CNName")
    private String cnName;

    @ApiModelProperty(value = "英文名称", dataType = "String")
    @TableField(value = "ENName")
    private String enName;

    @ApiModelProperty(value = "简称", dataType = "String")
    @TableField(value = "ShortName")
    private String shortName;

    @ApiModelProperty(value = "首字母", dataType = "String")
    @TableField(value = "Letter")
    private String letter;

    @ApiModelProperty(value = "网址", dataType = "String")
    @TableField(value = "WebSite")
    private String webSite;

    @ApiModelProperty(value = "生产商", dataType = "String")
    @TableField(value = "Manufacturer")
    private String manufacture;

    @ApiModelProperty(value = "是否代理(0否、1是)", dataType = "Integer")
    @TableField(value = "IsAgent")
    private Integer isAgent;

    @ApiModelProperty(value = "来源id)", dataType = "String")
    @TableField(value = "FromID")
    private String fromId;

    @ApiModelProperty(value = "是否代理(0否、1是)", dataType = "String")
    @TableField(value = "Summary")
    private String summary;

    @ApiModelProperty(value = "图标路径", dataType = "String")
    @TableField(value = "Image")
    private String image;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField("CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField("ModifyDate")
    private Date modifyDate;

    @ApiModelProperty(value = "创建人", dataType = "String")
    @TableField("CreatorID")
    private String creatorId;

    @ApiModelProperty(value = "状态", dataType = "Integer")
    @TableField("Status")
    private Integer status;

    @ApiModelProperty(value = "来源", dataType = "String")
    @TableField("Source")
    private String source;

    @ApiModelProperty(value = "样品入库", dataType = "Integer")
    @TableField("IsSampleStorage")
    private Integer isSampleStorage;
}
