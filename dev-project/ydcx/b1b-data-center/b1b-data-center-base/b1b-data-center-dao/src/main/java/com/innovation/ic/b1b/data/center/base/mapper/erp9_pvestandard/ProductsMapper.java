package com.innovation.ic.b1b.data.center.base.mapper.erp9_pvestandard;

import com.innovation.ic.b1b.data.center.base.mapper.EasyBaseMapper;
import com.innovation.ic.b1b.data.center.base.model.Products;
import com.innovation.ic.b1b.data.center.base.pojo.variable.inventory.InventoryBlurQueryBzkPartNumberListRespPojo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author linuo
 * @desc 产品表mapper
 * @time 2023年4月17日16:32:58
 */
@Mapper
public interface ProductsMapper extends EasyBaseMapper<Products> {
    /**
     * 模糊查询标准库型号
     *
     * @param partNumber 型号
     * @return 返回查询结果
     */
    List<InventoryBlurQueryBzkPartNumberListRespPojo> blurQueryBzkPartNumberList(@Param("partNumber") String partNumber);
}