package com.innovation.ic.b1b.data.center.base.mapper.erp9_pvestandard;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.innovation.ic.b1b.data.center.base.mapper.EasyBaseMapper;
import com.innovation.ic.b1b.data.center.base.model.PetNames;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zengqinglong
 * @desc 别名表
 * @Date 2023/4/14 15:05
 **/
@Mapper
public interface PetNamesMapper  extends EasyBaseMapper<PetNames> {
}
