package com.innovation.ic.b1b.data.center.base.mapper;

import com.innovation.ic.b1b.data.center.base.model.Client;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientMapper extends EasyBaseMapper<Client> {
}
