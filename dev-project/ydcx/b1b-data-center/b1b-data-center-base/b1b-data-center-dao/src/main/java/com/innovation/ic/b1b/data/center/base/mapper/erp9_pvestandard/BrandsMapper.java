package com.innovation.ic.b1b.data.center.base.mapper.erp9_pvestandard;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.innovation.ic.b1b.data.center.base.mapper.EasyBaseMapper;
import com.innovation.ic.b1b.data.center.base.pojo.BrandAndPetNamePojo;
import com.innovation.ic.b1b.data.center.base.model.Brands;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author zengqinglong
 * @desc p
 * @Date 2023/4/13 17:02
 **/
@Mapper
public interface BrandsMapper extends EasyBaseMapper<Brands> {
    /**
     * 获取erp9 大赢家所需参数品牌表和别名表所有
     *
     * @return
     */
    List<BrandAndPetNamePojo> dyjBrandNameConversionFindAll();


    /**
     * 获取erp9 大赢家所需参数品牌表和别名表所有
     *
     * @return
     */
    List<BrandAndPetNamePojo> findBrandsForPetNameAll();

    /**
     * 根据id查询 brand 表和 petNames表参数
     *
     * @param id
     * @return
     */
    BrandAndPetNamePojo findBrandsAndPetNamesByBrandsId(String id);

    /**
     * 根据别名id查询  brand 表和 petNames表参数
     *
     * @param petNameId
     * @return
     */
    BrandAndPetNamePojo findBrandsAndPetNamesByPetNamesId(String petNameId);
}
