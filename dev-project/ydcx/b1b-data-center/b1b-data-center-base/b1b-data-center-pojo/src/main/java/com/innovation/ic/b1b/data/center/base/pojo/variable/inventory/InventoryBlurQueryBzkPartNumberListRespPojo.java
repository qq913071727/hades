package com.innovation.ic.b1b.data.center.base.pojo.variable.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   模糊查询标准库型号返回的Pojo类
 * @author linuo
 * @time   2023年4月2日09:32:36
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryBlurQueryBzkPartNumberListRespPojo", description = "模糊查询标准库型号返回的Pojo类")
public class InventoryBlurQueryBzkPartNumberListRespPojo {
    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "封装", dataType = "String")
    private String packages;

    @ApiModelProperty(value = "包装(1:卷带(TR)、2:Pack)", dataType = "Integer")
    private Integer packing;

    @ApiModelProperty(value = "最小起订量", dataType = "Integer")
    private Integer moq;

    @ApiModelProperty(value = "最小包装量", dataType = "Integer")
    private Integer mpq;
}