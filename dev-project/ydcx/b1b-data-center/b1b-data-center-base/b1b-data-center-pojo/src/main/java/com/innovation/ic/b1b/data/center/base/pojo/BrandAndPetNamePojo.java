package com.innovation.ic.b1b.data.center.base.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 品牌表和别名表实体
 * @Date 2023/4/17 11:38
 **/
@Data
public class BrandAndPetNamePojo implements Serializable {
    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "品牌名称", dataType = "String")
    private String cnName;

    @ApiModelProperty(value = "英文名称", dataType = "String")
    private String enName;

    @ApiModelProperty(value = "简称", dataType = "String")
    private String shortName;

    @ApiModelProperty(value = "首字母", dataType = "String")
    private String letter;

    @ApiModelProperty(value = "网址", dataType = "String")
    private String webSite;

    @ApiModelProperty(value = "生产商", dataType = "String")
    private String manufacturer;

    @ApiModelProperty(value = "是否代理(0否、1是)", dataType = "Integer")
    private Integer isAgent;

    @ApiModelProperty(value = "来源id", dataType = "String")
    private String fromId;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String summary;

    @ApiModelProperty(value = "图标路径", dataType = "String")
    private String image;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createDate;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    private Date modifyDate;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String creatorId;

    @ApiModelProperty(value = "状态", dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "来源", dataType = "String")
    private String source;

    @ApiModelProperty(value = "样品入库", dataType = "Integer")
    private Integer isSampleStorage;

    @ApiModelProperty(value = "petName表 别名类型", dataType = "Integer")
    private Integer petNameType;

    @ApiModelProperty(value = "petName表 别名", dataType = "String")
    private String petNameNaming;

    @ApiModelProperty(value = "petName表 分类", dataType = "String")
    private String petNameClassify;
}
