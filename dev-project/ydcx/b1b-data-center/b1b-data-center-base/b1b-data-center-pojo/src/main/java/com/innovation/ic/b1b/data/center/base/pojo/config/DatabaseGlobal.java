package com.innovation.ic.b1b.data.center.base.pojo.config;

public class DatabaseGlobal {

    /**
     * b1b-data-center数据库
     */
    public static final String B1B_DATA_CENTER = "b1b-data-center";

    /**
     * erp9系统的PveStandard数据库
     */
    public static final String ERP9_PVESTANDARD = "erp9-pvestandard";

}
