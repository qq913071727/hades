package com.innovation.ic.b1b.data.center.base.pojo.constant;

/**
 * 数据库操作类型
 */
public class DatabaseOperationType {

    /**
     * 增
     */
    public static final String INSERT = "insert";

    /**
     * 删
     */
    public static final String DELETE = "delete";

    /**
     * 改
     */
    public static final String UPDATE = "update";

}
