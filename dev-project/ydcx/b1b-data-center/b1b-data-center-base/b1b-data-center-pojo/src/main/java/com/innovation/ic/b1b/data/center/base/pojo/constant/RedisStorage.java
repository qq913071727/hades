package com.innovation.ic.b1b.data.center.base.pojo.constant;

/**
 * redis存储时使用的常量
 */
public class RedisStorage {

    /**
     * 存入redis 下划线特殊标识
     */
    public static final String UNDERLINE = "_";

    /** 品牌信息前缀 */
    public static final String BRANDS_BY_FIND_PREFIX = "api:v1:brands:findByName_";

    /**
     * 大赢家品牌信息前缀
     */
    public static final String DYJ_BRAND_NAME_CONVERSION_PREFIX = "api:v1:brands:dyjBrandNameConversion_";

    /** 型号模糊查询数据的redis前缀 */
    public static final String PART_NUMBER_BLUR_QUERY_PREFIX = "api:v1:inventory:blurQueryBzkPartNumberList_";

    /** 型号与redis的key关联关系前缀 */
    public static final String PART_NUMBER_REDIS_KEY_CORRELATION_PREFIX = "api:v1:inventoryController:partNumberRedisKey:correlation_";
}