package com.innovation.ic.b1b.data.center.base.vo.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   根据品牌、型号删除redis信息接口的Vo类
 * @author linuo
 * @time   2023年5月18日09:59:54
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryDelByBrandPartNumberVo", description = "根据品牌、型号删除redis信息接口的Vo类")
public class InventoryDelByBrandPartNumberVo {
    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brand;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;
}