package com.innovation.ic.b1b.data.center.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 别名表操作
 * @Date 2023/4/19 10:55
 **/
@Data
public class PetNamesOperateVo implements Serializable {
    @ApiModelProperty(value = "操作 insert(新增), delete（删除）,update(修改)", dataType = "String")
    private String operate;

    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "别名类型", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "名称，和Brands表的Name字段对应", dataType = "String")
    private String originName;

    @ApiModelProperty(value = "别名", dataType = "String")
    private String naming;

    @ApiModelProperty(value = "创建时间", dataType = "String")
    private String createDate;

    @ApiModelProperty(value = "修改时间", dataType = "String")
    private String modifyDate;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String creatorId;

    @ApiModelProperty(value = "", dataType = "String")
    private String classify;
}
