package com.innovation.ic.b1b.data.center.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zengqinglong
 * @desc 品牌表操作
 * @Date 2023/4/19 9:27
 **/
@Data
public class BrandsOperateVo implements Serializable {
    @ApiModelProperty(value = "操作 insert(新增), delete（删除）,update(修改)", dataType = "String")
    private String operate;

    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "品牌名称", dataType = "String")
    private String cnName;

    @ApiModelProperty(value = "英文名称", dataType = "String")
    private String enName;

    @ApiModelProperty(value = "简称", dataType = "String")
    private String shortName;

    @ApiModelProperty(value = "首字母", dataType = "String")
    private String letter;

    @ApiModelProperty(value = "网址", dataType = "String")
    private String webSite;

    @ApiModelProperty(value = "生产商", dataType = "String")
    private String manufacturer;

    @ApiModelProperty(value = "是否代理(0否、1是)", dataType = "Integer")
    private Integer isAgent;

    @ApiModelProperty(value = "来源id)", dataType = "String")
    private String fromId;

    @ApiModelProperty(value = "是否代理(0否、1是)", dataType = "String")
    private String summary;

    @ApiModelProperty(value = "图标路径", dataType = "String")
    private String image;

    @ApiModelProperty(value = "创建时间", dataType = "String")
    private String createDate;

    @ApiModelProperty(value = "修改时间", dataType = "String")
    private String modifyDate;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String creatorId;

    @ApiModelProperty(value = "状态", dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "来源", dataType = "String")
    private String source;

    @ApiModelProperty(value = "样品入库", dataType = "Integer")
    private Integer isSampleStorage;
}
