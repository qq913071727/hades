package com.innovation.ic.b1b.data.center.base.vo.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   根据型号模糊查询信息的Vo类
 * @author linuo
 * @time   2023年4月18日14:03:40
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InventoryBlurQueryVo", description = "根据型号模糊查询信息的Vo类")
public class InventoryBlurQueryVo {
    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;
}