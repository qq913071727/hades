package com.innovation.ic.b1b.data.center.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   redis配置类
 * @author linuo
 * @time   2023年4月18日14:44:37
 */
@Data
@Component
@ConfigurationProperties(prefix = "redis")
public class RedisParamConfig {
    /** redis值的过期时间 */
    private Integer expireTimeout;
}