package com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.data.center.base.mapper.erp9_pvestandard.ProductsMapper;
import com.innovation.ic.b1b.data.center.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.data.center.base.pojo.variable.inventory.InventoryBlurQueryBzkPartNumberListRespPojo;
import com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard.ProductsService;
import com.innovation.ic.b1b.data.center.base.model.Products;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @desc   产品service实现类
 * @author linuo
 * @time   2023年4月17日16:25:49
 */
@Service
@Slf4j
public class ProductsServiceImpl extends ServiceImpl<ProductsMapper, Products> implements ProductsService {
    @Resource
    private ProductsMapper productsMapper;

    /**
     * 模糊查询标准库型号
     * @param partNumber 型号
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<InventoryBlurQueryBzkPartNumberListRespPojo>> blurQueryBzkPartNumberList(String partNumber) {
        String message = ServiceResult.SELECT_FAIL;

        List<InventoryBlurQueryBzkPartNumberListRespPojo> result = productsMapper.blurQueryBzkPartNumberList("%" + partNumber + "%");
        if(result != null && result.size() > 0){
            message = ServiceResult.SELECT_SUCCESS;
        }

        ServiceResult<List<InventoryBlurQueryBzkPartNumberListRespPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(message);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        return serviceResult;
    }
}