package com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.innovation.ic.b1b.data.center.base.mapper.erp9_pvestandard.BrandsMapper;
import com.innovation.ic.b1b.data.center.base.pojo.BrandAndPetNamePojo;
import com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard.BrandsService;
import com.innovation.ic.b1b.data.center.base.model.Brands;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 品牌管理
 * @Date 2023/4/14 15:31
 **/
@Service
public class BrandsServiceImpl implements BrandsService {
    @Resource
    private BrandsMapper brandsMapper;

    @Override
    public List<Brands> findAll() {
        return brandsMapper.selectList(new LambdaQueryWrapper<>());
    }

    @Override
    public List<BrandAndPetNamePojo> DyjBrandNameConversionFindAll() {
        return brandsMapper.dyjBrandNameConversionFindAll();
    }

    @Override
    public List<BrandAndPetNamePojo> findBrandsForPetNameAll() {
        return brandsMapper.findBrandsForPetNameAll();
    }

    @Override
    public BrandAndPetNamePojo findBrandsAndPetNamesByBrandsId(String id) {
        return brandsMapper.findBrandsAndPetNamesByBrandsId(id);
    }

    @Override
    public BrandAndPetNamePojo findBrandsAndPetNamesByPetNamesId(String id) {
        return brandsMapper.findBrandsAndPetNamesByPetNamesId(id);
    }
}
