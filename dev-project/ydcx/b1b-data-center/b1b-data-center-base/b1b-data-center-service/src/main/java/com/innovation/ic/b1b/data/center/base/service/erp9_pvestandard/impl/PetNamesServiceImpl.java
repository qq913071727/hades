package com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.innovation.ic.b1b.data.center.base.mapper.erp9_pvestandard.PetNamesMapper;
import com.innovation.ic.b1b.data.center.base.model.PetNames;
import com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard.PetNamesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * @author zengqinglong
 * @desc 品牌别名管理
 * @Date 2023/4/14 15:33
 **/
@Service
@Slf4j
public class PetNamesServiceImpl implements PetNamesService {
    @Resource
    private PetNamesMapper petNamesMapper;
    /**
     * 根据 brand表name 查询别名
     *
     * @param name
     * @return
     */
    @Override
    public PetNames findByOriginName(String name) {
        LambdaQueryWrapper<PetNames> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(PetNames::getOriginName,name);
        return  petNamesMapper.selectOne(wrapper);
    }
}
