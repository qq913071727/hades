package com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard;

import com.innovation.ic.b1b.data.center.base.pojo.variable.ServiceResult;
import com.innovation.ic.b1b.data.center.base.pojo.variable.inventory.InventoryBlurQueryBzkPartNumberListRespPojo;
import java.util.List;

/**
 * @desc   产品service
 * @author linuo
 * @time   2023年4月17日16:25:49
 */
public interface ProductsService {
    /**
     * 模糊查询标准库型号
     * @param partNumber 型号
     * @return 返回查询结果
     */
    ServiceResult<List<InventoryBlurQueryBzkPartNumberListRespPojo>> blurQueryBzkPartNumberList(String partNumber);
}