package com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard;

import com.innovation.ic.b1b.data.center.base.pojo.BrandAndPetNamePojo;
import com.innovation.ic.b1b.data.center.base.model.Brands;

import java.util.List;

/**
 * @author zengqinglong
 * @desc 品牌管理
 * @Date 2023/4/14 15:31
 **/
public interface BrandsService {
    /**
     * 品牌
     *
     * @return
     */
    List<Brands> findAll();

    /**
     * 获取erp9 大赢家所需参数品牌表和别名表所有
     *
     * @return
     */
    List<BrandAndPetNamePojo> DyjBrandNameConversionFindAll();

    /**
     * 获取品牌表和别名表所有
     *
     * @return
     */
    List<BrandAndPetNamePojo> findBrandsForPetNameAll();

    /**
     * 根据id查询 brand 表和 petNames表参数
     * @param id
     * @return
     */
    BrandAndPetNamePojo findBrandsAndPetNamesByBrandsId(String id);

    /**
     *  根据别名id查询  brand 表和 petNames表参数
     * @param id
     * @return
     */
    BrandAndPetNamePojo findBrandsAndPetNamesByPetNamesId(String id);
}
