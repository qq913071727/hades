package com.innovation.ic.b1b.data.center.base.service.erp9_pvestandard;

import com.innovation.ic.b1b.data.center.base.model.PetNames;

/**
 * @author zengqinglong
 * @desc 品牌别名管理
 * @Date 2023/4/14 15:33
 **/
public interface PetNamesService {
    /**
     * 根据 brand表name 查询别名
     * @param name
     * @return
     */
    PetNames findByOriginName(String name);
}
