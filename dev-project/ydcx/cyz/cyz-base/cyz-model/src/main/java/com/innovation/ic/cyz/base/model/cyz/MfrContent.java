package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MfrContent", description = "smt/pcb厂家内容")
@TableName("mfr_content")
public class MfrContent {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "mfr主键", dataType = "Long")
    @TableField(value = "mfr_id")
    private Long mfrId;

    @ApiModelProperty(value = "mfr_param主键", dataType = "Long")
    @TableField(value = "mfr_param_id")
    private Long mfrParamId;

    @ApiModelProperty(value = "分组号", dataType = "Integer")
    @TableField(value = "group_no")
    private Integer groupNo;

    @ApiModelProperty(value = "排序", dataType = "Integer")
    @TableField(value = "order_index")
    private Integer orderIndex;

    @ApiModelProperty(value = "内容（可能是输入的文字内容；可能是id，比如地区id；等等）", dataType = "String")
    @TableField(value = "content")
    private String content;

}
