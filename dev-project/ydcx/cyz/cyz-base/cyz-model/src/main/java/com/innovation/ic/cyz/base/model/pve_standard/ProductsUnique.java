package com.innovation.ic.cyz.base.model.pve_standard;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ProductsUnique", description = "产品唯一信息")
@TableName("ProductsUnique")
public class ProductsUnique {

    @ApiModelProperty(value = "唯一码", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "型号ID", dataType = "String")
    @TableField(value = "ProductID")
    private String productId;

    @ApiModelProperty(value = "生产状态 (未知)", dataType = "Integer")
    @TableField(value = "ProductionStatus")
    private Integer productionStatus;

    @ApiModelProperty(value = "MPQ", dataType = "Integer")
    @TableField(value = "MPQ")
    private Integer mpq;

    @ApiModelProperty(value = "封装", dataType = "String")
    @TableField(value = "Package")
    private String Package;

    @ApiModelProperty(value = "包装", dataType = "String")
    @TableField(value = "Packing")
    private String packing;

    @ApiModelProperty(value = "产品描述", dataType = "String")
    @TableField(value = "Description")
    private String description;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "Image")
    private String image;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField(value = "ModifyDate")
    private Date modifyDate;

    @ApiModelProperty(value = "状态", dataType = "Integer")
    @TableField(value = "Status")
    private Integer status;

    @ApiModelProperty(value = "来源ID （审批）", dataType = "String")
    @TableField(value = "FromID")
    private String fromId;

    @ApiModelProperty(value = "最小起订量", dataType = "Integer")
    @TableField(value = "MMOQ")
    private Integer mmoq;

}
