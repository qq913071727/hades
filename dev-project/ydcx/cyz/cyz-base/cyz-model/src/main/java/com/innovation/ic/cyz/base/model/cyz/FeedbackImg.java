package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "FeedbackImg", description = "信息反馈图片")
@TableName("feedback_img")
public class FeedbackImg {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "feedbackId", dataType = "Long")
    @TableField(value = "feedback_id")
    private Long feedbackId;

    @ApiModelProperty(value = "url", dataType = "String")
    @TableField(value = "url")
    private String url;

    @ApiModelProperty(value = "排序", dataType = "Integer")
    @TableField(value = "order_index")
    private Integer orderIndex;

    @ApiModelProperty(value = "状态(2-正常, 4-删除)", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;

}
