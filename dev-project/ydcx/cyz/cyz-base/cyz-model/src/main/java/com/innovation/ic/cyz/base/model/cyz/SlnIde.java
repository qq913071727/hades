package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SlnIde", description = "方案信息的开发平台")
@TableName("sln_ide")
public class SlnIde {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "SlnId", dataType = "Long")
    @TableField(value = "sln_id")
    private Long slnId;

    @ApiModelProperty(value = "开发平台名称", dataType = "String")
    @TableField(value = "cate_name")
    private String cateName;

}
