package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Tenant", description = "原厂数据")
@TableName("tenant")
public class Tenant {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "tenantsid", type = IdType.INPUT)
    private Integer tenantsid;

    @ApiModelProperty(value = "用户id", dataType = "Integer")
    @TableField(value = "memberid")
    private Integer memberid;

    @ApiModelProperty(value = "绑定后台管理员", dataType = "Integer")
    @TableField(value = "adminid")
    private Integer adminid;

    @ApiModelProperty(value = "用户名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "联系人", dataType = "String")
    @TableField(value = "contact")
    private String contact;

    @ApiModelProperty(value = "联系邮箱", dataType = "String")
    @TableField(value = "email")
    private String email;

    @ApiModelProperty(value = "电话", dataType = "String")
    @TableField(value = "mobile")
    private String mobile;

    @ApiModelProperty(value = "店铺名称", dataType = "String")
    @TableField(value = "shop_name")
    private String shop_name;

    @ApiModelProperty(value = "店铺邮箱", dataType = "String")
    @TableField(value = "shop_email")
    private String shop_email;

    @ApiModelProperty(value = "店铺电话", dataType = "String")
    @TableField(value = "shop_mobile")
    private String shop_mobile;

    @ApiModelProperty(value = "首字母，前端筛选时使用", dataType = "String")
    @TableField(value = "initials")
    private String initials;

    @ApiModelProperty(value = "网址", dataType = "String")
    @TableField(value = "url")
    private String url;

    @ApiModelProperty(value = "地址", dataType = "String")
    @TableField(value = "address")
    private String address;

    @ApiModelProperty(value = "黑名单1开启0未开启", dataType = "Boolean")
    @TableField(value = "black")
    private Boolean black;

    @ApiModelProperty(value = "广告位展示0未展示1展示", dataType = "Boolean")
    @TableField(value = "ad")
    private Boolean ad;

    @ApiModelProperty(value = "用户名", dataType = "String")
    @TableField(value = "username")
    private String username;

    @ApiModelProperty(value = "密码", dataType = "String")
    @TableField(value = "password")
    private String password;

    @ApiModelProperty(value = "代理权限1是0否", dataType = "Integer")
    @TableField(value = "permissions")
    private Integer permissions;

    @ApiModelProperty(value = "品牌", dataType = "Integer")
    @TableField(value = "brand")
    private Integer brand;

    @ApiModelProperty(value = "图片", dataType = "String")
    @TableField(value = "img")
    private String img;

    @ApiModelProperty(value = "公司简介", dataType = "String")
    @TableField(value = "description")
    private String description;

    @ApiModelProperty(value = "排序", dataType = "Integer")
    @TableField(value = "position")
    private Integer position;

    @ApiModelProperty(value = "装修风格", dataType = "Integer")
    @TableField(value = "style")
    private Integer style;

    @ApiModelProperty(value = "图文消息", dataType = "String")
    @TableField(value = "content")
    private String content;

}
