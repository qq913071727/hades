package com.innovation.ic.cyz.base.model.pve_standard;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Category", description = "分类表")
@TableName("Categories")
public class Category {

    @ApiModelProperty(value = "唯一码", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "父级ID", dataType = "String")
    @TableField(value = "FatherID")
    private String fatherId;

    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField(value = "Name")
    private String name;

    @ApiModelProperty(value = "编号", dataType = "String")
    @TableField(value = "Code")
    private String code;

    @ApiModelProperty(value = "序号", dataType = "Integer")
    @TableField(value = "Sort")
    private Integer sort;

    @ApiModelProperty(value = "备注", dataType = "String")
    @TableField(value = "Summary")
    private String summary;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField(value = "ModifyDate")
    private Date modifyDate;

    @ApiModelProperty(value = "状态", dataType = "Integer")
    @TableField(value = "State")
    private Integer state;

    @ApiModelProperty(value = "来源", dataType = "String")
    @TableField(value = "Source")
    private String source;

}
