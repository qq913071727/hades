package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   客户端信息
 * @author linuo
 * @time   2022年8月11日15:19:46
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Client", description = "客户端信息")
@TableName("client")
public class Client {
    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "客户端名称", dataType = "String")
    @TableField(value = "name")
    private String name;

//    @ApiModelProperty(value = "类型(1.erp9前端)", dataType = "String")
//    @TableField(value = "type_")
//    private String type;
}