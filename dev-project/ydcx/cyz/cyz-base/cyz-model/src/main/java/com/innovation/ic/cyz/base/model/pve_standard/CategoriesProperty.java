package com.innovation.ic.cyz.base.model.pve_standard;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sun.org.apache.xpath.internal.operations.Bool;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CategoriesProperty", description = "分类对应属性表")
@TableName("CategoriesProperty")
public class CategoriesProperty {

    @ApiModelProperty(value = "唯一码", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "分类ID", dataType = "String")
    @TableField(value = "CategoryID")
    private String categoryId;

    @ApiModelProperty(value = "属性ID", dataType = "String")
    @TableField(value = "PropertyID")
    private String propertyId;

    @ApiModelProperty(value = "属性名称", dataType = "String")
    @TableField(value = "PropertyName")
    private String propertyName;

    @ApiModelProperty(value = "分组ID", dataType = "String")
    @TableField(value = "GroupID")
    private String groupID;

    @ApiModelProperty(value = "值类型", dataType = "Integer")
    @TableField(value = "ValueType")
    private Integer valueType;

    @ApiModelProperty(value = "单位", dataType = "Integer")
    @TableField(value = "Unit")
    private Integer unit;

    @ApiModelProperty(value = "是否扩展", dataType = "Boolean")
    @TableField(value = "IsExpand")
    private Boolean isExpand;

    @ApiModelProperty(value = "是否用于搜索", dataType = "Boolean")
    @TableField(value = "IsSearch")
    private Boolean isSearch;

    @ApiModelProperty(value = "是否必填", dataType = "Boolean")
    @TableField(value = "IsRequired")
    private Boolean isRequired;

    @ApiModelProperty(value = "备注", dataType = "String")
    @TableField(value = "Summary")
    private String summary;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField(value = "ModifyDate")
    private Date modifyDate;

    @ApiModelProperty(value = "状态", dataType = "Integer")
    @TableField(value = "State")
    private Integer state;

}
