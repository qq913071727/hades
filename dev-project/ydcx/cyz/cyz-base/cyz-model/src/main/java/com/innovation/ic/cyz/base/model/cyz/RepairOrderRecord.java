package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderRecords", description = "工单流程记录表")
@TableName("repair_order_record")
public class RepairOrderRecord {
    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "工单id", dataType = "Long")
    @TableField(value = "repair_order_id")
    private Long repairOrderId;

    @ApiModelProperty(value = "流程记录", dataType = "String")
    @TableField(value = "record")
    private String record;

    @ApiModelProperty(value = "描述", dataType = "String")
    @TableField(value = "describe_")
    private String describe;

    @ApiModelProperty(value = "创建人", dataType = "String")
    @TableField(value = "create_user")
    private String createUser;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;
}