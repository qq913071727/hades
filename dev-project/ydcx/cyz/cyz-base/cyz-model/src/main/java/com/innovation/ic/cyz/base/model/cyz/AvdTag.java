package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AvdTag", description = "避坑指南标签")
@TableName("avd_tag")
public class AvdTag {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "标签", dataType = "String")
    @TableField(value = "tag")
    private String tag;

    @ApiModelProperty(value = "状态(2-正常, 4-删除)", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;

    @ApiModelProperty(value = "排序", dataType = "Integer")
    @TableField(value = "order_index")
    private Integer orderIndex;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    @TableField(value = "creator_id")
    private String creatorId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;

    @ApiModelProperty(value = "修改人id", dataType = "String")
    @TableField(value = "modifier_id")
    private String modifierId;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField(value = "modify_date")
    private Date modifyDate;

}
