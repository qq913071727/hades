package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @desc   品牌表实体类
 * @author linuo
 * @time   2022年10月28日10:15:41
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Brand", description = "品牌表")
@TableName("brand")
public class Brand {
    @ApiModelProperty(value = "主键", dataType = "String")
    private String id;

    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "中文名称", dataType = "String")
    @TableField(value = "cn_name")
    private String cnName;

    @ApiModelProperty(value = "英文名称", dataType = "String")
    @TableField(value = "en_name")
    private String enName;

    @ApiModelProperty(value = "简称", dataType = "String")
    @TableField(value = "short_name")
    private String shortName;

    @ApiModelProperty(value = "首字母", dataType = "String")
    @TableField(value = "letter")
    private String letter;

    @ApiModelProperty(value = "网址", dataType = "String")
    @TableField(value = "web_site")
    private String webSite;

    @ApiModelProperty(value = "生产商", dataType = "String")
    @TableField(value = "manufacture")
    private String manufacture;

    @ApiModelProperty(value = "是否代理(0否、1是)", dataType = "String")
    @TableField(value = "is_agent")
    private String isAgent;

    @ApiModelProperty(value = "备注", dataType = "String")
    @TableField(value = "summary")
    private String summary;

    @ApiModelProperty(value = "图标路径", dataType = "String")
    @TableField(value = "image")
    private String image;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField(value = "modify_date")
    private Date modifyDate;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    @TableField(value = "creator_id")
    private String creatorId;
}