package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SlnUrl", description = "方案信息中的Url")
@TableName("sln_url")
public class SlnUrl {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "SlnId", dataType = "Long")
    @TableField(value = "sln_id")
    private Long slnId;

    @ApiModelProperty(value = "类型(1-other_cover 其他封面, 2-file_path 方案文档)", dataType = "Integer")
    @TableField(value = "type")
    private Integer type;

    @ApiModelProperty(value = "url", dataType = "String")
    @TableField(value = "url")
    private String url;

}
