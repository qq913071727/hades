package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;

@Data
@ApiModel(value = "Region", description = "区域信息")
@TableName("region")
public class Region {
    @ApiModelProperty(value = "主键Id", dataType = "long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "上级Id", dataType = "long")
    @TableField(value = "parent_id")
    private Long parentId;

    @ApiModelProperty(value = "区域名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "级别。0表示全国，1表示省，2表示市，3表示区", dataType = "Integer")
    @TableField(value = "level")
    private Integer level;

    @ApiModelProperty(value = "排序", dataType = "Integer")
    @TableField(value = "order_index")
    private Integer orderIndex;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    @TableField(value = "creator_id")
    private String creatorId;

    @ApiModelProperty(value = "创建日期", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;

    @ApiModelProperty(value = "状态(2-正常，4-删除)", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;
}
