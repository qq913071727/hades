package com.innovation.ic.cyz.base.model.pve_standard;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Products", description = "产品表(型号)")
@TableName("Products")
public class Products {
    @ApiModelProperty(value = "唯一码", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "型号（产品名称）", dataType = "String")
    @TableField(value = "PartNumber")
    private String partNumber;

    @ApiModelProperty(value = "类别id", dataType = "String")
    @TableField(value = "CategoryID")
    private String categoryId;

    @ApiModelProperty(value = "类别组", dataType = "String")
    @TableField(value = "CategoryArr")
    private String categoryArr;

    @ApiModelProperty(value = "品牌id", dataType = "String")
    @TableField(value = "BrandID")
    private String brandId;

    @ApiModelProperty(value = "所属行业", dataType = "String")
    @TableField(value = "IndustryID")
    private String industryId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField(value = "ModifyDate")
    private Date modifyDate;

    @ApiModelProperty(value = "状态", dataType = "Integer")
    @TableField(value = "Status")
    private Integer status;

}
