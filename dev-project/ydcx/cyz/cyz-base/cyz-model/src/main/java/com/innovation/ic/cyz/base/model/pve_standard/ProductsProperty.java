package com.innovation.ic.cyz.base.model.pve_standard;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ProductsProperty", description = "商品属性信息表")
@TableName("ProductsProperty")
public class ProductsProperty {

    @ApiModelProperty(value = "唯一码", dataType = "String")
    @TableId(value = "ID")
    private String id;

    @ApiModelProperty(value = "属性ID", dataType = "String")
    @TableField(value = "PropertyID")
    private String propertyID;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "Name")
    private String name;

    @ApiModelProperty(value = "内容值", dataType = "String")
    @TableField(value = "Text")
    private String text;

    @ApiModelProperty(value = "数值", dataType = "Float")
    @TableField(value = "Value")
    private Float value;

    @ApiModelProperty(value = "实际单位", dataType = "String")
    @TableField(value = "Unit")
    private String unit;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField(value = "ModifyDate")
    private Date modifyDate;

}
