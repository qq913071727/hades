package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 器件选型类型
 */

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DeviceSelectionType", description = "器件选型类型")
@TableName("device_selection_type")
public class DeviceSelectionType {


    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "类别名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "类别名称", dataType = "String")
    private String levelName;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String creatorId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;

    @ApiModelProperty(value = "父类id", dataType = "String")
    private String fatherId;

    @ApiModelProperty(value = "是否是底层节点", dataType = "String")
    private Integer bottom;


}
