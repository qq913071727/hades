package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 器件选型类型
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DeviceSelectionParameter", description = "器件选型类型包含参数")
@TableName("device_selection_parameter")
public class DeviceSelectionParameter {


    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "类型 1、快速筛选 2、参数筛选", dataType = "Integer")
    private Integer type;


    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;



    @ApiModelProperty(value = "关联类型id", dataType = "String")
    private String deviceSelectionTypeId;


    @ApiModelProperty(value = "外部唯一id", dataType = "String")
    private String externallyUniqueId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createDate;


}
