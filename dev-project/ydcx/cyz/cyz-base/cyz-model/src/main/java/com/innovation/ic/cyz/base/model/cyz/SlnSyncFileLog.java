package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SlnSyncFileLog", description = "方案信息中同步文件的日志")
@TableName("sln_sync_file_log")
public class SlnSyncFileLog {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "SlnId", dataType = "Long")
    @TableField(value = "sln_id")
    private Long slnId;

    @ApiModelProperty(value = "同步文件日志类型", dataType = "Integer")
    @TableField(value = "type")
    private Integer type;

    @ApiModelProperty(value = "原url", dataType = "String")
    @TableField(value = "origin_url")
    private String originUrl;

    @ApiModelProperty(value = "新url", dataType = "String")
    @TableField(value = "new_url")
    private String newUrl;

    @ApiModelProperty(value = "错误信息", dataType = "String")
    @TableField(value = "error_msg")
    private String errorMsg;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;

}
