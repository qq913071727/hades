package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MfrParam", description = "smt/pcb厂家属性")
@TableName("mfr_param")
public class MfrParam {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "属性的一个数字标记", dataType = "Integer")
    @TableField(value = "module")
    private Integer module;

    @ApiModelProperty(value = "属性的一个重要标识（指导输出给前端的字段值类型；指导content内容如果保存的是id，从哪里获取实际内容；等等）", dataType = "Integer")
    @TableField(value = "type")
    private Integer type;

    @ApiModelProperty(value = "父id，指向该表id", dataType = "Long")
    @TableField(value = "parent_id")
    private Long parentId;

    @ApiModelProperty(value = "名称（可能有实际作用，需要被显示；也可能没有实际作用）", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "标识（比如给前端输出json中的key）", dataType = "String")
    @TableField(value = "code")
    private String code;

    @ApiModelProperty(value = "排序", dataType = "Integer")
    @TableField(value = "order_index")
    private Integer orderIndex;

    @ApiModelProperty(value = "状态(2-正常，4-删除)", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;

}
