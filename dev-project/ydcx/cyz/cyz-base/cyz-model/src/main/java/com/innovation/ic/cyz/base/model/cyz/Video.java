package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Video", description = "视频信息")
@TableName("video")
public class Video {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "标题", dataType = "String")
    @TableField(value = "title")
    private String title;

    @ApiModelProperty(value = "封面地址", dataType = "String")
    @TableField(value = "cover")
    private String cover;

    @ApiModelProperty(value = "视频地址", dataType = "String")
    @TableField(value = "video")
    private String video;

    @ApiModelProperty(value = "描述", dataType = "String")
    @TableField(value = "description")
    private String description;

    @ApiModelProperty(value = "正文", dataType = "String")
    @TableField(value = "paragraph")
    private String paragraph;

    @ApiModelProperty(value = "阅读量", dataType = "Integer")
    @TableField(value = "view_num")
    private Integer viewNum;

    @ApiModelProperty(value = "点赞量", dataType = "Integer")
    @TableField(value = "like_num")
    private Integer likeNum;

    @ApiModelProperty(value = "收藏量", dataType = "Integer")
    @TableField(value = "collect_num")
    private Integer collectNum;

    @ApiModelProperty(value = "审批状态", dataType = "Integer")
    @TableField(value = "approve_status")
    private Integer approveStatus;

    @ApiModelProperty(value = "状态(2-正常, 4-删除)", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    @TableField(value = "creator_id")
    private String creatorId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;

    @ApiModelProperty(value = "修改人id", dataType = "String")
    @TableField(value = "modifier_id")
    private String modifierId;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField(value = "modify_date")
    private Date modifyDate;
}