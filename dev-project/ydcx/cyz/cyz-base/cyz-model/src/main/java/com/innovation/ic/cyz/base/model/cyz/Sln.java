package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Sln", description = "方案信息")
@TableName("sln")
public class Sln {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "业务ID(BIB的业务主键，方案网业务主键)", dataType = "Long")
    @TableField(value = "sln_id")
    private Long slnId;

    @ApiModelProperty(value = "方案名称", dataType = "String")
    @TableField(value = "title")
    private String title;

    @ApiModelProperty(value = "方案价格", dataType = "Float")
    @TableField(value = "cash")
    private Float cash;

    @ApiModelProperty(value = "方案详情", dataType = "String")
    @TableField(value = "description")
    private String description;

    @ApiModelProperty(value = "方案封面", dataType = "String")
    @TableField(value = "goods_cover")
    private String goodsCover;

    @ApiModelProperty(value = "应用领域", dataType = "String")
    @TableField(value = "cate_name")
    private String cateName;

    @ApiModelProperty(value = "1比一比 2ICGOO 3虎趣 4IC交易网", dataType = "Integer")
    @TableField(value = "source")
    private Integer source;

    @ApiModelProperty(value = "交付形式", dataType = "String")
    @TableField(value = "delivery_cate_id_name")
    private String deliveryCateIdName;

    @ApiModelProperty(value = "性能参数", dataType = "String")
    @TableField(value = "performance_parameter")
    private String performanceParameter;

    @ApiModelProperty(value = "应用场景", dataType = "String")
    @TableField(value = "application_scene")
    private String applicationScene;

    @ApiModelProperty(value = "方案类型", dataType = "String")
    @TableField(value = "type_display")
    private String typeDisplay;

    @ApiModelProperty(value = "方案子名称", dataType = "String")
    @TableField(value = "subtitle")
    private String subtitle;

    @ApiModelProperty(value = "方案状态", dataType = "String")
    @TableField(value = "status_display")
    private String statusDisplay;

    @ApiModelProperty(value = "查看次数", dataType = "Integer")
    @TableField(value = "view_num")
    private Integer viewNum;

    @ApiModelProperty(value = "方案审核状态：1待审核 2审核通过 3审核不通过", dataType = "Integer")
    @TableField(value = "examine_status")
    private Integer examineStatus;

    @ApiModelProperty(value = "方案置顶状态： 0否 1是", dataType = "Integer")
    @TableField(value = "top_state")
    private Integer topState;

    @ApiModelProperty(value = "方案审核不通过原因", dataType = "Integer")
    @TableField(value = "examine_error_desc")
    private Integer examineErrorDesc;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "created_at")
    private Date createdAt;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "like_goods")
    private String likeGoods;

    @ApiModelProperty(value = "popularity_count", dataType = "Integer")
    @TableField(value = "popularity_count")
    private Integer popularityCount;

}
