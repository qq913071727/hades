package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderTypeHead", description = "工单分类负责人")
@TableName("repair_order_type_head")
public class RepairOrderTypeHead {
    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "真实姓名。erp9系统的Admins表的RealName字段", dataType = "String")
    @TableField(value = "real_name")
    private String realName;

    @ApiModelProperty(value = "账号。erp9系统的Admins表的UserName字段", dataType = "String")
    @TableField(value = "user_name")
    private String userName;

    @ApiModelProperty(value = "大赢家code。erp9系统的Admins表的DyjCode字段", dataType = "String")
    @TableField(value = "dyj_code")
    private String dyjCode;

    @ApiModelProperty(value = "电话号码。erp9系统的Staffs表的Mobile字段", dataType = "String")
    @TableField(value = "mobile")
    private String mobile;
}