package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderFile", description = "工单附件表")
@TableName("repair_order_file")
public class RepairOrderFile {
    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "工单id", dataType = "Long")
    @TableField(value = "repair_order_id")
    private Long repairOrderId;

    @ApiModelProperty(value = "工单流程id", dataType = "Long")
    @TableField(value = "repair_order_records_id")
    private Long repairOrderRecordsId;

    @ApiModelProperty(value = "文件名", dataType = "String")
    @TableField(value = "file_name")
    private String fileName;

    @ApiModelProperty(value = "文件路径", dataType = "String")
    @TableField(value = "file_path")
    private String filePath;

    @ApiModelProperty(value = "文件大小", dataType = "String")
    @TableField(value = "file_size")
    private String fileSize;

    @ApiModelProperty(value = "文件类型(1：文本、2：图片)", dataType = "Integer")
    @TableField(value = "file_type")
    private Integer fileType;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    @TableField(value = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建人", dataType = "String")
    @TableField(value = "create_user")
    private String createUser;

    @ApiModelProperty(value = "更新人id", dataType = "String")
    @TableField(value = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新人", dataType = "String")
    @TableField(value = "update_user")
    private String updateUser;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间", dataType = "Date")
    @TableField(value = "update_time")
    private Date updateTime;
}