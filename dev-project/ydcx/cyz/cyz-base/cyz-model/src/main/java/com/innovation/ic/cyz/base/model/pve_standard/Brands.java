package com.innovation.ic.cyz.base.model.pve_standard;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Brands", description = "品牌")
@TableName("Brands")
public class Brands {
    @ApiModelProperty(value = "唯一码", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "Name")
    private String name;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "CNName")
    private String CNName;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "ENName")
    private String ENName;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "ShortName")
    private String shortName;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "Letter")
    private String letter;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "WebSite")
    private String webSite;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "Manufacturer")
    private String manufacturer;

    @ApiModelProperty(value = "", dataType = "Boolean")
    @TableField(value = "IsAgent")
    private Boolean isAgent;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "FromID")
    private String fromID;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "Summary")
    private String summary;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "Image")
    private String image;

    @ApiModelProperty(value = "", dataType = "Date")
    @TableField(value = "CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "", dataType = "Date")
    @TableField(value = "ModifyDate")
    private Date modifyDate;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "CreatorID")
    private String creatorID;

    @ApiModelProperty(value = "", dataType = "Integer")
    @TableField(value = "Status")
    private Integer status;

    @ApiModelProperty(value = "", dataType = "String")
    @TableField(value = "Source")
    private String source;

}
