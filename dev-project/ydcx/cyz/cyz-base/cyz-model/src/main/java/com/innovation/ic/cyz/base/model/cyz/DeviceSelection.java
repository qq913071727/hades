package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 器件选型类型
 */

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DeviceSelection", description = "器件选型类型")
@TableName("device_selection")
public class DeviceSelection {


    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "类别名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String creatorId;


    @ApiModelProperty(value = "图片路径", dataType = "String")
    private String picturePath;


    @ApiModelProperty(value = "图片尺寸", dataType = "String")
    private String size;



    @ApiModelProperty(value = "排序编号", dataType = "Integer")
    private Integer sort;



    @ApiModelProperty(value = "是否显示,0为未启用  1为启用", dataType = "String")
    private Integer enable;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;




}
