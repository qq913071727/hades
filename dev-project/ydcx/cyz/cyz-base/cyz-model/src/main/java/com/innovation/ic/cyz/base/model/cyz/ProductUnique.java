package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @desc   产品唯一信息表实体类
 * @author linuo
 * @time   2022年11月1日13:47:47
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ProductUnique", description = "产品唯一信息")
@TableName("product_unique")
public class ProductUnique {
    @ApiModelProperty(value = "主键id", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "型号id", dataType = "String")
    @TableField(value = "product_id")
    private String productId;

    @ApiModelProperty(value = "生产状态", dataType = "Integer")
    @TableField(value = "production_status")
    private Integer productionStatus;

    @ApiModelProperty(value = "MPQ", dataType = "Integer")
    @TableField(value = "mpq")
    private Integer mpq;

    @ApiModelProperty(value = "封装", dataType = "String")
    @TableField(value = "package_")
    private String package_;

    @ApiModelProperty(value = "包装", dataType = "String")
    @TableField(value = "packing")
    private String packing;

    @ApiModelProperty(value = "产品描述", dataType = "String")
    @TableField(value = "description")
    private String description;

    @ApiModelProperty(value = "图片路径", dataType = "String")
    @TableField(value = "image")
    private String image;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createDate;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField(value = "modify_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date modifyDate;

    @ApiModelProperty(value = "最小起订量", dataType = "Integer")
    @TableField(value = "mmoq")
    private Integer mmoq;
}