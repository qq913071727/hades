package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VidRel", description = "视频关系表")
@TableName("vid_rel")
public class VidRel {
    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "关系类别(1-分类, 2-标签, 3-品牌，4-内容类型)", dataType = "Integer")
    @TableField(value = "module")
    private Integer module;

    @ApiModelProperty(value = "VidId", dataType = "Long")
    @TableField(value = "vid_id")
    private Long vidId;

    @ApiModelProperty(value = "分类/标签/品牌 的id", dataType = "Long")
    @TableField(value = "vid_rel_id")
    private Long vidRelId;
}
