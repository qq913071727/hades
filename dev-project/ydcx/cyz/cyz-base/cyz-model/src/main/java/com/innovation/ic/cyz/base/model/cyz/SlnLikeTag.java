package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SlnLikeTag", description = "方案信息中的方案标签")
@TableName("sln_like_tag")
public class SlnLikeTag {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "SlnId", dataType = "Long")
    @TableField(value = "sln_id")
    private Long slnId;

    @ApiModelProperty(value = "标签", dataType = "String")
    @TableField(value = "tag")
    private String tag;

}
