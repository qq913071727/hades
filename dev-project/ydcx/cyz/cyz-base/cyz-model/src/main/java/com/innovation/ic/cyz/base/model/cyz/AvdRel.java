package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AvdRel", description = "避坑指南关系表")
@TableName("avd_rel")
public class AvdRel {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "关系类别(1-分类, 2-标签, 3-品牌)", dataType = "Integer")
    @TableField(value = "module")
    private Integer module;

    @ApiModelProperty(value = "AvdId", dataType = "Long")
    @TableField(value = "avd_id")
    private Long avdId;

    @ApiModelProperty(value = "分类/标签/品牌 的id", dataType = "Long")
    @TableField(value = "avd_rel_id")
    private Long avdRelId;

}
