package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ContentType", description = "文件的扩展名和content-type的对应关系")
@TableName("content_type")
public class ContentType {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "文件后缀名", dataType = "String")
    @TableField(value = "file_ext")
    private String fileExt;

    @ApiModelProperty(value = "content_type", dataType = "String")
    @TableField(value = "content_type")
    private String contentType;

}
