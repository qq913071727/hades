package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Banner", description = "首頁Banner")
@TableName("banner")
public class Banner {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "图片路径", dataType = "String")
    @TableField(value = "picture_path")
    private String picturePath;

    @ApiModelProperty(value = "尺寸", dataType = "String")
    @TableField(value = "size")
    private String size;

    @ApiModelProperty(value = "是否启用。1表示启用，0表示未启用", dataType = "Integer")
    @TableField(value = "available")
    private Integer available;

    @ApiModelProperty(value = "二级页面路径", dataType = "String")
    @TableField(value = "secondary_page_path")
    private String secondaryPagePath;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    @TableField(value = "creator_id")
    private String creatorId;

    @ApiModelProperty(value = "最后修改人id", dataType = "String")
    @TableField(value = "modifier_id")
    private String modifierId;

    @ApiModelProperty(value = "创建时间", dataType = "String")
    @TableField(value = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "修改时间", dataType = "String")
    @TableField(value = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "图片", dataType = "String")
    @TableField(exist = false)
    private String picture;
}
