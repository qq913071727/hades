package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Mfr", description = "smt/pcb厂家")
@TableName("mfr")
public class Mfr {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "模块(1-smt，2-pcb)", dataType = "Integer")
    @TableField(value = "module")
    private Integer module;

    @ApiModelProperty(value = "状态(2-正常，4-删除)", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;

    @ApiModelProperty(value = "创建人id", dataType = "Integer")
    @TableField(value = "creator_id")
    private Integer creator_id;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    private Date create_date;

    @ApiModelProperty(value = "修改人id", dataType = "Integer")
    @TableField(value = "modifier_id")
    private Integer modifier_id;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField(value = "modify_date")
    private Date modify_date;

}
