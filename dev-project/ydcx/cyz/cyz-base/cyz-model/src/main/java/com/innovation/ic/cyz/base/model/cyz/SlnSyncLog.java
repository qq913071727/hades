package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SlnSyncLog", description = "方案信息同步记录")
@TableName("sln_sync_log")
public class SlnSyncLog {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "接口返回的count", dataType = "Integer")
    @TableField(value = "count")
    private Integer count;

    @ApiModelProperty(value = "接口返回的next", dataType = "String")
    @TableField(value = "next")
    private String next;

    @ApiModelProperty(value = "接口返回的previous", dataType = "String")
    @TableField(value = "previous")
    private String previous;

    @ApiModelProperty(value = "接口调用成功时间", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;

}
