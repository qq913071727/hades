package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Carousel", description = "首页轮播图")
@TableName("carousel")
public class Carousel {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "显示文字", dataType = "String")
    @TableField(value = "text")
    private String text;

    @ApiModelProperty(value = "图片url", dataType = "String")
    @TableField(value = "img")
    private String img;

    @ApiModelProperty(value = "类型(传递到器件选型页面，先设置为字符串类型)", dataType = "String")
    @TableField(value = "type")
    private String type;

    @ApiModelProperty(value = "排序", dataType = "Integer")
    @TableField(value = "order_index")
    private Integer orderIndex;

    @ApiModelProperty(value = "状态(2-正常, 4-删除)", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    @TableField(value = "creator_id")
    private String creator_id;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    private Date create_date;

    @ApiModelProperty(value = "修改人id", dataType = "String")
    @TableField(value = "modifier_id")
    private String modifier_id;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField(value = "modify_date")
    private Date modify_date;

}
