package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "categories", description = "器件选型基础数据")
@TableName("categories")
public class Categories {


    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;


    @ApiModelProperty(value = "父类id", dataType = "String")
    @TableField(value = "father_id")
    private String fatherId;


    @ApiModelProperty(value = "名称", dataType = "String")
    @TableField(value = "name")
    private String name;


    @ApiModelProperty(value = "编号", dataType = "String")
    @TableField(value = "code")
    private String code;


    @ApiModelProperty(value = "排序", dataType = "Integer")
    @TableField(value = "sort")
    private Integer sort;


    @ApiModelProperty(value = "备注", dataType = "String")
    @TableField(value = "summary")
    private String summary;


    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;


    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField(value = "modify_date")
    private Date modifyDate;


    @ApiModelProperty(value = "状态", dataType = "Integer")
    @TableField(value = "state")
    private Integer state;


    @ApiModelProperty(value = "来源", dataType = "String")
    @TableField(value = "source")
    private String source;

}
