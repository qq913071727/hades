package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrder", description = "工单表")
@TableName("repair_order")
public class RepairOrder {
    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "主题", dataType = "String")
    @TableField(value = "topic")
    private String topic;

    @ApiModelProperty(value = "描述", dataType = "String")
    @TableField(value = "describe_")
    private String describe;

    @ApiModelProperty(value = "优先级(1：普通、2：低、3：高、4：紧急、5：立刻)", dataType = "Integer")
    @TableField(value = "priority")
    private Integer priority;

    @ApiModelProperty(value = "一级工单类型id", dataType = "Long")
    @TableField(value = "first_type_id")
    private Long firstTypeId;

    @ApiModelProperty(value = "工单类型id", dataType = "Long")
    @TableField(value = "type_id")
    private Long typeId;

    @ApiModelProperty(value = "负责人", dataType = "String")
    @TableField(value = "head_user")
    private String headUser;

    @ApiModelProperty(value = "负责人姓名", dataType = "String")
    @TableField(value = "head_username")
    private String headUsername;

    @ApiModelProperty(value = "处理人", dataType = "String")
    @TableField(value = "deal_user")
    private String dealUser;

    @ApiModelProperty(value = "处理人姓名", dataType = "String")
    @TableField(value = "deal_username")
    private String dealUsername;

    @ApiModelProperty(value = "创建人", dataType = "String")
    @TableField(value = "create_user")
    private String createUser;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人", dataType = "String")
    @TableField(value = "update_user")
    private String updateUser;

    @ApiModelProperty(value = "更新时间", dataType = "Date")
    @TableField(value = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "工单状态(0:待解决、1:已回复、2:已解决、3:延期处理、4:已关闭)", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;

    @ApiModelProperty(value = "来源(1虎趣、2比一比)", dataType = "Integer")
    @TableField(value = "source")
    private Integer source;

    @ApiModelProperty(value = "虎趣工单id", dataType = "Long")
    @TableField(value = "hq_id")
    private Long hqId;

    @ApiModelProperty(value = "提交用户姓名", dataType = "String")
    @TableField(value = "submit_username")
    private String submitUsername;
}