package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.github.jeffreyning.mybatisplus.anno.MppMultiId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @desc   商品属性信息表实体类
 * @author linuo
 * @time   2022年11月3日09:32:38
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ProductProperty", description = "商品属性信息表")
@TableName("product_property")
public class ProductProperty {
    @ApiModelProperty(value = "主键id", dataType = "String")
    @MppMultiId
    @TableField(value = "id")
    private String id;

    @ApiModelProperty(value = "属性id", dataType = "String")
    @MppMultiId
    @TableField(value = "property_id")
    private String propertyId;

    @ApiModelProperty(value = "属性", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "内容值", dataType = "String")
    @TableField(value = "text")
    private String text;

    @ApiModelProperty(value = "数值", dataType = "BigDecimal")
    @TableField(value = "value")
    private BigDecimal value;

    @ApiModelProperty(value = "实际单位", dataType = "String")
    @TableField(value = "unit")
    private String unit;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createDate;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField(value = "modify_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date modifyDate;
}