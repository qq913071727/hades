package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderType", description = "工单分类表")
@TableName("repair_order_type")
public class RepairOrderType {
    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "分类名称", dataType = "String")
    @TableField(value = "class_name")
    private String className;

    @ApiModelProperty(value = "分类级别", dataType = "Integer")
    @TableField(value = "class_level")
    private Integer classLevel;

    @ApiModelProperty(value = "负责人id", dataType = "String")
    @TableField(value = "head_id")
    private String headId;

    @ApiModelProperty(value = "父级分类id", dataType = "String")
    @TableField(value = "parent_order_id")
    private Long parentOrderId;
}