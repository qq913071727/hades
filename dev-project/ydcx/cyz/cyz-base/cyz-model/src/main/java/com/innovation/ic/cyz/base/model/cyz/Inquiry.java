package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Inquiry", description = "在线询价保存")
@TableName("inquiry")
public class Inquiry {

    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @ApiModelProperty(value = "MfrId", dataType = "Long")
    @TableField(value = "mfr_id")
    private Long mfrId;

    @ApiModelProperty(value = "公司名称", dataType = "String")
    @TableField(value = "company_name")
    private String companyName;

    @ApiModelProperty(value = "姓名", dataType = "String")
    @TableField(value = "user_name")
    private String userName;

    @ApiModelProperty(value = "手机号", dataType = "String")
    @TableField(value = "tel")
    private String tel;

    @ApiModelProperty(value = "是否已经导出。1表示已导出，0表示未导出", dataType = "Integer")
    @TableField(value = "export_status")
    private Integer exportStatus;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_time")
    private Date createTime;

}
