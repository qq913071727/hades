package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "User", description = "用户表")
@TableName("user")
public class User {
    @ApiModelProperty(value = "主键, 用户id", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "用户名", dataType = "String")
    @TableField(value = "username")
    private String username;

    @ApiModelProperty(value = "密码", dataType = "String")
    @TableField(value = "password")
    private String password;

    @ApiModelProperty(value = "姓名", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "昵称", dataType = "String")
    @TableField(value = "nickname")
    private String nickname;

    @ApiModelProperty(value = "头像", dataType = "String")
    @TableField(value = "avatar")
    private String avatar;

    @ApiModelProperty(value = "手机号", dataType = "String")
    @TableField(value = "mobile")
    private String mobile;

    @ApiModelProperty(value = "职位", dataType = "String")
    @TableField(value = "position")
    private String position;

    @ApiModelProperty(value = "工作年限", dataType = "Integer")
    @TableField(value = "work_year")
    private Integer workYear;

    @ApiModelProperty(value = "用户来源:1 比一比 2 ic交易网", dataType = "Integer")
    @TableField(value = "source")
    private Integer source;

    @ApiModelProperty(value = "个人简介", dataType = "String")
    @TableField(value = "resume")
    private String resume;

    @ApiModelProperty(value = "积分", dataType = "Integer")
    @TableField(value = "point")
    private Integer point;

    @ApiModelProperty(value = "状态(2-正常, 4-删除)", dataType = "Integer")
    @TableField(value = "status")
    private Integer status;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    @TableField(value = "creator_id")
    private String creatorId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;
}