package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "TenantImg", description = "原厂数据相关图片")
@TableName("tenant_img")
public class TenantImg {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "tenant_img_id", type = IdType.INPUT)
    private String tenant_img_id;

    @ApiModelProperty(value = "Tenant的id", dataType = "Integer")
    @TableField(value = "tenantsid")
    private Integer tenantsid;

    @ApiModelProperty(value = "类型(1-轮播图, 2-公司录播图)", dataType = "Integer")
    @TableField(value = "type")
    private Integer type;

    @ApiModelProperty(value = "图片url", dataType = "String")
    @TableField(value = "url")
    private String url;
    
}
