package com.innovation.ic.cyz.base.model.pve_standard;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PcFiles", description = "文件")
@TableName("PcFiles")
public class PcFiles {
    @ApiModelProperty(value = "唯一码", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "主要id", dataType = "String")
    @TableField(value = "MainID")
    private String mainId;

    @ApiModelProperty(value = "文件类型", dataType = "Integer")
    @TableField(value = "Type")
    private Integer type;

    @ApiModelProperty(value = "文件原有名称", dataType = "String")
    @TableField(value = "CustomName")
    private String customName;

    @ApiModelProperty(value = "文件地址", dataType = "String")
    @TableField(value = "Url")
    private String url;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "CreateDate")
    private Date createDate;

    @ApiModelProperty(value = "上传人", dataType = "String")
    @TableField(value = "AdminID")
    private String adminId;

    @ApiModelProperty(value = "网站上传人", dataType = "String")
    @TableField(value = "SiteuserID")
    private String siteuserId;
}