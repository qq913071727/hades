package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CategoriesProperty", description = "器件选型配置属性")
@TableName("categories_property")
public class CategoriesProperty {


    @ApiModelProperty(value = "主键", dataType = "Long")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;


    @ApiModelProperty(value = "分类ID", dataType = "String")
    @TableField(value = "category_id")
    private String categoryId;


    @ApiModelProperty(value = "属性ID", dataType = "String")
    @TableField(value = "property_id")
    private String propertyId;


    @ApiModelProperty(value = "属性名字", dataType = "String")
    @TableField(value = "property_name")
    private String propertyName;


    @ApiModelProperty(value = "分组ID", dataType = "String")
    @TableField(value = "group_id")
    private String groupId;


    @ApiModelProperty(value = "值类型", dataType = "Integer")
    @TableField(value = "value_type")
    private Integer valueType;

    @ApiModelProperty(value = "单位", dataType = "Integer")
    @TableField(value = "unit")
    private Integer unit;

    @ApiModelProperty(value = "是否扩展", dataType = "Integer")
    @TableField(value = "is_expand")
    private Boolean isExpand;

    @ApiModelProperty(value = "是否用于搜索", dataType = "Integer")
    @TableField(value = "is_search")
    private Boolean isSearch;

    @ApiModelProperty(value = "是否必填", dataType = "Integer")
    @TableField(value = "is_required")
    private Boolean isRequired;

    @ApiModelProperty(value = "备注", dataType = "String")
    @TableField(value = "summary")
    private String summary;


    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;


    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @TableField(value = "modify_date")
    private Date modifyDate;


    @ApiModelProperty(value = "状态", dataType = "Integer")
    @TableField(value = "state")
    private Integer state;


}
