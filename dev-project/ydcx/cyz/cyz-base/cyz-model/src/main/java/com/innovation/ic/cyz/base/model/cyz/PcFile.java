package com.innovation.ic.cyz.base.model.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @desc   文件实体类
 * @author swq
 * @time   2022年11月23日15:27:58
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Product", description = "品牌表")
@TableName("pc_file")
public class PcFile {

    @ApiModelProperty(value = "主键", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "主要id", dataType = "String")
    @TableField(value = "main_id")
    private String mainId;

    @ApiModelProperty(value = "文件类型(100 DataSheet、200 媒体文件、300 设计资源、400 代理证、500 pdfurl、600 origin_pdfurl、700 封装、800 包装、 999 其他)", dataType = "Integer")
    @TableField(value = "type_")
    private Integer type;

    @ApiModelProperty(value = "文件原有名称", dataType = "String")
    @TableField(value = "custom_name")
    private String customName;

    @ApiModelProperty(value = "文件地址", dataType = "String")
    @TableField(value = "url")
    private String url;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @TableField(value = "create_date")
    private Date createDate;

    @ApiModelProperty(value = "上传人", dataType = "String")
    @TableField(value = "admin_id")
    private String adminId;

    @ApiModelProperty(value = "网站上传人", dataType = "String")
    @TableField(value = "siteuser_id")
    private String siteuserId;

    @ApiModelProperty(value = "辅助查询", dataType = "Integer")
    @TableField(value = "sort")
    private Integer sort;
}
