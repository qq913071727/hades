package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.innovation.ic.cyz.base.mapper.cyz.VidBrandMapper;
import com.innovation.ic.cyz.base.model.cyz.VidBrand;
import com.innovation.ic.cyz.base.service.cyz.VidBrandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class VidBrandServiceImpl extends ServiceImpl<VidBrandMapper, VidBrand> implements VidBrandService {


    @Override
    public List<VidBrand> findByPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        QueryWrapper<VidBrand> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("id");
        return this.baseMapper.selectList(queryWrapper);
    }
}
