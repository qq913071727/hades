package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.DeviceSelectionParameter;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_parameter.DeviceSelectionParameterListRespPojo;
import com.innovation.ic.cyz.base.vo.cyz.deviceselection.DeviceSelectionParameterVo;
import java.util.List;
import java.util.Map;

public interface DeviceSelectionParameterService {
    void insertBatch(List<DeviceSelectionParameterVo> deviceSelectionParameters);

    void insert(DeviceSelectionParameter deviceSelectionParameter);

    void deleteByTypeId(String typeId);

    List<DeviceSelectionParameter> selectByMap(Map<String, Object> param);

    /**
     * 通过器件选型类型id查询器件选型类型包含参数
     * @param typeId 器件选型类型id
     * @return 返回查询结果
     */
    ServiceResult<DeviceSelectionParameterListRespPojo> findDeviceSelectionParameterByTypeId(String typeId);

    /**
     * 通过器件选型类型id获取器件选型类型包含参数id集合
     * @param categoryId 器件选型类型id
     * @param type 类型 1、快速筛选 2、参数筛选
     * @return 返回查询结果
     */
    ServiceResult<List<String>> getExternallyUniqueIdListByCategoryId(String categoryId, Integer type);

    List<DeviceSelectionParameter> findByPage(int pageNum, int pageSize);

    Integer selectCount();
}