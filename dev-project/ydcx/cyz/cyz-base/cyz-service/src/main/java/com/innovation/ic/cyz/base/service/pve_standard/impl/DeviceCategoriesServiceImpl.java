package com.innovation.ic.cyz.base.service.pve_standard.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.pve_standard.CategoriesPropertyMapper;
import com.innovation.ic.cyz.base.mapper.pve_standard.CategoryMapper;
import com.innovation.ic.cyz.base.model.pve_standard.CategoriesProperty;
import com.innovation.ic.cyz.base.model.pve_standard.Category;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionTypePojo;
import com.innovation.ic.cyz.base.service.pve_standard.DeviceCategoriesService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DeviceCategoriesServiceImpl extends ServiceImpl<CategoryMapper, Category> implements DeviceCategoriesService {

    @Resource
    private CategoriesPropertyMapper categoriesPropertyMapper;

    @Override
    public ServiceResult<List<Category>> findByCategories(Map<String, Object> param) {
        ServiceResult<List<Category>> serviceResult = new ServiceResult();
        if (CollectionUtils.isEmpty(param)) {
            return null;
        }
        param.put("State", 200);
        List<Category> categories = this.baseMapper.selectByMap(param);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(categories);
        return serviceResult;
    }

    @Override
    public ServiceResult<List<Category>> categoriesQueryWrapper(QueryWrapper<Category> queryWrapper) {
        ServiceResult<List<Category>> serviceResult = new ServiceResult();
        List<Category> categories = this.baseMapper.selectList(queryWrapper);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(categories);
        return null;
    }

    @Override
    public ServiceResult<List<DeviceSelectionTypePojo>> findByDeviceSelectionType() {
        ServiceResult<List<DeviceSelectionTypePojo>> serviceResult = new ServiceResult<>();
        //找到根节点
        Map<String, Object> param = new HashMap<>();
        param.put("FatherID", null);
        param.put("State", 200);
        List<Category> categoriesRoot = this.baseMapper.selectByMap(param);
        //找到所有子节点
        QueryWrapper<Category> queryWrapper = new QueryWrapper();
        queryWrapper.isNotNull("FatherID");
        queryWrapper.eq("State", 200);
        List<Category> categoriesChild = this.baseMapper.selectList(queryWrapper);
        if (CollectionUtils.isEmpty(categoriesRoot)) {
            return serviceResult;
        }
        if (CollectionUtils.isEmpty(categoriesChild)) {
            return serviceResult;
        }
        categoriesChild.addAll(categoriesRoot);
        List<DeviceSelectionTypePojo> deviceSelectionTypePojoList = new ArrayList<>();
        categoriesChild.stream().forEach(e -> deviceSelectionTypePojoList.add(this.bindDeviceSelectionTypePojo(e)));
        List<DeviceSelectionTypePojo> deviceSelectionTypePojos = this.buildDeviceSelectionTypePojoTree(deviceSelectionTypePojoList, categoriesRoot.get(0).getId());
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(deviceSelectionTypePojos);
        return serviceResult;
    }


    @Override
    public ServiceResult<List<CategoriesProperty>> findByCategoriesPropertyIds(List<String> ids) {
        ServiceResult<List<CategoriesProperty>> serviceResult = new ServiceResult<>();
        if (CollectionUtils.isEmpty(ids)) {
            return serviceResult;
        }
        List<CategoriesProperty> categoriesProperties = categoriesPropertyMapper.selectBatchIds(ids);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(categoriesProperties);
        return serviceResult;
    }

    @Override
    public Category findById(String externalUniqueId) {
        return this.baseMapper.selectById(externalUniqueId);
    }

    @Override
    public List<Category> findByPage(int pageNum, int pageSize) {
        return this.baseMapper.findByPage(pageNum * pageSize, pageSize);
    }

    @Override
    public Integer effectiveData() {
        QueryWrapper<Category> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("State",200);
        return this.baseMapper.selectCount(queryWrapper);
    }


    /**
     * 绑定器件选型参数
     *
     * @param category
     * @return
     */
    private DeviceSelectionTypePojo bindDeviceSelectionTypePojo(Category category) {
        DeviceSelectionTypePojo deviceSelectionTypePojo = new DeviceSelectionTypePojo();
        if (category == null) {
            return null;
        }
        deviceSelectionTypePojo.setFatherId(category.getFatherId());
        deviceSelectionTypePojo.setId(category.getId());
        deviceSelectionTypePojo.setName(category.getName());
        return deviceSelectionTypePojo;
    }


    /**
     * 递归遍历成树结构
     *
     * @param treeList
     * @param id
     * @return
     */
    private List<DeviceSelectionTypePojo> buildDeviceSelectionTypePojoTree(List<DeviceSelectionTypePojo> treeList, String id) {
        List<DeviceSelectionTypePojo> children = new ArrayList<>();
        for (DeviceSelectionTypePojo deviceSelectionTypePojo : treeList) {
            if (id.equals(deviceSelectionTypePojo.getFatherId())) {
                deviceSelectionTypePojo.setChild(buildDeviceSelectionTypePojoTree(treeList, deviceSelectionTypePojo.getId()));
                children.add(deviceSelectionTypePojo);
            }
        }
        return children;
    }


}
