package com.innovation.ic.cyz.base.service.pve_standard;

import com.innovation.ic.cyz.base.pojo.global.ApiPageResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_parameter.DeviceSelectionParameterQueryListRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_category.PsCategoryResultPojo;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_productsunique.PsProdsUniqPojo;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_productsunique.PsProductQueryByBrandPartRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property.PsCategoryPropertyPojo;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property.PsPropertyResultPojo;
import com.innovation.ic.cyz.base.vo.cyz.deviceSelectionParameter.DeviceSelectionParameterFuzzyQueryVo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsPartNumbersQueryByContextVo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsProdsUniqVo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsProductQueryByIdVo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_property.PsPropertyVo;
import java.util.List;

/**
 * 器件选型接口
 */
public interface ProductSearchService {
    /**
     * test1
     */
    ServiceResult test1();

    /**
     * 查询器件选型分类
     */
    ServiceResult<PsCategoryResultPojo> getPsCategories();

    /**
     * 根据分类查询备选属性值
     */
    ServiceResult<PsPropertyResultPojo> getPsProperties(PsPropertyVo psPropertyVo);

    /**
     * 查询筛选参数
     * @param psPropertyVo 器件选型属性的vo类
     * @param externallyUniqueIdList 器件选型类型包含参数id集合
     * @return 返回查询结果
     */
    ServiceResult<DeviceSelectionParameterQueryListRespPojo> getSelectProperties(PsPropertyVo psPropertyVo, List<String> externallyUniqueIdList);

    /**
     * 查询器件选型列表
     *
     * @return
     */
    ServiceResult<ApiPageResult<PsProdsUniqPojo>> getPsProducts(PsProdsUniqVo param);

    /**
     * 根据查询器件选型列表
     *
     * @return
     */
    ServiceResult<List<PsProdsUniqPojo>> getPsProductsByUids(String[] ids);

    /**
     * 获取分类属性
     *
     * @return
     */
    ServiceResult<List<PsCategoryPropertyPojo>> getPsCategoriesProperty(String categoryId);

    /**
     * 参数筛选模糊查询
     * @param deviceSelectionParameterFuzzyQueryVo 参数筛选模糊查询的vo类
     * @return 返回查询结果
     */
    ServiceResult<List<String>> queryFuzzyParamSelectProperties(DeviceSelectionParameterFuzzyQueryVo deviceSelectionParameterFuzzyQueryVo);

    /**
     * 根据品牌、型号查询器件
     * @param param 查询条件
     * @return 返回查询结果
     */
    ServiceResult<PsProductQueryByBrandPartRespPojo> queryProductsByBrandPartNumber(PsProductQueryByIdVo param);

    /**
     * 型号模糊查询
     * @param psPartNumbersQueryByContextVo 型号模糊查询的vo类
     * @return 返回查询结果
     */
    ServiceResult<List<String>> queryPartNumbersByContext(PsPartNumbersQueryByContextVo psPartNumbersQueryByContextVo);
}