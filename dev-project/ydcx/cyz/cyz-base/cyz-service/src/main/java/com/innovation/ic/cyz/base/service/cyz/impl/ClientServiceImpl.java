package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.cyz.ClientMapper;
import com.innovation.ic.cyz.base.model.cyz.Client;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.service.cyz.ClientService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

/**
 * @desc   Client的具体实现类
 * @author linuo
 * @time   2022年9月15日17:33:23
 */
@Service
@Transactional
public class ClientServiceImpl extends ServiceImpl<ClientMapper, Client> implements ClientService {
    @Resource
    private ClientMapper clientMapper;

    /**
     * 查找client表中所有数据
     * @return 返回client表中所有数据
     */
    @Override
    public ServiceResult<List<Client>> findAll() {
        ServiceResult<List<Client>> serviceResult = new ServiceResult<>();
        List<Client> clientList = clientMapper.selectList(null);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(clientList);
        return serviceResult;
    }
}