package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.RepairOrderRecord;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;

/**
 * 工单记录接口
 */
public interface RepairOrderRecordService {
    /**
     * 根据工单id查询最近一条流程记录
     * @param id 工单id
     * @return 返回查询结果
     */
    ServiceResult<RepairOrderRecord> selectLastRecordByOrderId(Long id);
}