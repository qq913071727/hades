package com.innovation.ic.cyz.base.service.cyz;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.innovation.ic.cyz.base.model.cyz.Inquiry;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.vo.cyz.InquiryMiniSaveVo;

import java.util.List;

/**
 * 在线询价使用接口
 */
public interface InquiryService {

    /**
     * 在线询价提交
     *
     * @param inquiryMiniSaveVo
     * @return
     */
    ServiceResult miniSave(InquiryMiniSaveVo inquiryMiniSaveVo);

    /**
     * 分页显示
     * @param pageSize
     * @param pageNo
     * @return
     */
    ServiceResult<Page<Inquiry>> page(Integer pageSize, Integer pageNo);

    /**
     * 根据id，删除一条记录
     * @param id
     * @return
     */
    ServiceResult<Boolean> deleteById(Long id);

    /**
     * 查询所有记录，只返回excel需要的列。然后将所有记录的export_status字段更新为1
     * @return
     */
    ServiceResult<List<Object[]>> findAllRecordForExcel();

    /**
     * 查询所有未导出过记录，只返回excel需要的列。然后将所有记录的export_status字段更新为1
     * @return
     */
    ServiceResult<List<Object[]>> findNotExportRecordForExcel();

}
