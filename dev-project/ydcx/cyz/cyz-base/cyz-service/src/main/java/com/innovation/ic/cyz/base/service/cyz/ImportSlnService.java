package com.innovation.ic.cyz.base.service.cyz;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.cyz.base.model.cyz.Sln;
import com.innovation.ic.cyz.base.model.cyz.SlnUrl;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.sln_import.ImportSlnOnePagePojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.sln_import.ImportSlnUrlResultPojo;

/**
 * 获取方案数据服务类接口
 */
public interface ImportSlnService {

    /**
     *  ioc容器中SlnService默认的实现类，SpringContextUtil通过次属性获取bean
     */
    String IMPORT_SLN_SERVICE_IMPL_BEAN_NAME = "importSlnServiceImpl";

    /**
     * 获取ApiUrl
     *
     * @return
     */
    ServiceResult<ImportSlnUrlResultPojo> getApiUrl();

    /**
     * 获取ApiUrl补偿数据
     *
     * @return
     */
    ServiceResult<ImportSlnUrlResultPojo> getApiUrlAndCompensation();

    /**
     * 从接口获取一页的方案信息
     *
     * @param url
     * @return
     */
    ServiceResult<ImportSlnOnePagePojo> getOnePageSlnsFromApi(String url);

    /**
     * 从接口获取一页的方案信息
     *
     * @param url
     * @return
     */
    public ServiceResult<ImportSlnOnePagePojo> getOnePageSlnsFromApi(JSONObject obj);

    /**
     * 保存一页的方案信息数据
     *
     * @return
     */
    ServiceResult saveOnePage(ImportSlnOnePagePojo importSlnOnePagePojo);

    /**
     * 测试1
     */
    ServiceResult test1(String id);

    /**
     * 下载一个Sln的所有图片
     */
    ServiceResult downloadImageAll(Long slnId);


    /**
     * @Description: 下载并上传封面图片
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/1913:56
     * @return
     */
    String downloadImageGoodsCover(Sln sln);


        /**
     * @Description: 下载并上传封面图片
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/1913:56
     * @return
     */
    String downloadImageGoodsCoverByThread(Sln sln);

    /**
     * @Description: 下载其他封面
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/1616:53
     * @param slnUrl
     */
    String downloadImageOtherCover(SlnUrl slnUrl);

    /**
     * @Description: 下载其他封面
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/1616:53
     * @param slnUrl
     */
    String downloadImageOtherCoverByThread(SlnUrl slnUrl);


    /**
     * @Description: 删除无效的数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/2010:43
     */
    void deleteInvalidData();

    /**
     * @Description: 拉取数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/2816:58
     */
    void pullSlnInfo();

    void synSlnData2Redis();
}
