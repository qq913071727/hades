package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.rabbit_mq.FilesPojo;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 工单附件接口
 */
public interface RepairOrderFileService {

    /**
     * 删除工单附件
     * @param id 工单附件id
     * @return 返回删除结果
     */
    ServiceResult<Boolean> delete(Long id);

    /**
     * 工单附件下载
     * @param id 工单附件id
     * @return 返回附件内容
     */
    ServiceResult<Boolean> download(Long id, HttpServletResponse response) throws IOException;

    /**
     * 通过文件地址删除工单附件
     * @param filePath 文件地址
     * @return 返回删除结果
     */
    ServiceResult<Boolean> deleteByFilePath(String filePath);

    /**
     * 根据流程记录id查询附件地址集合
     * @param id 流程记录id
     * @return 返回查询结果
     */
    ServiceResult<List<FilesPojo>> selectFilesByRecordId(Long id);
}