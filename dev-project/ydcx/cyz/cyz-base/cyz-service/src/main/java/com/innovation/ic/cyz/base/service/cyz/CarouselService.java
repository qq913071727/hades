package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.Carousel;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.carousel.CarouselListPojo;

import java.util.List;

/**
 * 轮播图接口
 */
public interface CarouselService {

    /**
     * 查询轮播图列表
     */
    ServiceResult<List<CarouselListPojo>> findCarouselList();

    /**
     * 查询轮播图所有数据
     */
    ServiceResult<List<Carousel>> findCarouselListData();
}
