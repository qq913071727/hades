package com.innovation.ic.cyz.base.service.pve_standard;

import com.innovation.ic.cyz.base.model.cyz.ProductProperty;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import java.util.List;

/**
 * @desc   ProductsProperty表接口
 * @author linuo
 * @time   2022年11月3日09:43:36
 */
public interface ProductsPropertyService {
    /**
     * 查询标准库ProductsUnique表状态是正常的数据数量
     * @return 返回查询结果
     */
    ServiceResult<Long> getEffectiveDataCount();

    /**
     * 分页查询ProductsProperty表中有效数据
     * @param start 分页开始
     * @param end 分页结束
     * @return 返回查询结果
     */
    ServiceResult<List<ProductProperty>> getEffectiveDataByPage(long start, long end);
}