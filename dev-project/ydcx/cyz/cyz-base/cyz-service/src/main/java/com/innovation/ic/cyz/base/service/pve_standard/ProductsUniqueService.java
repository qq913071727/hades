package com.innovation.ic.cyz.base.service.pve_standard;

import com.innovation.ic.cyz.base.model.cyz.ProductUnique;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import java.util.List;

/**
 * @desc   ProductsUnique表接口
 * @author linuo
 * @time   2022年11月1日14:39:44
 */
public interface ProductsUniqueService {
    /**
     * 分页查询ProductsUnique表中状态为正常的数据
     * @param start 分页开始
     * @param end 分页结束
     * @return 返回查询结果
     */
    ServiceResult<List<ProductUnique>> getNormalStatusDataByPage(long start, long end);

    /**
     * 查询标准库ProductsUnique表状态是正常的数据数量
     * @return 返回查询结果
     */
    ServiceResult<Long> getNormalStatusDataCount();
}