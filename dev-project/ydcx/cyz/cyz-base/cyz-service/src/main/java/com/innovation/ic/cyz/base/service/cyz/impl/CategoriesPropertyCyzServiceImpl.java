package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.cyz.CategoriesPropertyCyzMapper;
import com.innovation.ic.cyz.base.model.cyz.CategoriesProperty;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.PsCategoryPropertyPojo;
import com.innovation.ic.cyz.base.service.cyz.CategoriesPropertyCyzService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class CategoriesPropertyCyzServiceImpl extends ServiceImpl<CategoriesPropertyCyzMapper, CategoriesProperty> implements CategoriesPropertyCyzService {
    @Resource
    private CategoriesPropertyCyzMapper categoriesPropertyCyzMapper;

    @Override
    public void batchSave(List<CategoriesProperty> categoriesList) {
        if (CollectionUtils.isEmpty(categoriesList)) {
            return;
        }
        this.baseMapper.insertBatchSomeColumn(categoriesList);
    }

    @Override
    public void truncatetCategoriesProperty() {
        this.baseMapper.truncatetCategoriesProperty();
    }


    //获取件选型配置属性
    @Override
    public ServiceResult<List<PsCategoryPropertyPojo>> selectCategoriesProperty(List<String> categoriesList){
        ServiceResult<List<PsCategoryPropertyPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setResult(categoriesPropertyCyzMapper.selectCategoriesProperty(categoriesList));
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 获取分类属性
     *
     * @return
     * @param categoriesList
     */
    @Override
    public ServiceResult<List<PsCategoryPropertyPojo>> getPsCategoriesProperty(List<String> categoriesList){
        ServiceResult<List<PsCategoryPropertyPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setResult(categoriesPropertyCyzMapper.getPsCategoriesProperty(categoriesList));
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

}
