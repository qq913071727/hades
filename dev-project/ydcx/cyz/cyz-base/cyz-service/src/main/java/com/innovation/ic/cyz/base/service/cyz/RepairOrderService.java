package com.innovation.ic.cyz.base.service.cyz;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.cyz.base.model.cyz.RepairOrder;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order.RepairOrderAdminListRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order.RepairOrderInfoRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order.RepairOrderWebListRespPojo;
import com.innovation.ic.cyz.base.vo.cyz.repairOrder.RepairOrderAddVo;
import com.innovation.ic.cyz.base.vo.cyz.repairOrder.RepairOrderQueryListVo;
import com.innovation.ic.cyz.base.vo.cyz.repairOrder.RepairOrderReplyVo;
import com.innovation.ic.cyz.base.vo.cyz.repairOrder.RepairOrderSolveVo;
import java.util.Map;

/**
 * @desc   工单接口
 * @author linuo
 * @time   2022年9月14日09:44:33
 */
public interface RepairOrderService {
    /**
     * 新增工单
     * @param repairOrderAddVo 工单新增的vo类
     * @param userId 登录用户id
     * @return 返回新增结果
     */
    ServiceResult<Boolean> addRepairOrder(RepairOrderAddVo repairOrderAddVo, String userId);

    /**
     * 工单回复
     * @param repairOrderReplyVo 工单回复的vo类
     * @param userInfo 登录用户信息
     * @param userId 登录用户id
     * @return 返回回复结果
     */
    ServiceResult<Boolean> replyRepairOrder(RepairOrderReplyVo repairOrderReplyVo, Map<String, String> userInfo, String userId);

    /**
     * 工单列表查询
     * @param repairOrderQueryListVo 工单列表查询的vo类
     * @return 返回工单查询结果
     */
    ServiceResult<RepairOrderAdminListRespPojo> queryRepairOrderList(RepairOrderQueryListVo repairOrderQueryListVo);

    /**
     * 根据工单id获取工单内容
     * @param id 工单id
     * @return 返回工单内容
     */
    ServiceResult<RepairOrderInfoRespPojo> getRepairOrderInfoById(Long id);

    /**
     * 查询我的工单列表
     * @param userId 登录用户id
     * @return 返回我的工单列表数据
     */
    ServiceResult<RepairOrderWebListRespPojo> queryMyRepairOrder(Integer pageNo, Integer pageSize, String userId);

    /**
     * 工单解决
     * @param repairOrderSolveVo 工单解决的vo类
     * @param userInfo 登录用户信息
     * @return 返回处理结果
     */
    ServiceResult<Boolean> solveRepairOrder(RepairOrderSolveVo repairOrderSolveVo, Map<String, String> userInfo);

    /**
     * 工单指派
     * @param orderId 工单id
     * @param headId FAE的id
     * @param userInfo 登录用户信息
     * @return 返回处理结果
     */
    ServiceResult<Boolean> assign(Long orderId, Long headId, Map<String, String> userInfo);

    /**
     * 处理虎趣添加工单的mq消息
     * @param json mq消息
     * @return 返回结果
     */
    ServiceResult<Boolean> handleHqAddRepairOrderMqMsg(JSONObject json);

    /**
     * 处理虎趣回复工单的mq消息
     * @param json mq消息
     * @return 返回结果
     */
    ServiceResult<Boolean> handleHqReplyRepairOrderMqMsg(JSONObject json);

    /**
     * 根据id获取工单信息
     * @param id 工单id
     * @return 返回查询结果
     */
    ServiceResult<RepairOrder> selectById(Long id);
}