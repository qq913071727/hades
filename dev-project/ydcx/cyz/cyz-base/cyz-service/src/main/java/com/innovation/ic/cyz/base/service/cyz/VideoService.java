package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.Video;
import com.innovation.ic.cyz.base.pojo.global.ApiPageResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.video.*;
import com.innovation.ic.cyz.base.vo.cyz.video.VidApproveVo;
import com.innovation.ic.cyz.base.vo.cyz.video.VidListVo;
import com.innovation.ic.cyz.base.vo.cyz.video.VideoAddVo;

import java.util.List;

/**
 * 视频相关接口
 */
public interface VideoService {
    /**
     * 获取视频类别
     *
     * @return
     */
    ServiceResult<List<VidCategoryPojo>> getCategories();

    /**
     * 获取视频品牌
     *
     * @return
     */
    ServiceResult<List<VidBrandPojo>> getBrands();

    /**
     * 获取视频内容类型
     *
     * @return
     */
    ServiceResult<List<VidTypePojo>> getTypes();

    /**
     * 获取视频标签
     *
     * @return
     */
    ServiceResult<List<VidTagPojo>> getTags();

    /**
     * 获取视频列表
     *
     * @return
     */
    ServiceResult<ApiPageResult<VidListPojo>> getList(VidListVo param, Integer pageIndex, Integer pageSize);

    /**
     * 获取视频详情
     *
     * @param id
     * @return
     */
    ServiceResult<VidDetailPojo> getDetail(Long id);

    /**
     * 新增
     * @param video
     * @param userId
     * @return
     */
    ServiceResult<Long> add(VideoAddVo video, String userId);

    /**
     * 修改
     * @param video
     * @param userId
     * @return
     */
    ServiceResult<Long> edit(VideoAddVo video, String userId);

    /**
     * 视频审批
     *
     * @param vo
     * @return
     */
    ServiceResult approved(VidApproveVo vo);

    List<Video> findByPage(int pageNum, int pageSize);
}
