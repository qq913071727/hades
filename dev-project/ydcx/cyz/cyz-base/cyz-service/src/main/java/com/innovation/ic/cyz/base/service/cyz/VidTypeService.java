package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.VidType;

import java.util.List;

public interface VidTypeService {
    List<VidType> findByPage(int pageNum, int pageSize);
}
