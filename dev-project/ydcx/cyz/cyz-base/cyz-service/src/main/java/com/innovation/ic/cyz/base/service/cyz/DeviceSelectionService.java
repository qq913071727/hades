package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.Categories;
import com.innovation.ic.cyz.base.model.cyz.DeviceSelection;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;

import com.innovation.ic.cyz.base.pojo.variable.cyz.PsCategoryPropertyPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionChildrenParamPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionTypePojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.UploadImagePojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionTypeQueryRespPojo;
import com.innovation.ic.cyz.base.vo.cyz.AddDeviceSelectionToConfigureVo;
import com.innovation.ic.cyz.base.vo.cyz.deviceselection.AddDeviceSelectionParamVo;
import com.innovation.ic.cyz.base.vo.cyz.deviceselection.AddDeviceSelectionTypeVo;
import java.util.List;

public interface DeviceSelectionService {
    ServiceResult addDeviceSelection(AddDeviceSelectionParamVo addDeviceSelectionParamVo);

    ServiceResult addDeviceSelectionType(Categories categories, AddDeviceSelectionTypeVo addDeviceSelectionTypeVo);

    ServiceResult<List<DeviceSelectionTypePojo>> findByDeviceSelectionToConfigure();

    ServiceResult addDeviceSelectionToConfigure(AddDeviceSelectionToConfigureVo addDeviceSelectionToConfigureVo);

    ServiceResult deleteDeviceSelectionType(String id);

    ServiceResult<List<PsCategoryPropertyPojo>> getPsCategoriesProperty(String categoryId, List<PsCategoryPropertyPojo> psCategoryPropertyPojoList);

    ServiceResult updateDeviceSelectionTypePicture(UploadImagePojo uploadImagePojo);

    /**
     * 查询器件选型类别
     * @return 返回查询结果
     */
    ServiceResult<List<DeviceSelectionTypeQueryRespPojo>> findDeviceSelection();

    /**
     * 查询器件选型菜单
     * @return 返回查询结果
     */
    ServiceResult<List<DeviceSelectionChildrenParamPojo>> findDeviceSelectionMenu();

    List<DeviceSelection> findByPage(int pageNum, int pageSize);

    Integer selectCount();
}