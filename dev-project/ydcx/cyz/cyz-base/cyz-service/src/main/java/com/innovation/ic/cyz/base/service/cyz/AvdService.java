package com.innovation.ic.cyz.base.service.cyz;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cyz.base.model.cyz.AvdTag;
import com.innovation.ic.cyz.base.model.cyz.AvdType;
import com.innovation.ic.cyz.base.model.cyz.UserViewLog;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.*;
import com.innovation.ic.cyz.base.vo.cyz.avd.*;

import java.util.List;

/**
 * 避坑指南接口
 */
public interface AvdService {

    /**
     * 新增保存
     * @param avdSaveVo
     * @param userId
     * @return
     */
    ServiceResult<AvdSavePojo> insertSave(AvdSaveVo avdSaveVo, String userId);

    /**
     * 编辑保存
     * @param avdSaveVo
     * @param avdId
     * @param userId
     * @return
     */
    ServiceResult<AvdSavePojo> updateSave(AvdSaveVo avdSaveVo, Long avdId, String userId);

    /**
     * 编辑选项
     *
     * @return
     */
    ServiceResult<AvdEditOptionsPojo> editOptions();

    /**
     * 根据传入的条件查询避坑指南列表
     * @param avdListVo
     * @param userId
     * @return
     */
    ServiceResult<AvdListResultPojo> findAvdList(AvdListVo avdListVo, String userId);

    /**
     * 根据id查询避坑指南详情
     *
     * @param avdId
     * @return
     */
    ServiceResult<AvdDetailPojo> findAvdDetailById(Long avdId);

    /**
     * 添加用户浏览记录
     *
     * @return
     */
    ServiceResult userViewLogAdd(UserViewLog userViewLog);

    /**
     * 减少用户点赞等记录
     *
     * @param userViewLogReduceVo
     * @return
     */
    ServiceResult userViewLogReduce(UserViewLogReduceVo userViewLogReduceVo);

    /**
     * 避坑指南审批通过
     *
     * @return
     */
    ServiceResult approveOk(AvdApproveVo avdApproveVo);

    /**
     * @Description: 获取类别下拉列表
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2614:10
     */
    ServiceResult<List<AvdType>> getAvdTypes();

    /**
     * @Description: 获取标签下拉菜单
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2614:12
     */
    ServiceResult<List<AvdTag>> getAvdTags();

    /**
     * @Description: 分页查看避坑指南列表
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2614:36
     */
    ServiceResult<PageInfo<AvdListPojo>> page(Integer pageNo, Integer pageSize, Long typeId, Integer module, String userName, String createDate);

    void selectAvdAndSetRedisTask();

    void synAvdData2Redis();
}
