package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.Banner;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.banner.BannerAvailableDataRespPojo;
import com.innovation.ic.cyz.base.vo.cyz.BennerVo;
import com.jcraft.jsch.SftpException;
import java.io.IOException;
import java.util.List;

/**
 * Banner图接口
 */
public interface BannerService {


    /**
     * 添加Bannner
     * @param bennerVo
     * @return
     */
    ServiceResult<ApiResult<Boolean>> addBannner(BennerVo bennerVo) throws IOException, SftpException;

    /**
     * 编辑Banner
     * @param bennerVo
     * @return
     */
    ServiceResult<ApiResult<Boolean>> updateBanner(BennerVo bennerVo) throws IOException, SftpException;

    /**
     * 通过id主键删除Banner
     * @param id
     * @return
     */
    void deleteBanner(Long id) throws SftpException;

    /**
     * 修改Banner状态
     * @param bennerVo
     * @return
     */
    void updateAvailable(BennerVo bennerVo);

    /**
     * Bannner列表
     * @param bennerVo
     * @return
     */
    ServiceResult<List<Banner>> allBanner(BennerVo bennerVo);

    /**
     * 获取启用的轮播图数据
     * @return 返回查询结果
     */
    ServiceResult<List<BannerAvailableDataRespPojo>> getAvailableBannerList();

    /**
     * 通过id查询banner数据
     * @param bennerVo
     * @return 返回查询结果
     */
    ServiceResult<Banner> selectIdBannner(BennerVo bennerVo);
}