package com.innovation.ic.cyz.base.service.cyz.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.framework.util.*;
import com.innovation.ic.cyz.base.model.cyz.*;
import com.innovation.ic.cyz.base.pojo.constant.DownloadSlnFileType;
import com.innovation.ic.cyz.base.pojo.constant.SlnSyncFileLogType;
import com.innovation.ic.cyz.base.pojo.enums.RedisKeyPrefixEnum;
import com.innovation.ic.cyz.base.pojo.enums.SlnSourceEnum;
import com.innovation.ic.cyz.base.pojo.variable.cyz.SlnListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.sln_import.*;
import com.innovation.ic.cyz.base.mapper.cyz.*;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.constant.SlnUrlType;
import com.innovation.ic.cyz.base.pojo.enums.ExamineStatusEnum;
import com.innovation.ic.cyz.base.service.cyz.ImportSlnService;
import com.jcraft.jsch.SftpException;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.reactive.function.client.WebClient;
import javax.annotation.Resource;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 获取方案数据具体实现类
 */
@Service
@Transactional
@Slf4j
public class ImportSlnServiceImpl extends ServiceImpl<SlnMapper, Sln> implements ImportSlnService {

    @Resource
    private ServiceHelper serviceHelper;



    final String SLN_KEY = "sln:maxSlnId";

    /**
     * 将字符串数组的JSONArray转换为List<String>
     *
     * @param arr
     * @return
     */
    private List<String> convertJSONArrayToStringList(JSONArray arr) {
        if (arr == null || arr.isEmpty()) {
            return new ArrayList<>();
        }

        List<String> stringList = new ArrayList<>();
        for (int i = 0; i < arr.size(); i++) {
            stringList.add((String) arr.get(i));
        }
        return stringList;
    }

    /**
     * 获取ApiUrl
     *
     * @return
     */
    @Override
    public ServiceResult<ImportSlnUrlResultPojo> getApiUrl() {
        String baseUrl = serviceHelper.getSlnApiConfig().getProgramme();
        ImportSlnUrlResultPojo result = new ImportSlnUrlResultPojo();

        QueryWrapper<SlnSyncLog> qwSlnSyncLog = new QueryWrapper<>();
        Date currentDayBeginTime = DateUtils.getCurrentDayBeginTime();
        Date nextDayBeginTime = DateUtils.getNextDayBeginTime();
        qwSlnSyncLog.lambda().ge(SlnSyncLog::getCreateDate, currentDayBeginTime).lt(SlnSyncLog::getCreateDate, nextDayBeginTime);
        qwSlnSyncLog.select().orderByDesc("create_date");
        qwSlnSyncLog.last("limit 0,1");
        SlnSyncLog latestSlnSyncLog = serviceHelper.getSlnSyncLogMapper().selectOne(qwSlnSyncLog);
        if (latestSlnSyncLog == null) {
            // 今天还未执行过
            result.setIsTodayFinish(false);
            result.setNextUrl(baseUrl);
        } else if (latestSlnSyncLog.getNext() == null || latestSlnSyncLog.getNext().isEmpty()) {
            // 今天已经执行到最后一页
            result.setIsTodayFinish(true);
            result.setNextUrl(baseUrl);
        } else {
            // 今天执行到中间页码, 继续下一页
            result.setIsTodayFinish(false);
            result.setNextUrl(latestSlnSyncLog.getNext());
        }

        ServiceResult<ImportSlnUrlResultPojo> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);

        return serviceResult;
    }


    @Override
    public ServiceResult<ImportSlnUrlResultPojo> getApiUrlAndCompensation() {
        String baseUrl = serviceHelper.getSlnApiConfig().getProgramme();
        ImportSlnUrlResultPojo result = new ImportSlnUrlResultPojo();

        QueryWrapper<SlnSyncLog> qwSlnSyncLog = new QueryWrapper<>();
        Date currentDayBeginTime = DateUtils.getCurrentDayBeginTime();
        Date nextDayBeginTime = DateUtils.getNextDayBeginTime();
        qwSlnSyncLog.lambda().ge(SlnSyncLog::getCreateDate, currentDayBeginTime).lt(SlnSyncLog::getCreateDate, nextDayBeginTime);
        qwSlnSyncLog.select().orderByDesc("create_date");
        qwSlnSyncLog.last("limit 0,1");
        SlnSyncLog latestSlnSyncLog = serviceHelper.getSlnSyncLogMapper().selectOne(qwSlnSyncLog);
        if (latestSlnSyncLog == null) {
            // 今天还未执行过
            result.setIsTodayFinish(false);
            result.setNextUrl(baseUrl);
        } else if (latestSlnSyncLog.getNext() == null || latestSlnSyncLog.getNext().isEmpty()) {
            // 今天已经执行到最后一页
            result.setIsTodayFinish(true);
            result.setNextUrl(baseUrl);
        } else {
            // 今天执行到中间页码, 继续下一页
            result.setIsTodayFinish(false);
            result.setNextUrl(latestSlnSyncLog.getNext());
        }

        ServiceResult<ImportSlnUrlResultPojo> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);

        return serviceResult;
    }

    @Override
    public ServiceResult<ImportSlnOnePagePojo> getOnePageSlnsFromApi(String url) {
        return this.getOnePageSlnsFromApi(JSONObject.parseObject(this.getApiResutl(url)));
    }

    /**
     * 从接口获取一页的方案信息
     *
     * @param obj
     * @return
     */
    @Override
    public ServiceResult<ImportSlnOnePagePojo> getOnePageSlnsFromApi(JSONObject obj) {
        ImportSlnOnePagePojo result = new ImportSlnOnePagePojo();
        try {
            Integer count = obj.getInteger("count");
            String next = obj.getString("next");
            String previous = obj.getString("previous");

            // 获取sln数据
            List<ImportSlnOneDataPojo> dataInPage = new ArrayList<>();

            JSONArray arr = obj.getJSONArray("results");
            String js = JSONObject.toJSONString(arr, SerializerFeature.WriteClassName);
            List<Map<String, Object>> slnData = (List<Map<String, Object>>) JSONObject.parseObject(js, List.class);

            for (int i = 0; i < slnData.size(); i++) {
                Sln sln = new Sln();
                List<SlnUrl> slnUrlList = new ArrayList<>();
                List<SlnLikeTag> slnLikeTagList = new ArrayList<>();

                JSONObject oneApiData = (JSONObject) slnData.get(i);

                Long slnId = oneApiData.getLong("id");
                sln.setSlnId(slnId);
                sln.setTitle(oneApiData.getString("title"));
                sln.setCash(oneApiData.getFloat("cash"));
                sln.setDescription(oneApiData.getString("desc"));
                sln.setGoodsCover(oneApiData.getString("goods_cover"));
                sln.setCateName(oneApiData.getString("cate_name"));
                sln.setDeliveryCateIdName(oneApiData.getString("delivery_cate_id_name"));
                sln.setPerformanceParameter(oneApiData.getString("performance_parameter"));
                sln.setApplicationScene(oneApiData.getString("application_scene"));
                sln.setTypeDisplay(oneApiData.getString("type_display"));
                sln.setSubtitle(oneApiData.getString("subtitle"));
                sln.setStatusDisplay(oneApiData.getString("status_display"));
                sln.setViewNum(oneApiData.getInteger("view_num"));
                sln.setCreatedAt(oneApiData.getDate("created_at"));
                sln.setSource(SlnSourceEnum.WAFAW.getKey());
                //上传图片并设置封面
                sln.setGoodsCover(downloadImageGoodsCoverByThread(sln));

                // 其他封面
                JSONArray otherCoverArr = oneApiData.getJSONArray("other_cover");
                var otherCoverStringList = convertJSONArrayToStringList(otherCoverArr);
                var otherCovers = otherCoverStringList.stream().map(url -> {
                    SlnUrl slnUrl = new SlnUrl();
                    slnUrl.setId(IdUtil.snowflakeNextId());
                    slnUrl.setSlnId(slnId);
                    slnUrl.setType(SlnUrlType.OtherCover);
                    slnUrl.setUrl(url);
                    // 其他封面
                    slnUrl.setUrl(downloadImageOtherCoverByThread(slnUrl));
                    return slnUrl;
                }).collect(Collectors.toList());
                slnUrlList.addAll(otherCovers);

                // 方案文档
                JSONArray filePathArr = oneApiData.getJSONArray("file_path");
                var filePathStringList = convertJSONArrayToStringList(filePathArr);
                var filePaths = filePathStringList.stream().map(url -> {
                    SlnUrl slnUrl = new SlnUrl();
                    slnUrl.setId(IdUtil.snowflakeNextId());
                    slnUrl.setSlnId(slnId);
                    slnUrl.setType(SlnUrlType.FilePath);
                    slnUrl.setUrl(url);
                    return slnUrl;
                }).collect(Collectors.toList());
                slnUrlList.addAll(filePaths);

                // 方案标签
                JSONArray likeTagsArr = oneApiData.getJSONArray("like_tags");
                var likeTagStringList = convertJSONArrayToStringList(likeTagsArr);
                var likeTags = likeTagStringList.stream().map(tag -> {
                    SlnLikeTag slnLikeTag = new SlnLikeTag();
                    slnLikeTag.setId(IdUtil.snowflakeNextId());
                    slnLikeTag.setSlnId(slnId);
                    slnLikeTag.setTag(tag);
                    return slnLikeTag;
                }).collect(Collectors.toList());
                slnLikeTagList.addAll(likeTags);

                // 开发平台
                List<String> slnIdeStringList = new ArrayList<>();
                String ideCateNameString = oneApiData.getString("ide_cate_name");
                if (ideCateNameString != null && !ideCateNameString.isEmpty()) {
                    slnIdeStringList = Arrays.asList(ideCateNameString.split(","));
                }
                var slnIdes = slnIdeStringList.stream()
                        .filter(t -> t != null && !t.isEmpty())
                        .map(ide -> {
                            ide = ide.trim();

                            SlnIde slnIde = new SlnIde();
                            slnIde.setId(IdUtil.snowflakeNextId());
                            slnIde.setSlnId(slnId);
                            slnIde.setCateName(ide);
                            return slnIde;
                        }).collect(Collectors.toList());

                // 放到 ImportSlnOneDataPojo 中
                ImportSlnOneDataPojo oneData = new ImportSlnOneDataPojo();
                oneData.setSln(sln);
                oneData.setSlnUrls(slnUrlList);
                oneData.setSlnLikeTags(slnLikeTagList);
                oneData.setSlnIdes(slnIdes);

                dataInPage.add(oneData);
            }

            result.setCount(count);
            result.setNext(next);
            result.setPrevious(previous);
            result.setResults(dataInPage);

        } catch (Exception e) {
            e.printStackTrace();
            ServiceResult<ImportSlnOnePagePojo> serviceResult = new ServiceResult<>();
            serviceResult.setMessage(ServiceResult.SELECT_FAIL);
            serviceResult.setSuccess(Boolean.FALSE);
            serviceResult.setMessage(e.getMessage());
            return serviceResult;
        }

        ServiceResult<ImportSlnOnePagePojo> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);

        return serviceResult;
    }

    /**
     * 保存一页的方案信息数据
     *
     * @param importSlnOnePagePojo
     * @return
     */
    @Override
    public ServiceResult saveOnePage(ImportSlnOnePagePojo importSlnOnePagePojo) {
        SlnSyncLog slnSyncLog = new SlnSyncLog();
        slnSyncLog.setId(IdUtil.snowflakeNextId());
        slnSyncLog.setCount(importSlnOnePagePojo.getCount());
        slnSyncLog.setNext(importSlnOnePagePojo.getNext());
        slnSyncLog.setPrevious(importSlnOnePagePojo.getPrevious());
        slnSyncLog.setCreateDate(new Date());
        serviceHelper.getSlnSyncLogMapper().insert(slnSyncLog);

        for (ImportSlnOneDataPojo result : importSlnOnePagePojo.getResults()) {
            // Sln
            Sln sln = result.getSln();
            sln.setExamineStatus(ExamineStatusEnum.EXAMINATION_PASSED.getKey());//审核通过
            sln.setStatusDisplay(ExamineStatusEnum.EXAMINATION_PASSED.getVal());
            sln.setSource(SlnSourceEnum.WAFAW.getKey());
            Sln existSln = serviceHelper.getSlnMapper().selectById(sln.getId());
            if (existSln == null) {
                serviceHelper.getSlnMapper().insert(sln);
            } else {
                serviceHelper.getSlnMapper().updateById(sln);
            }

            // SlnUrl
            QueryWrapper<SlnUrl> qwSlnUrl = new QueryWrapper<>();
            qwSlnUrl.lambda().eq(SlnUrl::getSlnId, sln.getId());
            serviceHelper.getSlnUrlMapper().delete(qwSlnUrl);

            var newSlnUrls = result.getSlnUrls();
            if (newSlnUrls != null && !newSlnUrls.isEmpty()) {
                serviceHelper.getSlnUrlMapper().insertBatchSomeColumn(newSlnUrls);
            }

            // SlnLikeTag
            QueryWrapper<SlnLikeTag> qwSlnLikeTag = new QueryWrapper<>();
            qwSlnLikeTag.lambda().eq(SlnLikeTag::getSlnId, sln.getId());
            serviceHelper.getSlnLikeTagMapper().delete(qwSlnLikeTag);

            var newSlnLikeTags = result.getSlnLikeTags();
            if (newSlnLikeTags != null && !newSlnLikeTags.isEmpty()) {
                serviceHelper.getSlnLikeTagMapper().insertBatchSomeColumn(newSlnLikeTags);
            }

            // SlnIde
            QueryWrapper<SlnIde> qwSlnIde = new QueryWrapper<>();
            qwSlnIde.lambda().eq(SlnIde::getSlnId, sln.getId());
            serviceHelper.getSlnIdeMapper().delete(qwSlnIde);

            var newSlnIdes = result.getSlnIdes();
            if (newSlnIdes != null && !newSlnIdes.isEmpty()) {
                serviceHelper.getSlnIdeMapper().insertBatchSomeColumn(newSlnIdes);
            }
        }

        ServiceResult<ImportSlnOnePagePojo> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);

        return serviceResult;
    }

    // 下载文件 begin

    /**
     * 测试1
     */
    @Override
    public ServiceResult test1(String id) {

        downloadFilePath(Long.valueOf(id));

//        downloadImageGoodsCover(Long.valueOf(id));
//        downloadImageOtherCover(Long.valueOf(id));
//        downloadImageDescription(Long.valueOf(id));

        ServiceResult serviceResult = new ServiceResult();
        serviceResult.setSuccess(true);
        return serviceResult;
    }

    /**
     * 下载一个Sln的所有图片
     */
    @Override
    public ServiceResult downloadImageAll(Long slnId) {
        // 下载 方案封面
        downloadImageGoodsCover(slnId);
        // 下载 其他封面
        downloadImageOtherCover(slnId);
        // 下载 方案详情
        downloadImageDescription(slnId);
        // 下载 方案文档
        downloadFilePath(slnId);

        ServiceResult serviceResult = new ServiceResult();
        serviceResult.setSuccess(true);
        return serviceResult;
    }

    /**
     * 下载 方案详情
     */
    private void downloadImageDescription(Long slnId) {
        Sln sln = serviceHelper.getSlnMapper().selectById(slnId);
        if (sln == null || StringUtils.isEmpty(sln.getDescription())) {
            return;
        }

        var imageInfos = getImagesFromDescription(sln.getDescription());
        imageInfos = imageInfos.stream().map(item -> {
            item.setRandomString(getRandomString(item.getLength()));
            return item;
        }).collect(Collectors.toList());
        String description = sln.getDescription();

        // 先替换为长度相等的随机字符串
        for (var imageInfo : imageInfos) {
            StringBuilder sb = new StringBuilder(description);
            sb.replace(imageInfo.getStart(), imageInfo.getStart() + imageInfo.getLength(), imageInfo.getRandomString());
            description = sb.toString();
        }

        // 再将随机字符串替换为其他类容
        for (var imageInfo : imageInfos) {
            String newAbsoluteUrl = downloadOneUrl(slnId, SlnSyncFileLogType.Description, imageInfo.getUrl(), DownloadSlnFileType.Image);
            if (!StringUtils.isEmpty(newAbsoluteUrl)) {
                description = description.replace(imageInfo.getRandomString(), newAbsoluteUrl);
                UpdateWrapper<Sln> uwSln = new UpdateWrapper<>();
                uwSln.lambda().set(Sln::getDescription, description);
                uwSln.lambda().eq(Sln::getId, slnId);
                serviceHelper.getSlnMapper().update(null, uwSln);
            }
        }
    }

    /**
     * 获取指定长度的随机字符串
     */
    private String getRandomString(int length) {
        if (length <= 0) {
            return "";
        }

        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random1 = new Random();
        // 指定字符串长度，拼接字符并toString
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            // 获取指定长度的字符串中任意一个字符的索引值
            int number = random1.nextInt(str.length());
            //根据索引值获取对应的字符
            char charAt = str.charAt(number);
            sb.append(charAt);
        }
        return sb.toString();
    }

    /**
     * 从描述中获取图片
     */
    private List<ImagesFromDescriptionPojo> getImagesFromDescription(String description) {
        Pattern p = Pattern.compile("<img\\b[^<>]*?\\bsrc[\\s\\t\\r\\n]*=[\\s\\t\\r\\n]*[\"\"']?[\\s\\t\\r\\n]*(?<imgUrl>[^\\s\\t\\r\\n\"\"'<>]*)[^<>]*?/?[\\s\\t\\r\\n]*>");
        Matcher match = p.matcher(description);

        List<ImagesFromDescriptionPojo> imageInfos = new ArrayList<>();
        while (match.find()) {
            String imgTag = match.group(0);
            Integer imgTagStart = match.start();

            String imgUrl = match.group("imgUrl");
            Integer imgUrlLength = imgUrl.length();

            Integer urlStartInTag = imgTag.indexOf(imgUrl);

            if (StringUtils.isEmpty(imgTag) || StringUtils.isEmpty(imgUrl)) {
                continue;
            }

            ImagesFromDescriptionPojo newImageInfo = new ImagesFromDescriptionPojo();
            newImageInfo.setUrl(imgUrl);
            newImageInfo.setStart(imgTagStart + urlStartInTag);
            newImageInfo.setLength(imgUrlLength);
            imageInfos.add(newImageInfo);
        }

        return imageInfos;
    }

    /**
     * 下载 其他封面
     */
    private void downloadImageOtherCover(Long slnId) {
        QueryWrapper<SlnUrl> qwSlnUrl = new QueryWrapper<>();
        qwSlnUrl.lambda().eq(SlnUrl::getSlnId, slnId);
        qwSlnUrl.lambda().eq(SlnUrl::getType, SlnUrlType.OtherCover);
        List<SlnUrl> slnUrls = serviceHelper.getSlnUrlMapper().selectList(qwSlnUrl);
        if (slnUrls == null || slnUrls.isEmpty()) {
            return;
        }

        for (SlnUrl slnUrl : slnUrls) {
            if (slnUrl == null || StringUtils.isEmpty(slnUrl.getUrl())) {
                return;
            }

            String newAbsoluteUrl = downloadOneUrl(slnId, SlnSyncFileLogType.OtherCover, slnUrl.getUrl(), DownloadSlnFileType.Image);
            if (!StringUtils.isEmpty(newAbsoluteUrl)) {
                UpdateWrapper<SlnUrl> uwSlnUrl = new UpdateWrapper<>();
                uwSlnUrl.lambda().set(SlnUrl::getUrl, newAbsoluteUrl);
                uwSlnUrl.lambda().eq(SlnUrl::getId, slnUrl.getId());
                serviceHelper.getSlnUrlMapper().update(null, uwSlnUrl);
            }
        }
    }


    /**
     * @Description: 下载其他封面
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/1616:53
     * @param slnUrl
     */
    @Override
    public String downloadImageOtherCover(SlnUrl slnUrl) {
        if(null == slnUrl)
            throw new RuntimeException("封面图片路径异常");
        return downloadOneUrl(slnUrl.getSlnId(), SlnSyncFileLogType.OtherCover, slnUrl.getUrl(), DownloadSlnFileType.Image);
    }

    /**
     * @Description: 下载其他封面 启用线程
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/110:53
     */
    @Override
    public String downloadImageOtherCoverByThread(SlnUrl slnUrl) {
        if(null == slnUrl)
            throw new RuntimeException("封面图片路径异常");
        AtomicReference<String> imgNewUrl = new AtomicReference<>();
        Thread imgThread = new Thread(()->{
            imgNewUrl.set(downloadOneUrl(slnUrl.getSlnId(), SlnSyncFileLogType.OtherCover, slnUrl.getUrl(), DownloadSlnFileType.Image));
        });
        try {
            imgThread.start();
            imgThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return imgNewUrl.get();
    }

    /**
     * 下载 方案文档
     */
    private void downloadFilePath(Long slnId) {
        QueryWrapper<SlnUrl> qwSlnUrl = new QueryWrapper<>();
        qwSlnUrl.lambda().eq(SlnUrl::getSlnId, slnId);
        qwSlnUrl.lambda().eq(SlnUrl::getType, SlnUrlType.FilePath);
        List<SlnUrl> slnUrls = serviceHelper.getSlnUrlMapper().selectList(qwSlnUrl);
        if (slnUrls == null || slnUrls.isEmpty()) {
            return;
        }

        for (SlnUrl slnUrl : slnUrls) {
            if (slnUrl == null || StringUtils.isEmpty(slnUrl.getUrl())) {
                return;
            }

            String newAbsoluteUrl = downloadOneUrl(slnId, SlnSyncFileLogType.FilePath, slnUrl.getUrl(), DownloadSlnFileType.File);
            if (!StringUtils.isEmpty(newAbsoluteUrl)) {
                UpdateWrapper<SlnUrl> uwSlnUrl = new UpdateWrapper<>();
                uwSlnUrl.lambda().set(SlnUrl::getUrl, newAbsoluteUrl);
                uwSlnUrl.lambda().eq(SlnUrl::getId, slnUrl.getId());
                serviceHelper.getSlnUrlMapper().update(null, uwSlnUrl);
            }
        }
    }

    /**
     * 下载 方案封面
     */
    private void downloadImageGoodsCover(Long slnId) {
        Sln sln = serviceHelper.getSlnMapper().selectById(slnId);
        if (sln == null || StringUtils.isEmpty(sln.getGoodsCover())) {
            return;
        }

        String newAbsoluteUrl = downloadOneUrl(slnId, SlnSyncFileLogType.GoodsCover, sln.getGoodsCover(), DownloadSlnFileType.Image);
        if (!StringUtils.isEmpty(newAbsoluteUrl)) {
            UpdateWrapper<Sln> uwSln = new UpdateWrapper<>();
            uwSln.lambda().set(Sln::getGoodsCover, newAbsoluteUrl);
            uwSln.lambda().eq(Sln::getId, slnId);
            serviceHelper.getSlnMapper().update(null, uwSln);
        }
    }

    /**
     * @Description: 下载方案封面
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/1616:44
     * @return
     */
    @Override
    public String downloadImageGoodsCover(Sln sln) {
        Long sinId = sln.getSlnId();
        return downloadOneUrl(sinId, SlnSyncFileLogType.GoodsCover, sln.getGoodsCover(), DownloadSlnFileType.Image);
    }

    /**
     * @Description: 下载方案封面（单独线程执行）
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/1616:44
     * @return
     */
    @Override
    public String downloadImageGoodsCoverByThread(Sln sln) {
        AtomicReference<String> imgNewUrl = new AtomicReference<>();
        Thread imgThread = new Thread(()->{
            imgNewUrl.set(downloadOneUrl(sln.getSlnId(), SlnSyncFileLogType.GoodsCover, sln.getGoodsCover(), DownloadSlnFileType.Image));
        });
        try {
            imgThread.start();
            imgThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return imgNewUrl.get();
    }

    /**
     * 下载一个url
     *
     * @param slnId              SlnId
     * @param slnSyncFileLogType 同步文件日志类型
     * @param originUrl          旧url
     * @return 新url
     */
    private String downloadOneUrl(Long slnId, Integer slnSyncFileLogType, String originUrl, int downloadSlnFileType) {
        String newUrlFromLog = getNewUrlFromSyncFileLog(slnId, slnSyncFileLogType, originUrl);
        if (!StringUtils.isEmpty(newUrlFromLog)) {
            return newUrlFromLog;
        }

        DownloadSlnFileReturnPojo downloadRtn = downloadFileAndLog(slnId, slnSyncFileLogType, originUrl, downloadSlnFileType);
        if (downloadRtn.getDownloadSuccess()) {
            return downloadRtn.getNewAbsoluteUrl();
        } else {
            return null;
        }
    }

    /**
     * 从同步文件日志表中获取新Url
     *
     * @param slnId              SlnId
     * @param slnSyncFileLogType 同步文件日志类型
     * @param originUrl          旧url
     * @return
     */
    private String getNewUrlFromSyncFileLog(Long slnId, Integer slnSyncFileLogType, String originUrl) {
        QueryWrapper<SlnSyncFileLog> qwSlnSyncFileLog = new QueryWrapper<>();
        qwSlnSyncFileLog.lambda().eq(SlnSyncFileLog::getSlnId, slnId);
        qwSlnSyncFileLog.lambda().eq(SlnSyncFileLog::getType, slnSyncFileLogType);
        qwSlnSyncFileLog.lambda().eq(SlnSyncFileLog::getOriginUrl, originUrl);
        qwSlnSyncFileLog.lambda().isNull(SlnSyncFileLog::getErrorMsg);
        qwSlnSyncFileLog.lambda().orderByDesc(SlnSyncFileLog::getCreateDate);
        SlnSyncFileLog slnSyncFileLog = serviceHelper.getSlnSyncFileLogMapper().selectOne(qwSlnSyncFileLog);
        if (slnSyncFileLog == null) {
            return null;
        } else {
            return slnSyncFileLog.getNewUrl();
        }
    }


    /**
     * 下载文件并记录日志
     *
     * @param slnId              SlnId
     * @param slnSyncFileLogType 同步文件日志类型
     * @param originUrl          旧url
     * @return
     * @throws SftpException
     * @throws IOException
     */
    private DownloadSlnFileReturnPojo downloadFileAndLog(Long slnId, Integer slnSyncFileLogType, String originUrl, int downloadSlnFileType) {
        // 新下载成功或失败, 都要记录到日志
        SlnSyncFileLog slnSyncFileLog = new SlnSyncFileLog();
        slnSyncFileLog.setId(IdUtil.snowflakeNextId());
        slnSyncFileLog.setSlnId(slnId);
        slnSyncFileLog.setType(slnSyncFileLogType);
        slnSyncFileLog.setOriginUrl(originUrl);
        slnSyncFileLog.setCreateDate(new Date());

        DownloadSlnFileReturnPojo downloadToFtpRtn = null;
        try {
            if (downloadSlnFileType == DownloadSlnFileType.Image) {
                // 下载图片并上传ftp
                downloadToFtpRtn = downloadImageToFtp(originUrl);
            } else if (downloadSlnFileType == DownloadSlnFileType.File) {
                // 下载文件并上传minio
                downloadToFtpRtn = downloadFileToMinio(originUrl);
            }

            // 下载失败
            if (!downloadToFtpRtn.getDownloadSuccess()) {
                slnSyncFileLog.setErrorMsg(downloadToFtpRtn.getErrorMsg());
                serviceHelper.getSlnSyncFileLogMapper().insert(slnSyncFileLog);
                return downloadToFtpRtn;
            }
        } catch (Exception e) {
            slnSyncFileLog.setErrorMsg(e.getMessage());
            serviceHelper.getSlnSyncFileLogMapper().insert(slnSyncFileLog);
            return downloadToFtpRtn;
        }

        // 获取 urlBasePath
        String urlBasePath = null;
        if (downloadSlnFileType == DownloadSlnFileType.Image) {
            urlBasePath = serviceHelper.getFtpAccountConfig().getImageUrlBasePath();
        } else if (downloadSlnFileType == DownloadSlnFileType.File) {
            urlBasePath = serviceHelper.getMinioPropConfig().getUrlBasePath();
        }

        // 下载成功
        String newRelativeUrl = downloadToFtpRtn.getNewRelativeUrl();
        String newAbsoluteUrl = urlBasePath + newRelativeUrl;
        slnSyncFileLog.setNewUrl(newAbsoluteUrl);
        serviceHelper.getSlnSyncFileLogMapper().insert(slnSyncFileLog);

        // 返回
        DownloadSlnFileReturnPojo downloadAndLogRtn = new DownloadSlnFileReturnPojo();
        downloadAndLogRtn.setDownloadSuccess(true);
        downloadAndLogRtn.setNewRelativeUrl(newRelativeUrl);
        downloadAndLogRtn.setNewAbsoluteUrl(newAbsoluteUrl);
        return downloadAndLogRtn;
    }

    /**
     * 下载图片并上传至ftp
     *
     * @param originAbsoluteUrl 旧的绝对url
     * @return
     */
    private DownloadSlnFileReturnPojo downloadImageToFtp(String originAbsoluteUrl) throws IOException, SftpException {
        byte[] data = null;
        try {
            data = getBytesFromUrl(originAbsoluteUrl);
        } catch (Exception e) {
            DownloadSlnFileReturnPojo errRtn = new DownloadSlnFileReturnPojo();
            errRtn.setDownloadSuccess(false);
            errRtn.setErrorMsg(e.getMessage());
            return errRtn;
        }

        // 获取年月日字符串
        Calendar now = Calendar.getInstance();
        String year = Integer.toString(now.get(Calendar.YEAR));
        String month = Integer.toString(now.get(Calendar.MONTH) + 1);
        month = StringUtils.padRight(month, 2, '0');
        String day = Integer.toString(now.get(Calendar.DAY_OF_MONTH));
        day = StringUtils.padRight(day, 2, '0');

        // 相对路径名
        String category = "sln_image";
        String relativeDirName = category + "/" + year + "/" + month + "/" + day; // 前后都已有斜杠
        // 新的文件名, 使用原文件名
        String newFileName = PathUtils.getFileNameWithExtensionFromUrl(originAbsoluteUrl);
        try {
            // 传ftp
            serviceHelper.getSftpChannelManager().personalityUpload(serviceHelper.getFtpAccountConfig().getImageSaveBasePath() + relativeDirName, newFileName, data);
        } catch (Exception e) {
            DownloadSlnFileReturnPojo errRtn = new DownloadSlnFileReturnPojo();
            errRtn.setDownloadSuccess(false);
            errRtn.setErrorMsg(e.getMessage());
            return errRtn;
        }
        DownloadSlnFileReturnPojo successRtn = new DownloadSlnFileReturnPojo();
        successRtn.setDownloadSuccess(true);
        successRtn.setNewRelativeUrl(relativeDirName + "/" + newFileName);
        return successRtn;
    }

    /**
     * @Description: 删除失效的数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/2010:44
     */
    @Override
    public void deleteInvalidData() {
        // 地址中包含192.168.80.110
        final String host = "192.168.80.110";
        new Thread(()->{
            LambdaQueryWrapper<SlnUrl> wrapper = new LambdaQueryWrapper<>();
            wrapper.like(SlnUrl::getUrl,host);
            List<SlnUrl> slnUrls = serviceHelper.getSlnUrlMapper().selectList(wrapper);
            //开始心跳
            if(!CollectionUtils.isEmpty(slnUrls)){
                // 获取失效列表
                List<SlnUrl> collect = slnUrls.parallelStream().filter(e -> {
                    return new ImgUtil.Survey().surveyImg(new Supplier<String>() {
                        @Override
                        public String get() {
                            return e.getUrl();
                        }
                    });
                }).collect(Collectors.toList());
                //执行删除任务
                this.deleteInvalidDatabasesBySlnUrls(collect);
            }
        },"*方案其他图片失效线程*：启动执行").start();

        try {
            //因为俩种加锁的方式不同，可能会导致mysql死锁，事务回滚，所以休眠5秒
            Thread.sleep(5 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(()-> {
            // tomcat线程执行
            LambdaQueryWrapper<Sln> slnWrapper = new LambdaQueryWrapper<>();
            slnWrapper.like(Sln::getGoodsCover, host);
            List<Sln> slns = serviceHelper.getSlnMapper().selectList(slnWrapper);
            //开始心跳
            if (!CollectionUtils.isEmpty(slns)) {
                // 获取失效列表
                List<Sln> collect = slns.parallelStream().filter(e -> {
                    return new ImgUtil.Survey().surveyImg(new Supplier<String>() {
                        @Override
                        public String get() {
                            return e.getGoodsCover();
                        }
                    });
                }).collect(Collectors.toList());
                //执行删除任务
                this.deleteInvalidDatabasesBySlns(collect);
            }
        },"*方案封面图片失效线程*：启动执行").start();

    }

    /**
     * @Description: 删除无效的数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/2015:16
     */
    @Transactional(noRollbackFor = Exception.class)
    public void deleteInvalidDatabasesBySlnUrls(List<SlnUrl> collect) {
        // 开始查询数据 并删除
        LambdaQueryWrapper<Sln> slnWrapper = new LambdaQueryWrapper<>();
        List<Long> slnIds = collect.stream().map(SlnUrl::getSlnId).collect(Collectors.toList());
        slnWrapper.in(Sln::getId,slnIds);
        //删除 sln_ide
        LambdaQueryWrapper<SlnIde> slnIdeWrapper = new LambdaQueryWrapper<>();
        slnIdeWrapper.in(SlnIde::getSlnId,slnIds);
        serviceHelper.getSlnIdeMapper().delete(slnIdeWrapper);
        //删除 sln_like_tag
        LambdaQueryWrapper<SlnLikeTag> slnLikeTagWrapper = new LambdaQueryWrapper<>();
        slnLikeTagWrapper.in(SlnLikeTag::getSlnId,slnIds);
        serviceHelper.getSlnLikeTagMapper().delete(slnLikeTagWrapper);
        //删除 sln_sync_file_log
        LambdaQueryWrapper<SlnSyncFileLog> slnFileLogWrapper = new LambdaQueryWrapper<>();
        slnFileLogWrapper.in(SlnSyncFileLog::getSlnId,slnIds);
        serviceHelper.getSlnSyncFileLogMapper().delete(slnFileLogWrapper);

        serviceHelper.getSlnMapper().deleteBatchIds(slnIds);

        serviceHelper.getSlnUrlMapper().deleteBatchIds(collect.stream().map(SlnUrl::getId).collect(Collectors.toList()));

    }

    /**
     * @Description: 删除无效的数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/2015:16
     */
    @Transactional(noRollbackFor = Exception.class)
    public void deleteInvalidDatabasesBySlns(List<Sln> collect) {
        // 开始查询数据 并删除
        List<Long> slnIds = collect.stream().map(Sln::getId).collect(Collectors.toList());
        //删除 sln_ide
        LambdaQueryWrapper<SlnIde> slnIdeWrapper = new LambdaQueryWrapper<>();
        slnIdeWrapper.in(SlnIde::getSlnId,slnIds);
        serviceHelper.getSlnIdeMapper().delete(slnIdeWrapper);
        //删除 sln_like_tag
        LambdaQueryWrapper<SlnLikeTag> slnLikeTagWrapper = new LambdaQueryWrapper<>();
        slnLikeTagWrapper.in(SlnLikeTag::getSlnId,slnIds);
        serviceHelper.getSlnLikeTagMapper().delete(slnLikeTagWrapper);
        //删除 sln_sync_file_log
        LambdaQueryWrapper<SlnSyncFileLog> slnFileLogWrapper = new LambdaQueryWrapper<>();
        slnFileLogWrapper.in(SlnSyncFileLog::getSlnId,slnIds);
        serviceHelper.getSlnSyncFileLogMapper().delete(slnFileLogWrapper);
        //删除 sln_url
        LambdaQueryWrapper<SlnUrl> slnUrlWrapper = new LambdaQueryWrapper<>();
        slnUrlWrapper.in(SlnUrl::getSlnId,slnIds);
        serviceHelper.getSlnUrlMapper().delete(slnUrlWrapper);

        serviceHelper.getSlnMapper().deleteBatchIds(slnIds);
    }


    /**
     * 下载图片并上传至ftp
     *
     * @param originAbsoluteUrl 旧的绝对url
     * @return
     */
    private DownloadSlnFileReturnPojo downloadFileToMinio(String originAbsoluteUrl) {
        byte[] data = null;
        try {
            data = getBytesFromUrl(originAbsoluteUrl);
        } catch (Exception e) {
            DownloadSlnFileReturnPojo errRtn = new DownloadSlnFileReturnPojo();
            errRtn.setDownloadSuccess(false);
            errRtn.setErrorMsg(e.getMessage());
            return errRtn;
        }

        // 获取年月日字符串
        Calendar now = Calendar.getInstance();
        String year = Integer.toString(now.get(Calendar.YEAR));
        String month = Integer.toString(now.get(Calendar.MONTH) + 1);
        month = StringUtils.padRight(month, 2, '0');
        String day = Integer.toString(now.get(Calendar.DAY_OF_MONTH));
        day = StringUtils.padRight(day, 2, '0');

        // 相对路径名
        String category = "sln_file";
        String relativeDirName = category + "/" + year + "/" + month + "/" + day; // 前后都已有斜杠
        // 新的文件名, 使用原文件名
        String newFileName = PathUtils.getFileNameWithExtensionFromUrl(originAbsoluteUrl);

        // 获取文件扩展名，并根据文件扩展名获取ContentType
        String fileExtension = PathUtils.getFileExtension(originAbsoluteUrl);
        QueryWrapper<ContentType> qwContentType = new QueryWrapper<>();
        qwContentType.lambda().eq(ContentType::getFileExt, fileExtension);
        ContentType contentType = serviceHelper.getContentTypeMapper().selectOne(qwContentType);
        String contentTypeString = contentType.getContentType();
        if (StringUtils.isEmpty(contentTypeString)) {
            DownloadSlnFileReturnPojo errRtn = new DownloadSlnFileReturnPojo();
            errRtn.setDownloadSuccess(false);
            errRtn.setErrorMsg("根据文件扩展名 " + fileExtension + " 找不到 ContentType");
            return errRtn;
        }

        try {
            InputStream is = new ByteArrayInputStream(data);
            serviceHelper.getMinioManager().putObject(serviceHelper.getMinioPropConfig().getBucketName(), relativeDirName + "/" + newFileName, is, Long.valueOf(is.available()), contentTypeString);
        } catch (Exception e) {
            DownloadSlnFileReturnPojo errRtn = new DownloadSlnFileReturnPojo();
            errRtn.setDownloadSuccess(false);
            errRtn.setErrorMsg(e.getMessage());
            return errRtn;
        }

        DownloadSlnFileReturnPojo successRtn = new DownloadSlnFileReturnPojo();
        successRtn.setDownloadSuccess(true);
        successRtn.setNewRelativeUrl(relativeDirName + "/" + newFileName);
        return successRtn;
    }

    /**
     * 从url获取文件bytes
     */
    private byte[] getBytesFromUrl(String originAbsoluteUrl) throws Exception {
        //定义一个URL对象，就是你想下载的图片的URL地址
        URL url = new URL(originAbsoluteUrl);
        //打开连接
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        //设置请求方式为"GET"
        conn.setRequestMethod("GET");
        //超时响应时间为10秒
        conn.setConnectTimeout(10 * 1000);
        //通过输入流获取图片数据
        InputStream is = conn.getInputStream();
        //得到图片的二进制数据，以二进制封装得到数据，具有通用性
        return readInputStream(is,originAbsoluteUrl);
    }

    private byte[] readInputStream(InputStream inStream,String originAbsoluteUrl) throws Exception {
        int len,available;
        if((available = (inStream.available() / 1024 / 1024)) >= 10 )
            log.warn("*********************原始图片路径："+originAbsoluteUrl+",文件长度"+available+"M大于经告值10M!!!***********************");

        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        //创建一个Buffer字符串
        byte[] buffer = new byte[6024];
        //每次读取的字符串长度，如果为-1，代表全部读取完毕
        //使用一个输入流从buffer里把数据读取出来
        while ((len = inStream.read(buffer)) != -1) {
            //用输出流往buffer里写入数据，中间参数代表从哪个位置开始读，len代表读取的长度
            outStream.write(buffer, 0, len);
        }
        //关闭输入流
        inStream.close();
        //把outStream里的数据写入内存
        return outStream.toByteArray();
    }

    /**
     * @Description: 从我爱方案网拉取数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/2816:58
     */
    @Override
    public void pullSlnInfo() {
        try {
            // 同步数量, 同步当前页码
            int count = 0,page = 1;
            // 结束循环标志
            boolean exit = true;
            // 我爱方案网地址
            String baseUrl = serviceHelper.getSlnApiConfig().getProgramme() + "&page=";
            // 系统目前最大的sln_id值
            Long maxSlnId = this.maxSlnId(false);
            // 开始执行业务sql操作
            while (exit){
                String apiResutl = this.getApiResutl(baseUrl + page++);
                JSONObject jsonObject = JSONObject.parseObject(apiResutl);
                exit = this.exit(jsonObject);
                //获取数据
                ServiceResult<ImportSlnOnePagePojo> onePageSlnsFromApi = this.getOnePageSlnsFromApi(jsonObject);
                Assert.isTrue(onePageSlnsFromApi.getSuccess(),"解析数据sln异常");
                ImportSlnOnePagePojo result = onePageSlnsFromApi.getResult();
                List<ImportSlnOneDataPojo> results = result.getResults();
                //  获取有效的数据
                List<Sln> slns = results.stream().map(ImportSlnOneDataPojo::getSln).filter(e->e.getSlnId() > maxSlnId).collect(Collectors.toList());
                // 没有匹配的方案数据 则退出
                if(CollectionUtils.isEmpty(slns))
                    return;

                List<List<SlnIde>> slnIdes = results.stream().map(ImportSlnOneDataPojo::getSlnIdes).collect(Collectors.toList());
                List<List<SlnUrl>> slnUrls = results.stream().map(ImportSlnOneDataPojo::getSlnUrls).collect(Collectors.toList());
                List<List<SlnLikeTag>> slnTags = results.stream().map(ImportSlnOneDataPojo::getSlnLikeTags).collect(Collectors.toList());

                // 插入数据
                for(int i = 0; i < slns.size(); i++){
                    List<SlnIde> slnIdeDatas = slnIdes.get(i);
                    List<SlnUrl> slnUrlDatas = slnUrls.get(i);
                    List<SlnLikeTag> slnLikeTagDatas = slnTags.get(i);
                    this.insertAll(slns.get(i),slnIdeDatas,slnUrlDatas,slnLikeTagDatas);
                }
                count += slns.size() ;
            }
            log.info("当前同步数据数量：" + count);
        } finally {
            maxSlnId(true);
        }
    }


    /**
     * @Description: 上传图片并插入数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/3111:34
     */
//    @Transactional(rollbackFor = Exception.class,propagation = Propagation.REQUIRED)
    public void insertAll(Sln sln, List<SlnIde> slnIdes,List<SlnUrl> slnUrls,List<SlnLikeTag> likeTagLIst){
        serviceHelper.getSlnMapper().insert(sln);
        // 开放平台设置slnId
        List<SlnIde> slnIdecollect = slnIdes.stream().filter(e -> {
            e.setSlnId(sln.getId());
            return true;
        }).collect(Collectors.toList());
        if(!CollectionUtils.isEmpty(slnIdecollect))
             serviceHelper.getSlnIdeMapper().insertBatchSomeColumn(slnIdecollect);
        //  标签设置slnId
        List<SlnLikeTag> slnLikeTagcollect = likeTagLIst.stream().filter(e -> {
            e.setSlnId(sln.getId());
            return true;
        }).collect(Collectors.toList());
        if(!CollectionUtils.isEmpty(slnLikeTagcollect))
            serviceHelper.getSlnLikeTagMapper().insertBatchSomeColumn(slnLikeTagcollect);
        // 设置方案ID
        List<SlnUrl> slnUrlCollect = slnUrls.stream().filter(e -> {
            e.setSlnId(sln.getId());
            return true;
        }).collect(Collectors.toList());
        if(!CollectionUtils.isEmpty(slnUrlCollect))
            serviceHelper.getSlnUrlMapper().insertBatchSomeColumn(slnUrlCollect);
    }

    /**
     * @Description: 获取方案最大的SLn_id并且来事是我爱方案网
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/319:55
     */
    private Long maxSlnId(boolean forceExec){
        // 不强制执行 正常流程
        if(!forceExec){
            Object o = serviceHelper.getRedisManager().get(SLN_KEY);
            if(!ObjectUtils.isEmpty(o) && Long.valueOf(o.toString()) > 0)
                return Long.valueOf(o.toString());
        }
        // forceExec is true, do updating redis value
        //如果为空直接返回0
        Long maxSlnId = this.baseMapper.getMaxSlnId();
        serviceHelper.getRedisManager().set(SLN_KEY,maxSlnId.toString());
        return maxSlnId;
    }

    /**
     * @Description: 解析json返回sln
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/2817:15
     */
    private boolean exit(JSONObject apiRes){
        // 失败 误解结果集
        final String DETAIL = "detail";
        return (apiRes.containsKey(DETAIL)) ? false : true;
    }

    /**
     * @Description: 返回结果集
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/2817:08
     */
    private String getApiResutl(String baseUrl){
        return WebClient.create().method(HttpMethod.GET)
                .uri(baseUrl)
                .header("Authorization", serviceHelper.getSlnApiConfig().getAuthorization())
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }


    /**
     * @Description: 同步方案数据到redis中
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/214:19
     */
    @Override
    public void synSlnData2Redis() {
        String redisPrefix = RedisKeyPrefixEnum.TABLE.getCode() + ":";

        LambdaQueryWrapper<Sln> slnLambdaQueryWrapper = new LambdaQueryWrapper<>();
        slnLambdaQueryWrapper.eq(Sln::getSource,SlnSourceEnum.WAFAW.getKey());
        List<Sln> slns = this.baseMapper.selectList(slnLambdaQueryWrapper);
        serviceHelper.getRedisManager().del(redisPrefix + RedisKeyPrefixEnum.TABLE_SLN.getCode());
        serviceHelper.getRedisManager().lPushAll(redisPrefix + RedisKeyPrefixEnum.TABLE_SLN.getCode(),slns);

        // 开放平台
        List<SlnIde> slnIdes = serviceHelper.getSlnIdeMapper().selectList(new LambdaQueryWrapper<>());
        serviceHelper.getRedisManager().del(redisPrefix + RedisKeyPrefixEnum.TABLE_SLN_IDE.getCode());
        serviceHelper.getRedisManager().lPushAll(redisPrefix + RedisKeyPrefixEnum.TABLE_SLN_IDE.getCode(),slnIdes);

        // 标签
        List<SlnLikeTag> slnLikeTags = serviceHelper.getSlnLikeTagMapper().selectList(new LambdaQueryWrapper<>());
        serviceHelper.getRedisManager().del(redisPrefix + RedisKeyPrefixEnum.TABLE_SLN_LIKE_TAG.getCode());
        serviceHelper.getRedisManager().lPushAll(redisPrefix + RedisKeyPrefixEnum.TABLE_SLN_LIKE_TAG.getCode(),slnLikeTags);

        // 其他封面
        List<SlnUrl> slnUrls = serviceHelper.getSlnUrlMapper().selectList(new LambdaQueryWrapper<>());
        serviceHelper.getRedisManager().del(redisPrefix + RedisKeyPrefixEnum.TABLE_SLN_URL.getCode());
        serviceHelper.getRedisManager().lPushAll(redisPrefix + RedisKeyPrefixEnum.TABLE_SLN_URL.getCode(),slnUrls);
    }

    /**
     * @Description:
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/315:04
     */
    @Deprecated
    public void syn(){
        String synSlnRedisKey = RedisKeyPrefixEnum.TABLE.getCode() + ":" + RedisKeyPrefixEnum.TABLE_SLN.getCode();
        serviceHelper.getRedisManager().del(synSlnRedisKey);

        LambdaQueryWrapper<Sln> slnLambdaQueryWrapper = new LambdaQueryWrapper<>();
        slnLambdaQueryWrapper.eq(Sln::getSource,SlnSourceEnum.WAFAW.getKey());
        List<Sln> slns = this.baseMapper.selectList(slnLambdaQueryWrapper);
        // 开放平台
        List<SlnIde> slnIdes = serviceHelper.getSlnIdeMapper().selectList(new LambdaQueryWrapper<>());
        // 标签
        List<SlnLikeTag> slnLikeTags = serviceHelper.getSlnLikeTagMapper().selectList(new LambdaQueryWrapper<>());
        // 其他封面
        List<SlnUrl> slnUrls = serviceHelper.getSlnUrlMapper().selectList(new LambdaQueryWrapper<>());
        /**
         *  list -map
         */
        Map<Long, List<SlnIde>> slnIdeMap = slnIdes.parallelStream().collect(Collectors.groupingBy(SlnIde::getSlnId));
        Map<Long, List<SlnLikeTag>> slnTagMap = slnLikeTags.parallelStream().collect(Collectors.groupingBy(SlnLikeTag::getSlnId));
        Map<Long, List<SlnUrl>> slnUrlMap = slnUrls.parallelStream().collect(Collectors.groupingBy(SlnUrl::getSlnId));

        List<SlnListPojo> slnListPojos = BeanPropertiesUtil.convertList(slns, SlnListPojo.class);
        slnListPojos.forEach(e->{
            e.setSlnLikeTags(slnTagMap.get(e.getId()));
            e.setSlnUrls(slnUrlMap.get(e.getId()));
            e.setSlnIdes(slnIdeMap.get(e.getId()));
        });
        // 放入redis
        serviceHelper.getRedisManager().lPushAll(synSlnRedisKey,slnListPojos);
    }

}
