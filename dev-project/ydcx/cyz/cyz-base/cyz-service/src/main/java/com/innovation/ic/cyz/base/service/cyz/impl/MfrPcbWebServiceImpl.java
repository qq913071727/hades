package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.model.cyz.Mfr;
import com.innovation.ic.cyz.base.model.cyz.MfrContent;
import com.innovation.ic.cyz.base.model.cyz.MfrParam;
import com.innovation.ic.cyz.base.mapper.cyz.*;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.annotation.MfrParamCode;
import com.innovation.ic.cyz.base.pojo.constant.MfrModule;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrPcbDetailPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrPcbListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrPcbListResultPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrPcbOurProductPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.mfrpcb_search_option.MfrPcbOptionItem;
import com.innovation.ic.cyz.base.pojo.variable.cyz.mfrpcb_search_option.MfrPcbOptionLine;
import com.innovation.ic.cyz.base.service.cyz.MfrPcbWebService;
import com.innovation.ic.cyz.base.vo.cyz.MfrPcbListVo;
import lombok.var;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * pcb厂家Web使用具体实现类
 */
@Service
@Transactional
public class MfrPcbWebServiceImpl extends ServiceImpl<MfrMapper, Mfr> implements MfrPcbWebService {

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 获取一个实体类中定义的MfrParamCode
     *
     * @param clazz
     * @return
     */
    private List<String> getCodesFromClass(Class<?> clazz) {
        List<String> codes = new ArrayList<>();

        var annotationClass = clazz.getAnnotation(MfrParamCode.class);
        if (annotationClass != null) {
            codes.add(annotationClass.value());
        }

        Field[] fields = clazz.getDeclaredFields();
        for (var field : fields) {
            var annotation = field.getAnnotation(MfrParamCode.class);
            if (annotation != null) {
                codes.add(annotation.value());
            }
        }

        return codes;
    }

    /**
     * 根据传入的条件查询PCB厂家列表
     *
     * @param mfrListVo
     * @return
     */
    @Override
    public ServiceResult<MfrPcbListResultPojo> findMfrList(MfrPcbListVo mfrListVo) throws NoSuchFieldException, IllegalAccessException {
        // 将查询的属性Id放到一起
        List<Long> paramIds = new ArrayList<>();
        if (mfrListVo.getFactoryFeatureIds() != null)
            paramIds.addAll(mfrListVo.getFactoryFeatureIds());
        if (mfrListVo.getQualificationIds() != null)
            paramIds.addAll(mfrListVo.getQualificationIds());
        if (mfrListVo.getTechnologyClassificationIds() != null)
            paramIds.addAll(mfrListVo.getTechnologyClassificationIds());
        if (mfrListVo.getQualityFeatureIds() != null)
            paramIds.addAll(mfrListVo.getQualityFeatureIds());

        // 根据注解的code, 获取所需要的MfrParam, 可以拿到MfrParamId
        var resModelCodes = getCodesFromClass(MfrPcbListPojo.class);
        var reqModelCodes = getCodesFromClass(MfrPcbListVo.class);
        List<String> allCodes = new ArrayList<>();
        allCodes.addAll(resModelCodes);
        allCodes.addAll(reqModelCodes);
        serviceHelper.getMfrParamDictionary().addCodes(MfrModule.PCB, allCodes);

        // 获取列表数据
        List<Long> mfrPcbIds = serviceHelper.getMfrMapper().findMfrPcbIds(mfrListVo.getPage(), mfrListVo.getSize(),
                paramIds,
                serviceHelper.getMfrParamDictionary().getMfrParamIdByCode("company_name"),
                mfrListVo.getMfrName(),
                serviceHelper.getMfrParamDictionary().getMfrParamIdByCode("region"),
                mfrListVo.getRegionIds());
        Long mfrPcbCount = serviceHelper.getMfrMapper().findMfrPcbCount(
                paramIds,
                serviceHelper.getMfrParamDictionary().getMfrParamIdByCode("company_name"),
                mfrListVo.getMfrName(),
                serviceHelper.getMfrParamDictionary().getMfrParamIdByCode("region"),
                mfrListVo.getRegionIds());
        List<MfrContent> mfrContentList = new ArrayList<>();
        if (mfrPcbIds != null && !mfrPcbIds.isEmpty()) {
            mfrContentList = serviceHelper.getMfrMapper().findMfrContentsByMfrIds(mfrPcbIds, serviceHelper.getMfrParamDictionary().getMfrParamIdsByCodes(resModelCodes));
        }

        // 组合列表数据
        List<MfrPcbListPojo> mfrPcbListPojoList = new ArrayList<>();
        for (Long mfrPcbId : mfrPcbIds) {
            MfrPcbListPojo mfrPcbListPojo = new MfrPcbListPojo();
            mfrPcbListPojo.setMfrId(mfrPcbId);

            // 给加了注解的字段赋值
            Field[] fields = MfrPcbListPojo.class.getDeclaredFields();
            for (var field : fields) {
                var annotation = field.getAnnotation(MfrParamCode.class);
                if (annotation != null) {
                    MfrContent mfrContent = mfrContentList.stream()
                            .filter(t -> t.getMfrId().equals(mfrPcbId) && t.getMfrParamId().equals(serviceHelper.getMfrParamDictionary().getMfrParamIdByCode(annotation.value())))
                            .findFirst().orElse(null);
                    if (mfrContent != null) {
                        Field f = mfrPcbListPojo.getClass().getDeclaredField(field.getName());
                        f.setAccessible(true);
                        f.set(mfrPcbListPojo, mfrContent.getContent());
                    }
                }
            }

            mfrPcbListPojoList.add(mfrPcbListPojo);
        }

        MfrPcbListResultPojo result = new MfrPcbListResultPojo();
        result.setList(mfrPcbListPojoList);
        result.setTotal(mfrPcbCount);

        ServiceResult<MfrPcbListResultPojo> serviceResult = new ServiceResult<MfrPcbListResultPojo>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);

        return serviceResult;
    }

    /**
     * 根据code获取MfrContent
     *
     * @param mfrContentList
     * @param mfrId
     * @param code
     * @return
     */
    private MfrContent getMfrContentByCode(List<MfrContent> mfrContentList, Long mfrId, String code) {
        return mfrContentList.stream()
                .filter(t -> t.getMfrId().equals(mfrId) && t.getMfrParamId().equals(serviceHelper.getMfrParamDictionary().getMfrParamIdByCode(code)))
                .findFirst().orElse(null);
    }

    /**
     * 根据code获取MfrContent
     *
     * @param mfrContentList
     * @param mfrId
     * @param code
     * @return
     */
    private List<MfrContent> getMfrContentsByCode(List<MfrContent> mfrContentList, Long mfrId, String code) {
        return mfrContentList.stream()
                .filter(t -> t.getMfrId().equals(mfrId) && t.getMfrParamId().equals(serviceHelper.getMfrParamDictionary().getMfrParamIdByCode(code)))
                .collect(Collectors.toList());
    }

    /**
     * 根据codes获取MfrContent
     *
     * @param mfrContentList
     * @param mfrId
     * @param codes
     * @return
     */
    private List<MfrContent> getMfrContentsByCodes(List<MfrContent> mfrContentList, Long mfrId, List<String> codes) {
        List<Long> mfrParamIds = serviceHelper.getMfrParamDictionary().getMfrParamIdsByCodes(codes);
        return mfrContentList.stream()
                .filter(t -> t.getMfrId().equals(mfrId) && mfrParamIds.contains(t.getMfrParamId()))
                .collect(Collectors.toList());
    }

    /**
     * 根据mfrId查询PCB厂家详情
     *
     * @param mfrId
     * @return
     */
    @Override
    public ServiceResult<MfrPcbDetailPojo> findMfrById(Long mfrId) throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException, InstantiationException {
        var resModelCodes1 = getCodesFromClass(MfrPcbDetailPojo.class);
        var resModelCodes2 = getCodesFromClass(MfrPcbOurProductPojo.class);

        List<String> allCodes = new ArrayList<>();
        allCodes.addAll(resModelCodes1);
        allCodes.addAll(resModelCodes2);
        serviceHelper.getMfrParamDictionary().addCodes(MfrModule.PCB, allCodes);

        List<MfrContent> mfrContentList = serviceHelper.getMfrMapper().findMfrContentsByMfrIds(new ArrayList<>(Arrays.asList(mfrId)), serviceHelper.getMfrParamDictionary().getMfrParamIdsByCodes(allCodes));

        MfrPcbDetailPojo mfrPcbDetailPojo = new MfrPcbDetailPojo();

        mfrPcbDetailPojo.setMfrId(mfrId);

        // 给加了注解的字段赋值
        Field[] fields = MfrPcbDetailPojo.class.getDeclaredFields();
        for (var field : fields) {
            var annotation = field.getAnnotation(MfrParamCode.class);
            if (annotation != null) {
                Field f = mfrPcbDetailPojo.getClass().getDeclaredField(field.getName());
                String typeName = f.getGenericType().getTypeName();
                if (typeName.equals("java.lang.String")) {
                    var content = getMfrContentByCode(mfrContentList, mfrId, annotation.value());
                    if (content != null) {
                        f.setAccessible(true);
                        f.set(mfrPcbDetailPojo, content.getContent());
                    }
                } else if (typeName.equals("java.util.List<java.lang.String>")) {
                    List<MfrContent> contents = getMfrContentsByCode(mfrContentList, mfrId, annotation.value());
                    if (contents != null && !contents.isEmpty()) {
                        List<String> stringList = contents.stream().map(MfrContent::getContent).collect(Collectors.toList());
                        f.setAccessible(true);
                        f.set(mfrPcbDetailPojo, stringList);
                    }
                } else if (typeName.matches("java.util.List<com.innovation.ic.cyz.base.pojo(\\w|\\.)+>")) {
                    Class<?> newClass = Class.forName(typeName.replace("java.util.List<", "").replace(">", ""));
                    var newClassCodes = getCodesFromClass(newClass);
                    List<MfrContent> contents = getMfrContentsByCodes(mfrContentList, mfrId, newClassCodes);
                    if (contents != null && !contents.isEmpty()) {
                        List<Object> newObjectList = new ArrayList<>();

                        Map<Integer, List<MfrContent>> collect = contents.stream().collect(Collectors.groupingBy(MfrContent::getGroupNo));
                        for (Map.Entry<Integer, List<MfrContent>> entry : collect.entrySet()) {
                            Object newObject = newClass.newInstance();
                            List<MfrContent> oneGroupContent = entry.getValue();

                            Field[] newClassFields = newObject.getClass().getDeclaredFields();
                            for (var newClassField : newClassFields) {
                                var newClassAnnotation = newClassField.getAnnotation(MfrParamCode.class);
                                if (newClassAnnotation != null) {
                                    var contentForNewClassField = getMfrContentByCode(oneGroupContent, mfrId, newClassAnnotation.value());
                                    if (contentForNewClassField != null) {
                                        newClassField.setAccessible(true);
                                        newClassField.set(newObject, contentForNewClassField.getContent());
                                    }
                                }
                            }

                            // 每个分组结束, 得到一个newObject
                            newObjectList.add(newObject);
                        }

                        f.setAccessible(true);
                        f.set(mfrPcbDetailPojo, newObjectList);
                    }
                }
            }
        }

        ServiceResult<MfrPcbDetailPojo> serviceResult = new ServiceResult<MfrPcbDetailPojo>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(mfrPcbDetailPojo);

        return serviceResult;
    }

    /**
     * 查询列表选项
     *
     * @return
     */
    @Override
    public ServiceResult<List<MfrPcbOptionLine>> findListOptions() {
        List<String> bigOptionCodes = new ArrayList<>(Arrays.asList("factory_feature", "qualification", "process_category", "quality_feature"));
        List<MfrParam> level1_OptionMfrParamList = serviceHelper.getMfrParamMapper().findMfrParamsByCodes(MfrModule.PCB, bigOptionCodes);
        List<Long> level1_OptionMfrParamIds = level1_OptionMfrParamList.stream().map(MfrParam::getId).collect(Collectors.toList());
        List<MfrParam> level2_OptionMfrParamList = serviceHelper.getMfrParamMapper().findMfrParamsByParentIds(MfrModule.PCB, level1_OptionMfrParamIds);

        List<MfrPcbOptionLine> mfrPcbOptionLineList = new ArrayList<>();

        for (MfrParam level1_OptionMfrParam : level1_OptionMfrParamList) {
            var mfrPcbOptionLine = new MfrPcbOptionLine();

            // 第一级选项
            var level1_Option = new MfrPcbOptionItem();
            level1_Option.setMfrParamId(level1_OptionMfrParam.getId());
            level1_Option.setName(level1_OptionMfrParam.getName());
            mfrPcbOptionLine.setLevel1Option(level1_Option);

            // 第二级选项
            Long level1_OptionMfrParamId = level1_OptionMfrParam.getId();
            var level2_OptionMfrParamList_Filtered = level2_OptionMfrParamList.stream()
                    .filter(t -> t.getParentId() != null && t.getParentId().equals(level1_OptionMfrParamId)).collect(Collectors.toList());
            if (level2_OptionMfrParamList_Filtered != null && !level2_OptionMfrParamList_Filtered.isEmpty()) {
                var level2_Options = level2_OptionMfrParamList_Filtered.stream()
                        .map(item -> {
                            MfrPcbOptionItem level2_Option = new MfrPcbOptionItem();
                            level2_Option.setMfrParamId(item.getId());
                            level2_Option.setName(item.getName());
                            return level2_Option;
                        }).collect(Collectors.toList());
                mfrPcbOptionLine.setLevel2Options(level2_Options);
            }

            // 加到结果列表中
            mfrPcbOptionLineList.add(mfrPcbOptionLine);
        }

        ServiceResult<List<MfrPcbOptionLine>> serviceResult = new ServiceResult<List<MfrPcbOptionLine>>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(mfrPcbOptionLineList);

        return serviceResult;
    }
}
