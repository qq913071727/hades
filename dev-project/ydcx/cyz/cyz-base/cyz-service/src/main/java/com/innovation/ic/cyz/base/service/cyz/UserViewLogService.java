package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.user_view_log.UserViewLogListResultPojo;
import com.innovation.ic.cyz.base.vo.cyz.UserViewLogListVo;

/**
 * 用户浏览记录接口
 */
public interface UserViewLogService {

    /**
     * 用户浏览/点赞/收藏记录列表
     * @param userViewLogListVo
     * @param userId
     * @return
     */
    ServiceResult<UserViewLogListResultPojo> findUserViewLogList(UserViewLogListVo userViewLogListVo, String userId);

}
