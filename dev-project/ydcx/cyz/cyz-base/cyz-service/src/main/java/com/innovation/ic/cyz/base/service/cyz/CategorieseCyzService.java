package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.Categories;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionTypePojo;

import java.util.List;

public interface CategorieseCyzService {


    void saveBatch(List<Categories> categoriesList);

    void truncateCategories();

    //通过id获取器件选型基础数据包含上级数据
    List<String> selectFindId(String categoryId);

    ServiceResult<List<DeviceSelectionTypePojo>> findByDeviceSelectionType();

    Categories findById(String id);
}
