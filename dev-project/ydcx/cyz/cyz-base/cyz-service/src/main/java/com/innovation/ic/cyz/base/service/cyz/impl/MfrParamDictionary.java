package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.cyz.MfrMapper;
import com.innovation.ic.cyz.base.model.cyz.Mfr;
import com.innovation.ic.cyz.base.model.cyz.MfrParam;
import lombok.var;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 厂家属性字典类
 */
@Service
@Scope("prototype")
public class MfrParamDictionary extends ServiceImpl<MfrMapper, Mfr> {
    @Resource
    private ServiceHelper serviceHelper;

    private Map<String, MfrParam> codeMap = new HashMap<>();


    private Map<String, MfrParam> allCodeMap = new HashMap<>();

    /**
     * 添加codes
     *
     * @param codes
     * @return
     */
    public MfrParamDictionary addCodes(Integer module, List<String> codes) {
        List<MfrParam> mfrParamList = serviceHelper.getMfrParamMapper().findMfrParamsByCodes(module, codes);
        for (MfrParam mfrParam : mfrParamList) {
            codeMap.put(mfrParam.getCode(), mfrParam);
        }
        return this;
    }


    /**
     * 添加codes
     *
     * @param
     * @return
     */
    public MfrParamDictionary addCodes(Integer module) {
        List<MfrParam> mfrParamList = serviceHelper.getMfrParamMapper().findMfrParamsByAllCodes(module);
        for (MfrParam mfrParam : mfrParamList) {
            allCodeMap.put(mfrParam.getCode(), mfrParam);
        }
        return this;
    }



    /**
     * 根据code获取MfrParamId
     *
     * @param code
     * @return
     */
    public Long getMfrParamIdByCode(String code) {
        MfrParam mfrParam = codeMap.get(code);
        return mfrParam != null ? mfrParam.getId() : null;
    }

    /**
     * 根据code获取MfrParamId
     *
     * @param code
     * @return
     */
    public Long getMfrParamIdByAllCode(String code) {
        MfrParam mfrParam = allCodeMap.get(code);
        return mfrParam != null ? mfrParam.getId() : null;
    }

    /**
     * 根据codes获取MfrParamIds
     *
     * @return
     */
    public List<Long> getMfrParamIdsByCodes(List<String> codes) {
        List<Long> mfrParamIds = new ArrayList<>();
        for (var code : codes) {
            MfrParam mfrParam = codeMap.get(code);
            if (mfrParam != null) {
                mfrParamIds.add(mfrParam.getId());
            }
        }
        return mfrParamIds;
    }
}
