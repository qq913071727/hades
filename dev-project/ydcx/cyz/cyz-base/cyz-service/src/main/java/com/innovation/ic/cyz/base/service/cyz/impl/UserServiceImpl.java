package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.model.cyz.User;
import com.innovation.ic.cyz.base.mapper.cyz.UserMapper;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.UserInfo;
import com.innovation.ic.cyz.base.pojo.constant.GeneralStatus;
import com.innovation.ic.cyz.base.service.cyz.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 根据用户名和密码查询用户信息
     */
    @Override
    public ServiceResult<User> findUserByUsernameAndPassword(String username, String password) {
        QueryWrapper<User> qwUser = new QueryWrapper<>();
        qwUser.lambda()
                .eq(User::getUsername, username)
                .eq(User::getPassword, password)
                .eq(User::getStatus, GeneralStatus.NORMAL);
        User user = serviceHelper.getUserMapper().selectOne(qwUser);

        ServiceResult<User> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(user);

        return serviceResult;
    }

    /**
     * 获取当前登录用户信息
     *
     * @param userId
     * @return
     */
    @Override
    public ServiceResult<UserInfo> getCurrentUserInfo(String userId) {
        UserInfo userInfo = serviceHelper.getUserInfo(userId);

        ServiceResult<UserInfo> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(userInfo);

        return serviceResult;
    }

    /**
     * 添加日志数据
     * @param ipAdrress
     * @param user
     * @return
     */
    public void addUserLoginLog(String ipAdrress, User user) {
        serviceHelper.addUserLoginLog(ipAdrress,user);
    }
}
