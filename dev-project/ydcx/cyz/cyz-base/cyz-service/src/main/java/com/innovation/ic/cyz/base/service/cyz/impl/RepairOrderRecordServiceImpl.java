package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.cyz.RepairOrderRecordMapper;
import com.innovation.ic.cyz.base.model.cyz.RepairOrderRecord;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.service.cyz.RepairOrderRecordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;

/**
 * 工单流程接口具体实现类
 */
@Service
@Transactional
public class RepairOrderRecordServiceImpl extends ServiceImpl<RepairOrderRecordMapper, RepairOrderRecord> implements RepairOrderRecordService {

    @Resource
    private RepairOrderRecordMapper repairOrderRecordMapper;

    /**
     * 根据工单id查询最近一条流程记录
     * @param id 工单id
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<RepairOrderRecord> selectLastRecordByOrderId(Long id) {
        RepairOrderRecord repairOrderRecord = repairOrderRecordMapper.selectLastRecordByOrderId(id);

        ServiceResult<RepairOrderRecord> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(repairOrderRecord);
        return serviceResult;
    }
}