package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.feedback.FeedbackListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.feedback.FeedbackTypeListPojo;
import com.innovation.ic.cyz.base.vo.cyz.FeedbackVo;
import com.innovation.ic.cyz.base.vo.cyz.feedback.FeedbackSaveVo;

import java.util.List;

/**
 * 信息反馈接口
 */
public interface FeedbackService {

    /**
     * 获取信息反馈类型
     */
    ServiceResult<List<FeedbackTypeListPojo>> getFeedbackTypes() throws IllegalAccessException;

    /**
     * 信息反馈保存
     */
    ServiceResult save(FeedbackSaveVo feedbackSaveVo);

    /**
     * 信息反馈展示数据
     *
     * @param feedbackVo
     * @return
     */
    ServiceResult<List<FeedbackListPojo>> allFeedback(FeedbackVo feedbackVo);
}
