package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.cyz.CategoriesCyzMapper;
import com.innovation.ic.cyz.base.model.cyz.Categories;
import com.innovation.ic.cyz.base.model.pve_standard.Category;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionTypePojo;
import com.innovation.ic.cyz.base.service.cyz.CategorieseCyzService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
@Slf4j
public class CategoriesServiceCyzImpl extends ServiceImpl<CategoriesCyzMapper, Categories> implements CategorieseCyzService {


    @Override
    public void saveBatch(List<Categories> categoriesList) {
        if (CollectionUtils.isEmpty(categoriesList)) {
            return;
        }
        baseMapper.insertBatchSomeColumn(categoriesList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void truncateCategories() {
        this.baseMapper.truncateCategories();
    }

    public List<String> selectFindId(String categoryId) {
        ArrayList<Categories> categoriesArrayList = new ArrayList<>();
        //获取当前一层 查询出的器件选型基础数据
        Categories categories = this.baseMapper.selectById(categoryId);
        if (!StringUtils.isEmpty(categories.getFatherId())) {
            categoriesArrayList.add(categories);
            //获取第一层fatherId 用于第下一层id查询条件
            String fatherId = categories.getFatherId();
            while (!StringUtils.isEmpty(fatherId)) {
                //根据fatherid 查询 下层器件选型基础数据
                Categories categoriesFather = this.baseMapper.selectById(fatherId);
                if (!StringUtils.isEmpty(categoriesFather.getFatherId())) {//当前father不为空
                    categoriesArrayList.add(categoriesFather);
                    fatherId = categoriesFather.getFatherId();//把最新的fatherId放进while循环中
                } else {//已经是最上级了
                    categoriesArrayList.add(categoriesFather);
                    break;
                }
            }
        }
        //从返回数据中过滤出只要器件选型基础数据中的id
        ArrayList<String> list = new ArrayList<>();
        for (Categories categories1 : categoriesArrayList) {
            String id = categories1.getId();
            list.add(id);
        }
        //对list 去重 避免数据重复
        //初始化TreeSet对象，并把list对象元素赋值给TreeSet对象
        TreeSet set = new TreeSet(list);
        //把List集合所有元素清空
        list.clear();
        //把TreeSet对象添加至List集合
        list.addAll(set);
        return list;
    }

    @Override
    public ServiceResult<List<DeviceSelectionTypePojo>> findByDeviceSelectionType() {
        ServiceResult<List<DeviceSelectionTypePojo>> serviceResult = new ServiceResult<>();
        //找到根节点
        Map<String, Object> param = new HashMap<>();
        param.put("father_id", null);
        param.put("state", 200);
        List<Categories> categoriesRoot = this.baseMapper.selectByMap(param);
        //找到所有子节点
        QueryWrapper<Categories> queryWrapper = new QueryWrapper();
        queryWrapper.isNotNull("father_id");
        queryWrapper.eq("state", 200);
        List<Categories> categoriesChild = this.baseMapper.selectList(queryWrapper);
        if (CollectionUtils.isEmpty(categoriesRoot)) {
            return serviceResult;
        }
        if (CollectionUtils.isEmpty(categoriesChild)) {
            return serviceResult;
        }
        categoriesChild.addAll(categoriesRoot);
        List<DeviceSelectionTypePojo> deviceSelectionTypePojoList = new ArrayList<>();
        categoriesChild.stream().forEach(e -> deviceSelectionTypePojoList.add(this.bindDeviceSelectionTypePojo(e)));
        List<DeviceSelectionTypePojo> deviceSelectionTypePojos = this.buildDeviceSelectionTypePojoTree(deviceSelectionTypePojoList, categoriesRoot.get(0).getId());
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(deviceSelectionTypePojos);
        return serviceResult;
    }

    @Override
    public Categories findById(String id) {
        return this.baseMapper.selectById(id);
    }

    /**
     * 绑定器件选型参数
     *
     * @param categories
     * @return
     */
    private DeviceSelectionTypePojo bindDeviceSelectionTypePojo(Categories categories) {
        DeviceSelectionTypePojo deviceSelectionTypePojo = new DeviceSelectionTypePojo();
        if (categories == null) {
            return null;
        }
        deviceSelectionTypePojo.setFatherId(categories.getFatherId());
        deviceSelectionTypePojo.setId(categories.getId());
        deviceSelectionTypePojo.setName(categories.getName());
        return deviceSelectionTypePojo;
    }

    /**
     * 递归遍历成树结构
     *
     * @param treeList
     * @param id
     * @return
     */
    private List<DeviceSelectionTypePojo> buildDeviceSelectionTypePojoTree(List<DeviceSelectionTypePojo> treeList, String id) {
        List<DeviceSelectionTypePojo> children = new ArrayList<>();
        for (DeviceSelectionTypePojo deviceSelectionTypePojo : treeList) {
            if (id.equals(deviceSelectionTypePojo.getFatherId())) {
                deviceSelectionTypePojo.setChild(buildDeviceSelectionTypePojoTree(treeList, deviceSelectionTypePojo.getId()));
                children.add(deviceSelectionTypePojo);
            }
        }
        return children;
    }

}
