package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.RegionPojo;

import java.util.List;

public interface RegionService {

    /**
     * 获取所有城市区域
     *
     * @return
     */
    ServiceResult<List<RegionPojo>> getCities();
}
