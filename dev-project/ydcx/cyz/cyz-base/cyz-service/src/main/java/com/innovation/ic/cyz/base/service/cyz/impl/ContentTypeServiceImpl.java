package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.cyz.ContentTypeMapper;
import com.innovation.ic.cyz.base.model.cyz.ContentType;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.service.cyz.ContentTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;

@Service
@Transactional
public class ContentTypeServiceImpl extends ServiceImpl<ContentTypeMapper, ContentType> implements ContentTypeService {
    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 通过文件扩展名获取ContentType
     * @param fileExtension
     * @return
     */
    @Override
    public ServiceResult<String> getContentTypeByFileExtension(String fileExtension) {
        ServiceResult<String> serviceResult = new ServiceResult<>();
        QueryWrapper<ContentType> qwContentType = new QueryWrapper<>();
        qwContentType.lambda().eq(ContentType::getFileExt, fileExtension);
        ContentType contentType = serviceHelper.getContentTypeMapper().selectOne(qwContentType);
        if (contentType != null) {
            serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
            serviceResult.setSuccess(Boolean.TRUE);
            serviceResult.setResult(contentType.getContentType());
            return serviceResult;
        } else {
            return null;
        }
    }
}
