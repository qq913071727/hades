package com.innovation.ic.cyz.base.service.pve_standard.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.pve_standard.PcFilesMapper;
import com.innovation.ic.cyz.base.model.pve_standard.PcFiles;
import com.innovation.ic.cyz.base.service.pve_standard.PcFilesService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * @desc   PcFiles表接口实现类
 * @author linuo
 * @time   2022年11月9日09:29:45
 */
@Service
public class PcFilesServiceImpl extends ServiceImpl<PcFilesMapper, PcFiles> implements PcFilesService {
    @Resource
    private PcFilesMapper pcFilesMapper;

}