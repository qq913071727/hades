package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.cyz.BrandMapper;
import com.innovation.ic.cyz.base.model.cyz.Brand;
import com.innovation.ic.cyz.base.pojo.enums.RedisKeyPrefixEnum;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.service.cyz.BrandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author linuo
 * @desc Brands表接口实现类
 * @time 2022年10月28日10:18:15
 */
@Service
//@Transactional
@Slf4j
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements BrandService {
    @Resource
    private BrandMapper brandMapper;

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 批量插入品牌数据
     *
     * @param brandList 品牌数据
     * @return 返回插入结果
     */
    @Override
    public ServiceResult<Boolean> batchInsertBrandsList(List<Brand> brandList) {
        Boolean result = Boolean.FALSE;
        //this.batchInsertBrandRedis(brandList);
        Integer integer = brandMapper.insertBatchSomeColumn(brandList);
        if (integer > 0) {
            result = Boolean.TRUE;
        }
        ServiceResult<Boolean> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(result);
        serviceResult.setResult(result);
        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        return serviceResult;
    }


    /***
     * 批量保存redis
     * @param brandList
     */
    private void batchInsertBrandRedis(List<Brand> brandList) {
        String key = RedisKeyPrefixEnum.TABLE.getCode() + ":" + RedisKeyPrefixEnum.TABLE_BRAND.getCode();
        try {
            List<Brand> insertDataList = new ArrayList<>();
            for (int i = 0; i < brandList.size(); i++) {
                insertDataList.add(brandList.get(i));
                if (i % 1000 == 0 && i != 0) {
                    serviceHelper.getRedisManager().lPushAll(key, insertDataList);
                    this.sleep();
                    insertDataList.clear();
                }
            }
            if (!CollectionUtils.isEmpty(insertDataList)) {
                serviceHelper.getRedisManager().lPushAll(key, insertDataList);
            }
        } catch (Exception e) {
            log.error("定时任务,保存Redis key :  {} 失败 ", key, e);
        }

    }

    /**
     * 线程处理睡眠
     */
    private void sleep() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            log.error("线程睡眠发生异常", e);
        }
    }


    /**
     * 清空brand表数据
     */
    @Override
    public void truncateBrand() {
        brandMapper.truncateBrand();
    }

    @Override
    public Integer total() {
        return this.baseMapper.selectCount(new QueryWrapper<>());
    }

    @Override
    public List<Brand> findByPage(int pageNum, int pageSize) {
        int start = pageNum * pageSize;
        int end = start + pageSize;
        if (start > 0) {
            start += 1;
        }
        return this.baseMapper.findByPage(start, end);
    }
}