package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrPcbDetailPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrPcbListResultPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.mfrpcb_search_option.MfrPcbOptionLine;
import com.innovation.ic.cyz.base.vo.cyz.MfrPcbListVo;

import java.util.List;

/**
 * pcb厂家Web使用接口
 */
public interface MfrPcbWebService {

    /**
     * 根据传入的条件查询PCB厂家列表
     *
     * @param mfrListVo
     * @return
     */
    ServiceResult<MfrPcbListResultPojo> findMfrList(MfrPcbListVo mfrListVo) throws NoSuchFieldException, IllegalAccessException;

    /**
     * 根据mfrId查询PCB厂家详情
     *
     * @param mfrId
     * @return
     */
    ServiceResult<MfrPcbDetailPojo> findMfrById(Long mfrId) throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException, InstantiationException;

    /**
     * 查询列表选项
     *
     * @return
     */
    ServiceResult<List<MfrPcbOptionLine>> findListOptions();
}
