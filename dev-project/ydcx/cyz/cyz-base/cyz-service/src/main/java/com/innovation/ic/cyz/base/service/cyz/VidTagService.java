package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.VidTag;

import java.util.List;

public interface VidTagService {
    List<VidTag> findByPage(int pageNum, int pageSize);
}
