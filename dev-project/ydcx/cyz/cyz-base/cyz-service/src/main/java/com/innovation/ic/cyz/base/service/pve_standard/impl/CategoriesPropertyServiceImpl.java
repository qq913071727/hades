package com.innovation.ic.cyz.base.service.pve_standard.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.pve_standard.CategoriesPropertyMapper;
import com.innovation.ic.cyz.base.model.pve_standard.CategoriesProperty;
import com.innovation.ic.cyz.base.service.pve_standard.CategoriesPropertyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CategoriesPropertyServiceImpl extends ServiceImpl<CategoriesPropertyMapper, CategoriesProperty> implements CategoriesPropertyService {


    @Override
    public List<CategoriesProperty> findByPage(int pageNum, int pageSize) {
       return this.baseMapper.findByPage(pageNum*pageSize,pageSize);
    }

    @Override
    public Integer effectiveData() {
        QueryWrapper<CategoriesProperty> categoriesPropertyQueryWrapper = new QueryWrapper<>();
        categoriesPropertyQueryWrapper.eq("State",200);
        return this.baseMapper.selectCount(categoriesPropertyQueryWrapper);
    }


}


