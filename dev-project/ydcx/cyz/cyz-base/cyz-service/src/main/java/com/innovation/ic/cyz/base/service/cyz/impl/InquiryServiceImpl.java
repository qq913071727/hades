package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.framework.util.IdUtil;
import com.innovation.ic.cyz.base.model.cyz.Inquiry;
import com.innovation.ic.cyz.base.mapper.cyz.InquiryMapper;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.constant.InquiryExportStatus;
import com.innovation.ic.cyz.base.pojo.variable.cyz.InquiryExportPojo;
import com.innovation.ic.cyz.base.service.cyz.InquiryService;
import com.innovation.ic.cyz.base.vo.cyz.InquiryMiniSaveVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 在线询价具体实现类
 */
@Service
@Transactional
public class InquiryServiceImpl extends ServiceImpl<InquiryMapper, Inquiry> implements InquiryService {

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 在线询价提交
     *
     * @param inquiryMiniSaveVo
     * @return
     */
    @Override
    public ServiceResult miniSave(InquiryMiniSaveVo inquiryMiniSaveVo) {
        Inquiry inquiry = new Inquiry();
        inquiry.setId(IdUtil.snowflakeNextId());
        inquiry.setMfrId(inquiryMiniSaveVo.getMfrId());
        inquiry.setCompanyName(inquiryMiniSaveVo.getCompanyName());
        inquiry.setUserName(inquiryMiniSaveVo.getUserName());
        inquiry.setTel(inquiryMiniSaveVo.getTel());
        inquiry.setExportStatus(InquiryExportStatus.NO);
        inquiry.setCreateTime(new Date());
        serviceHelper.getInquiryMapper().insert(inquiry);

        ServiceResult serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);

        return serviceResult;
    }

    /**
     * 分页显示
     *
     * @param pageSize
     * @param pageNo
     * @return
     */
    @Override
    public ServiceResult<Page<Inquiry>> page(Integer pageSize, Integer pageNo) {
        Page<Inquiry> page = new Page<Inquiry>(pageNo, pageSize);
        Page<Inquiry> inquiryPage = serviceHelper.getInquiryMapper().selectPage(page, null);

        ServiceResult serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(inquiryPage);
        return serviceResult;
    }

    /**
     * 根据id，删除一条记录
     *
     * @param id
     * @return
     */
    @Override
    public ServiceResult<Boolean> deleteById(Long id) {
        baseMapper.deleteById(id);

        ServiceResult serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.DELETE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        return serviceResult;
    }

    /**
     * 查询所有记录，只返回excel需要的列。然后将所有记录的export_status字段更新为1
     *
     * @return
     */
    @Override
    public ServiceResult<List<Object[]>> findAllRecordForExcel() {
        // 查询所有记录，只返回excel需要的列
        List<InquiryExportPojo> inquiryExportPojoList = serviceHelper.getInquiryMapper().findAllRecordForExcel();
        List<Object[]> objectArrayList = new ArrayList<>();
        for (int i = 0; i < inquiryExportPojoList.size(); i++) {
            InquiryExportPojo inquiryExportPojo = inquiryExportPojoList.get(i);
            Object[] objectArray = new Object[]{inquiryExportPojo.getCompanyName(),
                    inquiryExportPojo.getUserName(),
                    inquiryExportPojo.getTel(),
                    inquiryExportPojo.getCreateTime()};
            objectArrayList.add(objectArray);
        }

        // 将所有记录的export_status字段更新为1
        UpdateWrapper<Inquiry> updateWrapper = new UpdateWrapper<>();
        Inquiry inquiry = new Inquiry();
        inquiry.setExportStatus(InquiryExportStatus.YES);
        baseMapper.update(inquiry, updateWrapper);

        ServiceResult serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(objectArrayList);
        return serviceResult;
    }

    /**
     * 查询所有未导出过记录，只返回excel需要的列。然后将所有记录的export_status字段更新为1
     *
     * @return
     */
    @Override
    public ServiceResult<List<Object[]>> findNotExportRecordForExcel() {
        // 查询所有未导出过记录，只返回excel需要的列
//        List<Object[]> inquiryList = serviceHelper.getInquiryMapper().findNotExportRecordForExcel();

        // 查询所有记录，只返回excel需要的列
        List<InquiryExportPojo> inquiryExportPojoList = serviceHelper.getInquiryMapper().findNotExportRecordForExcel();
        List<Object[]> objectArrayList = new ArrayList<>();
        for (int i = 0; i < inquiryExportPojoList.size(); i++) {
            InquiryExportPojo inquiryExportPojo = inquiryExportPojoList.get(i);
            Object[] objectArray = new Object[]{inquiryExportPojo.getCompanyName(),
                    inquiryExportPojo.getUserName(),
                    inquiryExportPojo.getTel(),
                    inquiryExportPojo.getCreateTime()};
            objectArrayList.add(objectArray);
        }

        // 将所有记录的export_status字段更新为1
        UpdateWrapper<Inquiry> updateWrapper = new UpdateWrapper<>();
        Inquiry inquiry = new Inquiry();
        inquiry.setExportStatus(InquiryExportStatus.YES);
        baseMapper.update(inquiry, updateWrapper);

        ServiceResult serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(objectArrayList);
        return serviceResult;
    }
}
