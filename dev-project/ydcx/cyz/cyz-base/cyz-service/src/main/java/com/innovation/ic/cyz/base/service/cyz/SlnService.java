package com.innovation.ic.cyz.base.service.cyz;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cyz.base.model.cyz.Sln;
import com.innovation.ic.cyz.base.model.cyz.SlnIde;
import com.innovation.ic.cyz.base.model.cyz.SlnLikeTag;
import com.innovation.ic.cyz.base.model.cyz.SlnUrl;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.SlnDetailPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.SlnListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.SlnListResultPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.sln_search_option.SlnOptionLine;
import com.innovation.ic.cyz.base.vo.cyz.SlnListVo;

import java.util.List;

/**
 * 方案接口
 */
public interface SlnService {

    /**
     *  ioc容器中SlnService默认的实现类，SpringContextUtil通过次属性获取bean
     */
    String SLN_SERVICE_IMPL_BEAN_NAME = "slnServiceImpl";

    /**
     * 根据传入的条件查询方案列表
     *
     * @param slnListVo
     * @return
     */
    ServiceResult<SlnListResultPojo> findSlnList(SlnListVo slnListVo);

    /**
     * 根据id查询方案详情
     *
     * @param slnId
     * @return
     */
    ServiceResult<SlnDetailPojo> findSlnDetailById(Long slnId);

    /**
     * 查询列表选项
     *
     * @return
     */
    ServiceResult<List<SlnOptionLine>> findListOptions();

    /**
     * @params 新增BIB方案数据
     * @Date 2022/9/16 14:09
     * @Author Mr.myq
     * @Description
     * @Return
     */
    ServiceResult addSinInfo(Sln sin);


    /**
     * @params 新增BIB方案其他封面数据
     * @Date 2022/9/16 14:09
     * @Author Mr.myq
     * @Description
     * @Return
     */
    ServiceResult addSinUrls(List<SlnUrl> sinUrls);

    /**
     * @params 新增BIB方案数据
     * @Date 2022/9/16 14:09
     * @Author Mr.myq
     * @Description
     * @Return
     */
    ServiceResult updateSinInfo(Sln sin);


    /**
     * @params 新增BIB方案其他封面数据
     * @Date 2022/9/16 14:09
     * @Author Mr.myq
     * @Description
     * @Return
     */
    ServiceResult updateSinUrls(List<SlnUrl> sinUrls);


    /**
     * @Description: 删除方案以及方案图其数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2117:14
     */
    ServiceResult deleteSlnInfo(Sln sln);


    /**
     * @Description: 查看方案信息
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/229:37
     * @return
     */
    ServiceResult<PageInfo<SlnListPojo>> page(int pageNo, int pageSize, SlnListVo slnListVo);

    /**
     * @Description: 热门领域列表
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2210:38
     */
    ServiceResult<List<SlnLikeTag>> hotNameList();

    /**
     * @Description: 开发平台列表
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2210:46
     */
    ServiceResult<List<SlnIde>> slnIdeList();

    /**
     * @Description: 更新方案
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2211:01
     */
    ServiceResult update(Sln sln);

    /**
     * @Description: 删除方案
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2211:06
     * @param sln
     */
    ServiceResult delete(Long sln);

    /**
     * @Description: B1B数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2313:41
     */
    ServiceResult<PageInfo<SlnListPojo>> b1bPage(int pageNo, int pageSize, String startTime);
}
