package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.VidBrand;

import java.util.List;

public interface VidBrandService {
    List<VidBrand> findByPage(int pageNum, int pageSize);
}
