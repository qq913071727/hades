package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.Brand;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import java.util.List;

/**
 * @desc   Brand表接口
 * @author linuo
 * @time   2022年10月28日10:16:23
 */
public interface BrandService {
    /**
     * 批量插入品牌数据
     * @param brandList 品牌数据
     * @return 返回插入结果
     */
    ServiceResult<Boolean> batchInsertBrandsList(List<Brand> brandList);

    /**
     * 清空brand表数据
     */
    void truncateBrand();

    Integer total();


    /**
     * 不是一般排序,针对于大数据量分页查询优化
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<Brand> findByPage(int pageNum, int pageSize);
}