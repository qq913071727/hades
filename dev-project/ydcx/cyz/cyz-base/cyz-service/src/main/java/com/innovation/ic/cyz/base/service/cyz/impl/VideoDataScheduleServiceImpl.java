package com.innovation.ic.cyz.base.service.cyz.impl;

import com.innovation.ic.cyz.base.model.cyz.VidBrand;
import com.innovation.ic.cyz.base.model.cyz.VidCategory;
import com.innovation.ic.cyz.base.model.cyz.VidRel;
import com.innovation.ic.cyz.base.model.cyz.VidTag;
import com.innovation.ic.cyz.base.model.cyz.VidType;
import com.innovation.ic.cyz.base.model.cyz.Video;
import com.innovation.ic.cyz.base.pojo.enums.RedisKeyPrefixEnum;
import com.innovation.ic.cyz.base.service.cyz.VidBrandService;
import com.innovation.ic.cyz.base.service.cyz.VidCategoryService;
import com.innovation.ic.cyz.base.service.cyz.VidRelService;
import com.innovation.ic.cyz.base.service.cyz.VidTagService;
import com.innovation.ic.cyz.base.service.cyz.VidTypeService;
import com.innovation.ic.cyz.base.service.cyz.VideoDataScheduleService;
import com.innovation.ic.cyz.base.service.cyz.VideoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class VideoDataScheduleServiceImpl implements VideoDataScheduleService {

    @Resource
    private VideoService videoService;

    @Resource
    private VidCategoryService vidCategoryService;

    @Resource
    private VidBrandService vidBrandService;

    @Resource
    private VidTagService vidTagService;

    @Resource
    private VidTypeService vidTypeService;

    @Resource
    private VidRelService vidRelService;

    @Resource
    private ServiceHelper serviceHelper;

    @Override
    public void importVideoRedisSchedule() {
        //删除数据
        String key = RedisKeyPrefixEnum.TABLE.getCode() + ":" + RedisKeyPrefixEnum.TABLE_VIDEO.getCode();
        serviceHelper.getRedisManager().del(key);
        int pageNum = 0;
        int pageSize = 100;
        for (; ; ) {
            pageNum++;
            this.sleep(pageNum);
            List<Video> videoList = videoService.findByPage(pageNum, pageSize);
            if (CollectionUtils.isEmpty(videoList)) {
                break;
            }
            serviceHelper.getRedisManager().lPushAll(key, videoList);
        }
    }

    @Override
    public void importVidCategoryRedisSchedule() {
        //删除数据
        String key = RedisKeyPrefixEnum.TABLE.getCode() + ":" + RedisKeyPrefixEnum.TABLE_VID_CATEGORY.getCode();
        serviceHelper.getRedisManager().del(key);
        int pageNum = 0;
        int pageSize = 100;
        for (; ; ) {
            pageNum++;
            this.sleep(pageNum);
            List<VidCategory> vidCategoryList = vidCategoryService.findByPage(pageNum, pageSize);
            if (CollectionUtils.isEmpty(vidCategoryList)) {
                break;
            }
            serviceHelper.getRedisManager().lPushAll(key, vidCategoryList);
        }
    }

    @Override
    public void importVidBrandRedisSchedule() {
        //删除数据
        String key = RedisKeyPrefixEnum.TABLE.getCode() + ":" + RedisKeyPrefixEnum.TABLE_VID_BRAND.getCode();
        serviceHelper.getRedisManager().del(key);
        int pageNum = 0;
        int pageSize = 100;
        for (; ; ) {
            pageNum++;
            this.sleep(pageNum);
            List<VidBrand> vidBrandList = vidBrandService.findByPage(pageNum, pageSize);
            if (CollectionUtils.isEmpty(vidBrandList)) {
                break;
            }
            serviceHelper.getRedisManager().lPushAll(key, vidBrandList);
        }
    }

    @Override
    public void importVidTagRedisSchedule() {
        //删除数据
        String key = RedisKeyPrefixEnum.TABLE.getCode() + ":" + RedisKeyPrefixEnum.TABLE_VID_TAG.getCode();
        serviceHelper.getRedisManager().del(key);
        int pageNum = 0;
        int pageSize = 100;
        for (; ; ) {
            pageNum++;
            this.sleep(pageNum);
            List<VidTag> vidTagList = vidTagService.findByPage(pageNum, pageSize);
            if (CollectionUtils.isEmpty(vidTagList)) {
                break;
            }
            serviceHelper.getRedisManager().lPushAll(key, vidTagList);
        }
    }

    @Override
    public void importVidTypeRedisSchedule() {
        //删除数据
        String key = RedisKeyPrefixEnum.TABLE.getCode() + ":" + RedisKeyPrefixEnum.TABLE_VID_TYPE.getCode();
        serviceHelper.getRedisManager().del(key);
        int pageNum = 0;
        int pageSize = 100;
        for (; ; ) {
            pageNum++;
            this.sleep(pageNum);
            List<VidType> vidTagList = vidTypeService.findByPage(pageNum, pageSize);
            if (CollectionUtils.isEmpty(vidTagList)) {
                break;
            }
            serviceHelper.getRedisManager().lPushAll(key, vidTagList);
        }
    }

    @Override
    public void importVidRelRedisSchedule() {
        //删除数据
        String key = RedisKeyPrefixEnum.TABLE.getCode() + ":" + RedisKeyPrefixEnum.TABLE_VID_REL.getCode();
        serviceHelper.getRedisManager().del(key);
        int pageNum = 0;
        int pageSize = 100;
        for (; ; ) {
            pageNum++;
            this.sleep(pageNum);
            List<VidRel> vidRelList = vidRelService.findByPage(pageNum, pageSize);
            if (CollectionUtils.isEmpty(vidRelList)) {
                break;
            }
            serviceHelper.getRedisManager().lPushAll(key, vidRelList);
        }
    }


    /**
     * 线程处理睡眠
     */
    private void sleep(int pageNum) {
        try {
            if(pageNum % 3 == 0 && pageNum !=0){
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            log.error("线程睡眠发生异常",e);
        }
    }


}
