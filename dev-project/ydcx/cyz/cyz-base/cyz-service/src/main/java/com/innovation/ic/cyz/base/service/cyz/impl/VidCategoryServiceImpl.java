package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.innovation.ic.cyz.base.mapper.cyz.VidCategoryMapper;
import com.innovation.ic.cyz.base.model.cyz.VidCategory;
import com.innovation.ic.cyz.base.service.cyz.VidCategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class VidCategoryServiceImpl extends ServiceImpl<VidCategoryMapper, VidCategory> implements VidCategoryService {
    @Override
    public List<VidCategory> findByPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        QueryWrapper<VidCategory> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("id");
        return this.baseMapper.selectList(queryWrapper);
    }
}
