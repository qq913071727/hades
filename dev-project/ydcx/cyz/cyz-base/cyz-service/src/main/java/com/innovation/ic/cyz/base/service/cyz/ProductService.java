package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.Product;
import com.innovation.ic.cyz.base.pojo.global.ApiPageResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.product.ProductQueryRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ps_productunique.PsProdsUniqPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ps_property.PsPropertyResultPojo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsPartNumbersQueryByContextVo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsProdsUniqVo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_property.PsPropertyVo;

import java.util.List;

/**
 * @desc   Product表接口
 * @author linuo
 * @time   2022年10月28日17:30:12
 */
public interface ProductService {

    /**
     * 清空product表数据
     */
    void truncateProduct();

    /**
     * 批量插入Product表数据
     * @param list 需要插入的数据
     * @return 返回插入结果
     */
    ServiceResult<Boolean> batchInsertProductsList(List<Product> list) throws InterruptedException;

    /**
     * 型号模糊查询
     * @param psPartNumbersQueryByContextVo 型号模糊查询的vo类
     * @return 返回查询结果
     */
    ServiceResult<List<ProductQueryRespPojo>> queryPartNumbersByContext(PsPartNumbersQueryByContextVo psPartNumbersQueryByContextVo);

    Integer total();

    List<Product> findByPage(int pageNum, int pageSize);

    /**
     * 根据分类查询备选属性值
     */
    ServiceResult<PsPropertyResultPojo> getPsProperties(List<String> categoriesList);

    /**
     * 查询器件选型列表
     *
     * @return
     */
    ServiceResult<ApiPageResult<PsProdsUniqPojo>> getPsProducts(PsProdsUniqVo param);
}