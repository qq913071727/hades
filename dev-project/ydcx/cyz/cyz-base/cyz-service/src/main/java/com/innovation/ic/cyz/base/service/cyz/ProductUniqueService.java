package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.ProductUnique;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ps_productunique.PsProdsUniqPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ps_productunique.PsProductQueryByBrandPartRespPojo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsPartNumbersQueryByContextVo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsProductQueryByIdVo;

import java.util.List;

/**
 * @desc   product_unique表接口
 * @author linuo
 * @time   2022年11月1日13:54:38
 */
public interface ProductUniqueService {
    /**
     * 清空product_unique表数据
     */
    void truncateProductUnique();

    /**
     * 批量插入ProductUnique表数据
     * @param list 需要插入的数据
     * @return 返回插入结果
     */
    ServiceResult<Boolean> batchInsertProductUniquesList(List<ProductUnique> list) throws InterruptedException;

    Integer total();

    List<ProductUnique> findByPage(int pageNum, int pageSize);

    /**
     * 型号模糊查询
     * @param psPartNumbersQueryByContextVo 型号模糊查询的vo类
     * @return 返回查询结果
     */
    ServiceResult<List<String>> queryPartNumbersByContext(PsPartNumbersQueryByContextVo psPartNumbersQueryByContextVo);

    /**
     * 根据品牌、型号查询器件
     * @param psProductQueryByIdVo 查询条件
     * @return 返回查询结果
     */
    ServiceResult<PsProductQueryByBrandPartRespPojo> queryProductsByBrandPartNumber(PsProductQueryByIdVo psProductQueryByIdVo);

    /**
     * 根据查询器件选型列表
     *
     * @return
     */
    ServiceResult<List<PsProdsUniqPojo>> getPsPPProductsByUids(String[] uids);
}