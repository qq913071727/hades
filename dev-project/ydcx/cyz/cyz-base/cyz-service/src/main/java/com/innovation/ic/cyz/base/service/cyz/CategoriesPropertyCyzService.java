package com.innovation.ic.cyz.base.service.cyz;



import com.innovation.ic.cyz.base.model.cyz.CategoriesProperty;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.PsCategoryPropertyPojo;

import java.util.List;

public interface CategoriesPropertyCyzService {


    void batchSave(List<CategoriesProperty> categoriesList);

    void truncatetCategoriesProperty();

    //获取件选型配置属性
    ServiceResult<List<PsCategoryPropertyPojo>> selectCategoriesProperty(List<String> categoriesList);

    /**
     * 获取分类属性
     *
     * @return
     * @param categoriesList
     */
    ServiceResult<List<PsCategoryPropertyPojo>> getPsCategoriesProperty(List<String> categoriesList);
}
