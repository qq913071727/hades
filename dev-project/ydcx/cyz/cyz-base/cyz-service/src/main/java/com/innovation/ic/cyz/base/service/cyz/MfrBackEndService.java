package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrBackEndPageListPojo;
import com.innovation.ic.cyz.base.vo.cyz.AddMfrBackEndVo;
import com.innovation.ic.cyz.base.vo.cyz.MfrBackEndPageVo;

public interface MfrBackEndService {
    ServiceResult addMfrBackEndVo(AddMfrBackEndVo addMfrBackEndVo);

    void deleteMfrBackEnd(Long mfrId);

    ServiceResult<MfrBackEndPageListPojo> findByPage(MfrBackEndPageVo mfrBackEndPageVo) throws NoSuchFieldException, Exception;
}
