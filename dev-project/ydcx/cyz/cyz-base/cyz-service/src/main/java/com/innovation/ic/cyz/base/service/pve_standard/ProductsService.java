package com.innovation.ic.cyz.base.service.pve_standard;

import com.innovation.ic.cyz.base.model.cyz.Product;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import java.util.List;

/**
 * @desc   Products表接口
 * @author linuo
 * @time   2022年10月28日17:12:08
 */
public interface ProductsService {
    /**
     * 查询标准库Products状态是正常的数据数量
     * @return 返回查询结果
     */
    ServiceResult<Long> getNormalStatusDataCount();

    /**
     * 分页查询Products表中状态为正常的数据
     * @param start 分页开始
     * @param end 分页结束
     * @return 返回查询结果
     */
    ServiceResult<List<Product>> getNormalStatusDataByPage(long start, long end);
}