package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.pojo.global.ServiceResult;

/**
 * 数据处理的服务类接口
 */
public interface DataService {

    /**
     * 根据redis的key删除对应数据
     * @param redisKey
     * @return 返回删除结果
     */
    ServiceResult<Boolean> delRedisDataByKey(String redisKey);

    /**
     * 将carousel表中的数据导入redis
     * @return 返回导入结果
     */
    ServiceResult<Boolean> importCarouselIntoRedis();
}