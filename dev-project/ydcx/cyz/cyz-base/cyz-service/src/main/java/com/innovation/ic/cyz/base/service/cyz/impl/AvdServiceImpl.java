package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.innovation.ic.b1b.framework.util.BeanPropertiesUtil;
import com.innovation.ic.b1b.framework.util.IdUtil;
import com.innovation.ic.cyz.base.mapper.cyz.AvdMapper;
import com.innovation.ic.cyz.base.model.cyz.Avd;
import com.innovation.ic.cyz.base.model.cyz.AvdBrand;
import com.innovation.ic.cyz.base.model.cyz.AvdRel;
import com.innovation.ic.cyz.base.model.cyz.AvdTag;
import com.innovation.ic.cyz.base.model.cyz.AvdType;
import com.innovation.ic.cyz.base.model.cyz.User;
import com.innovation.ic.cyz.base.model.cyz.UserViewLog;
import com.innovation.ic.cyz.base.pojo.constant.AvdApproveStatus;
import com.innovation.ic.cyz.base.pojo.constant.AvdModule;
import com.innovation.ic.cyz.base.pojo.constant.AvdRelModule;
import com.innovation.ic.cyz.base.pojo.constant.GeneralStatus;
import com.innovation.ic.cyz.base.pojo.constant.UserViewLogType;
import com.innovation.ic.cyz.base.pojo.enums.RedisKeyPrefixEnum;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdDetailPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdEditOptionItemPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdEditOptionsPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdListRelItemPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdListResultPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdSavePojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdUserPojo;
import com.innovation.ic.cyz.base.service.cyz.AvdService;
import com.innovation.ic.cyz.base.vo.cyz.avd.AvdApproveVo;
import com.innovation.ic.cyz.base.vo.cyz.avd.AvdListVo;
import com.innovation.ic.cyz.base.vo.cyz.avd.AvdSaveVo;
import com.innovation.ic.cyz.base.vo.cyz.avd.UserViewLogReduceVo;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 避坑指南具体实现类
 */
@Service
@Transactional
public class AvdServiceImpl extends ServiceImpl<AvdMapper, Avd> implements AvdService {

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 将AvdRelIds转换为AvdRelList
     *
     * @param avdRelModule
     * @param avdId
     * @param avdRelIds
     * @return
     */
    private List<AvdRel> convertAvdRelIdsToAvdRelList(Integer avdRelModule, Long avdId, List<Long> avdRelIds) {
        List<AvdRel> avdRelList = new ArrayList<>();
        for (Long avdRelId : avdRelIds) {
            AvdRel avdRelType = new AvdRel();
            avdRelType.setId(IdUtil.snowflakeNextId());
            avdRelType.setModule(avdRelModule);
            avdRelType.setAvdId(avdId);
            avdRelType.setAvdRelId(avdRelId);
            avdRelList.add(avdRelType);
        }
        return avdRelList;
    }

    /**
     * 新增保存
     *
     * @param avdSaveVo
     * @return
     */
    @Override
    public ServiceResult<AvdSavePojo> insertSave(AvdSaveVo avdSaveVo, String userId) {
        Avd avd = new Avd();
        avd.setTitle(avdSaveVo.getTitle());
        avd.setCover(avdSaveVo.getCover());
        avd.setVideo(avdSaveVo.getVideo());
        avd.setModule(avdSaveVo.getModule());
        avd.setDescription(avdSaveVo.getDescription());
        avd.setParagraph(avdSaveVo.getParagraph());
        avd.setViewNum(0);
        avd.setLikeNum(0);
        avd.setCollectNum(0);
        avd.setApproveStatus((avdSaveVo.getIsDraft() != null && avdSaveVo.getIsDraft())
                ? AvdApproveStatus.Draft : AvdApproveStatus.UnApprove);
        avd.setStatus(GeneralStatus.NORMAL);
        avd.setCreatorId(userId);
        avd.setCreateDate(new Date());
        serviceHelper.getAvdMapper().insert(avd);

        // AvdRel
        List<AvdRel> avdRelList = new ArrayList<>();
        // AvdRel 分类
        avdRelList.addAll(convertAvdRelIdsToAvdRelList(AvdRelModule.Type, avd.getId(), avdSaveVo.getTypeIds()));
        // AvdRel 标签
        avdRelList.addAll(convertAvdRelIdsToAvdRelList(AvdRelModule.Tag, avd.getId(), avdSaveVo.getTagIds()));
        // AvdRel 品牌
        avdRelList.addAll(convertAvdRelIdsToAvdRelList(AvdRelModule.Brand, avd.getId(), avdSaveVo.getBrandIds()));

        // 插入数据
        serviceHelper.getAvdRelMapper().insertBatchSomeColumn(avdRelList);

        // 返回
        AvdSavePojo result = new AvdSavePojo();
        result.setAvdId(avd.getId());

        ServiceResult<AvdSavePojo> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);

        return serviceResult;
    }

    /**
     * 编辑保存
     *
     * @param avdSaveVo
     * @param avdId
     * @return
     */
    @Override
    public ServiceResult<AvdSavePojo> updateSave(AvdSaveVo avdSaveVo, Long avdId, String userId) {
        // Avd
        UpdateWrapper<Avd> uwAvd = new UpdateWrapper<>();
        uwAvd.lambda().eq(Avd::getId, avdId);
        uwAvd.lambda().set(Avd::getTitle, avdSaveVo.getTitle());
        uwAvd.lambda().set(Avd::getCover, avdSaveVo.getCover());
        uwAvd.lambda().set(Avd::getVideo, avdSaveVo.getVideo());
        uwAvd.lambda().set(Avd::getModule, avdSaveVo.getModule());
        uwAvd.lambda().set(Avd::getDescription, avdSaveVo.getDescription());
        uwAvd.lambda().set(Avd::getParagraph, avdSaveVo.getParagraph());
        uwAvd.lambda().set(Avd::getApproveStatus, (avdSaveVo.getIsDraft() != null && avdSaveVo.getIsDraft())
                ? AvdApproveStatus.Draft : AvdApproveStatus.UnApprove);
        uwAvd.lambda().set(Avd::getModifierId, userId);
        uwAvd.lambda().set(Avd::getModifyDate, new Date());

        // AvdRel
        List<AvdRel> avdRelList = new ArrayList<>();
        // AvdRel 分类
        avdRelList.addAll(convertAvdRelIdsToAvdRelList(AvdRelModule.Type, avdId, avdSaveVo.getTypeIds()));
        // AvdRel 标签
        avdRelList.addAll(convertAvdRelIdsToAvdRelList(AvdRelModule.Tag, avdId, avdSaveVo.getTagIds()));
        // AvdRel 品牌
        avdRelList.addAll(convertAvdRelIdsToAvdRelList(AvdRelModule.Brand, avdId, avdSaveVo.getBrandIds()));

        // 更新/插入数据
        serviceHelper.getAvdMapper().update(null, uwAvd);

        QueryWrapper<AvdRel> qwAvdRel = new QueryWrapper<>();
        qwAvdRel.lambda().eq(AvdRel::getAvdId, avdId);
        serviceHelper.getAvdRelMapper().delete(qwAvdRel);
        serviceHelper.getAvdRelMapper().insertBatchSomeColumn(avdRelList);

        // 返回
        AvdSavePojo result = new AvdSavePojo();
        result.setAvdId(avdId);

        ServiceResult<AvdSavePojo> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);

        return serviceResult;
    }

    /**
     * 编辑选项
     *
     * @return
     */
    @Override
    public ServiceResult<AvdEditOptionsPojo> editOptions() {
        // 文章/视频选项
        Map<Integer, String> moduleMap = new HashMap<>();
        moduleMap.put(AvdModule.ARTICLE, AvdModule.ARTICLE_CHINESE);
        moduleMap.put(AvdModule.VIDEO, AvdModule.VIDEO_CHINESE);
        var moduleOptions = moduleMap.entrySet().stream().map(mapItem -> {
            AvdEditOptionItemPojo itemPojo = new AvdEditOptionItemPojo();
            itemPojo.setAvdRelId(Long.valueOf(mapItem.getKey()));
            itemPojo.setName(mapItem.getValue());
            return itemPojo;
        }).collect(Collectors.toList());

        // 分类选项
        QueryWrapper<AvdType> qwType = new QueryWrapper();
        qwType.lambda().eq(AvdType::getStatus, GeneralStatus.NORMAL);
        qwType.orderByAsc("order_index");
        List<AvdType> avdTypes = serviceHelper.getAvdTypeMapper().selectList(qwType);
        var typeOptions = avdTypes.stream().map(item -> {
            AvdEditOptionItemPojo itemPojo = new AvdEditOptionItemPojo();
            itemPojo.setAvdRelId(item.getId());
            itemPojo.setName(item.getName());
            return itemPojo;
        }).collect(Collectors.toList());

        // 标签选项
        QueryWrapper<AvdTag> qwTag = new QueryWrapper();
        qwTag.lambda().eq(AvdTag::getStatus, GeneralStatus.NORMAL);
        qwTag.orderByAsc("order_index");
        List<AvdTag> avdTags = serviceHelper.getAvdTagMapper().selectList(qwTag);
        var tagOptions = avdTags.stream().map(item -> {
            AvdEditOptionItemPojo itemPojo = new AvdEditOptionItemPojo();
            itemPojo.setAvdRelId(item.getId());
            itemPojo.setName(item.getTag());
            return itemPojo;
        }).collect(Collectors.toList());

        // 品牌选项
        QueryWrapper<AvdBrand> qwBrand = new QueryWrapper<>();
        qwBrand.lambda().eq(AvdBrand::getStatus, GeneralStatus.NORMAL);
        qwBrand.orderByAsc("order_index");
        List<AvdBrand> avdBrands = serviceHelper.getAvdBrandMapper().selectList(qwBrand);
        var brandOptions = avdBrands.stream().map(item -> {
            AvdEditOptionItemPojo itemPojo = new AvdEditOptionItemPojo();
            itemPojo.setAvdRelId(item.getId());
            itemPojo.setName(item.getBrand());
            return itemPojo;
        }).collect(Collectors.toList());

        // 返回
        AvdEditOptionsPojo result = new AvdEditOptionsPojo();
        result.setModuleOptions(moduleOptions);
        result.setTypeOptions(typeOptions);
        result.setTagOptions(tagOptions);
        result.setBrandOptions(brandOptions);

        ServiceResult<AvdEditOptionsPojo> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);

        return serviceResult;
    }

    /**
     * 根据传入的条件查询避坑指南列表
     *
     * @param avdListVo
     * @param userId
     * @return
     */
    @Override
    public ServiceResult<AvdListResultPojo> findAvdList(AvdListVo avdListVo, String userId) {
        List<AvdListPojo> avdList = new ArrayList<>();
        Long avdCount = 0L;

        // 查询Avd数据及总数量
        if (avdListVo.getIsPublic()) {
            avdList = serviceHelper.getAvdMapper().findAvdListForPublic(avdListVo.getPage(), avdListVo.getSize(),
                    avdListVo.getTitle(),
                    avdListVo.getModule(),
                    avdListVo.getTypeIds());
            avdCount = serviceHelper.getAvdMapper().findAvdCountForPublic(
                    avdListVo.getTitle(),
                    avdListVo.getModule(),
                    avdListVo.getTypeIds());
        } else if (avdListVo.getIsPrivate()) {
            List<Integer> approveStatuses = avdListVo.getApproveStatuses();

            avdList = serviceHelper.getAvdMapper().findAvdListForPrivate(avdListVo.getPage(), avdListVo.getSize(),
                    approveStatuses,
                    userId);
            avdCount = serviceHelper.getAvdMapper().findAvdCountForPrivate(
                    approveStatuses,
                    userId);
        }

        // 给封面地址添加url前缀、查询标签
        if (avdList != null && !avdList.isEmpty()) {
            List<Long> avdIds = avdList.stream().map(t -> t.getAvdId()).collect(Collectors.toList());
            List<AvdListRelItemPojo> allTags = serviceHelper.getAvdTagMapper().findAvdRelTagsByAvdIds(avdIds);
            for (var avd : avdList) {
                // 给封面地址添加url前缀
                if (avd.getCover() != null && !avd.getCover().isEmpty()) {
                    avd.setCover(serviceHelper.getFtpAccountConfig().getImageUrlBasePath() + avd.getCover());
                }
                // 查询标签
                var theTags = allTags.stream().filter(t -> t.getAvdId().equals(avd.getAvdId()))
                        .collect(Collectors.toList());
                avd.setTags(theTags);
            }
        }

        // 查询作者信息
        if (avdList != null && !avdList.isEmpty()) {
            List<String> creatorIds = avdList.stream().map(t -> t.getCreatorId()).collect(Collectors.toList());
            QueryWrapper<User> qwUser = new QueryWrapper<>();
            qwUser.lambda().in(User::getId, creatorIds);
            List<User> users = serviceHelper.getUserMapper().selectList(qwUser);
            for (var avd : avdList) {
                Optional<User> userOptional = users.stream().filter(t -> t.getId().equals(avd.getCreatorId())).findFirst();
                User user = new User();
                if (userOptional.isPresent()) {
                    user = userOptional.get();
                }
                String avatar = null;
                if (user.getAvatar() != null) {
                    avatar = serviceHelper.getFtpAccountConfig().getImageUrlBasePath() + user.getAvatar();
                }

                AvdUserPojo userPojo = new AvdUserPojo();
                userPojo.setCreatorId(user.getId());
                userPojo.setCreatorName(user.getUsername());
                userPojo.setCreatorAvatar(avatar);
                avd.setUser(userPojo);
            }
        }

        AvdListResultPojo avdListResultPojo = new AvdListResultPojo();
        avdListResultPojo.setList(avdList);
        avdListResultPojo.setTotal(avdCount);

        ServiceResult<AvdListResultPojo> serviceResult = new ServiceResult<AvdListResultPojo>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(avdListResultPojo);

        return serviceResult;
    }

    /**
     * 根据id查询避坑指南详情
     *
     * @param avdId
     * @return
     */
    @Override
    public ServiceResult<AvdDetailPojo> findAvdDetailById(Long avdId) {
        Avd avd = serviceHelper.getAvdMapper().selectById(avdId);

        // 给封面地址添加url前缀
        if (avd.getCover() != null && !avd.getCover().isEmpty()) {
            avd.setCover(serviceHelper.getFtpAccountConfig().getImageUrlBasePath() + avd.getCover());
        }

        // 给视频地址添加url前缀
        if (avd.getVideo() != null && !avd.getVideo().isEmpty()) {
            avd.setVideo(serviceHelper.getFtpAccountConfig().getVideoUrlBasePath() + avd.getVideo());
        }

        // 填写AvdDetailPojo
        AvdDetailPojo avdDetailPojo = new AvdDetailPojo();
        avdDetailPojo.setAvdId(avd.getId());
        avdDetailPojo.setModule(avd.getModule());
        avdDetailPojo.setTitle(avd.getTitle());
        avdDetailPojo.setCover(avd.getCover());
        avdDetailPojo.setDescription(avd.getDescription());
        avdDetailPojo.setParagraph(avd.getParagraph());
        avdDetailPojo.setViewNum(avd.getViewNum());
        avdDetailPojo.setCreate_date(avd.getCreateDate());
        avdDetailPojo.setVideo(avd.getVideo());

        // 查询并填写作者信息
        User user = serviceHelper.getUserMapper().selectById(avd.getCreatorId());
        if (user == null) {
            user = new User();
        }
        String avatar = null;
        if (user.getAvatar() != null) {
            avatar = serviceHelper.getFtpAccountConfig().getImageUrlBasePath() + user.getAvatar();
        }

        AvdUserPojo userPojo = new AvdUserPojo();
        userPojo.setCreatorId(user.getId());
        userPojo.setCreatorName(user.getUsername());
        userPojo.setCreatorAvatar(avatar);
        avdDetailPojo.setUser(userPojo);

        ServiceResult<AvdDetailPojo> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(avdDetailPojo);

        return serviceResult;
    }

    /**
     * 添加用户浏览记录
     *
     * @return
     */
    @Override
    public ServiceResult userViewLogAdd(UserViewLog userViewLog) {
        serviceHelper.getUserViewLogMapper().insert(userViewLog);

        // 增加浏览记录的数字
        if (userViewLog.getType() == UserViewLogType.AvdView) {
            serviceHelper.getAvdMapper().addOneViewNum(userViewLog.getBusinessId());
        } else if (userViewLog.getType() == UserViewLogType.AvdLike) {
            serviceHelper.getAvdMapper().addOneLikeNum(userViewLog.getBusinessId());
        } else if (userViewLog.getType() == UserViewLogType.AvdCollect) {
            serviceHelper.getAvdMapper().addOneCollectNum(userViewLog.getBusinessId());
        } else if (userViewLog.getType() == UserViewLogType.VidView) {
            serviceHelper.getVideoMapper().addViewNum(userViewLog.getBusinessId());
        }

        ServiceResult serviceResult = new ServiceResult();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);

        return serviceResult;
    }

    /**
     * 减少用户点赞等记录
     *
     * @param userViewLogReduceVo
     * @return
     */
    @Override
    public ServiceResult userViewLogReduce(UserViewLogReduceVo userViewLogReduceVo) {
        UpdateWrapper<UserViewLog> uwUserViewLog = new UpdateWrapper<>();
        uwUserViewLog.lambda()
                .eq(UserViewLog::getBusinessId, userViewLogReduceVo.getBusinessId())
                .eq(UserViewLog::getType, userViewLogReduceVo.getType())
                .eq(UserViewLog::getStatus, GeneralStatus.NORMAL);
        uwUserViewLog.lambda().set(UserViewLog::getStatus, GeneralStatus.DELETE);
        serviceHelper.getUserViewLogMapper().update(null, uwUserViewLog);

        // 增加浏览记录的数字
        if (userViewLogReduceVo.getType() == UserViewLogType.AvdLike) {
            serviceHelper.getAvdMapper().reduceOneLikeNum(userViewLogReduceVo.getBusinessId());
        } else if (userViewLogReduceVo.getType() == UserViewLogType.AvdCollect) {
            serviceHelper.getAvdMapper().reduceOneCollectNum(userViewLogReduceVo.getBusinessId());
        }

        ServiceResult serviceResult = new ServiceResult();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);

        return serviceResult;
    }

    /**
     * 避坑指南审批通过
     *
     * @return
     */
    @Override
    public ServiceResult approveOk(AvdApproveVo avdApproveVo) {
        UpdateWrapper<Avd> uwAvd = new UpdateWrapper<>();
        uwAvd.lambda().eq(Avd::getId, avdApproveVo.getAvdId());
        uwAvd.lambda().set(Avd::getApproveStatus, AvdApproveStatus.Approved);
        serviceHelper.getAvdMapper().update(null, uwAvd);

        ServiceResult serviceResult = new ServiceResult();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);

        return serviceResult;
    }


    /**
     * @Description: 类别下拉菜单
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2614:10
     */
    @Override
    public ServiceResult<List<AvdType>> getAvdTypes() {
        List<AvdType> avdTypes = serviceHelper.getAvdTypeMapper().selectList(new QueryWrapper<>());
        return ServiceResult.ok(avdTypes, "ok");
    }


    /**
     * @Description: 获取标签下拉菜单
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2614:12
     */
    @Override
    public ServiceResult<List<AvdTag>> getAvdTags() {
        return ServiceResult.ok(serviceHelper.getAvdTagMapper().selectList(new QueryWrapper<>()), "ok");
    }

    /**
     * @Description: 分页查看避坑指南列表
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2614:36
     */
    @Override
    public ServiceResult<PageInfo<AvdListPojo>> page(Integer pageNo, Integer pageSize, Long typeId, Integer module, String userName, String createDate) {
        PageHelper.startPage(pageNo, pageSize);

        List<AvdListPojo> page = this.baseMapper.page(typeId, module, userName, createDate);

        return ServiceResult.ok(new PageInfo<>(page), "ok");
    }


    /**
     * @Description: 将avd表中数据放入到redis中
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/117:23
     */
    @Override
    public void selectAvdAndSetRedisTask() {
        String synSlnRedisKey = RedisKeyPrefixEnum.TABLE.getCode() +":"+ RedisKeyPrefixEnum.TABLE_AVD.getCode();
        serviceHelper.getRedisManager().del(synSlnRedisKey);

        LambdaQueryWrapper<Avd> avdQueryWrapper = new LambdaQueryWrapper<>();
        // 所有数据
        avdQueryWrapper.eq(Avd::getModule,1);
        avdQueryWrapper.eq(Avd::getStatus,2);
        avdQueryWrapper.orderByDesc(Avd::getCreateDate);
        List<Avd> avds = serviceHelper.getAvdMapper().selectList(avdQueryWrapper);
        // 中间表
        List<AvdRel> avdRels = serviceHelper.getAvdRelMapper().selectList(new QueryWrapper<>());
        // 分类
        LambdaQueryWrapper<AvdType> avdTypeWrapper = new LambdaQueryWrapper<>();
        avdTypeWrapper.eq(AvdType::getStatus,2);
        List<AvdType> avdTypes = serviceHelper.getAvdTypeMapper().selectList(avdTypeWrapper);
        // 品牌
        LambdaQueryWrapper<AvdBrand> avdBrandWrapper = new LambdaQueryWrapper<>();
        avdBrandWrapper.eq(AvdBrand::getStatus,2);
        List<AvdBrand> avdBrands = serviceHelper.getAvdBrandMapper().selectList(avdBrandWrapper);
        // 标签
        LambdaQueryWrapper<AvdTag> avdTagWrapper = new LambdaQueryWrapper<>();
        avdTagWrapper.eq(AvdTag::getStatus,2);
        List<AvdTag> avdTags = serviceHelper.getAvdTagMapper().selectList(avdTagWrapper);

        /**
         *  并行流  list -> map
         */
        Map<Long, AvdType> avdTypeCollect = avdTypes.parallelStream().collect(Collectors.toMap(AvdType::getId, e -> e, (k, k1) -> k));
        Map<Long, AvdTag> avdTagCollect = avdTags.parallelStream().collect(Collectors.toMap(AvdTag::getId, e -> e, (k1, k2) -> k1));
        Map<Long, AvdBrand> avdBrandCollect = avdBrands.parallelStream().collect(Collectors.toMap(AvdBrand::getId, e -> e, (k1, k2) -> k1));
        // 进行分组 list -> Map<Long, List<>>
        Map<Long, List<AvdRel>> redCollect = avdRels.parallelStream().collect(Collectors.groupingBy(AvdRel::getAvdId));
        // 构建关联中间表与品牌 分类 标签关系map
        Map<Long, List<AvdType>> avdTypeMap = new HashMap<>();
        Map<Long, List<AvdTag>> avdTagMap = new HashMap<>();
        Map<Long, List<AvdBrand>> avdBrandMap = new HashMap<>();
        // 开始封装数据
        redCollect.forEach((k, v) -> {
            List<AvdType> avdTypeList = new LinkedList<>();
            List<AvdTag> avdTagList = new LinkedList<>();
            List<AvdBrand> avdBrandList = new LinkedList<>();
            v.forEach(avdRel -> {
                // 分类
                if (1 == (avdRel.getModule())) {
                    avdTypeList.add(avdTypeCollect.get(avdRel.getAvdRelId()));
                    avdTypeMap.put(k, avdTypeList);
                }
                // 标签
                if (2 == (avdRel.getModule())) {
                    avdTagList.add(avdTagCollect.get(avdRel.getAvdRelId()));
                    avdTagMap.put(k, avdTagList);
                }
                // 品牌
                if (3 == (avdRel.getModule())) {
                    avdBrandList.add(avdBrandCollect.get(avdRel.getAvdRelId()));
                    avdBrandMap.put(k, avdBrandList);
                }
            });
        });

        // 转换
        List<AvdListPojo> avdListPojos = BeanPropertiesUtil.convertList(avds, AvdListPojo.class);
        // 封装成一对多的数据
        avdListPojos.parallelStream().peek(e->{
            e.setAvdTypes(avdTypeMap.get(e.getId()));
            e.setAvdTags(avdTagMap.get(e.getId()));
            e.setAvdBrands(avdBrandMap.get(e.getId()));
        }).collect(Collectors.toList());

        serviceHelper.getRedisManager().lPushAll(synSlnRedisKey,avdListPojos);
    }


    /**
     * @Description: 将必坑指南的数据同步至redis中
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/314:55
     */
    @Override
    public void synAvdData2Redis() {
        String prefix = RedisKeyPrefixEnum.TABLE.getCode();
        LambdaQueryWrapper<Avd> avdQueryWrapper = new LambdaQueryWrapper<>();
        // 所有数据,包括文章和视频
        avdQueryWrapper.eq(Avd::getStatus,GeneralStatus.NORMAL);
        avdQueryWrapper.eq(Avd::getApproveStatus,AvdApproveStatus.Approved);
        avdQueryWrapper.orderByDesc(Avd::getCreateDate);
        List<Avd> avds = serviceHelper.getAvdMapper().selectList(avdQueryWrapper);
       /* if(!CollectionUtils.isEmpty(avds)){
            for (Avd avd : avds) {
                // 给封面地址添加url前缀
                if (avd.getCover() != null && !avd.getCover().isEmpty()) {
                    avd.setCover(serviceHelper.getFtpAccountConfig().getImageUrlBasePath() + avd.getCover());
                }
                // 给视频地址添加url前缀
                if (avd.getVideo() != null && !avd.getVideo().isEmpty()) {
                    avd.setVideo(serviceHelper.getFtpAccountConfig().getVideoUrlBasePath() + avd.getVideo());
                }
            }
        }*/
        serviceHelper.getRedisManager().del(prefix + ":" + RedisKeyPrefixEnum.TABLE_AVD.getCode());
        serviceHelper.getRedisManager().lPushAll(prefix + ":" + RedisKeyPrefixEnum.TABLE_AVD.getCode(),avds);

        // 中间表
        List<AvdRel> avdRels = serviceHelper.getAvdRelMapper().selectList(new QueryWrapper<>());
        serviceHelper.getRedisManager().del(prefix + ":" + RedisKeyPrefixEnum.TABLE_AVD_REL.getCode());
        serviceHelper.getRedisManager().lPushAll(prefix + ":" + RedisKeyPrefixEnum.TABLE_AVD_REL.getCode(),avdRels);

        // 分类
        LambdaQueryWrapper<AvdType> avdTypeWrapper = new LambdaQueryWrapper<>();
        avdTypeWrapper.eq(AvdType::getStatus,2);
        List<AvdType> avdTypes = serviceHelper.getAvdTypeMapper().selectList(avdTypeWrapper);
        serviceHelper.getRedisManager().del(prefix + ":" + RedisKeyPrefixEnum.TABLE_AVD_TYPE.getCode());
        serviceHelper.getRedisManager().lPushAll(prefix + ":" + RedisKeyPrefixEnum.TABLE_AVD_TYPE.getCode(),avdTypes);

        // 品牌
        LambdaQueryWrapper<AvdBrand> avdBrandWrapper = new LambdaQueryWrapper<>();
        avdBrandWrapper.eq(AvdBrand::getStatus,2);
        List<AvdBrand> avdBrands = serviceHelper.getAvdBrandMapper().selectList(avdBrandWrapper);
        serviceHelper.getRedisManager().del(prefix + ":" + RedisKeyPrefixEnum.TABLE_AVD_BRAND.getCode());
        serviceHelper.getRedisManager().lPushAll(prefix + ":" + RedisKeyPrefixEnum.TABLE_AVD_BRAND.getCode(),avdBrands);

        // 标签
        LambdaQueryWrapper<AvdTag> avdTagWrapper = new LambdaQueryWrapper<>();
        avdTagWrapper.eq(AvdTag::getStatus,2);
        List<AvdTag> avdTags = serviceHelper.getAvdTagMapper().selectList(avdTagWrapper);
        serviceHelper.getRedisManager().del(prefix + ":" + RedisKeyPrefixEnum.TABLE_AVD_TAG.getCode());
        serviceHelper.getRedisManager().lPushAll(prefix + ":" + RedisKeyPrefixEnum.TABLE_AVD_TAG.getCode(),avdTags);
    }

}