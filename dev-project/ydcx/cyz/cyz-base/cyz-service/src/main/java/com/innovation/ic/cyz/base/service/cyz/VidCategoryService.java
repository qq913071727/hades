package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.VidCategory;

import java.util.List;

/**
 * 视频分类接口
 */
public interface VidCategoryService {

    List<VidCategory> findByPage(int pageNum, int pageSize);
}
