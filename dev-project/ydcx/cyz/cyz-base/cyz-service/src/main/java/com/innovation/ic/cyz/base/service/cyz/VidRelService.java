package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.VidRel;

import java.util.List;

public interface VidRelService {
    List<VidRel> findByPage(int pageNum, int pageSize);
}
