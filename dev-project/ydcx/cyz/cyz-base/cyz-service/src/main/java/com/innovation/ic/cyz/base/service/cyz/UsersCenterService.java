package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.AvdType;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.users_center.UserCenterListPojo;
import com.innovation.ic.cyz.base.vo.cyz.UserCenterVo;

import java.util.List;

/**
 * 用户中心接口数据
 */
public interface UsersCenterService {

    /**
     * 用户中心列表信息
     *
     * @param userCenterVo
     * @return
     */
    ServiceResult<UserCenterListPojo> allUsersCenter(UserCenterVo userCenterVo);

    /**
     * 用户中心中下拉框(所属类别)
     *
     * @return
     */
    ServiceResult<List<AvdType>> allType();

    /**
     * 用户中心投稿信息(删除)
     *
     * @param userCenterVo
     * @return
     */
    void deleteContribution(UserCenterVo userCenterVo);

    /**
     * 用户中心投稿信息(审核)
     *
     * @param userCenterVo
     * @return
     */
    void updateApproveStatus(UserCenterVo userCenterVo);
}
