package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrSmtDetailPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrSmtListResultPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.mfrsmt_search_option.MfrSmtOptionLine;
import com.innovation.ic.cyz.base.vo.cyz.MfrSmtListVo;

import java.util.List;

/**
 * smt 接口
 */
public interface MfrSmtService {

    /**
     * smt厂家列表
     *
     * @param param 参数
     * @return
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    ServiceResult<MfrSmtListResultPojo> getList(MfrSmtListVo param) throws NoSuchFieldException, IllegalAccessException;

    /**
     * 根据id查询详情
     *
     * @param mfrId
     * @return
     */
    ServiceResult<MfrSmtDetailPojo> getDetailById(Long mfrId) throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException, InstantiationException;

    ;

    /**
     * 查询列表选项
     *
     * @return
     */
    ServiceResult<List<MfrSmtOptionLine>> getListOptions();
}
