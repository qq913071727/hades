package com.innovation.ic.cyz.base.service.hqu;

import com.innovation.ic.cyz.base.model.cyz.Product;
import com.innovation.ic.cyz.base.model.cyz.ProductProperty;
import com.innovation.ic.cyz.base.model.cyz.ProductUnique;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;

import java.util.List;

/**
 * 与虎趣对接
 */
public interface HquService {

    /**
     * redis获取数据并分页
     *
     * @param tableName
     * @param pageSize
     * @param pageNo
     * @return
     */
    ServiceResult<List<Object>> selectRedisPage(String tableName, int pageSize, int pageNo);

    /**
     * Product表获取数据并分页
     *
     * @param tableName
     * @param pageSize
     * @param pageNo
     * @return
     */
    ServiceResult<List<Product>> selectMysqlProductPage(String tableName, Integer pageSize, Integer pageNo);

    /**
     * ProductProperty表获取数据并分页
     *
     * @param tableName
     * @param pageSize
     * @param pageNo
     * @return
     */
    ServiceResult<List<ProductProperty>> selectMysqlProductPropertyPage(String tableName, Integer pageSize, Integer pageNo);

    /**
     * ProductUnique表获取数据并分页
     *
     * @param tableName
     * @param pageSize
     * @param pageNo
     * @return
     */
    ServiceResult<List<ProductUnique>> selectMysqlProductUniquePage(String tableName, Integer pageSize, Integer pageNo);
}
