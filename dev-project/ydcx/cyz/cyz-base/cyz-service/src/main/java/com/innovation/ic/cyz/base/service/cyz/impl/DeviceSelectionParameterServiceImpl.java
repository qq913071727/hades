package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cyz.base.mapper.cyz.DeviceSelectionParameterMapper;
import com.innovation.ic.cyz.base.model.cyz.DeviceSelectionParameter;
import com.innovation.ic.cyz.base.pojo.enums.DeviceSelectionParameterTypeEnum;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_parameter.DeviceSelectionParameterListRespPojo;
import com.innovation.ic.cyz.base.service.cyz.DeviceSelectionParameterService;
import com.innovation.ic.cyz.base.vo.cyz.deviceselection.DeviceSelectionParameterVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DeviceSelectionParameterServiceImpl extends ServiceImpl<DeviceSelectionParameterMapper, DeviceSelectionParameter> implements DeviceSelectionParameterService {
    @Resource
    private DeviceSelectionParameterMapper deviceSelectionParameterMapper;

    /**
     * 通过器件选型类型id获取器件选型类型包含参数id集合
     * @param categoryId 器件选型类型id
     * @param type 类型 1、快速筛选 2、参数筛选
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<String>> getExternallyUniqueIdListByCategoryId(String categoryId, Integer type) {
        List<String> result = deviceSelectionParameterMapper.getExternallyUniqueIdListByCategoryId(categoryId, type);

        ServiceResult<List<String>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(result);
        return serviceResult;
    }

    @Override
    public List<DeviceSelectionParameter> findByPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        QueryWrapper<DeviceSelectionParameter> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_date");
        return deviceSelectionParameterMapper.selectList(queryWrapper);
    }

    @Override
    public Integer selectCount() {
        return deviceSelectionParameterMapper.selectCount(new QueryWrapper<>());
    }

    /**
     * 通过器件选型类型id查询器件选型类型包含参数
     * @param typeId 器件选型类型id
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<DeviceSelectionParameterListRespPojo> findDeviceSelectionParameterByTypeId(String typeId) {
        DeviceSelectionParameterListRespPojo deviceSelectionParameterListRespPojo = new DeviceSelectionParameterListRespPojo();

        deviceSelectionParameterListRespPojo.setFastScreen(deviceSelectionParameterMapper.selectFastScreenByTypeId(typeId, DeviceSelectionParameterTypeEnum.FAST_SCREEN.getCode()));
        deviceSelectionParameterListRespPojo.setParamScreen(deviceSelectionParameterMapper.selectFastScreenByTypeId(typeId, DeviceSelectionParameterTypeEnum.PARAM_SCREEN.getCode()));

        ServiceResult<DeviceSelectionParameterListRespPojo> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(deviceSelectionParameterListRespPojo);
        return serviceResult;
    }

    @Override
    public void insertBatch(List<DeviceSelectionParameterVo> deviceSelectionParameters) {
        List<DeviceSelectionParameter> deviceSelectionParameterList = new ArrayList<>();
        for (DeviceSelectionParameterVo deviceSelectionParameterVo : deviceSelectionParameters) {
            DeviceSelectionParameter deviceSelectionParameter = new DeviceSelectionParameter();
            BeanUtils.copyProperties(deviceSelectionParameterVo, deviceSelectionParameter);
            deviceSelectionParameterList.add(deviceSelectionParameter);
        }
        this.saveBatch(deviceSelectionParameterList);
    }

    @Override
    public void insert(DeviceSelectionParameter deviceSelectionParameter) {
        this.save(deviceSelectionParameter);
    }

    @Override
    public void deleteByTypeId(String typeId) {
        if (!StringUtils.validateParameter(typeId)) {
            return;
        }
        Map<String, Object> param = new HashMap<>();
        param.put("device_selection_type_id", typeId);
        this.baseMapper.deleteByMap(param);
    }

    @Override
    public List<DeviceSelectionParameter> selectByMap(Map<String, Object> param) {
        return this.baseMapper.selectByMap(param);
    }
}