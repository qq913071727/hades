package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.Tenant;
import com.innovation.ic.cyz.base.model.cyz.TenantImg;
import com.innovation.ic.cyz.base.pojo.variable.tenant_import.TenantImport;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;

import java.util.List;

/**
 * 获取原厂数据服务类接口
 */
public interface ImportTenantService {
    /**
     * 从比一比接口中获取原厂数据
     * @return
     */
    ServiceResult<TenantImport> getTenantsFromB1bApi();

    /**
     * 将TenantImport转为Tenant列表
     * @return
     */
    ServiceResult<List<Tenant>> ConvertTenantImportToTenant(TenantImport tenantImport);

    /**
     * 将TenantImport转为TenantImg列表
     * @param tenantImport
     * @return
     */
    ServiceResult<List<TenantImg>> ConvertTenantImportToTenantImg(TenantImport tenantImport);

    /**
     * 批量插入Tenant对象
     * @return
     */
    ServiceResult<Boolean> saveTenantList(List<Tenant> tenantList, List<TenantImg> tenantImgList);
}
