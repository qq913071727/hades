package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.innovation.ic.cyz.base.mapper.cyz.VidTagMapper;
import com.innovation.ic.cyz.base.model.cyz.VidTag;
import com.innovation.ic.cyz.base.service.cyz.VidTagService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class VidTagServiceImpl extends ServiceImpl<VidTagMapper, VidTag> implements VidTagService {
    @Override
    public List<VidTag> findByPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        QueryWrapper<VidTag> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("id");
        return this.baseMapper.selectList(queryWrapper);
    }
}
