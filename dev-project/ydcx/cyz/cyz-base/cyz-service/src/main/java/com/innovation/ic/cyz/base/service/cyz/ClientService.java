package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.Client;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import java.util.List;

/**
 * @desc   Client的服务类接口
 * @author linuo
 * @time   2022年8月11日15:24:30
 */
public interface ClientService {
    /**
     * 查找client表中所有数据
     * @return
     */
    ServiceResult<List<Client>> findAll();
}