package com.innovation.ic.cyz.base.service.pve_standard;

import com.innovation.ic.cyz.base.model.pve_standard.CategoriesProperty;

import java.util.List;

public interface CategoriesPropertyService {


    List<CategoriesProperty> findByPage(int pageNum, int pageSize);

    Integer effectiveData();
}


