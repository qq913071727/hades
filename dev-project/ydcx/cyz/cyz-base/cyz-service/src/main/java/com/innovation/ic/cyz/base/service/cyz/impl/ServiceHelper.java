package com.innovation.ic.cyz.base.service.cyz.impl;

import com.innovation.ic.b1b.framework.manager.MinioManager;
import com.innovation.ic.b1b.framework.manager.RabbitMqManager;
import com.innovation.ic.b1b.framework.manager.RedisManager;
import com.innovation.ic.b1b.framework.manager.SftpChannelManager;
import com.innovation.ic.cyz.base.handler.ModelHandler;
import com.innovation.ic.cyz.base.mapper.cyz.*;
import com.innovation.ic.cyz.base.model.cyz.User;
import com.innovation.ic.cyz.base.model.cyz.UserLoginLog;
import com.innovation.ic.cyz.base.pojo.UserInfo;
import com.innovation.ic.cyz.base.value.config.*;
import lombok.Getter;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.Date;

/**
 * Service的帮助类
 */
@Getter
@Component
public class ServiceHelper {
    /********************************* Manager *****************************/
    @Resource
    private MinioManager minioManager;

    @Resource
    private RedisManager redisManager;

    @Resource
    private RabbitMqManager rabbitMqManager;

    @Resource
    private SftpChannelManager sftpChannelManager;

    /********************************* Handler *****************************/
    @Resource
    private ModelHandler modelHandler;

    /********************************* Config *****************************/
    @Resource
    private B1bApiConfig b1bApiConfig;

    @Resource
    private FtpAccountConfig ftpAccountConfig;

    @Resource
    private MinioPropConfig minioPropConfig;

    @Resource
    private SlnApiConfig slnApiConfig;

    @Resource
    private HostParamsConfig hostParamsConfig;

    /********************************* Mapper *****************************/
    @Resource
    private AvdMapper avdMapper;

    @Resource
    private AvdTypeMapper avdTypeMapper;

    @Resource
    private AvdTagMapper avdTagMapper;

    @Resource
    private AvdBrandMapper avdBrandMapper;

    @Resource
    private AvdRelMapper avdRelMapper;

    @Resource
    private CarouselMapper carouselMapper;

    @Resource
    private FeedbackMapper feedbackMapper;

    @Resource
    private FeedbackTypeMapper feedbackTypeMapper;

    @Resource
    private FeedbackImgMapper feedbackImgMapper;

    @Resource
    private InquiryMapper inquiryMapper;

    @Resource
    private MfrParamMapper mfrParamMapper;

    @Resource
    private MfrMapper mfrMapper;

    @Resource
    private MfrContentMapper mfrContentMapper;

    @Resource
    private MfrParamDictionary mfrParamDictionary;

    @Resource
    private RegionMapper regionMapper;

    @Resource
    private RepairOrderMapper repairOrderMapper;

    @Resource
    private RepairOrderRecordMapper repairOrderRecordMapper;

    @Resource
    private RepairOrderFileMapper repairOrderFileMapper;

    @Resource
    private SlnMapper slnMapper;

    @Resource
    private SlnUrlMapper slnUrlMapper;

    @Resource
    private SlnLikeTagMapper slnLikeTagMapper;

    @Resource
    private SlnSyncLogMapper slnSyncLogMapper;

    @Resource
    private SlnIdeMapper slnIdeMapper;

    @Resource
    private SlnSyncFileLogMapper slnSyncFileLogMapper;

    @Resource
    private TenantMapper tenantMapper;

    @Resource
    private TenantImgMapper tenantImgMapper;

    @Autowired
    private ContentTypeMapper contentTypeMapper;

    @Autowired
    private UserMapper userMapper;

    @Resource
    private UserViewLogMapper userViewLogMapper;

    @Resource
    private VideoMapper videoMapper;

    @Resource
    private VidCategoryMapper vidCategoryMapper;

    @Resource
    private VidBrandMapper vidBrandMapper;

    @Resource
    private VidTypeMapper typeMapper;

    @Resource
    private VidTagMapper tagMapper;

    @Resource
    private VidRelMapper relMapper;

    @Resource
    private UserLoginLogMapper userLoginLogMapper;
    /**
     * 获取用户信息
     *
     * @return
     */
    protected UserInfo getUserInfo(String userId) {
        if (userId == null) {
            return null;
        }

        // 查询用户信息
        User user = userMapper.selectById(userId);

        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(user, userInfo);

        // 处理工作年限字段
        if(user.getWorkYear() != null){
            userInfo.setWorkYear(user.getWorkYear() + "年");
        }

        // 处理头像字段
        String avatar = "";
        if (user.getAvatar() != null) {
            avatar = ftpAccountConfig.getImageUrlBasePath() + user.getAvatar();
        }
        userInfo.setAvatar(avatar);

        // 处理积分字段
        Integer point = 0;
        if (user.getPoint() != null) {
            point = user.getPoint();
        }
        userInfo.setPoint(point);

        return userInfo;
    }

    /**
     * 添加日志数据
     * @param ipAdrress
     * @param user
     * @return
     */
    public void addUserLoginLog(String ipAdrress, User user) {
        UserLoginLog userLoginLog = new UserLoginLog();
        userLoginLog.setUsername(user.getUsername());
        userLoginLog.setCreateTime(new Date());
        userLoginLog.setRemoteAddress(ipAdrress);
        userLoginLogMapper.insert(userLoginLog);
    }
}