package com.innovation.ic.cyz.base.service.pve_standard.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.pve_standard.BrandsMapper;
import com.innovation.ic.cyz.base.model.cyz.Brand;
import com.innovation.ic.cyz.base.model.pve_standard.Brands;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.service.pve_standard.BrandsService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @desc   Brands表接口实现类
 * @author linuo
 * @time   2022年10月28日17:12:46
 */
@Service
public class BrandsServiceImpl extends ServiceImpl<BrandsMapper, Brands> implements BrandsService {
    @Resource
    private BrandsMapper brandsMapper;

    /**
     * 查询Brands状态是正常的数据
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<Brand>> selectNormalStatusData() {
        List<Brand> list = brandsMapper.selectNormalStatusData();

        ServiceResult<List<Brand>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(list);
        return serviceResult;
    }
}