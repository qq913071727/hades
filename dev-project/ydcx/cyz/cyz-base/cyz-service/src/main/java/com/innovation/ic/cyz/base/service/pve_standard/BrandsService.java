package com.innovation.ic.cyz.base.service.pve_standard;

import com.innovation.ic.cyz.base.model.cyz.Brand;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import java.util.List;

/**
 * @desc   Brands表接口
 * @author linuo
 * @time   2022年10月28日10:20:27
 */
public interface BrandsService {
    /**
     * 查询Brands状态是正常的数据
     * @return 返回查询结果
     */
    ServiceResult<List<Brand>> selectNormalStatusData();
}