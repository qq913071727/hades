package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.innovation.ic.cyz.base.mapper.cyz.VidTypeMapper;
import com.innovation.ic.cyz.base.model.cyz.VidType;
import com.innovation.ic.cyz.base.service.cyz.VidTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class VidTypeServiceImpl  extends ServiceImpl<VidTypeMapper, VidType> implements VidTypeService {
    @Override
    public List<VidType> findByPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        QueryWrapper<VidType> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("id");
        return this.baseMapper.selectList(queryWrapper);
    }
}
