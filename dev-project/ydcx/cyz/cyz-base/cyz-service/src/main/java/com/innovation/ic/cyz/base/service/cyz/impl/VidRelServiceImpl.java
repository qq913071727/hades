package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.innovation.ic.cyz.base.mapper.cyz.VidRelMapper;
import com.innovation.ic.cyz.base.model.cyz.VidRel;
import com.innovation.ic.cyz.base.service.cyz.VidRelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class VidRelServiceImpl extends ServiceImpl<VidRelMapper, VidRel> implements VidRelService {
    @Override
    public List<VidRel> findByPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        QueryWrapper<VidRel> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("id");
        return this.baseMapper.selectList(queryWrapper);
    }
}
