package com.innovation.ic.cyz.base.service.pve_standard.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.pve_standard.ProductsMapper;
import com.innovation.ic.cyz.base.model.cyz.Product;
import com.innovation.ic.cyz.base.model.pve_standard.Products;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.service.pve_standard.ProductsService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @desc   Products表接口实现类
 * @author linuo
 * @time   2022年10月28日17:12:08
 */
@Service
public class ProductsServiceImpl extends ServiceImpl<ProductsMapper, Products> implements ProductsService {
    @Resource
    private ProductsMapper productsMapper;

    /**
     * 查询标准库Products状态是正常的数据数量
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<Long> getNormalStatusDataCount() {
        Long result = productsMapper.getNormalStatusDataCount();

        ServiceResult<Long> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 分页查询Products表中状态为正常的数据
     * @param start 分页开始
     * @param end 分页结束
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<Product>> getNormalStatusDataByPage(long start, long end) {
        List<Product> result = productsMapper.getNormalStatusDataByPage(start, end);

        ServiceResult<List<Product>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(result);
        return serviceResult;
    }
}