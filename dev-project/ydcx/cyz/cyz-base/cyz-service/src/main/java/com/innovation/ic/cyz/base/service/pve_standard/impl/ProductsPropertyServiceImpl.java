package com.innovation.ic.cyz.base.service.pve_standard.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.pve_standard.ProductsPropertyMapper;
import com.innovation.ic.cyz.base.model.cyz.Product;
import com.innovation.ic.cyz.base.model.cyz.ProductProperty;
import com.innovation.ic.cyz.base.model.cyz.ProductUnique;
import com.innovation.ic.cyz.base.model.pve_standard.ProductsProperty;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.service.pve_standard.ProductsPropertyService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @desc   Brands表接口实现类
 * @author linuo
 * @time   2022年10月28日17:12:46
 */
@Service
public class ProductsPropertyServiceImpl extends ServiceImpl<ProductsPropertyMapper, ProductsProperty> implements ProductsPropertyService {
    @Resource
    private ProductsPropertyMapper productsPropertyMapper;

    /**
     * 查询标准库ProductsUnique表状态是正常的数据数量
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<Long> getEffectiveDataCount() {
        Long result = productsPropertyMapper.getEffectiveDataCount();

        ServiceResult<Long> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 分页查询ProductsProperty表中有效数据
     * @param start 分页开始
     * @param end 分页结束
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<ProductProperty>> getEffectiveDataByPage(long start, long end) {
        List<ProductProperty> result = productsPropertyMapper.getEffectiveDataByPage(start, end);

        ServiceResult<List<ProductProperty>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(result);
        return serviceResult;
    }
}