package com.innovation.ic.cyz.base.service.hqu.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.innovation.ic.cyz.base.mapper.cyz.ProductMapper;
import com.innovation.ic.cyz.base.mapper.cyz.ProductPropertyMapper;
import com.innovation.ic.cyz.base.mapper.cyz.ProductUniqueMapper;
import com.innovation.ic.cyz.base.model.cyz.Product;
import com.innovation.ic.cyz.base.model.cyz.ProductProperty;
import com.innovation.ic.cyz.base.model.cyz.ProductUnique;
import com.innovation.ic.cyz.base.pojo.enums.RedisKeyPrefixEnum;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.service.cyz.impl.ServiceHelper;
import com.innovation.ic.cyz.base.service.hqu.HquService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class HquServiceImpl implements HquService {

    @Resource
    private ServiceHelper serviceHelper;

    @Resource
    private ProductMapper productMapper;

    @Resource
    private ProductPropertyMapper productPropertyMapper;

    @Resource
    private ProductUniqueMapper productUniqueMapper;
    /**
     * Product表获取数据并分页
     *
     * @param tableName
     * @param pageSize
     * @param pageNo
     * @return
     */
    public ServiceResult<List<Product>> selectMysqlProductPage(String tableName, Integer pageSize, Integer pageNo) {
        Page<Product> page = new Page<Product>(pageNo, pageSize);
        Page<Product> productPage = productMapper.selectPage(page, null);
        List<Product> records = productPage.getRecords();
        ServiceResult<List<Product>> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(records);
        return serviceResult;
    }

    /**
     * ProductProperty表获取数据并分页
     *
     * @param tableName
     * @param pageSize
     * @param pageNo
     * @return
     */
    public ServiceResult<List<ProductProperty>> selectMysqlProductPropertyPage(String tableName, Integer pageSize, Integer pageNo) {
        Page<ProductProperty> page = new Page<ProductProperty>(pageNo, pageSize);
        Page<ProductProperty> productPropertyPage = productPropertyMapper.selectPage(page, null);
        List<ProductProperty> records = productPropertyPage.getRecords();
        ServiceResult<List<ProductProperty>> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(records);
        return serviceResult;
    }

    /**
     * ProductUnique表获取数据并分页
     *
     * @param tableName
     * @param pageSize
     * @param pageNo
     * @return
     */
    public ServiceResult<List<ProductUnique>> selectMysqlProductUniquePage(String tableName, Integer pageSize, Integer pageNo){
        Page<ProductUnique> page = new Page<ProductUnique>(pageNo, pageSize);
        Page<ProductUnique> productUniquePage = productUniqueMapper.selectPage(page, null);
        List<ProductUnique> records = productUniquePage.getRecords();
        ServiceResult<List<ProductUnique>> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(records);
        return serviceResult;
    }

    /**
     * redis获取数据并分页
     *
     * @param tableName
     * @param pageSize
     * @param pageNo
     * @return
     */
    @Override
    public ServiceResult<List<Object>> selectRedisPage(String tableName, int pageSize, int pageNo) {
        ServiceResult<List<Object>> serviceResult = new ServiceResult<>();
        Integer start = (pageNo - 1) * pageSize;
        Integer size = start + pageSize - 1;
        List<Object> selectDataList = new ArrayList<>();

        List<String> range = serviceHelper.getRedisManager().lRange(RedisKeyPrefixEnum.TABLE.getCode() + ":" + tableName, start, size);

        for (String data : range) {
            Object parse = JSONArray.parse(data);
            selectDataList.add(parse);
        }

        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(selectDataList);
        return serviceResult;
    }
}
