package com.innovation.ic.cyz.base.service.cyz.impl.handler;


import com.innovation.ic.b1b.framework.util.MQUtil;
import com.innovation.ic.cyz.base.pojo.constant.RabbitMQConstant;
import com.innovation.ic.cyz.base.model.cyz.Sln;
import com.innovation.ic.cyz.base.pojo.enums.OperateEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName SlnSendHandler
 * @Description 发送方案消息处理器
 * @Date 2022/9/22
 * @Author myq
 */
@Slf4j
@Component
public class SlnSendHandler {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * @Description: 发送数据的具体实现
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2211:16
     */
    private void requestToSend(Object msgBody) {
       MQUtil.send(RabbitMQConstant.SIN_UP_DIRECT_EXCHANGE, RabbitMQConstant.SIN_UP_ROUTE_KEY, msgBody);
    }


    /**
     * @Description: 向远程发送删除消息
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2213:55
     */
    public void sendRemoteDeleteMsg(String slnId) {
        Map<String, Object> map = new LinkedHashMap<>(3);
        map.put("operate", OperateEnum.DELETE.getKey());
        map.put("id", slnId);
        System.out.println(rabbitTemplate);
        this.requestToSend(map);
    }

    /**
     * @Description: 向远程发送修改消息
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2214:12
     */
    public void sendRemoteUpdateMsg(Sln sln, List<String> urls) {
        /**
         *  解析json串
         *  {"id": 123131,"cash": "19.22","desc": "方案详情信息","title":"方案名称",
         *  "goods_cover":"图片地址aa.png" ,"cate_name":"方案领域","id_cate_name":"方案开发平台",
         *  "delivery_cate_id_name":"方案交付新式","performance_parameter":"方案 性能参数",
         *  "application_scene":"方案应用场景","type_display":"方案类型","subtitle":"方案子名称",
         *  "view_num":"方案查看次数","like_goods":"不知道也传","other_cover":[],"created_at":"","operate":"insert"}
         */
        Map<String, Object> map = new LinkedHashMap<>(3);
        map.put("operate", OperateEnum.UPDATE.getKey());
        map.put("id", sln.getSlnId());
        map.put("cash", sln.getCash());
        map.put("desc", sln.getDescription());
        map.put("title", sln.getTitle());
        map.put("goods_cover", sln.getGoodsCover());
        map.put("cate_name", sln.getCateName());
        map.put("id_cate_name", sln.getCateName());
        map.put("delivery_cate_id_name", sln.getDeliveryCateIdName());
        map.put("performance_parameter", sln.getPerformanceParameter());
        map.put("application_scene", sln.getApplicationScene());
        map.put("type_display", sln.getTypeDisplay());
        map.put("subtitle", sln.getSubtitle());
        map.put("view_num", sln.getViewNum());
        map.put("like_goods", sln.getLikeGoods());
        map.put("other_cover", urls);
        map.put("examine_status",sln.getExamineStatus());
        map.put("examine_error_desc",sln.getExamineErrorDesc());
        this.requestToSend(map);
    }

}

