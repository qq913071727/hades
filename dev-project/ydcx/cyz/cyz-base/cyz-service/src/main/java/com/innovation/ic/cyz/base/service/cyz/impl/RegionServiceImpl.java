package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.model.cyz.Region;
import com.innovation.ic.cyz.base.mapper.cyz.RegionMapper;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.RegionPojo;
import com.innovation.ic.cyz.base.service.cyz.RegionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RegionServiceImpl extends ServiceImpl<RegionMapper, Region> implements RegionService {
    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 获取地区
     * @return
     */
    @Override
    public ServiceResult<List<RegionPojo>> getCities() {
        ServiceResult<List<RegionPojo>> serviceResult = new ServiceResult<>();

        QueryWrapper<Region> queryWrapper = new QueryWrapper<>();
        queryWrapper.ge("level", 3);     //大于等于
        queryWrapper.eq("status", 2);
        queryWrapper.orderByAsc("order_index");
        List<Region> regions = serviceHelper.getRegionMapper().selectList(queryWrapper);

        if (regions.size() <= 0) {
            serviceResult.setMessage(ServiceResult.SELECT_FAIL);
            serviceResult.setSuccess(Boolean.FALSE);
            return serviceResult;
        }

        List<RegionPojo> result = new ArrayList<>();
        for (Region region : getRoot(2)) {
            RegionPojo pojo = new RegionPojo();
            pojo.setId(String.valueOf(region.getId()));
            pojo.setLevel(region.getLevel());
            pojo.setName(region.getName());
            pojo.setOrderIndex(region.getOrderIndex());
            pojo.setParentId(String.valueOf(region.getParentId()));
            result.add(getChild(regions, pojo));
        }

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 获取子节点
     *
     * @param data
     * @param region
     * @return
     */
    private RegionPojo getChild(List<Region> data, RegionPojo region) {
        List<RegionPojo> childRegion = new ArrayList<>();

        for (Region r : data) {
            if (String.valueOf(r.getParentId()).equals(region.getId())) {
                RegionPojo pojo = new RegionPojo();
                pojo.setId(String.valueOf(r.getId()));
                pojo.setLevel(r.getLevel());
                pojo.setName(r.getName());
                pojo.setOrderIndex(r.getOrderIndex());
                pojo.setParentId(String.valueOf(r.getParentId()));
                childRegion.add(getChild(data, pojo));
            }
        }
        region.setChild(childRegion);
        return region;
    }

    /**
     * 获取根节点
     *
     * @return
     */
    private List<Region> getRoot(int level) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        // 大于等于
        queryWrapper.eq("level", level);
        queryWrapper.eq("status", 2);
        queryWrapper.orderByAsc("order_index");
        return serviceHelper.getRegionMapper().selectList(queryWrapper);
    }
}