package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.pve_standard.CategoriesProperty;
import com.innovation.ic.cyz.base.model.pve_standard.Category;

import java.util.List;

public interface DeviceSelectionScheduleService {
    void importDeviceSelectionTypeRedis();

    void importDeviceSelectionSchedule();

    void importDeviceSelectionParameterSchedule();

    void importCategoriesSchedule( List<Category> categoryList);

    void importCategoriesPropertySchedule(List<CategoriesProperty> categoriesPropertyList);

    void importBrandSchedule();

    void importProductSchedule();

    void importProductPropertySchedule();

    void importProductUniqueSchedule();
}
