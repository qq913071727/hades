package com.innovation.ic.cyz.base.service.cyz;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.innovation.ic.cyz.base.model.cyz.DeviceSelectionType;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionTypeRespPojo;
import java.util.List;

public interface DeviceSelectionTypeService {
    ServiceResult<List<DeviceSelectionType>> selectList(QueryWrapper<DeviceSelectionType> queryWrapper);

    void insert(DeviceSelectionType deviceSelectionType);

    DeviceSelectionType findById(String id);

    void updateBottom(String id,String level);

    void deleteById(String id);

    ServiceResult<Integer> checkValid(String id);

    /**
     * 根据id查询同级的器件选型类型
     * @param id 主键id
     * @return 返回查询结果
     */
    ServiceResult<List<DeviceSelectionTypeRespPojo>> findSameLevelTypeListById(String id);

    List<DeviceSelectionType> findByPage(int pageNum,int pageSize);


    Integer selectCount();
}