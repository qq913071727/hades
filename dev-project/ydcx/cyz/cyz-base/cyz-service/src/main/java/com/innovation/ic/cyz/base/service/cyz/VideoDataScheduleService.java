package com.innovation.ic.cyz.base.service.cyz;

public interface VideoDataScheduleService {
    void importVideoRedisSchedule();

    void importVidCategoryRedisSchedule();

    void importVidBrandRedisSchedule();

    void importVidTagRedisSchedule();

    void importVidTypeRedisSchedule();

    void importVidRelRedisSchedule();
}
