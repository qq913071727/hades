package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.b1b.framework.util.IdUtil;
import com.innovation.ic.cyz.base.mapper.cyz.FeedbackImgMapper;
import com.innovation.ic.cyz.base.mapper.cyz.FeedbackTypeMapper;
import com.innovation.ic.cyz.base.mapper.cyz.UserMapper;
import com.innovation.ic.cyz.base.model.cyz.Feedback;
import com.innovation.ic.cyz.base.model.cyz.FeedbackImg;
import com.innovation.ic.cyz.base.model.cyz.FeedbackType;
import com.innovation.ic.cyz.base.mapper.cyz.FeedbackMapper;
import com.innovation.ic.cyz.base.model.cyz.User;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.constant.FeedbackTypeEnum;
import com.innovation.ic.cyz.base.pojo.constant.GeneralStatus;
import com.innovation.ic.cyz.base.pojo.variable.cyz.feedback.FeedbackListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.feedback.FeedbackTypeListPojo;
import com.innovation.ic.cyz.base.service.cyz.FeedbackService;
import com.innovation.ic.cyz.base.handler.EnumHandler;
import com.innovation.ic.cyz.base.vo.cyz.FeedbackVo;
import com.innovation.ic.cyz.base.vo.cyz.feedback.FeedbackSaveVo;
import lombok.var;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * 信息反馈具体实现类
 */
@Service
@Transactional
public class FeedbackServiceImpl extends ServiceImpl<FeedbackMapper, Feedback> implements FeedbackService {

    @Resource
    private ServiceHelper serviceHelper;
    @Resource
    private FeedbackMapper feedbackMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private FeedbackTypeMapper feedbackTypeMapper;
    @Resource
    private FeedbackImgMapper feedbackImgMapper;

    /**
     * 获取信息反馈类型
     */
    @Override
    public ServiceResult<List<FeedbackTypeListPojo>> getFeedbackTypes() throws IllegalAccessException {
        var mapTypes = EnumHandler.getDescriptions(new FeedbackTypeEnum());
        List<FeedbackTypeListPojo> resultList = mapTypes.entrySet().stream().map(item -> {
            FeedbackTypeListPojo feedbackTypeListPojo = new FeedbackTypeListPojo();
            feedbackTypeListPojo.setValue(item.getKey());
            feedbackTypeListPojo.setName(item.getValue());
            return feedbackTypeListPojo;
        }).collect(Collectors.toList());

        ServiceResult<List<FeedbackTypeListPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(resultList);

        return serviceResult;
    }

    /**
     * 信息反馈保存
     */
    @Override
    public ServiceResult save(FeedbackSaveVo feedbackSaveVo) {
        Feedback feedback = serviceHelper.getModelHandler().feedbackSaveVoToFeedback(feedbackSaveVo);
        serviceHelper.getFeedbackMapper().insert(feedback);

        if (feedbackSaveVo.getTypes() != null && !feedbackSaveVo.getTypes().isEmpty()) {
            List<FeedbackType> feedbackTypes = feedbackSaveVo.getTypes().stream().map(item -> {
                FeedbackType feedbackType = new FeedbackType();
                feedbackType.setId(IdUtil.snowflakeNextId());
                feedbackType.setFeedbackId(feedback.getId());
                feedbackType.setType(item);
                feedbackType.setStatus(GeneralStatus.NORMAL);
                return feedbackType;
            }).collect(Collectors.toList());
            serviceHelper.getFeedbackTypeMapper().insertBatchSomeColumn(feedbackTypes);
        }

        if (feedbackSaveVo.getImages() != null && !feedbackSaveVo.getImages().isEmpty()) {
            AtomicInteger indexer = new AtomicInteger();
            indexer.getAndIncrement();
            List<FeedbackImg> feedbackImgs = feedbackSaveVo.getImages().stream().map(item -> {
                FeedbackImg feedbackImg = new FeedbackImg();
                feedbackImg.setId(IdUtil.snowflakeNextId());
                feedbackImg.setFeedbackId(feedback.getId());
                feedbackImg.setUrl(item);
                feedbackImg.setOrderIndex(indexer.getAndIncrement());
                feedbackImg.setStatus(GeneralStatus.NORMAL);
                return feedbackImg;
            }).collect(Collectors.toList());
            serviceHelper.getFeedbackImgMapper().insertBatchSomeColumn(feedbackImgs);
        }

        ServiceResult serviceResult = new ServiceResult();
        serviceResult.setMessage(ServiceResult.INSERT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);

        return serviceResult;
    }

    /**
     * 信息反馈展示数据
     *
     * @param feedbackVo
     * @return
     */
    public ServiceResult<List<FeedbackListPojo>> allFeedback(FeedbackVo feedbackVo) {
        ServiceResult<List<FeedbackListPojo>> listServiceResult = new ServiceResult<>();
        ArrayList<FeedbackListPojo> feedbackListPojoArrayList = new ArrayList<>();
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("id", feedbackVo.getUserId());
        User user = userMapper.selectOne(userQueryWrapper);//获取当前用户信息
        List<Feedback> feedbackList = feedbackMapper.findFeedback(user.getId(), feedbackVo.getCreateDate());
        for (Feedback feedback : feedbackList) {
            FeedbackListPojo feedbackListPojo = new FeedbackListPojo();
            Long id = feedback.getId();//获取信息反馈主键用于查询问题类型
            FeedbackType feedbackType = feedbackTypeMapper.findType(id, feedbackVo.getFeedbackType());//获取到问题类型数据通过id与问题类型
            if (feedbackType == null) {
                continue;
            }
            Integer type = feedbackType.getType();//类型
            QueryWrapper<FeedbackImg> feedbackImgQueryWrapper = new QueryWrapper<>();
            feedbackImgQueryWrapper.eq("feedback_id", id);
            FeedbackImg feedbackImg = feedbackImgMapper.selectOne(feedbackImgQueryWrapper);
            String url = feedbackImg.getUrl();//图片
            feedbackListPojo.setName(user.getUsername());//用户名
            feedbackListPojo.setType(type);//问题类型
            feedbackListPojo.setUrl(url);//图片
            feedbackListPojo.setText(feedback.getText());//问题内容
            feedbackListPojo.setCreateDate(feedback.getCreateDate());//提交时间
            feedbackListPojoArrayList.add(feedbackListPojo);
        }
        listServiceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        listServiceResult.setSuccess(Boolean.TRUE);
        listServiceResult.setResult(feedbackListPojoArrayList);
        return listServiceResult;
    }
}
