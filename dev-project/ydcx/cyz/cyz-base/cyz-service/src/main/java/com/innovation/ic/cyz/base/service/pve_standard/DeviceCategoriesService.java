package com.innovation.ic.cyz.base.service.pve_standard;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.innovation.ic.cyz.base.model.pve_standard.CategoriesProperty;
import com.innovation.ic.cyz.base.model.pve_standard.Category;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionTypePojo;

import java.util.List;
import java.util.Map;

public interface DeviceCategoriesService {


    /**
     * 只查State为200
     *
     * @param param
     * @return
     */
    ServiceResult<List<Category>> findByCategories(Map<String, Object> param);


    /**
     * 只查State为200
     *
     * @return
     */
    ServiceResult<List<Category>> categoriesQueryWrapper(QueryWrapper<Category> queryWrapper);


    ServiceResult<List<DeviceSelectionTypePojo>> findByDeviceSelectionType();



    ServiceResult<List<CategoriesProperty>> findByCategoriesPropertyIds(List<String> ids);

    Category findById(String externalUniqueId);

    List<Category> findByPage(int pageNum, int pageSize);

    Integer effectiveData();
}
