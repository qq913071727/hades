package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.cyz.RepairOrderTypeHeadMapper;
import com.innovation.ic.cyz.base.model.cyz.RepairOrderTypeHead;
import com.innovation.ic.cyz.base.service.cyz.RepairOrderTypeHeadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;

/**
 * @desc   工单分类负责人接口具体实现类
 * @author linuo
 * @time   2022年9月15日13:39:23
 */
@Service
@Transactional
public class RepairOrderTypeHeadServiceImpl extends ServiceImpl<RepairOrderTypeHeadMapper, RepairOrderTypeHead> implements RepairOrderTypeHeadService {
    private static final Logger log = LoggerFactory.getLogger(RepairOrderTypeHeadServiceImpl.class);

    @Resource
    private RepairOrderTypeHeadMapper repairOrderTypeHeadMapper;


}