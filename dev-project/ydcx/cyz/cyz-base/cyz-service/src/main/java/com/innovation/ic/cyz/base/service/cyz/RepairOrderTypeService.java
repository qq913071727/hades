package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type.FirstLevelRepairOrderTypeQueryRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type.RepairOrderTypeRespPojo;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type.RepairOrderTypeListResultPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type_head.RepairOrderTypeFaeListRespPojo;
import com.innovation.ic.cyz.base.vo.cyz.repairOrderType.RepairOrderTypeAddVo;
import com.innovation.ic.cyz.base.vo.cyz.repairOrderType.RepairOrderTypeEditVo;
import net.sf.json.JSONArray;
import java.util.List;

/**
 * @desc   工单分类接口
 * @author linuo
 * @time   2022年9月15日13:38:25
 */
public interface RepairOrderTypeService {
    /**
     * 工单分类添加
     * @param repairOrderTypeAddVo 工单分类添加的vo类
     * @return 返回工单分类添加结果
     */
    ServiceResult<Boolean> add(RepairOrderTypeAddVo repairOrderTypeAddVo);

    /**
     * 工单分类列表查询
     * @return 返回工单分类列表数据
     */
    ServiceResult<RepairOrderTypeListResultPojo> queryList(Integer pageNo, Integer pageSize);

    /**
     * 工单分类删除
     * @param id 工单分类id
     * @return 返回删除结果
     */
    ServiceResult<Boolean> delete(Long id);

    /**
     * 工单分类信息获取
     * @param id 工单分类id
     * @return 返回结果
     */
    ServiceResult<RepairOrderTypeRespPojo> info(Long id);

    /**
     * 工单分类编辑
     * @param repairOrderTypeEditVo 工单分类编辑的vo类
     * @return 返回编辑结果
     */
    ServiceResult<Boolean> edit(RepairOrderTypeEditVo repairOrderTypeEditVo);

    /**
     * 查询工单分类信息
     * @return 返回工单分类信息
     */
    ServiceResult<JSONArray> queryRepairOrderTypeList();

    /**
     * 获取FAE列表
     * @return 返回查询结果
     */
    ServiceResult<List<RepairOrderTypeFaeListRespPojo>> getFaeList();

    /**
     * 获取一级分类
     * @return 返回查询结果
     */
    ServiceResult<List<FirstLevelRepairOrderTypeQueryRespPojo>> getFirstTypeList();
}