package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.innovation.ic.cyz.base.mapper.cyz.DeviceSelectionTypeMapper;
import com.innovation.ic.cyz.base.model.cyz.DeviceSelectionType;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionTypeRespPojo;
import com.innovation.ic.cyz.base.service.cyz.DeviceSelectionTypeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DeviceSelectionTypeServiceImpl extends ServiceImpl<DeviceSelectionTypeMapper, DeviceSelectionType> implements DeviceSelectionTypeService {
    @Resource
    private DeviceSelectionTypeMapper deviceSelectionTypeMapper;

    @Override
    public ServiceResult<List<DeviceSelectionType>> selectList(QueryWrapper<DeviceSelectionType> queryWrapper) {
        ServiceResult<List<DeviceSelectionType>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(this.baseMapper.selectList(queryWrapper));
        return serviceResult;
    }

    @Override
    public void insert(DeviceSelectionType deviceSelectionType) {
        this.baseMapper.insert(deviceSelectionType);
    }

    @Override
    public DeviceSelectionType findById(String id) {
        return this.baseMapper.selectById(id);
    }

    @Override
    public void updateBottom(String id,String levelName) {
        if (StringUtils.isEmpty(id)) {
            return;
        }
        UpdateWrapper<DeviceSelectionType> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        updateWrapper.set("bottom", 1);
        updateWrapper.set("level_name", levelName);
        update(updateWrapper);
    }

    @Override
    public void deleteById(String id) {
        this.baseMapper.deleteById(id);
    }

    @Override
    public ServiceResult<Integer> checkValid(String id) {
        ServiceResult<Integer> serviceResult = new ServiceResult();
        serviceResult.setResult(this.baseMapper.checkValid(id));
        return serviceResult;
    }

    /**
     * 根据id查询同级的器件选型类型
     * @param id 主键id
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<DeviceSelectionTypeRespPojo>> findSameLevelTypeListById(String id) {
        List<DeviceSelectionTypeRespPojo> result = deviceSelectionTypeMapper.findSameLevelTypeListById(id);

        ServiceResult<List<DeviceSelectionTypeRespPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(result);
        return serviceResult;
    }

    @Override
    public List<DeviceSelectionType> findByPage(int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        QueryWrapper<DeviceSelectionType> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_date");
        return deviceSelectionTypeMapper.selectList(queryWrapper);
    }

    @Override
    public Integer selectCount() {
        return deviceSelectionTypeMapper.selectCount(new QueryWrapper<>());
    }
}