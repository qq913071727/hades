package com.innovation.ic.cyz.base.service.pve_standard.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.mapper.pve_standard.ProductsUniqueMapper;
import com.innovation.ic.cyz.base.model.cyz.ProductUnique;
import com.innovation.ic.cyz.base.model.pve_standard.ProductsUnique;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.service.pve_standard.ProductsUniqueService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @desc   ProductsUnique表接口实现类
 * @author linuo
 * @time   2022年10月28日17:12:08
 */
@Service
public class ProductsUniqueServiceImpl extends ServiceImpl<ProductsUniqueMapper, ProductsUnique> implements ProductsUniqueService {
    @Resource
    private ProductsUniqueMapper productsUniqueMapper;

    /**
     * 分页查询ProductsUnique表中状态为正常的数据
     * @param start 分页开始
     * @param end 分页结束
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<List<ProductUnique>> getNormalStatusDataByPage(long start, long end) {
        List<ProductUnique> result = productsUniqueMapper.getNormalStatusDataByPage(start, end);

        ServiceResult<List<ProductUnique>> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 查询标准库ProductsUnique表状态是正常的数据数量
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<Long> getNormalStatusDataCount() {
        Long result = productsUniqueMapper.getNormalStatusDataCount();

        ServiceResult<Long> serviceResult = new ServiceResult<>();
        serviceResult.setResult(result);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return serviceResult;
    }
}