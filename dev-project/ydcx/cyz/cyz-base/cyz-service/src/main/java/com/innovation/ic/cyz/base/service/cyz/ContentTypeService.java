package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.pojo.global.ServiceResult;

public interface ContentTypeService {

    /**
     * 通过文件扩展名获取ContentType
     * @param fileExtension
     * @return
     */
    ServiceResult<String> getContentTypeByFileExtension(String fileExtension);
}
