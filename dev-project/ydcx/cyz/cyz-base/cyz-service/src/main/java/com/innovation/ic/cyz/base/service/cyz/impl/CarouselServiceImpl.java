package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.model.cyz.Carousel;
import com.innovation.ic.cyz.base.mapper.cyz.CarouselMapper;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.constant.GeneralStatus;
import com.innovation.ic.cyz.base.pojo.variable.cyz.carousel.CarouselListPojo;
import com.innovation.ic.cyz.base.service.cyz.CarouselService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 轮播图具体实现类
 */
@Service
@Transactional
public class CarouselServiceImpl extends ServiceImpl<CarouselMapper, Carousel> implements CarouselService {

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 查询轮播图列表
     */
    @Override
    public ServiceResult<List<CarouselListPojo>> findCarouselList() {
        QueryWrapper<Carousel> qwCarousel = new QueryWrapper<>();
        qwCarousel.lambda().eq(Carousel::getStatus, GeneralStatus.NORMAL);
        qwCarousel.orderByAsc("order_index");
        List<Carousel> carousels = serviceHelper.getCarouselMapper().selectList(qwCarousel);

        List<CarouselListPojo> resultList = carousels.stream().map(item -> {
            CarouselListPojo carouselListPojo = new CarouselListPojo();
            carouselListPojo.setText(item.getText());

            String imgUrl = "";
            if (item.getImg() != null) {
                imgUrl = serviceHelper.getFtpAccountConfig().getImageUrlBasePath() + item.getImg();
            }
            carouselListPojo.setImg(imgUrl);

            carouselListPojo.setType(item.getType());
            return carouselListPojo;
        }).collect(Collectors.toList());

        ServiceResult<List<CarouselListPojo>> serviceResult = new ServiceResult<>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(resultList);

        return serviceResult;
    }

    /**
     * 查询轮播图所有数据
     */
    @Override
    public ServiceResult<List<Carousel>> findCarouselListData(){
        ServiceResult<List<Carousel>> serviceResult = new ServiceResult<>();
        List<Carousel> carouselList = serviceHelper.getCarouselMapper().selectList(null);

        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(carouselList);
        return serviceResult;
    }
}