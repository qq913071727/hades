package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.User;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.UserInfo;

/**
 * 用户接口
 */
public interface UserService {

    /**
     * 根据用户名和密码查询用户信息
     */
    ServiceResult<User> findUserByUsernameAndPassword(String username, String password);

    /**
     * 获取当前登录用户信息
     * @param userId
     * @return
     */
    ServiceResult<UserInfo> getCurrentUserInfo(String userId);

    /**
     * 添加日志数据
     * @param ipAdrress
     * @param user
     * @return
     */
    void addUserLoginLog(String ipAdrress, User user);
}
