package com.innovation.ic.cyz.base.service.cyz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.innovation.ic.cyz.base.model.cyz.Avd;
import com.innovation.ic.cyz.base.model.cyz.User;
import com.innovation.ic.cyz.base.model.cyz.UserViewLog;
import com.innovation.ic.cyz.base.mapper.cyz.UserViewLogMapper;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.constant.UserViewLogType;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdListRelItemPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdUserPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.user_view_log.UserViewLogListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.user_view_log.UserViewLogListResultPojo;
import com.innovation.ic.cyz.base.service.cyz.UserViewLogService;
import com.innovation.ic.cyz.base.vo.cyz.UserViewLogListVo;
import lombok.var;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 用户浏览记录的具体实现类
 */
@Service
@Transactional
public class UserViewLogServiceImpl extends ServiceImpl<UserViewLogMapper, UserViewLog> implements UserViewLogService {
    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 用户浏览/点赞/收藏记录列表
     * @param userViewLogListVo
     * @param userId
     * @return
     */
    @Override
    public ServiceResult<UserViewLogListResultPojo> findUserViewLogList(UserViewLogListVo userViewLogListVo, String userId) {
        // 结果
        List<UserViewLogListPojo> resultList = new ArrayList<>();

        List<UserViewLog> userViewLogList = serviceHelper.getUserViewLogMapper().findUserViewLogList(userViewLogListVo.getPage(), userViewLogListVo.getSize(),
                userViewLogListVo.getTypes(),
                userId);
        Long userViewLogCount = serviceHelper.getUserViewLogMapper().findUserViewLogCount(
                userViewLogListVo.getTypes(),
                userId);

        // 查询避坑指南的相关信息
        List<Integer> typeForAvd = Arrays.asList(
                UserViewLogType.AvdView,
                UserViewLogType.AvdLike,
                UserViewLogType.AvdCollect);

        List<Long> avdIds = userViewLogList.stream().filter(t -> typeForAvd.contains(t.getType()))
                .map(t -> t.getBusinessId()).collect(Collectors.toList());
        QueryWrapper<Avd> qwAvd = new QueryWrapper<>();
        qwAvd.lambda().in(Avd::getId, avdIds);
        List<Avd> avdList = serviceHelper.getAvdMapper().selectList(qwAvd);
        List<String> creatorIds = avdList.stream().map(Avd::getCreatorId).collect(Collectors.toList());

        if(creatorIds != null && creatorIds.size() > 0){
            // 查询作者信息
            QueryWrapper<User> qwUser = new QueryWrapper<>();
            qwUser.lambda().in(User::getId, creatorIds);
            List<User> users = serviceHelper.getUserMapper().selectList(qwUser);
            List<AvdListRelItemPojo> allTags = serviceHelper.getAvdTagMapper().findAvdRelTagsByAvdIds(avdIds);

            // 遍历UserViewLogListPojo添加相关信息
            for (var userViewLog : userViewLogList) {
                UserViewLogListPojo result = new UserViewLogListPojo();
                result.setBusinessId(userViewLog.getBusinessId());
                result.setType(userViewLog.getType());

                // 避坑指南
                if (typeForAvd.contains(userViewLog.getType())) {
                    Optional<Avd> avdOptional = avdList.stream().filter(t -> t.getId().equals(userViewLog.getBusinessId())).findFirst();
                    if (avdOptional.isPresent()) {
                        Avd theAvd = avdOptional.get();
                        result.setTitle(theAvd.getTitle());

                        String cover = "";
                        if (theAvd.getCover() != null) {
                            cover = serviceHelper.getFtpAccountConfig().getImageUrlBasePath() + theAvd.getCover();
                        }
                        result.setCover(cover);

                        result.setDescription(theAvd.getDescription());
                        result.setCreatorId(theAvd.getCreatorId());

                        // 作者
                        Optional<User> userOptional = users.stream().filter(t -> t.getId().equals(theAvd.getCreatorId())).findFirst();
                        if (userOptional.isPresent()) {
                            AvdUserPojo avdUserPojo = new AvdUserPojo();
                            avdUserPojo.setCreatorId(userOptional.get().getId());
                            avdUserPojo.setCreatorName(userOptional.get().getUsername());
                            String avator = "";
                            if (userOptional.get().getAvatar() != null){
                                avator = serviceHelper.getFtpAccountConfig().getImageUrlBasePath() + userOptional.get().getAvatar();
                            }
                            avdUserPojo.setCreatorAvatar(avator);
                            result.setUser(avdUserPojo);
                        }

                        // 标签
                        var theTags = allTags.stream().filter(t -> t.getAvdId().equals(userViewLog.getBusinessId()))
                                .collect(Collectors.toList());
                        result.setTags(theTags);
                    }
                }
                resultList.add(result);
            }
        }

        // 返回
        UserViewLogListResultPojo userViewLogListResultPojo = new UserViewLogListResultPojo();
        userViewLogListResultPojo.setList(resultList);
        userViewLogListResultPojo.setTotal(userViewLogCount);

        ServiceResult<UserViewLogListResultPojo> serviceResult = new ServiceResult<UserViewLogListResultPojo>();
        serviceResult.setMessage(ServiceResult.SELECT_SUCCESS);
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(userViewLogListResultPojo);

        return serviceResult;
    }
}