package com.innovation.ic.cyz.base.service.cyz;

import com.innovation.ic.cyz.base.model.cyz.ProductProperty;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_parameter.DeviceSelectionParameterQueryListRespPojo;
import com.innovation.ic.cyz.base.vo.cyz.deviceSelectionParameter.DeviceSelectionParameterFuzzyQueryVo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_property.PsPropertyVo;

import java.util.List;

/**
 * @author linuo
 * @desc products_property表接口
 * @time 2022年11月3日09:37:48
 */
public interface ProductPropertyService {
    /**
     * 清空products_property表数据
     */
    void truncateProductProperty();

    /**
     * 批量插入ProductProperty表数据
     *
     * @param list 需要插入的数据
     * @return 返回插入结果
     */
    ServiceResult<Boolean> batchInsertProductPropertyList(List<ProductProperty> list) throws InterruptedException;

    Integer total();

    List<ProductProperty> findByPage(int pageNum, int pageSize);

    /**
     * 查询筛选参数
     *
     * @param psPropertyVo           器件选型属性的vo类
     * @param externallyUniqueIdList 器件选型类型包含参数id集合
     * @return
     */
    ServiceResult<DeviceSelectionParameterQueryListRespPojo> getSelectProperties(PsPropertyVo psPropertyVo, List<String> externallyUniqueIdList);

    /**
     * 参数筛选模糊查询
     *
     * @param deviceSelectionParameterFuzzyQueryVo 参数筛选模糊查询的vo类
     * @return
     */
    ServiceResult<List<String>> queryFuzzyParamSelectPropertiesCyz(DeviceSelectionParameterFuzzyQueryVo deviceSelectionParameterFuzzyQueryVo);
}