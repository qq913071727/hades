//package com.innovation.ic.cyz.base.manager;
//
//import com.innovation.ic.cyz.base.value.config.RabbitMqParamConfig;
//import io.minio.MinioClient;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
//import javax.annotation.Resource;
//
//public abstract class AbstractManager {
//    @Autowired
//    protected MinioClient minioClient;
//
//    @Resource
//    protected RedisTemplate<String, String> redisTemplate;
//
//    @Resource
//    protected RabbitMqParamConfig rabbitMqParamConfig;
//}