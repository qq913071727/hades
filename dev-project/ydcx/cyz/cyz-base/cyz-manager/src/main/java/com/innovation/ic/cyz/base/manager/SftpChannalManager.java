//package com.innovation.ic.cyz.base.manager;
//
//import com.innovation.ic.cyz.base.value.config.FtpAccountConfig;
//import com.jcraft.jsch.Channel;
//import com.jcraft.jsch.ChannelSftp;
//import com.jcraft.jsch.JSch;
//import com.jcraft.jsch.Session;
//import org.springframework.stereotype.Component;
//import javax.annotation.PostConstruct;
//import javax.annotation.Resource;
//import java.util.Properties;
//
///**
// * @desc   SftpChannal的管理类
// * @author linuo
// * @time   2022年7月7日16:49:07
// */
//@Component
//public class SftpChannalManager {
//    public static ChannelSftp channelSftp;
//
//    @Resource
//    private FtpAccountConfig ftpAccountConfig;
//
//    @PostConstruct
//    public void init() {
//        try {
//            JSch jsch = new JSch();
//            jsch.getSession(ftpAccountConfig.getUsername(), ftpAccountConfig.getHost(), ftpAccountConfig.getPort());
//            Session sshSession = jsch.getSession(ftpAccountConfig.getUsername(), ftpAccountConfig.getHost(), ftpAccountConfig.getPort());
//            sshSession.setPassword(ftpAccountConfig.getPassword());
//            Properties sshConfig = new Properties();
//            sshConfig.put("StrictHostKeyChecking", "no");
//            sshSession.setConfig(sshConfig);
//            sshSession.connect();
//            Channel channel = sshSession.openChannel("sftp");
//            channel.connect();
//            channelSftp = (ChannelSftp) channel;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 获取sftp连接
//     * @return 返回sftp连接
//     */
//    public ChannelSftp getChannelSftp(){
//        return channelSftp;
//    }
//}