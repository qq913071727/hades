//package com.innovation.ic.cyz.base.manager;
//
//import com.google.common.base.Strings;
//import org.springframework.stereotype.Component;
//import javax.annotation.PostConstruct;
//import java.io.IOException;
//import com.rabbitmq.client.*;
//import java.util.concurrent.TimeoutException;
//
///**
// * Rabbitmq的管理类
// */
//@Component
//public class RabbitMqManager extends AbstractManager {
//    private static ConnectionFactory factory;
//    private static Connection connection;
//    private static Channel channel;
//
//    /** 默认交换机名称 */
//    private String DEFAULT_EXCHANGE = "cyz";
//
//    /**
//     * 向指定交换机发送消息
//     * @param exchange 交换机名称
//     * @param queueName 队列名称
//     * @param routingKey 路由Key
//     * @param basicProperties 基础配置信息，默认为null
//     * @param b 消息二进制数据
//     * @throws IOException 抛出IO异常
//     */
//    public void basicPublish(String exchange, String queueName, String routingKey, AMQP.BasicProperties basicProperties, byte[] b) throws IOException {
//        channel.queueBind(queueName, exchange, routingKey);
//        channel.exchangeDeclare(exchange, BuiltinExchangeType.DIRECT, true);
//        channel.basicPublish(exchange, routingKey, basicProperties, b);
//    }
//
//    @PostConstruct
//    public void init() {
//        factory = new ConnectionFactory();
//        factory.setHost(rabbitMqParamConfig.getHost());
//        factory.setPort(rabbitMqParamConfig.getPort());
//        factory.setUsername(rabbitMqParamConfig.getUsername());
//        factory.setPassword(rabbitMqParamConfig.getPassword());
//        if(!Strings.isNullOrEmpty(rabbitMqParamConfig.getVirtualHost())){
//            factory.setVirtualHost(rabbitMqParamConfig.getVirtualHost());
//        }
//
//        try {
//            connection = factory.newConnection();
//            channel = connection.createChannel();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (TimeoutException e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 获取channel
//     * @return 返回channel
//     */
//    public Channel getChannel(){
//        return channel;
//    }
//
//    /**
//     * 声明交换机
//     * @param exchange
//     * @param builtinExchangeType
//     * @param b
//     * @throws IOException
//     */
//    public void exchangeDeclare(String exchange, BuiltinExchangeType builtinExchangeType, boolean b) throws IOException {
//        channel.exchangeDeclare(exchange, builtinExchangeType, b);
//    }
//
//    /**
//     * 向指定交换机发送消息
//     * @param exchange 交换机名称
//     * @param queueName 队列名称
//     * @param basicProperties 基础配置信息，默认为null
//     * @param b 消息二进制数据
//     * @throws IOException 抛出IO异常
//     */
//    public void basicPublish(String exchange, String queueName, AMQP.BasicProperties basicProperties, byte[] b) throws IOException {
//        channel.exchangeDeclare(exchange, BuiltinExchangeType.DIRECT, true);
//        channel.basicPublish(exchange, queueName, basicProperties, b);
//    }
//
//    /**
//     * 发送消息，通过默认交换机
//     * @param queueName
//     * @param message
//     * @throws IOException
//     */
//    public void basicPublish(String queueName, String message) throws IOException {
//        channel.exchangeDeclare(this.DEFAULT_EXCHANGE, BuiltinExchangeType.DIRECT, true);
//        channel.basicPublish(this.DEFAULT_EXCHANGE, queueName, null, message.getBytes());
//    }
//
//    /**
//     * 关闭连接
//     * @throws IOException
//     * @throws TimeoutException
//     */
//    public void close() throws IOException, TimeoutException {
//        channel.close();
//        connection.close();
//    }
//}