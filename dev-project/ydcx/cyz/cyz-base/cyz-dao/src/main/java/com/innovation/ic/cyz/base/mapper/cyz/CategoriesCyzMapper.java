package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.Categories;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriesCyzMapper extends EasyBaseMapper<Categories> {

    void truncateCategories();
}
