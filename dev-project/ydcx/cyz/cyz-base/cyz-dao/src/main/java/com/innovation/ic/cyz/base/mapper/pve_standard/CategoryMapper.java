package com.innovation.ic.cyz.base.mapper.pve_standard;

import com.innovation.ic.cyz.base.model.pve_standard.Category;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryMapper extends EasyBaseMapper<Category> {

    List<Category> findByPage(@Param("offset") int offset, @Param("rows") int rows);
}
