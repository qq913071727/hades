package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.MfrContent;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface MfrContentMapper extends EasyBaseMapper<MfrContent> {

}
