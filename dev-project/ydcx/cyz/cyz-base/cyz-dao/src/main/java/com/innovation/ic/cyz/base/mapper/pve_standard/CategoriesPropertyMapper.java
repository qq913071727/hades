package com.innovation.ic.cyz.base.mapper.pve_standard;

import com.innovation.ic.cyz.base.model.pve_standard.CategoriesProperty;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property.PsCategoryPropertyPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoriesPropertyMapper extends EasyBaseMapper<CategoriesProperty> {
    /**
     * 获取分类属性
     *
     * @param categoryId
     * @return
     */
    List<PsCategoryPropertyPojo> getPsCategoriesProperty(String categoryId);

    /**
     * 根据器件选型类型包含参数id集合查询分类对应属性表的属性ID集合
     *
     * @param idList 器件选型类型包含参数id集合
     * @return 返回分类对应属性表的属性ID集合
     */
    List<String> getPropertyIdsByIdList(@Param("idList") List<String> idList);

    /**
     * 分页查询
     * @param offset
     * @param rows
     * @return
     */
    List<CategoriesProperty> findByPage(@Param("offset") int offset, @Param("rows") int rows);
}