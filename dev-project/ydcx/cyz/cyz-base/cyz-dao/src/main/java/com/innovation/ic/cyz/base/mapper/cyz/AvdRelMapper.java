package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.AvdRel;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AvdRelMapper extends EasyBaseMapper<AvdRel> {
    /**
     * 获取关联所属类型信息根据（文章id/关系/分类）
     *
     * @param id
     * @param type
     * @param nameId
     */
    AvdRel selectAvdRelData(@Param("id") Long id, @Param("type") int type, @Param("nameId") Integer nameId);
}
