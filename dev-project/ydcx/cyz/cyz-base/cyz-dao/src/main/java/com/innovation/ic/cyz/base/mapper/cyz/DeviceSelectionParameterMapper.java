package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.DeviceSelectionParameter;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_parameter.DeviceSelectionParameterParamPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface DeviceSelectionParameterMapper extends EasyBaseMapper<DeviceSelectionParameter> {
    /**
     * 查询筛选类型的器件选型类型包含参数
     * @param typeId 关联类型id
     * @return 返回查询结果
     */
    List<DeviceSelectionParameterParamPojo> selectFastScreenByTypeId(@Param("typeId") String typeId, @Param("type") Integer type);

    /**
     * 通过器件选型类型id获取器件选型类型包含参数id集合
     * @param categoryId 器件选型类型id
     * @param type 类型 1、快速筛选 2、参数筛选
     * @return 返回查询结果
     */
    List<String> getExternallyUniqueIdListByCategoryId(@Param("categoryId") String categoryId, @Param("type") Integer type);
}