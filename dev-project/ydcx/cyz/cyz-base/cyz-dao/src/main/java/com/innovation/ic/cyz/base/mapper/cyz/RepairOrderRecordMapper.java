package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.RepairOrderRecord;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface RepairOrderRecordMapper extends EasyBaseMapper<RepairOrderRecord> {
    /**
     * 查询记录信息集合
     * @param repairOrderId 工单id
     * @return 返回工单记录信息集合
     */
    List<RepairOrderRecord> selectRecordsList(@Param("repairOrderId") Long repairOrderId);

    /**
     * 查询创建用户记录id
     * @param repairOrderId 工单id
     * @return 返回创建用户记录id
     */
    Long selectCreateRecordId(@Param("repairOrderId") Long repairOrderId);

    /**
     * 根据工单id查询最近一条流程记录
     * @param id 工单id
     * @return 返回查询结果
     */
    RepairOrderRecord selectLastRecordByOrderId(@Param("id") Long id);
}