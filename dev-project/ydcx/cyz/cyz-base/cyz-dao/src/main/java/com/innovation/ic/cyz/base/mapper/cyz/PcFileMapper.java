package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.PcFile;

import com.innovation.ic.cyz.base.pojo.variable.cyz.pc_file.PcFileRespPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PcFileMapper  extends EasyBaseMapper<PcFile> {

    /**
     * 查询附件信息
     * @param productId 产品id
     * @return 返回查询结果
     */
    List<PcFileRespPojo> selectFilesByProductId(@Param("productId") String productId);
}
