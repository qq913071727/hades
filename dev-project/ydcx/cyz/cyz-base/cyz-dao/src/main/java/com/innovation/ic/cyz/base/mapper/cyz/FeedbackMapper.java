package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.Feedback;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface FeedbackMapper extends EasyBaseMapper<Feedback> {

    /**
     * 通过用户id与提交时间获取到信息反馈数据
     *
     * @param id
     * @param createDate
     * @return
     */
    List<Feedback> findFeedback(@Param("id") String id, @Param("createDate") Date createDate);
}
