package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.RepairOrderFile;
import com.innovation.ic.cyz.base.pojo.variable.cyz.rabbit_mq.FilesPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_file.RepairOrderFileRespPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface RepairOrderFileMapper extends EasyBaseMapper<RepairOrderFile> {
    /**
     * 根据工单id、记录id查询工单附件信息
     * @param repairOrderId 工单id
     * @param repairOrderRecordId 工单记录id
     * @return 返回工单附件信息
     */
    List<RepairOrderFileRespPojo> selectFilesByRepairOrderRecordId(@Param("repairOrderId") Long repairOrderId, @Param("repairOrderRecordId") Long repairOrderRecordId);

    /**
     * 根据流程记录id查询附件地址集合
     * @param id 流程记录id
     * @return 返回查询结果
     */
    List<FilesPojo> selectFilesByRecordId(@Param("id") Long id);
}