package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.Categories;
import com.innovation.ic.cyz.base.model.cyz.CategoriesProperty;
import com.innovation.ic.cyz.base.pojo.variable.cyz.PsCategoryPropertyPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_parameter.DeviceSelectionParamPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoriesPropertyCyzMapper extends EasyBaseMapper<CategoriesProperty> {

    void truncatetCategoriesProperty();

    /**
     * 根据器件选型类型包含参数id集合查询分类对应属性表数据集合
     *
     * @param idList 器件选型类型包含参数id集合
     * @return 返回分类对应属性表数据集合
     */
    List<DeviceSelectionParamPojo> getPropertyCyzByIdList(@Param("idList") List<String> idList);

    /**
     * 获取件选型配置属性
     *
     * @param categoriesList
     * @return 返回件选型配置属性数据集合
     */
    List<PsCategoryPropertyPojo> selectCategoriesProperty(@Param("categoriesList")List<String> categoriesList);

    /**
     * 获取分类属性
     *
     * @param categoriesList
     * @return
     */
    List<PsCategoryPropertyPojo> getPsCategoriesProperty(@Param("categoriesList")List<String> categoriesList);
}
