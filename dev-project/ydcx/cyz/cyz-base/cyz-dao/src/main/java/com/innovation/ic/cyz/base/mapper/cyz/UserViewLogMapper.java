package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.UserViewLog;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserViewLogMapper extends EasyBaseMapper<UserViewLog> {

    /**
     * 用户浏览/点赞/收藏记录列表
     */
    List<UserViewLog> findUserViewLogList(@Param("page") Integer page,
                                          @Param("size") Integer size,
                                          @Param("types") List<Integer> types,
                                          @Param("creatorId") String creatorId);

    /**
     * 用户浏览/点赞/收藏记录数量
     */
    Long findUserViewLogCount(@Param("types") List<Integer> types,
                              @Param("creatorId") String creatorId);


    /**
     * 获取到用户记录点赞相关数据并且分页
     * @param pageNo
     * @param pageSize
     * @param userId
     * @param myLike
     *
     */
    List<UserViewLog> selectPageUser(@Param("pageNo")Integer pageNo, @Param("pageSize")Integer pageSize, @Param("userId")String userId, @Param("myLike")int myLike);
}
