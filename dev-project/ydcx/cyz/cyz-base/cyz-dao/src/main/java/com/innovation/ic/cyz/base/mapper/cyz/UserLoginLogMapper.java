package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;

import com.innovation.ic.cyz.base.model.cyz.UserLoginLog;
import org.springframework.stereotype.Repository;

@Repository
public interface UserLoginLogMapper extends EasyBaseMapper<UserLoginLog> {
}
