package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.MfrParam;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MfrParamMapper extends EasyBaseMapper<MfrParam> {
    /**
     * 根据codes查询MfrParam
     *
     * @param codes
     * @return
     */
    List<MfrParam> findMfrParamsByCodes(@Param("module") Integer module,
                                        @Param("codes") List<String> codes);

    /**
     * 根据parentIds查询MfrParam
     *
     * @param module
     * @param parentIds
     * @return
     */
    List<MfrParam> findMfrParamsByParentIds(@Param("module") Integer module,
                                            @Param("parentIds") List<Long> parentIds);

    void insertReturnId(MfrParam mfrParam);

    MfrParam findByCodeAndModule(@Param("code") Object annotationValue, @Param("module")Integer module);

    List<MfrParam> findMfrParamsByAllCodes(Integer module);
}
