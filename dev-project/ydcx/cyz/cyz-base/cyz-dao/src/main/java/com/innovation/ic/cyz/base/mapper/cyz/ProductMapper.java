package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.Product;
import com.innovation.ic.cyz.base.pojo.variable.cyz.product.ProductQueryRespPojo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsPartNumbersQueryByContextVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ProductMapper extends EasyBaseMapper<Product> {
    /**
     * 清空product表数据
     */
    void truncateProduct();

    /**
     * 型号模糊查询
     * @param psPartNumbersQueryByContextVo 型号模糊查询的vo类
     * @return 返回查询结果
     */
    List<ProductQueryRespPojo> queryPartNumbersByContext(@Param("param") PsPartNumbersQueryByContextVo psPartNumbersQueryByContextVo);

    List<Product> findByPage(@Param("start")int start, @Param("end")int end);
}