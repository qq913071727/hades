package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.RepairOrderTypeHead;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type_head.RepairOrderTypeFaeListRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type_head.RepairOrderTypeHeadInfoPojo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepairOrderTypeHeadMapper extends EasyBaseMapper<RepairOrderTypeHead> {

    /**
     * 根据工单类型id查询负责人信息
     * @param typeId 工单类型id
     * @return 返回负责人姓名
     */
    RepairOrderTypeHeadInfoPojo selectRealNameByRepairOrderTypeId(Long typeId);

    /**
     * 获取FAE列表
     * @return 返回查询结果
     */
    List<RepairOrderTypeFaeListRespPojo> getFaeList();
}