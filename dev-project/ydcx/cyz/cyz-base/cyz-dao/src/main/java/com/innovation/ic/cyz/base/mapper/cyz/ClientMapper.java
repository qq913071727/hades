package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.Client;
import org.springframework.stereotype.Repository;

/**
 * @desc   Client表的mapper类
 * @author linuo
 * @time   2022年8月11日15:27:35
 */
@Repository
public interface ClientMapper extends EasyBaseMapper<Client> {

}