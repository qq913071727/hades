package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.TenantImg;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface TenantImgMapper extends EasyBaseMapper<TenantImg> {
    /**
     * 根据tenantsId删除TenantImg
     * @param tenantsId
     */
    void deleteTenantImgByTenantsId(int tenantsId);
}
