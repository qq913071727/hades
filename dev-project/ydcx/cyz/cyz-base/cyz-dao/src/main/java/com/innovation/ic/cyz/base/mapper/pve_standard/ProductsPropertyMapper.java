package com.innovation.ic.cyz.base.mapper.pve_standard;

import com.innovation.ic.cyz.base.model.cyz.ProductProperty;
import com.innovation.ic.cyz.base.model.pve_standard.ProductsProperty;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_parameter.DeviceSelectionParamPojo;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property.PsProductPropertyRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property.PsPropertyPojo;
import com.innovation.ic.cyz.base.vo.cyz.deviceSelectionParameter.DeviceSelectionParameterFuzzyQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ProductsPropertyMapper extends EasyBaseMapper<ProductsProperty> {
    /**
     * 根据分类Id查询属性信息
     */
    List<PsPropertyPojo> findProperties(@Param("categoryId") String categoryId);

    /**
     * 根据属性Ids查询属性值信息
     */
    List<ProductsProperty> findPropertyValuesByPropertyIds(@Param("propertyIds") List<String> propertyIds);

    /**
     * 根据器件选型类型包含参数id集合查询分类对应属性表数据集合
     * @param idList 器件选型类型包含参数id集合
     * @return 返回分类对应属性表数据集合
     */
    List<DeviceSelectionParamPojo> getPropertysByIdList(@Param("idList") List<String> idList);

    /**
     * 参数筛选模糊查询
     * @param deviceSelectionParameterFuzzyQueryVo 参数筛选模糊查询的vo类
     * @return 返回查询结果
     */
    List<String> queryFuzzyParamSelectProperties(@Param("param") DeviceSelectionParameterFuzzyQueryVo deviceSelectionParameterFuzzyQueryVo);

    /**
     * 查询标准库ProductsUnique表状态是正常的数据数量
     * @return 返回查询结果
     */
    Long getEffectiveDataCount();

    /**
     * 分页查询ProductsProperty表中有效数据
     * @param start 分页开始
     * @param end 分页结束
     * @return 返回查询结果
     */
    List<ProductProperty> getEffectiveDataByPage(@Param("start") long start, @Param("end") long end);

    /**
     * 根据产品id查询属性信息
     * @param productId 产品id
     * @return 返回查询结果
     */
    List<PsProductPropertyRespPojo> selectPropertyByProductId(@Param("productId") String productId);
}