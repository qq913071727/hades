package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.DeviceSelectionType;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionTypeRespPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface DeviceSelectionTypeMapper extends EasyBaseMapper<DeviceSelectionType> {
    Integer checkValid(@Param("id") String id);

    /**
     * 查询一级菜单
     * @return 返回查询结果
     */
    List<DeviceSelectionType> selectFirstLevelDeviceSelectionList();

    /**
     * 根据id查询同级的器件选型类型
     * @param id 主键id
     * @return 返回查询结果
     */
    List<DeviceSelectionTypeRespPojo> findSameLevelTypeListById(@Param("id") String id);
}