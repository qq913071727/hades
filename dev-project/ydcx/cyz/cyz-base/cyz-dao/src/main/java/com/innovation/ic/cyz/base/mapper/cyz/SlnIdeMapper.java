package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.SlnIde;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SlnIdeMapper extends EasyBaseMapper<SlnIde> {

    /**
     * 查询开发平台
     *
     * @return
     */
    List<String> findIdeCateNames();

}
