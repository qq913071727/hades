package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.Inquiry;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.pojo.variable.cyz.InquiryExportPojo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InquiryMapper extends EasyBaseMapper<Inquiry> {

    /**
     * 查询所有记录，只返回excel需要的列
     * @return
     */
    List<InquiryExportPojo> findAllRecordForExcel();

    /**
     * 查询所有未导出过记录，只返回excel需要的列
     * @return
     */
    List<InquiryExportPojo> findNotExportRecordForExcel();
    
}
