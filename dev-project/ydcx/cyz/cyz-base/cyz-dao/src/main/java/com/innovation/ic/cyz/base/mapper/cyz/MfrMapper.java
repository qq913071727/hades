package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.Mfr;
import com.innovation.ic.cyz.base.model.cyz.MfrContent;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.vo.cyz.MfrBackEndPageVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MfrMapper extends EasyBaseMapper<Mfr> {
    /**
     * 查询pcb厂商Id
     *
     * @param paramIds
     * @param mfrNameKey
     * @param mfrName
     * @return
     */
    List<Long> findMfrPcbIds(@Param("page") Integer page,
                             @Param("size") Integer size,
                             @Param("paramIds") List<Long> paramIds,
                             @Param("mfrNameKey") Long mfrNameKey,
                             @Param("mfrName") String mfrName,
                             @Param("regionKey") Long regionKey,
                             @Param("regionIds") List<Long> regionIds);

    /**
     * 查询pcb厂商数量
     *
     * @param paramIds
     * @param mfrNameKey
     * @param mfrName
     * @return
     */
    Long findMfrPcbCount(@Param("paramIds") List<Long> paramIds,
                         @Param("mfrNameKey") Long mfrNameKey,
                         @Param("mfrName") String mfrName,
                         @Param("regionKey") Long regionKey,
                         @Param("regionIds") List<Long> regionIds);

    /**
     * 查询smt厂商Id
     *
     * @param paramIds
     * @param mfrNameKey
     * @param mfrName
     * @return
     */
    List<Long> findMfrSmtIds(@Param("page") Integer page,
                             @Param("size") Integer size,
                             @Param("paramIds") List<Long> paramIds,
                             @Param("mfrNameKey") Long mfrNameKey,
                             @Param("mfrName") String mfrName,
                             @Param("regionKey") Long regionKey,
                             @Param("regionIds") List<Long> regionIds);


    /**
     * 查询smt厂商数量
     *
     * @param paramIds
     * @param mfrNameKey
     * @param mfrName
     * @return
     */
    Long findMfrSmtCount(@Param("paramIds") List<Long> paramIds,
                         @Param("mfrNameKey") Long mfrNameKey,
                         @Param("mfrName") String mfrName,
                         @Param("regionKey") Long regionKey,
                         @Param("regionIds") List<Long> regionIds);

    /**
     * 根据MfrIds查询 pcb厂商列表 需要的信息
     *
     * @param mfrIds
     * @return
     */
    List<MfrContent> findMfrContentsByMfrIds(@Param("mfrIds") List<Long> mfrIds,
                                             @Param("mfrParamIds") List<Long> mfrParamIds);

    void insertReturnId(Mfr mfr);

    List<Mfr> findByPage(MfrBackEndPageVo mfrBackEndPageVo);
}
