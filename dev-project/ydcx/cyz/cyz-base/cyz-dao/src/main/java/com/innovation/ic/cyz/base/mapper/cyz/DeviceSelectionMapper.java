package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.DeviceSelection;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionTypeQueryRespPojo;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface DeviceSelectionMapper extends EasyBaseMapper<DeviceSelection> {
    /**
     * 查询器件选型类别
     * @return 返回查询结果
     */
    List<DeviceSelectionTypeQueryRespPojo> findDeviceSelection();
}