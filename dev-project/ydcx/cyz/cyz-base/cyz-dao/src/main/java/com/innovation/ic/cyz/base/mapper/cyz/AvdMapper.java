package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.Avd;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.users_center.UserSumPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface AvdMapper extends EasyBaseMapper<Avd> {

    /**
     * 分页查询避坑指南列表(公开的)
     *
     * @param page
     * @param size
     * @return
     */
    List<AvdListPojo> findAvdListForPublic(@Param("page") Integer page,
                                           @Param("size") Integer size,
                                           @Param("title") String title,
                                           @Param("module") Integer module,
                                           @Param("typeIds") List<Long> typeIds);

    /**
     * 查询避坑指南数量(公开的)
     *
     * @return
     */
    Long findAvdCountForPublic(@Param("title") String title,
                               @Param("module") Integer module,
                               @Param("typeIds") List<Long> typeIds);

    /**
     * 分页查询避坑指南列表(个人的)
     */
    List<AvdListPojo> findAvdListForPrivate(@Param("page") Integer page,
                                            @Param("size") Integer size,
                                            @Param("approveStatuses") List<Integer> approveStatuses,
                                            @Param("userId") String userId);

    /**
     * 查询避坑指南数量(个人的)
     */
    Long findAvdCountForPrivate(@Param("approveStatuses") List<Integer> approveStatuses,
                                @Param("userId") String userId);

    /**
     * 增加一个浏览量
     */
    void addOneViewNum(@Param("avdId") Long avdId);

    /**
     * 增加一个点赞量
     */
    void addOneLikeNum(@Param("avdId") Long avdId);

    /**
     * 减少一个点赞量
     *
     * @param avdId
     */
    void reduceOneLikeNum(@Param("avdId") Long avdId);

    /**
     * 增加一个收藏量
     */
    void addOneCollectNum(@Param("avdId") Long avdId);

    /**
     * 减少一个收藏量
     *
     * @param avdId
     */
    void reduceOneCollectNum(@Param("avdId") Long avdId);

    /**
     * 拿到当前用户在避坑指南中(阅读数量、收藏数量、点赞数量)总和
     *
     * @param id
     */
    UserSumPojo allSum(@Param("id") String id);

    /**
     * @Description: 分页列表
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2615:10
     */
    List<AvdListPojo> page(@Param("typeId") Long typeId,
                           @Param("module") Integer module,
                           @Param("userName") String userName,
                           @Param("createDate") String createDate
    );

    /**
     * 去通过创建id查询相关数据并分页
     *
     * @param pageNo
     * @param pageSize
     * @param id
     * @param module
     * @param createDate
     */
    List<Avd> selectListAvd(@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize, @Param("id") String id, @Param("module") Integer module, @Param("createDate") Date createDate);

    /**
     * 获取避坑指南中的文章信息根据条件文章id查询以及时间
     *
     * @param businessId
     * @param createDate
     */
    Avd findAvdData(@Param("businessId") Long businessId, @Param("createDate") Date createDate, @Param("module") Integer module);
}
