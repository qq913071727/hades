package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.FeedbackType;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedbackTypeMapper extends EasyBaseMapper<FeedbackType> {

    /**
     * 获取到问题类型数据通过id与问题类型
     *
     * @param id
     * @param feedbackType
     * @return
     */
    FeedbackType findType(@Param("id") Long id, @Param("feedbackType") Integer feedbackType);
}
