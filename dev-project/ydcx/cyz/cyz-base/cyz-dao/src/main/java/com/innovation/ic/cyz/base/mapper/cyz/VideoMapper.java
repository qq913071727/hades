package com.innovation.ic.cyz.base.mapper.cyz;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.innovation.ic.cyz.base.model.cyz.Video;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.pojo.variable.cyz.video.VidListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.video.VidTagPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VideoMapper extends EasyBaseMapper<Video> {
    /**
     * 添加浏览量
     *
     * @param id
     */
    void addViewNum(@Param("id") long id);

    IPage<VidListPojo> getList(Page page,
                               @Param("title") String title,
                               @Param("tag") String tag,
                               @Param("categories") List<String> categories,
                               @Param("brands") List<String> brands,
                               @Param("types") List<String> types,
                               @Param("orderByName") String orderByName,
                               @Param("orderByType") String orderByType);

    List<VidTagPojo> getTagList(@Param("vid") long vid);
}
