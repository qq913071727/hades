package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.ProductProperty;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ps_property.PsProductPropertyRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ps_property.PsPropertyPojo;
import com.innovation.ic.cyz.base.vo.cyz.deviceSelectionParameter.DeviceSelectionParameterFuzzyQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author linuo
 * @desc ProductProperty表的mapper类
 * @time 2022年11月3日09:40:08
 */
@Repository
public interface ProductPropertyMapper extends EasyBaseMapper<ProductProperty> {
    /**
     * 清空products_property表数据
     */
    void truncateProductProperty();

    List<ProductProperty> findByPage(@Param("start") int start, @Param("end") int end);

    /**
     * 根据属性Ids查询属性值信息
     *
     * @param propertyIds
     * @return
     */
    List<ProductProperty> findPropertyCyzValuesByPropertyIds(@Param("propertyIds") List<String> propertyIds);

    /**
     * 参数筛选模糊查询
     *
     * @param deviceSelectionParameterFuzzyQueryVo
     * @return
     */
    List<String> queryFuzzyParamSelectPropertiesCyz(@Param("param") DeviceSelectionParameterFuzzyQueryVo deviceSelectionParameterFuzzyQueryVo);

    /**
     * 根据分类Id查询属性信息
     */
    List<PsPropertyPojo> findProperties(@Param("categoriesList")List<String> categoriesList);

    /**
     * 根据属性Ids查询属性值信息
     */
    List<ProductProperty> findPropertyValuesByPropertyIds(@Param("propertyIds")List<String> propertyIds);

    /**
     * 获取器件参数
     *
     * @param productId 产品id
     * @return 返回查询结果
     */
    List<PsProductPropertyRespPojo> selectPropertyByProductId(@Param("productId")String productId);
}