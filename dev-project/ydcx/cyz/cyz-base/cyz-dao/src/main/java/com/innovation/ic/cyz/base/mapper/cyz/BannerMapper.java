package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.Banner;
import com.innovation.ic.cyz.base.pojo.variable.cyz.banner.BannerAvailableDataRespPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface BannerMapper extends EasyBaseMapper<Banner> {

    /**
     * Bannner列表分页
     * @param pageNo
     * @param pageSize
     * @return
     */
    List<Banner> allBanner(@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);

    /**
     * 获取启用的轮播图数据
     * @return 返回查询结果
     */
    List<BannerAvailableDataRespPojo> getAvailableBannerList();
}