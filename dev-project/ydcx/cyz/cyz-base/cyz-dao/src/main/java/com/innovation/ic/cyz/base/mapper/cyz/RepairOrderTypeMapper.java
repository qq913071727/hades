package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.RepairOrderType;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type.FirstLevelRepairOrderTypeQueryRespPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

@Repository
public interface RepairOrderTypeMapper extends EasyBaseMapper<RepairOrderType> {
    /**
     * 根据id查询下属工单分类的id集合
     * @param id 上级工单分类id
     * @return 返回下属工单分类的id集合
     */
    List<Long> selectChildRepairOrderTypeIdList(@Param("id") Long id);

    /**
     * 分页查询分类级别为1的工单分类数据
     * @param map 查询参数
     * @return 返回查询结果
     */
    List<RepairOrderType> selectRepairOrderTypeList(@Param("map") Map<String, Object> map);

    /**
     * 查询一级分类数量
     * @return 返回一级分类数量
     */
    Integer selectFirstClassTypeCount();

    /**
     * 获取一级分类
     * @return 返回查询结果
     */
    List<FirstLevelRepairOrderTypeQueryRespPojo> getFirstTypeList();

    /**
     * 获取一级工单类型id
     * @param typeId 工单类型id
     * @return 返回一级工单类型id
     */
    Long getFirstTypeIdByTypeId(@Param("typeId") Long typeId);
}