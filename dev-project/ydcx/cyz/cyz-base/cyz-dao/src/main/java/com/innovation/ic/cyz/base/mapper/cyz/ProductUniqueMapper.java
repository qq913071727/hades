package com.innovation.ic.cyz.base.mapper.cyz;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.ProductUnique;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ps_productunique.PsProdsUniqPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ps_productunique.PsProductQueryByBrandPartRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ps_property.PsPropertyPackagePojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ps_property.PsPropertyPackingPojo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsPartNumbersQueryByContextVo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsProdsUniqVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ProductUniqueMapper extends EasyBaseMapper<ProductUnique> {
    /**
     * 清空product_unique表数据
     */
    void truncateProductUnique();

    List<ProductUnique> findByPage(@Param("start") int start, @Param("end")int end);

    /**
     * 查询封装信息
     */
    List<PsPropertyPackagePojo> findPackages(@Param("categoriesList") List<String> categoriesList);

    /**
     * 查询包装信息
     */
    List<PsPropertyPackingPojo> findPackings(@Param("categoriesList") List<String> categoriesList);

    /**
     * 查询列表
     *
     * @param p
     * @param param
     * @return
     */
    IPage<PsProdsUniqPojo> getList(Page<ProductUnique> p,@Param("param") PsProdsUniqVo param);

    /**
     * 型号模糊查询
     * @param psPartNumbersQueryByContextVo 型号模糊查询的vo类
     * @return 返回查询结果
     */
    List<String> queryPartNumbersByContext(@Param("param")PsPartNumbersQueryByContextVo psPartNumbersQueryByContextVo);

    /**
     * 根据品牌、型号查询器件
     * @param map 查询条件
     * @return 返回查询结果
     */
    PsProductQueryByBrandPartRespPojo queryProductByBrandPartNumber(@Param("map") Map<String, String> map);

    /**
     * 根据ProductUniqueIds查询列表
     *
     * @return
     */
    List<PsProdsUniqPojo> getListByUids(@Param("uids") String[] uids);
}