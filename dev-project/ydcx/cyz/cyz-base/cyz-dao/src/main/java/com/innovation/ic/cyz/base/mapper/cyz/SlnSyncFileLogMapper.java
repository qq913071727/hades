package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.SlnSyncFileLog;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface SlnSyncFileLogMapper extends EasyBaseMapper<SlnSyncFileLog> {

}
