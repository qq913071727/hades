package com.innovation.ic.cyz.base.mapper.pve_standard;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.pve_standard.PcFiles;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.pc_files.PcFilesRespPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface PcFilesMapper extends EasyBaseMapper<PcFiles> {

    /**
     * 查询附件信息
     * @param productId 产品id
     * @return 返回查询结果
     */
    List<PcFilesRespPojo> selectFilesByProductId(@Param("productId") String productId);
}