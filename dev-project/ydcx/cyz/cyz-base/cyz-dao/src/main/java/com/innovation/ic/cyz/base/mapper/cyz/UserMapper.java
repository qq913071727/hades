package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.User;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper extends EasyBaseMapper<User> {
    /**
     * 用户中心列表信息
     *
     * @param pageNo
     * @param pageSize
     * @return
     */
    List<User> allUsersCenter(@Param("pageNo") Integer pageNo, @Param("pageSize")Integer pageSize);

    /**
     * 获取出基本用户信息(来源)
     *
     * @param pageNo
     * @param pageSize
     * @param source
     * @return
     */
    List<User> allUsersCenterFindSource(@Param("pageNo")Integer pageNo, @Param("pageSize")Integer pageSize, @Param("source") Integer source);
}
