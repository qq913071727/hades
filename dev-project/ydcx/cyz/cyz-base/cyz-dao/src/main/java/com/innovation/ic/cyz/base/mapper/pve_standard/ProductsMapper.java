package com.innovation.ic.cyz.base.mapper.pve_standard;

import com.innovation.ic.cyz.base.model.cyz.Product;
import com.innovation.ic.cyz.base.model.pve_standard.Products;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ProductsMapper extends EasyBaseMapper<Products> {
    /**
     * 查询标准库Products状态是正常的数据数量
     * @return 返回查询结果
     */
    Long getNormalStatusDataCount();

    /**
     * 分页查询Products表中状态为正常的数据
     * @param start 分页开始
     * @param end 分页结束
     * @return 返回查询结果
     */
    List<Product> getNormalStatusDataByPage(@Param("start") long start, @Param("end") long end);
}