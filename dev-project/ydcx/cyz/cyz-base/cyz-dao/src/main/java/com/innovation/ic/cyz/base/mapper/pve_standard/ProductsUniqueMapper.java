package com.innovation.ic.cyz.base.mapper.pve_standard;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.innovation.ic.cyz.base.model.cyz.ProductUnique;
import com.innovation.ic.cyz.base.model.pve_standard.ProductsUnique;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_productsunique.PsProdsUniqPojo;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_productsunique.PsProductPropertyPojo;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_productsunique.PsProductQueryByBrandPartRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property.PsPropertyPackagePojo;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property.PsPropertyPackingPojo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsPartNumbersQueryByContextVo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsProdsUniqVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

@Repository
public interface ProductsUniqueMapper extends EasyBaseMapper<ProductsUnique> {
    /**
     * 查询封装信息
     */
    List<PsPropertyPackagePojo> findPackages(@Param("categoryId") String categoryId);

    /**
     * 查询包装信息
     */
    List<PsPropertyPackingPojo> findPackings(@Param("categoryId") String categoryId);

    /**
     * 查询列表
     *
     * @param page
     * @return
     */
    IPage<PsProdsUniqPojo> getList(Page page, @Param("param") PsProdsUniqVo param);

    /**
     * 根据ProductUniqueIds查询列表
     *
     * @return
     */
    List<PsProdsUniqPojo> getListByUids(@Param("uids") String[] uids);

    /**
     * 获取属性
     *
     * @param pid
     * @return
     */
    List<PsProductPropertyPojo> getProperties(@Param("pid") String pid);

    /**
     * 根据品牌、型号查询器件
     * @param map 查询条件
     * @return 返回查询结果
     */
    PsProductQueryByBrandPartRespPojo queryProductsByBrandPartNumber(@Param("map") Map<String, String> map);

    /**
     * 型号模糊查询
     * @param psPartNumbersQueryByContextVo 型号模糊查询的vo类
     * @return 返回查询结果
     */
    List<String> queryPartNumbersByContext(@Param("param") PsPartNumbersQueryByContextVo psPartNumbersQueryByContextVo);

    /**
     * 分页查询ProductsUnique表中状态为正常的数据
     * @param start 分页开始
     * @param end 分页结束
     * @return 返回查询结果
     */
    List<ProductUnique> getNormalStatusDataByPage(@Param("start") long start, @Param("end") long end);

    /**
     * 查询标准库ProductsUnique表状态是正常的数据数量
     * @return 返回查询结果
     */
    Long getNormalStatusDataCount();
}