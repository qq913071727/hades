package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.Brand;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ps_property.PsPropertyBrandPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @desc   Brands表的mapper类
 * @author linuo
 * @time   2022年10月28日10:17:47
 */
@Repository
public interface BrandMapper extends EasyBaseMapper<Brand> {
    /**
     * 清空brand表数据
     */
    void truncateBrand();

    /**
     * 针对于大数据查询
     * @param start
     * @param pageSize
     * @return
     */
    List<Brand> findByPage(@Param("start") int start, @Param("end")int pageSize);

    /**
     * 查询品牌信息
     */
    List<PsPropertyBrandPojo> findBrands(@Param("categoriesList")List<String> categoriesList);
}