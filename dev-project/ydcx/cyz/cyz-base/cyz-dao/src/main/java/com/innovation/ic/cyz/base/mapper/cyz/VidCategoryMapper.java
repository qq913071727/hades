package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.VidCategory;
import org.springframework.stereotype.Repository;

@Repository
public interface VidCategoryMapper extends EasyBaseMapper<VidCategory> {
}
