package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.VidTag;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.pojo.variable.cyz.video.VidTagPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VidTagMapper extends EasyBaseMapper<VidTag> {
    List<VidTagPojo> findByVidTagPojo(@Param("videoId") Long videoId);
}
