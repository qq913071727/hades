package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.model.cyz.RepairOrder;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order.RepairOrderAdminListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order.RepairOrderWebListPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

@Repository
public interface RepairOrderMapper extends EasyBaseMapper<RepairOrder> {

    /**
     * 查询使用了工单分类的数据数量
     * @param idList 工单分类id集合
     * @return 返回使用了工单分类的数据数量
     */
    int selectUseRepairOrderTypeDataCount(@Param("idList") List<Long> idList);

    /**
     * 查询我的工单列表
     * @param userId 登录用户id
     * @return 返回我的工单列表数据
     */
    List<RepairOrderWebListPojo> queryMyRepairOrder(@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize, @Param("userId") String userId);

    /**
     * 查询我的工单总数量
     * @param userId 登录用户id
     * @return 返回我的工单总数量
     */
    Long queryMyRepairOrderCount(@Param("userId") String userId);

    /**
     * 根据参数查询工单列表
     * @param map 请求参数
     * @return 返回查询结果
     */
    List<RepairOrderAdminListPojo> selectRepairOrderListByParam(@Param("map") Map<String, Object> map);

    /**
     * 查询工单数量
     * @return 返回工单数量
     */
    Long selectRepairOrderCount();

    /**
     * 根据虎趣工单id获取工单id
     * @param hqId 虎趣工单id
     * @return 返回工单id
     */
    Long getIdByHqId(@Param("hqId") Long hqId);
}