package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.AvdTag;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdListRelItemPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AvdTagMapper extends EasyBaseMapper<AvdTag> {

    /**
     * 根据AvdIds查询标签信息
     *
     * @param avdIds
     * @return
     */
    List<AvdListRelItemPojo> findAvdRelTagsByAvdIds(@Param("avdIds") List<Long> avdIds);

}
