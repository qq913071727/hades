package com.innovation.ic.cyz.base.mapper.pve_standard;

import com.innovation.ic.cyz.base.model.cyz.Brand;
import com.innovation.ic.cyz.base.model.pve_standard.Brands;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property.PsPropertyBrandPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface BrandsMapper extends EasyBaseMapper<Brands> {
    /**
     * 查询品牌信息
     */
    List<PsPropertyBrandPojo> findBrands(@Param("categoryId") String categoryId);

    /**
     * 查询Brands状态是正常的数据
     * @return 返回查询结果
     */
    List<Brand> selectNormalStatusData();
}