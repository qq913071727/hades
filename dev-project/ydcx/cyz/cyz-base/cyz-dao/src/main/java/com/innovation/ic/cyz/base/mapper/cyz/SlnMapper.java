package com.innovation.ic.cyz.base.mapper.cyz;

import com.innovation.ic.cyz.base.model.cyz.Sln;
import com.innovation.ic.cyz.base.mapper.EasyBaseMapper;
import com.innovation.ic.cyz.base.pojo.variable.cyz.SlnListPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SlnMapper extends EasyBaseMapper<Sln> {

    /**
     * 分页查询方案列表
     *
     * @param page
     * @param size
     * @return
     */
    List<SlnListPojo> findSlnList(@Param("page") Integer page,
                                  @Param("size") Integer size,
                                  @Param("orderByName") String orderByName,
                                  @Param("orderByType") String orderByType,
                                  @Param("hotNames") List<String> hotNames,
                                  @Param("cateNames") List<String> cateNames,
                                  @Param("typeDisplays") List<String> typeDisplays,
                                  @Param("ideCateNames") List<String> ideCateNames,
                                  @Param("keyWord") String keyWord,
                                  @Param("examineStatus") Integer examineStatus);

    /**
     * 查询方案数量
     *
     * @return
     */
    Long findSlnCount(@Param("hotNames") List<String> hotNames,
                      @Param("cateNames") List<String> cateNames,
                      @Param("typeDisplays") List<String> typeDisplays,
                      @Param("ideCateNames") List<String> ideCateNames,
                      @Param("keyWord") String keyWord);

    /**
     * 查询应用领域
     *
     * @return
     */
    List<String> findCateNames();

    /**
     * 查询方案类型
     *
     * @return
     */
    List<String> findTypeDisplays();


    /**
     * 分页查询方案列表
     * @param hotNames
     * @param cateNames
     * @param typeDisplays
     * @param ideCateNames
     * @param keyWord
     * @param examineStatus
     * @return
     */
    List<SlnListPojo> page(
            @Param("hotNames") List<String> hotNames,
            @Param("cateNames") List<String> cateNames,
            @Param("typeDisplays") List<String> typeDisplays,
            @Param("ideCateNames") List<String> ideCateNames,
            @Param("keyWord") String keyWord,
            @Param("examineStatus") Integer examineStatus);


    Long getMaxSlnId();

}
