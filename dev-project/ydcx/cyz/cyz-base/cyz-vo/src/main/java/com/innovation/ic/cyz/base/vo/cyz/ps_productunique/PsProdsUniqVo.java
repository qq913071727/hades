package com.innovation.ic.cyz.base.vo.cyz.ps_productunique;

import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_productsunique.PsProductPropertyPojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 商品型号查询vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PsProdsUniqVo", description = "商品型号查询vo类")
public class PsProdsUniqVo {
    @ApiModelProperty(value = "分类Id", dataType = "String")
    private String categoryId;

    @ApiModelProperty(value = "品牌Id", dataType = "String")
    private String[] brandId;

    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "封装", dataType = "String")
    private String[] packages;

    @ApiModelProperty(value = "包装", dataType = "String")
    private String[] packings;

    @ApiModelProperty(value = "属性", dataType = "List")
    private List<PsProductPropertyPojo> properties;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageIndex;

    @ApiModelProperty(value = "页大小", dataType = "Integer")
    private Integer pageSize;
}
