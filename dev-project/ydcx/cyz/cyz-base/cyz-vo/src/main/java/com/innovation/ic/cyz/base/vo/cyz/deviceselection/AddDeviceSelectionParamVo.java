package com.innovation.ic.cyz.base.vo.cyz.deviceselection;

import com.innovation.ic.cyz.base.model.pve_standard.Category;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 添加器件选型Vo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AddDeviceSelectionParamVo", description = "添加器件选型Vo")
public class AddDeviceSelectionParamVo {

    @ApiModelProperty(value = "根节点", dataType = "Category")
    public Category categoryRoot;


    /**
     * 子类ids
     */
    @ApiModelProperty(value = "子节点", dataType = "Category")
    public List<Category> categoryChild;


    /**
     * 子类ids
     */
    @ApiModelProperty(value = "选择参数集合", dataType = "List")
    public List<CategoriesPropertyVo> categoriesPropertys;





}
