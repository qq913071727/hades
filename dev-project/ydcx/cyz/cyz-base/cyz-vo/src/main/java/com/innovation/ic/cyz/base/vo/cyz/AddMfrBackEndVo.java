package com.innovation.ic.cyz.base.vo.cyz;

import com.innovation.ic.cyz.base.pojo.annotation.MfrParamCode;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrPcbOurProductPojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MfrBackEndVo", description = "PCB和SMT后端添加")
public class AddMfrBackEndVo {

    @ApiModelProperty(value = "主键id,删除时候携带", dataType = "Long")
    private Long mfrId;

    @ApiModelProperty(value = "类型:1-smt,2-pcb", dataType = "Integer")
    private Integer module;

    @ApiModelProperty(value = "公司名称", dataType = "String")
    @MfrParamCode(value = "company_name")
    private String companyName;


    @ApiModelProperty(value = "公司图片", dataType = "String")
    @MfrParamCode(value = "company_image")
    private String companyImage;


    @ApiModelProperty(value = "省", dataType = "String")
    @MfrParamCode(value = "province")
    private String province;


    @ApiModelProperty(value = "市", dataType = "String")
    @MfrParamCode(value = "city")
    private String city;

    @ApiModelProperty(value = "区", dataType = "String")
    @MfrParamCode(value = "area")
    private String area;


    @ApiModelProperty(value = "工厂特色", dataType = "String")
    @MfrParamCode(value = "factory_feature")
    private List<String> factoryFeature;

    @ApiModelProperty(value = "资质", dataType = "String")
    @MfrParamCode(value = "qualification")
    private List<String> qualification;


    @ApiModelProperty(value = "资质图片", dataType = "String")
    @MfrParamCode(value = "qualification_image")
    private List<String> qualificationImage;


    @ApiModelProperty(value = "工艺分类", dataType = "String")
    @MfrParamCode(value = "process_category")
    private List<String> processCategory;

    @ApiModelProperty(value = "品质特色", dataType = "String")
    @MfrParamCode(value = "quality_feature")
    private List<String> qualityFeature;


    @ApiModelProperty(value = "公司简介", dataType = "String")
    @MfrParamCode(value = "company_introduction")
    private String mfrIntroduction;

    @ApiModelProperty(value = "公司规模", dataType = "String")
    @MfrParamCode(value = "company_scale")
    private String mfrScale;

    @ApiModelProperty(value = "产品类别", dataType = "List")
    @MfrParamCode(value = "product_classification")
    private List<String> productClassification;

    @ApiModelProperty(value = "我们的产品", dataType = "List")
    @MfrParamCode(value = "our_products")
    private List<MfrPcbOurProductPojo> ourProducts;


}
