package com.innovation.ic.cyz.base.vo.cyz.deviceselection;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CategoriesPropertyVo", description = "分类对应属性表")
public class CategoriesPropertyVo {

    @ApiModelProperty(value = "唯一码", dataType = "String")
    private String id;

    @ApiModelProperty(value = "分类ID", dataType = "String")
    private String categoryId;

    @ApiModelProperty(value = "属性ID", dataType = "String")
    private String propertyId;

    @ApiModelProperty(value = "属性名称", dataType = "String")
    private String propertyName;

    @ApiModelProperty(value = "分组ID", dataType = "String")
    private String groupID;

    @ApiModelProperty(value = "值类型", dataType = "Integer")
    private Integer valueType;

    @ApiModelProperty(value = "单位", dataType = "Integer")
    private Integer unit;

    @ApiModelProperty(value = "是否扩展", dataType = "Boolean")
    private Boolean isExpand;

    @ApiModelProperty(value = "是否用于搜索", dataType = "Boolean")
    private Boolean isSearch;

    @ApiModelProperty(value = "是否必填", dataType = "Boolean")
    private Boolean isRequired;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String summary;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createDate;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    private Date modifyDate;

    @ApiModelProperty(value = "状态", dataType = "Integer")
    private Integer state;

    @ApiModelProperty(value = "状态", dataType = "Integer")
    private Integer type;


}
