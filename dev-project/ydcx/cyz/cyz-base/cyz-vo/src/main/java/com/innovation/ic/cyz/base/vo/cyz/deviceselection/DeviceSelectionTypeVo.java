package com.innovation.ic.cyz.base.vo.cyz.deviceselection;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 器件选型类型
 */

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DeviceSelectionType", description = "器件选型类型")
public class DeviceSelectionTypeVo {


    @ApiModelProperty(value = "主键", dataType = "Long")
    private String id;

    @ApiModelProperty(value = "类别名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String creatorId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createDate;

    @ApiModelProperty(value = "外部唯一id", dataType = "String")
    private String externalUniqueId;



}
