package com.innovation.ic.cyz.base.vo.cyz.deviceSelectionParameter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   参数模糊查询的vo类
 * @author linuo
 * @time   2022年10月24日10:15:18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DeviceSelectionParameterFuzzyQueryVo", description = "参数模糊查询的vo类")
public class DeviceSelectionParameterFuzzyQueryVo {
    @ApiModelProperty(value = "参数Id", dataType = "String")
    private String propertyId;

    @ApiModelProperty(value = "内容", dataType = "String")
    private String context;
}