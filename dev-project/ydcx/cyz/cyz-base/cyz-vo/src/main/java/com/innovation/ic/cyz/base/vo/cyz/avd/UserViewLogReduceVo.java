package com.innovation.ic.cyz.base.vo.cyz.avd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 减少用户浏览的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserViewLogReduceVo", description = "减少用户浏览的vo类")
public class UserViewLogReduceVo {

    @ApiModelProperty(value = "类型(1-避坑指南阅读, 2-避坑指南点赞, 3-避坑指南收藏)", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "业务Id", dataType = "Long")
    private Long businessId;

}
