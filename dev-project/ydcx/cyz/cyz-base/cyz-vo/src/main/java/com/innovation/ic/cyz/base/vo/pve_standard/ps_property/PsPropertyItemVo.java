package com.innovation.ic.cyz.base.vo.pve_standard.ps_property;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 器件选型属性Item的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PsPropertyItemVo", description = "器件选型属性Item的vo类")
public class PsPropertyItemVo {

    @ApiModelProperty(value = "属性Id", dataType = "String")
    private String propertyId;

    @ApiModelProperty(value = "属性值", dataType = "List")
    private List<String> valueText;

}
