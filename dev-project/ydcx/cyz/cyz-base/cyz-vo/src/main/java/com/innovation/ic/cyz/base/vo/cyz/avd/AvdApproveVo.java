package com.innovation.ic.cyz.base.vo.cyz.avd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 避坑指南审批的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AvdApproveVo", description = "避坑指南审批的vo类")
public class AvdApproveVo {

    @ApiModelProperty(value = "AvdId", dataType = "Long")
    private Long avdId;

}
