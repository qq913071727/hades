package com.innovation.ic.cyz.base.vo.cyz.video;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VideoAddVo", description = "视频保存类vo")
public class VideoAddVo {

    @ApiModelProperty(value = "id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "分类Ids", dataType = "List")
    private List<Long> categoryIds;

    @ApiModelProperty(value = "类型Ids", dataType = "List")
    private List<Long> typeIds;

    @ApiModelProperty(value = "标签Ids", dataType = "List")
    private List<Long> tagIds;

    @ApiModelProperty(value = "品牌Ids", dataType = "List")
    private List<Long> brandIds;

    @ApiModelProperty(value = "标题", dataType = "String")
    private String title;

    @ApiModelProperty(value = "封面相对url", dataType = "String")
    private String cover;

    @ApiModelProperty(value = "视频", dataType = "String")
    private String video;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String description;

    @ApiModelProperty(value = "正文", dataType = "String")
    private String paragraph;

    @ApiModelProperty(value = "是否草稿", dataType = "Boolean")
    private Boolean isDraft;
}
