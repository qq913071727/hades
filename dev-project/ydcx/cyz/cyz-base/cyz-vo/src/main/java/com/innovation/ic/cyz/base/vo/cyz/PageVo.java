package com.innovation.ic.cyz.base.vo.cyz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

@Getter
@ApiModel(value = "PageVo", description = "分页参数")
public abstract class PageVo {
    @ApiModelProperty(value = "当前页码", dataType = "Integer")
    private Integer page = 1;

    @ApiModelProperty(value = "每页条数", dataType = "Integer")
    private Integer size = 20;
}
