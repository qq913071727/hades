package com.innovation.ic.cyz.base.vo.cyz;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AddDeviceSelectionToConfigureVo", description = "添加器件选型类型")
public class AddDeviceSelectionToConfigureVo {

    private String deviceSelectionTypeId;

    private List<DeviceSelectionToConfigureVo> deviceSelectionToConfigureVo;

}
