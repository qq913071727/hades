package com.innovation.ic.cyz.base.vo.cyz;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 问题反馈的Vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "FeedbackVo", description = "问题反馈的Vo类")
public class FeedbackVo {

//    @ApiModelProperty(value = "当前页", dataType = "Integer")
//    private Integer pageNo;
//
//    @ApiModelProperty(value = "当前条数", dataType = "Integer")
//    private Integer pageSize;

    @ApiModelProperty(value = "问题类型", dataType = "Integer")
    private Integer feedbackType;

    @ApiModelProperty(value = "提交时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createDate;

    @ApiModelProperty(value = "用户id", dataType = "String")
    private String userId;

}
