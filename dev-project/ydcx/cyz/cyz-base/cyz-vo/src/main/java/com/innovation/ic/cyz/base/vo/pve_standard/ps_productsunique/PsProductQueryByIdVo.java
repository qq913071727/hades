package com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   根据id查询器件的vo类
 * @author linuo
 * @time   2022年10月24日16:35:48
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PsProductQueryByIdVo", description = "根据id查询器件的vo类")
public class PsProductQueryByIdVo {
    @ApiModelProperty(value = "id", dataType = "String")
    private String id;
}