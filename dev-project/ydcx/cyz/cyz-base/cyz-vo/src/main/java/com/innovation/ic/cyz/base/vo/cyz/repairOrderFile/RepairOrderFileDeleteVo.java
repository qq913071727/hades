package com.innovation.ic.cyz.base.vo.cyz.repairOrderFile;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   工单附件删除的vo类
 * @author linuo
 * @time   2022年10月12日13:16:24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderFileDeleteVo", description = "工单附件删除的vo类")
public class RepairOrderFileDeleteVo {
    @ApiModelProperty(value = "文件地址", dataType = "String")
    private String filePath;
}