package com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   型号模糊查询的vo类
 * @author linuo
 * @time   2022年10月25日14:07:07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PsPartNumbersQueryByContextVo", description = "型号模糊查询的vo类")
public class PsPartNumbersQueryByContextVo {
    @ApiModelProperty(value = "型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "查询类型(0:默认数据查询、1:完整数据查询)", dataType = "Integer")
    private Integer type;
}