package com.innovation.ic.cyz.base.vo.cyz;


import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 器件选型添加参数Vo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DeviceSelectionToConfigureVo", description = "器件选型添加参数Vo")
public class DeviceSelectionToConfigureVo {

    /**
     * 参数id
     */
    private String categoriesPropertyId;


    /**
     * 参数name
     */
    private String name;


    /**
     * 类型 1 快速筛选  参数筛选
     */
    private Integer type;


}
