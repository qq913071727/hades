package com.innovation.ic.cyz.base.vo.cyz.deviceselection;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 器件选型类型
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DeviceSelectionParameterVo", description = "器件选型类型包含参数")
public class DeviceSelectionParameterVo {


    @ApiModelProperty(value = "主键", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "类型 1、快速筛选 2、参数筛选", dataType = "Integer")
    private Integer type;


    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;


    @ApiModelProperty(value = "关联类型id", dataType = "Long")
    private Long deviceSelectionTypeId;


    @ApiModelProperty(value = "外部唯一id", dataType = "String")
    private String externalUniqueId;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createDate;


}
