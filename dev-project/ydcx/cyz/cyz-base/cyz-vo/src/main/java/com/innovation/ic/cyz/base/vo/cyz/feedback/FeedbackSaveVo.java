package com.innovation.ic.cyz.base.vo.cyz.feedback;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 信息反馈提交的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "FeedbackSaveVo", description = "信息反馈提交的vo类")
public class FeedbackSaveVo {

    @ApiModelProperty(value = "类型", dataType = "List")
    private List<Integer> types;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String text;

    @ApiModelProperty(value = "图片", dataType = "List")
    private List<String> images;

}
