package com.innovation.ic.cyz.base.vo.cyz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 在线询价提交的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InquiryMiniSaveVo", description = "在线询价提交的vo类")
public class InquiryMiniSaveVo {

    @ApiModelProperty(value = "MfrId", dataType = "Long")
    private Long mfrId;

    @ApiModelProperty(value = "公司名称", dataType = "String")
    private String companyName;

    @ApiModelProperty(value = "姓名", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String tel;

    @ApiModelProperty(value = "每页行数", dataType = "Integer")
    private Integer pageSize;

    @ApiModelProperty(value = "第几页", dataType = "Integer")
    private Integer pageNo;

}
