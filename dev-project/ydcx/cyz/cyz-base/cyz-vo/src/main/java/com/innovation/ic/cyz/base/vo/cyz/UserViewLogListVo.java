package com.innovation.ic.cyz.base.vo.cyz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 用户浏览/点赞/收藏记录列表的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserViewLogListVo", description = "用户浏览/点赞/收藏记录列表的vo类")
public class UserViewLogListVo {

    @ApiModelProperty(value = "当前页码", dataType = "Integer")
    private Integer page;

    @ApiModelProperty(value = "每页条数", dataType = "Integer")
    private Integer size;

    @ApiModelProperty(value = "类型(1-避坑指南阅读, 2-避坑指南点赞, 3-避坑指南收藏)", dataType = "List")
    private List<Integer> types;

    @ApiModelProperty(value = "内容的类型(1-文章, 2-视频)(0或null表示全部)", dataType = "Integer")
    private Integer contentModule;

}
