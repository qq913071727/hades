package com.innovation.ic.cyz.base.vo.cyz.video;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VidApproveVo", description = "视频审批Vo类")
public class VidApproveVo {

    /**
     * 视频Id
     */
    @ApiModelProperty(value = "视频Id", dataType = "Long")
    private Long vid;
}
