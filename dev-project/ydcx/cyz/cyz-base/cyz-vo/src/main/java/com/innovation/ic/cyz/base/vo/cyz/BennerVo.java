package com.innovation.ic.cyz.base.vo.cyz;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;


/**
 * 首页bannerd的Vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "BennerVo", description = "首页bannerd的Vo类")
public class BennerVo {

    @ApiModelProperty(value = "主键", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "图片url", dataType = "String")
    private String picturePath;

    @ApiModelProperty(value = "尺寸", dataType = "String")
    private String size;

    @ApiModelProperty(value = "是否启用。1表示启用，0表示未启用", dataType = "Integer")
    private Integer available;

//    @ApiModelProperty(value = "二级页面路径", dataType = "String")
//    private String secondaryPagePath;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String creatorId;

    @ApiModelProperty(value = "修改人id", dataType = "String")
    private String modifierId;

//    @ApiModelProperty(value = "文件", dataType = "String")
//    private MultipartFile file;

    @ApiModelProperty(value = "当前页", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "当前条数", dataType = "Integer")
    private Integer pageSize;
}
