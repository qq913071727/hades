package com.innovation.ic.cyz.base.vo.cyz;

import com.innovation.ic.cyz.base.pojo.annotation.MfrParamCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Smt厂家列表的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MfrSmtListVo", description = "Smt厂家列表的vo类")
public class MfrSmtListVo {

    @ApiModelProperty(value = "当前页码", dataType = "Integer")
    private Integer page;

    @ApiModelProperty(value = "每页条数", dataType = "Integer")
    private Integer size;

    @ApiModelProperty(value = "所在城市Ids", dataType = "List")
    @MfrParamCode(value = "region")
    private List<Long> regionIds;

    @ApiModelProperty(value = "工厂特色Ids", dataType = "List")
    private List<Long> factoryFeatureIds;

    @ApiModelProperty(value = "资质Ids", dataType = "List")
    private List<Long> qualificationIds;

    @ApiModelProperty(value = "工艺分类Ids", dataType = "List")
    private List<Long> technologyClassificationIds;

    @ApiModelProperty(value = "品质特色Ids", dataType = "List")
    private List<Long> qualityFeatureIds;

    @ApiModelProperty(value = "厂家名称", dataType = "String")
    @MfrParamCode(value = "company_name")
    private String mfrName;

}
