package com.innovation.ic.cyz.base.vo.cyz;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 用户中心的Vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserCenterVo", description = "用户中心的Vo类")
public class UserCenterVo {

    @ApiModelProperty(value = "用户id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "文章id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "当前页", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "当前条数", dataType = "Integer")
    private Integer pageSize;

    @ApiModelProperty(value = "来源", dataType = "Integer")
    private Integer source;

    @ApiModelProperty(value = "内容形式", dataType = "Integer")
    private Integer module;

    @ApiModelProperty(value = "发布时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createDate;

    @ApiModelProperty(value = "所属类别id", dataType = "Integer")
    private Integer nameId;

    @ApiModelProperty(value = "审批状态(1-草稿, 2-待审批, 3-审批通过, 4-审批不通过, 5-删除)", dataType = "Integer")
    private Integer approveStatus;

    @ApiModelProperty(value = "用户中心选项(1浏览2点赞3收藏4投稿)", dataType = "Integer")
    private Integer operation;

}
