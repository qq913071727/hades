package com.innovation.ic.cyz.base.vo.cyz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户登录的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserLoginVo", description = "用户登录的vo类")
public class UserLoginVo {

    @ApiModelProperty(value = "用户名", dataType = "String")
    private String username;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;

}
