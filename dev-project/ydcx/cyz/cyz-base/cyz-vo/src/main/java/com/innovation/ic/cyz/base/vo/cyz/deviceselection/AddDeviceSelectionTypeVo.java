package com.innovation.ic.cyz.base.vo.cyz.deviceselection;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 添加器件选型类型Vo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AddDeviceSelectionTypeVo", description = "添加器件选型Vo")
public class AddDeviceSelectionTypeVo {




    @ApiModelProperty(value = "主键id", dataType = "String")
    public String id;

    @ApiModelProperty(value = "主键id", dataType = "String")
    public String parentId;


    @ApiModelProperty(value = "分类名称", dataType = "String")
    public String name;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    public String creatorId;

    @ApiModelProperty(value = "子类", dataType = "AddDeviceSelectionTypeVo")
    public AddDeviceSelectionTypeVo childId;







}
