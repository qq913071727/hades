package com.innovation.ic.cyz.base.vo.cyz.RabbitMq;

import com.innovation.ic.cyz.base.vo.cyz.repairOrder.FilesVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @desc   虎趣 -> cyz回复工单的vo类
 * @author linuo
 * @time   2022年11月21日13:47:47
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "HqToCyzRepairOrderReplyVo", description = "虎趣 -> cyz回复工单的vo类")
public class HqToCyzRepairOrderReplyVo {
    @ApiModelProperty(value = "虎趣工单id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String describe;

    @ApiModelProperty(value = "回复人", dataType = "String")
    private String replyUser;

    @ApiModelProperty(value = "附件地址集合", dataType = "List")
    private List<FilesVo> fileUrls;
}