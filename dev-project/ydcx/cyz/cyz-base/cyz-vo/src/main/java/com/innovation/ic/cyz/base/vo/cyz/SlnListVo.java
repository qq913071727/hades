package com.innovation.ic.cyz.base.vo.cyz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 方案列表的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SlnListVo", description = "方案列表的vo类")
public class SlnListVo {

    @ApiModelProperty(value = "当前页码", dataType = "Integer")
    private Integer page;

    @ApiModelProperty(value = "每页条数", dataType = "Integer")
    private Integer size;

    @ApiModelProperty(value = "来源 1比一比 2ICGOO 3虎趣 4IC交易网", dataType = "Integer")
    private Integer source;

    @ApiModelProperty(value = "热门领域", dataType = "List")
    private List<String> hotNames;

    @ApiModelProperty(value = "应用领域", dataType = "List")
    private List<String> cateNames;

    @ApiModelProperty(value = "方案分类", dataType = "List")
    private List<String> typeDisplays;

    @ApiModelProperty(value = "开发平台", dataType = "List")
    private List<String> ideCateNames;

    @ApiModelProperty(value = "搜索关键字", dataType = "String")
    private String keyWord;

    @ApiModelProperty(value = "BIB查询方案-开始时间", dataType = "String")
    private String startTime;

    @ApiModelProperty(value = "BIB查询方案-结束时间", dataType = "String")
    private String endTime;

    @ApiModelProperty(value = "方案审核状态：1待审核 2审核通过 3审核不通过", dataType = "Integer")
    private Integer examineStatus;

    @ApiModelProperty(value = "排序字段", dataType = "String")
    private String orderByName;

    @ApiModelProperty(value = "搜索关键字", dataType = "String")
    public String orderByType;

}
