package com.innovation.ic.cyz.base.vo.cyz.RabbitMq;

import com.innovation.ic.cyz.base.vo.cyz.repairOrder.FilesVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @desc   虎趣 -> cyz创建工单的vo类
 * @author linuo
 * @time   2022年11月21日09:55:18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "HqToCyzRepairOrderAddVo", description = "虎趣 -> cyz创建工单的vo类")
public class HqToCyzRepairOrderAddVo {
    @ApiModelProperty(value = "虎趣工单id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "主题", dataType = "String")
    private String topic;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String describe;

    @ApiModelProperty(value = "优先级(1：普通、2：低、3：高、4：紧急、5：立刻)", dataType = "Integer")
    private Integer priority;

    @ApiModelProperty(value = "工单类型id", dataType = "Long")
    private Long typeId;

    @ApiModelProperty(value = "创建人", dataType = "String")
    private String createUser;

    @ApiModelProperty(value = "附件地址集合", dataType = "List")
    private List<FilesVo> fileUrls;
}