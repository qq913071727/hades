package com.innovation.ic.cyz.base.vo.cyz.repairOrderTypeHead;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   工单分类负责人信息的vo类
 * @author linuo
 * @time   2022年9月15日14:03:22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderTypeHeadVo", description = "工单分类负责人信息的vo类")
public class RepairOrderTypeHeadVo {
    @ApiModelProperty(value = "用户", dataType = "String")
    private String user;

    @ApiModelProperty(value = "用户名称", dataType = "String")
    private String userName;
}