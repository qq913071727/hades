package com.innovation.ic.cyz.base.vo.cyz.video;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "VidListVo", description = "列表查询参数")
public class VidListVo {
    @ApiModelProperty(value = "标题", dataType = "String")
    private String title;

    @ApiModelProperty(value = "标签", dataType = "String")
    private String tag;

    @ApiModelProperty(value = "类别", dataType = "List")
    private List<String> categories;

    @ApiModelProperty(value = "品牌", dataType = "List")
    private List<String> brands;

    @ApiModelProperty(value = "内容类型", dataType = "List")
    private List<String> types;

    @ApiModelProperty(value = "排序字段", dataType = "String")
    private String orderByName;

    @ApiModelProperty(value = "排序类型", dataType = "String")
    private String orderByType;
}

