package com.innovation.ic.cyz.base.vo.pve_standard.ps_property;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 器件选型属性的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PsPropertyVo", description = "器件选型属性的vo类")
public class PsPropertyVo {
    @ApiModelProperty(value = "分类Id", dataType = "String")
    private String categoryId;

//    @ApiModelProperty(value = "已选值", dataType = "List")
//    private List<PsPropertyItemVo> selectedValues;
}