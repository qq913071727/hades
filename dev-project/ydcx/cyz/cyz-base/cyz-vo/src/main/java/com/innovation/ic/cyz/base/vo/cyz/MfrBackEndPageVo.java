package com.innovation.ic.cyz.base.vo.cyz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MfrBackEndPageVo", description = "查询SMT和PCB分页")
public class MfrBackEndPageVo {


    @ApiModelProperty(value = "类型:1-smt,2-pcb", dataType = "Integer")
    private Integer module;



    @ApiModelProperty(value = "当前页数", dataType = "Integer")
    private Integer pageNum;


    @ApiModelProperty(value = "显示多少", dataType = "Integer")
    private Integer pageSize;

    @ApiModelProperty(value = "开始时间 2022-10-09 10:00:00", dataType = "String")
    private String startTime;

    @ApiModelProperty(value = "结束时间 2022-10-10 10:00:00", dataType = "String")
    private String endTime;




}
