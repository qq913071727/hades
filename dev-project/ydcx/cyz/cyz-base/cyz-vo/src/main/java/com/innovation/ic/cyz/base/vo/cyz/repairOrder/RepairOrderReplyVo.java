package com.innovation.ic.cyz.base.vo.cyz.repairOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @desc   工单回复的vo类
 * @author linuo
 * @time   2022年9月14日14:51:51
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderReplyVo", description = "工单回复的vo类")
public class RepairOrderReplyVo {
    @ApiModelProperty(value = "工单id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String describe;

    @ApiModelProperty(value = "附件地址集合", dataType = "List")
    private List<FilesVo> fileUrls;
}