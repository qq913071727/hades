package com.innovation.ic.cyz.base.vo.cyz.repairOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   工单列表查询的vo类
 * @author linuo
 * @time   2022年9月15日10:07:01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderQueryListVo", description = "工单列表查询的vo类")
public class RepairOrderQueryListVo {
    @ApiModelProperty(value = "类别", dataType = "Integer")
    private Integer firstTypeId;

    @ApiModelProperty(value = "序列号", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "优先级(1：普通、2：低、3：高、4：紧急、5：立刻)", dataType = "Integer")
    private Integer priority;

    @ApiModelProperty(value = "负责人", dataType = "String")
    private String headUser;

    @ApiModelProperty(value = "指派给", dataType = "String")
    private String dealUser;

    @ApiModelProperty(value = "创建日期", dataType = "String")
    private String createDate;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;
}