package com.innovation.ic.cyz.base.vo.cyz.repairOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   附件的vo类
 * @author linuo
 * @time   2022年9月14日14:17:41
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "FilesVo", description = "附件的vo类")
public class FilesVo {
    @ApiModelProperty(value = "文件类型(1：文本、2：图片)", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "附件地址", dataType = "String")
    private String url;

    @ApiModelProperty(value = "附件名称", dataType = "String")
    private String fileName;

    @ApiModelProperty(value = "附件大小", dataType = "String")
    private String fileSize;
}