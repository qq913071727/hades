package com.innovation.ic.cyz.base.vo.cyz.repairOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @desc   工单新增的vo类
 * @author linuo
 * @time   2022年9月14日09:23:43
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AvdApproveVo", description = "工单新增的vo类")
public class RepairOrderAddVo {
    @ApiModelProperty(value = "主题", dataType = "String")
    private String topic;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String describe;

    @ApiModelProperty(value = "优先级(1：普通、2：低、3：高、4：紧急、5：立刻)", dataType = "Integer")
    private Integer priority;

    @ApiModelProperty(value = "工单类型id", dataType = "Long")
    private Long typeId;

    @ApiModelProperty(value = "附件地址集合", dataType = "List")
    private List<FilesVo> fileUrls;
}