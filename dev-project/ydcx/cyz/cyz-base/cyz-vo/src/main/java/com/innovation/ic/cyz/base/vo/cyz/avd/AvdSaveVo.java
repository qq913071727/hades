package com.innovation.ic.cyz.base.vo.cyz.avd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 避坑指南保存的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AvdSaveVo", description = "避坑指南保存的vo类")
public class AvdSaveVo {

    @ApiModelProperty(value = "主键", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "是否草稿", dataType = "Boolean")
    private Boolean isDraft;

    @ApiModelProperty(value = "类型(1-文章, 2-视频)", dataType = "Integer")
    private Integer module;

    @ApiModelProperty(value = "标题", dataType = "String")
    private String title;

    @ApiModelProperty(value = "分类Ids", dataType = "List")
    private List<Long> typeIds;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String description;

    @ApiModelProperty(value = "标签Ids", dataType = "List")
    private List<Long> tagIds;

    @ApiModelProperty(value = "品牌Ids", dataType = "List")
    private List<Long> brandIds;

    @ApiModelProperty(value = "正文", dataType = "String")
    private String paragraph;

    @ApiModelProperty(value = "封面相对url", dataType = "String")
    private String cover;

    @ApiModelProperty(value = "视频", dataType = "String")
    private String video;

}
