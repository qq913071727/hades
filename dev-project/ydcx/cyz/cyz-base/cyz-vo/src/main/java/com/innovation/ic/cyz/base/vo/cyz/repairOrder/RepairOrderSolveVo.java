package com.innovation.ic.cyz.base.vo.cyz.repairOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @desc   工单解决的vo类
 * @author linuo
 * @time   2022年11月11日14:46:00
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderSolveVo", description = "工单解决的vo类")
public class RepairOrderSolveVo {
    @ApiModelProperty(value = "工单id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "解决方案(2解决、3延期处理、4关闭)", dataType = "Long")
    private Integer solvePlan;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String summary;

    @ApiModelProperty(value = "附件地址集合", dataType = "List")
    private List<FilesVo> fileUrls;
}