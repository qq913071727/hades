package com.innovation.ic.cyz.base.vo.cyz.repairOrderType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   工单分类编辑的vo类
 * @author linuo
 * @time   2022年9月27日15:00:49
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderTypeEditVo", description = "工单分类编辑的vo类")
public class RepairOrderTypeEditVo {
    @ApiModelProperty(value = "一级分类id", dataType = "String")
    private Long id;

    @ApiModelProperty(value = "一级分类负责人", dataType = "String")
    private String firstClassHead;

    @ApiModelProperty(value = "二级分类名称", dataType = "String")
    private String secondClassName;

    @ApiModelProperty(value = "二级分类负责人", dataType = "String")
    private String secondClassHead;

    @ApiModelProperty(value = "三级分类名称", dataType = "String")
    private String thirdClassName;

    @ApiModelProperty(value = "三级分类负责人", dataType = "String")
    private String thirdClassHead;
}