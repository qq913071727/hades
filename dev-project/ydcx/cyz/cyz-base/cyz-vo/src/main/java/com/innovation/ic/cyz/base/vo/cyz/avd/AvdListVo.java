package com.innovation.ic.cyz.base.vo.cyz.avd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 避坑指南的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AvdListVo", description = "避坑指南的vo类")
public class AvdListVo {

    @ApiModelProperty(value = "是否是公共的", dataType = "Boolean")
    private Boolean isPublic;

    // 个人的查询条件 Begin

    @ApiModelProperty(value = "是否是个人的", dataType = "Boolean")
    private Boolean isPrivate;

    @ApiModelProperty(value = "审批状态s", dataType = "List")
    private List<Integer> approveStatuses;

    // 个人的查询条件 End

    @ApiModelProperty(value = "当前页码", dataType = "Integer")
    private Integer page;

    @ApiModelProperty(value = "每页条数", dataType = "Integer")
    private Integer size;

    @ApiModelProperty(value = "标题", dataType = "String")
    private String title;

    @ApiModelProperty(value = "类型(1-文章, 2-视频)(0或null表示全部)", dataType = "Integer")
    private Integer module;

    @ApiModelProperty(value = "分类Ids", dataType = "List")
    private List<Long> typeIds;

}
