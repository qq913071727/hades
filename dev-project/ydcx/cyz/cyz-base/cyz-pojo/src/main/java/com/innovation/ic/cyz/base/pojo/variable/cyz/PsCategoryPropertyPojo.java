package com.innovation.ic.cyz.base.pojo.variable.cyz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PsCategoryPropertyPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "唯一码", dataType = "String")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "分类ID", dataType = "String")
    @TableField(value = "category_id")
    private String categoryId;

    @ApiModelProperty(value = "属性ID", dataType = "String")
    @TableField(value = "property_id")
    private String propertyId;

    @ApiModelProperty(value = "属性名称", dataType = "String")
    @TableField(value = "property_name")
    private String propertyName;

    @ApiModelProperty(value = "分组ID", dataType = "String")
    @TableField(value = "group_id")
    private String groupID;

    @ApiModelProperty(value = "是否用于搜索", dataType = "Boolean")
    @TableField(value = "is_search")
    private Boolean isSearch;


    @ApiModelProperty(value = "是否展示,true展示 ", dataType = "boolean")
    @TableField(exist = false)
    private boolean exhibition;

    @TableField(exist = false)
    @ApiModelProperty(value = "类型 1 快速筛选  2参数筛选", dataType = "boolean")
    private Integer type;
}
