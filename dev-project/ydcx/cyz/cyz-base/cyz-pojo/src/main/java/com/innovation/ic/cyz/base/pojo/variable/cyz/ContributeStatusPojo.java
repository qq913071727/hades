package com.innovation.ic.cyz.base.pojo.variable.cyz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * 我的投稿状态的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ContributeStatusPojo", description = "我的投稿状态的pojo类")
public class ContributeStatusPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "标签名", dataType = "String")
    private String name;

    @ApiModelProperty(value = "包含状态", dataType = "List")
    private List<Integer> statuses;
}