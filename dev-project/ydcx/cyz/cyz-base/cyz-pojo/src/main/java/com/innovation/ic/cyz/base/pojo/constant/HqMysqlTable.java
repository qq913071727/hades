package com.innovation.ic.cyz.base.pojo.constant;

/**
 * 虎趣分支获取数据走mysql数据源表面
 */
public class HqMysqlTable {

    /**
     * product表
     */
    public static final String TABLE_PRODUCT = "product";

    /**
     * product_property表
     */
    public static final String TABLE_PRODUCT_PROPERTY = "product_property";

    /**
     * product_unique表
     */
    public static final String TABLE_PRODUCT_UNIQUE = "product_unique";
}
