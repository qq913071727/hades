package com.innovation.ic.cyz.base.pojo.constant;

/**
 * 上传ftpBennner数据
 */
public class FtpModule {

    /**
     * 区分功能（上传）
     */
    public static final String FTP_ADD = "1";

    /**
     * 区分功能（修改）
     */
    public static final String FTP_UPDATE = "2";

    /**
     * 上传前缀名
     */
    public static final String FTP_NAME = "banner";
        }
