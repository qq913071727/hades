package com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_parameter;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * 器件选型筛选条件属性的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DeviceSelectionParamPojo", description = "器件选型筛选条件参数的pojo类")
public class DeviceSelectionParamPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "属性Id", dataType = "String")
    @TableField(value = "propertyId")
    private String propertyId;

    @ApiModelProperty(value = "属性名称", dataType = "String")
    @TableField(value = "propertyName")
    private String propertyName;

    @ApiModelProperty(value = "属性值", dataType = "List")
    private List<String> valueText;
}