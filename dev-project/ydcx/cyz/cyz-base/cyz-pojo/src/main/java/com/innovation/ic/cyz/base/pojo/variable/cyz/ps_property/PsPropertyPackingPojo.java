package com.innovation.ic.cyz.base.pojo.variable.cyz.ps_property;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 器件选型筛选条件包装的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PsPropertyPackingPojo", description = "器件选型筛选条件包装的pojo类")
public class PsPropertyPackingPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "包装名", dataType = "String")
    @TableField(value = "packingName")
    private String packingName;

}
