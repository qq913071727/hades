package com.innovation.ic.cyz.base.pojo.variable.cyz.sln_import;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DownloadSlnFileReturnPojo", description = "下载方案文件的pojo类")
public class DownloadSlnFileReturnPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "下载是否成功", dataType = "Boolean")
    private Boolean downloadSuccess;

    @ApiModelProperty(value = "新的相对url", dataType = "String")
    private String newRelativeUrl;

    @ApiModelProperty(value = "新的绝对url", dataType = "String")
    private String newAbsoluteUrl;

    @ApiModelProperty(value = "错误信息", dataType = "String")
    private String errorMsg;

}
