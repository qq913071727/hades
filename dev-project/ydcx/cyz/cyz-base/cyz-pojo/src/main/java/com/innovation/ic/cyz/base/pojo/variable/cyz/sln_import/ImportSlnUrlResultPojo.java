package com.innovation.ic.cyz.base.pojo.variable.cyz.sln_import;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ImportSlnUrlResultPojo", description = "导入方案信息获取Url结果")
public class ImportSlnUrlResultPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "今天是否执行完成", dataType = "Boolean")
    private Boolean isTodayFinish;

    @ApiModelProperty(value = "下一个url", dataType = "String")
    private String nextUrl;

}
