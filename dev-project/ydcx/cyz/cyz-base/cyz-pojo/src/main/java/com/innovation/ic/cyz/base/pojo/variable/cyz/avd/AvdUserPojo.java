package com.innovation.ic.cyz.base.pojo.variable.cyz.avd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 避坑指南列表中作者信息的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AvdUserPojo", description = "避坑指南列表中作者信息的pojo类")
public class AvdUserPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    private String creatorId;

    @ApiModelProperty(value = "创建人名称", dataType = "String")
    private String creatorName;

    @ApiModelProperty(value = "创建人头像", dataType = "String")
    private String creatorAvatar;

}
