package com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PsCategoryPropertyPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "唯一码", dataType = "String")
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "分类ID", dataType = "String")
    @TableField(value = "CategoryID")
    private String categoryId;

    @ApiModelProperty(value = "属性ID", dataType = "String")
    @TableField(value = "PropertyID")
    private String propertyId;

    @ApiModelProperty(value = "属性名称", dataType = "String")
    @TableField(value = "PropertyName")
    private String propertyName;

    @ApiModelProperty(value = "分组ID", dataType = "String")
    @TableField(value = "GroupID")
    private String groupID;

    @ApiModelProperty(value = "是否用于搜索", dataType = "Boolean")
    @TableField(value = "IsSearch")
    private Boolean isSearch;


    @ApiModelProperty(value = "是否展示,true展示 ", dataType = "boolean")
    @TableField(exist = false)
    private boolean exhibition;

    @TableField(exist = false)
    @ApiModelProperty(value = "类型 1 快速筛选  2参数筛选", dataType = "boolean")
    private Integer type;


}
