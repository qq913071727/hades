package com.innovation.ic.cyz.base.pojo.variable.cyz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 方案详情的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SlnDetailPojo", description = "方案详情的pojo类")
public class SlnDetailPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "SlnId", dataType = "Long")
    private Long slnId;

    @ApiModelProperty(value = "方案封面", dataType = "String")
    private String goodsCover;

    @ApiModelProperty(value = "其他封面", dataType = "List")
    private List<String> otherCover;

    @ApiModelProperty(value = "方案文档", dataType = "List")
    private List<String> filePath;

    @ApiModelProperty(value = "方案名称", dataType = "String")
    private String title;

    @ApiModelProperty(value = "方案详情", dataType = "String")
    private String description;

    @ApiModelProperty(value = "应用领域", dataType = "String")
    private String cateName;

    @ApiModelProperty(value = "开发平台", dataType = "String")
    private String ideCateName;

    @ApiModelProperty(value = "交付形式", dataType = "String")
    private String deliveryCateIdName;

    @ApiModelProperty(value = "性能参数", dataType = "String")
    private String performanceParameter;

    @ApiModelProperty(value = "应用场景", dataType = "String")
    private String applicationScene;

    @ApiModelProperty(value = "方案类型", dataType = "String")
    private String typeDisplay;

    @ApiModelProperty(value = "查看次数", dataType = "String")
    private Integer viewNum;

    @ApiModelProperty(value = "方案标签", dataType = "List")
    private List<String> likeTags;
}
