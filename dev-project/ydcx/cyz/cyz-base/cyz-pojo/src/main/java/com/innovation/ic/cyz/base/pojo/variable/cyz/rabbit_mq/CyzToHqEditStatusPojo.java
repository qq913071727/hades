package com.innovation.ic.cyz.base.pojo.variable.cyz.rabbit_mq;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @desc   cyz -> 虎趣修改工单状态的pojo类
 * @author linuo
 * @time   2022年11月21日15:48:23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CyzToHqEditStatusPojo", description = "cyz -> 虎趣修改工单状态的pojo类")
public class CyzToHqEditStatusPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "虎趣工单id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "工单状态(0:待解决、1:已回复、2:已解决、3:延期处理、4:已关闭)", dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "处理人", dataType = "String")
    private String dealUser;

    @ApiModelProperty(value = "处理人姓名", dataType = "String")
    private String dealUsername;

    @ApiModelProperty(value = "处理时间", dataType = "String")
    private String dealTime;

    @ApiModelProperty(value = "流程记录", dataType = "String")
    private String record;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String describe;

    @ApiModelProperty(value = "附件地址集合", dataType = "List")
    private List<FilesPojo> fileUrls;
}