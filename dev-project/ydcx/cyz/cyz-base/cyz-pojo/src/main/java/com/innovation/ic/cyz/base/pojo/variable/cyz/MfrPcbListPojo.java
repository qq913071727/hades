package com.innovation.ic.cyz.base.pojo.variable.cyz;

import com.innovation.ic.cyz.base.pojo.annotation.MfrParamCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * PCB厂家列表的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MfrListPojo", description = "PCB厂家列表的pojo类")
public class MfrPcbListPojo implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "MfrId", dataType = "Long")
    private Long mfrId;

    @ApiModelProperty(value = "厂家名称", dataType = "String")
    @MfrParamCode(value = "company_name")
    private String mfrName;

    @ApiModelProperty(value = "厂家图片", dataType = "String")
    @MfrParamCode(value = "company_image")
    private String mfrImg;
}
