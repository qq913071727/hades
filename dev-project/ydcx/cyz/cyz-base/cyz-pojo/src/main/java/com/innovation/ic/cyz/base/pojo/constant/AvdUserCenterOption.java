package com.innovation.ic.cyz.base.pojo.constant;

/**
 * 避坑指南用户中心选项
 */
public class AvdUserCenterOption {

    /**
     * 我的浏览
     */
    public static final int MyView = 1;

    /**
     * 我的点赞
     */
    public static final int MyLike = 2;

    /**
     * 我的收藏
     */
    public static final int MyCollect = 3;

    /**
     * 我的投稿
     */
    public static final int MyWrite = 4;

    /**
     * 操作删除
     */
    public static final int Delete = 4;

}
