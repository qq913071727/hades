package com.innovation.ic.cyz.base.pojo.enums;


/**
 * @ClassName ExamineStatusEnum
 * @Description 状态枚举类
 * @Date 2022/9/26
 * @Author myq
 */
public enum ExamineStatusEnum {

    PENDING_REVIEW(1,"待审核"),
    EXAMINATION_PASSED(2,"审核通过"),
    AUDIT_NOT_PASSED(3,"审核不通过");


    private Integer key;
    private String val;


    ExamineStatusEnum(Integer key, String val) {
        this.val = val;
        this.key = key;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
