package com.innovation.ic.cyz.base.pojo.enums;


/**
 * 存储redis结构的前缀
 */
public enum RedisKeyPrefixEnum {

    TABLE("table", "redis存储表前缀"),
    TABLE_AVD("avd", "avd表redis前缀"),
    TABLE_AVD_BRAND("avd_brand", "avd_brand表redis前缀"),
    TABLE_AVD_REL("avd_rel", "avd_brand表redis前缀"),
    TABLE_AVD_TAG("avd_tag", "avd_brand表redis前缀"),
    TABLE_AVD_TYPE("avd_type", "avd_brand表redis前缀"),
    TABLE_BRAND("brand", "器件选型-标准库同步mysql,brand表redis前缀"),
    TABLE_CATEGORIES("categories", "器件选型-标准库同步mysql,categories表redis前缀"),
    TABLE_CATEGORIES_PROPERTY("categories_property", "器件选型-标准库同步mysql,categories_property表redis前缀"),
    TABLE_DEVICE_SELECTION("device_selection", "器件选型,device_selection表redis前缀"),
    TABLE_DEVICE_SELECTION_PARAMETER("device_selection_parameter", "器件选型,device_selection_parameter表redis前缀"),
    TABLE_DEVICE_SELECTION_TYPE("device_selection_type", "器件选型,device_selection_type表redis前缀"),
    TABLE_PRODUCT("product", "器件选型-标准库同步mysql和redis,product表redis前缀"),
    TABLE_PRODUCT_UNIQUE("product_unique", "器件选型-标准库同步mysql和redis,product_unique表redis前缀"),
    TABLE_PRODUCT_PROPERTY("product_property", "器件选型-标准库同步mysql和redis,products_property表redis前缀"),
    TABLE_SLN_IDE("sln_ide", "sln_ide表redis前缀"),
    TABLE_SLN_LIKE_TAG("sln_like_tag", "sln_like_tag表redis前缀"),
    TABLE_SLN_URL("sln_url", "sln表redis前缀"),
    TABLE_SLN("sln", "sln表redis前缀"),
    TABLE_VIDEO("video", "video表redis前缀"),
    TABLE_VID_BRAND("vid_brand", "vid_brand表redis前缀"),
    TABLE_VID_CATEGORY("vid_category", "vid_category表redis前缀"),
    TABLE_VID_REL("vid_rel", "vid_rel表redis前缀"),
    TABLE_VID_TAG("vid_tag", "vid_tag表redis前缀"),
    TABLE_VID_TYPE("vid_type", "vid_type表redis前缀"),
    ;


    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    RedisKeyPrefixEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }


}
