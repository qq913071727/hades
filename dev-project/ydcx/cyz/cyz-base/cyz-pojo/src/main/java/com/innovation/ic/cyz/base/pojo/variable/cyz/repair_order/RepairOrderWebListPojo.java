package com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @time   查询我的工单列表接口的Vo类
 * @author linuo
 * @time   2022年9月29日16:24:14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderWebListPojo", description = "工单信息查询的返回Vo类")
public class RepairOrderWebListPojo {
    @ApiModelProperty(value = "主键id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "优先级", dataType = "String")
    private String priorityName;

    @ApiModelProperty(value = "主题", dataType = "String")
    private String topic;

    @ApiModelProperty(value = "创建时间", dataType = "String")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createTime;
}