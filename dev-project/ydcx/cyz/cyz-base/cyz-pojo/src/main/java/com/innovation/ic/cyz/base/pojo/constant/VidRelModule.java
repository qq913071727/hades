package com.innovation.ic.cyz.base.pojo.constant;

/**
 * 视频相关数据类型
 */
public class VidRelModule {

    /**
     * 分类
     */
    public static final int Category = 1;

    /**
     * 标签
     */
    public static final int Tag = 2;

    /**
     * 品牌
     */
    public static final int Brand = 3;

    /**
     * 内容类型
     */
    public static final int Type = 4;


}
