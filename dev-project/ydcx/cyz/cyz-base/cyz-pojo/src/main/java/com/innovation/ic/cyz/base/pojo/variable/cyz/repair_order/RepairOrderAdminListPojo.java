package com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @time   后台查询工单列表接口的返回Vo类
 * @author linuo
 * @time   2022年9月29日17:50:49
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderAdminListPojo", description = "工单列表查询的Vo类")
public class RepairOrderAdminListPojo {
    @ApiModelProperty(value = "序列号", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "负责人", dataType = "String")
    private String headUsername;

    @ApiModelProperty(value = "类别", dataType = "String")
    private String firstTypeName;

    @ApiModelProperty(value = "优先级", dataType = "String")
    private String priority;

    @ApiModelProperty(value = "状态(0:待解决、1:已回复、2:已解决、3:延期处理、4:已关闭)", dataType = "String")
    private String status;

    @ApiModelProperty(value = "主题", dataType = "String")
    private String topic;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "处理人姓名", dataType = "String")
    private String dealUsername;

    @ApiModelProperty(value = "更新时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date updateTime;

    @ApiModelProperty(value = "是否未处理(0:否、1:是)", dataType = "Integer")
    private Integer ifUntreated;
}