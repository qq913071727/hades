package com.innovation.ic.cyz.base.pojo.variable.tenant_import;

import java.util.List;

public class TenantImport {

    private int code;
    private String msg;
    private int total;
    private List<TenantImportData> data;
//    private Black black;
    private List<TenantImportBrand> brand;

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public void setData(List<TenantImportData> data) {
        this.data = data;
    }

    public List<TenantImportData> getData() {
        return data;
    }

//    public void setBlack(Black black) {
//        this.black = black;
//    }
//
//    public Black getBlack() {
//        return black;
//    }

    public void setBrand(List<TenantImportBrand> brand) {
        this.brand = brand;
    }

    public List<TenantImportBrand> getBrand() {
        return brand;
    }

}