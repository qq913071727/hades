package com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type_head;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   根据工单类型查询FAE列表返回的pojo类
 * @author linuo
 * @time   2022年11月11日16:34:41
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderTypeFaeListRespPojo", description = "工单分类列表返回的pojo类")
public class RepairOrderTypeFaeListRespPojo {
    @ApiModelProperty(value = "用户id", dataType = "Long")
    private Long userId;

    @ApiModelProperty(value = "用户名称", dataType = "String")
    private String userName;
}