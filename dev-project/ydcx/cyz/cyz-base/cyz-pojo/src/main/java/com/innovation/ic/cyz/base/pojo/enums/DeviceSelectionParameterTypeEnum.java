package com.innovation.ic.cyz.base.pojo.enums;

/**
 * @desc   器件选型类型包含参数类型枚举
 * @author linuo
 * @time   2022年10月13日13:46:59
 */
public enum DeviceSelectionParameterTypeEnum {
    FAST_SCREEN(1,"快速筛选"),
    PARAM_SCREEN(2,"参数筛选");

    private Integer code;
    private String desc;

    DeviceSelectionParameterTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static DeviceSelectionParameterTypeEnum of(Integer code) {
        for (DeviceSelectionParameterTypeEnum c : DeviceSelectionParameterTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (DeviceSelectionParameterTypeEnum c : DeviceSelectionParameterTypeEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}