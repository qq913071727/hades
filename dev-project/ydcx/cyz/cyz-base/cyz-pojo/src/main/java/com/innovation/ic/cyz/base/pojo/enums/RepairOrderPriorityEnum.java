package com.innovation.ic.cyz.base.pojo.enums;

/**
 * @desc   工单优先级枚举
 * @author linuo
 * @time   2022年9月29日14:49:01
 */
public enum RepairOrderPriorityEnum {
    ORDINARY(1,"普通"),
    LOW(2,"低"),
    HIGH(3,"高"),
    EMERGENCY(4,"紧急"),
    NOW(5,"立刻")
    ;

    private Integer code;
    private String desc;

    RepairOrderPriorityEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static RepairOrderPriorityEnum of(Integer code) {
        for (RepairOrderPriorityEnum c : RepairOrderPriorityEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (RepairOrderPriorityEnum c : RepairOrderPriorityEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
