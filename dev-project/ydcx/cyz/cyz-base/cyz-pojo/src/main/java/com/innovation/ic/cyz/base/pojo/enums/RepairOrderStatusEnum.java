package com.innovation.ic.cyz.base.pojo.enums;

/**
 * @desc   工单状态枚举
 * @author linuo
 * @time   2022年9月29日17:28:11
 */
public enum RepairOrderStatusEnum {
    TO_BE_SOLVED(0,"待解决"),
    HAVE_TO_REPLY(1,"已回复"),
    RESOLVED(2,"已解决"),
    DEFERRED_PROCESSING(3,"延期处理"),
    CLOSED(4,"已关闭"),
    ;

    private Integer code;
    private String desc;

    RepairOrderStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static RepairOrderStatusEnum of(Integer code) {
        for (RepairOrderStatusEnum c : RepairOrderStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (RepairOrderStatusEnum c : RepairOrderStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
