package com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   工单分类信息获取的返回pojo类
 * @author linuo
 * @time   2022年9月23日16:28:09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderTypeRespPojo", description = "工单分类信息获取的返回pojo类")
public class RepairOrderTypeRespPojo {
    @ApiModelProperty(value = "主键", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "一级分类名称", dataType = "String")
    private String firstClassName;

    @ApiModelProperty(value = "一级分类负责人Id", dataType = "String")
    private String firstClassHeadId;

    @ApiModelProperty(value = "二级分类名称", dataType = "String")
    private String secondClassName;

    @ApiModelProperty(value = "二级分类负责人Id", dataType = "String")
    private String secondClassHeadId;

    @ApiModelProperty(value = "三级分类名称", dataType = "String")
    private String thirdClassName;

    @ApiModelProperty(value = "三级分类负责人Id", dataType = "String")
    private String thirdClassHeadId;
}