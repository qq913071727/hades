package com.innovation.ic.cyz.base.pojo.variable.cyz.mfrsmt_search_option;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Smt厂家列表选项（选项）
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MfrSmtOptionItem", description = "Smt厂家列表选项（选项）")
public class MfrSmtOptionItem {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "MfrParamId", dataType = "Long")
    private Long mfrParamId;

    @ApiModelProperty(value = "选项名称", dataType = "String")
    private String name;
}
