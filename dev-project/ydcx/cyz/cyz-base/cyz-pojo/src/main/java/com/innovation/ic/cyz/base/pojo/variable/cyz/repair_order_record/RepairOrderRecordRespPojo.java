package com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_record;

import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_file.RepairOrderFileRespPojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   工单信息查询的返回Vo类
 * @author linuo
 * @time   2022年9月29日14:24:29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderRecordRespPojo", description = "工单信息查询的返回Vo类")
public class RepairOrderRecordRespPojo {
    @ApiModelProperty(value = "流程记录", dataType = "String")
    private String record;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String describe;

    @ApiModelProperty(value = "记录的附件信息集合", dataType = "Long")
    List<RepairOrderFileRespPojo> fileList;
}