package com.innovation.ic.cyz.base.pojo.constant.repairOrderType;

/**
 * @desc   工单分类常量类
 * @author linuo
 * @time   2022年9月23日11:44:58
 */
public class RepairOrderTypeConstant {
    /** 分类级别字段 */
    public static final String CLASS_LEVEL_FIELD = "class_level";

    /** 父级分类id字段 */
    public static final String PARENT_ORDER_ID_FIELD = "parent_order_id";

    /** 分类名称字段 */
    public static final String CLASS_NAME = "className";

    /** 负责人id字段 */
    public static final String HEAD_ID_FIELD = "headId";
}
