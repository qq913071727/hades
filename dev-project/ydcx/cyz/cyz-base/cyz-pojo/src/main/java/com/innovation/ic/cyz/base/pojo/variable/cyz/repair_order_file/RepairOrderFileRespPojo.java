package com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_file;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * @time   工单附件信息的返回Vo类
 * @author linuo
 * @time   2022年9月29日14:36:54
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderFileRespPojo", description = "工单附件信息的返回Vo类")
public class RepairOrderFileRespPojo {
    @ApiModelProperty(value = "主键id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "文件名称", dataType = "String")
    private String fileName;

    @ApiModelProperty(value = "文件路径", dataType = "String")
    private String filePath;

    @ApiModelProperty(value = "文件大小", dataType = "String")
    private String fileSize;

    @ApiModelProperty(value = "创建用户名称", dataType = "String")
    private String createUserName;

    @ApiModelProperty(value = "创建时间", dataType = "String")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createTime;
}