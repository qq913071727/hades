package com.innovation.ic.cyz.base.pojo.variable.cyz.carousel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 轮播图列表的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CarouselListPojo", description = "轮播图列表的pojo类")
public class CarouselListPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "显示文字", dataType = "text")
    private String text;

    @ApiModelProperty(value = "图片url", dataType = "String")
    private String img;

    @ApiModelProperty(value = "尺寸", dataType = "String")
    private String size;

    @ApiModelProperty(value = "状态(2-启用, 4-未启用)", dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "类型(传递到器件选型页面，先设置为字符串类型)", dataType = "String")
    private String type;

}
