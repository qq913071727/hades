package com.innovation.ic.cyz.base.pojo.variable.cyz.feedback;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 信息反馈页面返回相关数据的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "FeedbackListPojo", description = "信息反馈页面返回相关数据的pojo类")
public class FeedbackListPojo {

//    @ApiModelProperty(value = "用户id", dataType = "String")
//    private String userId;

    @ApiModelProperty(value = "用户名", dataType = "String")
    private String name;

    @ApiModelProperty(value = "问题类型", dataType = "int")
    private int type;

    @ApiModelProperty(value = "图片", dataType = "String")
    private String url;

    @ApiModelProperty(value = "问题内容", dataType = "String")
    private String text;

    @ApiModelProperty(value = "提交时间", dataType = "Date")
    private Date createDate;
}
