package com.innovation.ic.cyz.base.pojo.enums;

/**
 * @desc   工单解决方案枚举
 * @author linuo
 * @time   2022年11月11日15:22:52
 */
public enum RepairOrderSolveEnum {
    RESOLVED(2,"解决"),
    DEFERRED_PROCESSING(3,"延期处理"),
    CLOSED(4,"关闭"),
    ;

    private Integer code;
    private String desc;

    RepairOrderSolveEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static RepairOrderSolveEnum of(Integer code) {
        for (RepairOrderSolveEnum c : RepairOrderSolveEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (RepairOrderSolveEnum c : RepairOrderSolveEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
