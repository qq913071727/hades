package com.innovation.ic.cyz.base.pojo.constant;

/**
 * redis存储时使用的常量
 */
public class RedisStorage {
    /**
     * client表
     */
    public static final String CLIENT = "CLIENT";

    /**
     * 存储在redis中的token的前缀
     */
    public static final String TOKEN_PREFIX = "TOKEN:";

    /**
     * 存储在redis中的轮播图数据的前缀
     */
    public static final String BANNER_CAROUSEL_PREFIX = "table:carousel";
}
