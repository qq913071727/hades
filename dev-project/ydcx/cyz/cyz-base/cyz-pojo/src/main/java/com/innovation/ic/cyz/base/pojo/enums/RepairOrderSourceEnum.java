package com.innovation.ic.cyz.base.pojo.enums;

/**
 * @desc   工单来源枚举
 * @author linuo
 * @time   2022年11月15日11:18:08
 */
public enum RepairOrderSourceEnum {
    HQ(1,"虎趣"),
    B1B(2,"比一比"),
    ;

    private Integer code;
    private String desc;

    RepairOrderSourceEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static RepairOrderSourceEnum of(Integer code) {
        for (RepairOrderSourceEnum c : RepairOrderSourceEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (RepairOrderSourceEnum c : RepairOrderSourceEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
