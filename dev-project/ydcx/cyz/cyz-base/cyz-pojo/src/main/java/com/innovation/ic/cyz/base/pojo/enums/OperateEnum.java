package com.innovation.ic.cyz.base.pojo.enums;

/**
 * @ClassName OperateEnum
 * @Description 操作类型枚举
 * @Date 2022/9/21
 * @Author myq
 */
public enum OperateEnum {

    INSERT("insert","新增"),
    UPDATE("update","修改"),
    DELETE("delete","删除"),
    SELECT("select","查询")
    ;


    private String key;
    private String val;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    OperateEnum(String key,String val) {
        this.key = key;
        this.val = val;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
