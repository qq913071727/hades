package com.innovation.ic.cyz.base.pojo.variable.cyz.avd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 避坑指南编辑选项Item的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AvdEditOptionItemPojo", description = "避坑指南编辑选项Item的pojo类")
public class AvdEditOptionItemPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "AvdRelId", dataType = "Long")
    private Long avdRelId;

    @ApiModelProperty(value = "Item名称", dataType = "String")
    private String name;

}
