package com.innovation.ic.cyz.base.pojo.variable.tenant_import;

import java.util.List;
import java.util.Date;

public class TenantImportData {

    private int tenantsid;
    private int memberid;
    private Integer adminid;
    private String name;
    private String contact;
    private String email;
    private String mobile;
    private String url;
    private String address;
    private boolean black;
    private boolean ad;
    private String password;
    private int permissions;
    private int brand;
    private String img;
    private String desc;
    private List<String> atlas;
    private List<String> companyatlas;
    private int position;
    private Date created_at;
    private String username;
    private String initials;
    private String shop_name;
    private String shop_email;
    private String shop_mobile;
    private int style;
    private String content;

    public void setTenantsid(int tenantsid) {
        this.tenantsid = tenantsid;
    }

    public int getTenantsid() {
        return tenantsid;
    }

    public void setMemberid(int memberid) {
        this.memberid = memberid;
    }

    public int getMemberid() {
        return memberid;
    }

    public void setAdminid(Integer adminid) {
        this.adminid = adminid;
    }

    public Integer getAdminid() {
        return adminid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContact() {
        return contact;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setBlack(boolean black) {
        this.black = black;
    }

    public boolean getBlack() {
        return black;
    }

    public void setAd(boolean ad) {
        this.ad = ad;
    }

    public boolean getAd() {
        return ad;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPermissions(int permissions) {
        this.permissions = permissions;
    }

    public int getPermissions() {
        return permissions;
    }

    public void setBrand(int brand) {
        this.brand = brand;
    }

    public int getBrand() {
        return brand;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setAtlas(List<String> atlas) {
        this.atlas = atlas;
    }

    public List<String> getAtlas() {
        return atlas;
    }

    public void setCompanyatlas(List<String> companyatlas) {
        this.companyatlas = companyatlas;
    }

    public List<String> getCompanyatlas() {
        return companyatlas;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getInitials() {
        return initials;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_email(String shop_email) {
        this.shop_email = shop_email;
    }

    public String getShop_email() {
        return shop_email;
    }

    public void setShop_mobile(String shop_mobile) {
        this.shop_mobile = shop_mobile;
    }

    public String getShop_mobile() {
        return shop_mobile;
    }

    public void setStyle(int style) {
        this.style = style;
    }

    public int getStyle() {
        return style;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

}