package com.innovation.ic.cyz.base.pojo.variable.cyz;

import com.innovation.ic.cyz.base.model.cyz.SlnIde;
import com.innovation.ic.cyz.base.model.cyz.SlnLikeTag;
import com.innovation.ic.cyz.base.model.cyz.SlnUrl;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * 方案列表的pojo类
 */
@Getter
@Setter
@ApiModel(value = "SlnListPojo", description = "方案列表的pojo类")
public class SlnListPojo {

    @ApiModelProperty(value = "主键", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "业务ID(BIB的业务主键，方案网业务主键)", dataType = "Long")
    private Long slnId;

    @ApiModelProperty(value = "方案名称", dataType = "String")
    private String title;

    @ApiModelProperty(value = "方案价格", dataType = "Float")
    private Float cash;

    @ApiModelProperty(value = "方案详情", dataType = "String")
    private String description;

    @ApiModelProperty(value = "方案封面", dataType = "String")
    private String goodsCover;

    @ApiModelProperty(value = "应用领域", dataType = "String")
    private String cateName;

    @ApiModelProperty(value = "1比一比 2ICGOO 3虎趣 4IC交易网", dataType = "Integer")
    private Integer source;

    @ApiModelProperty(value = "交付形式", dataType = "String")
    private String deliveryCateIdName;

    @ApiModelProperty(value = "性能参数", dataType = "String")
    private String performanceParameter;

    @ApiModelProperty(value = "应用场景", dataType = "String")
    private String applicationScene;

    @ApiModelProperty(value = "方案类型", dataType = "String")
    private String typeDisplay;

    @ApiModelProperty(value = "方案子名称", dataType = "String")
    private String subtitle;

    @ApiModelProperty(value = "方案状态", dataType = "String")
    private String statusDisplay;

    @ApiModelProperty(value = "查看次数", dataType = "Integer")
    private Integer viewNum;

    @ApiModelProperty(value = "方案审核状态：1待审核 2审核通过 3审核不通过", dataType = "Integer")
    private Integer examineStatus;

    @ApiModelProperty(value = "方案置顶状态： 0否 1是", dataType = "Integer")
    private Integer topState;

    @ApiModelProperty(value = "方案审核不通过原因", dataType = "Integer")
    private Integer examineErrorDesc;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date createdAt;

    @ApiModelProperty(value = "", dataType = "String")
    private String likeGoods;

    @ApiModelProperty(value = "人气数量", dataType = "Integer")
    private Integer popularityCount;

    @ApiModelProperty(value = "其他封面", dataType = "String")
    private String goodsOtherCover;

    @ApiModelProperty(value = "标签列表",dataType = "com.innovation.ic.cyz.base.model.cyz.SlnLikeTag")
    private List<SlnLikeTag> slnLikeTags;

    @ApiModelProperty(value = "其他封面列表",dataType = "com.innovation.ic.cyz.base.model.cyz.SlnUrl")
    private List<SlnUrl> slnUrls;

    @ApiModelProperty(value = "开放平台列表",dataType = "com.innovation.ic.cyz.base.model.cyz.SlnIde")
    private List<SlnIde> slnIdes;

}
