package com.innovation.ic.cyz.base.pojo.variable.cyz.video;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 视频标签
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VidTagPojo", description = "视频标签")
public class VidTagPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "排序", dataType = "Integer")
    private Integer orderIndex;
}
