package com.innovation.ic.cyz.base.pojo.variable.cyz.video;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 视频列表pojo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VidListPojo", description = "视频列表pojo")
public class VidListPojo {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using= ToStringSerializer.class)
    @ApiModelProperty(value = "id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "标题", dataType = "String")
    private String title;

    @ApiModelProperty(value = "封面地址", dataType = "String")
    private String cover;

    @ApiModelProperty(value = "阅读量", dataType = "Integer")
    private Integer viewNum;

    @ApiModelProperty(value = "点赞量", dataType = "Integer")
    private Integer likeNum;

    @ApiModelProperty(value = "收藏量", dataType = "Integer")
    private Integer collectNum;

    @ApiModelProperty(value = "标签", dataType = "List")
    private List<VidTagPojo> tags;
}
