package com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   查询器件选型类别返回的pojo类
 * @author linuo
 * @time   2022年10月8日11:24:52
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DeviceSelectionTypeQueryRespPojo", description = "查询器件选型类别返回的pojo类")
public class DeviceSelectionTypeQueryRespPojo {
    @ApiModelProperty(value = "主键id", dataType = "Long")
    private String id;

    @ApiModelProperty(value = "类别名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "图片路径", dataType = "String")
    private String picturePath;

    @ApiModelProperty(value = "图片尺寸", dataType = "String")
    private String size;

    @ApiModelProperty(value = "排序", dataType = "Integer")
    private Integer sort;
}