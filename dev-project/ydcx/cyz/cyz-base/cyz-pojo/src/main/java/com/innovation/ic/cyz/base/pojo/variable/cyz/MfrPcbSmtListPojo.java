package com.innovation.ic.cyz.base.pojo.variable.cyz;

import com.innovation.ic.cyz.base.pojo.annotation.MfrParamCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * PCB厂家列表的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MfrPcbSmtListPojo", description = "pcb和smt管理端返回vo")
public class MfrPcbSmtListPojo implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id", dataType = "Integer")
    private Long mfrId;


    @ApiModelProperty(value = "类型:1-smt,2-pcb", dataType = "Integer")
    private Integer module;

    @ApiModelProperty(value = "公司图片", dataType = "String")
    @MfrParamCode(value = "company_image")
    private String companyImage;


    @ApiModelProperty(value = "公司名称", dataType = "String")
    @MfrParamCode(value = "company_name")
    private String companyName;


    @ApiModelProperty(value = "省", dataType = "String")
    @MfrParamCode(value = "province")
    private String province;


    @ApiModelProperty(value = "市", dataType = "String")
    @MfrParamCode(value = "city")
    private String city;

    @ApiModelProperty(value = "区", dataType = "String")
    @MfrParamCode(value = "area")
    private String area;


    @ApiModelProperty(value = "工厂特色", dataType = "String")
    @MfrParamCode(value = "factory_feature")
    private List<String> factoryFeature;

    @ApiModelProperty(value = "资质", dataType = "String")
    @MfrParamCode(value = "qualification")
    private List<String> qualification;


    @ApiModelProperty(value = "工艺分类", dataType = "String")
    @MfrParamCode(value = "process_category")
    private List<String> processCategory;

    @ApiModelProperty(value = "品质特色", dataType = "String")
    @MfrParamCode(value = "quality_feature")
    private List<String> qualityFeature;


    @ApiModelProperty(value = "创建人", dataType = "String")
    @MfrParamCode(value = "creatorUserName")
    private String creatorUserName;

    @ApiModelProperty(value = "创建时间", dataType = "String")
    @MfrParamCode(value = "creatorTime")
    private Date creatorTime;


}
