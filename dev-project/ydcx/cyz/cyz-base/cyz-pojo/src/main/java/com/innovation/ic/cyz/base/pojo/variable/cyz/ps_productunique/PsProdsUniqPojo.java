package com.innovation.ic.cyz.base.pojo.variable.cyz.ps_productunique;

import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property.PsPropertyPojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PsProdsUniqPojo", description = "器件选型唯一信息的pojo类")
public class PsProdsUniqPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品唯一信息ID", dataType = "String")
    private String productUniqueID;

    @ApiModelProperty(value = "产品ID", dataType = "String")
    private String productID;

    @ApiModelProperty(value = "产品型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "产品描述", dataType = "String")
    private String description;

    @ApiModelProperty(value = "类别id", dataType = "String")
    private String categoryID;

    @ApiModelProperty(value = "品牌id", dataType = "String")
    private String brandID;

    @ApiModelProperty(value = "品牌名称", dataType = "String")
    private String brandName;

    @ApiModelProperty(value = "制造商名称", dataType = "String")
    private String manufacturer;

    @ApiModelProperty(value = "MPQ", dataType = "Integer")
    private Integer mpq;

    @ApiModelProperty(value = "最小起定量", dataType = "Integer")
    private Integer mmoq;

    @ApiModelProperty(value = "封装", dataType = "String")
    private String packages;

    @ApiModelProperty(value = "包装", dataType = "String")
    private String packings;

    @ApiModelProperty(value = "所属行业", dataType = "String")
    private String industryID;

    @ApiModelProperty(value = "图片", dataType = "String")
    private String image;

    @ApiModelProperty(value = "key", dataType = "String")
    private String key;

    @ApiModelProperty(value = "value", dataType = "String")
    private String value;

    @ApiModelProperty(value = "属性", dataType = "List")
    private List<PsPropertyPojo> properties;
}
