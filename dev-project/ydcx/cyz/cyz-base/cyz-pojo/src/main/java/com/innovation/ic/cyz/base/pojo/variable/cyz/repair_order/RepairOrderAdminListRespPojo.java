package com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   后台查询工单列表接口的返回Vo类
 * @author linuo
 * @time   2022年9月29日17:50:49
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderWebListRespPojo", description = "工单列表查询的返回Vo类")
public class RepairOrderAdminListRespPojo {
    @ApiModelProperty(value = "数据数量", dataType = "Long")
    private Long count;

    @ApiModelProperty(value = "数据", dataType = "List")
    private List<RepairOrderAdminListPojo> data;
}