package com.innovation.ic.cyz.base.pojo.variable.cyz.avd;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.innovation.ic.cyz.base.model.cyz.AvdBrand;
import com.innovation.ic.cyz.base.model.cyz.AvdTag;
import com.innovation.ic.cyz.base.model.cyz.AvdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 避坑指南列表的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AvdListPojo", description = "避坑指南列表的pojo类")
public class AvdListPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "AvdId", dataType = "Long")
    @TableId(value = "avd_id", type = IdType.INPUT)
    private Long avdId;

    @ApiModelProperty(value = "类别名称", dataType = "String")
    private String typeName;

    @ApiModelProperty(value = "类型(1-文章, 2-视频)", dataType = "Integer")
    @TableField(value = "module")
    private Integer module;

    @ApiModelProperty(value = "标题", dataType = "String")
    @TableField(value = "title")
    private String title;

    @ApiModelProperty(value = "封面地址", dataType = "String")
    @TableField(value = "cover")
    private String cover;

    @ApiModelProperty(value = "描述", dataType = "String")
    @TableField(value = "description")
    private String description;

    @ApiModelProperty(value = "标签", dataType = "List")
    private List<AvdListRelItemPojo> tags;

    @ApiModelProperty(value = "阅读量", dataType = "Integer")
    @TableField(value = "view_num")
    private Integer viewNum;

    @ApiModelProperty(value = "点赞量", dataType = "Integer")
    @TableField(value = "like_num")
    private Integer likeNum;

    @ApiModelProperty(value = "收藏量", dataType = "Integer")
    @TableField(value = "collect_num")
    private Integer collectNum;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    @TableField(value = "creator_id")
    private String creatorId;

    @ApiModelProperty(value = "作者信息", dataType = "com.innovation.ic.cyz.base.pojo.cyz.avd.AvdUserPojo")
    private AvdUserPojo user;

    @ApiModelProperty(value = " ", dataType = "com.innovation.ic.cyz.base.model.cyz.AvdTag")
    private List<AvdTag> avdTags;

    @ApiModelProperty(value = " ", dataType = "com.innovation.ic.cyz.base.model.cyz.AvdType")
    private List<AvdType> avdTypes;

    @ApiModelProperty(value = " ", dataType = "com.innovation.ic.cyz.base.model.cyz.AvdBrand")
    private List<AvdBrand> avdBrands;

}
