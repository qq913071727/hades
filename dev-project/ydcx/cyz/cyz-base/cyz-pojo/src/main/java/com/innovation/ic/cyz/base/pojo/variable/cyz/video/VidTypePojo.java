package com.innovation.ic.cyz.base.pojo.variable.cyz.video;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 视频内容类型
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ViddTypePojo", description = "视频内容类型")
public class VidTypePojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;
}
