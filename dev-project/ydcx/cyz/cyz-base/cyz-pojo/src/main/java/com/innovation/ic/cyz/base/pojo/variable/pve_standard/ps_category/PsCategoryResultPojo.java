package com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_category;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 器件选型分类结果的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PsCategoryResultPojo", description = "器件选型分类结果的pojo类")
public class PsCategoryResultPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "顶层分类", dataType = "List")
    private List<PsCategoryItemPojo> tops;

}
