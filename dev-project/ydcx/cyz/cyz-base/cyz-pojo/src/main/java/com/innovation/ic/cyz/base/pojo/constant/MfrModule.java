package com.innovation.ic.cyz.base.pojo.constant;

/**
 * 厂家类型
 */
public class MfrModule {
    /**
     * SMT
     */
    public static final int SMT = 1;

    /**
     * PCB
     */
    public static final int PCB = 2;
}
