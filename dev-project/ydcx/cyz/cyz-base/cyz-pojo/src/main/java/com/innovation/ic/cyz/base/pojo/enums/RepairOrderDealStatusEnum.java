package com.innovation.ic.cyz.base.pojo.enums;

/**
 * @desc   工单处理状态枚举
 * @author linuo
 * @time   2022年9月29日17:28:11
 */
public enum RepairOrderDealStatusEnum {
    UNTREATED(0,"未处理"),
    HAVE_TO_DEAL_WITH(1,"已处理"),
    ;

    private Integer code;
    private String desc;

    RepairOrderDealStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static RepairOrderDealStatusEnum of(Integer code) {
        for (RepairOrderDealStatusEnum c : RepairOrderDealStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }

    public static String getDesc(Integer code) {
        for (RepairOrderDealStatusEnum c : RepairOrderDealStatusEnum.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
