package com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_parameter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   器件选型类型包含参数列表查询参数的pojo类
 * @author linuo
 * @time   2022年10月13日13:32:05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DeviceSelectionParameterParamPojo", description = "器件选型类型包含参数列表查询参数的pojo类")
public class DeviceSelectionParameterParamPojo {
    @ApiModelProperty(value = "主键id", dataType = "Long")
    private String id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;
}