package com.innovation.ic.cyz.base.pojo.annotation;

import java.lang.annotation.*;

/**
 * 厂家属性code 自定义注解
 */
@Documented
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MfrParamCode {
    String value() default "";
}
