package com.innovation.ic.cyz.base.pojo.variable.cyz.ps_productunique;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PsProductPropertyPojo", description = "产品属性pojo")
public class PsProductPropertyPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品ID", dataType = "String", position = 1)
    private String productID;

    @ApiModelProperty(value = "产品属性ID", dataType = "String", position = 2)
    private String propertyID;

    @ApiModelProperty(value = "名称", dataType = "String", position = 3)
    private String name;

    @ApiModelProperty(value = "值", dataType = "String", position = 4)
    private String text;
}
