package com.innovation.ic.cyz.base.pojo.variable.cyz.sln_import;

import com.innovation.ic.cyz.base.model.cyz.Sln;
import com.innovation.ic.cyz.base.model.cyz.SlnIde;
import com.innovation.ic.cyz.base.model.cyz.SlnLikeTag;
import com.innovation.ic.cyz.base.model.cyz.SlnUrl;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ImportSlnOneDataPojo", description = "导入方案信息一条数据的pojo类")
public class ImportSlnOneDataPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "方案信息", dataType = "com.innovation.ic.base.model.cyz.Sln")
    private Sln sln;

    @ApiModelProperty(value = "方案中的url", dataType = "List")
    private List<SlnUrl> slnUrls;

    @ApiModelProperty(value = "方案中的标签", dataType = "List")
    private List<SlnLikeTag> slnLikeTags;

    @ApiModelProperty(value = "方案中的开发平台", dataType = "List")
    private List<SlnIde> slnIdes;
}
