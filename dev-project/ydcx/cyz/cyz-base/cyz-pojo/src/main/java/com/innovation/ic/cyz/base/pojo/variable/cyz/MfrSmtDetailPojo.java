package com.innovation.ic.cyz.base.pojo.variable.cyz;

import com.innovation.ic.cyz.base.pojo.annotation.MfrParamCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Smt厂家详情的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MfrSmtDetailPojo", description = "SMT厂家详情的pojo类")
public class MfrSmtDetailPojo implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "MfrId", dataType = "Long")
    private Long mfrId;

    @ApiModelProperty(value = "厂家名称", dataType = "String")
    @MfrParamCode(value = "company_name")
    private String mfrName;

    @ApiModelProperty(value = "厂家图片", dataType = "String")
    @MfrParamCode(value = "company_image")
    private String mfrImg;

    @ApiModelProperty(value = "公司简介", dataType = "String")
    @MfrParamCode(value = "company_introduction")
    private String mfrIntroduction;

    @ApiModelProperty(value = "公司规模", dataType = "String")
    @MfrParamCode(value = "company_scale")
    private String mfrScale;

    @ApiModelProperty(value = "产品类别", dataType = "List")
    @MfrParamCode(value = "product_classification")
    private List<String> productClassification;

    @ApiModelProperty(value = "我们的产品", dataType = "List")
    @MfrParamCode(value = "our_products")
    private List<MfrSmtProductPojo> ourProducts;
}
