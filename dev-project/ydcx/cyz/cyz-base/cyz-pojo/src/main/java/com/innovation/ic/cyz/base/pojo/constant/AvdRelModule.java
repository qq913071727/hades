package com.innovation.ic.cyz.base.pojo.constant;

/**
 * Avd相关数据类型
 */
public class AvdRelModule {

    /**
     * 分类
     */
    public static final int Type = 1;

    /**
     * 标签
     */
    public static final int Tag = 2;

    /**
     * 品牌
     */
    public static final int Brand = 3;

}
