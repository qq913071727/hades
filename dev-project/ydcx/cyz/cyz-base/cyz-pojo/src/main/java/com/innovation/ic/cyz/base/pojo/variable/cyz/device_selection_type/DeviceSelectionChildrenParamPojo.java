package com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @desc   器件选型子集参数的pojo类
 * @author linuo
 * @time   2022年10月8日15:43:35
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DeviceSelectionChildrenParamPojo", description = "器件选型子集参数的pojo类")
public class DeviceSelectionChildrenParamPojo {
    @ApiModelProperty(value = "主键id", dataType = "Long")
    private String id;

    @ApiModelProperty(value = "类别名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "下级类型", dataType = "List")
    private List<JSONObject> children;
}