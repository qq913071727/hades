package com.innovation.ic.cyz.base.pojo.variable.cyz.video;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 视频品牌pojo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VidBrandPojo", description = "视频品牌pojo")
public class VidBrandPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;
}
