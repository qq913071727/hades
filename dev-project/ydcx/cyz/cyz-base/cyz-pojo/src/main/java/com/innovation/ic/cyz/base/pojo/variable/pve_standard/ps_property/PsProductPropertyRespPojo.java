package com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   器件参数返回的pojo类
 * @author linuo
 * @time   2022年11月4日14:26:55
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PsProductPropertyRespPojo", description = "器件参数返回的pojo类")
public class PsProductPropertyRespPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "属性Id", dataType = "String")
    @TableField(value = "propertyId")
    private String propertyId;

    @ApiModelProperty(value = "属性名称", dataType = "String")
    @TableField(value = "propertyName")
    private String propertyName;

    @ApiModelProperty(value = "属性值", dataType = "List")
    private String text;
}