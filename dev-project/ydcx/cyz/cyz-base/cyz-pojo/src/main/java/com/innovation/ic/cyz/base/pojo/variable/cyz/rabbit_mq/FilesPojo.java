package com.innovation.ic.cyz.base.pojo.variable.cyz.rabbit_mq;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   cyz -> 虎趣修改工单状态附件的pojo类
 * @author linuo
 * @time   2022年11月21日15:49:06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "FilesPojo", description = "cyz -> 虎趣修改工单状态附件的pojo类")
public class FilesPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文件类型(1：文本、2：图片)", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "附件地址", dataType = "String")
    private String url;

    @ApiModelProperty(value = "附件名称", dataType = "String")
    private String fileName;

    @ApiModelProperty(value = "附件大小", dataType = "String")
    private String fileSize;
}