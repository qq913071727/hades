package com.innovation.ic.cyz.base.pojo.variable.cyz.sln_import;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ImportSlnOnePagePojo", description = "导入方案信息一页数据的pojo类")
public class ImportSlnOnePagePojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "总数量", dataType = "Integer")
    private Integer count;

    @ApiModelProperty(value = "下一页地址", dataType = "String")
    private String next;

    @ApiModelProperty(value = "上一页地址", dataType = "String")
    private String previous;

    @ApiModelProperty(value = "导入方案信息一条数据的pojo类", dataType = "com.innovation.ic.cyz.base.pojo.cyz.sln_import.ImportSlnOneDataPojo")
    private List<ImportSlnOneDataPojo> results;
}
