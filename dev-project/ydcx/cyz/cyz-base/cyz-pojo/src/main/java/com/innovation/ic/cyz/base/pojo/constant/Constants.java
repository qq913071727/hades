package com.innovation.ic.cyz.base.pojo.constant;

/**
 * @desc   日常使用的常量值
 * @author linuo
 * @time   2022年9月29日09:29:39
 */
public class Constants {
    /** id */
    public static final String ID = "id";

    /* 名称id */
    public static final String NAME = "name";

    /** 状态 */
    public static final String STATUS_ = "status";

    /** 页数 */
    public static final String PAGE_NO = "pageNo";

    /** 开始时间 */
    public static final String START_TIME = "startTime";

    /** 结束时间 */
    public static final String END_TIME = "endTime";

    /** 每页数量 */
    public static final String PAGE_SIZE = "pageSize";

    /** 生产运行环境 */
    public static final String PROD_RUN_ENVIRONMENT = "prod";

    /** 创建时间字段 */
    public static final String CREATE_TIME_FIELD = "create_time";

    /** 数量 */
    public static final String COUNT = "count";

    /** 数据 */
    public static final String DATA = "data";

    /** 右侧小括号 */
    public static final String RIGHT_PARENTHESES = ")";

    /** 左侧小括号 */
    public static final String LEFT_PARENTHESES = "\\(";
}