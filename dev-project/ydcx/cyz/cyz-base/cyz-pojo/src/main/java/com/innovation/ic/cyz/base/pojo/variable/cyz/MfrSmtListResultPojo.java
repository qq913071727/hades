package com.innovation.ic.cyz.base.pojo.variable.cyz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Smt厂家列表的pojo类（带总条数）
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MfrSmtListResultPojo", description = "Smt厂家列表的pojo类（带总条数）")
public class MfrSmtListResultPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "总数", dataType = "Long")
    private Long total;

    @ApiModelProperty(value = "列表", dataType = "List")
    private List<MfrSmtListPojo> list;
}
