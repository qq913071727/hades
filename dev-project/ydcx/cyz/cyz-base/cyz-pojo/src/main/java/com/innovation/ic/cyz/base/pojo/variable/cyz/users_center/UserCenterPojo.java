package com.innovation.ic.cyz.base.pojo.variable.cyz.users_center;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserCenterPojo", description = "用户中心数据")
public class UserCenterPojo {

    @ApiModelProperty(value = "主键, 避坑指南id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "主键, 用户id", dataType = "String")
    private String userId;

    @ApiModelProperty(value = "用户名", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "用户来源:1 比一比 2 ic交易网", dataType = "Integer")
    private Integer source;

    @ApiModelProperty(value = "头像", dataType = "String")
    private String avatar;

    @ApiModelProperty(value = "个人简介", dataType = "String")
    private String resume;

    @ApiModelProperty(value = "投稿数量", dataType = "Integer")
    private Integer contributionNum;

    @ApiModelProperty(value = "点赞作品数量", dataType = "Integer")
    private Integer thumbsUpNum;

    @ApiModelProperty(value = "收藏作品量", dataType = "Integer")
    private Integer collectionNum;

    @ApiModelProperty(value = "浏览量", dataType = "Integer")
    private Integer browseNum;

    @ApiModelProperty(value = "内容形式", dataType = "Integer")
    private Integer module;

    @ApiModelProperty(value = "封面", dataType = "String")
    private String cover;

    @ApiModelProperty(value = "标题", dataType = "String")
    private String title;

    @ApiModelProperty(value = "所属类别", dataType = "String")
    private String name;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String description;

    @ApiModelProperty(value = "标签", dataType = "String")
    private String tag;

    @ApiModelProperty(value = "发布时间", dataType = "String")
    private Date createDate;

    @ApiModelProperty(value = "状态", dataType = "Integer")
    private Integer status;

    @ApiModelProperty(value = "正文", dataType = "String")
    private String paragraph;
}
