package com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 器件选型筛选条件品牌的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PsPropertyBrandPojo", description = "器件选型筛选条件品牌的pojo类")
public class PsPropertyBrandPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "品牌Id", dataType = "String")
    @TableField(value = "brandId")
    private String brandId;

    @ApiModelProperty(value = "品牌名称", dataType = "String")
    @TableField(value = "brandName")
    private String brandName;

}
