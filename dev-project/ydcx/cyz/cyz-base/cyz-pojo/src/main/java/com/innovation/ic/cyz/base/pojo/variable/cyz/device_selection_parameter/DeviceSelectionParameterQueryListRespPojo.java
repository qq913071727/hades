package com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_parameter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * 器件选型筛选条件结果的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DeviceSelectionParameterQueryListRespPojo", description = "器件选型筛选条件查询结果的pojo类")
public class DeviceSelectionParameterQueryListRespPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "多个属性", dataType = "List")
    private List<DeviceSelectionParamPojo> properties;
}