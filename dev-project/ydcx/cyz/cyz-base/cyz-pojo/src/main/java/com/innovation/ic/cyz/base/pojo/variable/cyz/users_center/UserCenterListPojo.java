package com.innovation.ic.cyz.base.pojo.variable.cyz.users_center;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserCenterPojo", description = "用户中心数据")
public class UserCenterListPojo {

    @ApiModelProperty(value = "用户中心相关数据", dataType = "List")
    private List<UserCenterPojo> userCenterPojo;

    @ApiModelProperty(value = "返回数据条数", dataType = "int")
    private int num;
}
