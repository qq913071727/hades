package com.innovation.ic.cyz.base.pojo.variable.cyz.mfrsmt_search_option;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Smt厂家列表选项（横向）
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MfrSmtOptionLine", description = "Smt厂家列表选项（横向）")
public class MfrSmtOptionLine {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "第一级选项", dataType = "com.innovation.ic.cyz.base.pojo.cyz.mfrpcb_search_option.MfrPcbOptionItem")
    private MfrSmtOptionItem level1Option;

    @ApiModelProperty(value = "第二级选项", dataType = "List")
    private List<MfrSmtOptionItem> level2Options;
}
