package com.innovation.ic.cyz.base.pojo.constant;

/**
 * 视频审批状态
 */
public class VidApproveStatus {
    /**
     * 草稿
     */
    public static final int Draft = 1;

    /**
     * 待审批
     */
    public static final int UnApprove = 2;

    /**
     * 审批通过
     */
    public static final int Approved = 3;

    /**
     * 审批不通过
     */
    public static final int Reject = 4;

    /**
     * 删除
     */
    public static final int Delete = 5;
}
