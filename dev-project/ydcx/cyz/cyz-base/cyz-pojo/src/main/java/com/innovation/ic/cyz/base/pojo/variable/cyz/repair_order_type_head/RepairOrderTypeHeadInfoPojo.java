package com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type_head;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   工单分类负责人的pojo类
 * @author linuo
 * @time   2022年9月30日10:37:29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderListResultPojo", description = "工单分类列表返回的pojo类")
public class RepairOrderTypeHeadInfoPojo {
    @ApiModelProperty(value = "用户id", dataType = "Long")
    private Long userId;

    @ApiModelProperty(value = "用户名称", dataType = "String")
    private String userName;
}