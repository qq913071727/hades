package com.innovation.ic.cyz.base.pojo.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 器件选型,特殊参数处理
 */
public enum DeviceSelectionToConfigureEnum {

    BRAND("品牌", "e4e65b37b604");

    private String name;
    private String id;

    DeviceSelectionToConfigureEnum(String name, String id) {
        this.name = name;
        this.id = id;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public static List<Map<String, String>> getList() {
        List<Map<String, String>> result = new ArrayList<>();
        for (DeviceSelectionToConfigureEnum deviceSelectionToConfigureEnum : DeviceSelectionToConfigureEnum.values()) {
            Map<String, String> param = new HashMap<>();
            param.put("id", deviceSelectionToConfigureEnum.getId());
            param.put("name", deviceSelectionToConfigureEnum.getName());
            result.add(param);
        }
        return result;
    }


}
