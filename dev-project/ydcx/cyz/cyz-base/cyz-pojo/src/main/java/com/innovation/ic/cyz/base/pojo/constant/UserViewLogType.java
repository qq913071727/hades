package com.innovation.ic.cyz.base.pojo.constant;

/**
 * 用户浏览记录类型
 */
public class UserViewLogType {

    /**
     * 避坑指南-浏览
     */
    public static final int AvdView = 1;

    /**
     * 避坑指南-点赞
     */
    public static final int AvdLike = 2;

    /**
     * 避坑指南-收藏
     */
    public static final int AvdCollect = 3;

    /**
     * 视频-浏览
     */
    public static final int VidView = 201;
}
