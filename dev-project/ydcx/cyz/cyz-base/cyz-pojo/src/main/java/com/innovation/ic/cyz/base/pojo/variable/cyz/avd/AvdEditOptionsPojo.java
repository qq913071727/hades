package com.innovation.ic.cyz.base.pojo.variable.cyz.avd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 避坑指南编辑选项的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AvdEditOptionsPojo", description = "避坑指南编辑选项的pojo类")
public class AvdEditOptionsPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文章/视频选项", dataType = "List")
    private List<AvdEditOptionItemPojo> moduleOptions;

    @ApiModelProperty(value = "分类选项", dataType = "List")
    private List<AvdEditOptionItemPojo> typeOptions;

    @ApiModelProperty(value = "标签选项", dataType = "List")
    private List<AvdEditOptionItemPojo> tagOptions;

    @ApiModelProperty(value = "品牌选项", dataType = "List")
    private List<AvdEditOptionItemPojo> brandOptions;

}
