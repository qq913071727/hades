package com.innovation.ic.cyz.base.pojo.variable.cyz.avd;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 避坑指南列表中相关项Item的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AvdListRelItemPojo", description = "避坑指南列表中相关项Item的pojo类")
public class AvdListRelItemPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "AvdId", dataType = "Long")
    @TableField(value = "avd_id")
    private Long avdId;

    @ApiModelProperty(value = "id", dataType = "Long")
    @TableField(value = "id")
    private Long id;

    @ApiModelProperty(value = "相关项目的Id", dataType = "Long")
    @TableField(value = "avd_rel_item_id")
    private Long avdRelItemId;

    @ApiModelProperty(value = "相关项目的名称", dataType = "String")
    @TableField(value = "name")
    private String name;

}
