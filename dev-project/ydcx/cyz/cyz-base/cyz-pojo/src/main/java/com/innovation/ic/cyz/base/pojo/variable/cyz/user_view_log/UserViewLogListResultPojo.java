package com.innovation.ic.cyz.base.pojo.variable.cyz.user_view_log;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 用户浏览/点赞/收藏记录列表的pojo类（带总条数）
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserViewLogListResultPojo", description = "用户浏览/点赞/收藏记录列表的pojo类（带总条数）")
public class UserViewLogListResultPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "总数", dataType = "Long")
    private Long total;

    @ApiModelProperty(value = "列表", dataType = "List")
    private List<UserViewLogListPojo> list;

}
