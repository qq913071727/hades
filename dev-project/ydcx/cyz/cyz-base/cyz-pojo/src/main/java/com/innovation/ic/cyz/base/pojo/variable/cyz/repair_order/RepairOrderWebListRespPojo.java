package com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   查询我的工单列表接口的返回Vo类
 * @author linuo
 * @time   2022年9月30日09:27:13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderWebListRespPojo", description = "工单信息查询的返回Vo类")
public class RepairOrderWebListRespPojo {
    @ApiModelProperty(value = "数据数量", dataType = "Long")
    private Long count;

    @ApiModelProperty(value = "数据", dataType = "List")
    private List<RepairOrderWebListPojo> data;
}