package com.innovation.ic.cyz.base.pojo.constant;

/**
 * @desc   器件的常量值
 * @author linuo
 * @time   2022年10月25日14:43:34
 */
public class ProductConstants {
    /** 品牌字段 */
    public static final String BRAND_FIELD = "brand";

    /** 型号字段 */
    public static final String PART_NUMBER_FIELD = "partNumber";

    /** id字段 */
    public static final String ID_FIELD = "id";
}