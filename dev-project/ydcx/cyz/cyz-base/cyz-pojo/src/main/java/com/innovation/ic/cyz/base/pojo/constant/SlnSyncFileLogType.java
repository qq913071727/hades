package com.innovation.ic.cyz.base.pojo.constant;

/**
 * 方案同步文件日志类型
 */
public class SlnSyncFileLogType {

    /**
     * 其他封面
     */
    public static final int OtherCover = 1;

    /**
     * 方案文档
     */
    public static final int FilePath = 2;

    /**
     * 方案封面
     */
    public static final int GoodsCover = 3;

    /**
     * 方案详情
     */
    public static final int Description = 4;

}
