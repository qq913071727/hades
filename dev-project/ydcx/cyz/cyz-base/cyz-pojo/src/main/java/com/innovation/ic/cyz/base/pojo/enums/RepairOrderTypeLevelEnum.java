package com.innovation.ic.cyz.base.pojo.enums;

/**
 * @desc   工单分类级别枚举
 * @author linuo
 * @time   2022年9月23日11:41:01
 */
public enum RepairOrderTypeLevelEnum {
    FIRST(1,"一级分类"),
    SECOND(2,"二级分类"),
    THIRD(3,"三级分类");

    private Integer code;
    private String desc;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    RepairOrderTypeLevelEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}