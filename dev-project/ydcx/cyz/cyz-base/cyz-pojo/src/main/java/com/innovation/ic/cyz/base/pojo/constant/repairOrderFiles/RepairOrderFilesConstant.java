package com.innovation.ic.cyz.base.pojo.constant.repairOrderFiles;

/**
 * @desc   工单附件常量类
 * @author linuo
 * @time   2022年9月29日15:22:19
 */
public class RepairOrderFilesConstant {
    /** 路径 */
    public static final String PATH = "path";

    /** 文件名 */
    public static final String FILE_NAME = "fileName";
}
