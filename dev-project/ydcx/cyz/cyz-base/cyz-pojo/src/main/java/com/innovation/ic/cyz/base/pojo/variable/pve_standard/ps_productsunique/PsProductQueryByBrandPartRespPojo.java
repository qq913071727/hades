package com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_productsunique;

import com.innovation.ic.cyz.base.pojo.variable.pve_standard.pc_files.PcFilesRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property.PsProductPropertyRespPojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @desc   根据品牌、型号查询器件返回信息的pojo类
 * @author linuo
 * @time   2022年10月26日09:41:54
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PsProductQueryByBrandPartRespPojo", description = "根据品牌、型号查询器件返回信息的pojo类")
public class PsProductQueryByBrandPartRespPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品唯一信息ID", dataType = "String")
    private String productUniqueID;

    @ApiModelProperty(value = "产品ID", dataType = "String")
    private String productID;

    @ApiModelProperty(value = "产品型号", dataType = "String")
    private String partNumber;

    @ApiModelProperty(value = "品牌", dataType = "String")
    private String brandName;

    @ApiModelProperty(value = "品牌介绍", dataType = "String")
    private String productIntroduce;

    @ApiModelProperty(value = "图片", dataType = "String")
    private String image;

    @ApiModelProperty(value = "属性", dataType = "List")
    private List<PsProductPropertyRespPojo> properties;

    @ApiModelProperty(value = "附件", dataType = "List")
    private List<PcFilesRespPojo> files;
}
