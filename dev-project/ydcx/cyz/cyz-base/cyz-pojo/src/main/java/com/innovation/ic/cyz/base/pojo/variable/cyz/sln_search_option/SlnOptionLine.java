package com.innovation.ic.cyz.base.pojo.variable.cyz.sln_search_option;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 方案列表选项（横向）
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SlnOptionLine", description = "方案列表选项（横向）")
public class SlnOptionLine {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "第一级选项", dataType = "String")
    private SlnOptionItem level1Option;

    @ApiModelProperty(value = "第二级选项", dataType = "List")
    private List<SlnOptionItem> level2Options;
}
