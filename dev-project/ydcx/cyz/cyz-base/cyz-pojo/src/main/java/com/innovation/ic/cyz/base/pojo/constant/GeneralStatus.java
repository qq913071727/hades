package com.innovation.ic.cyz.base.pojo.constant;

/**
 * 数据库的表中的status字段的取值
 */
public class GeneralStatus {
    /**
     * 正常
     */
    public static final int NORMAL = 2;

    /**
     * 删除
     */
    public static final int DELETE = 4;
}
