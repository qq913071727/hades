package com.innovation.ic.cyz.base.pojo.constant;

/**
 * 咨询导出类型
 */
public class InquiryExportStatus {

    /**
     * 已导出
      */
    public static final Integer YES = 1;

    /**
     * 未导出
     */
    public static final Integer NO = 0;
}
