package com.innovation.ic.cyz.base.pojo.variable.cyz.banner;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   启用的轮播图返回数据的Pojo类
 * @author linuo
 * @time   2022年10月17日09:40:48
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "BannerAvailableDataRespPojo", description = "启用的轮播图返回数据的Pojo类")
public class BannerAvailableDataRespPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "图片路径", dataType = "String")
    private String picturePath;

    @ApiModelProperty(value = "尺寸", dataType = "String")
    private String size;

    @ApiModelProperty(value = "二级页面路径", dataType = "String")
    private String secondaryPagePath;
}