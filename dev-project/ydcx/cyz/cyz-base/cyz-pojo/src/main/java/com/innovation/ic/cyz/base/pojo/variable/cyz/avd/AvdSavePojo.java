package com.innovation.ic.cyz.base.pojo.variable.cyz.avd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 避坑指南保存的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AvdSavePojo", description = "避坑指南保存的pojo类")
public class AvdSavePojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "AvdId", dataType = "Long")
    private Long avdId;

}
