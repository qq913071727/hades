package com.innovation.ic.cyz.base.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户信息
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserInfo", description = "用户信息")
public class UserInfo {
    @ApiModelProperty(value = "主键, 用户id", dataType = "String")
    private String id;

    @ApiModelProperty(value = "头像", dataType = "String")
    private String avatar;

    @ApiModelProperty(value = "昵称", dataType = "String")
    private String nickname;

    @ApiModelProperty(value = "姓名", dataType = "String")
    private String name;

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String mobile;

    @ApiModelProperty(value = "职位", dataType = "String")
    private String position;

    @ApiModelProperty(value = "工作年限", dataType = "String")
    private String workYear;

    @ApiModelProperty(value = "个人简介", dataType = "String")
    private String resume;

    @ApiModelProperty(value = "用户名", dataType = "String")
    private String username;

    @ApiModelProperty(value = "积分", dataType = "Integer")
    private Integer point;
}