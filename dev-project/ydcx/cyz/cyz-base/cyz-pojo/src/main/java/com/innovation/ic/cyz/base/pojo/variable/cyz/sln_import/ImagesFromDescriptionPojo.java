package com.innovation.ic.cyz.base.pojo.variable.cyz.sln_import;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ImagesFromDescriptionPojo", description = "从方案描述中获取图片信息的pojo类")
public class ImagesFromDescriptionPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "url", dataType = "String")
    private String url;

    @ApiModelProperty(value = "起始位置", dataType = "Integer")
    private Integer start;

    @ApiModelProperty(value = "长度", dataType = "Integer")
    private Integer length;

    @ApiModelProperty(value = "随机字符串", dataType = "String")
    private String randomString;

}
