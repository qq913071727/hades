package com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @time   一级分类查询返回的pojo类
 * @author linuo
 * @time   2022年11月16日11:38:19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "FirstLevelRepairOrderTypeQueryRespPojo", description = "一级分类查询返回的pojo类")
public class FirstLevelRepairOrderTypeQueryRespPojo {
    @ApiModelProperty(value = "主键id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "分类名称", dataType = "String")
    private String className;
}