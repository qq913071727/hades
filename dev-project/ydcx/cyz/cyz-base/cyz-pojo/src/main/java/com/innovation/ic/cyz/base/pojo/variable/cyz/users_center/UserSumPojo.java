package com.innovation.ic.cyz.base.pojo.variable.cyz.users_center;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserCenterPojo", description = "用户中心(阅读总数、点赞总数、收藏总数)")
public class UserSumPojo {

    @ApiModelProperty(value = "阅读总数", dataType = "int")
    private int viewSum;

    @ApiModelProperty(value = "点赞总数", dataType = "int")
    private int likeSum;

    @ApiModelProperty(value = "收藏总数", dataType = "int")
    private int collectSum;
}
