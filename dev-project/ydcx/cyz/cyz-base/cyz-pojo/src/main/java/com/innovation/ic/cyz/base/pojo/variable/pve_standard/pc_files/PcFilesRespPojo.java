package com.innovation.ic.cyz.base.pojo.variable.pve_standard.pc_files;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   器件附件返回的pojo类
 * @author linuo
 * @time   2022年11月8日17:16:19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PcFilesRespPojo", description = "器件附件返回的pojo类")
public class PcFilesRespPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文件名", dataType = "String")
    private String fileName;

    @ApiModelProperty(value = "文件类型", dataType = "String")
    private String type;

    @ApiModelProperty(value = "文件路径", dataType = "String")
    private String url;
}