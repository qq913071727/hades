package com.innovation.ic.cyz.base.pojo.variable.cyz.product;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   产品模糊查询返回的pojo类
 * @author linuo
 * @time   2022年11月9日11:01:17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ProductQueryRespPojo", description = "产品模糊查询返回的pojo类")
public class ProductQueryRespPojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id", dataType = "Long")
    private String id;

    @ApiModelProperty(value = "型号信息", dataType = "String")
    private String partNumberInfo;
}
