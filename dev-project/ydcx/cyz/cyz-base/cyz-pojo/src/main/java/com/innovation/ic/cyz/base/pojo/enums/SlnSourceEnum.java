package com.innovation.ic.cyz.base.pojo.enums;

public enum SlnSourceEnum {

    /**
     * 1比一比 2ICGOO 3虎趣 4IC交易网 5我爱方案网
     */
    BIB(1,"比一比"),
    ICGOO(2,"ICGOO"),
    HQ(3,"虎趣"),
    ICJYW(4,"IC交易网"),
    WAFAW(5,"我爱方案网"),
    ;


    private Integer key;
    private String val;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    SlnSourceEnum(Integer key,String val) {
        this.key = key;
        this.val = val;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
