package com.innovation.ic.cyz.base.pojo.variable.cyz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 上传图片的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UploadImagePojo", description = "上传图片的pojo类")
public class UploadImagePojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "原文件名", dataType = "String")
    private String originalFileName;

    @ApiModelProperty(value = "图片相对url", dataType = "String")
    private String relativeUrl;

    @ApiModelProperty(value = "图片绝对url", dataType = "String")
    private String absoluteUrl;


    @ApiModelProperty(value = "做更新用,只用作于器件选型", dataType = "String")
    private String uniqueId;

}
