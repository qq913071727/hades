package com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * 器件选型筛选条件结果的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PsPropertyResultPojo", description = "器件选型筛选条件结果的pojo类")
public class PsPropertyResultPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "多个属性", dataType = "List")
    private List<PsPropertyPojo> properties;

    @ApiModelProperty(value = "品牌", dataType = "List")
    private List<PsPropertyBrandPojo> brands;

    @ApiModelProperty(value = "封装", dataType = "List")
    private List<PsPropertyPackagePojo> packages;

    @ApiModelProperty(value = "包装", dataType = "List")
    private List<PsPropertyPackingPojo> packings;
}