package com.innovation.ic.cyz.base.pojo.variable.cyz.avd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 避坑指南列表的pojo类（带总条数）
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AvdListResultPojo", description = "避坑指南列表的pojo类（带总条数）")
public class AvdListResultPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "总数", dataType = "Long")
    private Long total;

    @ApiModelProperty(value = "列表", dataType = "List")
    private List<AvdListPojo> list;

}
