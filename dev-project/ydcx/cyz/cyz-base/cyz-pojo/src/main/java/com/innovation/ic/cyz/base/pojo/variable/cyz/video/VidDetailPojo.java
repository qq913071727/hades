package com.innovation.ic.cyz.base.pojo.variable.cyz.video;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdUserPojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VidDetailPojo", description = "视频详情Pojo")
public class VidDetailPojo {
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using= ToStringSerializer.class)
    @ApiModelProperty(value = "id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "标题", dataType = "String")
    private String title;

    @ApiModelProperty(value = "封面地址", dataType = "String")
    private String cover;

    @ApiModelProperty(value = "视频地址", dataType = "String")
    private String video;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String description;

    @ApiModelProperty(value = "正文", dataType = "String")
    private String paragraph;

    @ApiModelProperty(value = "阅读量", dataType = "Integer")
    private Integer viewNum;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
//    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    @ApiModelProperty(value = "作者信息", dataType = "com.innovation.ic.cyz.base.pojo.cyz.avd.AvdUserPojo")
    private AvdUserPojo user;

    @ApiModelProperty(value = "标签", dataType = "List")
    private List<VidTagPojo> tags;

}
