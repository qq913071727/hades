package com.innovation.ic.cyz.base.pojo.constant;

public class DatabaseGlobal {
    /**
     * b1b系统
     */
//    public static final String IM_B1B = "im-b1b";
//    public static final String B1B = "b1b";

    /**
     * erp9系统的sqlserver数据库
     */
    public static final String ERP9_SQLSERVER = "erp9-sqlserver";

    /**
     * cyz系统
     */
    public static final String CYZ = "cyz";

    /**
     * 标准型号库(sqlserver数据库)
     */
    public static final String PVE_STANDARD = "pve-standard";
}