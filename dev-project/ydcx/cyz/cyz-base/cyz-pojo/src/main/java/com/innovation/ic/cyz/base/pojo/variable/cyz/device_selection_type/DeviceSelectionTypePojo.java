package com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * 器件选型类型
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DeviceSelectionTypePojo", description = "器件选型类型")
public class DeviceSelectionTypePojo {


    @ApiModelProperty(value = "主键", dataType = "Long")
    private String id;

    @ApiModelProperty(value = "类别名称", dataType = "String")
    private String levelName;

    @ApiModelProperty(value = "类别名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "子类别", dataType = "String")
    private List<DeviceSelectionTypePojo> child;


    @ApiModelProperty(value = "外部父类id", dataType = "String")
    private String fatherId;



}
