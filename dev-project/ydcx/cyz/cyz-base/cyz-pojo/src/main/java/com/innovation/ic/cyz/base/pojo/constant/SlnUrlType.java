package com.innovation.ic.cyz.base.pojo.constant;

/**
 * 方案Url类型
 */
public class SlnUrlType {

    /**
     * 其他封面
     */
    public static final int OtherCover = 1;

    /**
     * 方案文档
     */
    public static final int FilePath = 2;
}
