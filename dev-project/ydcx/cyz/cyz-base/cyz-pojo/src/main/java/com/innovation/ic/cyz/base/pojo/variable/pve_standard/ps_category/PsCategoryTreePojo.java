package com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_category;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 分类
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PsCategoryTreePojo {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分类Id", dataType = "String")
    private String id;

    @ApiModelProperty(value = "父级Id", dataType = "String")
    private String fatherId;

    @ApiModelProperty(value = "分类名称", dataType = "String")
    private String name;
}
