package com.innovation.ic.cyz.base.pojo.constant;

/**
 * 避坑指南文章/视频
 */
public class AvdModule {

    /**
     * 文章
     */
    public static final int ARTICLE = 1;

    public static final String ARTICLE_CHINESE = "文章";

    /**
     * 视频
     */
    public static final int VIDEO = 2;

    public static final String VIDEO_CHINESE = "视频";

}
