package com.innovation.ic.cyz.base.pojo.constant;

/**
 * @desc   rabbitmq的常量
 * @author linuo
 * @time   2022年11月16日10:35:03
 */
public class RabbitMqConstants {
    /** 虎趣 -> CYZ的交换机名称 */
    public static final String HQ_CYZ_EXCHANGE_FIELD = "hqCyz";

    /** CYZ -> 虎趣的交换机名称 */
    public static final String CYZ_HQ_EXCHANGE_FIELD = "cyzHq";

    /** 队列类型 */
    public static final String DIRECT_TYPE = "direct";

    /** 队列类型 */
    public static final String CYZ_TO_HQ_QUEUE_NAME = "cyz.to.hq";

    /** 新增工单(虎趣 -> 远大支持论坛) */
    public static final String HQ_CYZ_REPAIRORDER_ADD_EXCHANGE = "hq.to.cyz.repairOrder.add";

    /** 新增回复(虎趣 -> 远大支持论坛) */
    public static final String HQ_CYZ_REPAIRORDER_REPLY_EXCHANGE = "hq.to.cyz.repairOrder.reply";

    /** 工单信息变更(远大支持论坛 -> 虎趣) */
    public static final String CYZ_HQ_REPAIRORDER_STATUS_EXCHANGE = "cyz.to.hq.repairOrder.status";
}