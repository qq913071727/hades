package com.innovation.ic.cyz.base.pojo.constant.deviceSelectionType;

/**
 * @desc   器件选型类型常量类
 * @author linuo
 * @time   2022年10月8日14:05:55
 */
public class DeviceSelectionTypeConstant {
    /** 父类id字段 */
    public static final String FATHER_ID_FIELD = "father_id";

    /** 主键id字段 */
    public static final String ID_FIELD = "id";

    /** 名称字段 */
    public static final String NAME_FIELD = "name";

    /** 子类字段 */
    public static final String CHILDREN_FIELD = "children";
}
