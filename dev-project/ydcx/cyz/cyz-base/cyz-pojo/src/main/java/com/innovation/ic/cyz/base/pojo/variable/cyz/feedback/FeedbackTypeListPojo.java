package com.innovation.ic.cyz.base.pojo.variable.cyz.feedback;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 信息反馈列表的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "FeedbackTypeListPojo", description = "信息反馈列表的pojo类")
public class FeedbackTypeListPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "值", dataType = "Integer")
    private Integer value;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

}
