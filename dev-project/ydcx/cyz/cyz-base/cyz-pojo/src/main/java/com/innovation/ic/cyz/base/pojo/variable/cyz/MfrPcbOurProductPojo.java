package com.innovation.ic.cyz.base.pojo.variable.cyz;

import com.innovation.ic.cyz.base.pojo.annotation.MfrParamCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * PCB厂家信息中,我们的产品的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MfrOurProductPojo", description = "PCB或者SMT厂家产品信息,我们的产品的pojo类")
@MfrParamCode(value = "our_products_item")
public class MfrPcbOurProductPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "图片", dataType = "String")
    @MfrParamCode(value = "our_products_image")
    private String img;

    @ApiModelProperty(value = "标题", dataType = "String")
    @MfrParamCode(value = "our_products_title")
    private String title;

    @ApiModelProperty(value = "文字", dataType = "String")
    @MfrParamCode(value = "our_products_text")
    private String txt;
}
