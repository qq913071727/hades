package com.innovation.ic.cyz.base.pojo.global;

import com.innovation.ic.cyz.base.pojo.constant.HttpStatusCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 调用API的返回对象
 *
 * @param <T>
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ApiResult<T>", description = "接口返回值对象")
@AllArgsConstructor
@NoArgsConstructor
public class ApiResult<T> implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "代码", dataType = "Integer")
    private Integer code;

    @ApiModelProperty(value = "消息", dataType = "String")
    private String message;

    @ApiModelProperty(value = "返回结果", dataType = "T")
    private T result = null;

    @ApiModelProperty(value = "是否成功", dataType = "Boolean")
    private Boolean success;

    private static final String OPERATION_SUCCESS = "操作成功!";
    private static final String OPERATION_FAILED = "操作失败!";

    /**
     * 操作成功
     */
    public static ApiResult success() {
        return new ApiResult<Object>(HttpStatusCode.SUCCESS, OPERATION_SUCCESS, null, true);
    }

    /**
     * 操作成功
     *
     * @param data 返回结果
     */
    public static <T> ApiResult<T> success(T data) {
        return new ApiResult<T>(HttpStatusCode.SUCCESS, OPERATION_SUCCESS, data, true);
    }

    /**
     * 操作成功
     *
     * @param data    返回结果
     * @param message 消息
     */
    public static <T> ApiResult<T> success(T data, String message) {
        return new ApiResult<T>(HttpStatusCode.SUCCESS, message, data, true);
    }


    /**
     * 操作失败
     */
    public static ApiResult failed(String message) {
        return new ApiResult<Object>(HttpStatusCode.ERROR, message, null, false);
    }

    /**
     * 操作失败
     *
     * @param data 返回结果
     */
    public static <T> ApiResult<T> failed(T data) {
        return new ApiResult<T>(HttpStatusCode.ERROR, OPERATION_FAILED, data, false);
    }

    /**
     * 操作失败
     *
     * @param data    返回结果
     * @param message 消息
     */
    public static <T> ApiResult<T> failed(T data, String message) {
        return new ApiResult<T>(HttpStatusCode.ERROR, message, data, false);
    }

    /**
     * 操作失败
     *
     * @param httpStatusCode
     * @param data
     * @param message
     * @param <T>
     * @return
     */
    public static <T> ApiResult<T> failed(int httpStatusCode, T data, String message) {
        return new ApiResult<T>(httpStatusCode, message, data, false);
    }
}
