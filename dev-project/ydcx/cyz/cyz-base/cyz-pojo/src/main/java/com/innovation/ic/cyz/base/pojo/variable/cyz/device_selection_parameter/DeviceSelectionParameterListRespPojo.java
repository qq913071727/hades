package com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_parameter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @desc   器件选型类型包含参数列表查询返回的pojo类
 * @author linuo
 * @time   2022年10月13日13:27:38
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DeviceSelectionParameterListRespPojo", description = "器件选型类型包含参数列表查询返回的pojo类")
public class DeviceSelectionParameterListRespPojo {
    @ApiModelProperty(value = "快速筛选", dataType = "List")
    private List<DeviceSelectionParameterParamPojo> fastScreen;

    @ApiModelProperty(value = "参数筛选", dataType = "List")
    private List<DeviceSelectionParameterParamPojo> paramScreen;
}