package com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc   工单分类列表返回的pojo类
 * @author linuo
 * @time   2022年9月23日11:22:30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderListRespPojo", description = "工单分类列表返回的pojo类")
public class RepairOrderTypeListRespPojo {
    @ApiModelProperty(value = "主键", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "一级分类名称", dataType = "String")
    private String firstClassName;

    @ApiModelProperty(value = "一级分类负责人", dataType = "String")
    private String firstClassHead;

    @ApiModelProperty(value = "二级分类名称", dataType = "String")
    private String secondClassName;

    @ApiModelProperty(value = "二级分类负责人", dataType = "String")
    private String secondClassHead;

    @ApiModelProperty(value = "三级分类名称", dataType = "String")
    private String thirdClassName;

    @ApiModelProperty(value = "三级分类负责人", dataType = "String")
    private String thirdClassHead;
}