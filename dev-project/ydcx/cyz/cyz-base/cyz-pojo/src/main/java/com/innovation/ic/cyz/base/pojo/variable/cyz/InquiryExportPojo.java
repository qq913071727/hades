package com.innovation.ic.cyz.base.pojo.variable.cyz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 咨询类导出数据的pojo
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "InquiryExportPojo", description = "咨询类导出数据的pojo")
public class InquiryExportPojo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "公司名称", dataType = "String")
    private String companyName;

    @ApiModelProperty(value = "姓名", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "手机号", dataType = "String")
    private String tel;

    @ApiModelProperty(value = "创建时间", dataType = "String")
    private String createTime;
}
