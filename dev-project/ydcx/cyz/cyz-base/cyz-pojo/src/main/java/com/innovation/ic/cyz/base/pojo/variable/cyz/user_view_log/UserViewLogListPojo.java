package com.innovation.ic.cyz.base.pojo.variable.cyz.user_view_log;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdListRelItemPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdUserPojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 用户浏览/点赞/收藏记录列表的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserViewLogListPojo", description = "用户浏览/点赞/收藏记录列表的pojo类")
public class UserViewLogListPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "AvdId", dataType = "Long")
    @TableId(value = "business_id", type = IdType.INPUT)
    private Long businessId;

    @ApiModelProperty(value = "类型(1-避坑指南阅读, 2-避坑指南点赞, 3-避坑指南收藏)", dataType = "Integer")
    @TableField(value = "type")
    private Integer type;

    @ApiModelProperty(value = "标题", dataType = "String")
    @TableField(value = "title")
    private String title;

    @ApiModelProperty(value = "封面地址", dataType = "String")
    @TableField(value = "cover")
    private String cover;

    @ApiModelProperty(value = "描述", dataType = "String")
    @TableField(value = "description")
    private String description;

    @ApiModelProperty(value = "标签", dataType = "List")
    private List<AvdListRelItemPojo> tags;

    @ApiModelProperty(value = "创建人id", dataType = "String")
    @TableField(value = "creator_id")
    private String creatorId;

    @ApiModelProperty(value = "作者信息", dataType = "com.innovation.ic.cyz.base.pojo.cyz.avd.AvdUserPojo")
    private AvdUserPojo user;

}
