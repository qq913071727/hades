package com.innovation.ic.cyz.base.pojo.constant;

/**
 * 咨询数据导出的文件
 */
public class InquiryExportFile {

    public static final String ALL = "导出全部";

    public static final String NEWEST = "导出最新";

    public static final String[] COLUMN_HEAD = {"公司名称", "姓名", "手机号", "提交时间"};
}
