package com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order;

import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_file.RepairOrderFileRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_record.RepairOrderRecordRespPojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @time   工单信息查询的返回Vo类
 * @author linuo
 * @time   2022年9月29日14:24:29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderInfoRespPojo", description = "工单信息查询的返回Vo类")
public class RepairOrderInfoRespPojo {
    @ApiModelProperty(value = "主键id", dataType = "Long")
    private Long id;

    @ApiModelProperty(value = "优先级", dataType = "String")
    private String priorityName;

    @ApiModelProperty(value = "负责人", dataType = "String")
    private String headUser;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String describe;

    @ApiModelProperty(value = "附件信息", dataType = "List")
    private List<RepairOrderFileRespPojo> fileList;

    @ApiModelProperty(value = "记录信息", dataType = "List")
    private List<RepairOrderRecordRespPojo> recordList;
}