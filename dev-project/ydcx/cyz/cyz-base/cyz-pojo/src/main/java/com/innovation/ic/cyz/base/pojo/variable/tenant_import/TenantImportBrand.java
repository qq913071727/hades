package com.innovation.ic.cyz.base.pojo.variable.tenant_import;

import java.util.Date;

public class TenantImportBrand {

    private int brandid;
    private String name;
    private String img;
    private int position;
    private int publish;
    private String deleted_at;
    private Date created_at;
    private Date updated_at;

    public void setBrandid(int brandid) {
        this.brandid = brandid;
    }

    public int getBrandid() {
        return brandid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPublish(int publish) {
        this.publish = publish;
    }

    public int getPublish() {
        return publish;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

}