package com.innovation.ic.cyz.base.pojo.constant.repairOrder;

/**
 * @desc   工单常量类
 * @author linuo
 * @time   2022年9月30日10:16:38
 */
public class RepairOrderConstant {
    /** 一级工单类型id字段 */
    public static final String FIRST_TYPE_ID_FIELD = "firstTypeId";

    /** 优先级字段 */
    public static final String PRIORITY_FIELD = "priority";

    /** 负责人字段 */
    public static final String HEAD_USER_FIELD = "headUser";

    /** 当前处理人字段 */
    public static final String DEAL_USER_FIELD = "dealUser";
}
