package com.innovation.ic.cyz.base.pojo.variable.cyz.avd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 避坑指南详情的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AvdDetailPojo", description = "避坑指南详情的pojo类")
public class AvdDetailPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "AvdId", dataType = "Long")
    private Long avdId;

    @ApiModelProperty(value = "类型(1-文章, 2-视频)", dataType = "Integer")
    private Integer module;

    @ApiModelProperty(value = "标题", dataType = "String")
    private String title;

    @ApiModelProperty(value = "封面地址", dataType = "String")
    private String cover;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String description;

    @ApiModelProperty(value = "正文", dataType = "String")
    private String paragraph;

    @ApiModelProperty(value = "阅读量", dataType = "Integer")
    private Integer viewNum;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    private Date create_date;

    @ApiModelProperty(value = "作者信息", dataType = "com.innovation.ic.cyz.base.pojo.cyz.avd.AvdUserPojo")
    private AvdUserPojo user;

    @ApiModelProperty(value = "视频地址", dataType = "String")
    private String video;

}
