package com.innovation.ic.cyz.base.pojo.variable.cyz.video;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VidRelPojo", description = "映射类")
public class VidRelPojo {
    public static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "映射表主键Id", dataType = "String")
    private String rid;
    @ApiModelProperty(value = "视频主键Id", dataType = "String")
    private String vid;
    @ApiModelProperty(value = "类型", dataType = "Integer")
    private Integer module;
    @ApiModelProperty(value = "id", dataType = "Long")
    private Long id;
    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;
    @ApiModelProperty(value = "排序", dataType = "Integer")
    private Integer orderIndex;
}
