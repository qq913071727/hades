package com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @desc   工单分类列表返回的pojo类
 * @author linuo
 * @time   2022年9月23日11:22:30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RepairOrderListResultPojo", description = "工单分类列表返回的pojo类")
public class RepairOrderTypeListResultPojo {
    @ApiModelProperty(value = "数据", dataType = "List")
    private List<RepairOrderTypeListRespPojo> data;

    @ApiModelProperty(value = "数据总数量", dataType = "Integer")
    private Integer count;
}