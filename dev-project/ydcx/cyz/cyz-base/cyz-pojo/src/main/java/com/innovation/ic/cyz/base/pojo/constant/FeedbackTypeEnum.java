package com.innovation.ic.cyz.base.pojo.constant;

import com.innovation.ic.cyz.base.pojo.annotation.Description;

/**
 * 信息反馈类型
 */
public class FeedbackTypeEnum {

    @Description(context = "功能建议")
    public static final int Type1 = 1;

    @Description(context = "BUG反馈")
    public static final int Type2 = 2;

    @Description(context = "账号问题")
    public static final int Type3 = 3;

    @Description(context = "其他")
    public static final int Type4 = 4;

}
