package com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_category;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 器件选型分类选项的pojo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PsCategoryItemPojo", description = "器件选型分类选项的pojo类")
public class PsCategoryItemPojo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分类Id", dataType = "String")
    private String categoryId;

    @ApiModelProperty(value = "分类名称", dataType = "String")
    private String categoryName;

    @ApiModelProperty(value = "子分类", dataType = "List")
    private List<PsCategoryItemPojo> children;

}
