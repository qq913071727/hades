package com.innovation.ic.cyz.base.pojo.variable.cyz;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MfrBackEndPageListPojo", description = "pcb和smt管理端返回pojo")
public class MfrBackEndPageListPojo {

    private Long total;

    private int pages;

    private List<MfrPcbSmtListPojo> data;


}
