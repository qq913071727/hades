package com.innovation.ic.cyz.base.pojo.variable.cyz.sln_search_option;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 方案列表选项（选项）
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SlnOptionItem", description = "方案列表选项（选项）")
public class SlnOptionItem {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "Name", dataType = "String")
    private String name;

}
