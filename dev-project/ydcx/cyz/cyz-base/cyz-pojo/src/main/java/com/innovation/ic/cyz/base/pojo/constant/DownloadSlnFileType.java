package com.innovation.ic.cyz.base.pojo.constant;

/**
 * 下载方案信息文件类型
 */
public class DownloadSlnFileType {

    /**
     * 图片
     */
    public static final int Image = 1;

    /**
     * 文件
     */
    public static final int File = 2;

}
