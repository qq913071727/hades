package com.innovation.ic.cyz.base.pojo.variable.cyz;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 区域
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "RegionPojo", description = "区域信息")
public class RegionPojo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id", dataType = "long")
    private String id;

    @ApiModelProperty(value = "上级id", dataType = "long")
    private String parentId;

    @ApiModelProperty(value = "名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "等级", dataType = "Integer")
    private Integer level;

    @ApiModelProperty(value = "排序", dataType = "Integer")
    private Integer orderIndex;

    @ApiModelProperty(value = "子级", dataType = "List")
    private List<RegionPojo> child;
}
