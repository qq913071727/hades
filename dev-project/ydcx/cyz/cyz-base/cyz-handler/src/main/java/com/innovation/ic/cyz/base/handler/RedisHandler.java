//package com.innovation.ic.cyz.base.handler;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.serializer.SerializerFeature;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.data.redis.core.ZSetOperations;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//import java.util.Set;
//
///**
// * redis处理器
// */
//@Component
//public class RedisHandler<T> extends AbstractHandler {
//    private static final Logger logger = LoggerFactory.getLogger(RedisHandler.class);
//
//    public void set(String key, String value, long time) {
//        redisManager.set(key, value, time);
//    }
//
//    public void set(String key, String value) {
//        redisManager.set(key, value);
//    }
//
//    public Object get(String key) {
//        return redisManager.get(key);
//    }
//
//    /**
//     * 设置过期时间
//     *
//     * @param key
//     * @param time
//     * @return
//     */
//    public Boolean expire(String key, long time) {
//        return redisManager.expire(key, time);
//    }
//
//    /**
//     * 删除一条记录
//     *
//     * @param key
//     * @return
//     */
//    public Boolean del(String key) {
//        return redisManager.del(key);
//    }
//
//    /**
//     * 批量删除
//     *
//     * @param keys
//     * @return
//     */
//    public Long del(List<String> keys) {
//        return redisManager.del(keys);
//    }
//
//    /**
//     * 批量删除
//     *
//     * @param keys
//     * @return
//     */
//    public Long del(Set<String> keys) {
//        return redisManager.del(keys);
//    }
//
//    /**
//     * 模糊查询，匹配key的前缀
//     *
//     * @param keyPrefix
//     * @return
//     */
//    public Set<String> keysPrefix(String keyPrefix) {
//        return redisManager.keysPrefix(keyPrefix);
//    }
//
//    public Long sAdd(String key, String... values) {
//        return redisManager.sAdd(key, values);
//    }
//
//    public Long sAdd(String key, long time, String... values) {
//        Long count = redisManager.sAdd(key, values);
//        expire(key, time);
//        return count;
//    }
//
//    /**
//     * 添加元素，有序集合是按照元素的score值由小到大排列
//     *
//     * @param key
//     * @param value
//     * @param score
//     * @return
//     */
//    public Boolean zAdd(String key, String value, double score) {
//        return redisManager.zAdd(key, value, score);
//    }
//
//    /**
//     * 添加元素
//     *
//     * @param key
//     * @param values
//     * @return
//     */
//    public Long zAdd(String key, Set<ZSetOperations.TypedTuple<String>> values) {
//        return redisManager.zAdd(key, values);
//    }
//
//    /**
//     * @param key
//     * @param values
//     * @return
//     */
//    public Long zRemove(String key, Object... values) {
//        return redisManager.zRemove(key, values);
//    }
//
//    public List page(String key, String pattern) {
//        return redisManager.page(key, pattern);
//    }
//
//
//    public void lPushAll(String key, List<Object> data) {
//        if (data != null && data.size() > 0) {
//            String[] resultSize = new String[data.size()];
//            for (int i = 0; i < data.size(); i++) {
//                String resultData = JSON.toJSONString(data.get(i), SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
//                resultSize[i] = resultData;
//            }
//            redisManager.lPushAll(key, resultSize);
//        }
//    }
//
//}
