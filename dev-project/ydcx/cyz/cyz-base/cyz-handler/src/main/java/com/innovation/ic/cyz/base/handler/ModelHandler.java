package com.innovation.ic.cyz.base.handler;

import com.innovation.ic.b1b.framework.util.IdUtil;
import com.innovation.ic.cyz.base.model.cyz.Feedback;
import com.innovation.ic.cyz.base.pojo.constant.GeneralStatus;
import com.innovation.ic.cyz.base.vo.cyz.feedback.FeedbackSaveVo;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ModelHandler extends AbstractHandler {

    /**
     * 将FeedbackSaveVo类型对象转换为Feedback类型对象
     * @param feedbackSaveVo
     * @return
     */
    public Feedback feedbackSaveVoToFeedback(FeedbackSaveVo feedbackSaveVo){
//        String userId = UserUtils.getUserId();
        Long newFeedbackId = IdUtil.snowflakeNextId();
        Feedback feedback = new Feedback();
        feedback.setId(newFeedbackId);
        feedback.setText(feedbackSaveVo.getText());
        feedback.setStatus(GeneralStatus.NORMAL);
//        feedback.setCreatorId(userId);
        feedback.setCreateDate(new Date());
        return feedback;
    }
}
