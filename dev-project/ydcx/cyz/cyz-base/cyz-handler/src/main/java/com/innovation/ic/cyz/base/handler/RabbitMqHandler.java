//package com.innovation.ic.cyz.base.handler;
//
//import com.rabbitmq.client.AMQP;
//import com.rabbitmq.client.Channel;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//import java.io.IOException;
//
///**
// * @desc   发送rabbitmq处理类
// * @author linuo
// * @time   2022年6月28日09:05:24
// */
//@Component
//public class RabbitMqHandler extends AbstractHandler {
//    private static final Logger logger = LoggerFactory.getLogger(RabbitMqHandler.class);
//
//    /**
//     * 推送rabbitMq消息
//     * @param username
//     */
//    public void sendRabbitMqMsg(String username, String message, String exchange){
//        try {
//            logger.info("向用户:[{}]推送rabbitMq消息,消息内容为:[{}]", username, message);
//            rabbitMqManager.basicPublish(exchange, username, null, message.getBytes());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 向指定交换机发送消息
//     * @param exchange 交换机名称
//     * @param queueName 队列名称
//     * @param routingKey 路由Key
//     * @param basicProperties 基础配置信息，默认为null
//     * @param b 消息二进制数据
//     * @throws IOException 抛出IO异常
//     */
//    public void basicPublish(String exchange, String queueName, String routingKey, AMQP.BasicProperties basicProperties, byte[] b) throws IOException {
//        rabbitMqManager.basicPublish(exchange, queueName, routingKey, basicProperties, b);
//    }
//
//    /**
//     * 获取channel
//     * @return 返回channel
//     */
//    public Channel getChannel(){
//        return rabbitMqManager.getChannel();
//    }
//
//    /**
//     * 推送rabbitMq消息
//     * @param param 参数
//     */
////    public void sendRabbitMqGroupMsg(Map<String, Object> param){
////        RabbitMqManager rabbitMqManager = (RabbitMqManager) param.get(Constants.QUEUE_MESSAGE_MANAGER);
////        String groupId = (String) param.get(Constants.GROUP_ID);
////        String fromUserAccount = (String) param.get(Constants.FROM_USER_ACCOUNT);
////        RefGroupAccountService refGroupAccountService = (RefGroupAccountService) param.get(Constants.REF_GROUP_ACCOUNT_SERVICE);
////        Map<String, Endpoint> onlineMap = (Map<String, Endpoint>) param.get(Constants.ONLINE_MAP);
////        UserGroupService userGroupService = (UserGroupService) param.get(Constants.USER_GROUP_SERVICE);
////
////        // 查询当前默认群组中除自己之外的用户账号
////        ServiceResult<List<String>> result = refGroupAccountService.getGroupAccountsNotHaveSelf(groupId, fromUserAccount);
////        if(result != null && result.getResult() != null){
////            List<String> userNames = result.getResult();
////            for (int i = 0; i < userNames.size(); i++) {
////                String userName = userNames.get(i);
////                // 判断用户是否在线，在线时推送rabbitMq消息
////                if(onlineMap.get(userName) != null){
////                    ServiceResult<List<AccountGroupLastContactPojo>> lastContact = userGroupService.findLastContact(userName);
////                    if(lastContact != null && lastContact.getResult() != null){
////                        String json = JSON.toJSONString(lastContact);
////                        // 推送消息
////                        logger.info("将消息推送给用户:[{}]的mq队列中,内容为:[{}]", userName, json);
////                        try {
////                            rabbitMqManager.basicPublish(userName, json);
////                        } catch (IOException e) {
////                            e.printStackTrace();
////                        }
////                    }
////                }
////            }
////        }
////    }
////
////    /**
////     * 推送rabbitMq消息
////     * @param param 参数
////     */
////    public void sendRabbitMqUserGroupMsg(Map<String, Object> param){
////        RabbitMqManager rabbitMqManager = (RabbitMqManager) param.get(Constants.QUEUE_MESSAGE_MANAGER);
////        RefUserGroupAccountService refUserGroupAccountService = (RefUserGroupAccountService) param.get(Constants.REF_USER_GROUP_ACCOUNT_SERVICE);
////        Integer userGroupId = (Integer) param.get(Constants.USER_GROUP_ID);
////        String fromUserAccount = (String) param.get(Constants.FROM_USER_ACCOUNT);
////        Map<String, Endpoint> onlineMap = (Map<String, Endpoint>) param.get(Constants.ONLINE_MAP);
////        UserGroupService userGroupService = (UserGroupService) param.get(Constants.USER_GROUP_SERVICE);
////
////        // 查询当前自定义群组中除自己之外的用户账号
////        ServiceResult<List<String>> result = refUserGroupAccountService.getUserGroupAccountsNotHaveSelf(userGroupId, fromUserAccount);
////        if(result != null && result.getResult() != null){
////            List<String> userNames = result.getResult();
////            for (int i = 0; i < userNames.size(); i++) {
////                String userName = userNames.get(i);
////                // 判断用户是否在线，在线时推送rabbitMq消息
////                if(onlineMap.get(userName) != null){
////                    ServiceResult<List<AccountGroupLastContactPojo>> lastContact = userGroupService.findLastContact(userName);
////                    if(lastContact != null && lastContact.getResult() != null){
////                        String json = JSON.toJSONString(lastContact);
////                        // 动态处理队列，不存在时创建队列
////                        rabbitMqManager.handleDynamicallyQuene(userName);
////                        // 推送消息
////                        logger.info("将消息推送给用户:[{}]的mq队列中,内容为:[{}]", userName, json);
////                        rabbitMqManager.basicPublish("im-end", userName, json);
////                    }
////                }
////            }
////        }
////    }
//}