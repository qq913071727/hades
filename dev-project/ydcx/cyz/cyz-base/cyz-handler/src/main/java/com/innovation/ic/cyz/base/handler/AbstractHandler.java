package com.innovation.ic.cyz.base.handler;

import com.innovation.ic.b1b.framework.manager.RedisManager;

import javax.annotation.Resource;

public abstract class AbstractHandler {
    @Resource
    protected RedisManager redisManager;
}