package com.innovation.ic.cyz.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 方案网的接口
 */
@Data
@Component
@ConfigurationProperties(prefix = "api.sln")
public class SlnApiConfig {

    /**
     * 调用方案网接口，获取方案数据
     */
    private String programme;

    /**
     * header中的authorization
     */
    private String authorization;
}
