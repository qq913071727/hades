package com.innovation.ic.cyz.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   erp接口地址配置类
 * @author linuo
 * @time   2022年9月16日09:40:55
 */
@Data
@Component
@ConfigurationProperties(prefix = "erp")
public class ErpInterfaceAddressConfig {
    /** 获取用户信息 */
    private String getInfo;
}