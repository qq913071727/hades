package com.innovation.ic.cyz.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "mybatis-plus")
public class MyBatisPlusParamConfig {

    /** 是否打印sql */
    private Class logImpl;
}
