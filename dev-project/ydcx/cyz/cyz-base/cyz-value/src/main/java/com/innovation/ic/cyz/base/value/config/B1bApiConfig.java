package com.innovation.ic.cyz.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * b1b的接口
 */
@Data
@Component
@ConfigurationProperties(prefix = "api.b1b")
public class B1bApiConfig {

    /**
     * 调用一次比一比接口，获取原厂数据
     */
    private String tenants;

    /**
     * header中的Authorization
     */
    private String authorization;
}
