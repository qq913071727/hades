package com.innovation.ic.cyz.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @desc   项目启动环境配置类
 * @author linuo
 * @time   2022年9月16日09:52:43
 */
@Data
@Component
@ConfigurationProperties(prefix = "run")
public class RunEnvConfig {
    /** 项目启动环境 */
    private String env;
}