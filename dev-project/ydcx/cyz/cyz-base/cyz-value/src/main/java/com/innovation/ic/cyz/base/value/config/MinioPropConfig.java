package com.innovation.ic.cyz.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "minio")
public class MinioPropConfig {
    /**
     * 连接url
     */
    private String endpoint;

    /**
     * 公钥
     */
    private String accessKey;

    /**
     * 私钥
     */
    private String secretKey;

    /**
     * 桶名
     */
    private String bucketName;

    /**
     * 下载文件前缀
     */
    private String urlBasePath;
}