package com.innovation.ic.cyz.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "host.cyz")
public class HostParamsConfig {
    /**
     * 图片host地址
     */
    private String cyzImage;

    /**
     * 视频host地址
     */
    private String cyzVideo;
}