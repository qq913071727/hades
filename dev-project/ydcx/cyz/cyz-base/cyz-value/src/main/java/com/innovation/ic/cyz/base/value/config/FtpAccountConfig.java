package com.innovation.ic.cyz.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * ftp账号
 */
@Data
@Component
@ConfigurationProperties(prefix = "ftp-config")
public class FtpAccountConfig {
    /**
     * ip地址
     */
    private String host;

    /**
     * 端口号
     */
    private Integer port;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 图片保存基础路径
     */
    private String imageSaveBasePath;

    /**
     * 视频保存基础路径
     */
    private String videoSaveBasePath;

    /**
     * 附件保存基础路径
     */
    private String protocolSaveBasePath;

    /**
     * 图片url基础路径
     */
    private String imageUrlBasePath;

    /**
     * 视频url基础路径
     */
    private String videoUrlBasePath;

    /**
     * 附件url基础路径
     */
    private String protocolUrlBasePath;
}