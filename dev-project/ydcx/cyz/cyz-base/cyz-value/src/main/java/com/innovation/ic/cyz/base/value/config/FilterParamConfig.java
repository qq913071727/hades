package com.innovation.ic.cyz.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * @desc   过滤器参数配置类
 * @author linuo
 * @time   2022年7月18日14:52:42
 */
@Data
@Component
@ConfigurationProperties(prefix = "filter")
public class FilterParamConfig {
    /**
     * 允许通过的路径
     */
    private List<String> allowPathList;

    /**
     * 键值对中的key
     */
    private String authorization;

    /**
     * 存储在header中的token
     */
    private String token;

    /**
     * cyz-web项目路径
     */
    private String webPath;

    /**
     * cyz-admin项目路径
     */
    private String adminPath;
}