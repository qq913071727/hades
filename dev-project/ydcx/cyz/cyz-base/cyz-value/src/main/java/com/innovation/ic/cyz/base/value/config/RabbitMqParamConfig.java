package com.innovation.ic.cyz.base.value.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import java.util.Map;

/**
 * @desc   rabbitmq交换机配置
 * @author linuo
 * @time   2022年7月13日15:54:33
 */
@Data
@Component
@ConfigurationProperties(prefix = "rabbitmq")
public class RabbitMqParamConfig {
    /** 地址 */
    private String host;

    /** 端口 */
    private int port;

    /** 用户名 */
    private String username;

    /** 密码 */
    private String password;

    /** 虚拟机地址 */
    private String virtualHost;

    /** 交换机配置 */
    private Map<String, String> exchange;

    /** 交换机配置 */
    private Map<String, String> queue;
}