package com.innovation.ic.cyz.base.value.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @ClassName ThreadPoolConfig
 * @Description 线程池配置类
 * @Date 2022/9/29
 * @Author myq
 */
@Data
@Component
@ConfigurationProperties(prefix = "threadpool")
public class ThreadPoolConfig {

    private Integer corePoolSize;

    private Integer maximumPoolSize;

    private Integer keepAliveTime;

    private Integer queueSize;

    private String threadNamePrefix;

}
