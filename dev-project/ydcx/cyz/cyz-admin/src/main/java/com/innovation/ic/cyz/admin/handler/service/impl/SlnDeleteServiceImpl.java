package com.innovation.ic.cyz.admin.handler.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.cyz.admin.handler.service.SlnCore;
import com.innovation.ic.cyz.admin.handler.service.SlnCoreService;
import com.innovation.ic.cyz.base.model.cyz.Sln;
import com.innovation.ic.cyz.base.pojo.enums.SlnSourceEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * @ClassName SlnDeleteServiceImpl
 * @Description 方案删除操作实现类
 * @Date 2022/9/21
 * @Author myq
 */
@Slf4j
@Service
public class SlnDeleteServiceImpl extends SlnCore implements SlnCoreService {

    @Override
    public void executing(JSONObject apiData) {
        Sln sln = new Sln();
        sln.setSlnId(apiData.getLong("id"));
        sln.setSource(SlnSourceEnum.BIB.getKey());
        Assert.isTrue(slnService.deleteSlnInfo(sln).getSuccess(), "删除方案数据异常");
    }
}
