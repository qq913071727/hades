package com.innovation.ic.cyz.admin.controller;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cyz.base.model.cyz.Categories;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.PsCategoryPropertyPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.UploadImagePojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionTypePojo;
import com.innovation.ic.cyz.base.service.cyz.CategoriesPropertyCyzService;
import com.innovation.ic.cyz.base.service.cyz.CategorieseCyzService;
import com.innovation.ic.cyz.base.service.cyz.DeviceSelectionService;
import com.innovation.ic.cyz.base.service.pve_standard.DeviceCategoriesService;
import com.innovation.ic.cyz.base.service.pve_standard.ProductSearchService;
import com.innovation.ic.cyz.base.vo.cyz.AddDeviceSelectionToConfigureVo;
import com.innovation.ic.cyz.base.vo.cyz.DeviceSelectionToConfigureVo;
import com.innovation.ic.cyz.base.vo.cyz.deviceselection.AddDeviceSelectionTypeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "器件选型", tags = "DeviceSelectionController")
@RestController
@RequestMapping("/api/v1/deviceSelection")
public class DeviceSelectionController extends AbstractController {
    @Autowired
    protected ProductSearchService productSearchService;

    @Autowired
    protected DeviceSelectionService deviceSelectionService;

    @Autowired
    protected CategorieseCyzService categorieseCyzService;

    @Autowired
    protected CategoriesPropertyCyzService categoriesPropertyCyzService;

    @Resource
    private DeviceCategoriesService deviceCategoriesService;

    /**
     * 查询元器件类别
     *
     * @return
     */
    @ApiOperation(value = "查询元器件类别")
    @GetMapping("/findByDeviceSelectionType")
    @ResponseBody
    public ResponseEntity<ApiResult<List<DeviceSelectionTypePojo>>> findByDeviceSelectionType() {
//        ServiceResult<List<DeviceSelectionTypePojo>> deviceSelectionTypeResult = deviceCategoriesService.findByDeviceSelectionType();
        ServiceResult<List<DeviceSelectionTypePojo>> deviceSelectionTypeResult = categorieseCyzService.findByDeviceSelectionType();

        ApiResult<List<DeviceSelectionTypePojo>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(deviceSelectionTypeResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    @ApiOperation(value = "根据分类获取分类属性")
    @ApiImplicitParam(name = "categoryId", value = "当前列表主键id", required = true, dataType = "String")
    @GetMapping("getCategoriesProperty")
    @ResponseBody
    public ResponseEntity<ApiResult<List<PsCategoryPropertyPojo>>> getPsCategoriesProperty(@RequestParam String categoryId) {
        ApiResult<List<PsCategoryPropertyPojo>> result = new ApiResult<>();
        //找到所有配置
//        ServiceResult<List<com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property.PsCategoryPropertyPojo>> serviceResult = productSearchService.getPsCategoriesProperty(categoryId);
        //通过id获取器件选型基础数据包含上级数据
        List<String> categoriesList = categorieseCyzService.selectFindId(categoryId);
        //获取件选型配置属性
        ServiceResult<List<PsCategoryPropertyPojo>> serviceResult = categoriesPropertyCyzService.selectCategoriesProperty(categoriesList);
        ServiceResult<List<PsCategoryPropertyPojo>> listServiceResult = deviceSelectionService.getPsCategoriesProperty(categoryId, serviceResult.getResult());
        result.setResult(listServiceResult.getResult());
        result.setCode(listServiceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(listServiceResult.getSuccess());
        result.setMessage(listServiceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    /**
     * 添加元器件类别
     *
     * @return
     */
    @ApiOperation(value = "添加元器件类别")
    @PostMapping("/addDeviceSelectionType")
    @ResponseBody
    public ResponseEntity<ApiResult> addDeviceSelectionType(@RequestBody AddDeviceSelectionTypeVo addDeviceSelectionTypeVo) {
        Map<String, String> map = new HashMap<>();
        ApiResult apiResult = new ApiResult<>();
        if (!StringUtils.validateParameter(addDeviceSelectionTypeVo.getId())) {
            map.put("status", "failed");
            return new ResponseEntity<>(ApiResult.failed(map, "id不能为空"), HttpStatus.OK);
        }
        //找到根节点
        addDeviceSelectionTypeVo.setCreatorId(getUserId());
//        Category categoryRoot = deviceCategoriesService.findById(addDeviceSelectionTypeVo.getId());
        Categories categories = categorieseCyzService.findById(addDeviceSelectionTypeVo.getId());
        ServiceResult serviceResult = deviceSelectionService.addDeviceSelectionType(categories, addDeviceSelectionTypeVo);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 更新器件选型图片尺寸和路径
     *
     * @return
     */
    @ApiOperation(value = "更新器件选型图片尺寸和路径")
    @PostMapping("/updateDeviceSelectionTypePicture")
    @ResponseBody
    public ResponseEntity<ApiResult> updateDeviceSelectionTypePicture(@RequestBody UploadImagePojo uploadImagePojo) {
        Map<String, String> map = new HashMap<>();
        ApiResult apiResult = new ApiResult<>();
        if (!StringUtils.validateParameter(uploadImagePojo.getAbsoluteUrl())) {
            map.put("status", "failed");
            return new ResponseEntity<>(ApiResult.failed(map, "图片路径不合法"), HttpStatus.OK);
        }
        if (!StringUtils.validateParameter(uploadImagePojo.getUniqueId())) {
            map.put("status", "failed");
            return new ResponseEntity<>(ApiResult.failed(map, "uniqueId不合法"), HttpStatus.OK);
        }
        //找到根节点
        ServiceResult serviceResult = deviceSelectionService.updateDeviceSelectionTypePicture(uploadImagePojo);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 添加器件类别配置参数
     *
     * @return
     */
    @ApiOperation(value = "添加器件类别配置参数")
    @PostMapping("/addDeviceSelectionToConfigure")
    @ResponseBody
    public ResponseEntity<ApiResult> addDeviceSelectionToConfigure(@RequestBody AddDeviceSelectionToConfigureVo addDeviceSelectionToConfigureVo) {
        Map<String, String> map = new HashMap<>();
        ApiResult apiResult = new ApiResult<>();
        if (!StringUtils.validateParameter(addDeviceSelectionToConfigureVo.getDeviceSelectionTypeId())) {
            map.put("status", "failed");
            return new ResponseEntity<>(ApiResult.failed(map, "id不能为空"), HttpStatus.OK);
        }
        if (CollectionUtils.isEmpty(addDeviceSelectionToConfigureVo.getDeviceSelectionToConfigureVo())) {
            map.put("status", "failed");
            return new ResponseEntity<>(ApiResult.failed(map, "deviceSelectionToConfigureVo不能为空"), HttpStatus.OK);
        }
        List<DeviceSelectionToConfigureVo> deviceSelectionToConfigureVo = addDeviceSelectionToConfigureVo.getDeviceSelectionToConfigureVo();
        for (DeviceSelectionToConfigureVo selectionToConfigureVo : deviceSelectionToConfigureVo) {
            if (selectionToConfigureVo.getType() == null || StringUtils.isEmpty(selectionToConfigureVo.getCategoriesPropertyId())) {
                map.put("status", "failed");
                return new ResponseEntity<>(ApiResult.failed(map, "deviceSelectionToConfigureVo参数不合法"), HttpStatus.OK);
            }
        }
        ServiceResult serviceResult = deviceSelectionService.addDeviceSelectionToConfigure(addDeviceSelectionToConfigureVo);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 删除器件分类
     *
     * @return
     */
    @ApiOperation(value = "删除器件分类")
    @PostMapping("/deleteDeviceSelectionType")
    @ResponseBody
    public ResponseEntity<ApiResult> deleteDeviceSelectionType(@RequestBody String id) {
        Map<String, String> map = new HashMap<>();
        ApiResult apiResult = new ApiResult<>();
        if (!StringUtils.validateParameter(id)) {
            map.put("status", "failed");
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
        ServiceResult serviceResult = deviceSelectionService.deleteDeviceSelectionType(id);
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询所有器件分类配置
     *
     * @return
     */
    @ApiOperation(value = "查询所有器件分类配置")
    @PostMapping("/findByDeviceSelectionToConfigure")
    @ResponseBody
    public ResponseEntity<ApiResult<List<DeviceSelectionTypePojo>>> findByDeviceSelectionToConfigure() {
        ApiResult<List<DeviceSelectionTypePojo>> apiResult = new ApiResult<>();
        ServiceResult<List<DeviceSelectionTypePojo>> serviceResult = deviceSelectionService.findByDeviceSelectionToConfigure();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 添加元器件类别
     *
     * @return
     *//*
    @ApiOperation(value = "添加元器件类别")
    @PostMapping("/addDeviceSelection")
    @ResponseBody
    public ResponseEntity<ApiResult> addDeviceSelection1(@RequestBody AddDeviceSelectionVo addDeviceSelectionVo) {
        Map<String, String> map = new HashMap<>();
        if (!StringUtils.validateParameter(addDeviceSelectionVo.getExternalUniqueId())) {
            map.put("status", "failed");
            return new ResponseEntity<>(ApiResult.failed(map, "externalUniqueId不能为空"), HttpStatus.OK);
        }
        if (CollectionUtils.isEmpty(addDeviceSelectionVo.getChildIds())) {
            map.put("status", "failed");
            return new ResponseEntity<>(ApiResult.failed(map, "childIds不能为空"), HttpStatus.OK);
        }
        if (CollectionUtils.isEmpty(addDeviceSelectionVo.getCategoriesPropertyMap())) {
            map.put("status", "failed");
            return new ResponseEntity<>(ApiResult.failed(map, "categoryPropertyIs不能为空"), HttpStatus.OK);
        }
        //找到根节点
        QueryWrapper<Category> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ID", addDeviceSelectionVo.getExternalUniqueId());
        //最外层节点
        ServiceResult<List<Category>> categoryRoot = deviceCategoriesService.categoriesQueryWrapper(queryWrapper);
        //子节点
        ServiceResult<List<Category>> categoryChild = deviceCategoriesService.categoriesQueryWrapper(queryWrapper);

        List<String> categoriesPropertyIds = new ArrayList<>();
        List<Map<String, String>> categoryPropertyIMap = addDeviceSelectionVo.getCategoriesPropertyMap();
        for (Map<String, String> categoryProperty : categoryPropertyIMap) {
            categoriesPropertyIds.add(categoryProperty.get("categoriesPropertyId"));
        }
        List<CategoriesPropertyVo> categoriesPropertyVoList = new ArrayList<>();
        //选择的参数查询
        ServiceResult<List<CategoriesProperty>> categoriesPropertys = deviceCategoriesService.findByCategoriesPropertyIds(categoriesPropertyIds);
        List<CategoriesProperty> CategoriesPropertyResult = categoriesPropertys.getResult();
        //封装参数类型
        for (CategoriesProperty categoriesProperty : CategoriesPropertyResult) {
            for (Map<String, String> categoryProperty : categoryPropertyIMap) {
                if (categoriesProperty.getId().equals(categoryProperty.get("categoriesPropertyId"))) {
                    CategoriesPropertyVo categoriesPropertyVo = new CategoriesPropertyVo();
                    BeanUtils.copyProperties(categoriesProperty, categoriesPropertyVo);
                    categoriesPropertyVo.setType(Integer.valueOf(categoryProperty.get("type")));
                    categoriesPropertyVoList.add(categoriesPropertyVo);
                }
            }
        }
        AddDeviceSelectionParamVo addDeviceSelectionParamVo = new AddDeviceSelectionParamVo();
        addDeviceSelectionParamVo.setCategoryRoot(categoryRoot.getResult().get(0));
        addDeviceSelectionParamVo.setCategoryChild(categoryChild.getResult());
        addDeviceSelectionParamVo.setCategoriesPropertys(categoriesPropertyVoList);
        ServiceResult deviceSelectionTypeResult = deviceSelectionService.addDeviceSelection(addDeviceSelectionParamVo);
        ApiResult<List<DeviceSelectionTypePojo>> apiResult = new ApiResult<>();
        apiResult.setSuccess(deviceSelectionTypeResult.getSuccess());
        apiResult.setMessage(deviceSelectionTypeResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }*/
}