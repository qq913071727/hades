/*
package com.innovation.ic.cyz.admin.handler.factory;


import com.innovation.ic.cyz.admin.config.SlnThreadPoolConfig;
import com.innovation.ic.cyz.admin.handler.worker.Worker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

*/
/**
 * @ClassName WorkHelper
 * @Description 执行器帮助助手
 * @Date 2022/9/16
 * @Author myq
 *//*

@Slf4j
@Component
public class WorkHelper {

    //默认的超时时间
    private final Long TIMEOUT = 5L;

    */
/**
     * 线程池类
     *//*

    @Resource(name = SlnThreadPoolConfig.SLN_EXECUTOR_SERVICE)
    private ExecutorService executorService;

    */
/**
     * @Description: 处理业务
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/1615:42
     *//*

    public String commit(Worker worker) {
        try {
            return executorService.submit(worker).get(TIMEOUT, TimeUnit.SECONDS);
        } catch (Exception e) {
            log.error("当前线程池：" + Thread.currentThread().getName() + "执行任务异常：" + e);
            throw new RuntimeException("上传图片异常");
        }
    }

}
*/
