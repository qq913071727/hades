package com.innovation.ic.cyz.admin.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cyz.base.model.cyz.AvdTag;
import com.innovation.ic.cyz.base.model.cyz.AvdType;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.AvdListPojo;
import com.innovation.ic.cyz.base.service.cyz.AvdService;
import com.innovation.ic.cyz.base.vo.cyz.avd.AvdSaveVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @ClassName AvdController
 * @Description 避坑指南
 * @Date 2022/9/26
 * @Author myq
 */
@Api("避坑指南")
@RestController
@RequestMapping("/api/v1/avd")
public class AvdController extends AbstractController {
    @Autowired
    private AvdService avdService;

    @PostMapping("add")
    @ApiOperation("新增")
    public ResponseEntity<ApiResult> add(@RequestBody AvdSaveVo avdSaveVo) {
        ServiceResult rs = avdService.insertSave(avdSaveVo, getUserId());
        return new ResponseEntity<>(ApiResult.success(rs.getResult()), HttpStatus.OK);
    }

    @PutMapping("update")
    @ApiOperation("修改")
    public ResponseEntity<ApiResult> update(@RequestBody AvdSaveVo avdSaveVo) {
        ServiceResult rs = avdService.updateSave(avdSaveVo, avdSaveVo.getId(), getUserId());
        return new ResponseEntity<>(ApiResult.success(rs.getResult()), HttpStatus.OK);
    }

    @GetMapping("getAvdTypes")
    @ApiOperation("获取类别列表")
    public ResponseEntity<ApiResult> getAvdTypes() {
        ServiceResult<List<AvdType>> rs = avdService.getAvdTypes();
        return new ResponseEntity<>(ApiResult.success(rs.getResult()), HttpStatus.OK);
    }

    @GetMapping("getAvdTags")
    @ApiOperation("获取类别列表")
    public ResponseEntity<ApiResult> getAvdTags() {
        ServiceResult<List<AvdTag>> rs = avdService.getAvdTags();
        return new ResponseEntity<>(ApiResult.success(rs.getResult()), HttpStatus.OK);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int", required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", required = true, defaultValue = "20")
    })
    @GetMapping("page")
    @ApiOperation("避坑指南分页列表")
    public ResponseEntity<ApiResult<PageInfo<AvdListPojo>>> page(@RequestParam("pageNo") Integer pageNo,
                                                                 @RequestParam("pageSize") Integer pageSize,
                                                                 String userName,
                                                                 Integer module,
                                                                 Long typeId,
                                                                 String createDate) {
        ServiceResult<PageInfo<AvdListPojo>> rs = avdService.page(pageNo, pageSize, typeId, module, userName, createDate);
        return new ResponseEntity<>(ApiResult.success(rs.getResult()), HttpStatus.OK);
    }
}