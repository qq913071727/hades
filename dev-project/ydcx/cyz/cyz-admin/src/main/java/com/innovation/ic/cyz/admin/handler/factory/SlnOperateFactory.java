package com.innovation.ic.cyz.admin.handler.factory;

import com.innovation.ic.b1b.framework.util.SpringContextUtil;
import com.innovation.ic.cyz.admin.handler.service.SlnCoreService;
import com.innovation.ic.cyz.admin.handler.service.impl.SlnDeleteServiceImpl;
import com.innovation.ic.cyz.admin.handler.service.impl.SlnInsertServiceImpl;
import com.innovation.ic.cyz.admin.handler.service.impl.SlnUpdateServiceImpl;
import com.innovation.ic.cyz.base.pojo.enums.OperateEnum;
import org.springframework.stereotype.Component;

/**
 * @ClassName SlnOperateFactory
 * @Description 方案操作类型单例工厂
 * @Date 2022/9/21
 * @Author myq
 */
@Component
public class SlnOperateFactory {
    private final String SLN_INSERT_SERVICE_IMPL = "slnInsertServiceImpl";
    private final String SLN_UPDATE_SERVICE_IMPL = "slnUpdateServiceImpl";
    private final String SLN_DELETE_SERVICE_IMPL = "slnDeleteServiceImpl";

    /**
     * @Description: 根据操作类型初始化对应的实现类
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2911:24
     */
    public SlnCoreService choose(String operateEnum) {
        if (operateEnum == null) {
            throw new RuntimeException("方案操作工厂无法创建方案操作类");
        }

        if (OperateEnum.INSERT.getKey().equals(operateEnum)) {
            return SpringContextUtil.getBean(SLN_INSERT_SERVICE_IMPL, SlnInsertServiceImpl.class);
        } else if (OperateEnum.UPDATE.getKey().equals(operateEnum)) {
            return SpringContextUtil.getBean(SLN_UPDATE_SERVICE_IMPL, SlnUpdateServiceImpl.class);
        } else if (OperateEnum.DELETE.getKey().equals(operateEnum)) {
            return SpringContextUtil.getBean(SLN_DELETE_SERVICE_IMPL, SlnDeleteServiceImpl.class);
        } else {
            throw new RuntimeException("非法的参数异常");
        }
    }
}