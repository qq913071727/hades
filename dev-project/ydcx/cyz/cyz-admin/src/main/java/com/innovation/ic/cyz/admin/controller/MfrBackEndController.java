package com.innovation.ic.cyz.admin.controller;

import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrBackEndPageListPojo;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_property.PsCategoryPropertyPojo;
import com.innovation.ic.cyz.base.service.cyz.MfrBackEndService;
import com.innovation.ic.cyz.base.vo.cyz.AddMfrBackEndVo;
import com.innovation.ic.cyz.base.vo.cyz.MfrBackEndPageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.List;

@Api(value = "PCB和SMT后台管理", tags = "MfrBackEndController")
@RestController
@RequestMapping("/api/v1/deviceSelection")
public class MfrBackEndController extends AbstractController {

    @Resource
    private MfrBackEndService mfrBackEndService;

    @ApiOperation(value = "添加PCB和SMT,或者修改(修改带mfrId)")
    @PostMapping("addMfrBackEnd")
    @ResponseBody
    public ResponseEntity<ApiResult> addMfrBackEndVo(@RequestBody AddMfrBackEndVo addMfrBackEndVo) {
        ApiResult<List<PsCategoryPropertyPojo>> result = new ApiResult<>();
        ServiceResult serviceResult = mfrBackEndService.addMfrBackEndVo(addMfrBackEndVo);
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "删除PCB和SMT")
    @PostMapping("deleteMfrBackEnd")
    @ResponseBody
    public ResponseEntity<ApiResult> addMfrBackEndVo(@RequestParam Long mfrId) {
        if (null == mfrId) {
            String message = "参数id不能为空";
            ApiResult<Boolean> apiResult = new ApiResult<Boolean>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());

            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ApiResult<List<PsCategoryPropertyPojo>> result = new ApiResult<>();
        mfrBackEndService.deleteMfrBackEnd(mfrId);
        result.setSuccess(Boolean.TRUE);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "查询列表")
    @PostMapping("findByPage")
    @ResponseBody
    public ResponseEntity<ApiResult<MfrBackEndPageListPojo>> findByPage(@RequestBody MfrBackEndPageVo mfrBackEndPageVo) throws Exception {
        ServiceResult<MfrBackEndPageListPojo> serviceResult = mfrBackEndService.findByPage(mfrBackEndPageVo);
        return new ResponseEntity<>(ApiResult.success(serviceResult.getResult()), HttpStatus.OK);
    }
}