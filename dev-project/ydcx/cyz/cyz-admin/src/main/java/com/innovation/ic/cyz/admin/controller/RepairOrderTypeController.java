package com.innovation.ic.cyz.admin.controller;

import com.google.common.base.Strings;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type.FirstLevelRepairOrderTypeQueryRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type.RepairOrderTypeRespPojo;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type.RepairOrderTypeListResultPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order_type_head.RepairOrderTypeFaeListRespPojo;
import com.innovation.ic.cyz.base.vo.cyz.repairOrderType.RepairOrderTypeAddVo;
import com.innovation.ic.cyz.base.vo.cyz.repairOrderType.RepairOrderTypeEditVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Api(value = "工单分类API", tags = "RepairOrderTypeController")
@RestController
@RequestMapping("/api/v1/repairOrderType")
public class RepairOrderTypeController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(RepairOrderTypeController.class);

    /**
     * 工单分类列表查询
     */
    @ApiOperation(value = "工单分类列表查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int", required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", required = true, defaultValue = "20")
    })
    @RequestMapping(value = "/queryList/{pageNo}/{pageSize}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> queryList(@PathVariable("pageNo")Integer pageNo, @PathVariable("pageSize")Integer pageSize, HttpServletRequest request, HttpServletResponse response) {
        ServiceResult<RepairOrderTypeListResultPojo> result = repairOrderTypeService.queryList(pageNo, pageSize);

        ApiResult<RepairOrderTypeListResultPojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 工单分类添加
     */
    @ApiOperation(value = "工单分类添加")
    @ApiImplicitParam(name = "RepairOrderTypeAddVo", value = "工单分类添加的vo类", required = true, dataType = "RepairOrderTypeAddVo")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> add(@RequestBody RepairOrderTypeAddVo repairOrderTypeAddVo, HttpServletRequest request, HttpServletResponse response) {
        if (Strings.isNullOrEmpty(repairOrderTypeAddVo.getFirstClassName()) || Strings.isNullOrEmpty(repairOrderTypeAddVo.getFirstClassHead())) {
            String message = "调用接口[/api/v1/repairOrderType/add]时，参数firstClassName和firstClassHead不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> result = repairOrderTypeService.add(repairOrderTypeAddVo);

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 工单分类编辑
     */
    @ApiOperation(value = "工单分类编辑")
    @ApiImplicitParam(name = "RepairOrderTypeEditVo", value = "工单分类编辑的vo类", required = true, dataType = "RepairOrderTypeEditVo")
    @RequestMapping(value = "/edit", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> edit(@RequestBody RepairOrderTypeEditVo repairOrderTypeEditVo, HttpServletRequest request, HttpServletResponse response) {
        if (repairOrderTypeEditVo.getId() == null || Strings.isNullOrEmpty(repairOrderTypeEditVo.getFirstClassHead())) {
            String message = "调用接口[/api/v1/repairOrderType/edit]时，参数id和firstClassHead不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> result = repairOrderTypeService.edit(repairOrderTypeEditVo);

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(result.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(result.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 工单分类信息获取
     */
    @ApiOperation(value = "工单分类信息获取")
    @ApiImplicitParam(name = "id", value = "工单分类主键id", required = true, dataType = "Long")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> info(@PathVariable("id") Long id, HttpServletRequest request, HttpServletResponse response) {
        if (id == null) {
            String message = "调用接口[/api/v1/repairOrderType/info]时，参数id不能为空";
            log.warn(message);
            ApiResult<RepairOrderTypeRespPojo> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<RepairOrderTypeRespPojo> result = repairOrderTypeService.info(id);

        ApiResult<RepairOrderTypeRespPojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 工单分类删除
     */
    @ApiOperation(value = "工单分类删除")
    @ApiImplicitParam(name = "id", value = "工单分类主键id", required = true, dataType = "Long")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> delete(@PathVariable("id") Long id, HttpServletRequest request, HttpServletResponse response) {
        if (id == null) {
            String message = "调用接口[/api/v1/repairOrderType/delete]时，参数id不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> result = repairOrderTypeService.delete(id);

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(result.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(result.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 获取FAE列表
     */
    @ApiOperation(value = "获取FAE列表")
    @RequestMapping(value = "/getFaeList", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> getFaeList(HttpServletRequest request, HttpServletResponse response) {
        ServiceResult<List<RepairOrderTypeFaeListRespPojo>> result = repairOrderTypeService.getFaeList();

        ApiResult<List<RepairOrderTypeFaeListRespPojo>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 获取一级分类
     */
    @ApiOperation(value = "获取一级分类")
    @RequestMapping(value = "/getFirstTypeList", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> getFirstTypeList(HttpServletRequest request, HttpServletResponse response) {
        ServiceResult<List<FirstLevelRepairOrderTypeQueryRespPojo>> result = repairOrderTypeService.getFirstTypeList();

        ApiResult<List<FirstLevelRepairOrderTypeQueryRespPojo>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}