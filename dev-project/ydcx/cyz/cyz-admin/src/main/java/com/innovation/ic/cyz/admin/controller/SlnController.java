package com.innovation.ic.cyz.admin.controller;

import com.github.pagehelper.PageInfo;
import com.innovation.ic.cyz.base.model.cyz.Sln;
import com.innovation.ic.cyz.base.model.cyz.SlnIde;
import com.innovation.ic.cyz.base.model.cyz.SlnLikeTag;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.SlnListPojo;
import com.innovation.ic.cyz.base.service.cyz.SlnService;
import com.innovation.ic.cyz.base.vo.cyz.SlnListVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @ClassName SlnController
 * @Description 方案控制器
 * @Date 2022/9/15
 * @Author myq
 */
@RestController
@RequestMapping("api/v1/sln")
@Api(value = "方案控制器", tags = "slnController")
public class SlnController extends AbstractController{
    @Autowired
    private SlnService slnService;

    /**
     * @Description: 查询方案数据列表
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2117:48
     */
    @PostMapping(value = "page", produces = {"application/json; charset=utf-8"})
    @ApiOperation("查询方案数据列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int", required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", required = true, defaultValue = "20"),
    })
    public ResponseEntity<ApiResult<PageInfo<SlnListPojo>>> page(@RequestParam("pageNo") int pageNo,
                                                                 @RequestParam("pageSize") int pageSize,
                                                                 @RequestBody SlnListVo slnListVo) {
        ServiceResult<PageInfo<SlnListPojo>> result = slnService.page(pageNo, pageSize, slnListVo);

        return new ResponseEntity<>(ApiResult.success(result.getResult()), HttpStatus.OK);
    }

    /**
     * @Description: 热门领域列表
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2210:35
     */
    @GetMapping("/hotNameList")
    @ApiOperation("热门领域列表")
    public ResponseEntity<ApiResult<List<SlnLikeTag>>> hotNameList() {
        ServiceResult<List<SlnLikeTag>> slnLikeTagServiceResult = slnService.hotNameList();

        return new ResponseEntity<>(ApiResult.success(slnLikeTagServiceResult.getResult()), HttpStatus.OK);
    }

    /**
     * @Description: 开发平台列表
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2210:43
     */
    @GetMapping("/slnIdeList")
    @ApiOperation("开发平台列表")
    public ResponseEntity<ApiResult<List<SlnIde>>> slnIdeList() {
        ServiceResult<List<SlnIde>> slnLikeTagServiceResult = slnService.slnIdeList();

        return new ResponseEntity<>(ApiResult.success(slnLikeTagServiceResult.getResult()), HttpStatus.OK);
    }

    @PutMapping("/update")
    @ApiOperation("审核方案")
    public ResponseEntity<ApiResult> update(@RequestBody Sln sln) {

        ServiceResult res = slnService.update(sln);

        return new ResponseEntity<>(ApiResult.success(res.getResult()), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("删除方案")
    public ResponseEntity<ApiResult> delete(@PathVariable("id") Long id) {

        ServiceResult res = slnService.delete(id);

        return new ResponseEntity<>(ApiResult.success(res.getResult()), HttpStatus.OK);
    }
}