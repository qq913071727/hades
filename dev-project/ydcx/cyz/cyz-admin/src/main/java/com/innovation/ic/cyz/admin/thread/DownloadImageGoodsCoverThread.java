package com.innovation.ic.cyz.admin.thread;

import com.innovation.ic.cyz.base.model.cyz.Sln;
import com.innovation.ic.cyz.base.service.cyz.ImportSlnService;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;

@Slf4j
public class DownloadImageGoodsCoverThread implements Callable<String> {


    private ImportSlnService importSlnService;


    private Sln sln;


    public DownloadImageGoodsCoverThread(ImportSlnService importSlnService, Sln sln) {
        this.importSlnService = importSlnService;
        this.sln = sln;
    }


    @Override
    public String call() throws Exception {
        log.info(">>>>>>>>>>>>>>>>>>>>>>>开始下载并上传封面图片>>>>>>>>>>>>>>>>>>>>>>>");
        return importSlnService.downloadImageGoodsCover(sln);
    }
}
