/*
package com.innovation.ic.cyz.admin.handler.worker;


import com.innovation.ic.b1b.framework.util.SpringContextUtil;
import com.innovation.ic.cyz.base.service.cyz.ImportSlnService;

import java.util.concurrent.Callable;


*/
/**
 * @ClassName WorkService
 * @Description 任务接口类
 * @Date 2022/9/16
 * @Author myq
 *//*

public abstract class Worker implements Callable<String> {

    protected ImportSlnService importSlnService;

    public Worker() {
        this.importSlnService = SpringContextUtil.getBean(ImportSlnService.IMPORT_SLN_SERVICE_IMPL_BEAN_NAME);
    }

    abstract String action();

    @Override
    public String call() throws Exception {
        return this.action();
    }
}
*/
