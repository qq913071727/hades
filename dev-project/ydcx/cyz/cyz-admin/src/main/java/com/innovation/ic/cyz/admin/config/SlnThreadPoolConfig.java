//package com.innovation.ic.cyz.admin.config;
//
//import com.innovation.ic.cyz.base.value.config.ThreadPoolConfig;
//import io.netty.util.concurrent.DefaultThreadFactory;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Lazy;
//import org.springframework.context.annotation.Scope;
//
//import javax.annotation.Resource;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.LinkedBlockingQueue;
//import java.util.concurrent.ThreadPoolExecutor;
//import java.util.concurrent.TimeUnit;
//
///**
// * @ClassName ThreadPoolConfig
// * @Description 线程池配置 bean-type
// * @Date 2022/9/29
// * @Author myq
// */
//@Configuration
//@Slf4j
//@ConfigurationProperties(prefix = "sln.thread-pool")
//public class SlnThreadPoolConfig extends ThreadPoolConfig {
//
//    /**
//     * bean名称，方便后续查找并维护此bean
//     */
//    public static final String SLN_EXECUTOR_SERVICE = "slnExecutorService";
//
//    @Resource
//    private ThreadPoolConfig threadPoolConfig;
//
//
//    /**
//     * @Description: 创建一个单例，懒加载的bean
//     * @Params:
//     * @Return:
//     * @Author: Mr.myq
//     * @Date: 2022/9/2910:38
//     */
//     @Lazy
//     @Bean("slnExecutorService")
//     @Scope("singleton")
//     public ExecutorService getSlnExecutorService(){
//         log.info("sin-work-pool init");
//             return new ThreadPoolExecutor(threadPoolConfig.getCorePoolSize(), threadPoolConfig.getMaximumPoolSize(),
//                 getKeepAliveTime(), TimeUnit.MILLISECONDS,
//                 new LinkedBlockingQueue<Runnable>(getQueueSize()),
//                 new DefaultThreadFactory(threadPoolConfig.getThreadNamePrefix(), true, 5));
//     }
//
//}
