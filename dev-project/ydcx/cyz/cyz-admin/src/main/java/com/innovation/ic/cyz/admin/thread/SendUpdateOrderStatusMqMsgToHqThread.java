package com.innovation.ic.cyz.admin.thread;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.cyz.base.model.cyz.RepairOrder;
import com.innovation.ic.cyz.base.model.cyz.RepairOrderRecord;
import com.innovation.ic.cyz.base.pojo.constant.RabbitMqConstants;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.rabbit_mq.CyzToHqEditStatusPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.rabbit_mq.FilesPojo;
import com.innovation.ic.cyz.base.service.cyz.RepairOrderFileService;
import com.innovation.ic.cyz.base.service.cyz.RepairOrderRecordService;
import com.innovation.ic.cyz.base.service.cyz.impl.ServiceHelper;
import com.innovation.ic.cyz.base.value.config.RabbitMqParamConfig;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @desc   推送更新工单状态的rabbitmq消息到虎趣
 * @author linuo
 * @time   2022年11月21日14:38:22
 */
public class SendUpdateOrderStatusMqMsgToHqThread extends Thread {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private RepairOrderRecordService repairOrderRecordService;

    private RepairOrderFileService repairOrderFileService;

    private ServiceHelper serviceHelper;

    private RepairOrder repairOrder;

    private RabbitMqParamConfig rabbitMqParamConfig;

    public SendUpdateOrderStatusMqMsgToHqThread(RabbitMqParamConfig rabbitMqParamConfig, ServiceHelper serviceHelper, RepairOrderRecordService repairOrderRecordService, RepairOrderFileService repairOrderFileService, RepairOrder repairOrder) {
        this.serviceHelper = serviceHelper;
        this.repairOrderRecordService = repairOrderRecordService;
        this.repairOrderFileService = repairOrderFileService;
        this.repairOrder = repairOrder;
        this.rabbitMqParamConfig = rabbitMqParamConfig;
    }

    @SneakyThrows
    @Override
    public void run() {
        CyzToHqEditStatusPojo cyzToHqEditStatusPojo = new CyzToHqEditStatusPojo();
        BeanUtils.copyProperties(repairOrder, cyzToHqEditStatusPojo);
        cyzToHqEditStatusPojo.setId(repairOrder.getHqId());

        Date updateTime = repairOrder.getUpdateTime();
        if(updateTime != null){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            cyzToHqEditStatusPojo.setDealTime(simpleDateFormat.format(updateTime));
        }

        // 根据工单id查询最近一条流程记录
        ServiceResult<RepairOrderRecord> stringServiceResult = repairOrderRecordService.selectLastRecordByOrderId(repairOrder.getId());
        RepairOrderRecord repairOrderRecord = stringServiceResult.getResult();
        if(repairOrderRecord != null){
            cyzToHqEditStatusPojo.setRecord(repairOrderRecord.getRecord());
            cyzToHqEditStatusPojo.setDescribe(repairOrderRecord.getDescribe());

            // 根据流程记录id查询附件地址集合
            ServiceResult<List<FilesPojo>> serviceResult = repairOrderFileService.selectFilesByRecordId(repairOrderRecord.getId());
            List<FilesPojo> list = serviceResult.getResult();
            if(list != null && list.size() > 0){
                cyzToHqEditStatusPojo.setFileUrls(serviceResult.getResult());
            }
        }

        JSONObject json = (JSONObject) JSONObject.toJSON(cyzToHqEditStatusPojo);
        serviceHelper.getRabbitMqManager().basicPublish(rabbitMqParamConfig.getExchange().get(RabbitMqConstants.CYZ_HQ_EXCHANGE_FIELD), RabbitMqConstants.CYZ_TO_HQ_QUEUE_NAME, RabbitMqConstants.CYZ_HQ_REPAIRORDER_STATUS_EXCHANGE, null, json.toString().getBytes());
    }
}