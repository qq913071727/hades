package com.innovation.ic.cyz.admin.controller;

import com.innovation.ic.cyz.base.model.cyz.Banner;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.service.cyz.BannerService;
import com.innovation.ic.cyz.base.vo.cyz.BennerVo;
import com.jcraft.jsch.SftpException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 首页banner
 */
@Api(value = "首页banner", tags = "BannerController")
@RestController
@RequestMapping("/api/v1/banner")
public class BannerController {
    private static final Logger log = LoggerFactory.getLogger(BannerController.class);

    @Autowired
    private BannerService bannerService;

    /**
     * Bannner列表
     *
     * @param bennerVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "Bannner列表")
    @RequestMapping(value = "/allBanner", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> allBanner(@RequestBody BennerVo bennerVo, HttpServletRequest request, HttpServletResponse response) {
        if (bennerVo.getPageNo() == null || bennerVo.getPageNo() == 0 || bennerVo.getPageSize() == null || bennerVo.getPageSize() == 0) {
            String message = "调用接口[/api/v1/banner/allBanner]时，pageNo、pageSize不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<List<Banner>> bannerServiceResult = bannerService.allBanner(bennerVo);

        ApiResult<List<Banner>> apiResult = new ApiResult<>();
        apiResult.setSuccess(bannerServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(bannerServiceResult.getMessage());
        apiResult.setResult(bannerServiceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 添加Bannner
     *
     * @param bennerVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "添加Bannner")
    @RequestMapping(value = "/addBenner", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> addBenner(@RequestBody BennerVo bennerVo, HttpServletRequest request, HttpServletResponse response) throws IOException, SftpException {
        if (StringUtils.isEmpty(bennerVo.getName()) || StringUtils.isEmpty(bennerVo.getSize()) || bennerVo.getAvailable() == null|| bennerVo.getPicturePath() == null) {
            String message = "调用接口[/api/v1/banner/addBenner]时，name、size、available、picturePath不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<ApiResult<Boolean>> resultServiceResult = bannerService.addBannner(bennerVo);
        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(resultServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(resultServiceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询Banner
     *
     * @param bennerVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "查询Banner")
    @RequestMapping(value = "/selectIdBannner", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> selectBannner(@RequestBody BennerVo bennerVo, HttpServletRequest request, HttpServletResponse response){
        if (bennerVo.getId() == null) {
            String message = "调用接口[/api/v1/banner/selectBannner]时，id不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<Banner> resultServiceResult = bannerService.selectIdBannner(bennerVo);
        ApiResult<Banner> apiResult = new ApiResult<>();
        apiResult.setSuccess(resultServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(resultServiceResult.getResult());
        apiResult.setMessage(resultServiceResult.getMessage());
        return new ResponseEntity(apiResult, HttpStatus.OK);
    }

    /**
     * 编辑Banner
     *
     * @param bennerVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "编辑Banner")
    @RequestMapping(value = "/updateBanner", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> updateBanner(BennerVo bennerVo, HttpServletRequest request, HttpServletResponse response) throws IOException, SftpException {
        if (bennerVo.getId() == null || StringUtils.isEmpty(bennerVo.getName()) || StringUtils.isEmpty(bennerVo.getSize()) || bennerVo.getAvailable() == null || bennerVo.getPicturePath() == null) {
            String message = "调用接口[/api/v1/banner/updateBanner]时，id、name、size、available、picturePath不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<ApiResult<Boolean>> resultServiceResult = bannerService.updateBanner(bennerVo);
        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(resultServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(resultServiceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 删除轮播图
     *
     * @param bennerVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "删除轮播图")
    @RequestMapping(value = "/deleteBanner", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> deleteBanner(@RequestBody BennerVo bennerVo, HttpServletRequest request, HttpServletResponse response) throws SftpException {
        if (bennerVo.getId() == null || bennerVo.getId() == 0) {
            String message = "调用接口[/api/v1/banner/deleteBanner]时，id不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        bannerService.deleteBanner(bennerVo.getId());
        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.DELETE_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 修改Banner状态
     *
     * @param bennerVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "修改Banner状态")
    @RequestMapping(value = "/updateAvailable", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> updateAvailable(@RequestBody BennerVo bennerVo, HttpServletRequest request, HttpServletResponse response) {
        if (bennerVo.getId() == null || bennerVo.getId() == 0 || bennerVo.getAvailable() == null) {
            String message = "调用接口[/api/v1/banner/updateAvailable]时，id、available不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        bannerService.updateAvailable(bennerVo);
        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
