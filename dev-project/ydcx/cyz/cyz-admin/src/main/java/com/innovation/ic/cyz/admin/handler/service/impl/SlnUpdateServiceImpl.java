package com.innovation.ic.cyz.admin.handler.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.b1b.framework.util.IdUtil;
import com.innovation.ic.cyz.admin.handler.service.SlnCore;
import com.innovation.ic.cyz.admin.handler.service.SlnCoreService;
import com.innovation.ic.cyz.admin.thread.DownloadImageGoodsCoverThread;
import com.innovation.ic.cyz.admin.thread.DownloadImageOtherCoverThread;
import com.innovation.ic.cyz.base.model.cyz.Sln;
import com.innovation.ic.cyz.base.model.cyz.SlnUrl;
import com.innovation.ic.cyz.base.pojo.constant.SlnUrlType;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @ClassName SlnUpdateServiceImpl
 * @Description 修改方案实现类
 * @Date 2022/9/21
 * @Author myq
 */
@Slf4j
@Service
public class SlnUpdateServiceImpl extends SlnCore implements SlnCoreService {

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void executing(JSONObject apiData) {
        Sln sln = new Sln();
        Long slnId = apiData.getLong("id");
        sln.setSlnId(slnId);
        sln.setTitle(apiData.getString("title"));
        sln.setCash(apiData.getFloat("cash"));
        sln.setDescription(apiData.getString("desc"));
        sln.setGoodsCover(apiData.getString("goods_cover"));
        sln.setCateName(apiData.getString("cate_name"));
        sln.setDeliveryCateIdName(apiData.getString("delivery_cate_id_name"));
        sln.setPerformanceParameter(apiData.getString("performance_parameter"));
        sln.setApplicationScene(apiData.getString("application_scene"));
        sln.setTypeDisplay(apiData.getString("type_display"));
        sln.setSubtitle(apiData.getString("subtitle"));
        sln.setStatusDisplay(apiData.getString("status_display"));
        sln.setViewNum(apiData.getInteger("view_num"));
        sln.setCreatedAt(apiData.getDate("created_at"));
        sln.setSource(1);
        //上传封面图片
        //String newSlnImgPath = workHelper.commit(new CoverWorker(sln));
        String newSlnImgPath = "";
        try {
            newSlnImgPath = threadPoolManager.submit(new DownloadImageGoodsCoverThread(super.importSlnService, sln)).get(TIMEOUT, TimeUnit.SECONDS);
        } catch (Exception e) {
            log.error("当前线程：" + Thread.currentThread().getName() + "执行任务异常：" + e);
            throw new RuntimeException("上传图片异常");
        }
        sln.setGoodsCover(newSlnImgPath);
        //添加sln数据
        slnService.updateSinInfo(sln);
        JSONArray otherCoverArr = apiData.getJSONArray("other_cover");
        List<SlnUrl> slnUrls = new ArrayList<>(otherCoverArr.size());
        // 其他封面
        var otherCovers = otherCoverArr.stream().map(url -> {
            SlnUrl slnUrl = new SlnUrl();
            slnUrl.setId(IdUtil.snowflakeNextId());
            slnUrl.setSlnId(sln.getId()); //主键
            slnUrl.setType(SlnUrlType.OtherCover);
            slnUrl.setUrl(url.toString());

            //上传其他封面图片
            //String newImgPath = workHelper.commit(new OtherCoverWorker(slnUrl));
            String newImgPath = "";
            try {
                newImgPath = threadPoolManager.submit(new DownloadImageOtherCoverThread(importSlnService, slnUrl)).get(TIMEOUT, TimeUnit.SECONDS);
            } catch (Exception e) {
                log.error("当前线程池：" + Thread.currentThread().getName() + "执行任务异常：" + e);
                throw new RuntimeException("上传图片异常");
            }
            slnUrl.setUrl(newImgPath);
            return slnUrl;
        }).collect(Collectors.toList());
        slnUrls.addAll(otherCovers);

        slnService.updateSinUrls(slnUrls);

    }

}
