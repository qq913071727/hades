package com.innovation.ic.cyz.admin.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.innovation.ic.cyz.base.pojo.constant.DatabaseGlobal;
import com.innovation.ic.cyz.base.value.config.MyBatisPlusParamConfig;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@EnableTransactionManagement
@Configuration
@MapperScan("com.innovation.ic.cyz.base.mapper," +
        "com.innovation.ic.cyz.base.mapper.cyz," +
        "com.innovation.ic.cyz.base.mapper.pve_standard")
public class MybatisPlusConfig {

    @Resource
    private MyBatisPlusParamConfig myBatisPlusParamConfig;

    //需要注入的Bean
    @Bean
    @Primary
    public EasySqlInjector easySqlInjector() {
        return new EasySqlInjector();
    }

    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

//    @Bean(name = DatabaseGlobal.IM_ERP9)
//    @ConfigurationProperties(prefix = "spring.datasource.im-erp9")
//    public DataSource imErp9() {
//        return DruidDataSourceBuilder.create().build();
//    }
//
//    @Bean(name = DatabaseGlobal.IM_B1B)
//    @ConfigurationProperties(prefix = "spring.datasource.im-b1b")
//    public DataSource imB1b() {
//        return DruidDataSourceBuilder.create().build();
//    }

    @Bean(name = DatabaseGlobal.CYZ)
    @ConfigurationProperties(prefix = "spring.datasource.cyz")
    public DataSource cyz() {
        return DruidDataSourceBuilder.create().build();
    }

    @Bean(name = DatabaseGlobal.PVE_STANDARD)
    @ConfigurationProperties(prefix = "spring.datasource.pve-standard")
    public DataSource pveStandard() {
        return DruidDataSourceBuilder.create().build();
    }

    /**
     * 动态数据源配置
     *
     * @return
     */
    @Bean
    @Primary
    public DataSource multipleDataSource(/*@Qualifier(DatabaseGlobal.IM_ERP9) DataSource imErp9DataSource,
                                         @Qualifier(DatabaseGlobal.IM_B1B) DataSource imB1bDataSource,*/
            @Qualifier(DatabaseGlobal.CYZ) DataSource imCyzDataSource,
            @Qualifier(DatabaseGlobal.PVE_STANDARD) DataSource imStandardDataSource
    ) {
        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        Map<Object, Object> targetDataSources = new HashMap<Object, Object>();
//        targetDataSources.put(DatabaseGlobal.IM_ERP9, imErp9DataSource);
//        targetDataSources.put(DatabaseGlobal.IM_B1B, imB1bDataSource);
        targetDataSources.put(DatabaseGlobal.CYZ, imCyzDataSource);
        targetDataSources.put(DatabaseGlobal.PVE_STANDARD, imStandardDataSource);
        dynamicDataSource.setTargetDataSources(targetDataSources);
        // 程序默认数据源，这个要根据程序调用数据源频次，经常把常调用的数据源作为默认
        dynamicDataSource.setDefaultTargetDataSource(cyz());
        return dynamicDataSource;
    }

    @Bean("sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(multipleDataSource(/*imErp9(), imB1b(),*/ cyz(), pveStandard()));

        MybatisConfiguration configuration = new MybatisConfiguration();
        configuration.setJdbcTypeForNull(JdbcType.NULL);
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.setCacheEnabled(false);
        configuration.setLogImpl(myBatisPlusParamConfig.getLogImpl());
        sqlSessionFactory.setConfiguration(configuration);
        sqlSessionFactory.setGlobalConfig(globalConfiguration());
        // 乐观锁插件
        //PerformanceInterceptor(),OptimisticLockerInterceptor()
        // 分页插件
        sqlSessionFactory.setPlugins(paginationInterceptor());
        return sqlSessionFactory.getObject();
    }

    @Bean
    public GlobalConfig globalConfiguration() {
        GlobalConfig conf = new GlobalConfig();
        // 自定义的注入需要在这里进行配置
        conf.setSqlInjector(easySqlInjector());
        return conf;
    }

    /**
     * 分页插件配置
     *
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        // 向MyBatis-Plus的过滤器链中添加分页拦截器，需要设置数据库类型（主要用于分页）
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }
}
