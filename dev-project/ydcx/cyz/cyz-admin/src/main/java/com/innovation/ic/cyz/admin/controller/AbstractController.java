package com.innovation.ic.cyz.admin.controller;

import com.auth0.jwt.interfaces.Claim;
import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.b1b.framework.util.JwtUtils;
import com.innovation.ic.cyz.base.pojo.constant.Constants;
import com.innovation.ic.cyz.base.pojo.constant.HttpHeader;
import com.innovation.ic.cyz.base.service.cyz.*;
import com.innovation.ic.cyz.base.service.cyz.impl.ServiceHelper;
import com.innovation.ic.cyz.base.value.config.FilterParamConfig;
import com.innovation.ic.cyz.base.value.config.FtpAccountConfig;
import com.innovation.ic.cyz.base.value.config.MinioPropConfig;
import com.innovation.ic.cyz.base.value.config.RabbitMqParamConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 抽象controller
 */
public abstract class AbstractController {
    @Resource
    protected RepairOrderService repairOrderService;

    @Autowired
    protected InquiryService inquiryService;

    @Resource
    protected RepairOrderTypeService repairOrderTypeService;

    @Resource
    protected RepairOrderFileService repairOrderFileService;

    @Resource
    protected RepairOrderRecordService repairOrderRecordService;

    @Resource
    protected ClientService clientService;

    @Resource
    protected ServiceHelper serviceHelper;

    @Autowired
    protected FilterParamConfig filterParamConfig;

    /************************************* config *****************************************/
    @Autowired
    protected FtpAccountConfig ftpAccountConfig;

    @Autowired
    protected MinioPropConfig minioPropConfig;

    @Autowired
    protected RabbitMqParamConfig rabbitMqParamConfig;

    /************************************* manager *****************************************/
    @Autowired
    ThreadPoolManager threadPoolManager;

    /**
     * 获取request对象
     */
    protected HttpServletRequest getRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            return null;
        }
        return ((ServletRequestAttributes) requestAttributes).getRequest();
    }

    /**
     * 获取response对象
     **/
    protected HttpServletResponse getResponse() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            return null;
        }
        return ((ServletRequestAttributes) requestAttributes).getResponse();
    }

    /**
     * 获取Token
     */
    protected String getToken() {
        HttpServletRequest request = getRequest();
        String tokenString = request.getHeader(filterParamConfig.getToken());
        return tokenString.split(HttpHeader.TOKEN_SPLIT)[1];
    }

    /**
     * 获取用户Id
     * @return
     */
    protected String getUserId() {
        String token = getToken();
        if (token == null) {
            return null;
        }

        Map<String, Claim> claims = JwtUtils.getPayloadByToken(token);
        return claims.get("userId").asString().replace("\"", "");
    }

    /**
     * 获取用户信息
     * @param request 请求数据
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     * @return 返回用户信息
     */
    protected Map<String, String> getUserInfo(HttpServletRequest request) throws UnsupportedEncodingException {
        Map<String, String> map = new HashMap<>();
        Enumeration<String> enumeration = request.getHeaderNames();
        while (enumeration.hasMoreElements()) {
            String name	= enumeration.nextElement();
            String value = request.getHeader(name);

            if(name.equals(Constants.ID)){
                map.put(Constants.ID, value);
                continue;
            }

            if(name.equals(Constants.NAME)){
                // 对汉字进行解码操作
                map.put(Constants.NAME, URLDecoder.decode(value,"UTF-8"));
            }
        }
        return map;
    }
}