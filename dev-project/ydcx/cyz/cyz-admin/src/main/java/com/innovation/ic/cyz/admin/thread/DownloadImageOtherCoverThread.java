package com.innovation.ic.cyz.admin.thread;

import com.innovation.ic.cyz.base.model.cyz.SlnUrl;
import com.innovation.ic.cyz.base.service.cyz.ImportSlnService;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;

@Slf4j
public class DownloadImageOtherCoverThread implements Callable<String> {


    private ImportSlnService importSlnService;


    private SlnUrl slnUrl;


    public DownloadImageOtherCoverThread(ImportSlnService importSlnService, SlnUrl slnUrl){
        this.importSlnService = importSlnService;
        this.slnUrl = slnUrl;
    }


    @Override
    public String call() throws Exception {
        log.info(">>>>>>>>>>>>>>>>>>>>>>>开始下载并上传其他封面图片>>>>>>>>>>>>>>>>>>>>>>>");
        return importSlnService.downloadImageOtherCover(slnUrl);
    }
}
