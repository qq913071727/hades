/*
package com.innovation.ic.cyz.admin;

import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.function.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo {
}

*/
/**
 * @Description: 函数式接口
 * @Params:
 * @Return:
 * @Author: Mr.myq
 * @Date: 2022/9/2817:08
 *//*

@FunctionalInterface
interface ClientFunction<T,R>{

    */
/**
     *  传入T返回R，函数逻辑自己编织
     * @param t
     * @return r
     *//*

    R test(T t);

    */
/**
     * 只允许一个接口 或抽象方法
     *//*

//    void add (T t);
//   abstract void add (T t);
    // default 可以
    default <R> R test1(){

        return null;
    }
}

*/
/**
 * @Description: 函数式接口
 * @Params:
 * @Return:
 * @Author: Mr.myq
 * @Date: 2022/9/289:36
 *//*

class C<T,A,R>{
    private T t;
    private A a;
    private R r;

    public C(T t,A a,R r){
        this.t = t;
        this.a = a;
        this.r = r;
    }

    public C(){
    }

    public R supplier(Supplier<R> r){
        return r.get();
    }

    public boolean predicate(Predicate<T> predicate, T string){
        return predicate.test(string);
    }

    public boolean predicate(Predicate<T> predicate){
        return predicate.test(t);
    }

    public R function(Function<T,R> t, T t1){
        return t.apply(t1);
    }

    public void consumer(Consumer<A> c, A t){
        c.accept(t);
    }

    */
/**
     * 方法内部自己实现起逻辑
     * @param args
     *//*

    public static void main(String[] args) {
      new C<String,Integer,Long>().supplier(new Supplier<Long>() {
          @Override
          public Long get() {
              System.out.println("1");
              return 1L;
          }
      });
//
//        new C<String,Integer,Long>().predicate(new Predicate<String>() {
//            @Override
//            public boolean test(String s) {
//                return false;
//            }
//        },"c");
//
//       new C<String,Integer,Long>().function(new Function<String, Long>() {
//           @Override
//           public Long apply(String s) {
//               return Long.valueOf(s);
//           }
//       },"c");
//
//        new C<String,Integer,Long>().consumer(new Consumer<Integer>() {
//            @Override
//            public void accept(Integer integer) {
//                System.out.println(integer);
//            }
//        },1);
////
//        new TestClient<String, Sink<String,String>>("").test(new ClientFunction<String, Sink<String,String>>() {
//
//            @Override
//            public Sink<String,String> test(String s) {
//
//                return null;
//            }
//        });
//

//        Collector<Object, ?, List<Object>> objectListCollector = Collectors.toList();
        List<Integer> list = new ArrayList<>();
        list.add(1);
        Stream<Integer> stream = list.stream();
        Stream<String> stringStream = stream.map(Integer::toHexString);
//        List<String> collect = stringStream.collect(Collectors.toList());
//
//        ArrayList<String> add = new TestClient<String, Integer, String>("").add(ArrayList<String>::new);
//        System.out.println(add.size());

//        new TestClient<String, Integer, String>(List::add);


//        Father f = new ChildA();

*/
/*        AbstractPipeline.ReferencePipeline<String, String> referencePipeline = new AbstractPipeline.ReferencePipeline<String, String>() {

            @Override
            public Sink<String, String> opWrapSink(Sink<String, String> p) {
                return new Sink.Provider<String, String>(p) {
                    @Override
                    public String apply(String s) {
                        System.out.println("11");
                        return p.apply(s);
                    }

                };
            }
        };

        referencePipeline.opWrapSink(new SinkImpl<>()).apply("ces");*//*




    }





}

class TestClient<T,A,R>{
    private T t;
    private R r;
    public TestClient(T t){
        this.t = t;
    }

    public <A> A add(Supplier<A> a){
        return a.get();
    }

    public TestClient(BiConsumer<A,T> c){
        System.out.println(c.toString());
    }


    public R test(ClientFunction<T,R> clientFunction){
        return clientFunction.test(t);
    }
}

abstract class AbstractPipeline<P_INT,P_OUT>{

    public abstract Sink<P_INT,P_OUT> opWrapSink(Sink<P_INT,P_OUT> p_int);

    static abstract class ReferencePipeline<P_INT,P_OUT> extends AbstractPipeline<P_INT,P_OUT>{

    }

}

interface Sink<T,R> extends Function<T,R>{

    default Sink accpet(T value) {
        throw new IllegalStateException("called wrong accept method");
    }

    static abstract class Provider<T,R> implements Sink<T,R>{
        protected final Sink<T,R> client;

        public Provider(Sink<T,R> client){
            this.client = client;
        }
    }

}

class SinkImpl<T,R> implements Sink<T,R>{

    @Override
    public R apply(T t) {
        System.out.println("sss" + t);
        return null;
    }
}



class Father{
    private Father father ;
    public Father(){
       father = this;
    }
}

class ChildA extends Father {
    public ChildA() {
    }
}

class ChildB extends Father{
    public ChildB(){}
}*/
