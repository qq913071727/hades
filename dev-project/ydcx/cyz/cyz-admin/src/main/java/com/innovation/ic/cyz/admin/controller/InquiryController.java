package com.innovation.ic.cyz.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.innovation.ic.b1b.framework.util.ExcelUtil;
import com.innovation.ic.cyz.base.model.cyz.Inquiry;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.constant.InquiryExportFile;
import com.innovation.ic.cyz.base.vo.cyz.InquiryMiniSaveVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 在线询价API
 */
@Api(value = "在线询价API", tags = "InquiryController")
@RestController
@RequestMapping("/api/v1/inquiry")
public class InquiryController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(InquiryController.class);

    /**
     * 分页显示
     */
    @ApiOperation(value = "分页显示")
    @RequestMapping(value = "/page", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Page<Inquiry>>> page(@RequestBody InquiryMiniSaveVo inquiryMiniSaveVo,
                                                         HttpServletRequest request, HttpServletResponse response) {
        if (null == inquiryMiniSaveVo || null == inquiryMiniSaveVo.getPageNo() || null == inquiryMiniSaveVo.getPageSize()) {
            String message = "参数pageSize、pageNo不能为空";
            log.warn(message);
            ApiResult<Page<Inquiry>> apiResult = new ApiResult<Page<Inquiry>>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<ApiResult<Page<Inquiry>>>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Page<Inquiry>> serviceResult = inquiryService.page(inquiryMiniSaveVo.getPageSize(), inquiryMiniSaveVo.getPageNo());

        ApiResult<Page<Inquiry>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<ApiResult<Page<Inquiry>>>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id，删除一条记录
     */
    @ApiOperation(value = "根据id，删除一条记录")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Long")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> deleteById(@PathVariable("id") Long id,
                                                         HttpServletRequest request, HttpServletResponse response) {
        if (null == id) {
            String message = "参数id不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<Boolean>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<ApiResult<Boolean>>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> serviceResult = inquiryService.deleteById(id);

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.DELETE_SUCCESS);
        return new ResponseEntity<ApiResult<Boolean>>(apiResult, HttpStatus.OK);
    }

    /**
     * 导出全部。无论之前是否被导出过
     */
    @ApiOperation(value = "导出全部。无论之前是否被导出过")
    @RequestMapping(value = "/exportAll", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> exportAll(HttpServletRequest request, HttpServletResponse response) {

        // 查询所有记录，只返回excel需要的列。然后将所有记录的export_status字段更新为1
        ServiceResult<List<Object[]>> serviceResult = (ServiceResult<List<Object[]>>) inquiryService.findAllRecordForExcel();
        if (null != serviceResult && serviceResult.getResult().size() == 0){
            String message = "没有数据";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<Boolean>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<ApiResult<Boolean>>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 下载excel文件
        ExcelUtil.exportExcel(response, InquiryExportFile.ALL, InquiryExportFile.COLUMN_HEAD, serviceResult.getResult());

        return null;

//        ApiResult<Boolean> apiResult = new ApiResult<>();
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
//        return new ResponseEntity<ApiResult<Boolean>>(apiResult, HttpStatus.OK);
    }

    /**
     * 导出最新。只导出之前没有被导出的，然后所有的都变为已导出
     */
    @ApiOperation(value = "导出最新。只导出之前没有被导出的，然后所有的都变为已导出")
    @RequestMapping(value = "/exportNewest", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> exportNewest(HttpServletRequest request, HttpServletResponse response) {

        // 查询所有未导出过记录，只返回excel需要的列。然后将所有记录的export_status字段更新为1
        ServiceResult<List<Object[]>> serviceResult = inquiryService.findNotExportRecordForExcel();
        if (null != serviceResult && serviceResult.getResult().size() == 0){
            String message = "没有最新的数据";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<Boolean>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<ApiResult<Boolean>>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 下载excel文件
        ExcelUtil.exportExcel(response, InquiryExportFile.ALL, InquiryExportFile.COLUMN_HEAD, serviceResult.getResult());

        return null;
    }
}