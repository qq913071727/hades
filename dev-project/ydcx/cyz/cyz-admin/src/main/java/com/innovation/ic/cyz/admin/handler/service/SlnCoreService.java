package com.innovation.ic.cyz.admin.handler.service;


import com.alibaba.fastjson.JSONObject;

/**
 * @ClassName SlnCoreService
 * @Description 方案核心接口类
 * @Date 2022/9/21
 * @Author myq
 */
public interface SlnCoreService {

    void executing(JSONObject slnJson);

}
