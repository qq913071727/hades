package com.innovation.ic.cyz.admin.listener;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.cyz.admin.handler.factory.SlnOperateFactory;
import com.innovation.ic.cyz.base.pojo.constant.RabbitMQConstant;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName SinHandler
 * @Description 用于处理BIB方案数据的处理类
 * @Date 2022/9/16
 * @Author myq
 */
@Component
@Slf4j
public class SlnReceiveListener {

    @Autowired
    private SlnOperateFactory slnOperateFactory;

    private ConcurrentHashMap<String, Long> curr = new ConcurrentHashMap<>();

    /**
     * @Description 监听BIB发送的方案数据
     * @Date 2022/9/15 16:00
     * @Author myq
     **/
    @RabbitListener(queues = RabbitMQConstant.SIN_DOWN_QUEUE)
    public void process(Message msg, Channel channel) throws Exception {
        log.info("*********************start examine sin data*********************");
        String body = new String(msg.getBody(), StandardCharsets.UTF_8);
        long id = msg.getMessageProperties().getDeliveryTag();
        log.info(String.format("当前消息体：{ %s }", body));
        try {
            this.process(body);

            channel.basicAck(id, true);
        } catch (Exception e) {
            log.error("监听到方案数据：{}，异常原因：{}", body, e);
            //丢弃数据
            channel.basicNack(id, true, false);
        }
        // ifRemoveByMaxCount("")
        log.info("*********************end examine sin data*********************");
    }


    /**
     * @Description: 执行函数 (接收json串，并执行业务锁机)，将异常抛出交给rabbitmq函数处理
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/1914:17
     */
    public void process(String body) throws Exception {
        /**
         *  解析json串
         *  {"id": 123131,"cash": "19.22","desc": "方案详情信息","title":"方案名称",
         *  "goods_cover":"图片地址aa.png" ,"cate_name":"方案领域","id_cate_name":"方案开发平台",
         *  "delivery_cate_id_name":"方案交付新式","performance_parameter":"方案 性能参数",
         *  "application_scene":"方案应用场景","type_display":"方案类型","subtitle":"方案子名称",
         *  "view_num":"方案查看次数","like_goods":"不知道也传","other_cover":[],"created_at":"","operate":"insert"}
         */
        JSONObject apiData = JSONObject.parseObject(body);
        String operate = apiData.getString("operate");
        log.debug("操作类型： " + operate+ "开始解析方案数据");
        slnOperateFactory.choose(operate).executing(apiData);
    }


    /**
     * @Description: 是否达到最大重试次数
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/9/2110:52
     */
    private boolean ifRemoveByMaxCount(String logId) {
        String key = logId;
        Long newVal;
        if (curr.containsKey(key)) {
            Long oldVal = curr.get(key);
            newVal = ++oldVal;
        } else {
            newVal = 1L;
            curr.put(key, newVal);
        }
        System.out.println(newVal);
        if (newVal >= 3) {
            return false;
        }
        return true;
    }

}


