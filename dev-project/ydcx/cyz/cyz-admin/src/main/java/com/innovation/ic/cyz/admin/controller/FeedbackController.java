package com.innovation.ic.cyz.admin.controller;

import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.feedback.FeedbackListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.feedback.FeedbackTypeListPojo;
import com.innovation.ic.cyz.base.service.cyz.FeedbackService;
import com.innovation.ic.cyz.base.vo.cyz.FeedbackVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 问题反馈
 */
@Api(value = "问题反馈", tags = "feedbackController")
@RestController
@RequestMapping("/api/v1/feedback")
public class FeedbackController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(FeedbackController.class);

    @Autowired
    private FeedbackService feedbackService;

    /**
     * 问题类型下拉框
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "问题类型下拉框")
    @RequestMapping(value = "/allFeedbackType", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> allFeedbackType(HttpServletRequest request, HttpServletResponse response) throws IllegalAccessException {
        ServiceResult<List<FeedbackTypeListPojo>> feedbackTypes = feedbackService.getFeedbackTypes();

        ApiResult<List<FeedbackTypeListPojo>> apiResult = new ApiResult<>();
        apiResult.setSuccess(feedbackTypes.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(feedbackTypes.getMessage());
        apiResult.setResult(feedbackTypes.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 信息反馈展示数据
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "信息反馈展示数据")
    @RequestMapping(value = "/allFeedback", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> allFeedback(@RequestBody FeedbackVo feedbackVo, HttpServletRequest request, HttpServletResponse response) throws IllegalAccessException {
        String userId = getUserId();
        feedbackVo.setUserId(userId);
        ServiceResult<List<FeedbackListPojo>> listServiceResult = feedbackService.allFeedback(feedbackVo);

        ApiResult<List<FeedbackListPojo>> apiResult = new ApiResult<>();
        apiResult.setSuccess(listServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(listServiceResult.getMessage());
        apiResult.setResult(listServiceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}