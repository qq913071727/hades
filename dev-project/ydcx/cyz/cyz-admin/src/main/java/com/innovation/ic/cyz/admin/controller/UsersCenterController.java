package com.innovation.ic.cyz.admin.controller;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cyz.base.model.cyz.AvdType;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.users_center.UserCenterListPojo;
import com.innovation.ic.cyz.base.service.cyz.UsersCenterService;
import com.innovation.ic.cyz.base.vo.cyz.UserCenterVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 用户中心
 */
@Api(value = "用户中心", tags = "usersCenterController")
@RestController
@RequestMapping("/api/v1/usercenter")
public class UsersCenterController {
    private static final Logger log = LoggerFactory.getLogger(UsersCenterController.class);
    @Autowired
    private UsersCenterService usersCenterService;

    /**
     * 用户中心列表信息
     *
     * @param userCenterVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "用户中心列表信息")
    @RequestMapping(value = "/allUsersCenter", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> allUsersCenter(@RequestBody UserCenterVo userCenterVo, HttpServletRequest request, HttpServletResponse response) {
        if (userCenterVo.getPageNo() == null || userCenterVo.getPageNo() == 0 || userCenterVo.getPageSize() == null || userCenterVo.getPageSize() == 0) {
            String message = "调用接口[/api/v1/usercenter/allUsersCenter]时，参数pageNo、pageSize不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<UserCenterListPojo> usersCenterServiceResult = usersCenterService.allUsersCenter(userCenterVo);

        ApiResult<UserCenterListPojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(usersCenterServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(usersCenterServiceResult.getMessage());
        apiResult.setResult(usersCenterServiceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 用户中心列表信息(根据来源)返回
     *
     * @param userCenterVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "用户中心列表信息(根据来源)返回")
    @RequestMapping(value = "/allUsersCenterFindSource", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> allUsersCenterFindSource(@RequestBody UserCenterVo userCenterVo, HttpServletRequest request, HttpServletResponse response) {
        if (userCenterVo.getPageNo() == null || userCenterVo.getPageNo() == 0 || userCenterVo.getPageSize() == null || userCenterVo.getPageSize() == 0 || userCenterVo.getSource() == null) {
            String message = "调用接口[/api/v1/usercenter/allUsersCenterFindSource]时，参数pageNo、pageSize、source不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<UserCenterListPojo> usersCenterServiceResult = usersCenterService.allUsersCenter(userCenterVo);

        ApiResult<UserCenterListPojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(usersCenterServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(usersCenterServiceResult.getMessage());
        apiResult.setResult(usersCenterServiceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 用户中心(投稿信息)
     *
     * @param userCenterVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "用户中心(投稿信息)")
    @RequestMapping(value = "/allContribution", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> allContribution(@RequestBody UserCenterVo userCenterVo, HttpServletRequest request, HttpServletResponse response) {
        if (userCenterVo.getPageNo() == null || userCenterVo.getPageNo() == 0 || userCenterVo.getPageSize() == null || userCenterVo.getPageSize() == 0 || !StringUtils.validateParameter(userCenterVo.getUserId()) || userCenterVo.getOperation() == null || userCenterVo.getOperation() == 0) {
            String message = "调用接口[/api/v1/usercenter/allContribution]时，参数pageNo、pageSize、userId、operation不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<UserCenterListPojo> usersCenterServiceResult = usersCenterService.allUsersCenter(userCenterVo);

        ApiResult<UserCenterListPojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(usersCenterServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(usersCenterServiceResult.getMessage());
        apiResult.setResult(usersCenterServiceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 用户中心(点赞信息)
     *
     * @param userCenterVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "用户中心(点赞信息)")
    @RequestMapping(value = "/allFabulous", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> allFabulous(@RequestBody UserCenterVo userCenterVo, HttpServletRequest request, HttpServletResponse response) {
        if (userCenterVo.getPageNo() == null || userCenterVo.getPageNo() == 0 || userCenterVo.getPageSize() == null || userCenterVo.getPageSize() == 0 || !StringUtils.validateParameter(userCenterVo.getUserId()) || userCenterVo.getOperation() == null || userCenterVo.getOperation() == 0) {
            String message = "调用接口[/api/v1/usercenter/allFabulous]时，参数pageNo、pageSize、userId、operation不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<UserCenterListPojo> usersCenterServiceResult = usersCenterService.allUsersCenter(userCenterVo);

        ApiResult<UserCenterListPojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(usersCenterServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(usersCenterServiceResult.getMessage());
        apiResult.setResult(usersCenterServiceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 用户中心(收藏信息)
     *
     * @param userCenterVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "用户中心(收藏信息)")
    @RequestMapping(value = "/allCollection", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> allCollection(@RequestBody UserCenterVo userCenterVo, HttpServletRequest request, HttpServletResponse response) {
        if (userCenterVo.getPageNo() == null || userCenterVo.getPageNo() == 0 || userCenterVo.getPageSize() == null || userCenterVo.getPageSize() == 0 || !StringUtils.validateParameter(userCenterVo.getUserId()) || userCenterVo.getOperation() == null || userCenterVo.getOperation() == 0) {
            String message = "调用接口[/api/v1/usercenter/allCollection]时，参数pageNo、pageSize、userId、operation不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<UserCenterListPojo> usersCenterServiceResult = usersCenterService.allUsersCenter(userCenterVo);

        ApiResult<UserCenterListPojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(usersCenterServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(usersCenterServiceResult.getMessage());
        apiResult.setResult(usersCenterServiceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 用户中心(浏览信息)
     *
     * @param userCenterVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "用户中心(浏览信息)")
    @RequestMapping(value = "/allView", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> allView(@RequestBody UserCenterVo userCenterVo, HttpServletRequest request, HttpServletResponse response) {
        if (userCenterVo.getPageNo() == null || userCenterVo.getPageNo() == 0 || userCenterVo.getPageSize() == null || userCenterVo.getPageSize() == 0 || !StringUtils.validateParameter(userCenterVo.getUserId()) || userCenterVo.getOperation() == null || userCenterVo.getOperation() == 0) {
            String message = "调用接口[/api/v1/usercenter/allView]时，参数pageNo、pageSize、userId、operation不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        ServiceResult<UserCenterListPojo> usersCenterServiceResult = usersCenterService.allUsersCenter(userCenterVo);

        ApiResult<UserCenterListPojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(usersCenterServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(usersCenterServiceResult.getMessage());
        apiResult.setResult(usersCenterServiceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 用户中心中下拉框(所属类别)
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "用户中心中下拉框(所属类别)")
    @RequestMapping(value = "/allType", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> allType(HttpServletRequest request, HttpServletResponse response) {
        //查询所有所属类别数据
        ServiceResult<List<AvdType>> serviceResult = usersCenterService.allType();
        ApiResult<List<AvdType>> apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 用户中心投稿信息(删除)
     *
     * @param userCenterVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "用户中心投稿信息(删除)")
    @RequestMapping(value = "/deleteContribution", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> deleteContribution(@RequestBody UserCenterVo userCenterVo, HttpServletRequest request, HttpServletResponse response) {
        if (userCenterVo.getId() == null || userCenterVo.getId() == 0) {
            String message = "调用接口[/api/v1/usercenter/deleteContribution]时，参数id不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        usersCenterService.deleteContribution(userCenterVo);
        ApiResult<ApiResult> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.DELETE_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 用户中心投稿信息(审核)
     *
     * @param userCenterVo
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "用户中心投稿信息(审核状态)")
    @RequestMapping(value = "/examineContribution", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> examineContribution(@RequestBody UserCenterVo userCenterVo, HttpServletRequest request, HttpServletResponse response) {
        if (userCenterVo.getId() == null || userCenterVo.getId() == 0 || userCenterVo.getApproveStatus() == null || userCenterVo.getApproveStatus() == 0) {
            String message = "调用接口[/api/v1/usercenter/examineContribution]时，参数id、approveStatus不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        usersCenterService.updateApproveStatus(userCenterVo);
        ApiResult<ApiResult> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.UPDATE_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
