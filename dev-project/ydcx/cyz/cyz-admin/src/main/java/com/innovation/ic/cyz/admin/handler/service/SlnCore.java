package com.innovation.ic.cyz.admin.handler.service;

import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.cyz.base.service.cyz.ImportSlnService;
import com.innovation.ic.cyz.base.service.cyz.SlnService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class SlnCore {

    /**
     * 获取结果超时时间
     */
    protected final Long TIMEOUT = 5L;

    @Autowired
    protected SlnService slnService;

    @Autowired
    protected ImportSlnService importSlnService;

    @Autowired
    protected ThreadPoolManager threadPoolManager;

}
