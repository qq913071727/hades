package com.innovation.ic.cyz.admin.config;

import com.innovation.ic.cyz.base.pojo.constant.RabbitMQConstant;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName RabbitMQConfig
 * @Description 消息队列服务器配置类
 * @Date 2022/9/15
 * @Author myq
 */
@Configuration
public class RabbitMQConfig {


    @Bean
    public DirectExchange sinDownDirectExchange() {
        return new DirectExchange(RabbitMQConstant.SIN_DOWN_DIRECT_EXCHANGE);
    }

    @Bean
    public DirectExchange sinUpDirectExchange() {
        return new DirectExchange(RabbitMQConstant.SIN_UP_DIRECT_EXCHANGE);
    }

    @Bean
    public Queue sinUpQueue() {
        return new Queue(RabbitMQConstant.SIN_UP_QUEUE, false);
    }

    @Bean
    public Queue sinDownQueue() {
        return new Queue(RabbitMQConstant.SIN_DOWN_QUEUE, false);
    }

    @Bean
    public Binding bindingUpSINDirectExchange() {
        return BindingBuilder.bind(sinUpQueue()).to(sinUpDirectExchange()).with(RabbitMQConstant.SIN_UP_ROUTE_KEY);
    }

    @Bean
    public Binding bindingDownSINDirectExchange() {
        return BindingBuilder.bind(sinDownQueue()).to(sinDownDirectExchange()).with(RabbitMQConstant.SIN_DOWN_ROUTE_KEY);
    }


}
