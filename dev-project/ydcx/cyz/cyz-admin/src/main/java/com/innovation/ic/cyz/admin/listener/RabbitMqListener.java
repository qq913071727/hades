package com.innovation.ic.cyz.admin.listener;

import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.cyz.admin.thread.ListenHqToCyzRepairAddQueueThread;
import com.innovation.ic.cyz.admin.thread.ListenHqToCyzRepairReplyQueueThread;
import com.innovation.ic.cyz.base.pojo.constant.RabbitMqConstants;
import com.innovation.ic.cyz.base.service.cyz.RepairOrderService;
import com.innovation.ic.cyz.base.service.cyz.impl.ServiceHelper;
import com.innovation.ic.cyz.base.value.config.RabbitMqParamConfig;
import com.rabbitmq.client.Channel;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;

/**
 * @desc   mq队列收到消息后，调用这个监听器的方法
 * @author linuo
 * @time   2022年10月8日17:29:50
 */
@Component
@RequiredArgsConstructor
public class RabbitMqListener implements ApplicationRunner {
    @Resource
    private ServiceHelper serviceHelper;

    @Resource
    private RabbitMqParamConfig rabbitMqParamConfig;

    @Resource
    private RepairOrderService repairOrderService;

    @Resource
    private ThreadPoolManager threadPoolManager;

    @Override
    public void run(ApplicationArguments args) {
        Channel channel = serviceHelper.getRabbitMqManager().getChannel();

        String exchange = rabbitMqParamConfig.getExchange().get(RabbitMqConstants.HQ_CYZ_EXCHANGE_FIELD);

        // 监听虎趣新增工单队列
        ListenHqToCyzRepairAddQueueThread listenHqToCyzRepairAddQueueThread = new ListenHqToCyzRepairAddQueueThread(channel, exchange, repairOrderService);
        threadPoolManager.execute(listenHqToCyzRepairAddQueueThread);

        // 监听虎趣回复工单队列
        ListenHqToCyzRepairReplyQueueThread listenHqToCyzRepairReplyQueueThread = new ListenHqToCyzRepairReplyQueueThread(channel, exchange, repairOrderService);
        threadPoolManager.execute(listenHqToCyzRepairReplyQueueThread);
    }
}