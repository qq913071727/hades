package com.innovation.ic.cyz.admin.controller.api;


import com.github.pagehelper.PageInfo;
import com.innovation.ic.cyz.admin.controller.AbstractController;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.SlnListPojo;
import com.innovation.ic.cyz.base.service.cyz.SlnService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName SlnController
 * @Description 方案控制器
 * @Date 2022/9/15
 * @Author myq
 */
@RestController
@RequestMapping("api/v2/sln")
@Api(value = "方案控制器", tags = "slnController")
public class SlnClientController extends AbstractController {


    @Autowired
    private SlnService slnService;

    @PostMapping(value = "b1bPage")
    @ApiOperation("查询方案数据列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int", required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", required = true, defaultValue = "20"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", dataType = "datetime", required = true)
    })
    public ResponseEntity<ApiResult<PageInfo<SlnListPojo>>> b1bPage(@RequestParam("pageNo") int pageNo,
                                                                 @RequestParam("pageSize") int pageSize,
                                                                 @RequestParam("startTime")String startTime) {
        ServiceResult<PageInfo<SlnListPojo>> result = slnService.b1bPage(pageNo, pageSize, startTime);

        return new ResponseEntity<>(ApiResult.success(result.getResult()), HttpStatus.OK);
    }



}
