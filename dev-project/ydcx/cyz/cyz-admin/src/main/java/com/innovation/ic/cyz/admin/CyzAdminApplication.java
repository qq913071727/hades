package com.innovation.ic.cyz.admin;

import com.innovation.ic.b1b.framework.helper.LogbackHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = {"com.innovation.ic.cyz", "com.innovation.ic.b1b.framework.util"})
public class CyzAdminApplication {

    private static final Logger log = LoggerFactory.getLogger(CyzAdminApplication.class);

    public static void main(String[] args) {
        //设置日志端口
        LogbackHelper.setYml("application.yml");
        LogbackHelper.setLogbackPort(args);
        SpringApplication.run(CyzAdminApplication.class, args);
        log.info("cyz-admin服务启动成功");
    }
}
