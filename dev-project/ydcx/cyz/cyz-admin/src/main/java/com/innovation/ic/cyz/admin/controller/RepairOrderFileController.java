package com.innovation.ic.cyz.admin.controller;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.vo.cyz.repairOrderFile.RepairOrderFileDeleteVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Api(value = "工单附件API", tags = "RepairOrderFileController")
@RestController
@RequestMapping("/api/v1/repairOrderFile")
public class RepairOrderFileController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(RepairOrderFileController.class);

    /**
     * 工单附件下载
     */
    //@ApiOperation(value = "工单附件下载")
    //@ApiImplicitParam(name = "id", value = "工单附件id", dataType = "Long", required = true)
    @RequestMapping(value = "/download/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> download(@PathVariable("id")Long id, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (id == null) {
            String message = "调用接口[/api/v1/repairOrderFile/download]时，参数id不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> result = repairOrderFileService.download(id, response);

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(result.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(result.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 删除工单附件
     */
    //@ApiOperation(value = "删除工单附件")
   //@ApiImplicitParam(name = "id", value = "工单附件id", dataType = "Long", required = true)
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> delete(@PathVariable("id")Long id, HttpServletRequest request, HttpServletResponse response) {
        if (id == null) {
            String message = "调用接口[/api/v1/repairOrderFile/delete]时，参数id不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> result = repairOrderFileService.delete(id);

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(result.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(result.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 通过文件地址删除工单附件
     */
    //@ApiOperation(value = "通过文件地址删除工单附件")
    //@ApiImplicitParam(name = "RepairOrderFileDeleteVo", value = "工单附件删除的vo类", required = true, dataType = "RepairOrderFileDeleteVo")
    @RequestMapping(value = "/deleteByFilePath", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> deleteByFilePath(@RequestBody RepairOrderFileDeleteVo repairOrderFileDeleteVo, HttpServletRequest request, HttpServletResponse response) {
        if (!StringUtils.validateParameter(repairOrderFileDeleteVo.getFilePath())) {
            String message = "调用接口[/api/v1/repairOrderFile/deleteByFilePath]时，参数filePath不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> result = repairOrderFileService.deleteByFilePath(repairOrderFileDeleteVo.getFilePath());

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(result.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(result.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}