package com.innovation.ic.cyz.gateway;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.innovation.ic.b1b.framework.helper.LogbackHelper;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(scanBasePackages = {"com.innovation.ic.cyz"})
@MapperScan("com.innovation.ic.cyz.base.mapper")
@EnableDiscoveryClient
@EnableAutoConfiguration(exclude = {DruidDataSourceAutoConfigure.class})
public class CyzGatewayApplication {

    private static final Logger log = LoggerFactory.getLogger(CyzGatewayApplication.class);

    public static void main(String[] args) {
        //设置日志端口
        LogbackHelper.setYml("application.yml");
        LogbackHelper.setLogbackPort(args);

        SpringApplication.run(CyzGatewayApplication.class, args);

        log.info("cyz-gateway服务启动成功");
    }
}
