package com.innovation.ic.cyz.gateway.config;

import com.innovation.ic.cyz.base.pojo.constant.DatabaseGlobal;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value = -100)
@Aspect
public class DataSourceAspect {
    private static final Logger log = LoggerFactory.getLogger(DataSourceAspect.class);

//    @Pointcut("execution(* com.innovation.ic.cyz.base.service.im_erp9.impl..*.*(..))")
//    private void imErp9Aspect() {
//    }
//
//    @Pointcut("execution(* com.innovation.ic.cyz.base.service.im_b1b.impl..*.*(..))")
//    private void imB1bAspect() {
//    }

    @Pointcut("execution(* com.innovation.ic.cyz.base.service.cyz.impl..*.*(..))")
    private void cyzAspect() {
    }

    @Pointcut("execution(* com.innovation.ic.cyz.base.service.pve_standard.impl..*.*(..))")
    private void pveStandardAspect() {
    }

//    @Before("imErp9Aspect()")
//    public void imErp9() {
//        log.debug("切换到{} 数据源...", DatabaseGlobal.IM_ERP9);
//        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.IM_ERP9);
//    }
//
//    @Before("imB1bAspect()")
//    public void imB1b() {
//        log.debug("切换到{} 数据源...", DatabaseGlobal.IM_B1B);
//        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.IM_B1B);
//    }

    @Before("cyzAspect()")
    public void cyz() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.CYZ);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.CYZ);
    }

    @Before("pveStandardAspect()")
    public void pveStandard() {
        log.debug("切换到{} 数据源...", DatabaseGlobal.PVE_STANDARD);
        DynamicDataSourceContextHolder.setDataSourceKey(DatabaseGlobal.PVE_STANDARD);
    }
}
