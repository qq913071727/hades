package com.innovation.ic.cyz.gateway.schedule;

import com.innovation.ic.cyz.base.handler.ContextHandler;
import javax.annotation.Resource;

/**
 * 定时任务的抽象类
 */
public abstract class AbstractSchedule {
    @Resource
    protected ContextHandler contextHandler;
}