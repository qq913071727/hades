package com.innovation.ic.cyz.eureka;


import com.innovation.ic.b1b.framework.helper.LogbackHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication(scanBasePackages = {"com.innovation.ic.cyz.eureka"})
@EnableEurekaServer
public class CyzEurekaApplication {

    private static final Logger log = LoggerFactory.getLogger(CyzEurekaApplication.class);

    public static void main(String[] args) {

        //设置日志端口
        LogbackHelper.setYml("application.yml");
        LogbackHelper.setLogbackPort(args);

        SpringApplication.run(CyzEurekaApplication.class, args);

        log.info("cyz-eureka服务启动成功");
    }
}
