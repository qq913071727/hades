package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.b1b.framework.util.IdUtil;
import com.innovation.ic.cyz.base.model.cyz.UserViewLog;
import com.innovation.ic.cyz.base.pojo.constant.AvdApproveStatus;
import com.innovation.ic.cyz.base.pojo.constant.GeneralStatus;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ContributeStatusPojo;
import com.innovation.ic.cyz.base.pojo.global.ApiPageResult;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
//import com.innovation.ic.cyz.base.pojo.constant.JwtTokenType;
import com.innovation.ic.cyz.base.pojo.variable.cyz.avd.*;
import com.innovation.ic.cyz.base.vo.cyz.avd.*;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.var;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Api(value = "避坑指南API", tags = "AvdController")
@RestController
@RequestMapping("/api/v1/avd")
public class AvdController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(AvdController.class);

    /**
     * 避坑指南编辑页选项
     *
     * @return
     */
    @ApiOperation(value = "避坑指南编辑页选项")
    @RequestMapping(value = "/editOptions", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<AvdEditOptionsPojo>> editOptions() {
        ServiceResult<AvdEditOptionsPojo> editOptionsResult = avdService.editOptions();

        ApiResult<AvdEditOptionsPojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(editOptionsResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<ApiResult<AvdEditOptionsPojo>>(apiResult, HttpStatus.OK);
    }

    /**
     * 避坑指南新增/编辑保存
     *
     * @param avdSaveVo
     * @return
     */
//    @JwtToken(type = JwtTokenType.Required)
    @ApiOperation(value = "避坑指南新增/编辑保存【需要验证Token】")
    @ApiImplicitParam(name = "avdId", value = "AvdId", required = false, dataType = "Long")
    @RequestMapping(value = {"/save/{avdId}", "/save"}, method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<AvdSavePojo>> save(@PathVariable(value = "avdId", required = false) Long avdId,
                                                       @RequestBody AvdSaveVo avdSaveVo,
                                                       HttpServletRequest request, HttpServletResponse response) {
        ServiceResult<AvdSavePojo> avdSaveResult;
        if (avdId == null) {
            avdSaveResult = avdService.insertSave(avdSaveVo, this.getUserId());
        } else {
            avdSaveResult = avdService.updateSave(avdSaveVo, avdId, this.getUserId());
        }

        ApiResult<AvdSavePojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(avdSaveResult.getResult());
        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
        return new ResponseEntity<ApiResult<AvdSavePojo>>(apiResult, HttpStatus.OK);
    }

    /**
     * 避坑指南列表/用户中心我的投稿列表
     *
     * @return
     */
//    @JwtToken(type = JwtTokenType.Nullable)
    @ApiOperation(value = "避坑指南列表/用户中心我的投稿列表【Token可空】" +
            "【module(1-文章, 2-视频)(0或null表示全部)】")
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<ApiPageResult<AvdListPojo>>> list(@RequestBody AvdListVo avdListVo,
                                                                      HttpServletRequest request, HttpServletResponse response) {
        if (avdListVo.getIsPublic() == null) {
            avdListVo.setIsPublic(false);
        }
        if (avdListVo.getIsPrivate() == null) {
            avdListVo.setIsPrivate(false);
        }
        if (avdListVo.getIsPublic() == avdListVo.getIsPrivate()) {
            return new ResponseEntity<>(ApiResult.failed("isPublic 和 isPrivate 有且只能有一个为true"), HttpStatus.OK);
        }

        if (avdListVo.getIsPrivate() && this.getUserId() == null) {
            return new ResponseEntity<>(ApiResult.failed("isPrivate 为true时, 需要传递正确的Token"), HttpStatus.OK);
        }

        if (avdListVo.getIsPrivate() && (avdListVo.getApproveStatuses() == null || avdListVo.getApproveStatuses().isEmpty())) {
            return new ResponseEntity<>(ApiResult.failed("isPrivate 为true时, approveStatuses 不能为空"), HttpStatus.OK);
        }

        ServiceResult<AvdListResultPojo> avdsResult = avdService.findAvdList(avdListVo, this.getUserId());

        ApiPageResult<AvdListPojo> apiPageResult = new ApiPageResult<AvdListPojo>();
        apiPageResult.setPageNum(avdListVo.getPage());
        apiPageResult.setPageSize(avdListVo.getSize());
        apiPageResult.setTotal(avdsResult.getResult().getTotal());
        apiPageResult.setList(avdsResult.getResult().getList());
        return new ResponseEntity<>(ApiResult.success(apiPageResult), HttpStatus.OK);
    }

    /**
     * 避坑指南详情
     *
     * @param avdId
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "避坑指南详情")
    @ApiImplicitParam(name = "avdId", value = "AvdId", required = true, dataType = "Long")
    @RequestMapping(value = "/detail/{avdId}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<AvdDetailPojo>> detail(@PathVariable("avdId") Long avdId,
                                                           HttpServletRequest request, HttpServletResponse response) {
        ServiceResult<AvdDetailPojo> avdDetailResult = avdService.findAvdDetailById(avdId);

        ApiResult<AvdDetailPojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(avdDetailResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<ApiResult<AvdDetailPojo>>(apiResult, HttpStatus.OK);
    }

    /**
     * 添加用户浏览记录
     *
     * @param userViewLogAddVo
     * @return
     */
//    @JwtToken(type = JwtTokenType.Nullable)
    @ApiOperation(value = "添加用户浏览记录【Token可空】")
    @RequestMapping(value = "/userViewLogAdd", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> userViewLogAdd(@RequestBody UserViewLogAddVo userViewLogAddVo,
                                                    HttpServletRequest request, HttpServletResponse response) {
        String userId = this.getUserId();

        UserViewLog userViewLog = new UserViewLog();
        userViewLog.setId(IdUtil.snowflakeNextId());
        userViewLog.setType(userViewLogAddVo.getType());
        userViewLog.setBusinessId(userViewLogAddVo.getBusinessId());
        userViewLog.setStatus(GeneralStatus.NORMAL);
        userViewLog.setCreatorId(userId);
        userViewLog.setCreateDate(new Date());
        avdService.userViewLogAdd(userViewLog);

        return new ResponseEntity<>(ApiResult.success(), HttpStatus.OK);
    }

    /**
     * 减少用户点赞等记录
     *
     * @return
     */
//    @JwtToken(type = JwtTokenType.Required)
    @ApiOperation(value = "减少用户点赞等记录【需要验证Token】")
    @RequestMapping(value = "/userViewLogReduce", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> userViewLogReduce(@RequestBody UserViewLogReduceVo userViewLogReduceVo,
                                                       HttpServletRequest request, HttpServletResponse response) {
        avdService.userViewLogReduce(userViewLogReduceVo);
        return new ResponseEntity<>(ApiResult.success(), HttpStatus.OK);
    }

    /**
     * 避坑指南审批通过
     */
    @ApiOperation(value = "避坑指南审批通过")
    @RequestMapping(value = "/approveOk", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> approveOk(@RequestBody AvdApproveVo avdApproveVo,
                                               HttpServletRequest request, HttpServletResponse response) {
        avdService.approveOk(avdApproveVo);
        return new ResponseEntity<>(ApiResult.success(), HttpStatus.OK);
    }

    /**
     * 我的投稿标签
     */
    @ApiOperation(value = "我的投稿标签")
    @RequestMapping(value = "/contributeStatus", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<ContributeStatusPojo>>> contributeStatus() {
        List<ContributeStatusPojo> contributeStatusList = new ArrayList<>();

        var s1 = new ContributeStatusPojo();
        s1.setName("全部");
        s1.setStatuses(Arrays.asList(
                AvdApproveStatus.Draft,
                AvdApproveStatus.UnApprove,
                AvdApproveStatus.Approved,
                AvdApproveStatus.Reject
        ));
        contributeStatusList.add(s1);

        var s2 = new ContributeStatusPojo();
        s2.setName("已发布");
        s2.setStatuses(Arrays.asList(AvdApproveStatus.Approved));
        contributeStatusList.add(s2);

        var s3 = new ContributeStatusPojo();
        s3.setName("未发布");
        s3.setStatuses(Arrays.asList(
                AvdApproveStatus.UnApprove,
                AvdApproveStatus.Reject
        ));
        contributeStatusList.add(s3);

        var s4 = new ContributeStatusPojo();
        s4.setName("草稿箱");
        s4.setStatuses(Arrays.asList(AvdApproveStatus.Draft));
        contributeStatusList.add(s4);

        return new ResponseEntity<>(ApiResult.success(contributeStatusList), HttpStatus.OK);
    }

}
