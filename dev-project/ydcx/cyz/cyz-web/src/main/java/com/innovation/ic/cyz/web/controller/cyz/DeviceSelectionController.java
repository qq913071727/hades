package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionChildrenParamPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionTypeQueryRespPojo;
import com.innovation.ic.cyz.base.service.cyz.DeviceSelectionService;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

@Api(value = "器件选型", tags = "DeviceSelectionController")
@RestController
@RequestMapping("/api/v1/deviceSelection")
public class DeviceSelectionController extends AbstractController {
    @Resource
    protected DeviceSelectionService deviceSelectionService;

    /**
     * 查询器件选型类别
     * @return 返回查询结果
     */
    @ApiOperation(value = "查询器件选型类别")
    @RequestMapping(value = "/findDeviceSelection", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<DeviceSelectionTypeQueryRespPojo>>> findDeviceSelection() {
        ServiceResult<List<DeviceSelectionTypeQueryRespPojo>> deviceSelectionTypeResult = deviceSelectionService.findDeviceSelection();
        ApiResult<List<DeviceSelectionTypeQueryRespPojo>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(deviceSelectionTypeResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询器件选型菜单
     * @return 返回查询结果
     */
    @ApiOperation(value = "查询器件选型菜单")
    @RequestMapping(value = "/findDeviceSelectionMenu", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<DeviceSelectionChildrenParamPojo>>> findDeviceSelectionMenu() {
        ServiceResult<List<DeviceSelectionChildrenParamPojo>> deviceSelectionTypeResult = deviceSelectionService.findDeviceSelectionMenu();
        ApiResult<List<DeviceSelectionChildrenParamPojo>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(deviceSelectionTypeResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}