package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.cyz.base.pojo.global.ApiPageResult;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
//import com.innovation.ic.cyz.base.pojo.annotation.JwtToken;
//import com.innovation.ic.cyz.base.pojo.constant.JwtTokenType;
import com.innovation.ic.cyz.base.pojo.variable.cyz.user_view_log.UserViewLogListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.user_view_log.UserViewLogListResultPojo;
import com.innovation.ic.cyz.base.vo.cyz.UserViewLogListVo;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(value = "用户浏览/点赞/收藏记录API", tags = "UserViewLogController")
@RestController
@RequestMapping("/api/v1/userViewLog")
public class UserViewLogController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(AvdController.class);

    /**
     * 用户浏览/点赞/收藏记录列表
     */
//    @JwtToken(type = JwtTokenType.Required)
    @ApiOperation(value = "用户浏览/点赞/收藏记录列表【需要验证Token】" +
            "【(我的收藏 types: [3], 我的点赞 types: [2], 最近浏览 types: [1])】" +
            "【contentModule 内容的类型(1-文章, 2-视频)(0或null表示全部)】")
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<ApiPageResult<UserViewLogListPojo>>> list(@RequestBody UserViewLogListVo userViewLogListVo,
                                                                              HttpServletRequest request, HttpServletResponse response) {
        ServiceResult<UserViewLogListResultPojo> serviceResult = userViewLogService.findUserViewLogList(userViewLogListVo, this.getUserId());

        ApiPageResult<UserViewLogListPojo> apiPageResult = new ApiPageResult<>();
        apiPageResult.setPageNum(userViewLogListVo.getPage());
        apiPageResult.setPageSize(userViewLogListVo.getSize());
        apiPageResult.setTotal(serviceResult.getResult().getTotal());
        apiPageResult.setList(serviceResult.getResult().getList());
        return new ResponseEntity<>(ApiResult.success(apiPageResult), HttpStatus.OK);
    }

}
