package com.innovation.ic.cyz.web.schedule;

import com.innovation.ic.cyz.base.model.cyz.Tenant;
import com.innovation.ic.cyz.base.model.cyz.TenantImg;
import com.innovation.ic.cyz.base.pojo.variable.tenant_import.TenantImport;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class ImportTenantTask extends AbstractSchedule {
    private static final Logger log = LoggerFactory.getLogger(ImportTenantTask.class);

    /**
     * 调用比一比接口，获取原厂数据并保存
     */
  //  @Scheduled(cron = "* 0/10 * * * ?")
//    @Scheduled(fixedRate = 10000)
//    @PostConstruct
    @XxlJob("importTenantTaskHandler")
    private void importTenant() {
        log.info("定时任务开启：调用一次比一比接口，获取原厂数据并保存");

        ServiceResult<TenantImport> tenantsFromB1bApiServiceResult = importTenantService.getTenantsFromB1bApi();
        if (!tenantsFromB1bApiServiceResult.getSuccess()) {
            log.error("定时任务中报错：调用比一比原厂数据接口报错:" + tenantsFromB1bApiServiceResult.getMessage());
            return;
        }

        ServiceResult<List<Tenant>> convertToTenantResult = importTenantService.ConvertTenantImportToTenant(tenantsFromB1bApiServiceResult.getResult());
        ServiceResult<List<TenantImg>> convertToTenantImgResult = importTenantService.ConvertTenantImportToTenantImg(tenantsFromB1bApiServiceResult.getResult());
        importTenantService.saveTenantList(convertToTenantResult.getResult(), convertToTenantImgResult.getResult());
    }
}