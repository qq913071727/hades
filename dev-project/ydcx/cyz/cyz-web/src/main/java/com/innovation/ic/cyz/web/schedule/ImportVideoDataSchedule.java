package com.innovation.ic.cyz.web.schedule;


import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


/**
 * 视频相关表同步
 */
@Component
@Slf4j
public class ImportVideoDataSchedule extends AbstractSchedule {



    /**
     * video,批量存入redis
     */
    @XxlJob("importVideoHandler")
    private void importVideoSchedule() {
        super.videoDataScheduleService.importVideoRedisSchedule();
    }


    /**
     * vid_category,批量存入redis
     */
    @XxlJob("importVidCategoryHandler")
    private void importVidCategorySchedule() {
        super.videoDataScheduleService.importVidCategoryRedisSchedule();
    }



    /**
     * vid_brand,批量存入redis
     */
    @XxlJob("importVidBrandHandler")
    private void importVidBrandSchedule() {
        super.videoDataScheduleService.importVidBrandRedisSchedule();
    }


    /**
     * vid_tag,批量存入redis
     */
    @XxlJob("importVidTagHandler")
    private void importVidTagSchedule() {
        super.videoDataScheduleService.importVidTagRedisSchedule();
    }


    /**
     * vid_type,批量存入redis
     */
    @XxlJob("importVidTypeHandler")
    private void importVidTypeSchedule() {
        super.videoDataScheduleService.importVidTypeRedisSchedule();
    }

    /**
     *
     * vid_rel 视频关系表,批量存入redis
     */
    @XxlJob("importVidRelHandler")
    private void importVidRelSchedule() {
        super.videoDataScheduleService.importVidRelRedisSchedule();
    }

}
