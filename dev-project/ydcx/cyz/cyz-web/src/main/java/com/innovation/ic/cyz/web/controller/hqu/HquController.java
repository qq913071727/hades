package com.innovation.ic.cyz.web.controller.hqu;

import com.innovation.ic.b1b.framework.util.StringUtils;

import com.innovation.ic.cyz.base.model.cyz.Product;
import com.innovation.ic.cyz.base.model.cyz.ProductProperty;
import com.innovation.ic.cyz.base.model.cyz.ProductUnique;
import com.innovation.ic.cyz.base.pojo.constant.HqMysqlTable;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;


import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


@Api(value = "与虎趣交互数据API", tags = "HquController")
@RestController
@RequestMapping("/api/v3/hqu")
public class HquController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(HquController.class);

    /**
     * 虎趣从redis获取相应数据
     *
     * @return
     */
    @ApiOperation(value = "虎趣从redis获取相应数据")
    @RequestMapping(value = "/page", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> page(@RequestParam("tableName") String tableName, @RequestParam("pageSize") Integer pageSize, @RequestParam("pageNo") Integer pageNo,
                                          HttpServletRequest request, HttpServletResponse response) {
        if (!StringUtils.validateParameter(tableName) || null == pageSize || null == pageNo) {
            String message = "调用接口[/api/v3/hqu/page]时，tableName、pageSize、pageNo不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        //特殊的表 需要走mysql
        if (tableName.equals(HqMysqlTable.TABLE_PRODUCT)) {
            ServiceResult<List<Product>> serviceResult = hquService.selectMysqlProductPage(tableName, pageSize, pageNo);
            ApiResult<List<Product>> apiResult = new ApiResult<>();
            apiResult.setSuccess(serviceResult.getSuccess());
            apiResult.setCode(HttpStatus.OK.value());
            apiResult.setResult(serviceResult.getResult());
            apiResult.setMessage(serviceResult.getMessage());
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } else if (tableName.equals(HqMysqlTable.TABLE_PRODUCT_PROPERTY)) {
            ServiceResult<List<ProductProperty>> serviceResult = hquService.selectMysqlProductPropertyPage(tableName, pageSize, pageNo);
            ApiResult<List<ProductProperty>> apiResult = new ApiResult<>();
            apiResult.setSuccess(serviceResult.getSuccess());
            apiResult.setCode(HttpStatus.OK.value());
            apiResult.setResult(serviceResult.getResult());
            apiResult.setMessage(serviceResult.getMessage());
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } else if (tableName.equals(HqMysqlTable.TABLE_PRODUCT_UNIQUE)) {
            ServiceResult<List<ProductUnique>> serviceResult = hquService.selectMysqlProductUniquePage(tableName, pageSize, pageNo);
            ApiResult<List<ProductUnique>> apiResult = new ApiResult<>();
            apiResult.setSuccess(serviceResult.getSuccess());
            apiResult.setCode(HttpStatus.OK.value());
            apiResult.setResult(serviceResult.getResult());
            apiResult.setMessage(serviceResult.getMessage());
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        } else {
            //去redis中获取需要的数据并进行分页返回
            ServiceResult<List<Object>> listServiceResult = hquService.selectRedisPage(tableName, pageSize, pageNo);
            ApiResult<List<Object>> apiResult = new ApiResult<>();
            apiResult.setSuccess(listServiceResult.getSuccess());
            apiResult.setCode(HttpStatus.OK.value());
            apiResult.setResult(listServiceResult.getResult());
            apiResult.setMessage(listServiceResult.getMessage());
            return new ResponseEntity<>(apiResult, HttpStatus.OK);
        }
    }

}
