package com.innovation.ic.cyz.web.controller;

import com.auth0.jwt.interfaces.Claim;
import com.innovation.ic.b1b.framework.util.JwtUtils;
import com.innovation.ic.cyz.base.pojo.constant.HttpHeader;
import com.innovation.ic.cyz.base.service.cyz.*;
import com.innovation.ic.cyz.base.service.cyz.impl.ServiceHelper;
import com.innovation.ic.cyz.base.service.hqu.HquService;
import com.innovation.ic.cyz.base.service.pve_standard.ProductSearchService;
import com.innovation.ic.cyz.base.value.config.FilterParamConfig;
import com.innovation.ic.cyz.base.value.config.FtpAccountConfig;
import com.innovation.ic.cyz.base.value.config.MinioPropConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 抽象controller
 */
public abstract class AbstractController {

    /************************************* config *****************************************/
    @Autowired
    protected FtpAccountConfig ftpAccountConfig;

    @Autowired
    protected MinioPropConfig minioPropConfig;

    @Autowired
    protected FilterParamConfig filterParamConfig;

    /************************************* manager *****************************************/
    @Autowired
    protected ServiceHelper serviceHelper;

    /************************************* service *****************************************/
    @Autowired
    protected AvdService avdService;

    @Autowired
    protected HquService hquService;

    @Autowired
    protected CarouselService carouselService;

    @Autowired
    protected FeedbackService feedbackService;

    @Autowired
    protected InquiryService inquiryService;

    @Autowired
    protected ImportSlnService importSlnService;

    @Autowired
    protected MfrPcbWebService mfrPcbWebService;

    @Autowired
    protected ProductSearchService productSearchService;

    @Autowired
    protected RegionService regionService;

    @Autowired
    protected CategoriesPropertyCyzService categoriesPropertyCyzService;

    @Resource
    protected RepairOrderService repairOrderService;

    @Autowired
    protected MfrSmtService smtService;

    @Autowired
    protected SlnService slnService;

    @Autowired
    protected UserService userService;

    @Autowired
    protected UserViewLogService userViewLogService;

    @Autowired
    protected VideoService videoService;

    @Autowired
    protected RepairOrderTypeService repairOrderTypeService;

    @Autowired
    protected RepairOrderFileService repairOrderFileService;

    @Autowired
    protected DeviceSelectionParameterService deviceSelectionParameterService;

    @Autowired
    protected ProductService productService;

    @Autowired
    protected ProductPropertyService productPropertyService;

    @Autowired
    protected CategorieseCyzService categorieseCyzService;

    @Autowired
    protected ProductUniqueService productUniqueService;
    /**
     * 获取request对象
     */
    protected HttpServletRequest getRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            return null;
        }
        return ((ServletRequestAttributes) requestAttributes).getRequest();
    }

    /**
     * 获取response对象
     **/
    protected HttpServletResponse getResponse() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            return null;
        }
        return ((ServletRequestAttributes) requestAttributes).getResponse();
    }

    /**
     * 获取Token
     */
    protected String getToken() {
        HttpServletRequest request = getRequest();
        String tokenString = request.getHeader(filterParamConfig.getToken());
        return tokenString.split(HttpHeader.TOKEN_SPLIT)[1];
    }

    /**
     * 获取用户Id
     *
     * @return
     */
    protected String getUserId() {
        String token = getToken();
        if (token == null) {
            return null;
        }

        Map<String, Claim> claims = JwtUtils.getPayloadByToken(token);
        return claims.get("userId").asString().replace("\"", "");
    }
}

