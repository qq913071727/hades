package com.innovation.ic.cyz.web;

import com.innovation.ic.b1b.framework.helper.LogbackHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = {"com.innovation.ic.cyz"})
@EnableDiscoveryClient
@EnableScheduling
public class CyzWebApplication {

    private static final Logger log = LoggerFactory.getLogger(CyzWebApplication.class);

    public static void main(String[] args) {
        //设置日志端口
        LogbackHelper.setYml("application.yml");
        LogbackHelper.setLogbackPort(args);

        SpringApplication.run(CyzWebApplication.class, args);

        log.info("cyz-web服务启动成功");
    }

}
