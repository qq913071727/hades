package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.RegionPojo;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@Api(value = "区域Api", tags = "RegionController")
@RestController
@RequestMapping("/api/v1/region")
public class RegionController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(RegionController.class);

    @ApiOperation(value = "获取列表")
    @ResponseBody
    @GetMapping("/getCities")
    public ResponseEntity<ApiResult<List<RegionPojo>>> list() {
        ApiResult<List<RegionPojo>> result = new ApiResult<>();

        ServiceResult<List<RegionPojo>> serviceResult = regionService.getCities();
        result.setResult(serviceResult.getResult());
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }
}