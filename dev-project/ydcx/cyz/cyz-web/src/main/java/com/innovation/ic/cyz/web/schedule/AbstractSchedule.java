package com.innovation.ic.cyz.web.schedule;

import com.innovation.ic.b1b.framework.manager.ThreadPoolManager;
import com.innovation.ic.cyz.base.service.cyz.BrandService;
import com.innovation.ic.cyz.base.service.cyz.CarouselService;
import com.innovation.ic.cyz.base.service.cyz.CategoriesPropertyCyzService;
import com.innovation.ic.cyz.base.service.cyz.CategorieseCyzService;
import com.innovation.ic.cyz.base.service.cyz.DataService;
import com.innovation.ic.cyz.base.service.cyz.DeviceSelectionScheduleService;
import com.innovation.ic.cyz.base.service.cyz.ImportSlnService;
import com.innovation.ic.cyz.base.service.cyz.ImportTenantService;
import com.innovation.ic.cyz.base.service.cyz.ProductPropertyService;
import com.innovation.ic.cyz.base.service.cyz.ProductService;
import com.innovation.ic.cyz.base.service.cyz.ProductUniqueService;
import com.innovation.ic.cyz.base.service.cyz.VideoDataScheduleService;
import com.innovation.ic.cyz.base.service.cyz.impl.ServiceHelper;
import com.innovation.ic.cyz.base.service.pve_standard.BrandsService;
import com.innovation.ic.cyz.base.service.pve_standard.CategoriesPropertyService;
import com.innovation.ic.cyz.base.service.pve_standard.DeviceCategoriesService;
import com.innovation.ic.cyz.base.service.pve_standard.ProductsPropertyService;
import com.innovation.ic.cyz.base.service.pve_standard.ProductsService;
import com.innovation.ic.cyz.base.service.pve_standard.ProductsUniqueService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 定时任务的抽象类
 */
@Slf4j
public abstract class AbstractSchedule {
    @Autowired
    protected ImportTenantService importTenantService;

    @Autowired
    protected ImportSlnService importSlnService;

    @Autowired
    protected CarouselService carouselService;

    @Autowired
    protected DataService dataService;

    @Autowired
    protected BrandsService brandsService;

    @Autowired
    protected BrandService brandService;

    @Autowired
    protected ProductService productService;

    @Autowired
    protected ProductsService productsService;

    @Autowired
    protected ProductUniqueService productUniqueService;

    @Autowired
    protected ProductsUniqueService productsUniqueService;

    @Autowired
    protected DeviceSelectionScheduleService deviceSelectionScheduleService;

    @Autowired
    protected VideoDataScheduleService videoDataScheduleService;

    @Autowired
    protected DeviceCategoriesService deviceCategoriesService;

    @Autowired
    protected CategoriesPropertyService categoriesPropertyService;

    @Autowired
    protected CategoriesPropertyCyzService categoriesPropertyCyzService;

    @Autowired
    protected CategorieseCyzService categorieseCyzService;

    @Autowired
    protected ProductsPropertyService productsPropertyService;

    @Autowired
    protected ProductPropertyService productPropertyService;

    @Autowired
    protected ServiceHelper serviceHelper;

    @Autowired
    protected ThreadPoolManager threadPoolManager;

    /**
     * 删除Redis key
     *
     * @param key
     */
    protected void deleteRedisKey(String key) {
        if (StringUtils.isEmpty(key)) {
            return;
        }
        try {
            //删除redis数据
            serviceHelper.getRedisManager().del(key);
            log.info("定时任务,删除Redis key :  {} 成功 ", key);
        } catch (Exception e) {
            log.error("定时任务,删除Redis key :  {} 失败 ", key, e);
        }
    }


}