package com.innovation.ic.cyz.web.controller.cyz;

import com.google.common.base.Strings;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cyz.base.pojo.enums.DeviceSelectionParameterTypeEnum;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_parameter.DeviceSelectionParameterListRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_parameter.DeviceSelectionParameterQueryListRespPojo;
import com.innovation.ic.cyz.base.service.cyz.DeviceSelectionParameterService;
import com.innovation.ic.cyz.base.vo.cyz.deviceSelectionParameter.DeviceSelectionParameterFuzzyQueryVo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_property.PsPropertyVo;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Api(value = "器件选型类型包含参数", tags = "DeviceSelectionParameterController")
@RestController
@RequestMapping("/api/v1/deviceSelectionParameter")
public class DeviceSelectionParameterController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(DeviceSelectionParameterController.class);

    @Resource
    protected DeviceSelectionParameterService deviceSelectionParameterService;

    /**
     * 通过器件选型类型id查询器件选型类型包含参数
     *
     * @return 返回查询结果
     */
    //@ApiOperation(value = "通过器件选型类型id查询器件选型类型包含参数")
    //@ApiImplicitParam(name = "typeId", value = "器件选型类型id", dataType = "String", required = true)
    @RequestMapping(value = "/findDeviceSelectionParameterByTypeId/{typeId}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<DeviceSelectionParameterListRespPojo>> findDeviceSelectionParameterByTypeId(@PathVariable("typeId") String typeId) {
        if (typeId == null) {
            String message = "调用接口[/api/v1/deviceSelectionParameter/findDeviceSelectionParameterByTypeId]时，参数typeId不能为空";
            log.warn(message);
            ApiResult<DeviceSelectionParameterListRespPojo> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<DeviceSelectionParameterListRespPojo> serviceResult = deviceSelectionParameterService.findDeviceSelectionParameterByTypeId(typeId);

        ApiResult<DeviceSelectionParameterListRespPojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(serviceResult.getResult());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 通过分类id获取快速筛选参数
     */
    @ApiOperation(value = "通过分类id获取快速筛选参数")
    @ApiImplicitParam(name = "PsPropertyVo", value = "器件选型属性的vo类", required = true, dataType = "PsPropertyVo")
    @RequestMapping(value = "/getFastSelectProperties", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<DeviceSelectionParameterQueryListRespPojo>> getFastSelectProperties(@RequestBody PsPropertyVo psPropertyVo, HttpServletRequest request, HttpServletResponse response) {
        if (!StringUtils.validateParameter(psPropertyVo.getCategoryId())) {
            String message = "调用接口[/api/v1/productSearch/getFastSelectProperties]时，参数categoryId不能为空";
            log.warn(message);
            ApiResult<DeviceSelectionParameterQueryListRespPojo> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 通过器件选型类型id获取器件选型类型包含参数id集合
        ServiceResult<List<String>> externallyUniqueIdList = deviceSelectionParameterService.getExternallyUniqueIdListByCategoryId(psPropertyVo.getCategoryId(), DeviceSelectionParameterTypeEnum.FAST_SCREEN.getCode());

        // 获取筛选参数
        // ServiceResult<DeviceSelectionParameterQueryListRespPojo> propertiesResult = productSearchService.getSelectProperties(psPropertyVo, externallyUniqueIdList.getResult());
        //更改为mysql查询
        ServiceResult<DeviceSelectionParameterQueryListRespPojo> propertiesResult = productPropertyService.getSelectProperties(psPropertyVo, externallyUniqueIdList.getResult());

        return new ResponseEntity<>(ApiResult.success(propertiesResult.getResult()), HttpStatus.OK);
    }

    /**
     * 通过分类id获取参数筛选参数
     */
    @ApiOperation(value = "通过分类id获取参数筛选参数")
    @ApiImplicitParam(name = "PsPropertyVo", value = "器件选型属性的vo类", required = true, dataType = "PsPropertyVo")
    @RequestMapping(value = "/getParamSelectProperties", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<DeviceSelectionParameterQueryListRespPojo>> getParamSelectProperties(@RequestBody PsPropertyVo psPropertyVo, HttpServletRequest request, HttpServletResponse response) {
        if (!StringUtils.validateParameter(psPropertyVo.getCategoryId())) {
            String message = "调用接口[/api/v1/productSearch/getParamSelectProperties]时，参数categoryId不能为空";
            log.warn(message);
            ApiResult<DeviceSelectionParameterQueryListRespPojo> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 通过器件选型类型id获取器件选型类型包含参数id集合
        ServiceResult<List<String>> externallyUniqueIdList = deviceSelectionParameterService.getExternallyUniqueIdListByCategoryId(psPropertyVo.getCategoryId(), DeviceSelectionParameterTypeEnum.PARAM_SCREEN.getCode());

        // 获取筛选参数
        //ServiceResult<DeviceSelectionParameterQueryListRespPojo> propertiesResult = productSearchService.getSelectProperties(psPropertyVo, externallyUniqueIdList.getResult());
        //更改为mysql查询
        ServiceResult<DeviceSelectionParameterQueryListRespPojo> propertiesResult = productPropertyService.getSelectProperties(psPropertyVo, externallyUniqueIdList.getResult());
        return new ResponseEntity<>(ApiResult.success(propertiesResult.getResult()), HttpStatus.OK);
    }

    /**
     * 参数模糊查询
     */
    @ApiOperation(value = "参数模糊查询")
    @ApiImplicitParam(name = "DeviceSelectionParameterFuzzyQueryVo", value = "参数模糊查询的vo类", required = true, dataType = "DeviceSelectionParameterFuzzyQueryVo")
    @RequestMapping(value = "/queryFuzzySelectProperties", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> queryFuzzySelectProperties(@RequestBody DeviceSelectionParameterFuzzyQueryVo deviceSelectionParameterFuzzyQueryVo, HttpServletRequest request, HttpServletResponse response) {
        if (Strings.isNullOrEmpty(deviceSelectionParameterFuzzyQueryVo.getPropertyId()) || Strings.isNullOrEmpty(deviceSelectionParameterFuzzyQueryVo.getContext())) {
            String message = "调用接口[/api/v1/productSearch/queryFuzzySelectProperties]时，参数propertyId和context不能为空";
            log.warn(message);
            ApiResult<List<String>> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        // 参数筛选模糊查询
        //ServiceResult<List<String>> propertiesResult = productSearchService.queryFuzzyParamSelectProperties(deviceSelectionParameterFuzzyQueryVo);
        //更改为mysql查询
        ServiceResult<List<String>> propertiesResult = productPropertyService.queryFuzzyParamSelectPropertiesCyz(deviceSelectionParameterFuzzyQueryVo);

        ApiResult<List<String>> apiResult = new ApiResult<>();
        apiResult.setSuccess(propertiesResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(propertiesResult.getMessage());
        apiResult.setResult(propertiesResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}