package com.innovation.ic.cyz.web.schedule;

import com.innovation.ic.cyz.base.pojo.enums.RedisKeyPrefixEnum;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.web.thread.HandleProductUniqueDataThread;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @desc   导入标准库产品唯一信息表数据定时任务
 * @author linuo
 * @time   2022年11月1日13:58:42
 */
@Component
public class ImportProductUniqueDataTask extends AbstractSchedule {
    private static final Logger log = LoggerFactory.getLogger(ImportProductUniqueDataTask.class);

    /**
     * 获取标准库ProductsUnique表数据导入到cyz的数据库中
     */
    @XxlJob("importProductUniqueDataTaskJobHandler")
    private ReturnT<String> importProductDataTaskJob() {
        log.info("定时任务开启: 先删除本地product_unique表的数据,获取标准库ProductsUnique表数据导入到cyz的数据库中");

        // 清空product_unique表数据
        productUniqueService.truncateProductUnique();
        //清空redis缓存
        String key = RedisKeyPrefixEnum.TABLE.getCode() + ":" + RedisKeyPrefixEnum.TABLE_PRODUCT_UNIQUE.getCode();
        super.deleteRedisKey(key);
        // 查询标准库ProductsUnique表状态是正常的数据数量
        ServiceResult<Long> result = productsUniqueService.getNormalStatusDataCount();
        Long count = result.getResult();
        if(count != null && count > 0) {
            log.info("ProductsUnique表共有[{}]条数据需要处理", count);

            // 最大线程数
            int maxThreadCount = 15;

            // 每个线程处理的数据数量
            int threadHandleDataCount = (int) Math.ceil(count * 1.0 / maxThreadCount);

            for (int i = 0; i < maxThreadCount; i++) {
                long start = threadHandleDataCount * i + 1;
                long end = threadHandleDataCount * (i + 1);
                if(end > count){
                    end = count;
                }

                // 创建多线程处理标准库ProductsUnique数据
                HandleProductUniqueDataThread handleProductUniqueDataThread = new HandleProductUniqueDataThread(productsUniqueService, productUniqueService, start, end);
                handleProductUniqueDataThread.setDaemon(Boolean.TRUE);
                threadPoolManager.execute(handleProductUniqueDataThread);
            }
        }

        return ReturnT.SUCCESS;
    }
}