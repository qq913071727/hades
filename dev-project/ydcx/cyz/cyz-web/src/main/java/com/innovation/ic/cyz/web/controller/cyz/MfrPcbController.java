package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.cyz.base.pojo.global.ApiPageResult;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrPcbDetailPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrPcbListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrPcbListResultPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.mfrpcb_search_option.MfrPcbOptionLine;
import com.innovation.ic.cyz.base.vo.cyz.MfrPcbListVo;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * PCB厂家API
 */
@Api(value = "PCB厂家API", tags = "MfrPcbController")
@RestController
@RequestMapping("/api/v1/mfrpcb")
public class MfrPcbController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(MfrPcbController.class);

    /**
     * PCB厂家列表
     *
     * @return
     */
    @ApiOperation(value = "PCB厂家列表")
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<ApiPageResult<MfrPcbListPojo>>> list(@RequestBody MfrPcbListVo mfrListVo,
                                                                         HttpServletRequest request, HttpServletResponse response) throws NoSuchFieldException, IllegalAccessException {
        ServiceResult<MfrPcbListResultPojo> mfrsResult = mfrPcbWebService.findMfrList(mfrListVo);

        ApiPageResult<MfrPcbListPojo> apiPageResult = new ApiPageResult<MfrPcbListPojo>();
        apiPageResult.setPageNum(mfrListVo.getPage());
        apiPageResult.setPageSize(mfrListVo.getSize());
        apiPageResult.setTotal(mfrsResult.getResult().getTotal());
        apiPageResult.setList(mfrsResult.getResult().getList());
        return new ResponseEntity<>(ApiResult.success(apiPageResult), HttpStatus.OK);
    }

    /**
     * PCB厂家详情
     *
     * @param response
     * @return
     */
    @ApiOperation(value = "PCB厂家详情")
    @ApiImplicitParam(name = "mfrId", value = "MfrId", required = true, dataType = "Long")
    @RequestMapping(value = "/detail/{mfrId}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<MfrPcbDetailPojo>> detail(@PathVariable("mfrId") Long mfrId,
                                                              HttpServletRequest request, HttpServletResponse response)
            throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException, InstantiationException {
        ServiceResult<MfrPcbDetailPojo> mfrPcbDetailResult = mfrPcbWebService.findMfrById(mfrId);

        ApiResult<MfrPcbDetailPojo> apiResult = new ApiResult<MfrPcbDetailPojo>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(mfrPcbDetailResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<ApiResult<MfrPcbDetailPojo>>(apiResult, HttpStatus.OK);
    }

    /**
     * PCB厂家列表选项
     *
     * @return
     */
    @ApiOperation(value = "PCB厂家列表选项")
    @RequestMapping(value = "/listOptions", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<MfrPcbOptionLine>>> listOptions() {
        ServiceResult<List<MfrPcbOptionLine>> listOptionsResult = mfrPcbWebService.findListOptions();

        ApiResult<List<MfrPcbOptionLine>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(listOptionsResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<ApiResult<List<MfrPcbOptionLine>>>(apiResult, HttpStatus.OK);
    }
}
