package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.product.ProductQueryRespPojo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsPartNumbersQueryByContextVo;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * 产品表API
 */
@Api(value = "产品表API", tags = "ProductController")
@RestController
@RequestMapping("/api/v1/product")
public class ProductController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(MfrPcbController.class);

    @ApiOperation(value = "型号模糊查询")
    @ApiImplicitParam(name = "PsPartNumbersQueryByContextVo", value = "型号模糊查询的vo类", required = true, dataType = "PsPartNumbersQueryByContextVo")
    @RequestMapping(value = "/queryPartNumbersByContext", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> queryPartNumbersByContext(@RequestBody PsPartNumbersQueryByContextVo psPartNumbersQueryByContextVo) {
        if (!StringUtils.validateParameter(psPartNumbersQueryByContextVo.getPartNumber()) || psPartNumbersQueryByContextVo.getType() == null) {
            String message = "调用接口[/api/v1/product/queryPartNumbersByContext]时，参数partNumber、type不能为空";
            log.warn(message);
            ApiResult<ApiResult<List<String>>> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<List<ProductQueryRespPojo>> serviceResult = productService.queryPartNumbersByContext(psPartNumbersQueryByContextVo);

        ApiResult<List<ProductQueryRespPojo>> result = new ApiResult<>();
        result.setResult(serviceResult.getResult());
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }
}