package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
//import com.innovation.ic.cyz.base.pojo.annotation.JwtToken;
//import com.innovation.ic.cyz.base.pojo.constant.JwtTokenType;
import com.innovation.ic.cyz.base.pojo.variable.cyz.feedback.FeedbackTypeListPojo;
import com.innovation.ic.cyz.base.vo.cyz.feedback.FeedbackSaveVo;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Api(value = "信息反馈API", tags = "FeedbackController")
@RestController
@RequestMapping("/api/v1/feedback")
public class FeedbackController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(FeedbackController.class);

    /**
     * 信息反馈类型
     */
    @ApiOperation(value = "信息反馈类型")
    @RequestMapping(value = "/types", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<FeedbackTypeListPojo>>> types(HttpServletRequest request, HttpServletResponse response) throws IllegalAccessException {
        ServiceResult<List<FeedbackTypeListPojo>> serviceResult = feedbackService.getFeedbackTypes();
        return new ResponseEntity<ApiResult<List<FeedbackTypeListPojo>>>(ApiResult.success(serviceResult.getResult()), HttpStatus.OK);
    }

    /**
     * 信息反馈保存
     */
//    @JwtToken(type = JwtTokenType.Nullable)
    @ApiOperation(value = "信息反馈保存【Token可空】")
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> save(@RequestBody FeedbackSaveVo feedbackSaveVo,
                                          HttpServletRequest request, HttpServletResponse response) {
        feedbackService.save(feedbackSaveVo);
        return new ResponseEntity<ApiResult>(ApiResult.success(), HttpStatus.OK);
    }

}
