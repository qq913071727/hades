package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.cyz.base.pojo.global.ApiPageResult;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.SlnDetailPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.SlnListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.SlnListResultPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.sln_search_option.SlnOptionLine;
import com.innovation.ic.cyz.base.vo.cyz.SlnListVo;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 方案API
 */
@Api(value = "方案API", tags = "SlnController")
@RestController
@RequestMapping("/api/v1/sln")
public class SlnController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(MfrPcbController.class);

    /**
     * 方案列表
     *
     * @return
     */
    @ApiOperation(value = "方案列表")
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<ApiPageResult<SlnListPojo>>> list(@RequestBody SlnListVo slnListVo,
                                                                      HttpServletRequest request, HttpServletResponse response) {
        ServiceResult<SlnListResultPojo> slnResult = slnService.findSlnList(slnListVo);

        ApiPageResult<SlnListPojo> apiPageResult = new ApiPageResult<SlnListPojo>();
        apiPageResult.setPageNum(slnListVo.getPage());
        apiPageResult.setPageSize(slnListVo.getSize());
        apiPageResult.setTotal(slnResult.getResult().getTotal());
        apiPageResult.setList(slnResult.getResult().getList());
        return new ResponseEntity<>(ApiResult.success(apiPageResult), HttpStatus.OK);
    }

    @ApiOperation(value = "方案详情")
    @ApiImplicitParam(name = "slnId", value = "SlnId", required = true, dataType = "Long")
    @RequestMapping(value = "/detail/{slnId}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<SlnDetailPojo>> detail(@PathVariable("slnId") Long slnId,
                                                           HttpServletRequest request, HttpServletResponse response) {
        ServiceResult<SlnDetailPojo> slnDetailResult = slnService.findSlnDetailById(slnId);

        ApiResult<SlnDetailPojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(slnDetailResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<ApiResult<SlnDetailPojo>>(apiResult, HttpStatus.OK);
    }

    /**
     * 方案列表选项
     *
     * @return
     */
    @ApiOperation(value = "方案列表选项")
    @RequestMapping(value = "/listOptions", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<SlnOptionLine>>> listOptions() {
        ServiceResult<List<SlnOptionLine>> listOptionsResult = slnService.findListOptions();

        ApiResult<List<SlnOptionLine>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(listOptionsResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<ApiResult<List<SlnOptionLine>>>(apiResult, HttpStatus.OK);
    }

}
