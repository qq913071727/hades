package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.vo.cyz.InquiryMiniSaveVo;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 在线询价API
 */
@Api(value = "在线询价API", tags = "InquiryController")
@RestController
@RequestMapping("/api/v1/inquiry")
public class InquiryController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(InquiryController.class);

    /**
     * 在线询价提交
     */
    @ApiOperation(value = "在线询价提交")
    @RequestMapping(value = "/miniSave", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> miniSave(@RequestBody InquiryMiniSaveVo inquiryMiniSaveVo,
                                              HttpServletRequest request, HttpServletResponse response) {
        

        inquiryService.miniSave(inquiryMiniSaveVo);
        return new ResponseEntity<>(ApiResult.success(), HttpStatus.OK);
    }

}
