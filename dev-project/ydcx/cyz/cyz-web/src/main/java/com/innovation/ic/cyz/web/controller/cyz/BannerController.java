package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.banner.BannerAvailableDataRespPojo;
import com.innovation.ic.cyz.base.service.cyz.BannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

/**
 * @desc   首页banner
 * @author linuo
 * @time   2022年10月17日09:37:12
 */
@Api(value = "首页banner", tags = "BannerController")
@RestController
@RequestMapping("/api/v1/banner")
public class BannerController {
    @Resource
    private BannerService bannerService;

    /**
     * 获取启用的轮播图数据
     * @return 返回查询结果
     */
    @ApiOperation(value = "获取首页轮播图数据")
    @RequestMapping(value = "/getAvailableBannerList", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<BannerAvailableDataRespPojo>>> allBanner() {
        ServiceResult<List<BannerAvailableDataRespPojo>> bannerServiceResult = bannerService.getAvailableBannerList();

        ApiResult<List<BannerAvailableDataRespPojo>> apiResult = new ApiResult<>();
        apiResult.setSuccess(bannerServiceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(bannerServiceResult.getMessage());
        apiResult.setResult(bannerServiceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}