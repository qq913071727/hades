package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 测试解决方案中同步数据中的 下载功能 API
 */
@Api(value = "测试解决方案中同步数据中的 下载功能 API", tags = "TestSlnDownloadController")
@RestController
@RequestMapping("/api/v1/testSlnDownload")
public class TestSlnDownloadController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(TestSlnDownloadController.class);

    @ApiOperation(value = "测试1")
    @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "String")
    @RequestMapping(value = "/test1/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> test1(@PathVariable("id") String id,
                                           HttpServletRequest request, HttpServletResponse response) {
        importSlnService.test1(id);

        ApiResult apiResult = new ApiResult();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<ApiResult>(apiResult, HttpStatus.OK);
    }


}
