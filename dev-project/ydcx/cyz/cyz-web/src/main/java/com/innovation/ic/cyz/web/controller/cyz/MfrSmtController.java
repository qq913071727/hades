package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.cyz.base.pojo.global.ApiPageResult;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrSmtDetailPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrSmtListPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.MfrSmtListResultPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.mfrsmt_search_option.MfrSmtOptionLine;
import com.innovation.ic.cyz.base.vo.cyz.MfrSmtListVo;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * SMT厂家API
 */
@Api(value = "SMT厂家API", tags = "MfrSmtController")
@RestController
@RequestMapping("/api/v1/mfrsmt")
public class MfrSmtController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(MfrSmtController.class);

    /**
     * SMT厂家列表
     *
     * @return
     */
    @ApiOperation(value = "SMT厂家列表")
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<ApiPageResult<MfrSmtListPojo>>> list(@RequestBody MfrSmtListVo mfrListVo,
                                                                         HttpServletRequest request, HttpServletResponse response) throws NoSuchFieldException, IllegalAccessException {
        ServiceResult<MfrSmtListResultPojo> mfrsResult = smtService.getList(mfrListVo);

        ApiPageResult<MfrSmtListPojo> apiPageResult = new ApiPageResult<>();
        apiPageResult.setPageNum(mfrListVo.getPage());
        apiPageResult.setPageSize(mfrListVo.getSize());
        apiPageResult.setTotal(mfrsResult.getResult().getTotal());
        apiPageResult.setList(mfrsResult.getResult().getList());
        return new ResponseEntity<>(ApiResult.success(apiPageResult), HttpStatus.OK);
    }

    /**
     * SMT厂家详情
     *
     * @param response
     * @return
     */
    @ApiOperation(value = "SMT厂家详情")
    @ApiImplicitParam(name = "mfrId", value = "MfrId", required = true, dataType = "Long")
    @RequestMapping(value = "/detail/{mfrId}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<MfrSmtDetailPojo>> detail(@PathVariable("mfrId") Long mfrId,
                                                              HttpServletRequest request, HttpServletResponse response) throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException, InstantiationException {
        ApiResult<MfrSmtDetailPojo> result = new ApiResult<>();

        ServiceResult<MfrSmtDetailPojo> serviceResult = smtService.getDetailById(mfrId);
        result.setResult(serviceResult.getResult());
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    /**
     * Smt厂家列表选项
     *
     * @return
     */
    @ApiOperation(value = "SMT厂家列表选项")
    @RequestMapping(value = "/listOptions", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<MfrSmtOptionLine>>> listOptions() {
        ServiceResult<List<MfrSmtOptionLine>> listOptionsResult = smtService.getListOptions();

        ApiResult<List<MfrSmtOptionLine>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(listOptionsResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
