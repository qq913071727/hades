package com.innovation.ic.cyz.web.thread;

import com.innovation.ic.cyz.base.model.cyz.Product;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.service.cyz.ProductService;
import com.innovation.ic.cyz.base.service.pve_standard.ProductsService;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

/**
 * @desc   处理标准库Products表数据线程
 * @author linuo
 * @time   2022年10月31日10:54:34
 */
public class HandleProductsDataThread extends Thread {
    private static final Logger log = LoggerFactory.getLogger(HandleProductsDataThread.class);

    private ProductsService productsService;
    private ProductService productService;
    private long start;
    private long end;

    public HandleProductsDataThread(ProductsService productsService, ProductService productService, long start, long end) {
        this.productsService = productsService;
        this.productService = productService;
        this.start = start;
        this.end = end;
    }

    @SneakyThrows
    @Override
    public void run() {
        log.info("当前线程:[{}],处理数据为第[{}]条到第[{}]条的数据", Thread.currentThread().getName(), start, end);

        // 本次需要处理的数据条数
        long count = end - start;

        // 分几次处理数据
        int handleNo = 5;

        // 每次处理的数据数量
        int handleCount = (int)Math.ceil(count * 1.0 / handleNo);

        long nowStart = start;

        for (int i = 0; i < handleNo; i++) {
            long nowEnd = nowStart + handleCount;
            if(nowEnd > end){
                nowEnd = end;
            }

            // 分页查询Products表中状态为正常的数据
            log.info("分页查询Products表中状态为正常的数据,start:[{}],end:[{}]", nowStart, nowEnd);
            ServiceResult<List<Product>> serviceResult;
            try {
                serviceResult = productsService.getNormalStatusDataByPage(nowStart, nowEnd);
            }catch (Exception e){
                log.error("分页查询Products表中状态为正常的数据出现异常,待30秒后重试,异常原因:[{}],start:[{}],end:[{}]", e.getCause(), nowStart, nowEnd);
                Thread.sleep(30000);
                log.info("重试分页查询Products表中状态为正常的数据,start:[{}],end:[{}]", nowStart, nowEnd);

                try {
                    serviceResult = productsService.getNormalStatusDataByPage(nowStart, nowEnd);
                }catch (Exception e1){
                    log.error("分页查询Products表中状态为正常的数据出现异常,待30秒后重试,异常原因:[{}],start:[{}],end:[{}]", e1.getCause(), nowStart, nowEnd);
                    Thread.sleep(30000);
                    log.info("重试分页查询Products表中状态为正常的数据,start:[{}],end:[{}]", nowStart, nowEnd);
                    serviceResult = productsService.getNormalStatusDataByPage(nowStart, nowEnd);
                }
            }

            if(serviceResult != null && serviceResult.getResult() != null){
                List<Product> list = serviceResult.getResult();
                if(list != null && list.size() > 0){
                    ServiceResult<Boolean> result = productService.batchInsertProductsList(list);
                    if(result.getResult()){
                        log.info("log.info(第[{}]条到第[{}]条的数据已处理完毕", nowStart, nowEnd);
                    }
                }
            }

            Thread.sleep(1000);
            nowStart = nowEnd + 1;
        }
    }
}