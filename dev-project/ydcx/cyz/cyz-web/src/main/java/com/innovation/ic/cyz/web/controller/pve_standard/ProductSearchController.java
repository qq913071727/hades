package com.innovation.ic.cyz.web.controller.pve_standard;

import com.alibaba.fastjson.JSONObject;
import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cyz.base.pojo.constant.Constants;
import com.innovation.ic.cyz.base.pojo.global.ApiPageResult;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;

import com.innovation.ic.cyz.base.pojo.variable.cyz.PsCategoryPropertyPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ps_productunique.PsProdsUniqPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ps_productunique.PsProductQueryByBrandPartRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.ps_property.PsPropertyResultPojo;
import com.innovation.ic.cyz.base.pojo.variable.pve_standard.ps_category.PsCategoryResultPojo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsPartNumbersQueryByContextVo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsProdsUniqVo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_productsunique.PsProductQueryByIdVo;
import com.innovation.ic.cyz.base.vo.pve_standard.ps_property.PsPropertyVo;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Api(value = "器件选型API", tags = "ProductSearchController")
@RestController
@RequestMapping("/api/v1/productSearch")
public class ProductSearchController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(ProductSearchController.class);

    /**
     * test1
     */
    @ApiOperation(value = "test1")
    @RequestMapping(value = "/test1", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> test1() {
        productSearchService.test1();
        ApiResult apiResult = new ApiResult();
        return new ResponseEntity<ApiResult>(apiResult, HttpStatus.OK);
    }

    /**
     * 器件选型分类
     */
    @ApiOperation(value = "器件选型分类")
    @RequestMapping(value = "/categories", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<PsCategoryResultPojo>> categories(HttpServletRequest request, HttpServletResponse response) {
        ServiceResult<PsCategoryResultPojo> categoriesResult = productSearchService.getPsCategories();
        return new ResponseEntity<>(ApiResult.success(categoriesResult.getResult()), HttpStatus.OK);
    }

    /**
     * 器件选型属性
     */
    @ApiOperation(value = "器件选型属性")
    @RequestMapping(value = "/properties", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<PsPropertyResultPojo>> properties(@RequestBody PsPropertyVo psPropertyVo, HttpServletRequest request, HttpServletResponse response) {
//        ServiceResult<PsPropertyResultPojo> propertiesResult = productSearchService.getPsProperties(psPropertyVo);
        //通过id获取器件选型基础数据包含上级数据
        List<String> categoriesList = categorieseCyzService.selectFindId(psPropertyVo.getCategoryId());
        //查询mysql
        ServiceResult<PsPropertyResultPojo> propertiesResult = productService.getPsProperties(categoriesList);
        return new ResponseEntity<>(ApiResult.success(propertiesResult.getResult()), HttpStatus.OK);
    }

    @ApiOperation(value = "器件选型列表")
    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<ApiPageResult<PsProdsUniqPojo>>> getPsProducts(@RequestBody PsProdsUniqVo param) {
        ApiResult<ApiPageResult<PsProdsUniqPojo>> result = new ApiResult<>();
//        ServiceResult<ApiPageResult<PsProdsUniqPojo>> serviceResult = productSearchService.getPsProducts(param);
        //去mysql中拿数据
        ServiceResult<ApiPageResult<PsProdsUniqPojo>> serviceResult = productService.getPsProducts(param);

        result.setResult(serviceResult.getResult());
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "型号模糊查询")
    @ApiImplicitParam(name = "PsPartNumbersQueryByContextVo", value = "型号模糊查询的vo类", required = true, dataType = "PsPartNumbersQueryByContextVo")
    @RequestMapping(value = "/queryPartNumbersByContext", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> queryPartNumbersByContext(@RequestBody PsPartNumbersQueryByContextVo psPartNumbersQueryByContextVo) {
        if (psPartNumbersQueryByContextVo.getPartNumber() == null || !StringUtils.validateParameter(psPartNumbersQueryByContextVo.getPartNumber())) {
            String message = "调用接口[/api/v1/productSearch/queryPartNumbersByContext]时，参数partNumber不能为空";
            log.warn(message);
            ApiResult<ApiResult<List<String>>> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

//        ServiceResult<List<String>> serviceResult = productSearchService.queryPartNumbersByContext(psPartNumbersQueryByContextVo);
        //走mysql数据源
        ServiceResult<List<String>> serviceResult = productUniqueService.queryPartNumbersByContext(psPartNumbersQueryByContextVo);

        ApiResult<List<String>> result = new ApiResult<>();
        result.setResult(serviceResult.getResult());
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "根据id查询器件")
    @ApiImplicitParam(name = "PsProductQueryByIdVo", value = "根据id查询器件的vo类", required = true, dataType = "PsProductQueryByIdVo")
    @RequestMapping(value = "/queryProductsByBrandPartNumber", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> queryProductsByBrandPartNumber(@RequestBody PsProductQueryByIdVo param) {
        if (param.getId() == null || !StringUtils.validateParameter(param.getId())) {
            String message = "调用接口[/api/v1/productSearch/queryByPartNumberContext]时，参数id不能为空";
            log.warn(message);
            ApiResult<ApiResult> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

//        ServiceResult<PsProductQueryByBrandPartRespPojo> serviceResult = productSearchService.queryProductsByBrandPartNumber(param);

        //走mysql
        ServiceResult<PsProductQueryByBrandPartRespPojo> serviceResult = productUniqueService.queryProductsByBrandPartNumber(param);

        ApiResult<PsProductQueryByBrandPartRespPojo> result = new ApiResult<>();
        result.setResult(serviceResult.getResult());
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "根据Ids获取器件选型列表")
    @RequestMapping(value = "/getListByUids", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> getPsProductsByUids(@RequestBody String[] uids) {
        ApiResult<JSONObject> result = new ApiResult<>();
//        ServiceResult<List<PsProdsUniqPojo>> serviceResult = productSearchService.getPsProductsByUids(uids);
        //走mysql
        ServiceResult<List<PsProdsUniqPojo>> serviceResult = productUniqueService.getPsPPProductsByUids(uids);

        JSONObject json = new JSONObject();
        json.put(Constants.COUNT, serviceResult.getResult().size());
        json.put(Constants.DATA, serviceResult.getResult());

        result.setResult(json);
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "根据分类获取分类属性")
    @RequestMapping(value = "/getCategoriesProperty", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<PsCategoryPropertyPojo>>> getPsCategoriesProperty(@RequestParam String categoryId) {
        ApiResult<List<PsCategoryPropertyPojo>> result = new ApiResult<>();
//        ServiceResult<List<PsCategoryPropertyPojo>> serviceResult = productSearchService.getPsCategoriesProperty(categoryId);
        //通过id获取器件选型基础数据包含上级数据
        List<String> categoriesList = categorieseCyzService.selectFindId(categoryId);
        //走mysql
        ServiceResult<List<PsCategoryPropertyPojo>> serviceResult = categoriesPropertyCyzService.getPsCategoriesProperty(categoriesList);

        result.setResult(serviceResult.getResult());
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }
}
