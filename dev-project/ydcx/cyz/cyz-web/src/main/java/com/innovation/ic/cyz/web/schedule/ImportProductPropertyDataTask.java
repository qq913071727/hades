package com.innovation.ic.cyz.web.schedule;

import com.innovation.ic.cyz.base.pojo.enums.RedisKeyPrefixEnum;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.web.thread.HandleProductPropertyDataThread;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @desc   导入标准库商品属性信息表数据定时任务
 * @author linuo
 * @time   2022年11月3日09:45:04
 */
@Component
public class ImportProductPropertyDataTask extends AbstractSchedule {
    private static final Logger log = LoggerFactory.getLogger(ImportProductPropertyDataTask.class);

    /**
     * 获取标准库ProductProperty表数据导入到cyz的数据库中
     */
    @XxlJob("importProductPropertyDataTaskJobHandler")
    private ReturnT<String> importProductPropertyDataTaskJob() {
        log.info("定时任务开启: 先删除本地products_property表的数据,获取标准库ProductProperty表数据导入到cyz的数据库中");

        // 清空products_property表数据
        productPropertyService.truncateProductProperty();

        //清空redis数据
        String key = RedisKeyPrefixEnum.TABLE.getCode() + ":" + RedisKeyPrefixEnum.TABLE_PRODUCT_PROPERTY.getCode();
        super.deleteRedisKey(key);
        // 查询标准库ProductProperty表有效数据数量
        ServiceResult<Long> result = productsPropertyService.getEffectiveDataCount();
        Long count = result.getResult();
        if(count != null && count > 0) {
            log.info("ProductsProperty表共有[{}]条数据需要处理", count);

            // 最大线程数
            int maxThreadCount = 15;

            // 每个线程处理的数据数量
            int threadHandleDataCount = (int) Math.ceil(count * 1.0 / maxThreadCount);

            for (int i = 0; i < maxThreadCount; i++) {
                long start = threadHandleDataCount * i + 1;
                long end = threadHandleDataCount * (i + 1);
                if(end > count){
                    end = count;
                }

                // 创建多线程处理标准库ProductsProperty数据
                HandleProductPropertyDataThread handleProductPropertyDataThread = new HandleProductPropertyDataThread(productsPropertyService, productPropertyService, start, end);
                handleProductPropertyDataThread.setDaemon(Boolean.TRUE);
                threadPoolManager.execute(handleProductPropertyDataThread);
            }
        }

        return ReturnT.SUCCESS;
    }
}