package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.sf.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(value = "工单分类API", tags = "RepairOrderTypeController")
@RestController
@RequestMapping("/api/v1/repairOrderType")
public class RepairOrderTypeController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(RepairOrderTypeController.class);

    /**
     * 查询工单分类信息
     */
    @ApiOperation(value = "查询工单分类信息")
    @RequestMapping(value = "/queryRepairOrderTypeList", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<JSONArray>> queryRepairOrderTypeList(HttpServletRequest request, HttpServletResponse response) {
        ServiceResult<JSONArray> result = repairOrderTypeService.queryRepairOrderTypeList();

        ApiResult<JSONArray> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}