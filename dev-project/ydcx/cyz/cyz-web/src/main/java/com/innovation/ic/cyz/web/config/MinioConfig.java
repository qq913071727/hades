//package com.innovation.ic.cyz.web.config;
//
//import com.innovation.ic.cyz.base.value.config.MinioPropConfig;
//import io.minio.MinioClient;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class MinioConfig {
//    @Autowired
//    private MinioPropConfig minioPropConfig;
//
//    /**
//     * 获取 MinioClient
//     * @return MinioClient
//     */
//    @Bean
//    public MinioClient minioClient() {
//        return MinioClient.builder().endpoint(minioPropConfig.getEndpoint()).
//                credentials(minioPropConfig.getAccessKey(), minioPropConfig.getSecretKey()).build();
//    }
//}