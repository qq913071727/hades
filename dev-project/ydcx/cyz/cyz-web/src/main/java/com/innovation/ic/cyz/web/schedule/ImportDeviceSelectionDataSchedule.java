package com.innovation.ic.cyz.web.schedule;


import com.innovation.ic.cyz.base.model.pve_standard.CategoriesProperty;
import com.innovation.ic.cyz.base.model.pve_standard.Category;
import com.innovation.ic.cyz.base.pojo.enums.RedisKeyPrefixEnum;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;


/**
 * 器件选型相关表同步
 */
@Component
@Slf4j
public class ImportDeviceSelectionDataSchedule extends AbstractSchedule {

    /**
     * device_selection_type,批量存入redis
     */
    @XxlJob("importDeviceSelectionTypeHandler")
    private void importDeviceSelectionTypeSchedule() {
        super.deviceSelectionScheduleService.importDeviceSelectionTypeRedis();
    }


    /**
     * device_selection,批量存入redis
     */
    @XxlJob("importDeviceSelectionHandler")
    private void importDeviceSelectionSchedule() {
        super.deviceSelectionScheduleService.importDeviceSelectionSchedule();
    }


    /**
     * device_selection_parameter,批量存入redis
     */
    @XxlJob("importDeviceSelectionParameterHandler")
    private void importDeviceSelectionParameterSchedule() {
        super.deviceSelectionScheduleService.importDeviceSelectionParameterSchedule();
    }


    /**
     * Categories,批量存入redis并存储mysql
     */
    @XxlJob("importCategoriesHandler")
    private void importCategoriesSchedule() {
        //直接全部删除mysql数据
        super.categorieseCyzService.truncateCategories();
        //删除redis数据
        String key = RedisKeyPrefixEnum.TABLE.getCode() + ":" + RedisKeyPrefixEnum.TABLE_CATEGORIES.getCode();
        super.deleteRedisKey(key);
        //找到需要保存的数据
        int pageNum = -1;
        int pageSize = 1000;
        Integer effectiveData = super.deviceCategoriesService.effectiveData();
        Integer count = 0;
        log.info("importCategoriesHandler 需要导入数据 {} ", effectiveData);
        for (; ; ) {
            pageNum++;
            List<Category> categoryList = super.deviceCategoriesService.findByPage(pageNum, pageSize);
            if (CollectionUtils.isEmpty(categoryList)) {
                break;
            }
            count += categoryList.size();
            super.deviceSelectionScheduleService.importCategoriesSchedule(categoryList);
            this.sleep();
        }
        log.info("importCategoriesHandler 完成数据 {} ", count);
    }


    /**
     * CategoriesProperty,批量存入redis并存储mysql
     */
    @XxlJob("importCategoriesPropertyHandler")
    private void importCategoriesPropertySchedule() {
        //先删除数据
        super.categoriesPropertyCyzService.truncatetCategoriesProperty();
        //删除redis数据
        String key = RedisKeyPrefixEnum.TABLE.getCode() + ":" + RedisKeyPrefixEnum.TABLE_CATEGORIES_PROPERTY.getCode();
        super.deleteRedisKey(key);
        //找到需要保存的数据
        int pageNum = -1;
        int pageSize = 1000;
        Integer effectiveData = super.categoriesPropertyService.effectiveData();
        Integer count = 0;
        log.info("importCategoriesPropertySchedule 需要导入数据 {} ", effectiveData);
        for (; ; ) {
            pageNum++;
            List<CategoriesProperty> categoriesPropertyList = categoriesPropertyService.findByPage(pageNum, pageSize);
            if (CollectionUtils.isEmpty(categoriesPropertyList)) {
                break;
            }
            super.deviceSelectionScheduleService.importCategoriesPropertySchedule(categoriesPropertyList);
            count += categoriesPropertyList.size();
            this.sleep();
        }
        log.info("importCategoriesPropertySchedule 导入成功数据 {} ", count);
    }


    /**
     * 器件选型brand,批量存入redis
     */
    @XxlJob("importBrandHandler")
    private void importBrandSchedule() {
        super.deviceSelectionScheduleService.importBrandSchedule();
    }


    /**
     * 器件选型product,批量存入redis
     */
    @XxlJob("importProductHandler")
    private void importProductSchedule() {
        super.deviceSelectionScheduleService.importProductSchedule();
    }


    /**
     * 器件选型product_property,批量存入redis
     */
    @XxlJob("importProductPropertyHandler")
    private void importProductPropertySchedule() {
        super.deviceSelectionScheduleService.importProductPropertySchedule();
    }


    /**
     * 器件选型product_unique,批量存入redis
     */
    @XxlJob("importProductUniqueHandler")
    private void importProductUniqueSchedule() {
        super.deviceSelectionScheduleService.importProductUniqueSchedule();
    }


    /**
     * 线程处理睡眠
     */
    private void sleep() {
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            log.error(Thread.currentThread().getName() + " :线程睡眠发生异常", e);
        }
    }

}
