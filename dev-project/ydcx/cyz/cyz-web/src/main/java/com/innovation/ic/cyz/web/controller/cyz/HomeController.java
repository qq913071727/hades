package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.carousel.CarouselListPojo;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 首页API(登录、获取当前登录用户信息，在UserController中)
 */
@Api(value = "首页API", tags = "HomeController")
@RestController
@RequestMapping("/api/v1/home")
public class HomeController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(HomeController.class);

    /**
     * 轮播图列表
     */
    @ApiOperation(value = "轮播图列表")
    @RequestMapping(value = "/carousels", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<CarouselListPojo>>> carousels(HttpServletRequest request, HttpServletResponse response) {
        ServiceResult<List<CarouselListPojo>> carouselListResult = carouselService.findCarouselList();
        return new ResponseEntity<ApiResult<List<CarouselListPojo>>>(ApiResult.success(carouselListResult.getResult()), HttpStatus.OK);
    }

}
