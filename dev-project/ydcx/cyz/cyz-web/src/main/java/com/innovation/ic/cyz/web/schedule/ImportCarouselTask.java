package com.innovation.ic.cyz.web.schedule;

import com.innovation.ic.cyz.base.pojo.constant.RedisStorage;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 从首頁轮播图获取数据的定时任务
 */
@Component
public class ImportCarouselTask extends AbstractSchedule {
    private static final Logger log = LoggerFactory.getLogger(ImportCarouselTask.class);

    /**
     * 首页轮播图数据，同步到redis中
     */
    @XxlJob("importCarouselTaskHandler")
    private void importCarousel() {
        try {
            log.info("删除redis中TABLE:{carousel}开头的数据，将carousel表中的数据导入redis");
            // 删除redis中DIALOGUE:开头的数据
            ServiceResult<Boolean> deleteResult = dataService.delRedisDataByKey(RedisStorage.BANNER_CAROUSEL_PREFIX);
            log.info(deleteResult.getMessage());
            // 将carousel表中的数据导入redis
            log.info("将carousel表中的数据导入redis");
            ServiceResult<Boolean> serviceResult = dataService.importCarouselIntoRedis();
            if (serviceResult.getResult()) {
                log.info("将carousel表中的数据导入redis完成");
            }else {
                log.info("将carousel表中的数据导入redis失败");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
