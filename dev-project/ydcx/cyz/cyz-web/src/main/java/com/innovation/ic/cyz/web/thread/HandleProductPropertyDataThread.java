package com.innovation.ic.cyz.web.thread;

import com.innovation.ic.cyz.base.model.cyz.ProductProperty;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.service.cyz.ProductPropertyService;
import com.innovation.ic.cyz.base.service.pve_standard.ProductsPropertyService;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

/**
 * @desc   创建多线程处理标准库ProductsProperty数据
 * @author linuo
 * @time   2022年11月3日10:05:05
 */
public class HandleProductPropertyDataThread extends Thread {
    private static final Logger log = LoggerFactory.getLogger(HandleProductPropertyDataThread.class);

    private ProductsPropertyService productsPropertyService;
    private ProductPropertyService productPropertyService;
    private long start;
    private long end;

    public HandleProductPropertyDataThread(ProductsPropertyService productsPropertyService, ProductPropertyService productPropertyService, long start, long end) {
        this.productsPropertyService = productsPropertyService;
        this.productPropertyService = productPropertyService;
        this.start = start;
        this.end = end;
    }

    @SneakyThrows
    @Override
    public void run() {
        log.info("当前线程:[{}],处理数据为第[{}]条到第[{}]条的数据", Thread.currentThread().getName(), start, end);

        // 本次需要处理的数据条数
        long count = end - start;

        // 分几次处理数据
        int handleNo = 30;

        // 每次处理的数据数量
        int handleCount = (int)Math.ceil(count * 1.0 / handleNo);

        long nowStart = start;

        for (int i = 0; i < handleNo; i++) {
            long nowEnd = nowStart + handleCount;
            if (nowEnd > end) {
                nowEnd = end;
            }

            // 分页查询ProductsProperty表中有效数据
            log.info("分页查询ProductProperty表中状态为正常的数据,start:[{}],end:[{}]", nowStart, nowEnd);

            ServiceResult<List<ProductProperty>> serviceResult;
            try {
                serviceResult = productsPropertyService.getEffectiveDataByPage(nowStart, nowEnd);
            }catch (Exception e){
                log.error("分页查询ProductProperty表中状态为正常的数据出现异常,待30秒后重试,异常原因:[{}],start:[{}],end:[{}]", e.getCause(), nowStart, nowEnd);
                Thread.sleep(30000);
                log.info("重试分页查询ProductProperty表中状态为正常的数据,start:[{}],end:[{}]", nowStart, nowEnd);

                try {
                    serviceResult = productsPropertyService.getEffectiveDataByPage(nowStart, nowEnd);
                }catch (Exception e1){
                    log.error("分页查询ProductProperty表中状态为正常的数据出现异常,待30秒后重试,异常原因:[{}],start:[{}],end:[{}]", e1.getCause(), nowStart, nowEnd);
                    Thread.sleep(30000);
                    log.info("重试分页查询ProductProperty表中状态为正常的数据,start:[{}],end:[{}]", nowStart, nowEnd);
                    serviceResult = productsPropertyService.getEffectiveDataByPage(nowStart, nowEnd);
                }
            }

            List<ProductProperty> list = serviceResult.getResult();
            if(list != null && list.size() > 0){
                ServiceResult<Boolean> result = productPropertyService.batchInsertProductPropertyList(list);
                if(result.getResult()){
                    log.info("log.info(第[{}]条到第[{}]条的数据已处理完毕", nowStart, nowEnd);
                }
            }

            Thread.sleep(1000);
            nowStart = nowEnd + 1;
        }
    }
}