package com.innovation.ic.cyz.web.schedule;

import com.innovation.ic.cyz.base.service.cyz.AvdService;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName ImportAvdTask
 * @Description 将必坑指南数据添加到redis
 * @Date 2022/11/1
 * @Author myq
 */
@Component
public class ImportAvdTask{

    @Autowired
    private AvdService avdService;

    /**
     * @Description: 查看数据并放入redis中
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/117:14
     */
//    @Scheduled(cron = "*/20 * * * * ?")
    @Deprecated
    public void selectAvdAndSetRedisTask(){
        avdService.selectAvdAndSetRedisTask();
    }


    /**
     * @Description: 同步数据到redis中
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/314:53
     */
    @XxlJob("ImportAvdTask_synAvdData2Redis")
    public void synAvdData2Redis(){
        avdService.synAvdData2Redis();
    }




}
