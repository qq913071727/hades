package com.innovation.ic.cyz.web.schedule;

import com.innovation.ic.cyz.base.model.cyz.Brand;
import com.innovation.ic.cyz.base.pojo.enums.RedisKeyPrefixEnum;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * @desc   导入标准库品牌表数据定时任务
 * @author linuo
 * @time   2022年10月28日10:09:16
 */
@Component
public class ImportBrandDataTask extends AbstractSchedule {
    private static final Logger log = LoggerFactory.getLogger(ImportBrandDataTask.class);

    /**
     * 获取标准库Brands表数据导入到cyz的数据库中
     */
    @XxlJob("importBrandDataTaskJobHandler")
    private void importBrandDataTaskJob() {
        log.info("定时任务开启: 先删除本地brand表的数据,获取标准库Brands表数据导入到cyz的数据库中");

        // 清空brand表数据
        brandService.truncateBrand();
        //删除redis数据
        String key = RedisKeyPrefixEnum.TABLE.getCode() + ":" + RedisKeyPrefixEnum.TABLE_BRAND.getCode();
        super.deleteRedisKey(key);
        // 查询标准库Brands状态是正常的数据
        ServiceResult<List<Brand>> serviceResult = brandsService.selectNormalStatusData();
        if(serviceResult.getResult() != null && serviceResult.getResult().size() > 0){
            // 批量插入品牌数据
            ServiceResult<Boolean> result = brandService.batchInsertBrandsList(serviceResult.getResult());
            if(result.getResult()){
                log.info("标准库Brands状态是正常的数据已经导入cyz库中");
            }
        }

        log.info("定时任务执行结束: 获取标准库Brands表数据导入到cyz的数据库中");
    }
}