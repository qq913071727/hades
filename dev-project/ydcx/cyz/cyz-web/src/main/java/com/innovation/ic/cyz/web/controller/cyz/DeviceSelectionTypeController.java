package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.device_selection_type.DeviceSelectionTypeRespPojo;
import com.innovation.ic.cyz.base.service.cyz.DeviceSelectionTypeService;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

@Api(value = "器件选型类型", tags = "DeviceSelectionTypeController")
@RestController
@RequestMapping("/api/v1/deviceSelection")
public class DeviceSelectionTypeController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(DeviceSelectionTypeController.class);

    @Resource
    protected DeviceSelectionTypeService deviceSelectionTypeService;

    /**
     * 根据id查询同级的器件选型类型
     * @return 返回查询结果
     */
    @ApiOperation(value = "根据id查询同级的器件选型类型")
    @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "String")
    @RequestMapping(value = "/findSameLevelTypeListById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<DeviceSelectionTypeRespPojo>>> findSameLevelTypeListById(@PathVariable("id") String id) {
        if (!StringUtils.validateParameter(id)) {
            String message = "调用接口[/api/v1/deviceSelection/findSameLevelTypeListById]时,参数id不能为空";
            log.warn(message);
            ApiResult<List<DeviceSelectionTypeRespPojo>> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<List<DeviceSelectionTypeRespPojo>> deviceSelectionTypeResult = deviceSelectionTypeService.findSameLevelTypeListById(id);
        ApiResult<List<DeviceSelectionTypeRespPojo>> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(deviceSelectionTypeResult.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}