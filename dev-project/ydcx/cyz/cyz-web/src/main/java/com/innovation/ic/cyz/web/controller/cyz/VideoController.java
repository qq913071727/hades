package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.cyz.base.pojo.global.ApiPageResult;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
//import com.innovation.ic.cyz.base.pojo.annotation.JwtToken;
//import com.innovation.ic.cyz.base.pojo.constant.JwtTokenType;
import com.innovation.ic.cyz.base.pojo.variable.cyz.video.*;
import com.innovation.ic.cyz.base.vo.cyz.video.VidApproveVo;
import com.innovation.ic.cyz.base.vo.cyz.video.VidListVo;
import com.innovation.ic.cyz.base.vo.cyz.video.VideoAddVo;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Api(value = "视频API", tags = "VideoController")
@RestController
@RequestMapping("/api/v1/video")
public class VideoController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(VideoController.class);

    @ApiOperation(value = "获取视频类别")
    @RequestMapping(value = "/getCategories", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ApiResult<List<VidCategoryPojo>>> getCategories() {
        ServiceResult<List<VidCategoryPojo>> serviceResult = videoService.getCategories();

        ApiResult<List<VidCategoryPojo>> result = new ApiResult<>();
        result.setResult(serviceResult.getResult());
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "获取视频品牌")
    @RequestMapping(value = "/getBrands", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ApiResult<List<VidBrandPojo>>> getBrands() {
        ServiceResult<List<VidBrandPojo>> serviceResult = videoService.getBrands();

        ApiResult<List<VidBrandPojo>> result = new ApiResult<>();
        result.setResult(serviceResult.getResult());
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "获取视频标签")
    @RequestMapping(value = "/getTags", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ApiResult<List<VidTagPojo>>> getTags() {
        ServiceResult<List<VidTagPojo>> serviceResult = videoService.getTags();

        ApiResult<List<VidTagPojo>> result = new ApiResult<>();
        result.setResult(serviceResult.getResult());
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "获取视频内容类型")
    @RequestMapping(value = "/getTypes", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ApiResult<List<VidTypePojo>>> getTypes() {
        ServiceResult<List<VidTypePojo>> serviceResult = videoService.getTypes();

        ApiResult<List<VidTypePojo>> result = new ApiResult<>();
        result.setResult(serviceResult.getResult());
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

//    @JwtToken(type = JwtTokenType.Required)
    @ApiOperation(value = "视频新增/修改")
    @RequestMapping(value = "/enter", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Long>> enter(@RequestBody VideoAddVo video) {
        ServiceResult<Long> serviceResult = new ServiceResult<>();
        if (video.getId() == null || video.getId() <= 0) {
            serviceResult = videoService.add(video, this.getUserId());
        } else {
            serviceResult = videoService.edit(video, this.getUserId());
        }
        ApiResult<Long> result = new ApiResult<>();
        result.setResult(serviceResult.getResult());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "审批通过")
    @RequestMapping(value = "/approved", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult> approved(@RequestBody VidApproveVo vo) {
        videoService.approved(vo);
        return new ResponseEntity<>(ApiResult.success(), HttpStatus.OK);
    }

    @ApiOperation(value = "获取列表")
    @RequestMapping(value = "/getList", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ApiResult<ApiPageResult<VidListPojo>>> getList(VidListVo param,
                                                                         @RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex,
                                                                         @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize) {

        ApiResult<ApiPageResult<VidListPojo>> result = new ApiResult<>();
        ServiceResult<ApiPageResult<VidListPojo>> serviceResult = videoService.getList(param, pageIndex, pageSize);
        result.setResult(serviceResult.getResult());
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "获取视频详情")
    @RequestMapping(value = "/getDetail/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ApiResult<VidDetailPojo>> getDetail(@PathVariable Long id) {
        ServiceResult<VidDetailPojo> serviceResult = videoService.getDetail(id);
        ApiResult<VidDetailPojo> result = new ApiResult<>();
        result.setResult(serviceResult.getResult());
        result.setCode(serviceResult.getSuccess().equals(Boolean.TRUE) ? HttpStatus.OK.value() : HttpStatus.BAD_REQUEST.value());
        result.setSuccess(serviceResult.getSuccess());
        result.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(result, Boolean.TRUE.equals(serviceResult.getSuccess()) ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }
}
