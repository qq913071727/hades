package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.b1b.framework.util.StringUtils;
import com.innovation.ic.cyz.base.pojo.UserInfo;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order.RepairOrderInfoRespPojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.repair_order.RepairOrderWebListRespPojo;
import com.innovation.ic.cyz.base.vo.cyz.repairOrder.RepairOrderAddVo;
import com.innovation.ic.cyz.base.vo.cyz.repairOrder.RepairOrderReplyVo;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(value = "工单API", tags = "RepairOrderController")
@RestController
@RequestMapping("/api/v1/repairOrder")
public class RepairOrderController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(RepairOrderController.class);

    /**
     * 查询我的工单列表
     */
    @ApiOperation(value = "查询我的工单列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int", required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", required = true, defaultValue = "20")
    })
    @RequestMapping(value = "/queryMyRepairOrder/{pageNo}/{pageSize}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<RepairOrderWebListRespPojo>> queryMyRepairOrder(@PathVariable("pageNo")Integer pageNo, @PathVariable("pageSize")Integer pageSize, HttpServletRequest request, HttpServletResponse response) {
        ServiceResult<RepairOrderWebListRespPojo> result = repairOrderService.queryMyRepairOrder(pageNo, pageSize, this.getUserId());

        ApiResult<RepairOrderWebListRespPojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(result.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(result.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 新增工单
     */
    @ApiOperation(value = "新增工单")
    @ApiImplicitParam(name = "RepairOrderAddVo", value = "工单新增的vo类", required = true, dataType = "RepairOrderAddVo")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> add(@RequestBody RepairOrderAddVo repairOrderAddVo, HttpServletRequest request, HttpServletResponse response) {
        if (!StringUtils.validateParameter(repairOrderAddVo.getTopic()) || repairOrderAddVo.getPriority() == null || repairOrderAddVo.getTypeId() == null) {
            String message = "调用接口[/api/v1/repairOrder/add]时，参数topic、priority和typeId不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> result = repairOrderService.addRepairOrder(repairOrderAddVo, this.getUserId());

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 工单回复
     */
    @ApiOperation(value = "工单回复")
    @ApiImplicitParam(name = "RepairOrderReplyVo", value = "工单回复的vo类", required = true, dataType = "RepairOrderReplyVo")
    @RequestMapping(value = "/reply", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> reply(@RequestBody RepairOrderReplyVo repairOrderReplyVo, HttpServletRequest request, HttpServletResponse response) {
        if (repairOrderReplyVo.getId() == null) {
            String message = "调用接口[/api/v1/repairOrder/reply]时，参数id不能为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        if(!StringUtils.validateParameter(repairOrderReplyVo.getDescribe()) && (repairOrderReplyVo.getFileUrls() == null || repairOrderReplyVo.getFileUrls().size() == 0)){
            String message = "调用接口[/api/v1/repairOrder/reply]时，参数describe、fileUrls不能同时为空";
            log.warn(message);
            ApiResult<Boolean> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> result = repairOrderService.replyRepairOrder(repairOrderReplyVo, null, this.getUserId());

        ApiResult<Boolean> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(ServiceResult.INSERT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据工单id获取工单内容
     */
    @ApiOperation(value = "根据工单id获取工单内容")
    @ApiImplicitParam(name = "id", value = "工单id", dataType = "Long", required = true)
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<RepairOrderInfoRespPojo>> info(@PathVariable("id")Long id, HttpServletRequest request, HttpServletResponse response) {
        if (id == null) {
            String message = "调用接口[/api/v1/repairOrder/info]时，参数id不能为空";
            log.warn(message);
            ApiResult<RepairOrderInfoRespPojo> apiResult = new ApiResult<>();
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<RepairOrderInfoRespPojo> result = repairOrderService.getRepairOrderInfoById(id);

        ApiResult<RepairOrderInfoRespPojo> apiResult = new ApiResult<>();
        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setResult(result.getResult());
        apiResult.setMessage(ServiceResult.SELECT_SUCCESS);
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}