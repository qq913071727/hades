package com.innovation.ic.cyz.web.schedule;

import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.pojo.variable.cyz.sln_import.ImportSlnOnePagePojo;
import com.innovation.ic.cyz.base.pojo.variable.cyz.sln_import.ImportSlnUrlResultPojo;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 从方案网获取数据的定时任务
 */
@Component
public class ImportSlnTask extends AbstractSchedule {
    private static final Logger log = LoggerFactory.getLogger(ImportSlnTask.class);

    /**
     * 调用方案网接口，获取方案数据并保存
     * 注意：现在是每5分钟调用一次接口，每次只获取少量数据，这样对对方的服务器不会造成负载。但是，如果在一天之内无法
     * 将所有的数据都获取到的话，只能将5分钟修改得更短。或者采取别的方案。
     */
    //@Scheduled(cron = "* 0/5 9-23 * * ?")
//    @Scheduled(fixedRate = 10000)
//    @XxlJob("importSlnTaskHandler")
    @Deprecated
    private void importSln() {
        log.info("定时任务开启：调用方案网接口，获取方案数据并保存");

        // 从表sln_sync_log表中获取接口的url。如果今日的调用已经完成了，则结束定时任务
        ServiceResult<ImportSlnUrlResultPojo> urlResult = importSlnService.getApiUrl();
        if (urlResult.getResult().getIsTodayFinish()) {
            log.info("定时任务：调用方案网接口，今日已调取结束");
            return;
        }

        // 调用接口，从方案网获取数据
        String url = urlResult.getResult().getNextUrl();
        ServiceResult<ImportSlnOnePagePojo> slnImportResult = importSlnService.getOnePageSlnsFromApi(url);
        if (!slnImportResult.getSuccess()) {
            log.error("定时任务中报错：调用方案网接口报错:" + slnImportResult.getMessage());
            return;
        }

        importSlnService.saveOnePage(slnImportResult.getResult());

        // 下载图片
        log.info("定时任务：下载一页的方案数据的图片-开始");
        List<Long> slnIds = slnImportResult.getResult().getResults()
                .stream().map(item -> item.getSln().getSlnId())
                .collect(Collectors.toList());
        for (Long slnId : slnIds) {
            importSlnService.downloadImageAll(slnId);
        }
        log.info("定时任务：下载一页的方案数据的图片-结束");

    }


    /**
     * @Description: 定时删除无效的数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/2010:42
     */
//    @Scheduled(cron = "0/5 * * * * ?")
    @Deprecated
    public void deleteInvalidData(){
        log.info("*******************"+ new Date() + "启动删除无效的数据任务 ********************");
        importSlnService.deleteInvalidData();
        log.info("*******************"+ new Date() + "完成删除无效的数据任务 ********************");
    }


    /**
     * @Description: 第一次全量拉取数据，之后增量拉取数据
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/10/2816:54
     */
    @XxlJob("importSlnTaskHandler")
    public void pullSlnInfo(){
        log.info("******************* "+ new Date() + "开始增量拉取数据 ********************");
        importSlnService.pullSlnInfo();
        log.info("******************* "+ new Date() + "结束拉取数据结束 ********************");
    }


    /**
     * @Description: 同步方案数据到redis中
     * @Params:
     * @Return:
     * @Author: Mr.myq
     * @Date: 2022/11/214:51
     */
//    @Scheduled(cron = "0/15 * * * * ?")
    @XxlJob("ImportSlnTask_synSlnData2Redis")
    public void synSlnData2Redis(){
        log.info("******************* "+ new Date() + "开始增量拉取数据 ********************");
        importSlnService.synSlnData2Redis();
        log.info("******************* "+ new Date() + "结束拉取数据结束 ********************");
    }




}