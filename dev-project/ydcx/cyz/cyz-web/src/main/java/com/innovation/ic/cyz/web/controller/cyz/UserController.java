package com.innovation.ic.cyz.web.controller.cyz;

import com.innovation.ic.b1b.framework.util.IpAdrressUtil;
import com.innovation.ic.b1b.framework.util.JwtUtils;
import com.innovation.ic.cyz.base.pojo.UserInfo;
//import com.innovation.ic.cyz.base.pojo.annotation.JwtToken;
//import com.innovation.ic.cyz.base.pojo.constant.JwtTokenType;
import com.innovation.ic.cyz.base.model.cyz.User;
import com.innovation.ic.cyz.base.pojo.global.ApiResult;
import com.innovation.ic.cyz.base.pojo.global.ServiceResult;
import com.innovation.ic.cyz.base.vo.cyz.UserLoginVo;
import com.innovation.ic.cyz.web.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Api(value = "用户API", tags = "UserController")
@RestController
@RequestMapping("/api/v1/user")
public class UserController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    /**
     * 用户登录
     *
     * @return
     */
    @ApiOperation(value = "用户登录")
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Map<String, String>>> login(@RequestBody UserLoginVo userLoginVo,
                                                                HttpServletRequest request, HttpServletResponse response) {
        ServiceResult<User> userResult = userService.findUserByUsernameAndPassword(userLoginVo.getUsername(), userLoginVo.getPassword());
        User user = userResult.getResult();
        Map<String, String> map = new HashMap<>();
        if (user == null) {
            map.put("status", "failed");
            return new ResponseEntity<>(ApiResult.failed(map, "用户名或密码错误"), HttpStatus.OK);
        }
        //获取当前请求ip
        String ipAdrress = IpAdrressUtil.getIpAdrress(request);
        //添加日志数据
        userService.addUserLoginLog(ipAdrress,user);
        Map<String, String> payload = new HashMap<>();
        payload.put("userId", user.getId() + "");
        payload.put("username", user.getUsername());
        String token = JwtUtils.generateToken(payload);
        map.put("status", "OK");
        map.put("token", token);
        return new ResponseEntity<>(ApiResult.success(map), HttpStatus.OK);
    }

    /**
     * 获取当前登录用户信息
     */
//    @JwtToken(type = JwtTokenType.Required)
    @ApiOperation(value = "获取当前登录用户信息【需要验证Token】")
    @RequestMapping(value = "/info", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<UserInfo>> info(HttpServletRequest request, HttpServletResponse response) {
        ServiceResult<UserInfo> userInfoResult = userService.getCurrentUserInfo(this.getUserId());
        UserInfo userInfo = userInfoResult.getResult();
        return new ResponseEntity<>(ApiResult.success(userInfo), HttpStatus.OK);
    }
}