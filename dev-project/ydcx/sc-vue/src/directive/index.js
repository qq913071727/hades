import hasRole from './permission/hasRole'
import hasPermi from './permission/hasPermi'
import dialogDrag from './dialog/drag'
import adaptive from './adaptive/adaptive'

const install = function(Vue) {
    Vue.directive('hasRole', hasRole)
    Vue.directive('hasPermi', hasPermi)
    Vue.directive('dialogDrag', dialogDrag)
    Vue.directive('adaptive', adaptive)
}

if (window.Vue) {
    window['hasRole'] = hasRole
    window['hasPermi'] = hasPermi
    window['dialogDrag'] = dialogDrag
    window['adaptive'] = adaptive
    Vue.use(install) // eslint-disable-line
}

export default install