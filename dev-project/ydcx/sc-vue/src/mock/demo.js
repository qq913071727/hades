import { mock } from "mockjs";
import menu from '../api/menu.json'

// const projectList = mock({
// 	"list|20": [{
// 		'name': '@cname', // 中文名
// 		'account': `@word`, // 英文单词
// 		'phone': /1[3-9][0-9]{9}/, // 正则模式
// 		'deptName': mock('@cword(2,4)'), // 随机2-4字中文单词
// 		'id': '@guid', // guid
// 	}]
// })

export default [
	// {
	// 	url: '/api/login',
	// 	type: 'post',
	// 	response: (res) => {
	// 		let _fileter_list = [];
	// 		if (res.body.key) {
	// 			let _fileter_list = projectList.fileter(i => i.name == res.body.key)
	// 		}

	// 		// 没错，你应该已经猜到了，res.body就是我们传入到接口的数据，我们可以在这里做些逻辑操作
	// 		// res包含完整的请求头信息
	// 		return {
	// 			code: 200,
	// 			message: "操作成功",
	// 			data: _fileter_list
	// 		}
	// 		// 使用return返回前端需要的数据
	// 	}
	// },
	{
		url: '/demo/login',
		type: 'post',
		response: (res) => {
			return {
				code: 200,
				message: "操作成功",
				result:{
					token:'SHHHHHHHHHH'
				}
			}
		}
	},
	{
		url: '/demo/getInfo',
		type: 'get',
		response: (res) => {
			// return { "msg": "操作成功", "code": 200, "permissions": ["*:*:*"], "roles": ["admin"], "user": { "searchValue": null, "createBy": "admin", "createTime": "2021-09-09 17:25:28", "updateBy": null, "updateTime": null, "remark": "管理员", "params": {}, "userId": 1, "deptId": 103, "userName": "admin", "nickName": "admin", "avatar": "", "status": "0", "admin": true } }
			return {
				"msg": "操作成功",
				"code": 200,
				// "permissions": [
				// 	"*:*:*"
				// ],
				"data": {
					"userId": 1,
					"userName": "admin",
					"nickName": "admin",
					"avatar": "",
					"status": "0",
					"admin": true
				}
			}
		}
	},
	{
		url: '/demo/getRouters',
		type: 'get',
		response: (res) => {
			return menu
		}
	},
	{
		url: '/demo/logout',
		type: 'post',
		response: (res) => {
			return {
				code: 200,
				message: "操作成功",
			}
		}
	},
]