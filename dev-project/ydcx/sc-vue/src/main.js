import Vue from 'vue'
import App from './App.vue'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './routers'
import { resetForm } from '@/utils/index'
// import { mockXHR } from './mock/index'
import Cookies from 'js-cookie'
import '@/assets/styles/index.scss' // global css
import '@/assets/styles/ruoyi.scss' // ruoyi css
import store from './store'
import directive from './directive' //directive
import './assets/icons' // icon
import './permission' // permission control
import './utils/element' //包含自定义全局组件   用到的控件  自定义指令
import Pagination from "@/components/Pagination"
// 自定义表格工具组件
import RightToolbar from "@/components/RightToolbar"
// 富文本组件
import Editor from "@/components/Editor"
// 文件上传组件
import FileUpload from "@/components/FileUpload"
// 图片上传组件
import ImageUpload from "@/components/ImageUpload"
// 字典标签组件
import DictTag from '@/components/DictTag'
// 头部标签组件
import VueMeta from 'vue-meta'

//图片预览
import 'viewerjs/dist/viewer.css'
import VueViewer from 'v-viewer'
//全局方法挂载
Vue.prototype.$message = function (msg) {
  return Element.Message({
    message: msg.message,
    type: msg.type,
    duration: 2000
  })
};
//分别对success、warning和error等样式进行设置
Vue.prototype.$message.success = function (msg) {
  return Element.Message.success({
    message: msg,
    duration: 2000
  })
};
Vue.prototype.$message.warning = function (msg) {
  return Element.Message.warning({
    message: msg,
    duration: 2000
  })
};
Vue.prototype.$message.error = function (msg) {
  return Element.Message.error({
    message: msg,
    duration: 2000
  })
}
Vue.prototype.resetForm = resetForm
Vue.prototype.msgSuccess = function (msg) {
  this.$message({ showClose: true, message: msg, type: "success" })
}

Vue.prototype.msgError = function (msg) {
  this.$message({ showClose: true, message: msg, type: "error" })
}

Vue.prototype.msgInfo = function (msg) {
  this.$message.info(msg)
}

// 全局组件挂载
Vue.component('Pagination', Pagination)
Vue.component('DictTag', DictTag)
Vue.component('RightToolbar', RightToolbar)
Vue.component('Editor', Editor)
Vue.component('FileUpload', FileUpload)
Vue.component('ImageUpload', ImageUpload)

Vue.use(directive)
Vue.use(VueMeta)
Vue.use(VueViewer)
Vue.use(Element, {
  size: Cookies.get('size') || 'small' // set element-ui default size
})

// if (process.env.NODE_ENV == 'development') {
//   mockXHR();
// }

Vue.config.productionTip = false
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})