import request from '@/utils/request';
import request_erp from "@/utils/request_erp";

// 未读消息总数-消息提醒
export function getUnReadTotal() {
    return request({
        url: '/api/v1/actionMessage/unReadTotal',
        method: 'get',
    })
}

// 获取菜单标注数量-报价管理
export function getInquiry(data) {
    return request_erp({
        url: '/scsapi/Inquiry/GetMenuMarkCount?userId=' + data.userId,
        method: 'POST',
        data: {}
    })
}