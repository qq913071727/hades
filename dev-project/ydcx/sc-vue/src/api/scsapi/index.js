import request from "../../utils/request_erp"
import store from '@/store/index'

// 报价公司
export function getSuppliers() {
    return request({
        url: '/scsapi/Inquiry/GetSuppliers',
        method: 'get',
        params: {
            userid: store.getters.id,
        }
    })
}

//实单报价列表
export function getRealList(params = {}) {
    return request({
        url: '/scsapi/Inquiry/RealList',
        method: 'get',
        params: {
            userid: store.getters.id,
            partnumber: params.partnumber,
            brand: params.brand,
            status: params.status,
            quotestatus: params.quotestatus,
            startdate: (params.date && params.date.length) > 1 ? params.date[0] : '',
            enddate: (params.date && params.date.length) > 1 ? params.date[1] : '',
            page: params.page,
            pageSize: params.pageSize,

        }
    })
}


// 报价任务列表
export function getList(params = {}){
    return request({
        url:"/scsapi/Inquiry/List",
        method:"get",
        params:{
            userid: store.getters.id,
            partnumber: params.partnumber,
            brand: params.brand,
            status: params.status,
            startdate: (params.date && params.date.length) > 1 ? params.date[0] : '',
            enddate: (params.date && params.date.length) > 1 ? params.date[1] : '',
            page: params.page,
            pageSize: params.pageSize,
        }
    })
}
// 普通报价列表
export function getNormalList(params = {}) {
    return request({
        url: '/scsapi/Inquiry/NormalList',
        method: 'get',
        params: {
            userid: store.getters.id,
            partnumber: params.partnumber,
            brand: params.brand,
            status: params.status,
            quotestatus: params.quotestatus,
            startdate: (params.date && params.date.length) > 1 ? params.date[0] : '',
            enddate: (params.date && params.date.length) > 1 ? params.date[1] : '',
            page: params.page,
            pageSize: params.pageSize,

        }
    })
}
// 报价
export function TaskQuote(data = {}) {

    return request({
        url: '/scsapi/Inquiry/TaskQuote',
        method: 'post',
        data: {
            userid: store.getters.id,
            "inquiryID": data.inquiryID,
            "inquiryItemID": data.inquiryItemID,
            "unitPrice": data.unitPrice,
            "currency": data.currency,
            "quantity": data.quantity,
            "deliveryDate": data.deliveryDate,
            "dateCode": data.dateCode,
            "deliveryPlace": data.deliveryPlace,
            "supplierID": data.supplierID,
            "supplierName": data.supplierName,
            "summary": data.summary,
            termDate: data.termDate,
        }


    })
}
// 无法报价
export function UnableQuote(data = {}) {
    return request({
        url: '/scsapi/Inquiry/UnableQuote',
        method: 'post',
        data: {
            "ids": data.ids,
            "reason": data.reason,

        }
    })
}

// 搜商机查询
export function getSearchList(data = {}) {
    return request({
        url: '/scsapi/Inquiry/SearchList',
        method: 'get',
        params: {
            userid: store.getters.id,
            partnumber: data.partnumber,
            page: data.page,
            pageSize: data.pageSize,

        }
    })
}



// 搜商机报价
export function SearchQuote(data = {}) {
    return request({
        url: '/scsapi/Inquiry/SearchQuote',
        method: 'post',
        data: {
            userid: store.getters.id,
            "inquiryID": data.inquiryID,
            "inquiryItemID": data.inquiryItemID,
            "unitPrice": data.unitPrice,
            "currency": data.currency,
            "quantity": data.quantity,
            "deliveryDate": data.deliveryDate,
            "termDate":data.deliveryDate,
            "dateCode": data.dateCode,
            "deliveryPlace": data.deliveryPlace,
            "supplierID": data.supplierID,
            "supplierName": data.supplierName,
            "summary": data.summary,
        }
    })
}

// 搜商机申请报价
export function SearchClaim(data = {}) {
    return request({
        url: '/scsapi/Inquiry/SearchClaim',
        method: 'post',
        data: {
            userid: store.getters.id,
            "inquiryID": data.inquiryID,
            "inquiryItemID": data.inquiryItemID,
            "unitPrice": data.unitPrice,
            "currency": data.currency,
            "quantity": data.quantity,
            "deliveryDate": data.deliveryDate,
            "dateCode": data.dateCode,
            "deliveryPlace": data.deliveryPlace,
            "supplierID": data.supplierID,
            "supplierName": data.supplierName,
            "summary": data.summary,
        }
    })
}








// 议价列表
export function getQuoteList(params = {}) {
    return request({
        url: '/scsapi/Quote/List',
        method: 'get',
        params: {
            userid: store.getters.id,
            id: params.id,
            partnumber: params.partnumber,
            brand: params.brand,
            status: params.status,
            startdate: (params.date && params.date.length) > 1 ? params.date[0] : '',
            enddate: (params.date && params.date.length) > 1 ? params.date[1] : '',
            page: params.page,
            pageSize: params.pageSize,
        }
    })
}

// 议价
export function Bargain(data = {}) {
    return request({
        url: '/scsapi/Quote/Bargain',
        method: 'post',
        data: {
            id: data.id,
            unitPrice: data.unitPrice,
            quantity: data.quantity,
            deliveryDate: data.deliveryDate,
            deliveryPlace: data.deliveryPlace,
            termDate: data.termDate,
        }
    })
}
// 不支持议价
export function UnableBargain(data = {}) {
    return request({
        url: '/scsapi/Quote/UnableBargain',
        method: 'post',
        data: {
            ids: data.ids,
            reason: data.reason,

        }
    })
}




// 首页热门询价
export function popularInquiry(){
    return request({
        url:"/scsapi/Report/GetPopularInquiryProducts",
        method:"get"
    })
}

// 首页实弹求购
export function realInquiry(){
    return request({
        url:"/scsapi/Report/GetRealInquiryOrders",
        method:"get",
        params:{
            userId: store.getters.id,
        }
    })
}