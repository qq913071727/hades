import request from '@/utils/request'

// 模糊查询标准库型号
export function queryBzkPartNumber(data) {
    return request({
        url:'/api/v1/inventory/blurQueryBzkPartNumberList',
        method: 'post',
        requestBaseUrl:'outside',
        data
    })
}
//查询列表带分页
export function brandList(params) {
    return request({
        url:'/api/v1/advantageBrand/findByPage',
        method: 'get',
        params
    })
}
//新增优势品牌
export function addAdvantageBrand(data) {
    return request({
        url:'/api/v1/advantageBrand/addAdvantageBrand',
        method: 'post',
        data
    })
}
// 取消优势品牌
export function cancelAdvantageBrand(data) {
    return request({
        url:'/api/v1/advantageBrand/cancelAdvantageBrand',
        method: 'post',
        data
        
    })
}
// 再次发起
export function reApply(data) {
    return request({
        url:'/api/v1/advantageBrand/reApply',
        method: 'post',
        data
    })
}
// 删除优势品牌
export function deleteAdvantageBrand(data) {
    return request({
        url:'/api/v1/advantageBrand/deleteAdvantageBrand',
        method: 'post',
        data
    })
}
// 品牌
export function brandByPage(data) {
    return request({
        url:'/api/v1/advantageBrand/findBrand',
        method: 'post',
        data,
    })
}
// 校验优势品牌是否存在
export function checkAdvantageBrand(params) {
    return request({
        url:'/api/v1/advantageBrand/checkAdvantageBrand',
        method: 'get',
        params
    })
}
// 确认提交图片
export function confirmPicture(id,data) {
    return request({
        url:'/api/v1/advantageBrand/confirmPicture?id=' + id + '&pctureIds=' + data,
        method: 'post',
    })
}
// 申请延期
export function application(data) {
    return request({
        url:'/api/v1/advantageBrand/applicationForExtension',
        method: 'post',
        data
    })
}

// 优势品牌
export function modelList(data) {
    return request({
        url:'/api/v1/advantageModel/query',
        method: 'post',
        data
    })
}
// 单条新增
export function addModel(data) {
    return request({
        url:'/api/v1/advantageModel/add',
        method: 'post',
        data
    })
}
// 根据型号查询所属品牌

export function queryBrandsByPartNumber(data) {
    return request({
        url:'/api/v1/advantageModel/queryBrandsByPartNumber',
        method: 'post',
        data
    })
}
// 校验当前型号是否已添加(true已添加、false未添加)
export function checkAdvantageModel(data) {
    return request({
        url:'/api/v1/advantageModel/checkAdvantageModelIfAdd',
        method: 'post',
        data
    })
}
// 取消优势型号
export function cancleAdvantageModel(data) {
    return request({
        url:'/api/v1/advantageModel/cancleAdvantageModel',
        method: 'post',
        data
        
    })
}
// 优势型号再次发起
export function againInitiate(data) {
    return request({
        url:'/api/v1/advantageModel/againInitiate',
        method: 'post',
        data
    })
}
// 设置优势型号
export function updateAdvantageStatus(data) {
    return request({
        url:'/api/v1/inventory/updateAdvantageStatus',
        method: 'post',
        data
    })
}
// 删除优势型号
export function deleteModel(data) {
    return request({
        url:'/api/v1/advantageModel/delete',
        method: 'post',
        data
    })
}
// 批量新增
export function batchAdd(data) {
    return request({
        url:'/api/v1/advantageModel/batchAdd',
        method: 'post',
        data
    })
}
// 价格阶梯列表
export function priceList(params) {
    return request({
        url:'/api/v1/ladderPrice/queryLadderPriceConfigListByMoq',
        method: 'get',
        params
    })
}
// 查询全部阶梯价格配置信息
export function priceStepList(params) {
    return request({
        url:'/api/v1/ladderPrice/queryLadderPriceConfigListByAmount/' + params,
        method: 'get',
        
    })
}
// 按单价配置的列表
export function allLadderList(params) {
    return request({
        url:'/api/v1/ladderPrice/queryAllLadderPriceInfo',
        method: 'get',
        params
    })
}
// 按起订量批量配置库存时的阶梯价格信息查询
export function LadderPriceByMoq(data) {
    return request({
        url:'/api/v1/ladderPrice/queryBatchSetLadderPriceConfigListByMoq' ,
        method: 'post',
        data,
    })
}
// 按单价批量配置库存时的阶梯价格信息查询
export function LadderPriceByAmount(data) {
    return request({
        url:'/api/v1/ladderPrice/queryBatchSetLadderPriceConfigListByAmount',
        method: 'post',
        data
    })
}
// 新增阶梯
export function addPrice(data) {
    return request({
        url:'/api/v1/ladderPrice/add',
        method: 'post',
        data
    })
}
// 阶梯价格信息删除
export function deletePrice(params) {
    return request({
        url:'/api/v1/ladderPrice/delete/' +params ,
        method: 'get',
       
    })
}
// 阶梯价格信息查询
export function priceInfo(params) {
    return request({
        url:'/api/v1/ladderPrice/info/' +params ,
        method: 'get',
       
    })
}
// 阶梯价格修改
export function editPrice(data) {
    return request({
        url:'/api/v1/ladderPrice/edit',
        method: 'post',
        data
    })
}
// 库存信息列表查询
export function queryInventory(data) {
    return request({
        url:'/api/v1/inventory/query',
        method: 'post',
        data
    })
}
// 库存信息删除
export function deleteInventory(data) {
    return request({
        url:'/api/v1/inventory/delete',
        method: 'post',
        data
    })
}
// 库存信息编辑
export function getInventoryList(data) {
    return request({
        url:'/api/v1/inventory/getInventoryList',
        method: 'post',
        data
    })
}
// 库存信息编辑提交
export function updateInventoryList(data) {
    return request({
        url:'/api/v1/inventory/update',
        method: 'post',
        data
    })
}
// 下、上架
export function updateStatusList(data) {
    return request({
        url:'/api/v1/inventory/updateStatus',
        method: 'post',
        data
    })
}
// 更新时间
export function updateInventoryDate(data) {
    return request({
        url:'/api/v1/inventory/updateInventoryDate',
        method: 'post',
        data
    })
}
// 阶梯价格配置
export function setLadderPrice(data) {
    return request({
        url:'/api/v1/inventory/setLadderPrice',
        method: 'post',
        data
    })
}
// 库存新增
export function addInventory(data) {
    return request({
        url:'/api/v1/inventory/add',
        method: 'post',
        data
    })
}
// 批量导入
export function batchAddInventory(data) {
    return request({
        url:'/api/v1/inventory/batchAdd',
        method: 'post',
        data
    })
}
// 根据单价、阶梯价格配置id获取阶梯价格数据
export function getLadderPriceData(data) {
    return request({
        url:'/api/v1/ladderPrice/getLadderPriceData',
        method: 'post',
        data
    })
}
// 判断阶梯价格是否被库存使用
export function judgeIfUsedByInventory(data) {
    return request({
        url:'/api/v1/ladderPrice/judgeIfUsedByInventory/' + data,
        method: 'get',
       
    })
}
// 未匹配成功列表
export function queryUnMatchData(data) {
    return request({
        url:'/api/v1/inventory/queryUnMatchData',
        method: 'post',
        data
    })
}
// 未匹配成功确认
export function confirmStandard(data) {
    return request({
        url:'/api/v1/inventory/confirmStandardMatchResult',
        method: 'post',
        data
    })
}
// 未匹配成功删除
export function deleteStandard(data) {
    return request({
        url:'/api/v1/inventory/delete',
        method: 'post',
        data
    })
}
// 批量导出
export function batchExport(data) {
    return request({
        url:'/api/v1/inventory/batchExport',
        method: 'post',
        data
    })
}

// 与大赢家传参的接口
export function findByEpId(data) {
    return request({
        url:'/api/v1/myCompany/findByEpId',
        method: 'get',
        data,
       
    })
}






