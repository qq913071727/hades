import request from "../utils/request";

//获取唯一标识数据
export function getMedicalSharing (params) {
	return request({
		url: '/api/medical/sharing',
		method: 'get',
		params: params
	})
}

//获取产品标识详情数据
export function getMedicalSharingDetail (params) {
	return request({
		url: '/api/medical/sharing/detail',
		method: 'get',
		params: params
	})
}

//获取唯一标识历史列表
export function getMedicalSharingHistory (params) {
	return request({
		url: '/api/medical/sharing/history',
		method: 'get',
		params: params
	})
}

//获取产品标识数据
export function getMedicalIdentifier (params) {
	return request({
		url: '/api/medical/identifier',
		method: 'get',
		params: params
	})
}

//获取历史版本数据
export function getMedicalIdentifierHistory (params) {
	return request({
		url: '/api/medical/identifier/history',
		method: 'get',
		params: params
	})
}

//产品标识数据填报
export function saveIdentifier (data) {
	return request({
		url: '/api/medical/identifier/save',
		method: 'POST',
		data: data
	})
}

//产品标识数据提交
export function submitIdentifier (data) {
	return request({
		url: '/api/medical/identifier/submit',
		method: 'POST',
		data: data
	})
}