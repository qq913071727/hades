import request from '@/utils/request'

// 用户登录日志
export function userLoginLog(params) {
    return request({
        url: '/api/v1/userLoginLog/save',
        method: 'post',
        requestBaseUrl: 'log',
        data: params
    })
}

// 页面访问日志
export function pageVisitLog(params) {
    return request({
        url: '/api/v1/pageVisitLog/save',
        method: 'post',
        requestBaseUrl: 'log',
        data: params
    })
}

// 接口调用日志
// export function interfaceCall(params) {
//     return request({
//         url: '/api/v1/interfaceCallLog/save',
//         method: 'post',
//         requestBaseUrl: 'log',
//         data: params
//     })
// }