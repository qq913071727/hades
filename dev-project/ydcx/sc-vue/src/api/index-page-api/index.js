import request from "../../utils/request_erp"
import store from '@/store/index'

//首页
export function GetHomePageData() {
    return request({
        url: '/scsapi/Report/GetHomePageData',
        method: 'get',
        params: {
            userid: store.getters.id,
        }
    })
}

// 代开发票数量 
// 大赢家api  http://cw.51db.com:9098/services/core.ashx
export function GetHomePageData1() {
    return request({
        fullUrl: true,
        url: process.env.VUE_APP_BASE_URL_WINNER + '/services/core.ashx',
        method: 'post',
        data: {
            "request_service": "B1B供应商协同开票处理",
            "request_item": "更新已开发票B1B状态",
            "data": {
                "ID": 80890 /*主表ID*/
            },
            "token": "731f9608-1653-4f31-9de7-002be83683aa"
        }
    })
}
// 待交货,待付款
// 大赢家api /api/login/JSRInfoByIDStype?id=&stype=
export function getDYJtoken() {
    return request({
        fullUrl: true,
        url: process.env.VUE_APP_BASE_URL_WINNER + '/810_api/login/JSRInfoByIDStype',
        method: 'get',
        params: {
            id: store.getters.id,
            stype: "WaitInfo"
        }
    })
}
// 待交货,待付款
// 大赢家api /api/DJProcess/WaitInfo?key=&gys=
export function getWaitInfo(params) {
    return request({
        fullUrl: true,
        url:  process.env.VUE_APP_BASE_URL_WINNER + '/810_api/DJProcess/WaitInfo',
        method: 'get',
        params: {
            key: params.key,
            gys: params.gys
        }
    })
}

//待付款 WaitInfoPay
export function getWaitInfoPay(params) {
    return request({
        fullUrl: true,
        url:  process.env.VUE_APP_BASE_URL_WINNER + '/810_api/DJProcess/WaitInfoPay',
        method: 'get',
        params: {
            key: params.key,
            gys: params.gys
        }
    })
}

// 报价采纳率
export function GetHomePageSupplierQuoteStatistics(params) {
    return request({
        url: '/scsapi/Report/GetSupplierQuoteStatistics',
        method: 'get',
        params: {
            userid: store.getters.id,
            year: params.year
        }
    })
}


// 订单统计
export function GetOrderNumberStatistics() {
    return request({
        url: '/smsapi/smsapi/Purchase/GetOrderNumberStatistics',
        method: 'get',
        params: {
            userId: store.getters.id,
        }
    })
}


// 成交金额统计

export function GetOrderAmountStatistics() {
    return request({

        url: '/smsapi/smsapi/Purchase/GetOrderAmountStatistics',
        method: 'get',
        params: {
            userId: store.getters.id,

        }
    })
}



export function addTest(a) {
    return request({
        url: '/scsapi/Inquiry/Add',
        method: 'post',
        data: {
            "inquiryid": a,
            "useids": [
                store.getters.id
            ]

        }
    })
}