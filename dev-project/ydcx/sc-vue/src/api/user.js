import request from '@/utils/request'

//新增账号
export function addAccount(data) {
	return request({
		url: '/api/v1/user/save',
		method: 'post',
		data
	})
}

//修改
export function updateAccount(data) {
	return request({
		url: '/api/v1/user/updateUser',
		method: 'post',
		data
	})
}
//列表
export function accountList(data) {
	return request({
		url: '/api/v1/user/pageUser',
		method: 'post',
		data
	})
}

//权限管理下拉列表
export function getAllroles() {
	return request({
		url: '/api/v1/role/findAll',
		method: 'get'
	})
}

//子账号角色列表
export function childRoleList(userId){
	return request({
		url:`/api/v1/mapUserRole/findByUserId/${userId}`,
		method: 'get'
	})
}
//权限设置菜单列表
export function getMenu(userId) {
	return request({
		url: `/api/v1/menuTree/findByUserId/${userId}`,
		method: 'get',
	})
}
export function getMenuTree(isManager){
	return request({
		url:`/api/v1/menuTree/showMenuForSetRole/${isManager}`,
		method: 'get',
	})
}

export function getUserMenu(data){
	return request({
		url:'/api/v1/menuTree/saveOrUpdateMenuTree',
		method: 'post',
		data
	})
}

//用户与角色关系
export function userAndRole(data) {
	return request({
		url: '/api/v1/mapUserRole/saveOrUpdateMapUserRoleList',
		method: 'post',
		data
	})
}

//用户、选中权限与角色关系- 新接口
export function saveOrUpdateMenuTreeAndRole(data) {
	return request({
		url: '/api/v1/menuTree/saveOrUpdateMenuTreeAndRole',
		method: 'post',
		data
	})
}
//帐号状态启用，停用
export function userStatus(data) {
	return request({
		url: '/api/v1/user/modifyState',
		method: 'post',
		data
	})

}
//重置密码
export function resetPwd(data){
	return request({
		url:'/api/v1/user/resetPassword',
		method: 'post',
		data
	})
}

//人员管理
export function personnel(data){
	return request({
		url:'/api/v1/personnelManagement/addManagedUserIds',
		method: 'post',
		data
	})
}
//人员管理-新
export function personnelNew(data){
	return request({
		url:'/api/v1/personnelManagement/updateManagedUserIds',
		method: 'post',
		data
	})
}

export function childPersonnel(data){
	return request({
		url:'/api/v1/personnelManagement/findUserId',
		method: 'post',
		data
	}) 
}

//校验账号名是否重复
export function findUserName(data){
	return request({
		url:'/api/v1/check/findUserName',
		method: 'post',
		data
	})
}

//校验邮箱是否重复
export function findEmail(data){
	return request({
		url:'/api/v1/check/findEmail',
		method: 'post',
		data
	})
}

//校验手机号是否重复
export function findPhone(data){
	return request({
		url:'/api/v1/check/findPhone',
		method: 'post',
		data
	})
}

//校验原密码是否重复
export function findPassword(data){
	return request({
		url:'/api/v1/check/findPassWord',
		method: 'post',
		data
	})
}

//查询组织架构
export async function queryAllDepartment(data){
	return request({
		url:'/api/v1/department/findUserIdAllDepartment',
		method: 'post',
		data
	})
}
//组织架构新增部门
export async function addDepartment(data){
	return request({
		url:'/api/v1/department/addDepartment',
		method: 'post',
		data
	})
}

//删除当前部门信息
export async function dellDepartment(data){
	return request({
		url:'/api/v1/department/dellDepartment',
		method: 'post',
		data
	})
}

//组织架构删除部门
export async function delAllDepartment(data){
	return request({
		url:'/api/v1/department/dellAllDepartment',
		method: 'post',
		data
	})
}

//查询人员
export async function findDepartmentUser(data){
	return request({
		url:'/api/v1/userManagement/findDepartmentUser',
		method: 'post',
		data
	})
}

//保存人员管理数据
export async function addUserManagement(data){
	return request({
		url:'/api/v1/userManagement/addUserManagement',
		method: 'post',
		data
	})
}