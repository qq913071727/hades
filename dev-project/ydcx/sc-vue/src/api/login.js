import request from '@/utils/request'

// 登录方法
export function login(username, password) {
  const data = {
    "userName": username,
    "password": password,
  }
  return request({
    url: '/api/v1/user/loginByUsername',
    method: 'post',
    data: data
  })
}

//修改手机号
export function updatePhone(data) {
  return request({
    url: '/api/v1/user/updatePhone',
    method: 'post',
    data
  })
}

// 获取手机验证码
export function getTelcode(mobile) {
  return request({
    url: '/api/v1/smsInfo/send/' + mobile,
    method: 'get'
  })
}
// 手机验证码登录
export function loginTel(tel, code) {
  const data = {
    "phoneNum": tel,
    "code": code,
  }
  return request({
    url: '/api/v1/login/loginBySms',
    method: 'post',
    data: data
  })
}

//忘记密码
export function forgetPwd(data) {
  return request({
    url: '/api/v1/user/forgetPassword',
    method: 'post',
    data: data
  })
}
//修改密码
export function updatePwd(data) {
  return request({
    url:'/api/v1/user/updatePassword',
    method: 'post',
    data
  })
}
//
// 微信授权
export function getWechatAuthUrl(data) {
  return request({
    url: '/api/v1/login/getWechatAuthUrl',
    method: 'post',
    data: data
  })
}
// QQ授权
export function getQQAuthUrl(data) {
  return request({
    url: '/api/v1/login/getQQAuthUrl',
    method: 'post',
    data: data
  })
}

// 支付宝授权
export function getAlipayAuthUrl(data) {
  return request({
    url: '/api/v1/login/getAlipayAuthUrl',
    method: 'post',
    data: data
  })
}

// 第三方绑定
export function bindThirdAccount(data) {
  return request({
    url: '/api/v1/login/bindThirdAccount',
    method: 'post',
    data: data
  })
}

// 微信登录
export function loginByWechat(data) {
  return request({
    url: '/api/v1/login/loginByWechat',
    method: 'post',
    data: data
  })
}
// qq登录
export function loginByQQ(data) {
  return request({
    url: '/api/v1/login/loginByQQ',
    method: 'post',
    data: data
  })
}

// 支付宝登录
export function loginByAlipay(data) {
  return request({
    url: '/api/v1/login/loginByAlipay',
    method: 'post',
    data: data
  })
}

//校验验证码
export function getCode(data){
  return request({
    url:'/api/v1/user/verificationCode',
    method: 'post',
    data
  })
}

//校验账号和手机号是否不正确
export function getUsernameOrPhone(data){
  return request({
    url:'/api/v1/check/findAccountPhone',
    method:'post',
    data
  })
}
// 获取用户详细信息
// export function getInfo () {
//   return request({
//     url: '/ermapi/admins/GetInfo',
//     method: 'get'
//   })
// }

// 获取用户路由
export function getRouters() {
  return request({
    url: `/api/v1/menuTree/findByLoginUserId`,
    method: 'get'
  })
}

//获取用户信息
export function getInfo(data){
  return request({
    url:'/api/v1/user/findUserData',
    method: 'post',
    data
  })
}
// 手机绑定账号
export function bindAccount(data){
  return request({
    url:'/api/v1/smsInfo/bindAccount',
    method: 'post',
    data
  })
}

//获取子账号的菜单
// export function getChildRouters(userId){
//   return request({
//     url:`/api/v1/menuTree/findByUserId/` + userId,
//     method: 'get'
//   })
// }
// 退出方法
// export function logout () {
//   return request({
//     url: '/user/logout',
//     method: 'post'
//   })
// }