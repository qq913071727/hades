import request from "../utils/request";

//获取3c
export function getCCC (params) {
	return request({
		url: '/api/certificate/get3c',
		method: 'get',
		params: params
	})
}

//获取3c列表
export function getCCCList (params) {
	return request({
		url: '/api/certificate/get3cs',
		method: 'get',
		params: params
	})
}

//获取工业产品许可证
export function getIndustry (params) {
	return request({
		url: '/api/certificate/getindustry',
		method: 'get',
		params: params
	})
}

//根据id获取工业产品许可证
export function getIndustryById (params) {
	return request({
		url: '/api/certificate/getindustryById',
		method: 'get',
		params: params
	})
}

//获取工业产品许可证列表
export function getIndustries (params) {
	return request({
		url: '/api/certificate/getindustries',
		method: 'get',
		params: params
	})
}

//获取产品基础数据查询接口
export function getProduct (params) {
	return request({
		url: '/api/certificate/product',
		method: 'get',
		params: params
	})
}

export function health () {
	return request({
		url: '/api/health',
		method: 'get'
	})
}