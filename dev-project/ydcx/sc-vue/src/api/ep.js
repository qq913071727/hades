import request from "../utils/request";

//获取机构列表
export function getEps (params) {
	return request({
		url: '/api/ep/geteps',
		method: 'get',
		params: params
	})
}

//获取机构信息
export function getEp (params) {
	return request({
		url: '/api/ep/getep',
		method: 'get',
		params: params
	})
}

//新增or修改
export function enter (data) {
	return request({
		url: '/api/ep/enter',
		method: 'post',
		data: data
	})
}

//删除
export function del (data) {
	return request({
		url: '/api/ep/delete',
		method: 'post',
		data: data
	})
}