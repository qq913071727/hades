import request from '@/utils/request'

//新增角色
export function addRoles(data) {
    return request({
        url:'/api/v1/role/saveRole',
        method: 'post',
        data
    })
}

//所有角色
export function allRoles() {
    return request({
        url:'/api/v1/role/findAll',
        method: 'get',
    })
}
//分页获取角色
export function pageRoles(pageNo,pageSize){
    return request({
        url:`/api/v1/role/pageFindAll/${pageNo}/${pageSize}`,
        method: 'get',
    })
}

//获取菜单列表
export function menuList() {
    return request({
        url:'/api/v1/menuTree/showMenuForAddRole',
        method: 'get',
    })
}

//角色管理停用
export function roleStop(id,status) {
    return request({
        url:`/api/v1/role/changeAvailableStatus/${id}/${status}`,
        method:'get',
       
    })
}

//修改角色
export function updateRole(roleId){
    return request({
        url:'/api/v1/role/showMenuForRoleById/' + roleId,
        method:'get',
    })
}
export function update(data){
    return request({
        url:'/api/v1/role/showMenuForUpdateRole',
        method:'post',
        data
    })
}
