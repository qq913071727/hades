import request from '@/utils/request'

//添加公司
export function addCompany(data){
    return request({
        url:'/api/v1/myCompany/registerCompany',
        method: 'post',
        data
    })
}

//模糊搜索公司
export function searchCompany(params){
    return request({
        url:'/api/v1/myCompany/searchCompany',
        method: 'get',
        params: params
    })
}

//通过天眼查查公司
export function searchCompanyTYC(params){
    return request({
        url:'/api/v1/myCompany/findByMyCompanyTYC',
        method: 'get',
        params:params
    })
}

//获取公司详情
export function companyDetail(params){
    return request({
        url:'/api/v1/myCompany/getDetail',
        method: 'get',
        params: params
    })
}

//获取公司列表
export function companyList(data){
    return request({
        url:'/api/v1/myCompany/findByMyCompany',
        method: 'post',
        data
    })
}

//停用
export function companyStop(params){
    return request({
        url:'/api/v1/myCompany/deactivateMyCompany',
        method: 'get',
        params: params
    })
}

//校验公司是否存在
export function checkCompany(params){
    return request({
        url:'/api/v1/myCompany/checkRegistrationCompany',
        method: 'get',
        params: params
    })
}

//查字典
export function searchDistrict(){
    return request({
        url:'/api/v1/myCompany/searchMyCompanyDistrict',
        method: 'get',
    })
}

//上传文件
export function fileUpload(data){
    return request({
        url:'/api/v1/uploadFile/pictureUpload',
        method: 'post',
        data
    })
}

// 我的公司管理- 编辑保存
export async function updateBookAccount(data){
    return request({
        url:'/api/v1/myCompany/updateBookAccount',
        method: 'post',
        data
    })
}

// 我的公司管理- 修改公司信息
export async function updateMyCompany(data){
    return request({
        url:'/api/v1/myCompany/updateMyCompany',
        method: 'post',
        data
    })
}