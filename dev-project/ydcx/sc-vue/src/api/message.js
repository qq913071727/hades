import request from '@/utils/request'

//消息提醒
export function getMessage(params){
    return request({
        url:'/api/v1/actionMessage/page',
        method:'get',
        params:params
    })
}

//全部已读
export function readAll(){
    return request({
        url:'/api/v1/actionMessage/isReadAll',
        method:'get',
    })
}