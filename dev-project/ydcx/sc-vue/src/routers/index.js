import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: 路由配置项
 *
 * hidden: true                   // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true               // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect           // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'             // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * meta : {
		noCache: true                // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
		title: 'title'               // 设置该路由在侧边栏和面包屑中展示的名字
		icon: 'svg-name'             // 设置该路由的图标，对应路径src/assets/icons/svg
		breadcrumb: false            // 如果设置为false，则不会在breadcrumb面包屑中显示
		activeMenu: '/system/user'   // 当路由设置了该属性，则会高亮相对应的侧边栏。
	}
 */

//公共路由
export const constantRoutes = [
	{
		path: '/redirect',
		component: Layout,
		hidden: true,
		children: [
			{
				path: '/redirect/:path(.*)',
				component:() =>import('@/views/redirect'),
			}
		]
	},
	{
		path: '/login',
		component: () =>
			import('@/views/login'),
		hidden: true
	},
	//忘记密码
	{
		path: '/change-password',
		component: () => import('@/views/password/changePassword'),
		hidden: true,
	},
	//修改密码
	{
		path: '/success-password',
		name:'success-password',
		component: () => import('@/views/password/successPassword'),
		hidden: true,
	},
	{
		path: '/sso',
		meta: {
			title: '供应商协同系统-登录',
		},
		component: () =>
			import('@/views/sso'),
		hidden: true,
	},
	{
		path: '/404',
		component:() => import ('@/views/404'),
		hidden: true
	},
	// {
	// 	path: '/401',
	// 	component: (resolve) => require(['@/views/error/401'], resolve),
	// 	hidden: true
	// },

	{
		path: '/',
		component: Layout,
		redirect: 'index',
		children: [
			{
				path: '/',
				component: () => import('@/views/index'),
				name: 'Index',
				meta: { title: '首页', icon: 'dashboard', affix: true }
			},
			{
				path: '/userDetail',
				name:'UserDetail',
				component: () =>
					import('@/views/userDetail'),
				meta: { title: '账号信息'},
				hidden: true
			},
		]
	},
	{
		path: '/orderStock',
		component: Layout,
		name: 'OrderStock',
		meta: { title: '订单管理', icon: 'chart' },
		children: [
			{
				path: '/myOrder',
				name: 'MyOrder',
				component: () => import('@/views/orderStock/myOrder/list'),
				meta: { title: '我的订单'},
			},
			{
				path: '/orderDetail',
				name:'OrderDetail',
				component: () => import('@/views/orderStock/orderDetail/list'),
				meta:{title:'我的订单详情'},
				hidden: true 
			}
		],
		alwaysShow: true,
	},
	{
		path: '/manageOffer',
		component: Layout,
		name: 'ManageOffer',
		meta: { title: '报价管理', icon: 'post' },
		children: [
			// {
			// 	path: '/actualOffer',
			// 	name: 'ActualOffer',
			// 	component: () => import('@/views/manageOffer/actualOffer/list'),
			// 	meta: { title: '实单报价' }
			// },
			{
				path: '/quotationTask',
				name: 'CommonOffer',
				component: () => import('@/views/manageOffer/commonOffer/list'),
				meta: { title: '报价任务' }
			},
			{
				path: '/myBragining',
				name: 'MyBragining',
				component: () => import('@/views/manageOffer/myBragining/list'),
				meta: { title: '我的议价' }
			},
			{
				path: '/sommer',
				name: 'Sommer',
				component: () => import('@/views/manageOffer/sommer/list.vue'),
				meta: { title: '搜商机' }
			},
			// {
			// 	path: '/sommer/list',
			// 	name: 'SommerList',
			// 	component: () => import('@/views/manageOffer/sommer/list.vue'),
			// 	meta: { title: '搜商机列表' }
			// },
		],
	},
	{
		path: '/stock',
		component: Layout,
		name: 'Stock',
		meta: { title: '库存管理', icon: 'dict' },
		children: [
			{
				path: '/uploadStock',
				name: 'UploadStock',
				component: () => import('@/views/stock/uploadStock/list'),
				meta: { title: '上传库存'}
			},
			{
				path: '/steppedPrice',
				name: 'SteppedPrice',
				component: () => import('@/views/stock/steppedPrice/list'),
				meta: { title: '阶梯价格管理'}
			},
			{
				path: '/manageStock',
				name: 'ManageStock',
				component: () => import('@/views/stock/manageStock/list'),
				meta: { title: '管理库存'}
			},
			{
				path: '/advantageBrand',
				name: 'AdvantageBrand',
				component: () => import('@/views/stock/advantageBrand/list'),
				meta: { title: '优势品牌' }
			},
			// {
			// 	path: '/advantageModel',
			// 	name: 'AdvantageModel',
			// 	component: () => import('@/views/stock/advantageModel/list'),
			// 	meta: { title: '优势型号'}
			// },
			
		],
	},
	{
		path: '/finance',
		name: 'Finance',
		component: Layout,
		meta: { title: '财务管理', icon: 'money' },
		children: [
			{
				path: '/awaitBill',
				name: 'AwaitBill',
				component: () => import('@/views/finance/awaitBill'),
				meta: { title: '对账单'}
			},
			{
				path: '/billDetail',
				name: 'billDetail',
				component: () => import(/* webpackChunkName: "group-main" */ '@/views/finance/billDetail'),
				meta: { title: '对账单详情'},
				hidden: true 
			},
			{
				path: '/awaitInvoiced',
				name: 'AwaitInvoiced',
				component: () => import('@/views/finance/awaitInvoiced'),
				meta: { title: '待开发票' }
			},
			{
				path: '/awaitInvoicedInfo',
				name: 'awaitInvoicedInfo',
				component: () => import(/* webpackChunkName: "group-main" */ '@/views/finance/awaitInvoicedInfo'),
				meta: { title: '申请发票详情' },
				hidden: true 
			},
			{
				path: '/invoiced',
				name: 'Invoiced',
				component: () => import('@/views/finance/Invoiced'),
				meta: { title: '已开发票'},
			},
			{
				path: '/invoicedInfo',
				name: 'InvoicedInfo',
				component: () => import('@/views/finance/InvoicedInfo'),
				meta: { title: '已开发票详情'},
			},
			{
				path: '/contractManage',
				name: 'ContractManage',
				component: () => import('@/views/finance/contractManage'),
				meta: { title: '合同管理' }
			},
			
		]
	},
	{
		path: '/system',
		name: 'System',
		component: Layout,
		meta: { title: '系统管理', icon: 'build' },
		children: [
			{
				path: '/manageAccount',
				name: 'ManageAccount',
				component: () => import('@/views/system/manageAccount/list'),
				meta: { title: '账号管理'}
			},
			{
				path: '/manageRole',
				name: 'ManageRole',
				component: () => import('@/views/system/manageRole/list'),
				meta: { title: '角色管理' }
			},
			{
				path: '/myCompany',
				name: 'MyCompany',
				component: () => import('@/views/system/myCompany/list'),
				meta: { title: '我的公司管理'},
			},
			{
				path:'/add',
				name:'Add',
				component: () => import('@/views/system/myCompany/add'),
				meta: { title: '我的公司新增'},
				hidden: true
			},
			{
				path:'/see',
				name:'See',
				component: () => import('@/views/system/myCompany/see'),
				meta: { title: '我的公司详情'},
				hidden: true
			},
			{
				path:'/edit',
				name:'Edit',
				component: () => import('@/views/system/myCompany/edit'),
				meta: { title: '我的公司编辑'},
				hidden: true
			},
			{
				path:'/edit2',
				name: 'Edit2',
				component: () => import('@/views/system/myCompany/add'),
				meta: { title: '我的公司编辑'},
				hidden: true
			},
			{
				path: '/systemNotice',
				name: 'SystemNotice',
				component: () => import('@/views/system/systemNotice/list'),
				meta: { title: '系统公告' }
			},
			{
				path: '/news',
				name: 'News',
				component: () => import('@/views/system/news/list'),
				meta: { title: '消息提醒' }
			},
			{
				path: '/newsDetail',
				name: 'NewsDetail',
				component: () => import('@/views/system/news/newsDetail'),
				meta: { title: '查看' },
				hidden: true 
			},
			// {
			// 	path: '/noticeDetail',
			// 	name: 'NoticeDetail',
			// 	component: () => import('@/views/system/noticeDetail/list'),
			// 	meta: { title: '查看' },
			// 	hidden: true 
			// },
		]
	},
	
	
]

// 防止连续点击多次路由报错
let routerPush = Router.prototype.push;
Router.prototype.push = function push(location) {
	return routerPush.call(this, location).catch(err => err)
}

export default new Router({
	mode: "hash", 
	scrollBehavior: () => ({ y: 0 }),
	routes: constantRoutes
});
// export default constantRoutes
