import { getUnReadTotal} from '@/api/menu';
const state = {
    actionMessageUnReadTotal: 0,
}
const mutations = {
    "GET_ACTION_NUM": (state, val) => {
        state.actionMessageUnReadTotal = val || 0;
    },
}
const actions = {
    // 未读消息总数-消息提醒
    getActionMessageUnReadTotal({ commit }) {
        getUnReadTotal()
            .then(res => {
                if (res.success) {
                    commit('GET_ACTION_NUM', res.result)
                } else {
                    commit('GET_ACTION_NUM', 0)
                }
            })
    },
}
export default {
    namespaced: true,
    state,
    mutations,
    actions
}