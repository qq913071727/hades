import { getUnReadTotal, getInquiry } from '@/api/menu';

const state = {
    actionMessageUnReadTotal: 0,
    inquiryTotal: {
        "实单报价数量": 0,
        "普通报价数量": 0,
        "议价任务数量": 0,
        "total": 0
    }
}
const mutations = {
    "GET_ACTION_NUM": (state, val) => {
        state.actionMessageUnReadTotal = val || 0;
    },
    "GET_INQUIRY_NUM": (state, val) => {
        state.inquiryTotal = val || {
            "实单报价数量": 0,
            "普通报价数量": 0,
            "议价任务数量": 0,
            "total": 0
        };
    }
}

const actions = {
    // 未读消息总数-消息提醒
    getActionMessageUnReadTotal({ commit }) {
        getUnReadTotal()
            .then(res => {
                if (res.success) {
                    commit('GET_ACTION_NUM', res.result)
                } else {
                    commit('GET_ACTION_NUM', 0)
                }
            })
    },
    // 未读消息总数-报价管理
    getInquiryTotal({ commit }, data) {
        getInquiry(data)
            .then(res => {
                if (res.success) {
                    res.data.total =  res.data["实单报价数量"] + res.data["普通报价数量"] + res.data["议价任务数量"];
                    commit('GET_INQUIRY_NUM', res.data)
                } else {
                    commit('GET_INQUIRY_NUM', {
                        "实单报价数量": 0,
                        "普通报价数量": 0,
                        "议价任务数量": 0,
                        "total": 0
                    })
                }
            })
    }
}
export default {
    namespaced: true,
    state,
    mutations,
    actions
}