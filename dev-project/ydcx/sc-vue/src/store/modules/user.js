import { login,loginTel } from '@/api/login'
import {  getInfo } from '@/api/demo'
import { getToken, setToken, removeToken } from '@/utils/auth'

const user = {
  state: {
    token: getToken(),
    name: (localStorage.getItem("SET_NAME")|| ''),
    avatar: '',
    id:(localStorage.getItem("SET_USERID")|| ''),
    phone:(localStorage.getItem("SET_PHONE")|| ''),
    email:(localStorage.getItem("SET_EMAIL")|| ''),
    status:(localStorage.getItem("SET_STATUS")|| ''),
    createDate:(localStorage.getItem("SET_CREATEDATE")|| ''),
    // myCompanyNameList:(sessionStorage.getItem("SET_COMPANY")|| '').split(',')|| [],
    position:(localStorage.getItem("SET_POSITION")|| ''),
    username:(localStorage.getItem("SET_USERNAME")|| ''),
    score:(localStorage.getItem("SET_SCORE")|| ''),
    isMain:(localStorage.getItem("SET_ISMAIN")|| ''),
    epName:(localStorage.getItem("SET_EPNAME")|| ''),
    roles: [],
    permissions: (localStorage.getItem("SET_PERMISSIONS")|| '').split(',')|| [],
    fatherId:(localStorage.getItem("SET_FATHENRID") || ''),
  },

  mutations: {
    SET_ISMAIN:(state,isMain)=>{
      state.isMain = isMain;
      localStorage.setItem("SET_ISMAIN", isMain)
    },
    SET_USERNAME:(state,username)=>{
      state.username = username;
      localStorage.setItem("SET_USERNAME", username)
    },
    SET_POSITION:(state,position)=>{
      state.position = position;
      localStorage.setItem("SET_POSITION", position)
    },
    SET_CREATEDATE:(state,createDate)=>{
      state.createDate = createDate;
      localStorage.setItem("SET_CREATEDATE", createDate)
    },
    SET_SCORE:(state,score)=>{
      state.score = score;
      localStorage.setItem("SET_SCORE", score)
    },
    SET_STATUS:(state,status)=>{
      state.status = status;
      localStorage.setItem("SET_STATUS", status)
    },
    SET_EMAIL:(state,email)=>{
      state.email = email;
      localStorage.setItem("SET_EMAIL", email)
    },
    SET_PHONE:(state,phone)=>{
      state.phone = phone;
      localStorage.setItem("SET_PHONE", phone)
    },
    SET_USERID:(state,id) => {
      state.id = id
      localStorage.setItem("SET_USERID", id)
    },
    SET_EPNAME:(state,epName) => {
      state.epName = epName
      localStorage.setItem("SET_EPNAME", epName)
    },
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, name) => {
      state.name = name
      localStorage.setItem("SET_NAME", name)
    },
    // SET_COMPANY:(state,myCompanyNameList)=>{
    //   state.myCompanyNameList = myCompanyNameList
    //   sessionStorage.setItem("SET_COMPANY", myCompanyNameList)
    // },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_PERMISSIONS: (state, permissions) => {
      state.permissions = permissions
      localStorage.setItem("SET_PERMISSIONS", permissions)
    },
    SET_FATHENRID:(state,fatherId) =>{
      state.fatherId = fatherId
      localStorage.setItem("SET_FATHENRID", fatherId)
    },
  },

  actions: {
    // 登录
    Login ({ commit }, userInfo) {
      console.log(userInfo,'3333333333-------');
      const username = userInfo.username.trim();
      const password = userInfo.password.trim();
      return new Promise((resolve, reject) => {
        login(username, password).then(res => {
          // console.log(res,'1234567');
          if(res.code == 200){
            setToken(res.result.token)
            commit('SET_TOKEN', res.result.token);
            commit('SET_ROLES', ['ROLE_DEFAULT'])
            commit('SET_NAME', res.result.name)
            // commit('SET_COMPANY', res.result.myCompanyNameList)
            commit('SET_USERID',res.result.id)
            commit('SET_PHONE',res.result.phone)
            commit('SET_EMAIL',res.result.email)
            commit('SET_STATUS',res.result.status)
            commit('SET_CREATEDATE',res.result.createDate)
            commit('SET_POSITION',res.result.position)
            commit('SET_USERNAME',res.result.username)
            commit('SET_SCORE',res.result.score)
            commit('SET_ISMAIN',res.result.isMain)
            commit('SET_EPNAME',res.result.epName)
            commit('SET_FATHENRID',res.result.fatherId)
          }
          resolve(res)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 手机登录
    TelLogin ({ commit }, userInfo) {
      const tel = userInfo.tel.trim()
      const code = userInfo.code.trim()
      return new Promise((resolve, reject) => {
        loginTel(tel,code).then(res => {
          console.log(res,'手机登录');
          if(!res.success){
            resolve(res)
          }else{
            setToken(res.result.token)
            commit('SET_TOKEN', res.result.token);
            commit('SET_ROLES', ['ROLE_DEFAULT'])
            commit('SET_NAME', res.result.name)
            // commit('SET_COMPANY', res.result.myCompanyNameList)
            commit('SET_USERID',res.result.id)
            commit('SET_PHONE',res.result.phone)
            commit('SET_EMAIL',res.result.email)
            commit('SET_STATUS',res.result.status)
            commit('SET_CREATEDATE',res.result.createDate)
            commit('SET_POSITION',res.result.position)
            commit('SET_USERNAME',res.result.username)
            commit('SET_SCORE',res.result.score)
            commit('SET_ISMAIN',res.result.isMain)
            commit('SET_EPNAME',res.result.epname)
            commit('SET_FATHENRID',res.result.fatherId)
            resolve(res)
          }
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 第三方登录

    // 获取用户信息
    // GetInfo ({ commit, state }) {
    //   return new Promise((resolve, reject) => {
    //     getInfo().then(res => {
    //       console.log(res);
    //       const user = res.data
    //       // const avatar = user.avatar == "" ? require("@/assets/images/profile.jpg") : process.env.VUE_APP_BASE_API + user.avatar;
    //       const avatar = require("@/assets/images/profile.jpg")
    //       // if (res.roles && res.roles.length > 0) { // 验证返回的roles是否是一个非空数组
    //       //   commit('SET_ROLES', res.roles)
    //       //   commit('SET_PERMISSIONS', res.permissions)
    //       // } else {
    //       commit('SET_ROLES', ['ROLE_DEFAULT'])
    //       // }
    //       commit('SET_NAME', user.nikename)
    //       commit('SET_AVATAR', avatar)
    //       resolve(res)
    //       debugger;;
    //     }).catch(error => {
    //       reject(error)
    //     })
    //   })
    // },
     // 设置token
    SetInToken({ commit }, res) {
      console.log(res,'eeeeeeeeeeeeeeeeeeee');
      return new Promise((resolve) => {
        console.log(res,'store');
          setToken(res.token)
          commit('SET_ROLES', ['ROLE_DEFAULT'])
          // setRrfreshToken(res.refresh_token, res.expires_in || '')
          commit('SET_TOKEN', res.token)
          resolve()
      })
    },

    // 退出系统
    LogOut ({ commit, state }) {
      return new Promise((resolve, reject) => {
        // logout(state.token).then(() => {
        //   commit('SET_TOKEN', '')
        //   // commit('SET_ROLES', [])
        //   // commit('SET_PERMISSIONS', [])
        //   removeToken()
        //   resolve()
        // }).catch(error => {
        //   reject(error)
        // })

        commit('SET_TOKEN', '');
        removeToken();
        resolve();
      })
    },

    // 前端 登出
    FedLogOut ({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  }
}

export default user
