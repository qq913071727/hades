import { addResizeListener, removeResizeListener } from 'element-ui/src/utils/resize-event'
import vue from 'vue'
const doResize = async(el, binding, vnode) => {
    const { componentInstance: $table } = await vnode
    const { value } = binding
    if (!$table.height) {
        throw new Error(`el-$table must set the height. Such as height='100px'`)
    }
    const bottomOffset = (value && value.bottomOffset) || 30
    if (!$table) return
    const height = window.innerHeight - el.getBoundingClientRect().top - bottomOffset
    $table.layout.setHeight(height)
    $table.doLayout()

}
vue.directive('el-select-loadmore',{
    // bind (el, binding) {
    //     console.log(el,'333333333333333333el');
    //     const SELECTWRAP_DOM = el.querySelector(
    //       '.el-select-dropdown .el-select-dropdown__wrap'
    //     )
    //     SELECTWRAP_DOM.addEventListener('scroll', function () {
    //       const condition =
    //         this.scrollHeight - this.scrollTop <= this.clientHeight
    //       if (condition) {
    //         binding.value()
    //       }
    //     })
    //   }
    inserted(el, binding) {
        const SELECTWRAP_DOM = el.querySelector('.el-select-dropdown .el-select-dropdown__wrap');
        SELECTWRAP_DOM.addEventListener('scroll', function() {
          const condition = this.scrollHeight - this.scrollTop <= this.clientHeight;
          console.log(condition,'condition111111111111111');
          if (condition) {
            binding.value();
          }
        });
      }
})
vue.directive('adaptive', {
    bind(el, binding, vnode) {
        if (binding.value.bottomOffset) {
            el.resizeListener = async() => {
                await doResize(el, binding, vnode)
            }
            addResizeListener(el, el.resizeListener)
            addResizeListener(window.document.body, el.resizeListener)
        }
    },
    async inserted(el, binding, vnode) {
        if (binding.value.bottomOffset) {
            await doResize(el, binding, vnode)
        }
    },
    async componentUpdated(el, binding, vnode) {
        if (binding.value.bottomOffset) {
            await doResize(el, binding, vnode)
        }
    },
    unbind(el) {
        removeResizeListener(el, el.resizeListener)
    },
})

vue.directive('changeprice', {
    inserted(el) {
        let $inp = el.querySelector('input')
        if ($inp.value && $inp.value * 1 == 0) {
            $inp.value = 0.01
            let ev = document.createEvent("HTMLEvents")
            ev.initEvent("change", false, true)
            $inp.dispatchEvent(ev)
        }
        $inp.addEventListener('blur', (e) => {
            if (e.target.value && e.target.value * 1 == 0) {
                e.target.value = 0.01
                let ev = document.createEvent("HTMLEvents")
                ev.initEvent("change", false, true)
                $inp.dispatchEvent(ev)
            }

        })
    },
})


//引入elementui
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

if (window.innerWidth < 1300) {
    vue.use(ElementUI, { size: 'mini', zIndex: 3000 })
} else {
    vue.use(ElementUI, { size: 'small', zIndex: 3000 })
}

//--------------------------
// import 'vxe-table/lib/style.css'
// import XEUtils from 'xe-utils'
// import { VXETable, Validator, Header, Column, Pager, Checkbox, Table, Tooltip, Edit, Select } from 'vxe-table'
// import zhCN from 'vxe-table/lib/locale/lang/zh-CN'
// 按需加载的方式默认是不带国际化的，自定义国际化需要自行解析占位符 '{0}'，例如：
// VXETable.setup({
//     i18n: (key, args) => XEUtils.toFormatString(XEUtils.get(zhCN, key), args),
//     table: {
//         border: true,
//         resizable: true,
//     },
// })
// VXETable.interceptor.add('event.clearActived', () => {
//         // 比如点击了某个组件的弹出层面板之后，此时被激活单元格不应该被自动关闭，通过返回 false 可以阻止默认的行为。
//         return false
//     })
    // 表格功能
// vue.use(VXETable)
// vue.use(Header)
// vue.use(Tooltip)
// vue.use(Edit)
// vue.use(Validator)
// vue.use(Column)
// vue.use(Pager)
// vue.use(Checkbox)
// vue.use(Table)
// vue.use(Select)
    //--------------------------



// import hncomponents from 'hn-components'
// vue.use(hncomponents)
// import validator from '@/utils/validator'
// vue.use(validator)
// import mcard from '@/components/mcard.vue'
// vue.component('mcard', mcard)
import myTable from '@/components/myTable/index.vue'
vue.component('myTable', myTable)
// import Pagination from '@/components/pagination.vue'
// vue.component('Pagination',Pagination)
// import search from '@/components/search.vue'
// vue.component('search', search)
// import myselect from '@/components/select.vue'
// vue.component('myselect', myselect)
// import myTag from '@/components/tag.vue'
// vue.component('myTag', myTag)
// import mixin from '@/mixins/index'
// vue.mixin(mixin)
vue.config.productionTip = false