/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
  const valid_map = ['admin', 'editor']
  return valid_map.indexOf(str.trim()) >= 0
}

/**
 * @param {string} url
 * @returns {Boolean}
 */
export function validURL(url) {
  const reg = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
  return reg.test(url)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validLowerCase(str) {
  const reg = /^[a-z]+$/
  return reg.test(str)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUpperCase(str) {
  const reg = /^[A-Z]+$/
  return reg.test(str)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validAlphabets(str) {
  const reg = /^[A-Za-z]+$/
  return reg.test(str)
}

/**
 * @param {string} email
 * @returns {Boolean}
 */
export function validEmail(email) {
  const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return reg.test(email)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function isString(str) {
  if (typeof str === 'string' || str instanceof String) {
    return true
  }
  return false
}

/**
 * @param {Array} arg
 * @returns {Boolean}
 */
export function isArray(arg) {
  if (typeof Array.isArray === 'undefined') {
    return Object.prototype.toString.call(arg) === '[object Array]'
  }
  return Array.isArray(arg)
}



/**匹配不包含汉字校验*/
export function validateCommon(rule, value, callback) {
  let commonReg = /^[A-Za-z0-9]+$/
  if (value === '' || value === undefined || value == null) {
    return callback(new Error('此项不能为空'))
  }
  setTimeout(() => {
    if (commonReg.test(value)) {
      callback()
    } else {
      callback(new Error('只能包含数字和字母，请重新输入'))
    }
  }, 300)
}

/**手机号码校验*/
export function validatephonenumber(rule, value, callback) {
  let commonReg = /^[1][3,4,5,6,7,8,9][0-9]{9}$/
  if (value === '' || value === undefined || value === null) {
    return callback(new Error('手机号不能为空'))
  }
  setTimeout(() => {
    if (commonReg.test(value)) {
      callback()
    } else {
      callback(new Error('手机号有误,请重新输入'))
    }
  }, 300)
}

/**邮箱校验*/
export function checkEmail(rule, value, callback) {
  let emailReg =  /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;
  if (value === "" || value === undefined || value == null) {
    return callback(new Error("请输入电子邮箱"));
  }
  setTimeout(() => {
    if (emailReg.test(value)) {
      callback();
    } else {
      callback(new Error("请输入正确的邮箱格式"));
    }
  }, 300);
}

/**账号名校验*/
export function validateUsername(rule, value, callback) {
  const username = /^[\a-\z\A-\Z0-9\u4e00-\u9fe5]{8,20}$/
  if (value.indexOf(' ') !== -1) {
    return callback(new Error("请设置账号信息"));
  } else {
    if (!username.test(value)) {
      callback(new Error('长度8-20位，可输入汉字、字母或数字'))
    } else {
      callback()
    }
  }
}

/**员工姓名校验*/
export function validateRealname(rule, value, callback) {
  const realname = /^[\a-\z\A-\Z\/.\u4e00-\u9fe5\s?]{0,10}$/
    if (!realname.test(value)) {
      callback(new Error('长度10位以内，可输入汉字、字母、空格或英文点'))
    } else if(value.trim() == "") {
      callback(new Error('输入部分内容后，允许输入空格'))
    }else {
      callback()
    }
}

/**校验新密码*/
export function pwdCheck(rule, value, callback) {
  const reg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/;
  if (!reg.test(value)) {
    return callback(new Error('请输入6-20位密码，要求数字、字母组合'))
  } else {
    callback()
  }
} 

/**校验收款公司全部输入空格 */
export function checkCollectionCompany(rule,value,callback) {
  const reg = value.split(" ").join("").length == 0;
  if(reg){
    return callback(new Error('收款公司不能为空'))
  }else{
    callback()
  }
}

/**校验开户银行全部输入空格 */
export function checkopenBank(rule,value,callback) {
  const reg = value.split(" ").join("").length == 0;
  if(reg){
    return callback(new Error('开户银行不能为空'))
  }else{
    callback()
  }
}

/**校验账号全部输入空格 */
export function checkaccountNum(rule,value,callback) {
  const reg = value.split(" ").join("").length == 0;
  if(reg){
    return callback(new Error('账号不能为空'))
  }else{
    callback()
  }
}
