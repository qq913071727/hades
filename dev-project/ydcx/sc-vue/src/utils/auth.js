import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'

export function getToken(host = '') {
  return Cookies.get(TokenKey, { domain: host })
}
export function setToken(token, time = 86400, host = '') {
  return Cookies.set(TokenKey, token, {
      expires: new Date(new Date().getTime() + (time * 1000)),
      domain: host
  })
}
export function removeToken(host = '') {
  return Cookies.remove(TokenKey, { domain: host })
}
// export function getToken() {
//   return Cookies.get(TokenKey)
// }

// export function setToken(token) {
//   return Cookies.set(TokenKey, token)
// }

// export function removeToken() {
//   return Cookies.remove(TokenKey)
// }
