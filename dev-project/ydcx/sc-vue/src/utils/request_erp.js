import axios from 'axios'
import { Notification, Message } from "element-ui"
import * as auth from '../utils/auth'
axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8'


// 创建axios实例
const service = axios.create({
    // axios中请求配置有baseURL选项，表示请求URL公共部分
    baseURL: process.env.VUE_APP_BASE_URL_ERP,
    // 超时
    timeout: 60000,
    crossDomain: true
})


let errorflag = null

let interfaceParams = {
    environmentId:1,
    method:'',
    serviceId:13,
    systemId:3,
    url:'',
}
function interfaceLog(params){
    axios.post(process.env.NODE_ENV == 'development' ? process.env.VUE_APP_MONITOR +'api/v1/interfaceCallLog/save':  process.env.VUE_APP_MONITOR +'api/v1/interfaceCallLog/save',params).then(res=>{
        return res.data
    })
}

//请求拦截，在每个请求发出去之前，针对每个域名做不同的配置
service.interceptors.request.use((config) => {
    // console.log(config,'configconfig');
    if(config.baseURL == 'https://erpve.b1b.cn'){
        interfaceParams.environmentId = 2
    }else if(config.baseURL == 'https://erp.b1b.cn'){
        interfaceParams.environmentId = 4
    }else if(config.baseURL == 'https://erp9.b1b.com'){
        interfaceParams.environmentId = 1
    }
    // 如果由fullUrl属性  则表示请求的是另一个域名  处理李亚楠提供的api
    config.fullUrl && (config.baseURL = '')
    interfaceParams.method = config.method;
    interfaceParams.url = config.url;
    // interfaceLog(interfaceParams)
        // config.baseURL = process.env.VUE_APP_BASE_URL_ERP
        // 是否需要设置 token
        // const isToken = (config.headers || {}).isToken === false;
        // let Token = auth.getToken()
        // if (Token && !isToken) {
        //   config.headers["Token"] = "Bearer " + auth.getToken(); // 让每个请求携带自定义token 请根据实际情况自行修改
        //   // config.headers["Cookie"] = auth.getToken();
        // }
        // config.headers['Access-Control-Allow-Origin'] = '*'
    return config
})

// 响应拦截器
service.interceptors.response.use(
    (res) => {

        let code = res.status
        if (res.data.status) {
            code = res.data.status
        }
        const msg = res.data.msg || res.data.message || '发生错误:' + code + ',请重试'
        if (code * 1 !== 200) {
            if (errorflag) {
                errorflag.close()
            }
            errorflag = Message({
                message: msg,
                type: 'error',
                duration: 3 * 1000,
                offset: window.screen.height / 3,
            })
            return {}
        } else {
            if (res.data && res.data.status) {
                return res.data
            } else {
                return res
            }

        }

    },
    (error) => {
        console.log(error);
        if (errorflag) {
            errorflag.close()
        }
        errorflag = Message({
            message: (error.toJSON() && error.toJSON().message) || '发生错误,请重试 ',
            type: 'error',
            duration: 3 * 1000,
            offset: window.screen.height / 3,
        })
    }
)

export default service