'use strict'
const path = require('path')

function resolve (dir) {
	return path.join(__dirname, dir)
}

const name = process.env.VUE_APP_TITLE || '供应商协同系统' // 网页标题

// vue.config.js 配置说明
//官方vue.config.js 参考文档 https://cli.vuejs.org/zh/config/#css-loaderoptions
// 这里只列一部分，具体配置参考文档
module.exports = {
	// 部署生产环境和开发环境下的URL。
	// 默认情况下，Vue CLI 会假设你的应用是被部署在一个域名的根路径上
	// 例如 https://www.ruoyi.vip/。如果应用被部署在一个子路径上，你就需要用这个选项指定这个子路径。例如，如果你的应用被部署在 https://www.ruoyi.vip/admin/，则设置 baseUrl 为 /admin/。
	publicPath: "./",
	// 在npm run build 或 yarn build 时 ，生成文件的目录名称（要和baseUrl的生产环境路径一致）（默认dist）
	outputDir: 'dist', 
	// 用于放置生成的静态资源 (js、css、img、fonts) 的；（项目打包之后，静态资源会放在这个文件夹下）
	assetsDir: 'static',
	// 是否开启eslint保存检测，有效值：ture | false | 'error'
	// lintOnSave: process.env.NODE_ENV === 'development',
	// 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
	productionSourceMap: false,
	// webpack-dev-server 相关配置
	devServer: {
		// host: '0.0.0.0',
		port: 8080,
		disableHostCheck: true,
		headers: {
			'Access-Control-Allow-Origin': '*',
		},
		// 代理跨域的配置
		// proxy: {
		// 	// 当我们的本地的请求 有/api的时候，就会代理我们的请求地址向另外一个服务器发出请求
		// 	// 这里的api 表示如果我们的请求地址有/api的时候,就出触发代理机制
		// 	// localhost:8888/api/abc  => 代理给另一个服务器
		// 	[process.env.VUE_APP_BASE_API]: {
		// 		target: `http://erp9.b1b.com/`,		 // 跨域请求的地址
		// 		changeOrigin: true,		// 只有这个值为true的情况下 才表示开启跨域
		// 		secure: false, // 如果是https接口，需要配置这个参数
		// 		// 路径重写
		// 		// pathRewrite: {
		// 		// 	// 重新路由  localhost:8888/api/login  => localhost:8081/api/login
		// 		// 	['^' + process.env.VUE_APP_BASE_API]: '/'
		// 		// }
		// 	}
		// },
	},
	configureWebpack: config => {
		const sassLoader = require.resolve('sass-loader');
		config.module.rules.filter(rule => {
			return rule.test.toString().indexOf("scss") !== -1;
		})
		.forEach(rule => {
			rule.oneOf.forEach(oneOfRule => {
				const sassLoaderIndex = oneOfRule.use.findIndex(item => item.loader === sassLoader);
				oneOfRule.use.splice(sassLoaderIndex, 0,
					{ loader: require.resolve("css-unicode-loader")}
				);
			});
		})
		if(process.env.NODE_ENV === 'production') {
			const terserWebpackPlugin = config.optimization.minimizer[0];
			const { terserOptions } = terserWebpackPlugin.options;
			terserOptions.compress.drop_console = true;
			config.mode = 'production';
			config['performance'] = {
				"maxEntrypointSize": 10000000,
				'maxAssetSize':30000000,
			}
		}
		config.output.library = `${name}-[name]`,
		config.output.libraryTarget = 'umd',
		config.output.jsonpFunction = `webpackJsonp_${name}`

	},
	chainWebpack (config) {
		config.plugins.delete('preload') // TODO: need test
		config.plugins.delete('prefetch') // TODO: need test

		// set svg-sprite-loader
		config.module
			.rule('svg')
			.exclude.add(resolve('src/assets/icons'))
			.end()
		config.module
			.rule('icons')
			.test(/\.svg$/)
			.include.add(resolve('src/assets/icons'))
			.end()
			.use('svg-sprite-loader')
			.loader('svg-sprite-loader')
			.options({
				symbolId: 'icon-[name]'
			})
			.end()
	},
	css: {
		loaderOptions: {
		  css: {},
		  postcss: {
			plugins: [
			  require('postcss-px2rem')({
				remUnit: 16
			  })
			]
		  }
		}
	  }
}
