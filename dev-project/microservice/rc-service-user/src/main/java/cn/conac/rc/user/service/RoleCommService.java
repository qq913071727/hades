package cn.conac.rc.user.service;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.conac.rc.framework.utils.BeanMapper;
import cn.conac.rc.framework.utils.IdGen;
import cn.conac.rc.user.entity.RoleEntity2;
import cn.conac.rc.user.entity.RolePermissionEntity;
import cn.conac.rc.user.vo.RoleInfoVo;

/**
 * UserCommonService类
 *
 * @author serviceCreator
 * @date 2017-04-11
 * @version 1.0
 */
@Service
@Transactional
public class RoleCommService {

	@Autowired
	private RoleService2 roleService2;

	@Autowired
	private RolePermissionService rolePermissionService;

	/**
	 * 保存用户信息
	 * 
	 * @param userInfoVo
	 * @return 用户ID
	 */
	public Map<String, String> SaveRoleInfo(RoleInfoVo roleInfoVo) throws Exception {

		Map<String, String> resultMap = new HashMap<String, String>();
		Integer roleId = null;
		// 角色信息的保存
		RoleEntity2 roleEntity2 = roleInfoVo.getRoleEntity();
		if (null != roleEntity2) {
			roleService2.save(roleEntity2);
			roleId = roleEntity2.getRoleId();
			if (null != roleId) {
				resultMap.put("roleId", roleId.toString());
			} else {
				resultMap.put("roleId", null);
				return resultMap;
			}
		}

		// 角色权限信息的保存
		List<RolePermissionEntity> rolePermissionEntityList = roleInfoVo.getRolePermissionEntityList();
		for (RolePermissionEntity rolePermissionEntity : rolePermissionEntityList) {
			// 设置角色ID
			rolePermissionEntity.setRoleId(roleId);
			rolePermissionEntity.setId(IdGen.uuid());
			rolePermissionService.save(rolePermissionEntity);
		}
		return resultMap;
	}

	/**
	 * 更新用户信息
	 * 
	 * @param userInfoVo
	 * @return 用户ID
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public Map<String, String> UpdateRoleInfo(Integer roleId, RoleInfoVo roleInfoVo) throws Exception {

		Map<String, String> resultMap = new HashMap<String, String>();
		// 用户私密信息的更新
		RoleEntity2 roleEntity2 = roleInfoVo.getRoleEntity();
		if (null != roleEntity2) {
			RoleEntity2 roleEntity2Db = roleService2.findById(roleId);
			if (null == roleEntity2Db) {
				resultMap.put("error", "根据" + roleId.toString() + "取得角色信息的数据为null");
				return resultMap;
			}
			RoleEntity2 RoleEntity2New = new RoleEntity2();
			BeanMapper.copy(roleEntity2Db, RoleEntity2New);
			// 将页面设置的不为null值覆盖DB中的值
			BeanMapper.copyIgnorNull(roleEntity2, RoleEntity2New);
			RoleEntity2New.setRoleId(roleId);
			roleService2.save(RoleEntity2New);
		}

		// 角色权限信息的更新
		// 根据角色ID删除既存权限的信息
		List<RolePermissionEntity> rolePermList = rolePermissionService.findByRoleId(roleId);
		if (null != rolePermList) {
			rolePermissionService.deleteByRoleId(roleId);
		}
		// 角色权限信息的保存
		List<RolePermissionEntity> rolePermissionEntityList = roleInfoVo.getRolePermissionEntityList();
		for (RolePermissionEntity rolePermissionEntity : rolePermissionEntityList) {
			// 设置角色ID
			rolePermissionEntity.setRoleId(roleId);
			rolePermissionEntity.setId(IdGen.uuid());
			rolePermissionService.save(rolePermissionEntity);
		}

		// 设定结果返回值
		resultMap.put("roleId", roleId.toString());

		return resultMap;
	}

}
