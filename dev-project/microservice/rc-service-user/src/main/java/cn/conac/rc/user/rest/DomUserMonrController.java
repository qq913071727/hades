package cn.conac.rc.user.rest;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.utils.IdGen;
import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.user.entity.DomUserMonEntity;
import cn.conac.rc.user.entity.UserEntity;
import cn.conac.rc.user.service.DomUserMonService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * DomUserMonrController类
 *
 * @author DomUserMonrController
 * @date 2017-05-08
 * @version 1.0
 */
@RestController
@RequestMapping(value="dum/")
public class DomUserMonrController {
	
	@Autowired
	DomUserMonService domUserMonService;
	
	@ApiOperation(value = "保存触发同步资源库表的信息", httpMethod = "POST", response = UserEntity.class, notes = "保存触发同步资源库表的信息")
	@RequestMapping(value = "/{userId}/{impCnt}/save", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> insertData(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "userId", required = true) @PathVariable("userId") Integer userId,
			@ApiParam(value = "impCnt", required = true) @PathVariable("impCnt") Integer impCnt) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		DomUserMonEntity domUserMonEntity = new DomUserMonEntity();
		domUserMonEntity.setId(IdGen.uuid());
		domUserMonEntity.setUserId(userId);
		domUserMonEntity.setImpCnt(impCnt);
		domUserMonEntity.setCreateDate(new Date());
		domUserMonService.save(domUserMonEntity);

		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(domUserMonEntity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
