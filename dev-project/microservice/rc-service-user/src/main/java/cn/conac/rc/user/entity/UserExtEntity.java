package cn.conac.rc.user.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * UserExtEntity类
 *
 * @author beanCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="ICP_USER_EXT")
public class UserExtEntity implements Serializable {

	private static final long serialVersionUID = 149182112377786258L;

	@Id
	@ApiModelProperty("用户ID")
	private Integer userId;

	@ApiModelProperty("姓名")
	private String realname;

	@ApiModelProperty("性别 (0：女 1：男)")
	private Integer gender;

	@ApiModelProperty("birthday")
	private Date birthday;

	@ApiModelProperty("intro")
	private String intro;

	@ApiModelProperty("comefrom")
	private String comefrom;

	@ApiModelProperty("QQ")
	private String qq;

	@ApiModelProperty("MSN")
	private String msn;

	@ApiModelProperty("phone")
	private String phone;

	@ApiModelProperty("手机号")
	private String mobile;

	@ApiModelProperty("userImg")
	private String userImg;

	@ApiModelProperty("userSignature")
	private String userSignature;

	@ApiModelProperty("部门库ID")
	private Integer orgId;

	@ApiModelProperty("用户类别")
	private String userType;

	@ApiModelProperty("身份证号")
	private String idNumber;
	
	@ApiModelProperty("姓名（编制域名用）")
	private String fullName;
	
	@ApiModelProperty("职务岗位")
	private String post;

	@ApiModelProperty("编制类型")
	private String formType;

	@ApiModelProperty("编制状态")
	private String formStatus;

	@ApiModelProperty("编制编码")
	private String formCode;

	@ApiModelProperty("监督电话")
	private String monitPhone;

	public void setUserId(Integer userId){
		this.userId=userId;
	}

	public Integer getUserId(){
		return userId;
	}

	public void setRealname(String realname){
		this.realname=realname;
	}

	public String getRealname(){
		return realname;
	}

	public void setGender(Integer gender){
		this.gender=gender;
	}

	public Integer getGender(){
		return gender;
	}

	public void setBirthday(Date birthday){
		this.birthday=birthday;
	}

	public Date getBirthday(){
		return birthday;
	}

	public void setIntro(String intro){
		this.intro=intro;
	}

	public String getIntro(){
		return intro;
	}

	public void setComefrom(String comefrom){
		this.comefrom=comefrom;
	}

	public String getComefrom(){
		return comefrom;
	}

	public void setQq(String qq){
		this.qq=qq;
	}

	public String getQq(){
		return qq;
	}

	public void setMsn(String msn){
		this.msn=msn;
	}

	public String getMsn(){
		return msn;
	}

	public void setPhone(String phone){
		this.phone=phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setMobile(String mobile){
		this.mobile=mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setUserImg(String userImg){
		this.userImg=userImg;
	}

	public String getUserImg(){
		return userImg;
	}

	public void setUserSignature(String userSignature){
		this.userSignature=userSignature;
	}

	public String getUserSignature(){
		return userSignature;
	}

	public void setOrgId(Integer orgId){
		this.orgId=orgId;
	}

	public Integer getOrgId(){
		return orgId;
	}

	public void setUserType(String userType){
		this.userType=userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setIdNumber(String idNumber){
		this.idNumber=idNumber;
	}

	public String getIdNumber(){
		return idNumber;
	}
	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}

	public String getFormStatus() {
		return formStatus;
	}

	public void setFormStatus(String formStatus) {
		this.formStatus = formStatus;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getMonitPhone() {
		return monitPhone;
	}

	public void setMonitPhone(String monitPhone) {
		this.monitPhone = monitPhone;
	}
}
