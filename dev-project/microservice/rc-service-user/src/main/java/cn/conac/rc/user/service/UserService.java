package cn.conac.rc.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.user.entity.UserEntity;
import cn.conac.rc.user.repository.UserRepository;

/**
 * UserService类
 *
 * @author serviceCreator
 * @date 2017-04-10
 * @version 1.0
 */
@Service
@Transactional
public class UserService extends GenericService<UserEntity, Integer> {

	@Autowired
	private UserRepository repository;

	/**
	 * 更新用户表的是否删除字段
	 * @param userId
	 * @return 更新数量
	 */
	public int  updateIsDeletedByUserId(List<Integer> userIdList) {
		int updateCount=-1;
		int sum = 0;
		for(int i=0; i < userIdList.size(); i++) {
			updateCount=-1;
			updateCount = repository.updateIsDeletedByUserId(userIdList.get(i));
			if(updateCount == -1) {
				return -1;
			}
			sum += updateCount;
		}
		return sum;
	}
	
	/**
	 * 更新用户表的是否启用/禁用字段
	 * @param userId
	 * @param idDisabled (1：禁用 0：启用)
	 * @return 更新数量
	 */
	public int  updateIsDisableByUserId(List<Integer> userIdList, int isDisabled) {
		int updateCount=-1;
		int sum = 0;
		for(int i=0; i < userIdList.size(); i++) {
			updateCount=-1;
			updateCount = repository.updateIsDisableByUserId(userIdList.get(i), isDisabled);
			if(updateCount == -1) {
				return -1;
			}
			sum += updateCount;
		}
		return sum;
	}

}
