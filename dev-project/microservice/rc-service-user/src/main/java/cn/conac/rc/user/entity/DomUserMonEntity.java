package cn.conac.rc.user.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DomUserMonEntity类
 *
 * @author beanCreator
 * @date 2017-05-08
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="ICP_DOM_USER_MON")
public class DomUserMonEntity implements Serializable {

	private static final long serialVersionUID = 1494224472344149071L;

	@Id
	@ApiModelProperty("id")
	private String id;
	
	@ApiModelProperty("userId")
	private Integer userId;

	@ApiModelProperty("impCnt")
	private Integer impCnt;
	
	@ApiModelProperty("createDate")
	private  Date createDate;
	
	public void setId(String id){
		this.id=id;
	}

	public String getId(){
		return id;
	}
	
	public void setUserId(Integer userId){
		this.userId=userId;
	}

	public Integer getUserId(){
		return userId;
	}

	public void setImpCnt(Integer impCnt){
		this.impCnt=impCnt;
	}

	public Integer getImpCnt(){
		return impCnt;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
