package cn.conac.rc.user.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.user.entity.UserAreaEntity;
import cn.conac.rc.user.service.UserCommService;
import cn.conac.rc.user.vo.UserInfoVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * UserAreaController类
 *
 * @author controllerCreator
 * @date 2017-04-10
 * @version 1.0
 */
@RestController
@RequestMapping(value="userComm/")
public class UserCommController {

	@Autowired
	UserCommService service;

	@ApiOperation(value = "保存用户信息", httpMethod = "POST", response = UserAreaEntity.class, notes = "保存用户信息")
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> saveUserInfo(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "保存对象", required = true) @RequestBody UserInfoVo userInfoVo) throws Exception {
		
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		
		// 保存用户信息
		Map<String, String> resultMap = service.SaveUserInfo(userInfoVo);
		if(null != resultMap.get("userId")) {
			// 结果集设定
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setResult(resultMap);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		} else {
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setResult(resultMap);
			result.setMsg(ResultPojo.MSG_FAILURE + "保存时生成的userId为null");
		}
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "更新用户信息", httpMethod = "POST", response = UserAreaEntity.class, notes = "根据UserId更新用户信息")
	@RequestMapping(value = "{userId}/update", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> updateUserInfo(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "userId", required = true) @PathVariable("userId") Integer userId,
			@ApiParam(value = "更新对象", required = true) @RequestBody UserInfoVo userInfoVo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		
		// 保存用户信息
		Map<String, String> resultMap = service.UpdateUserInfo(userId, userInfoVo);
		if(null != resultMap.get("error")) {
			// 结果集设定
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setResult(resultMap);
			result.setMsg(ResultPojo.MSG_FAILURE + resultMap.get("error"));
		} else {
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setResult(resultMap);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		}
		
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "批量保存用户信息", httpMethod = "POST", response = UserAreaEntity.class, notes = "批量保存用户信息")
	@RequestMapping(value = "batchSave", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> saveUserInfoList(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "保存对象", required = true) @RequestBody List<UserInfoVo> userInfoVoList) throws Exception {
		
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		
		// 保存用户信息
		Map<String, String> resultMap = service.SaveUserInfoList(userInfoVoList);
		if(null != resultMap.get("error")) {
			// 结果集设定
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setResult(resultMap);
			result.setMsg(ResultPojo.MSG_FAILURE +  resultMap.get("error"));
		} else {
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setResult(resultMap);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		}
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "批量保存或更新用户信息", httpMethod = "POST", response = UserAreaEntity.class, notes = "批量保存或更新用户信息")
	@RequestMapping(value = "batchSaveOrUpdate", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> saveOrUpdateUserInfoList(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "保存对象", required = true) @RequestBody List<UserInfoVo> userInfoVoList) throws Exception {
		
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		
		// 保存用户信息
		Map<String, String> resultMap = new HashMap<String,String>();
		try{
			resultMap = service.SaveOrUpdateUserInfoList(userInfoVoList);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		if(null != resultMap.get("errInfo")) {
			// 结果集设定
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setResult(resultMap);
			result.setMsg(ResultPojo.MSG_FAILURE +  resultMap.get("errInfo"));
		} else {
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setResult(resultMap);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		}
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
