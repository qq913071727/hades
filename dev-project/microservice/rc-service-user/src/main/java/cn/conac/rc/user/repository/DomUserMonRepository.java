package cn.conac.rc.user.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.user.entity.DomUserMonEntity;

/**
 * DomUserMonRepository类
 *
 * @author repositoryCreator
 * @date 2017-05-08
 * @version 1.0
 */
@Repository
public interface DomUserMonRepository extends GenericDao<DomUserMonEntity, Integer> {
	
}
