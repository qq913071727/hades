package cn.conac.rc.user.repository;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.user.entity.RcUserEntity;

/**
 * RcUserRepository类
 *
 * @author repositoryCreator
 * @date 2017-04-11
 * @version 1.0
 */
@Repository
public interface RcUserRepository extends GenericDao<RcUserEntity, Integer> {
	
	public RcUserEntity findByMobile(@Param("moblie") String moblie);
	
	public RcUserEntity findByIdNumber(@Param("idNumber") String idNumber);
	
	public List<RcUserEntity> findByIdNumberIn(@Param("idNumbers") List<String> idNumbers);
	
}
