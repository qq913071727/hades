package cn.conac.rc.user.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.user.entity.RcUserEntity;
import cn.conac.rc.user.service.RcUserService;
import cn.conac.rc.user.vo.RcUserVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * RcUserController类
 *
 * @author controllerCreator
 * @date 2017-04-11
 * @version 1.0
 */
@RestController
@RequestMapping(value="rcUser/")
public class RcUserController {

	@Autowired
	RcUserService service;

	@ApiOperation(value = "根据用户id获取用户信息详情", httpMethod = "GET", response = RcUserEntity.class, notes = "根据用户id获取用户信息详情")
	@RequestMapping(value = "{userId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findDetailById(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "userId", required = true) @PathVariable("userId") Integer userId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		RcUserEntity entity = service.findById(userId);
		if(StringUtils.isNotBlank(entity.getFormCode()) && StringUtils.isNotBlank(entity.getFormStatus()) && StringUtils.isNotBlank(entity.getFormType()) ) {
			if(StringUtils.isNotBlank(entity.getIdNumber()) && StringUtils.isNotBlank(entity.getFullName())) {
				String codeKeyStr = entity.getIdNumber() + entity.getFullName();
				entity.setQrCodeKey(DigestUtils.md5Hex(codeKeyStr));
			}
		}
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "根据手机号获取用户信息详情", httpMethod = "GET", response = RcUserEntity.class, notes = "根据手机号获取用户信息详情")
	@RequestMapping(value = "mobile/{mobile}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findDetailByMobile(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "mobile", required = true) @PathVariable("mobile") String mobile) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		RcUserEntity entity = service.findByMobile(mobile);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "根据身份证号获取用户信息详情", httpMethod = "GET", response = RcUserEntity.class, notes = "根据身份证号获取用户信息详情")
	@RequestMapping(value = "idnum/{idNumber}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findDetailByIdNumber(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "idNumber", required = true) @PathVariable("idNumber") String idNumber) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		RcUserEntity entity = service.findByIdNumber(idNumber);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "根据身份证号列表获取用户信息详情列表", httpMethod = "POST", response = RcUserEntity.class, notes = "根据身份证号列表获取用户信息详情列表")
	@RequestMapping(value = "idNumbers", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> findByIdNumbers(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "idNumbers", required = true) @RequestBody List<String> idNumbers) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<RcUserEntity> entity = service.findByIdNumberIn(idNumbers);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "用户信息列表", httpMethod = "POST", response = RcUserEntity.class, notes = "根据条件获得用户信息列表")
	@RequestMapping(value = "page", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> findPageByCoditon(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody RcUserVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<RcUserEntity> list = service.findPageByCoditon(vo);
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "用户信息列表", httpMethod = "POST", response = RcUserEntity.class, notes = "根据条件获得用户信息列表")
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> findListByCoditon(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody RcUserVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<RcUserEntity> list = service.findListByCoditon(vo);

		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);

		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
