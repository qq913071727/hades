package cn.conac.rc.user.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.user.entity.UserEntity;

/**
 * UserRepository类
 *
 * @author repositoryCreator
 * @date 2017-04-10
 * @version 1.0
 */
@Repository
public interface UserRepository extends GenericDao<UserEntity, Integer> {
	
	/**
	 * 更新用户表的是否启用/禁用字段
	 * @param userId
	 * @param idDisabled (1：禁用 0：启用)
	 * @return 更新数量
	 */
	@Query(value = "update icp_user iu set iu.is_disabled =:idDisabled where iu.user_id =:userId " , nativeQuery = true)
	@Modifying
	int updateIsDisableByUserId(@Param("userId") Integer userId, @Param("idDisabled") int idDisabled);
	
	
	/**
	 * 更新用户表的是否删除字段
	 * @param userId
	 * @return 更新数量
	 */
	@Query(value = "update icp_user iu set iu.is_deleted =1, iu.is_disabled=1 where iu.user_id =:userId " , nativeQuery = true)
	@Modifying
	int updateIsDeletedByUserId(@Param("userId") Integer userId);
}
