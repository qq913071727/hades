package cn.conac.rc.user.rest;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.user.entity.UserAreaEntity;
import cn.conac.rc.user.service.RoleCommService;
import cn.conac.rc.user.vo.RoleInfoVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * UserAreaController类
 *
 * @author controllerCreator
 * @date 2017-04-10
 * @version 1.0
 */
@RestController
@RequestMapping(value="roleComm/")
public class RoleCommController {

	@Autowired
	RoleCommService service;

	@ApiOperation(value = "保存角色信息", httpMethod = "POST", response = UserAreaEntity.class, notes = "保存角色信息")
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> saveUserInfo(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "保存对象", required = true) @RequestBody RoleInfoVo roleInfoVo) throws Exception {
		
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		
		// 保存角色信息
		Map<String, String> resultMap = service.SaveRoleInfo(roleInfoVo);
		if(null != resultMap.get("roleId")) {
			// 结果集设定
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setResult(resultMap);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		} else {
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setResult(resultMap);
			result.setMsg(ResultPojo.MSG_FAILURE + "保存时生成的roleId为null");
		}
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "更新角色信息", httpMethod = "POST", response = UserAreaEntity.class, notes = "根据UserId更新角色信息")
	@RequestMapping(value = "{roleId}/update", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> updateUserInfo(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "roleId", required = true) @PathVariable("roleId") Integer roleId,
			@ApiParam(value = "更新对象", required = true) @RequestBody RoleInfoVo roleInfoVo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		
		// 更新角色信息
		Map<String, String> resultMap = service.UpdateRoleInfo(roleId, roleInfoVo);
		if(null != resultMap.get("error")) {
			// 结果集设定
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setResult(resultMap);
			result.setMsg(ResultPojo.MSG_FAILURE + resultMap.get("error"));
		} else {
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setResult(resultMap);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		}
		
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
