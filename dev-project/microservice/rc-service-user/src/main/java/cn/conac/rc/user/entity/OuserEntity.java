package cn.conac.rc.user.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * UserEntity类
 *
 * @author beanCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="ICPO_USER")
public class OuserEntity implements Serializable {

	private static final long serialVersionUID = 1491821123812869982L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="mySeqGenerator")
	@SequenceGenerator(name = "mySeqGenerator", sequenceName = "S_ICPO_USER", allocationSize = 1)
	@ApiModelProperty("用户ID")
	private Integer userId;

	@ApiModelProperty("用户名")
	private String username;

	@ApiModelProperty("电子邮箱")
	private String email;

	@ApiModelProperty("密码")
	private String password;

	@ApiModelProperty("注册时间")
	private Date registerTime;

	@ApiModelProperty("注册的IP")
	private String registerIp;

	@ApiModelProperty("最后一次登录时间")
	private Date lastLoginTime;

	@ApiModelProperty("最后一次登录IP")
	private String lastLoginIp;

	@ApiModelProperty("登录次数")
	private Integer loginCount;

	@ApiModelProperty("重置KEY")
	private String resetKey;

	@ApiModelProperty("重置路径")
	private String resetPwd;

	@ApiModelProperty("发生错误时间")
	private Date errorTime;

	@ApiModelProperty("发生错误次数")
	private Integer errorCount;

	@ApiModelProperty("发生错误IP")
	private String errorIp;

	@ApiModelProperty("激活")
	private Integer activation;

	@ApiModelProperty("激活码")
	private String activationCode;

	public void setUserId(Integer userId){
		this.userId=userId;
	}

	public Integer getUserId(){
		return userId;
	}

	public void setUsername(String username){
		this.username=username;
	}

	public String getUsername(){
		return username;
	}

	public void setEmail(String email){
		this.email=email;
	}

	public String getEmail(){
		return email;
	}

	public void setPassword(String password){
		this.password=password;
	}

	public String getPassword(){
		return password;
	}

	public void setRegisterTime(Date registerTime){
		this.registerTime=registerTime;
	}

	public Date getRegisterTime(){
		return registerTime;
	}

	public void setRegisterIp(String registerIp){
		this.registerIp=registerIp;
	}

	public String getRegisterIp(){
		return registerIp;
	}

	public void setLastLoginTime(Date lastLoginTime){
		this.lastLoginTime=lastLoginTime;
	}

	public Date getLastLoginTime(){
		return lastLoginTime;
	}

	public void setLastLoginIp(String lastLoginIp){
		this.lastLoginIp=lastLoginIp;
	}

	public String getLastLoginIp(){
		return lastLoginIp;
	}

	public void setLoginCount(Integer loginCount){
		this.loginCount=loginCount;
	}

	public Integer getLoginCount(){
		return loginCount;
	}

	public void setResetKey(String resetKey){
		this.resetKey=resetKey;
	}

	public String getResetKey(){
		return resetKey;
	}

	public void setResetPwd(String resetPwd){
		this.resetPwd=resetPwd;
	}

	public String getResetPwd(){
		return resetPwd;
	}

	public void setErrorTime(Date errorTime){
		this.errorTime=errorTime;
	}

	public Date getErrorTime(){
		return errorTime;
	}

	public void setErrorCount(Integer errorCount){
		this.errorCount=errorCount;
	}

	public Integer getErrorCount(){
		return errorCount;
	}

	public void setErrorIp(String errorIp){
		this.errorIp=errorIp;
	}

	public String getErrorIp(){
		return errorIp;
	}

	public void setActivation(Integer activation){
		this.activation=activation;
	}

	public Integer getActivation(){
		return activation;
	}

	public void setActivationCode(String activationCode){
		this.activationCode=activationCode;
	}

	public String getActivationCode(){
		return activationCode;
	}

}
