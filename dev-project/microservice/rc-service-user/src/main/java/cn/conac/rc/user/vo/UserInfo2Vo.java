package cn.conac.rc.user.vo;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;

/**
 * UserInfo2Vo类
 *
 * @author voCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
public class UserInfo2Vo  implements Serializable {

	private static final long serialVersionUID = 4815754215532667625L;

	private List<Integer> userIdList;
	
	private int isDisabled;

	public List<Integer> getUserIdList() {
		return userIdList;
	}

	public void setUserIdList(List<Integer> userIdList) {
		this.userIdList = userIdList;
	}

	public int getIsDisabled() {
		return isDisabled;
	}

	public void setIsDisabled(int isDisabled) {
		this.isDisabled = isDisabled;
	}
	
}
