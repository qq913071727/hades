package cn.conac.rc.user.contsant;

public class Contsants {
	
	public static final  String  SEMICOLON_FLAG = ";";
	
	/**
	 * 机构域名或者编制域名默认生成的用户名的前缀 
	 */
	public static final  String  DEFAULT_USER_NAME_PREFIX = "rc_";
	
	public static final  String  DOUBLE_BAR_STR = "||";

}
