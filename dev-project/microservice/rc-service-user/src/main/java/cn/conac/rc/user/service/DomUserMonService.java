package cn.conac.rc.user.service;

import org.springframework.stereotype.Service;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.user.entity.DomUserMonEntity;

/**
 * DomUserMonService类
 *
 * @author serviceCreator
 * @date 2017-05-08
 * @version 1.0
 */
@Service
public class DomUserMonService extends GenericService<DomUserMonEntity, Integer> {

}
