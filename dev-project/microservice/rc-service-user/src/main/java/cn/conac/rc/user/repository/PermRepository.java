package cn.conac.rc.user.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.user.entity.PermEntity;

/**
 * PermRepository类
 *
 * @author repositoryCreator
 * @date 2017-04-10
 * @version 1.0
 */
@Repository
public interface PermRepository extends GenericDao<PermEntity, Integer> {
}
