package cn.conac.rc.user.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * RoleEntity类
 *
 * @author beanCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
public class RolePermVO implements Serializable {

	private static final long serialVersionUID = -559087498225265072L;

	@ApiModelProperty("roleId")
	private Integer roleId;

	@ApiModelProperty("siteId")
	private Integer siteId;

	@ApiModelProperty("角色名字")
	private String roleName;

	@ApiModelProperty("顺序号")
	private Integer priority;

	@ApiModelProperty("拥有所用权限")
	private String isSuper;

	@ApiModelProperty("角色类型（1：全站 2： 本站）")
	private String roleType;
	
	@ApiModelProperty("权限节点")
	private PermNodeVo permNodeVo;
	
	public void setRoleId(Integer roleId){
		this.roleId=roleId;
	}

	public Integer getRoleId(){
		return roleId;
	}

	public void setSiteId(Integer siteId){
		this.siteId=siteId;
	}

	public Integer getSiteId(){
		return siteId;
	}

	public void setRoleName(String roleName){
		this.roleName=roleName;
	}

	public String getRoleName(){
		return roleName;
	}

	public void setPriority(Integer priority){
		this.priority=priority;
	}

	public Integer getPriority(){
		return priority;
	}

	public void setIsSuper(String isSuper){
		this.isSuper=isSuper;
	}

	public String getIsSuper(){
		return isSuper;
	}

	public void setRoleType(String roleType){
		this.roleType=roleType;
	}

	public String getRoleType(){
		return roleType;
	}

	public PermNodeVo getPermNodeVo() {
		return permNodeVo;
	}

	public void setPermNodeVo(PermNodeVo permNodeVo) {
		this.permNodeVo = permNodeVo;
	}
}
