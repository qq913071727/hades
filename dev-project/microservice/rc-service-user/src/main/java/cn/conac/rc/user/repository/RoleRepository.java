package cn.conac.rc.user.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.user.entity.RoleEntity;

/**
 * RoleRepository类
 *
 * @author repositoryCreator
 * @date 2017-04-10
 * @version 1.0
 */
@Repository
public interface RoleRepository extends GenericDao<RoleEntity, Integer> {

	/**
	 * 根据roleId删除角色信息
	 * @param roleId
	 * @return 删除数量
	 */
	@Query(value = "delete from icp_role ir where ir.role_id= :roleId", nativeQuery = true)
	@Modifying
	int deleteByRoleId(@Param("roleId") Integer roleId);
}
