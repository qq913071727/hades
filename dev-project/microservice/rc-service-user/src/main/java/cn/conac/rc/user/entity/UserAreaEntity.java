package cn.conac.rc.user.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * UserAreaEntity类
 *
 * @author beanCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="ICP_USER_AREA")
public class UserAreaEntity implements Serializable {

	private static final long serialVersionUID = 1491821123837926287L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="mySeqGenerator")
	@SequenceGenerator(name = "mySeqGenerator", sequenceName = "S_ICP_USER_AREA", allocationSize = 1)
	@ApiModelProperty("id")
	private Integer id;
	
	@ApiModelProperty("用户ID")
	private Integer userId;

	@ApiModelProperty("区域ID")
	private String areaId;

	@ApiModelProperty("区域编码")
	private String areaCode;

	public void setUserId(Integer userId){
		this.userId=userId;
	}

	public Integer getUserId(){
		return userId;
	}

	public void setAreaId(String areaId){
		this.areaId=areaId;
	}

	public String getAreaId(){
		return areaId;
	}

	public void setAreaCode(String areaCode){
		this.areaCode=areaCode;
	}

	public String getAreaCode(){
		return areaCode;
	}

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

}
