package cn.conac.rc.user.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.user.entity.RcUserEntity;
import cn.conac.rc.user.entity.RoleEntity;
import cn.conac.rc.user.repository.RcUserRepository;
import cn.conac.rc.user.vo.RcUserVo;

/**
 * RcUserService类
 *
 * @author serviceCreator
 * @date 2017-04-11
 * @version 1.0
 */
@Service
public class RcUserService extends GenericService<RcUserEntity, Integer> {

	@Autowired
	private RcUserRepository repository;
	
	/**
	 * 根据手机号查询用户信息
	 * @param mobile 手机号
	 * @return RcUserEntity
	 * @throws Exception
	 */
	public RcUserEntity findByMobile(String  mobile) throws Exception {
		return repository.findByMobile(mobile);
	}
	
	/**
	 * 根据身份证号查询用户信息
	 * @param idNumber 身份证号
	 * @return RcUserEntity
	 * @throws Exception
	 */
	public RcUserEntity findByIdNumber(String  idNumber) throws Exception {
		return repository.findByIdNumber(idNumber);
	}
	
	/**
	 * 根据身份证号查询用户信息
	 * @param idNumber 身份证号
	 * @return RcUserEntity
	 * @throws Exception
	 */
	public List<RcUserEntity> findByIdNumberIn(List<String> idNumbers) throws Exception {
		return repository.findByIdNumberIn(idNumbers);
	}
	
	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<RcUserEntity> findPageByCoditon(RcUserVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<RcUserEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	/**
	 * 动态查询，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public List<RcUserEntity> findListByCoditon(RcUserVo vo) throws Exception {
		try {
//			Sort sort = new Sort(Direction.ASC, "id");
			Criteria<RcUserEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<RcUserEntity> createCriteria(RcUserVo param) {
		Criteria<RcUserEntity> dc = new Criteria<RcUserEntity>();
		// 区域ID
		if(RoleEntity.ROLE_CONAC_QDGLY.equals(param.getCurrRoleId())) {
			if (StringUtils.isNotBlank(param.getAreaId())) {
				dc.add(Restrictions.ne("areaId", param.getAreaId(), true));
			}
			// 渠道管理员过滤掉sys管理员账号信息
			dc.add(Restrictions.ne("roleId", 1, true));
		} else  {
			if (StringUtils.isNotBlank(param.getAreaId())) {
				dc.add(Restrictions.eq("areaId", param.getAreaId(), true));
			}
		}
		
		// 域名名称
		if (StringUtils.isNotBlank(param.getOrgDomainName())) {
			dc.add(Restrictions.like("orgDomainName", param.getOrgDomainName(), true));
		}
		
		// 手机号
		if (StringUtils.isNotBlank(param.getMobile())) {
			dc.add(Restrictions.eq("mobile", param.getMobile(), true));
		}
		
		// 身份证号
		if (StringUtils.isNotBlank(param.getIdNumber())) {
			dc.add(Restrictions.eq("idNumber", param.getIdNumber(), true));
		}
		
		// 区分机构域名和编制域名
		if (null != param.getIsAdmin()) {
			dc.add(Restrictions.eq("isAdmin", param.getIsAdmin(), true));
		}
		
		// 部门名称
		if (StringUtils.isNotBlank(param.getOrgName())) {
			dc.add(Restrictions.like("orgName", param.getOrgName(), true));
		}
		
		// 真实姓名
		if (StringUtils.isNotBlank(param.getRealName())) {
			dc.add(Restrictions.like("realName", param.getRealName(), true));
		}
		
		// 编制域名用姓名
		if (StringUtils.isNotBlank(param.getFullName())) {
			dc.add(Restrictions.like("fullName", param.getFullName(), true));
		}
		
		// SI唯一代码
		if (StringUtils.isNotBlank(param.getSiCode())) {
			dc.add(Restrictions.eq("siCode", param.getSiCode(), true));
		}
		// 部门ID
		if (param.getOrgId() != null) {
			dc.add(Restrictions.eq("orgId", param.getOrgId(), true));
		}
		// 登录用户名
		if(null != param.getEqFlag() &&  param.getEqFlag()==1) {
			if (StringUtils.isNotBlank(param.getLoginName())) {
				dc.add(Restrictions.eq("loginName", param.getLoginName(), true));
			}
		} else {
			if (StringUtils.isNotBlank(param.getLoginName())) {
				dc.add(Restrictions.like("loginName", param.getLoginName(), true));
			}
		}
		
		// 角色ID
		if (null != param.getRoleId()) {
			dc.add(Restrictions.eq("roleId", param.getRoleId(), true));
		}
		
		// userId(列表中不包含自身)
		if (null != param.getId()) {
			dc.add(Restrictions.ne("id", param.getId(), true));
		}
		
		// 是否上传头像标示
		if (null != param.getImgFlag()) {
			dc.add(Restrictions.eq("imgFlag", param.getImgFlag(), true));
		}
		
		return dc;
	}

}
