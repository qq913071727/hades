package cn.conac.rc.user.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.user.entity.PermEntity;
import cn.conac.rc.user.repository.PermRepository;
import cn.conac.rc.user.vo.PermNodeVo;
import cn.conac.rc.user.vo.PermVo;

/**
 * PermService类
 *
 * @author serviceCreator
 * @date 2017-04-10
 * @version 1.0
 */
@Service
public class PermService extends GenericService<PermEntity, Integer> {

	@Autowired
	private PermRepository repository;

	
	/**
	 * 动态查询，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public List<Map<String,Object>> findPermStrByCondition(PermVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");
			Criteria<PermEntity> dc = this.createCriteria(vo);
			List<PermEntity>  permList = repository.findAll(dc, sort);
			Map<String,Object> permMap = null;
			List<Map<String,Object>> permMapList = new ArrayList<Map<String,Object>>();
 			for(int i=0;i<permList.size();i++) {
 				permMap = new HashMap<String,Object>();
 				permMap.put("id", permList.get(i).getId());
 				permMap.put("pId", permList.get(i).getPid());
 				permMap.put("name", permList.get(i).getName());
 				if("True".equals(permList.get(i).getOpen())) {
 					permMap.put("open", Boolean.TRUE);
 				}
 				if(null != permList.get(i).getPerm()) {
 					permMap.put("perm", permList.get(i).getPerm());
 				}
 				permMapList.add(permMap);
			}
			return permMapList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public List<PermEntity> findByCondition(PermVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "name");
			Criteria<PermEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, sort);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<PermEntity> createCriteria(PermVo param) {
		Criteria<PermEntity> dc = new Criteria<PermEntity>();
		
		// 是否显示标识
		if (StringUtils.isNotBlank(param.getDisplay())) {
			dc.add(Restrictions.eq("display", param.getDisplay(), true));
		}
		return dc;
	}
	
	/**
	 * 根据条件查询权限信息(并按照权限名称升序排序)
	 * @param PermVo
	 * @return  权限树的root节点
	 * @throws Exception 
	 */
	public PermNodeVo findPermTreeByCodition(PermVo vo) throws Exception {
		
		Map<Integer, PermNodeVo> permNodeMap = new HashMap<Integer, PermNodeVo>();
		PermNodeVo permNodeVo = null;
		PermNodeVo rootPermNodeVo =new PermNodeVo();
		Integer parentId = null;
		List<PermEntity> permList  = this.findByCondition(vo);
		
		for(PermEntity permEntity: permList) {
			permNodeVo= new PermNodeVo(permEntity);
			permNodeMap.put(permNodeVo.getId(), permNodeVo);
		}
		
		for (PermNodeVo  permNode : permNodeMap.values()) {
			parentId = permNode.getPid();
			if(parentId.intValue() == 0) {
				rootPermNodeVo = permNode;
			} else {
				if (null != parentId && permNodeMap.containsKey(parentId)) {
					permNodeMap.get(parentId).addChild(permNode);
				}
			}
		}
		
		return rootPermNodeVo;
	}
	

}
