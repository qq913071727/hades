package cn.conac.rc.user.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * RolePermissionEntity类
 *
 * @author beanCreator
 * @date 2017-04-14
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="ICP_ROLE_PERMISSION")
public class RolePermissionEntity implements Serializable {

	private static final long serialVersionUID = 1492169959321496528L;
	
	@Id
	@ApiModelProperty("ID")
	private String  id;
	
	@ApiModelProperty("uri")
	private String uri;
	
	@ApiModelProperty("角色ID")
	private Integer roleId;
    
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUri(String uri){
		this.uri=uri;
	}

	public String getUri(){
		return uri;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
}
