package cn.conac.rc.user.vo;

import javax.persistence.Transient;

import cn.conac.rc.user.entity.PermEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * PermVo类
 *
 * @author voCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
public class PermVo extends PermEntity {

	private static final long serialVersionUID = 1491821123865771513L;

	@Transient
	@ApiModelProperty("当前分页")
	private Integer page;

	@Transient
	@ApiModelProperty("每页个数")
	private Integer size;

	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}

}
