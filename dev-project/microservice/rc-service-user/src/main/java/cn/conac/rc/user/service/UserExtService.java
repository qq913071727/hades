package cn.conac.rc.user.service;

import org.springframework.stereotype.Service;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.user.entity.UserExtEntity;

/**
 * UserExtService类
 *
 * @author serviceCreator
 * @date 2017-04-10
 * @version 1.0
 */
@Service
public class UserExtService extends GenericService<UserExtEntity, Integer> {

}
