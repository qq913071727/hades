package cn.conac.rc.user.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.conac.rc.user.entity.PermEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * PermVo类
 *
 * @author voCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
public class PermNodeVo implements Serializable  {

	private static final long serialVersionUID = -9084770618292210167L;

	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("pid")
	private Integer pid;

	@ApiModelProperty("权限名字")
	private String name;

	@ApiModelProperty("open")
	private String open;

	@ApiModelProperty("perm")
	private String perm;

	@ApiModelProperty("display")
	private String display;
	
	@ApiModelProperty("display")
	private int isCheck=0;

	@ApiModelProperty("权限子节点")
	private List<PermNodeVo> children = new ArrayList<PermNodeVo>();
	
	public PermNodeVo(){
		
	}

	public PermNodeVo(PermEntity permEntity) {
		this.id = permEntity.getId();
		this.pid = permEntity.getPid();
		this.name = permEntity.getName();
		this.open = permEntity.getOpen();
		this.perm = permEntity.getPerm();
		this.display = permEntity.getDisplay();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}

	public String getPerm() {
		return perm;
	}

	public void setPerm(String perm) {
		this.perm = perm;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public int getIsCheck() {
		return isCheck;
	}

	public void setIsCheck(int isCheck) {
		this.isCheck = isCheck;
	}

	public void addChild(PermNodeVo permNodeVo) {
		children.add(permNodeVo);
	}
	
	public List<PermNodeVo> getChildren() {
		return children;
	}

	public void setChildren(List<PermNodeVo> children) {
		this.children = children;
	}
}
