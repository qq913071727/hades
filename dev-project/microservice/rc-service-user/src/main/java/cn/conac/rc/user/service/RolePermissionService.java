package cn.conac.rc.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.user.entity.RolePermissionEntity;
import cn.conac.rc.user.repository.RolePermissionRepository;

/**
 * RolePermissionService类
 *
 * @author serviceCreator
 * @date 2017-04-10
 * @version 1.0
 */
@Service
public class RolePermissionService extends GenericService<RolePermissionEntity, String> {

	@Autowired
	private RolePermissionRepository repository;
	
	/**
	 * 根据roleId删除全部权限信息
	 * @param roleId
	 * @return 删除数量
	 */
	public int deleteByRoleId(Integer roleId)  {			
			return repository.deleteByRoleId(roleId);
	}
	

	/**
	 * 根据roleId删除全部权限信息
	 * @param roleId
	 * @return 删除数量
	 */
	public List<RolePermissionEntity> findByRoleId(Integer roleId)  {
			return repository.findByRoleId(roleId);
	}
	
}
