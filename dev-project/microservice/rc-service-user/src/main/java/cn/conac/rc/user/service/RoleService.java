package cn.conac.rc.user.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.user.entity.PermEntity;
import cn.conac.rc.user.entity.RoleEntity;
import cn.conac.rc.user.entity.RolePermissionEntity;
import cn.conac.rc.user.repository.RoleRepository;
import cn.conac.rc.user.vo.PermNodeVo;
import cn.conac.rc.user.vo.PermVo;
import cn.conac.rc.user.vo.RolePermVO;

/**
 * RoleService类
 *
 * @author serviceCreator
 * @date 2017-04-10
 * @version 1.0
 */
@Service
@Transactional
public class RoleService extends GenericService<RoleEntity, Integer> {

	@Autowired
	private RoleRepository repository;
	
	@Autowired
	private PermService permService;
	

	/**
	 *  取得所有的角色信息并按照priority字段升序排序显示
	 * @return 角色列表
	 */
	public List<RoleEntity> findRoleList()  {
			Sort sort = new Sort(Direction.ASC, "priority");
			return repository.findAll(sort);
	}
	
	/**
	 * 删除角色表的是否删除字段
	 * @param userId
	 * @return 删除数量
	 */
	public int  deleteByRoleIds(List<Integer> roleIds) {
		int deleteCount=-1;
		int sum = 0;
		for(int i=0; i < roleIds.size(); i++) {
			deleteCount=-1;
			deleteCount = repository.deleteByRoleId(roleIds.get(i));
			if(deleteCount == -1) {
				return -1;
			}
			sum += deleteCount;
		}
		return sum;
	}
	
	
	/**
	 * 根据角色ID查询权限树信息(并按照权限名称升序排序)
	 * 当前角色存在该权限场合，将权限节点的isCheck设置为1
	 * @param roleId
	 * @param PermVo
	 * @return RolePermVO
	 * @throws Exception 
	 */
	public RolePermVO findPermTreeByRoleId(Integer roleId, PermVo vo) throws Exception {
		
		RoleEntity roleEntity = this.findById(roleId);
		List<RolePermissionEntity> rolePermissionList = roleEntity.getRolePermissionList();
		
		Map<Integer, PermNodeVo> permNodeMap = new HashMap<Integer, PermNodeVo>();
		PermNodeVo permNodeVo = null;
		PermNodeVo rootPermNodeVo =new PermNodeVo();
		Integer parentId = null;
		List<PermEntity> permList  = permService.findByCondition(vo);
		
		for(PermEntity permEntity: permList) {
			permNodeVo= new PermNodeVo(permEntity);
			for(RolePermissionEntity rolePermissionEntity: rolePermissionList) {
				if(null != permEntity.getPerm() && 
						permEntity.getPerm().indexOf(rolePermissionEntity.getUri()) != -1){
					permNodeVo.setIsCheck(1);
					break;
				}
			}
			permNodeMap.put(permNodeVo.getId(), permNodeVo);
		}
		
		for (PermNodeVo  permNode : permNodeMap.values()) {
			parentId = permNode.getPid();
			if(parentId.intValue() == 0) {
				rootPermNodeVo = permNode;
			} else {
				if (null != parentId && permNodeMap.containsKey(parentId)) {
					permNodeMap.get(parentId).addChild(permNode);
				}
			}
		}
		
		RolePermVO rolePermVO = new RolePermVO();
		rolePermVO.setPermNodeVo(rootPermNodeVo);
		rolePermVO.setIsSuper(roleEntity.getIsSuper());
		rolePermVO.setPriority(roleEntity.getPriority());
		rolePermVO.setRoleId(roleEntity.getRoleId());
		rolePermVO.setRoleName(roleEntity.getRoleName());
		rolePermVO.setRoleType(roleEntity.getRoleType());
		rolePermVO.setSiteId(roleEntity.getSiteId());
		
		return rolePermVO;
	}
	
}
