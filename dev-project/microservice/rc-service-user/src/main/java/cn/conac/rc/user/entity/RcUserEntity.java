package cn.conac.rc.user.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * RcUserEntity类
 *
 * @author beanCreator
 * @date 2017-04-11
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="RC_USER")
public class RcUserEntity implements Serializable {

	private static final long serialVersionUID = 1491881200086565606L;

	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("siCode")
	private String siCode;

	@ApiModelProperty("orgName")
	private String orgName;

	@ApiModelProperty("orgDomainName")
	private String orgDomainName;

	@ApiModelProperty("email")
	private String email;

	@ApiModelProperty("loginName")
	private String loginName;

	@ApiModelProperty("password")
	private String password;

	@ApiModelProperty("nickName")
	private String nickName;

	@ApiModelProperty("deptName")
	private String deptName;

	@ApiModelProperty("mobile")
	private String mobile;

	@ApiModelProperty("realName")
	private String realName;
	
	@ApiModelProperty("姓名（编制域名用）")
	private String fullName;

	@ApiModelProperty("idNumber")
	private String idNumber;

	@ApiModelProperty("areaId")
	private String areaId;
	
	@ApiModelProperty("areaCode")
	private String areaCode;

	@ApiModelProperty("imgUrl")
	private String imgUrl;

	@ApiModelProperty("orgId")
	private Integer orgId;

	@ApiModelProperty("userType")
	private String userType;

	@ApiModelProperty("lastLoginTime")
	private Date lastLoginTime;

	@ApiModelProperty("lastLoginIp")
	private String lastLoginIp;

	@ApiModelProperty("loginCount")
	private Integer loginCount;

	@ApiModelProperty("areaName")
	private String areaName;

	@ApiModelProperty("isDisabled")
	private Integer isDisabled;
	
	@ApiModelProperty("对应关联角色表")
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name = "IcpUserRole", joinColumns = {@JoinColumn(name = "userId")}, inverseJoinColumns ={@JoinColumn(name = "roleId") })
	private List<RoleEntity2> roleList;
	
	@ApiModelProperty("区分机构域名和编制域名（1：机构域名  0：编制域名）PS：实际对应的字段是IS_ORG_DOMAIN（为方便不该前台进行映射）")
	private Integer isAdmin;
	
	@ApiModelProperty("角色Id")
	private Integer roleId;
	
	@ApiModelProperty("虚拟绝度路径的前缀")
	private String absolutePath;
	
	@ApiModelProperty("性别 (0：女 1：男)")
	private Integer gender;

	@ApiModelProperty("职务岗位")
	private String post;

	@ApiModelProperty("编制类型")
	private String formType;

	@ApiModelProperty("编制状态")
	private String formStatus;

	@ApiModelProperty("编制编码")
	private String formCode;

	@ApiModelProperty("监督电话")
	private String monitPhone;
		
	@Transient
	@ApiModelProperty("取得编制域名二维码的Key")
	private String qrCodeKey;
	
	@ApiModelProperty("是否上传头像标示(1: 已上传 0:未上传)")
	private String imgFlag;
	
	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setSiCode(String siCode){
		this.siCode=siCode;
	}

	public String getSiCode(){
		return siCode;
	}

	public void setOrgName(String orgName){
		this.orgName=orgName;
	}

	public String getOrgName(){
		return orgName;
	}

	public void setOrgDomainName(String orgDomainName){
		this.orgDomainName=orgDomainName;
	}

	public String getOrgDomainName(){
		return orgDomainName;
	}

	public void setEmail(String email){
		this.email=email;
	}

	public String getEmail(){
		return email;
	}

	public void setLoginName(String loginName){
		this.loginName=loginName;
	}

	public String getLoginName(){
		return loginName;
	}

	public void setNickName(String nickName){
		this.nickName=nickName;
	}

	public String getNickName(){
		return nickName;
	}

	public void setDeptName(String deptName){
		this.deptName=deptName;
	}

	public String getDeptName(){
		return deptName;
	}

	public void setMobile(String mobile){
		this.mobile=mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setRealName(String realName){
		this.realName=realName;
	}

	public String getRealName(){
		return realName;
	}
	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public void setIdNumber(String idNumber){
		this.idNumber=idNumber;
	}

	public String getIdNumber(){
		return idNumber;
	}

	public void setAreaId(String areaId){
		this.areaId=areaId;
	}

	public String getAreaId(){
		return areaId;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public void setImgUrl(String imgUrl){
		this.imgUrl=imgUrl;
	}

	public String getImgUrl(){
		return imgUrl;
	}

	public void setOrgId(Integer orgId){
		this.orgId=orgId;
	}

	public Integer getOrgId(){
		return orgId;
	}

	public void setUserType(String userType){
		this.userType=userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setLastLoginTime(Date lastLoginTime){
		this.lastLoginTime=lastLoginTime;
	}

	public Date getLastLoginTime(){
		return lastLoginTime;
	}

	public void setLastLoginIp(String lastLoginIp){
		this.lastLoginIp=lastLoginIp;
	}

	public String getLastLoginIp(){
		return lastLoginIp;
	}

	public void setLoginCount(Integer loginCount){
		this.loginCount=loginCount;
	}

	public Integer getLoginCount(){
		return loginCount;
	}

	public void setAreaName(String areaName){
		this.areaName=areaName;
	}

	public String getAreaName(){
		return areaName;
	}

	public void setIsDisabled(Integer isDisabled){
		this.isDisabled=isDisabled;
	}

	public Integer getIsDisabled(){
		return isDisabled;
	}

	public List<RoleEntity2> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<RoleEntity2> roleList) {
		this.roleList = roleList;
	}

	public Integer getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Integer isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}

	public String getFormStatus() {
		return formStatus;
	}

	public void setFormStatus(String formStatus) {
		this.formStatus = formStatus;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getMonitPhone() {
		return monitPhone;
	}

	public void setMonitPhone(String monitPhone) {
		this.monitPhone = monitPhone;
	}

	public String getQrCodeKey() {
		return qrCodeKey;
	}

	public void setQrCodeKey(String qrCodeKey) {
		this.qrCodeKey = qrCodeKey;
	}

	public String getImgFlag() {
		return imgFlag;
	}

	public void setImgFlag(String imgFlag) {
		this.imgFlag = imgFlag;
	}
}
