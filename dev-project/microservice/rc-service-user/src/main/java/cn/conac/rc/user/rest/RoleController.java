package cn.conac.rc.user.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.user.entity.RoleEntity;
import cn.conac.rc.user.entity.RolePermissionEntity;
import cn.conac.rc.user.service.RoleService;
import cn.conac.rc.user.vo.PermVo;
import cn.conac.rc.user.vo.RolePermVO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * RoleController类
 *
 * @author controllerCreator
 * @date 2017-04-10
 * @version 1.0
 */
@RestController
@RequestMapping(value="role/")
public class RoleController {

	@Autowired
	RoleService service;

	@ApiOperation(value = "获取角色详情", httpMethod = "GET", response = RoleEntity.class, notes = "根据角色ID获取角色详情")
	@RequestMapping(value = "{roleId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByRoleId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "roleId", required = true) @PathVariable("roleId") Integer roleId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		RoleEntity entity = service.findById(roleId);
		
		Map<String,Object> roleMap = new HashMap<String,Object>();
		roleMap.put("roleId", entity.getRoleId());
		roleMap.put("siteId", entity.getSiteId());
		roleMap.put("roleName", entity.getRoleName());
		roleMap.put("priority", entity.getPriority());
		roleMap.put("isSuper", entity.getIsSuper());
		roleMap.put("roleType", entity.getRoleType());
		
		List<RolePermissionEntity> rolePermissionList = entity.getRolePermissionList();
	    StringBuffer sbPerms = new StringBuffer();
		for(RolePermissionEntity rolePermissionEntity : rolePermissionList) {
			sbPerms.append(rolePermissionEntity.getUri());
			sbPerms.append(",");
		}
		String perms = sbPerms.toString();
		if(StringUtils.isNotBlank(perms)) {
			roleMap.put("perms", perms.substring(0, perms.length()-1));
		} else {
			roleMap.put("perms", perms);
		}
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(roleMap);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "获取角色及其对应的权限树信息", httpMethod = "POST", response = RoleEntity.class, notes = "获取角色及其对应的权限树信息")
	@RequestMapping(value = "{roleId}/perm/tree", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> findPermTreeByRoleId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "roleId", required = true) @PathVariable("roleId") Integer roleId, 
			@ApiParam(value = "查询条件对象", required = true) @RequestBody PermVo permVo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		RolePermVO rolePermVO = service.findPermTreeByRoleId(roleId, permVo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(rolePermVO);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "获取角色列表信息", httpMethod = "GET", response = RoleEntity.class, notes = "获取角色列表信息")
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findAll(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<RoleEntity> roleList = service.findRoleList();
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(roleList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "物理删除角色信息", httpMethod = "DELETE", response = RoleEntity.class, notes = "根据roleId物理删除角色信息")
	@RequestMapping(value = "delete", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> deleteByRoleId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody List<Integer> roleIdList) {
		
			// 声明返回结果集
			ResultPojo result = new ResultPojo();
			// service调用
			int deleteCountSum =-1;
			Map<String, Object> mapResult = new HashMap<String, Object>();
			if(null ==roleIdList || roleIdList.isEmpty() ) {
				mapResult.put("error", "选择的角色ID为空");
				result.setCode(ResultPojo.CODE_FAILURE);
				result.setResult(mapResult);
				result.setMsg(ResultPojo.MSG_FAILURE);
				return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
			}
			deleteCountSum = service.deleteByRoleIds(roleIdList);
			mapResult.put("roleIds", roleIdList);
			mapResult.put("roleIdCount", roleIdList.size());
			mapResult.put("roleDeleteSum", deleteCountSum);
			if(deleteCountSum > 0) {
				result.setCode(ResultPojo.CODE_SUCCESS);
				result.setResult(mapResult);
				result.setMsg(ResultPojo.MSG_SUCCESS);
			} else {
				result.setCode(ResultPojo.CODE_FAILURE);
				result.setResult(mapResult);
				result.setMsg(ResultPojo.MSG_FAILURE);
			}
			return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
