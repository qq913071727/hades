package cn.conac.rc.user.vo;

import java.io.Serializable;

import cn.conac.rc.user.entity.OuserEntity;
import cn.conac.rc.user.entity.UserAreaEntity;
import cn.conac.rc.user.entity.UserEntity;
import cn.conac.rc.user.entity.UserExtEntity;
import io.swagger.annotations.ApiModel;

/**
 * UserInfoVo类
 *
 * @author voCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
public class UserInfoVo  implements Serializable {

	private static final long serialVersionUID = 1491824827231520518L;
	
	private UserExtEntity userExtEntity;
	
	private UserEntity userEntity;
	
	private UserAreaEntity userAreaEntity;
	
	private OuserEntity ouserEntity;

	public UserExtEntity getUserExtEntity() {
		return userExtEntity;
	}

	public void setUserExtEntity(UserExtEntity userExtEntity) {
		this.userExtEntity = userExtEntity;
	}

	public UserEntity getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}

	public UserAreaEntity getUserAreaEntity() {
		return userAreaEntity;
	}

	public void setUserAreaEntity(UserAreaEntity userAreaEntity) {
		this.userAreaEntity = userAreaEntity;
	}

	public OuserEntity getOuserEntity() {
		return ouserEntity;
	}

	public void setOuserEntity(OuserEntity ouserEntity) {
		this.ouserEntity = ouserEntity;
	}
	
}
