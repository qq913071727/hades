package cn.conac.rc.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.user.entity.UserAreaEntity;
import cn.conac.rc.user.repository.UserAreaRepository;

/**
 * UserAreaService类
 *
 * @author serviceCreator
 * @date 2017-04-10
 * @version 1.0
 */
@Service
public class UserAreaService extends GenericService<UserAreaEntity, Integer> {

	@Autowired
	private UserAreaRepository repository;
	
	/**
	 * 根据用户ID取得用户区域信息
	 * @param userId
	 * @return 用户区域信息
	 */
	public UserAreaEntity findByUserId(Integer userId) {
		return repository.findByUserId(userId);
	}
}
