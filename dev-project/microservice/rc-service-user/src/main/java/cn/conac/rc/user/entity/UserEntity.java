package cn.conac.rc.user.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * UserEntity类
 *
 * @author beanCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="ICP_USER")
public class UserEntity implements Serializable {

	private static final long serialVersionUID = 1491824827139919756L;

	@Id
	@ApiModelProperty("用户ID")
	private Integer userId;

	@ApiModelProperty("groupId")
	private Integer groupId;

	@ApiModelProperty("科室ID")
	private Integer departId;

	@ApiModelProperty("用户名 ")
	private String username;

	@ApiModelProperty("电子邮箱")
	private String email;

	@ApiModelProperty("registerTime")
	private Date registerTime;

	@ApiModelProperty("registerIp")
	private String registerIp;

	@ApiModelProperty("lastLoginTime")
	private Date lastLoginTime;

	@ApiModelProperty("lastLoginIp")
	private String lastLoginIp;

	@ApiModelProperty("loginCount")
	private Integer loginCount;

	@ApiModelProperty("rank")
	private Integer rank;

	@ApiModelProperty("uploadTotal")
	private Integer uploadTotal;

	@ApiModelProperty("上传大小")
	private Integer uploadSize;

	@ApiModelProperty("上传时间")
	private Date uploadDate;

	@ApiModelProperty("isAdmin")
	private Integer isAdmin;

	@ApiModelProperty("isViewonlyAdmin")
	private Integer isViewonlyAdmin;

	@ApiModelProperty("isSelfAdmin")
	private Integer isSelfAdmin;

	@ApiModelProperty("是否禁用")
	private Integer isDisabled;

	@ApiModelProperty("fileTotal")
	private Integer fileTotal;

	@ApiModelProperty("grain")
	private Integer grain;

	@ApiModelProperty("头像上传URL")
	private String fileUrl;

	@ApiModelProperty("昵称")
	private String nickName;

	@ApiModelProperty("科室")
	private String deptName;

	@ApiModelProperty("域名类别(1：政务:  2：公益)")
	private Integer domainType;

	@ApiModelProperty("是否删除（1：删除 0：正常）")
	private Integer isDeleted;

	@ApiModelProperty("注册的用户ID")
	private Integer registerUserId;

	@ApiModelProperty("更新时间")
	private Date updateDate;

	@ApiModelProperty("更新用户ID")
	private Integer updateUserId;
	
	@ApiModelProperty("对应关联角色表")
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name = "IcpUserRole", joinColumns = {@JoinColumn(name = "userId")}, inverseJoinColumns ={@JoinColumn(name = "roleId") })
	private List<RoleEntity> roleList;
	
	@ApiModelProperty("区分机构域名和编制域名（1：机构域名  0：编制域名）")
	private Integer isOrgDomain;

	public void setUserId(Integer userId){
		this.userId=userId;
	}

	public Integer getUserId(){
		return userId;
	}

	public void setGroupId(Integer groupId){
		this.groupId=groupId;
	}

	public Integer getGroupId(){
		return groupId;
	}

	public void setDepartId(Integer departId){
		this.departId=departId;
	}

	public Integer getDepartId(){
		return departId;
	}

	public void setUsername(String username){
		this.username=username;
	}

	public String getUsername(){
		return username;
	}

	public void setEmail(String email){
		this.email=email;
	}

	public String getEmail(){
		return email;
	}

	public void setRegisterTime(Date registerTime){
		this.registerTime=registerTime;
	}

	public Date getRegisterTime(){
		return registerTime;
	}

	public void setRegisterIp(String registerIp){
		this.registerIp=registerIp;
	}

	public String getRegisterIp(){
		return registerIp;
	}

	public void setLastLoginTime(Date lastLoginTime){
		this.lastLoginTime=lastLoginTime;
	}

	public Date getLastLoginTime(){
		return lastLoginTime;
	}

	public void setLastLoginIp(String lastLoginIp){
		this.lastLoginIp=lastLoginIp;
	}

	public String getLastLoginIp(){
		return lastLoginIp;
	}

	public void setLoginCount(Integer loginCount){
		this.loginCount=loginCount;
	}

	public Integer getLoginCount(){
		return loginCount;
	}

	public void setRank(Integer rank){
		this.rank=rank;
	}

	public Integer getRank(){
		return rank;
	}

	public void setUploadTotal(Integer uploadTotal){
		this.uploadTotal=uploadTotal;
	}

	public Integer getUploadTotal(){
		return uploadTotal;
	}

	public void setUploadSize(Integer uploadSize){
		this.uploadSize=uploadSize;
	}

	public Integer getUploadSize(){
		return uploadSize;
	}

	public void setUploadDate(Date uploadDate){
		this.uploadDate=uploadDate;
	}

	public Date getUploadDate(){
		return uploadDate;
	}

	public void setIsAdmin(Integer isAdmin){
		this.isAdmin=isAdmin;
	}

	public Integer getIsAdmin(){
		return isAdmin;
	}

	public void setIsViewonlyAdmin(Integer isViewonlyAdmin){
		this.isViewonlyAdmin=isViewonlyAdmin;
	}

	public Integer getIsViewonlyAdmin(){
		return isViewonlyAdmin;
	}

	public void setIsSelfAdmin(Integer isSelfAdmin){
		this.isSelfAdmin=isSelfAdmin;
	}

	public Integer getIsSelfAdmin(){
		return isSelfAdmin;
	}

	public void setIsDisabled(Integer isDisabled){
		this.isDisabled=isDisabled;
	}

	public Integer getIsDisabled(){
		return isDisabled;
	}

	public void setFileTotal(Integer fileTotal){
		this.fileTotal=fileTotal;
	}

	public Integer getFileTotal(){
		return fileTotal;
	}

	public void setGrain(Integer grain){
		this.grain=grain;
	}

	public Integer getGrain(){
		return grain;
	}

	public void setFileUrl(String fileUrl){
		this.fileUrl=fileUrl;
	}

	public String getFileUrl(){
		return fileUrl;
	}

	public void setNickName(String nickName){
		this.nickName=nickName;
	}

	public String getNickName(){
		return nickName;
	}

	public void setDeptName(String deptName){
		this.deptName=deptName;
	}

	public String getDeptName(){
		return deptName;
	}

	public void setDomainType(Integer domainType){
		this.domainType=domainType;
	}

	public Integer getDomainType(){
		return domainType;
	}

	public void setIsDeleted(Integer isDeleted){
		this.isDeleted=isDeleted;
	}

	public Integer getIsDeleted(){
		return isDeleted;
	}

	public Integer getRegisterUserId() {
		return registerUserId;
	}

	public void setRegisterUserId(Integer registerUserId) {
		this.registerUserId = registerUserId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public List<RoleEntity> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<RoleEntity> roleList) {
		this.roleList = roleList;
	}

	public Integer getIsOrgDomain() {
		return isOrgDomain;
	}

	public void setIsOrgDomain(Integer isOrgDomain) {
		this.isOrgDomain = isOrgDomain;
	}
}
