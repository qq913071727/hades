package cn.conac.rc.user.repository;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.user.entity.UserAreaEntity;

/**
 * UserAreaRepository类
 *
 * @author repositoryCreator
 * @date 2017-04-10
 * @version 1.0
 */
@Repository
public interface UserAreaRepository extends GenericDao<UserAreaEntity, Integer> {
	
	/**
	 * 根据用户ID取得用户区域信息
	 * @param userId
	 * @return 用户区域信息
	 */
	public UserAreaEntity findByUserId(@Param("userId") Integer userId);
	
}
