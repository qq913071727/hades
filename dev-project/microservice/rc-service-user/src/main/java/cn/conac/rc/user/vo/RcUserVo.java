package cn.conac.rc.user.vo;

import javax.persistence.Transient;

import cn.conac.rc.user.entity.RcUserEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * RcUserVo类
 *
 * @author voCreator
 * @date 2017-04-11
 * @version 1.0
 */
@ApiModel
public class RcUserVo extends RcUserEntity {

	private static final long serialVersionUID = 1491881200190777712L;

	@Transient
	@ApiModelProperty("当前分页")
	private Integer page;

	@Transient
	@ApiModelProperty("每页个数")
	private Integer size;
	
	@Transient
	@ApiModelProperty("是否精确查询(1:是 0:否)")
	private Integer eqFlag;
	
	@Transient
	@ApiModelProperty("当前登录用户的角色ID")
	private String currRoleId;

	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}

	public Integer getEqFlag() {
		return eqFlag;
	}

	public void setEqFlag(Integer eqFlag) {
		this.eqFlag = eqFlag;
	}

	public String getCurrRoleId() {
		return currRoleId;
	}

	public void setCurrRoleId(String currRoleId) {
		this.currRoleId = currRoleId;
	}
}
