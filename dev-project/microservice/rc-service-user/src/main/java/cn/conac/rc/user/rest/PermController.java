package cn.conac.rc.user.rest;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.user.entity.PermEntity;
import cn.conac.rc.user.service.PermService;
import cn.conac.rc.user.vo.PermNodeVo;
import cn.conac.rc.user.vo.PermVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * PermController类
 *
 * @author controllerCreator
 * @date 2017-04-10
 * @version 1.0
 */
@RestController
@RequestMapping(value="perm/")
public class PermController {

	@Autowired
	PermService service;

	@ApiOperation(value = "获取权限树", httpMethod = "POST", response = PermEntity.class, notes = "获取权限树")
	@RequestMapping(value = "tree", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> findPermTreeByCodition(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody PermVo permVo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		PermNodeVo  permNodeVo= service.findPermTreeByCodition(permVo);
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(permNodeVo);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "获取权限列表", httpMethod = "POST", response = PermEntity.class, notes = "获取权限列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> findPermListByCodition(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody PermVo permVo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<Map<String,Object>> permList  = service.findPermStrByCondition(permVo);
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(permList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
