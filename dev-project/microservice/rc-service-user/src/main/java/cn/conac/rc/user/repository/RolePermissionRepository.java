package cn.conac.rc.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.user.entity.RolePermissionEntity;

/**
 * RolePermissionRepository类
 *
 * @author repositoryCreator
 * @date 2017-04-10
 * @version 1.0
 */
@Repository
public interface RolePermissionRepository extends GenericDao<RolePermissionEntity, String> {

	/**
	 * 根据roleId删除全部权限信息
	 * @param roleId
	 * @return 删除数量
	 */
	@Query(value = "delete from icp_role_permission irp where irp.role_id= :roleId", nativeQuery = true)
	@Modifying
	int deleteByRoleId(@Param("roleId") Integer roleId);
	
	
	/**
	 * 通过角色ID获取角色权限相关数据
	 * @return
	 */
	List<RolePermissionEntity> findByRoleId(@Param("roleId") Integer roleId);
}
