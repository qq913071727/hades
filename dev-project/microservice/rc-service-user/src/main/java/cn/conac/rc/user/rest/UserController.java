package cn.conac.rc.user.rest;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.user.entity.UserEntity;
import cn.conac.rc.user.service.UserService;
import cn.conac.rc.user.vo.UserInfo2Vo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * UserController类
 *
 * @author controllerCreator
 * @date 2017-04-10
 * @version 1.0
 */
@RestController
@RequestMapping(value="user/")
public class UserController {

	@Autowired
	UserService service;

	@ApiOperation(value = "逻辑删除用户信息", httpMethod = "POST", response = UserEntity.class, notes = "根据用户id逻辑删除用户信息")
	@RequestMapping(value = "ud", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> updateIsDeletedByUserId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody UserInfo2Vo vo) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		int updateCountSum =-1;
		Map<String, Object> mapResult = new HashMap<String, Object>();
		if(null == vo.getUserIdList() || vo.getUserIdList().isEmpty() ) {
			mapResult.put("error", "选择的用户ID为空");
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setResult(mapResult);
			result.setMsg(ResultPojo.MSG_FAILURE);
			return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
		}
		updateCountSum = service.updateIsDeletedByUserId(vo.getUserIdList());
		mapResult.put("userIds", vo.getUserIdList());
		mapResult.put("userIdCount", vo.getUserIdList().size());
		mapResult.put("updateCountSum", updateCountSum);
		if(updateCountSum > 0) {
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setResult(mapResult);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		} else {
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setResult(mapResult);
			result.setMsg(ResultPojo.MSG_FAILURE);
		}
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "启用/禁用用户", httpMethod = "POST", response = UserEntity.class, notes = "根据用户id启用/禁用用户")
	@RequestMapping(value = "active", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> updateIsDisableByUserId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody UserInfo2Vo vo) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		int updateCountSum =-1;
		Map<String, Object> mapResult = new HashMap<String, Object>();
		if(null == vo.getUserIdList() || vo.getUserIdList().isEmpty() ) {
			mapResult.put("error", "选择的用户ID为空");
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setResult(mapResult);
			result.setMsg(ResultPojo.MSG_FAILURE);
			return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
		}
		updateCountSum = service.updateIsDisableByUserId(vo.getUserIdList(), vo.getIsDisabled());
		mapResult.put("userIds", vo.getUserIdList());
		mapResult.put("userIdCount", vo.getUserIdList().size());
		mapResult.put("updateCountSum", updateCountSum);
		if(updateCountSum > 0) {
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setResult(mapResult);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		} else {
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setResult(mapResult);
			result.setMsg(ResultPojo.MSG_FAILURE);
		}
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
}
