package cn.conac.rc.share.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.framework.utils.DateUtils;
import cn.conac.rc.share.entity.ShareAreaInfoEntity;
import cn.conac.rc.share.repository.ShareAreaInfoRepository;
import cn.conac.rc.share.vo.ShareAreaInfoMsgVo;
import cn.conac.rc.share.vo.ShareAreaInfoVo;

/**
 * ShareAreaInfoService类
 *
 * @author serviceCreator
 * @date 2017-07-21
 * @version 1.0
 */
@Service
public class ShareAreaInfoService extends GenericService<ShareAreaInfoEntity, Integer> {

	@Autowired
	private ShareAreaInfoRepository repository;
	
	/**
	 * 批量新增或者
	 * @param shareAreaInfoEntityList
	 * @return 更新或保存的个数
	 * @throws Exception
	 */
	public int batchSaveOrUpdate(List<ShareAreaInfoEntity> shareAreaInfoEntityList ) throws Exception{
		// 批量新增或者更新
		int cnt = 0;
		for(ShareAreaInfoEntity shareAreaInfoEntity : shareAreaInfoEntityList ) {
			this.save(shareAreaInfoEntity);
			cnt++;
		}
		return cnt;
	}
	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<ShareAreaInfoEntity> list(ShareAreaInfoVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<ShareAreaInfoEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<ShareAreaInfoEntity> createCriteria(ShareAreaInfoVo param) {
		Criteria<ShareAreaInfoEntity> dc = new Criteria<ShareAreaInfoEntity>();
		// TODO 具体条件赋值

		return dc;
	}
	
	/**
	 * 通过用户登录的地区代码查找申请地区列表信息(审核中和审核通过并且在有效期内)
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public List<ShareAreaInfoMsgVo> findShareAreaInfoList(String areaCode) throws Exception {
		try {
			long remDays  = -1;
			ShareAreaInfoMsgVo shareAreaInfoMsgVo = null;
			List<ShareAreaInfoMsgVo> shareAreaInfoMsgVoList = new ArrayList<ShareAreaInfoMsgVo>();
			Criteria<ShareAreaInfoEntity> dc = this.createCriteriaByAreaCode(areaCode);
			 List<ShareAreaInfoEntity>   shareAreaInfoEntityList = repository.findAll(dc);
			 for(ShareAreaInfoEntity shareAreaInfoEntity : shareAreaInfoEntityList) {
				 if(shareAreaInfoEntity.getStatus().intValue() 
						 == ShareAreaInfoEntity.SHARE_AREA_INFO_STATUS_AUDITING ) {
					 shareAreaInfoMsgVo = new ShareAreaInfoMsgVo();
					 if(shareAreaInfoEntity.getAreaCodeSource().equals(areaCode)) {
						 shareAreaInfoMsgVo.setAreaCode(shareAreaInfoEntity.getAreaCodeDest());
						 shareAreaInfoMsgVo.setAreaName(shareAreaInfoEntity.getAreaDestNameRe());
					 } else {
						 shareAreaInfoMsgVo.setAreaCode(shareAreaInfoEntity.getAreaCodeSource());
						 shareAreaInfoMsgVo.setAreaName(shareAreaInfoEntity.getAreaSourceNameRe());
					 }
					 shareAreaInfoMsgVo.setMsg(shareAreaInfoMsgVo.getAreaName() +"已提交共享申请，目前处于审核中，不能重复提交申请");
					 shareAreaInfoMsgVoList.add(shareAreaInfoMsgVo);
				 } else {
					 // 审核通过
					 if(null != shareAreaInfoEntity.getEndTime()) {
						 remDays = DateUtils.remDays(shareAreaInfoEntity.getEndTime());
					 }
					 // 审核通过的共享地区放过
					 if(remDays < 0) {
						 continue;
					 }
					 shareAreaInfoMsgVo = new ShareAreaInfoMsgVo();
					 if(shareAreaInfoEntity.getAreaCodeSource().equals(areaCode)) {
						 shareAreaInfoMsgVo.setAreaCode(shareAreaInfoEntity.getAreaCodeDest());
						 shareAreaInfoMsgVo.setAreaName(shareAreaInfoEntity.getAreaDestNameRe());
					 } else {
						 shareAreaInfoMsgVo.setAreaCode(shareAreaInfoEntity.getAreaCodeSource());
						 shareAreaInfoMsgVo.setAreaName(shareAreaInfoEntity.getAreaSourceNameRe());
					 }
					 shareAreaInfoMsgVo.setMsg(shareAreaInfoMsgVo.getAreaName() +"已提交共享申请并审核通过，目前仍处于有效期内，暂不需要申请");
					 shareAreaInfoMsgVoList.add(shareAreaInfoMsgVo);
				 }
			 }
			 return shareAreaInfoMsgVoList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param areaCode
	 * @return Criteria
	 */
	private Criteria<ShareAreaInfoEntity> createCriteriaByAreaCode(String areaCode) {
		Criteria<ShareAreaInfoEntity> dc = new Criteria<ShareAreaInfoEntity>();
		dc.add(Restrictions.or(Restrictions.eq("areaCodeSource", areaCode, true),
				Restrictions.eq("areaCodeDest", areaCode, true)));
		dc.add(Restrictions.ne("status", ShareAreaInfoEntity.SHARE_AREA_INFO_STATUS_REFUSE, true));
		return dc;
	}

}
