package cn.conac.rc.share.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.share.entity.ShareAreaInfoEntity;
import cn.conac.rc.share.service.ShareAreaInfoService;
import cn.conac.rc.share.vo.ShareAreaInfoCUVo;
import cn.conac.rc.share.vo.ShareAreaInfoMsgVo;
import cn.conac.rc.share.vo.ShareAreaInfoVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * ShareAreaInfoController类
 *
 * @author controllerCreator
 * @date 2017-07-21
 * @version 1.0
 */
@RestController
@RequestMapping(value="shareAreaInfo/")
public class ShareAreaInfoController {

	@Autowired
	ShareAreaInfoService service;
	
	@ApiOperation(value = "列表", httpMethod = "POST", response = ShareAreaInfoEntity.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody ShareAreaInfoVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<ShareAreaInfoEntity> list = service.list(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "批量新增或者更新申请地区共享信息表", httpMethod = "POST", response = ShareAreaInfoEntity.class, notes = "批量新增或者更新申请地区共享信息表")
	@RequestMapping(value = "saveOrUpdate", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> save(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "保存对象", required = true) @RequestBody ShareAreaInfoCUVo shareAreaInfoCUVo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		List<ShareAreaInfoEntity> shareAreaInfoEntityList =	shareAreaInfoCUVo.getShareAreaInfoEntityList();
		for(ShareAreaInfoEntity shareAreaInfoEntity : shareAreaInfoEntityList) {
			// 数据格式校验
			String valMsg = service.validate(shareAreaInfoEntity);
			if (valMsg != null) {
				result.setCode(ResultPojo.CODE_FORMAT_ERR);
				result.setMsg(valMsg);
				return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
			}
		}
		service.batchSaveOrUpdate(shareAreaInfoEntityList);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(shareAreaInfoCUVo.getShareAreaInfoEntityList());
		result.setMsg(ResultPojo.MSG_SUCCESS);
		
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "获取审核中和审核通过并在有效期内的地区共享信息(验证是否可提交申请用)", httpMethod = "GET", response = ShareAreaInfoMsgVo.class, notes = "获取审核中和审核通过并在有效期内的地区共享信息(验证是否可提交申请用)")
	@RequestMapping(value = "{areaCode}/list", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findShareAreaInfoList(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "地区代码", required = true) @PathVariable("areaCode") String areaCode ) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<ShareAreaInfoMsgVo>  shareAreaInfoMsgVoList = service.findShareAreaInfoList(areaCode);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(shareAreaInfoMsgVoList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
