package cn.conac.rc.share.vo;

import javax.persistence.Transient;

import cn.conac.rc.share.entity.ShareAreaInfoEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ShareAreaInfoVo类
 *
 * @author voCreator
 * @date 2017-07-21
 * @version 1.0
 */
@ApiModel
public class ShareAreaInfoVo extends ShareAreaInfoEntity {

	private static final long serialVersionUID = 1500615505043479122L;

	@Transient
	@ApiModelProperty("当前分页")
	private Integer page;

	@Transient
	@ApiModelProperty("每页个数")
	private Integer size;

	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}
}
