package cn.conac.rc.share.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.share.entity.ShareAreaReviewEntity;

/**
 * ShareAreaReviewRepository类
 *
 * @author repositoryCreator
 * @date 2017-07-21
 * @version 1.0
 */
@Repository
public interface ShareAreaReviewRepository extends GenericDao<ShareAreaReviewEntity, Integer> {
}
