package cn.conac.rc.share.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.share.entity.ShareAreaEntity;

/**
 * ShareAreaRepository类
 *
 * @author repositoryCreator
 * @date 2017-07-21
 * @version 1.0
 */
@Repository
public interface ShareAreaRepository extends GenericDao<ShareAreaEntity, Integer> {
	
	
	/**
	 * 根据地区代码申请方和地区代码被申请方取得共享信息
	 * @param areaCodeSource
	 * 	@param areaCodeDest
	 * @return List<ShareAreaEntity> 防止有异常数据定义为List
	 */
	public List<ShareAreaEntity>  findByAreaCodeSourceAndAreaCodeDest(String areaCodeSource, String areaCodeDest);
	
	/**
	 * 根据地区代码申请方和地区代码被申请方删除共享信息
	 * @param areaCodeSource
	 * 	@param areaCodeDest
	 * @return void
	 */
	@Query(value = "delete from share_area sa where sa.area_code_source= :areaCodeSource and sa.area_code_dest= :areaCodeDest", nativeQuery = true)
	@Modifying
	public void deleteByAreaCodeInfo(@Param("areaCodeSource") String areaCodeSource, @Param("areaCodeDest") String areaCodeDest);
}
