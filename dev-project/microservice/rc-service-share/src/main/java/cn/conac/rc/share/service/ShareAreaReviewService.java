package cn.conac.rc.share.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.framework.utils.BeanMapper;
import cn.conac.rc.framework.utils.DateUtils;
import cn.conac.rc.share.entity.ShareAreaEntity;
import cn.conac.rc.share.entity.ShareAreaInfoEntity;
import cn.conac.rc.share.entity.ShareAreaReviewEntity;
import cn.conac.rc.share.repository.ShareAreaRepository;
import cn.conac.rc.share.repository.ShareAreaReviewRepository;
import cn.conac.rc.share.vo.ShareAreaReviewCUVo;
import cn.conac.rc.share.vo.ShareAreaReviewVo;

/**
 * ShareAreaReviewService类
 *
 * @author serviceCreator
 * @date 2017-07-21
 * @version 1.0
 */
@Service
@Transactional
public class ShareAreaReviewService extends GenericService<ShareAreaReviewEntity, Integer> {

	@Autowired
	private ShareAreaInfoService  shareAreaInfoService;
	
	@Autowired
	private ShareAreaService  shareAreaService;
	
	@Autowired
	private ShareAreaReviewRepository repository;
	
	@Autowired 
	ShareAreaRepository shareAreaRepository;

	/**
	 * 批量审核地区共享信息
	 * @param shareAreaInfoEntityList
	 * @return 更新或保存的个数
	 * @throws Exception
	 */
	public int batchSaveOrUpdate(ShareAreaReviewCUVo shareAreaReviewCUVo) throws Exception{
		// 批量新增或者更新
		int cnt = 0;
		for(ShareAreaReviewEntity shareAreaReviewEntity : shareAreaReviewCUVo.getShareAreaReviewEntityList() ) {
			this.save(shareAreaReviewEntity);
			cnt++;
		}
		// 审核通过时同时要 更新申请地区共享信息表和地区共享表
		if(shareAreaReviewCUVo.getReviewStatus().intValue() 
				 == ShareAreaReviewEntity.SHARE_AREA_INFO_STATUS_PASSED) {
			Integer shareInfoId = null;
			ShareAreaInfoEntity shareAreaInfoEntityDb = null;
			ShareAreaInfoEntity shareAreaInfoEntityNew = null;
			ShareAreaEntity shareAreaEntity = null;
//			List<ShareAreaEntity> shareAreaEntityList = null;
			for(int i=0; i < shareAreaReviewCUVo.getShareInfoIdList().size(); i ++) {
				// 更新申请地区共享信息表
				shareInfoId = shareAreaReviewCUVo.getShareInfoIdList().get(i);
				shareAreaInfoEntityDb = shareAreaInfoService.findById(shareInfoId);
				if(null != shareAreaInfoEntityDb) {
					shareAreaInfoEntityNew = new ShareAreaInfoEntity();
					BeanMapper.copy(shareAreaInfoEntityDb, shareAreaInfoEntityNew);
					Date currDate = new Date();
					Date endDate = DateUtils.getDateAfter(currDate,shareAreaReviewCUVo.getValidDays().intValue());
					shareAreaInfoEntityNew.setStartTime(currDate);
					shareAreaInfoEntityNew.setEndTime(endDate);
					shareAreaInfoService.save(shareAreaInfoEntityNew);
					// 地区共享表检查是否存在相同的地区代码申请方和地区代码被申请方,如果存在先删除
//					shareAreaEntityList = shareAreaRepository.findByAreaCodeSourceAndAreaCodeDest(
//							shareAreaInfoEntityDb.getAreaCodeSource(), shareAreaInfoEntityDb.getAreaCodeDest());
//					if(shareAreaEntityList != null && shareAreaEntityList.size() > 0) {
//						shareAreaRepository.deleteByAreaCodeInfo(
//								shareAreaInfoEntityDb.getAreaCodeSource(), shareAreaInfoEntityDb.getAreaCodeDest());
//					}
					shareAreaRepository.deleteByAreaCodeInfo(
							shareAreaInfoEntityDb.getAreaCodeSource(), shareAreaInfoEntityDb.getAreaCodeDest());
					shareAreaEntity = new ShareAreaEntity();
					shareAreaEntity.setAreaCodeDest(shareAreaInfoEntityDb.getAreaCodeDest());
					shareAreaEntity.setAreaCodeSource(shareAreaInfoEntityDb.getAreaCodeSource());
					shareAreaEntity.setAreaNameDestRe(shareAreaInfoEntityDb.getAreaDestNameRe());
					shareAreaEntity.setAreaNameSourceRe(shareAreaInfoEntityDb.getAreaSourceNameRe());
					shareAreaEntity.setShareInfoId(shareAreaInfoEntityDb.getId());
					shareAreaEntity.setEndTime(endDate);
					shareAreaService.save(shareAreaEntity);
				}
			}
		}
		return cnt;
	}
	

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<ShareAreaReviewEntity> list(ShareAreaReviewVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<ShareAreaReviewEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<ShareAreaReviewEntity> createCriteria(ShareAreaReviewVo param) {
		Criteria<ShareAreaReviewEntity> dc = new Criteria<ShareAreaReviewEntity>();
		// TODO 具体条件赋值

		return dc;
	}

}
