package cn.conac.rc.share.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.share.entity.ShareAreaInfoEntity;

/**
 * ShareAreaInfoRepository类
 *
 * @author repositoryCreator
 * @date 2017-07-21
 * @version 1.0
 */
@Repository
public interface ShareAreaInfoRepository extends GenericDao<ShareAreaInfoEntity, Integer> {
}
