package cn.conac.rc.share.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.share.entity.ShareAreaEntity;
import cn.conac.rc.share.service.ShareAreaService;
import cn.conac.rc.share.vo.SharedAreaVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * ShareAreaController类
 *
 * @author controllerCreator
 * @date 2017-07-21
 * @version 1.0
 */
@RestController
@RequestMapping(value="sharedArea/")
public class ShareAreaController {

	@Autowired
	ShareAreaService service;
	
	@ApiOperation(value = "获取已共享的地区信息", httpMethod = "GET", response = ShareAreaEntity.class, notes = "获取可共享的地区信息")
	@RequestMapping(value = "{areaCode}/list", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findSharedAreaList(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "地区代码", required = true) @PathVariable("areaCode") String areaCode ) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<SharedAreaVo>  list = service.findSharedAreaList(areaCode);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "新增", httpMethod = "POST", response = ShareAreaEntity.class, notes = "保存资源到数据库")
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> save(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "保存对象", required = true) @RequestBody ShareAreaEntity entity) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();

		//TODO 1.用户权限校验

		//TODO 2.数据格式校验
		String valMsg = service.validate(entity);
		if (valMsg != null) {
			result.setCode(ResultPojo.CODE_FORMAT_ERR);
			result.setMsg(valMsg);
			return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
		}

		//TODO 3.业务逻辑校验

		//TODO 4.业务操作

		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);// TODO 根据具体需求配置返回消息
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "物理删除", httpMethod = "POST", response = ShareAreaEntity.class, notes = "根据id物理删除资源")
	@RequestMapping(value = "{id}/delete", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> delete(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		if(service.exists(id)){
			service.delete(id);
			// 结果集设定
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		} else {
			// 结果集设定
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg(ResultPojo.MSG_FAILURE);
		}
		result.setResult(id);

		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
