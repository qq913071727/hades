package cn.conac.rc.share.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.share.entity.ShareAreaInfoEntity;
import cn.conac.rc.share.entity.ShareAreaReviewEntity;
import cn.conac.rc.share.service.ShareAreaReviewService;
import cn.conac.rc.share.vo.ShareAreaReviewCUVo;
import cn.conac.rc.share.vo.ShareAreaReviewVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * ShareAreaReviewController类
 *
 * @author controllerCreator
 * @date 2017-07-21
 * @version 1.0
 */
@RestController
@RequestMapping(value="shareAreaReview/")
public class ShareAreaReviewController {

	@Autowired
	ShareAreaReviewService service;

	@ApiOperation(value = "详情", httpMethod = "GET", response = ShareAreaReviewEntity.class, notes = "根据id获取资源详情")
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		ShareAreaReviewEntity entity = service.findById(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "列表", httpMethod = "POST", response = ShareAreaReviewEntity.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody ShareAreaReviewVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<ShareAreaReviewEntity> list = service.list(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "批量共享地区审核", httpMethod = "POST", response = ShareAreaInfoEntity.class, notes = "批量共享地区审核")
	@RequestMapping(value = "saveOrUpdate", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> saveOrUpdate(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "保存对象", required = true) @RequestBody ShareAreaReviewCUVo shareAreaReviewCUVo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		List<ShareAreaReviewEntity> shareAreaReviewEntityList = shareAreaReviewCUVo.getShareAreaReviewEntityList();
		for(ShareAreaReviewEntity shareAreaReviewEntity : shareAreaReviewEntityList) {
			// 数据格式校验
			String valMsg = service.validate(shareAreaReviewEntity);
			if (valMsg != null) {
				result.setCode(ResultPojo.CODE_FORMAT_ERR);
				result.setMsg(valMsg);
				return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
			}
		}
		service.batchSaveOrUpdate(shareAreaReviewCUVo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(shareAreaReviewCUVo.getShareAreaReviewEntityList());
		result.setMsg(ResultPojo.MSG_SUCCESS);
		
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
