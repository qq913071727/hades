package cn.conac.rc.share.vo;

import java.io.Serializable;
import java.util.List;

import cn.conac.rc.share.entity.ShareAreaReviewEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ShareAreaInfoVo类
 *
 * @author voCreator
 * @date 2017-07-21
 * @version 1.0
 */
@ApiModel
public class ShareAreaReviewCUVo implements Serializable {

	private static final long serialVersionUID = 4024575722367459220L;
	
	List<ShareAreaReviewEntity> shareAreaReviewEntityList;
	
	List<Integer> shareInfoIdList;
	
	@ApiModelProperty("审核结果（2-通过  3-拒绝 4-挂起)")
	private Integer reviewStatus;
	
	// 地区共享的有效期限90天
	private Integer validDays = 90;

	public List<ShareAreaReviewEntity> getShareAreaReviewEntityList() {
		return shareAreaReviewEntityList;
	}

	public void setShareAreaReviewEntityList(List<ShareAreaReviewEntity> shareAreaReviewEntityList) {
		this.shareAreaReviewEntityList = shareAreaReviewEntityList;
	}

	public List<Integer> getShareInfoIdList() {
		return shareInfoIdList;
	}

	public void setShareInfoIdList(List<Integer> shareInfoIdList) {
		this.shareInfoIdList = shareInfoIdList;
	}

	public Integer getReviewStatus() {
		return reviewStatus;
	}

	public void setReviewStatus(Integer reviewStatus) {
		this.reviewStatus = reviewStatus;
	}

	public Integer getValidDays() {
		return validDays;
	}

	public void setValidDays(Integer validDays) {
		this.validDays = validDays;
	}
}
