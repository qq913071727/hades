package cn.conac.rc.share.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ShareAreaReviewEntity类
 *
 * @author beanCreator
 * @date 2017-07-21
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="SHARE_AREA_REVIEW")
public class ShareAreaReviewEntity implements Serializable {

	private static final long serialVersionUID = 1500615504971525386L;
	
    /**
	 * 地区共享状态通过
	 */
    public static final int SHARE_AREA_INFO_STATUS_PASSED = 2; 
    
    /**
 	 * 地区共享状态拒绝
 	 */
     public static final int SHARE_AREA_INFO_STATUS_REFUSE = 3; 
     
     /**
  	 * 地区共享状态挂起
  	 */
      public static final int SHARE_AREA_INFO_STATUS_HUNGUP = 4; 

	@ApiModelProperty("主键")
	private Integer id;

	@NotNull
	@ApiModelProperty("申请地区共享信息表外键")
	private Integer shareInfoId;

	@NotNull
	@ApiModelProperty("审核人")
	private Integer reviewUserId;

	@NotNull
	@ApiModelProperty("审核日期")
	private Date reviewTime;

	@NotNull
	@ApiModelProperty("审核结果（2-通过  3-拒绝 4-挂起)")
	private Integer reviewStatus;

	@ApiModelProperty("审核拒绝原因")
	private String refusalReason;

	@ApiModelProperty("审核备注信息")
	private String reviewRemark;

	public void setId(Integer id){
		this.id=id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="mySeqGenerator")
	@SequenceGenerator(name = "mySeqGenerator", sequenceName = "S_SHARE_AREA_REVIEW", allocationSize = 1)
	public Integer getId(){
		return id;
	}

	public void setShareInfoId(Integer shareInfoId){
		this.shareInfoId=shareInfoId;
	}

	public Integer getShareInfoId(){
		return shareInfoId;
	}

	public void setReviewUserId(Integer reviewUserId){
		this.reviewUserId=reviewUserId;
	}

	public Integer getReviewUserId(){
		return reviewUserId;
	}

	public void setReviewTime(Date reviewTime){
		this.reviewTime=reviewTime;
	}

	public Date getReviewTime(){
		return reviewTime;
	}

	public void setReviewStatus(Integer reviewStatus){
		this.reviewStatus=reviewStatus;
	}

	public Integer getReviewStatus(){
		return reviewStatus;
	}

	public void setRefusalReason(String refusalReason){
		this.refusalReason=refusalReason;
	}

	public String getRefusalReason(){
		return refusalReason;
	}

	public void setReviewRemark(String reviewRemark){
		this.reviewRemark=reviewRemark;
	}

	public String getReviewRemark(){
		return reviewRemark;
	}

}
