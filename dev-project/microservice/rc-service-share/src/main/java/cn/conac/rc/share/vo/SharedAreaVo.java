package cn.conac.rc.share.vo;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * SharedAreaVo类
 *
 * @author voCreator
 * @date 2017-07-21
 * @version 1.0
 */
@ApiModel
public class SharedAreaVo implements Serializable  {

	private static final long serialVersionUID = 2539030003005440895L;
	
	@ApiModelProperty("主键")
	private Integer id;

	@ApiModelProperty("申请地区共享信息表外键")
	private Integer shareInfoId;
	
	@ApiModelProperty("共享的地区代码")
	private String areaCode;

	@ApiModelProperty("共享的地区名称")
	private String areaName;
	
	@ApiModelProperty("失效时间")
	private Date endTime;
	
	@ApiModelProperty("剩余天数")
	private long remDays = -1;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getShareInfoId() {
		return shareInfoId;
	}

	public void setShareInfoId(Integer shareInfoId) {
		this.shareInfoId = shareInfoId;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public long getRemDays() {
		return remDays;
	}

	public void setRemDays(long remDays) {
		this.remDays = remDays;
	}
	
}
