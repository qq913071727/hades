package cn.conac.rc.code.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.code.entity.RcEncodeRecordEntity;
import cn.conac.rc.code.entity.RcEncodeRecordRemoveEntity;
import cn.conac.rc.code.entity.RcOrgEncodeRecordEntity;
import cn.conac.rc.code.service.RcEncodeRecordRemoveService;
import cn.conac.rc.code.service.RcEncodeRecordService;
import cn.conac.rc.code.service.RcOrgEncodeRecordService;
import cn.conac.rc.framework.utils.IdGen;
import cn.conac.rc.framework.vo.ResultPojo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * RcOrgEncodeRecordController类
 *
 * @author controllerCreator
 * @date 2016-11-23
 * @version 1.0
 */
@RestController
@RequestMapping(value="rcOrgEncodeRecord/")
public class RcOrgEncodeRecordController {

	@Autowired
	RcOrgEncodeRecordService service;
	@Autowired
	RcEncodeRecordService encodeService;
	@Autowired
	RcEncodeRecordRemoveService encodeRemoveService;

	@ApiOperation(value = "详情", httpMethod = "GET", response = RcOrgEncodeRecordEntity.class, notes = "根据id获取资源详情")
	@RequestMapping(value = "{findId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "findId", required = true) @PathVariable("findId") String findId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		RcOrgEncodeRecordEntity entity = service.findByFindId(findId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
//
//	@ApiOperation(value = "个数", httpMethod = "POST", response = Long.class, notes = "根据条件获得资源数量")
//	@RequestMapping(value = "count", method = RequestMethod.POST)
//	public ResponseEntity<ResultPojo> count(HttpServletRequest request, HttpServletResponse response,
//			@ApiParam(value = "查询条件对象", required = true) @RequestBody RcOrgEncodeRecordVo vo) throws Exception {
//		// 声明返回结果集
//		ResultPojo result = new ResultPojo();
//		// service调用
//		long cnt = service.count(vo);
//		// 结果集设定
//		result.setCode(ResultPojo.CODE_SUCCESS);
//		result.setResult(cnt);
//		result.setMsg(ResultPojo.MSG_SUCCESS);
//		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
//	}
//
//	@ApiOperation(value = "列表", httpMethod = "POST", response = RcOrgEncodeRecordEntity.class, notes = "根据条件获得资源列表")
//	@RequestMapping(value = "list", method = RequestMethod.POST)
//	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
//			@ApiParam(value = "查询条件对象", required = true) @RequestBody RcOrgEncodeRecordVo vo) throws Exception {
//		// 声明返回结果集
//		ResultPojo result = new ResultPojo();
//		// service调用
//		Page<RcOrgEncodeRecordEntity> list = service.list(vo);
//		// 结果集设定
//		result.setCode(ResultPojo.CODE_SUCCESS);
//		result.setResult(list);
//		result.setMsg(ResultPojo.MSG_SUCCESS);
//		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
//	}

//	@ApiOperation(value = "新增", httpMethod = "POST", response = RcOrgEncodeRecordEntity.class, notes = "保存资源到数据库")
//	@RequestMapping(value = "", method = RequestMethod.POST)
//	public ResponseEntity<ResultPojo> save(HttpServletRequest request, HttpServletResponse response,
//			@ApiParam(value = "保存对象", required = true) @RequestBody RcOrgEncodeRecordEntity entity) throws Exception {
//		// 声明返回结果集
//		ResultPojo result = new ResultPojo();
//
//		//TODO 1.用户权限校验
//
//		//TODO 2.数据格式校验
//		String valMsg = service.validate(entity);
//		if (valMsg != null) {
//			result.setCode(ResultPojo.CODE_FORMAT_ERR);
//			result.setMsg(valMsg);
//			return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
//		}
//
//		//TODO 3.业务逻辑校验
//
//		//TODO 4.业务操作
//
//		// 结果集设定
//		result.setCode(ResultPojo.CODE_SUCCESS);
//		result.setResult(entity);
//		result.setMsg(ResultPojo.MSG_SUCCESS);// TODO 根据具体需求配置返回消息
//		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
//	}
//
//	@ApiOperation(value = "更新", httpMethod = "POST", response = RcOrgEncodeRecordEntity.class, notes = "根据id更新数据库资源")
//	@RequestMapping(value = "{id}", method = RequestMethod.POST)
//	public ResponseEntity<ResultPojo> update(HttpServletRequest request, HttpServletResponse response,
//			@ApiParam(value = "更新对象", required = true) @RequestBody RcOrgEncodeRecordEntity entity) {
//		// 声明返回结果集
//		ResultPojo result = new ResultPojo();
//		// service调用
//		service.save(entity);
//		// 结果集设定
//		result.setCode(ResultPojo.CODE_SUCCESS);
//		result.setResult(entity);
//		result.setMsg(ResultPojo.MSG_SUCCESS);// TODO 根据具体需求配置返回消息
//		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
//	}

//	@ApiOperation(value = "物理删除", httpMethod = "POST", response = RcOrgEncodeRecordEntity.class, notes = "根据id物理删除资源")
//	@RequestMapping(value = "{id}/delete", method = RequestMethod.POST)
//	public ResponseEntity<ResultPojo> delete(HttpServletRequest request, HttpServletResponse response,
//			@ApiParam(value = "id", required = true) @PathVariable("id") String id) {
//		// 声明返回结果集
//		ResultPojo result = new ResultPojo();
//		// service调用
//		service.delete(id);
//		// 结果集设定
//		result.setCode(ResultPojo.CODE_SUCCESS);
//		result.setResult(id);
//		result.setMsg(ResultPojo.MSG_SUCCESS);// TODO 根据具体需求配置返回消息
//		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
//	}
	
	@ApiOperation(value = "生成部门编码", httpMethod = "POST", response = RcOrgEncodeRecordEntity.class, notes = "根据地区码、基本码和部门类型生成部门编码")
	@RequestMapping(value = "{aCode}/{bCode}/{orgType}", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> createOrgCode(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "aCode", required = true) @PathVariable("aCode") String aCode,
			@ApiParam(value = "bCode", required = true) @PathVariable("bCode") String bCode,
			@ApiParam(value = "orgType", required = true) @PathVariable("orgType") String orgType) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		//输入数据校验
		if(aCode == null || bCode == null){
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg(ResultPojo.MSG_FAILURE);
		}else{
			String findId = aCode.concat(bCode);
			RcOrgEncodeRecordEntity entity = service.findByFindId(findId);
			if(entity != null && "9999".equals(entity.getCodeNum())){
				result.setCode(ResultPojo.CODE_FAILURE);
				result.setMsg(ResultPojo.MSG_FAILURE);
			}else{
				//生成新编码
				String code = service.createOrgCode(findId,orgType);
				//更新当前entity
				int codeLength = code.length();
				if(entity == null){
					entity = new RcOrgEncodeRecordEntity();
					entity.setFId(findId);
					entity.setId(IdGen.uuid());
				}
				entity.setCodeNum(code.substring(codeLength-4,codeLength));
				service.save(entity);
				//返回生成码
				result.setCode(ResultPojo.CODE_SUCCESS);
				result.setResult(code);
				result.setMsg(ResultPojo.MSG_SUCCESS);
			}
		}
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "根据findId物理删除编码表信息", httpMethod = "POST", response = RcOrgEncodeRecordEntity.class, notes = "根据findId物理删除编码表信息")
	@RequestMapping(value = "{findId}/delete", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> deleteByFindId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "findId", required = true) @PathVariable("findId") String findId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		service.deleteByFindId(findId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(findId);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "生成人员编制信息编码", httpMethod = "POST", response = RcOrgEncodeRecordEntity.class, notes = "根据统一社会信用代码、编制类型生成编码")
	@RequestMapping(value = "encode/R/{orgCode}/{empType}", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> createEmpCode(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "9位主体标识代码", required = true) @PathVariable("orgCode") String orgCode,
			@ApiParam(value = "1位人员编制类型码", required = true) @PathVariable("empType") String empType) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		//输入数据校验,是否增加统一社会编码校验
		if(orgCode == null || empType == null){
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg(ResultPojo.MSG_FAILURE);
		}else{
			//在编码回收表中查找是否有可用编码
			//如果有则使用并从回收表中删除
			String empEncode;
			RcEncodeRecordRemoveEntity removeEntity = encodeRemoveService.findByCodeAndType(orgCode,"R");
			if(removeEntity != null){
				empEncode = "R".concat(removeEntity.getOrgCode())
						       .concat(empType)
						       .concat(removeEntity.getCodeNum());
				//从回收表里删除该条记录
				encodeRemoveService.delete(removeEntity);
				//返回生成码
				result.setCode(ResultPojo.CODE_SUCCESS);
				result.setResult(empEncode);
				result.setMsg(ResultPojo.MSG_SUCCESS);
			}else{
				RcEncodeRecordEntity entity = encodeService.findByCodeAndType(orgCode,"R");
				if(entity != null && "99999".equals(entity.getCodeNum())){
					result.setCode(ResultPojo.CODE_FAILURE);
					result.setMsg(ResultPojo.MSG_FAILURE);
				}else{
					//生成新编码
					String code = encodeService.createEmpEnCode(orgCode,empType);
					//更新当前entity
					int codeLength = code.length();
					if(entity == null){
						entity = new RcEncodeRecordEntity();
						entity.setOrgCode(orgCode);
						entity.setOrgType("R");
						entity.setId(IdGen.uuid());
					}
					entity.setCodeNum(code.substring(codeLength-5,codeLength));
					encodeService.save(entity);
					//返回生成码
					result.setCode(ResultPojo.CODE_SUCCESS);
					result.setResult(code);
					result.setMsg(ResultPojo.MSG_SUCCESS);
				}
			}
		}
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "人员编制编码回收", httpMethod = "POST", response = RcOrgEncodeRecordEntity.class, notes = "人员删除或迁移时,人员编码需要回收")
	@RequestMapping(value = "encode/remove/R/{empEncode}", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> removeEmpCode(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "16位人员编码", required = true) @PathVariable("empEncode") String empEncode) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		//输入数据校验,是否增加统一社会编码校验
		if(empEncode == null || !empEncode.startsWith("R") || empEncode.length() != 16){
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg(ResultPojo.MSG_FAILURE);
		}else{
			RcEncodeRecordRemoveEntity entity = new RcEncodeRecordRemoveEntity();
			String orgCode = empEncode.substring(1, 10);
			String code = empEncode.substring(11, 16);
			RcEncodeRecordRemoveEntity removeEntity = encodeRemoveService.findByCodeTypeAndNum(orgCode, "R", code);
			if(removeEntity == null){
				entity.setId(IdGen.uuid());
				entity.setOrgType("R");
				entity.setCodeNum(code);
				entity.setOrgCode(orgCode);
				encodeRemoveService.save(entity);
				//返回生成码
				result.setCode(ResultPojo.CODE_SUCCESS);
				result.setResult("create:" + empEncode);
				result.setMsg(ResultPojo.MSG_SUCCESS);
			}
		}
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult("not create:" + empEncode);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
