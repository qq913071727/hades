package cn.conac.rc.code.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import cn.conac.rc.code.entity.RcEncodeRecordEntity;
import cn.conac.rc.code.entity.RcEncodeRecordRemoveEntity;
import cn.conac.rc.code.repository.RcEncodeRecordRepository;
import cn.conac.rc.code.vo.RcEncodeRecordVo;
import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;

/**
 * RcEncodeRecordService类
 *
 * @author serviceCreator
 * @date 2017-06-21
 * @version 1.0
 */
@Service
public class RcEncodeRecordService extends GenericService<RcEncodeRecordEntity, Integer> {

	@Autowired
	private RcEncodeRecordRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(RcEncodeRecordVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<RcEncodeRecordEntity> list(RcEncodeRecordVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<RcEncodeRecordEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<RcEncodeRecordEntity> createCriteria(RcEncodeRecordVo param) {
		Criteria<RcEncodeRecordEntity> dc = new Criteria<RcEncodeRecordEntity>();
		// TODO 具体条件赋值
		return dc;
	}
	
	/**
	 * 通过findByCodeAndType查找可用编码
	 * @param findByCodeAndType
	 * @return
	 */
	public RcEncodeRecordEntity findByCodeAndType(String orgCode,String orgType){
		return repository.findByCodeAndType(orgCode, orgType);
	};
	
	/**
	 * 生成人员编制信息编码
	 * @param orgCode
	 * @param EmpType
	 * @return
	 */
	public String createEmpEnCode(String orgCode,String empType){
		return repository.createEmpEnCode(orgCode, empType);
	}

}
