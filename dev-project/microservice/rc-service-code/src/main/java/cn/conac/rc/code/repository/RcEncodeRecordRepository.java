package cn.conac.rc.code.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.code.entity.RcEncodeRecordEntity;
import cn.conac.rc.code.entity.RcEncodeRecordRemoveEntity;
import cn.conac.rc.framework.repository.GenericDao;

/**
 * RcEncodeRecordRepository类
 *
 * @author repositoryCreator
 * @date 2017-06-21
 * @version 1.0
 */
@Repository
public interface RcEncodeRecordRepository extends GenericDao<RcEncodeRecordEntity, Integer> {
	/**
	 * 通过findByCodeAndType查找可用编码
	 * @param findByCodeAndType
	 * @return
	 */
	@Query(value="select t.* from RC_ENCODE_RECORD t where t.ORG_CODE=:orgCode and t.ORG_TYPE=:orgType and rownum=1",nativeQuery = true)
	public RcEncodeRecordEntity findByCodeAndType(@Param("orgCode") String orgCode,@Param("orgType") String orgType);
	
	/**
	 * 生成人员编制信息编码
	 * @param orgCode
	 * @param EmpType
	 * @return
	 */
	@Query(value = "select 'R' || :orgCode || :empType || to_char(lpad((select (case when (SELECT COUNT(*) FROM RC_ENCODE_RECORD WHERE ORG_CODE=:orgCode and ORG_TYPE='R')=0 then 0 else (SELECT CODE_NUM FROM RC_ENCODE_RECORD WHERE ORG_CODE=:orgCode and ORG_TYPE='R' and rownum=1)+1 end) from DUAL),5,0)) from dual", nativeQuery = true)
	public String createEmpEnCode(@Param("orgCode") String orgCode,@Param("empType") String empType);	
}
