package cn.conac.rc.code.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.code.entity.RcOrgEncodeRecordEntity;
import cn.conac.rc.code.repository.RcOrgEncodeRecordRepository;
import cn.conac.rc.code.vo.RcOrgEncodeRecordVo;
import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;

/**
 * RcOrgEncodeRecordService类
 *
 * @author serviceCreator
 * @date 2016-11-23
 * @version 1.0
 */
@Service
public class RcOrgEncodeRecordService extends GenericService<RcOrgEncodeRecordEntity, String> {

	@Autowired
	private RcOrgEncodeRecordRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(RcOrgEncodeRecordVo vo){
		return super.count(this.createCriteria(vo));
	}
	
	/**
	 * 生成机构编码
	 * @param findId
	 * @return
	 */
	public String createOrgCode(String findId,String orgType){		
		return repository.createOrgCode(findId,orgType);
	}
	
	/**
	 * 通过findId查找当前部门编码
	 * @param findId
	 * @return
	 */
	public RcOrgEncodeRecordEntity findByFindId(String findId){		
		return repository.findByFindId(findId);
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<RcOrgEncodeRecordEntity> list(RcOrgEncodeRecordVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<RcOrgEncodeRecordEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<RcOrgEncodeRecordEntity> createCriteria(RcOrgEncodeRecordVo param) {
		Criteria<RcOrgEncodeRecordEntity> dc = new Criteria<RcOrgEncodeRecordEntity>();
		// TODO 具体条件赋值

		return dc;
	}
	
	/**
	 * 通过findId查找当前部门编码
	 * @param findId
	 * @return
	 */
	public void deleteByFindId(String findId){	
		if( null != repository.findByFindId(findId)) {
			repository.deleteByFindId(findId);
		}
	}

}
