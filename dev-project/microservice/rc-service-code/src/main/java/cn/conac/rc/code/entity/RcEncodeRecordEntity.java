package cn.conac.rc.code.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * RcEncodeRecordEntity类
 *
 * @author beanCreator
 * @date 2017-06-21
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="RC_ENCODE_RECORD")
public class RcEncodeRecordEntity implements Serializable {

	private static final long serialVersionUID = 1498028193172935391L;

	@Id
	@ApiModelProperty("id号")
	private String id;

	@ApiModelProperty("机构基本码")
	private String orgCode;

	@ApiModelProperty("N:内设机构编码Z:职能职责编码R:人员编制信息编码L:领导职数信息编码")
	private String orgType;

	@ApiModelProperty("根据org_type区分长度；NZ:3 R:5 L:2")
	private String codeNum;

	public void setId(String id){
		this.id=id;
	}

	public String getId(){
		return id;
	}

	public void setOrgCode(String orgCode){
		this.orgCode=orgCode;
	}

	public String getOrgCode(){
		return orgCode;
	}

	public void setOrgType(String orgType){
		this.orgType=orgType;
	}

	public String getOrgType(){
		return orgType;
	}

	public void setCodeNum(String codeNum){
		this.codeNum=codeNum;
	}

	public String getCodeNum(){
		return codeNum;
	}

}
