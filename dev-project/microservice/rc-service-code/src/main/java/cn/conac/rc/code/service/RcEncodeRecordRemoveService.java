package cn.conac.rc.code.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import cn.conac.rc.code.entity.RcEncodeRecordRemoveEntity;
import cn.conac.rc.code.repository.RcEncodeRecordRemoveRepository;
import cn.conac.rc.code.vo.RcEncodeRecordRemoveVo;
import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;

/**
 * RcEncodeRecordRemoveService类
 *
 * @author serviceCreator
 * @date 2017-06-21
 * @version 1.0
 */
@Service
public class RcEncodeRecordRemoveService extends GenericService<RcEncodeRecordRemoveEntity, Integer> {

	@Autowired
	private RcEncodeRecordRemoveRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(RcEncodeRecordRemoveVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<RcEncodeRecordRemoveEntity> list(RcEncodeRecordRemoveVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<RcEncodeRecordRemoveEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<RcEncodeRecordRemoveEntity> createCriteria(RcEncodeRecordRemoveVo param) {
		Criteria<RcEncodeRecordRemoveEntity> dc = new Criteria<RcEncodeRecordRemoveEntity>();
		// TODO 具体条件赋值

		return dc;
	}
	
	/**
	 * 通过findByCodeAndType查找回收池可用编码
	 * @param findByCodeAndType
	 * @return
	 */
	public RcEncodeRecordRemoveEntity findByCodeAndType(String orgCode,String orgType){
		return repository.findByCodeAndType(orgCode, orgType);
	};
	
	/**
	 * 通过findByCodeTypeAndNum查找回收池可用编码
	 * @param findByCodeTypeAndNum
	 * @return
	 */
	public RcEncodeRecordRemoveEntity findByCodeTypeAndNum(String orgCode,String orgType,String codeNum){
		return repository.findByCodeTypeAndNum(orgCode, orgType, codeNum);
	}

}
