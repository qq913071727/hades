package cn.conac.rc.code.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * RcEncodeRecordRemoveEntity类
 *
 * @author beanCreator
 * @date 2017-06-21
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="RC_ENCODE_RECORD_REMOVE")
public class RcEncodeRecordRemoveEntity implements Serializable {

	private static final long serialVersionUID = 14980281932544685L;

	@Id
	@ApiModelProperty("id")
	private String id;

	@ApiModelProperty("orgCode")
	private String orgCode;

	@ApiModelProperty("orgType")
	private String orgType;

	@ApiModelProperty("codeNum")
	private String codeNum;

	public void setId(String id){
		this.id=id;
	}

	public String getId(){
		return id;
	}

	public void setOrgCode(String orgCode){
		this.orgCode=orgCode;
	}

	public String getOrgCode(){
		return orgCode;
	}

	public void setOrgType(String orgType){
		this.orgType=orgType;
	}

	public String getOrgType(){
		return orgType;
	}

	public void setCodeNum(String codeNum){
		this.codeNum=codeNum;
	}

	public String getCodeNum(){
		return codeNum;
	}

}
