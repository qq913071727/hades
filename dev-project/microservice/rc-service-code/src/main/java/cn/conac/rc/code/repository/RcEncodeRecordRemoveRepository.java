package cn.conac.rc.code.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.code.entity.RcEncodeRecordRemoveEntity;
import cn.conac.rc.code.entity.RcOrgEncodeRecordEntity;
import cn.conac.rc.framework.repository.GenericDao;

/**
 * RcEncodeRecordRemoveRepository类
 *
 * @author repositoryCreator
 * @date 2017-06-21
 * @version 1.0
 */
@Repository
public interface RcEncodeRecordRemoveRepository extends GenericDao<RcEncodeRecordRemoveEntity, Integer> {
	
	/**
	 * 通过findByCodeAndType查找回收池可用编码
	 * @param findByCodeAndType
	 * @return
	 */
	@Query(value="select f.* from (select t.* from RC_ENCODE_RECORD_REMOVE t where t.ORG_CODE=:orgCode and t.ORG_TYPE=:orgType order by to_number(t.CODE_NUM) asc) f where rownum=1",nativeQuery = true)
	public RcEncodeRecordRemoveEntity findByCodeAndType(@Param("orgCode") String orgCode,@Param("orgType") String orgType);
	
	/**
	 * 通过findByCodeTypeAndNum查找回收池可用编码
	 * @param findByCodeTypeAndNum
	 * @return
	 */
	@Query(value="select t.* from RC_ENCODE_RECORD_REMOVE t where t.ORG_CODE=:orgCode and t.ORG_TYPE=:orgType and t.CODE_NUM =:codeNum and rownum=1",nativeQuery = true)
	public RcEncodeRecordRemoveEntity findByCodeTypeAndNum(@Param("orgCode") String orgCode,@Param("orgType") String orgType,@Param("codeNum") String codeNum);
}
