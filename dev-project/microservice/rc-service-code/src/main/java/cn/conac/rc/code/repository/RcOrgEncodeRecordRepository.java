package cn.conac.rc.code.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.code.entity.RcOrgEncodeRecordEntity;
import cn.conac.rc.framework.repository.GenericDao;

/**
 * RcOrgEncodeRecordRepository类
 *
 * @author repositoryCreator
 * @date 2016-11-23
 * @version 1.0
 */
@Repository
public interface RcOrgEncodeRecordRepository extends GenericDao<RcOrgEncodeRecordEntity, String> {
	
	/**
	 * 生成部门编码
	 * @param findId
	 * @return
	 */
	@Query(value = "select :findId || :orgType || substr('0000',1,4-length((select (case when (SELECT COUNT(*) FROM RC_ORG_ENCODE_RECORD WHERE F_ID=:findId)=0 then '0000'+0 else (SELECT CODE_NUM FROM RC_ORG_ENCODE_RECORD WHERE F_ID=:findId)+1 end) from DUAL)))||to_char((select (case when (SELECT COUNT(*) FROM RC_ORG_ENCODE_RECORD WHERE F_ID=:findId)=0 then '0000'+0 else (SELECT CODE_NUM FROM RC_ORG_ENCODE_RECORD WHERE F_ID=:findId)+1 end) from DUAL)) from DUAL", nativeQuery = true)
	public String createOrgCode(@Param("findId") String findId,@Param("orgType") String orgType);	
	
	/**
	 * 通过findId查找当前部门编码
	 * @param findId
	 * @return
	 */
	@Query("select bean from RcOrgEncodeRecordEntity bean where bean.fId=:findId")
	public RcOrgEncodeRecordEntity findByFindId(@Param("findId") String findId);
	
	
	/**
	 * 通过findId删除当前部门编码
	 * @param findId
	 * @return void
	 */
	@Query(value ="delete from RC_ORG_ENCODE_RECORD code where code.f_id =:findId", nativeQuery = true)
	@Modifying
	public void deleteByFindId(@Param("findId") String findId);
}
