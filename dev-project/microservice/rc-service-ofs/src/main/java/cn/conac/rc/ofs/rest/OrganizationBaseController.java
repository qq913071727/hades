package cn.conac.rc.ofs.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.ofs.entity.OrganizationBaseEntity;
import cn.conac.rc.ofs.service.ApprovalService;
import cn.conac.rc.ofs.service.DepartmentService;
import cn.conac.rc.ofs.service.DutyService;
import cn.conac.rc.ofs.service.OrgEmpService;
import cn.conac.rc.ofs.service.OrgOthersService;
import cn.conac.rc.ofs.service.OrgStatusService;
import cn.conac.rc.ofs.service.OrganizationBaseService;
import cn.conac.rc.ofs.vo.OrganizationBaseVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="organizationBase/")
public class OrganizationBaseController {

	@Autowired
	OrganizationBaseService orgBaseService;
	
	@Autowired
	ApprovalService appService;

	@Autowired
	DepartmentService departService;
	
	@Autowired
	DutyService dutyService;
	
	@Autowired
	OrgEmpService orgEmpService;
	
	@Autowired
	OrgOthersService orgOthersService;
	
	@Autowired
	OrgStatusService orgStatusService;

	@ApiOperation(value = "根据部门基本表ID取得部门基本信息", httpMethod = "GET", response = OrganizationBaseEntity.class, notes = "根据部门基本表ID取得部门基本信息")
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findOrgDetailById(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		OrganizationBaseEntity organizationBase = orgBaseService.findById(id);
		if(null == organizationBase) {
			// 结果集设定
			result.setCode(ResultPojo.CODE_NULL_ERR);
			result.setMsg(ResultPojo.MSG_NULL_ERR);
		} else {
			// 结果集设定
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setResult(organizationBase);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		}
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "根据部门库ID取得部门基本信息", httpMethod = "GET", response = OrganizationBaseEntity.class, notes = "根据部门基本表ID取得部门基本信息")
	@RequestMapping(value = "orgnization/{orgId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findOrgDetailByOrgId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "orgId", required = true) @PathVariable("orgId") Integer orgId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		OrganizationBaseEntity organizationBase = orgBaseService.findByOrgId(orgId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(organizationBase);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "个数", httpMethod = "POST", response = Long.class, notes = "根据条件获得资源数量")
	@RequestMapping(value = "count", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> count(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody OrganizationBaseVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		long cnt = orgBaseService.count(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(cnt);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "列表", httpMethod = "POST", response = OrganizationBaseEntity.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "page", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> pageList(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody OrganizationBaseVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<OrganizationBaseEntity> list = orgBaseService.pageList(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "新增或者更新数据", httpMethod = "POST", response = OrganizationBaseEntity.class, notes = "保存资源到数据库")
	@RequestMapping(value = "{submitType}", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> save(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "提交类别", required = true) @PathVariable("submitType") String submitType,
			@ApiParam(value = "保存对象", required = true) @RequestBody OrganizationBaseEntity entity) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		if (!"0".equals(submitType)){
			// 数据格式校验
			String valMsg = orgBaseService.validate(entity);
			if (valMsg != null) {
				result.setCode(ResultPojo.CODE_FORMAT_ERR);
				result.setMsg(valMsg);
				return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
			}
		}
		orgBaseService.save(entity);

		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "物理删除", httpMethod = "POST", response = OrganizationBaseEntity.class, notes = "根据id物理删除资源")
	@RequestMapping(value = "{id}/delete", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> delete(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		orgBaseService.delete(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(id);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "待提交列表", httpMethod = "GET", response = OrganizationBaseEntity.class, notes = "根据条件获得待提交列表")
	@RequestMapping(value = "{areaId}/preRealseList", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> preReaseList(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "areaId", required = true) @PathVariable("areaId") String areaId) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<OrganizationBaseEntity> baseList = orgBaseService.preRealseList(areaId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(baseList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
