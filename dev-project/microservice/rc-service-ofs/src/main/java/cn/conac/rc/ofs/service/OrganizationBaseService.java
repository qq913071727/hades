package cn.conac.rc.ofs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.framework.utils.BeanMapper;
import cn.conac.rc.ofs.entity.OrganizationBaseEntity;
import cn.conac.rc.ofs.repository.OrganizationBaseRepository;
import cn.conac.rc.ofs.vo.OrganizationBaseVo;

@Service
public class OrganizationBaseService extends GenericService<OrganizationBaseEntity, Integer> {

	@Autowired
	private OrganizationBaseRepository orgBaseRepository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(OrganizationBaseVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<OrganizationBaseEntity> pageList(OrganizationBaseVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<OrganizationBaseEntity> dc = this.createCriteria(vo);
			return orgBaseRepository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	
	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public List<OrganizationBaseEntity> preRealseList(String areaId) throws Exception {
		try {
			return orgBaseRepository.findPreRealseList(areaId);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<OrganizationBaseEntity> createCriteria(OrganizationBaseVo param) {
		Criteria<OrganizationBaseEntity> dc = new Criteria<OrganizationBaseEntity>();
		// TODO 具体条件赋值

		return dc;
	}

	/**
	 * 通过orgId找到当前OrgBase表信息
	 * @param orgId
	 * @return
	 */
	public OrganizationBaseEntity findByOrgId(Integer orgId){
		return orgBaseRepository.findByOrgId(orgId);
	}
	
	/**
	 * 通过orgId复制OrgBase表信息
	 * @param orgId
	 * @return
	 */
	public OrganizationBaseEntity copyOrgBaseInfo(Integer orgId){
		// 获取旧Base表信息
		OrganizationBaseEntity orgBaseEntity = orgBaseRepository.findByOrgId(orgId);
		// 判断为空
		if(orgBaseEntity == null){
			return null;
		}
		// 复制Base表信息
		OrganizationBaseEntity copyBaseEntity = new OrganizationBaseEntity();
		BeanMapper.copy(orgBaseEntity,copyBaseEntity);
		copyBaseEntity.setId(null);
		// 保存新Base信息并返回
		return orgBaseRepository.save(copyBaseEntity);
	}
	
	/**
	 * 通过BaseId复制OrgBase表信息
	 * @param orgId
	 * @return
	 */
	public OrganizationBaseEntity copyOrgBaseInfoByBaseId(Integer oldbaseId){
		// 获取旧Base表信息
		OrganizationBaseEntity orgBaseEntity = this.findById(oldbaseId);
		// 判断为空
		if(orgBaseEntity == null){
			return null;
		}
		// 复制Base表信息
		OrganizationBaseEntity copyBaseEntity = new OrganizationBaseEntity();
		BeanMapper.copy(orgBaseEntity,copyBaseEntity);
		copyBaseEntity.setId(null);
		// 保存新Base信息并返回
		return orgBaseRepository.save(copyBaseEntity);
	}

	/**
	 * 通过orgId找到当前OrgBase表信息
	 * @param orgId
	 * @return
	 */
	public List<OrganizationBaseEntity> findListByOrgId(Integer orgId){
		return orgBaseRepository.findListByOrgId(orgId);
	}
	
	
//	/**
//	 * 更新处理
//	 * @param id
//	 * @param entity
//	 * @return
//	 */
//	public OrganizationBaseEntity update(Integer id, OrganizationBaseEntity entity){
//		// 检索要更新的数据信息
//		OrganizationBaseEntity targetEntity = orgBaseRepository.findOne(id);
//		// 空判断
//		if(targetEntity == null){
//			return null;
//		}
//		// 复制表信息
//		BeanMapper.copy(entity,targetEntity,new String[]{"id","orgId","code"});
//		// 保存信息并返回
//		return orgBaseRepository.save(targetEntity);
//	}

}
