package cn.conac.rc.ofs.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.conac.rc.ofs.entity.DepartmentEntity2;

public class DepartmentTreeNodeVo2 implements Serializable{

	private static final long serialVersionUID = -5093237370151154695L;
	//ID
	private String id;
	//机构ID号
	private Integer baseId;
	//部门序号
	private Integer depNum;
	//部门名称
	private String name;
	//上级部门ID号
	private Integer parentId;
	//层级
	private Integer level;
//	//部门类型
//	private String deptFuncType;
	//部门职能
	private String deptDesc;
//	//备注
//	private String remarks;
//	//deptLevel
//	private Integer deptLevel;
//	//depCode
//	private String depCode;
	// 显示组织架构图中是否可以点击标示
	private String hrefFlag;
	
	// 内设机构选择上级机构使用
	private String type;
	
	private List<DepartmentTreeNodeVo2> children = new ArrayList<DepartmentTreeNodeVo2>();

	public DepartmentTreeNodeVo2(){}
	
	public DepartmentTreeNodeVo2(DepartmentEntity2 entity, String hrefFlag){
		this.id = entity.getId().toString();
		this.baseId =entity.getBaseId();
		this.depNum = entity.getDepNum();
		this.name = entity.getDeptName();
		this.parentId = entity.getParentId();
		this.deptDesc = entity.getDeptDesc();
		this.hrefFlag = hrefFlag;
		this.type = "2";
	}

	public DepartmentTreeNodeVo2(DepartmentEntity2 entity){
		this.id = entity.getId().toString();
		this.baseId =entity.getBaseId();
		this.depNum = entity.getDepNum();
		this.name = entity.getDeptName();
		this.parentId = entity.getParentId();
		this.deptDesc = entity.getDeptDesc();
	}
	
	public List<DepartmentTreeNodeVo2> getChildren() {
		return children;
	}

	public void setChildren(List<DepartmentTreeNodeVo2> children) {
		this.children = children;
	}
	
	public void addChild(DepartmentTreeNodeVo2 child) {
		if(null == children){
			children = new ArrayList<DepartmentTreeNodeVo2>();
		}
		children.add(child);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getBaseId() {
		return baseId;
	}

	public void setBaseId(Integer baseId) {
		this.baseId = baseId;
	}

	public Integer getDepNum() {
		return depNum;
	}

	public void setDepNum(Integer depNum) {
		this.depNum = depNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getHrefFlag() {
		return hrefFlag;
	}

	public void setHrefFlag(String hrefFlag) {
		this.hrefFlag = hrefFlag;
	}

	public String getDeptDesc() {
		return deptDesc;
	}

	public void setDeptDesc(String deptDesc) {
		this.deptDesc = deptDesc;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
