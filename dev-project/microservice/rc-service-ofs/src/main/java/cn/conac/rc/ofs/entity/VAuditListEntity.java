package cn.conac.rc.ofs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * VAuditListEntity类
 *
 * @author beanCreator
 * @date 2016-11-21
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="RCO_V_AUDIT_LIST")
public class VAuditListEntity implements Serializable {

	private static final long serialVersionUID = 1479698647903986011L;
	@Id
	@ApiModelProperty("机构baseId")
	private Integer id;
	
	@ApiModelProperty("机构ID")
	private Integer orgId;

	@ApiModelProperty("部门名称")
	private String name;
	
	@ApiModelProperty("部门类别")
	private String type;

	@ApiModelProperty("所属系统")
	private String ownSys;

	@ApiModelProperty("主管部门")
	private String compName;

	@ApiModelProperty("经办部门")
	private String userOrgName;

	@ApiModelProperty("所属用户")
	private String userName;

	@ApiModelProperty("提交时间")
	private Date updateTime;

	@ApiModelProperty("变更类型")
	private String updateType;

	@ApiModelProperty("部门状态")
	private String status;

	@ApiModelProperty("审批意见")
	private String orgOpinions;
	
	@ApiModelProperty("审核时间")
	private Date auditTime;
	
	@ApiModelProperty("所属地区")
	private String areaId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setOwnSys(String ownSys){
		this.ownSys=ownSys;
	}

	public String getOwnSys(){
		return ownSys;
	}

	public void setCompName(String compName){
		this.compName=compName;
	}

	public String getCompName(){
		return compName;
	}

	public void setUserOrgName(String userOrgName){
		this.userOrgName=userOrgName;
	}

	public String getUserOrgName(){
		return userOrgName;
	}

	public void setUserName(String userName){
		this.userName=userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime=updateTime;
	}

	public Date getUpdateTime(){
		return updateTime;
	}

	public void setUpdateType(String updateType){
		this.updateType=updateType;
	}

	public String getUpdateType(){
		return updateType;
	}

	public void setStatus(String status){
		this.status=status;
	}

	public String getStatus(){
		return status;
	}

	public String getOrgOpinions() {
		return orgOpinions;
	}

	public void setOrgOpinions(String orgOpinions) {
		this.orgOpinions = orgOpinions;
	}

	public void setAuditTime(Date auditTime){
		this.auditTime=auditTime;
	}

	public Date getAuditTime(){
		return auditTime;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

}
