package cn.conac.rc.ofs.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.conac.rc.ofs.entity.OrganizationEntity;

public class OrganizationTreeNodeVo2 implements Serializable{

	private static final long serialVersionUID = -8720170768316645969L;
	
	private String id;
	private String baseId;
	private String hasChild;
	private String areaCode;
	private String areaId;
	private String code;
	private String isComp;
	private String isSecret;
	private String localCode;
	private String name;
	private String ownSys;
	private Integer parentId;
	private Integer sort;
	private String status;
	private String type;
	private String userId;
	private List<OrganizationTreeNodeVo2> children = new  ArrayList<OrganizationTreeNodeVo2>();

	public OrganizationTreeNodeVo2(){}
	
	public OrganizationTreeNodeVo2(OrganizationEntity entity){
		this.setId(entity.getId().toString());
		this.setAreaCode(entity.getAreaCode());
		this.setAreaId(entity.getAreaId());
		this.setBaseId(entity.getBaseId()!=null?entity.getBaseId().toString():"");
		this.setCode(entity.getCode());
		this.setIsComp(entity.getIsComp());
		this.setIsSecret(entity.getIsSecret());
		this.setLocalCode(entity.getLocalCode());
		this.setName(entity.getName());
		this.setOwnSys(entity.getOwnSys());
		this.setParentId(entity.getParentId());
		this.setSort(entity.getSort());
		this.setStatus(entity.getStatus());
		this.setType(entity.getType());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBaseId() {
		return baseId;
	}

	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}

	public String getHasChild() {
		return hasChild;
	}

	public void setHasChild(String hasChild) {
		this.hasChild = hasChild;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getIsComp() {
		return isComp;
	}

	public void setIsComp(String isComp) {
		this.isComp = isComp;
	}

	public String getIsSecret() {
		return isSecret;
	}

	public void setIsSecret(String isSecret) {
		this.isSecret = isSecret;
	}

	public String getLocalCode() {
		return localCode;
	}

	public void setLocalCode(String localCode) {
		this.localCode = localCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwnSys() {
		return ownSys;
	}

	public void setOwnSys(String ownSys) {
		this.ownSys = ownSys;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<OrganizationTreeNodeVo2> getChildren() {
		return children;
	}

	public void setChildren(List<OrganizationTreeNodeVo2> children) {
		this.children = children;
	}
	
	public void addChild(OrganizationTreeNodeVo2 child){
		children.add(child);
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
