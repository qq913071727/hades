package cn.conac.rc.ofs.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.conac.rc.ofs.entity.OrganizationEntity;

public class OrganizationTreeNodeVo3 implements Serializable{

	private static final long serialVersionUID = -8720170768316645969L;
	
	private String id;
	private String name;
	// 默认值为“0”
	private String hrefFlag = "0";
	private Integer parentId;
	private Integer sort;
	private String status;
	private String type;

	private List<OrganizationTreeNodeVo3> children = new  ArrayList<OrganizationTreeNodeVo3>();

	public OrganizationTreeNodeVo3(){}
	
	public OrganizationTreeNodeVo3(OrganizationEntity entity){
		this.setId(entity.getId().toString());
		this.setName(entity.getName());
		this.setParentId(entity.getParentId());
		this.setSort(entity.getSort());
		this.setStatus(entity.getStatus());
		this.setType(entity.getType());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHrefFlag() {
		return hrefFlag;
	}

	public void setHrefFlag(String hrefFlag) {
		this.hrefFlag = hrefFlag;
	}
	
	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public List<OrganizationTreeNodeVo3> getChildren() {
		return children;
	}

	public void setChildren(List<OrganizationTreeNodeVo3> children) {
		this.children = children;
	}
	
	public void addChild(OrganizationTreeNodeVo3 child){
		children.add(child);
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
