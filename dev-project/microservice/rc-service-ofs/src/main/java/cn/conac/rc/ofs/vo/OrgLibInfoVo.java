package cn.conac.rc.ofs.vo;

import javax.persistence.Transient;

import cn.conac.rc.ofs.entity.OrgLibInfoEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * OfsOrgLibInfoVo类
 *
 * @author voCreator
 * @date 2017-07-10
 * @version 1.0
 */
@ApiModel
public class OrgLibInfoVo extends OrgLibInfoEntity {

	private static final long serialVersionUID = 1499669556176949679L;

	@Transient
	@ApiModelProperty("当前分页")
	private Integer page;

	@Transient
	@ApiModelProperty("每页个数")
	private Integer size;

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}
}
