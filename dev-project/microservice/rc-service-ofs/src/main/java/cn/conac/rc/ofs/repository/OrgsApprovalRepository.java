package cn.conac.rc.ofs.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.OrgsApprovalEntity;

@Repository
public interface OrgsApprovalRepository extends GenericDao<OrgsApprovalEntity, String> {

}
