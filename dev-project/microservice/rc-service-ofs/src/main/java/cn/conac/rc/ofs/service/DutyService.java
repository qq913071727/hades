package cn.conac.rc.ofs.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.framework.utils.BeanMapper;
import cn.conac.rc.ofs.entity.DutyEntity;
import cn.conac.rc.ofs.repository.DutyRepository;
import cn.conac.rc.ofs.vo.DutyVo;

@Service
@Transactional
public class DutyService extends GenericService<DutyEntity, Integer> {

	@Autowired
	private DutyRepository repository;

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<DutyEntity> list(DutyVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "dutyNumber");// 默认用职责序号排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<DutyEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<DutyEntity> createCriteria(DutyVo param) {
		Criteria<DutyEntity> dc = new Criteria<DutyEntity>();
		// 创建时间Start
		if (param.getCreateDateStart() != null) {
            dc.add(Restrictions.gte("createDate", param.getCreateDateStart(), true));
        }
		// 创建时间End
		if (param.getCreateDateEnd() != null) {
            dc.add(Restrictions.gte("createDate", param.getCreateDateEnd(), true));
        }
		// 职责编码
		if (param.getBaseId() != null) {
            dc.add(Restrictions.eq("orgId", param.getBaseId(), true));
        }
		// 职责编码
		if (StringUtils.isNotBlank(param.getDutyCode())) {
            dc.add(Restrictions.eq("dutyCode", param.getDutyCode(), true));
        }
		// 职能职责内容
		if (StringUtils.isNotBlank(param.getDutyContent())) {
            dc.add(Restrictions.eq("dutyContent", param.getDutyContent(), true));
        }

		return dc;
	}
	
	/**
	 * 根据baseId查询该机构的所有职责信息
	 * @param baseId
	 * @return DutyEntity的List
	 */
	public List<DutyEntity> findByBaseId(Integer baseId){
		return repository.findByBaseIdOrderByDutyNumberAsc(baseId);
	}

	/**
	 * 根据departId查询该内设机构对应的的所有职责信息(并按照职责顺序号升序排序)
	 * @param departId 内设机构Id
	 * @return DutyEntity的List
	 */
	public List<DutyEntity> findByDepatId(Integer departId){
		return repository.findByDepatId(departId);
	}
	
	/**
	 * 根据部门库Id获取没有关联内设的职能职责
	 * @return
	 */
	public List<DutyEntity> findDutyWithNoDutyListByOrgId(Integer id){
		return repository.findDutyWithNoDutyListByOrgId(id);
	}
	
	/**
	 * 通过BaseId将旧的职能职责复制给新的职能职责
	 * @param oldBaseId
	 * @param newBaseId
	 * @return List<DutyEntity>
	 */
	public List<DutyEntity> copyDutyListWithOrgId(Integer oldBaseId, Integer newBaseId){
		// 通过旧BaseId获取职能职责列表
		List<DutyEntity> oldDutyList = repository.findByBaseIdOrderByDutyNumberAsc(oldBaseId);
		List<DutyEntity> newDutyList = new ArrayList<>();
		// 判断旧列表是否为空
		if(oldDutyList == null || oldDutyList.size() ==0 ){
			return null;
		}
		// 循环复制职能职责，并新建
		for(DutyEntity oldDuty : oldDutyList){
			DutyEntity newDuty = new DutyEntity();
			BeanMapper.copy(oldDuty, newDuty);
			newDuty.setId(null);
			newDuty.setBaseId(newBaseId);
			newDuty.setOldId4Copy(oldDuty.getId());
			newDutyList.add(repository.save(newDuty));
		}
		// 返回新建职能职责列表信息
		return newDutyList;
	}

	/**
	 * 通过dutyId将上级部门的职能职责复制给新的职能职责
	 * @param dutyIds
	 * @param baseId
	 * @return List<DutyEntity>
	 */
	public List<DutyEntity> copyDutyList(List<String> dutyIds, Integer baseId){
		// 复制后新的DutyList
		List<DutyEntity> newDutyList = new ArrayList<>();
		// 通过旧BaseId获取职能职责列表
		for(String dutyId : dutyIds){
			DutyEntity dutyEntity = repository.findOne(new Integer(dutyId));

			DutyEntity newDuty = new DutyEntity();
			BeanMapper.copy(dutyEntity, newDuty);
			newDuty.setId(null);
			newDuty.setBaseId(baseId);
			newDuty.setOldId4Copy(dutyEntity.getId());
			newDutyList.add(repository.save(newDuty));
		}
		// 返回新建职能职责列表信息
		return newDutyList;
	}

	/**
	 * 根据部门库Id获取职能职责最大DutyNum，没有的情况返回0
	 * @return
	 */
	public int findMaxDutyNumber(Integer id){
		// 获取最大职能职责DutyNumber
		return repository.findMaxDutyNumberByOrgId(id);
	}

	//▼▼▼▼▼▼▼▼权责用DAO▼▼▼▼▼▼▼▼
	public List<DutyEntity> findDutyBy(String orgId){
		return repository.findDutyBy(orgId);
	}
	
	public DutyEntity[] findByCodes(String[] codes, Integer orgId) {
		int len = (codes != null) ? codes.length : 0;
		List<DutyEntity> beans = new ArrayList<DutyEntity>();
		for (int i = 0; i < len; i++) {
			String code = codes[i];
			if (StringUtils.isNotBlank(code)) {
				List<DutyEntity> codeList = repository.findByCode(code, orgId);
				if (null != codeList) {
					beans.addAll(codeList);
				}
			}
		}
		DutyEntity[] dutys = new DutyEntity[beans.size()];
		beans.toArray(dutys);
		return dutys;
	}

	public DutyEntity findByCode(String dutyCode, Integer orgId) {
		List<DutyEntity> list = repository.findByCode(dutyCode, orgId);
		if (null != list && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}
	
	public List<DutyEntity> findByItemIdAndOrgId(Integer itemId, Integer orgId){
		return repository.findByItemIdAndOrgId(itemId,orgId);
	}
	//▲▲▲▲▲▲▲▲权责用DAO▲▲▲▲▲▲▲▲
}
