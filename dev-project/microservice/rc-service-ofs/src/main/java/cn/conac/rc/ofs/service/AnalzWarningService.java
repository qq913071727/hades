package cn.conac.rc.ofs.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.ofs.entity.AnalzWarningEntity;
import cn.conac.rc.ofs.entity.OrganizationEntity;
import cn.conac.rc.ofs.repository.AnalzWarningRepository;
import cn.conac.rc.ofs.repository.OrganizationRepository;
import cn.conac.rc.ofs.vo.AnalzWarningVo;

/**
 * 三定预警分析
 *
 */
@Service
public class AnalzWarningService extends GenericService<AnalzWarningEntity, Integer> {

	@Autowired
	private AnalzWarningRepository repository;
	@Autowired
	private OrganizationRepository orgRepository;

	/**
	 * 计数查询
	 * 
	 * @param vo
	 * @return 计数结果
	 */
	public long count(AnalzWarningVo vo) {
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * 
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<AnalzWarningEntity> list(AnalzWarningVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<AnalzWarningEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * 
	 * @param param
	 * @return Criteria
	 */
	private Criteria<AnalzWarningEntity> createCriteria(AnalzWarningVo param) {
		Criteria<AnalzWarningEntity> dc = new Criteria<AnalzWarningEntity>();
		if(param.getAreaCode() != null){
			dc.add(Restrictions.eq("areaCode", param.getAreaCode(), true));
		}
		if(param.getOrgName() != null){
			dc.add(Restrictions.like("orgName", "%"+param.getOrgName()+"%", true));
		}
		// 创建时间Start
		if (param.getCreateDateStart() != null) {
            dc.add(Restrictions.gte("createDate", param.getCreateDateStart(), true));
        }
		// 创建时间End
		if (param.getCreateDateEnd() != null) {
            dc.add(Restrictions.lt("createDate", param.getCreateDateEnd(), true));
        }
		return dc;
	}

	public void scheduler() {
		// 清空原数据并重新计算
		repository.deleteAll();

		Map<Integer, AnalzWarningEntity> warningMaps = new HashMap<Integer, AnalzWarningEntity>();
		List<OrganizationEntity> orgIdsWithNoDept = orgRepository.findOrgIdWithNoDeptList();
		for (OrganizationEntity orgIdObj : orgIdsWithNoDept) {
			OrganizationEntity orgId = (OrganizationEntity) orgIdObj;
			if (!warningMaps.containsKey(orgId.getId())) {
				AnalzWarningEntity entity = new AnalzWarningEntity();
				entity.setOrgId(orgId.getId());
				entity.setOrgName(orgId.getName());
				entity.setAreaCode(orgId.getAreaCode());
				warningMaps.put(orgId.getId(), entity);
			}
			warningMaps.get(orgId.getId()).setWarningDept(Boolean.TRUE);
		}
		List<OrganizationEntity> orgIdsWithNoDuty = orgRepository.findOrgIdWithNoDutyList();
		for (OrganizationEntity orgIdObj : orgIdsWithNoDuty) {
			if (!warningMaps.containsKey(orgIdObj.getId())) {
				AnalzWarningEntity entity = new AnalzWarningEntity();
				entity.setOrgId(orgIdObj.getId());
				entity.setOrgName(orgIdObj.getName());
				entity.setAreaCode(orgIdObj.getAreaCode());
				warningMaps.put(orgIdObj.getId(), entity);
			}
			warningMaps.get(orgIdObj.getId()).setWarningDuty(Boolean.TRUE);
		}
		// List<OrganizationEntity> orgIdsWithNoItem =
		// orgRepository.findOrgIdWithNoItemList();
		// for (OrganizationEntity orgIdObj : orgIdsWithNoItem) {
		// if (!warningMaps.containsKey(orgIdObj.getId())) {
		// AnalzWarningEntity entity = new AnalzWarningEntity();
		// entity.setOrgId(orgId.getId());
		// entity.setAreaCode(orgIdObj.getAreaCode());
		// warningMaps.put(orgIdObj.getId(), entity);
		// }
		// warningMaps.get(orgIdObj.getId()).setWarningEmpRemarks(Boolean.TRUE);
		// }
		List<OrganizationEntity> orgIdsWithNoOLName = orgRepository.findOrgIdWithNoOLNameList();
		for (OrganizationEntity orgIdObj : orgIdsWithNoOLName) {
			if (!warningMaps.containsKey(orgIdObj.getId())) {
				AnalzWarningEntity entity = new AnalzWarningEntity();
				entity.setOrgId(orgIdObj.getId());
				entity.setOrgName(orgIdObj.getName());
				entity.setAreaCode(orgIdObj.getAreaCode());
				warningMaps.put(orgIdObj.getId(), entity);
			}
			warningMaps.get(orgIdObj.getId()).setWarningOnlineName(Boolean.TRUE);
		}
		for (AnalzWarningEntity warning : warningMaps.values()) {
			repository.save(warning);
		}
	}
}
