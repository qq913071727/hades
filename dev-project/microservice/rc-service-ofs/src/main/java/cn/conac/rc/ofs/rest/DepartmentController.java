package cn.conac.rc.ofs.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.utils.StringUtils;
import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.ofs.entity.DepartmentEntity;
import cn.conac.rc.ofs.entity.OrganizationBaseEntity;
import cn.conac.rc.ofs.service.DepartmentService;
import cn.conac.rc.ofs.service.OrganizationBaseService;
import cn.conac.rc.ofs.vo.DepartmentTreeNodeVo2;
import cn.conac.rc.ofs.vo.DepartmentVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="department/")
public class DepartmentController {

	@Autowired
	DepartmentService service;
	
	@Autowired
	OrganizationBaseService organizationBaseService;

	@ApiOperation(value = "详情", httpMethod = "GET", response = DepartmentEntity.class, notes = "根据id获取资源详情")
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		DepartmentEntity department = service.findById(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(department);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "获取部门列表", httpMethod = "GET", response = DepartmentEntity.class, notes = "根据dutyId获取部门列表")
	@RequestMapping(value = "correlation/{dutyId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> departListWithDutyId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "dutyId", required = true) @PathVariable("dutyId") Integer dutyId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<DepartmentEntity> departList = service.findByDutyId(dutyId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(departList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据baseId获取内设机构列表资源详情", httpMethod = "GET", response = DepartmentEntity.class, notes = "根据baseId获取内设机构列表资源详情")
	@RequestMapping(value = "list/{baseId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByBaseId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "baseId", required = true) @PathVariable("baseId") Integer baseId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<DepartmentEntity> departmentList = service.findByBaseId(baseId);	
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(departmentList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "根据baseId获取内设机构列表资源详情", httpMethod = "GET", response = DepartmentEntity.class, notes = "根据baseId获取内设机构列表资源详情")
	@RequestMapping(value = "list/level/flag/{baseId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findLevelFlagByBaseId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "baseId", required = true) @PathVariable("baseId") Integer baseId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<DepartmentEntity>  departmentEntityList = service.findLevelFlagsByBaseIdOpt(baseId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(departmentEntityList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据baseId获取内设机构列表", httpMethod = "GET", response = DepartmentEntity.class, notes = "根据baseId获取内设机构列表")
	@RequestMapping(value = "mapList/{baseId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findMapListByBaseId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "baseId", required = true) @PathVariable("baseId") Integer baseId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		DepartmentTreeNodeVo2  departmentTreeNodeVo2 = service.findMapListByBaseIdOpt(baseId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(departmentTreeNodeVo2.getChildren());
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据baseId获取内设机构树", httpMethod = "GET", response = DepartmentEntity.class, notes = "根据baseId获取内设机构树")
	@RequestMapping(value = "mapTree/{baseId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findMapTreeByBaseId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "baseId", required = true) @PathVariable("baseId") Integer baseId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		DepartmentTreeNodeVo2  departmentTreeNodeVo2 = service.findMapTreeByBaseIdOpt(baseId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(departmentTreeNodeVo2.getChildren());
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "根据baseId获取包含机构名称的内设机构树", httpMethod = "GET", response = DepartmentEntity.class, notes = "录入内设机构页面选择内设机构的上级机构用")
	@RequestMapping(value = "deptTree/{baseId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findDeptTreeByBaseId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "baseId", required = true) @PathVariable("baseId") Integer baseId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		Map<String,Object> mapTree = new HashMap<String,Object>();
		// 取得该机构的名称作为内设机构的父节点
		OrganizationBaseEntity  organizationBaseEntity  = organizationBaseService.findById(baseId);
		mapTree.put("id",baseId);
		mapTree.put("type","1");
		mapTree.put("name", organizationBaseEntity.getName());
		// service调用
		DepartmentTreeNodeVo2  departmentTreeNodeVo2 = service.findMapTreeByBaseIdOpt(baseId);
		mapTree.put("children", departmentTreeNodeVo2.getChildren());
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(mapTree);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据baseId获取一级内设机构列表", httpMethod = "GET", response = DepartmentEntity.class, notes = "根据baseId获取一级内设机构列表")
	@RequestMapping(value = "list/levelOne/{baseId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findLevelOneByBaseId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "baseId", required = true) @PathVariable("baseId") Integer baseId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<DepartmentVo> departmentList = service.findLevelOneByBaseId(baseId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(departmentList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据parentId获取内设机构列表", httpMethod = "GET", response = DepartmentEntity.class, notes = "根据parentId获取内设机构列表")
	@RequestMapping(value = "list/child/{parentId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByParentId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "parentId", required = true) @PathVariable("parentId") Integer parentId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<DepartmentEntity> departmentList = service.findByParentIdOrderByDepNumAsc(parentId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(departmentList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "个数", httpMethod = "POST", response = Long.class, notes = "根据条件获得资源数量")
	@RequestMapping(value = "count", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> count(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody DepartmentVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		long cnt = service.count(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(cnt);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "列表", httpMethod = "POST", response = DepartmentEntity.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody DepartmentVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<DepartmentEntity> list = service.list(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "新增或者更新数据", httpMethod = "POST", response = DepartmentEntity.class, notes = "保存资源到数据库")
	@RequestMapping(value = "{submitType}", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> save(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "提交类别", required = true) @PathVariable("submitType") String submitType,
			@ApiParam(value = "保存对象", required = true) @RequestBody DepartmentEntity entity) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();

		if(entity.getId() == null){
			entity.setDepCode(StringUtils.replace(UUID.randomUUID().toString(),"-", ""));
			entity.setDepNum(service.findMaxDepNum(entity.getBaseId()) + 1);
		} else {
			DepartmentEntity deptEntity = service.findById(entity.getId());
			entity.setDepCode(deptEntity.getDepCode());
			if(null == entity.getDepNum()){
				entity.setDepNum(deptEntity.getDepNum());
			}
		}
		
		// 数据格式校验
		if(!"0".equals(submitType)){
			String valMsg = service.validate(entity);
			if (valMsg != null) {
				result.setCode(ResultPojo.CODE_FORMAT_ERR);
				result.setMsg(valMsg);
				return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
			}
		}
		
		service.save(entity);

		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "物理删除", httpMethod = "POST", response = String.class, notes = "根据id物理删除资源")
	@RequestMapping(value = "{id}/delete", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> delete(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		Map<String,Object> resultInfoMap = new HashMap<String,Object>();
		// service调用
		List<Integer> deletedDeptIds =  service.recvDeleteByDeptId(id);
		resultInfoMap.put("deletedDeptIds", deletedDeptIds);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(resultInfoMap);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据部门库Id获取没有关联职能职责的内设机构", httpMethod = "GET", response = DepartmentEntity.class, notes = "根据部门库Id获取没有关联职能职责的内设机构")
	@RequestMapping(value = "/org/{id}/deptsWithNoDuty", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> finddeptWithNoDeptListByOrgId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<DepartmentEntity> list = service.finddeptWithNoDeptListByOrgId(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "复制上级部门对应的内设机构", httpMethod = "POST", response = DepartmentEntity.class, notes = "根据上级部门Id复制其相关内设机构")
	@RequestMapping(value = "/{departIds}/{baseId}/copy", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> copy(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "departIds", required = true) @PathVariable("departIds") String departIds,
			@ApiParam(value = "baseId", required = true) @PathVariable("baseId") Integer baseId) throws Exception {

		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		List<DepartmentEntity> departList = new ArrayList<>();

		// service调用
		if(StringUtils.isNotBlank(departIds)){
			String[] departmentIds = departIds.split(",");
			List<String> departIdList = Arrays.asList(departmentIds);
			departList = service.copyDepartmentInfoWithDepartIds(departIdList,baseId);
		}
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(departList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "内设机构上移", httpMethod = "POST", response = DepartmentEntity.class, notes = "内设机构上移")
	@RequestMapping(value = "move/up", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> moveUp(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "内设机构移动条件", required = true) @RequestBody Map<String, Integer> condMap) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		
		Integer baseId  = condMap.get("baseId"); 
		Integer deptId  = condMap.get("deptId");
		
		// service调用
		Map<String, String> actionMsgMap = service.moveUp(deptId, baseId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(actionMsgMap);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "内设机构上移", httpMethod = "POST", response = DepartmentEntity.class, notes = "内设机构上移")
	@RequestMapping(value = "move/down", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> moveDown(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "内设机构移动条件", required = true) @RequestBody Map<String, Integer> condMap) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		
		Integer baseId  = condMap.get("baseId"); 
		Integer deptId  = condMap.get("deptId");
		
		// service调用
		Map<String, String> actionMsgMap = service.moveDown(deptId, baseId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(actionMsgMap);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	//▼▼▼▼▼▼▼▼权责用DAO▼▼▼▼▼▼▼▼
	@ApiOperation(value = "findByItemIdAndOrgId", httpMethod = "GET", response = DepartmentEntity.class, notes = "findByItemIdAndOrgId")
	@RequestMapping(value = "findByItemIdAndOrgId/{itemId}/{orgId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByItemIdAndOrgId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "itemId", required = true) @PathVariable("itemId") Integer itemId,
			@ApiParam(value = "orgId", required = true) @PathVariable("orgId") Integer orgId) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(service.findByItemIdAndOrgId(itemId,orgId));
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "findByCode", httpMethod = "GET", response = DepartmentEntity.class, notes = "findByCode")
	@RequestMapping(value = "findByCode/{depCode}/{orgId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByCode(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "depCode", required = true) @PathVariable("depCode") String depCode,
			@ApiParam(value = "orgId", required = true) @PathVariable("orgId") Integer orgId) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(service.findByCode(depCode,orgId));
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "findByCodeId", httpMethod = "GET", response = DepartmentEntity.class, notes = "findByCodeId")
	@RequestMapping(value = "findByCodeId/{depCode}/{baseId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByCodeId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "depCode", required = true) @PathVariable("depCode") String depCode,
			@ApiParam(value = "baseId", required = true) @PathVariable("baseId") Integer baseId) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(service.findByCodeId(depCode,baseId));
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "findByCodes", httpMethod = "GET", response = DepartmentEntity.class, notes = "findByCodes")
	@RequestMapping(value = "findByCodes/{depCodes}/{orgId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByCodes(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "depCodes", required = true) @PathVariable("depCodes") String[] depCodes,
			@ApiParam(value = "orgId", required = true) @PathVariable("orgId") Integer orgId) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(service.findByCodes(depCodes,orgId));
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	//▲▲▲▲▲▲▲▲权责用DAO▲▲▲▲▲▲▲▲
}
