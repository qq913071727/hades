package cn.conac.rc.ofs.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.framework.utils.BeanMapper;
import cn.conac.rc.ofs.contsant.Contsants;
import cn.conac.rc.ofs.entity.OrganizationEntity;
import cn.conac.rc.ofs.repository.OrganizationRepository;
import cn.conac.rc.ofs.vo.OrganizationTreeNodeVo3;
import cn.conac.rc.ofs.vo.OrganizationVo;

@Service
@Transactional
public class OrganizationService extends GenericService<OrganizationEntity, Integer> {

	@Autowired
	private OrganizationRepository repository;
	
	/**
	 * 根据指定数量获取最近变更的三定信息（App用）
	 * 
	 * @param num
	 * @return
	 */
	public List<OrganizationEntity> findRecentOrgsByNum(int num, String areaCode) {
		return repository.findRecentOrgsByNum(num, areaCode);
	}

	/**
	 * 计数查询
	 * 
	 * @param vo
	 * @return 计数结果
	 */
	public long count(OrganizationVo vo) {
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * 
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<OrganizationEntity> list(OrganizationVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "sort");
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<OrganizationEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * 
	 * @param param
	 * @return Criteria
	 */
	private Criteria<OrganizationEntity> createCriteria(OrganizationVo param) {
		Criteria<OrganizationEntity> dc = new Criteria<OrganizationEntity>();
		
		if (StringUtils.isNotBlank(param.getAreaId())) {
			dc.add(Restrictions.eq("areaId", param.getAreaId(), true));
		}
		if (StringUtils.isNotBlank(param.getAreaCode())) {
			dc.add(Restrictions.eq("areaCode", param.getAreaCode(), true));
		}
		if (StringUtils.isNotBlank(param.getType())) {
			dc.add(Restrictions.eq("type", param.getType(), true));
		}
		if (param.getParentId()!=null) {
			dc.add(Restrictions.eq("compId", param.getParentId(), true));
		}
		if (StringUtils.isNotBlank(param.getIsComp())) {
			dc.add(Restrictions.eq("isComp", param.getIsComp(), true));
		}
		if (StringUtils.isNotBlank(param.getName())) {
			dc.add(Restrictions.like("name", param.getName(), true));
		}
		if (StringUtils.isNotBlank(param.getOwnSys())) {
			dc.add(Restrictions.eq("ownSys", param.getOwnSys(), true));
		}
		if (StringUtils.isNotBlank(param.getStatus())) {
			dc.add(Restrictions.eq("status", param.getStatus(), true));
		} else {
			dc.add(Restrictions.or(Restrictions.eq("status", Contsants.OFS_ORGANIZATION_FLAG_NORMAL, true),
					Restrictions.eq("status", Contsants.OFS_ORGANIZATION_FLAG_CANCEL, true)));
		}
		if (StringUtils.isNotBlank(param.getCode())) {
			dc.add(Restrictions.eq("code", param.getCode(), true));
		}
		if (StringUtils.isNotBlank(param.getLocalCode())) {
			dc.add(Restrictions.eq("localCode", param.getLocalCode(), true));
		}
		return dc;
	}

	public List<OrganizationEntity> findOrgsByParam(OrganizationVo param) {
		Criteria<OrganizationEntity> dc = new Criteria<OrganizationEntity>();
		if (StringUtils.isNotBlank(param.getAreaId())) {
			dc.add(Restrictions.eq("areaId", param.getAreaId(), true));
		}
		if (StringUtils.isNotBlank(param.getAreaCode())) {
			dc.add(Restrictions.eq("areaCode", param.getAreaCode(), true));
		}
		if (StringUtils.isNotBlank(param.getType())) {
			dc.add(Restrictions.eq("type", param.getType(), true));
		}
		if (param.getParentId()!=null) {
			dc.add(Restrictions.eq("compId", param.getParentId(), true));
		}
		if (StringUtils.isNotBlank(param.getIsComp())) {
			dc.add(Restrictions.eq("isComp", param.getIsComp(), true));
		}
		if (StringUtils.isNotBlank(param.getName())) {
			dc.add(Restrictions.like("name", param.getName(), true));
		}
		if (StringUtils.isNotBlank(param.getOwnSys())) {
			dc.add(Restrictions.eq("ownSys", param.getOwnSys(), true));
		}
		if (StringUtils.isNotBlank(param.getStatus())) {
			dc.add(Restrictions.eq("status", param.getStatus(), true));
		} else {
			dc.add(Restrictions.or(Restrictions.eq("status", Contsants.OFS_ORGANIZATION_FLAG_NORMAL, true),
					Restrictions.eq("status", Contsants.OFS_ORGANIZATION_FLAG_CANCEL, true)));
		}
		if (StringUtils.isNotBlank(param.getCode())) {
			dc.add(Restrictions.eq("code", param.getCode(), true));
		}
		if (StringUtils.isNotBlank(param.getLocalCode())) {
			dc.add(Restrictions.eq("localCode", param.getLocalCode(), true));
		}
		
		// 排序规则字段优先顺序(status(0,1),type(1,2),sort)
		// ps:撤并的在同级后面、行政机关排在事业单位前面，然后在
		// 按照排序号进行排序
//		Order order1 = new Order(Direction.ASC, "status");
//		Order order2 = new Order(Direction.ASC, "type");
//		Order order3 = new Order(Direction.ASC, "sort");
//		List<Order> orderList = new ArrayList<Order>();
//		orderList.add(order1);
//		orderList.add(order2);
//		orderList.add(order3);
//		Sort sort = new Sort(orderList) ;
		
		// 根据动态查询条件及排序依据查询集合
//		return sortList(repository.findAll(dc, sort));
		return sortList(repository.findAll(dc));
	}
	
	/**
	 * 查询全部子节点，一直到叶子节点
	 * 
	 * @param parentId
	 * @return
	 */
	public List<OrganizationEntity> findAllChildOrgs(String parentId) {
		Criteria<OrganizationEntity> dc = new Criteria<OrganizationEntity>();
		if (StringUtils.isNotBlank(parentId)) {
			dc.add(Restrictions.like("parentIds", parentId, true));
		}
		// 根据动态查询条件及排序依据查询集合
		return sortList(repository.findAll(dc));
	}

	/**
	 * 查询第一级子节点
	 * 
	 * @param parentId
	 * @return
	 */
	public List<OrganizationEntity> findChildOrgs(Integer parentId) {
		Criteria<OrganizationEntity> dc = new Criteria<OrganizationEntity>();
		if (parentId!=null) {
			dc.add(Restrictions.eq("parentId", parentId, true));
		}
		dc.add(Restrictions.or(Restrictions.eq("status", Contsants.OFS_ORGANIZATION_FLAG_NORMAL, true),
				Restrictions.eq("status", Contsants.OFS_ORGANIZATION_FLAG_CANCEL, true)));
		// 根据动态查询条件及排序依据查询集合
		return sortList(repository.findAll(dc));
	}
	
	/**
	 * 根据parentId和areaCode查询部门库信息
	 * 
	 * @param parentId
	 * @param ownSys
	 * @param areaCode
	 * @return
	 */
	public List<OrganizationEntity> findByPidAndAreaCode(Integer parentId, String ownSys, String type, String areaCode, String status) {
		Criteria<OrganizationEntity> dc = new Criteria<OrganizationEntity>();
		if (parentId!=null) {
			dc.add(Restrictions.eq("parentId", parentId, true));
		}
		if(StringUtils.isNotEmpty(ownSys)) {
			dc.add(Restrictions.eq("ownSys", ownSys, true));
		}
		if (StringUtils.isNotBlank(type)) {
			dc.add(Restrictions.eq("type", type, true));
		}
		if(StringUtils.isNotEmpty(areaCode)) {
			dc.add(Restrictions.eq("areaCode", areaCode, true));
		}
		if (StringUtils.isNotBlank(status) && Contsants.OFS_ORGANIZATION_FLAG_NORMAL.equals(status)) {
			dc.add(Restrictions.eq("status", Contsants.OFS_ORGANIZATION_FLAG_NORMAL, true));
		} else {
			dc.add(Restrictions.or(Restrictions.eq("status", Contsants.OFS_ORGANIZATION_FLAG_NORMAL, true),
					Restrictions.eq("status", Contsants.OFS_ORGANIZATION_FLAG_CANCEL, true)));
		}
		// 根据动态查询条件及排序依据查询集合
		return sortList(repository.findAll(dc));
	}

	public List<OrganizationEntity> findOrgsByOwnSys(String ownSys, String type, String areaCode, String status) {
		if (StringUtils.isEmpty(ownSys)) {
			return null;
		}
		Criteria<OrganizationEntity> dc = new Criteria<OrganizationEntity>();
		if (StringUtils.isNotBlank(ownSys)) {
			dc.add(Restrictions.eq("ownSys", ownSys, true));
		}
		if (StringUtils.isNotBlank(type)) {
			dc.add(Restrictions.eq("type", type, true));
		}
		if (StringUtils.isNotBlank(areaCode)) {
			dc.add(Restrictions.eq("areaCode", areaCode, true));
		}
		if (StringUtils.isNotBlank(status) && Contsants.OFS_ORGANIZATION_FLAG_NORMAL.equals(status)) {
			dc.add(Restrictions.eq("status", Contsants.OFS_ORGANIZATION_FLAG_NORMAL, true));
		} else {
			dc.add(Restrictions.or(Restrictions.eq("status", Contsants.OFS_ORGANIZATION_FLAG_NORMAL, true),
					Restrictions.eq("status", Contsants.OFS_ORGANIZATION_FLAG_CANCEL, true)));
		}
		dc.add(Restrictions.eq("isComp", "1", true));
		dc.add(Restrictions.isNull("parentId"));
		// 根据动态查询条件及排序依据查询集合
		return sortList(repository.findAll(dc));
	}

//	private List<OrganizationEntity> sortList(List<OrganizationEntity> list) {
//		Collections.sort(list, new Comparator<OrganizationEntity>() {
//			@Override
//			public int compare(OrganizationEntity arg0, OrganizationEntity arg1) {
//				if (null == arg1.getSort()) {
//					return 1;
//				}
//				if (null == arg0.getSort()) {
//					return -1;
//				}
//				if (arg0.getSort() > arg1.getSort()) {
//					return 1;
//				} else if (arg0.getSort() < arg1.getSort()) {
//					return -1;
//				}
//				return 0;
//			}
//		});
//		return list;
//	}
	
	private  List<OrganizationEntity> sortList(List<OrganizationEntity> orgEntityList) {
		if (null == orgEntityList || orgEntityList.isEmpty()) {
			return new ArrayList<OrganizationEntity>();
		}
		Collections.sort(orgEntityList, new Comparator<OrganizationEntity>() {
			@Override
			public int compare(OrganizationEntity o1, OrganizationEntity o2) {
				if (StringUtils.isEmpty(o2.getStatus())) {
					return 1;
				}
				if (StringUtils.isEmpty(o1.getStatus())) {
					return -1;
				}
				if (Integer.valueOf(o1.getStatus()) > Integer.valueOf(o2.getStatus())) {
					return 1;
				} else if (Integer.valueOf(o1.getStatus()) < Integer.valueOf(o2.getStatus())) {
					return -1;
				} else {
					if (StringUtils.isEmpty(o2.getType())) {
						return 1;
					}
					if (StringUtils.isEmpty(o1.getType())) {
						return -1;
					}
					if (Integer.valueOf(o1.getType()) > Integer.valueOf(o2.getType())) {
						return 1;
					} else if (Integer.valueOf(o1.getType()) < Integer.valueOf(o2.getType())) {
						return -1;
					} else {
						if (null == o2.getSort()) {
							return 1;
						}
						if (null == o1.getSort()) {
							return -1;
						}
						if (o1.getSort() > o2.getSort() ) {
							return 1;
						} else if(o1.getSort() < o2.getSort() ){
							return -1;
						} else {
							return 0;
						}
					}
				}
			}
		});
		
		return orgEntityList;
	}

	public Integer countXZJG(String areaCode) {
		if (StringUtils.isEmpty(areaCode)) {
			return null;
		}
		// 根据动态查询条件及排序依据查询集合
		return repository.countXZJG(areaCode + "%");
	}

	public Integer countSYDW(String areaCode) {
		if (StringUtils.isEmpty(areaCode)) {
			return null;
		}
		// 根据动态查询条件及排序依据查询集合
		return repository.countSYDW(areaCode + "%");
	}
	
	/**
	 * 根据id查询该机构的下设机构或者事业单位信息
	 * 
	 * @param id
	 * @param type
	 *            (1: 下设机构 2:事业单位)
	 * @param mapTree
	 * @return 一级内设机构List
	 */
	public void findMapTreeByCondition(OrganizationEntity orgEntity, String type, Map<String, Object> mapTree) {
		if ("1".equals(orgEntity.getType())) {
			mapTree.put("id", "02");
			mapTree.put("name", "下设机构");
		} else {
			mapTree.put("id", "03");
			mapTree.put("name", "事业单位");
		}
		mapTree.put("hrefFlag", "0");

		OrganizationVo vo = new OrganizationVo();
		vo.setAreaCode(orgEntity.getAreaCode());
		vo.setOwnSys(orgEntity.getOwnSys());
		vo.setType(type);
		List<OrganizationEntity> orgInDBs = this.findOrgsByParam(vo);
		// 组成树结构
		Map<String, OrganizationTreeNodeVo3> orgTreeNodeMap = new HashMap<String, OrganizationTreeNodeVo3>();
		for (OrganizationEntity entity : orgInDBs) {
			// 过滤异常数据(部门库中的部门名字为null的部门)
			if(StringUtils.isEmpty(entity.getName())) {
				continue;
			}
			// 过滤异常数据(部门库中ParetnId为null且又不是主管部门的标示)
			if(entity.getParentId() == null && Contsants.OFS_ORGANIZATION_NOT_COMP.equals(entity.getIsComp())) {
				continue;
			}
			OrganizationTreeNodeVo3 treeNode = new OrganizationTreeNodeVo3(entity);
			orgTreeNodeMap.put(treeNode.getId(), treeNode);
		}
		List<OrganizationTreeNodeVo3> childOrgList  = new ArrayList<OrganizationTreeNodeVo3>();
		for (OrganizationTreeNodeVo3 treeNode : orgTreeNodeMap.values()) {
			Integer parentId = treeNode.getParentId();
			// 将部门的ID和parentID相等的异常数据过滤掉，避免出现无限死循环
			if(Integer.valueOf(treeNode.getId()).equals(parentId)) {
				continue;
			}
			if(orgEntity.getId().equals(treeNode.getParentId())) {
				childOrgList.add(treeNode);
			}
			if (parentId != null ) {
				if(orgTreeNodeMap.containsKey(parentId.toString())){
					orgTreeNodeMap.get(parentId.toString()).addChild(treeNode);
				}
			}
		}
		sort3(childOrgList);
		mapTree.put("children", childOrgList);
		return;
	}

	/**
	 * 根据id查询该机构的下设机构或者事业单位信息
	 * 
	 * @param id
	 * @param type
	 *            (1: 下设机构 2:事业单位)
	 * @param mapTree
	 * @return 一级内设机构List
	 */
	public void findMapTreeById(Integer id, String type, Map<String, Object> mapTree) {
		if ("1".equals(type)) {
			mapTree.put("id", "02");
			mapTree.put("name", "下设机构");
		} else {
			mapTree.put("id", "03");
			mapTree.put("name", "事业单位");
		}
		mapTree.put("hrefFlag", "0");
		List<Map<String, Object>> childDeptList = new ArrayList<Map<String, Object>>();
		Map<String, Object> deptMap = null;
		List<OrganizationEntity> organizationEntityList = sortList(repository.findByParentIdAndTypeAndStatus(id, type,Contsants.OFS_ORGANIZATION_FLAG_NORMAL));
		for (OrganizationEntity organizationEntity : organizationEntityList) {
			deptMap = new HashMap<String, Object>();
			deptMap.put("id", organizationEntity.getId());
			deptMap.put("name", organizationEntity.getName());
			deptMap.put("hrefFlag", "0");
			recursiveDeptTree(organizationEntity.getId(), type, deptMap);
			childDeptList.add(deptMap);
		}
		mapTree.put("children", childDeptList);
		return;
	}

	/**
	 * 根据主键ID递归查询下设机构或者事业单位信息
	 * 
	 * @param id
	 * @param type
	 *            (1: 下设机构 2:事业单位)
	 * @param map
	 * @return 返回以该ID为父的内设机构List
	 */
	public List<OrganizationEntity> recursiveDeptTree(Integer id, String type, Map<String, Object> map) {
		List<OrganizationEntity> orgList = sortList(repository.findByParentIdAndTypeAndStatus(id, type,Contsants.OFS_ORGANIZATION_FLAG_NORMAL));
		List<Map<String, Object>> childOrgList = new ArrayList<Map<String, Object>>();
		Map<String, Object> orgMap = null;
		for (OrganizationEntity orgEntity : orgList) {
			orgMap = new HashMap<String, Object>();
			orgMap.put("id", orgEntity.getId());
			orgMap.put("name", orgEntity.getName());
			orgMap.put("hrefFlag", "0");
			recursiveDeptTree(orgEntity.getId(), type, orgMap); // 递归调用
			childOrgList.add(orgMap);
		}
		map.put("children", childOrgList);
		return orgList;
	}

	/**
	 * 根据部门名称校验部门的存在
	 * @param name
	 * @return String
	 */
	public String checkOrgName(String name,String areaCode, String orgId) {
		// 返回结果声明
		String checkRtl = "true";
		OrganizationEntity org = null;
		if(StringUtils.isEmpty(orgId)) {
			org = repository.findByNameAndAreaCodeAndStatus(name,areaCode, Contsants.OFS_ORGANIZATION_FLAG_NORMAL);
		} else {
			org = repository.findByNameAndAreaCodeUpdate(name,areaCode,Integer.valueOf(orgId));
		}
		// 判断是否存在对应部门
		if(org != null && StringUtils.isNotBlank(org.getName())){
			checkRtl = "false";
		}
		return checkRtl;
	}

	/**
	 * 根据userId查询部门库信息
	 * 
	 * @param userId
	 * @return 部门库实体
	 */
	public OrganizationEntity  findOrgInfoByUserId(Integer userId) {
		return repository.findOrgInfoByUserId(userId);
	}
	
	/**
	 * 保存部门树向下移动时的部门库信息
	 * 
	 * @param currentOrgInfo  当前选中的部门
	 * @param otherOrgInfo  要交换的部门
	 * @return void
	 */
	public void  saveUpAndDownOrgInfo(OrganizationEntity currentOrgInfo, OrganizationEntity otherOrgInfo) {
		repository.save(currentOrgInfo);
		repository.save(otherOrgInfo);
	}
	
	/**
	 * 保存当前部门同时更新父部门的hasChild字段
	 * 
	 * @param parentOrgInfo  父部门
	 * @param currentOrgInfo  当前的部门
	 * @return void
	 */
	public void  updateHasChildCol(Integer curParentId, Integer originParentId, Integer curOrgId) {
		// 更新当前的parentId
		if(null != curParentId) {
			repository.updateHasChildByPid(curParentId);
		}
		// 更新原来的parentId
		if(null != originParentId && !originParentId.equals(curParentId)) {
			repository.updateHasChildByOriginPid(originParentId);
		}
		// 更新场合维护自身的hasChild的值
		if(null != curOrgId) {
			repository.updateHasChildByPid(curOrgId);
		}
	}
	
	
	/**
	 * 保存当前部门同时更新父部门的hasChild字段
	 * 
	 * @param curParentId  当前父部门ID
	 * @return void
	 */
	public void  updateHasChildColByPid(Integer curParentId) {
		// 更新当前的parentId
		if(null != curParentId) {
			repository.updateHasChildByOriginPid(curParentId);
		}
	}
	
//	/**
//	 * 更新处理
//	 * @param id
//	 * @param entity
//	 * @return
//	 */
//	public OrganizationEntity update(Integer id, OrganizationEntity entity){
//		// 检索要更新的数据信息
//		OrganizationEntity targetEntity = repository.findOne(id);
//		// 空判断
//		if(targetEntity == null){
//			return null;
//		}
//		// 复制表信息
//		BeanMapper.copy(entity,targetEntity,new String[]{"id","baseId"});
//		// 保存信息并返回
//		return repository.save(targetEntity);
//	}

	/***
	 * 批量保存或者更新部门库信息
	 * @param orgInfoList
	 * @return 附带部门库ID的orgList
	 * @throws Exception
	 */
	public Map<String, Object> SaveOrUpdateOrgInfoList(List<OrganizationEntity> orgInfoList) throws Exception {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<OrganizationEntity> retOrgInfoList = new ArrayList<OrganizationEntity>();
		List<String> warMsgList = new ArrayList<String>();
		if(null == orgInfoList) {
			resultMap.put("errInfo", "批量保存或更新部门库信息的参数为null");
			return resultMap;
		}
		OrganizationEntity organizationEntityDb = null;
		OrganizationEntity organizationEntityNew = null;
		for(OrganizationEntity organizationEntity : orgInfoList) {
			if(null != organizationEntity.getId()) {
				organizationEntityDb = this.findById(organizationEntity.getId());
				if(null != organizationEntityDb) {
					organizationEntityNew = new OrganizationEntity();
					BeanMapper.copy(organizationEntityDb, organizationEntityNew);
					// 将页面设置的不为null值覆盖DB中的值
					BeanMapper.copyIgnorNull(organizationEntity, organizationEntityNew);
					this.save(organizationEntityNew);
					retOrgInfoList.add(organizationEntityNew);
				} else {
					warMsgList.add("根据"+ organizationEntity.getId().toString() + "取得部门库的数据为null,此数据的更新被忽略");
					continue;
				}
				
			} else {
				this.save(organizationEntity);
				retOrgInfoList.add(organizationEntity);
			}
		}
		if(warMsgList.size() > 0){
			resultMap.put("warInfo", warMsgList);
		}
		resultMap.put("retInfo", retOrgInfoList);
		return resultMap;
	}
	/**
	 * 对组织架构图的下设及事业单位的部门树排序
	 * @param treeResult
	 */
	private void sort3(List<OrganizationTreeNodeVo3> treeResult) {
		if (null == treeResult || treeResult.isEmpty()) {
			return;
		}
		Collections.sort(treeResult, new Comparator<OrganizationTreeNodeVo3>() {
			@Override
			public int compare(OrganizationTreeNodeVo3 o1, OrganizationTreeNodeVo3 o2) {
				if (StringUtils.isEmpty(o2.getStatus())) {
					return 1;
				}
				if (StringUtils.isEmpty(o1.getStatus())) {
					return -1;
				}
				if (Integer.valueOf(o1.getStatus()) > Integer.valueOf(o2.getStatus())) {
					return 1;
				} else if (Integer.valueOf(o1.getStatus()) < Integer.valueOf(o2.getStatus())) {
					return -1;
				} else {
					if (StringUtils.isEmpty(o2.getType())) {
						return 1;
					}
					if (StringUtils.isEmpty(o1.getType())) {
						return -1;
					}
					if (Integer.valueOf(o1.getType()) > Integer.valueOf(o2.getType())) {
						return 1;
					} else if (Integer.valueOf(o1.getType()) < Integer.valueOf(o2.getType())) {
						return -1;
					} else {
						if (null == o2.getSort()) {
							return 1;
						}
						if (null == o1.getSort()) {
							return -1;
						}
						if (o1.getSort() > o2.getSort() ) {
							return 1;
						} else if(o1.getSort() < o2.getSort() ){
							return -1;
						} else {
							return 0;
						}
					}
				}
			}
		});

		for (OrganizationTreeNodeVo3 vo : treeResult) {
			sort3(vo.getChildren());
		}
	}
}
