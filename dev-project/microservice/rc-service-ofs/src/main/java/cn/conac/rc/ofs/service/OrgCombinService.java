package cn.conac.rc.ofs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.ofs.entity.OrgCombinEntity;
import cn.conac.rc.ofs.repository.OrgCombinRepository;
import cn.conac.rc.ofs.vo.OrgCombinVo;
import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;

@Service
public class OrgCombinService extends GenericService<OrgCombinEntity, Integer> {

	@Autowired
	private OrgCombinRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(OrgCombinVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<OrgCombinEntity> list(OrgCombinVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<OrgCombinEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<OrgCombinEntity> createCriteria(OrgCombinVo param) {
		Criteria<OrgCombinEntity> dc = new Criteria<OrgCombinEntity>();
		// TODO 具体条件赋值

		return dc;
	}

}
