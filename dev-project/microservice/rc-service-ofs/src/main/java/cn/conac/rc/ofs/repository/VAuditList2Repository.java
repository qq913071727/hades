package cn.conac.rc.ofs.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.VAuditList2Entity;

/**
 * VAuditList2Repository类
 *
 * @author repositoryCreator
 * @date 2016-12-26
 * @version 1.0
 */
@Repository
public interface VAuditList2Repository extends GenericDao<VAuditList2Entity, Integer> {
}
