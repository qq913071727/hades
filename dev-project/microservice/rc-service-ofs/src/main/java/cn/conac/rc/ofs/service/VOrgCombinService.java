package cn.conac.rc.ofs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.ofs.entity.VOrgCombinEntity;
import cn.conac.rc.ofs.repository.VOrgCombinRepository;

/**
 * VOrgCombinService类
 *
 * @author serviceCreator
 * @date 2017-01-16
 * @version 1.0
 */
@Service
public class VOrgCombinService extends GenericService<VOrgCombinEntity, Integer> {

	@Autowired
	private VOrgCombinRepository repository;

	/**
	 * 通过部门的baseId来获得并入部门信息（机构ID及机构名称）
	 * @param baseId
	 * @return List<VOrgCombinEntity>
	 * @throws Exception
	 */
	public List<VOrgCombinEntity> findByBaseId(Integer baseId) throws Exception {
		try {
			return repository.findByBaseId(baseId);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
