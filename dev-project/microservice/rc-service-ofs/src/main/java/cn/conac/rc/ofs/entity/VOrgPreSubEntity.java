package cn.conac.rc.ofs.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * VOrgPreSubEntity类
 *
 * @author beanCreator
 * @date 2016-12-14
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_V_ORG_PRE_SUB")
public class VOrgPreSubEntity implements Serializable {

	private static final long serialVersionUID = 1481698396382297766L;
	@Id
	@ApiModelProperty("部门BaseId")
	private Integer id;
	
	@ApiModelProperty("机构ID")
	private Integer orgId;

	@ApiModelProperty("地区ID")
	private String areaId;

	@ApiModelProperty("信息完整度")
	private String contentMask;

	@ApiModelProperty("部门名称")
	private String name;

	@ApiModelProperty("所属系统")
	private String ownSys;

	@ApiModelProperty("主管部门")
	private String compName;

	@ApiModelProperty("申请类型")
	private String updateType;

	@ApiModelProperty("部门类型")
	private String type;

	@ApiModelProperty("更新时间")
	private Date updateTime;
	
	@ApiModelProperty("录入人ID号")
	private Integer userId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public void setContentMask(String contentMask){
		this.contentMask=contentMask;
	}

	public String getContentMask(){
		return contentMask;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setOwnSys(String ownSys){
		this.ownSys=ownSys;
	}

	public String getOwnSys(){
		return ownSys;
	}

	public void setCompName(String compName){
		this.compName=compName;
	}

	public String getCompName(){
		return compName;
	}

	public void setUpdateType(String updateType){
		this.updateType=updateType;
	}

	public String getUpdateType(){
		return updateType;
	}

	public void setType(String type){
		this.type=type;
	}

	public String getType(){
		return type;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
