package cn.conac.rc.ofs.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by wangmeng on 2016-12-23.
 */
@ApiModel
public class AppdateCheckVo implements Serializable {

    private static final long serialVersionUID = 1477992275860885656L;

    @ApiModelProperty("Check结果")
    private String checkResult;

    @ApiModelProperty("Check批文日期")
    private String checkAppDate;

    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }

    public String getCheckAppDate() {
        return checkAppDate;
    }

    public void setCheckAppDate(String checkAppDate) {
        this.checkAppDate = checkAppDate;
    }
}
