package cn.conac.rc.ofs.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.ofs.entity.VHistApprovalEntity;
import cn.conac.rc.ofs.service.VHistApprovalService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="history/")
public class VHistApprovalController {

	@Autowired
	VHistApprovalService service;

	@ApiOperation(value = "历史沿革的列表", httpMethod = "GET", response = VHistApprovalEntity.class, notes = "根据部门的OrgId获得历史沿革列表")
	@RequestMapping(value = "list/{orgId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> historyList(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "部门的OrgId", required = true) @PathVariable("orgId") String orgId) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<VHistApprovalEntity> list = service.list(orgId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
