package cn.conac.rc.ofs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.rc.framework.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@Entity
@Table(name="RCO_V_HIST_APPROVAL")
public class VHistApprovalEntity extends BaseEntity<VHistApprovalEntity> implements Serializable {

	private static final long serialVersionUID = 1473775241900291705L;

	@ApiModelProperty("orgId")
	private String orgId;

	@ApiModelProperty("appName")
	private String appName;

	@ApiModelProperty("appType")
	private String appType;

	@ApiModelProperty("appNum")
	private String appNum;

	@ApiModelProperty("appDate")
	private Date appDate;

	@ApiModelProperty("payOrgName")
	private String payOrgName;

	@ApiModelProperty("remarks")
	private String remarks;

	@ApiModelProperty("isRedefined")
	private String isRedefined;

	@ApiModelProperty("regUserId")
	private Integer regUserId;

	@ApiModelProperty("updateType")
	private String updateType;
	
	@ApiModelProperty("是否历史")
	// 是否历史：0当前 1历史
	private String isHistory;
	
	@ApiModelProperty("更新时间")
	private Date updateTime;

	@ApiModelProperty("loginName")
	private String loginName;

	@ApiModelProperty("regUserName")
	private String regUserName;

	@ApiModelProperty("isSimple")
	private String isSimple;

	@ApiModelProperty("fileId")
	private String fileId;
	
	@ApiModelProperty("fileName")
	private String fileName;

	public void setOrgId(String orgId){
		this.orgId=orgId;
	}

	public String getOrgId(){
		return orgId;
	}

	public void setAppName(String appName){
		this.appName=appName;
	}

	public String getAppName(){
		return appName;
	}

	public void setAppType(String appType){
		this.appType=appType;
	}

	public String getAppType(){
		return appType;
	}

	public void setAppNum(String appNum){
		this.appNum=appNum;
	}

	public String getAppNum(){
		return appNum;
	}

	public void setAppDate(Date appDate){
		this.appDate=appDate;
	}

	public Date getAppDate(){
		return appDate;
	}

	public void setPayOrgName(String payOrgName){
		this.payOrgName=payOrgName;
	}

	public String getPayOrgName(){
		return payOrgName;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void setIsRedefined(String isRedefined){
		this.isRedefined=isRedefined;
	}

	public String getIsRedefined(){
		return isRedefined;
	}

	public void setRegUserId(Integer regUserId){
		this.regUserId=regUserId;
	}

	public Integer getRegUserId(){
		return regUserId;
	}

	public void setUpdateType(String updateType){
		this.updateType=updateType;
	}

	public String getUpdateType(){
		return updateType;
	}

	public String getIsHistory() {
		return isHistory;
	}

	public void setIsHistory(String isHistory) {
		this.isHistory = isHistory;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public void setLoginName(String loginName){
		this.loginName=loginName;
	}

	public String getLoginName(){
		return loginName;
	}

	public void setRegUserName(String regUserName){
		this.regUserName=regUserName;
	}

	public String getRegUserName(){
		return regUserName;
	}

	public String getIsSimple() {
		return isSimple;
	}

	public void setIsSimple(String isSimple) {
		this.isSimple = isSimple;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
