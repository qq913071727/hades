package cn.conac.rc.ofs.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.OrgStatusEntity;

/**
 * OrgStatusRepository类
 *
 * @author repositoryCreator
 * @date 2016-11-25
 * @version 1.0
 */
@Repository
public interface OrgStatusRepository extends GenericDao<OrgStatusEntity, Integer> {

    /**
     * 通过OrgId检索Status表
     * @param orgId
     */
    @Query("select os from OrgStatusEntity os, OrganizationBaseEntity ob, OrganizationEntity o where os.id = ob.id and o.baseId = ob.id and ob.orgId = o.id and o.id = :orgId")
    OrgStatusEntity findOrgStatusByOrgId(@Param("orgId") Integer orgId);

}
