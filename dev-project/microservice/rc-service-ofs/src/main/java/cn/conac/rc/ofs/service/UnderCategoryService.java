package cn.conac.rc.ofs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.ofs.entity.UnderCategoryEntity;
import cn.conac.rc.ofs.repository.UnderCategoryRepository;
import cn.conac.rc.ofs.vo.UnderCategoryVo;
import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;

/**
 * UnderCategoryService类
 *
 * @author serviceCreator
 * @date 2016-11-25
 * @version 1.0
 */
@Service
public class UnderCategoryService extends GenericService<UnderCategoryEntity, Integer> {

	@Autowired
	private UnderCategoryRepository underCategoryRepository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(UnderCategoryVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<UnderCategoryEntity> list(UnderCategoryVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<UnderCategoryEntity> dc = this.createCriteria(vo);
			return underCategoryRepository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<UnderCategoryEntity> createCriteria(UnderCategoryVo param) {
		Criteria<UnderCategoryEntity> dc = new Criteria<>();
		// TODO 具体条件赋值

		return dc;
	}

}
