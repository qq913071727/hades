package cn.conac.rc.ofs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.OrgCombinEntity;

@Repository
public interface OrgCombinRepository extends GenericDao<OrgCombinEntity, Integer> {
	
	/**
	 * 根据baseID删除部门合并信息
	 * @param baseId
	 * @return void
	 */
	@Query(value = "delete  from ofs_org_combin oc where oc.org_id= :baseId", nativeQuery = true)
	@Modifying
	void deleteByBaseId(@Param("baseId") Integer baseId);
	
	
	/**
	 * 通过机构BaseID获取合并部门相关数据
	 * @return
	 */
	List<OrgCombinEntity> findByBaseId(@Param("baseId") Integer baseId);
	
}
