package cn.conac.rc.ofs.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.AnalzWarningEntity;

@Repository
public interface AnalzWarningRepository extends GenericDao<AnalzWarningEntity, Integer> {

	/**
	 * 删除全部
	 * @param id
	 * @return 删除数量
	 */
	@Query("delete from AnalzWarningEntity ")
	@Modifying
	void deleteAll();
}
