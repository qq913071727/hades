package cn.conac.rc.ofs.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.rc.framework.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@Entity
@Table(name="RCO_V_DOMAIN_INFO")
public class VDomainInfoEntity extends BaseEntity<VDomainInfoEntity> implements Serializable {

	private static final long serialVersionUID = 1473339485538846798L;

	@ApiModelProperty("domainName")
	private String domainName;

	@ApiModelProperty("orgName")
	private String orgName;

	@ApiModelProperty("domainNameEn")
	private String domainNameEn;

	@ApiModelProperty("netName")
	private String netName;

	public void setDomainName(String domainName){
		this.domainName=domainName;
	}

	public String getDomainName(){
		return domainName;
	}

	public void setOrgName(String orgName){
		this.orgName=orgName;
	}

	public String getOrgName(){
		return orgName;
	}

	public void setDomainNameEn(String domainNameEn){
		this.domainNameEn=domainNameEn;
	}

	public String getDomainNameEn(){
		return domainNameEn;
	}

	public void setNetName(String netName){
		this.netName=netName;
	}

	public String getNetName(){
		return netName;
	}

}
