package cn.conac.rc.ofs.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * UnderCategoryEntity类
 *
 * @author beanCreator
 * @date 2016-11-25
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_UNDER_CATEGORY")
public class UnderCategoryEntity implements Serializable {

	private static final long serialVersionUID = 148005393952070406L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("pid")
	@NotNull
	private Integer pid;

	@ApiModelProperty("name")
	@NotNull
	private String name;

	@ApiModelProperty("open")
	private String open;

	@ApiModelProperty("perm")
	private String perm;

	@ApiModelProperty("leaf")
	private Integer leaf;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setPid(Integer pid){
		this.pid=pid;
	}

	public Integer getPid(){
		return pid;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setOpen(String open){
		this.open=open;
	}

	public String getOpen(){
		return open;
	}

	public void setPerm(String perm){
		this.perm=perm;
	}

	public String getPerm(){
		return perm;
	}

	public void setLeaf(Integer leaf){
		this.leaf=leaf;
	}

	public Integer getLeaf(){
		return leaf;
	}

}
