package cn.conac.rc.ofs.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.rc.framework.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * VOrgCombinEntity类
 *
 * @author beanCreator
 * @date 2017-01-16
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_V_ORG_COMBIN")
public class VOrgCombinEntity extends BaseEntity<VOrgCombinEntity>  implements Serializable {

	private static final long serialVersionUID = 1484532804702190904L;

	@ApiModelProperty("baseId")
	private Integer baseId;

	@ApiModelProperty("orgId")
	private Integer orgId;

	@ApiModelProperty("name")
	private String name;

	public void setBaseId(Integer baseId){
		this.baseId=baseId;
	}

	public Integer getBaseId(){
		return baseId;
	}

	public void setOrgId(Integer orgId){
		this.orgId=orgId;
	}

	public Integer getOrgId(){
		return orgId;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

}
