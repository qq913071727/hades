package cn.conac.rc.ofs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.ofs.entity.VDomainInfoEntity;
import cn.conac.rc.ofs.repository.VDomainInfoRepository;

@Service
public class VDomainInfoService extends GenericService<VDomainInfoEntity, String> {

	@Autowired
	private VDomainInfoRepository repository;
	
	/**
	 * 根据orgName查询该机构的对应的域名信息
	 * @param orgName
	 * @return VDomainInfoEntity的实体
	 */
	public VDomainInfoEntity findByOrgName(String orgName){
		return repository.findByOrgName(orgName);
	}
	
}
