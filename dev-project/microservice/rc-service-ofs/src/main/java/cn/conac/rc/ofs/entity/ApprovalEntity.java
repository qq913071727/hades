package cn.conac.rc.ofs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ApprovalEntity类
 *
 * @author beanCreator
 * @date 2016-11-02
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_APPROVAL")
public class ApprovalEntity implements Serializable {

	private static final long serialVersionUID = 1478071306885897733L;
	@Id
	@ApiModelProperty("ID")
	private Integer id;

	@ApiModelProperty("批文名称")
	//@NotNull
	private String name;

	@ApiModelProperty("批文类型")
	private String appType;

	@ApiModelProperty("批文号")
	//@NotNull
	private String appNum;

	@ApiModelProperty("批文时间")
	//@NotNull
	private Date appDate;

	@ApiModelProperty("发放单位")
	//@NotNull
	private String payOrgName;

	@ApiModelProperty("撤并原因")
	private String removeReason;

	@ApiModelProperty("摘要(备注)")
	//@NotNull
	private String remarks;

	@ApiModelProperty("是否重定义标识")
	//@NotNull
	private String isRedefined;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setAppType(String appType){
		this.appType=appType;
	}

	public String getAppType(){
		return appType;
	}

	public void setAppNum(String appNum){
		this.appNum=appNum;
	}

	public String getAppNum(){
		return appNum;
	}

	public void setAppDate(Date appDate){
		this.appDate=appDate;
	}

	public Date getAppDate(){
		return appDate;
	}

	public void setPayOrgName(String payOrgName){
		this.payOrgName=payOrgName;
	}

	public String getPayOrgName(){
		return payOrgName;
	}

	public void setRemoveReason(String removeReason){
		this.removeReason=removeReason;
	}

	public String getRemoveReason(){
		return removeReason;
	}

	public void setRemarks(String remarks){
		this.remarks=remarks;
	}

	public String getRemarks(){
		return remarks;
	}

	public void setIsRedefined(String isRedefined){
		this.isRedefined=isRedefined;
	}

	public String getIsRedefined(){
		return isRedefined;
	}
}
