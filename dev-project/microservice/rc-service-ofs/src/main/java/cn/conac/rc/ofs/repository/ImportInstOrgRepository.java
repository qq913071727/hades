package cn.conac.rc.ofs.repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.ImportInstOrgEntity;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * ImportInstOrgRepository类
 *
 * @author repositoryCreator
 * @date 2016-12-23
 * @version 1.0
 */
@Repository
public interface ImportInstOrgRepository extends GenericDao<ImportInstOrgEntity, Integer> {

    /**
     * 通过名称获取对应详情
     * @param name
     * @return ImportInstOrgEntity
     */
    ImportInstOrgEntity findByName(@Param("name") String name);
}
