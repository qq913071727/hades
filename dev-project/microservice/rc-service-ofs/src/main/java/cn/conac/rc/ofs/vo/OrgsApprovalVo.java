package cn.conac.rc.ofs.vo;

import java.util.Date;
import java.util.List;

import javax.persistence.Transient;

import cn.conac.rc.ofs.entity.OrgsApprovalEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class OrgsApprovalVo extends OrgsApprovalEntity {

	private static final long serialVersionUID = 1472903587533964490L;

	@Transient
	@ApiModelProperty("当前分页")
	private Integer page;

	@Transient
	@ApiModelProperty("每页个数")
	private Integer size;

	@Transient
	@ApiModelProperty("批文号/名称")
	private String appNumOrName;

	@Transient
	@ApiModelProperty("批文时间Start")
	private Date appDateStart;

	@Transient
	@ApiModelProperty("批文时间End")
	private Date appDateEnd;

	@ApiModelProperty("选择地区IDs")
	private List<String> areaIds;

	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}

	public String getAppNumOrName() {
		return appNumOrName;
	}

	public void setAppNumOrName(String appNumOrName) {
		this.appNumOrName = appNumOrName;
	}

	public Date getAppDateStart() {
		return appDateStart;
	}

	public void setAppDateStart(Date appDateStart) {
		this.appDateStart = appDateStart;
	}

	public Date getAppDateEnd() {
		return appDateEnd;
	}

	public void setAppDateEnd(Date appDateEnd) {
		this.appDateEnd = appDateEnd;
	}

	public List<String> getAreaIds() {
		return areaIds;
	}

	public void setAreaIds(List<String> areaIds) {
		this.areaIds = areaIds;
	}

}
