package cn.conac.rc.ofs.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.utils.StringUtils;
import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.ofs.entity.DepartmentEntity;
import cn.conac.rc.ofs.entity.DutyEntity;
import cn.conac.rc.ofs.service.DepartmentService;
import cn.conac.rc.ofs.service.DutyService;
import cn.conac.rc.ofs.vo.DutyVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="duty/")
public class DutyController {

	@Autowired
	DutyService service;

	@Autowired
	DepartmentService departmentService;
	
	@ApiOperation(value = "职能职责详情", httpMethod = "GET", response = DutyEntity.class, notes = "根据id获取职能职责详情")
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		DutyEntity duty = service.findById(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(duty);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "根据baseid获取职能职责列表信息", httpMethod = "GET", response = DutyEntity.class, notes = "根据baseid获取职能职责列表")
	@RequestMapping(value = "list/{baseId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByBaseId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "baseId", required = true) @PathVariable("baseId") Integer baseId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<DutyEntity>  dutyList = service.findByBaseId(baseId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(dutyList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "关联职责列表信息", httpMethod = "GET", response = DutyEntity.class, notes = "根据departId获取关联职责列表信息")
	@RequestMapping(value = "correlation/list/{departId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByDepartId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "departId", required = true) @PathVariable("departId") Integer departId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<DutyEntity>  dutyList = service.findByDepatId(departId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(dutyList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "动态查询职能职责", httpMethod = "POST", response = DutyEntity.class, notes = "根据条件动态查询职能职责，返回信息列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody DutyVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<DutyEntity> list = service.list(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "新增或者更新职能职责", httpMethod = "POST", response = DutyEntity.class, notes = "保存职能职责相关信息")
	@RequestMapping(value = "{submitType}", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> save(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "提交类别", required = true) @PathVariable("submitType") String submitType,
			@ApiParam(value = "保存对象", required = true) @RequestBody DutyEntity entity) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();

		// 业务操作
		// 当是新建的时候，重新获得DutyNum以及DutyCode
		if(entity.getId() == null){
			entity.setDutyNumber(service.findMaxDutyNumber(entity.getBaseId()) + 1);
			entity.setDutyCode(StringUtils.replace(UUID.randomUUID().toString(),"-", ""));
		} else {
			DutyEntity   dutyEntity  = service.findById(entity.getId());
			entity.setDutyCode(dutyEntity.getDutyCode());
			if(null == entity.getDutyNumber()){
				entity.setDutyNumber(dutyEntity.getDutyNumber());
			}
		}
		
		// 数据格式校验
		if(!"0".equals(submitType)){
			String valMsg = service.validate(entity);
			if (valMsg != null) {
				result.setCode(ResultPojo.CODE_FORMAT_ERR);
				result.setMsg(valMsg);
				return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
			}
		}
		
		service.save(entity);

		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "职能职责物理删除", httpMethod = "POST", response = String.class, notes = "根据id物理删除职能职责")
	@RequestMapping(value = "{id}/delete", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> delete(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		Map<String,Integer> resultInfoMap = new HashMap<String,Integer>();
		
		List<DepartmentEntity> deptEntityList =  departmentService.findByDutyId(id);
		if(deptEntityList.size() == 0){
			// service调用
			service.delete(id);
			resultInfoMap.put("id", id);
		} 
		
		resultInfoMap.put("deptCount", deptEntityList.size());

		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(resultInfoMap);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据部门库Id获取没有关联内设的职能职责", httpMethod = "GET", response = DutyEntity.class, notes = "根据部门库Id获取没有关联内设的职能职责")
	@RequestMapping(value = "/org/{id}/dutysWithNoDept", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findDutyWithNoDutyListByOrgId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<DutyEntity> list = service.findDutyWithNoDutyListByOrgId(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "复制上级相关职能职责", httpMethod = "POST", response = DutyEntity.class, notes = "根据上级部门Id复制其相关职能职责")
	@RequestMapping(value = "/{dutyIds}/{baseId}/copy", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> copy(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "dutyIds", required = true) @PathVariable("dutyIds") String dutyIds,
			@ApiParam(value = "baseId", required = true) @PathVariable("baseId") Integer baseId) throws Exception {

		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		List<DutyEntity> list = new ArrayList<>();
		// service调用
		if(StringUtils.isNotBlank(dutyIds)){
			String[] ids = dutyIds.split(",");
			List<String> idList = Arrays.asList(ids);
			list = service.copyDutyList(idList,baseId);
		}
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	//▼▼▼▼▼▼▼▼权责用DAO▼▼▼▼▼▼▼▼
	@ApiOperation(value = "findDutyBy", httpMethod = "GET", response = DutyEntity.class, notes = "findDutyBy")
	@RequestMapping(value = "findDutyBy/{orgId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findDutyBy(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "orgId", required = true) @PathVariable("orgId") String orgId) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(service.findDutyBy(orgId));
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "findByCodes", httpMethod = "GET", response = DutyEntity.class, notes = "findByCodes")
	@RequestMapping(value = "findByCodes/{codes}/{orgId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByCodes(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "codes", required = true) @PathVariable("codes") String[] codes,
			@ApiParam(value = "orgId", required = true) @PathVariable("orgId") Integer orgId) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(service.findByCodes(codes, orgId));
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "findByCode", httpMethod = "GET", response = DutyEntity.class, notes = "findByCode")
	@RequestMapping(value = "findByCode/{dutyCode}/{orgId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByCodes(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "dutyCode", required = true) @PathVariable("dutyCode") String dutyCode,
			@ApiParam(value = "orgId", required = true) @PathVariable("orgId") Integer orgId) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(service.findByCode(dutyCode, orgId));
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "findByItemIdAndOrgId", httpMethod = "GET", response = DutyEntity.class, notes = "findByItemIdAndOrgId")
	@RequestMapping(value = "findByItemIdAndOrgId/{itemId}/{orgId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findByItemIdAndOrgId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "itemId", required = true) @PathVariable("itemId") Integer itemId,
			@ApiParam(value = "orgId", required = true) @PathVariable("orgId") Integer orgId) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(service.findByItemIdAndOrgId(itemId, orgId));
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	//▲▲▲▲▲▲▲▲权责用DAO▲▲▲▲▲▲▲▲
}
