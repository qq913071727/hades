package cn.conac.rc.ofs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * OrgOpinionsEntity类
 *
 * @author beanCreator
 * @date 2016-11-01
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_ORG_OPINIONS")
public class OrgOpinionsEntity implements Serializable {

	private static final long serialVersionUID = 1477995462735257655L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("当前机构ID号")
	private Integer baseId;

	@ApiModelProperty("审批意见")
	private String orgOpinions;

	@ApiModelProperty("审批时间")
	private Date auditTime;
	
	@ApiModelProperty("审批用户ID")
	private Integer  auditUserId;

	public void setId(Integer id){
		this.id=id;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="mySeqGenerator")
	@SequenceGenerator(name = "mySeqGenerator", sequenceName = "s_ofs_org_opinions", allocationSize = 1)
	public Integer getId(){
		return id;
	}

	public void setBaseId(Integer baseId){
		this.baseId=baseId;
	}

	@Column(name="current_org_id")
	public Integer getBaseId(){
		return baseId;
	}

	public void setOrgOpinions(String orgOpinions){
		this.orgOpinions=orgOpinions;
	}

	public String getOrgOpinions(){
		return orgOpinions;
	}

	public void setAuditTime(Date auditTime){
		this.auditTime=auditTime;
	}

	public Date getAuditTime(){
		return auditTime;
	}

	public Integer getAuditUserId() {
		return auditUserId;
	}

	public void setAuditUserId(Integer auditUserId) {
		this.auditUserId = auditUserId;
	}
	
}
