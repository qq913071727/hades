package cn.conac.rc.ofs.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.rc.framework.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * VHistOrgsInfoEntity类
 *
 * @author beanCreator
 * @date 2017-02-10
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="RCO_V_HIST_ORGS_INFO_2")
public class VHistOrgsInfo2Entity extends BaseEntity<VHistOrgsInfo2Entity> implements Serializable {

	private static final long serialVersionUID = 5949416771658523L;

	@ApiModelProperty("orgId")
	private Integer orgId;

	@ApiModelProperty("orgName")
	private String orgName;

	@ApiModelProperty("type")
	private String type;

	@ApiModelProperty("areaId")
	private String areaId;

	@ApiModelProperty("regUserId")
	private Integer regUserId;

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public Integer getRegUserId() {
		return regUserId;
	}

	public void setRegUserId(Integer regUserId) {
		this.regUserId = regUserId;
	}
}
