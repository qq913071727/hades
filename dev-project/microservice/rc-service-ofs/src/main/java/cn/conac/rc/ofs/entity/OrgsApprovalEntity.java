package cn.conac.rc.ofs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.rc.framework.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@Entity
@Table(name="RCO_V_ORGS_APPROVAL")
public class OrgsApprovalEntity extends BaseEntity<OrgsApprovalEntity> implements Serializable {

	private static final long serialVersionUID = 1472903587533641747L;

	@ApiModelProperty("批文名称")
	private String name;

	@ApiModelProperty("批文类型")
	private String appType;

	@ApiModelProperty("批文号")
	private String appNum;

	@ApiModelProperty("批文时间")
	private Date appDate;

	@ApiModelProperty("发放单位")
	private String payOrgName;

	@ApiModelProperty("删除原因")
	private String removeReason;

	@ApiModelProperty("是否重定义")
	private String isRedefined;

	@ApiModelProperty("地区ID")
	private String areaId;

	@ApiModelProperty("地区CODE")
	private String areaCode;

	@ApiModelProperty("文件ID")
	private String fileId;

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setAppType(String appType){
		this.appType=appType;
	}

	public String getAppType(){
		return appType;
	}

	public void setAppNum(String appNum){
		this.appNum=appNum;
	}

	public String getAppNum(){
		return appNum;
	}

	public void setAppDate(Date appDate){
		this.appDate=appDate;
	}

	public Date getAppDate(){
		return appDate;
	}

	public void setPayOrgName(String payOrgName){
		this.payOrgName=payOrgName;
	}

	public String getPayOrgName(){
		return payOrgName;
	}

	public String getRemoveReason() {
		return removeReason;
	}

	public void setRemoveReason(String removeReason) {
		this.removeReason = removeReason;
	}

	public void setIsRedefined(String isRedefined){
		this.isRedefined=isRedefined;
	}

	public String getIsRedefined(){
		return isRedefined;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

}
