package cn.conac.rc.ofs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.framework.utils.BeanMapper;
import cn.conac.rc.ofs.entity.OrgOthersEntity;
import cn.conac.rc.ofs.repository.OrgOthersRepository;
import cn.conac.rc.ofs.vo.OrgOthersVo;

@Service
public class OrgOthersService extends GenericService<OrgOthersEntity, Integer> {

	@Autowired
	private OrgOthersRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(OrgOthersVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<OrgOthersEntity> list(OrgOthersVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<OrgOthersEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<OrgOthersEntity> createCriteria(OrgOthersVo param) {
		Criteria<OrgOthersEntity> dc = new Criteria<OrgOthersEntity>();
		// TODO 具体条件赋值

		return dc;
	}
	
	/**
	 * 将旧的其他信息复制一份，新建一份新的其他信息
	 * @param oldBaseId
	 * @param newBaseId
	 * @return 
	 */
	public OrgOthersEntity copyOrgOthersInfo(Integer oldBaseId, Integer newBaseId){
		// 通过OldBaseID获取旧的其他信息
		OrgOthersEntity oldOrgOthers = repository.findOne(oldBaseId);
		// 判断为空
		if(oldOrgOthers == null){
			return null;
		}
		// 复制旧其他息到新的Bean中
		OrgOthersEntity newOrgOthers = new OrgOthersEntity();
		BeanMapper.copy(oldOrgOthers, newOrgOthers);
		newOrgOthers.setId(newBaseId);
		// 返回新建的其他信息
		return repository.save(newOrgOthers);
	}

//	/**
//	 * 更新处理
//	 * @param id
//	 * @param entity
//	 * @return
//	 */
//	public OrgOthersEntity update(Integer id, OrgOthersEntity entity){
//		// 检索要更新的数据信息
//		OrgOthersEntity targetEntity = repository.findOne(id);
//		// 空判断
//		if(targetEntity == null){
//			return null;
//		}
//		// 复制表信息
//		BeanMapper.copy(entity,targetEntity,new String[]{"id"});
//		// 保存信息并返回
//		return repository.save(targetEntity);
//	}

}
