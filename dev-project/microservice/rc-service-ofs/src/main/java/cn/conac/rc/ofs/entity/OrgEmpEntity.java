package cn.conac.rc.ofs.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * OrgEmpEntity类
 *
 * @author beanCreator
 * @date 2016-11-01
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_ORG_EMP")
public class OrgEmpEntity implements Serializable {

	private static final long serialVersionUID = 1477995462700629609L;
	@Id
	@ApiModelProperty("ID")
	private Integer id;

	@ApiModelProperty("编制总数")
	private Integer empNum;

	@ApiModelProperty("行政编制数")
	private Integer polNum;

	@ApiModelProperty("工勤编制")
	private Integer workNum;

	@ApiModelProperty("其他编制数")
	private Integer otherNum;

	@ApiModelProperty("机构领导职数总数")
	private Integer orgLeaderNum;

	@ApiModelProperty("机构领导职数正职数")
	private Integer orgMleaderNum;

	@ApiModelProperty("机构领导职数副职数")
	private Integer orgSleaderNum;

	@ApiModelProperty("编制总数描述")
	private String empRemarks;

	@ApiModelProperty("行政编制数描述")
	private String polRemarks;

	@ApiModelProperty("工勤编制描述")
	private String workRemarks;

	@ApiModelProperty("其他编制数描述")
	private String otherRemarks;

	@ApiModelProperty("部门领导职数总数")
	private Integer depLeaderNum;

	@ApiModelProperty("部门领导职数正职")
	private Integer depMleaderNum;

	@ApiModelProperty("部门领导职数副职")
	private Integer depSleaderNum;

	@ApiModelProperty("机构领导职数高配机构总计")
	private Integer orgLeaderHighNum;

	@ApiModelProperty("内设机构领导职数高配部门总计")
	private Integer depLeaderHighNum;

	@ApiModelProperty("机构领导职数描述")
	private String orgLeaderRemarks;

	@ApiModelProperty("机构领导职数正职数描述")
	private String orgMleaderRemarks;

	@ApiModelProperty("机构领导职数副职数描述")
	private String orgSleaderRemarks;

	@ApiModelProperty("部门领导职数正职描述")
	private String depMleaderRemarks;

	@ApiModelProperty("部门领导职数副职描述")
	private String depSleaderRemarks;

	@ApiModelProperty("机构领导职数高配机构描述")
	private String orgLeaderHighRemarks;

	@ApiModelProperty("领导职数高配内设机构描述")
	private String depLeaderHighRemarks;

	@ApiModelProperty("depLeaderOtherNum")
	private Integer depLeaderOtherNum;

	@ApiModelProperty("全额拨款编制数")
	private Integer fullAppropriationNum;

	@ApiModelProperty("差额拨款编制数")
	private Integer balanceAppropriationNum;

	@ApiModelProperty("经费自理编制数")
	private Integer fundSelfCareNum;

	@ApiModelProperty("fullAppropriationRemarks")
	private String fullAppropriationRemarks;

	@ApiModelProperty("balanceAppropriationRemarks")
	private String balanceAppropriationRemarks;

	@ApiModelProperty("fundSelfCareRemarks")
	private String fundSelfCareRemarks;

	@ApiModelProperty("部门领导职数描述")
	private String depLeaderRemarks;

	@ApiModelProperty("depLeaderOtherRemarks")
	private String depLeaderOtherRemarks;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setEmpNum(Integer empNum){
		this.empNum=empNum;
	}

	public Integer getEmpNum(){
		return empNum;
	}

	public void setPolNum(Integer polNum){
		this.polNum=polNum;
	}

	public Integer getPolNum(){
		return polNum;
	}

	public void setWorkNum(Integer workNum){
		this.workNum=workNum;
	}

	public Integer getWorkNum(){
		return workNum;
	}

	public void setOtherNum(Integer otherNum){
		this.otherNum=otherNum;
	}

	public Integer getOtherNum(){
		return otherNum;
	}

	public void setOrgLeaderNum(Integer orgLeaderNum){
		this.orgLeaderNum=orgLeaderNum;
	}

	public Integer getOrgLeaderNum(){
		return orgLeaderNum;
	}

	public void setOrgMleaderNum(Integer orgMleaderNum){
		this.orgMleaderNum=orgMleaderNum;
	}

	public Integer getOrgMleaderNum(){
		return orgMleaderNum;
	}

	public void setOrgSleaderNum(Integer orgSleaderNum){
		this.orgSleaderNum=orgSleaderNum;
	}

	public Integer getOrgSleaderNum(){
		return orgSleaderNum;
	}

	public void setEmpRemarks(String empRemarks){
		this.empRemarks=empRemarks;
	}

	public String getEmpRemarks(){
		return empRemarks;
	}

	public void setPolRemarks(String polRemarks){
		this.polRemarks=polRemarks;
	}

	public String getPolRemarks(){
		return polRemarks;
	}

	public void setWorkRemarks(String workRemarks){
		this.workRemarks=workRemarks;
	}

	public String getWorkRemarks(){
		return workRemarks;
	}

	public void setOtherRemarks(String otherRemarks){
		this.otherRemarks=otherRemarks;
	}

	public String getOtherRemarks(){
		return otherRemarks;
	}

	public void setDepLeaderNum(Integer depLeaderNum){
		this.depLeaderNum=depLeaderNum;
	}

	public Integer getDepLeaderNum(){
		return depLeaderNum;
	}

	public void setDepMleaderNum(Integer depMleaderNum){
		this.depMleaderNum=depMleaderNum;
	}

	public Integer getDepMleaderNum(){
		return depMleaderNum;
	}

	public void setDepSleaderNum(Integer depSleaderNum){
		this.depSleaderNum=depSleaderNum;
	}

	public Integer getDepSleaderNum(){
		return depSleaderNum;
	}

	public void setOrgLeaderHighNum(Integer orgLeaderHighNum){
		this.orgLeaderHighNum=orgLeaderHighNum;
	}

	public Integer getOrgLeaderHighNum(){
		return orgLeaderHighNum;
	}

	public void setDepLeaderHighNum(Integer depLeaderHighNum){
		this.depLeaderHighNum=depLeaderHighNum;
	}

	public Integer getDepLeaderHighNum(){
		return depLeaderHighNum;
	}

	public void setOrgLeaderRemarks(String orgLeaderRemarks){
		this.orgLeaderRemarks=orgLeaderRemarks;
	}

	public String getOrgLeaderRemarks(){
		return orgLeaderRemarks;
	}

	public void setOrgMleaderRemarks(String orgMleaderRemarks){
		this.orgMleaderRemarks=orgMleaderRemarks;
	}

	public String getOrgMleaderRemarks(){
		return orgMleaderRemarks;
	}

	public void setOrgSleaderRemarks(String orgSleaderRemarks){
		this.orgSleaderRemarks=orgSleaderRemarks;
	}

	public String getOrgSleaderRemarks(){
		return orgSleaderRemarks;
	}

	public void setDepMleaderRemarks(String depMleaderRemarks){
		this.depMleaderRemarks=depMleaderRemarks;
	}

	public String getDepMleaderRemarks(){
		return depMleaderRemarks;
	}

	public void setDepSleaderRemarks(String depSleaderRemarks){
		this.depSleaderRemarks=depSleaderRemarks;
	}

	public String getDepSleaderRemarks(){
		return depSleaderRemarks;
	}

	public void setOrgLeaderHighRemarks(String orgLeaderHighRemarks){
		this.orgLeaderHighRemarks=orgLeaderHighRemarks;
	}

	public String getOrgLeaderHighRemarks(){
		return orgLeaderHighRemarks;
	}

	public void setDepLeaderHighRemarks(String depLeaderHighRemarks){
		this.depLeaderHighRemarks=depLeaderHighRemarks;
	}

	public String getDepLeaderHighRemarks(){
		return depLeaderHighRemarks;
	}

	public void setDepLeaderOtherNum(Integer depLeaderOtherNum){
		this.depLeaderOtherNum=depLeaderOtherNum;
	}

	public Integer getDepLeaderOtherNum(){
		return depLeaderOtherNum;
	}

	public void setFullAppropriationNum(Integer fullAppropriationNum){
		this.fullAppropriationNum=fullAppropriationNum;
	}

	public Integer getFullAppropriationNum(){
		return fullAppropriationNum;
	}

	public void setBalanceAppropriationNum(Integer balanceAppropriationNum){
		this.balanceAppropriationNum=balanceAppropriationNum;
	}

	public Integer getBalanceAppropriationNum(){
		return balanceAppropriationNum;
	}

	public void setFundSelfCareNum(Integer fundSelfCareNum){
		this.fundSelfCareNum=fundSelfCareNum;
	}

	public Integer getFundSelfCareNum(){
		return fundSelfCareNum;
	}

	public void setFullAppropriationRemarks(String fullAppropriationRemarks){
		this.fullAppropriationRemarks=fullAppropriationRemarks;
	}

	public String getFullAppropriationRemarks(){
		return fullAppropriationRemarks;
	}

	public void setBalanceAppropriationRemarks(String balanceAppropriationRemarks){
		this.balanceAppropriationRemarks=balanceAppropriationRemarks;
	}

	public String getBalanceAppropriationRemarks(){
		return balanceAppropriationRemarks;
	}

	public void setFundSelfCareRemarks(String fundSelfCareRemarks){
		this.fundSelfCareRemarks=fundSelfCareRemarks;
	}

	public String getFundSelfCareRemarks(){
		return fundSelfCareRemarks;
	}

	public void setDepLeaderRemarks(String depLeaderRemarks){
		this.depLeaderRemarks=depLeaderRemarks;
	}

	public String getDepLeaderRemarks(){
		return depLeaderRemarks;
	}

	public void setDepLeaderOtherRemarks(String depLeaderOtherRemarks){
		this.depLeaderOtherRemarks=depLeaderOtherRemarks;
	}

	public String getDepLeaderOtherRemarks(){
		return depLeaderOtherRemarks;
	}

}
