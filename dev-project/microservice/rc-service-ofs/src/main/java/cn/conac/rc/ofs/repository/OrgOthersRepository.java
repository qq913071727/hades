package cn.conac.rc.ofs.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.OrgOthersEntity;

@Repository
public interface OrgOthersRepository extends GenericDao<OrgOthersEntity, Integer> {
}
