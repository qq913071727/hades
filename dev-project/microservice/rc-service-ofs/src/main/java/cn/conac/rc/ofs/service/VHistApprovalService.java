package cn.conac.rc.ofs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.ofs.entity.VHistApprovalEntity;
import cn.conac.rc.ofs.repository.VHistApprovalRepository;

@Service
public class VHistApprovalService extends GenericService<VHistApprovalEntity, String> {

	@Autowired
	private VHistApprovalRepository repository;

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public List<VHistApprovalEntity> list(String orgId) throws Exception {
		try {
			return repository.findByOrgIdOrderByAppDateDescIsHistoryAscUpdateTimeDesc(orgId);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
