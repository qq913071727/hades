package cn.conac.rc.ofs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.ofs.entity.OrgStatusEntity;
import cn.conac.rc.ofs.repository.OrgStatusRepository;
import cn.conac.rc.ofs.vo.OrgStatusVo;

@Service
public class OrgStatusService extends GenericService<OrgStatusEntity, Integer> {

	@Autowired
	private OrgStatusRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(OrgStatusVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<OrgStatusEntity> list(OrgStatusVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<OrgStatusEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<OrgStatusEntity> createCriteria(OrgStatusVo param) {
		Criteria<OrgStatusEntity> dc = new Criteria<OrgStatusEntity>();
		// TODO 具体条件赋值

		return dc;
	}

//	/**
//	 * 更新处理
//	 * @param id
//	 * @param entity
//	 * @return
//	 */
//	public OrgStatusEntity update(Integer id, OrgStatusEntity entity){
//		// 检索要更新的数据信息
//		OrgStatusEntity targetEntity = repository.findOne(id);
//		// 空判断
//		if(targetEntity == null){
//			return null;
//		}
//		// 复制表信息
//		BeanMapper.copy(entity,targetEntity,new String[]{"id"});
//		// 保存信息并返回
//		return repository.save(targetEntity);
//	}

}
