package cn.conac.rc.ofs.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.RcOrgEncodeRecordEntity;

/**
 * RcOrgEncodeRecordRepository类
 *
 * @author repositoryCreator
 * @date 2016-11-23
 * @version 1.0
 */
@Repository
public interface RcOrgEncodeRecordRepository extends GenericDao<RcOrgEncodeRecordEntity, String> {
	
//	/**
//	 * 通过findId查找当前部门编码
//	 * @param findId
//	 * @return
//	 */
//	@Query("select bean from RcOrgEncodeRecordEntity bean where bean.fId=:findId")
//	public RcOrgEncodeRecordEntity findByFindId(@Param("findId") String findId);
	
	
//	/**
//	 * 通过findId删除当前部门编码
//	 * @param findId
//	 * @return void
//	 */
//	@Query(value ="delete from RC_ORG_ENCODE_RECORD code where code.f_id =:findId", nativeQuery = true)
//	@Modifying
//	public void deleteByFindId(@Param("findId") String findId);
}
