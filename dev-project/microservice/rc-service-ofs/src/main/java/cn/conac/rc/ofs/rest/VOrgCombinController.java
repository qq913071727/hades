package cn.conac.rc.ofs.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.ofs.entity.VOrgCombinEntity;
import cn.conac.rc.ofs.service.VOrgCombinService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * VOrgCombinController类
 *
 * @author controllerCreator
 * @date 2017-01-16
 * @version 1.0
 */
@RestController
@RequestMapping(value="vOrgCombin/")
public class VOrgCombinController {

	@Autowired
	VOrgCombinService service;

	@ApiOperation(value = "通过部门的baseId来获得并入部门信息", httpMethod = "GET", response = VOrgCombinEntity.class, notes = "通过部门的baseId来获得并入部门信息")
	@RequestMapping(value = "{baseId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "baseId", required = true) @PathVariable("baseId") Integer baseId) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<VOrgCombinEntity> combinList = service.findByBaseId(baseId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(combinList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
