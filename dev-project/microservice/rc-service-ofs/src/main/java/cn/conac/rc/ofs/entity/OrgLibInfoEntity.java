package cn.conac.rc.ofs.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * OfsOrgLibInfoEntity类
 *
 * @author beanCreator
 * @date 2017-07-10
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="ofs_org_lib_info")
public class OrgLibInfoEntity implements Serializable {

	private static final long serialVersionUID = 1499669555941328623L;

	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("baseId")
	private Integer baseId;

	@ApiModelProperty("orgName")
	private String orgName;

	@ApiModelProperty("type")
	private String type;

	@ApiModelProperty("ownSys")
	private String ownSys;

	@ApiModelProperty("status")
	private String status;

	@ApiModelProperty("areaId")
	private String areaId;

	@ApiModelProperty("areaCode")
	private String areaCode;

	@ApiModelProperty("isComp")
	private String isComp;

	@ApiModelProperty("isSecret")
	private String isSecret;

	@ApiModelProperty("siUserCode")
	private String siUserCode;

	@ApiModelProperty("orgDomainName")
	private String orgDomainName;

	@ApiModelProperty("parentId")
	private Integer parentId;

	@ApiModelProperty("orgParentName")
	private String orgParentName;
	
	@ApiModelProperty("备注")
	private String remarks;
	
	@ApiModelProperty("编码")
	private String code;
	
	@ApiModelProperty("排序号")
	private Integer sort;
	
	@ApiModelProperty("地方编办")
	private String localCode;
	
	@ApiModelProperty("区域名称")
	private String areaName;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setBaseId(Integer baseId){
		this.baseId=baseId;
	}

	public Integer getBaseId(){
		return baseId;
	}

	public void setOrgName(String orgName){
		this.orgName=orgName;
	}

	public String getOrgName(){
		return orgName;
	}

	public void setType(String type){
		this.type=type;
	}

	public String getType(){
		return type;
	}

	public void setOwnSys(String ownSys){
		this.ownSys=ownSys;
	}

	public String getOwnSys(){
		return ownSys;
	}

	public void setStatus(String status){
		this.status=status;
	}

	public String getStatus(){
		return status;
	}

	public void setAreaId(String areaId){
		this.areaId=areaId;
	}

	public String getAreaId(){
		return areaId;
	}

	public void setAreaCode(String areaCode){
		this.areaCode=areaCode;
	}

	public String getAreaCode(){
		return areaCode;
	}

	public void setIsComp(String isComp){
		this.isComp=isComp;
	}

	public String getIsComp(){
		return isComp;
	}

	public void setIsSecret(String isSecret){
		this.isSecret=isSecret;
	}

	public String getIsSecret(){
		return isSecret;
	}

	public void setSiUserCode(String siUserCode){
		this.siUserCode=siUserCode;
	}

	public String getSiUserCode(){
		return siUserCode;
	}

	public void setOrgDomainName(String orgDomainName){
		this.orgDomainName=orgDomainName;
	}

	public String getOrgDomainName(){
		return orgDomainName;
	}

	public void setParentId(Integer parentId){
		this.parentId=parentId;
	}

	public Integer getParentId(){
		return parentId;
	}

	public void setOrgParentName(String orgParentName){
		this.orgParentName=orgParentName;
	}

	public String getOrgParentName(){
		return orgParentName;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getLocalCode() {
		return localCode;
	}

	public void setLocalCode(String localCode) {
		this.localCode = localCode;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
}
