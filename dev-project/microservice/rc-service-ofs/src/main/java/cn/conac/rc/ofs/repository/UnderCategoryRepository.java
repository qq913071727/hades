package cn.conac.rc.ofs.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.ofs.entity.UnderCategoryEntity;
import cn.conac.rc.framework.repository.GenericDao;

/**
 * UnderCategoryRepository类
 *
 * @author repositoryCreator
 * @date 2016-11-25
 * @version 1.0
 */
@Repository
public interface UnderCategoryRepository extends GenericDao<UnderCategoryEntity, Integer> {
}
