package cn.conac.rc.ofs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.ofs.entity.FileEntity;
import cn.conac.rc.ofs.repository.FileRepository;

@Service
public class FileService extends GenericService<FileEntity, Integer> {

	@Autowired
	private FileRepository fileRepository;
	
	/**
	 * 根据文件ID检索文件信息
	 * 
	 * @param id 文件ID
	 * @return 相关文件信息
	 */
	public FileEntity searchFileById(Integer id) {
		return fileRepository.findById(id);
	}

}

