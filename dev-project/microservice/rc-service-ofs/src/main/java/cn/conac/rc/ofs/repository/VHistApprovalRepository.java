package cn.conac.rc.ofs.repository;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.VHistApprovalEntity;

@Repository
public interface VHistApprovalRepository extends GenericDao<VHistApprovalEntity, String> {

	/**
	 * 通过部门的OrgId来获得部门对应的历史沿革列表信息
	 * @param orgId
	 * @return
	 */
	List<VHistApprovalEntity> findByOrgIdOrderByAppDateDescIsHistoryAscUpdateTimeDesc(@Param("orgId") String orgId);
}
