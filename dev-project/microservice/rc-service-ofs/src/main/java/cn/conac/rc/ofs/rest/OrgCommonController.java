package cn.conac.rc.ofs.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.ofs.entity.OrganizationBaseEntity;
import cn.conac.rc.ofs.service.OrgCommonService;
import cn.conac.rc.ofs.vo.OrgChangeVo;
import cn.conac.rc.ofs.vo.OrgRevVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="orgCommon/")
public class OrgCommonController {

	@Autowired
	OrgCommonService orgCommonService;

	@ApiOperation(value = "三定变更", httpMethod = "POST", response = OrganizationBaseEntity.class, notes = "数据维护，三定变更功能")
	@RequestMapping(value = "change", method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<ResultPojo> orgChange(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "状态信息entity", required = true) @RequestBody OrgChangeVo orgChangeVo){

	    // Service方法调用
		Integer	newBaseId = orgCommonService.orgChange(orgChangeVo);
	
		Map<String, Integer> idmap =new HashMap<String,Integer>();
		
		idmap.put("newBaseId", newBaseId);

		return new ResponseEntity<>(new ResultPojo(ResultPojo.CODE_SUCCESS,ResultPojo.MSG_SUCCESS,idmap), HttpStatus.OK);
	}
	
	@ApiOperation(value = "三定撤并保存", httpMethod = "POST", response = OrganizationBaseEntity.class, notes = "数据维护，三定撤并保存功能")
	@RequestMapping(value = "{orgId}/{bianBanFlag}/rev/save", method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<ResultPojo> orgRevSave(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "orgId", required = true) @PathVariable("orgId") Integer orgId,
			@ApiParam(value = "bianBanFlag", required = true) @PathVariable("bianBanFlag") String bianBanFlag,
			@ApiParam(value = "三定撤并信息VO", required = true) @RequestBody OrgRevVo orgRevVo){

		// Service方法调用
		String retStr = orgCommonService.orgRevSave(orgId, bianBanFlag, orgRevVo);
		
		if(null == retStr) {
			return new ResponseEntity<>(new ResultPojo(ResultPojo.CODE_NULL_ERR, 
					"根据部门库ID【" + orgId+  "】查找部门基本信息的" + ResultPojo.MSG_NULL_ERR), HttpStatus.OK);
		}
		Map<String,String> idmap =new HashMap<String,String>();
		
		if(StringUtils.isNotEmpty(retStr)) {
			String [] ids = retStr.split(",");
			idmap.put("newBaseId", ids[0] );
			if(ids.length > 1) {
				idmap.put("findId", ids[1] );
			}
		}
		return new ResponseEntity<>(new ResultPojo(ResultPojo.CODE_SUCCESS,ResultPojo.MSG_SUCCESS, idmap), HttpStatus.OK);
	}
	
	@ApiOperation(value = "三定撤并更新", httpMethod = "POST", response = OrganizationBaseEntity.class, notes = "数据维护，三定撤并更新功能")
	@RequestMapping(value = "{baseId}/rev/update", method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<ResultPojo> orgRevUpdate(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "baseId", required = true) @PathVariable("baseId") Integer baseId,
			@ApiParam(value = "三定撤并信息VO", required = true) @RequestBody OrgRevVo orgRevVo){

		// Service方法调用
		Integer  retBaseId = orgCommonService.orgRevUpdate(baseId, orgRevVo);
		
		Map<String,Integer> idmap =new HashMap<String,Integer>();
		
		idmap.put("baseId", retBaseId);

		return new ResponseEntity<>(new ResultPojo(ResultPojo.CODE_SUCCESS,ResultPojo.MSG_SUCCESS,idmap), HttpStatus.OK);
	}
	
//	@ApiOperation(value = "物理删除待提交部门信息", httpMethod = "POST", response = OrganizationEntity.class, notes = "根据baseId物理删除待提交部门信息")
//	@RequestMapping(value = "{baseId}/delete", method = RequestMethod.POST)
//	public ResponseEntity<ResultPojo> delete(HttpServletRequest request, HttpServletResponse response,
//			@ApiParam(value = "baseId", required = true) @PathVariable("baseId") Integer baseId) {
//
//		Map<String,Integer> resultInfoMap = new HashMap<String,Integer>();
//		
//		orgCommonService.deletePreReleaseOrgs(baseId);
//		resultInfoMap.put("baseId", baseId);
//		
//		return new ResponseEntity<>(new ResultPojo(ResultPojo.CODE_SUCCESS,ResultPojo.MSG_SUCCESS, resultInfoMap), HttpStatus.OK);
//	}

	@ApiOperation(value = "是否能够三定变更或部门撤并的校验", httpMethod = "GET", response = String.class, notes = "通过OrgId校验该机构是否能够三定变更或部门撤并")
	@RequestMapping(value = "{orgId}/check", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> check4ChangeOrRev(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "orgId", required = true) @PathVariable("orgId") Integer orgId) {

		Map<String,String> resultInfoMap = new HashMap<>();

		String checkRtl = orgCommonService.ofsChangeOrRevCheck(orgId);
		resultInfoMap.put("checkRtl", checkRtl);

		return new ResponseEntity<>(new ResultPojo(ResultPojo.CODE_SUCCESS,ResultPojo.MSG_SUCCESS, resultInfoMap), HttpStatus.OK);
	}
	
	@ApiOperation(value = "物理删除审核通过三定信息（部门库信息不删除只是对baseId置空）", httpMethod = "POST",  notes = "根据部门库ID物理删除审核通过三定信息（部门库信息不删除只是对baseId置空）")
	@RequestMapping(value = "audited/{orgId}/delete", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> orgAuditedDelete(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "orgId", required = true) @PathVariable("orgId") Integer orgId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		
		Map<String,Object> resultInfoMap = new HashMap<String,Object>();
		
		//  删除部门相关的所有表
		List<Integer>  baseIdList = orgCommonService.deleteAuditedOrgs(orgId);
		resultInfoMap.put("orgId", orgId);
		resultInfoMap.put("baseIds", baseIdList);
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(resultInfoMap);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "物理删除历史沿革节点信息和待提交信息（最新节点场合部门库信息不删除只是对baseId置空）", httpMethod = "POST",  notes = "根据baseId物理删除历史沿革节点信息和待提交信息（最新节点场合部门库信息不删除只是对baseId置空）")
	@RequestMapping(value = "{baseId}/delete", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> orgHistNodeDelete(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "baseId", required = true) @PathVariable("baseId") Integer baseId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		
		Map<String,Object> resultInfoMap = new HashMap<String,Object>();
		
		//  删除部门相关的所有表
		Integer  nextNodeBaseId = orgCommonService.deleteHistNodeOrg(baseId);
		if(null != nextNodeBaseId) {
			resultInfoMap.put("nextNodeBaseId", nextNodeBaseId);
		}
		resultInfoMap.put("baseId", baseId);
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(resultInfoMap);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
