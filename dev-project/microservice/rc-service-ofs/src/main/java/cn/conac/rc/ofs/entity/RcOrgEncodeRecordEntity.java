package cn.conac.rc.ofs.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * RcOrgEncodeRecordEntity类
 *
 * @author beanCreator
 * @date 2016-11-23
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="RC_ORG_ENCODE_RECORD")
public class RcOrgEncodeRecordEntity implements Serializable {

	private static final long serialVersionUID = 1479882672415518438L;

	@Id
	@ApiModelProperty("ID号")
	private String id;

	@ApiModelProperty("查找码:根据地区码(六位)、基本码(三位)生成")
	@NotNull
	private String fId;

	@ApiModelProperty("顺序号:每生成一个编码顺序加1")
	@NotNull
	private String codeNum;

	public void setId(String id){
		this.id=id;
	}

	public String getId(){
		return id;
	}

	public void setFId(String fId){
		this.fId=fId;
	}

	public String getFId(){
		return fId;
	}

	public void setCodeNum(String codeNum){
		this.codeNum=codeNum;
	}

	public String getCodeNum(){
		return codeNum;
	}

}
