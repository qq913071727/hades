package cn.conac.rc.ofs.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.VAuditListEntity;

/**
 * VAuditListRepository类
 *
 * @author repositoryCreator
 * @date 2016-11-21
 * @version 1.0
 */
@Repository
public interface VAuditListRepository extends GenericDao<VAuditListEntity, Integer> {
}
