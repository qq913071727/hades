package cn.conac.rc.ofs.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.ofs.entity.VOrgPreSubEntity;
import cn.conac.rc.ofs.service.VOrgPreSubService;
import cn.conac.rc.ofs.vo.VOrgPreSubVo;
import cn.conac.rc.framework.vo.ResultPojo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * VOrgPreSubController类
 *
 * @author controllerCreator
 * @date 2016-12-14
 * @version 1.0
 */
@RestController
@RequestMapping(value="vOrgPreSub/")
public class VOrgPreSubController {

	@Autowired
	VOrgPreSubService service;

	@ApiOperation(value = "详情", httpMethod = "GET", response = VOrgPreSubEntity.class, notes = "根据id获取资源详情")
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		VOrgPreSubEntity entity = service.findById(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "个数", httpMethod = "POST", response = Long.class, notes = "根据条件获得资源数量")
	@RequestMapping(value = "count", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> count(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody VOrgPreSubVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		long cnt = service.count(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(cnt);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "列表", httpMethod = "POST", response = VOrgPreSubEntity.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody VOrgPreSubVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<VOrgPreSubEntity> list = service.list(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
