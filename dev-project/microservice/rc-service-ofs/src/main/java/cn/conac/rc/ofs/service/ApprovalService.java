package cn.conac.rc.ofs.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.framework.utils.BeanMapper;
import cn.conac.rc.framework.utils.DateUtils;
import cn.conac.rc.ofs.entity.ApprovalEntity;
import cn.conac.rc.ofs.entity.OrgStatusEntity;
import cn.conac.rc.ofs.repository.ApprovalRepository;
import cn.conac.rc.ofs.repository.OrgStatusRepository;
import cn.conac.rc.ofs.vo.AppdateCheckVo;

@Service
@Transactional
public class ApprovalService extends GenericService<ApprovalEntity, Integer> {

	@Autowired
	private ApprovalRepository appRepository;

	@Autowired
	private OrgStatusRepository orgStatusRepository;

	/**
	 * 查询当前部门批文信息列表（包括主批文，副批文）
	 * @param orgId
	 * @return 批文信息列表
	 */
	public List<ApprovalEntity> findDetailByOrgId(Integer orgId) {
		return appRepository.findDetailByOrgId(orgId);
	}	
	
	/**
	 * 查询部门批文信息列表（App用）
	 * @param orgId
	 * @return 批文信息列表
	 */
	public List<ApprovalEntity> findByOrgId(Integer orgId) {
		return appRepository.findByOrgId(orgId);
	}
	
	/**
	 * 通过旧baseId获取批文信息，并且新建新的批文信息
	 * @param oldBaseId，newBaseId
	 * @return
	 */
	public ApprovalEntity copyApprovalInfo(Integer oldBaseId,Integer newBaseId){
		// 获取旧Base表信息
		ApprovalEntity approvalEntity = appRepository.findOne(oldBaseId);
		// 判断为空
		if(approvalEntity==null){
			return null;
		}
		// 复制Base表信息
		ApprovalEntity copyAppEntity = new ApprovalEntity();
		BeanMapper.copy(approvalEntity,copyAppEntity);
		copyAppEntity.setId(newBaseId);
		copyAppEntity.setIsRedefined("0");
		// 保存新Base信息并返回
		return appRepository.save(copyAppEntity);
	}

	/**
	 * 校验新添加批文时间是否在新建部门批文时间之前(历史沿革批文时间校验)
	 * @param orgId
	 * @param newAppDate
	 * @return AppdateCheckVo
	 */
	public AppdateCheckVo checkAppDateBefore(Integer orgId,String newAppDate){
		// 返回结果定义
		AppdateCheckVo checkVo = new AppdateCheckVo();
		String returnFlg = "true";
		String checkAppdateStr = "";
		// 获取IsHistory字段
		OrgStatusEntity orgStatus = orgStatusRepository.findOrgStatusByOrgId(orgId);
		if(null != orgStatus && "0".equals(orgStatus.getIsHistory())){
			Date newDate = DateUtils.parseDate(newAppDate,DateUtils.FORMAT_YYYYMMDD);
			// 获取批文时间
			List<ApprovalEntity> appList = appRepository.fingApprovalInfoByOrgId(orgId);
			if(null != appList && appList.size() > 0){
				ApprovalEntity appEntity = appList.get(0);
				if(null != appEntity && null != appEntity.getAppDate()){
					Date checkAppdate = appEntity.getAppDate();
					checkAppdateStr = DateUtils.formatDate(checkAppdate);
					Integer timeDifference = DateUtils.getSecondBetweenDate(checkAppdate,newDate);
					if(timeDifference>0){
						returnFlg = "false";
					}
				}
			}
		}
		// 返回结果设定
		checkVo.setCheckResult(returnFlg);
		checkVo.setCheckAppDate(checkAppdateStr);
		return checkVo;
	}

	/**
	 * 校验新添加批文时间是否在新建部门批文时间之后(部门管理批文时间校验)
	 * @param orgId
	 * @param newAppDate
	 * @return AppdateCheckVo
	 */
	public AppdateCheckVo checkAppDateAfter(Integer orgId,String newAppDate){
		// 返回结果定义
		AppdateCheckVo checkVo = new AppdateCheckVo();
		String returnFlg = "true";
		String checkAppdateStr = "";
		// 格式化时间
		Date newDate = DateUtils.parseDate(newAppDate, DateUtils.FORMAT_YYYYMMDD);
		// 获取最新的AppDate
		List<ApprovalEntity> appList = appRepository.findLatestApprovalDate(orgId);
		if(null != appList && appList.size() > 0){
			ApprovalEntity appEntity = appList.get(0);
			if(null != appEntity && null != appEntity.getAppDate()){
				Date checkAppdate = appEntity.getAppDate();
				checkAppdateStr = DateUtils.formatDate(checkAppdate);
				Integer timeDifference = DateUtils.getSecondBetweenDate(checkAppdate,newDate);
				if(timeDifference<0){
					returnFlg = "false";
				}
			}
		}
		// 返回结果设定
		checkVo.setCheckResult(returnFlg);
		checkVo.setCheckAppDate(checkAppdateStr);
		return checkVo;
	}
}
