package cn.conac.rc.ofs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.ofs.entity.AppFilesEntity;
import cn.conac.rc.ofs.repository.AppFilesRepository;

@Service
@Transactional
public class AppFilesService extends GenericService<AppFilesEntity, Integer> {

	@Autowired
	private AppFilesRepository repository;

	/**
	 * 动态查询，分页，排序查询
	 * @param baseId
	 * @return Page
	 * @throws Exception
	 */
	public List<AppFilesEntity> listByBaseId(Integer baseId) throws Exception {
		try {
			return repository.findByBaseId(baseId);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	/**
	 * 复制AppFiles
	 * @param oldBaseId
	 * @param newBaseId
	 */
	public void copyAppFilesInfo(Integer oldBaseId, Integer newBaseId) {
		List<AppFilesEntity> appFilesList = repository.findByBaseId(oldBaseId);
		if(appFilesList != null && appFilesList.size() != 0){
			for(AppFilesEntity appFiles : appFilesList){
				AppFilesEntity newAppFiles = new AppFilesEntity();
				newAppFiles.setAppUrl(appFiles.getAppUrl());
				newAppFiles.setBaseId(newBaseId);
				repository.save(newAppFiles);
			}
		}
	}
	
	/**
	 * 根据baseId删除批文文件的信息
	 * @param baseId
	 * @throws Exception
	 */
	public void deleteByBaseId(Integer baseId) throws Exception {
		try {
			if(null != repository.findByBaseId(baseId) ){
				repository.deleteByBaseId(baseId);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
