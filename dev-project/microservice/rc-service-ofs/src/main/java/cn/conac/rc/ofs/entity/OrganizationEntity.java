package cn.conac.rc.ofs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * OrganizationEntity类
 *
 * @author beanCreator
 * @date 2016-11-02
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_ORGANIZATION")
public class OrganizationEntity implements Serializable {

	private static final long serialVersionUID = 1478053012133447168L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("baseId")
	private Integer baseId;

	@ApiModelProperty("备注")
	private String remarks;

	@ApiModelProperty("父单位")
	private Integer parentId;

	@ApiModelProperty("三定名称")
	private String name;

	@ApiModelProperty("行编事编")
	private String type;

	@ApiModelProperty("所属系统")
	private String ownSys;

	@ApiModelProperty("编码")
	private String code;

	@ApiModelProperty("状态（发布，撤并）")
	private String status;

	@ApiModelProperty("排序号")
	private Integer sort;

	@ApiModelProperty("areaId")
	private String areaId;

	@ApiModelProperty("areaCode")
	private String areaCode;

	@ApiModelProperty("是否主管")
	private String isComp;

	@ApiModelProperty("是否涉密")
	private String isSecret;

	@ApiModelProperty("地方编办")
	private String localCode;

	@ApiModelProperty("是否有下级机构")
	private String hasChild;
	
	@ApiModelProperty("录入人ID号")
	private Integer createUserId;
	
	@ApiModelProperty("创建时间")
	private Date createTime;
	
	@ApiModelProperty("更新用户ID")
	private Integer  updateUserId;
	
	@ApiModelProperty("更新时间")
	private Date updateTime;
		
	@ApiModelProperty("siUserCode")
	private String siUserCode;

	@ApiModelProperty("机构域名（多个以分号分割）")
	private String orgDomainName;
	
	public void setId(Integer id) {
		this.id=id;
		//将主表的ID也同时赋值给部门库的排序号
		if(null == this.getSort()){
			this.setSort(id);
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="mySeqGenerator")
	@SequenceGenerator(name = "mySeqGenerator", sequenceName = "s_ofs_organization", allocationSize = 1)
	public Integer getId(){
		return id;
	}

	public void setBaseId(Integer baseId){
		this.baseId=baseId;
	}

	public Integer getBaseId(){
		return baseId;
	}

	public void setRemarks(String remarks){
		this.remarks=remarks;
	}

	public String getRemarks(){
		return remarks;
	}

	public void setParentId(Integer parentId){
		this.parentId=parentId;
	}

	public Integer getParentId(){
		return parentId;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setType(String type){
		this.type=type;
	}

	public String getType(){
		return type;
	}

	public void setOwnSys(String ownSys){
		this.ownSys=ownSys;
	}

	public String getOwnSys(){
		return ownSys;
	}

	public void setCode(String code){
		this.code=code;
	}

	public String getCode(){
		return code;
	}

	public void setStatus(String status){
		this.status=status;
	}

	public String getStatus(){
		return status;
	}

	public void setSort(Integer sort){
		this.sort=sort;
	}

	public Integer getSort(){
		return sort;
	}

	public void setAreaId(String areaId){
		this.areaId=areaId;
	}

	public String getAreaId(){
		return areaId;
	}

	public void setAreaCode(String areaCode){
		this.areaCode=areaCode;
	}

	public String getAreaCode(){
		return areaCode;
	}

	public void setIsComp(String isComp){
		this.isComp=isComp;
	}

	public String getIsComp(){
		return isComp;
	}

	public void setIsSecret(String isSecret){
		this.isSecret=isSecret;
	}

	public String getIsSecret(){
		return isSecret;
	}

	public void setLocalCode(String localCode){
		this.localCode=localCode;
	}

	public String getLocalCode(){
		return localCode;
	}

	public void setHasChild(String hasChild){
		this.hasChild=hasChild;
	}

	public String getHasChild(){
		return hasChild;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public void setSiUserCode(String siUserCode){
		this.siUserCode=siUserCode;
	}

	public String getSiUserCode(){
		return siUserCode;
	}
	
	public void setOrgDomainName(String orgDomainName){
		this.orgDomainName=orgDomainName;
	}

	public String getOrgDomainName(){
		return orgDomainName;
	}
}
