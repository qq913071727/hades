package cn.conac.rc.ofs.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AnalzWarningEntity类
 *
 * @author beanCreator
 * @date 2016-11-01
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_ANALZ_WARNING")
public class AnalzWarningEntity implements Serializable {

	private static final long serialVersionUID = 1477992275860885656L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("warningDept")
	private Boolean warningDept;

	@ApiModelProperty("warningDuty")
	private Boolean warningDuty;

	@ApiModelProperty("warningEmpRemarks")
	private Boolean warningEmpRemarks;

	@ApiModelProperty("warningOnlineName")
	private Boolean warningOnlineName;

	@ApiModelProperty("warningDegree")
	private Integer warningDegree;

	@ApiModelProperty("haveToDelete")
	private Integer haveToDelete;

	@ApiModelProperty("areaCode")
	private String areaCode;

	@ApiModelProperty("orgId")
	private Integer orgId;

	@ApiModelProperty("orgName")
	private String orgName;

	public void setId(Integer id){
		this.id=id;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="mySeqGenerator")
	@SequenceGenerator(name = "mySeqGenerator", sequenceName = "s_ofs_analz_warning", allocationSize = 1)
	public Integer getId(){
		return id;
	}

	public void setWarningDept(Boolean warningDept){
		this.warningDept=warningDept;
	}

	public Boolean getWarningDept(){
		return warningDept;
	}

	public void setWarningDuty(Boolean warningDuty){
		this.warningDuty=warningDuty;
	}

	public Boolean getWarningDuty(){
		return warningDuty;
	}

	public void setWarningEmpRemarks(Boolean warningEmpRemarks){
		this.warningEmpRemarks=warningEmpRemarks;
	}

	public Boolean getWarningEmpRemarks(){
		return warningEmpRemarks;
	}

	public void setWarningOnlineName(Boolean warningOnlineName){
		this.warningOnlineName=warningOnlineName;
	}

	public Boolean getWarningOnlineName(){
		return warningOnlineName;
	}

	public void setWarningDegree(Integer warningDegree){
		this.warningDegree=warningDegree;
	}

	public Integer getWarningDegree(){
		return warningDegree;
	}

	public void setHaveToDelete(Integer haveToDelete){
		this.haveToDelete=haveToDelete;
	}

	public Integer getHaveToDelete(){
		return haveToDelete;
	}

	public void setAreaCode(String areaCode){
		this.areaCode=areaCode;
	}

	public String getAreaCode(){
		return areaCode;
	}

	public void setOrgId(Integer orgId){
		this.orgId=orgId;
	}

	public Integer getOrgId(){
		return orgId;
	}

	public void setOrgName(String orgName){
		this.orgName=orgName;
	}

	public String getOrgName(){
		return orgName;
	}

}
