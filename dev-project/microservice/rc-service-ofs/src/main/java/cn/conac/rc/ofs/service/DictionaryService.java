package cn.conac.rc.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

@FeignClient("RC-SERVICE-COMMON")
public interface DictionaryService {
	// 查询数据字典
	@RequestMapping(value = "/comm/getDicByKey/{key}", method = RequestMethod.GET)
	JSONObject findDicByKey(@PathVariable("key") String key) ;

}
