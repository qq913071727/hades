package cn.conac.rc.ofs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.OrgOpinionsEntity;

@Repository
public interface OrgOpinionsRepository extends GenericDao<OrgOpinionsEntity, Integer> {
	
	/**
	 * 根据baseID删除全部
	 * @param baseId
	 * @return 删除数量
	 */
	@Query(value = "delete  from ofs_org_opinions op where op.current_org_id= :baseId", nativeQuery = true)
	@Modifying
	void deleteByBaseId(@Param("baseId") Integer baseId);
	
	
	/**
	 * 通过机构BaseID获取审核意见相关数据
	 * @return
	 */
	List<OrgOpinionsEntity> findByBaseId(@Param("baseId") Integer baseId);
}
