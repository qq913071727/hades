package cn.conac.rc.ofs.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.ofs.entity.AnalzWarningEntity;
import cn.conac.rc.ofs.entity.DepartmentEntity;
import cn.conac.rc.ofs.entity.DutyEntity;
import cn.conac.rc.ofs.service.AnalzWarningService;
import cn.conac.rc.ofs.service.DepartmentService;
import cn.conac.rc.ofs.service.DutyService;
import cn.conac.rc.ofs.vo.AnalzWarningVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="analzWarning/")
public class AnalzWarningController {

	@Autowired
	AnalzWarningService service;
	
	@Autowired
	DutyService dutyService;
	
	@Autowired
	DepartmentService departService;

	@ApiOperation(value = "详情", httpMethod = "GET", response = AnalzWarningEntity.class, notes = "根据id获取资源详情")
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		AnalzWarningEntity analzWarning = service.findById(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(analzWarning);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "个数", httpMethod = "POST", response = AnalzWarningEntity.class, notes = "根据条件获得资源数量")
	@RequestMapping(value = "count", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> count(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody AnalzWarningVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		long cnt = service.count(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(cnt);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "列表", httpMethod = "POST", response = AnalzWarningEntity.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody AnalzWarningVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<AnalzWarningEntity> list = service.list(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "获取内设Id关联的职能职责", httpMethod = "GET", response = AnalzWarningEntity.class, notes = "获取内设关联的职能职责")
	@RequestMapping(value = "Department/{id}/Dutys", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findDutyByDepartment(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<DutyEntity> dutys = dutyService.findByDepatId(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(dutys);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "获取职能职责Id关联的内设", httpMethod = "GET", response = AnalzWarningEntity.class, notes = "获取职能职责Id关联的内设")
	@RequestMapping(value = "Duty/{id}/Departments", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findDepartmentByDuty(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<DepartmentEntity> departs = departService.findByDutyId(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(departs);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "引擎", httpMethod = "POST", response = AnalzWarningEntity.class, notes = "引擎")
	@RequestMapping(value = "engine", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> engine(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		service.scheduler();
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult("ok");
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
