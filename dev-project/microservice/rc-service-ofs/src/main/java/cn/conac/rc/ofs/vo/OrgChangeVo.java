package cn.conac.rc.ofs.vo;

import java.io.Serializable;

import cn.conac.rc.ofs.entity.OrgStatusEntity;
import io.swagger.annotations.ApiModel;

@ApiModel
public class OrgChangeVo implements Serializable {

	private static final long serialVersionUID = -5755721205720919999L;

	private OperationStatusVo operationStatusVo;
	
	private OrgStatusEntity orgStatusEntity;

	public OperationStatusVo getOperationStatusVo() {
		return operationStatusVo;
	}

	public void setOperationStatusVo(OperationStatusVo operationStatusVo) {
		this.operationStatusVo = operationStatusVo;
	}

	public OrgStatusEntity getOrgStatusEntity() {
		return orgStatusEntity;
	}

	public void setOrgStatusEntity(OrgStatusEntity orgStatusEntity) {
		this.orgStatusEntity = orgStatusEntity;
	}

}
