package cn.conac.rc.ofs.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * DepartmentEntity类
 *
 * @author beanCreator
 * @date 2016-11-01
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_DEPARTMENT")
public class DepartmentEntity2 implements Serializable {

	private static final long serialVersionUID = 1477992276059361839L;

	@ApiModelProperty("ID")
	private Integer id;

	@ApiModelProperty("机构ID号")
	@NotNull
	private Integer baseId;

	@ApiModelProperty("部门序号")
	private Integer depNum;

	@ApiModelProperty("部门名称")
	@NotNull
	private String deptName;
	
	@ApiModelProperty("部门名称层级展示用")
	private String displayName;

	@ApiModelProperty("上级部门ID号")
	private Integer parentId;

	@ApiModelProperty("部门类型")
	@NotNull
	private String deptFuncType;

	@ApiModelProperty("部门职能")
	private String deptDesc;

	@ApiModelProperty("备注")
	private String remarks;

	@ApiModelProperty("deptLevel")
	private Integer deptLevel;

	@ApiModelProperty("depCode")
	private String depCode;
	
	@ApiModelProperty("复制用旧ID")
	private Integer oldId4Copy;

	@ApiModelProperty("对应关联表")
	private List<DutyEntity> dutyList;

	public void setId(Integer id){
		this.id=id;
	}

	@Id
	public Integer getId(){
		return id;
	}

	public void setBaseId(Integer baseId){
		this.baseId=baseId;
	}

	@Column(name="org_id")
	public Integer getBaseId(){
		return baseId;
	}

	public void setDepNum(Integer depNum){
		this.depNum=depNum;
	}

	public Integer getDepNum(){
		return depNum;
	}

	public void setDeptName(String deptName){
		this.deptName=deptName;
	}

	public String getDeptName(){
		return deptName;
	}

	public void setParentId(Integer parentId){
		this.parentId=parentId;
	}

	public Integer getParentId(){
		return parentId;
	}

	public void setDeptFuncType(String deptFuncType){
		this.deptFuncType=deptFuncType;
	}

	public String getDeptFuncType(){
		return deptFuncType;
	}

	public void setDeptDesc(String deptDesc){
		this.deptDesc=deptDesc;
	}

	public String getDeptDesc(){
		return deptDesc;
	}

	public void setRemarks(String remarks){
		this.remarks=remarks;
	}

	public String getRemarks(){
		return remarks;
	}

	public void setDeptLevel(Integer deptLevel){
		this.deptLevel=deptLevel;
	}

	public Integer getDeptLevel(){
		return deptLevel;
	}

	public void setDepCode(String depCode){
		this.depCode=depCode;
	}

	public String getDepCode(){
		return depCode;
	}

}
