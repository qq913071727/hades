package cn.conac.rc.ofs.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.conac.rc.ofs.entity.DepartmentEntity;
import cn.conac.rc.ofs.entity.DutyEntity;

public class DepartmentTreeNodeVo implements Serializable{

	private static final long serialVersionUID = 1434158601837168L;
	
	//ID
	private String id;
	//机构ID号
	private Integer baseId;
	//部门序号
	private Integer depNum;
	//部门名称
	private String deptName;
	//部门名称显示层级
	private String displayName;
	//上级部门ID号
	private Integer parentId;
	//部门类型
	private String deptFuncType;
	//部门职能
	private String deptDesc;
	//备注
	private String remarks;
	//deptLevel
	private Integer deptLevel;
	//depCode
	private String depCode;
	//对应职责关联表
	private List<DutyEntity> dutyList;
	
	private List<DepartmentTreeNodeVo> children = new ArrayList<DepartmentTreeNodeVo>();

	public DepartmentTreeNodeVo(){}

	public DepartmentTreeNodeVo(DepartmentEntity entity){
		this.id = entity.getId().toString();
		this.baseId =entity.getBaseId();
		this.depNum = entity.getDepNum();
		this.deptName = entity.getDeptName();
		this.parentId = entity.getParentId();
		this.deptFuncType=entity.getDeptFuncType();
		this.deptDesc = entity.getDeptDesc();
		this.remarks = entity.getRemarks();
		this.deptLevel= entity.getDeptLevel();
		this.depCode = entity.getDepCode();
		this.dutyList = entity.getDutyList();
	}
	
	public List<DepartmentTreeNodeVo> getChildren() {
		return children;
	}

	public void setChildren(List<DepartmentTreeNodeVo> children) {
		this.children = children;
	}
	
	public void addChild(DepartmentTreeNodeVo child) {
		if(null == children){
			children = new ArrayList<DepartmentTreeNodeVo>();
		}
		children.add(child);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getBaseId() {
		return baseId;
	}

	public void setBaseId(Integer baseId) {
		this.baseId = baseId;
	}

	public Integer getDepNum() {
		return depNum;
	}

	public void setDepNum(Integer depNum) {
		this.depNum = depNum;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setName(String deptName) {
		this.deptName = deptName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getDeptDesc() {
		return deptDesc;
	}

	public void setDeptDesc(String deptDesc) {
		this.deptDesc = deptDesc;
	}
	
	public String getDeptFuncType() {
		return deptFuncType;
	}

	public void setDeptFuncType(String deptFuncType) {
		this.deptFuncType = deptFuncType;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getDeptLevel() {
		return deptLevel;
	}

	public void setDeptLevel(Integer deptLevel) {
		this.deptLevel = deptLevel;
	}

	public String getDepCode() {
		return depCode;
	}

	public void setDepCode(String depCode) {
		this.depCode = depCode;
	}

	public List<DutyEntity> getDutyList() {
		return dutyList;
	}

	public void setDutyList(List<DutyEntity> dutyList) {
		this.dutyList = dutyList;
	}

}
