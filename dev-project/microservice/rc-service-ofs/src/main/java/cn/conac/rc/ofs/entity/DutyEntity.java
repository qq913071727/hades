package cn.conac.rc.ofs.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DutyEntity类
 *
 * @author beanCreator
 * @date 2016-11-01
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_DUTY")
public class DutyEntity implements Serializable {

	private static final long serialVersionUID = 1477992276075208783L;
	@Id
	@ApiModelProperty("ID")
	private Integer id;

	@ApiModelProperty("机构ID号")
	@NotNull
	private Integer baseId;

	@ApiModelProperty("职责序号")
	@NotNull
	private Integer dutyNumber;

	@ApiModelProperty("职责编号")
	@NotNull
	private String dutyCode;

	@ApiModelProperty("职责内容")
	@NotNull
	private String dutyContent;

	@ApiModelProperty("备注")
	private String remarks;
	
	@ApiModelProperty("复制用旧ID")
	private Integer oldId4Copy;

	public void setId(Integer id){
		this.id=id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="mySeqGenerator")
	@SequenceGenerator(name = "mySeqGenerator", sequenceName = "s_ofs_duty", allocationSize = 1)
	public Integer getId(){
		return id;
	}

	public void setBaseId(Integer baseId){
		this.baseId=baseId;
	}
	
	@Column(name="org_id")
	public Integer getBaseId(){
		return baseId;
	}

	public void setDutyNumber(Integer dutyNumber){
		this.dutyNumber=dutyNumber;
	}

	public Integer getDutyNumber(){
		return dutyNumber;
	}

	public void setDutyCode(String dutyCode){
		this.dutyCode=dutyCode;
	}

	public String getDutyCode(){
		return dutyCode;
	}

	public void setDutyContent(String dutyContent){
		this.dutyContent=dutyContent;
	}

	public String getDutyContent(){
		return dutyContent;
	}

	public void setRemarks(String remarks){
		this.remarks=remarks;
	}

	public String getRemarks(){
		return remarks;
	}

	@Transient
	public Integer getOldId4Copy() {
		return oldId4Copy;
	}

	public void setOldId4Copy(Integer oldId4Copy) {
		this.oldId4Copy = oldId4Copy;
	}

}
