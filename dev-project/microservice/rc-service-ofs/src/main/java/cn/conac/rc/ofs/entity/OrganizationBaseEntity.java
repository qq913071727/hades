package cn.conac.rc.ofs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * OrganizationBaseEntity类
 *
 * @author beanCreator
 * @date 2016-11-01
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_ORGANIZATION_BASE")
public class OrganizationBaseEntity implements Serializable {

	private static final long serialVersionUID = 147799227632644590L;
	@Id
	@ApiModelProperty("ID")
	private Integer id;

	@ApiModelProperty("机构ORG号")
	@NotNull
	private Integer orgId;

	@ApiModelProperty("机构编号")
	private String code;

	@ApiModelProperty("机构名称")
	@NotNull
	private String name;

	@ApiModelProperty("其他名称")
	private String orgOtherName;

	@ApiModelProperty("机构类型")
	private String type;

	@ApiModelProperty("所属系统")
	private String ownSys;

	@ApiModelProperty("机构规格")
	private String mechStandard;

	@ApiModelProperty("是否主管机构")
	private String isComp;

	@ApiModelProperty("主管机构ID")
	private Integer compId;

	@ApiModelProperty("是否垂管")
	private String isManu;

	@ApiModelProperty("垂管类型")
	private String manuType;

	@ApiModelProperty("组织机构代码")
	private String orgCode;

	@ApiModelProperty("事业单位分类")
	private String insClassify;

	@ApiModelProperty("经费来源")
	private String fundSource;

	@ApiModelProperty("事业单位法人证号")
	private String insLegalNum;

	@ApiModelProperty("办公地址")
	private String address;

	@ApiModelProperty("联系电话")
	private String phoneNum;

	@ApiModelProperty("所属地区")
	private String areaId;

	@ApiModelProperty("规范简称")
	private String simpleSname;

	@ApiModelProperty("习惯简称")
	private String simpleUname;

	@ApiModelProperty("是否涉密单位")
	private String isSecret;

	@ApiModelProperty("机构性质")
	private String orgNatrue;

	@ApiModelProperty("机构类别")
	private String orgCategory;

	@ApiModelProperty("邮政编码")
	private String postCode;

	@ApiModelProperty("行业分类")
	private Integer industryId;

	@ApiModelProperty("事业单位性质")
	private String careerNature;

	@ApiModelProperty("设立批准时间")
	private Date buildTime;

	@ApiModelProperty("设立批准机关")
	private String buildOrgName;

	@ApiModelProperty("法定代表人")
	private String legalPerson;

	@ApiModelProperty("统一社会信用代码")
	private String creditCode;

	@ApiModelProperty("是否简要信息")
	private String isSimple;

	@ApiModelProperty("职能职责")
	private String dutyRemarks;

	@ApiModelProperty("内设机构")
	private String depRemarks;

	@ApiModelProperty("编制信息")
	private String empRemarks;

	@ApiModelProperty("摘要")
	private String description;

	@ApiModelProperty("备注")
	private String remarks;

	@ApiModelProperty("是否挂标")
	private String isLable;

	@ApiModelProperty("underOrgCategory")
	private Integer underOrgCategory;

	@ApiModelProperty("是否挂标")
	private String isLabel;

	@ApiModelProperty("basicData")
	private String basicData;

	@ApiModelProperty("宗旨和服务范围")
	private String serveArea;

	public void setId(Integer id){
		this.id=id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="mySeqGenerator")
	@SequenceGenerator(name = "mySeqGenerator", sequenceName = "s_ofs_organization_base", allocationSize = 1)
	public Integer getId(){
		return id;
	}

	public void setOrgId(Integer orgId){
		this.orgId=orgId;
	}

	public Integer getOrgId(){
		return orgId;
	}

	public void setCode(String code){
		this.code=code;
	}

	public String getCode(){
		return code;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setOrgOtherName(String orgOtherName){
		this.orgOtherName=orgOtherName;
	}

	public String getOrgOtherName(){
		return orgOtherName;
	}

	public void setType(String type){
		this.type=type;
	}

	public String getType(){
		return type;
	}

	public void setOwnSys(String ownSys){
		this.ownSys=ownSys;
	}

	public String getOwnSys(){
		return ownSys;
	}

	public void setMechStandard(String mechStandard){
		this.mechStandard=mechStandard;
	}

	public String getMechStandard(){
		return mechStandard;
	}

	public void setIsComp(String isComp){
		this.isComp=isComp;
	}

	public String getIsComp(){
		return isComp;
	}

	public void setCompId(Integer compId){
		this.compId=compId;
	}

	public Integer getCompId(){
		return compId;
	}

	public void setIsManu(String isManu){
		this.isManu=isManu;
	}

	public String getIsManu(){
		return isManu;
	}

	public void setManuType(String manuType){
		this.manuType=manuType;
	}

	public String getManuType(){
		return manuType;
	}

	public void setOrgCode(String orgCode){
		this.orgCode=orgCode;
	}

	public String getOrgCode(){
		return orgCode;
	}

	public void setInsClassify(String insClassify){
		this.insClassify=insClassify;
	}

	public String getInsClassify(){
		return insClassify;
	}

	public void setFundSource(String fundSource){
		this.fundSource=fundSource;
	}

	public String getFundSource(){
		return fundSource;
	}

	public void setInsLegalNum(String insLegalNum){
		this.insLegalNum=insLegalNum;
	}

	public String getInsLegalNum(){
		return insLegalNum;
	}

	public void setAddress(String address){
		this.address=address;
	}

	public String getAddress(){
		return address;
	}

	public void setPhoneNum(String phoneNum){
		this.phoneNum=phoneNum;
	}

	public String getPhoneNum(){
		return phoneNum;
	}

	public void setAreaId(String areaId){
		this.areaId=areaId;
	}

	public String getAreaId(){
		return areaId;
	}

	public void setSimpleSname(String simpleSname){
		this.simpleSname=simpleSname;
	}

	public String getSimpleSname(){
		return simpleSname;
	}

	public void setSimpleUname(String simpleUname){
		this.simpleUname=simpleUname;
	}

	public String getSimpleUname(){
		return simpleUname;
	}

	public void setIsSecret(String isSecret){
		this.isSecret=isSecret;
	}

	public String getIsSecret(){
		return isSecret;
	}

	public void setOrgNatrue(String orgNatrue){
		this.orgNatrue=orgNatrue;
	}

	public String getOrgNatrue(){
		return orgNatrue;
	}

	public void setOrgCategory(String orgCategory){
		this.orgCategory=orgCategory;
	}

	public String getOrgCategory(){
		return orgCategory;
	}

	public void setPostCode(String postCode){
		this.postCode=postCode;
	}

	public String getPostCode(){
		return postCode;
	}

	public void setIndustryId(Integer industryId){
		this.industryId=industryId;
	}

	public Integer getIndustryId(){
		return industryId;
	}

	public void setCareerNature(String careerNature){
		this.careerNature=careerNature;
	}

	public String getCareerNature(){
		return careerNature;
	}

	public void setBuildTime(Date buildTime){
		this.buildTime=buildTime;
	}

	public Date getBuildTime(){
		return buildTime;
	}

	public void setBuildOrgName(String buildOrgName){
		this.buildOrgName=buildOrgName;
	}

	public String getBuildOrgName(){
		return buildOrgName;
	}

	public void setLegalPerson(String legalPerson){
		this.legalPerson=legalPerson;
	}

	public String getLegalPerson(){
		return legalPerson;
	}

	public void setCreditCode(String creditCode){
		this.creditCode=creditCode;
	}

	public String getCreditCode(){
		return creditCode;
	}

	public void setIsSimple(String isSimple){
		this.isSimple=isSimple;
	}

	public String getIsSimple(){
		return isSimple;
	}

	public void setDutyRemarks(String dutyRemarks){
		this.dutyRemarks=dutyRemarks;
	}

	public String getDutyRemarks(){
		return dutyRemarks;
	}

	public void setDepRemarks(String depRemarks){
		this.depRemarks=depRemarks;
	}

	public String getDepRemarks(){
		return depRemarks;
	}

	public void setEmpRemarks(String empRemarks){
		this.empRemarks=empRemarks;
	}

	public String getEmpRemarks(){
		return empRemarks;
	}

	public void setDescription(String description){
		this.description=description;
	}

	public String getDescription(){
		return description;
	}

	public void setRemarks(String remarks){
		this.remarks=remarks;
	}

	public String getRemarks(){
		return remarks;
	}

	public void setIsLable(String isLable){
		this.isLable=isLable;
	}

	public String getIsLable(){
		return isLable;
	}

	public void setUnderOrgCategory(Integer underOrgCategory){
		this.underOrgCategory=underOrgCategory;
	}

	public Integer getUnderOrgCategory(){
		return underOrgCategory;
	}

	public void setIsLabel(String isLabel){
		this.isLabel=isLabel;
	}

	public String getIsLabel(){
		return isLabel;
	}

	public void setBasicData(String basicData){
		this.basicData=basicData;
	}

	public String getBasicData(){
		return basicData;
	}

	public void setServeArea(String serveArea){
		this.serveArea=serveArea;
	}

	public String getServeArea(){
		return serveArea;
	}

}
