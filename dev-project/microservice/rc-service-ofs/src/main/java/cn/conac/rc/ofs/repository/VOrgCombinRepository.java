package cn.conac.rc.ofs.repository;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.VOrgCombinEntity;

/**
 * VOrgCombinRepository类
 *
 * @author repositoryCreator
 * @date 2017-01-16
 * @version 1.0
 */
@Repository
public interface VOrgCombinRepository extends GenericDao<VOrgCombinEntity, Integer> {
	
	/**
	 * 通过部门的baseId来获得并入部门信息（机构ID及机构名称）
	 * @param baseId
	 * @return
	 */
	List<VOrgCombinEntity> findByBaseId(@Param("baseId") Integer baseId);
}
