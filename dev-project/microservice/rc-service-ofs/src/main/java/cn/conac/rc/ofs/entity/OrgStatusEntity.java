package cn.conac.rc.ofs.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * OrgStatusEntity类
 *
 * @author beanCreator
 * @date 2016-11-02
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_ORG_STATUS")
public class OrgStatusEntity implements Serializable {

	private static final long serialVersionUID = 1478053012110105024L;
	@Id
	@ApiModelProperty("ID")
	private Integer id;

	@ApiModelProperty("更新时间")
	private Date updateTime;

	@ApiModelProperty("信息完整度")
	private Integer orgIntegrity;

	@ApiModelProperty("变更类型")
	// 变更类型：10部门新建 20基本信息变更 30三定信息变更（31：职能职责  32：编制  33：内设机构）  40其他变更 50撤并
	private String updateType;

	@ApiModelProperty("最新状态")
	// 最新状态：00待提交 01待审核 02驳回 03通过
	private String status;

	@ApiModelProperty("录入人ID号")
	private Integer regUserId;

	@ApiModelProperty("变更强度")
	// 变更强度：0弱 1中 2强
	private String updateLevel;

	@ApiModelProperty("是否历史")
	// 是否历史：0当前 1历史
	private String isHistory;

	@ApiModelProperty("变更原因")
	private String updateReason;

	@ApiModelProperty("childType")
	private String childType;

	@ApiModelProperty("contentMask")
	private String contentMask;
	
	@ApiModelProperty("更新用户ID")
	private Integer  updateUserId;

	@ApiModelProperty("创建时间")
	private Date createTime;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime=updateTime;
	}

	public Date getUpdateTime(){
		return updateTime;
	}

	public void setOrgIntegrity(Integer orgIntegrity){
		this.orgIntegrity=orgIntegrity;
	}

	public Integer getOrgIntegrity(){
		return orgIntegrity;
	}

	public void setUpdateType(String updateType){
		this.updateType=updateType;
	}

	public String getUpdateType(){
		return updateType;
	}

	public void setStatus(String status){
		this.status=status;
	}

	public String getStatus(){
		return status;
	}

	public void setRegUserId(Integer regUserId){
		this.regUserId=regUserId;
	}

	public Integer getRegUserId(){
		return regUserId;
	}

	public void setUpdateLevel(String updateLevel){
		this.updateLevel=updateLevel;
	}

	public String getUpdateLevel(){
		return updateLevel;
	}

	public void setIsHistory(String isHistory){
		this.isHistory=isHistory;
	}

	public String getIsHistory(){
		return isHistory;
	}

	public void setUpdateReason(String updateReason){
		this.updateReason=updateReason;
	}

	public String getUpdateReason(){
		return updateReason;
	}

	public void setChildType(String childType){
		this.childType=childType;
	}

	public String getChildType(){
		return childType;
	}

	public void setContentMask(String contentMask){
		this.contentMask=contentMask;
	}

	public String getContentMask(){
		return contentMask;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
