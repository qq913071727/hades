package cn.conac.rc.ofs.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.VDomainInfoEntity;

@Repository
public interface VDomainInfoRepository extends GenericDao<VDomainInfoEntity, String> {

	/**
	 * 根据orgName查询该机构的对应的域名信息
	 * @param orgName
	 * @return VDomainInfoEntity的实体
	 */
	public VDomainInfoEntity findByOrgName(String orgName);
}
