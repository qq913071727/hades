package cn.conac.rc.ofs.repository;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.VHistOrgsInfo2Entity;

/**
 * VHistOrgsInfoRepository类
 *
 * @author repositoryCreator
 * @date 2017-02-10
 * @version 1.0
 */
@Repository
public interface VHistOrgsInfo2Repository extends GenericDao<VHistOrgsInfo2Entity, Integer> {
	
	/**
	 * 通过部门的OrgId来获得部门对应的历史沿革部门列表信息
	 * @param orgId
	 * @return
	 */
	public List<VHistOrgsInfo2Entity> findByOrgIdOrderByIdDesc(@Param("orgId") Integer orgId);
	
	/**
	 * 通过部门的areaId来获得部门对应的历史沿革部门列表信息
	 * @param areaId
	 * @return
	 */
	public List<VHistOrgsInfo2Entity> findByAreaIdOrderByOrgId(@Param("areaId") String areaId);
}
