package cn.conac.rc.ofs.rest;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.ofs.entity.ImportInstOrgEntity;
import cn.conac.rc.ofs.service.ImportInstOrgService;
import cn.conac.rc.ofs.vo.ImportInstOrgVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ImportInstOrgController类
 *
 * @author controllerCreator
 * @date 2016-12-23
 * @version 1.0
 */
@RestController
@RequestMapping(value="importInstOrg/")
public class ImportInstOrgController {

	@Autowired
	ImportInstOrgService service;

	@ApiOperation(value = "详情", httpMethod = "GET", response = ImportInstOrgEntity.class, notes = "根据name获取资源详情")
	@RequestMapping(value = "{name}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detailByName(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "name", required = true) @PathVariable("name") String name) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		ImportInstOrgEntity entity = service.getEntityByName(name);
		// 结果集设定
		if(entity == null || entity.getId() == null){
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg(ResultPojo.MSG_FAILURE);
		} else {
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setResult(entity);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		}
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "列表", httpMethod = "POST", response = ImportInstOrgEntity.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody ImportInstOrgVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<ImportInstOrgEntity> list = service.list(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
