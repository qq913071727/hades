package cn.conac.rc.ofs.service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.ofs.entity.ImportInstOrgEntity;
import cn.conac.rc.ofs.repository.ImportInstOrgRepository;
import cn.conac.rc.ofs.vo.ImportInstOrgVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

/**
 * ImportInstOrgService类
 *
 * @author serviceCreator
 * @date 2016-12-23
 * @version 1.0
 */
@Service
public class ImportInstOrgService extends GenericService<ImportInstOrgEntity, Integer> {

	@Autowired
	private ImportInstOrgRepository repository;

	/**
	 * 通过name获取对应详情
	 * @param name
	 * @return ImportInstOrgEntity
	 */
	public ImportInstOrgEntity getEntityByName(String name){
		return repository.findByName(name);
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<ImportInstOrgEntity> list(ImportInstOrgVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<ImportInstOrgEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<ImportInstOrgEntity> createCriteria(ImportInstOrgVo param) {
		Criteria<ImportInstOrgEntity> dc = new Criteria<ImportInstOrgEntity>();
		// TODO 具体条件赋值

		return dc;
	}

}
