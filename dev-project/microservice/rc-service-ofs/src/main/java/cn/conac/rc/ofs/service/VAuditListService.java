package cn.conac.rc.ofs.service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.framework.utils.DateUtils;
import cn.conac.rc.ofs.entity.VAuditListEntity;
import cn.conac.rc.ofs.repository.VAuditListRepository;
import cn.conac.rc.ofs.vo.VAuditListVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

/**
 * VAuditListService类
 *
 * @author serviceCreator
 * @date 2016-11-21
 * @version 1.0
 */
@Service
public class VAuditListService extends GenericService<VAuditListEntity, Integer> {

	@Autowired
	private VAuditListRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(VAuditListVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<VAuditListEntity> list(VAuditListVo vo) throws Exception {
		try {
			Sort sort;
			if(null ==vo.getOrderFlag()) {
				sort = new Sort(Direction.DESC, "updateTime");
			} else {
				if(vo.getOrderFlag().intValue() == 1){
					sort = new Sort(Direction.ASC, "updateTime");
				} else {
					sort = new Sort(Direction.DESC, "updateTime");
				}
			}
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<VAuditListEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<VAuditListEntity> createCriteria(VAuditListVo param) {
		Criteria<VAuditListEntity> dc = new Criteria<>();
		// 提交时间Start  
		if (param.getUpdateTimeStart() != null) {
			dc.add(Restrictions.gte("updateTime",DateUtils.parseDate(DateUtils.formatDate(param.getUpdateTimeStart()) + " 00:00:00"), true));
		}
		// 提交时间End
		if (param.getUpdateTimeEnd() != null) {
			dc.add(Restrictions.lte("updateTime", DateUtils.parseDate(DateUtils.formatDate(param.getUpdateTimeEnd()) + " 23:59:59"), true));
		}
		// 部门名称
		if (StringUtils.isNotBlank(param.getName())) {
			dc.add(Restrictions.like("name", param.getName(), true));
		}
		
		// 部门类型
		if (StringUtils.isNotBlank(param.getType())) {
			dc.add(Restrictions.eq("type", param.getType(), true));
		}
		// 状态
		if (StringUtils.isNotBlank(param.getStatus())) {
			dc.add(Restrictions.eq("status", param.getStatus(), true));
		}
		// 变更类型
		if (StringUtils.isNotBlank(param.getUpdateType())) {
			dc.add(Restrictions.eq("updateType", param.getUpdateType(), true));
		}
		// 区域
		if (StringUtils.isNotBlank(param.getAreaId())) {
			dc.add(Restrictions.eq("areaId", param.getAreaId(), true));
		}
		
		// 部门ID
		if (null != param.getOrgId() ) {
			dc.add(Restrictions.eq("orgId", param.getOrgId(), true));
		}
		
		return dc;
	}

}
