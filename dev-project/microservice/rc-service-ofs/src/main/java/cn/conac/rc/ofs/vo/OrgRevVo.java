package cn.conac.rc.ofs.vo;

import java.io.Serializable;
import java.util.List;

import cn.conac.rc.ofs.entity.AppFilesEntity;
import cn.conac.rc.ofs.entity.ApprovalEntity;
import cn.conac.rc.ofs.entity.OrgCombinEntity;
import cn.conac.rc.ofs.entity.OrgStatusEntity;
import io.swagger.annotations.ApiModel;

@ApiModel
public class OrgRevVo implements Serializable {

	private static final long serialVersionUID = 2789283526747372139L;

	OrgStatusEntity orgStatusEntity;
	
	ApprovalEntity  approvalEntity;
	
	AppFilesEntity appFilesEntity;
	
	private List<OrgCombinEntity> orgCombinList;

	public OrgStatusEntity getOrgStatusEntity() {
		return orgStatusEntity;
	}

	public void setOrgStatusEntity(OrgStatusEntity orgStatusEntity) {
		this.orgStatusEntity = orgStatusEntity;
	}

	public ApprovalEntity getApprovalEntity() {
		return approvalEntity;
	}

	public void setApprovalEntity(ApprovalEntity approvalEntity) {
		this.approvalEntity = approvalEntity;
	}

	public List<OrgCombinEntity> getOrgCombinList() {
		return orgCombinList;
	}

	public void setOrgCombinList(List<OrgCombinEntity> orgCombinList) {
		this.orgCombinList = orgCombinList;
	}

	public AppFilesEntity getAppFilesEntity() {
		return appFilesEntity;
	}

	public void setAppFilesEntity(AppFilesEntity appFilesEntity) {
		this.appFilesEntity = appFilesEntity;
	}
	
}
