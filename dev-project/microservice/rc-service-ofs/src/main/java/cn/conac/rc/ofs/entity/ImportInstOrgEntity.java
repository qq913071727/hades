package cn.conac.rc.ofs.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * ImportInstOrgEntity类
 *
 * @author beanCreator
 * @date 2016-12-23
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_IMPORT_INST_ORG")
public class ImportInstOrgEntity implements Serializable {

	private static final long serialVersionUID = 148245989180293316L;
	@Id
	@ApiModelProperty("ID")
	private Integer id;

	@ApiModelProperty("name")
	private String name;

	@ApiModelProperty("orgOtherName")
	private String orgOtherName;

	@ApiModelProperty("mechStandard")
	private String mechStandard;

	@ApiModelProperty("orgCode")
	private String orgCode;

	@ApiModelProperty("fundSource")
	private String fundSource;

	@ApiModelProperty("insLegalNum")
	private String insLegalNum;

	@ApiModelProperty("address")
	private String address;

	@ApiModelProperty("simpleSname")
	private String simpleSname;

	@ApiModelProperty("simpleUname")
	private String simpleUname;

	@ApiModelProperty("postCode")
	private String postCode;

	@ApiModelProperty("buildTime")
	private String buildTime;

	@ApiModelProperty("buildOrgName")
	private String buildOrgName;

	@ApiModelProperty("legalPerson")
	private String legalPerson;

	@ApiModelProperty("creditCode")
	private String creditCode;

	@ApiModelProperty("serveArea")
	private String serveArea;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setOrgOtherName(String orgOtherName){
		this.orgOtherName=orgOtherName;
	}

	public String getOrgOtherName(){
		return orgOtherName;
	}

	public void setMechStandard(String mechStandard){
		this.mechStandard=mechStandard;
	}

	public String getMechStandard(){
		return mechStandard;
	}

	public void setOrgCode(String orgCode){
		this.orgCode=orgCode;
	}

	public String getOrgCode(){
		return orgCode;
	}

	public void setFundSource(String fundSource){
		this.fundSource=fundSource;
	}

	public String getFundSource(){
		return fundSource;
	}

	public void setInsLegalNum(String insLegalNum){
		this.insLegalNum=insLegalNum;
	}

	public String getInsLegalNum(){
		return insLegalNum;
	}

	public void setAddress(String address){
		this.address=address;
	}

	public String getAddress(){
		return address;
	}

	public void setSimpleSname(String simpleSname){
		this.simpleSname=simpleSname;
	}

	public String getSimpleSname(){
		return simpleSname;
	}

	public void setSimpleUname(String simpleUname){
		this.simpleUname=simpleUname;
	}

	public String getSimpleUname(){
		return simpleUname;
	}

	public void setPostCode(String postCode){
		this.postCode=postCode;
	}

	public String getPostCode(){
		return postCode;
	}

	public void setBuildTime(String buildTime){
		this.buildTime=buildTime;
	}

	public String getBuildTime(){
		return buildTime;
	}

	public void setBuildOrgName(String buildOrgName){
		this.buildOrgName=buildOrgName;
	}

	public String getBuildOrgName(){
		return buildOrgName;
	}

	public void setLegalPerson(String legalPerson){
		this.legalPerson=legalPerson;
	}

	public String getLegalPerson(){
		return legalPerson;
	}

	public void setCreditCode(String creditCode){
		this.creditCode=creditCode;
	}

	public String getCreditCode(){
		return creditCode;
	}

	public void setServeArea(String serveArea){
		this.serveArea=serveArea;
	}

	public String getServeArea(){
		return serveArea;
	}

}
