package cn.conac.rc.ofs.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.DepartmentEntity2;

@Repository
public interface DepartmentRepository2 extends GenericDao<DepartmentEntity2, Integer> {

	/**
	 * 根据baseId查询该机构的所有内设机构信息(并按照内设顺序号升序排序)
	 * @param baseId
	 * @return DutyEntity的List
	 */
	public List<DepartmentEntity2> findByBaseId(Integer baseId);
	
}
