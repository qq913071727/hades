package cn.conac.rc.ofs.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.OrgLibInfoEntity;

/**
 * OfsOrgLibInfoRepository类
 *
 * @author repositoryCreator
 * @date 2017-07-10
 * @version 1.0
 */
@Repository
public interface OrgLibInfoRepository extends GenericDao<OrgLibInfoEntity, Integer> {
	
}
