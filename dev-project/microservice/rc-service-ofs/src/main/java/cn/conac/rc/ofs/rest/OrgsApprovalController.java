package cn.conac.rc.ofs.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.ofs.entity.OrgsApprovalEntity;
import cn.conac.rc.ofs.service.OrgsApprovalService;
import cn.conac.rc.ofs.vo.OrgsApprovalVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="orgsApp/")
public class OrgsApprovalController {

	@Autowired
	OrgsApprovalService service;

	@ApiOperation(value = "个数", httpMethod = "POST", response = Long.class, notes = "根据条件获得资源数量")
	@RequestMapping(value = "count", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> count(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody OrgsApprovalVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		long cnt = service.count(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(cnt);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "列表", httpMethod = "POST", response = OrgsApprovalEntity.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody OrgsApprovalVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<OrgsApprovalEntity> list = service.list(vo);
		// 结果集设定
		if(list!=null&&list.getNumberOfElements()>0){
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setResult(list);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		} else {
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg("没有查到相关结果!");
		}
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
