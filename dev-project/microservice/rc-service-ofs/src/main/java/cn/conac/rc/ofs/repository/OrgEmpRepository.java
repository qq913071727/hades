package cn.conac.rc.ofs.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.OrgEmpEntity;

@Repository
public interface OrgEmpRepository extends GenericDao<OrgEmpEntity, Integer> {
}
