package cn.conac.rc.ofs.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.ofs.entity.OrgOthersEntity;
import cn.conac.rc.ofs.service.OrgOthersService;
import cn.conac.rc.ofs.vo.OrgOthersVo;
import cn.conac.rc.framework.vo.ResultPojo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="orgOthers/")
public class OrgOthersController {

	@Autowired
	OrgOthersService service;

	@ApiOperation(value = "其他事项及附则资源详情", httpMethod = "GET", response = OrgOthersEntity.class, notes = "根据id获取其他事项及附则资源详情")
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findDetailInfoById(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		OrgOthersEntity orgOthers = service.findById(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(orgOthers);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "个数", httpMethod = "POST", response = Long.class, notes = "根据条件获得资源数量")
	@RequestMapping(value = "count", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> count(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody OrgOthersVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		long cnt = service.count(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(cnt);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "列表", httpMethod = "POST", response = OrgOthersEntity.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody OrgOthersVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<OrgOthersEntity> list = service.list(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "新增或者更新其他信息", httpMethod = "POST", response = OrgOthersEntity.class, notes = "保存资源到数据库")
	@RequestMapping(value = "{submitType}", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> saveOrUpdate(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "提交类别", required = true) @PathVariable("submitType") String submitType,
			@ApiParam(value = "保存对象", required = true) @RequestBody OrgOthersEntity entity) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();

		// 数据格式校验
		if(!"0".equals(submitType)){
			String valMsg = service.validate(entity);
			if (valMsg != null) {
				result.setCode(ResultPojo.CODE_FORMAT_ERR);
				result.setMsg(valMsg);
				return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
			}
		}

		service.save(entity);

		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "物理删除", httpMethod = "POST", response = OrgOthersEntity.class, notes = "根据id物理删除资源")
	@RequestMapping(value = "{id}/delete", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> delete(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		service.delete(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(id);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
