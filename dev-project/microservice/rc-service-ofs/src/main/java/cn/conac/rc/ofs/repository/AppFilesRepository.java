package cn.conac.rc.ofs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.AppFilesEntity;

@Repository
public interface AppFilesRepository extends GenericDao<AppFilesEntity, Integer> {
	
	/**
	 * 通过机构BaseID获取文件关联数据
	 * @return
	 */
	List<AppFilesEntity> findByBaseId(@Param("baseId") Integer baseId);
	
	/**
	 * 根据baseID删除内设机构信息
	 * @param baseId
	 * @return void
	 */
	@Query(value = "delete  from ofs_app_files af where af.org_id= :baseId", nativeQuery = true)
	@Modifying
	void deleteByBaseId(@Param("baseId") Integer baseId);
	
}
