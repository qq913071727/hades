package cn.conac.rc.ofs.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * OrgCombinEntity类
 *
 * @author beanCreator
 * @date 2016-11-01
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_ORG_COMBIN")
public class OrgCombinEntity implements Serializable {

	private static final long serialVersionUID = 1477995462653141260L;
	@Id
	@ApiModelProperty("ID")
	private Integer id;

	@ApiModelProperty("机构ID号")
	@NotNull
	private Integer baseId;

	@ApiModelProperty("并入机构ID号")
	@NotNull
	private Integer orgId;

	public void setId(Integer id){
		this.id=id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="mySeqGenerator")
	@SequenceGenerator(name = "mySeqGenerator", sequenceName = "s_ofs_org_combin", allocationSize = 1)
	public Integer getId(){
		return id;
	}

	public void setBaseId(Integer baseId){
		this.baseId=baseId;
	}

	@Column(name="ORG_ID")
	public Integer getBaseId(){
		return baseId;
	}

	public void setOrgId(Integer orgId){
		this.orgId=orgId;
	}

	@Column(name="ORG_COMBIN_ID")
	public Integer getOrgId(){
		return orgId;
	}

}
