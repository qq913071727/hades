package cn.conac.rc.ofs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.framework.utils.BeanMapper;
import cn.conac.rc.ofs.entity.OrgEmpEntity;
import cn.conac.rc.ofs.repository.OrgEmpRepository;

@Service
public class OrgEmpService extends GenericService<OrgEmpEntity, Integer> {

	@Autowired
	private OrgEmpRepository repository;

	/**
	 * 将旧的编制信息复制一份，新建一份新的编制信息
	 * @param oldBaseId
	 * @param newBaseId
	 * @return 
	 */
	public OrgEmpEntity copyOrgEmpInfo(Integer oldBaseId, Integer newBaseId){
		// 通过OldBaseID获取旧的编制信息
		OrgEmpEntity oldOrgEmp = repository.findOne(oldBaseId);
		// 判断为空
		if(oldOrgEmp == null){
			return null;
		}
		// 复制旧编制信息到新的Bean中
		OrgEmpEntity newOrgEmp = new OrgEmpEntity();
		BeanMapper.copy(oldOrgEmp, newOrgEmp);
		newOrgEmp.setId(newBaseId);
		// 返回新复制的编制信息
		return repository.save(newOrgEmp);
	}

//	/**
//	 * 更新处理
//	 * @param id
//	 * @param entity
//	 * @return
//	 */
//	public OrgEmpEntity update(Integer id, OrgEmpEntity entity){
//		// 检索要更新的数据信息
//		OrgEmpEntity targetEntity = repository.findOne(id);
//		// 空判断
//		if(targetEntity == null){
//			return null;
//		}
//		// 复制表信息
//		BeanMapper.copy(entity,targetEntity,new String[]{"id"});
//		// 保存信息并返回
//		return repository.save(targetEntity);
//	}

}
