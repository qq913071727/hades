package cn.conac.rc.ofs.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.ofs.entity.VOrgPreSubEntity;

/**
 * VOrgPreSubRepository类
 *
 * @author repositoryCreator
 * @date 2016-12-14
 * @version 1.0
 */
@Repository
public interface VOrgPreSubRepository extends GenericDao<VOrgPreSubEntity, Integer> {
}
