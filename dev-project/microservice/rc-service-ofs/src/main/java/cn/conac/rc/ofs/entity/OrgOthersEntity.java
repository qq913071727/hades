package cn.conac.rc.ofs.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * OrgOthersEntity类
 *
 * @author beanCreator
 * @date 2016-11-01
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_ORG_OTHERS")
public class OrgOthersEntity implements Serializable {

	private static final long serialVersionUID = 1477995462748654683L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("其他事项")
	private String orgOtherRemarks;

	@ApiModelProperty("附则")
	private String orgPrevisions;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setOrgOtherRemarks(String orgOtherRemarks){
		this.orgOtherRemarks=orgOtherRemarks;
	}

	public String getOrgOtherRemarks(){
		return orgOtherRemarks;
	}

	public void setOrgPrevisions(String orgPrevisions){
		this.orgPrevisions=orgPrevisions;
	}

	public String getOrgPrevisions(){
		return orgPrevisions;
	}

}
