package cn.conac.rc.ofs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.ofs.entity.OrgOpinionsEntity;
import cn.conac.rc.ofs.repository.OrgOpinionsRepository;
import cn.conac.rc.ofs.vo.OrgOpinionsVo;
import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;

@Service
public class OrgOpinionsService extends GenericService<OrgOpinionsEntity, Integer> {

	@Autowired
	private OrgOpinionsRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(OrgOpinionsVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<OrgOpinionsEntity> list(OrgOpinionsVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<OrgOpinionsEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<OrgOpinionsEntity> createCriteria(OrgOpinionsVo param) {
		Criteria<OrgOpinionsEntity> dc = new Criteria<OrgOpinionsEntity>();
		// TODO 具体条件赋值

		return dc;
	}

}
