package cn.conac.rc.ofs.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.ofs.entity.VDomainInfoEntity;
import cn.conac.rc.ofs.service.VDomainInfoService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="domainInfo/") 
public class VDomainInfoController {

	@Autowired
	VDomainInfoService service;

	@ApiOperation(value = "域名相关信息", httpMethod = "GET", response = VDomainInfoEntity.class, notes = "根据机构名称获取域名相关信息")
	@RequestMapping(value = "{orgName}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "orgName", required = true) @PathVariable("orgName") String orgName) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		VDomainInfoEntity vDomainInfo = service.findByOrgName(orgName);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(vDomainInfo);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
