package cn.conac.rc.ofs.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import cn.conac.rc.framework.entity.DataEntity;
import io.swagger.annotations.ApiModel;

@ApiModel
@Entity
@Table(name="RC_FILE")
@Where(clause="DELETE_MARK = 0")
public class FileEntity extends DataEntity<FileEntity> implements Serializable  {
	
	private static final long serialVersionUID = -4880566342419881457L;
	
	/**
	 * fileName
	 */
	private String fileName;
	/**
	 * absolutePath
	 */
	private String absolutePath;
	/**
	 * relativePath
	 */
	private String relativePath;
	/**
	 * useCnt
	 */
	private Integer useCnt;
	/**
	 * type
	 */
	private String type;
	
	public void setFileName(String fileName){
		this.fileName=fileName;
	}
	@Column(name = "FILE_NAME")
	public String getFileName(){
		return fileName;
	}
	@Column(name = "ABSOLUTE_PATH")
	public String getAbsolutePath() {
		return absolutePath;
	}
	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}
	@Column(name = "RELATIVE_PATH")
	public String getRelativePath() {
		return relativePath;
	}
	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}
	public void setUseCnt(Integer useCnt){
		this.useCnt=useCnt;
	}
	@Column(name = "USE_CNT")
	public Integer getUseCnt(){
		return useCnt;
	}
	public void setType(String type){
		this.type=type;
	}
	@Column(name = "TYPE")
	public String getType(){
		return type;
	}
}

