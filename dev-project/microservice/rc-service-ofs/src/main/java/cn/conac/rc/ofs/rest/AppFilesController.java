package cn.conac.rc.ofs.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.utils.StringUtils;
import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.ofs.entity.AppFilesEntity;
import cn.conac.rc.ofs.service.AppFilesService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="appFiles/")
public class AppFilesController {

	@Autowired
	AppFilesService service;

	@ApiOperation(value = "列表", httpMethod = "POST", response = AppFilesEntity.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "list/{baseId}", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> listByBaseId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @PathVariable("baseId") Integer baseId) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<AppFilesEntity> list = service.listByBaseId(baseId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "新增或者更新资源到数据库", httpMethod = "POST", response = AppFilesEntity.class, notes = "保存资源到数据库")
	@RequestMapping(value = "{submitType}", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> save(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "提交类别", required = true) @PathVariable("submitType") String submitType,
			@ApiParam(value = "保存对象", required = true) @RequestBody AppFilesEntity entity) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		
		// 数据格式校验
		if(!"0".equals(submitType)){
			String valMsg = service.validate(entity);
			if (valMsg != null) {
				result.setCode(ResultPojo.CODE_FORMAT_ERR);
				result.setMsg(valMsg);
				return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
			}
			if(StringUtils.isEmpty(entity.getAppUrl())){
				result.setCode(ResultPojo.CODE_FORMAT_ERR);
				result.setMsg("存储文件URL的值不能为空。");
				return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
			}
		}
		
		List<AppFilesEntity> list = service.listByBaseId(entity.getBaseId());
		if(null != list  && list.size() > 0){
			entity.setId(list.get(0).getId());
		}
		// 再保存数据
		service.save(entity);

		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
