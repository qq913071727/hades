package cn.conac.rc.repository;

import cn.conac.rc.entity.UnderCategoryEntity;
import cn.conac.rc.framework.repository.GenericDao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UnderCategoryRepository extends GenericDao<UnderCategoryEntity, Integer> {
	
	/**
	 * 从数据库中获取下设机构类别的根名称
	 * @return 下设机构类别
	 */
	@Query("select a from UnderCategoryEntity a where a.pid = -1 and a.leaf = 0")
	public UnderCategoryEntity findUnderCategoryRootName();

	/**
	 * 从数据库中获取下设机构类别的二级菜单名称
	 * @param pid
	 * @return 下设机构类别
	 */
	public List<UnderCategoryEntity> findByPidOrderByIdAsc(@Param("pid") Integer pid);
}
