package cn.conac.rc.auth.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.auth.entity.RoleEntity;
import cn.conac.rc.framework.repository.GenericDao;


@Repository
public interface RoleRepository extends GenericDao<RoleEntity, Integer>{
	
	RoleEntity findByRoleId(@Param("id") Integer id);
	
	RoleEntity findByRoleName(@Param("roleName") String roleName);
	
	List<RoleEntity> findByRoleNameLike(@Param("roleName") String roleName);
	
	@Query(value = "select R.* from ICP_USER U,ICP_ROLE R,ICP_USER_ROLE UR where U.USER_ID=UR.USER_ID AND UR.ROLE_ID=R.ROLE_ID  AND U.USER_ID=:userId", nativeQuery = true)
	List<RoleEntity> findRolesByUserId(@Param("userId") String userId);
}


