package cn.conac.rc.vo;

import java.util.Date;

import javax.persistence.Transient;

import cn.conac.rc.entity.DicTreeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DicTreeVo类
 *
 * @author voCreator
 * @date 2016-12-08
 * @version 1.0
 */
@ApiModel
public class DicTreeVo extends DicTreeEntity {

	private static final long serialVersionUID = 1481186380649790114L;

	@Transient
	@ApiModelProperty("当前分页")
	private Integer page;

	@Transient
	@ApiModelProperty("每页个数")
	private Integer size;

	@Transient
	@ApiModelProperty("创建 开始时间")
	private Date createDateStart;

	@Transient
	@ApiModelProperty("创建 结束时间")
	private Date createDateEnd;

	@Transient
	@ApiModelProperty("更新 开始时间")
	private Date updateDateStart;

	@Transient
	@ApiModelProperty("更新 结束时间")
	private Date updateDateEnd;

	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}

	public void setCreateDateStart(Date createDateStart){
		this.createDateStart=createDateStart;
	}

	public Date getCreateDateStart(){
		return createDateStart;
	}

	public void setCreateDateEnd(Date createDateEnd){
		this.createDateEnd=createDateEnd;
	}

	public Date getCreateDateEnd(){
		return createDateEnd;
	}

	public void setUpdateDateStart(Date updateDateStart){
		this.updateDateStart=updateDateStart;
	}

	public Date getUpdateDateStart(){
		return updateDateStart;
	}

	public void setUpdateDateEnd(Date updateDateEnd){
		this.updateDateEnd=updateDateEnd;
	}

	public Date getUpdateDateEnd(){
		return updateDateEnd;
	}

}
