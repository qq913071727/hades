package cn.conac.rc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.entity.CareerIndustryDictEntity;
import cn.conac.rc.framework.repository.GenericDao;

@Repository
public interface CareerIndustryDictRepository extends GenericDao<CareerIndustryDictEntity, Integer> {
	
	/**
	 * 从数据库中获取行业分类的根名称
	 * @return 行业分类
	 */
	@Query("select a from CareerIndustryDictEntity a where a.pid = -1 and a.leaf = 0")
	public CareerIndustryDictEntity findCareerIndustryRootName();
	
	/**
	 * 从数据库中获取行业分类的二级菜单名称
	 * @param pid
	 * @return 行业分类
	 */
	public List<CareerIndustryDictEntity> findByPidOrderByOrderNumAsc(@Param("pid") Integer pid);
}
