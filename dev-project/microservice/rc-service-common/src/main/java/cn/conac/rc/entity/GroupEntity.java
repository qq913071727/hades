package cn.conac.rc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * GroupEntity类
 *
 * @author beanCreator
 * @date 2016-12-14
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="ICP_GROUP")
public class GroupEntity implements Serializable {

	private static final long serialVersionUID = 1481700078577855969L;
	@Id
	@ApiModelProperty("groupId")
	private Integer groupId;

	@ApiModelProperty("groupName")
	private String groupName;

	@ApiModelProperty("priority")
	private Integer priority;

	@ApiModelProperty("needCaptcha")
	private Integer needCaptcha;

	@ApiModelProperty("needCheck")
	private Integer needCheck;

	@ApiModelProperty("allowPerDay")
	private Integer allowPerDay;

	@ApiModelProperty("allowMaxFile")
	private Integer allowMaxFile;

	@ApiModelProperty("allowSuffix")
	private String allowSuffix;

	@ApiModelProperty("isRegDef")
	private Integer isRegDef;

	@ApiModelProperty("allowFileSize")
	private Integer allowFileSize;

	@ApiModelProperty("allowFileTotal")
	private Integer allowFileTotal;

	public void setGroupId(Integer groupId){
		this.groupId=groupId;
	}

	public Integer getGroupId(){
		return groupId;
	}

	public void setGroupName(String groupName){
		this.groupName=groupName;
	}

	public String getGroupName(){
		return groupName;
	}

	public void setPriority(Integer priority){
		this.priority=priority;
	}

	public Integer getPriority(){
		return priority;
	}

	public void setNeedCaptcha(Integer needCaptcha){
		this.needCaptcha=needCaptcha;
	}

	public Integer getNeedCaptcha(){
		return needCaptcha;
	}

	public void setNeedCheck(Integer needCheck){
		this.needCheck=needCheck;
	}

	public Integer getNeedCheck(){
		return needCheck;
	}

	public void setAllowPerDay(Integer allowPerDay){
		this.allowPerDay=allowPerDay;
	}

	public Integer getAllowPerDay(){
		return allowPerDay;
	}

	public void setAllowMaxFile(Integer allowMaxFile){
		this.allowMaxFile=allowMaxFile;
	}

	public Integer getAllowMaxFile(){
		return allowMaxFile;
	}

	public void setAllowSuffix(String allowSuffix){
		this.allowSuffix=allowSuffix;
	}

	public String getAllowSuffix(){
		return allowSuffix;
	}

	public void setIsRegDef(Integer isRegDef){
		this.isRegDef=isRegDef;
	}

	public Integer getIsRegDef(){
		return isRegDef;
	}

	public void setAllowFileSize(Integer allowFileSize){
		this.allowFileSize=allowFileSize;
	}

	public Integer getAllowFileSize(){
		return allowFileSize;
	}

	public void setAllowFileTotal(Integer allowFileTotal){
		this.allowFileTotal=allowFileTotal;
	}

	public Integer getAllowFileTotal(){
		return allowFileTotal;
	}

}
