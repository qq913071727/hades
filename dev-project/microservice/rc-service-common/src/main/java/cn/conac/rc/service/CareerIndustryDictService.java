package cn.conac.rc.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.entity.CareerIndustryDictEntity;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.repository.CareerIndustryDictRepository;

@Service
public class CareerIndustryDictService extends GenericService<CareerIndustryDictEntity, Integer> {

	@Autowired
	public CareerIndustryDictRepository careerIndustryRepository;

	/**
	 * 根据baseId查询该机构的所有内设机构信息(并按照内设顺序号升序排序)
	 * @param baseId
	 * @param mapTree
	 * @return 一级内设机构List
	 */
	public List<CareerIndustryDictEntity> findMapTree(Map<String,Object> mapTree) {
		mapTree.put("id", 1);
		mapTree.put("code", careerIndustryRepository.findCareerIndustryRootName().getCode());
		mapTree.put("name", careerIndustryRepository.findCareerIndustryRootName().getName());

		List<Map<String, Object>> childCareerIndustryList= new ArrayList<Map<String, Object>>();
		Map<String, Object> industryMap = null;
		List<CareerIndustryDictEntity> careerIndustryList  = careerIndustryRepository.findByPidOrderByOrderNumAsc(1);
		for(CareerIndustryDictEntity careerIndustryEntity: careerIndustryList){
			industryMap = new HashMap<String, Object>();
			industryMap.put("id",careerIndustryEntity.getId());
			industryMap.put("name",careerIndustryEntity.getName());
			industryMap.put("code", careerIndustryEntity.getCode());
			// 递归调用枝干节点
			if(new Integer(0).equals(careerIndustryEntity.getLeaf())){
				recursiveCategoryTree(careerIndustryEntity.getId(), industryMap);
			}
		    childCareerIndustryList.add(industryMap);
		}
		mapTree.put("children",childCareerIndustryList);
		return careerIndustryList;
	}
	
	/**
	 * 根据主键ID递归查询level2一下的内设机构信息
	 * @param id
	 * @param map
	 * @return 返回以该ID为父的内设机构List
	 */
	public  List<CareerIndustryDictEntity> recursiveCategoryTree(Integer id, Map<String,Object> map) {
		List<CareerIndustryDictEntity> careerIndustryList = careerIndustryRepository.findByPidOrderByOrderNumAsc(id);
	    List<Map<String, Object>> childIndustryList= new ArrayList<Map<String, Object>>();
	    Map<String, Object> industryMap = null;
	    for(CareerIndustryDictEntity industryEntity : careerIndustryList){
	    	industryMap = new HashMap<String, Object>();
			industryMap.put("id",industryEntity.getId());
			industryMap.put("name",industryEntity.getName());
			industryMap.put("code", industryEntity.getCode());
			// 递归调用枝干节点
			if(new Integer(0).equals(industryEntity.getLeaf())){
				recursiveCategoryTree(industryEntity.getId(),industryMap);
			}
	    	childIndustryList.add(industryMap);
	    }
	    map.put("children",childIndustryList);
	    return careerIndustryList;
	}
}
