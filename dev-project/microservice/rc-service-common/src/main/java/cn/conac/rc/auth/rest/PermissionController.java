package cn.conac.rc.auth.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.auth.entity.PermissionEntity;
import cn.conac.rc.auth.entity.UserEntity;
import cn.conac.rc.auth.service.PermissionService;
import cn.conac.rc.auth.vo.PermissionVo;
import cn.conac.rc.framework.vo.ResultPojo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="permission/")
public class PermissionController {
	
	@Autowired
	PermissionService permissionService;
	
	@ApiOperation(value = "权限列表", httpMethod = "POST", response = UserEntity.class, notes = "根据传入参数获取权限列表信息")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request,
			             HttpServletResponse response,
			             @ApiParam(value = "权限查询参数", required = true) @RequestBody PermissionVo permission) throws Exception{
		 Page<PermissionEntity> list = permissionService.list(permission);
	     ResultPojo result = new ResultPojo();
	     result.setCode(ResultPojo.CODE_SUCCESS);
	     result.setResult(list);
	     return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "添加权限", httpMethod = "POST", response = UserEntity.class, notes = "对权限信息进行添加操作")
	@RequestMapping(value = "persistence", method = RequestMethod.POST)
	public ResultPojo saveUser(HttpServletRequest request,
            HttpServletResponse response,
            @ApiParam(value = "权限信息", required = true) @RequestBody PermissionEntity permission) throws Exception{
		permissionService.save(permission);
		return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				permission);
	}
	
	@ApiOperation(value = "删除权限", httpMethod = "GET", response = UserEntity.class, notes = "对权限进行逻辑删除操作")
	@RequestMapping(value = "{id}/delete", method = RequestMethod.GET)
	public ResultPojo deleteUserLogic(HttpServletRequest request,
                                       HttpServletResponse response,
    @ApiParam(value = "权限id", required = true) @PathVariable("id") String id) throws Exception{
		permissionService.delete(id);
		return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				id);
    }
	
	@ApiOperation(value = "获取角色权限", httpMethod = "GET", response = UserEntity.class, notes = "获取角色权限信息")
	@RequestMapping(value = "/role/{id}/permissions", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> getPermissionsByRoleId(HttpServletRequest request,
			             HttpServletResponse response,
			             @ApiParam(value = "角色id", required = true) @PathVariable("id") String id) throws Exception{
		 List<PermissionEntity> permissions = permissionService.findPermissionsByRoleId(id);
	     ResultPojo result = new ResultPojo();
	     result.setCode(ResultPojo.CODE_SUCCESS);
	     result.setResult(permissions);
	     return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
