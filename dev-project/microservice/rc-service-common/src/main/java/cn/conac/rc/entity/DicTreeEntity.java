package cn.conac.rc.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DicTreeEntity类
 *
 * @author beanCreator
 * @date 2016-12-08
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_DIC_TREE")
public class DicTreeEntity implements Serializable {

	private static final long serialVersionUID = 1481186380531585538L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("pid")
	private Integer pid;

	@ApiModelProperty("name")
	private String name;

	@ApiModelProperty("OPEN")
	private String open;

	@ApiModelProperty("name")
	private String perm;

	@ApiModelProperty("leaf")
	private Integer leaf;

	@ApiModelProperty("code")
	private Integer code;

	@ApiModelProperty("orderNum")
	private Integer orderNum;

	@ApiModelProperty("type")
	private String type;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setPid(Integer pid){
		this.pid=pid;
	}

	public Integer getPid(){
		return pid;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setOpen(String open){
		this.open=open;
	}

	public String getOpen(){
		return open;
	}

	public void setPerm(String perm){
		this.perm=perm;
	}

	public String getPerm(){
		return perm;
	}

	public void setLeaf(Integer leaf){
		this.leaf=leaf;
	}

	public Integer getLeaf(){
		return leaf;
	}

	public void setCode(Integer code){
		this.code=code;
	}

	public Integer getCode(){
		return code;
	}

	public void setOrderNum(Integer orderNum){
		this.orderNum=orderNum;
	}

	public Integer getOrderNum(){
		return orderNum;
	}

	public void setType(String type){
		this.type=type;
	}

	public String getType(){
		return type;
	}

}
