package cn.conac.rc.service;

import cn.conac.rc.entity.GroupEntity;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * GroupService类
 *
 * @author serviceCreator
 * @date 2016-12-14
 * @version 1.0
 */
@Service
public class GroupService extends GenericService<GroupEntity, Integer> {

	@Autowired
	GroupRepository groupRepository;

	/**
	 * 通过GroupID获取Group信息
	 * @param groupId
	 * @return GroupEntity
	 */
	public GroupEntity fingByGroupId(Integer groupId){
		return groupRepository.findByGroupId(groupId);
	};

}
