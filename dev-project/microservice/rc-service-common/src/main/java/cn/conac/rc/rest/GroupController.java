package cn.conac.rc.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.entity.GroupEntity;
import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.service.GroupService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * GroupController类
 *
 * @author controllerCreator
 * @date 2016-12-14
 * @version 1.0
 */
@RestController
@RequestMapping(value="group/")
public class GroupController {

	@Autowired
	GroupService service;

	@ApiOperation(value = "详情", httpMethod = "GET", response = GroupEntity.class, notes = "根据groupId获取资源详情")
	@RequestMapping(value = "{groupId}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("groupId") Integer groupId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		GroupEntity entity = service.fingByGroupId(groupId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
