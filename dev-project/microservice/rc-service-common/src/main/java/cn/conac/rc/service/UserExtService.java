package cn.conac.rc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.conac.rc.entity.UserExtEntity;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.repository.UserExtRepository;

/**
 * UserExtService类
 *
 * @author serviceCreator
 * @date 2016-12-14
 * @version 1.0
 */
@Service
@Transactional
public class UserExtService extends GenericService<UserExtEntity, Integer> {

	@Autowired
	private UserExtRepository repository;

	/**
	 * 更新对应OrgId的用户信息中OrgId为空值
	 * @param userId
	 * @return int
	 */
	public int updateUserOrgIdToNull(Integer orgId){
		
		int ret =0;
		List<UserExtEntity> userExtEntityList = repository.findByOrgId(orgId);
		
		if(null != userExtEntityList) {
			ret = repository.updateUserOrgIdToNull(orgId);
		}
		return ret;
	}
	
	
	 /**
     * 根据区域ID找到该区域的编办用户信息
     * @param areaCode
     * @return List<UserExtEntity>
     */
	public  List<UserExtEntity> findBBUserInfoByAreaCode(String areaCode){
		 return repository.findBBUserInfoByAreaCode(areaCode);
	}


}
