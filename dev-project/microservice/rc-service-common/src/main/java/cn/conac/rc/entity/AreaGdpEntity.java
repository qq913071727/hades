package cn.conac.rc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel
@Entity
@Table(name = "OFS_AREA_GDP")
public class AreaGdpEntity implements Serializable {
	
	public AreaGdpEntity(){
	}
	private static final long serialVersionUID = 5792171343640428052L;
	
	@Id
	@ApiModelProperty("ID")
	private String id;
	
	@ApiModelProperty("数据字典展示名称")
	private String areaId;
	@ApiModelProperty("数据字典实际存储值")
	private String totalGdp;
	@ApiModelProperty("数据字典值的类型")
	private String perSpeedGdp;
	@ApiModelProperty("排序号")
	private Integer totalPeople;
	@ApiModelProperty("父")
	private String areaOfZone;
	
    @ApiModelProperty("更新者")
    protected String updateUser;
    
    @ApiModelProperty("更新日期")
    protected Date updateDate;
    
    @ApiModelProperty("备注")
    protected String remarks;

    @ApiModelProperty("创建者")
    protected String createUser;

    @ApiModelProperty("创建时间")
    protected Date createDate;//
    
    @ApiModelProperty("删除标志0：正常 1：删除")
    protected DeleteMark deleteMark;
    
    /**
	 * 是否可用枚举定义
	 */
	public enum DeleteMark{
		//可用
		AVAILABLE,
		//不可用
		UNAVAILABLE;
	}
	
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getTotalGdp() {
		return totalGdp;
	}
	public void setTotalGdp(String totalGdp) {
		this.totalGdp = totalGdp;
	}
	public String getPerSpeedGdp() {
		return perSpeedGdp;
	}
	public void setPerSpeedGdp(String perSpeedGdp) {
		this.perSpeedGdp = perSpeedGdp;
	}
	public Integer getTotalPeople() {
		return totalPeople;
	}
	public void setTotalPeople(Integer totalPeople) {
		this.totalPeople = totalPeople;
	}
	public String getAreaOfZone() {
		return areaOfZone;
	}
	public void setAreaOfZone(String areaOfZone) {
		this.areaOfZone = areaOfZone;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public DeleteMark getDeleteMark() {
		return deleteMark;
	}
	public void setDeleteMark(DeleteMark deleteMark) {
		this.deleteMark = deleteMark;
	}
	
}
