package cn.conac.rc.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.entity.RcConfEntity;
import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.service.RcConfService;
import io.swagger.annotations.ApiOperation;

/**
 * RcConfController类
 *
 * @author controllerCreator
 * @date 2017-07-21
 * @version 1.0
 */
@RestController
@RequestMapping(value="rcConf/")
public class RcConfController {

	@Autowired
	RcConfService service;

	@ApiOperation(value = "列表", httpMethod = "POST", response = RcConfEntity.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> findAll(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<RcConfEntity> list = service.list();
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
