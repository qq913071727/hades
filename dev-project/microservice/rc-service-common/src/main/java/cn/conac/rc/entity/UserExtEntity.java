package cn.conac.rc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * UserExtEntity类
 *
 * @author beanCreator
 * @date 2016-12-14
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="ICP_USER_EXT")
public class UserExtEntity implements Serializable {

	private static final long serialVersionUID = 1481701007977243149L;
	@Id
	@ApiModelProperty("userId")
	private Integer userId;

	@ApiModelProperty("realname")
	private String realname;

	@ApiModelProperty("gender")
	private Integer gender;

	@ApiModelProperty("birthday")
	private Date birthday;

	@ApiModelProperty("intro")
	private String intro;

	@ApiModelProperty("comefrom")
	private String comefrom;

	@ApiModelProperty("QQ")
	private String qq;

	@ApiModelProperty("MSN")
	private String msn;

	@ApiModelProperty("phone")
	private String phone;

	@ApiModelProperty("mobile")
	private String mobile;

	@ApiModelProperty("userImg")
	private String userImg;

	@ApiModelProperty("userSignature")
	private String userSignature;

	@ApiModelProperty("orgId")
	private Integer orgId;

	@ApiModelProperty("userType")
	private String userType;

	public void setUserId(Integer userId){
		this.userId=userId;
	}

	public Integer getUserId(){
		return userId;
	}

	public void setRealname(String realname){
		this.realname=realname;
	}

	public String getRealname(){
		return realname;
	}

	public void setGender(Integer gender){
		this.gender=gender;
	}

	public Integer getGender(){
		return gender;
	}

	public void setBirthday(Date birthday){
		this.birthday=birthday;
	}

	public Date getBirthday(){
		return birthday;
	}

	public void setIntro(String intro){
		this.intro=intro;
	}

	public String getIntro(){
		return intro;
	}

	public void setComefrom(String comefrom){
		this.comefrom=comefrom;
	}

	public String getComefrom(){
		return comefrom;
	}

	public void setQq(String qq){
		this.qq=qq;
	}

	public String getQq(){
		return qq;
	}

	public void setMsn(String msn){
		this.msn=msn;
	}

	public String getMsn(){
		return msn;
	}

	public void setPhone(String phone){
		this.phone=phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setMobile(String mobile){
		this.mobile=mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setUserImg(String userImg){
		this.userImg=userImg;
	}

	public String getUserImg(){
		return userImg;
	}

	public void setUserSignature(String userSignature){
		this.userSignature=userSignature;
	}

	public String getUserSignature(){
		return userSignature;
	}

	public void setOrgId(Integer orgId){
		this.orgId=orgId;
	}

	public Integer getOrgId(){
		return orgId;
	}

	public void setUserType(String userType){
		this.userType=userType;
	}

	public String getUserType(){
		return userType;
	}

}
