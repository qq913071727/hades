package cn.conac.rc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.entity.AreaEntity;
import cn.conac.rc.framework.repository.GenericDao;

@Repository
public interface AreaRepository extends GenericDao<AreaEntity, String> {
	
	/**
	 * findByPid
	 * @param pid
	 * @return
	 */
	List<AreaEntity> findByPid(String pid);
	
	/**
	 * 更新地区的试点类型字段（包含下级地区只是自己）（1：开通试点 0：未开通试点）
	 * @param parentId
	 * @return 更新数量
	 */
	@Query(value = "update icp_area ia set ia.type=1 where ia.code =:areaCode" , nativeQuery = true)
	@Modifying
	int updateTypeByAreaCode(@Param("areaCode") String areaCode);
	
	/**
	 * 更新地区的试点类型字段包含下级地区（1：开通试点 0：未开通试点）
	 * @param parentId
	 * @return 更新数量
	 */
	@Query(value = "update icp_area ia set ia.type=1 where ia.code like :areaCodeLike" , nativeQuery = true)
	@Modifying
	int updateTypeByAreaCodeLike(@Param("areaCodeLike") String areaCodeLike);
}
