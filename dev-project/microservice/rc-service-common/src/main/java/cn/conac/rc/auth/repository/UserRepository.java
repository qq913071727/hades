package cn.conac.rc.auth.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.auth.entity.UserEntity;
import cn.conac.rc.framework.repository.GenericDao;
@Repository
public interface UserRepository extends GenericDao<UserEntity, String> {

	UserEntity findById(@Param("id") String id);
	
	@Query(value = "select * from RC_USER where LOGIN_NAME=:loginName ", nativeQuery = true)
	UserEntity findByLoginName(@Param("loginName") String loginName);

	@Query(value = "select * from RC_USER where SI_CODE=:siCode and FULL_NAME=:fullName ", nativeQuery = true)
	List<UserEntity> findBySiUserCodeFullname(@Param("siCode") String siCode,@Param("fullName") String fullName);

	@Query(value = "select * from RC_USER where SI_CODE=:siCode", nativeQuery = true)
	List<UserEntity> findBySiUserCode(@Param("siCode") String siCode);
	List<UserEntity> findByLoginNameLike(@Param("loginName") String loginName);
		
}

