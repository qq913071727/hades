package cn.conac.rc.vo;

import javax.persistence.Transient;

import cn.conac.rc.entity.VAreaInfoEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * VAreaInfoVo类
 *
 * @author voCreator
 * @date 2017-07-14
 * @version 1.0
 */
@ApiModel
public class VAreaInfoVo extends VAreaInfoEntity {

	private static final long serialVersionUID = 1500010878727214270L;

	@Transient
	@ApiModelProperty("当前分页")
	private Integer page;

	@Transient
	@ApiModelProperty("每页个数")
	private Integer size;

	@Transient
	@ApiModelProperty("每页个数")
	private Integer noSelfFlag;
	
	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}

	public Integer getNoSelfFlag() {
		return noSelfFlag;
	}

	public void setNoSelfFlag(Integer noSelfFlag) {
		this.noSelfFlag = noSelfFlag;
	}
}
