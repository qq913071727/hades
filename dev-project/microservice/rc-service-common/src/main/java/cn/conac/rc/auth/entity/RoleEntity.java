package cn.conac.rc.auth.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * RoleEntity类
 *
 * @author beanCreator
 * @date 2016-11-02
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="ICP_ROLE")
public class RoleEntity implements Serializable {

	private static final long serialVersionUID = 1478071306928486578L;

	@Id
	@ApiModelProperty("roleId")
	private Integer roleId;

	@ApiModelProperty("siteId")
	private Integer siteId;

	@ApiModelProperty("????")
	private String roleName;

	@ApiModelProperty("????")
	private Integer priority;

	@ApiModelProperty("??????")
	private String isSuper;

	@ApiModelProperty("roleType")
	private String roleType;

	public void setRoleId(Integer roleId){
		this.roleId=roleId;
	}

	public Integer getRoleId(){
		return roleId;
	}

	public void setSiteId(Integer siteId){
		this.siteId=siteId;
	}

	public Integer getSiteId(){
		return siteId;
	}

	public void setRoleName(String roleName){
		this.roleName=roleName;
	}

	public String getRoleName(){
		return roleName;
	}

	public void setPriority(Integer priority){
		this.priority=priority;
	}

	public Integer getPriority(){
		return priority;
	}

	public void setIsSuper(String isSuper){
		this.isSuper=isSuper;
	}

	public String getIsSuper(){
		return isSuper;
	}

	public void setRoleType(String roleType){
		this.roleType=roleType;
	}

	public String getRoleType(){
		return roleType;
	}

}
