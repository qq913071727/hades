package cn.conac.rc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.entity.RcConfEntity;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.repository.RcConfRepository;

/**
 * RcConfService类
 *
 * @author serviceCreator
 * @date 2017-07-21
 * @version 1.0
 */
@Service
public class RcConfService extends GenericService<RcConfEntity, Integer> {

	@Autowired
	private RcConfRepository repository;
	
	/**
	 * 获取所有的配置信息列表
	 * @param vo
	 * @return List<RcConfEntity>
	 * @throws Exception
	 */
	public List<RcConfEntity> list() throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");
			return repository.findAll(sort);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
