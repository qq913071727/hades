package cn.conac.rc.auth.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.auth.entity.PermissionEntity;
import cn.conac.rc.framework.repository.GenericDao;


@Repository
public interface PermissionRepository extends GenericDao<PermissionEntity, String>{
	
	PermissionEntity findById(@Param("id") String id);
	
	PermissionEntity findByPermissionName(@Param("permissionName") String permissionName);
	
	List<PermissionEntity> findByPermissionNameLike(@Param("permissionName") String permissionName);
	
	@Query(value = "select R.* from RC_PERMISSION P,RC_ROLES R,RC_ROLE_PERMISSION RP where P.ID=RP.PERMISSION_ID AND RP.ROLE_ID=R.ID AND R.ID=:id", nativeQuery = true)
	List<PermissionEntity> findPermissionsByRoleId(@Param("id") String id);
}


