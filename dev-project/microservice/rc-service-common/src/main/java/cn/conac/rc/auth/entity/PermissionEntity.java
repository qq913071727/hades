package cn.conac.rc.auth.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cn.conac.rc.framework.entity.DataEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@Entity
@Table(name = "RC_PERMISSION")
public class PermissionEntity extends DataEntity<PermissionEntity> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty("权限名称")
	@Column(nullable = true,length = 300)
	private String permissionName;
	
	@ApiModelProperty("资源url")
	@Column(nullable = true,length = 500)
	private String url;
	
	@ApiModelProperty("权限码如role:create")
	@Column(nullable = true,length = 100)
	private String permission;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JsonIgnore
	@JoinColumn(name="PARENT_ID", nullable=false, updatable=false)
	private PermissionEntity parentPermission;
	
	public String getPermissionName() {
		return permissionName;
	}

	public String getUrl() {
		return url;
	}

	public String getPermission() {
		return permission;
	}

	public PermissionEntity getParentPermission() {
		return parentPermission;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public void setParentPermission(PermissionEntity parentPermission) {
		this.parentPermission = parentPermission;
	}
}
