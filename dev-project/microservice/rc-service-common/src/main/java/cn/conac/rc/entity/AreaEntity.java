package cn.conac.rc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AreaEntity类
 *
 * @author beanCreator
 * @date 2016-11-02
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="ICP_AREA")
public class AreaEntity implements Serializable {

	private static final long serialVersionUID = 1478071306579636911L;

	@Id
	@NotBlank
	@ApiModelProperty("ID")
	private String id;

	@NotBlank
	@ApiModelProperty("pid")
	private String pid;

	@NotBlank
	@ApiModelProperty("name")
	private String name;

	@NotBlank
	@ApiModelProperty("code")
	private String code;

	@NotNull
	@ApiModelProperty("weight")
	private Integer weight;

	@NotNull
	@ApiModelProperty("lvl")
	private Integer lvl;

	@NotNull
	@ApiModelProperty("leaf")
	private Integer leaf;

	@ApiModelProperty("行政区划代码")
	private String adminCode;

	@NotBlank
	@ApiModelProperty("1：试点 0：非试点")
	private String type;
	
    @Transient
    @ApiModelProperty("下级地区")
    private List<AreaEntity> child;
    
    @NotNull
	@ApiModelProperty("1: 有公开办法 0：无公开办法")
	private Integer isPublic;

	public void setId(String id){
		this.id=id;
	}

	public String getId(){
		return id;
	}

	public void setPid(String pid){
		this.pid=pid;
	}

	public String getPid(){
		return pid;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setCode(String code){
		this.code=code;
	}

	public String getCode(){
		return code;
	}

	public void setWeight(Integer weight){
		this.weight=weight;
	}

	public Integer getWeight(){
		return weight;
	}

	public void setLvl(Integer lvl){
		this.lvl=lvl;
	}

	public Integer getLvl(){
		return lvl;
	}

	public void setLeaf(Integer leaf){
		this.leaf=leaf;
	}

	public Integer getLeaf(){
		return leaf;
	}

	public void setAdminCode(String adminCode){
		this.adminCode=adminCode;
	}

	public String getAdminCode(){
		return adminCode;
	}

    public List<AreaEntity> getChild() {
        return child;
    }

    public void setChild(List<AreaEntity> child) {
        this.child = child;
    }

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public Integer getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Integer isPublic) {
		this.isPublic = isPublic;
	}
}
