package cn.conac.rc.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * VAreaInfoEntity类
 *
 * @author beanCreator
 * @date 2017-07-14
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="ICP_V_AREA_INFO")
public class VAreaInfoEntity implements Serializable {

	private static final long serialVersionUID = 1500012604423561961L;

	@Id
	@ApiModelProperty("id")
	private String id;

	@ApiModelProperty("name")
	private String name;

	@ApiModelProperty("code")
	private String code;

	@ApiModelProperty("weight")
	private Integer weight;

	@ApiModelProperty("lvl")
	private Integer lvl;

	@ApiModelProperty("leaf")
	private Integer leaf;

	@ApiModelProperty("adminCode")
	private String adminCode;

	@ApiModelProperty("type")
	private Integer type;

	@ApiModelProperty("pid")
	private String pid;
	
	@ApiModelProperty("1: 有公开办法 0：无公开办法")
	private Integer isPublic;

	@ApiModelProperty("areaParentName")
	private String areaParentName;

	public void setId(String id){
		this.id=id;
	}

	public String getId(){
		return id;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setCode(String code){
		this.code=code;
	}

	public String getCode(){
		return code;
	}

	public void setWeight(Integer weight){
		this.weight=weight;
	}

	public Integer getWeight(){
		return weight;
	}

	public void setLvl(Integer lvl){
		this.lvl=lvl;
	}

	public Integer getLvl(){
		return lvl;
	}

	public void setLeaf(Integer leaf){
		this.leaf=leaf;
	}

	public Integer getLeaf(){
		return leaf;
	}

	public void setAdminCode(String adminCode){
		this.adminCode=adminCode;
	}

	public String getAdminCode(){
		return adminCode;
	}

	public void setType(Integer type){
		this.type=type;
	}

	public Integer getType(){
		return type;
	}

	public void setPid(String pid){
		this.pid=pid;
	}

	public String getPid(){
		return pid;
	}

	public Integer getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Integer isPublic) {
		this.isPublic = isPublic;
	}

	public void setAreaParentName(String areaParentName){
		this.areaParentName=areaParentName;
	}

	public String getAreaParentName(){
		return areaParentName;
	}

}
