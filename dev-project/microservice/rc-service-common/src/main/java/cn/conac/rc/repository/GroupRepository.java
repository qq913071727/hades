package cn.conac.rc.repository;

import cn.conac.rc.entity.GroupEntity;
import cn.conac.rc.framework.repository.GenericDao;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * GroupRepository类
 *
 * @author repositoryCreator
 * @date 2016-12-14
 * @version 1.0
 */
@Repository
public interface GroupRepository extends GenericDao<GroupEntity, Integer> {

    /**
     * 通过GroupID获取Group信息
     * @param groupId
     * @return GroupEntity
     */
    GroupEntity findByGroupId(@Param("groupId") Integer groupId);
}
