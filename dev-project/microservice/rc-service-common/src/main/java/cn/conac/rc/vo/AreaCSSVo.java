package cn.conac.rc.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;

@ApiModel
public class AreaCSSVo implements Serializable  {

	private static final long serialVersionUID = -4419747760699351026L;

	private String id;
	private String text;
	private String classes;
	private Boolean hasChildren;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getClasses() {
		return classes;
	}
	public void setClasses(String classes) {
		this.classes = classes;
	}
	public Boolean getHasChildren() {
		return hasChildren;
	}
	public void setHasChildren(Boolean hasChildren) {
		this.hasChildren = hasChildren;
	}

}
