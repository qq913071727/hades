package cn.conac.rc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.entity.DicTreeEntity;
import cn.conac.rc.framework.repository.GenericDao;

/**
 * DicTreeRepository类
 *
 * @author repositoryCreator
 * @date 2016-12-08
 * @version 1.0
 */
@Repository
public interface DicTreeRepository extends GenericDao<DicTreeEntity, Integer> {

	/**
	 * 从数据库中获取行业分类的根名称
	 * @return 行业分类
	 */
	@Query("select a from DicTreeEntity a where a.pid = -1 and a.leaf = 0")
	public DicTreeEntity findDicTreeRootName();

	/**
	 * 从数据库中获取行业分类的二级菜单名称
	 * @param pid
	 * @return 行业分类
	 */
	public List<DicTreeEntity> findByPidOrderByOrderNumAsc(@Param("pid") Integer pid);
}
