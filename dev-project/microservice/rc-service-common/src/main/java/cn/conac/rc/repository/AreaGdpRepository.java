package cn.conac.rc.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.entity.AreaGdpEntity;
import cn.conac.rc.framework.repository.GenericDao;
@Repository
public interface AreaGdpRepository extends GenericDao<AreaGdpEntity, String> {

}
