package cn.conac.rc.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.entity.Dictionary;
import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.service.DictionaryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
public class DictionaryController {
	@Autowired
	DictionaryService dicService;

	// 新增数据字典
	@ApiOperation(value = "保存字典的value", httpMethod = "POST", response = Dictionary.class, notes = "保存字典的value")
	@RequestMapping(value = "/comm/saveDic", 	method = RequestMethod.POST)
	public ResultPojo saveDic(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "字典的key", required = false) @RequestParam(value="key", required=false) String key,
			@ApiParam(value = "字典的value", required = false) @RequestParam(value="value", required=false) String value,
			@ApiParam(value = "value的展示名称", required = false) @RequestParam(value="name", required=false) String name,
			@ApiParam(value = "value的排序号", required = false) @RequestParam(value="sort", required=false) Integer sort,
			@ApiParam(value = "value的父value的id", required = false) @RequestParam(value="parentId", required=false) String parentId) {
		Dictionary dic = new Dictionary();
//		dic.setId(UUID.randomUUID().toString());
		dic.setType(key);
		dic.setValue(value);
		dic.setName(name);
		dic.setSort(sort);
//		if (StringUtils.isNotEmpty(parentId)) {
//			dic.setParentId(parentId);
//			RCDictionaryValue parentDicValue = dicValueService.findById(parentId);
//			if (null != parentDicValue) {
//				dic.setParentIds(parentDicValue.getParentIds() + "," + parentId);
//			}
//		}
		dicService.save(dic);
		ResultPojo result = new ResultPojo();
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(dic);
//		result.setMsg(dic.getId());
		return result;
	}

	// 更新数据字典
	@ApiOperation(value = "保存字典的value", httpMethod = "POST", response = Dictionary.class, notes = "更新字典的value")
	@RequestMapping(value = "/comm/updateDic", method = RequestMethod.POST)
	public ResultPojo updateDic(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "字典的id", required = false) @RequestParam(value="id", required=false) Integer id,
			@ApiParam(value = "字典的key", required = false) @RequestParam(value="key", required=false) String key,
			@ApiParam(value = "字典的value", required = false) @RequestParam(value="value", required=false) String value,
			@ApiParam(value = "value的展示名称", required = false) @RequestParam(value="name", required=false) String name,
			@ApiParam(value = "value的排序号", required = false) @RequestParam(value="sort", required=false) Integer sort,
			@ApiParam(value = "value的父value的id", required = false) @RequestParam(value="parentId", required=false) String parentId) {
		Dictionary dic = new Dictionary();
		dic.setId(id);
		dic.setType(key);
		dic.setValue(value);
		dic.setName(name);
		dic.setSort(sort);
//		if (StringUtils.isNotEmpty(parentId)) {
//			dic.setParentId(parentId);
//			RCDictionaryValue parentDicValue = dicValueService.findById(parentId);
//			if (null != parentDicValue) {
//				dic.setParentIds(parentDicValue.getParentIds() + "," + parentId);
//			}
//		}
		dicService.save(dic);
		ResultPojo result = new ResultPojo();
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(dic);
//		result.setMsg(dic.getId());
		return result;
	}

	// 删除数据字典
	@ApiOperation(value = "根据字典key的id删除数据字典", httpMethod = "POST", response = String.class, notes = "根据字典key的id删除数据字典")
	@RequestMapping(value = "/comm/deleteDic", method = RequestMethod.POST)
	public ResultPojo deleteDic(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "字典的id", required = false) @RequestParam(value="id", required=false) Integer id) {
		if (id!=null) {
			dicService.delete(id);
		}
		ResultPojo result = new ResultPojo();
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(id);
//		result.setMsg(id);
		return result;
	}

	// 删除数据字典
	@ApiOperation(value = "根据字典key的id删除数据字典", httpMethod = "POST", response = String.class, notes = "根据字典key的id删除数据字典")
	@RequestMapping(value = "/comm/deleteDicByKey", method = RequestMethod.POST)
	public ResultPojo deleteDicByKey(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "字典的key", required = false) @RequestParam(value="id", required=false) String key) {
		if (StringUtils.isNotEmpty(key)) {
			dicService.deleteByKey(key);
		}
		ResultPojo result = new ResultPojo();
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(key);
		result.setMsg(key);
		return result;
	}

	// 查询数据字典
	@ApiOperation(value = "根据字典key查询数据字典", httpMethod = "GET", response = Dictionary.class, notes = "根据字典key查询数据字典")
	@RequestMapping(value = "/comm/getDicByKey/{key}", method = RequestMethod.GET)
	public ResultPojo getDicByKey(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "字典key", required = false) @PathVariable("key") String key) {
		ResultPojo result = new ResultPojo();
		if (StringUtils.isEmpty(key)) {
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg(ResultPojo.CODE_LOGIC_ERR);
			return result;

		}
		List<Dictionary> dics = dicService.findByKey(key);
		List<Map<String, Object>> dataMaps = new ArrayList<Map<String, Object>>();
		for(Dictionary dic : dics){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", dic.getId());
			map.put("key", dic.getType());
			map.put("name", dic.getName());
			map.put("value", dic.getValue());
			map.put("sort", dic.getSort());
			dataMaps.add(map);
		}
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(dataMaps);
		result.setMsg(key);
		return result;
	}

	@ApiOperation(value = "根据字典的id查询数据字典", httpMethod = "GET", response = Dictionary.class, notes = "根据字典的id查询数据字典")
	@RequestMapping(value = "/comm/getDicById/{id}", method = RequestMethod.GET)
	public ResultPojo getDicById(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "字典的id", required = false) @PathVariable("id") Integer id) {
		Dictionary dic = dicService.findById(id);
		ResultPojo result = new ResultPojo();
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(dic);
//		result.setMsg(id);
		return result;
	}

	@ApiOperation(value = "获取所有数据字典", httpMethod = "GET", response = ArrayList.class, notes = "获取所有数据字典")
	@RequestMapping(value = "/comm/getAllDics", method = RequestMethod.GET)
	public ResultPojo getAllDics(HttpServletRequest request, HttpServletResponse response) {
		List<Dictionary> dicKeys = dicService.findAll();
		ResultPojo result = new ResultPojo();
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(dicKeys);
		return result;
	}
}
