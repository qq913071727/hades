package cn.conac.rc.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.entity.VAreaInfoEntity;
import cn.conac.rc.framework.repository.GenericDao;

/**
 * VAreaInfoRepository类
 *
 * @author repositoryCreator
 * @date 2017-07-14
 * @version 1.0
 */
@Repository
public interface VAreaInfoRepository extends GenericDao<VAreaInfoEntity, String> {
}
