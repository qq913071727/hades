package cn.conac.rc.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.entity.AreaEntity;
import cn.conac.rc.entity.AreaGdpEntity;
import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.service.AreaGdpService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
public class AreaGdpController {
	@Autowired
	AreaGdpService service;
	
	@ApiOperation(value = "查询地区的GDP和面积等信息", httpMethod = "GET", response = AreaEntity.class, notes = "查询地区的GDP和面积等信息")
    @RequestMapping(value = "/areaGdp/{areaId}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "areaId", required = true) @PathVariable("areaId") String areaId) {
		// 声明返回结果集
        ResultPojo result = new ResultPojo();
        // service调用
        AreaGdpEntity area = service.findAreaGdpByAreaId(areaId);
        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(area);
        result.setMsg(ResultPojo.MSG_SUCCESS);// TODO 根据具体需求配置返回消息
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
