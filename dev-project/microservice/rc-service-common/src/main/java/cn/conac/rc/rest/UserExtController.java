package cn.conac.rc.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.entity.UserExtEntity;
import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.service.UserExtService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * UserExtController类
 *
 * @author controllerCreator
 * @date 2016-12-14
 * @version 1.0
 */
@RestController
@RequestMapping(value="userExt/")
public class UserExtController {

	@Autowired
	UserExtService service;

	@ApiOperation(value = "更新", httpMethod = "POST", response = UserExtEntity.class, notes = "根据orgId更新用户扩展表")
	@RequestMapping(value = "{orgId}", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> update(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "orgId", required = true) @PathVariable("orgId") Integer orgId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		int rtlNum = service.updateUserOrgIdToNull(orgId);
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		result.setResult(rtlNum);
	
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "根据区域code找到编办用户ID信息", httpMethod = "GET", response = UserExtEntity.class, notes = "根据区域code找到编办用户ID信息")
	@RequestMapping(value = "{areaCode}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findBBUserIdsByAreaCode(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "areaCode", required = true) @PathVariable("areaCode")  String  areaCode) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		List<Integer> userIdList = new ArrayList<Integer>();
		// service调用
		List<UserExtEntity>  userExtEntityList = service.findBBUserInfoByAreaCode(areaCode);
		for(UserExtEntity  userExtEntity: userExtEntityList) {
			userIdList.add(userExtEntity.getUserId());
		}
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		result.setResult(userIdList);
	
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
