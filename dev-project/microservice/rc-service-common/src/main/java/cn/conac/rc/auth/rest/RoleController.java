package cn.conac.rc.auth.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.auth.entity.RoleEntity;
import cn.conac.rc.auth.entity.UserEntity;
import cn.conac.rc.auth.service.RoleService;
import cn.conac.rc.auth.vo.RoleVo;
import cn.conac.rc.framework.vo.ResultPojo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="role/")
public class RoleController {
	
	@Autowired
	RoleService roleService;
	
	@ApiOperation(value = "角色列表", httpMethod = "POST", response = UserEntity.class, notes = "根据传入参数获取角色列表信息")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request,
			             HttpServletResponse response,
			             @ApiParam(value = "角色查询参数", required = true) @RequestBody RoleVo role) throws Exception{
		 Page<RoleEntity> list = roleService.list(role);
	     ResultPojo result = new ResultPojo();
	     result.setCode(ResultPojo.CODE_SUCCESS);
	     result.setResult(list);
	        // result.setMsg();
	     return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "添加角色", httpMethod = "POST", response = UserEntity.class, notes = "对角色信息进行添加操作")
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResultPojo saveUser(HttpServletRequest request,
            HttpServletResponse response,
            @ApiParam(value = "角色信息", required = true) @RequestBody RoleEntity role) throws Exception{
		roleService.save(role);
		return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				role);
	}
	
	@ApiOperation(value = "删除角色", httpMethod = "GET", response = UserEntity.class, notes = "对角色进行逻辑删除操作")
	@RequestMapping(value = "{userId}/delete", method = RequestMethod.GET)
	public ResultPojo deleteUserLogic(HttpServletRequest request,
                                       HttpServletResponse response,
    @ApiParam(value = "角色id", required = true) @PathVariable("userId") Integer userId) throws Exception{
		roleService.delete(userId);
		return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				userId);
    }
	
}
