package cn.conac.rc.service;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.entity.AreaGdpEntity;
import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.repository.AreaGdpRepository;
@Service
public class AreaGdpService extends GenericService<AreaGdpEntity, String> {
	@Autowired
	AreaGdpRepository repository;
	
	public AreaGdpEntity findAreaGdpByAreaId(String areaId){
		if(StringUtils.isEmpty(areaId)){
			return null;
		}
		Criteria<AreaGdpEntity> dc = new Criteria<AreaGdpEntity>();
		if (StringUtils.isNotBlank(areaId)) {
			dc.add(Restrictions.eq("areaId", areaId, true));
		}
		return repository.findOne(dc);
	}
}
