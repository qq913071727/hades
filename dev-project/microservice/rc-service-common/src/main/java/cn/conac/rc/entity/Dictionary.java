package cn.conac.rc.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Dictionary类
 *
 * @author beanCreator
 * @date 2016-11-02
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="ICP_DICTIONARY")
public class Dictionary implements Serializable {

	private static final long serialVersionUID = 147807130685661154L;

	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("name")
	private String name;

	@ApiModelProperty("value")
	private String value;

	@ApiModelProperty("type")
	private String type;

	@ApiModelProperty("sort")
	private Integer sort;

	public void setId(Integer id){
		this.id=id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="mySeqGenerator")
	@SequenceGenerator(name = "mySeqGenerator", sequenceName = "s_icp_dictionary", allocationSize = 1)
	public Integer getId(){
		return id;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setValue(String value){
		this.value=value;
	}

	public String getValue(){
		return value;
	}

	public void setType(String type){
		this.type=type;
	}

	public String getType(){
		return type;
	}

	public void setSort(Integer sort){
		this.sort=sort;
	}

	public Integer getSort(){
		return sort;
	}

}
