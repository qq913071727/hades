package cn.conac.rc.repository;


import org.springframework.stereotype.Repository;

import cn.conac.rc.entity.RcConfEntity;
import cn.conac.rc.framework.repository.GenericDao;

/**
 * RcConfRepository类
 *
 * @author repositoryCreator
 * @date 2017-07-21
 * @version 1.0
 */
@Repository
public interface RcConfRepository extends GenericDao<RcConfEntity, Integer> {
	
}
