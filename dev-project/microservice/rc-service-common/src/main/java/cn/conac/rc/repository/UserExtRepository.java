package cn.conac.rc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.entity.UserExtEntity;
import cn.conac.rc.framework.repository.GenericDao;

/**
 * UserExtRepository类
 *
 * @author repositoryCreator
 * @date 2016-12-14
 * @version 1.0
 */
@Repository
public interface UserExtRepository extends GenericDao<UserExtEntity, Integer> {

    /**
     * 更新UserExt表的OrgId为空值
     * @param userId
     * @return int
     */
    @Query(value = "update ICP_USER_EXT u set u.org_Id = null where u.org_Id = :orgId", nativeQuery = true)
    @Modifying
    int updateUserOrgIdToNull(@Param("orgId") Integer orgId);
    
    List<UserExtEntity>  findByOrgId(@Param("orgId") Integer orgId);
    
    /**
     * 根据区域ID找到该区域的编办用户信息
     * @param areaCode
     * @return List<UserExtEntity>
     */
	@Query(value ="select user_ext.* from ICP_USER_AREA user_area,icp_user_ext user_ext, icp_user user_info where user_info.is_deleted=0 and user_info.user_id=user_ext.user_id and user_ext.user_id=user_area.user_id and user_ext.user_type='1' and user_area.area_code= :areaCode", nativeQuery = true)
	public List<UserExtEntity> findBBUserInfoByAreaCode(@Param("areaCode") String areaCode);
    
    
}
