package cn.conac.rc.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.entity.Dictionary;
import cn.conac.rc.framework.repository.GenericDao;

@Repository
public interface DictionaryRepository extends GenericDao<Dictionary, Integer>{
}
