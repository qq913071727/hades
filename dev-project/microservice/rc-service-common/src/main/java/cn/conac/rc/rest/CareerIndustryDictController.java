package cn.conac.rc.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.entity.CareerIndustryDictEntity;
import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.service.CareerIndustryDictService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="industryDict/")
public class CareerIndustryDictController {

	@Autowired
	CareerIndustryDictService service;

	@ApiOperation(value = "详情", httpMethod = "GET", response = CareerIndustryDictEntity.class, notes = "根据id获取资源详情")
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		CareerIndustryDictEntity careerIndustryDict = service.findById(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(careerIndustryDict);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "获取所有Industry数据", httpMethod = "GET", response = CareerIndustryDictEntity.class, notes = "获得所有数据的列表")
	@RequestMapping(value = "getAllIndustry", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<CareerIndustryDictEntity> list = service.findAll();
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "获取行业类别树结构", httpMethod = "GET", response = CareerIndustryDictEntity.class, notes = "获取行业类别树结构")
	@RequestMapping(value = "mapTree", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> getMapTree(HttpServletRequest request, HttpServletResponse response) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		Map<String,Object> mapTree = new HashMap<String,Object>();
		// service调用
		service.findMapTree(mapTree);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(mapTree);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
