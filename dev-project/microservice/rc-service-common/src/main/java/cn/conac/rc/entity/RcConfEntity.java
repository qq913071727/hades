package cn.conac.rc.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * RcConfEntity类
 *
 * @author beanCreator
 * @date 2017-07-21
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="RC_CONF")
public class RcConfEntity implements Serializable {

	private static final long serialVersionUID = 1500615504987253346L;

	@Id
	@ApiModelProperty("主键")
	private Integer id;

	@ApiModelProperty("参数名")
	private String paraName;

	@ApiModelProperty("参数值")
	private String paraValue;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setParaName(String paraName){
		this.paraName=paraName;
	}

	public String getParaName(){
		return paraName;
	}

	public void setParaValue(String paraValue){
		this.paraValue=paraValue;
	}

	public String getParaValue(){
		return paraValue;
	}

}
