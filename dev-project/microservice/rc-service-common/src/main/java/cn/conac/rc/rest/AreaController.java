package cn.conac.rc.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.entity.AreaEntity;
import cn.conac.rc.entity.VAreaInfoEntity;
import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.service.AreaService;
import cn.conac.rc.service.VAreaInfoService;
import cn.conac.rc.vo.AreaCSSVo;
import cn.conac.rc.vo.AreaListVo;
import cn.conac.rc.vo.AreaVo;
import cn.conac.rc.vo.VAreaInfoVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "area/")
public class AreaController {

    @Autowired
    AreaService service;
    
    @Autowired
    VAreaInfoService vAreaInfoService;

    @ApiOperation(value = "详情", httpMethod = "GET", response = AreaEntity.class, notes = "根据id获取资源详情")
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "id", required = true) @PathVariable("id") String id) {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();
        // service调用
        AreaEntity area = service.findById(id);
        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(area);
        result.setMsg(ResultPojo.MSG_SUCCESS);// TODO 根据具体需求配置返回消息
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }
    
    @ApiOperation(value = "包含上级部门名称的详情", httpMethod = "GET", response = AreaEntity.class, notes = "根据id获取包含上级部门名称的详情")
    @RequestMapping(value = "v/{id}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> vDetail(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "id", required = true) @PathVariable("id") String id) {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();
        // service调用
        VAreaInfoEntity area = vAreaInfoService.findById(id);
        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(area);
        result.setMsg(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "个数", httpMethod = "POST", response = AreaEntity.class, notes = "根据条件获得资源数量")
    @RequestMapping(value = "count", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> count(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody AreaVo vo) throws Exception {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();
        // service调用
        long cnt = service.count(vo);
        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(cnt);
        result.setMsg(ResultPojo.MSG_SUCCESS);// TODO 根据具体需求配置返回消息
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "列表", httpMethod = "POST", response = AreaEntity.class, notes = "根据条件获得资源列表")
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody AreaVo vo) throws Exception {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();
        // service调用
        Page<AreaEntity> list = service.list(vo);
        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(list);
        result.setMsg(ResultPojo.MSG_SUCCESS);// TODO 根据具体需求配置返回消息
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }
    
    @ApiOperation(value = "包含上级部门名称的列表", httpMethod = "POST", response = AreaEntity.class, notes = "根据条件获得包含上级部门名称的列表")
    @RequestMapping(value = "v/list", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> vlist(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody VAreaInfoVo vo) throws Exception {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();
        // service调用
        Page<VAreaInfoEntity> list = vAreaInfoService.list(vo);
        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(list);
        result.setMsg(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }
    
    @ApiOperation(value = "包含上级部门名称的列表", httpMethod = "POST", response = AreaEntity.class, notes = "根据条件获得包含上级部门名称的列表")
    @RequestMapping(value = "para/list", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> findListByPara(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody AreaVo vo) throws Exception {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();
        // service调用
        List<AreaEntity> list = service.findListByPara(vo);
        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(list);
        result.setMsg(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "新增或者更新", httpMethod = "POST", response = AreaEntity.class, notes = "保存资源到数据库")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> saveOrUpdate(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "保存对象", required = true) @RequestBody AreaEntity entity) throws Exception {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();

        String valMsg = service.validate(entity);
        if (valMsg != null) {
            result.setCode(ResultPojo.CODE_FORMAT_ERR);
            result.setMsg(valMsg);
            return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
        }
        service.save(entity);
        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(entity);
        result.setMsg(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "更新", httpMethod = "POST", response = AreaEntity.class, notes = "根据id更新数据库资源")
    @RequestMapping(value = "{id}", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> update(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "更新对象", required = true) @RequestBody AreaEntity entity) {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();
        // service调用
        service.save(entity);
        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(entity);
        result.setMsg(ResultPojo.MSG_SUCCESS);// TODO 根据具体需求配置返回消息
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "物理删除", httpMethod = "POST", response = AreaEntity.class, notes = "根据id物理删除资源")
    @RequestMapping(value = "{id}/delete", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> delete(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "id", required = true) @PathVariable("id") String id) {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();
        // service调用
        service.delete(id);
        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(id);
        result.setMsg(ResultPojo.MSG_SUCCESS);// TODO 根据具体需求配置返回消息
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "地区信息列表", httpMethod = "GET", response = AreaEntity.class, notes = "根据pid和lvl获得地区信息,lvl=0时获取所有级别")
    @RequestMapping(value = "pid/{pid}/{lvl}", method = RequestMethod.GET)
    public ResultPojo getAreaByPid(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "地区pid", required = true) @PathVariable("pid") String pid,
            @ApiParam(value = "lvl级别", required = true) @PathVariable("lvl") String lvl) {
        AreaEntity areaParam = new AreaEntity();
        areaParam.setPid(pid);
        areaParam.setLvl(Integer.valueOf(lvl));
        List<AreaEntity> areaList = service.getListByCriteria(areaParam);
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, areaList);
    }
    
    @ApiOperation(value = "地区信息列表", httpMethod = "GET", response = AreaEntity.class, notes = "根据pid和lvl获得地区信息,lvl=0时获取所有级别")
    @RequestMapping(value = "lvl/{lvl}", method = RequestMethod.GET)
    public ResultPojo getAreaByLvl(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "lvl级别", required = true) @PathVariable("lvl") Integer lvl) {
        AreaEntity areaParam = new AreaEntity();
        areaParam.setLvl(lvl);
        List<AreaEntity> areaList = service.getListByCriteria(areaParam);
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, areaList);
    }
    
    @ApiOperation(value = "下属地区列表", httpMethod = "GET", response = AreaEntity.class, notes = "根据id该地区的下属地区信息")
    @RequestMapping(value = "{id}/subs", method = RequestMethod.GET)
    public ResultPojo getSubAreas(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "id", required = true) @PathVariable("id") String id) {
        AreaEntity areaParam = new AreaEntity();
        areaParam.setPid(id);
        List<AreaEntity> areaList = service.getListByCriteria(areaParam);
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, areaList);
    }

    @ApiOperation(value = "地区信息下拉列表", httpMethod = "GET", response = AreaEntity.class, notes = "地区信息下拉列表")
    @RequestMapping(value = "dorpdown", method = RequestMethod.GET)
    public ResultPojo getDorpdown(HttpServletRequest request) {
        List<AreaEntity> shengList = null;
        List<AreaEntity> areaList = service.getAreaListByPid("-1");
        if (null == areaList || areaList.isEmpty()) {

        } else {
            areaList.get(0).getId();
            shengList = service.getAreaListByPid(areaList.get(0).getId());
            for (AreaEntity icpArea : shengList) {
                List<AreaEntity> shiList = service.getAreaListByPid(icpArea.getId());
                for (AreaEntity icpArea2 : shiList) {
                    List<AreaEntity> quList = service.getAreaListByPid(icpArea2.getId());
                    icpArea2.setChild(quList);
                }
                icpArea.setChild(shiList);
            }
        }
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, shengList);
    }
    
    @ApiOperation(value = "获得用户信息树用的地区列表信息（附带样式）", httpMethod = "GET", response = AreaEntity.class, notes = "根据pid获得用户信息树用的地区列表信息（附带样式）")
    @RequestMapping(value = "css/list/{pid}", method = RequestMethod.GET)
    public List<AreaCSSVo> getAreaInfoByPid(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "地区pid", required = true) @PathVariable("pid") String pid) {
        AreaEntity areaParam = new AreaEntity();
        areaParam.setPid(pid);
        List<AreaEntity> areaList = service.getListByCriteria(areaParam);
        
        List<AreaCSSVo> areaCSSVoList = new ArrayList<AreaCSSVo>();
        AreaCSSVo areaCSSVo = null;
        for(AreaEntity areaEntity : areaList) {
        	areaCSSVo = new AreaCSSVo();
        	areaCSSVo.setId(areaEntity.getId());
        	if(areaEntity.getLeaf() ==1 ){
        		areaCSSVo.setHasChildren(Boolean.FALSE);
        		areaCSSVo.setClasses("file");
        	} else {
        		areaCSSVo.setHasChildren(Boolean.TRUE);
        		areaCSSVo.setClasses("folder");
        	}
        	areaCSSVo.setText("<input type='radio' value='" + areaEntity.getCode() +"' id='area_" +areaEntity.getId() + "' name='areas' /><span>" +areaEntity.getName() + "</span>" );
        	areaCSSVoList.add(areaCSSVo);
        }
        return areaCSSVoList;
    }
    
    @ApiOperation(value = "获得用户信息树用的地区列表信息", httpMethod = "GET", response = AreaEntity.class, notes = "获得用户信息树用的地区列表信息")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<AreaListVo> getAreaInfoList(HttpServletRequest request, HttpServletResponse response) {
        AreaEntity areaParam = new AreaEntity();
        List<AreaEntity> areaList = service.getListByCriteria(areaParam);
        List<AreaListVo> areaListVoList = new ArrayList<AreaListVo>();
        AreaListVo areaListVo = null;
        for(AreaEntity areaEntity : areaList) {
        	areaListVo = new AreaListVo();
        	areaListVo.setId(areaEntity.getId());
        	areaListVo.setpId(areaEntity.getPid());
        	areaListVo.setName(areaEntity.getName());
        	areaListVo.setAdminCode(areaEntity.getAdminCode());
        	areaListVo.setCode(areaEntity.getCode());
        	areaListVo.setLeaf(areaEntity.getLeaf());
        	areaListVo.setLvl(areaEntity.getLvl());
        	areaListVo.setType(areaEntity.getType());
        	areaListVo.setWeight(areaEntity.getWeight());
        	areaListVoList.add(areaListVo);
        }
        return areaListVoList;
    }

    @ApiOperation(value = "根据Pid获得用户信息树用的地区列表信息", httpMethod = "GET", response = AreaEntity.class, notes = "根据Pid获得用户信息树用的地区列表信息")
    @RequestMapping(value = "/list/{pid}", method = RequestMethod.GET)
    public List<AreaListVo> getAreaInfoList(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "地区pid", required = true) @PathVariable("pid") String pid) {
        AreaEntity areaParam = new AreaEntity();
        areaParam.setPid(pid);
        List<AreaEntity> areaList = service.getListByCriteria(areaParam);
        List<AreaListVo> areaListVoList = new ArrayList<AreaListVo>();
        AreaListVo areaListVo = null;
        for(AreaEntity areaEntity : areaList) {
        	areaListVo = new AreaListVo();
        	areaListVo.setId(areaEntity.getId());
        	areaListVo.setpId(areaEntity.getPid());
        	areaListVo.setName(areaEntity.getName());
        	areaListVo.setAdminCode(areaEntity.getAdminCode());
        	areaListVo.setCode(areaEntity.getCode());
        	areaListVo.setLeaf(areaEntity.getLeaf());
        	areaListVo.setLvl(areaEntity.getLvl());
        	areaListVo.setType(areaEntity.getType());
        	areaListVo.setWeight(areaEntity.getWeight());
        	areaListVoList.add(areaListVo);
        }
        return areaListVoList;
    }
    
    @ApiOperation(value = "根据区域Code和开通方式开通试点", httpMethod = "POST", response = AreaEntity.class, notes = "根据区域Code和开通方式开通试点")
    @RequestMapping(value = "/{areaCode}/open/{openPilotType}", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> openPilotType(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "areaCode", required = true) @PathVariable("areaCode") String areaCode,
            @ApiParam(value = "openPilotType", required = true) @PathVariable("openPilotType") String openPilotType) {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();
        // service调用
        int cnt = service.updateTypeByCond(areaCode, openPilotType);
        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(cnt);
        result.setMsg(ResultPojo.MSG_SUCCESS);
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }

}
