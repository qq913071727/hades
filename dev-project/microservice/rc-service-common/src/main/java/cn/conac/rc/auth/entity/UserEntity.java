package cn.conac.rc.auth.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@Entity
@Table(name = "RC_USER")
public class UserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @ApiModelProperty("用户ID")
    private Integer id;

    @ApiModelProperty("登录名")
    @Column(nullable = true, length = 100, unique = true)
    private String loginName;

    @ApiModelProperty("si_code")
    @Column(length = 300)
    private String siCode;

    @ApiModelProperty("org_name")
    private String orgName;

    @ApiModelProperty("真实名")
    @Column(length = 300)
    private String realName;

    @ApiModelProperty("姓名（编制域名用）")
    @Column(length = 200)
    private String fullName;

    @ApiModelProperty("密码")
    @Column(nullable = true, length = 32)
    private String password;

    @ApiModelProperty("loginCount")
    private Integer loginCount;

    @ApiModelProperty("电话号码")
    @Column(length = 50)
    private String mobile;

    @ApiModelProperty("电子邮箱")
    @Column(length = 200)
    private String email;

    @ApiModelProperty("区分机构域名和编制域名（1：机构域名  0：编制域名）")
    private Integer isAdmin;

    @ApiModelProperty("用户角色清单")
    @ManyToMany(fetch = FetchType.EAGER) // 立即从数据库中进行加载数据;
    @JoinTable(name = "IcpUserRole", joinColumns = { @JoinColumn(name = "userId") }, inverseJoinColumns = {
            @JoinColumn(name = "roleId") })
    private List<RoleEntity> roleList;

    private String imgUrl;

    @Column(length = 32)
    private String areaId;

    @Column(length = 32)
    private String orgId;

    @ApiModelProperty("用户类型")
    @Column(length = 1)
    private int userType;

    @ApiModelProperty("是否禁用（1：禁用 0：正常）")
    private Integer isDisabled;

    public String getLoginName() {
        return loginName;
    }

    public String getRealName() {
        return realName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<RoleEntity> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<RoleEntity> roleList) {
        this.roleList = roleList;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getAreaId() {
        return areaId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 密码盐.
     * @return
     */
    public String getCredentialsSalt() {
        return this.getLoginName() + "8d78869f470951332959580424d4bf4f";

    }

    public String getSiCode() {
        return siCode;
    }

    public void setSiCode(String siCode) {
        this.siCode = siCode;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Integer isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Integer getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    public Integer getIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(Integer isDisabled) {
        this.isDisabled = isDisabled;
    }
}
