package cn.conac.rc.auth.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.auth.entity.RoleEntity;
import cn.conac.rc.auth.entity.UserEntity;
import cn.conac.rc.auth.service.RoleService;
import cn.conac.rc.auth.service.UserService;
import cn.conac.rc.auth.vo.UserVo;
import cn.conac.rc.framework.utils.MD5Utils;
import cn.conac.rc.framework.vo.ResultPojo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="user/")
public class UserController {
	
	@Autowired
    UserService userService;
    @Autowired
    RoleService roleService;
	
	@ApiOperation(value = "用户列表", httpMethod = "POST", response = UserEntity.class, notes = "根据传入参数获取用户列表信息")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request,
			             HttpServletResponse response,
			             @ApiParam(value = "用户查询参数", required = true) @RequestBody UserVo user) throws Exception{
		 Page<UserEntity> list = userService.list(user);
	     ResultPojo result = new ResultPojo();
	     result.setCode(ResultPojo.CODE_SUCCESS);
	     result.setResult(list);
	        // result.setMsg();
	     return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
//	@ApiOperation(value = "添加用户", httpMethod = "POST", response = UserEntity.class, notes = "对用户信息进行添加操作")
//	@RequestMapping(value = "", method = RequestMethod.POST)
//	public ResultPojo saveUser(HttpServletRequest request,
//            HttpServletResponse response,
//            @ApiParam(value = "用户信息", required = true) @RequestBody UserEntity user) throws Exception{
//		//TODO 对密码进行MD5加密
//		MD5Utils md5 = new MD5Utils(null,"MD5");
//		user.setPassword(md5.encode(user.getPassword()));
//		userService.save(user);
//		return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
//				user);
//	}
	
//	@ApiOperation(value = "逻辑删除用户", httpMethod = "GET", response = UserEntity.class, notes = "对用户进行逻辑删除操作")
//	@RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
//	public ResultPojo deleteUserLogic(HttpServletRequest request,
//                                       HttpServletResponse response,
//    @ApiParam(value = "用户id", required = true) @PathVariable("id") String id) throws Exception{
//		UserEntity user = userService.findById(id);
//		//设置为不可用
//		user.setDeleteMark(UserEntity.DeleteMark.UNAVAILABLE);
//		userService.save(user);
//		return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
//				user);
//    }
	
	@ApiOperation(value = "通过登录名查询用户", httpMethod = "GET", response = UserEntity.class, notes = "通过登录名查询用户")
	@RequestMapping(value = "name/{loginName}", method = RequestMethod.GET)
	public ResultPojo getUserByLoginName(HttpServletRequest request,
            HttpServletResponse response,
            @ApiParam(value = "用户信息", required = true) @PathVariable("loginName") String loginName) throws Exception{
		UserEntity user = userService.findByLoginName(loginName);
		return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				user);
	}
	@ApiOperation(value = "通过siCode查询用户", httpMethod = "GET", response = UserEntity.class, notes = "通过登录名查询用户")
	@RequestMapping(value = "sicode/{siCode}/{fullName}", method = RequestMethod.GET)
	public ResultPojo getUserBySiCodeFullname(HttpServletRequest request,
			HttpServletResponse response,
			@ApiParam(value = "用户信息", required = true) @PathVariable("siCode") String siCode,
			@ApiParam(value = "用户信息", required = true) @PathVariable("fullName") String fullName) throws Exception{
		List<UserEntity> user = userService.findBySiUserCodeFullname(siCode,fullName);
		return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				user);
	}
	@ApiOperation(value = "通过siCode查询用户", httpMethod = "GET", response = UserEntity.class, notes = "通过登录名查询用户")
	@RequestMapping(value = "sicode/{siCode}", method = RequestMethod.GET)
	public ResultPojo getUserBySiCode(HttpServletRequest request,
			HttpServletResponse response,
			@ApiParam(value = "用户信息", required = true) @PathVariable("siCode") String siCode) throws Exception{
		List<UserEntity> user = userService.findBySiUserCode(siCode);
		return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				user);
	}
	@ApiOperation(value = "通过id查询用户", httpMethod = "GET", response = UserEntity.class, notes = "通过id查询用户")
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResultPojo getUserById(HttpServletRequest request,
            HttpServletResponse response,
            @ApiParam(value = "用户id", required = true) @PathVariable("id") String id) throws Exception{
		UserEntity user = userService.findById(id);
		return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				user);
	}
	

    @ApiOperation(value = "获取用户角色", httpMethod = "GET", response = UserEntity.class, notes = "获取用户角色信息")
    @RequestMapping(value = "{id}/roles", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> getRolesByUserId(HttpServletRequest request,
                         HttpServletResponse response,
                         @ApiParam(value = "用户id", required = true) @PathVariable("id") String id) throws Exception{
         List<RoleEntity> roles = roleService.findRolesByUserId(id);
         ResultPojo result = new ResultPojo();
         result.setCode(ResultPojo.CODE_SUCCESS);
         result.setResult(roles);
            // result.setMsg();
         return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }
}
