package cn.conac.rc.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.entity.Dictionary;
import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.repository.DictionaryRepository;
@Service
public class DictionaryService extends GenericService<Dictionary, Integer> {
	@Autowired
	private DictionaryRepository dicRepository;
	
	public List<Dictionary> findByKey(String key) {
		Criteria<Dictionary> dc = new Criteria<Dictionary>();
		if (StringUtils.isNotBlank(key)) {
			dc.add(Restrictions.eq("type", key, true));
		}
		Sort sort = new Sort(Direction.ASC, "sort"); 
		//根据动态查询条件及排序依据查询集合
		return dicRepository.findAll(dc, sort);
	}

	public void deleteByKey(String key) {
		Criteria<Dictionary> dc = new Criteria<Dictionary>();
		if (StringUtils.isNotBlank(key)) {
			dc.add(Restrictions.eq("type", key, true));
		}
		//根据动态查询条件及排序依据查询集合
		List<Dictionary> dics = this.findAll(dc);
		if(null == dics || dics.isEmpty()){
			return ;
		}
		dicRepository.deleteInBatch(dics);
	}
}
