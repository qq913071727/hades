package cn.conac.rc.ofs.mq.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 发送消息队列所用的bean
 * @author lucj
 */
@ApiModel
public class MqMessageVo implements Serializable {
	
	private static final long serialVersionUID = 7493293046556830360L;
	
	/**
	 * 表主键ID
	 */
	@ApiModelProperty("表主键ID")
	private String id;
	
	/**
	 * 操作标示
	 * 0：新增
	 * 1：更新
	 * 2：删除
	 */
	@ApiModelProperty("操作标志")
    private Integer flag;

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
