package cn.conac.rc.file.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.rc.framework.entity.DataEntity;
import io.swagger.annotations.ApiModel;

@ApiModel
@Entity
@Table(name="RC_FILE")
public class FileEntity extends DataEntity<FileEntity> implements Serializable  {
	
	private static final long serialVersionUID = -4880566342419881457L;
	// 文件类型：头像文件
	public static final String TYPE_PHOTO = "0";
	// 文件类型：批文文件
	public static final String TYPE_APPROVAL = "1";
	// 文件类型：清单文件
	public static final String TYPE_ITEM = "2";
	
	/**
	 * fileName
	 */
	private String fileName;
	/**
	 * absolutePath
	 */
	private String absolutePath;
	/**
	 * relativePath
	 */
	private String relativePath;
	/**
	 * useCnt
	 */
	private Integer useCnt;
	/**
	 * type
	 */
	private String type;
	
	public void setFileName(String fileName){
		this.fileName=fileName;
	}
	@Column(name = "FILE_NAME")
	public String getFileName(){
		return fileName;
	}
	@Column(name = "ABSOLUTE_PATH")
	public String getAbsolutePath() {
		return absolutePath;
	}
	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}
	@Column(name = "RELATIVE_PATH")
	public String getRelativePath() {
		return relativePath;
	}
	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}
	public void setUseCnt(Integer useCnt){
		this.useCnt=useCnt;
	}
	@Column(name = "USE_CNT")
	public Integer getUseCnt(){
		return useCnt;
	}
	public void setType(String type){
		this.type=type;
	}
	@Column(name = "TYPE")
	public String getType(){
		return type;
	}
}

