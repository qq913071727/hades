package cn.conac.rc.file.repository;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.file.entity.FileEntity;
import cn.conac.rc.framework.repository.GenericDao;

@Repository
//@Transactional
public interface FileRepository extends GenericDao<FileEntity, Integer> {

	/**
	 * 根据文件ID获取文件信息
	 * 
	 * @param Id 文件ID
	 * @return 对应的文件信息
	 */
	FileEntity findById(@Param("id") Integer id);
}