package cn.conac.rc.file.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import cn.conac.rc.file.entity.AppFilesEntity;
import cn.conac.rc.file.entity.FileEntity;
import cn.conac.rc.file.service.FileService;
import cn.conac.rc.framework.vo.ResultPojo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 文件管理（上传、下载功能等）
 * 
 * @author wangmeng
 */
@RestController
public class FileController {

	@Autowired
	FileService rcFileService;

	@ApiOperation(value = "文件信息存储", httpMethod = "POST", response = FileEntity.class, notes = "暂存、下一步以及提交的时候，存储文件信息并且返回文件相关信息")
	@RequestMapping(value = "/saveFile", method = RequestMethod.POST,  produces = "application/json;charset=utf-8")
	public @ResponseBody ResultPojo saveFile(HttpServletRequest request,	HttpServletResponse response,
			@RequestBody @ApiParam(value = "文件信息对象", required = true) AppFilesEntity appFiles ) {

		ResultPojo result = new ResultPojo();
		// 存储文件信息
		appFiles = rcFileService.saveFileInfo(appFiles);
		// 返回存储结果
		if(appFiles == null){
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg(ResultPojo.MSG_FAILURE);
			result.setResult(null);
		} else {
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setMsg(ResultPojo.MSG_SUCCESS);
			result.setResult(appFiles);
		}
		return result;
	}

	@ApiOperation(value = "文件下载", httpMethod = "GET", response = FileEntity.class, notes = "下载相关文件")
	@RequestMapping(value = "/download/{fileId}", method = RequestMethod.GET,  produces = "application/json;charset=utf-8")
	public @ResponseBody ResultPojo download(HttpServletRequest request,	HttpServletResponse response,
			@PathVariable("fileId") @ApiParam(value = "文件ID", required = true) Integer fileId){
		
		ResultPojo result = new ResultPojo();
		try{
			result = rcFileService.download(request, response, fileId);
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg("文件下载失败！文件ID："+fileId);
		}
		return result;
	}

	@ApiOperation(value = "文件检索", httpMethod = "GET", response = FileEntity.class, notes = "检索相关文件信息")
	@RequestMapping(value = "/search/{fileId}", method = RequestMethod.GET,  produces = "application/json;charset=utf-8")
	public @ResponseBody ResultPojo searchFile(HttpServletRequest request,	HttpServletResponse response,
			@PathVariable("fileId") @ApiParam(value = "文件ID", required = true) Integer fileId){

		// 根据文件ID检索文件信息
		ResultPojo result = new ResultPojo();
		FileEntity rcFile = rcFileService.searchFileById(fileId);

		// 文件存在
		if(null != rcFile && rcFile.getId()!=null){
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setMsg("检索文件成功！文件ID：" + fileId);
			result.setResult(rcFile);
		// 文件不存在
		} else {
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg("检索文件失败！文件ID：" + fileId);
		}
		return result;
	}

//	@ApiOperation(value = "文件删除(物理删除)", httpMethod = "POST", response = Integer.class, notes = "删除相关文件信息以及服务器端文件")
//	@RequestMapping(value = "/deleteInPhysics", method = RequestMethod.POST,  produces = "application/json;charset=utf-8")
//	public @ResponseBody ResultPojo deleteFileInPhysics(HttpServletRequest request,	HttpServletResponse response,
//			@RequestParam("fileId") @ApiParam(value = "文件ID", required = true) String fileId){
//
//		ResultPojo result = new ResultPojo();
//		int delNum = rcFileService.deleteFileByIdInPhysics(fileId);
//		// 文件删除成功
//		if(delNum>0){
//			result.setCode(ResultPojo.CODE_SUCCESS);
//			result.setMsg("物理删除文件成功！文件ID：" + fileId);
//			result.setResult(delNum);
//		// 文件信息不存在
//		} else {
//			result.setCode(ResultPojo.CODE_FAILURE);
//			result.setMsg("物理删除文件失败！文件ID：" + fileId);
//			result.setResult(delNum);
//		}
//		return result;
//	}
}
