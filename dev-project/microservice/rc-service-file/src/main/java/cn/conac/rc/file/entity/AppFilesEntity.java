package cn.conac.rc.file.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AppFilesEntity类
 *
 * @author beanCreator
 * @date 2016-11-02
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="OFS_APP_FILES")
public class AppFilesEntity implements Serializable {

	private static final long serialVersionUID = 1478071306899285540L;

	@Id
	@ApiModelProperty("ID")
	private Integer id;

	@ApiModelProperty("机构ID号")
	private Integer baseId;

	@ApiModelProperty("存储文件URL")
	private String appUrl;

	public void setId(Integer id){
		this.id=id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="mySeqGenerator")
	@SequenceGenerator(name = "mySeqGenerator", sequenceName = "s_ofs_app_files", allocationSize = 1)
	public Integer getId(){
		return id;
	}

	public void setBaseId(Integer baseId){
		this.baseId=baseId;
	}

	@Column(name="org_id")
	public Integer getBaseId(){
		return baseId;
	}

	public void setAppUrl(String appUrl){
		this.appUrl=appUrl;
	}

	public String getAppUrl(){
		return appUrl;
	}

}
