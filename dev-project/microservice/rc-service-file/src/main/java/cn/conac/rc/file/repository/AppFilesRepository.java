package cn.conac.rc.file.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.file.entity.AppFilesEntity;

@Repository
public interface AppFilesRepository extends GenericDao<AppFilesEntity, Integer> {
}
