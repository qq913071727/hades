package cn.conac.rc.workflow.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.workflow.service.ActivitiService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Created by haocm on 2017-4-6.
 */
@RestController
@RequestMapping(value = "workflow/")
public class WorkFlowRestController
{
    @Autowired
    private ActivitiService myService;

    @ApiOperation(value = "开启流程实例", httpMethod = "GET", notes = "开启流程实例")

    @RequestMapping(value = "/process/{childItemId}/{sysType}/{areaId}/{userType}", method = RequestMethod.GET)

    public void startProcessInstance(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "childItemId", required = true) @PathVariable("childItemId") String childItemId,
            @ApiParam(value = "sysType", required = true) @PathVariable("sysType") String sysType,
            @ApiParam(value = "areaId", required = true) @PathVariable("areaId") String areaId,
            @ApiParam(value = "userType", required = true) @PathVariable("userType") String userType)
    {
        try {
            myService.startProcess(childItemId, sysType,areaId,userType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ApiOperation(value = "完成任务", httpMethod = "GET", notes = "完成任务")
    @RequestMapping(value = "/complete/{joinApproved}/{taskId}", method = RequestMethod.GET)

    public String complete(@PathVariable Boolean joinApproved, @PathVariable String taskId)
    {
        try {
            myService.completeTasks(joinApproved, taskId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "ok";
    }

    @ApiOperation(value = "通过id号和系统类别获取活动任务", httpMethod = "GET", notes = "通过id号和系统类别获取活动任务")
    @RequestMapping(value = "/task/{childItemId}/{sysType}", method = RequestMethod.GET)

    public ResponseEntity<ResultPojo> getTask(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "childItemId", required = true) @PathVariable("childItemId") String childItemId,
            @ApiParam(value = "sysType", required = true) @PathVariable("sysType") String sysType)
    {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();
        // service调用
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setMsg(ResultPojo.MSG_SUCCESS);
        List<Task> tasks = null;
        Task task = null;
        try {
            tasks = myService.getTasks(childItemId, sysType);
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(ResultPojo.CODE_FAILURE);
            result.setMsg(ResultPojo.MSG_FAILURE);
        }
        if (tasks != null && tasks.size() > 0) {
            task = tasks.get(0);
        } else {
            task = null;
        }
        result.setResult(task);
        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }
}
