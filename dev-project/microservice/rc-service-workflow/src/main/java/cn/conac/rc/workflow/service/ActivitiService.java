package cn.conac.rc.workflow.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by haocm on 2017-4-6.
 */
@Service
@Transactional
public class ActivitiService
{
    @Autowired
    private TaskService taskService;

    @Autowired
    private RuntimeService runtimeService;

    /*
       开始流程
     */
    public void startProcess(String childItemId, String sysType, String areaId, String userType) throws Exception
    {

        Map<String, Object> variables = new HashMap<String, Object>();

        variables.put("childItemId", childItemId);

        variables.put("sysType", sysType);
        variables.put("areaId", areaId);
        variables.put("userType", userType);
        runtimeService.startProcessInstanceByKey("itemProcessDefinition", variables);
    }

    /*
        获得某个人的任务列表
     */
    public List<Task> getTasks(String childItemId,String sysType) throws Exception
    {
        return taskService.createTaskQuery().processVariableValueEquals("childItemId", childItemId)
                .processVariableValueEquals("sysType",sysType).list();
//        return  taskService.createTaskQuery().processVariableValueEquals(variables).list();
//        return taskService.createTaskQuery().taskCandidateUser(assignee).list();
    }

    /*
    完成任务
     */
    public void completeTasks(Boolean joinApproved, String taskId) throws Exception
    {

        Map<String, Object> taskVariables = new HashMap<String, Object>();

        taskVariables.put("joinApproved", joinApproved);

        taskService.complete(taskId, taskVariables);
    }
}
