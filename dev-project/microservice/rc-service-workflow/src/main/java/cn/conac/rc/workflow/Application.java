package cn.conac.rc.workflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableAutoConfiguration(exclude = {
     org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration.class,
     org.activiti.spring.boot.SecurityAutoConfiguration.class,
     org.springframework.boot.actuate.autoconfigure.ManagementWebSecurityAutoConfiguration.class
 })
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
