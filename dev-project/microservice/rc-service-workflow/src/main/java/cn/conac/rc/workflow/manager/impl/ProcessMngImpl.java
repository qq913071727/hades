package cn.conac.rc.workflow.manager.impl;

import java.util.Map;

import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.stereotype.Service;

@Service
public class ProcessMngImpl extends ProcessCommonMngImpl {
	
	/**
	 * 部署流程
	 * @return
	 */
	public Deployment deployProcess() {
		return this.getRepositoryService()
				   .createDeployment()
				   .addClasspathResource("com/conac/process/resources/processDefinition.bpmn").deploy();
	}
	
	/**
	 * 启动流程
	 * @return
	 */
	public ProcessInstance startProcess(Map<String,Object> variables) {
		ProcessInstance instance = this.getRuntimeService().startProcessInstanceByKey("itemProcessDefinition",variables);
		Integer childItemId = (Integer)variables.get("childItemId");
		String sysType = (String)variables.get("sysType");
		setProcessVariables(instance.getId(), variables);
		//获取流程实例启动后的第一个任务
		Task task = this.findTaskByVariable(childItemId, sysType);
		//设置第一个节点任务的节点定义id到流程变量中
		if(task != null && task.getTaskDefinitionKey() != null){
		  variables.put("startActivityId", task.getTaskDefinitionKey());
		  setProcessVariables(instance.getId(), variables);
		}
		return instance;
	}
	
	/**
	 * 设置流程变量
	 * @return
	 */
	public void setProcessVariables(String ExcutionId,Map<String,Object> variables) {
		this.getRuntimeService().setVariables(ExcutionId, variables);
	}
	
	/**
	 * 获取流程变量
	 * @return
	 */
	public Map<String,Object> getProcessVariables(String ExcutionId) {
		return this.getRuntimeService().getVariables(ExcutionId);
	}

}
