package cn.conac.rc.msg.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.msg.entity.SmsBean;
import cn.conac.rc.msg.service.SmsService;
import cn.conac.rc.framework.vo.ResultPojo;

/**
 * 短信发送
 * @author haocm
 * @version 1.0
 */
@RestController
public class SmsController {
    /**
     * 短信发送服务
     */
    @Autowired
    private SmsService smsService;

    /**
     * 发送短信.
     * @param smsBean 短信封装bean
     * @return 短信发送状态
     * @author haocm
     */
    @RequestMapping(value = "/sms/send", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> sendSms(@RequestBody SmsBean smsBean) {
        ResponseEntity<ResultPojo> response = null;
        // Map<String, String> parameter = new HashMap<String, String>();
        // parameter.put("checkCode", "123456");
        // parameter.put("time", "30");
        int returnValue = smsService.sendSms(smsBean);
        if (returnValue != 0) {
            response = new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_FAILURE, ResultPojo.MSG_FAILURE,
                    null), HttpStatus.OK);
        } else {
            response = new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                    null), HttpStatus.OK);
        }
        return response;
    }
}
