package cn.conac.rc.msg.utils;

import java.security.MessageDigest;

/**
 * MD5加盐加密算法
 * @author haocm
 * @version 1.0
 */
public class MD5Utils {

    /**
     * 
     */
    private static final String[] HEX_DIGITS = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d",
        "e", "f" };

    /**
     * 盐值
     */
    private Object salt;
    /**
     * algorithm
     */
    private String algorithm;

    public MD5Utils(Object salt, String algorithm) {
        this.salt = salt;
        this.algorithm = algorithm;
    }

    /**
     * 转码.
     * @param rawPass 转码字符串
     * @return 转码后字符串
     * @author haocm
     */
    public String encode(String rawPass) {
        String result = null;
        try {
            MessageDigest md = MessageDigest.getInstance(algorithm);
            // 加密后的字符串
            result = byteArrayToHexString(md.digest(mergePasswordAndSalt(rawPass).getBytes("utf-8")));
        } catch (Exception ex) {
        }
        return result;
    }

    /**
     * 密码验证.
     * @param encPass encPass
     * @param rawPass rawPass
     * @return 是否通过
     * @author haocm
     */
    public boolean isPasswordValid(String encPass, String rawPass) {
        String pass1 = "" + encPass;
        String pass2 = encode(rawPass);

        return pass1.equals(pass2);
    }

    /**
     * 合并密码和盐值.
     * @param password 密码
     * @return 加盐后的密码
     * @author haocm
     */
    private String mergePasswordAndSalt(String password) {
        if (password == null) {
            password = "";
        }

        if ((salt == null) || "".equals(salt)) {
            return password;
        } else {
            return password + "{" + salt.toString() + "}";
        }
    }

    /**
     * 转换字节数组为16进制字串
     * @param b 字节数组
     * @return 16进制字串
     */
    private static String byteArrayToHexString(byte[] b) {
        StringBuffer resultSb = new StringBuffer();
        for (byte element : b) {
            resultSb.append(byteToHexString(element));
        }
        return resultSb.toString();
    }

    /**
     * 转16进制方法.
     * @param b 待转换字符
     * @return 转换后的字符串
     * @author haocm
     */
    private static String byteToHexString(byte b) {
        int n = b;
        if (n < 0) {
            n = 256 + n;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return HEX_DIGITS[d1] + HEX_DIGITS[d2];
    }
}