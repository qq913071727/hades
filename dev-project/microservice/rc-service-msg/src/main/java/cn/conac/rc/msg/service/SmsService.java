package cn.conac.rc.msg.service;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cn.conac.rc.msg.entity.SmsBean;
import cn.conac.rc.msg.utils.SendSmsUtil;
import cn.emay.sdk.client.api.Client;

/**
 * 短信发送service
 * @author haocm
 * @version 1.0 2015-8-6
 */
@Service
public class SmsService {
    /**
     * id
     */
    @Value("${spring.sms.id}")
    private String id;
    /**
     * key
     */
    @Value("${spring.sms.key}")
    private String key;
    /**
     * 密码
     */
    @Value("${spring.sms.password}")
    private String password;
    /**
     * smsPriority
     */
    @Value("${spring.sms.smsPriority}")
    private String smsPriority;
    /**
     * 日志
     */
    private Logger logger = Logger.getLogger(SmsService.class.getName());

    /**
     * 发送短信.
     * @param smsBean 短信封装bean
     * @return 发送状态
     * @author haocm
     */
    public int sendSms(SmsBean smsBean) {

        // ResourceBundle smsconfig = PropertyResourceBundle
        // .getBundle("smsconfig");
        int fret = 0;
        try {
            fret = SendSmsUtil.sendSmsJson(smsBean.getMobile(),smsBean.getSmsTemplate(),smsBean.getMap(),new Date());
        } catch (Exception e) {
            e.printStackTrace();
            return fret;
        } /*
         * finally { sdkclient.closeChannel(); }
         */
        return fret;
    }

    /**
     * 模板替换方法.
     * @param templateContent 模板内容
     * @param parameter 参数
     * @return 替换后的string
     * @author haocm
     */
    public static String replaceParameter(String templateContent, Map<String, String> parameter) {
        if (parameter == null) {
            return templateContent;
        }
        for (String key : parameter.keySet()) {
            templateContent = templateContent.replace("{" + key + "}", parameter.get(key));
        }
        return templateContent;
    }

    /**
     * 替换字符串.
     * @param str str
     * @param str2 str2
     * @return 替换后的字符串
     * @author haocm
     */
    public static String replaceProname(String str, String str2) {
        StringBuffer bf = new StringBuffer();
        bf.append(str.substring(0, str.indexOf("p")));
        bf.append(str2);
        bf.append(str.substring(str.indexOf("e") + 1, str.length()));
        return bf.toString();
    }

    /**
     * 替换字符串.
     * @param str str
     * @param str2 str2
     * @return 替换后的字符串
     * @author haocm
     */
    public static String replaceDoamins(String str, String str2) {
        StringBuffer bf = new StringBuffer();
        bf.append(str.substring(0, str.indexOf("d")));
        bf.append(str2);
        bf.append(str.substring(str.indexOf("s") + 1, str.length()));
        return bf.toString();
    }

}
