//package cn.conac.bpp;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.logging.Logger;
//
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//
//import cn.conac.bpp.common.persistence.entity.ResultPojo;
//import SmsBean;
//import SmsController;
//
//import com.alibaba.fastjson.JSON;
//
///**
// * 测试类
// * @author haocm
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
//@WebAppConfiguration
//public class MsgControllerTests {
//
//    protected static final String LOGIN_NAME = "20000424";
//    protected static final String MAIL_VERIFY_FTL = "register-mailverify.ftl";
//    @Autowired
//    SmsController smsController;
//
//    // @Test
//    // public void sendMail() {
//    // MailBean bean = new MailBean();
//    // bean.setEmailTemplate(MAIL_VERIFY_FTL);
//    // bean.setMailTo("haocm@conac.cn");
//    // Map<String, String> map = new HashMap<String, String>();
//    // map.put("userName", "abcdefg");
//    // map.put("title", "邮件注册");
//    // map.put("urlAddress", "www.baidu.com");
//    // bean.setMap(map);
//    // bean.setTitle("hello world");
//    // ResponseEntity<ResultPojo> result = mailController.sendMail(bean);
//    // Assert.assertNotNull(result);
//    // Assert.assertEquals(200, result.getStatusCode().value());
//    // Logger.getGlobal()
//    // .info("End keywords test" + JSON.toJSONString(result));
//    // }
//    @Test
//    public void sendSms() {
//        SmsBean bean = new SmsBean();
//        // parameter.put("checkCode", "123456");
//        // parameter.put("time", "30");
//        bean.setMobile("15210003509");
//        Map<String, String> map = new HashMap<String, String>();
//        map.put("checkCode", "123456");
//        map.put("time", "30");
//        bean.setMap(map);
//        bean.setOrgName("【域名管理中心】");
//        bean.setSmsTemplate("您的验证码是{checkCode}有效时间{time}分钟");
//        ResponseEntity<ResultPojo> result = smsController.sendSms(bean);
//        Assert.assertNotNull(result);
//        Assert.assertEquals(200, result.getStatusCode().value());
//        Logger.getGlobal().info("End keywords test" + JSON.toJSONString(result));
//    }
//}
