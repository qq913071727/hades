package cn.conac.rc.monitor.service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.monitor.entity.LocalWeight;
import cn.conac.rc.monitor.repository.LocalWeightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.framework.utils.StringUtils;

import java.util.List;


@Service
public class LocalWeightService extends GenericService<LocalWeight, String> {
    @Autowired
    private LocalWeightRepository localWeightRepository;

    /**
     * 根据条件查询
     *
     * @param param
     * @return
     */
    public List<LocalWeight> getCalendarByCriteria(LocalWeight param) {
        Criteria<LocalWeight> criteria = this.createCriteria(param);
        return localWeightRepository.findAll(criteria);
    }

    /**
     * 拼接查询条件
     *
     * @param param
     * @return Criteria<LocalWeight>
     * @author haocm
     */
    private Criteria<LocalWeight> createCriteria(LocalWeight param) {
        Criteria<LocalWeight> dc = new Criteria<LocalWeight>();
        if (StringUtils.isNotBlank(param.getAreaCode())) {
            dc.add(Restrictions.eq("areaCode", param.getAreaCode(), true));
        }
        if (StringUtils.isNotBlank(param.getLocal())) {
            dc.add(Restrictions.eq("local", param.getLocal(), true));
        }
        if(StringUtils.isNotBlank(param.getAreaId())){
            dc.add(Restrictions.eq("areaId",param.getAreaId(),true));
        }
//		if (null != calendarParam.getRowdate()) {
//			dc.add(Restrictions.gte("rowdate", calendarParam.getRowdate(), true));
//		}
//		if (null != calendarParam.getRowdate()) {
//			dc.add(Restrictions.lte("rowdate", calendarParam.getRowdate(), true));
//		}

//		// dc.add(Restrictions.in("", list, true));
//		// dc.add(Restrictions.lte("", insertDateEnd, true));
//		// dc.add(Restrictions.gte("", insertDateEnd, true));
        return dc;
    }
}