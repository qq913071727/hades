package cn.conac.rc.monitor.repository;

import java.util.List;

import cn.conac.rc.monitor.entity.LocalStatus;
import cn.conac.rc.monitor.entity.VLocalMonitorDay;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;

/**
 * 检测系统统计用jpa
 *
 * @author haocm
 */
@Repository
public interface LocalStatusRepository extends GenericDao<LocalStatus, String> {



    @Query(value = "select count(1) from local_status t where  t.xzqhbm =:xzqhbm and t.blbm =:blbm ", nativeQuery = true)
    Integer getMatterCounts(@Param("xzqhbm") String xzqhbm, @Param("blbm") String blbm);

    @Query(value = "select count(1) from local_status t where  t.xzqhbm =:xzqhbm ", nativeQuery = true)
    Integer getMatterCounts(@Param("xzqhbm") String xzqhbm);
}
