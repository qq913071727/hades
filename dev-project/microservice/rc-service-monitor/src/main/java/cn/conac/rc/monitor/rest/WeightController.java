package cn.conac.rc.monitor.rest;

import cn.conac.rc.framework.utils.ResultPojo;
import cn.conac.rc.framework.utils.StringUtils;
import cn.conac.rc.monitor.entity.LocalWeight;
import cn.conac.rc.monitor.service.LocalWeightService;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * Created by haocm on 2016-8-30.
 */
@RestController
public class WeightController {
    @Autowired
    LocalWeightService localWeightService;


    @ApiOperation(value = "获取权重信息", httpMethod = "POST", response = LocalWeight.class, notes = "获取权重信息")
    @RequestMapping(value = "/weight", method = RequestMethod.POST)
    public ResultPojo getWeight(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestBody LocalWeight localWeight) {
        List<LocalWeight> weightList = localWeightService.getCalendarByCriteria(localWeight);
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,weightList);
    }
    @ApiOperation(value = "获取权重信息", httpMethod = "POST", response = LocalWeight.class, notes = "获取权重信息")
    @RequestMapping(value = "/weight/save", method = RequestMethod.POST)
    public ResultPojo save(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestBody LocalWeight localWeight) {
        if(StringUtils.isNotBlank(localWeight.getId())){
            LocalWeight dbWeight = localWeightService.findById(localWeight.getId());
            localWeight.setId(dbWeight.getId());
        }
        localWeight.setLocal("1");
        localWeightService.save(localWeight);
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,localWeight);
    }

}
