package cn.conac.rc.monitor.config;

import cn.conac.rc.framework.utils.ConfigUtil;

public class GlobalConfig
{
    private static ConfigUtil configUtil = new ConfigUtil("application.properties", true);

    public static String
            ADDRESS = "http://" + configUtil.getValue("elasticsearch_ip", "172.17.80.166") + ":" + configUtil.getValue("elasticsearch_port", "9200");

    public static String ES_INDEX = configUtil.getValue("elasticsearch_index", "test");
    public static String ES_TYPE = configUtil.getValue("elasticsearch_type", "news");
    public static String SPRIT = "/";
}
