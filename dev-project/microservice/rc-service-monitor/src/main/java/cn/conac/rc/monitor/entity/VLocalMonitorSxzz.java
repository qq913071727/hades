package cn.conac.rc.monitor.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.rc.framework.entity.MonitorBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

;


@ApiModel
@Entity
@Table(name="V_LOCAL_MONITOR_SXZZ")
public class VLocalMonitorSxzz extends MonitorBaseEntity implements Serializable  {
    @ApiModelProperty("地方事项编码")
    private String dfsxbm;
    @ApiModelProperty("办理环节")
    private String blhj;
    @ApiModelProperty("办理时间")
    private String blsj;
    @ApiModelProperty("办理状态")
    private String blzt;
    @ApiModelProperty("原系统业务流水号")
    private String yxtywlsh;
    @ApiModelProperty("排序")
    private String sortt;
    public void setDfsxbm(String dfsxbm){
        this.dfsxbm=dfsxbm;
    }
    public String getDfsxbm(){
        return dfsxbm;
    }

    public String getBlhj() {
        return blhj;
    }

    public void setBlhj(String blhj) {
        this.blhj = blhj;
    }

    public String getBlsj() {
        return blsj;
    }

    public void setBlsj(String blsj) {
        this.blsj = blsj;
    }

    public String getBlzt() {
        return blzt;
    }

    public void setBlzt(String blzt) {
        this.blzt = blzt;
    }

    public String getYxtywlsh() {
        return yxtywlsh;
    }

    public void setYxtywlsh(String yxtywlsh) {
        this.yxtywlsh = yxtywlsh;
    }

    public String getSortt() {
        return sortt;
    }

    public void setSortt(String sortt) {
        this.sortt = sortt;
    }
}