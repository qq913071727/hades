package cn.conac.rc.monitor.service;

import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import org.apache.commons.lang.StringUtils;
import org.hibernate.NullPrecedence;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.monitor.entity.LocalStatus;
import cn.conac.rc.monitor.repository.LocalStatusRepository;
import cn.conac.rc.monitor.vo.StatisticsVO;

//import cn.conac.rc.framework.jpa.Criteria;

@Service
public class LocalStatusService extends GenericService<LocalStatus, String> {
	@Autowired
	private LocalStatusRepository localStatusRepository;

	/**
	 * 事项总数
	 *
	 * @param param
	 * @return
	 */
	public Integer getMatterCount(StatisticsVO param) {
		int count = 0;
		if("0".equals(param.getDepartment()) || StringUtils.isBlank(param.getDepartment())){
			//所有部门
			count = localStatusRepository.getMatterCounts(param.getDfsxbm());
		}else{
			count = localStatusRepository.getMatterCounts(param.getDfsxbm(),param.getDepartment());

		}
		return count;
	}
	/**
	 * 根据条件查询(分页)
	 * 
	 * @param param
	 * @return
	 */
	public Page<LocalStatus> getStatusPageByCriteria(Pageable pageable,StatisticsVO param) {
		Criteria<LocalStatus> criteria = this.createCriteria(param);

		Sort sort = new Sort(Direction.DESC, "bjcsFlag","bjDate");
		return localStatusRepository.findAll(criteria, pageable);
	}
	/**
	 * 根据条件查询（所有）
	 *
	 * @param param
	 * @return
	 */
	public List<LocalStatus> getStatusByCriteria(StatisticsVO param) {


		Criteria<LocalStatus> criteria = this.createCriteria(param);

		List<Sort.Order> orders = new ArrayList<>();
		Sort.Order orderBjcsFlag = new Sort.Order(Sort.Direction.DESC,"bjcsFlag");
		Sort.Order orderBjDate = new Sort.Order(Sort.Direction.DESC,"bjDate");
		orders.add(orderBjcsFlag.nullsLast());
		orders.add(orderBjDate.nullsLast());

		Sort sort = new Sort(orders);
		return localStatusRepository.findAll(criteria,sort);
	}

	/**
	 * 拼接查询条件
	 * 
	 * @param param
	 *            参数对象
	 * @return Criteria<LocalStatus> JPA封装类
	 * @author haocm
	 */
	private Criteria<LocalStatus> createCriteria(StatisticsVO param) {
		Criteria<LocalStatus> dc = new Criteria<LocalStatus>();
		dc.add(Restrictions.eq("newFlag", "1", true));
		if (null != param.getStartDate()) {
			dc.add(Restrictions.gte("sqDate", param.getStartDate(), true));
		}
		if (null != param.getEndDate()) {
			dc.add(Restrictions.lte("sqDate", param.getEndDate(), true));
		}
		if (StringUtils.isNotBlank(param.getDepartment())
				&& !"0".equals(param.getDepartment())) {
			dc.add(Restrictions.eq("blbm", param.getDepartment(), true));
		}
		if (StringUtils.isNotBlank(param.getSlzt())) {
			dc.add(Restrictions.isNotNull("slzt"));
			if (!LocalStatus.slzt00.equals(param.getSlzt())) {
				dc.add(Restrictions.eq("slzt", param.getSlzt(), true));
			}
		}
		if (StringUtils.isNotBlank(param.getBjcs())) {
			if(LocalStatus.bjcs01.equals(param.getBjcs())){
				dc.add(Restrictions.eq("bjcsFlag", param.getBjcs(), true));
			}else{
				dc.add(Restrictions.or(Restrictions.isNull("bjcsFlag"),Restrictions.eq("bjcsFlag", "0", true)));
			}
		}
		if(StringUtils.isNotBlank(param.getAreaCode())){
			dc.add(Restrictions.eq("xzqhbm",param.getAreaCode(),true));
		}
		if(StringUtils.isNotBlank(param.getYxtywlsh())){
			dc.add(Restrictions.eq("yxtywlsh",param.getYxtywlsh(),true));
		}
		if(StringUtils.isNotBlank(param.getDfsxbm())){
			dc.add(Restrictions.eq("dfsxbm",param.getDfsxbm(),true));
		}
		if(StringUtils.isNotBlank(param.getSxmc())){
			dc.add(Restrictions.like("sxmc",param.getSxmc(),true));
		}
		if(StringUtils.isNotBlank(param.getStage())){
			if ("3".equals(param.getStage())){
				//sq
				dc.add(Restrictions.isNull("slzt"));
				dc.add(Restrictions.isNull("bjzt"));
				dc.add(Restrictions.isNotNull("sqDate"));
			}else if ("2".equals(param.getStage())){
				//sl
				dc.add(Restrictions.isNull("bjzt"));
				dc.add(Restrictions.isNotNull("slzt"));
				dc.add(Restrictions.isNotNull("sqDate"));
			}else if ("1".equals(param.getStage())){
				//bj
				dc.add(Restrictions.isNotNull("bjzt"));
				dc.add(Restrictions.isNotNull("slzt"));
				dc.add(Restrictions.isNotNull("sqDate"));
			}
		}
		// if (null != calendarParam.getRowdate()) {
		// dc.add(Restrictions.gte("rowdate", calendarParam.getRowdate(),
		// true));
		// }
		// if (null != calendarParam.getRowdate()) {
		// dc.add(Restrictions.lte("rowdate", calendarParam.getRowdate(),
		// true));
		// }

		// // dc.add(Restrictions.in("", list, true));
		// // dc.add(Restrictions.lte("", insertDateEnd, true));
		// // dc.add(Restrictions.gte("", insertDateEnd, true));
		return dc;
	}

	
}
