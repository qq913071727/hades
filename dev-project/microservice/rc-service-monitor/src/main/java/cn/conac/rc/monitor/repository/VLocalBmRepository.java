package cn.conac.rc.monitor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.monitor.entity.LocalStatus;
import cn.conac.rc.monitor.entity.VLocalBm;

/**
 * 检测系统统计用jpa
 *
 * @author haocm
 */
@Repository
public interface VLocalBmRepository extends GenericDao<VLocalBm, String>
{

}