package cn.conac.rc.monitor.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.utils.DateUtils;
import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.monitor.entity.LocalStatus;
import cn.conac.rc.monitor.entity.VLocalBm;
import cn.conac.rc.monitor.entity.VLocalSx;
import cn.conac.rc.monitor.service.LocalStatusService;
import cn.conac.rc.monitor.service.VLocalBmService;
import cn.conac.rc.monitor.service.VLocalMonitorBlbmService;
import cn.conac.rc.monitor.service.VLocalMonitorDayService;
import cn.conac.rc.monitor.service.VLocalSxService;
import cn.conac.rc.monitor.vo.StatisticsVO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 功能: 用户相关controller
 *
 * @author haocm
 */
@RestController
public class StatusController
{
    @Autowired
    LocalStatusService localStatusService;

    @Autowired
    VLocalMonitorDayService vLocalMonitorDayService;

    @Autowired
    VLocalMonitorBlbmService vLocalMonitorBlbmService;

    @Autowired VLocalBmService vLocalBmService;

    @Autowired VLocalSxService vLocalSxService;

    @ApiOperation(value = "根据id获取状态信息", httpMethod = "GET", response = LocalStatus.class, notes = "根据id获取状态信息")
    @RequestMapping(value = "/status/{id}", method = RequestMethod.GET)
    public ResultPojo getStatus(
            HttpServletRequest request,
            HttpServletResponse response,
            @ApiParam(value = "user_id", required = true) @PathVariable("id") String id)
    {
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                localStatusService.findById(id));
    }

    @ApiOperation(value = "获取部门列表", httpMethod = "GET", response = LocalStatus.class, notes = "获取部门列表")
    @RequestMapping(value = "/monitor/statistics/blbm/{areaCode}", method = RequestMethod.GET)
    public ResultPojo getBlbmList(
            HttpServletRequest request,
            HttpServletResponse response,
            @ApiParam(value = "areaCode", required = true) @PathVariable("areaCode") String areaCode)
    {
        VLocalBm parm = new VLocalBm();
        parm.setXzqhbm(areaCode);
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                vLocalBmService.getBlbmList(parm));
    }

    @ApiOperation(value = "获取事项数", httpMethod = "GET", response = LocalStatus.class, notes = "获取事项数")
    @RequestMapping(value = "/monitor/statistics/sx/{areaCode}", method = RequestMethod.GET)
    public ResultPojo getBlsxList(
            HttpServletRequest request,
            HttpServletResponse response,
            @ApiParam(value = "areaCode", required = true) @PathVariable("areaCode") String areaCode)
    {
        VLocalSx parm = new VLocalSx();
        parm.setXzqhbm(areaCode);
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                vLocalSxService.getBlbmSxList(parm));
    }

    @ApiOperation(value = "事项", httpMethod = "POST", response = LocalStatus.class, notes = "事项总数")
    @RequestMapping(value = "/monitor/statistics/counts", method = RequestMethod.POST)
    public ResultPojo getStatusCounts(HttpServletRequest request,
            HttpServletResponse response, @RequestBody StatisticsVO localStatus)
    {
        localStatus.setStartDate(DateUtils.getDateStart(localStatus.getStartDate()));
        localStatus.setEndDate(DateUtils.getDateEnd(localStatus.getEndDate()));
        // 按注册日期排序
        //Sort sort = new Sort(Direction.ASC, "sqDate", "keyword");
        if (localStatus.getPage() == null) {
            return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                    localStatusService.getStatusByCriteria(localStatus));
        } else {
            Sort sort = new Sort(Sort.Direction.DESC, "sqDate");
            Pageable pageable = new PageRequest(localStatus.getPage(), localStatus.getSize(), sort);
            return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                    localStatusService.getStatusPageByCriteria(pageable, localStatus));
        }
    }

    @ApiOperation(value = "事项总数", httpMethod = "POST", response = LocalStatus.class, notes = "事项总数")
    @RequestMapping(value = "/monitor/statistics/matter/counts", method = RequestMethod.POST)
    public ResultPojo getMatterCounts(HttpServletRequest request,
            HttpServletResponse response, @RequestBody StatisticsVO localStatus)
    {
        if (StringUtils.isBlank(localStatus.getDepartment())) {
            return new ResultPojo(ResultPojo.CODE_FAILURE, ResultPojo.MSG_FAILURE, 0);
        }
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                localStatusService.getMatterCount(localStatus));
    }

    @ApiOperation(value = "事项总数", httpMethod = "GET", response = LocalStatus.class, notes = "事项总数")
    @RequestMapping(value = "/monitor/statistics/countsmonth/{department}/{startDate}/{endDate}/{areaCode}", method = RequestMethod.GET)
    public ResultPojo getStatusCounts(
            HttpServletRequest request,
            HttpServletResponse response,
            @ApiParam(value = "department", required = true) @PathVariable("department") String department,
            @ApiParam(value = "startDate", required = true) @PathVariable("startDate") String startDate,
            @ApiParam(value = "endDate", required = true) @PathVariable("endDate") String endDate,
            @ApiParam(value = "areaCode", required = true) @PathVariable("areaCode") String areaCode)
    {
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                vLocalMonitorDayService.getCountsByMonth(department, areaCode,
                        startDate, endDate));
    }

    @ApiOperation(value = "事项总数", httpMethod = "GET", response = LocalStatus.class, notes = "事项总数")
    @RequestMapping(value = "/monitor/statistics/countsyear/{department}/{startDate}/{endDate}/{areaCode}", method = RequestMethod.GET)
    public ResultPojo getStatusYear(
            HttpServletRequest request,
            HttpServletResponse response,
            @ApiParam(value = "department", required = true) @PathVariable("department") String department,
            @ApiParam(value = "startDate", required = true) @PathVariable("startDate") String startDate,
            @ApiParam(value = "endDate", required = true) @PathVariable("endDate") String endDate,
            @ApiParam(value = "areaCode", required = true) @PathVariable("areaCode") String areaCode)
    {
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                vLocalMonitorDayService.getCountsByYear(department, areaCode,
                        startDate, endDate));
    }

    @ApiOperation(value = "事项总数", httpMethod = "GET", response = LocalStatus.class, notes = "事项总数")
    @RequestMapping(value = "/monitor/statistics/countsquarter/{department}/{startDate}/{endDate}/{areaCode}", method = RequestMethod.GET)
    public ResultPojo getStatusQuarter(
            HttpServletRequest request,
            HttpServletResponse response,
            @ApiParam(value = "department", required = true) @PathVariable("department") String department,
            @ApiParam(value = "startDate", required = true) @PathVariable("startDate") String startDate,
            @ApiParam(value = "endDate", required = true) @PathVariable("endDate") String endDate,
            @ApiParam(value = "areaCode", required = true) @PathVariable("areaCode") String areaCode)
    {
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                vLocalMonitorDayService.getCountsByQuarter(department,
                        areaCode, startDate, endDate));
    }

    @ApiOperation(value = "事项总数", httpMethod = "GET", response = LocalStatus.class, notes = "事项总数")
    @RequestMapping(value = "/monitor/statistics/countsblbm/{department}/{startDate}/{endDate}/{areaCode}", method = RequestMethod.GET)
    public ResultPojo getStatusBlbm(
            HttpServletRequest request,
            HttpServletResponse response,
            @ApiParam(value = "department", required = true) @PathVariable("department") String department,
            @ApiParam(value = "startDate", required = true) @PathVariable("startDate") String startDate,
            @ApiParam(value = "endDate", required = true) @PathVariable("endDate") String endDate,
            @ApiParam(value = "areaCode", required = true) @PathVariable("areaCode") String areaCode)
    {
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                vLocalMonitorDayService.getCountsByBlbm(department, areaCode,
                        startDate, endDate));
    }

//	@ApiOperation(value = "事项总数", httpMethod = "GET", response = LocalStatus.class, notes = "事项总数")
//	@RequestMapping(value = "/monitor/statistics/blbm/{scope}/{startDate}/{endDate}/{areaCode}/{group}", method = RequestMethod.GET)
//	public ResultPojo getStatusBlbmList(
//			HttpServletRequest request,
//			HttpServletResponse response,
//			@ApiParam(value = "查询范围：1-本级，2-全局", required = true) @PathVariable("scope") String scope,
//			@ApiParam(value = "开始时间", required = true) @PathVariable("startDate") String startDate,
//			@ApiParam(value = "截止时间", required = true) @PathVariable("endDate") String endDate,
//			@ApiParam(value = "分组查询条件：1-按部门，2-按办理事项", required = true) @PathVariable("group") String group,
//			@ApiParam(value = "areaCode", required = true) @PathVariable("areaCode") String areaCode) {
//		return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
//				vLocalMonitorBlbmService.getCountsBroupByBlbm(startDate,
//						endDate, areaCode, group,scope, null));
//	}

    @ApiOperation(value = "事项总数", httpMethod = "GET", response = LocalStatus.class, notes = "事项总数")
    @RequestMapping(value = "/monitor/statistics/blbm/{scope}/{startDate}/{endDate}/{areaCode}/{group}/{bjzt}", method = RequestMethod.GET)
    public ResultPojo getStatusBlbmListByBjzt(
            HttpServletRequest request,
            HttpServletResponse response,
            @ApiParam(value = "查询范围：1-本级，2-全局", required = true) @PathVariable("scope") String scope,
            @ApiParam(value = "开始时间", required = true) @PathVariable("startDate") String startDate,
            @ApiParam(value = "截止时间", required = true) @PathVariable("endDate") String endDate,
            @ApiParam(value = "地区code", required = true) @PathVariable("areaCode") String areaCode,
            @ApiParam(value = "分组查询条件：1-按部门，2-按办理事项", required = true) @PathVariable("group") String group,
            @ApiParam(value = "办理状态", required = true) @PathVariable("bjzt") String bjzt)
    {

//		Sort sort = new Sort(Sort.Direction.DESC, "sqDate");
//		Pageable pageable = new PageRequest(0, 10, sort);

        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                vLocalMonitorBlbmService.getCountsBroupByBlbm(startDate,
                        endDate, areaCode, group, scope, bjzt));
    }
}
