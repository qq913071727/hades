package cn.conac.rc.monitor.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.monitor.entity.VLocalMonitorDay;
import cn.conac.rc.monitor.repository.VLocalMonitorDayRepository;

@Service
public class VLocalMonitorDayService extends
		GenericService<VLocalMonitorDay, String> {
	@Autowired
	private VLocalMonitorDayRepository vLocalMonitorDayRepository;

	public List<VLocalMonitorDay> getCountsByMonth(String department,
			String startDate, String endDate, String areaCode) {
		List<VLocalMonitorDay> list = null;
		if ("0".equals(department)) {
			// 所有部门
			list = vLocalMonitorDayRepository.getCountsByMonth(startDate,
					endDate, areaCode);
		} else {
			list = vLocalMonitorDayRepository.getCountsByMonth(department,
					startDate, endDate, areaCode);
		}
		return list;
	}

	public List<VLocalMonitorDay> getCountsByQuarter(String department,
			String startDate, String endDate, String areaCode) {
		List<VLocalMonitorDay> list = null;
		if ("0".equals(department)) {
			// 所有部门
			list = vLocalMonitorDayRepository.getCountsByQuarter(startDate,
					endDate, areaCode);
		} else {
			list = vLocalMonitorDayRepository.getCountsByQuarter(department,
					startDate, endDate, areaCode);
		}
		return list;
	}

	public List<VLocalMonitorDay> getCountsByYear(String department,
			String startDate, String endDate, String areaCode) {
		List<VLocalMonitorDay> list = null;
		if ("0".equals(department)) {
			// 所有部门
			list = vLocalMonitorDayRepository.getCountsByYear(startDate,
					endDate, areaCode);
		} else {
			list = vLocalMonitorDayRepository.getCountsByYear(department,
					startDate, endDate, areaCode);
		}
		return list;
	}

	public List<VLocalMonitorDay> getCountsByBlbm(String department,
			String startDate, String endDate, String areaCode) {
		List<VLocalMonitorDay> list = null;
		if ("0".equals(department)) {
			// 所有部门
			list = vLocalMonitorDayRepository.getCountsByBlbm(startDate,
					endDate, areaCode);
		} else {
			list = vLocalMonitorDayRepository.getCountsByBlbm(department,
					startDate, endDate, areaCode);
		}
		return list;
	}
}
