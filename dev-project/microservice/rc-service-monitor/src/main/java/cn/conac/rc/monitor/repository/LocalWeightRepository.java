package cn.conac.rc.monitor.repository;

import cn.conac.rc.monitor.entity.LocalWeight;
import org.springframework.stereotype.Repository;
import cn.conac.rc.framework.repository.GenericDao;


@Repository
public interface LocalWeightRepository extends GenericDao<LocalWeight, String> {
}