package cn.conac.rc.monitor.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.framework.utils.StringUtils;
import cn.conac.rc.monitor.entity.LocalWeight;
import cn.conac.rc.monitor.entity.VLocalBm;
import cn.conac.rc.monitor.repository.LocalWeightRepository;
import cn.conac.rc.monitor.repository.VLocalBmRepository;

@Service
public class VLocalBmService extends GenericService<VLocalBm, String> {
    @Autowired
    private VLocalBmRepository vLocalBmRepository;

    /**
     * 根据条件查询
     *
     * @param param
     * @return
     */
    public List<VLocalBm> getBlbmList(VLocalBm param) {
        Criteria<VLocalBm> criteria = this.createCriteria(param);
        return vLocalBmRepository.findAll(criteria);
    }

    /**
     * 拼接查询条件
     *
     * @param param
     * @return Criteria<LocalWeight>
     * @author haocm
     */
    private Criteria<VLocalBm> createCriteria(VLocalBm param) {
        Criteria<VLocalBm> dc = new Criteria<VLocalBm>();
        if(StringUtils.isNoneBlank(param.getXzqhbm())){
            dc.add(Restrictions.eq("xzqhbm",param.getXzqhbm()));
        }
//		if (null != calendarParam.getRowdate()) {
//			dc.add(Restrictions.gte("rowdate", calendarParam.getRowdate(), true));
//		}
//		if (null != calendarParam.getRowdate()) {
//			dc.add(Restrictions.lte("rowdate", calendarParam.getRowdate(), true));
//		}

//		// dc.add(Restrictions.in("", list, true));
//		// dc.add(Restrictions.lte("", insertDateEnd, true));
//		// dc.add(Restrictions.gte("", insertDateEnd, true));
        return dc;
    }
}