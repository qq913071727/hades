package cn.conac.rc.monitor.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.monitor.entity.VLocalSx;

/**
 * 检测系统统计用jpa
 *
 * @author haocm
 */
@Repository
public interface VLocalSxRepository extends GenericDao<VLocalSx, String>
{

}