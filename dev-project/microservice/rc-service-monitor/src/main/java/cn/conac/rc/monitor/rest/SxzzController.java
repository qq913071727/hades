package cn.conac.rc.monitor.rest;

import cn.conac.rc.framework.utils.ResultPojo;
import cn.conac.rc.monitor.entity.VLocalMonitorSxzz;
import cn.conac.rc.monitor.service.VLocalMonitorSxzzService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 事项追踪
 * Created by haocm on 2016-9-1.
 */
@RestController
public class SxzzController {
    @Autowired
    VLocalMonitorSxzzService service;

    @ApiOperation(value = "事项追踪信息", httpMethod = "POST", response = VLocalMonitorSxzz.class, notes = "事项追踪信息")
    @RequestMapping(value = "/sxzz/list", method = RequestMethod.POST)
    public ResultPojo getSxzz(HttpServletRequest request, HttpServletResponse response,
                              @RequestBody VLocalMonitorSxzz vlocalMonitorSxzz
    ) {
        ResultPojo resultPojo = new ResultPojo();
        resultPojo.setCode(ResultPojo.CODE_SUCCESS);
        resultPojo.setMsg(ResultPojo.MSG_SUCCESS);
        resultPojo.setResult(service.getStatusByCriteria(vlocalMonitorSxzz));
        return resultPojo;
    }

}
