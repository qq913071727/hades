package cn.conac.rc.monitor.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.monitor.entity.VLocalMonitorDayStage1;
import cn.conac.rc.monitor.repository.VLocalMonitorDayStage1Repository;

import static org.bouncycastle.asn1.x500.style.RFC4519Style.st;

@Service
public class VLocalMonitorDayStageService extends GenericService<VLocalMonitorDayStage1, String>
{
    @Autowired
    private VLocalMonitorDayStage1Repository epository;

    public List<VLocalMonitorDayStage1> getCountsByMonth(String department,String dfsxbm,
            String startDate, String endDate, String areaCode,String group)
    {
        System.out.println(department);
        System.out.println(startDate);
        System.out.println(endDate);
        System.out.println(areaCode);
        List<VLocalMonitorDayStage1> list = null;
        if(StringUtils.isNotBlank(group) && "2".equals(group)){
            if ("0".equals(department)|| StringUtils.isBlank(department)) {
                // 所有部门
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByMonthStage1(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByMonthStage2(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByMonthStage3(areaCode, startDate,
                        endDate);
                list = this.getRealListTjDate(list1, list2, list3);
            } else {
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByMonthStage1(department,dfsxbm,
                        areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByMonthStage2(department,dfsxbm,
                        areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByMonthStage3(department,dfsxbm,
                        areaCode, startDate,
                        endDate);
                list = this.getRealListTjDate(list1, list2, list3);
            }
        }else if(StringUtils.isNotBlank(group) && "3".equals(group)){
            //三定系统调用
            List<VLocalMonitorDayStage1> list1 = epository.getF3dCountsByMonthStage1(dfsxbm,areaCode, startDate,
                    endDate);
            List<VLocalMonitorDayStage1> list2 = epository.getF3dCountsByMonthStage2(dfsxbm,areaCode, startDate,
                    endDate);
            List<VLocalMonitorDayStage1> list3 = epository.getF3dCountsByMonthStage3(dfsxbm,areaCode, startDate,
                    endDate);
            list = this.getRealListTjDate(list1, list2, list3);
        } else {
            if ("0".equals(department)|| StringUtils.isBlank(department)) {
                // 所有部门
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByMonthStage1(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByMonthStage2(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByMonthStage3(areaCode, startDate,
                        endDate);
                list = this.getRealListTjDate(list1, list2, list3);
            } else {
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByMonthStage1(department,
                        areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByMonthStage2(department,
                        areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByMonthStage3(department,
                        areaCode, startDate,
                        endDate);
                list = this.getRealListTjDate(list1, list2, list3);
            }
        }
        System.out.println("=================getCountsByMonth==================");
        System.out.println(JSON.toJSONString(list));
        return list;
    }

    public List<VLocalMonitorDayStage1> getCountsByQuarter(String department,String dfsxbm,
            String startDate, String endDate, String areaCode,String group)
    {
        List<VLocalMonitorDayStage1> list = null;
        if(StringUtils.isNotBlank(group) && "2".equals(group)){
            if ("0".equals(department)|| StringUtils.isBlank(department)) {
                // 所有部门
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByQuarterStage1(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByQuarterStage2(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByQuarterStage3(areaCode, startDate,
                        endDate);
                list = this.getRealListTjDate(list1, list2, list3);
            } else {
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByQuarterStage1(department,dfsxbm,
                        areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByQuarterStage2(department,dfsxbm,
                        areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByQuarterStage3(department,dfsxbm,
                        areaCode, startDate,
                        endDate);
                list = this.getRealListTjDate(list1, list2, list3);
            }
        }else{
            if ("0".equals(department)|| StringUtils.isBlank(department)) {
                // 所有部门
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByQuarterStage1(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByQuarterStage2(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByQuarterStage3(areaCode, startDate,
                        endDate);
                list = this.getRealListTjDate(list1, list2, list3);
            } else {
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByQuarterStage1(department,
                        areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByQuarterStage2(department,
                        areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByQuarterStage3(department,
                        areaCode, startDate,
                        endDate);
                list = this.getRealListTjDate(list1, list2, list3);
            }
        }
        return list;
    }

    public List<VLocalMonitorDayStage1> getCountsByYear(String department,String dfsxbm,
            String startDate, String endDate, String areaCode,String group)
    {
        List<VLocalMonitorDayStage1> list = null;
        if(StringUtils.isNotBlank(group) && "2".equals(group)){
            if ("0".equals(department) || StringUtils.isBlank(department)) {
                // 所有部门
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByYearStage1(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByYearStage2(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByYearStage3(areaCode, startDate,
                        endDate);
                list = this.getRealListTjDate(list1, list2, list3);
            } else {
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByYearStage1(department,dfsxbm,
                        areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByYearStage2(department,dfsxbm,
                        areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByYearStage3(department,dfsxbm,
                        areaCode, startDate,
                        endDate);
                list = this.getRealListTjDate(list1, list2, list3);
            }
        }else{
            if ("0".equals(department) || StringUtils.isBlank(department)) {
                // 所有部门
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByYearStage1(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByYearStage2(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByYearStage3(areaCode, startDate,
                        endDate);
                list = this.getRealListTjDate(list1, list2, list3);
            } else {
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByYearStage1(department,
                        areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByYearStage2(department,
                        areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByYearStage3(department,
                        areaCode, startDate,
                        endDate);
                list = this.getRealListTjDate(list1, list2, list3);
            }
        }
        return list;
    }


    public List<VLocalMonitorDayStage1> getRealListBlbmSx(List<VLocalMonitorDayStage1> list1,
            List<VLocalMonitorDayStage1> list2, List<VLocalMonitorDayStage1> list3)
    {
        List<VLocalMonitorDayStage1> retList = new ArrayList<VLocalMonitorDayStage1>();
        Map<String, VLocalMonitorDayStage1> map1 = new HashMap<String, VLocalMonitorDayStage1>();
        Map<String, VLocalMonitorDayStage1> map2 = new HashMap<String, VLocalMonitorDayStage1>();
        Map<String, VLocalMonitorDayStage1> map3 = new HashMap<String, VLocalMonitorDayStage1>();
        Set<String> set = new HashSet<String>();
        //将数据分别放入map中
        int a = list1.size();
        int b = list2.size();
        int c = list3.size();
        int mid = Math.max(a, b);
        int max = Math.max(mid, c);

        for (int i = 0; i < max; i++) {
            VLocalMonitorDayStage1 item = new VLocalMonitorDayStage1();
            if (i < a) {
                VLocalMonitorDayStage1 stage1 = list1.get(i);
                map1.put(stage1.getBlbm()+stage1.getDfsxbm()+stage1.getSxmc(), stage1);
                set.add(stage1.getBlbm()+stage1.getDfsxbm()+stage1.getSxmc());
            }
            if (i < b) {
                VLocalMonitorDayStage1 stage2 = list2.get(i);
                map2.put(stage2.getBlbm()+stage2.getDfsxbm()+stage2.getSxmc(), stage2);
                set.add(stage2.getBlbm()+stage2.getDfsxbm()+stage2.getSxmc());
            }
            if (i < c) {
                VLocalMonitorDayStage1 stage3 = list3.get(i);
                map3.put(stage3.getBlbm()+stage3.getDfsxbm()+stage3.getSxmc(), stage3);
                set.add(stage3.getBlbm()+stage3.getDfsxbm()+stage3.getSxmc());
            }
        }
        for (String str : set) {
            VLocalMonitorDayStage1 item = new VLocalMonitorDayStage1();
            VLocalMonitorDayStage1 stage1 = map1.get(str);
            VLocalMonitorDayStage1 stage2 = map2.get(str);
            VLocalMonitorDayStage1 stage3 = map3.get(str);
            //申请环节
            item.setSqCount(stage1 != null ? stage1.getSqCount() : 0);
            item.setWssqCount(stage1 != null ? stage1.getWssqCount() : 0);
            item.setCksqCount(stage1 != null ? stage1.getCksqCount() : 0);
            //受理环节
            item.setSlCount(stage2 != null ? stage2.getSlCount() : 0);
            item.setBslCount(stage2 != null ? stage2.getBslCount() : 0);
            item.setTbcxsqCount(stage2 != null ? stage2.getTbcxsqCount() : 0);
            //办结环节
            item.setBzsCount(stage3 != null ? stage3.getBzsCount() : 0);
            item.setBztsCount(stage3 != null ? stage3.getBztsCount() : 0);
            item.setYcxbzCount(stage3 != null ? stage3.getYcxbzCount() : 0);
            item.setBjCount(stage3 != null ? stage3.getBjCount() : 0);
            item.setGrbjCount(stage3 != null ? stage3.getGrbjCount() : 0);
            item.setFrbjCount(stage3 != null ? stage3.getFrbjCount() : 0);
            item.setXkbjCount(stage3 != null ? stage3.getXkbjCount() : 0);
            item.setByxkbjCount(stage3 != null ? stage3.getByxkbjCount() : 0);
            item.setZbbjCount(stage3 != null ? stage3.getZbbjCount() : 0);
            item.setZzbjCount(stage3 != null ? stage3.getZzbjCount() : 0);
            item.setBjcsCount(stage3 != null ? stage3.getBjcsCount() : 0);
            item.setTbcxcsCount(stage3 != null ? stage3.getTbcxcsCount() : 0);
            item.setTbcxcscsCount(stage3 != null ? stage3.getTbcxcscsCount() : 0);
            item.setTbcxtsCount(stage3 != null ? stage3.getTbcxtsCount() : 0);
            item.setBjystsCount(stage3 != null ? stage3.getBjystsCount() : 0);

            if (stage1 != null) {
                item.setBlbm(stage1.getBlbm());
                item.setDfsxbm(stage1.getDfsxbm());
                item.setSxmc(stage1.getSxmc());
                item.setXzqhbm(stage1.getXzqhbm());
            } else if (stage2 != null) {
                item.setBlbm(stage2.getBlbm());
                item.setDfsxbm(stage2.getDfsxbm());
                item.setSxmc(stage2.getSxmc());
                item.setXzqhbm(stage2.getXzqhbm());
            } else if (stage3 != null) {
                item.setBlbm(stage3.getBlbm());
                item.setDfsxbm(stage3.getDfsxbm());
                item.setSxmc(stage3.getSxmc());
                item.setXzqhbm(stage3.getXzqhbm());
            }

            retList.add(item);
        }

        return retList;
    }
    public List<VLocalMonitorDayStage1> getRealListTjDate(List<VLocalMonitorDayStage1> list1,
            List<VLocalMonitorDayStage1> list2, List<VLocalMonitorDayStage1> list3)
    {
        List<VLocalMonitorDayStage1> retList = new ArrayList<VLocalMonitorDayStage1>();
        Map<String, VLocalMonitorDayStage1> map1 = new HashMap<String, VLocalMonitorDayStage1>();
        Map<String, VLocalMonitorDayStage1> map2 = new HashMap<String, VLocalMonitorDayStage1>();
        Map<String, VLocalMonitorDayStage1> map3 = new HashMap<String, VLocalMonitorDayStage1>();
        Set<String> set = new HashSet<String>();
        //将数据分别放入map中
        int a = list1.size();
        int b = list2.size();
        int c = list3.size();
        int mid = Math.max(a, b);
        int max = Math.max(mid, c);

        for (int i = 0; i < max; i++) {
            VLocalMonitorDayStage1 item = new VLocalMonitorDayStage1();
            if (i < a) {
                VLocalMonitorDayStage1 stage1 = list1.get(i);
                map1.put(stage1.getBlbm()+stage1.getTjDate(), stage1);
                set.add(stage1.getBlbm()+stage1.getTjDate());
            }
            if (i < b) {
                VLocalMonitorDayStage1 stage2 = list2.get(i);
                map2.put(stage2.getBlbm()+stage2.getTjDate(), stage2);
                set.add(stage2.getBlbm()+stage2.getTjDate());
            }
            if (i < c) {
                VLocalMonitorDayStage1 stage3 = list3.get(i);
                map3.put(stage3.getBlbm()+stage3.getTjDate(), stage3);
                set.add(stage3.getBlbm()+stage3.getTjDate());
            }
        }
        for (String str : set) {
            VLocalMonitorDayStage1 item = new VLocalMonitorDayStage1();
            VLocalMonitorDayStage1 stage1 = map1.get(str);
            VLocalMonitorDayStage1 stage2 = map2.get(str);
            VLocalMonitorDayStage1 stage3 = map3.get(str);
            item.setSqCount(stage1 != null ? stage1.getSqCount() : 0);
            item.setWssqCount(stage1 != null ? stage1.getWssqCount() : 0);
            item.setCksqCount(stage1 != null ? stage1.getCksqCount() : 0);

            item.setSlCount(stage2 != null ? stage2.getSlCount() : 0);
            item.setBslCount(stage2 != null ? stage2.getBslCount() : 0);
            item.setTbcxsqCount(stage2 != null ? stage2.getTbcxsqCount() : 0);


            item.setBzsCount(stage3 != null ? stage3.getBzsCount() : 0);
            item.setBztsCount(stage3 != null ? stage3.getBztsCount() : 0);
            item.setTbcxcsCount(stage3 != null ? stage3.getTbcxcsCount() : 0);
            item.setTbcxcscsCount(stage3 != null ? stage3.getTbcxcscsCount() : 0);
            item.setTbcxtsCount(stage3 != null ? stage3.getTbcxtsCount() : 0);


            item.setYcxbzCount(stage3 != null ? stage3.getYcxbzCount() : 0);
            item.setBjCount(stage3 != null ? stage3.getBjCount() : 0);
            item.setGrbjCount(stage3 != null ? stage3.getGrbjCount() : 0);
            item.setFrbjCount(stage3 != null ? stage3.getFrbjCount() : 0);
            item.setXkbjCount(stage3 != null ? stage3.getXkbjCount() : 0);
            item.setByxkbjCount(stage3 != null ? stage3.getByxkbjCount() : 0);
            item.setZbbjCount(stage3 != null ? stage3.getZbbjCount() : 0);
            item.setZzbjCount(stage3 != null ? stage3.getZzbjCount() : 0);
            item.setBjcsCount(stage3 != null ? stage3.getBjcsCount() : 0);

            item.setBjystsCount(stage3 != null ? stage3.getBjystsCount() : 0);

            if (stage1 != null) {
                item.setTjDate(stage1.getTjDate());
                item.setBlbm(stage1.getBlbm());
                item.setDfsxbm(stage1.getDfsxbm());
                item.setXzqhbm(stage1.getXzqhbm());
            } else if (stage2 != null) {
                item.setTjDate(stage2.getTjDate());
                item.setBlbm(stage2.getBlbm());
                item.setDfsxbm(stage2.getDfsxbm());
                item.setXzqhbm(stage2.getXzqhbm());
            } else if (stage3 != null) {
                item.setTjDate(stage3.getTjDate());
                item.setBlbm(stage3.getBlbm());
                item.setDfsxbm(stage3.getDfsxbm());
                item.setXzqhbm(stage3.getXzqhbm());
            }

            retList.add(item);
        }

        return retList;
    }

    public List<VLocalMonitorDayStage1> getCountsByBlbm(String department,
            String startDate, String endDate, String areaCode)
    {
        List<VLocalMonitorDayStage1> list = null;
        if ("0".equals(department)) {
            // 所有部门
            list = epository.getCountsByBlbm(areaCode, startDate,
                    endDate);
        } else {
            list = epository.getCountsBySx(department,
                    areaCode, startDate,
                    endDate);
        }
        return list;
    }

    public List<VLocalMonitorDayStage1> getCountsByBlbmSx(String department,
            String startDate, String endDate, String areaCode,String group)
    {
        List<VLocalMonitorDayStage1> list = null;
        if(StringUtils.isNotBlank(group) && "1".equals(group)){
            if (StringUtils.isBlank(department) || "0".equals(department)) {
                // 所有部门
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByBlbm1(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByBlbm2(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByBlbm3(areaCode, startDate,
                        endDate);
                list = this.getRealListBlbmSx(list1, list2, list3);
            } else {
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByBlbm1(department, areaCode, startDate, endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByBlbm2(department, areaCode, startDate, endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByBlbm3(department, areaCode, startDate, endDate);
                list = this.getRealListBlbmSx(list1, list2, list3);
            }
        }else{

            if (StringUtils.isBlank(department) || "0".equals(department)) {
                // 所有部门
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByBlbmSx1(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByBlbmSx2(areaCode, startDate,
                        endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByBlbmSx3(areaCode, startDate,
                        endDate);
                list = this.getRealListBlbmSx(list1, list2, list3);
            } else {
                List<VLocalMonitorDayStage1> list1 = epository.getCountsByBlbmSx1(department, areaCode, startDate, endDate);
                List<VLocalMonitorDayStage1> list2 = epository.getCountsByBlbmSx2(department, areaCode, startDate, endDate);
                List<VLocalMonitorDayStage1> list3 = epository.getCountsByBlbmSx3(department, areaCode, startDate, endDate);
                list = this.getRealListBlbmSx(list1, list2, list3);
            }
        }
        return list;
    }
}
