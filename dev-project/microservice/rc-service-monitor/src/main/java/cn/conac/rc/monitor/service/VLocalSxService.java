package cn.conac.rc.monitor.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.framework.utils.StringUtils;
import cn.conac.rc.monitor.entity.VLocalSx;
import cn.conac.rc.monitor.repository.VLocalSxRepository;

@Service
public class VLocalSxService extends GenericService<VLocalSx, String> {
    @Autowired
    private VLocalSxRepository vLocalSxRepository;

    /**
     * 根据条件查询
     *
     * @param param
     * @return
     */
    public List<VLocalSx> getBlbmSxList(VLocalSx param) {
        Criteria<VLocalSx> criteria = this.createCriteria(param);
        return vLocalSxRepository.findAll(criteria);
    }

    /**
     * 拼接查询条件
     *
     * @param param
     * @return Criteria<LocalWeight>
     * @author haocm
     */
    private Criteria<VLocalSx> createCriteria(VLocalSx param) {
        Criteria<VLocalSx> dc = new Criteria<VLocalSx>();
        if(StringUtils.isNoneBlank(param.getXzqhbm())){
            dc.add(Restrictions.eq("xzqhbm",param.getXzqhbm()));
        }
        return dc;
    }
}