package cn.conac.rc.monitor.service;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.utils.OkHttpUtil;
import cn.conac.rc.monitor.config.GlobalConfig;

//import org.apache.solr.client.solrj.impl.HttpClientUtil;

/**
 * Created by haocm on 2016-11-15.
 */
@Service
public class EslService
{
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(EslService.class);
    public String findCliente(String queryString)
    {
        //http://172.17.80.166:9200/test/news/_search
        return OkHttpUtil.post(
                GlobalConfig.ADDRESS+GlobalConfig.SPRIT+
                GlobalConfig.ES_INDEX+GlobalConfig.SPRIT+
                GlobalConfig.ES_TYPE+"/_search", queryString);
    }
}
