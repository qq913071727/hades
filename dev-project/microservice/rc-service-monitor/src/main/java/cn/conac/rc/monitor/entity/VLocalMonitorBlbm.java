package cn.conac.rc.monitor.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.rc.framework.entity.MonitorBaseEntity;
import io.swagger.annotations.ApiModel;


@ApiModel
@Entity
@Table(name="V_LOCAL_MONITOR_BLBM")
public class VLocalMonitorBlbm extends MonitorBaseEntity implements Serializable  {
	/**
	 * tjDate
	 */
	private String tjDate;
	/**
	 * dfsxbm
	 */
	private String dfsxbm;
	/**
	 * blbm
	 */
	private String blbm;
	/**
	 * xzqhbm
	 */
	private String xzqhbm;
	/**
	 * sqCount
	 */
	private Integer sqCount;
	/**
	 * slCount
	 */
	private Integer slCount;
	/**
	 * bslCount
	 */
	private Integer bslCount;
	/**
	 * bjCount
	 */
	private Integer bjCount;
	/**
	 * xkbjCount
	 */
	private Integer xkbjCount;
	/**
	 * byxkbjCount
	 */
	private Integer byxkbjCount;
	/**
	 * zbbjCount
	 */
	private Integer zbbjCount;
	/**
	 * zzbjCount
	 */
	private Integer zzbjCount;
	/**
	 * 办结超时数
	 */
	private Integer bjcsCount;
	/**
	 * 一次性补正数
	 */
	private Integer ycxbzCount;
	/**
	 * 补正数
	 */
	private Integer bzsCount;
	/**
	 * 特别程序申请数
	 */
	private Integer tbcxsqCount;
	/**
	 * 特别程序超时数
	 */
	private Integer tbcxcsCount;
	/**
	 * 特别程序超时次数
	 */
	private Integer tbcxcscsCount;
	/**
	 * 办结用时天数
	 */
	private Integer bjystsCount;
	/**
	 * 补正用时天数
	 */
	private Integer bztsCount;
	/**
	 * 特别程序天数
	 */
	private Integer tbcxtsCount;
	/**
	 * 窗口申请
	 */
	private Integer cksqCount;
	/**
	 * 网上申请
	 */
	private Integer wssqCount;
	public void setTjDate(String tjDate){
		this.tjDate=tjDate;
	}
	public String getTjDate(){
		return tjDate;
	}
	public void setDfsxbm(String dfsxbm){
		this.dfsxbm=dfsxbm;
	}
	public String getDfsxbm(){
		return dfsxbm;
	}
	public void setBlbm(String blbm){
		this.blbm=blbm;
	}
	public String getBlbm(){
		return blbm;
	}
	public void setXzqhbm(String xzqhbm){
		this.xzqhbm=xzqhbm;
	}
	public String getXzqhbm(){
		return xzqhbm;
	}
	public void setSqCount(Integer sqCount){
		this.sqCount=sqCount;
	}
	public Integer getSqCount(){
		return sqCount;
	}
	public void setSlCount(Integer slCount){
		this.slCount=slCount;
	}
	public Integer getSlCount(){
		return slCount;
	}
	public void setBslCount(Integer bslCount){
		this.bslCount=bslCount;
	}
	public Integer getBslCount(){
		return bslCount;
	}
	public void setBjCount(Integer bjCount){
		this.bjCount=bjCount;
	}
	public Integer getBjCount(){
		return bjCount;
	}
	public void setXkbjCount(Integer xkbjCount){
		this.xkbjCount=xkbjCount;
	}
	public Integer getXkbjCount(){
		return xkbjCount;
	}
	public void setByxkbjCount(Integer byxkbjCount){
		this.byxkbjCount=byxkbjCount;
	}
	public Integer getByxkbjCount(){
		return byxkbjCount;
	}
	public void setZbbjCount(Integer zbbjCount){
		this.zbbjCount=zbbjCount;
	}
	public Integer getZbbjCount(){
		return zbbjCount;
	}
	public void setZzbjCount(Integer zzbjCount){
		this.zzbjCount=zzbjCount;
	}
	public Integer getZzbjCount(){
		return zzbjCount;
	}
	public Integer getBjcsCount() {
		return bjcsCount;
	}
	public void setBjcsCount(Integer bjcsCount) {
		this.bjcsCount = bjcsCount;
	}
	public Integer getYcxbzCount() {
		return ycxbzCount;
	}
	public void setYcxbzCount(Integer ycxbzCount) {
		this.ycxbzCount = ycxbzCount;
	}
	public Integer getBzsCount() {
		return bzsCount;
	}
	public void setBzsCount(Integer bzsCount) {
		this.bzsCount = bzsCount;
	}
	public Integer getTbcxsqCount() {
		return tbcxsqCount;
	}
	public void setTbcxsqCount(Integer tbcxsqCount) {
		this.tbcxsqCount = tbcxsqCount;
	}
	public Integer getTbcxcsCount() {
		return tbcxcsCount;
	}
	public void setTbcxcsCount(Integer tbcxcsCount) {
		this.tbcxcsCount = tbcxcsCount;
	}
	public Integer getTbcxcscsCount() {
		return tbcxcscsCount;
	}
	public void setTbcxcscsCount(Integer tbcxcscsCount) {
		this.tbcxcscsCount = tbcxcscsCount;
	}
	public Integer getBjystsCount() {
		return bjystsCount;
	}
	public void setBjystsCount(Integer bjystsCount) {
		this.bjystsCount = bjystsCount;
	}
	public Integer getBztsCount() {
		return bztsCount;
	}
	public void setBztsCount(Integer bztsCount) {
		this.bztsCount = bztsCount;
	}
	public Integer getTbcxtsCount() {
		return tbcxtsCount;
	}
	public void setTbcxtsCount(Integer tbcxtsCount) {
		this.tbcxtsCount = tbcxtsCount;
	}
	public Integer getCksqCount() {
		return cksqCount;
	}
	public void setCksqCount(Integer cksqCount) {
		this.cksqCount = cksqCount;
	}
	public Integer getWssqCount() {
		return wssqCount;
	}
	public void setWssqCount(Integer wssqCount) {
		this.wssqCount = wssqCount;
	}
	
}

