package cn.conac.rc.monitor.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

import cn.conac.rc.monitor.entity.LocalStatus;

/**
 * 监控首页model
 * 
 * @author haocm
 *
 */
@ApiModel
public class StatisticsVO {
	private String tjDate;
	
	@ApiModelProperty("日期标识")
	private String dateFlag;
	@ApiModelProperty("地区code")
	private String areaCode;
	@ApiModelProperty("部门")
	private String department;
	@ApiModelProperty("原系统业务流水号")
	private String yxtywlsh;
	@ApiModelProperty("地方事项编码")
	private String dfsxbm;
	private String sxmc;
	private String stage;
	private String status;
	private Date startDate;
	private Date endDate;
	private String group;
	@ApiModelProperty("事项总数")
	private Integer matterTotal;
	@ApiModelProperty("累计申请数")
	private Integer sqCount;
	@ApiModelProperty("累计办结数")
	private Integer bjCount;
	@ApiModelProperty("累计受理数")
	private Integer slCount;
	@ApiModelProperty("一次性补正数")
	private Integer ycxbzCount;
	@ApiModelProperty("办结超时数")
	private Integer bjcsCount;
	@ApiModelProperty("补正数")
	private Integer bzscount;
	private String slzt;
	private String bjzt;
	private String bjcs;
	@ApiModelProperty("受理比例")
	private String acceptProportion;
	@ApiModelProperty("办结比例")
	private String banjieProportion;
	@ApiModelProperty("超时办结数")
	private Integer timeoutBanjieCount;
	@ApiModelProperty("超时未办结数")
	private Integer timeoutWeiBanjieCount;
	private Integer page;
	private Integer size;
	@ApiModelProperty("Essearch")
	private String queryString;
	public Integer getMatterTotal() {
		return matterTotal;
	}
	public void setMatterTotal(Integer matterTotal) {
		this.matterTotal = matterTotal;
	}
	public String getAcceptProportion() {
		return acceptProportion;
	}
	public void setAcceptProportion(String acceptProportion) {
		this.acceptProportion = acceptProportion;
	}
	public String getBanjieProportion() {
		return banjieProportion;
	}
	public void setBanjieProportion(String banjieProportion) {
		this.banjieProportion = banjieProportion;
	}
	public Integer getTimeoutBanjieCount() {
		return timeoutBanjieCount;
	}
	public void setTimeoutBanjieCount(Integer timeoutBanjieCount) {
		this.timeoutBanjieCount = timeoutBanjieCount;
	}
	public Integer getTimeoutWeiBanjieCount() {
		return timeoutWeiBanjieCount;
	}
	public void setTimeoutWeiBanjieCount(Integer timeoutWeiBanjieCount) {
		this.timeoutWeiBanjieCount = timeoutWeiBanjieCount;
	}
	public String getDateFlag() {
		return dateFlag;
	}
	public void setDateFlag(String dateFlag) {
		this.dateFlag = dateFlag;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getSlzt() {
		return slzt;
	}
	public void setSlzt(String slzt) {
		this.slzt = slzt;
	}
	public String getBjzt() {
		return bjzt;
	}
	public void setBjzt(String bjzt) {
		this.bjzt = bjzt;
	}
	public String getBjcs() {
		return bjcs;
	}
	public void setBjcs(String bjcs) {
		this.bjcs = bjcs;
	}
	public String getTjDate() {
		return tjDate;
	}
	public void setTjDate(String tjDate) {
		this.tjDate = tjDate;
	}
	public Integer getSqCount() {
		return sqCount;
	}
	public void setSqCount(Integer sqCount) {
		this.sqCount = sqCount;
	}
	public Integer getBjCount() {
		return bjCount;
	}
	public void setBjCount(Integer bjCount) {
		this.bjCount = bjCount;
	}
	public Integer getSlCount() {
		return slCount;
	}
	public void setSlCount(Integer slCount) {
		this.slCount = slCount;
	}
	public Integer getYcxbzCount() {
		return ycxbzCount;
	}
	public void setYcxbzCount(Integer ycxbzCount) {
		this.ycxbzCount = ycxbzCount;
	}
	public Integer getBjcsCount() {
		return bjcsCount;
	}
	public void setBjcsCount(Integer bjcsCount) {
		this.bjcsCount = bjcsCount;
	}
	public Integer getBzscount() {
		return bzscount;
	}
	public void setBzscount(Integer bzscount) {
		this.bzscount = bzscount;
	}

	public String getYxtywlsh() {
		return yxtywlsh;
	}

	public void setYxtywlsh(String yxtywlsh) {
		this.yxtywlsh = yxtywlsh;
	}

	public String getDfsxbm() {
		return dfsxbm;
	}

	public void setDfsxbm(String dfsxbm) {
		this.dfsxbm = dfsxbm;
	}

	public String getSxmc() {
		return sxmc;
	}

	public void setSxmc(String sxmc) {
		this.sxmc = sxmc;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public String getGroup()
	{
		return group;
	}

	public void setGroup(String group)
	{
		this.group = group;
	}

	public String getQueryString()
	{
		return queryString;
	}

	public void setQueryString(String queryString)
	{
		this.queryString = queryString;
	}
}
