package cn.conac.rc.monitor.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.rc.framework.entity.MonitorBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by haocm on 2016-8-30.
 */


@ApiModel
@Entity
@Table(name = "local_weight")
public class LocalWeight extends MonitorBaseEntity implements Serializable {
    @ApiModelProperty("地区code")
    private String areaCode;
    @ApiModelProperty("一次性补正率")
    private String ycxbz;
    @ApiModelProperty("补正率")
    private String bz;
    @ApiModelProperty("办结率")
    private String bj;
    @ApiModelProperty("受理率")
    private String sl;
    @ApiModelProperty("满意度")
    private String myd;
    @ApiModelProperty("本级标识：0-缺省，1-本级")
    private String local;
    @ApiModelProperty("地区id")
    private String areaId;
    public String getYcxbz() {
        return ycxbz;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getBj() {
        return bj;
    }

    public void setBj(String bj) {
        this.bj = bj;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getMyd() {
        return myd;
    }

    public void setMyd(String myd) {
        this.myd = myd;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public void setYcxbz(String ycxbz) {
        this.ycxbz = ycxbz;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }
}
