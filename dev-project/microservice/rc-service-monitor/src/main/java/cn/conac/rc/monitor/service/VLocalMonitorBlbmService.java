package cn.conac.rc.monitor.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.monitor.entity.VLocalMonitorBlbm;
import cn.conac.rc.monitor.repository.VLocalMonitorBlbmRepository;

@Service
public class VLocalMonitorBlbmService extends
		GenericService<VLocalMonitorBlbm, String> {
	@Autowired
	private VLocalMonitorBlbmRepository vLocalMonitorBlbmRepository;

	public List<VLocalMonitorBlbm> getCountsBroupByBlbm(String startDate,
			String endDate, String areaCode, String group, String scope,
			String bjzt) {
		List<VLocalMonitorBlbm> list = null;
		if ("1".equals(scope)) {
			// 本级查询

			if (StringUtils.isNotBlank(group) && "2".equals(group)) {
				// 按办理事项分组查询
				if ("1".equals(bjzt)) {
					list = vLocalMonitorBlbmRepository
							.getSjtjBjListGroupBySxbm(areaCode, startDate,
									endDate);
				} else {
					list = vLocalMonitorBlbmRepository.getSjtjListGroupBySxbm(
							areaCode, startDate, endDate);
				}

			} else {
				// 按办理部门分组查询
				if ("1".equals(bjzt)) {
					list = vLocalMonitorBlbmRepository
							.getSjtjBjListGroupByBlbm(areaCode, startDate,
									endDate);
				} else {
					list = vLocalMonitorBlbmRepository.getSjtjListGroupByBlbm(
							areaCode, startDate, endDate);
				}
			}
		} else {
			//全局的时候，省份查询前两位相同，市查询前4位相同
			if("0000".equals(StringUtils.substring(areaCode, 2))){
				areaCode = StringUtils.substring(areaCode, 0, 2)+"%";
			}else if("00".equals(StringUtils.substring(areaCode, 4))){
				areaCode = StringUtils.substring(areaCode, 0, 4)+"%";
			}
			if (StringUtils.isNotBlank(group) && "2".equals(group)) {
				// 按办理事项分组查询
				if ("1".equals(bjzt)) {
					list = vLocalMonitorBlbmRepository
							.getSjtjBjListGroupBySxbm(areaCode, startDate,
									endDate);
				} else {
					list = vLocalMonitorBlbmRepository.getSjtjListGroupBySxbm(
							areaCode, startDate, endDate);
				}

			} else {
				// 按办理部门分组查询
				if ("1".equals(bjzt)) {
					list = vLocalMonitorBlbmRepository
							.getSjtjBjListGroupByBlbm(areaCode, startDate,
									endDate);
				} else {
					list = vLocalMonitorBlbmRepository.getQuanjuSjtjListGroupByBlbm(
							areaCode, startDate, endDate);
				}
			}
		}
		return list;
	}
}
