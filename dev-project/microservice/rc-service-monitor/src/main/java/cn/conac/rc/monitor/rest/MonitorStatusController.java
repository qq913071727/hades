package cn.conac.rc.monitor.rest;

import cn.conac.rc.framework.utils.DateUtils;
import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.monitor.entity.LocalStatus;
import cn.conac.rc.monitor.entity.VLocalMonitorDayStage1;
import cn.conac.rc.monitor.service.VLocalMonitorDayStageService;
import cn.conac.rc.monitor.vo.StatisticsVO;

import com.alibaba.fastjson.JSON;

import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Date;
import java.util.List;

/**
 * 功能: 监测相关controller
 *
 * @author haocm
 */
@RestController
public class MonitorStatusController
{
    @Autowired
    VLocalMonitorDayStageService service;

    @ApiOperation(value = "查询统计基础数据", httpMethod = "POST", response = LocalStatus.class, notes = "事项总数")
    @RequestMapping(value = "/monitor/statistics/month", method = RequestMethod.POST)
    public ResultPojo getStatusCounts(HttpServletRequest request,
            HttpServletResponse response, @RequestBody StatisticsVO localStatus)
    {
        Date startDate = localStatus.getStartDate();
        Date endDate = localStatus.getEndDate();
        String startDateStr = DateUtils.formatDate(startDate, "yyyy-MM-dd");
        String endDateStr = DateUtils.formatDate(endDate, "yyyy-MM-dd");
        List<VLocalMonitorDayStage1> list =
                service.getCountsByMonth(localStatus.getDepartment(), localStatus.getDfsxbm(),startDateStr, endDateStr,
                        localStatus.getAreaCode(),localStatus.getGroup());
        System.out.println(JSON.toJSONString(list));
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                list);
    }

    @ApiOperation(value = "查询统计基础数据", httpMethod = "POST", response = LocalStatus.class, notes = "事项总数")
    @RequestMapping(value = "/monitor/statistics/quarter", method = RequestMethod.POST)
    public ResultPojo getStatusCountsQuarter(HttpServletRequest request,
            HttpServletResponse response, @RequestBody StatisticsVO localStatus)
    {
        Date startDate = localStatus.getStartDate();
        Date endDate = localStatus.getEndDate();
        String startDateStr = DateUtils.formatDate(startDate, "yyyy-MM-dd");
        String endDateStr = DateUtils.formatDate(endDate, "yyyy-MM-dd");
        List<VLocalMonitorDayStage1> list =
                service.getCountsByQuarter(localStatus.getDepartment(), localStatus.getDfsxbm(), startDateStr, endDateStr,
                        localStatus.getAreaCode(),localStatus.getGroup());
        System.out.println(JSON.toJSONString(list));
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                list);
    }

    @ApiOperation(value = "查询统计基础数据", httpMethod = "POST", response = LocalStatus.class, notes = "事项总数")
    @RequestMapping(value = "/monitor/statistics/year", method = RequestMethod.POST)
    public ResultPojo getStatusCountsYear(HttpServletRequest request,
            HttpServletResponse response, @RequestBody StatisticsVO localStatus)
    {
        //开始时间
        Date startDate = localStatus.getStartDate();
        Date endDate = localStatus.getEndDate();
        String startDateStr = DateUtils.formatDate(startDate, "yyyy-MM-dd");
        String endDateStr = DateUtils.formatDate(endDate, "yyyy-MM-dd");
        List<VLocalMonitorDayStage1> list =
                service.getCountsByYear(localStatus.getDepartment(),localStatus.getDfsxbm(), startDateStr, endDateStr,
                        localStatus.getAreaCode(),localStatus.getGroup());
        System.out.println(JSON.toJSONString(list));
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                list);
    }

    @ApiOperation(value = "查询统计基础数据", httpMethod = "POST", response = LocalStatus.class, notes = "事项总数")
    @RequestMapping(value = "/monitor/statistics/blbm", method = RequestMethod.POST)
    public ResultPojo getStatusCountsBlbm(HttpServletRequest request,
            HttpServletResponse response, @RequestBody StatisticsVO localStatus)
    {
        //开始时间
        Date startDate = localStatus.getStartDate();
        Date endDate = localStatus.getEndDate();
        String startDateStr = DateUtils.formatDate(startDate, "yyyy-MM-dd");
        String endDateStr = DateUtils.formatDate(endDate, "yyyy-MM-dd");
        List<VLocalMonitorDayStage1> list =
                service.getCountsByBlbm(localStatus.getDepartment(), startDateStr, endDateStr,
                        localStatus.getAreaCode());
        System.out.println(JSON.toJSONString(list));
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                list);
    }

    @ApiOperation(value = "查询统计基础数据", httpMethod = "POST", response = LocalStatus.class, notes = "事项总数")
    @RequestMapping(value = "/monitor/statistics/blbmsx", method = RequestMethod.POST)
    public ResultPojo getStatusCountsBlbmSx(HttpServletRequest request,
            HttpServletResponse response, @RequestBody StatisticsVO localStatus)
    {
        //开始时间
        Date startDate = localStatus.getStartDate();
        Date endDate = localStatus.getEndDate();
        String startDateStr = DateUtils.formatDate(startDate, "yyyy-MM-dd");
        String endDateStr = DateUtils.formatDate(endDate, "yyyy-MM-dd");
        List<VLocalMonitorDayStage1> list =
                service.getCountsByBlbmSx(localStatus.getDepartment(), startDateStr, endDateStr,
                        localStatus.getAreaCode(),localStatus.getGroup());
        System.out.println("==========getStatusCountsBlbmSx==========");
        System.out.println(JSON.toJSONString(list));
        return new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, list);
    }
}
