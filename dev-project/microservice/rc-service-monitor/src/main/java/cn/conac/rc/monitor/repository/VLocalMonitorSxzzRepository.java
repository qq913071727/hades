
package cn.conac.rc.monitor.repository;

import cn.conac.rc.monitor.entity.VLocalMonitorSxzz;
import org.springframework.stereotype.Repository;
import cn.conac.rc.framework.repository.GenericDao;


@Repository
public interface VLocalMonitorSxzzRepository extends GenericDao<VLocalMonitorSxzz, String> {
}
