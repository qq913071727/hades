package cn.conac.rc.monitor.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.rc.framework.entity.MonitorBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

;


@ApiModel
@Entity
@Table(name = "V_LOCAL_MONITOR_DAY_STAGE1")
public class VLocalMonitorDayStage1 extends MonitorBaseEntity implements Serializable {
    @ApiModelProperty("统计时间")
    private String tjDate;
    @ApiModelProperty("地方事项编码")
    private String dfsxbm;
    @ApiModelProperty("办理部门")
    private String blbm;
    @ApiModelProperty("事项名称")
    private String sxmc;
    @ApiModelProperty("行政区划编码")
    private String xzqhbm;
    @ApiModelProperty("申请数")
    private Integer sqCount;
    @ApiModelProperty("受理数")
    private Integer slCount;
    @ApiModelProperty("不受理数")
    private Integer bslCount;
    @ApiModelProperty("办结数")
    private Integer bjCount;
    @ApiModelProperty("许可办结数")
    private Integer xkbjCount;
    @ApiModelProperty("不予许可办结数")
    private Integer byxkbjCount;
    @ApiModelProperty("转报办结数")
    private Integer zbbjCount;
    @ApiModelProperty("终止办结数")
    private Integer zzbjCount;
    @ApiModelProperty("个人办结数")
    private Integer grbjCount;
    @ApiModelProperty("法人办结数")
    private Integer frbjCount;
    @ApiModelProperty("一次性补正数")
    private Integer ycxbzCount;
    @ApiModelProperty("办结超时数")
    private Integer bjcsCount;
    @ApiModelProperty("办结未超时数")
    private Integer wbjcsCount;
    @ApiModelProperty("窗口申请数")
    private Integer cksqCount;
    @ApiModelProperty("网上申请数")
    private Integer wssqCount;
    @ApiModelProperty("特别程序超时数")
    private Integer tbcxcsCount;
    @ApiModelProperty("特别程序申请数")
    private Integer tbcxsqCount;
    @ApiModelProperty("补正数")
    private Integer bzsCount;
    @ApiModelProperty("特别程序超时次数")
    private Integer tbcxcscsCount;
    @ApiModelProperty("办结用时天数")
    private Integer bjystsCount;
    @ApiModelProperty("补正天数")
    private Integer bztsCount;
    @ApiModelProperty("特别程序天数")
    private Integer tbcxtsCount;

    public String getTjDate() {
        return tjDate;
    }

    public void setTjDate(String tjDate) {
        this.tjDate = tjDate;
    }

    public String getDfsxbm() {
        return dfsxbm;
    }

    public void setDfsxbm(String dfsxbm) {
        this.dfsxbm = dfsxbm;
    }

    public String getBlbm() {
        return blbm;
    }

    public void setBlbm(String blbm) {
        this.blbm = blbm;
    }

    public String getXzqhbm() {
        return xzqhbm;
    }

    public void setXzqhbm(String xzqhbm) {
        this.xzqhbm = xzqhbm;
    }

    public Integer getSqCount() {
        return sqCount;
    }

    public void setSqCount(Integer sqCount) {
        this.sqCount = sqCount;
    }

    public Integer getSlCount() {
        return slCount;
    }

    public void setSlCount(Integer slCount) {
        this.slCount = slCount;
    }

    public Integer getBslCount() {
        return bslCount;
    }

    public void setBslCount(Integer bslCount) {
        this.bslCount = bslCount;
    }

    public Integer getBjCount() {
        return bjCount;
    }

    public void setBjCount(Integer bjCount) {
        this.bjCount = bjCount;
    }

    public Integer getXkbjCount() {
        return xkbjCount;
    }

    public void setXkbjCount(Integer xkbjCount) {
        this.xkbjCount = xkbjCount;
    }

    public Integer getByxkbjCount() {
        return byxkbjCount;
    }

    public void setByxkbjCount(Integer byxkbjCount) {
        this.byxkbjCount = byxkbjCount;
    }

    public Integer getZbbjCount() {
        return zbbjCount;
    }

    public void setZbbjCount(Integer zbbjCount) {
        this.zbbjCount = zbbjCount;
    }

    public Integer getZzbjCount() {
        return zzbjCount;
    }

    public void setZzbjCount(Integer zzbjCount) {
        this.zzbjCount = zzbjCount;
    }

    public Integer getGrbjCount() {
        return grbjCount;
    }

    public void setGrbjCount(Integer grbjCount) {
        this.grbjCount = grbjCount;
    }

    public Integer getFrbjCount() {
        return frbjCount;
    }

    public Integer getWbjcsCount()
    {
        return wbjcsCount;
    }

    public void setWbjcsCount(Integer wbjcsCount)
    {
        this.wbjcsCount = wbjcsCount;
    }

    public void setFrbjCount(Integer frbjCount) {
        this.frbjCount = frbjCount;
    }

    public Integer getYcxbzCount() {
        return ycxbzCount;
    }

    public void setYcxbzCount(Integer ycxbzCount) {
        this.ycxbzCount = ycxbzCount;
    }

    public Integer getBjcsCount() {
        return bjcsCount;
    }

    public void setBjcsCount(Integer bjcsCount) {
        this.bjcsCount = bjcsCount;
    }

    public Integer getCksqCount() {
        return cksqCount;
    }

    public void setCksqCount(Integer cksqCount) {
        this.cksqCount = cksqCount;
    }

    public Integer getWssqCount() {
        return wssqCount;
    }

    public void setWssqCount(Integer wssqCount) {
        this.wssqCount = wssqCount;
    }

    public Integer getTbcxcsCount() {
        return tbcxcsCount;
    }

    public void setTbcxcsCount(Integer tbcxcsCount) {
        this.tbcxcsCount = tbcxcsCount;
    }

    public Integer getTbcxsqCount() {
        return tbcxsqCount;
    }

    public void setTbcxsqCount(Integer tbcxsqCount) {
        this.tbcxsqCount = tbcxsqCount;
    }

    public Integer getBzsCount() {
        return bzsCount;
    }

    public void setBzsCount(Integer bzsCount) {
        this.bzsCount = bzsCount;
    }

    public Integer getTbcxcscsCount() {
        return tbcxcscsCount;
    }

    public void setTbcxcscsCount(Integer tbcxcscsCount) {
        this.tbcxcscsCount = tbcxcscsCount;
    }

    public Integer getBjystsCount() {
        return bjystsCount;
    }

    public void setBjystsCount(Integer bjystsCount) {
        this.bjystsCount = bjystsCount;
    }

    public Integer getBztsCount() {
        return bztsCount;
    }

    public void setBztsCount(Integer bztsCount) {
        this.bztsCount = bztsCount;
    }

    public Integer getTbcxtsCount() {
        return tbcxtsCount;
    }

    public void setTbcxtsCount(Integer tbcxtsCount) {
        this.tbcxtsCount = tbcxtsCount;
    }

    public String getSxmc()
    {
        return sxmc;
    }

    public void setSxmc(String sxmc)
    {
        this.sxmc = sxmc;
    }
}
