package cn.conac.rc.monitor.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.rc.framework.entity.MonitorBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by haocm on 2016-9-20.
 */
@ApiModel
@Entity
@Table(name = "V_LOCAL_SX")
public class VLocalSx extends MonitorBaseEntity implements Serializable
{
    @ApiModelProperty("部门名称")
    private String blbm;

    @ApiModelProperty("行政区划")
    private String xzqhbm;

    @ApiModelProperty("事项总数")
    private Integer sxzs;

    public String getBlbm()
    {
        return blbm;
    }

    public void setBlbm(String blbm)
    {
        this.blbm = blbm;
    }

    public String getXzqhbm()
    {
        return xzqhbm;
    }

    public void setXzqhbm(String xzqhbm)
    {
        this.xzqhbm = xzqhbm;
    }

    public Integer getSxzs()
    {
        return sxzs;
    }

    public void setSxzs(Integer sxzs)
    {
        this.sxzs = sxzs;
    }
}
