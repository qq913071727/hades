package cn.conac.rc.monitor.service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.monitor.entity.LocalStatus;
import cn.conac.rc.monitor.entity.VLocalMonitorSxzz;
import cn.conac.rc.monitor.repository.VLocalMonitorSxzzRepository;
import cn.conac.rc.monitor.vo.StatisticsVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import cn.conac.rc.framework.service.GenericService;

import java.util.List;


@Service
public class VLocalMonitorSxzzService extends GenericService<VLocalMonitorSxzz, String> {
    @Autowired
    private VLocalMonitorSxzzRepository vLocalMonitorSxzzRepository;

    /**
     * 根据条件查询
     *
     * @param param
     * @return
     */
    public List<VLocalMonitorSxzz> getStatusByCriteria(VLocalMonitorSxzz param) {
        Criteria<VLocalMonitorSxzz> criteria = this.createCriteria(param);
        // 创建排序（权重）
        Sort sort = new Sort(Sort.Direction.ASC, "sortt");
        return vLocalMonitorSxzzRepository.findAll(criteria, sort);
    }

    /**
     * 拼接查询条件
     *
     * @param param 参数对象
     * @return Criteria<LocalStatus> JPA封装类
     * @author haocm
     */
    private Criteria<VLocalMonitorSxzz> createCriteria(VLocalMonitorSxzz param) {
        Criteria<VLocalMonitorSxzz> dc = new Criteria<VLocalMonitorSxzz>();
        if (StringUtils.isNotBlank(param.getDfsxbm())) {
            dc.add(Restrictions.eq("dfsxbm", param.getDfsxbm(), true));
        }
        if (StringUtils.isNotBlank(param.getYxtywlsh())) {
            dc.add(Restrictions.eq("yxtywlsh", param.getYxtywlsh(), true));
        }
        return dc;
    }
}
