package cn.conac.rc.monitor.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.monitor.service.EslService;
import cn.conac.rc.monitor.vo.StatisticsVO;

/**
 * Created by haocm on 2016-11-15.
 */
@RestController
@RequestMapping("es")
public class EslController
{
    @Autowired
    private EslService service;

    @RequestMapping(value = "status",method = RequestMethod.POST)
    public Object test(@RequestBody StatisticsVO localStatus){
        return service.findCliente(localStatus.getQueryString());
    }
}
