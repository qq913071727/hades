/**
 * Copyright &copy; 2012-2013 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package cn.conac.rc.framework.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModelProperty;

/**
 * 数据Entity类
 * @param <T>
 * @author zhangfz
 * @version 1.0
 */
@MappedSuperclass
//@Where(clause="is_active=1")
public abstract class DataEntity<T> extends ReadOnlyDataEntity<T> implements Serializable {

     static final long serialVersionUID = 1L;

    @ApiModelProperty("更新者")
    protected String updateUser;
    
    @ApiModelProperty("更新日期")
    protected Date updateDate;
    
    public DataEntity() {
        super();
    }
    
    @ApiModelProperty("删除标志0：正常 1：删除")
    protected DeleteMark deleteMark;
	
	/**
	 * 是否可用枚举定义
	 */
	public enum DeleteMark{
		//可用
		AVAILABLE,
		//不可用
		UNAVAILABLE;
	}

//    @Override
//    @PrePersist
//    public void prePersist() {
//        super.prePersist();
//        this.createDate = new Date();
//        this.updateDate = this.createDate;
//    }

    /**
     * preUpdate.
     */
    @PreUpdate
    public void preUpdate() {
        this.updateDate = new Date();
    }

    @Length(min = 0, max = 255)
    @Column(name = "REMARKS")
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE", updatable = false)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name = "UPDATE_DATE")
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Column(name = "UPDATE_USER")
	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Column(name = "DELETE_MARK",nullable = false,length = 1)
    @Enumerated(EnumType.ORDINAL)
    @NotNull
	public DeleteMark getDeleteMark() {
		return deleteMark;
	}

	public void setDeleteMark(DeleteMark deleteMark) {
		this.deleteMark = deleteMark;
	}
}
