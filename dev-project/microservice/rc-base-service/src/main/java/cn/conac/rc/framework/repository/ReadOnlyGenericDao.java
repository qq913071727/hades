package cn.conac.rc.framework.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

/**
 * 通用CRUD Dao接口
 */
@NoRepositoryBean
public interface ReadOnlyGenericDao<T, ID extends Serializable> extends Repository<T, ID>, JpaSpecificationExecutor<T> {

    <S extends T> S save(S entity);

    T findOne(ID primaryKey);

    Iterable<T> findAll();

    Long count();

    // void delete(T entity); 

    boolean exists(ID primaryKey);
}
