OLD_BUILD_ID=$BUILD_ID
echo $OLD_BUILD_ID
BUILD_ID=dontKillMe

export JAVA_HOME=/usr/java/jdk1.7.0_80
export MAVEN_HOME=/home/soft/apache-maven-3.3.3
export PATH=$PATH:$MAVEN_HOME/bin:$JAVA_HOME/bin:


cd $WORKSPACE

mvn clean install


cd ./rc-server-eureka
nohup mvn spring-boot:run &


cd ../rc-service-common
nohup mvn spring-boot:run &

cd ../rc-service-demo 
nohup mvn spring-boot:run &

cd ../rc-gateway-web
nohup mvn spring-boot:run &


#改回原来的BUILD_ID值
#BUILD_ID=$OLD_BUILD_ID
#echo $BUILD_ID