package cn.conac.rc.gateway.modules.common.vo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cn.conac.rc.framework.utils.StringUtils;
import cn.conac.rc.gateway.base.BaseVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class UserVo extends BaseVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("登录名")
    private String loginName;

    @ApiModelProperty("真实名")
    private String realName;

    @ApiModelProperty("姓名")
    private String fullName;

    @ApiModelProperty("机构名称")
    private String orgName;

    @ApiModelProperty("si_code")
    private String siCode;

    @JsonIgnore
    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("用户类型")
    private int userType;

    @ApiModelProperty("电话号码")
    private String mobile;

    @ApiModelProperty("电子邮箱")
    private String email;

    @ApiModelProperty("住址")
    private String address;

    private Integer loginCount;

    @ApiModelProperty("区分机构域名和编制域名（1：机构域名  0：编制域名）")
    private String isAdmin;

    @ApiModelProperty("用户角色清单")
    private List<RoleVo> roleList;

    private String imgId;

    private String areaId;

    private String orgId;

    private Integer isDisabled;

    @ApiModelProperty("昵称")
    private String nickName;
    
	@ApiModelProperty("imgUrl")
	private String imgUrl;
	
	@ApiModelProperty("性别 (0：女 1：男)")
	private Integer gender;

	@ApiModelProperty("职务岗位")
	private String post;

	@ApiModelProperty("编制类型")
	private String formType;

	@ApiModelProperty("编制状态")
	private String formStatus;

	@ApiModelProperty("编制编码")
	private String formCode;

	@ApiModelProperty("监督电话")
	private String monitPhone;
    
	@ApiModelProperty("orgDomainName")
	private String orgDomainName;

	@ApiModelProperty("deptName")
	private String deptName;
	
	@ApiModelProperty("idNumber")
	private String idNumber;
	
	@ApiModelProperty("取得编制域名二维码的Key")
	private String qrCodeKey;
	
    public String getLoginName() {
        return loginName;
    }

    public String getRealName() {
        return realName;
    }

    public String getPassword() {
        return password;
    }


    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<RoleVo> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<RoleVo> roleList) {
        this.roleList = roleList;
    }

    public String getImgId() {
        return imgId;
    }

    public String getAreaId() {
        return areaId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setImgId(String imgId) {
        this.imgId = imgId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * 密码盐.
     * @return
     */
    public String getCredentialsSalt() {
        return this.getLoginName() + "8d78869f470951332959580424d4bf4f";

    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getIsAdmin()
    {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin)
    {
        this.isAdmin = isAdmin;
    }

    public String getOrgName()
    {
        return orgName;
    }

    public void setOrgName(String orgName)
    {
        this.orgName = orgName;
    }

    public Integer getLoginCount()
    {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount)
    {
        this.loginCount = loginCount;
    }

    public Integer getIsDisabled()
    {
        return isDisabled;
    }

    public void setIsDisabled(Integer isDisabled)
    {
        this.isDisabled = isDisabled;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSiCode() {
        return siCode;
    }

    public void setSiCode(String siCode) {
        this.siCode = siCode;
    }

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}

	public String getFormStatus() {
		return formStatus;
	}

	public void setFormStatus(String formStatus) {
		this.formStatus = formStatus;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getMonitPhone() {
		return monitPhone;
	}

	public void setMonitPhone(String monitPhone) {
		this.monitPhone = monitPhone;
	}

	public String getOrgDomainName() {
		return orgDomainName;
	}

	public void setOrgDomainName(String orgDomainName) {
		this.orgDomainName = orgDomainName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getQrCodeKey() {
		return qrCodeKey;
	}

	public void setQrCodeKey(String qrCodeKey) {
		this.qrCodeKey = qrCodeKey;
	}
}
