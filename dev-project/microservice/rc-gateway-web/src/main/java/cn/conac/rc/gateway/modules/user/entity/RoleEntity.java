package cn.conac.rc.gateway.modules.user.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * RoleEntity类
 *
 * @author beanCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
public class RoleEntity implements Serializable {

	private static final long serialVersionUID = -4464624226845256606L;
	
    public static final String ROLE_ADMIN_SYS = "1"; //CONAC系统管理员
    public static final String ROLE_WBJ = "104";// 委办局管理员
    public static final String ROLE_BB = "106";// 编办电子政务中心人员（原编办业务员）
    public static final String ROLE_FZB = "105";// 法制办角色
    public static final String ROLE_BB_GLY = "107";// 编办管理员
    public static final String ROLE_BBBZGL = "108";// 编办编制管理工作人员
    public static final String ROLE_BBTGSG = "109";// 编办体改/审改工作人员
    public static final String ROLE_WBJPT = "110";// 委办局普通用户
    public static final String ROLE_BBPT = "111";// 编办普通用户
    public static final String ROLE_CONAC_QDGLY = "1000040";// CONAC渠道管理员
    
	@ApiModelProperty("roleId")
	private Integer roleId;

	@ApiModelProperty("siteId")
	private Integer siteId;

	@ApiModelProperty("权限名字")
	private String roleName;

	@ApiModelProperty("顺序号")
	private Integer priority;

	@ApiModelProperty("拥有所用权限")
	private String isSuper;

	@ApiModelProperty("角色类型")
	private String roleType;
	
	@ApiModelProperty("注册用户ID")
	private Integer registerUserId;

	@ApiModelProperty("注册时间")
	private Date registerDate;

	@ApiModelProperty("更新用户ID")
	private Integer updateUserId;

	@ApiModelProperty("更新时间")
	private Date updateDate;
	
	@ApiModelProperty("角色对应的权限URI")
	private List<RolePermissionEntity>  RolePermissionList;

	public void setRoleId(Integer roleId){
		this.roleId=roleId;
	}

	public Integer getRoleId(){
		return roleId;
	}

	public void setSiteId(Integer siteId){
		this.siteId=siteId;
	}

	public Integer getSiteId(){
		return siteId;
	}

	public void setRoleName(String roleName){
		this.roleName=roleName;
	}

	public String getRoleName(){
		return roleName;
	}

	public void setPriority(Integer priority){
		this.priority=priority;
	}

	public Integer getPriority(){
		return priority;
	}

	public void setIsSuper(String isSuper){
		this.isSuper=isSuper;
	}

	public String getIsSuper(){
		return isSuper;
	}

	public void setRoleType(String roleType){
		this.roleType=roleType;
	}

	public String getRoleType(){
		return roleType;
	}
	
	public Integer getRegisterUserId() {
		return registerUserId;
	}

	public void setRegisterUserId(Integer registerUserId) {
		this.registerUserId = registerUserId;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	public List<RolePermissionEntity> getRolePermissionList() {
		return RolePermissionList;
	}

	public void setRolePermissionList(List<RolePermissionEntity> rolePermissionList) {
		RolePermissionList = rolePermissionList;
	}

}
