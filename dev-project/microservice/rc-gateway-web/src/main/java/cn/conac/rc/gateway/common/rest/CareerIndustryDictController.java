package cn.conac.rc.gateway.common.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.common.service.CareerIndustryDictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags="字典接口", description="共通")
@RequestMapping("comm/")
public class CareerIndustryDictController {
	
    @Autowired
    CareerIndustryDictService  careerIndustryDictService;
    
    @ApiOperation(value = "获得行业分类树结构信息",  notes = "获得行业分类树结构信息")
    @RequestMapping(value = "dic/industry", method = RequestMethod.GET)
    public JSONObject getMapTree(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return careerIndustryDictService.getMapTree();
    }
}
