package cn.conac.rc.gateway.modules.monitor.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSON;

import cn.conac.rc.framework.utils.DateUtils;
import cn.conac.rc.gateway.modules.monitor.vo.LocalStatus;
import cn.conac.rc.gateway.modules.monitor.vo.StatisticsVO;
import cn.conac.rc.gateway.modules.monitor.vo.VLocalMonitorDay;

/**
 * 逻辑处理类
 *
 * @author haocm
 */
public class MonitorLogic
{
    /**
     * 组织页面echar展示数据
     */
    public static List<Map.Entry<String,VLocalMonitorDay>>  createCountChart(String dateFlag,String department, List echarDatalist,
            List echarBlbmlist, Map<String, VLocalMonitorDay> map,
            Map<String, VLocalMonitorDay> bmmap)
    {
        for (Object object : echarBlbmlist) {
            VLocalMonitorDay tmp = JSON.toJavaObject(
                    JSON.parseObject(JSON.toJSONString(object)),
                    VLocalMonitorDay.class);

            // 按部门分组 办结数统计

            String blbm = "";
            if(StringUtils.isBlank(department) || "0".equals(department)){
                blbm = tmp.getBlbm();
            }else{
                blbm = tmp.getSxmc();
            }
            if (StringUtils.isBlank(blbm)) {
                continue;
            }
            bmmap.put(blbm, tmp);
        }
        List<Map.Entry<String,VLocalMonitorDay>> list = new ArrayList<Map.Entry<String,VLocalMonitorDay>>(bmmap.entrySet());
        //然后通过比较器来实现排序
        Collections.sort(list,new Comparator<Map.Entry<String,VLocalMonitorDay>>() {
            //升序排序
            public int compare(Map.Entry<String, VLocalMonitorDay> o1,
                    Map.Entry<String, VLocalMonitorDay> o2) {
                return o1.getValue().getBjCount().compareTo(o2.getValue().getBjCount());
            }
        });
//        Map<String, VLocalMonitorDay> retbmmap = new HashedMap();
//        for(Map.Entry<String,VLocalMonitorDay> mapping:list){
//            retbmmap.put(mapping.getKey(),mapping.getValue());
//            System.out.println(mapping.getKey()+":"+mapping.getValue().getBjCount());
//        }
        for (Object object : echarDatalist) {
            VLocalMonitorDay tmp = JSON.toJavaObject(
                    JSON.parseObject(JSON.toJSONString(object)),
                    VLocalMonitorDay.class);
            // 按部门分组 办结数统计
            String tjDate = tmp.getTjDate();
            if ("2".equals(dateFlag)) {
                if (!"#".equals(tmp.getBlbm())) {
                    if ("1".equals(tmp.getBlbm())) {
                        tjDate = tjDate + "-" + tmp.getBlbm();
                    }
                    if ("2".equals(tmp.getBlbm())) {
                        tjDate = tjDate + "-" + tmp.getBlbm();
                    }
                    if ("3".equals(tmp.getBlbm())) {
                        tjDate = tjDate + "-" + tmp.getBlbm();
                    }
                    if ("4".equals(tmp.getBlbm())) {
                        tjDate = tjDate + "-" + tmp.getBlbm();
                    }
                }
            }
            map.put(tjDate, tmp);
        }

        return list;
    }

    /**
     * 组织页面echar展示数据
     */
    public static void createCountChart(String dateFlag, List echarDatalist,
            Map<String, VLocalMonitorDay> bmmap)
    {
        for (Object object : echarDatalist) {
            VLocalMonitorDay tmp = JSON.toJavaObject(
                    JSON.parseObject(JSON.toJSONString(object)),
                    VLocalMonitorDay.class);
            // 按部门分组 办结数统计
            String tjDate = tmp.getTjDate();
            if (!"#".equals(tmp.getBlbm())) {
                if ("1".equals(tmp.getBlbm())) {
                    tjDate = tjDate + "-3";
                }
                if ("2".equals(tmp.getBlbm())) {
                    tjDate = tjDate + "-6";
                }
                if ("3".equals(tmp.getBlbm())) {
                    tjDate = tjDate + "-9";
                }
                if ("4".equals(tmp.getBlbm())) {
                    tjDate = tjDate + "-12";
                }
            }
            bmmap.put(tjDate, tmp);
        }
    }

    /**
     * 组织页面echar展示数据
     */
    public static void createCountChart(String dateFlag, List echarDatalist,
            Map<String, StatisticsVO> map, Map<String, StatisticsVO> bmmap)
    {
        for (Object object : echarDatalist) {
            LocalStatus tmp = JSON.toJavaObject(
                    JSON.parseObject(JSON.toJSONString(object)),
                    LocalStatus.class);
            // 按部门分组 办结数统计
            String blbm = tmp.getBlbm();
            StatisticsVO blbmStatistics = bmmap.get(blbm);
            if (blbmStatistics == null) {
                blbmStatistics = new StatisticsVO();
                blbmStatistics.setFrBanjieCount(0);
                blbmStatistics.setGrBanjieCount(0);
                if (StringUtils.isNotBlank(tmp.getBjzt())
                        && LocalStatus.cltjfs1.equals(tmp.getCltjfs()))
                {
                    // 个人
                    blbmStatistics.setGrBanjieCount(1);
                } else if (StringUtils.isNotBlank(tmp.getBjzt())
                        && LocalStatus.cltjfs2.equals(tmp.getCltjfs()))
                {
                    // 法人
                    blbmStatistics.setFrBanjieCount(1);
                }
                bmmap.put(blbm, blbmStatistics);
            } else {
                if (StringUtils.isNotBlank(tmp.getBjzt())
                        && LocalStatus.cltjfs1.equals(tmp.getCltjfs()))
                {

                    blbmStatistics.setGrBanjieCount(blbmStatistics
                            .getGrBanjieCount() + 1);
                } else if (StringUtils.isNotBlank(tmp.getBjzt())
                        && LocalStatus.cltjfs2.equals(tmp.getCltjfs()))
                {

                    blbmStatistics.setFrBanjieCount(blbmStatistics
                            .getFrBanjieCount() + 1);
                }
            }

            // 按日期分组
            String sqDate;
            if ("1".equals(dateFlag)) {
                sqDate = DateUtils.getFormatDateTime(tmp.getSqDate(),
                        DateUtils.FORMAT_YYYYMM);
            } else if ("2".equals(dateFlag)) {
                sqDate = createSqDate(DateUtils.getFormatDateTime(
                        tmp.getSqDate(), DateUtils.FORMAT_YYYYMM));
            } else {
                sqDate = DateUtils.getFormatDateTime(tmp.getSqDate(),
                        DateUtils.FORMAT_YYYY);
            }
            StatisticsVO statistics = map.get(sqDate);
            // 10. 特别程序率 = 特别程序数 / 受理数 *100%
            // 11. 特别程序超时率 = 特别程序超时次数 / 特别程序次数 *100%

            if (statistics == null) {
                statistics = new StatisticsVO();
                statistics.setApplyCount(1);
                statistics.setAcceptCount(0);
                statistics.setNoacceptCount(0);
                statistics.setBanjieCount(0);
                statistics.setCksqCount(0);
                statistics.setWssqCount(0);
                statistics.setTscxCount(0);
                statistics.setTscxcsCount(0);
                statistics.setTimeoutBanjieCount(0);
                statistics.setXkBanjieCount(0);
                statistics.setYcxbzCount(0);
                statistics.setBzCount(0);
                if (StringUtils.isNotBlank(tmp.getSlzt())
                        && LocalStatus.slzt01.equals(tmp.getSlzt()))
                {
                    // 受理+1
                    statistics.setAcceptCount(1);
                } else {
                    statistics.setNoacceptCount(1);
                }
                if (StringUtils.isNotBlank(tmp.getBjzt())) {
                    // 办结+1
                    statistics.setBanjieCount(1);
                }
                if (StringUtils.isNotBlank(tmp.getBjzt())
                        && LocalStatus.bjzt01.equals(tmp.getBjzt()))
                {
                    // 办结+1
                    statistics.setXkBanjieCount(1);
                }
                if (LocalStatus.bjcs01.equals(tmp.getBjcsFlag())) {
                    statistics.setTimeoutBanjieCount(1);
                }
                if (StringUtils.isNotBlank(tmp.getCltjfs())) {

                    if (LocalStatus.sqfs00.equals(tmp.getCltjfs())) {
                        // 窗口申请
                        statistics.setCksqCount(1);
                    } else if (LocalStatus.sqfs01.equals(tmp.getCltjfs())) {
                        // 网上申请
                        statistics.setWssqCount(1);
                    }
                }
                if (tmp.getTscxsqDate() != null) {
                    statistics.setTscxCount(1);
                }
                if (StringUtils.isNotBlank(tmp.getTscxcsFlag())
                        && LocalStatus.tscxcs01.equals(tmp.getTscxcsFlag()))
                {
                    statistics.setTscxcsCount(1);
                }
                if (tmp.getBzbqDate() != null && tmp.getBzgzDate() != null) {
                    statistics.setBzCount(1);
                }
                if (StringUtils.isNotBlank(tmp.getYcxbzFlag())
                        && LocalStatus.ycxbz1.equals(tmp.getTscxcsFlag()))
                {
                    statistics.setYcxbzCount(1);
                }
                map.put(sqDate, statistics);
            } else {
                // 申请+1
                statistics.setApplyCount(statistics.getApplyCount() + 1);
                if (StringUtils.isNotBlank(tmp.getSlzt())
                        && LocalStatus.slzt01.equals(tmp.getSlzt()))
                {
                    // 受理+1
                    statistics.setAcceptCount(statistics.getAcceptCount() + 1);
                } else {
                    statistics
                            .setNoacceptCount(statistics.getNoacceptCount() + 1);
                }
                if (StringUtils.isNotBlank(tmp.getBjzt())) {
                    // 办结+1
                    statistics.setBanjieCount(statistics.getBanjieCount() + 1);
                }
                if (StringUtils.isNotBlank(tmp.getBjzt())
                        && LocalStatus.bjzt01.equals(tmp.getBjzt()))
                {
                    // 办结+1
                    statistics
                            .setXkBanjieCount(statistics.getXkBanjieCount() + 1);
                }
                if (StringUtils.isNotBlank(tmp.getCltjfs())) {

                    if (LocalStatus.sqfs00.equals(tmp.getCltjfs())) {
                        // 窗口申请
                        statistics.setCksqCount(statistics.getCksqCount() + 1);
                    } else if (LocalStatus.sqfs01.equals(tmp.getCltjfs())) {
                        statistics.setWssqCount(statistics.getWssqCount() + 1);
                    }
                }
                if (tmp.getTscxsqDate() != null) {
                    statistics.setTscxCount(statistics.getTscxCount() + 1);
                }
                if (StringUtils.isNotBlank(tmp.getTscxcsFlag())
                        && LocalStatus.tscxcs01.equals(tmp.getTscxcsFlag()))
                {
                    statistics.setTscxcsCount(statistics.getTscxcsCount() + 1);
                }
                if (LocalStatus.bjcs01.equals(tmp.getBjcsFlag())) {
                    statistics.setTimeoutBanjieCount(statistics
                            .getTimeoutBanjieCount());
                }
                if (tmp.getBzbqDate() != null && tmp.getBzgzDate() != null) {
                    statistics.setBzCount(statistics.getBzCount());
                }
                if (StringUtils.isNotBlank(tmp.getYcxbzFlag())
                        && LocalStatus.ycxbz1.equals(tmp.getTscxcsFlag()))
                {
                    statistics.setYcxbzCount(statistics.getYcxbzCount());
                }
            }
        }
    }

    public static String createSqDate(String sqDate)
    {
        String yearAndDay[] = sqDate.split("-");
        String retDay = "";
        switch (yearAndDay[1]) {
            case "01":
            case "02":
            case "03": {
                retDay = "03";
                break;
            }
            case "04":
            case "05":
            case "06": {
                retDay = "06";
                break;
            }
            case "07":
            case "08":
            case "09": {
                retDay = "09";
                break;
            }
            case "10":
            case "11":
            case "12": {
                retDay = "12";
                break;
            }
        }
        return yearAndDay[0] + "-" + retDay;
    }
}
