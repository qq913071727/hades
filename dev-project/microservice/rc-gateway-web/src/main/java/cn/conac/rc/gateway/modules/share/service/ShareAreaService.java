package cn.conac.rc.gateway.modules.share.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

@FeignClient("RC-SERVICE-SHARE")
public interface ShareAreaService {

	@RequestMapping(value = "sharedArea/{areaCode}/list", method = RequestMethod.GET)
	public JSONObject findSharedAreaList(@PathVariable("areaCode") String areaCode);
	
}
