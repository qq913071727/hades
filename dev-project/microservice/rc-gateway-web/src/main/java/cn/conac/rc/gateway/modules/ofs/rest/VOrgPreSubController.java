package cn.conac.rc.gateway.modules.ofs.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.RestClientException;
import cn.conac.rc.gateway.modules.ofs.service.VOrgPreSubService;
import cn.conac.rc.gateway.modules.ofs.vo.VOrgPreSubVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="ofs/")
@Api(tags="部门的基本信息", description="三定信息")
public class VOrgPreSubController {

    @Autowired
    VOrgPreSubService vOrgPreSubService;
    
    @ApiOperation(value = "待提交部门信息", httpMethod = "POST", response = JSONObject.class, notes = "待提交部门信息")
    @RequestMapping(value = "bases/page", method = RequestMethod.POST)
    public JSONObject auditListInfo(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam(value = "查询条件对象", required = true) @RequestBody VOrgPreSubVo vOrgPreSubVo)
            throws RestClientException, Exception {
        return vOrgPreSubService.list(vOrgPreSubVo);
    }

}