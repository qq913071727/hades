package cn.conac.rc.gateway.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel
public class CareerIndustryDictEntity {

    @ApiModelProperty("主键id")
    protected String id;

	@ApiModelProperty("pid")
	private String pid;

	@ApiModelProperty("name")
	private String name;

	@ApiModelProperty("open")
	private String open;

	@ApiModelProperty("perm")
	private String perm;

	@ApiModelProperty("leaf")
	private Integer leaf;

	@ApiModelProperty("code")
	private Integer code;

	@ApiModelProperty("orderNum")
	private Integer orderNum;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}

	public String getPerm() {
		return perm;
	}

	public void setPerm(String perm) {
		this.perm = perm;
	}

	public Integer getLeaf() {
		return leaf;
	}

	public void setLeaf(Integer leaf) {
		this.leaf = leaf;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

}
