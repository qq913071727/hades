package cn.conac.rc.gateway.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel
public class Dictionary {

	@ApiModelProperty("主键id")
	private String id;
	@ApiModelProperty("数据字典展示名称")
	private String name;
	@ApiModelProperty("数据字典实际存储值")
	private String value;
	@ApiModelProperty("数据字典值的类型")
	private String type;
	@ApiModelProperty("排序号")
	private Integer sort;
	@ApiModelProperty("父")
	private String parentId;
	@ApiModelProperty("一支父")
	private String parentIds;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getParentIds() {
		return parentIds;
	}
	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
