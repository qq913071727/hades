package cn.conac.rc.gateway.common.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.common.service.DicTreeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * DicTreeController类
 *
 * @author controllerCreator
 * @date 2016-12-08
 * @version 1.0
 */
@RestController
@Api(tags="字典接口", description="共通")
@RequestMapping("comm/")
public class DicTreeController {

	 	@Autowired
	 	DicTreeService  dicTreeService;
	    
	    @ApiOperation(value = "获得基本码树结构信息",  notes = "获得基本码树结构信息")
	    @RequestMapping(value = "dic/bcode", method = RequestMethod.GET)
	    public JSONObject getMapTree(HttpServletRequest request, HttpServletResponse response)
	            throws Exception {
	        return dicTreeService.getMapTree();
	    }
}
