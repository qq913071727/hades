package cn.conac.rc.gateway.modules.ofs.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.RestClientException;
import cn.conac.rc.gateway.modules.ofs.service.VAuditListService;
import cn.conac.rc.gateway.modules.ofs.vo.VAuditListVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="ofs/")
@Api(tags="部门审核信息", description="三定信息")
public class VAuditListController {

    @Autowired
    VAuditListService vAuditListService;

    @ApiOperation(value = "根据部门基本表ID取得审核相关信息", httpMethod = "GET", response = JSONObject.class, notes = "根据部门基本表ID取得审核相关信息")
    @RequestMapping(value = "opinions/{id}/r", method = RequestMethod.GET)
    public JSONObject findDetailInfoById(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "部门基本表id", required = true) @PathVariable("id") String id)
            throws RestClientException, Exception {
        return vAuditListService.findById(id);
    }
    
    @ApiOperation(value = "部门审核信息", httpMethod = "POST", response = JSONObject.class, notes = "根据条件查询部门审核信息")
    @RequestMapping(value = "opinions/page", method = RequestMethod.POST)
    public JSONObject auditListInfo(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam(value = "查询条件对象", required = true) @RequestBody VAuditListVo vAuditListVo)
            throws RestClientException, Exception {
        return vAuditListService.list(vAuditListVo);
    }

}