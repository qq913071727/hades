package cn.conac.rc.gateway.common.service;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.common.entity.AreaEntity;
import cn.conac.rc.gateway.common.vo.AreaVo;
import cn.conac.rc.gateway.common.vo.VAreaInfoVo;
import io.swagger.annotations.ApiParam;

/**
 * 地区服务
 * @author haocm
 */
@FeignClient("RC-SERVICE-COMMON")
@CacheConfig(keyGenerator = "keyGenerator", cacheNames = "area_cache", cacheManager = "redisCache1h")
@Cacheable
public interface AreaService {

    @RequestMapping(method = RequestMethod.GET, value = "/area/pid/{pid}/{lvl}")
    JSONObject getAreaList(@PathVariable(value = "pid") String pid, @PathVariable(value = "lvl") String lvl);

    /**
     * 通过areaId获取地区信息
     * @param id
     * @return 地区信息
     */
    @RequestMapping(method = RequestMethod.GET, value = "/area/{id}")
    JSONObject getAreaInfo(@PathVariable(value = "id") String id);

    /**
     * 通过areaId获取包含上级地区信息
     * @param id
     * @return 地区信息
     */
    @RequestMapping(method = RequestMethod.GET, value = "/area/v/{id}")
    JSONObject getVAreaInfo(@PathVariable(value = "id") String id);

    @RequestMapping(method = RequestMethod.GET, value = "/area/lvl/{lvl}")
    JSONObject getAreaListByLvl(@PathVariable(value = "lvl") Integer lvl);

    @RequestMapping(method = RequestMethod.GET, value = "/area/css/list/{pid}")
    JSONArray getAreaCSSList(@PathVariable(value = "pid") String pid);

    @RequestMapping(method = RequestMethod.GET, value = "/area/list")
    JSONArray getAreaList();

    /**
     * 通过条件获取包含上级地区的地区列表信息
     * @return 地区信息
     */
    @RequestMapping(method = RequestMethod.POST, value = "/area/v/list")
    JSONObject getVAreaList(@RequestBody VAreaInfoVo vo);
    
    /**
     * 通过条件获取包含上级地区的地区列表信息
     * @return 地区信息
     */
    @RequestMapping(method = RequestMethod.POST, value = "/area/para/list")
    JSONObject getAreaListByPara(@RequestBody AreaVo vo);
    
    

    @RequestMapping(method = RequestMethod.GET, value = "/area/list/{pid}")
    JSONArray getAreaListByPid(@PathVariable(value = "pid") String pid);

    /**
     * 地区信息的新增或者更新
     * @param AreaEntity
     * @return JSONObject
     */
    @CacheEvict
    @RequestMapping(method = RequestMethod.POST, value = "/area/")
    JSONObject saveOrUpdate(@ApiParam(value = "保存对象", required = true) @RequestBody AreaEntity areaEntity);
    
    /**
     * 根据区域Code和开通方式开通试点
     * @param areaCode
     * @param openPilotType
     * @return JSONObject
     */
    @CacheEvict
    @RequestMapping(method = RequestMethod.POST, value = "/area/{areaCode}/open/{openPilotType}")
    JSONObject updateTypeByCond(@PathVariable(value = "areaCode") String areaCode,@PathVariable(value = "openPilotType") String openPilotType);
    
}