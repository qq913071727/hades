package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

@FeignClient("RC-SERVICE-OFS")
public interface VDomainInfoService {
	
	@RequestMapping(method = RequestMethod.GET, value = "/domainInfo/{orgName}")
	 public JSONObject findByOrgName(@PathVariable(value = "orgName") String orgName);

}
