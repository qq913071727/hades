package cn.conac.rc.gateway.modules.monitor.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 监控首页model
 *
 * @author haocm
 */
@ApiModel
public class StatisticsVO {


    public StatisticsVO() {
        super();
    }

    public StatisticsVO(String areaCode, String department,
                        String startDate, String endDate) {
        super();
        this.areaCode = areaCode;
        this.department = department;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    private String id;
    @ApiModelProperty("日期标识")
    private String dateFlag;
    @ApiModelProperty("地区code")
    private String areaCode;
    @ApiModelProperty("地区areaId")
    private String areaId;
    private String local;
    @ApiModelProperty("部门")
    private String department;
    private String startDate;
    private String endDate;
    @ApiModelProperty("事项总数")
    private Integer matterTotal;
    @ApiModelProperty("累计申请数")
    private Integer applyCount;
    @ApiModelProperty("累计办结数")
    private String group;
    private String scope;
    private Integer banjieCount;
    private Integer frBanjieCount;
    private Integer grBanjieCount;
    private String bjzt;
    private String bjcs;
    private Integer cksqCount;
    private Integer wssqCount;
    private Integer tscxCount;
    private Integer tscxcsCount;
    private Integer xkBanjieCount;
    private Integer bzCount;
    private Integer ycxbzCount;
    @ApiModelProperty("阶段")
    private String stage;
    @ApiModelProperty("状态")
    private String status;
    @ApiModelProperty("累计受理数")
    private Integer acceptCount;
    @ApiModelProperty("未受理数")
    private Integer noacceptCount;
    private String slzt;
    private String sl;
    private String ycxbz;
    private String bj;
    private String bz;
    private String myd;
    @ApiModelProperty("受理比例")
    private String acceptProportion;
    @ApiModelProperty("办结比例")
    private String banjieProportion;
    @ApiModelProperty("超时办结数")
    private Integer timeoutBanjieCount;
    @ApiModelProperty("超时未办结数")
    private Integer timeoutWeiBanjieCount;
    @ApiModelProperty("当前时间的数据")
    private Object dataList;
    @ApiModelProperty("按时间分组统计")
    private Object blsEchar;
    @ApiModelProperty("按部门分组统计")
    private Object bmsEchar;
    @ApiModelProperty("办理部门List")
    private Object blbmList;
    @ApiModelProperty("原系统业务流水号")
    private String yxtywlsh;
    @ApiModelProperty("地方事项编码")
    private String dfsxbm;
    @ApiModelProperty("事项名称")
    private String sxmc;
    private Object sysWeight;
    private Object localWeight;
    private Integer page;
    private Integer size;
    public void setMatterTotal(Integer matterTotal) {
        this.matterTotal = matterTotal;
    }

    public Integer getMatterTotal() {
        return matterTotal;
    }

    public Integer getApplyCount() {
        return applyCount;
    }

    public void setApplyCount(Integer applyCount) {
        this.applyCount = applyCount;
    }

    public Integer getBanjieCount() {
        return banjieCount;
    }

    public void setBanjieCount(Integer banjieCount) {
        this.banjieCount = banjieCount;
    }

    public Integer getAcceptCount() {
        return acceptCount;
    }

    public void setAcceptCount(Integer acceptCount) {
        this.acceptCount = acceptCount;
    }

    public String getAcceptProportion() {
        return acceptProportion;
    }

    public void setAcceptProportion(String acceptProportion) {
        this.acceptProportion = acceptProportion;
    }

    public String getBanjieProportion() {
        return banjieProportion;
    }

    public void setBanjieProportion(String banjieProportion) {
        this.banjieProportion = banjieProportion;
    }

    public Integer getTimeoutBanjieCount() {
        return timeoutBanjieCount;
    }

    public void setTimeoutBanjieCount(Integer timeoutBanjieCount) {
        this.timeoutBanjieCount = timeoutBanjieCount;
    }

    public Integer getTimeoutWeiBanjieCount() {
        return timeoutWeiBanjieCount;
    }

    public void setTimeoutWeiBanjieCount(Integer timeoutWeiBanjieCount) {
        this.timeoutWeiBanjieCount = timeoutWeiBanjieCount;
    }

    public String getDateFlag() {
        return dateFlag;
    }

    public void setDateFlag(String dateFlag) {
        this.dateFlag = dateFlag;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getSlzt() {
        return slzt;
    }

    public void setSlzt(String slzt) {
        this.slzt = slzt;
    }

    public String getBjzt() {
        return bjzt;
    }

    public void setBjzt(String bjzt) {
        this.bjzt = bjzt;
    }

    public String getBjcs() {
        return bjcs;
    }

    public void setBjcs(String bjcs) {
        this.bjcs = bjcs;
    }

    public Object getBlsEchar() {
        return blsEchar;
    }

    public void setBlsEchar(Object blsEchar) {
        this.blsEchar = blsEchar;
    }

    public Integer getCksqCount() {
        return cksqCount;
    }

    public void setCksqCount(Integer cksqCount) {
        this.cksqCount = cksqCount;
    }

    public Integer getWssqCount() {
        return wssqCount;
    }

    public void setWssqCount(Integer wssqCount) {
        this.wssqCount = wssqCount;
    }

    public Integer getTscxCount() {
        return tscxCount;
    }

    public void setTscxCount(Integer tscxCount) {
        this.tscxCount = tscxCount;
    }

    public Integer getTscxcsCount() {
        return tscxcsCount;
    }

    public void setTscxcsCount(Integer tscxcsCount) {
        this.tscxcsCount = tscxcsCount;
    }

    public Integer getNoacceptCount() {
        return noacceptCount;
    }

    public void setNoacceptCount(Integer noacceptCount) {
        this.noacceptCount = noacceptCount;
    }

    public Integer getXkBanjieCount() {
        return xkBanjieCount;
    }

    public void setXkBanjieCount(Integer xkBanjieCount) {
        this.xkBanjieCount = xkBanjieCount;
    }

    public Integer getBzCount() {
        return bzCount;
    }

    public void setBzCount(Integer bzCount) {
        this.bzCount = bzCount;
    }

    public Integer getYcxbzCount() {
        return ycxbzCount;
    }

    public void setYcxbzCount(Integer ycxbzCount) {
        this.ycxbzCount = ycxbzCount;
    }

    public Integer getFrBanjieCount() {
        return frBanjieCount;
    }

    public void setFrBanjieCount(Integer frBanjieCount) {
        this.frBanjieCount = frBanjieCount;
    }

    public Integer getGrBanjieCount() {
        return grBanjieCount;
    }

    public void setGrBanjieCount(Integer grBanjieCount) {
        this.grBanjieCount = grBanjieCount;
    }

    public Object getBmsEchar() {
        return bmsEchar;
    }

    public void setBmsEchar(Object bmsEchar) {
        this.bmsEchar = bmsEchar;
    }

    public Object getBlbmList() {
        return blbmList;
    }

    public void setBlbmList(Object blbmList) {
        this.blbmList = blbmList;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Object getSysWeight() {
        return sysWeight;
    }

    public void setSysWeight(Object sysWeight) {
        this.sysWeight = sysWeight;
    }

    public Object getLocalWeight() {
        return localWeight;
    }

    public void setLocalWeight(Object localWeight) {
        this.localWeight = localWeight;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getYcxbz() {
        return ycxbz;
    }

    public void setYcxbz(String ycxbz) {
        this.ycxbz = ycxbz;
    }

    public String getBj() {
        return bj;
    }

    public void setBj(String bj) {
        this.bj = bj;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getMyd() {
        return myd;
    }

    public void setMyd(String myd) {
        this.myd = myd;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getYxtywlsh() {
        return yxtywlsh;
    }

    public void setYxtywlsh(String yxtywlsh) {
        this.yxtywlsh = yxtywlsh;
    }

    public String getDfsxbm() {
        return dfsxbm;
    }

    public void setDfsxbm(String dfsxbm) {
        this.dfsxbm = dfsxbm;
    }

    public String getSxmc() {
        return sxmc;
    }

    public void setSxmc(String sxmc) {
        this.sxmc = sxmc;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Object getDataList() {
        return dataList;
    }

    public void setDataList(Object dataList) {
        this.dataList = dataList;
    }
}
