package cn.conac.rc.gateway.modules.share.entity;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ShareAreaEntity类
 *
 * @author beanCreator
 * @date 2017-07-21
 * @version 1.0
 */
@ApiModel
public class ShareAreaEntity implements Serializable {

	private static final long serialVersionUID = 15006155047077296L;

	@ApiModelProperty("主键")
	private Integer id;

	@ApiModelProperty("申请地区共享信息表外键")
	private Integer shareInfoId;

	@ApiModelProperty("地区代码申请方")
	private String areaCodeSource;

	@ApiModelProperty("地区申请方名称")
	private String areaNameSourceRe;
	
	@ApiModelProperty("地区代码被申请方")
	private String areaCodeDest;
	
	@ApiModelProperty("地区被申请方名称")
	private String areaNameDestRe;

	@ApiModelProperty("失效时间")
	private Date endTime;

	@ApiModelProperty("备注")
	private String remark;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setShareInfoId(Integer shareInfoId){
		this.shareInfoId=shareInfoId;
	}

	public Integer getShareInfoId(){
		return shareInfoId;
	}

	public void setAreaCodeSource(String areaCodeSource){
		this.areaCodeSource=areaCodeSource;
	}

	public String getAreaCodeSource(){
		return areaCodeSource;
	}
	
	public String getAreaNameSourceRe() {
		return areaNameSourceRe;
	}

	public void setAreaNameSourceRe(String areaNameSourceRe) {
		this.areaNameSourceRe = areaNameSourceRe;
	}

	public void setAreaCodeDest(String areaCodeDest){
		this.areaCodeDest=areaCodeDest;
	}

	public String getAreaCodeDest(){
		return areaCodeDest;
	}
	
	public String getAreaNameDestRe() {
		return areaNameDestRe;
	}

	public void setAreaNameDestRe(String areaNameDestRe) {
		this.areaNameDestRe = areaNameDestRe;
	}

	public void setEndTime(Date endTime){
		this.endTime=endTime;
	}

	public Date getEndTime(){
		return endTime;
	}

	public void setRemark(String remark){
		this.remark=remark;
	}

	public String getRemark(){
		return remark;
	}

}
