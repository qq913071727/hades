package cn.conac.rc.gateway.modules.ofs.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DutyEntity类
 *
 * @author beanCreator
 * @date 2016-11-01
 * @version 1.0
 */
@ApiModel
public class DutyEntity implements Serializable {

	private static final long serialVersionUID = 539368467800305803L;

	@ApiModelProperty("ID")
	private String id;

	@ApiModelProperty("机构ID号")
	private String baseId;

	@ApiModelProperty("职责序号")
	private String dutyNumber;

	@ApiModelProperty("职责编号")
	private String dutyCode;

	@ApiModelProperty("职责内容")
	private String dutyContent;

	@ApiModelProperty("备注")
	private String remarks;

	public void setId(String id){
		this.id=id;
	}

	public String getId(){
		return id;
	}

	public void setBaseId(String baseId){
		this.baseId=baseId;
	}
	
	public String getBaseId(){
		return baseId;
	}

	public void setDutyNumber(String dutyNumber){
		this.dutyNumber=dutyNumber;
	}

	public String getDutyNumber(){
		return dutyNumber;
	}

	public void setDutyCode(String dutyCode){
		this.dutyCode=dutyCode;
	}

	public String getDutyCode(){
		return dutyCode;
	}

	public void setDutyContent(String dutyContent){
		this.dutyContent=dutyContent;
	}

	public String getDutyContent(){
		return dutyContent;
	}

	public void setRemarks(String remarks){
		this.remarks=remarks;
	}

	public String getRemarks(){
		return remarks;
	}

}
