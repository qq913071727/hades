package cn.conac.rc.gateway.modules.user.vo;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;

/**
 * RoleInfo2Vo类
 *
 * @author voCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
public class RoleInfo2Vo  implements Serializable {

	private static final long serialVersionUID = -6343660368953003125L;
	
	private List<Integer> roleIdList;

	public List<Integer> getRoleIdList() {
		return roleIdList;
	}

	public void setRoleIdList(List<Integer> roleIdList) {
		this.roleIdList = roleIdList;
	}
	
}
