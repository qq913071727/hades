package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface VOrgCombinService {
	
	@RequestMapping(method = RequestMethod.GET, value = "/vOrgCombin/{baseId}")
	public JSONObject findByBaseId(@ApiParam(value = "部门baseId", required = true) @PathVariable("baseId") Integer baseId);

}
