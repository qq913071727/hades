package cn.conac.rc.gateway.modules.share.vo;

import java.io.Serializable;
import java.util.List;

import cn.conac.rc.gateway.common.entity.AreaEntity;
import io.swagger.annotations.ApiModel;

/**
 * ShareAreaVo类
 *
 * @author voCreator
 * @date 2017-07-21
 * @version 1.0
 */
@ApiModel
public class ShareAreaVo implements Serializable {

	private static final long serialVersionUID = -9137070046451123042L;

	private List<AreaEntity>  applShareAreaList;

	public List<AreaEntity> getApplShareAreaList() {
		return applShareAreaList;
	}

	public void setApplShareAreaList(List<AreaEntity> applShareAreaList) {
		this.applShareAreaList = applShareAreaList;
	}
	
}
