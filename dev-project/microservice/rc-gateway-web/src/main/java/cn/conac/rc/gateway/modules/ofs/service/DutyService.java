package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.entity.DutyEntity;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface DutyService {
	
	/**
	 * 通过机构ID查询相关职能职责
	 * @param baseId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/duty/list/{baseId}")
	 public JSONObject findListByBaseId(@PathVariable(value = "baseId") String baseId);
	
	/**
	 * 通过动态查询获取职能职责
	 * @param duty
	 * @return
	 */
    @RequestMapping(method = RequestMethod.POST, value = "/duty/list")
    JSONObject dutyList(@ApiParam(value = "查询条件对象", required = true) @RequestBody JSONObject duty);
    
    
    /**
     * 机构职能职责的新增或者更新
     * @param dutyEntity
     * @param submitType  （1： 正常保存 0： 暂存）
     * @return JSONObject
     */
    @RequestMapping(method = RequestMethod.POST, value = "/duty/{submitType}")
    public JSONObject saveOrUpdate(@PathVariable("submitType") String submitType, @ApiParam(value = "保存对象", required = true) @RequestBody DutyEntity dutyEntity);


    /**
	 * 根据ID删除职能职责
	 * @param id
	 * @return
	 */
    @RequestMapping(method = RequestMethod.POST, value = "/duty/{id}/delete")
    JSONObject delete(@ApiParam(value = "删除对象id", required = true) @PathVariable(value = "id") String id);

    /**
	 * 通过内设部门ID查询相关职能职责
	 * @param departId
	 * @return
	 */
    @RequestMapping(method = RequestMethod.GET, value = "/duty/correlation/list/{departId}")
    public JSONObject findByDepartId(@PathVariable(value = "departId") String departId);
    
    
    /**
 	 * 根据ID取得职能职责
 	 * @param id
 	 * @return
 	 */
     @RequestMapping(method = RequestMethod.GET, value = "/duty/{id}")
     JSONObject findById(@ApiParam(value = "对象id", required = true) @PathVariable(value = "id") String id);

}
