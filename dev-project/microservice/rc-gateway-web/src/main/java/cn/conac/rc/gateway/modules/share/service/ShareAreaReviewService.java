package cn.conac.rc.gateway.modules.share.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.share.vo.ShareAreaReviewCUVo;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-SHARE")
public interface ShareAreaReviewService {

	/**
	 * 批量共享地区审核
	 * @param shareAreaReviewCUVo
	 * @return 更新结果
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/shareAreaReview/saveOrUpdate")
	public JSONObject batchSaveOrUpdate(@ApiParam(value = "封装申请地区共享信息表的对象", required = true) @RequestBody ShareAreaReviewCUVo shareAreaReviewCUVo);

}
