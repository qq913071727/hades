package cn.conac.rc.gateway.common.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 地区信息实体
 * @author haocm
 */
@ApiModel("地区信息")
public class AreaVo implements Serializable {
    
    private static final long serialVersionUID = 8030023983021201711L;
    
    @ApiModelProperty("主键id")
    private String id;
    
    @ApiModelProperty("上级pid")
    private String pid;
    
    @ApiModelProperty("地区名称")
    private String name;
    
    @ApiModelProperty("地区code")
    private String code;
    
    @ApiModelProperty("地区权重")
    private Integer weight;
    
    @ApiModelProperty("等级lvl")
    private Integer lvl;
    
    @ApiModelProperty("节点leaf")
    private Integer leaf;
    
    @ApiModelProperty("行政区划编码")
    private String adminCode;
    
    @ApiModelProperty("试点1：试点0：非试点")
    private String type;
    
	@ApiModelProperty("1: 有公开办法 0：无公开办法")
	private Integer isPublic;
	
	@ApiModelProperty("是否不包含自己（1：是  0：否）")
	private Integer noSelfFlag;
	
	@ApiModelProperty("1:已共享 0：未共享")
	private Integer isShared = 0;
	
    @ApiModelProperty("下级地区")
    private List<AreaTreeVo> child = new ArrayList<AreaTreeVo>();
	
    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPid() {
        return pid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setLvl(Integer lvl) {
        this.lvl = lvl;
    }

    public Integer getLvl() {
        return lvl;
    }

    public void setLeaf(Integer leaf) {
        this.leaf = leaf;
    }

    public Integer getLeaf() {
        return leaf;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdminCode() {
        return adminCode;
    }

    public void setAdminCode(String adminCode) {
        this.adminCode = adminCode;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

	public Integer getIsPublic() {
		return isPublic;
	}
	
	public void setIsPublic(Integer isPublic) {
		this.isPublic = isPublic;
	}

	public Integer getNoSelfFlag() {
		return noSelfFlag;
	}

	public void setNoSelfFlag(Integer noSelfFlag) {
		this.noSelfFlag = noSelfFlag;
	}

	public Integer getIsShared() {
		return isShared;
	}

	public void setIsShared(Integer isShared) {
		this.isShared = isShared;
	}

	public List<AreaTreeVo> getChild() {
		return child;
	}

	public void setChild(List<AreaTreeVo> child) {
		this.child = child;
	}
	
}
