package cn.conac.rc.gateway.modules.ofs.vo;

import java.io.Serializable;

import cn.conac.rc.gateway.modules.ofs.entity.OrgStatusEntity;
import io.swagger.annotations.ApiModel;

@ApiModel
public class OrgChangeVo implements Serializable {

	private static final long serialVersionUID = 2632801362700071278L;

	private OperationStatusVo operationStatusVo;
	
	private OrgStatusEntity orgStatusEntity;

	public OperationStatusVo getOperationStatusVo() {
		return operationStatusVo;
	}

	public void setOperationStatusVo(OperationStatusVo operationStatusVo) {
		this.operationStatusVo = operationStatusVo;
	}

	public OrgStatusEntity getOrgStatusEntity() {
		return orgStatusEntity;
	}

	public void setOrgStatusEntity(OrgStatusEntity orgStatusEntity) {
		this.orgStatusEntity = orgStatusEntity;
	}

}
