package cn.conac.rc.gateway.common.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.ResultPojo;
import cn.conac.rc.gateway.common.entity.AreaEntity;
import cn.conac.rc.gateway.common.service.AreaService;
import cn.conac.rc.gateway.common.vo.AreaVo;
import cn.conac.rc.gateway.common.vo.VAreaInfoVo;
import cn.conac.rc.gateway.modules.ofs.vo.OrganizationVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags="地区接口", description="共通")
@RequestMapping("comm/")
public class AreaController {
    @Autowired
    AreaService areaService;
    
    @ApiOperation(value = "获得地区信息", notes = "获得地区信息", response = AreaVo.class)
    @RequestMapping(value = "/area/pid/{pid}/{lvl}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> getStatisticsSjfx(HttpServletRequest request, HttpServletResponse response,
            @ApiParam("地区父id") @PathVariable("pid") String pid, @ApiParam("") @PathVariable("lvl") String lvl)
            throws Exception {
        if (StringUtils.isBlank(pid) || StringUtils.isBlank(lvl)) {
            return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_FAILURE, "参数错误！"), HttpStatus.OK);
        }
        return new ResponseEntity<ResultPojo>(
                new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, areaService.getAreaList(pid, lvl)),
                HttpStatus.OK);
    }

    @ApiOperation(value = "通过ID获得对应地区信息", notes = "获得地区信息", response = AreaVo.class)
    @RequestMapping(value = "/area/{id}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> getAreaInfo(HttpServletRequest request, HttpServletResponse response,
            @ApiParam("地区id") @PathVariable("id") String id)
            throws Exception {
        if (StringUtils.isBlank(id)) {
            return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_FAILURE, "参数错误！"), HttpStatus.OK);
        }
        return new ResponseEntity<ResultPojo>(
                new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, areaService.getAreaInfo(id)),
                HttpStatus.OK);
    }
    
    @ApiOperation(value = "通过ID获得包含上级地区信息的地区信息", notes = "获得包含上级地区信息的地区信息", response = AreaVo.class)
    @RequestMapping(value = "/area/v/{id}", method = RequestMethod.GET)
    public Object getVAreaInfo(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value ="地区id", required = true) @PathVariable("id") String id ) throws Exception {
    	ResultPojo resultInfo = new ResultPojo();
    	// 确保地区id不能为空
		if(StringUtils.isBlank(id)) {
			// 地区id为空场合
			resultInfo.setCode(ResultPojo.CODE_FAILURE);
			resultInfo.setMsg("参数错误: 地区id不能为null或者空");
			resultInfo.setResult(null);
			return resultInfo;
		}
        return areaService.getVAreaInfo(id);
    }
    
    @ApiOperation(value = "获得省一级地区信息", notes = "获得省一级地区信息", response = AreaVo.class)
    @RequestMapping(value = "/area/lvl", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> getProvince(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return new ResponseEntity<ResultPojo>(
                new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, areaService.getAreaListByLvl(1)),
                HttpStatus.OK);
    }
    
    @ApiOperation(value = "根据Pid获得用户信息树用的地区列表信息（附带样式）", notes = "根据Pid获得用户信息树用的地区列表信息（附带样式）", response =JSONArray.class)
    @RequestMapping(value = "/area/css/list/{pid}", method = RequestMethod.GET)
    public JSONArray getAreaCSSList(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam("地区父id") @PathVariable("pid") String pid)
            throws Exception {
    	return areaService.getAreaCSSList(pid);
    }
    
    @ApiOperation(value = "获得用户信息树用的地区列表信息", notes = "获得用户信息树用的地区列表信息", response =JSONArray.class)
    @RequestMapping(value = "/area/list", method = RequestMethod.GET)
    public JSONArray getAreaList(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	return areaService.getAreaList();
    }
    
    @ApiOperation(value = "获得用户信息树用的地区列表信息", notes = "获得用户信息树用的地区列表信息", response =JSONArray.class)
    @RequestMapping(value = "/area/v/list", method = RequestMethod.POST)
    public JSONObject getVAreaList(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam(value = "根据参数查询部门库", required = false) @RequestBody VAreaInfoVo vo)
            throws Exception {
    	return areaService.getVAreaList(vo);
    }
    
    @ApiOperation(value = "根据Pid获得用户信息树用的地区列表信息", notes = "根据Pid获得用户信息树用的地区列表信息", response =JSONArray.class)
    @RequestMapping(value = "/area/list/{pid}", method = RequestMethod.GET)
    public JSONArray getAreaList(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam("地区父id") @PathVariable("pid") String pid)
            throws Exception {
    	return areaService.getAreaListByPid(pid);
    }
    
    @ApiOperation(value = "新增", httpMethod = "POST", response = OrganizationVo.class, notes = "保存部门库")
	@RequestMapping(value = "area/c", method = RequestMethod.POST)
	public JSONObject cudAreaSave(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "保存对象", required = true) @RequestBody AreaEntity areaEntity) throws Exception {		
		return  areaService.saveOrUpdate(areaEntity);
	}
    
    @ApiOperation(value = "根据区域Code和开通方式开通试点", httpMethod = "POST", response = JSONObject.class, notes = "根据区域Code和开通方式开通试点")
   	@RequestMapping(value = "area/{areaCode}/open/{openPilotType}", method = RequestMethod.POST)
   	public JSONObject cudUpdateTypeByCond(HttpServletRequest request, HttpServletResponse response,
   			@ApiParam(value = "areaCode", required = true) @PathVariable("areaCode") String areaCode,
            @ApiParam(value = "openPilotType", required = true) @PathVariable("openPilotType") String openPilotType) throws Exception {		
   		return  areaService.updateTypeByCond(areaCode,openPilotType);
   	}
}
