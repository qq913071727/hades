package cn.conac.rc.gateway.modules.ofs.vo;

import java.util.Date;

import cn.conac.rc.gateway.modules.ofs.entity.VOrgPreSubEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * VOrgPreSubVo类
 *
 * @author voCreator
 * @date 2016-12-14
 * @version 1.0
 */
@ApiModel
public class VOrgPreSubVo extends VOrgPreSubEntity {

	private static final long serialVersionUID = 5980485268652403236L;

	@ApiModelProperty("当前分页")
	private Integer page;

	@ApiModelProperty("每页个数")
	private Integer size;

	@ApiModelProperty("创建 开始时间")
	private Date createDateStart;

	@ApiModelProperty("创建 结束时间")
	private Date createDateEnd;

	@ApiModelProperty("更新 开始时间")
	private Date updateDateStart;

	@ApiModelProperty("更新 结束时间")
	private Date updateDateEnd;

	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}

	public void setCreateDateStart(Date createDateStart){
		this.createDateStart=createDateStart;
	}

	public Date getCreateDateStart(){
		return createDateStart;
	}

	public void setCreateDateEnd(Date createDateEnd){
		this.createDateEnd=createDateEnd;
	}

	public Date getCreateDateEnd(){
		return createDateEnd;
	}

	public void setUpdateDateStart(Date updateDateStart){
		this.updateDateStart=updateDateStart;
	}

	public Date getUpdateDateStart(){
		return updateDateStart;
	}

	public void setUpdateDateEnd(Date updateDateEnd){
		this.updateDateEnd=updateDateEnd;
	}

	public Date getUpdateDateEnd(){
		return updateDateEnd;
	}

}
