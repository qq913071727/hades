package cn.conac.rc.gateway.modules.ofs.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AppFilesEntity类
 *
 * @author beanCreator
 * @date 2016-11-02
 * @version 1.0
 */
@ApiModel
public class AppFilesEntity implements Serializable {

	private static final long serialVersionUID = 1478071306899285540L;

	@ApiModelProperty("ID")
	private String id;

	@ApiModelProperty("机构ID号")
	private String baseId;

	@ApiModelProperty("存储文件URL")
	private String appUrl;

	public void setId(String id){
		this.id=id;
	}
	
	public String getId(){
		return id;
	}

	public void setBaseId(String baseId){
		this.baseId=baseId;
	}

	public String getBaseId(){
		return baseId;
	}

	public void setAppUrl(String appUrl){
		this.appUrl=appUrl;
	}

	public String getAppUrl(){
		return appUrl;
	}

}
