package cn.conac.rc.gateway.modules.ofs.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.base.ResultPojo;
import cn.conac.rc.gateway.modules.ofs.service.AnalzWarningService;
import cn.conac.rc.gateway.modules.ofs.vo.AnalzWarning;
import cn.conac.rc.gateway.modules.ofs.vo.AnalzWarningVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags="三定分析", description="三定信息")
@RequestMapping(value="ofs/analzWarning/") // TODO 具体根据情况可以改修
public class AnalzWarningController {

	@Autowired
	AnalzWarningService service;

	@ApiOperation(value = "详情", httpMethod = "GET", response = AnalzWarning.class, notes = "根据id获取资源详情")
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") String id) {
		return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				service.detail(id)), HttpStatus.OK);
	}

	@ApiOperation(value = "个数", httpMethod = "POST", response = AnalzWarning.class, notes = "根据条件获得资源数量")
	@RequestMapping(value = "count", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> count(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody AnalzWarning vo) throws Exception {
		return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				service.count(vo)), HttpStatus.OK);
	}

	@ApiOperation(value = "列表", httpMethod = "POST", response = AnalzWarning.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody AnalzWarningVo vo) throws Exception {
		return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				service.list(vo)), HttpStatus.OK);
	}

	@ApiOperation(value = "获取内设Id关联的职能职责", httpMethod = "GET", response = AnalzWarning.class, notes = "获取内设关联的职能职责")
	@RequestMapping(value = "Duty/{id}/Departments", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findDutyByDepartment(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") String id) {
		return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				service.findDutyByDepartment(id)), HttpStatus.OK);
	}

	@ApiOperation(value = "获取职能职责Id关联的内设", httpMethod = "GET", response = AnalzWarning.class, notes = "获取职能职责Id关联的内设")
	@RequestMapping(value = "Department/{id}/Dutys", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findDepartmentByDuty(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") String id) {
		return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				service.findDepartmentByDuty(id)), HttpStatus.OK);
	}
	
	@ApiOperation(value = "根据部门库Id获取没有关联内设的职能职责", httpMethod = "GET", response = JSONObject.class, notes = "根据部门库Id获取没有关联内设的职能职责")
	@RequestMapping(value = "/org/{id}/dutysWithNoDept", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findDutyWithNoDutyListByOrgId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") String id) {
		return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				service.findDutyWithNoDutyListByOrgId(id)), HttpStatus.OK);
	}

	@ApiOperation(value = "根据部门库Id获取没有关联职能职责的内设机构", httpMethod = "GET", response = JSONObject.class, notes = "根据部门库Id获取没有关联职能职责的内设机构")
	@RequestMapping(value = "/org/{id}/deptsWithNoDuty", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> finddeptWithNoDeptListByOrgId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") String id) {
		return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				service.finddeptWithNoDeptListByOrgId(id)), HttpStatus.OK);
	}
}
