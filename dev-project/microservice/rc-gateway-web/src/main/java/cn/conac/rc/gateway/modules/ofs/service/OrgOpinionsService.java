package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.entity.OrgOpinionsEntity;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface OrgOpinionsService {
	
	 /**
     *部门审核意见信息的新增或者更新
     * @param opinionsEntity
     * @return JSONObject
     */
	@RequestMapping(method = RequestMethod.POST, value = "/orgOpinions/")
	 public JSONObject saveOrUpdate(@ApiParam(value = "保存审核意见信息表", required = true) @RequestBody OrgOpinionsEntity opinionsEntity);

}
