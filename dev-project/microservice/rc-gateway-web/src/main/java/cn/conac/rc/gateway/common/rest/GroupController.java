package cn.conac.rc.gateway.common.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.common.service.GroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags="文件上传类型", description="共通")
@RequestMapping("comm/")
public class GroupController {

	@Autowired
	GroupService  groupService;
    
    @ApiOperation(value = "获得上传的文件类型",  notes = "获得上传的文件类型")
    @RequestMapping(value = "upload/types", method = RequestMethod.GET)
    public JSONObject getFileTypes(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return groupService.getFileTypes(1);
    }
}
