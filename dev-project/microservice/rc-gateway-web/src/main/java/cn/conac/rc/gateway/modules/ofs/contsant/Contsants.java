package cn.conac.rc.gateway.modules.ofs.contsant;

public class Contsants {
	// 部门库中部门状态
	/**
	 * 正常
	 */
	public static String OFS_ORGANIZATION_FLAG_NORMAL = "0";
	/**
	 * 撤并
	 */
	public static String OFS_ORGANIZATION_FLAG_CANCEL = "1";
	/**
	 * 其他状态
	 */
	public static String OFS_ORGANIZATION_FLAG_OTHER = "2";
	
	
	/**
	 * 有子部门标示“1”
	 */
	public static String OFS_ORGANIZATION_FLAG_HAS_CHILD = "1";
	/**
	 * 无子部门标示“0”
	 */
	public static String OFS_ORGANIZATION_FLAG_HAS_NO_CHILD = "0";
	
	/**
	 * 三定信息系统文件上传路径
	 */
	public static final String ORGINFO_PATH = "/orginfo";
	
	/**
	 * 保存类型：10变更基本信息 21变更职能职责 22变更内设机构 23变更编制信息 24变更职能和内设机构 25变更内设和编制信息 30变更其他 40编辑
	 */
	public static String OFS_ORGANIZATION_FLAG_UPDATE_BASIC = "10";
	/**
	 * 编辑
	 */
	public static String OFS_ORGANIZATION_FLAG_EDIT = "40";
	
	/**
	 * 添加
	 */
	public static String OFS_ORGANIZATION_FLAG_SAVE_ADD = "1";
	/**
	 * 暂存
	 */
	public static String OFS_ORGANIZATION_FLAG_SAVE_TEMPADD = "3";
	
	/**
	 * 1：增加简要信息  
	 */
	public static String OFS_EVOL_ORG_FLAG_SIMPLE_INFO_TYPE = "1";
	
	/**
	 * 2： 增加详细信息
	 */
	
	public static String OFS_EVOL_ORG_FLAG_SIMPLE_INFO_TYPE_DETAIL = "2";
	
	// ****************变更类型定义******************
	/**
	 * 机构最新状态记录表 - 变更类型 - 部门新建
	 */
	public static String OFS_UPDATE_TYPE_INPUT = "10";
	/**
	 * 机构最新状态记录表 - 变更类型 - 基本信息变更 -- 20
	 */
	public static String OFS_UPDATE_TYPE_BASIC_INFO = "20";
	/**
	 * 机构最新状态记录表 - 变更类型 - 基本信息-部门名称变更 -- 21
	 */
	public static String OFS_UPDATE_TYPE_BASIC_INFO_DEPT_NAME = "21";
	/**
	 * 机构最新状态记录表 - 变更类型 - 基本信息-部门类型变更 -- 22
	 */
	public static String OFS_UPDATE_TYPE_BASIC_INFO_DEPT_TYPE = "22";
	/**
	 * 机构最新状态记录表 - 变更类型 - 基本信息-机构规格变更 -- 23
	 */
	public static String OFS_UPDATE_TYPE_BASIC_INFO_SPECIFICATIONS = "23";
	/**
	 * 机构最新状态记录表 - 变更类型 - 基本信息-其他信息变更 -- 24
	 */
	public static String OFS_UPDATE_TYPE_BASIC_INFO_OTHERS = "24";
	/**
	 * 机构最新状态记录表 - 变更类型 - 三定信息变更
	 */
	public static String OFS_UPDATE_TYPE_THREE_DIREC = "30";
	/**
	 * 机构最新状态记录表 - 变更类型 - 三定信息-职能职责变更
	 */
	public static String OFS_UPDATE_TYPE_THREE_DIREC_DUTY = "31";
	/**
	 * 机构最新状态记录表 - 变更类型 - 三定信息-编制变更
	 */
	public static String OFS_UPDATE_TYPE_THREE_DIREC_EMP = "32";
	/**
	 * 机构最新状态记录表 - 变更类型 - 三定信息-内设机构变更
	 */
	public static String OFS_UPDATE_TYPE_THREE_DIREC_DEPT = "33";
	/**
	 * 机构最新状态记录表 - 变更类型 - 其他信息变更
	 */
	public static String OFS_UPDATE_TYPE_OTHERS_INFO = "40";
	/**
	 * 机构最新状态记录表 - 变更类型 - 撤并变更
	 */
	public static String OFS_UPDATE_TYPE_MERGE = "50";
	/**
	 * 机构最新状态记录表 - 变更类型 - 除撤并以外的其他类型
	 */
	public static String OFS_EXCEPT_TYPE_MERGE = "55";		
	/**
	 * 机构最新状态记录表 - 变更类型 - 编辑
	 */
	public static String OFS_UPDATE_TYPE_EDIT = "60";
	/**
	 * 机构最新状态记录表 - 变更类型 - 非撤并变更状态定义为正常
	 */
	public static String OFS_UPDATE_TYPE_NORMAL = "正常";

	// ****************最新状态定义******************
	/**
	 * 机构最新状态记录表 - 最新状态- 待提交
	 */
	public static String OFS_STATUS_PRE_SUB = "00";
	/**
	 * 机构最新状态记录表 - 最新状态 - 待审核
	 */
	public static String OFS_STATUS_PRE_AUDIT = "01";
	/**
	 * 机构最新状态记录表 - 最新状态 - 驳回
	 */
	public static String OFS_STATUS_REJECT = "02";
	/**
	 * 机构最新状态记录表 - 最新状态 - 通过
	 */
	public static  String OFS_AUDIT_THROUGH = "03";
	
	// ****************业务无关的常量定义******************
	/**
	 * 逗号
	 */
	public static String OFS_COMMA = ",";
	/**
	 * 分号
	 */
	public static String OFS_SEMIC = ";";
	/**
	 * 路径分隔符 "/"
	 */
	public static String OFS_SLASH= "/";
	/**
	 * 空白符 ""
	 */
	public static String OFS_EMPTY= "";
	
	/**
	 * "--"文字列 
	 */
	public static String OFS_NOTHING_STR= "--";
	
	/**
	 * "<br/>"文字列 
	 */
	public static String OFS_HTML_BR_STR= "<br/>";
	
	/**
	 * 区分机构ID和机构名称的区分标志符 @@ (撤并部门用)
	 */
	public static String OFS_REV_SEP= "@@";
	
	/**
	 * 字符 "0"
	 */
	public static String OFS_STR_ZERO= "0";
	
	/**
	 * 字符  "1"
	 */
	public static String OFS_STR_ONE= "1";
	
	/**
	 * 字符  "OK"
	 */
	public static String OFS_STR_OK= "OK";
	
	/**
	 * 字符  "DataNull"
	 */
	public static String OFS_STR_DATA_NULL= "DataNull";
	
	// ****************信息完整度定义******************
	/**
	 * 0：表示只是给这个部门建立一个空壳子，只有部门名称和相关状态信息(部门撤并用)
	 */
	public static Integer OFS_INTEGRITY_STEP_ZERO = 0;
	/**
	 * 1：基本信息完整
	 */
	public static Integer OFS_INTEGRITY_STEP_ONE = 1;
	/**
	 * 2：基本信息和批文信息完整
	 */
	public static Integer OFS_INTEGRITY_STEP_TWO = 2;
	/**
	 * 3：基本信息、批文信息和职能职责信息完整
	 */
	public static Integer OFS_INTEGRITY_STEP_THREE = 3;
	/**
	 * 4: 基本信息、批文信息、职能职责和内设机构信息完整
	 */
	public static Integer OFS_INTEGRITY_STEP_FOUR = 4;
	/**
	 * 5: 基本信息、批文信息、职能职责、内设机构和编制信息完整
	 */
	public static Integer OFS_INTEGRITY_STEP_FIVE = 5;
	/**
	 * 6: 基本信息、批文信息、职能职责、内设机构、编制信和其他事项及附则完整
	 */
	public static Integer OFS_INTEGRITY_STEP_SIX = 6;
	
	/**
	 * 是否历史：0当前
	 */
	public static Integer OFS_ORG_STATUS_IS_HISTORY_FALSE = 0;
	/**
	 * 是否历史：1历史
	 */
	public static Integer OFS_ORG_STATUS_IS_HISTORY_TRUE = 1;
	
	/**
	 * 是否主管部门：1是
	 */
	public static Integer OFS_ORG_IS_COMP = 1;
	
	/**
	 * 是否主管部门字符型：是
	 */
	public static String OFS_ORG_STR_IS_COMP = "1";
	/**
	 * 信息完整度掩码初始化
	 */
	public static String OFS_DEFAULT_CONTENT_MASK = "000000";
	/**
	 * 信息完整度掩码完整
	 */
	public static String OFS_FULL_CONTENT_MASK = "111111";
	
	/**
	 * 是否对三定方案重新定义—是
	 */
	public static String ISDEFINE_TRUE = "1";
	
	/**
	 * 是否对三定方案重新定义—否
	 */
	public static String ISDEFINE_FALSE = "0";
	
	/**
	 * 设置信息完备度掩码
	 * 举例：六部分信息（000000），每一位代表一部分信息，如基本信息完备就为100000，step为第几步
	 * @param one 代表初始掩码串
	 * @param two 代表设置为0或1
	 * @param step 代表哪位设置，从1-6
	 * @return
	 */
	public static String mergeOrgContentMask(String one,String two,int step){
		if(one == null){
			one = "000000";
		}
		return one.substring(0, step-1) + two + one.substring(step);
	}
	
	// **************部门相关定义*************
	/**
	 * 主管部门标示
	 */
	public static String ORG_COMP_FLAG = "1";
	
	// **************部门类型定义*************
	/**
	 * 行政机关标示
	 */
	public static String ORG_TYPE_XZJG = "1";
	
	/**
	 * 事业单位标示
	 */
	public static String ORG_TYPE_SYDW = "2";
	
	// **************经费来源定义*************
	/**
	 * 全额拨款
	 */
	public static String FUN_SOURCE_CODE_ALL = "11";
	/**
	 * 差额补贴
	 */
	public static String FUN_SOURCE_CODE_SUB = "12";
	
	/**
	 * 经费自理/自收自支
	 */
	public static String FUN_SOURCE_CODE_SELF = "20";
	
	/**
	 * 以上以外场合（页面表示[--请选择--]）
	 */
	public static String FUN_SOURCE_CODE_NONE = "";
	
	
	/**
	 * 全额文字常量定义
	 */
	public static String FUN_SOURCE_STR_ALL = "全额";
	/**
	 * 差额文字常量定义
	 */
	public static String FUN_SOURCE_STR_SUB = "差额";
	
	/**
	 * 经费自理文字常量定义
	 */
	public static String FUN_SOURCE_STR_SELF1 = "经费自理";
	
	
	/**
	 * 自收自支文字常量定义
	 */
	public static String FUN_SOURCE_STR_SELF2 = "自收自支";
	
	
	// **************部门管理和历史沿革变更元类型*************
	/**
	 *部门管理中最新部门数据为基础进行复制
	 */
	public static String ORG_CHANGE_MODEL_CURR = "0";
	
	/**
	 * 历史沿革根据所选择节点为基础进行复制
	 */
	public static String ORG_CHANGE_MODEL_HIST = "1";
	
	/**
	 * 直接录入的方式进行历史沿革的变更
	 */
	public static String ORG_CHANGE_MODEL_INPUT = "2";
	
}
