package cn.conac.rc.gateway.modules.user.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * UserAreaEntity类
 *
 * @author beanCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
public class UserAreaEntity implements Serializable {

	private static final long serialVersionUID = 1491821123837926287L;

	@ApiModelProperty("id")
	private Integer id;
	
	@ApiModelProperty("用户ID")
	private Integer userId;

	@ApiModelProperty("区域ID")
	private String areaId;

	@ApiModelProperty("区域编码")
	private String areaCode;

	public void setUserId(Integer userId){
		this.userId=userId;
	}

	public Integer getUserId(){
		return userId;
	}

	public void setAreaId(String areaId){
		this.areaId=areaId;
	}

	public String getAreaId(){
		return areaId;
	}

	public void setAreaCode(String areaCode){
		this.areaCode=areaCode;
	}

	public String getAreaCode(){
		return areaCode;
	}

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

}
