package cn.conac.rc.gateway.modules.share.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.conac.rc.gateway.modules.share.entity.ShareAreaInfoEntity;
import io.swagger.annotations.ApiModel;

/**
 * ShareAreaInfoVo类
 *
 * @author voCreator
 * @date 2017-07-21
 * @version 1.0
 */
@ApiModel
public class ShareAreaInfoCUVo implements Serializable {

	private static final long serialVersionUID = 9184160125372992037L;
	
	List<ShareAreaInfoEntity> shareAreaInfoEntityList = new ArrayList<ShareAreaInfoEntity>();

	public List<ShareAreaInfoEntity> getShareAreaInfoEntityList() {
		return shareAreaInfoEntityList;
	}

	public void setShareAreaInfoEntityList(List<ShareAreaInfoEntity> shareAreaInfoEntityList) {
		this.shareAreaInfoEntityList = shareAreaInfoEntityList;
	}
	
}
