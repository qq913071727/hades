package cn.conac.rc.gateway.modules.ofs.service;

import java.util.Map;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.entity.DepartmentEntity;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface DepartmentService {
	
	@RequestMapping(method = RequestMethod.GET, value = "/department/list/{baseId}")
	public JSONObject findListByBaseId(@PathVariable(value = "baseId") String baseId);
	
	@RequestMapping(method = RequestMethod.GET, value = "/department/list/level/flag/{baseId}")
	public JSONObject findLevelFlagListByBaseId(@PathVariable(value = "baseId") String baseId);
	
	@RequestMapping(method = RequestMethod.GET, value = "/department/correlation/{dutyId}")
	public JSONObject findListByDutyId(@PathVariable(value = "dutyId") String dutyId);
	
	@RequestMapping(method = RequestMethod.GET, value = "/department/list/levelOne/{baseId}")
	public JSONObject findLevelOneByBaseId(@PathVariable(value = "baseId") String baseId);
	
	@RequestMapping(method = RequestMethod.GET, value = "/department/list/child/{parentId}")
	public JSONObject findByParentId(@PathVariable(value = "parentId") String parentId);
	
	@RequestMapping(method = RequestMethod.GET, value = "/department/mapTree/{baseId}")
	public JSONObject findMapTreeByBaseId(@PathVariable(value = "baseId") String baseId);
	
	@RequestMapping(method = RequestMethod.GET, value = "/department/deptTree/{baseId}")
	public JSONObject findDeptTreeByBaseId(@PathVariable(value = "baseId") String baseId);
	
	@RequestMapping(method = RequestMethod.GET, value = "/department/mapList/{baseId}")
	public JSONObject findMapListByBaseId(@PathVariable(value = "baseId") String baseId);
	
	 /**
     * 机构内设机构的新增或者更新
     * @param departmentEntity
     * @param submitType  （1： 正常保存 0： 暂存）
     * @return JSONObject
     */
    @RequestMapping(method = RequestMethod.POST, value = "/department/{submitType}")
    public JSONObject saveOrUpdate(@PathVariable("submitType") String submitType, @ApiParam(value = "保存对象", required = true) @RequestBody DepartmentEntity departmentEntity);
    
    
    /**
	 * 根据ID删除内设机构信息
	 * @param id
	 * @return
	 */
    @RequestMapping(method = RequestMethod.POST, value = "/department/{id}/delete")
    JSONObject delete(@ApiParam(value = "删除对象id", required = true) @PathVariable(value = "id") String id);
    
    /**
 	 * 根据ID取得内设机构信息
 	 * @param id
 	 * @return
 	 */
     @RequestMapping(method = RequestMethod.GET, value = "/department/{id}")
     JSONObject findById(@ApiParam(value = "对象id", required = true) @PathVariable(value = "id") String id);
     
     /**
 	 * 向上移动内设机构
 	 * @param condMap
 	 * @return JSONObject
 	 */
 	@RequestMapping(method = RequestMethod.POST, value = "/department/move/up")
 	public JSONObject moveUp(@ApiParam(value = "内设机构移动条件对象", required = true) @RequestBody Map<String, Integer> condMap);
 	
 	 /**
 	 * 向下移动内设机构
 	 * @param condMap
 	 * @return JSONObject
 	 */
 	@RequestMapping(method = RequestMethod.POST, value = "/department/move/down")
 	public JSONObject moveDown(@ApiParam(value = "内设机构移动条件对象", required = true) @RequestBody Map<String, Integer> condMap);
	
}
