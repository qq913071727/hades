package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.vo.VAuditListVo;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface VAuditListService {
	
	@RequestMapping(method = RequestMethod.GET, value = "/auditList/{id}")
	 public JSONObject findById(@ApiParam(value = "id", required = true) @PathVariable("id") String id);
	
	@RequestMapping(method = RequestMethod.GET, value = "/auditList/list")
	 public JSONObject list(@ApiParam(value = "查询审核信息对象", required = true) @RequestBody VAuditListVo vAuditListVo);
	


}
