package cn.conac.rc.gateway.modules.monitor.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.monitor.vo.LocalStatus;

/**
 * 监测服务
 * 
 * @author haocm
 *
		*/
@FeignClient("RC-SERVICE-MONITOR")
public interface EsService
{

	@RequestMapping(method = RequestMethod.POST, value = "/es/status", produces = MediaType.APPLICATION_JSON_VALUE)
	JSONObject getStatus(@RequestBody LocalStatus localStatus);
}