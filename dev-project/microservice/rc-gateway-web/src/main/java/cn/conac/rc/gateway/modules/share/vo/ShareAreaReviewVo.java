package cn.conac.rc.gateway.modules.share.vo;

import cn.conac.rc.gateway.modules.share.entity.ShareAreaReviewEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ShareAreaReviewVo类
 *
 * @author voCreator
 * @date 2017-07-21
 * @version 1.0
 */
@ApiModel
public class ShareAreaReviewVo extends ShareAreaReviewEntity {

	private static final long serialVersionUID = 1500615505046522594L;

	@ApiModelProperty("当前分页")
	private Integer page;

	@ApiModelProperty("每页个数")
	private Integer size;

	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}
}
