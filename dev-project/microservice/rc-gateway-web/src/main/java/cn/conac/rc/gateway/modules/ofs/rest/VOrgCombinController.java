package cn.conac.rc.gateway.modules.ofs.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.RestClientException;
import cn.conac.rc.gateway.modules.ofs.service.VOrgCombinService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="ofs/")
@Api(tags="部门的并入部门信息", description="三定信息")
public class VOrgCombinController {

    @Autowired
    VOrgCombinService vOrgCombinService;
    
    @ApiOperation(value = "部门的并入部门信息", httpMethod = "GET", response = JSONObject.class, notes = "部门的并入部门信息")
    @RequestMapping(value = "bases/{baseId}/combins", method = RequestMethod.GET)
    public JSONObject auditListInfo(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam(value = "部门baseId", required = true) @PathVariable("baseId") Integer baseId)
            throws RestClientException, Exception {
        return vOrgCombinService.findByBaseId(baseId);
    }

}