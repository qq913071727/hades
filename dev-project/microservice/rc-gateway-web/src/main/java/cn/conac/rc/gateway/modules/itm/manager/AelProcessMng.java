package cn.conac.rc.gateway.modules.itm.manager;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.FastJsonUtil;
import cn.conac.rc.gateway.modules.common.vo.UserVo;
import cn.conac.rc.gateway.modules.itm.service.WorkflowService;

@Service
public class AelProcessMng{

	@Autowired
	WorkflowService service;
	
//	public boolean commitProcess(Integer childItemId,String sysType,CmsUser user, AelItemReview itemReview){
//		//调用父类接口，并返回流程是否结束标志
//		if(!isTaskExist(childItemId, sysType)){
//			this.startProcess(childItemId, sysType, user);
//		}
//		boolean isProcessEnded = super.commitProcess(childItemId, sysType);
//		//审批流水记录表添加审批记录
//		AelItemInfo itemInfo = aelItemInfoDao.findById(childItemId);
//		itemInfo = saveReviewInfo(itemInfo, user, itemReview);
//		if(isProcessEnded){
//			//事项变更、取消、废置时对权利事项进行相应更新操作
//			Operation itemOper = itemInfo.getOperation();
//			if(itemOper.equals(Operation.C) ||
//					itemOper.equals(Operation.W) ||
//					itemOper.equals(Operation.P)){
//			        AelItems mainItem = aelItemsDao.findById(itemInfo.getItemsId());
//			        Set<DelBasicItemInfo> delBaseItemInfos = mainItem.getItemInfo().getDelBaseItemInfo();
//			        if(delBaseItemInfos != null && delBaseItemInfos.size() > 0){
//						//事项取消废置的场合,连同责任事项一块取消废置
//						if(itemOper.equals(Operation.W) || itemOper.equals(Operation.P)){
//						    for(DelBasicItemInfo delBaseInfo : delBaseItemInfos){
//						    	//取消待审核的责任事项
//						    	if( !DelBasicItemInfo.Status.AUDIT_PASS.equals(delBaseInfo.getStatus()) ||
//						    		// 取消当前主表绑定的已发布的责任事项
//						    		 (delBaseInfo.getRespItem() != null &&
//						    	     delBaseInfo.getRespItem().getBasicRespItemInfo() != null &&
//						    	     delBaseInfo.getRespItem().getBasicRespItemInfo().getId().equals(delBaseInfo.getId()))
//						    	     )
//						    	{
//									    if(itemOper.equals(Operation.W)){
//										    delBaseInfo.setOperation(DelBasicItemInfo.Operation.W);
//									    }else if(itemOper.equals(Operation.P)){
//										    delBaseInfo.setOperation(DelBasicItemInfo.Operation.P);
//									    }
//									    delBaseInfo.setStatus(DelBasicItemInfo.Status.AUDIT_PASS);
//						    	}
//						    }
//						}
//						Set<DelBasicItemInfo> copyBasicItems = new HashSet<DelBasicItemInfo>(delBaseItemInfos.size());
//						for(DelBasicItemInfo delBaseInfo : delBaseItemInfos){
//							copyBasicItems.add(delBaseInfo);
//						}
//						//责任事项与新生成的权利事项进行关联
//						itemInfo.setDelBaseItemInfo(copyBasicItems);
//				}
//			}
//			//设置事项子表状态为审核通过
//			itemInfo.setItemStatus(BaseItemInfo.Status.AUDIT_PASS);
//			//更新主表
//			AelItems items = aelItemsDao.findById(itemInfo.getItemsId());
//			items.setItemInfo(itemInfo);
//			aelItemsDao.update(items);
//		}else{
//			Status status = itemInfo.getItemStatus();
//			if(status.equals(Status.UNCOMMITTED) ||
//					status.equals(Status.AUDIT_UNPASS)||
//					   status.equals(Status.HANGUP)){
//				itemInfo.setItemStatus(Status.COMMITTED);
//			}
//		}
//		aelItemInfoDao.update(itemInfo);
//		return isProcessEnded;
//	}
//
//	public void backProcess(Integer childItemId,String sysType,CmsUser user, AelItemReview itemReview){
//		//调用父类驳回接口
//		super.backProcess(childItemId, sysType);
//		//更新事项子表状态为驳回
//		AelItemInfo itemInfo = aelItemInfoDao.findById(childItemId);
//		itemInfo.setItemStatus(BaseItemInfo.Status.AUDIT_UNPASS);
//		//aelItemInfoDao.update(itemInfo);
//		//审批流水记录表添加审批记录
//		itemInfo = this.saveReviewInfo(itemInfo, user, itemReview);
//		aelItemInfoDao.update(itemInfo);
//	}

//	public void suspendProcess(Integer childItemId,String sysType,CmsUser user, AelItemReview itemReview){
//		//调用父类接口
//		super.suspendProcess(childItemId, sysType);
//		//审批流水记录表添加审批记录
//		AelItemInfo itemInfo = aelItemInfoDao.findById(childItemId);
//		itemInfo.setItemStatus(BaseItemInfo.Status.HANGUP);
//		//更新事项子表状态为挂起
//		this.saveReviewInfo(itemInfo, user, itemReview);
//		aelItemInfoDao.update(itemInfo);
//	}
	
	/**
	 * 添加审核记录
	 * @param itemInfo
	 * 		权力清单
	 * @param user
	 * 		用户
	 * @param itemReview
	 * 		审核信息
	 */
//	private AelItemInfo saveReviewInfo(AelItemInfo itemInfo, CmsUser user, AelItemReview itemReview){
//		Set<AelItemReview> reviews = itemInfo.getReviewItems();
//		if(reviews == null){
//			reviews = new HashSet<AelItemReview>();
//		}
//		if(itemReview == null){
//			itemReview = new AelItemReview();
//			itemReview.setItemInfo(itemInfo);
//			itemReview.setReviewType(ReviewStatus.COMMITTED.getStringValue());
//		}
//		itemReview.setUser(user);
//		itemReview.setCreateDate(new Date());
//		reviews.add(itemReview);
//		itemInfo.setReviewItems(reviews);
//		return itemInfo;
//	}
	
	/**
	 * 启动流程
	 * @param childItemId
	 * @param sysType
	 * @return 流程实例
	 */
//	public ProcessInstance startProcess(Integer childItemId,String sysType, CmsUser user){
//		Map<String,Object> variables = new HashMap<String,Object>();
//		String areaId = user.getArea().getCode().substring(0, 5);
//		String userType = user.getUserType();
//		variables.put("childItemId", childItemId);
//		variables.put("sysType", sysType);
//		variables.put("areaId", areaId);
//		variables.put("userType", userType);
//		return super.startProcess(variables);
//	}
	
	/**
	 * 查询是否已经启动任务
	 * @param childItemId
	 * @param sysType
	 * @return
	 */
	public boolean isTaskExist(String childItemId,String sysType){
		JSONObject rst = service.findTaskByVariable(childItemId, sysType);
		List<UserVo> list = FastJsonUtil.getList(rst.getJSONArray("result").toJSONString(), UserVo.class);

		if(service.findTaskByVariable(childItemId, sysType) != null){
			return true;
		}else{
			return false;
		}
	}
}
