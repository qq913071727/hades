package cn.conac.rc.gateway.modules.ofs.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class VHistApprovalVo implements Serializable {

	private static final long serialVersionUID = 1473775241900186890L;

	@ApiModelProperty("当前分页")
	private Integer page;
	
	@ApiModelProperty("每页个数")
	private Integer size;

	@ApiModelProperty("部门机构主表ID")
	private String orgId;

	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
}
