package cn.conac.rc.gateway.modules.user.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.user.vo.PermVo;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-USER")
public interface PermService {
	
	/**
	 * 通过条件取得权限树信息
	 * @param permVo 查询权限信息对象
	 * @return JSONObject
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/perm/tree")
	public JSONObject findPermTreeByCondtion(@ApiParam(value = "查询条件对象", required = true) @RequestBody PermVo permVo);
	
	
	/**
	 * 通过条件取得权限列表信息
	 * @param permVo 查询权限信息对象
	 * @return JSONObject
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/perm/list")
	public JSONObject findPermListByCondtion(@ApiParam(value = "查询条件对象", required = true) @RequestBody PermVo permVo);
	
}
