package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.entity.ApprovalEntity;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface ApprovalService {
	
	/**
	 * 通过批文ID获取批文信息
	 * @param id
	 * @return JSONObject
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/approval/{id}")
	public JSONObject appDetail(@PathVariable(value = "id") String id);
	
	/**
	 * 通过机构ID获取机构的批文列表
	 * @param orgId
	 * @return JSONObject
	 */
    @RequestMapping(method = RequestMethod.GET, value = "/approval/list/{orgId}")
    public JSONObject appList(@PathVariable(value = "orgId") String orgId);
	
	/**
	 * 通过机构ID获取机构的批文列表
	 * @param orgId
	 * @return JSONObject
	 */
    @RequestMapping(method = RequestMethod.GET, value = "/approval/main/{orgId}/list")
    public JSONObject appMainList(@PathVariable(value = "orgId") String orgId);
    
    /**
     * 机构对应批文的新增或者更新
     * @param approval
     * @param submitType  （1： 正常保存 0： 暂存）
     * @return JSONObject
     */
    @RequestMapping(method = RequestMethod.POST, value = "/approval/{submitType}")
    public JSONObject saveOrUpdate(@PathVariable("submitType") String submitType, @ApiParam(value = "保存对象", required = true) @RequestBody ApprovalEntity approvalEntity);


//    /**
//     * 批文的逻辑删除
//     * @param id
//     * @return JSONObject
//     */
//    @RequestMapping(method = RequestMethod.POST, value = "/approval/{id}/uDelete")
//    public JSONObject uDeleteApp(@ApiParam(value = "id", required = true) @PathVariable("id") String id);

    /**
     * 三定查询之批文查询
     * @param id
     * @return JSONObject
     */
    @RequestMapping(method = RequestMethod.POST, value = "/orgsApp/list")
    public JSONObject ofsApprovalList(@ApiParam(value = "查询条件对象", required = true) @RequestBody JSONObject approvalVo);
    
    
    /**
	 * 校验新添加批文时间是否在新建部门批文时间之后(部门管理批文时间校验)
	 * @param orgId
	 * @param newAppDate
	 * @return JSONObject
	 */
    @RequestMapping(method = RequestMethod.GET, value = "/approval/{orgId}/checkAppDateAfter/{newAppDate}")
    public JSONObject checkAppDateAfter(@PathVariable(value = "orgId") Integer orgId, @PathVariable(value = "newAppDate") String newAppDate);
    
    
	/**
	 * 校验新添加批文时间是否在新建部门批文时间之前(历史沿革批文时间校验)
	 * @param orgId
	 * @param newAppDate
	 * @return JSONObject
	 */
    @RequestMapping(method = RequestMethod.GET, value = "/approval/{orgId}/checkAppDateBefore/{newAppDate}")
    public JSONObject checkAppDateBefore(@PathVariable(value = "orgId") Integer orgId, @PathVariable(value = "newAppDate") String newAppDate);
    
}
