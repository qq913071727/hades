package cn.conac.rc.gateway.modules.ofs.vo;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class AnalzWarningVo {


	@ApiModelProperty("当前分页")
	private Integer page;
	
	@ApiModelProperty("每页个数")
	private Integer size;

	
	@ApiModelProperty("创建 开始时间")
	private Date createDateStart;

	
	@ApiModelProperty("创建 结束时间")
	private Date createDateEnd;

	
	@ApiModelProperty("更新 开始时间")
	private Date updateDateStart;
	
	@ApiModelProperty("更新 结束时间")
	private Date updateDateEnd;

	@ApiModelProperty("warningDept")
	private Boolean warningDept;

	@ApiModelProperty("areaCode")
	private String areaCode;
	
	@ApiModelProperty("warningDuty")
	private Boolean warningDuty;

	@ApiModelProperty("warningEmpRemarks")
	private Boolean warningEmpRemarks;

	@ApiModelProperty("warningOnlineName")
	private Boolean warningOnlineName;

	@ApiModelProperty("warningDegree")
	private Integer warningDegree;

	@ApiModelProperty("haveToDelete")
	private Integer haveToDelete;
	
	@ApiModelProperty("id")
	private String id;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setWarningDept(Boolean warningDept){
		this.warningDept=warningDept;
	}

	public Boolean getWarningDept(){
		return warningDept;
	}

	public void setWarningDuty(Boolean warningDuty){
		this.warningDuty=warningDuty;
	}

	public Boolean getWarningDuty(){
		return warningDuty;
	}

	public void setWarningEmpRemarks(Boolean warningEmpRemarks){
		this.warningEmpRemarks=warningEmpRemarks;
	}

	public Boolean getWarningEmpRemarks(){
		return warningEmpRemarks;
	}

	public void setWarningOnlineName(Boolean warningOnlineName){
		this.warningOnlineName=warningOnlineName;
	}

	public Boolean getWarningOnlineName(){
		return warningOnlineName;
	}

	public void setWarningDegree(Integer warningDegree){
		this.warningDegree=warningDegree;
	}

	public Integer getWarningDegree(){
		return warningDegree;
	}

	public void setHaveToDelete(Integer haveToDelete){
		this.haveToDelete=haveToDelete;
	}

	public Integer getHaveToDelete(){
		return haveToDelete;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	
	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}

	public void setCreateDateStart(Date createDateStart){
		this.createDateStart=createDateStart;
	}

	public Date getCreateDateStart(){
		return createDateStart;
	}

	public void setCreateDateEnd(Date createDateEnd){
		this.createDateEnd=createDateEnd;
	}

	public Date getCreateDateEnd(){
		return createDateEnd;
	}

	public void setUpdateDateStart(Date updateDateStart){
		this.updateDateStart=updateDateStart;
	}

	public Date getUpdateDateStart(){
		return updateDateStart;
	}

	public void setUpdateDateEnd(Date updateDateEnd){
		this.updateDateEnd=updateDateEnd;
	}

	public Date getUpdateDateEnd(){
		return updateDateEnd;
	}

}
