package cn.conac.rc.gateway.modules.ofs.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * OrgEmpEntity类
 *
 * @author beanCreator
 * @date 2016-11-01
 * @version 1.0
 */
@ApiModel
public class OrgEmpEntity implements Serializable {

	private static final long serialVersionUID = 221600489364480782L;

	@ApiModelProperty("ID")
	private String id;

	@ApiModelProperty("编制总数")
	private String empNum;

	@ApiModelProperty("行政编制数")
	private String polNum;

	@ApiModelProperty("工勤编制")
	private String workNum;

	@ApiModelProperty("其他编制数")
	private String otherNum;

	@ApiModelProperty("机构领导职数总数")
	private String orgLeaderNum;

	@ApiModelProperty("机构领导职数正职数")
	private String orgMleaderNum;

	@ApiModelProperty("机构领导职数副职数")
	private String orgSleaderNum;

	@ApiModelProperty("编制总数描述")
	private String empRemarks;

	@ApiModelProperty("行政编制数描述")
	private String polRemarks;

	@ApiModelProperty("工勤编制描述")
	private String workRemarks;

	@ApiModelProperty("其他编制数描述")
	private String otherRemarks;

	@ApiModelProperty("部门领导职数总数")
	private String depLeaderNum;

	@ApiModelProperty("部门领导职数正职")
	private String depMleaderNum;

	@ApiModelProperty("部门领导职数副职")
	private String depSleaderNum;

	@ApiModelProperty("机构领导职数高配机构总计")
	private String orgLeaderHighNum;

	@ApiModelProperty("内设机构领导职数高配部门总计")
	private String depLeaderHighNum;

	@ApiModelProperty("机构领导职数描述")
	private String orgLeaderRemarks;

	@ApiModelProperty("机构领导职数正职数描述")
	private String orgMleaderRemarks;

	@ApiModelProperty("机构领导职数副职数描述")
	private String orgSleaderRemarks;

	@ApiModelProperty("部门领导职数正职描述")
	private String depMleaderRemarks;

	@ApiModelProperty("部门领导职数副职描述")
	private String depSleaderRemarks;

	@ApiModelProperty("机构领导职数高配机构描述")
	private String orgLeaderHighRemarks;

	@ApiModelProperty("领导职数高配内设机构描述")
	private String depLeaderHighRemarks;

	@ApiModelProperty("depLeaderOtherNum")
	private String depLeaderOtherNum;

	@ApiModelProperty("全额拨款编制数")
	private String fullAppropriationNum;

	@ApiModelProperty("差额拨款编制数")
	private String balanceAppropriationNum;

	@ApiModelProperty("经费自理编制数")
	private String fundSelfCareNum;

	@ApiModelProperty("fullAppropriationRemarks")
	private String fullAppropriationRemarks;

	@ApiModelProperty("balanceAppropriationRemarks")
	private String balanceAppropriationRemarks;

	@ApiModelProperty("fundSelfCareRemarks")
	private String fundSelfCareRemarks;

	@ApiModelProperty("部门领导职数描述")
	private String depLeaderRemarks;

	@ApiModelProperty("depLeaderOtherRemarks")
	private String depLeaderOtherRemarks;

	public void setId(String id){
		this.id=id;
	}

	public String getId(){
		return id;
	}

	public void setEmpNum(String empNum){
		this.empNum=empNum;
	}

	public String getEmpNum(){
		return empNum;
	}

	public void setPolNum(String polNum){
		this.polNum=polNum;
	}

	public String getPolNum(){
		return polNum;
	}

	public void setWorkNum(String workNum){
		this.workNum=workNum;
	}

	public String getWorkNum(){
		return workNum;
	}

	public void setOtherNum(String otherNum){
		this.otherNum=otherNum;
	}

	public String getOtherNum(){
		return otherNum;
	}

	public void setOrgLeaderNum(String orgLeaderNum){
		this.orgLeaderNum=orgLeaderNum;
	}

	public String getOrgLeaderNum(){
		return orgLeaderNum;
	}

	public void setOrgMleaderNum(String orgMleaderNum){
		this.orgMleaderNum=orgMleaderNum;
	}

	public String getOrgMleaderNum(){
		return orgMleaderNum;
	}

	public void setOrgSleaderNum(String orgSleaderNum){
		this.orgSleaderNum=orgSleaderNum;
	}

	public String getOrgSleaderNum(){
		return orgSleaderNum;
	}

	public void setEmpRemarks(String empRemarks){
		this.empRemarks=empRemarks;
	}

	public String getEmpRemarks(){
		return empRemarks;
	}

	public void setPolRemarks(String polRemarks){
		this.polRemarks=polRemarks;
	}

	public String getPolRemarks(){
		return polRemarks;
	}

	public void setWorkRemarks(String workRemarks){
		this.workRemarks=workRemarks;
	}

	public String getWorkRemarks(){
		return workRemarks;
	}

	public void setOtherRemarks(String otherRemarks){
		this.otherRemarks=otherRemarks;
	}

	public String getOtherRemarks(){
		return otherRemarks;
	}

	public void setDepLeaderNum(String depLeaderNum){
		this.depLeaderNum=depLeaderNum;
	}

	public String getDepLeaderNum(){
		return depLeaderNum;
	}

	public void setDepMleaderNum(String depMleaderNum){
		this.depMleaderNum=depMleaderNum;
	}

	public String getDepMleaderNum(){
		return depMleaderNum;
	}

	public void setDepSleaderNum(String depSleaderNum){
		this.depSleaderNum=depSleaderNum;
	}

	public String getDepSleaderNum(){
		return depSleaderNum;
	}

	public void setOrgLeaderHighNum(String orgLeaderHighNum){
		this.orgLeaderHighNum=orgLeaderHighNum;
	}

	public String getOrgLeaderHighNum(){
		return orgLeaderHighNum;
	}

	public void setDepLeaderHighNum(String depLeaderHighNum){
		this.depLeaderHighNum=depLeaderHighNum;
	}

	public String getDepLeaderHighNum(){
		return depLeaderHighNum;
	}

	public void setOrgLeaderRemarks(String orgLeaderRemarks){
		this.orgLeaderRemarks=orgLeaderRemarks;
	}

	public String getOrgLeaderRemarks(){
		return orgLeaderRemarks;
	}

	public void setOrgMleaderRemarks(String orgMleaderRemarks){
		this.orgMleaderRemarks=orgMleaderRemarks;
	}

	public String getOrgMleaderRemarks(){
		return orgMleaderRemarks;
	}

	public void setOrgSleaderRemarks(String orgSleaderRemarks){
		this.orgSleaderRemarks=orgSleaderRemarks;
	}

	public String getOrgSleaderRemarks(){
		return orgSleaderRemarks;
	}

	public void setDepMleaderRemarks(String depMleaderRemarks){
		this.depMleaderRemarks=depMleaderRemarks;
	}

	public String getDepMleaderRemarks(){
		return depMleaderRemarks;
	}

	public void setDepSleaderRemarks(String depSleaderRemarks){
		this.depSleaderRemarks=depSleaderRemarks;
	}

	public String getDepSleaderRemarks(){
		return depSleaderRemarks;
	}

	public void setOrgLeaderHighRemarks(String orgLeaderHighRemarks){
		this.orgLeaderHighRemarks=orgLeaderHighRemarks;
	}

	public String getOrgLeaderHighRemarks(){
		return orgLeaderHighRemarks;
	}

	public void setDepLeaderHighRemarks(String depLeaderHighRemarks){
		this.depLeaderHighRemarks=depLeaderHighRemarks;
	}

	public String getDepLeaderHighRemarks(){
		return depLeaderHighRemarks;
	}

	public void setDepLeaderOtherNum(String depLeaderOtherNum){
		this.depLeaderOtherNum=depLeaderOtherNum;
	}

	public String getDepLeaderOtherNum(){
		return depLeaderOtherNum;
	}

	public void setFullAppropriationNum(String fullAppropriationNum){
		this.fullAppropriationNum=fullAppropriationNum;
	}

	public String getFullAppropriationNum(){
		return fullAppropriationNum;
	}

	public void setBalanceAppropriationNum(String balanceAppropriationNum){
		this.balanceAppropriationNum=balanceAppropriationNum;
	}

	public String getBalanceAppropriationNum(){
		return balanceAppropriationNum;
	}

	public void setFundSelfCareNum(String fundSelfCareNum){
		this.fundSelfCareNum=fundSelfCareNum;
	}

	public String getFundSelfCareNum(){
		return fundSelfCareNum;
	}

	public void setFullAppropriationRemarks(String fullAppropriationRemarks){
		this.fullAppropriationRemarks=fullAppropriationRemarks;
	}

	public String getFullAppropriationRemarks(){
		return fullAppropriationRemarks;
	}

	public void setBalanceAppropriationRemarks(String balanceAppropriationRemarks){
		this.balanceAppropriationRemarks=balanceAppropriationRemarks;
	}

	public String getBalanceAppropriationRemarks(){
		return balanceAppropriationRemarks;
	}

	public void setFundSelfCareRemarks(String fundSelfCareRemarks){
		this.fundSelfCareRemarks=fundSelfCareRemarks;
	}

	public String getFundSelfCareRemarks(){
		return fundSelfCareRemarks;
	}

	public void setDepLeaderRemarks(String depLeaderRemarks){
		this.depLeaderRemarks=depLeaderRemarks;
	}

	public String getDepLeaderRemarks(){
		return depLeaderRemarks;
	}

	public void setDepLeaderOtherRemarks(String depLeaderOtherRemarks){
		this.depLeaderOtherRemarks=depLeaderOtherRemarks;
	}

	public String getDepLeaderOtherRemarks(){
		return depLeaderOtherRemarks;
	}

}
