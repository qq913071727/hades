package cn.conac.rc.gateway.modules.share.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.base.ResultPojo;
import cn.conac.rc.gateway.common.entity.AreaEntity;
import cn.conac.rc.gateway.modules.ofs.vo.OrganizationVo;
import cn.conac.rc.gateway.modules.share.entity.ShareAreaInfoEntity;
import cn.conac.rc.gateway.modules.share.service.ShareAreaInfoService;
import cn.conac.rc.gateway.modules.share.vo.ShareAreaInfoCUVo;
import cn.conac.rc.gateway.modules.share.vo.ShareAreaInfoMsgVo;
import cn.conac.rc.gateway.modules.share.vo.ShareAreaInfoParamVo;
import cn.conac.rc.gateway.security.UserUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value={"share/"} )
@Api(tags = "地区共享信息", description = "共享地区")
public class ShareAreaInfoController {
	
	@Autowired
	ShareAreaInfoService  shareAreaInfoService;
	
	@ApiOperation(value = "共享地区申请", httpMethod = "POST", response = OrganizationVo.class, notes = "共享地区申请")
	@RequestMapping(value = "areaInfos/c", method = RequestMethod.POST)
	public Object cudShareAreaInfoSave(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "保存对象", required = true) @RequestBody  ShareAreaInfoParamVo shareAreaInfoParamVo) throws Exception {
		ResultPojo resultInfo = new ResultPojo();
		// 共享地区申请参数的合法性检查
		if(null == shareAreaInfoParamVo.getAreaDestList() || shareAreaInfoParamVo.getAreaDestList().size() == 0) {
			// 共享的地区为空的场合
			resultInfo.setCode(ResultPojo.CODE_FAILURE);
			resultInfo.setMsg("要共享地区列表不能为null或为空");
			resultInfo.setResult(null);
			return resultInfo;
		}
		AreaEntity areaEntity = null;
		for(int j=0; j < shareAreaInfoParamVo.getAreaDestList().size(); j++) {
			areaEntity = shareAreaInfoParamVo.getAreaDestList().get(j);
			if(StringUtils.isBlank(areaEntity.getCode())) {
				resultInfo.setCode(ResultPojo.CODE_FAILURE);
				resultInfo.setMsg("要共享地区代码不能为null或为空");
				resultInfo.setResult(null);
				return resultInfo;
			}
		}
		String currAreaCode = UserUtils.getCurrentUser().getAreaCode();
		JSONObject jsonSharedAreaInfoObj =null;
		ShareAreaInfoMsgVo shareAreaInfoMsgVoTmp = null;
		List<ShareAreaInfoMsgVo> invalidShareAreaInfoMsgVoList = new ArrayList<ShareAreaInfoMsgVo>();
		int findFlag = 0;
		// 取得已经共享的地区信息
	    // 检查该共享地区的地区编码是否在共享范围内并且是否在有效期限内
		JSONObject sharedAreaInfoJson = shareAreaInfoService.findSharedAreaInfoList(currAreaCode);
		if (ResultPojo.CODE_SUCCESS.equals(sharedAreaInfoJson.getString("code")) ) {
			JSONArray  sharedAreaInfoArray =  sharedAreaInfoJson.getJSONArray("result");
			for (int k=0; k < sharedAreaInfoArray.size(); k++) {
				findFlag = 0;
				jsonSharedAreaInfoObj = sharedAreaInfoArray.getJSONObject(k);
				shareAreaInfoMsgVoTmp = JSONObject.toJavaObject(jsonSharedAreaInfoObj, ShareAreaInfoMsgVo.class);
				for(int cnt=0; cnt < shareAreaInfoParamVo.getAreaDestList().size(); cnt++) {
					areaEntity = shareAreaInfoParamVo.getAreaDestList().get(cnt);
					if(shareAreaInfoMsgVoTmp.getAreaCode().equals(areaEntity.getCode())) {
						findFlag = 1;
						break;
					}
				}
				if(findFlag == 1) {
					invalidShareAreaInfoMsgVoList.add(shareAreaInfoMsgVoTmp);
				}
			}
		}
		if(invalidShareAreaInfoMsgVoList.size() > 0) {
			// 共享的地区为空的场合
			resultInfo.setCode(ResultPojo.RET_CODE_SHARE_AREA_ERR_5002);
			resultInfo.setMsg(ResultPojo.RET_MSG_SHARE_AREA_ERR_5002);
			resultInfo.setResult(invalidShareAreaInfoMsgVoList);
			return resultInfo;
		}
		ShareAreaInfoEntity  shareAreaInfoEntity = null;
		List<ShareAreaInfoEntity> shareAreaInfoEntityList = new ArrayList<ShareAreaInfoEntity>();
		for(int i=0; i < shareAreaInfoParamVo.getAreaDestList().size(); i++) {
			areaEntity = shareAreaInfoParamVo.getAreaDestList().get(i);
			shareAreaInfoEntity = new ShareAreaInfoEntity();
			shareAreaInfoEntity.setUserId(Integer.valueOf(UserUtils.getCurrentUser().getId()));
			shareAreaInfoEntity.setMobile(shareAreaInfoParamVo.getMobile());
			shareAreaInfoEntity.setFullName(shareAreaInfoParamVo.getFullName());
			shareAreaInfoEntity.setAreaCodeDest(areaEntity.getCode());
			shareAreaInfoEntity.setAreaDestNameRe(areaEntity.getName());
			shareAreaInfoEntity.setAreaCodeSource(UserUtils.getCurrentUser().getAreaCode());
			shareAreaInfoEntity.setAreaSourceNameRe(UserUtils.getCurrentUser().getAreaName());
			shareAreaInfoEntity.setCreateTime(new Date());
			shareAreaInfoEntity.setOrgDomainNameRe(UserUtils.getCurrentUser().getOrgDomainName());
			shareAreaInfoEntity.setStatus(ShareAreaInfoEntity.SHARE_AREA_INFO_STATUS_AUDITING);
			shareAreaInfoEntityList.add(shareAreaInfoEntity);
		}
		
		ShareAreaInfoCUVo shareAreaInfoCUVo = new ShareAreaInfoCUVo();
		shareAreaInfoCUVo.setShareAreaInfoEntityList(shareAreaInfoEntityList);
		
		return shareAreaInfoService.batchSaveOrUpdate(shareAreaInfoCUVo);
	}
}
