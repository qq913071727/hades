package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.vo.OrgChangeVo;
import cn.conac.rc.gateway.modules.ofs.vo.OrgRevVo;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface OrgCommonService {

	@RequestMapping(value = "orgCommon/{baseId}/delete", method = RequestMethod.POST)
	public JSONObject delete(@PathVariable("baseId") String baseId);
	
	@RequestMapping(value = "orgCommon/audited/{orgId}/delete", method = RequestMethod.POST)
	public JSONObject orgAuditeddelete(@PathVariable("orgId") String orgId);
	
	
	@RequestMapping(value = "orgCommon/change", method = RequestMethod.POST)
	public JSONObject orgChange(@ApiParam(value = "部门状态VO", required = true) @RequestBody OrgChangeVo orgChangeVo);
	
	
	@RequestMapping(value = "orgCommon/{orgId}/{bianBanFlag}/rev/save", method = RequestMethod.POST)
	public JSONObject orgRevSave(@PathVariable("orgId") String orgId, @PathVariable("bianBanFlag") String bianBanFlag, @ApiParam(value = "部门撤并Vo", required = true) @RequestBody OrgRevVo orgRevVo);
	
	@RequestMapping(value = "orgCommon/{baseId}/rev/update", method = RequestMethod.POST)
	public JSONObject orgRevUpdate(@PathVariable("baseId") String baseId, @ApiParam(value = "部门撤并Vo", required = true) @RequestBody OrgRevVo orgRevVo);
	
	@RequestMapping(value = "orgCommon/{orgId}/check", method = RequestMethod.GET)
	public JSONObject orgCheck(@PathVariable("orgId") Integer orgId);
	
}
