package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.entity.OrgOthersEntity;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface OrgOthersService {
	
	@RequestMapping(method = RequestMethod.GET, value = "/orgOthers/{id}")
	 public JSONObject findById(@PathVariable(value = "id") String baseId);
	
	 /**
     * 其他事项及附则的新增或者更新
     * @param orgOthersEntity
     * @param submitType  （1： 正常保存 0： 暂存）
     * @return JSONObject
     */
    @RequestMapping(method = RequestMethod.POST, value = "/orgOthers/{submitType}")
    public JSONObject saveOrUpdate(@PathVariable("submitType") String submitType, @ApiParam(value = "保存对象", required = true) @RequestBody  OrgOthersEntity orgOthersEntity);
}
