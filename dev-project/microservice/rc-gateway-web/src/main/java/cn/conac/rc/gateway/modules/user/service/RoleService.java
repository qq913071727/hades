package cn.conac.rc.gateway.modules.user.service;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.user.vo.PermVo;
import cn.conac.rc.gateway.modules.user.vo.RoleInfoVo;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-USER")
public interface RoleService {
	
	/**
	 * 获取角色信息列表
	 * @return JSONObject
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/role/list")
	public JSONObject findRoleList();
	
	/**
	 * 更新用户表的是否删除字段(逻辑删除用户信息)
	 * @param roleId
	 * @return 更新结果
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/role/delete")
	public JSONObject deletedByBatch(@ApiParam(value = "封装接收页面选择角色ID的对象", required = true) @RequestBody List<Integer> roleIdList);
	
	
	/**
	 * 根据角色ID获取角色详情
	 * @param roleId
	 * @return 更新结果
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/role/{roleId}")
	public JSONObject findByRoleId(@PathVariable(value = "roleId") Integer roleId);
	
	/**
	 * 根据角色ID获取角色及权限树信息
	 * @param roleId
	 * @param permVo
	 * @return 更新结果
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/role/{roleId}/perm/tree")
	public JSONObject findPermTreeByRoleId(@PathVariable(value = "roleId") Integer roleId,@ApiParam(value = "查询条件对象", required = true) @RequestBody PermVo permVo);
	
	/**
	 * 保存角色信息
	 * @param RoleInfoVo 封装接收页面相关角色对象
	 * @return JSONObject
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/roleComm/save")
	public JSONObject saveRoleInfo(@ApiParam(value = "封装接收页面相关角色对象", required = true) @RequestBody RoleInfoVo roleInfoVo);
	
	/**
	 * 更新角色信息
	 * @param RoleInfoVo 封装接收页面相关角色对象
	 * @return JSONObject
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/roleComm/{roleId}/update")
	public JSONObject updateRoleInfo(@PathVariable(value = "roleId") Integer roleId, @ApiParam(value = "封装接收页面相关角色对象", required = true) @RequestBody  RoleInfoVo roleInfoVo);
	
    
}
