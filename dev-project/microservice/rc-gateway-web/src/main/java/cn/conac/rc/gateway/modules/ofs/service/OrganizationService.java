package cn.conac.rc.gateway.modules.ofs.service;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.entity.OrganizationEntity;
import cn.conac.rc.gateway.modules.ofs.vo.OrgLibInfoVo;
import cn.conac.rc.gateway.modules.ofs.vo.OrganizationVo;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
@CacheConfig(keyGenerator = "keyGenerator", cacheNames = "ofs_cache")
public interface OrganizationService {

	@RequestMapping(value = "organization/recentOrgs", method = RequestMethod.GET)
	public JSONObject findRecentOrgs4App(@RequestParam("num") int num, @RequestParam("areaCode") String areaCode);

	@RequestMapping(value = "organization/{id}", method = RequestMethod.GET)
	public JSONObject findById(@PathVariable("id") String id);

	@RequestMapping(value = "orgLibInfo/{id}", method = RequestMethod.GET)
	public JSONObject findOrgLibById(@PathVariable("id") String id);
	
	@RequestMapping(value = "organization/count", method = RequestMethod.POST)
	public JSONObject count(@RequestBody OrganizationVo vo);

	@RequestMapping(value = "organization/list", method = RequestMethod.POST)
	public JSONObject list(@RequestBody OrganizationVo vo);
	
	@RequestMapping(value = "orgLibInfo/list", method = RequestMethod.POST)
	public JSONObject findOrgLibList(@RequestBody OrgLibInfoVo vo);

	@RequestMapping(value = "organization/", method = RequestMethod.POST)
	public JSONObject saveOrUpdate(@ApiParam(value = "保存部门库", required = true) @RequestBody JSONObject orgInfo);

	@RequestMapping(value = "organization/{id}/delete", method = RequestMethod.POST)
	public JSONObject delete(@PathVariable("id") String id);

	// 删除多个部门库
	@RequestMapping(value = "organization/delete/Organization/{ids}", method = RequestMethod.POST)
	public JSONObject deleteOrganizationsByIds(@RequestParam(value = "param", required = false) String param);

	// 根据参数查询部门库
	@RequestMapping(value = "organization/Org", method = RequestMethod.GET)
	public JSONObject findOrgByParam(@RequestBody OrganizationVo vo);

	// 根据父部门标识查询第一级子部门库
	@RequestMapping(value = "organization/ChildOrgs/{parentId}", method = RequestMethod.GET)
	public JSONObject findChildOrgs(@PathVariable("parentId") String parentId);

	// 部门库赋码
	@RequestMapping(value = "rcOrgEncodeRecord/{aCode}/{bCode}", method = RequestMethod.POST)
	public JSONObject encode(
				@ApiParam(value = "地区码", required = true)  @PathVariable("aCode") String aCode,  
				@ApiParam(value = "基本码", required = true)  @PathVariable("bCode") String bCode);

	// 部门库调整顺序
	@RequestMapping(value = "organization/order", method = RequestMethod.POST)
	public JSONObject sortOrg( @RequestBody Map<String, Object> param);

	// 查询九大系统
	@RequestMapping(value = "organization/findOwnSys", method = RequestMethod.POST)
	public JSONObject findOwnSys();

	// 统计地区的事业单位和行政机关
	@Cacheable
	@RequestMapping(value = "organization/CountXZJGAndSYDW/{areaCode}", method = RequestMethod.GET)
	public JSONObject CountXZJGAndSYDW(@PathVariable("areaCode") String areaCode);

	// 统计地区的事业单位和行政机关
	@RequestMapping(value = "organization/orgTree/{areaCode}", method = RequestMethod.GET)
	public JSONObject findOrgTree(@PathVariable("areaCode") String areaCode);
	
	// 查询已共享地区的事业单位和行政机关
	@RequestMapping(value = "organization/shared/orgTree/{areaCode}", method = RequestMethod.GET)
	public JSONObject findSharedOrgTree(@PathVariable("areaCode") String areaCode);
	
	
	// 根据id获取机构树信息
	@RequestMapping(value = "organization/mapTree/{id}", method = RequestMethod.GET)
	 public JSONObject findMapTreeById(@PathVariable("id") String id);


	// 统计地区的事业单位和行政机关
	@RequestMapping(value = "organizationBase/orgnization/{orgId}", method = RequestMethod.GET)
	public JSONObject findOrgDetailByOrgId(@PathVariable("orgId") String orgId);
	
	// 根据用户Id查询部门树
	@RequestMapping(value = "organization/{userId}/orgTree", method = RequestMethod.GET)
	public JSONObject findOrgTreeByUserId(@PathVariable("userId") Integer userId);
	
	// 部门库调整顺序
	@RequestMapping(value = "organization/check/name", method = RequestMethod.POST)
	public JSONObject checkOrgName( @RequestBody Map<String, Object> condMap);
	
	@RequestMapping(value = "organization/batchSaveOrUpdate", method = RequestMethod.POST)
	public JSONObject saveOrUpdateOrgInfoList(@ApiParam(value = "批量保存或更新部门库", required = true) @RequestBody List<OrganizationEntity> orgInfoList);
	
}
