package cn.conac.rc.gateway.modules.common.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.conac.rc.framework.utils.ResultPojo;

@FeignClient("RC-SERVICE-COMMON")
public interface AreaService {

    @RequestMapping(value = "/area/pid/{pid}/{lvl}", method = RequestMethod.GET)
    public ResultPojo getAreaByPid(@PathVariable("pid") String pid, @PathVariable("lvl") String lvl);
    
    @RequestMapping(value = "/area/{id}/subs", method = RequestMethod.GET)
    public ResultPojo getSubAreas(@PathVariable("id") String id);

    @RequestMapping(value = "/area/{id}", method = RequestMethod.GET)
    public ResultPojo detail(@PathVariable("id") String id);

}