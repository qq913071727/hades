package cn.conac.rc.gateway.modules.ofs.service;

import io.swagger.annotations.ApiParam;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

@FeignClient("RC-SERVICE-CODE")
public interface CodeService {

//	// 部门库赋码
//	@RequestMapping(value = "rcOrgEncodeRecord/{aCode}/{bCode}/{orgType}", method = RequestMethod.POST)
//	public JSONObject encode(
//				@ApiParam(value = "地区码", required = true)  @PathVariable("aCode") String aCode,  
//				@ApiParam(value = "基本码", required = true)  @PathVariable("bCode") String bCode,
//				@ApiParam(value = "orgType", required = true) @PathVariable("orgType") String orgType);
//	
//	
//    /**
//	 * 根据ID删除编码信息
//	 * @param id
//	 * @return
//	 */
//    @RequestMapping(method = RequestMethod.POST, value = "/rcOrgEncodeRecord/{findId}/delete")
//    JSONObject deleteByFindId(@ApiParam(value = "删除对象的F_ID", required = true) @PathVariable(value = "findId") String findId);
//    
//    
//    /**
//	 * 根据ID查找编码信息
//	 * @param id
//	 * @return
//	 */
//    @RequestMapping(method = RequestMethod.GET, value = "/rcOrgEncodeRecord/{findId}")
//    JSONObject findByFindId(@ApiParam(value = "对象的F_ID", required = true) @PathVariable(value = "findId") String findId);
	/**
	 * 生成人员编制码
	 * @param orgCode
	 * @param empType
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "rcOrgEncodeRecord/encode/R/{orgCode}/{empType}")
	JSONObject createEmpCode(@ApiParam(value = "9位主体标识代码", required = true) @PathVariable("orgCode") String orgCode,
			                 @ApiParam(value = "1位人员编制类型码", required = true) @PathVariable("empType") String empType);
	
	/**
	 * 人员编制码回收
	 * @param empEncode
	 * @return
	 */
	@RequestMapping(value = "rcOrgEncodeRecord/encode/remove/R/{empEncode}", method = RequestMethod.POST)
	public JSONObject removeEmpCode(@ApiParam(value = "16位人员编码", required = true) @PathVariable("empEncode") String empEncode);

}
