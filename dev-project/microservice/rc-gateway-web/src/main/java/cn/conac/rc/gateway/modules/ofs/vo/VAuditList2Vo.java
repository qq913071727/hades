package cn.conac.rc.gateway.modules.ofs.vo;

import java.util.Date;


import cn.conac.rc.gateway.modules.ofs.entity.VAuditList2Entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * VAuditList2Vo类
 *
 * @author voCreator
 * @date 2016-12-26
 * @version 1.0
 */
@ApiModel
public class VAuditList2Vo extends VAuditList2Entity {

	private static final long serialVersionUID = -8316120057479353629L;

	@ApiModelProperty("当前分页")
	private Integer page;

	@ApiModelProperty("每页个数")
	private Integer size;

	@ApiModelProperty("创建 开始时间")
	private Date createDateStart;

	@ApiModelProperty("创建 结束时间")
	private Date createDateEnd;

	@ApiModelProperty("更新 开始时间")
	private Date updateDateStart;

	@ApiModelProperty("更新 结束时间")
	private Date updateDateEnd;
	
	@ApiModelProperty("检索用机构名称")
	private String name;
	
	@ApiModelProperty("用户ID和部门库ID是否允许Or操作标示（1：允许 0： 不允许）")
	private Integer orOperFlag;


	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}

	public void setCreateDateStart(Date createDateStart){
		this.createDateStart=createDateStart;
	}

	public Date getCreateDateStart(){
		return createDateStart;
	}

	public void setCreateDateEnd(Date createDateEnd){
		this.createDateEnd=createDateEnd;
	}

	public Date getCreateDateEnd(){
		return createDateEnd;
	}

	public void setUpdateDateStart(Date updateDateStart){
		this.updateDateStart=updateDateStart;
	}

	public Date getUpdateDateStart(){
		return updateDateStart;
	}

	public void setUpdateDateEnd(Date updateDateEnd){
		this.updateDateEnd=updateDateEnd;
	}

	public Date getUpdateDateEnd(){
		return updateDateEnd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrOperFlag() {
		return orOperFlag;
	}

	public void setOrOperFlag(Integer orOperFlag) {
		this.orOperFlag = orOperFlag;
	}
}
