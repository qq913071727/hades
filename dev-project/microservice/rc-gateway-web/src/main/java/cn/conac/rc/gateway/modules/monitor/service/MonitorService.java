package cn.conac.rc.gateway.modules.monitor.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.conac.rc.gateway.modules.monitor.vo.StatisticsVO;

import com.alibaba.fastjson.JSONObject;

/**
 * 监测服务
 * 
 * @author haocm
 *
 */
@FeignClient("RC-SERVICE-MONITOR")
public interface MonitorService {

	@RequestMapping(method = RequestMethod.POST, value = "/monitor/statistics/month", produces = MediaType.APPLICATION_JSON_VALUE)
	JSONObject getStatisticsMonth(@RequestBody StatisticsVO localStatus);
	@RequestMapping(method = RequestMethod.POST, value = "/monitor/statistics/quarter", produces = MediaType.APPLICATION_JSON_VALUE)
	JSONObject getStatisticsQuarter(@RequestBody StatisticsVO localStatus);
	@RequestMapping(method = RequestMethod.POST, value = "/monitor/statistics/year", produces = MediaType.APPLICATION_JSON_VALUE)
	JSONObject getStatisticsYear(@RequestBody StatisticsVO localStatus);
	@RequestMapping(method = RequestMethod.POST, value = "/monitor/statistics/blbm", produces = MediaType.APPLICATION_JSON_VALUE)
	JSONObject getStatisticsBlbm(@RequestBody StatisticsVO localStatus);
	@RequestMapping(method = RequestMethod.POST, value = "/monitor/statistics/blbmsx", produces = MediaType.APPLICATION_JSON_VALUE)
	JSONObject getStatisticsBlbmSx(@RequestBody StatisticsVO localStatus);






	@RequestMapping(method = RequestMethod.POST,value = "/sxzz/list",produces = MediaType.APPLICATION_JSON_VALUE)
	JSONObject getSxzz(@RequestBody StatisticsVO localStatus);
	@RequestMapping(method = RequestMethod.POST, value = "/monitor/statistics/counts", produces = MediaType.APPLICATION_JSON_VALUE)
	JSONObject getStatisticsCounts(@RequestBody StatisticsVO localStatus);
	@RequestMapping(method = RequestMethod.POST, value = "/monitor/statistics/matter/counts", produces = MediaType.APPLICATION_JSON_VALUE)
	JSONObject getMatterCounts(@RequestBody StatisticsVO localStatus);

	@RequestMapping(method = RequestMethod.GET, value = "/monitor/statistics/blbm/{areaCode}")
	JSONObject getBlbmList(@PathVariable(value = "areaCode") String areaCode);
	@RequestMapping(method = RequestMethod.GET, value = "/monitor/statistics/sx/{areaCode}")
	JSONObject getBlbmsxList(@PathVariable(value = "areaCode") String areaCode);
	// @RequestMapping(method = RequestMethod.GET, value =
	// "/monitor/statistics/counts/{department}/{startDate}/{endDate}/{areaCode}")
	// JSONObject getStatisticsCounts(
	// @PathVariable(value = "department") String department,
	// @PathVariable(value = "startDate") String startDate,
	// @PathVariable(value = "endDate") String endDate,
	// @PathVariable(value = "areaCode") String areaCode);
	@RequestMapping(method = RequestMethod.POST, value = "/monitor/statistics/accept", produces = MediaType.APPLICATION_JSON_VALUE)
	JSONObject getAcceptCounts(@RequestBody StatisticsVO acceptQuary);
	@RequestMapping(method = RequestMethod.POST, value = "/weight", produces = MediaType.APPLICATION_JSON_VALUE)
	JSONObject getWeight(@RequestBody StatisticsVO acceptQuary);
	@RequestMapping(method = RequestMethod.POST, value = "/weight/save", produces = MediaType.APPLICATION_JSON_VALUE)
	JSONObject save(@RequestBody StatisticsVO acceptQuary);
	// @RequestMapping(method = RequestMethod.GET, value =
	// "/monitor/statistics/accept/{slzt}/{department}/{startDate}/{endDate}/{areaCode}")
	// JSONObject getAcceptCounts(@PathVariable(value = "slzt") String slzt,
	// @PathVariable(value = "department") String department,
	// @PathVariable(value = "startDate") String startDate,
	// @PathVariable(value = "endDate") String endDate,
	// @PathVariable(value = "areaCode") String areaCode);
	//

	// @RequestMapping(method = RequestMethod.GET, value =
	// "/monitor/statistics/accept/{department}/{startDate}/{endDate}/{areaCode}")
	// JSONObject getAcceptCounts(
	// @PathVariable(value = "department") String department,
	// @PathVariable(value = "startDate") String startDate,
	// @PathVariable(value = "endDate") String endDate,
	// @PathVariable(value = "areaCode") String areaCode);
	//
	@RequestMapping(method = RequestMethod.POST, value = "/monitor/statistics/banjie", produces = MediaType.APPLICATION_JSON_VALUE)
	JSONObject getBanjieCounts(@RequestBody StatisticsVO statisticsVO);

	// @RequestMapping(method = RequestMethod.GET, value =
	// "/monitor/statistics/banjie/{bjzt}/{department}/{bjcsFlag}/{startDate}/{endDate}/{areaCode}")
	// JSONObject getBanjieCounts(@PathVariable(value = "bjzt") String bjzt,
	// @PathVariable(value = "department") String department,
	// @PathVariable(value = "bjcsFlag") String bjcsFlag,
	// @PathVariable(value = "startDate") String startDate,
	// @PathVariable(value = "endDate") String endDate,
	// @PathVariable(value = "areaCode") String areaCode);
	//
	@RequestMapping(method = RequestMethod.GET, value = "/monitor/statistics/countsquarter/{department}/{startDate}/{endDate}/{areaCode}")
	JSONObject getCountsByQuarter(
			@PathVariable(value = "department") String department,
			@PathVariable(value = "startDate") String startDate,
			@PathVariable(value = "endDate") String endDate,
			@PathVariable(value = "areaCode") String areaCode);
	@RequestMapping(method = RequestMethod.GET, value = "/monitor/statistics/countsmonth/{department}/{startDate}/{endDate}/{areaCode}")
	JSONObject getCountsByMonth(
			@PathVariable(value = "department") String department,
			@PathVariable(value = "startDate") String startDate,
			@PathVariable(value = "endDate") String endDate,
			@PathVariable(value = "areaCode") String areaCode);
	@RequestMapping(method = RequestMethod.GET, value = "/monitor/statistics/countsyear/{department}/{startDate}/{endDate}/{areaCode}")
	JSONObject getCountsByYear(
			@PathVariable(value = "department") String department,
			@PathVariable(value = "startDate") String startDate,
			@PathVariable(value = "endDate") String endDate,
			@PathVariable(value = "areaCode") String areaCode);
	@RequestMapping(method = RequestMethod.GET, value = "/monitor/statistics/countsblbm/{department}/{startDate}/{endDate}/{areaCode}")
	JSONObject getCountsByBlbm(
			@PathVariable(value = "department") String department,
			@PathVariable(value = "startDate") String startDate,
			@PathVariable(value = "endDate") String endDate,
			@PathVariable(value = "areaCode") String areaCode);
	@RequestMapping(method = RequestMethod.GET, value = "/monitor/statistics/blbm/{scope}/{startDate}/{endDate}/{areaCode}/{group}/{bjzt}")
	JSONObject getListByBlbm(
			@PathVariable(value = "scope") String scope,
			@PathVariable(value = "startDate") String startDate,
			@PathVariable(value = "endDate") String endDate,
			@PathVariable(value = "areaCode") String areaCode,
			@PathVariable(value = "group") String group,
			@PathVariable(value = "bjzt") String bjzt);
}