package cn.conac.rc.gateway.modules.ofs.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class AnalzWarning {

	@ApiModelProperty("id")
	private String id;
	
	@ApiModelProperty("warningDept")
	private Boolean warningDept;

	@ApiModelProperty("areaCode")
	private String areaCode;
	
	@ApiModelProperty("warningDuty")
	private Boolean warningDuty;

	@ApiModelProperty("warningEmpRemarks")
	private Boolean warningEmpRemarks;

	@ApiModelProperty("warningOnlineName")
	private Boolean warningOnlineName;

	@ApiModelProperty("warningDegree")
	private Integer warningDegree;

	@ApiModelProperty("haveToDelete")
	private Integer haveToDelete;

	public void setWarningDept(Boolean warningDept){
		this.warningDept=warningDept;
	}

	public Boolean getWarningDept(){
		return warningDept;
	}

	public void setWarningDuty(Boolean warningDuty){
		this.warningDuty=warningDuty;
	}

	public Boolean getWarningDuty(){
		return warningDuty;
	}

	public void setWarningEmpRemarks(Boolean warningEmpRemarks){
		this.warningEmpRemarks=warningEmpRemarks;
	}

	public Boolean getWarningEmpRemarks(){
		return warningEmpRemarks;
	}

	public void setWarningOnlineName(Boolean warningOnlineName){
		this.warningOnlineName=warningOnlineName;
	}

	public Boolean getWarningOnlineName(){
		return warningOnlineName;
	}

	public void setWarningDegree(Integer warningDegree){
		this.warningDegree=warningDegree;
	}

	public Integer getWarningDegree(){
		return warningDegree;
	}

	public void setHaveToDelete(Integer haveToDelete){
		this.haveToDelete=haveToDelete;
	}

	public Integer getHaveToDelete(){
		return haveToDelete;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
