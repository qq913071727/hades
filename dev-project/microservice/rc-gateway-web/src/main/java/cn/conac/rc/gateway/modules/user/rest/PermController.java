package cn.conac.rc.gateway.modules.user.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.RestClientException;
import cn.conac.rc.gateway.modules.user.service.PermService;
import cn.conac.rc.gateway.modules.user.vo.PermVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="sys/")
@Api(tags="权限信息接口", description="权限信息")
public class PermController {

    @Autowired
    PermService permService;
    

    @ApiOperation(value = "获取权限树信息", httpMethod = "POST", response = JSONObject.class, notes = "获取权限树信息")
    @RequestMapping(value = "perms/tree", method = RequestMethod.POST)
    public Object findPermTreeByCondtion(HttpServletRequest request, HttpServletResponse response)
            throws RestClientException, Exception {
    	
    	PermVo permVo = new PermVo();
    	permVo.setDisplay("1");
         // 成功保存部门信息表和部门状态表返回
    	 return  permService.findPermTreeByCondtion(permVo);
    }
    
    @ApiOperation(value = "获取权限列表信息", httpMethod = "POST", response = JSONObject.class, notes = "获取权限列表信息")
    @RequestMapping(value = "perms/list", method = RequestMethod.POST)
    public JSONObject findPermListByCondtion(HttpServletRequest request, HttpServletResponse response)
            throws RestClientException, Exception {
    	 
    	PermVo permVo = new PermVo();
    	permVo.setDisplay("1");

    	return  permService.findPermListByCondtion(permVo);
    	
    }
}
