package cn.conac.rc.gateway.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 事业单位数量和行政机关数量统计实体
 * 
 * @author haocm
 *
 */
@ApiModel
public class OrgGovCount {
	@ApiModelProperty("行政机关数量")
	private String orgCount;
	@ApiModelProperty("事业单位数量")
	private String govCount;

	public String getOrgCount() {
		return orgCount;
	}

	public void setOrgCount(String orgCount) {
		this.orgCount = orgCount;
	}

	public String getGovCount() {
		return govCount;
	}

	public void setGovCount(String govCount) {
		this.govCount = govCount;
	}

}
