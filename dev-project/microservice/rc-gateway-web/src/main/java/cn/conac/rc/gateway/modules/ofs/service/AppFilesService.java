package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.entity.AppFilesEntity;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface AppFilesService {
	
    /**
     * 机构对应批文的新增或者更新
     * @param appFilesEntity
     * @param submitType  （1： 正常保存 0： 暂存）
     * @return JSONObject
     */
    @RequestMapping(method = RequestMethod.POST, value = "/appFiles/{submitType}")
    public JSONObject saveOrUpdate(@ApiParam(value = "保存对象", required = true) @RequestBody AppFilesEntity appFilesEntity, @PathVariable("submitType") String submitType);

}
