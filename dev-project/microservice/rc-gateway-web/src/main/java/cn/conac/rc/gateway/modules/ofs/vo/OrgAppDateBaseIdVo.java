package cn.conac.rc.gateway.modules.ofs.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;

@ApiModel
public class OrgAppDateBaseIdVo implements Serializable {

	private static final long serialVersionUID = -4493854382247570747L;

	private String baseId;
	
	private String appDateStr;

	public OrgAppDateBaseIdVo(String baseId, String appDateStr) {
		super();
		this.baseId = baseId;
		this.appDateStr = appDateStr;
	}

	public String getBaseId() {
		return baseId;
	}

	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}


	public String getAppDateStr() {
		return appDateStr;
	}

	public void setAppDateStr(String appDateStr) {
		this.appDateStr = appDateStr;
	}

}
