package cn.conac.rc.gateway.modules.monitor.vo;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel
public class LocalStatus {
	/**
	 * 材料提交方式-个人
	 */
	public static final String cltjfs1="1";
	/**
	 * 材料提交方式-法人
	 */
	public static final String cltjfs2="2";
	/**
	 * 一次性补正0
	 */
	public static final String ycxbz0="0";
	/**
	 * 一次性补正1
	 */
	public static final String ycxbz1="1";
	/**
	 * 特殊程序超时0
	 */
	public static final String tscxcs00 = "0";
	/**
	 * 特殊程序超时1
	 */
	public static final String tscxcs01 = "1";
	/**
	 * 受理
	 */
	public static final String slzt01 = "01";
	/**
	 * 未受理
	 */
	public static final String slzt02 = "02";
	/**
	 * 未办结
	 */
	public static final String bjzt_1 = "-1";
	/**
	 * 办结
	 */
	public static final String bjzt00 = "00";
	/**
	 * 准予许可办结
	 */
	public static final String bjzt01 = "01";
	/**
	 * 不予许可办结
	 */
	public static final String bjzt02 = "02";
	/**
	 * 专报办结
	 */
	public static final String bjzt03 = "03";
	/**
	 * 终止办结
	 */
	public static final String bjzt04 = "04";
	/**
	 * 办结超时
	 */
	public static final String bjcs01 = "1";
	/**
	 * 办结未超时
	 */
	public static final String bjcs00 = "0";
	/**
	 * 窗口申请
	 */
	public static final String sqfs00 = "0";
	/**
	 * 网上申请
	 */
	public static final String sqfs01 = "1";
	
	@ApiModelProperty("主键id")
	private String id;
	@ApiModelProperty("创建时间")
	private Date createDate;
	@ApiModelProperty("修改时间")
	private Date updateDate;
	@ApiModelProperty("创建人")
	private String createBy;
	@ApiModelProperty("修改人")
	private String updateBy;
	@ApiModelProperty("统一事项编号")
	private String tysxbm;
	@ApiModelProperty("地方事项编号")
	private String dfsxbm;
	@ApiModelProperty("原系统业务流水号")
	private String yxtywlsh;
	@ApiModelProperty("办理部门")
	private String blbm;
	@ApiModelProperty("申请时间")
	private Date sqDate;
	@ApiModelProperty("补正告知时间")
	private Date bzgzDate;
	@ApiModelProperty("补正补齐时间")
	private Date bzbqDate;
	@ApiModelProperty("受理时间")
	private Date slDate;
	@ApiModelProperty("特殊程序申请时间")
	private Date tscxsqDate;
	@ApiModelProperty("特殊程序结束时间")
	private Date tscxjsDate;
	@ApiModelProperty("办结时间")
	private Date bjDate;
	@ApiModelProperty("满意度评价时间")
	private Date mydpjDate;
	@ApiModelProperty("投诉时间")
	private Date tsDate;
	@ApiModelProperty("受理状态")
	private String slzt;
	@ApiModelProperty("办结状态")
	private String bjzt;
	@ApiModelProperty("计算办结时间")
	private String jsbjDate;
	@ApiModelProperty("计算特殊程序结束时间")
	private String jstscxjsDate;
	@ApiModelProperty("计算满意度得分")
	private Integer jsmyddf;
	@ApiModelProperty("办结超时标识")
	private String bjcsFlag;
	@ApiModelProperty("特殊程序超时标识")
	private String tscxcsFlag;
	@ApiModelProperty("一次性补正标识")
	private String ycxbzFlag;
	@ApiModelProperty("最新申请方式")
	private String cltjfs;
	@ApiModelProperty("申请人类型")
	private String sqrlx;
	@ApiModelProperty("行政区划名称")
	private String xzqhmc;
	@ApiModelProperty("行政区划编码")
	private String xzqhbm;
	private String stage;
	private String Status;
	private Date cnbjsjDate;
	private String sxmc;
	@ApiModelProperty("Essearch")
	private String queryString;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
	public Date getCreateDate(){
		return createDate;
	}
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
	public Date getUpdateDate(){
		return updateDate;
	}
	public void setCreateBy(String createBy){
		this.createBy=createBy;
	}
	public String getCreateBy(){
		return createBy;
	}
	public void setUpdateBy(String updateBy){
		this.updateBy=updateBy;
	}
	public String getUpdateBy(){
		return updateBy;
	}
	public void setTysxbm(String tysxbm){
		this.tysxbm=tysxbm;
	}
	public String getTysxbm(){
		return tysxbm;
	}
	public void setDfsxbm(String dfsxbm){
		this.dfsxbm=dfsxbm;
	}
	public String getDfsxbm(){
		return dfsxbm;
	}
	public void setYxtywlsh(String yxtywlsh){
		this.yxtywlsh=yxtywlsh;
	}
	public String getYxtywlsh(){
		return yxtywlsh;
	}
	public void setBlbm(String blbm){
		this.blbm=blbm;
	}
	public String getBlbm(){
		return blbm;
	}
	public void setSqDate(Date sqDate){
		this.sqDate=sqDate;
	}
	public Date getSqDate(){
		return sqDate;
	}
	public void setBzgzDate(Date bzgzDate){
		this.bzgzDate=bzgzDate;
	}
	public Date getBzgzDate(){
		return bzgzDate;
	}
	public void setBzbqDate(Date bzbqDate){
		this.bzbqDate=bzbqDate;
	}
	public Date getBzbqDate(){
		return bzbqDate;
	}
	public void setSlDate(Date slDate){
		this.slDate=slDate;
	}
	public Date getSlDate(){
		return slDate;
	}
	public void setTscxsqDate(Date tscxsqDate){
		this.tscxsqDate=tscxsqDate;
	}
	public Date getTscxsqDate(){
		return tscxsqDate;
	}
	public void setTscxjsDate(Date tscxjsDate){
		this.tscxjsDate=tscxjsDate;
	}
	public Date getTscxjsDate(){
		return tscxjsDate;
	}
	public void setBjDate(Date bjDate){
		this.bjDate=bjDate;
	}
	public Date getBjDate(){
		return bjDate;
	}
	public void setMydpjDate(Date mydpjDate){
		this.mydpjDate=mydpjDate;
	}
	public Date getMydpjDate(){
		return mydpjDate;
	}
	public void setTsDate(Date tsDate){
		this.tsDate=tsDate;
	}
	public Date getTsDate(){
		return tsDate;
	}
	public void setSlzt(String slzt){
		this.slzt=slzt;
	}
	public String getSlzt(){
		return slzt;
	}
	public void setBjzt(String bjzt){
		this.bjzt=bjzt;
	}
	public String getBjzt(){
		return bjzt;
	}
	public void setJsbjDate(String jsbjDate){
		this.jsbjDate=jsbjDate;
	}
	public String getJsbjDate(){
		return jsbjDate;
	}
	public void setJstscxjsDate(String jstscxjsDate){
		this.jstscxjsDate=jstscxjsDate;
	}
	public String getJstscxjsDate(){
		return jstscxjsDate;
	}
	public void setJsmyddf(Integer jsmyddf){
		this.jsmyddf=jsmyddf;
	}
	public Integer getJsmyddf(){
		return jsmyddf;
	}
	public void setBjcsFlag(String bjcsFlag){
		this.bjcsFlag=bjcsFlag;
	}
	public String getBjcsFlag(){
		return bjcsFlag;
	}
	public void setTscxcsFlag(String tscxcsFlag){
		this.tscxcsFlag=tscxcsFlag;
	}
	public String getTscxcsFlag(){
		return tscxcsFlag;
	}
	public void setYcxbzFlag(String ycxbzFlag){
		this.ycxbzFlag=ycxbzFlag;
	}
	public String getYcxbzFlag(){
		return ycxbzFlag;
	}
	public void setCltjfs(String cltjfs){
		this.cltjfs=cltjfs;
	}
	public String getCltjfs(){
		return cltjfs;
	}
	public void setSqrlx(String sqrlx){
		this.sqrlx=sqrlx;
	}
	public String getSqrlx(){
		return sqrlx;
	}
	public void setXzqhmc(String xzqhmc){
		this.xzqhmc=xzqhmc;
	}
	public String getXzqhmc(){
		return xzqhmc;
	}
	public void setXzqhbm(String xzqhbm){
		this.xzqhbm=xzqhbm;
	}
	public String getXzqhbm(){
		return xzqhbm;
	}

	public String getStage()
	{
		return stage;
	}

	public void setStage(String stage)
	{
		this.stage = stage;
	}

	public String getStatus()
	{
		return Status;
	}

	public void setStatus(String status)
	{
		Status = status;
	}

	public Date getCnbjsjDate()
	{
		return cnbjsjDate;
	}

	public void setCnbjsjDate(Date cnbjsjDate)
	{
		this.cnbjsjDate = cnbjsjDate;
	}

	public String getSxmc()
	{
		return sxmc;
	}

	public void setSxmc(String sxmc)
	{
		this.sxmc = sxmc;
	}

	public String getQueryString()
	{
		return queryString;
	}

	public void setQueryString(String queryString)
	{
		this.queryString = queryString;
	}
}

