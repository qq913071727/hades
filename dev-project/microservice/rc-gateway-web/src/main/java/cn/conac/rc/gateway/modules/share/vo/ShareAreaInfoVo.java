package cn.conac.rc.gateway.modules.share.vo;

import cn.conac.rc.gateway.modules.share.entity.ShareAreaInfoEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ShareAreaInfoVo类
 *
 * @author voCreator
 * @date 2017-07-21
 * @version 1.0
 */
@ApiModel
public class ShareAreaInfoVo extends ShareAreaInfoEntity {

	private static final long serialVersionUID = 1500615505043479122L;

	@ApiModelProperty("当前分页")
	private Integer page;

	@ApiModelProperty("每页个数")
	private Integer size;
	
	@ApiModelProperty("地区代码被申请方（多个地区代码用逗号分隔）")
	private String areaCodeDests;

	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}

	public String getAreaCodeDests() {
		return areaCodeDests;
	}

	public void setAreaCodeDests(String areaCodeDests) {
		this.areaCodeDests = areaCodeDests;
	}
}
