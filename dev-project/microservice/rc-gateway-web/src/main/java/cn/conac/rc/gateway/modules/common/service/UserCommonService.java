package cn.conac.rc.gateway.modules.common.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.user.vo.RcUserVo;

@FeignClient("RC-SERVICE-COMMON")
public interface UserCommonService
{

    @RequestMapping(method = RequestMethod.GET, value = "/user/name/{loginName}")
    JSONObject getLoginUser(@PathVariable(value = "loginName") String loginName);
    @RequestMapping(method = RequestMethod.GET, value = "/user/sicode/{userCode}")
    JSONObject getUserByUserCode(@PathVariable(value = "userCode") String userCode);
    @RequestMapping(method = RequestMethod.GET, value = "/user/sicode/{userCode}/{fullName}")
    JSONObject getUserByUserCodeFullname(@PathVariable(value = "userCode") String userCode,@PathVariable(value = "fullName") String fullName);
    @RequestMapping(method = RequestMethod.POST, value = "/user/list")
    JSONObject getUsers(@RequestBody RcUserVo rcUserVo);
    @RequestMapping(value = "/user/{id}/roles", method = RequestMethod.POST)
    public JSONObject getRolesByUserId(@PathVariable("id") String id);

}