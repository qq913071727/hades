package cn.conac.rc.gateway.security.vo;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("用户登录请求")
public class JwtAuthRequest implements Serializable {

    private static final long serialVersionUID = -8445943548965154778L;

    @ApiModelProperty("用户名")
    @NotNull
    private String username;
    @ApiModelProperty("密码，用对称加密算法进行加密")
    private String password;
    @ApiModelProperty("登录验证码")
    @NotNull
    private String validateCode;
    @ApiModelProperty("登录验证码KEY")
    @NotNull
    private String validateKey;
    @ApiModelProperty("登录方式1-用户名 2-手机号 3-个人域名 4-机构域名")
    @NotNull
    private String loginType;
    @ApiModelProperty("手机号码后4位")
    private String fourNum;
    public JwtAuthRequest() {
        super();
    }

    public JwtAuthRequest(String username, String password,String validateCode) {
        this.setUsername(username);
        this.setPassword(password);
        this.setValidateCode(validateCode);
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getValidateCode()
    {
        return validateCode;
    }

    public void setValidateCode(String validateCode)
    {
        this.validateCode = validateCode;
    }

    public String getValidateKey()
    {
        return validateKey;
    }

    public void setValidateKey(String validateKey)
    {
        this.validateKey = validateKey;
    }

    public String getLoginType()
    {
        return loginType;
    }

    public void setLoginType(String loginType)
    {
        this.loginType = loginType;
    }

    public String getFourNum()
    {
        return fourNum;
    }

    public void setFourNum(String fourNum)
    {
        this.fourNum = fourNum;
    }
}
