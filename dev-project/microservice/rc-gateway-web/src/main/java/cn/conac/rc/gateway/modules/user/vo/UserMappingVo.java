package cn.conac.rc.gateway.modules.user.vo;


import java.io.Serializable;

import io.swagger.annotations.ApiModel;

/**
 * UserMappingVo类
 *
 * @author voCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
public class UserMappingVo implements Serializable{

	private static final long serialVersionUID = -1491821456865771513L;
    
    private String jgsyName;
    
    private String xm;
    
    private String sfzh;
    
    private String zbzt;
    
    private String zbztDesp;
    
    private String bzlx;
    
    private String bzlxDesp;
    
    private String szjgName;
    
    private String zw;
    
    private String unifyCode;

	public String getJgsyName() {
		return jgsyName;
	}

	public String getXm() {
		return xm;
	}

	public String getSfzh() {
		return sfzh;
	}

	public String getZbzt() {
		return zbzt;
	}

	public String getZbztDesp() {
		return zbztDesp;
	}

	public String getBzlx() {
		return bzlx;
	}

	public String getBzlxDesp() {
		return bzlxDesp;
	}

	public String getSzjgName() {
		return szjgName;
	}

	public String getZw() {
		return zw;
	}

	public String getUnifyCode() {
		return unifyCode;
	}

	public void setJgsyName(String jgsyName) {
		this.jgsyName = jgsyName;
	}

	public void setXm(String xm) {
		this.xm = xm;
	}

	public void setSfzh(String sfzh) {
		this.sfzh = sfzh;
	}

	public void setZbzt(String zbzt) {
		this.zbzt = zbzt;
	}

	public void setZbztDesp(String zbztDesp) {
		this.zbztDesp = zbztDesp;
	}

	public void setBzlx(String bzlx) {
		this.bzlx = bzlx;
	}

	public void setBzlxDesp(String bzlxDesp) {
		this.bzlxDesp = bzlxDesp;
	}

	public void setSzjgName(String szjgName) {
		this.szjgName = szjgName;
	}

	public void setZw(String zw) {
		this.zw = zw;
	}

	public void setUnifyCode(String unifyCode) {
		this.unifyCode = unifyCode;
	}
    
}
