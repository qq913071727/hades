/**
 * Copyright &copy; 2012-2013 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package cn.conac.rc.gateway.security;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import cn.conac.rc.framework.utils.ResultPojo;
import cn.conac.rc.framework.utils.StringUtils;

/**
 * 生成随机验证码图片
 *
 * @author zhangfz
 * @version 1.0
 */
@Component
public class ValidateCodeUtils
{
    private static final long serialVersionUID = 1L;

    /**
     * VALIDATE_CODE
     */
    public static final String VALIDATE_CODE = "validateCode";

    /**
     * ww
     */
    private int ww = 70;

    /**
     * hh
     */
    private int hh = 26;

    public ValidateCodeUtils()
    {
        super();
    }

    /**
     * validate
     *
     * @param validateCode String
     * @return boolean
     * @author wangmeng
     */
    public static boolean validate(String code, String validateCode)
    {
        return validateCode.toUpperCase().equals(code);
    }

    /**
     * createImage.
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @throws IOException IO异常
     * @author wangmeng
     */
    public String createImage(HttpServletRequest request, HttpServletResponse response) throws IOException
    {

        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpeg");

        /*
         * 得到参数高，宽，都为数字时，则使用设置高宽，否则使用默认值
         */
        String width = request.getParameter("width");
        String height = request.getParameter("height");
        if (StringUtils.isNumeric(width) && StringUtils.isNumeric(height)) {
            ww = NumberUtils.toInt(width);
            hh = NumberUtils.toInt(height);
        }

        BufferedImage image = new BufferedImage(ww, hh, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();

        /*
         * 生成背景
         */
        createBackground(g);

        /*
         * 生成字符
         */
        String s = createCharacter(g);
        System.out.println(s);
        g.dispose();
        OutputStream out = response.getOutputStream();
        ImageIO.write(image, "JPEG", out);
        out.close();
        return s;
    }

    /**
     * getRandColor.
     *
     * @param fc int
     * @param bc int
     * @return Color
     * @author wangmeng
     */
    public Color getRandColor(int fc, int bc)
    {
        int f = fc;
        int b = bc;
        Random random = new Random();
        if (f > 255) {
            f = 255;
        }
        if (b > 255) {
            b = 255;
        }
        return new Color(f + random.nextInt(b - f), f + random.nextInt(b - f), f + random.nextInt(b - f));
    }

    /**
     * createBackground.
     *
     * @param g Graphics
     * @author wangmeng
     */
    public void createBackground(Graphics g)
    {
        // 填充背景
        g.setColor(getRandColor(220, 250));
        g.fillRect(0, 0, ww, hh);
        // 加入干扰线条
        for (int i = 0; i < 8; i++) {
            g.setColor(getRandColor(40, 150));
            Random random = new Random();
            int x = random.nextInt(ww);
            int y = random.nextInt(hh);
            int x1 = random.nextInt(ww);
            int y1 = random.nextInt(hh);
            g.drawLine(x, y, x1, y1);
        }
    }

    /**
     * createCharacter.
     *
     * @param g Graphics
     * @return String
     * @author wangmeng
     */
    public String createCharacter(Graphics g)
    {
        char[] codeSeq = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T',
                'U', 'V', 'W', 'X', 'Y', 'Z', '2', '3', '4', '5', '6', '7', '8', '9' };
        String[] fontTypes = { "\u5b8b\u4f53", "\u65b0\u5b8b\u4f53", "\u9ed1\u4f53", "\u6977\u4f53", "\u96b6\u4e66" };
        Random random = new Random();
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            String r = String.valueOf(codeSeq[random.nextInt(codeSeq.length)]);// random.nextInt(10));
            g.setColor(new Color(50 + random.nextInt(100), 50 + random.nextInt(100), 50 + random.nextInt(100)));
            g.setFont(new Font(fontTypes[random.nextInt(fontTypes.length)], Font.BOLD, 26));
            g.drawString(r, 15 * i + 5, 19 + random.nextInt(8));
            // g.drawString(r, i*w/4, h-5);
            s.append(r);
        }
        return s.toString();
    }
}
