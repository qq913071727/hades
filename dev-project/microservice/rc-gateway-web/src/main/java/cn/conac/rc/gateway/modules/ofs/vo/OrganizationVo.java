package cn.conac.rc.gateway.modules.ofs.vo;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class OrganizationVo {

	@ApiModelProperty("当前分页")
	private Integer page;

	@ApiModelProperty("每页个数")
	private Integer size;

	@ApiModelProperty("创建 开始时间")
	private Date createDateStart;

	@ApiModelProperty("创建 结束时间")
	private Date createDateEnd;

	@ApiModelProperty("更新 开始时间")
	private Date updateDateStart;

	@ApiModelProperty("更新 结束时间")
	private Date updateDateEnd;

	@ApiModelProperty("id")
	private String id;
	
	@ApiModelProperty("baseId")
	private String baseId;

	@ApiModelProperty("父单位")
	private String parentId;

	@ApiModelProperty("三定名称")
	private String name;

	@ApiModelProperty("行编事编")
	private String type;

	@ApiModelProperty("所属系统")
	private String ownSys;

	@ApiModelProperty("编码")
	private String code;

	@ApiModelProperty("状态（发布，撤并）")
	private String status;

	@ApiModelProperty("排序号")
	private Integer sort;

	@ApiModelProperty("地区")
	private String areaId;

	@ApiModelProperty("areaCode")
	private String areaCode;

	@ApiModelProperty("是否主管")
	private String isComp;

	@ApiModelProperty("是否涉密")
	private String isSecret;

	@ApiModelProperty("地方编办")
	private String localCode;

	@ApiModelProperty("是否有子机构")
	private String hasChild;
	
	public String getHasChild() {
		return hasChild;
	}

	public void setHasChild(String hasChild) {
		this.hasChild = hasChild;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setBaseId(String baseId){
		this.baseId=baseId;
	}

	public String getBaseId(){
		return baseId;
	}

	public void setParentId(String parentId){
		this.parentId=parentId;
	}

	public String getParentId(){
		return parentId;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setType(String type){
		this.type=type;
	}

	public String getType(){
		return type;
	}

	public void setOwnSys(String ownSys){
		this.ownSys=ownSys;
	}

	public String getOwnSys(){
		return ownSys;
	}

	public void setCode(String code){
		this.code=code;
	}

	public String getCode(){
		return code;
	}

	public void setStatus(String status){
		this.status=status;
	}

	public String getStatus(){
		return status;
	}

	public void setSort(Integer sort){
		this.sort=sort;
	}

	public Integer getSort(){
		return sort;
	}

	public void setAreaId(String areaId){
		this.areaId=areaId;
	}

	public String getAreaId(){
		return areaId;
	}

	public void setAreaCode(String areaCode){
		this.areaCode=areaCode;
	}

	public String getAreaCode(){
		return areaCode;
	}

	public void setIsComp(String isComp){
		this.isComp=isComp;
	}

	public String getIsComp(){
		return isComp;
	}

	public void setIsSecret(String isSecret){
		this.isSecret=isSecret;
	}

	public String getIsSecret(){
		return isSecret;
	}

	public void setLocalCode(String localCode){
		this.localCode=localCode;
	}

	public String getLocalCode(){
		return localCode;
	}
	
	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}

	public void setCreateDateStart(Date createDateStart){
		this.createDateStart=createDateStart;
	}

	public Date getCreateDateStart(){
		return createDateStart;
	}

	public void setCreateDateEnd(Date createDateEnd){
		this.createDateEnd=createDateEnd;
	}

	public Date getCreateDateEnd(){
		return createDateEnd;
	}

	public void setUpdateDateStart(Date updateDateStart){
		this.updateDateStart=updateDateStart;
	}

	public Date getUpdateDateStart(){
		return updateDateStart;
	}

	public void setUpdateDateEnd(Date updateDateEnd){
		this.updateDateEnd=updateDateEnd;
	}

	public Date getUpdateDateEnd(){
		return updateDateEnd;
	}

}
