package cn.conac.rc.gateway.modules.ofs.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.RestClientException;
import cn.conac.rc.gateway.modules.ofs.service.VAuditList2Service;
import cn.conac.rc.gateway.modules.ofs.vo.VAuditList2Vo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="ofs/")
@Api(tags="部门共通信息", description="三定信息")
public class VAuditList2Controller {

    @Autowired
    VAuditList2Service vAuditList2Service;
    
    @ApiOperation(value = "委办局审核状态信息", httpMethod = "POST", response = JSONObject.class, notes = "根据条件查询部门委办局审核状态信息")
    @RequestMapping(value = "3/audit/status/page", method = RequestMethod.POST)
    public JSONObject auditListInfo(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam(value = "查询条件对象", required = true) @RequestBody VAuditList2Vo vAuditList2Vo)
            throws RestClientException, Exception {
        return vAuditList2Service.list(vAuditList2Vo);
    }

}