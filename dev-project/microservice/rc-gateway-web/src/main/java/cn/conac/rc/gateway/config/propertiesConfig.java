package cn.conac.rc.gateway.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import cn.conac.rc.framework.utils.BeanMapper;
import cn.conac.rc.gateway.security.vo.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class propertiesConfig implements Serializable {


    @Value("${rc.user.default.pwd}")
    private String defaultPwd;

    @Value("${rc.user.default.vcode}")
    private String defaultVcode;

    @Value("${rc.user.default.sendsms}")
    private String defaultSendsms;

    @Value("${rc.user.default.smstime}")
    private String smstime;

    @Value("${rc.user.default.smslongtime}")
    private Long smslongtime;

    @Value("${rc.user.default.imglongtime}")
    private Long imglongtime;

    public String getDefaultPwd()
    {
        return defaultPwd;
    }

    public void setDefaultPwd(String defaultPwd)
    {
        this.defaultPwd = defaultPwd;
    }

    public String getDefaultVcode()
    {
        return defaultVcode;
    }

    public void setDefaultVcode(String defaultVcode)
    {
        this.defaultVcode = defaultVcode;
    }

    public String getDefaultSendsms()
    {
        return defaultSendsms;
    }

    public void setDefaultSendsms(String defaultSendsms)
    {
        this.defaultSendsms = defaultSendsms;
    }

    public String getSmstime()
    {
        return smstime;
    }

    public void setSmstime(String smstime)
    {
        this.smstime = smstime;
    }

    public Long getSmslongtime()
    {
        return smslongtime;
    }

    public void setSmslongtime(Long smslongtime)
    {
        this.smslongtime = smslongtime;
    }

    public Long getImglongtime()
    {
        return imglongtime;
    }

    public void setImglongtime(Long imglongtime)
    {
        this.imglongtime = imglongtime;
    }
}