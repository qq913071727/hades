package cn.conac.rc.gateway.modules.itm.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.itm.vo.AelItemVo;

@FeignClient("RC-SERVICE-WORKFLOW")
public interface WorkflowService
{

    @RequestMapping(value = "/workflow/task/{childItemId}/{sysType}", method = RequestMethod.GET)
    public JSONObject findTaskByVariable(@PathVariable("childItemId") String childItemId,
            @PathVariable("sysType") String sysType);
}
