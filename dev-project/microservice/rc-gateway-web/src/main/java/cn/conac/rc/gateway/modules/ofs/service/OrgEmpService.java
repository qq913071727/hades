package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.entity.OrgEmpEntity;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface OrgEmpService {

	/**
	 * 通过ID获取编制信息
	 * @param id
	 * @return
	 */
    @RequestMapping(method = RequestMethod.GET, value = "/orgEmp/{id}")
    JSONObject empDetail(@PathVariable(value = "id") String id);

	/**
	 * 编制信息的保存功能
	 * @param 保存对象信息
	 * @return
	 */
    @RequestMapping(method = RequestMethod.POST, value = "/orgEmp/")
    JSONObject saveEmp(@ApiParam(value = "保存对象", required = true) @RequestBody JSONObject orgEmp);

	/**
	 * 编制信息的更新功能
	 * @param 更新对象信息
	 * @return
	 */
    @RequestMapping(method = RequestMethod.POST, value = "/orgEmp/update")
    JSONObject updateEmp(@ApiParam(value = "更新对象", required = true) @RequestBody JSONObject orgEmp);

	/**
	 * 编制信息的逻辑删除功能
	 * @param 删除对象ID
	 * @return
	 */
    @RequestMapping(method = RequestMethod.POST, value = "/orgEmp/{id}/uDelete")
    JSONObject uDeleteEmp(@ApiParam(value = "id", required = true) @PathVariable("id") String id);


    /**
     * 机构编制的新增或者更新
     * @param orgEmpEntity
     * @param submitType  （1： 正常保存 0： 暂存）
     * @return JSONObject
     */
    @RequestMapping(method = RequestMethod.POST, value = "/orgEmp/{submitType}")
    public JSONObject saveOrUpdate(@PathVariable("submitType") String submitType, @ApiParam(value = "保存对象", required = true) @RequestBody OrgEmpEntity orgEmpEntity);

}