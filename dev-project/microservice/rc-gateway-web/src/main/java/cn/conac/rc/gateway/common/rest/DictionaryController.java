package cn.conac.rc.gateway.common.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;

import cn.conac.rc.framework.utils.ResultPojo;
import cn.conac.rc.framework.utils.StringUtils;
import cn.conac.rc.gateway.common.service.DictionaryService;
import cn.conac.rc.gateway.common.vo.CareerIndustryDictEntity;
import cn.conac.rc.gateway.common.vo.Dictionary;
import cn.conac.rc.gateway.common.vo.UnderCategoryEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags="字典接口", description="共通")
@CacheConfig(keyGenerator = "keyGenerator", cacheNames = "dic_cache", cacheManager="redisCache1h")
public class DictionaryController {
	@Autowired
	DictionaryService dicService;
	
	// 新增数据字典
	@ApiOperation(value = "保存字典的value", notes = "保存字典的value", response = Dictionary.class)
	@RequestMapping(value = "/comm/dic/saveDic", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> saveDic(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "key", required = false) String key,
			@RequestParam(value = "value", required = false) String value,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "sort", required = false) Integer sort,
			@RequestParam(value = "parentId", required = false) String parentId) {

		return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
				dicService.saveDic(key, value, name, sort, parentId)), HttpStatus.OK);
	}

	// 更新数据字典
	@ApiOperation(value = "保存字典的value", httpMethod = "POST", response = Dictionary.class, notes = "更新字典的value")
	@RequestMapping(value = "/comm/dic/updateDic", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> updateDic(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "字典的id", required = false) @RequestParam(value="id", required=false) String id,
			@ApiParam(value = "字典的key", required = false) @RequestParam(value="key", required=false) String key,
			@ApiParam(value = "字典的value", required = false) @RequestParam(value="value", required=false) String value,
			@ApiParam(value = "value的展示名称", required = false) @RequestParam(value="name", required=false) String name,
			@ApiParam(value = "value的排序号", required = false) @RequestParam(value="sort", required=false) Integer sort,
			@ApiParam(value = "value的父value的id", required = false) @RequestParam(value="parentId", required=false) String parentId) {
		
		return new ResponseEntity<ResultPojo>(new ResultPojo(
					ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, dicService.saveDic(key, value, name, sort, parentId)),
					HttpStatus.OK);
	}

	// 删除数据字典
	@ApiOperation(value = "根据字典key的id删除数据字典", httpMethod = "POST", response = String.class, notes = "根据字典key的id删除数据字典")
	@RequestMapping(value = "/comm/dic/deleteDic", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> deleteDic(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "字典的id", required = false) @RequestParam(value="id", required=false) String id) {
		return new ResponseEntity<ResultPojo>(new ResultPojo(
				ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, dicService.deleteDic(id)),
				HttpStatus.OK);
	}

	// 删除数据字典
	@ApiOperation(value = "根据字典key的id删除数据字典", httpMethod = "POST", response = String.class, notes = "根据字典key的id删除数据字典")
	@RequestMapping(value = "/comm/dic/deleteDicByKey", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> deleteDicByKey(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "字典的key", required = false) @RequestParam(value="id", required=false) String key) {
		return new ResponseEntity<ResultPojo>(new ResultPojo(
				ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, dicService.deleteDicByKey(key)),
				HttpStatus.OK);
	}

	@ApiOperation(value = "根据字典的id查询数据字典", httpMethod = "GET", response = Dictionary.class, notes = "根据字典的id查询数据字典")
	@RequestMapping(value = "/comm/dic/getDicById/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> getDicById(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "字典的id", required = false) @PathVariable("id") String id) {
		return new ResponseEntity<ResultPojo>(new ResultPojo(
				ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, dicService.getDicById(id)),
				HttpStatus.OK);
	}

	@ApiOperation(value = "根据type查询数据字典", httpMethod = "GET", response = Dictionary.class, notes = "根据type查询数据字典")
	@RequestMapping(value = "/comm/dic/Dics/{type}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> getAllDics(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "字典的type", required = false) @PathVariable("type") String type) {
		return new ResponseEntity<ResultPojo>(new ResultPojo(
				ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, dicService.getDicByKey(type)),
				HttpStatus.OK);
	}
	
	@Cacheable
	@ApiOperation(value = "获取所有数据字典", httpMethod = "GET", response = Dictionary.class, notes = "获取所有数据字典")
	@RequestMapping(value = "/comm/dic/getAllDics", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> getDicByKey() {
		return new ResponseEntity<ResultPojo>(new ResultPojo(
				ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, dicService.getAllDics()),
				HttpStatus.OK);
	}

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "获取所有数据字典map<type,map<value,name>>", httpMethod = "GET", response = ArrayList.class, notes = "获取所有数据字典")
	@Cacheable
	@RequestMapping(value = "/comm/dictionarys/map", method = RequestMethod.GET)
	public ResultPojo getAllDicsMap() {
		System.out.println("执行map方法查询字典信息");
		// dicService.getAllDics();
		ResultPojo result = JSON.toJavaObject(dicService.getAllDics(), ResultPojo.class);
		List<Dictionary> list = (ArrayList<Dictionary>)result.getResult();
		Map<String,Object> restMap = new HashMap<String,Object>();
		for (Object object : list) {
			Dictionary item = JSON.toJavaObject(JSON.parseObject(JSON.toJSONString(object)), Dictionary.class);
			String type = item.getType();
			Map<String,String> itemMap = (Map<String,String>)restMap.get(type);
			if(itemMap == null){
				itemMap = new HashMap<String,String>();
				itemMap.put(item.getValue(), item.getName());
				restMap.put(type,itemMap);
			}else{
				String value = item.getValue();
				String name = itemMap.get(value);
				if(StringUtils.isBlank(name)){
					itemMap.put(value, item.getName());
				}
			}
		}

		// 加行业分类
		result = JSON.toJavaObject(dicService.getAllIndustry(), ResultPojo.class);
		List<CareerIndustryDictEntity> industryList = (ArrayList<CareerIndustryDictEntity>)result.getResult();
		String type = "industry";
		Map<String,String> itemMap = new HashMap<String,String>();
		for (Object object : industryList) {
			CareerIndustryDictEntity item = JSON.toJavaObject(JSON.parseObject(JSON.toJSONString(object)), CareerIndustryDictEntity.class);
			itemMap.put(item.getId(), item.getName());
		}
		restMap.put(type,itemMap);

		// 加下设机构类别
		result = JSON.toJavaObject(dicService.getAllCategory(), ResultPojo.class);
		List<UnderCategoryEntity> categoryList = (ArrayList<UnderCategoryEntity>)result.getResult();
		String type2 = "category";
		Map<String,String> itemMap2 = new HashMap<String,String>();
		for (Object object : categoryList) {
			UnderCategoryEntity item = JSON.toJavaObject(JSON.parseObject(JSON.toJSONString(object)), UnderCategoryEntity.class);
			itemMap2.put(item.getId(), item.getName());
		}
		restMap.put(type2,itemMap2);

		return new ResultPojo(ResultPojo.CODE_SUCCESS,ResultPojo.MSG_SUCCESS,restMap);
	}
}
