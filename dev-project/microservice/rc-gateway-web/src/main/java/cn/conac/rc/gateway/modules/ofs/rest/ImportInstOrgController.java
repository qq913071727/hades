package cn.conac.rc.gateway.modules.ofs.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.RestClientException;
import cn.conac.rc.gateway.modules.ofs.service.ImportInstOrgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="ofs/")
@Api(tags="外部导入域名相关信息", description="三定信息")
public class ImportInstOrgController {

    @Autowired
    ImportInstOrgService  importInstOrgService;

    
	@ApiOperation(value = "根据baseId取得职责列表信息", httpMethod = "GET", response = JSONObject.class, notes = "根据baseId取得职责列表信息")
    @RequestMapping(value = "imps/{name}", method = RequestMethod.GET)
    public JSONObject findListById(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "部门名称", required = true) @PathVariable("name") String name)
            throws RestClientException, Exception {
        return importInstOrgService.findByName(name);
    }
}
