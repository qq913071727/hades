package cn.conac.rc.gateway.modules.itm.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;

import cn.conac.rc.gateway.modules.itm.service.BasisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags = "三定分析/权责相关接口", description = "清单信息")
@RequestMapping("itm/")
public class BasisController {
	
    @Autowired
    BasisService basisService;
    
    @ApiOperation(value = "获得实施依据类型列表信息", notes = "获得实施依据类型列表信息", response =JSONArray.class)
    @RequestMapping(value = "/basis/list", method = RequestMethod.GET)
    public JSONArray getAreaList(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	return basisService.getBasisList();
    }
    
    @ApiOperation(value = "根据Pid获得实施依据类型列表信息", notes = "根据Pid获得实施依据类型列表信息", response =JSONArray.class)
    @RequestMapping(value = "/basis/list/{pid}", method = RequestMethod.GET)
    public JSONArray getAreaList(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam("实施依据类型父id") @PathVariable("pid") String pid)
            throws Exception {
    	return basisService.getBasisListByPid(pid);
    }
}
