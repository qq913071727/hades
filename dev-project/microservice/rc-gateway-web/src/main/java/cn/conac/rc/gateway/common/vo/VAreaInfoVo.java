package cn.conac.rc.gateway.common.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * VAreaInfoVo类
 *
 * @author voCreator
 * @date 2017-07-14
 * @version 1.0
 */
@ApiModel
public class VAreaInfoVo implements Serializable {

	private static final long serialVersionUID = 1500010878727214270L;

	@ApiModelProperty("id")
	private String id;

	@ApiModelProperty("name")
	private String name;

	@ApiModelProperty("code")
	private String code;

	@ApiModelProperty("weight")
	private Integer weight;

	@ApiModelProperty("lvl")
	private Integer lvl;

	@ApiModelProperty("leaf")
	private Integer leaf;

	@ApiModelProperty("adminCode")
	private String adminCode;

	@ApiModelProperty("type")
	private Integer type;

	@ApiModelProperty("pid")
	private String pid;

	@ApiModelProperty("areaParentName")
	private String areaParentName;
	
	@ApiModelProperty("1: 有公开办法 0：无公开办法")
	private Integer isPublic;
	
	@ApiModelProperty("当前分页")
	private Integer page;

	@ApiModelProperty("每页个数")
	private Integer size;
	
	@ApiModelProperty("是否不包含自己（1：是  0：否）")
	private Integer noSelfFlag;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Integer getLvl() {
		return lvl;
	}

	public void setLvl(Integer lvl) {
		this.lvl = lvl;
	}

	public Integer getLeaf() {
		return leaf;
	}

	public void setLeaf(Integer leaf) {
		this.leaf = leaf;
	}

	public String getAdminCode() {
		return adminCode;
	}

	public void setAdminCode(String adminCode) {
		this.adminCode = adminCode;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getAreaParentName() {
		return areaParentName;
	}

	public void setAreaParentName(String areaParentName) {
		this.areaParentName = areaParentName;
	}

	public Integer getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Integer isPublic) {
		this.isPublic = isPublic;
	}

	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}

	public Integer getNoSelfFlag() {
		return noSelfFlag;
	}

	public void setNoSelfFlag(Integer noSelfFlag) {
		this.noSelfFlag = noSelfFlag;
	}
}
