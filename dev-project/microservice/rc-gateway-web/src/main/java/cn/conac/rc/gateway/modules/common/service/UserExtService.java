package cn.conac.rc.gateway.modules.common.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

@FeignClient("RC-SERVICE-COMMON")
public interface UserExtService {

    @RequestMapping(value = "/userExt/{orgId}", method = RequestMethod.POST)
    public JSONObject updateOrgIdToNull(@PathVariable("orgId") String orgId);

}