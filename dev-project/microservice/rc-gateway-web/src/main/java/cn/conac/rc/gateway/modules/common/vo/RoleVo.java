package cn.conac.rc.gateway.modules.common.vo;

import java.io.Serializable;

import cn.conac.rc.gateway.base.BaseVo;
import io.swagger.annotations.ApiModelProperty;

public class RoleVo extends BaseVo implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String ROLE_ADMIN_SYS = "1"; //CONAC系统管理员
    public static final String ROLE_WBJ = "104";// 委办局管理员
    public static final String ROLE_BB = "106";// 编办电子政务中心人员（原编办业务员）
    public static final String ROLE_FZB = "105";// 法制办角色
    public static final String ROLE_BB_GLY = "107";// 编办管理员
    public static final String ROLE_BBBZGL = "108";// 编办编制管理工作人员
    public static final String ROLE_BBTGSG = "109";// 编办体改/审改工作人员
    public static final String ROLE_WBJPT = "110";// 委办局普通用户

    @ApiModelProperty("角色码")
    private String roleId;

    @ApiModelProperty("角色名称")
    private String roleName;


    public String getRoleName() {
        return roleName;
    }

	public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
	
    public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
		this.id = roleId;
	}

}