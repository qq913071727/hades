package cn.conac.rc.gateway.modules.ofs.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * OrgOpinionsEntity类
 *
 * @author beanCreator
 * @date 2016-11-01
 * @version 1.0
 */
@ApiModel
public class OrgOpinionsEntity implements Serializable {

	private static final long serialVersionUID = 3333362221384376991L;

	@ApiModelProperty("id")
	private String id;

	@ApiModelProperty("当前机构ID号")
	private String baseId;

	@ApiModelProperty("审批意见")
	private String orgOpinions;

	@ApiModelProperty("审批时间")
	private String auditTime;
	
	@ApiModelProperty("审批用户ID")
	private Integer  auditUserId;

	public void setId(String id){
		this.id=id;
	}
	
	public String getId(){
		return id;
	}

	public void setBaseId(String baseId){
		this.baseId=baseId;
	}

	public String getBaseId(){
		return baseId;
	}

	public void setOrgOpinions(String orgOpinions){
		this.orgOpinions=orgOpinions;
	}

	public String getOrgOpinions(){
		return orgOpinions;
	}

	public void setAuditTime(String auditTime){
		this.auditTime=auditTime;
	}

	public String getAuditTime(){
		return auditTime;
	}

	public Integer getAuditUserId() {
		return auditUserId;
	}

	public void setAuditUserId(Integer auditUserId) {
		this.auditUserId = auditUserId;
	}
}
