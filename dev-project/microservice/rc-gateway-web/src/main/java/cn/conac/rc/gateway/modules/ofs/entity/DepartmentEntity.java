package cn.conac.rc.gateway.modules.ofs.entity;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DepartmentEntity类
 *
 * @author beanCreator
 * @date 2016-11-01
 * @version 1.0
 */
@ApiModel
public class DepartmentEntity implements Serializable {

	private static final long serialVersionUID = -7474822009862763519L;

	@ApiModelProperty("ID")
	private String id;

	@ApiModelProperty("机构ID号")
	private String baseId;

	@ApiModelProperty("部门序号")
	private String depNum;

	@ApiModelProperty("部门名称")
	private String deptName;

	@ApiModelProperty("上级部门ID号")
	private String parentId;

	@ApiModelProperty("部门类型")
	private String deptFuncType;

	@ApiModelProperty("部门职能")
	private String deptDesc;

	@ApiModelProperty("备注")
	private String remarks;

	@ApiModelProperty("deptLevel")
	private String deptLevel;

	@ApiModelProperty("depCode")
	private String depCode;
	
	@ApiModelProperty("复制用旧ID")
	private String oldId4Copy;

	@ApiModelProperty("对应关联表")
	private List<DutyEntity> dutyList;

	public void setId(String id){
		this.id=id;
	}

	public String getId(){
		return id;
	}

	public void setBaseId(String baseId){
		this.baseId=baseId;
	}

	public String getBaseId(){
		return baseId;
	}

	public void setDepNum(String depNum){
		this.depNum=depNum;
	}

	public String getDepNum(){
		return depNum;
	}

	public void setDeptName(String deptName){
		this.deptName=deptName;
	}

	public String getDeptName(){
		return deptName;
	}

	public void setParentId(String parentId){
		this.parentId=parentId;
	}

	public String getParentId(){
		return parentId;
	}

	public void setDeptFuncType(String deptFuncType){
		this.deptFuncType=deptFuncType;
	}

	public String getDeptFuncType(){
		return deptFuncType;
	}

	public void setDeptDesc(String deptDesc){
		this.deptDesc=deptDesc;
	}

	public String getDeptDesc(){
		return deptDesc;
	}

	public void setRemarks(String remarks){
		this.remarks=remarks;
	}

	public String getRemarks(){
		return remarks;
	}

	public void setDeptLevel(String deptLevel){
		this.deptLevel=deptLevel;
	}

	public String getDeptLevel(){
		return deptLevel;
	}

	public void setDepCode(String depCode){
		this.depCode=depCode;
	}

	public String getDepCode(){
		return depCode;
	}

	public List<DutyEntity> getDutyList() {
		return dutyList;
	}

	public void setDutyList(List<DutyEntity> dutyList) {
		this.dutyList = dutyList;
	}
}
