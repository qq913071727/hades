package cn.conac.rc.gateway.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * UnderCategoryEntity类
 *
 * @author beanCreator
 * @date 2016-12-06
 * @version 1.0
 */
@ApiModel
public class UnderCategoryEntity implements Serializable {

	private static final long serialVersionUID = 1478071307064890506L;

	@ApiModelProperty("id")
	private String id;

	@ApiModelProperty("pid")
	private String pid;

	@ApiModelProperty("name")
	private String name;

	@ApiModelProperty("open")
	private String open;

	@ApiModelProperty("perm")
	private String perm;

	@ApiModelProperty("leaf")
	private Integer leaf;

	public void setId(String id){
		this.id=id;
	}

	public String getId(){
		return id;
	}

	public void setPid(String pid){
		this.pid=pid;
	}

	public String getPid(){
		return pid;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setOpen(String open){
		this.open=open;
	}

	public String getOpen(){
		return open;
	}

	public void setPerm(String perm){
		this.perm=perm;
	}

	public String getPerm(){
		return perm;
	}

	public void setLeaf(Integer leaf){
		this.leaf=leaf;
	}

	public Integer getLeaf(){
		return leaf;
	}
}
