package cn.conac.rc.gateway.modules.ofs.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.DateUtils;
import cn.conac.rc.framework.utils.RestClientException;
import cn.conac.rc.gateway.base.ResultPojo;
import cn.conac.rc.gateway.modules.ofs.contsant.Contsants;
import cn.conac.rc.gateway.modules.ofs.service.VHistApprovalService;
import cn.conac.rc.gateway.modules.ofs.vo.OrgAppDateBaseIdVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="ofs/")
@Api(tags="历史沿革的列表", description="三定信息")
public class VHistApprovalController {

    @Autowired
    VHistApprovalService service;
    
	@ApiOperation(value = "对应orgId部门的历史沿革", httpMethod = "GET", response = JSONObject.class, notes = "根据部门主表的OrgId取得对应的历史沿革信息")
    @RequestMapping(value = "orgs/{orgId}/history", method = RequestMethod.GET)
    public JSONObject historyList(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "部门OrgId", required = true) @PathVariable("orgId") String orgId)
            throws RestClientException, Exception {
        return service.historyList(orgId);
    }
	
	
	@ApiOperation(value = "对应orgId部门的历史沿革的Map(baseId,批文时间)", httpMethod = "GET", response = JSONObject.class, notes = "根据部门主表的OrgId取得对应的历史沿革信息的Map(baseId,批文时间)")
    @RequestMapping(value = "history/{orgId}/map", method = RequestMethod.GET)
    public Object historyMap(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "部门OrgId", required = true) @PathVariable("orgId") String orgId)
            throws RestClientException, Exception {
		
		ResultPojo resultInfo = new ResultPojo();
		JSONObject  jsonObject =  service.historyList(orgId);
		JSONArray   jsonArrayList  =   jsonObject.getJSONArray("result");
		String isSimple = null;
		String baseId = null;
		Date appDate = null;
		String appDateStr = null;
		OrgAppDateBaseIdVo orgAppDateBaseIdVo =null;
		List <OrgAppDateBaseIdVo>  orgAppDateBaseIdVoList = new ArrayList<OrgAppDateBaseIdVo>();
		
		for (int cnt=0; cnt < jsonArrayList.size(); cnt++) {
			JSONObject   vHistApprovalJsonEntity =   (JSONObject)jsonArrayList.get(cnt);
			isSimple =  vHistApprovalJsonEntity.getString("isSimple");
			if(!Contsants.OFS_EVOL_ORG_FLAG_SIMPLE_INFO_TYPE.equals(isSimple)){
				baseId =  vHistApprovalJsonEntity.getString("id");
				appDate =  vHistApprovalJsonEntity.getDate("appDate");
				if(null != baseId && null != appDate){
					appDateStr = DateUtils.formatDate(appDate);
					orgAppDateBaseIdVo = new OrgAppDateBaseIdVo(baseId, appDateStr);
					orgAppDateBaseIdVoList.add(orgAppDateBaseIdVo);
				}
			}
		}
		
		resultInfo.setCode(ResultPojo.CODE_SUCCESS);
		resultInfo.setMsg(ResultPojo.MSG_SUCCESS);
		resultInfo.setResult(orgAppDateBaseIdVoList);
		
		return resultInfo;
    }

}
