package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.vo.VOrgPreSubVo;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface VOrgPreSubService {
	
	@RequestMapping(method = RequestMethod.GET, value = "/vOrgPreSub/list")
	 public JSONObject list(@ApiParam(value = "查询待提交部门信息对象", required = true) @RequestBody VOrgPreSubVo vOrgPreSubVo);

}
