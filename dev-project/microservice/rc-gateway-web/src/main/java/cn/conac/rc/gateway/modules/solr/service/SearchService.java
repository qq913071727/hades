package cn.conac.rc.gateway.modules.solr.service;

import java.util.Map;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-SOLR")
public interface SearchService {
	
	/**
	 * 根据输入关键字和区域ID检索机构信息
	 * @param condMap
	 * @return JSONObject
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/solr/searchInfo/org/")
	public JSONObject searchOrgInfo(@ApiParam(value = "查询条件对象", required = true) @RequestBody Map<String, String> condMap);
	
	/**
	 * 根据输入关键字和区域ID检索历史沿革机构信息
	 * @param condMap
	 * @return JSONObject
	 */
    @RequestMapping(method = RequestMethod.POST, value = "/solr/searchInfo/histOrg/")
    public JSONObject searchHistOrgInfo(@ApiParam(value = "查询条件对象", required = true) @RequestBody Map<String, String> condMap);
    
    
    /**
	 * 根据输入关键字和区域ID检索历史沿革机构信息
	 * @param condMap
	 * @return JSONObject
	 */
    @RequestMapping(method = RequestMethod.POST, value = "/solr/searchInfo/orgAnaly/")
    public JSONObject searchOrgAnalyInfo(@ApiParam(value = "查询条件对象", required = true) @RequestBody Map<String, String> condMap);
    
    
    /**
     * 根据输入关键字检索机构提示词信息
     * @param queryWord
     * @return JSONObject
     */
    @RequestMapping(method = RequestMethod.POST, value = "/solr/searchInfo/suggest/org/")
    public JSONObject searchOrgSuggInfo(@ApiParam(value = "条件对象", required = true) @RequestBody Map<String, String> condMap);

    /**
     * 根据输入关键字检索历史沿革机构提示词信息
     * @param queryWord
     * @return JSONObject
     */
    @RequestMapping(method = RequestMethod.GET, value = "/solr/searchInfo/suggest/histOrg/{queryWord}")
    public JSONObject searchHistOrgSuggInfo(@ApiParam(value = "queryWord", required = true) @PathVariable("queryWord") String queryWord);
    
    /**
     * 根据输入组合条件进行综合查询
     * @param condMap 组合条件封装在Map中
     * @return JSONObject
     */
	@RequestMapping(method = RequestMethod.POST, value = "/solr/compInfo/org/")
	public JSONObject searchOrgCompInfo(@ApiParam(value = "查询条件对象", required = true) @RequestBody Map<String, String> condMap) ;
	
	/**
	 * 根据输入关键字（批文名称或批文号）、区域ID、批文开始时间和批文结束时间检索机构信息
	 * @param condMap
	 * @return JSONObject
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/solr/searchInfo/orgApproval/")
	public JSONObject searchOrgApprovalInfo(@ApiParam(value = "查询条件对象", required = true) @RequestBody Map<String, String> condMap);


}
