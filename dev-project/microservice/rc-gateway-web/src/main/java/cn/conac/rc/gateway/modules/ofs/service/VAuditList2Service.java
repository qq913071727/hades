package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.vo.VAuditList2Vo;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface VAuditList2Service {
	
	@RequestMapping(method = RequestMethod.GET, value = "/vAuditList2/list")
	 public JSONObject list(@ApiParam(value = "查询委办局审核状态信息对象", required = true) @RequestBody VAuditList2Vo vAuditList2Vo);
	


}
