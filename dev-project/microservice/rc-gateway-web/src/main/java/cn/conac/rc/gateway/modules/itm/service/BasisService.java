package cn.conac.rc.gateway.modules.itm.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONArray;

/**
 * 实施依据类型相关Service
 * 
 * @author lucj
 *
 */
@FeignClient("RC-SERVICE-ITM")
public interface BasisService {

	@RequestMapping(method = RequestMethod.GET, value = "/aelItemBasis/list")
	JSONArray getBasisList();
	
	@RequestMapping(method = RequestMethod.GET, value = "/aelItemBasis/list/{pid}")
	JSONArray getBasisListByPid(@PathVariable(value = "pid") String pid);

}