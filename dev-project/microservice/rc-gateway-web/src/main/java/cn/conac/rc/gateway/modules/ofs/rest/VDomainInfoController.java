package cn.conac.rc.gateway.modules.ofs.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.RestClientException;
import cn.conac.rc.gateway.modules.ofs.service.VDomainInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="ofs/")
@Api(tags="部门的域名信息", description="三定信息")
public class VDomainInfoController {

    @Autowired
    VDomainInfoService vDomainInfoService;

    @ApiOperation(value = "域名相关信息", httpMethod = "GET", response = JSONObject.class, notes = "根据机构名称取得域名相关信息")
    @RequestMapping(value = "orgs/{orgName}/domainInfos", method = RequestMethod.GET)
    public JSONObject findListById(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "机构名称", required = true) @PathVariable("orgName") String orgName)
            throws RestClientException, Exception {
        return vDomainInfoService.findByOrgName(orgName);
    }

}