package cn.conac.rc.gateway.modules.ofs.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * OrgCombinEntity类
 *
 * @author beanCreator
 * @date 2016-11-01
 * @version 1.0
 */
@ApiModel
public class OrgCombinEntity implements Serializable {

	private static final long serialVersionUID = 2662194909293172604L;

	@ApiModelProperty("ID")
	private String id;

	@ApiModelProperty("机构ID号")
	private String baseId;

	@ApiModelProperty("并入机构ID号")
	private String orgId;

	public void setId(String id){
		this.id=id;
	}

	public String getId(){
		return id;
	}

	public void setBaseId(String baseId){
		this.baseId=baseId;
	}

	public String getBaseId(){
		return baseId;
	}

	public void setOrgId(String orgId){
		this.orgId=orgId;
	}

	public String getOrgId(){
		return orgId;
	}
}
