package cn.conac.rc.gateway.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

@Component
public class ZuulFileFilter extends ZuulFilter {
    private static Logger log = LoggerFactory.getLogger(ZuulFileFilter.class);

    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        //设置content-length header
        if (ctx.getOriginContentLength() != null)
            ctx.addZuulResponseHeader("Content-Length", ctx.getOriginContentLength() + "");
        return ctx;
    }
}