package cn.conac.rc.gateway.modules.share.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.gateway.base.ResultPojo;
import cn.conac.rc.gateway.modules.ofs.vo.OrganizationVo;
import cn.conac.rc.gateway.modules.share.entity.ShareAreaReviewEntity;
import cn.conac.rc.gateway.modules.share.service.ShareAreaReviewService;
import cn.conac.rc.gateway.modules.share.vo.ShareAreaReviewCUVo;
import cn.conac.rc.gateway.modules.share.vo.ShareAreaReviewParamVo;
import cn.conac.rc.gateway.security.UserUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value={"share/"} )
@Api(tags = "地区共享信息", description = "共享地区")
public class ShareAreaReviewController {
	
	@Autowired
	ShareAreaReviewService  shareAreaReviewService;
	
	@ApiOperation(value = "共享地区审核", httpMethod = "POST", response = OrganizationVo.class, notes = "共享地区审核")
	@RequestMapping(value = "areaInfoReview/c", method = RequestMethod.POST)
	public Object cudShareAreaReviewSave(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "保存对象", required = true) @RequestBody  ShareAreaReviewParamVo shareAreaReviewParamVo) throws Exception {
		ResultPojo resultInfo = new ResultPojo();
		// 共享地区申请参数的合法性检查
		if(null == shareAreaReviewParamVo.getShareInfoIdList() || shareAreaReviewParamVo.getShareInfoIdList().size() == 0) {
			// 共享的地区为空的场合
			resultInfo.setCode(ResultPojo.CODE_FAILURE);
			resultInfo.setMsg("要审核的共享信息ID列表不能为null或为空");
			resultInfo.setResult(null);
			return resultInfo;
		}
		Integer shareInfoId = null;
		for(int j=0; j < shareAreaReviewParamVo.getShareInfoIdList().size(); j++) {
			shareInfoId = shareAreaReviewParamVo.getShareInfoIdList().get(j);
			if(null == shareInfoId) {
				resultInfo.setCode(ResultPojo.CODE_FAILURE);
				resultInfo.setMsg("要审核的共享信息ID不能为null或为空");
				resultInfo.setResult(null);
				return resultInfo;
			}
		}
		
		if(null == shareAreaReviewParamVo.getReviewStatus()) {
			// 共享的地区为空的场合
			resultInfo.setCode(ResultPojo.CODE_FAILURE);
			resultInfo.setMsg("审核状态不能为空");
			resultInfo.setResult(null);
			return resultInfo;
		}
		ShareAreaReviewEntity shareAreaReviewEntity = null;
		List<ShareAreaReviewEntity> shareAreaReviewEntityList = new ArrayList<ShareAreaReviewEntity>();
		for(int j=0; j < shareAreaReviewParamVo.getShareInfoIdList().size(); j++) {
			shareAreaReviewEntity = new ShareAreaReviewEntity();
			shareAreaReviewEntity.setRefusalReason(shareAreaReviewParamVo.getRefusalReason());
			shareAreaReviewEntity.setReviewRemark(shareAreaReviewParamVo.getReviewRemark());
			if(shareAreaReviewParamVo.getReviewStatus().intValue() == 
					ShareAreaReviewEntity.SHARE_AREA_INFO_STATUS_PASSED) {
				shareAreaReviewEntity.setReviewStatus(ShareAreaReviewEntity.SHARE_AREA_INFO_STATUS_PASSED);
			} else if(shareAreaReviewParamVo.getReviewStatus().intValue() == 
					ShareAreaReviewEntity.SHARE_AREA_INFO_STATUS_REFUSE) {
				shareAreaReviewEntity.setReviewStatus(ShareAreaReviewEntity.SHARE_AREA_INFO_STATUS_REFUSE);
			}else {
				shareAreaReviewEntity.setReviewStatus(ShareAreaReviewEntity.SHARE_AREA_INFO_STATUS_HUNGUP);
			}
				
			shareAreaReviewEntity.setReviewTime(new Date());
			shareAreaReviewEntity.setReviewUserId(Integer.valueOf(UserUtils.getCurrentUser().getId()));
			shareAreaReviewEntity.setShareInfoId(shareAreaReviewParamVo.getShareInfoIdList().get(j));
			shareAreaReviewEntityList.add(shareAreaReviewEntity);
		}
		
		ShareAreaReviewCUVo shareAreaReviewCUVo = new ShareAreaReviewCUVo();
		shareAreaReviewCUVo.setShareAreaReviewEntityList(shareAreaReviewEntityList);
		shareAreaReviewCUVo.setShareInfoIdList(shareAreaReviewParamVo.getShareInfoIdList());
		shareAreaReviewCUVo.setReviewStatus(shareAreaReviewParamVo.getReviewStatus());
		// TODO 从DB中RC_CONF表中取值
		//shareAreaReviewCUVo.setValidDays(validDays);
		
		return shareAreaReviewService.batchSaveOrUpdate(shareAreaReviewCUVo);
	}
	
}
