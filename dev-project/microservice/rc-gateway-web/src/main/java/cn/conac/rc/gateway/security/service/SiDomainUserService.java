package cn.conac.rc.gateway.security.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.DateUtils;
import cn.conac.rc.framework.utils.MD5Utils;

@CacheConfig(keyGenerator = "keyGenerator", cacheNames = "si_cache", cacheManager = "redisCache1h")
@Service
public class SiDomainUserService {

	@Value("${si.api.url}")
	private String targetURL;

	@Value("${si.api.itemKey}")
	private String itemKey;

	@Value("${si.api.itemCode}")
	private String itemCode;

	public String getTargetURL() {
		return targetURL;
	}

	public String getItemKey() {
		return itemKey;
	}

	public void setTargetURL(String targetURL) {
		this.targetURL = targetURL;
	}

	public void setItemKey(String itemKey) {
		this.itemKey = itemKey;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	@Cacheable
	public JSONObject getSiDomainUser(String name, String product, String password) {
		try {
			URL targetUrl = new URL(this.targetURL);
			HttpURLConnection httpConnection = (HttpURLConnection) targetUrl.openConnection();
			httpConnection.setDoOutput(true);
			httpConnection.setRequestMethod("POST");
			httpConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
			JSONObject req = new JSONObject();
			// itemKey为预定一致的
			req.put("itemKey", this.itemKey);
			// req.put("domainName", "海口市救助管理站.公益");
			req.put("name", name);
			req.put("product", product);
			// req.put("password", "ysbb123456");
			req.put("password", password);
			// 发送请求时服务器时间 时间格式为yyyy-MM-dd hh:mm:ss
			Date currentDate = new Date();
			req.put("reqTime", DateUtils.getFormatDateTime(currentDate, "yyyy-MM-dd HH:mm:ss"));
			String itemCode = this.itemCode;
			MD5Utils encoderMd5 = new MD5Utils(itemCode, "MD5");
			String sign = encoderMd5.encode(DateUtils.getFormatDateTime(currentDate, "yyyy-MM-dd"));
			// System.out.println(sign);
			// 加密后的grandDate作为sign传入
			req.put("sign", sign);
			String input = req.toString();
			// System.out.println(input);
			OutputStreamWriter outputStream = new OutputStreamWriter(httpConnection.getOutputStream(), "UTF-8");
			// OutputStream outputStream = httpConnection.getOutputStream();
			outputStream.write(input);
			// outputStream.write(input.getBytes());
			outputStream.flush();

			BufferedReader responseBuffer = new BufferedReader(
					new InputStreamReader((httpConnection.getInputStream()), "UTF-8"));
			String output;
			// System.out.println("Output from Server:\n");
			JSONObject obj = null;
			while ((output = responseBuffer.readLine()) != null) {
				// 打印数据返回结果json
				// System.out.println(output);
				obj = JSONObject.parseObject(output);
			}
			JSONObject siUser = (JSONObject) obj.get("domainUserInfo_get_response");
			httpConnection.disconnect();
			return siUser;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getMsgByCode(String code) {
		switch (code) {
		case "100000":
			return "成功";
		case "110010":
			return "参数非法";
		case "110020":
			return "签名验证失败";
		case "110030":
			return "域名不存在";
		case "110040":
			return "域名已过期";
		case "110050":
			return "密码错误";
		case "110060":
			return "用户锁定";
		case "110099":
			return "系统异常";
		default:
			break;
		}
		return null;
	}

	/*
	 * public static void main(String[] args) { SiDomainUserService service =
	 * new SiDomainUserService(); service.setTargetURL(
	 * "http://172.17.80.17:9999/siServer/queryWs/getSiDomainUser/");
	 * service.setItemCode("72b0fc0833d7fb177b1682469744bb96");
	 * service.setItemKey("17172bd9bb53b887aa40706906986feb");
	 * System.out.println(service.getSiDomainUser("贵州省铜仁市编委办.政务", "30856001"));
	 * System.out.println(service.getMsgByCode(100000)); }
	 */

}
