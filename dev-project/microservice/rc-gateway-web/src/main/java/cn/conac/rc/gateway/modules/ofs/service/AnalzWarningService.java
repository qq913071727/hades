package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.vo.AnalzWarning;
import cn.conac.rc.gateway.modules.ofs.vo.AnalzWarningVo;

@FeignClient("RC-SERVICE-OFS")
public interface AnalzWarningService {
	@RequestMapping(value = "analzWarning/{id}", method = RequestMethod.GET)
	public JSONObject detail(
			@PathVariable("id") String id);

	@RequestMapping(value = "analzWarning/count", method = RequestMethod.POST)
	public JSONObject count(
			@RequestBody AnalzWarning vo) ;

	@RequestMapping(value = "analzWarning/list", method = RequestMethod.POST)
	public JSONObject list(
			@RequestBody AnalzWarningVo vo) ;

	@RequestMapping(value = "analzWarning/Department/{id}/Dutys", method = RequestMethod.GET)
	public JSONObject findDutyByDepartment(
			@PathVariable("id") String id) ;

	@RequestMapping(value = "analzWarning/Duty/{id}/Departments", method = RequestMethod.GET)
	public JSONObject findDepartmentByDuty(
			@PathVariable("id") String id) ;
	
	@RequestMapping(value = "/department/org/{id}/deptsWithNoDuty", method = RequestMethod.GET)
	public JSONObject finddeptWithNoDeptListByOrgId(
			@PathVariable("id") String id) ;

	@RequestMapping(value = "/duty/org/{id}/dutysWithNoDept", method = RequestMethod.GET)
	public JSONObject findDutyWithNoDutyListByOrgId(
			@PathVariable("id") String id) ;
}
