package cn.conac.rc.gateway.modules.ofs.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * OrgOthersEntity类
 *
 * @author beanCreator
 * @date 2016-11-01
 * @version 1.0
 */
@ApiModel
public class OrgOthersEntity implements Serializable {

	private static final long serialVersionUID = 6351175862554725544L;

	@ApiModelProperty("id")
	private String id;

	@ApiModelProperty("其他事项")
	private String orgOtherRemarks;

	@ApiModelProperty("附则")
	private String orgPrevisions;

	public void setId(String id){
		this.id=id;
	}

	public String getId(){
		return id;
	}

	public void setOrgOtherRemarks(String orgOtherRemarks){
		this.orgOtherRemarks=orgOtherRemarks;
	}

	public String getOrgOtherRemarks(){
		return orgOtherRemarks;
	}

	public void setOrgPrevisions(String orgPrevisions){
		this.orgPrevisions=orgPrevisions;
	}

	public String getOrgPrevisions(){
		return orgPrevisions;
	}
}
