package cn.conac.rc.gateway.modules.share.vo;

import java.io.Serializable;
import java.util.List;

import cn.conac.rc.gateway.common.entity.AreaEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ShareAreaInfoVo类
 *
 * @author voCreator
 * @date 2017-07-21
 * @version 1.0
 */
@ApiModel
public class ShareAreaInfoParamVo implements Serializable {
	
	private static final long serialVersionUID = 6462405758688598537L;

	@ApiModelProperty("申请人姓名")
	private String fullName;

	@ApiModelProperty("申请人电话")
	private String mobile;

	List<AreaEntity> areaDestList;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public List<AreaEntity> getAreaDestList() {
		return areaDestList;
	}

	public void setAreaDestList(List<AreaEntity> areaDestList) {
		this.areaDestList = areaDestList;
	}
	
}
