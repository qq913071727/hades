package cn.conac.rc.gateway.modules.monitor.vo;

import io.swagger.annotations.ApiModel;

@ApiModel
public class VLocalMonitorDay {
	/**
	 * tjDate
	 */
	private String tjDate;
	/**
	 * dfsxbm
	 */
	private String dfsxbm;
	/**
	 * sxmc
	 */
	private String sxmc;
	/**
	 * blbm
	 */
	private String blbm;
	/**
	 * xzqhbm
	 */
	private String xzqhbm;
	/**
	 * sqCount
	 */
	private Integer sqCount;
	private Integer cksqCount;
	private Integer wssqCount;
	/**
	 * slCount
	 */
	private Integer slCount;
	/**
	 * bslCount
	 */
	private Integer bslCount;
	/**
	 * bjCount
	 */
	private Integer bjCount;
	/**
	 * grbjCount
	 */
	private Integer grbjCount;
	
	/**
	 * frbjCount
	 */
	private Integer frbjCount;
	/**
	 * ycxbzCount
	 */
	private Integer ycxbzCount;
	/**
	 * bjcsCount
	 */
	private Integer bjcsCount;
	/**
	 * bzsCount
	 */
	private Integer bzsCount;
	/**
	 * 许可办结数
	 */
	private Integer xkbjCount;
	private Integer tbcxsqCount;
	private Integer tbcxcsCount;
	private Integer wbjcsCount;
	/**
	 * 办结用时天数
	 */
	private Integer bjystsCount;

	public void setTjDate(String tjDate) {
		this.tjDate = tjDate;
	}
	
	public String getTjDate() {
		return tjDate;
	}

	public void setDfsxbm(String dfsxbm) {
		this.dfsxbm = dfsxbm;
	}

	public String getDfsxbm() {
		return dfsxbm;
	}

	public void setBlbm(String blbm) {
		this.blbm = blbm;
	}

	public String getBlbm() {
		return blbm;
	}

	public void setXzqhbm(String xzqhbm) {
		this.xzqhbm = xzqhbm;
	}

	public String getXzqhbm() {
		return xzqhbm;
	}

	public void setSqCount(Integer sqCount) {
		this.sqCount = sqCount;
	}

	public Integer getSqCount() {
		return sqCount;
	}

	public void setSlCount(Integer slCount) {
		this.slCount = slCount;
	}

	public Integer getSlCount() {
		return slCount;
	}

	public void setBslCount(Integer bslCount) {
		this.bslCount = bslCount;
	}

	public Integer getBslCount() {
		return bslCount;
	}

	public void setBjCount(Integer bjCount) {
		this.bjCount = bjCount;
	}

	public Integer getBjCount() {
		return bjCount;
	}

	public void setYcxbzCount(Integer ycxbzCount) {
		this.ycxbzCount = ycxbzCount;
	}

	public Integer getYcxbzCount() {
		return ycxbzCount;
	}

	public void setBjcsCount(Integer bjcsCount) {
		this.bjcsCount = bjcsCount;
	}

	public Integer getBjcsCount() {
		return bjcsCount;
	}

	public void setBzsCount(Integer bzsCount) {
		this.bzsCount = bzsCount;
	}

	public Integer getBzsCount() {
		return bzsCount;
	}

	public Integer getGrbjCount() {
		return grbjCount;
	}

	public void setGrbjCount(Integer grbjCount) {
		this.grbjCount = grbjCount;
	}

	public Integer getFrbjCount() {
		return frbjCount;
	}

	public void setFrbjCount(Integer frbjCount) {
		this.frbjCount = frbjCount;
	}

	public Integer getXkbjCount() {
		return xkbjCount;
	}

	public void setXkbjCount(Integer xkbjCount) {
		this.xkbjCount = xkbjCount;
	}

	public Integer getCksqCount() {
		return cksqCount;
	}

	public void setCksqCount(Integer cksqCount) {
		this.cksqCount = cksqCount;
	}

	public Integer getWssqCount() {
		return wssqCount;
	}

	public void setWssqCount(Integer wssqCount) {
		this.wssqCount = wssqCount;
	}

	public Integer getTbcxsqCount() {
		return tbcxsqCount;
	}

	public void setTbcxsqCount(Integer tbcxsqCount) {
		this.tbcxsqCount = tbcxsqCount;
	}

	public Integer getTbcxcsCount() {
		return tbcxcsCount;
	}

	public void setTbcxcsCount(Integer tbcxcsCount) {
		this.tbcxcsCount = tbcxcsCount;
	}

	public Integer getWbjcsCount() {
		return wbjcsCount;
	}

	public void setWbjcsCount(Integer wbjcsCount) {
		this.wbjcsCount = wbjcsCount;
	}

	public String getSxmc()
	{
		return sxmc;
	}

	public void setSxmc(String sxmc)
	{
		this.sxmc = sxmc;
	}

	public Integer getBjystsCount()
	{
		return bjystsCount;
	}

	public void setBjystsCount(Integer bjystsCount)
	{
		this.bjystsCount = bjystsCount;
	}
}
