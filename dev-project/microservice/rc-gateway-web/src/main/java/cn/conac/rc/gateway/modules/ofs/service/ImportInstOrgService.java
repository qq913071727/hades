package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

@FeignClient("RC-SERVICE-OFS")
public interface ImportInstOrgService {
	
	/**
	 * 通过机构ID查询相关职能职责
	 * @param baseId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/importInstOrg/{name}")
	 public JSONObject findByName(@PathVariable(value = "name") String name);

}
