package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.entity.OrgStatusEntity;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface OrgStatusService {
	
    @RequestMapping(method = RequestMethod.POST, value = "/orgStatus/")
    public JSONObject saveOrUpdate(@ApiParam(value = "保存对象", required = true) @RequestBody OrgStatusEntity statusEntity);

	@RequestMapping(value = "orgStatus/{id}", method = RequestMethod.GET)
	public JSONObject findById(@PathVariable("id") Integer id);
}
