package cn.conac.rc.gateway.modules.ofs.entity;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * OrganizationEntity类
 *
 * @author beanCreator
 * @date 2016-11-02
 * @version 1.0
 */
@ApiModel
public class OrganizationEntity implements Serializable {

	private static final long serialVersionUID = 5058968014845526733L;

	@ApiModelProperty("id")
	private String id;

	@ApiModelProperty("baseId")
	private String baseId;

	@ApiModelProperty("备注")
	private String remarks;

	@ApiModelProperty("父单位")
	private String parentId;

	@ApiModelProperty("三定名称")
	private String name;

	@ApiModelProperty("行编事编")
	private String type;

	@ApiModelProperty("所属系统")
	private String ownSys;

	@ApiModelProperty("编码")
	private String code;

	@ApiModelProperty("状态（发布，撤并）")
	private String status;

	@ApiModelProperty("排序号")
	private String sort;

	@ApiModelProperty("areaId")
	private String areaId;

	@ApiModelProperty("areaCode")
	private String areaCode;

	@ApiModelProperty("是否主管")
	private String isComp;

	@ApiModelProperty("是否涉密")
	private String isSecret;

	@ApiModelProperty("地方编办")
	private String localCode;

	@ApiModelProperty("是否有下级机构")
	private String hasChild;
	
	@ApiModelProperty("录入人ID号")
	private Integer createUserId;
	
	@ApiModelProperty("创建时间")
	private Date createTime;
	
	@ApiModelProperty("更新用户ID")
	private Integer  updateUserId;
	
	@ApiModelProperty("更新时间")
	private Date updateTime;
	
	@ApiModelProperty("siUserCode")
	private String siUserCode;

	@ApiModelProperty("机构域名（多个以分号分割）")
	private String orgDomainName;

	public void setId(String id){
		this.id=id;
	}

	public String getId(){
		return id;
	}

	public void setBaseId(String baseId){
		this.baseId=baseId;
	}

	public String getBaseId(){
		return baseId;
	}

	public void setRemarks(String remarks){
		this.remarks=remarks;
	}

	public String getRemarks(){
		return remarks;
	}

	public void setParentId(String parentId){
		this.parentId=parentId;
	}

	public String getParentId(){
		return parentId;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setType(String type){
		this.type=type;
	}

	public String getType(){
		return type;
	}

	public void setOwnSys(String ownSys){
		this.ownSys=ownSys;
	}

	public String getOwnSys(){
		return ownSys;
	}

	public void setCode(String code){
		this.code=code;
	}

	public String getCode(){
		return code;
	}

	public void setStatus(String status){
		this.status=status;
	}

	public String getStatus(){
		return status;
	}

	public void setSort(String sort){
		this.sort=sort;
	}

	public String getSort(){
		return sort;
	}

	public void setAreaId(String areaId){
		this.areaId=areaId;
	}

	public String getAreaId(){
		return areaId;
	}

	public void setAreaCode(String areaCode){
		this.areaCode=areaCode;
	}

	public String getAreaCode(){
		return areaCode;
	}

	public void setIsComp(String isComp){
		this.isComp=isComp;
	}

	public String getIsComp(){
		return isComp;
	}

	public void setIsSecret(String isSecret){
		this.isSecret=isSecret;
	}

	public String getIsSecret(){
		return isSecret;
	}

	public void setLocalCode(String localCode){
		this.localCode=localCode;
	}

	public String getLocalCode(){
		return localCode;
	}

	public void setHasChild(String hasChild){
		this.hasChild=hasChild;
	}

	public String getHasChild(){
		return hasChild;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public void setSiUserCode(String siUserCode){
		this.siUserCode=siUserCode;
	}

	public String getSiUserCode(){
		return siUserCode;
	}
	
	public void setOrgDomainName(String orgDomainName){
		this.orgDomainName=orgDomainName;
	}

	public String getOrgDomainName(){
		return orgDomainName;
	}
}
