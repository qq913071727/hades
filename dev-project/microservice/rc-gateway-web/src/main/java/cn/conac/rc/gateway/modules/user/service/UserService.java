package cn.conac.rc.gateway.modules.user.service;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.user.vo.RcUserVo;
import cn.conac.rc.gateway.modules.user.vo.UserInfo2Vo;
import cn.conac.rc.gateway.modules.user.vo.UserInfoVo;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-USER")
public interface UserService {
	
	/**
	 * 通过条件查询用户分页列表信息
	 * @param rcUserVo 查询用户信息视图对象
	 * @return JSONObject
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/rcUser/page")
	public JSONObject findPageByCondtion(@ApiParam(value = "查询用户信息视图对象", required = true) @RequestBody RcUserVo rcUserVo);
	/**
	 * 通过条件查询用户列表信息（不分页）
	 * @param rcUserVo 查询用户信息视图对象
	 * @return JSONObject
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/rcUser/")
	public JSONObject findListByCondtion(@ApiParam(value = "查询用户信息视图对象", required = true) @RequestBody RcUserVo rcUserVo);
	/**
	 * 更新用户表的是否删除字段(逻辑删除用户信息)
	 * @param userInfo2Vo
	 * @return 更新结果
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/user/ud")
	public JSONObject updateIsDeletedByBatch(@ApiParam(value = "封装接收页面选择用户ID的对象", required = true) @RequestBody UserInfo2Vo userInfo2Vo);
	
	
	/**
	 * 保存触发同步资源库表的信息
	 * @param userId 当前导入域名用户信息的userId
	 * @param impCnt 导入的域名用户数
	 * @return 结果
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/dum/{userId}/{impCnt}/save")
	public JSONObject saveTriggerData(@PathVariable(value = "userId") String userId, @PathVariable(value = "impCnt") String impCnt);
	
	
	/**
	 * 更新用户表的是否删除字段(逻辑删除用户信息)
	 * @param userInfo2Vo 
	 * @return 更新结果
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/user/active")
	public JSONObject updateIsDisabledByBatch(@ApiParam(value = "封装接收页面选择用户ID的对象", required = true) @RequestBody UserInfo2Vo userInfo2Vo);
	
	
	/**
	 * 保存用户信息
	 * @param UserInfoVo 封装接收页面相关用户对象
	 * @return JSONObject
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/userComm/save")
	public JSONObject saveUserInfo(@ApiParam(value = "封装接收页面相关用户对象", required = true) @RequestBody UserInfoVo userInfoVo);
	
	
	/**
	 * 更新用户信息
	 * @param UserInfoVo 封装接收页面相关用户对象
	 * @return JSONObject
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/userComm/{userId}/update")
	public JSONObject updateUserInfo(@PathVariable(value = "userId") Integer userId, @ApiParam(value = "封装接收页面相关用户对象", required = true) @RequestBody UserInfoVo userInfoVo);
	
	/**
	 * 根据用户ID查询用户信息
	 * @param userId
	 * @return 用户信息
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/rcUser/{userId}")
	public JSONObject findByUserId(@PathVariable(value = "userId") Integer userId);
	
	/**
	 * 根据手机号查询用户信息
	 * @param mobile
	 * @return 用户信息
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/rcUser/mobile/{mobile}")
	public JSONObject findByMobile(@PathVariable(value = "mobile") String mobile);
	
	
	/**
	 * 根据身份证号查询用户信息
	 * @param idNumber
	 * @return 用户信息
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/rcUser/idnum/{idNumber}")
	public JSONObject findByIdNumber(@PathVariable(value = "idNumber") String idNumber);
	
	/**
	 * 根据身份证号查询用户信息
	 * @param idNumbers
	 * @return 用户信息
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/rcUser/idNumbers")
	public JSONObject findByIdNumbers(@ApiParam(value = "身份证对象List", required = true) @RequestBody List<String> idNumbers);
	
	/**
	 * 保存用户信息
	 * @param UserInfoVo 封装接收页面相关用户对象
	 * @return JSONObject
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/userComm/batchSave")
	public JSONObject saveUserInfoList(@ApiParam(value = "封装导入用户信息对象List", required = true) @RequestBody List<UserInfoVo> userInfoVoList);
	
	/**
	 * 保存或者更新用户信息
	 * @param UserInfoVo 封装接收页面相关用户对象
	 * @return JSONObject
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/userComm/batchSaveOrUpdate")
	public JSONObject saveOrUpdateUserInfoList(@ApiParam(value = "封装导入用户信息对象List", required = true) @RequestBody List<UserInfoVo> userInfoVoList);
	
}
