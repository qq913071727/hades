package cn.conac.rc.gateway.modules.ofs.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ApprovalEntity类
 *
 * @author beanCreator
 * @date 2016-11-02
 * @version 1.0
 */
@ApiModel
public class ApprovalEntity implements Serializable {

	private static final long serialVersionUID = -7438805563011166440L;

	@ApiModelProperty("ID")
	private String id;

	@ApiModelProperty("批文名称")
	private String name;

	@ApiModelProperty("批文类型")
	private String appType;

	@ApiModelProperty("批文号")
	private String appNum;

	@ApiModelProperty("批文时间")
	private String appDate;

	@ApiModelProperty("发放单位")
	private String payOrgName;

	@ApiModelProperty("撤并原因")
	private String removeReason;

	@ApiModelProperty("摘要(备注)")
	private String remarks;

	@ApiModelProperty("是否重定义标识")
	private String isRedefined;

	public void setId(String id){
		this.id=id;
	}

	public String getId(){
		return id;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setAppType(String appType){
		this.appType=appType;
	}

	public String getAppType(){
		return appType;
	}

	public void setAppNum(String appNum){
		this.appNum=appNum;
	}

	public String getAppNum(){
		return appNum;
	}

	public void setAppDate(String appDate){
		this.appDate=appDate;
	}

	public String getAppDate(){
		return appDate;
	}

	public void setPayOrgName(String payOrgName){
		this.payOrgName=payOrgName;
	}

	public String getPayOrgName(){
		return payOrgName;
	}

	public void setRemoveReason(String removeReason){
		this.removeReason=removeReason;
	}

	public String getRemoveReason(){
		return removeReason;
	}

	public void setRemarks(String remarks){
		this.remarks=remarks;
	}

	public String getRemarks(){
		return remarks;
	}

	public void setIsRedefined(String isRedefined){
		this.isRedefined=isRedefined;
	}

	public String getIsRedefined(){
		return isRedefined;
	}
}
