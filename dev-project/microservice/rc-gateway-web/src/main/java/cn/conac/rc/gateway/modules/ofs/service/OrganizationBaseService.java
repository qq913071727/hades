package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.entity.OrganizationBaseEntity;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface OrganizationBaseService {
	
	@RequestMapping(method = RequestMethod.GET, value = "/organizationBase/{id}")
	 public JSONObject findById(@ApiParam(value = "id", required = true) @PathVariable("id") Integer id);
	
	@RequestMapping(method = RequestMethod.GET, value = "/orgnization/{orgId}")
	 public JSONObject findByOrgId(@ApiParam(value = "orgId", required = true) @PathVariable("orgId") Integer orgId);
	
	 /**
     *部门基本信息的新增或者更新
     * @param baseEntity
     * @param submitType  （1： 正常保存 0： 暂存）
     * @return JSONObject
     */
	@RequestMapping(method = RequestMethod.POST, value = "/organizationBase/{submitType}")
	 public JSONObject saveOrUpdate(@ApiParam(value = "保存部门基本信息表", required = true) @RequestBody OrganizationBaseEntity baseEntity, @PathVariable("submitType") String submitType);

	
	@RequestMapping(method = RequestMethod.GET, value = "/organizationBase/{areaId}/preRealseList")
	 public JSONObject preRealseList(@ApiParam(value = "areaId", required = true) @PathVariable("areaId") String areaId);

}
