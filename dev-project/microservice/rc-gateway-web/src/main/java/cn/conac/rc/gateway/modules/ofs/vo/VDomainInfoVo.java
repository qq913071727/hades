package cn.conac.rc.gateway.modules.ofs.vo;

import java.io.Serializable;

import cn.conac.rc.gateway.base.BaseVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class VDomainInfoVo extends BaseVo implements Serializable {

	private static final long serialVersionUID = 1473339485538846798L;

	@ApiModelProperty("domainName")
	private String domainName;

	@ApiModelProperty("orgName")
	private String orgName;

	@ApiModelProperty("domainNameEn")
	private String domainNameEn;

	@ApiModelProperty("netName")
	private String netName;

	public void setDomainName(String domainName){
		this.domainName=domainName;
	}

	public String getDomainName(){
		return domainName;
	}

	public void setOrgName(String orgName){
		this.orgName=orgName;
	}

	public String getOrgName(){
		return orgName;
	}

	public void setDomainNameEn(String domainNameEn){
		this.domainNameEn=domainNameEn;
	}

	public String getDomainNameEn(){
		return domainNameEn;
	}

	public void setNetName(String netName){
		this.netName=netName;
	}

	public String getNetName(){
		return netName;
	}

}
