package cn.conac.rc.gateway.modules.user.vo;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;

/**
 * UserInfo2Vo类
 *
 * @author voCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
public class UserInfo2Vo  implements Serializable {

	private static final long serialVersionUID = -6540651223115582792L;

	private List<Integer> userIdList;
	
	private Integer isDisabled;

	public List<Integer> getUserIdList() {
		return userIdList;
	}

	public void setUserIdList(List<Integer> userIdList) {
		this.userIdList = userIdList;
	}

	public Integer getIsDisabled() {
		return isDisabled;
	}

	public void setIsDisabled(Integer isDisabled) {
		this.isDisabled = isDisabled;
	}
	
}
