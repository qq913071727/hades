package cn.conac.rc.gateway.common.vo;

import java.io.Serializable;

/**
 * 对数据库表（增删改（CUD））操作日志VO
 * @author lucj
 */

public class LogVo  implements Serializable {
	
	private static final long serialVersionUID = -128572963232361402L;

	/**
     * 用户ID
     */
    private String userId;
    
	/**
     * 用户名
     */
    private String userName;
    
    /**
     * 访问IP地址
     */
    private String remoteAddr;
    
    /**
     * 本地服务IP地址
     */
    private String  localAddr;

    /**
     * 用户代理
     */
    private String userAgent;

    /**
     * 请求URI
     */
    private String requestUri;

    /**
     * 方法名
     */
    private String method;
    
    /**
     * 请求的方法（GET/POST）
     */
    private String httpMethod;

    /**
     * 方法参数
     */
    private String params;
    

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRemoteAddr() {
		return remoteAddr;
	}

	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}
	
	public String getLocalAddr() {
		return localAddr;
	}

	public void setLocalAddr(String localAddr) {
		this.localAddr = localAddr;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getRequestUri() {
		return requestUri;
	}

	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	@Override
	public String toString() {
		return "LogVo [userId=" + userId + ", userName=" + userName + ", remoteAddr=" + remoteAddr + ", localAddr="
				+ localAddr + ", userAgent=" + userAgent + ", requestUri=" + requestUri + ", method=" + method
				+ ", httpMethod=" + httpMethod + ", params=" + params + "]";
	}
}