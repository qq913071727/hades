package cn.conac.rc.gateway.modules.user.rest;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.RestClientException;
import cn.conac.rc.framework.utils.ResultPojo;
import cn.conac.rc.gateway.modules.user.entity.RoleEntity;
import cn.conac.rc.gateway.modules.user.entity.RoleEntity2;
import cn.conac.rc.gateway.modules.user.service.RoleService;
import cn.conac.rc.gateway.modules.user.vo.PermVo;
import cn.conac.rc.gateway.modules.user.vo.RoleInfo2Vo;
import cn.conac.rc.gateway.modules.user.vo.RoleInfoVo;
import cn.conac.rc.gateway.security.UserUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="sys/")
@Api(tags="角色信息接口", description="角色信息")
public class RoleController {

    @Autowired
    RoleService roleService;
    
    @ApiOperation(value = "角色信息保存", httpMethod = "POST", response = JSONObject.class, notes = "角色信息保存<br>" + "示例如下：<br>" +
    "{<br>\"isSuper\":\"0\",<br>\"priority\":8,<br>\"roleName\":\"测试CONAC系统管理员\",<br>\"rolePermissionList\":[<br>{<br>\"uri\":\"group:o_save\"<br>},<br>{<br>\"uri\":\"role:v_tree\"<br>},<br>{<br>\"uri\":\"frame:workbench\"<br>}<br>],<br>\"roleType\":\"1\",<br>}<br>")
    @RequestMapping(value = "roles/c", method = RequestMethod.POST)
    public Object cudRoleInfoSave(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "角色信息的entity(附带权限)", required = true)  @RequestBody RoleEntity roleEntity  )
            throws RestClientException, Exception {
    	
    	 RoleEntity2 roleEntity2 = new RoleEntity2();
    	 // 设置角色名字
    	 roleEntity2.setRoleName(roleEntity.getRoleName());
    	 // 设置是否拥有所用权限
    	 roleEntity2.setIsSuper(roleEntity.getIsSuper());
    	// 设置角色顺序号
    	 roleEntity2.setPriority(roleEntity.getPriority());
    	// 设置角色类型
    	 roleEntity2.setRoleType(roleEntity.getRoleType());
    	 // 设置创建时间
    	 roleEntity2.setRegisterDate(new Date());
    	 // 设置创建用户ID
    	 roleEntity2.setRegisterUserId(Integer.valueOf(UserUtils.getCurrentUser().getId()));
    	 
    	 RoleInfoVo roleInfoVo =new RoleInfoVo();
    	 roleInfoVo.setRoleEntity(roleEntity2);
    	 roleInfoVo.setRolePermissionEntityList(roleEntity.getRolePermissionList());
    	 
    	 return roleService.saveRoleInfo(roleInfoVo);
    }
    
    @ApiOperation(value = "角色信息更新", httpMethod = "POST", response = JSONObject.class, notes = "角色信息更新<br>" + "示例如下：<br>" +
        "{<br>\"isSuper\":\"0\",<br>\"priority\":8,<br>\"roleName\":\"测试CONAC系统管理员\",<br>\"rolePermissionList\":[<br>{<br>\"uri\":\"group:o_save\"<br>},<br>{<br>\"uri\":\"role:v_tree\"<br>},<br>{<br>\"uri\":\"frame:workbench\"<br>}<br>],<br>\"roleType\":\"1\",<br>}<br>")
    @RequestMapping(value = "roles/{roleId}/u", method = RequestMethod.POST)
    public Object cudRoleInfoUpdate(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam("角色ID") @PathVariable("roleId") Integer roleId,
            @ApiParam(value = "角色信息的entity(附带权限)", required = true)  @RequestBody RoleEntity roleEntity )
            throws RestClientException, Exception {
    	
    	 ResultPojo resultInfo = new ResultPojo();
    	 if(null == roleId) {
    		  // 参数baseId为null场合
	       	  resultInfo.setCode(ResultPojo.CODE_FAILURE);
	          resultInfo.setMsg("[ roleId of parameter is null ] in function of cudRoleInfoUpdate " + ResultPojo.MSG_FAILURE);
	       	  resultInfo.setResult(null);
	       	  return resultInfo;
    	 }
    	 RoleEntity2 roleEntity2 = new RoleEntity2();
    	 // 设置角色名字
    	 roleEntity2.setRoleName(roleEntity.getRoleName());
    	 // 设置是否拥有所用权限
    	 roleEntity2.setIsSuper(roleEntity.getIsSuper());
    	// 设置角色顺序号
    	 roleEntity2.setPriority(roleEntity.getPriority());
    	// 设置角色类型
    	 roleEntity2.setRoleType(roleEntity.getRoleType());
    	 // 设置更新用户ID
    	 roleEntity2.setUpdateUserId(Integer.valueOf(UserUtils.getCurrentUser().getId()));
    	 // 设置更新时间
    	 roleEntity2.setUpdateDate(new Date());
    	 // 设置角色ID
    	 roleEntity2.setRoleId(roleId);
    	 
    	 RoleInfoVo roleInfoVo =new RoleInfoVo();
    	 roleInfoVo.setRoleEntity(roleEntity2);
    	 roleInfoVo.setRolePermissionEntityList(roleEntity.getRolePermissionList());
    	 
    	 return roleService.updateRoleInfo(roleId,roleInfoVo);
    }
    
    
    @ApiOperation(value = "角色信息物理删除", httpMethod = "POST", response = JSONObject.class, notes = "角色信息物理删除")
    @RequestMapping(value = "roles/d", method = RequestMethod.POST)
    public Object cudDeleteRoleInfo(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam(value = "角色ID的list", required = true) @RequestBody RoleInfo2Vo  roleInfo2Vo )
            throws RestClientException, Exception {
    	
    	 ResultPojo resultInfo = new ResultPojo();
    	 if(null == roleInfo2Vo || null == roleInfo2Vo.getRoleIdList()) {
	       	  resultInfo.setCode(ResultPojo.CODE_FAILURE);
	          resultInfo.setMsg("[ roleInfo2Vo or roleInfo2Vo.getRoleIdList() of parameter is null ] in function of cudDeleteRoleInfo " + ResultPojo.MSG_FAILURE);
	       	  resultInfo.setResult(null);
	       	  return resultInfo;
 	    }
         return  roleService.deletedByBatch(roleInfo2Vo.getRoleIdList());
    }
    
    @ApiOperation(value = "角色详情(附带权限列表信息)", httpMethod = "GET", response = JSONObject.class, notes = "角色详情(附带限列表信息)")
    @RequestMapping(value = "roles/{roleId}/r", method = RequestMethod.GET)
    public Object findRoleInfoByRoleId(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam("角色ID") @PathVariable("roleId") Integer roleId)
            throws RestClientException, Exception {
    	 ResultPojo resultInfo = new ResultPojo();
    	 if(null == roleId) {
    		  // 参数baseId为null场合
	       	  resultInfo.setCode(ResultPojo.CODE_FAILURE);
	          resultInfo.setMsg("[ roleId of parameter is null ] in function of findRoleInfoByRoleId " + ResultPojo.MSG_FAILURE);
	       	  resultInfo.setResult(null);
	       	  return resultInfo;
    	 }
         return  roleService.findByRoleId(roleId);
    }
    
    @ApiOperation(value = "根据角色ID获取权限树信息", httpMethod = "POST", response = JSONObject.class, notes = "根据角色ID获取权限树信息")
    @RequestMapping(value = "roles/{roleId}/perms/tree", method = RequestMethod.POST)
    public Object findPermTreeByRoleId(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam("角色ID") @PathVariable("roleId") Integer roleId)
            throws RestClientException, Exception {
    	
    	 ResultPojo resultInfo = new ResultPojo();
    	 if(null == roleId) {
    		  // 参数baseId为null场合
	       	  resultInfo.setCode(ResultPojo.CODE_FAILURE);
	          resultInfo.setMsg("[ roleId of parameter is null ] in function of findRoleInfoByRoleId " + ResultPojo.MSG_FAILURE);
	       	  resultInfo.setResult(null);
	       	  return resultInfo;
    	 }
    	 PermVo permVo = new PermVo();
     	 permVo.setDisplay("1");
     	 
         // 成功保存部门信息表和部门状态表返回
    	 return  roleService.findPermTreeByRoleId(roleId,permVo);
    }
    
    @ApiOperation(value = "角色信息列表", httpMethod = "GET", response = JSONObject.class, notes = "根据条件查询角色信息")
    @RequestMapping(value = "roles/list", method = RequestMethod.GET)
    public Object findRoleInfoList(HttpServletRequest request, HttpServletResponse response)
            throws RestClientException, Exception {
	   	 return  roleService.findRoleList();
    }
    
}
