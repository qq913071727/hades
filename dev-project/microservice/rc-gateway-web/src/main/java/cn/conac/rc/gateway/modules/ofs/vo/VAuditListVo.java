package cn.conac.rc.gateway.modules.ofs.vo;

import java.util.Date;

import cn.conac.rc.gateway.modules.ofs.entity.VAuditListEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * VAuditListVo类
 *
 * @author voCreator
 * @date 2016-11-21
 * @version 1.0
 */
@ApiModel
public class VAuditListVo extends VAuditListEntity {

	private static final long serialVersionUID = -633077112702357941L;

	@ApiModelProperty("当前分页")
	private Integer page;

	@ApiModelProperty("每页个数")
	private Integer size;

	@ApiModelProperty("提交开始时间")
	private Date updateTimeStart;

	@ApiModelProperty("提交结束时间")
	private Date updateTimeEnd;
	
	@ApiModelProperty("排序标示[1:升序  2:降序 (orderFlag的值不传的时候，默认为降序)]")
	private Integer orderFlag;

	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}

	public void setUpdateTimeStart(Date updateTimeStart){
		this.updateTimeStart = updateTimeStart;
	}

	public Date getUpdateTimeStart(){
		return updateTimeStart;
	}

	public void setUpdateTimeEnd(Date updateTimeEnd){
		this.updateTimeEnd = updateTimeEnd;
	}

	public Date getUpdateTimeEnd(){
		return updateTimeEnd;
	}

	public Integer getOrderFlag() {
		return orderFlag;
	}

	public void setOrderFlag(Integer orderFlag) {
		this.orderFlag = orderFlag;
	}
}
