package cn.conac.rc.gateway.modules.other.vo;

import cn.conac.rc.framework.utils.StringUtils;
import cn.conac.rc.gateway.modules.common.vo.UserVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class OauthUserVO {

    @ApiModelProperty("用户id")
    private String id;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("人员照片Url")
    private String imgPath;

    @ApiModelProperty("编制编码")
    private String bzbm;

    @ApiModelProperty("身份证号")
    private String idnum;

    @ApiModelProperty("编制域名")
    private String bzym;

    @ApiModelProperty("监督电话")
    private String jddh;

    /////////////////////////////////////////

    @ApiModelProperty("所属机构")
    private String ssjg;

    @ApiModelProperty("内设机构")
    private String nsjg;

    @ApiModelProperty("编制类型")
    private String bzlx;

    @ApiModelProperty("在编状态信息")
    private String zbzt;

    // @ApiModelProperty("取得人员照片api接口")
    // private String imgApi;

    public OauthUserVO() {
    }

    public OauthUserVO(UserVo user) {
        this.id = user.getId();
        this.ssjg = user.getOrgName();
        this.name = user.getFullName();
        this.setBzbm(user.getFormCode());
        this.setBzlx(user.getFormType());
        if (StringUtils.isNotEmpty(user.getOrgDomainName()) && StringUtils.isNotEmpty(name)) {
            this.setBzym(name + "." + user.getOrgDomainName());
        }
        this.setZbzt(user.getFormStatus());
        this.setIdnum(user.getIdNumber());
        this.setNsjg(user.getDeptName());
        this.setJddh(user.getMonitPhone());
        this.setImgPath("http://jgbzy.conac.cn/api/public/oauth/user/portrait/" + user.getId());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getBzbm() {
        return bzbm;
    }

    public void setBzbm(String bzbm) {
        this.bzbm = bzbm;
    }

    public String getIdnum() {
        return idnum;
    }

    public void setIdnum(String idnum) {
        this.idnum = idnum;
    }

    public String getBzym() {
        return bzym;
    }

    public void setBzym(String bzym) {
        this.bzym = bzym;
    }

    public String getJddh() {
        return jddh;
    }

    public void setJddh(String jddh) {
        this.jddh = jddh;
    }

    public String getSsjg() {
        return ssjg;
    }

    public void setSsjg(String ssjg) {
        this.ssjg = ssjg;
    }

    public String getNsjg() {
        return nsjg;
    }

    public void setNsjg(String nsjg) {
        this.nsjg = nsjg;
    }

    public String getBzlx() {
        return bzlx;
    }

    public void setBzlx(String bzlx) {
        this.bzlx = bzlx;
    }

    public String getZbzt() {
        return zbzt;
    }

    public void setZbzt(String zbzt) {
        this.zbzt = zbzt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
