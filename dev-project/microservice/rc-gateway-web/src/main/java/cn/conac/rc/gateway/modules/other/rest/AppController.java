package cn.conac.rc.gateway.modules.other.rest;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.ResultPojo;
import cn.conac.rc.gateway.modules.common.service.AreaService;
import cn.conac.rc.gateway.modules.itm.service.ItemService;
import cn.conac.rc.gateway.modules.monitor.vo.StatisticsVO;
import cn.conac.rc.gateway.modules.ofs.service.OrganizationReadService;
import cn.conac.rc.gateway.security.UserUtils;
import cn.conac.rc.gateway.security.vo.JwtUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags = "业务组合接口", description = "APP")
public class AppController {

    @Autowired
    OrganizationReadService organizationService;

    @Autowired
    AreaService areaService;

    @Autowired
    ItemService itemService;

    @ApiOperation(value = "app首页数据", notes = "返回app首页的数据，包括下属区划、部门统计、近期变更等", response = StatisticsVO.class)
    @RequestMapping(value = "/app/home", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> getStatisticsCounts(HttpServletRequest request, HttpServletResponse response,
            @ApiParam("地区id") @RequestParam String areaId, @ApiParam("地区code") @RequestParam String areaCode)
            throws Exception {
    	
    	JwtUser user = UserUtils.getCurrentUser();
    	
        JSONObject json = new JSONObject();

        // 取得下属地区
        ResultPojo result = new ResultPojo();

        result = areaService.getSubAreas(user.getAreaId());

        if (ResultPojo.CODE_SUCCESS.equals(result.getCode())) {
            json.put("subAreas", result.getResult());
        }
        
        //增加用户本身的地区
        if(!areaId.equals(user.getAreaId())){
            JSONArray array = json.getJSONArray("subAreas");
            result = areaService.detail(user.getAreaId());
            array.add(0, result.getResult());
            json.put("subAreas", array);
        }

        // 近期三定变更
        JSONObject obj = organizationService.findRecentOrgs4App(5, areaCode);
        if (ResultPojo.CODE_SUCCESS.equals(obj.getString("code"))) {
            json.put("recentOrgs", obj.getJSONArray("result"));
        }

        // 机关与事业单位比率
        obj = organizationService.CountXZJGAndSYDW(areaCode);
        if (ResultPojo.CODE_SUCCESS.equals(obj.getString("code"))) {
            json.put("count", obj.getJSONArray("result"));
        }

        // 部门对应权责数量
        obj = itemService.findItemsCntByAreaId(areaId);
        if(ResultPojo.CODE_SUCCESS.equals(obj.getString("code"))){
            json.put("itemsCnt", obj.getJSONObject("result"));
        }
        
        HashMap<String, String> map = new HashMap<>();
		if ("中央机构编制委员会办公室".equals(user.getOrgName()) && user.getIsAdmin().equals("0")) {
			// 1) 办内人员不开放业务圈，业务圈置灰，不可点击，打开APP后直接进入到信息圈；
			// 2) 信息圈不开放交流系统和资料共享系统，两个系统图标置灰，不可点击，首页屏蔽交流系统内容。
			map.put("YW", "0");// 业务圈 0禁用，1启用
			map.put("XX.JL", "0");// 信息圈的交流系统
			map.put("XX.ZL", "0");// 信息圈的资料系统
		}
		json.put("view", map);
        
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(json);

        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);

    }
}
