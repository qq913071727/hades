package cn.conac.rc.gateway.modules.itm.service;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.itm.vo.AelItemVo;

@FeignClient("RC-SERVICE-ITM")
@CacheConfig(keyGenerator = "keyGenerator", cacheNames = "itm_cache")
public interface ItemService
{
    @RequestMapping(value = "/duty/{dutyId}/aelItems", method = RequestMethod.GET)
    public JSONObject findAelItemByDutyId(@PathVariable("dutyId") String dutyId);

    @RequestMapping(value = "/aelItem/{id}", method = RequestMethod.GET)
    public JSONObject AelItemDetail(@PathVariable("id") String id);

    @RequestMapping(value = "/delItemApp/{id}", method = RequestMethod.GET)
    public JSONObject DelItemDetailApp(@PathVariable("id") String id);

    @RequestMapping(value = "/delItem/{id}", method = RequestMethod.GET)
    public JSONObject DelItemDetail(@PathVariable("id") String id);

    @RequestMapping(value = "/duty/{dutyId}/delItems", method = RequestMethod.GET)
    public JSONObject findDelItemByDutyId(@PathVariable("dutyId") String dutyId);

    @RequestMapping(value = "/aelItem/list", method = RequestMethod.POST)
    public JSONObject findAelItemByOrgId(@RequestBody JSONObject aelItemVo);

    @RequestMapping(value = "/aelItem/list", method = RequestMethod.POST)
    public JSONObject findAelItemList(@RequestBody AelItemVo aelItemVo);

    @RequestMapping(value = "/aelItemsInfo/list", method = RequestMethod.POST)
    public JSONObject findAelItemInfoList(@RequestBody AelItemVo aelItemVo);

    @RequestMapping(value = "/aelItemDeploied/{id}", method = RequestMethod.GET)
    public JSONObject aelItemDeploiedDetail(@PathVariable("id") String itemId);

    @RequestMapping(value = "/delItem/list", method = RequestMethod.POST)
    public JSONObject findDelItemByOrgId(@RequestBody JSONObject delItemVo);
    
    @Cacheable(cacheManager="redisCache4h")
    @RequestMapping(value = "/org/{orgId}/itemCnts", method = RequestMethod.GET)
    public JSONObject findItemsCnt(@PathVariable("orgId") String orgId);
    
    @Cacheable(cacheManager="redisCache4h")
    @RequestMapping(value = "/{areaId}/item/counts", method = RequestMethod.GET)
    public JSONObject findItemsCntByAreaId(@PathVariable("areaId") String areaId);

    @RequestMapping(value = "/aelItemsInfo/", method = RequestMethod.POST)
    public JSONObject addAelItemsInfo(@RequestBody AelItemVo aelItemVo);

    @RequestMapping(value = "/aelItems/{id}/delete", method = RequestMethod.POST)
    public JSONObject deleteAelItems(@PathVariable("id") String id);
}
