package cn.conac.rc.gateway.modules.share.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.share.vo.ShareAreaInfoCUVo;
import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-SHARE")
public interface ShareAreaInfoService {

	/**
	 * 批量保存或者更新申请地区共享信息表
	 * @param shareAreaInfoCUVo
	 * @return 更新结果
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/shareAreaInfo/saveOrUpdate")
	public JSONObject batchSaveOrUpdate(@ApiParam(value = "封装申请地区共享信息表的对象", required = true) @RequestBody ShareAreaInfoCUVo shareAreaInfoCUVo);
	
	/**
	 * 获取审核中和审核通过并在有效期内的地区共享信息(验证是否可提交申请用)
	 * @param areaCode
	 * @return 更新结果
	 */
	@RequestMapping(value = "shareAreaInfo/{areaCode}/list", method = RequestMethod.GET)
	public JSONObject findSharedAreaInfoList(@PathVariable("areaCode") String areaCode);
	
}
