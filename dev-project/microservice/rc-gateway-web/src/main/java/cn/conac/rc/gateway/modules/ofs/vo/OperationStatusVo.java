package cn.conac.rc.gateway.modules.ofs.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;

@ApiModel
public class OperationStatusVo implements Serializable {

	private static final long serialVersionUID = 1966859978593854876L;

	private Integer orgId;
	
	private Integer baseId;
	
	private String operation;

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public Integer getBaseId() {
		return baseId;
	}

	public void setBaseId(Integer baseId) {
		this.baseId = baseId;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

}
