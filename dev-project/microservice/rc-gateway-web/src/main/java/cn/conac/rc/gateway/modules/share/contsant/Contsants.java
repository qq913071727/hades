package cn.conac.rc.gateway.modules.share.contsant;

public class Contsants {
	
	/**
	 * 地区lvl=0
	 */
	public static String AREA_LVL_ZERO = "0";
	
	/**
	 * 地区lvl=1
	 */
	public static String AREA_LVL_ONE = "1";
	
	/**
	 * 地区lvl=2
	 */
	public static String AREA_LVL_TWO = "2";
	
	/**
	 * 地区lvl=3
	 */
	public static String AREA_LVL_THREE = "3";
	
	/**
	 * 有公开办法
	 */
	public static Integer AREA_PUBLIC = 1;
	
	
	/**
	 * 地区被共享标示
	 */
	public static Integer AREA_SHARED_FLAG = 1;
}
