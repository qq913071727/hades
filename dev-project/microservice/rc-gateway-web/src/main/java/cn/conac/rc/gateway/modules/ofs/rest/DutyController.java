package cn.conac.rc.gateway.modules.ofs.rest;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.BeanMapper;
import cn.conac.rc.framework.utils.RestClientException;
import cn.conac.rc.framework.utils.ResultPojo;
import cn.conac.rc.gateway.modules.ofs.entity.DutyEntity;
import cn.conac.rc.gateway.modules.ofs.entity.OrgStatusEntity;
import cn.conac.rc.gateway.modules.ofs.service.DutyService;
import cn.conac.rc.gateway.modules.ofs.service.OrgStatusService;
import cn.conac.rc.gateway.security.UserUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="ofs/")
@Api(tags="部门的职能职责", description="三定信息")
public class DutyController {

    @Autowired
    DutyService dutyService;
    
    @Autowired
    OrgStatusService  orgStatusService;
    
	@ApiOperation(value = "根据baseId取得职责列表信息", httpMethod = "GET", response = JSONObject.class, notes = "根据baseId取得职责列表信息")
    @RequestMapping(value = "bases/{baseId}/dutys", method = RequestMethod.GET)
    public JSONObject findListById(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "部门基本表id", required = true) @PathVariable("baseId") String baseId)
            throws RestClientException, Exception {
        return dutyService.findListByBaseId(baseId);
    }
	
	@ApiOperation(value = "职能职责列表", httpMethod = "POST", response = JSONObject.class, notes = "根据检索条件获取职能职责列表")
	@RequestMapping(value = "dutys/list", method = RequestMethod.POST)
	public JSONObject empDetail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody JSONObject duty) {
		return dutyService.dutyList(duty);
	}
	
	@ApiOperation(value = "部门职能职责信息保存", httpMethod = "POST", response = JSONObject.class, notes = "部门职能职责信息保存")
    @RequestMapping(value = "dutys/c", method = RequestMethod.POST)
    public Object cudDutySave(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "部门职能职责信息和状态信息的entity", required = true)  @RequestBody Map<String, Map<String, Object>> entityMap  )
            throws RestClientException, Exception {
    	
    	 ResultPojo resultInfo = new ResultPojo();
    	 Map<String,String> idMap = new HashMap<String, String>();
    	 
    	 //******************第一步从页面侧获取部门职能职责信息和部门状态实体对象***********
         // 获得部门职能职责信息
    	 DutyEntity dutyEntityInfo = new DutyEntity();
    	 BeanMapper.copy(entityMap.get("dutyEntity"), dutyEntityInfo);
    	
    	 // 确保部门职能职责信息中的ID不为null
    	 String baseId = dutyEntityInfo.getBaseId();
    	 if(null == baseId){
    		 //部门职能职责信息中的ID为null场合返回错误信息
	       	  resultInfo.setCode(ResultPojo.CODE_FAILURE);
	          resultInfo.setMsg("[ the baseId of dutyEntityInfo is null] in function of dutySave " + ResultPojo.MSG_FAILURE);
	       	  resultInfo.setResult(null);
	       	  return resultInfo;
    	 }
   
         // 获得部门状态信息
         OrgStatusEntity statusEntityInfo=  new OrgStatusEntity();
         BeanMapper.copy(entityMap.get("statusEntity"), statusEntityInfo);
        
         // 取得完整度掩码
         String stepMask = statusEntityInfo.getContentMask();
         // 判断是否是暂存[0]还是正常保存[1]，默认为正常保存 
         String submitType = "1";
         if(null  != stepMask ) {
        	 // 完整度掩码的格式为【111111】，当暂存场合 完整度掩码的第一个字符为[0],正常保存时为【1】
        	 submitType = stepMask.substring(2, 3);
         }
         
         //******************第二步保存部门职能职责信息表**************************************
         // 执行保存部门职能职责信息表
    	 JSONObject dutyInfoJson =  dutyService.saveOrUpdate(submitType,dutyEntityInfo);
    	 
    	 // 判断保存是否成功失败
    	  if (ResultPojo.CODE_SUCCESS.equals(dutyInfoJson.getString("code"))) {
    		  // 保存成功场合
    		  idMap.put("id", dutyInfoJson.getJSONObject("result").getString("id"));
    		  idMap.put("baseId", dutyInfoJson.getJSONObject("result").getString("baseId"));
    		  resultInfo.setResult(idMap);
          } else {
        	 // 保存失败场合
        	  resultInfo.setCode(dutyInfoJson.getString("code"));
        	  resultInfo.setMsg("[dutyService.saveOrUpdate] in function of dutySave " 
        			  					+ ResultPojo.MSG_FAILURE + "【"  + dutyInfoJson.getString("msg") + "】");
        	  resultInfo.setResult(null);
         	  return resultInfo;
          }
    	  
    	 //*******************第三步保存部门的状态表******************************************
    	 // 设置状态表ID
    	 statusEntityInfo.setId(idMap.get("baseId"));
    	  // 设置登录用户ID
		 statusEntityInfo.setUpdateUserId(UserUtils.getCurrentUser().getId());
		  // 执行保存部门的状态表
		  JSONObject statusInfoJson = orgStatusService.saveOrUpdate(statusEntityInfo);
		 
		  // 判断保存是否成功失败
		  if (ResultPojo.CODE_SUCCESS.equals(statusInfoJson.getString("code")) ) {
			  // 保存成功场合
			  resultInfo.setCode(ResultPojo.CODE_SUCCESS);
			  resultInfo.setMsg(ResultPojo.MSG_SUCCESS);
		  } else {
			 // 保存失败场合
			  resultInfo.setCode(statusInfoJson.getString("code"));
			  resultInfo.setMsg("[orgStatusService.saveOrUpdate] in function of dutySave " 
					  				+ ResultPojo.MSG_FAILURE + "【"  + statusInfoJson.getString("msg") + "】");
			  resultInfo.setResult(null);
		      // 程序事务控制
		  	  // 删除保存的部门基本信息 TODO
			  return  resultInfo;
		  }
    	 
         // 成功保存部门信息表和部门状态表返回
         return  resultInfo;
    }
    
    @ApiOperation(value = "部门职能职责信息更新", httpMethod = "POST", response = JSONObject.class, notes = "部门职能职责信息更新")
    @RequestMapping(value = "dutys/{baseId}/u", method = RequestMethod.POST)
    public Object cudDutyUpdate(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam("部门基本信息Id") @PathVariable("baseId") String baseId,
            @ApiParam(value = "部门职能职责信息和状态信息的entity", required = true)  @RequestBody Map<String, Map<String, Object>> entityMap  )
            throws RestClientException, Exception {
    	
    	 ResultPojo resultInfo = new ResultPojo();
    	 if(null == baseId) {
    		  // 参数baseId为null场合
	       	  resultInfo.setCode(ResultPojo.CODE_FAILURE);
	          resultInfo.setMsg("[ baseId of parameter is null ] in function of dutyUpdate " + ResultPojo.MSG_FAILURE);
	       	  resultInfo.setResult(null);
	       	  return resultInfo;
    	 }
    	 
    	 Map<String,String> idMap = new HashMap<String, String>();
    	 
    	 //******************第一步从页面侧获取部门职能职责信息和部门状态实体对象***********
         // 获得部门职能职责信息
    	 DutyEntity dutyEntityInfo = new DutyEntity();
    	 BeanMapper.copy(entityMap.get("dutyEntity"), dutyEntityInfo);
    	
    	 // 确保部门职能职责信息中的ID不为null
    	 if(null == dutyEntityInfo.getId()) {
    		 // 参数baseId为null场合
	       	  resultInfo.setCode(ResultPojo.CODE_FAILURE);
	          resultInfo.setMsg("[ id of parameter is null ] in function of dutyUpdate " + ResultPojo.MSG_FAILURE);
	       	  resultInfo.setResult(null);
	       	  return resultInfo;
    	 }
    	 
    	// 确保部门职能职责信息中的baseID不为null
    	 dutyEntityInfo.setBaseId(baseId);
    	 
         // 获得部门状态信息
         OrgStatusEntity statusEntityInfo=  new OrgStatusEntity();
         BeanMapper.copy(entityMap.get("statusEntity"), statusEntityInfo);
        
         // 取得完整度掩码
         String stepMask = statusEntityInfo.getContentMask();
         // 判断是否是暂存[0]还是正常保存[1]，默认为正常保存 
         String submitType = "1";
         if(null  != stepMask ) {
        	 // 完整度掩码的格式为【111111】，当暂存场合 完整度掩码的第一个字符为[0],正常保存时为【1】
        	 submitType = stepMask.substring(2, 3);
         }
         
         //******************第二步保存部门职能职责信息表**************************************
         // 执行保存部门职能职责信息表
    	 JSONObject dutyInfoJson =  dutyService.saveOrUpdate(submitType,dutyEntityInfo);
    	 
    	 // 判断保存是否成功失败
    	  if (ResultPojo.CODE_SUCCESS.equals(dutyInfoJson.getString("code"))) {
    		  // 保存成功场合
    		  idMap.put("id", dutyInfoJson.getJSONObject("result").getString("id"));
    		  idMap.put("baseId", dutyInfoJson.getJSONObject("result").getString("baseId"));
    		  resultInfo.setResult(idMap);
          } else {
        	 // 保存失败场合
        	  resultInfo.setCode(dutyInfoJson.getString("code"));
        	  resultInfo.setMsg("[dutyService.saveOrUpdate] in function of dutyUpdate " + 
        			  						ResultPojo.MSG_FAILURE + "【"  + dutyInfoJson.getString("msg") + "】");
        	  resultInfo.setResult(null);
         	  return resultInfo;
          }
    	 
    	 //*******************第三步保存部门的状态表******************************************
    	 // 设置状态表ID
    	 statusEntityInfo.setId(idMap.get("baseId"));
    	  // 设置登录用户ID
		 statusEntityInfo.setUpdateUserId(UserUtils.getCurrentUser().getId());
		  // 执行保存部门的状态表
		  JSONObject statusInfoJson = orgStatusService.saveOrUpdate(statusEntityInfo);
		 
		  // 判断保存是否成功失败
		  if (ResultPojo.CODE_SUCCESS.equals(statusInfoJson.getString("code")) ) {
			  // 保存成功场合
			  resultInfo.setCode(ResultPojo.CODE_SUCCESS);
			  resultInfo.setMsg(ResultPojo.MSG_SUCCESS);
		  } else {
			 // 保存失败场合
			  resultInfo.setCode(statusInfoJson.getString("code"));
			  resultInfo.setMsg("[orgStatusService.saveOrUpdate] in function of dutyUpdate " 
					  						+ ResultPojo.MSG_FAILURE + "【"   + statusInfoJson.getString("msg") + "】");
			  resultInfo.setResult(null);
		      // 程序事务控制
		  	  // 删除保存的部门基本信息 TODO
			  return  resultInfo;
		  }
    	 
         // 成功保存部门信息表和部门状态表返回
         return  resultInfo;
    }
    
	@ApiOperation(value = "根据职能职责id删除职责信息", httpMethod = "POST", response = JSONObject.class, notes = "根据职能职责id删除职责信息")
    @RequestMapping(value = "dutys/{id}/d", method = RequestMethod.POST)
    public JSONObject cudDutyDelete(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam(value = "删除对象id", required = true) @PathVariable(value = "id") String id)
            throws RestClientException, Exception {
		
        return dutyService.delete(id);
	}

	@ApiOperation(value = "获取内设机构id对应的职能职责信息", httpMethod = "GET", response = JSONObject.class, notes = "根据内设机构departId取得关联职责信息")
    @RequestMapping(value = "departments/{departId}/dutys", method = RequestMethod.GET)
    public JSONObject findByDepartId(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "内设机构ID", required = true) @PathVariable("departId") String departId)
            throws RestClientException, Exception {
        return dutyService.findByDepartId(departId);
    }
	
	@ApiOperation(value = "根据职能职责id取得职责信息", httpMethod = "GET", response = JSONObject.class, notes = "根据职能职责id取得职责信息")
    @RequestMapping(value = "dutys/{id}/r", method = RequestMethod.GET)
    public JSONObject dutyFindById(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam(value = "对象id", required = true) @PathVariable(value = "id") String id)
            throws RestClientException, Exception {
		
        return dutyService.findById(id);
	}
}
