package cn.conac.rc.gateway.common.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

@FeignClient("RC-SERVICE-COMMON")
public interface AreaGdpService {
	@RequestMapping(method = RequestMethod.GET, value = "/areaGdp/{areaId}")
	public JSONObject detail(@PathVariable("areaId") String areaId);
}
