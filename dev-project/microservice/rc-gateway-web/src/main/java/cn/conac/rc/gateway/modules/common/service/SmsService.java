package cn.conac.rc.gateway.modules.common.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.common.vo.SmsBean;

@FeignClient("RC-SERVICE-MSG")
public interface SmsService
{

    @RequestMapping(method = RequestMethod.POST, value = "/sms/send")
    JSONObject sendSms(@RequestBody SmsBean SmsBean);

}