package cn.conac.rc.gateway.modules.monitor.rest;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;

import cn.conac.rc.framework.utils.DateUtils;
import cn.conac.rc.framework.utils.ResultPojo;
import cn.conac.rc.gateway.modules.monitor.service.EsService;
import cn.conac.rc.gateway.modules.monitor.service.MonitorLogic;
import cn.conac.rc.gateway.modules.monitor.service.MonitorService;
import cn.conac.rc.gateway.modules.monitor.vo.LocalStatus;
import cn.conac.rc.gateway.modules.monitor.vo.StatisticsVO;
import cn.conac.rc.gateway.modules.monitor.vo.VLocalMonitorDay;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 监测Controller
 *
 * @author haocm
 * @version 1.0
 */
@RestController
@Api(tags = "统计接口", description = "审批监控")
public class MonitorController
{
    @Autowired
    MonitorService monitorService;
    @Autowired
    EsService esSearch;

    @ApiOperation(value = "办理查询-事项详情-事件追踪", notes = "办理查询-事项详情-事件追踪.", response = StatisticsVO.class)
    @RequestMapping(value = "/es/monitor/status", method = RequestMethod.POST)
    public String getStatusList(
            HttpServletRequest request, HttpServletResponse response,
            @RequestBody LocalStatus localStatus) throws Exception
    {
        //首先获取当前登录用户的地区code
        System.out.println(JSON.toJSONString(esSearch.getStatus(localStatus)));
        return JSON.toJSONString(esSearch.getStatus(localStatus));
//        return new ResponseEntity<ResultPojo>(esSearch.getStatus(localStatus).toJavaObject(ResultPojo.class),
//                HttpStatus.OK);
    }

    @SuppressWarnings("rawtypes")
    @ApiOperation(value = "累计申请数", notes = "查询时间段内累计的事项申请数.", response = StatisticsVO.class)
    @RequestMapping(value = "/monitor/statistics/counts", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> getStatisticsCounts(
            HttpServletRequest request, HttpServletResponse response,
            @RequestBody StatisticsVO statisticsVO) throws Exception
    {
        //首先获取当前登录用户的地区code
        if (StringUtils.isBlank(statisticsVO.getDateFlag())
                || StringUtils.isBlank(statisticsVO.getAreaCode()))
        {
            return new ResponseEntity<ResultPojo>(new ResultPojo(
                    ResultPojo.CODE_FAILURE, "参数错误！"), HttpStatus.OK);
        }
        String department = statisticsVO.getDepartment();
        String dateFlag = statisticsVO.getDateFlag();
        String areaCode = statisticsVO.getAreaCode();
        String year = statisticsVO.getStartDate();
        String group = statisticsVO.getGroup();
        String dfsxbm = statisticsVO.getDfsxbm();
        String startDate = "";
        String monthOrQuarter = statisticsVO.getEndDate();
        String endDate = "";
        String echarStartDate;
        String echarEndDate;
        List echarDatalist = null;
        List echarBlbmlist = null;
        List dataList = null;
        StatisticsVO query = new StatisticsVO();
        query.setDepartment(department);
        query.setAreaCode(areaCode);
        query.setGroup(group);
        query.setDfsxbm(dfsxbm);
        //0-三定调用图表展示
        if ("0".equals(dateFlag)) {
            // 本月
            Calendar current = Calendar.getInstance();
            //拼接日期
            Date queryDate = new Date();
            current.setTime(queryDate);
            startDate = DateUtils.formatDate(DateUtils.getCurrentMonthStartTime(current), DateUtils.FORMAT_YYYYMMDD);
            current.setTime(queryDate);
            endDate = DateUtils.formatDate(DateUtils.getCurrentMonthEndTime(current), DateUtils.FORMAT_YYYYMMDD);
            // echar 不包含本月，开始日期是当前日期月份-7 结束如期是当前月份-1
            Calendar c = Calendar.getInstance();
            c.setTime(queryDate);
            c.add(Calendar.MONTH, -12);
            echarStartDate = DateUtils.formatDate(DateUtils.getCurrentMonthStartTime(c), DateUtils.FORMAT_YYYYMMDD);
            c.setTime(queryDate);
            c.add(Calendar.MONTH, -1);
            echarEndDate = DateUtils.formatDate(DateUtils.getCurrentMonthEndTime(c), DateUtils.FORMAT_YYYYMMDD);
            query.setStartDate(echarStartDate);
            query.setEndDate(echarEndDate);
            echarDatalist =
                    (List) JSON.toJavaObject(monitorService.getStatisticsMonth(query), ResultPojo.class).getResult();
        }else if ("1".equals(dateFlag)) {
            // 本月
            Calendar current = Calendar.getInstance();
            //拼接日期
            Date queryDate = new Date();
            if (StringUtils.isNotBlank(year)) {
                queryDate = DateUtils.parseDate(year + '-' + monthOrQuarter, "yyyy-MM");
            }
            current.setTime(queryDate);
            startDate = DateUtils.formatDate(DateUtils.getCurrentMonthStartTime(current), DateUtils.FORMAT_YYYYMMDD);
            current.setTime(queryDate);
            endDate = DateUtils.formatDate(DateUtils.getCurrentMonthEndTime(current), DateUtils.FORMAT_YYYYMMDD);
            //查询当前的数据
            query.setStartDate(startDate);
            query.setEndDate(endDate);
            dataList = (List) JSON.toJavaObject(monitorService.getStatisticsMonth(query), ResultPojo.class).getResult();
            // echar 不包含本月，开始日期是当前日期月份-7 结束如期是当前月份-1
            Calendar c = Calendar.getInstance();
            c.setTime(queryDate);
            c.add(Calendar.MONTH, -6);
            echarStartDate = DateUtils.formatDate(DateUtils.getCurrentMonthStartTime(c), DateUtils.FORMAT_YYYYMMDD);
            c.setTime(queryDate);
            c.add(Calendar.MONTH, -1);
            echarEndDate = DateUtils.formatDate(DateUtils.getCurrentMonthEndTime(c), DateUtils.FORMAT_YYYYMMDD);
            query.setStartDate(echarStartDate);
            query.setEndDate(echarEndDate);

            echarDatalist =
                    (List) JSON.toJavaObject(monitorService.getStatisticsMonth(query), ResultPojo.class).getResult();
        } else if ("2".equals(dateFlag)) {
            // 本季
            Calendar current = Calendar.getInstance();
            //拼接日期
            // 本季
            if (StringUtils.isNotBlank(year)) {
                startDate = DateUtils.formatDate(DateUtils.getStartDateByQuarter(year, monthOrQuarter),
                        DateUtils.FORMAT_YYYYMMDD);
                endDate = DateUtils.formatDate(DateUtils.getEndDateByQuarter(year, monthOrQuarter),
                        DateUtils.FORMAT_YYYYMMDD);
            } else {
                // 本季
                Date currentQuarterStartTime = DateUtils
                        .getCurrentQuarterStartTime(current);
                startDate = DateUtils.formatDate(currentQuarterStartTime,
                        DateUtils.FORMAT_YYYYMMDD);
                endDate = DateUtils.formatDate(
                        DateUtils.getCurrentQuarterEndTime(current),
                        DateUtils.FORMAT_YYYYMMDD);
            }
            //查询当前的数据
            query.setStartDate(startDate);
            query.setEndDate(endDate);
            dataList =
                    (List) JSON.toJavaObject(monitorService.getStatisticsQuarter(query), ResultPojo.class).getResult();

            // echar 不包含本季度，往前6个季，每季度3个月，共18个月
            // 本季
            Calendar c = Calendar.getInstance();
            if (StringUtils.isNotBlank(year)) {
                c.setTime(DateUtils.getStartDateByQuarter(year, monthOrQuarter));
            }else{
                //如果为空，设置当前时间所在季度第一天
                c.setTime(DateUtils.getCurrentQuarterStartTime(c));
            }

            c.add(Calendar.MONTH, -18);
            echarStartDate = DateUtils.formatDate(
                    DateUtils.getCurrentMonthStartTime(c),
                    DateUtils.FORMAT_YYYYMMDD);
            c = Calendar.getInstance();
            if (StringUtils.isNotBlank(year)) {
                c.setTime(DateUtils.getStartDateByQuarter(year, monthOrQuarter));
            }else{
                //如果为空，设置当前时间所在季度第一天
                c.setTime(DateUtils.getCurrentQuarterStartTime(c));
            }
            c.add(Calendar.MONTH, -1);
            echarEndDate = DateUtils.formatDate(
                    DateUtils.getCurrentMonthEndTime(c),
                    DateUtils.FORMAT_YYYYMMDD);
            query.setStartDate(echarStartDate);
            query.setEndDate(echarEndDate);
            System.out.println("startDate:"+startDate);
            System.out.println("endDate:"+endDate);
            System.out.println("echarStartDate:"+echarStartDate);
            System.out.println("echarEndDate:"+echarEndDate);
            echarDatalist =
                    (List) JSON.toJavaObject(monitorService.getStatisticsQuarter(query), ResultPojo.class).getResult();
        } else {
            // 本年
            Calendar current = Calendar.getInstance();
            //拼接日期
            if (StringUtils.isNotBlank(year)) {
                current.setTime(DateUtils.parseDate(year + '-' + monthOrQuarter, "yyyy-MM"));
            }
            startDate = DateUtils.formatDate(DateUtils.getCurrentYearStartTime(current), DateUtils.FORMAT_YYYYMMDD);
            endDate = DateUtils.formatDate(DateUtils.getCurrentYearEndTime(current), DateUtils.FORMAT_YYYYMMDD);
            //查询当前的数据
            query.setStartDate(startDate);
            query.setEndDate(endDate);
            dataList = (List) JSON.toJavaObject(monitorService.getStatisticsYear(query), ResultPojo.class).getResult();
            Calendar c = Calendar.getInstance();
            if (StringUtils.isNotBlank(year)) {
                c.setTime(DateUtils.parseDate(year + '-' + monthOrQuarter, "yyyy-MM"));
            }
            c.add(Calendar.YEAR, -6);
            echarStartDate = DateUtils.formatDate(
                    DateUtils.getCurrentYearStartTime(c),
                    DateUtils.FORMAT_YYYYMMDD);
            c = Calendar.getInstance();
            if (StringUtils.isNotBlank(year)) {
                c.setTime(DateUtils.parseDate(year + '-' + monthOrQuarter, "yyyy-MM"));
            }
            c.add(Calendar.YEAR, -1);
            echarEndDate = DateUtils.formatDate(
                    DateUtils.getCurrentYearEndTime(c),
                    DateUtils.FORMAT_YYYYMMDD);
            // echar 不包含本年，往前6年
            query.setStartDate(echarStartDate);
            query.setEndDate(echarEndDate);
            echarDatalist =
                    (List) JSON.toJavaObject(monitorService.getStatisticsYear(query), ResultPojo.class).getResult();
        }
        System.out.println(echarStartDate);
        System.out.println(echarEndDate);
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        //当选择具体部门时按事项分组
        echarBlbmlist = (List) JSON.toJavaObject(monitorService.getStatisticsBlbm(query), ResultPojo.class).getResult();

        Map<String, VLocalMonitorDay> map = new HashMap<String, VLocalMonitorDay>();
        Map<String, VLocalMonitorDay> bmmap = new HashMap<String, VLocalMonitorDay>();

        List<Map.Entry<String,VLocalMonitorDay>> bmmapSort = MonitorLogic.createCountChart(dateFlag, department,echarDatalist, echarBlbmlist,
                map, bmmap);
        statisticsVO.setBlsEchar(map);
        statisticsVO.setBmsEchar(bmmapSort);
        statisticsVO.setDataList(dataList);

        StatisticsVO matterQuary = new StatisticsVO();
        matterQuary.setDepartment(statisticsVO.getDepartment());
        matterQuary.setDfsxbm(statisticsVO.getAreaCode());
        ArrayList blbmList = (ArrayList) monitorService.getBlbmList(areaCode)
                .toJavaObject(ResultPojo.class).getResult();
        statisticsVO.setBlbmList(blbmList);
        StatisticsVO qzQuery = new StatisticsVO();
        qzQuery.setLocal("1");
        qzQuery.setAreaCode(areaCode);
        //缺省权重
        List list1 = (List) monitorService.getWeight(qzQuery).toJavaObject(ResultPojo.class).getResult();
        qzQuery.setLocal("0");
        qzQuery.setAreaCode("");
        qzQuery.setAreaId("");

        List list2 = (List) monitorService.getWeight(qzQuery).toJavaObject(ResultPojo.class).getResult();
        statisticsVO.setLocalWeight(list1);
        statisticsVO.setSysWeight(list2);
        return new ResponseEntity<ResultPojo>(new ResultPojo(
                ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, statisticsVO),
                HttpStatus.OK);
    }

    @ApiOperation(value = "数据分析", notes = "数据分析.", response = StatisticsVO.class)
    @RequestMapping(value = "/monitor/statistics/sjfx", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> getStatisticsSjfx(
            HttpServletRequest request, HttpServletResponse response,
            @RequestBody StatisticsVO statisticsVO) throws Exception
    {
        //首先获取当前登录用户的地区code
        if (StringUtils.isBlank(statisticsVO.getAreaCode())) {
            return new ResponseEntity<ResultPojo>(new ResultPojo(
                    ResultPojo.CODE_FAILURE, "参数错误！"), HttpStatus.OK);
        }
        String dateFlag = statisticsVO.getDateFlag();
        String areaCode = statisticsVO.getAreaCode();
        String bjzt = statisticsVO.getBjzt();
        String year = statisticsVO.getStartDate();
        String group = statisticsVO.getGroup();
        if (StringUtils.isBlank(group)) {
            //默认按部门查询
            group = "1";
        }
        String scope = statisticsVO.getScope();
        String startDate = "";
        String monthOrQuarter = statisticsVO.getEndDate();
        String endDate = "";
        List echarDatalist = null;
        if ("1".equals(dateFlag)) {
            // 本月
            Calendar current = Calendar.getInstance();
            //拼接日期
            Date queryDate = new Date();
            if (StringUtils.isNotBlank(year)) {
                queryDate = DateUtils.parseDate(year + '-' + monthOrQuarter, "yyyy-MM");
            }
            current.setTime(queryDate);
            startDate = DateUtils.formatDate(
                    DateUtils.getCurrentMonthStartTime(current),
                    DateUtils.FORMAT_YYYYMMDD);
            current.setTime(queryDate);
            endDate = DateUtils.formatDate(
                    DateUtils.getCurrentMonthEndTime(current),
                    DateUtils.FORMAT_YYYYMMDD);
        } else if ("2".equals(dateFlag)) {
            // 本季
            startDate = DateUtils.formatDate(DateUtils.getStartDateByQuarter(year, monthOrQuarter),
                    DateUtils.FORMAT_YYYYMMDD);
            endDate = DateUtils.formatDate(DateUtils.getEndDateByQuarter(year, monthOrQuarter),
                    DateUtils.FORMAT_YYYYMMDD);
        } else {
            // 本年
            Calendar current = Calendar.getInstance();
            //拼接日期
            Date queryDate = DateUtils.parseDate(year + '-' + monthOrQuarter, "yyyy-MM");
            current.setTime(queryDate);
            startDate = DateUtils.formatDate(
                    DateUtils.getCurrentYearStartTime(current),
                    DateUtils.FORMAT_YYYYMMDD);
            endDate = DateUtils.formatDate(
                    DateUtils.getCurrentYearEndTime(current),
                    DateUtils.FORMAT_YYYYMMDD);
        }
        StatisticsVO query = new StatisticsVO();
        System.out.println(startDate);
        System.out.println(endDate);
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setDepartment(statisticsVO.getDepartment());
        query.setAreaCode(areaCode);
        query.setGroup(group);
        echarDatalist =
                (List) JSON.toJavaObject(monitorService.getStatisticsBlbmSx(query), ResultPojo.class).getResult();

        ArrayList blbmList = (ArrayList) monitorService.getBlbmList(areaCode)
                .toJavaObject(ResultPojo.class).getResult();
        statisticsVO.setBlbmList(blbmList);
        statisticsVO.setBmsEchar(echarDatalist);
        StatisticsVO qzQuery = new StatisticsVO();
        qzQuery.setLocal("1");
        //缺省权重
        List list1 = (List) monitorService.getWeight(statisticsVO).toJavaObject(ResultPojo.class).getResult();
        qzQuery.setLocal("0");
        qzQuery.setAreaCode("");
        qzQuery.setAreaId("");

        List list2 = (List) monitorService.getWeight(statisticsVO).toJavaObject(ResultPojo.class).getResult();
        statisticsVO.setLocalWeight(list1);
        statisticsVO.setSysWeight(list2);

        return new ResponseEntity<ResultPojo>(new ResultPojo(
                ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, statisticsVO),
                HttpStatus.OK);
    }

    @ApiOperation(value = "权重查询", notes = "权重查询.", response = StatisticsVO.class)
    @RequestMapping(value = "/monitor/weight", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> getWeight(
            HttpServletRequest request, HttpServletResponse response,
            @RequestBody StatisticsVO statisticsVO) throws Exception
    {
        //首先获取当前登录用户的地区code
        //本级权重
        statisticsVO.setLocal("1");
        List list1 = (List) monitorService.getWeight(statisticsVO).toJavaObject(ResultPojo.class).getResult();
        //缺省权重
        statisticsVO.setLocal("0");
        statisticsVO.setAreaCode("");
        statisticsVO.setAreaId("");
        List list2 = (List) monitorService.getWeight(statisticsVO).toJavaObject(ResultPojo.class).getResult();
        statisticsVO.setLocalWeight(list1);
        statisticsVO.setSysWeight(list2);
        return new ResponseEntity<ResultPojo>(new ResultPojo(
                ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, statisticsVO),
                HttpStatus.OK);
    }

    @ApiOperation(value = "保存权重信息", notes = "保存权重信息.", response = StatisticsVO.class)
    @RequestMapping(value = "/monitor/weight/save", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> save(
            HttpServletRequest request, HttpServletResponse response,
            @RequestBody StatisticsVO statisticsVO) throws Exception
    {
        //首先获取当前登录用户的地区code
        //本级权重
        // monitorService.save(statisticsVO).toJavaObject(ResultPojo.class);
        return new ResponseEntity<ResultPojo>(monitorService.save(statisticsVO).toJavaObject(ResultPojo.class),
                HttpStatus.OK);
    }

    @ApiOperation(value = "办理查询-事项详情", notes = "办理查询-事项详情.", response = StatisticsVO.class)
    @RequestMapping(value = "/monitor/status/detail", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> getStatusDetail(
            HttpServletRequest request, HttpServletResponse response,
            @RequestBody StatisticsVO statisticsVO) throws Exception
    {
        //首先获取当前登录用户的地区code

        // monitorService.save(statisticsVO).toJavaObject(ResultPojo.class);
        LocalStatus tmp = null;
        ArrayList dbList =
                (ArrayList) monitorService.getStatisticsCounts(statisticsVO).toJavaObject(ResultPojo.class).getResult();
        if (dbList.size() > 0) {
            tmp = JSON.toJavaObject(
                    JSON.parseObject(JSON.toJSONString(dbList.get(0))),
                    LocalStatus.class);
            String stage = "";
            String status = "";
            if (StringUtils.isNotBlank(tmp.getBjzt())) {
                //办结阶段
                stage = "办结";
                //01：准予许可
                //02：不予许可
                //03：转报办结
                //04：终止
                if (LocalStatus.bjzt01.equals(tmp.getBjzt())) {
                    status = "准予许可";
                } else if (LocalStatus.bjzt02.equals(tmp.getBjzt())) {
                    status = "不予许可";
                } else if (LocalStatus.bjzt03.equals(tmp.getBjzt())) {
                    status = "转报办结";
                } else if (LocalStatus.bjzt04.equals(tmp.getBjzt())) {
                    status = "终止办结";
                }
            } else if (StringUtils.isNotBlank(tmp.getSlzt())) {
                //受理阶段
                //01：受理
                //02：不受理
                stage = "受理";
                if (LocalStatus.slzt01.equals(tmp.getSlzt())) {
                    status = "受理";
                } else if (LocalStatus.slzt02.equals(tmp.getSlzt())) {
                    status = "不予受理";
                }
            } else {
                //申请阶段
                stage = "申请";
                status = "申请";
            }
            tmp.setStage(stage);
            tmp.setStatus(status);
            //申请
            //补正
            //受理
            //特殊程序
            //办结
        }

        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, tmp),
                HttpStatus.OK);
    }

    @ApiOperation(value = "办理查询-事项详情-事件追踪", notes = "办理查询-事项详情-事件追踪.", response = StatisticsVO.class)
    @RequestMapping(value = "/monitor/sxzz/list", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> getSxzzList(
            HttpServletRequest request, HttpServletResponse response,
            @RequestBody StatisticsVO statisticsVO) throws Exception
    {
        //首先获取当前登录用户的地区code
        return new ResponseEntity<ResultPojo>(monitorService.getSxzz(statisticsVO).toJavaObject(ResultPojo.class),
                HttpStatus.OK);
    }

    @ApiOperation(value = "办理查询-办理部门列表", notes = "办理查询-办理部门列表.", response = StatisticsVO.class)
    @RequestMapping(value = "/monitor/blbm/list", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> getBlibmList(
            HttpServletRequest request, HttpServletResponse response,
            @RequestBody StatisticsVO statisticsVO) throws Exception
    {
        //首先获取当前登录用户的地区code
        return new ResponseEntity<ResultPojo>(
                monitorService.getBlbmList(statisticsVO.getAreaCode()).toJavaObject(ResultPojo.class), HttpStatus.OK);
    }

    @ApiOperation(value = "当前事项数", notes = "当前事项数", response = StatisticsVO.class)
    @RequestMapping(value = "/monitor/sx/list", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> getBlbmSxList(
            HttpServletRequest request, HttpServletResponse response,
            @RequestBody StatisticsVO statisticsVO) throws Exception
    {
        //首先获取当前登录用户的地区code
        return new ResponseEntity<ResultPojo>(
                monitorService.getBlbmsxList(statisticsVO.getAreaCode()).toJavaObject(ResultPojo.class), HttpStatus.OK);
    }

    @ApiOperation(value = "获取状态列表", notes = "获取状态列表.", response = StatisticsVO.class)
    @RequestMapping(value = "/monitor/status", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> getStatus(
            HttpServletRequest request, HttpServletResponse response,
            @RequestBody StatisticsVO statisticsVO) throws Exception
    {
        //首先获取当前登录用户的地区code
        if (StringUtils.isBlank(statisticsVO.getAreaCode())) {
            return new ResponseEntity<ResultPojo>(new ResultPojo(
                    ResultPojo.CODE_FAILURE, "参数错误！"), HttpStatus.OK);
        }
        String dateFlag = statisticsVO.getDateFlag();
        //String bjzt = statisticsVO.getBjzt();
        String year = statisticsVO.getStartDate();
        String startDate = "";
        String monthOrQuarter = statisticsVO.getEndDate();
        String endDate = "";
        if ("1".equals(dateFlag)) {
            // 本月
            Calendar current = Calendar.getInstance();
            //拼接日期
            Date queryDate = new Date();
            if (StringUtils.isNotBlank(year)) {
                queryDate = DateUtils.parseDate(year + '-' + monthOrQuarter, "yyyy-MM");
            }
            current.setTime(queryDate);
            startDate = DateUtils.formatDate(
                    DateUtils.getCurrentMonthStartTime(current),
                    DateUtils.FORMAT_YYYYMMDD);
            current.setTime(queryDate);
            endDate = DateUtils.formatDate(
                    DateUtils.getCurrentMonthEndTime(current),
                    DateUtils.FORMAT_YYYYMMDD);
        } else if ("2".equals(dateFlag)) {
            // 本季
            startDate = DateUtils.formatDate(DateUtils.getStartDateByQuarter(year, monthOrQuarter),
                    DateUtils.FORMAT_YYYYMMDD);
            endDate = DateUtils.formatDate(DateUtils.getEndDateByQuarter(year, monthOrQuarter),
                    DateUtils.FORMAT_YYYYMMDD);
        } else {
            // 本年
            Calendar current = Calendar.getInstance();
            //拼接日期
            Date queryDate = DateUtils.parseDate(year + '-' + monthOrQuarter, "yyyy-MM");
            current.setTime(queryDate);
            startDate = DateUtils.formatDate(
                    DateUtils.getCurrentYearStartTime(current),
                    DateUtils.FORMAT_YYYYMMDD);
            endDate = DateUtils.formatDate(
                    DateUtils.getCurrentYearEndTime(current),
                    DateUtils.FORMAT_YYYYMMDD);
        }
        System.out.println("查询开始日期:" + startDate);
        System.out.println("查询截止日期:" + endDate);
        statisticsVO.setStartDate(startDate);
        statisticsVO.setEndDate(endDate);
        // monitorService.save(statisticsVO).toJavaObject(ResultPojo.class);
        ArrayList dbList =
                (ArrayList) monitorService.getStatisticsCounts(statisticsVO).toJavaObject(ResultPojo.class).getResult();
        ArrayList<LocalStatus> rstList = new ArrayList<LocalStatus>();
        for (Object object : dbList) {
            LocalStatus tmp = JSON.toJavaObject(
                    JSON.parseObject(JSON.toJSONString(object)),
                    LocalStatus.class);
            String stage = "";
            String status = "";
            if (StringUtils.isNotBlank(tmp.getBjzt())) {
                //办结阶段
                stage = "办结";
                //01：准予许可
                //02：不予许可
                //03：转报办结
                //04：终止
                if (LocalStatus.bjzt01.equals(tmp.getBjzt())) {
                    status = "准予许可";
                } else if (LocalStatus.bjzt02.equals(tmp.getBjzt())) {
                    status = "不予许可";
                } else if (LocalStatus.bjzt03.equals(tmp.getBjzt())) {
                    status = "转报办结";
                } else if (LocalStatus.bjzt04.equals(tmp.getBjzt())) {
                    status = "终止";
                }
            } else if (StringUtils.isNotBlank(tmp.getSlzt())) {
                //受理阶段
                //01：受理
                //02：不受理
                stage = "受理";
                if (LocalStatus.slzt01.equals(tmp.getSlzt())) {
                    status = "受理";
                } else if (LocalStatus.slzt02.equals(tmp.getSlzt())) {
                    status = "不予受理";
                }
            } else {
                //申请阶段
                stage = "申请";
                status = "申请";
            }
            tmp.setStage(stage);
            tmp.setStatus(status);
            rstList.add(tmp);       }
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, rstList),
                HttpStatus.OK);
    }
}
