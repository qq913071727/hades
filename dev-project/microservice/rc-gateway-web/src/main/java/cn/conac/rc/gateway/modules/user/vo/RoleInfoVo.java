package cn.conac.rc.gateway.modules.user.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.conac.rc.gateway.modules.user.entity.RoleEntity2;
import cn.conac.rc.gateway.modules.user.entity.RolePermissionEntity;
import io.swagger.annotations.ApiModel;

/**
 * RoleInfoVo类
 *
 * @author voCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
public class RoleInfoVo  implements Serializable {
	
	private static final long serialVersionUID = -5033726169143304989L;

	private RoleEntity2 roleEntity;
	
	private List<RolePermissionEntity> rolePermissionEntityList = new ArrayList<RolePermissionEntity>();

	public RoleEntity2 getRoleEntity() {
		return roleEntity;
	}

	public void setRoleEntity(RoleEntity2 roleEntity) {
		this.roleEntity = roleEntity;
	}

	public List<RolePermissionEntity> getRolePermissionEntityList() {
		return rolePermissionEntityList;
	}

	public void setRolePermissionEntityList(List<RolePermissionEntity> rolePermissionEntityList) {
		this.rolePermissionEntityList = rolePermissionEntityList;
	}
}
