package cn.conac.rc.gateway.modules.user.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;



import java.net.URLEncoder;

@Service
public class SmUserMappingService {

    @Value("${sm.api.url}")
    private String targetURL;

    @Value("${sm.api.itemKey}")
    private String itemKey;

    @Value("${sm.api.itemCode}")
    private String itemCode;

    public String getTargetURL() {
        return targetURL;
    }

    public String getItemKey() {
        return itemKey;
    }

    public void setTargetURL(String targetURL) {
        this.targetURL = targetURL;
    }

    public void setItemKey(String itemKey) {
        this.itemKey = itemKey;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public JSONArray getUserMapping(String orgName,Integer page,Integer pageSize) {
        try {
            URL targetUrl = new URL(this.targetURL + URLEncoder.encode(orgName, "UTF-8") +"/" + page + "/" + pageSize);
            HttpURLConnection httpConnection = (HttpURLConnection) targetUrl.openConnection();
            httpConnection.setDoOutput(true);
            httpConnection.setRequestMethod("GET");
            httpConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            //JSONObject req = new JSONObject();
            // itemKey为预定一致的
            //req.put("itemKey", this.itemKey);
            // req.put("domainName", "海口市救助管理站.公益");
            //req.put("name", name);
            //req.put("product", product);
            //req.put("password", "ysbb123456");
            //req.put("password", password);
            // 发送请求时服务器时间 时间格式为yyyy-MM-dd hh:mm:ss
            //Date currentDate = new Date();
            //req.put("reqTime", DateUtils.getFormatDateTime(currentDate, "yyyy-MM-dd HH:mm:ss"));
            //String itemCode = this.itemCode;
            //MD5Utils encoderMd5 = new MD5Utils(itemCode, "MD5");
            //String sign = encoderMd5.encode(DateUtils.getFormatDateTime(currentDate, "yyyy-MM-dd"));
            // System.out.println(sign);
            // 加密后的grandDate作为sign传入
            //req.put("sign", sign);
            //String input = req.toString();
            // System.out.println(input);
            //OutputStreamWriter outputStream = new OutputStreamWriter(httpConnection.getOutputStream(), "UTF-8");
            // OutputStream outputStream = httpConnection.getOutputStream();
            //outputStream.write(input);
            // outputStream.write(input.getBytes());
            //outputStream.flush();

            BufferedReader responseBuffer = new BufferedReader(
                    new InputStreamReader((httpConnection.getInputStream())));
            String output;
            // System.out.println("Output from Server:\n");
            JSONArray obj = null;
            while (StringUtils.isNotBlank(output = responseBuffer.readLine())) {
                // 打印数据返回结果json
                // System.out.println(output);
                obj = JSONObject.parseArray(output);
            }
            httpConnection.disconnect();
            return obj;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
