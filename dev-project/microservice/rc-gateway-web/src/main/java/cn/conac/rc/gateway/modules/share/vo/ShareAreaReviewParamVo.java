package cn.conac.rc.gateway.modules.share.vo;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ShareAreaInfoVo类
 *
 * @author voCreator
 * @date 2017-07-21
 * @version 1.0
 */
@ApiModel
public class ShareAreaReviewParamVo implements Serializable {

	private static final long serialVersionUID = 4268834493163416753L;
	
	List<Integer> shareInfoIdList;
	
	@ApiModelProperty("审核结果（2-通过  3-拒绝 4-挂起)")
	private Integer reviewStatus;

	@ApiModelProperty("审核拒绝原因")
	private String refusalReason;

	@ApiModelProperty("审核备注信息")
	private String reviewRemark;
	
	public Integer getReviewStatus() {
		return reviewStatus;
	}

	public void setReviewStatus(Integer reviewStatus) {
		this.reviewStatus = reviewStatus;
	}

	public String getRefusalReason() {
		return refusalReason;
	}

	public void setRefusalReason(String refusalReason) {
		this.refusalReason = refusalReason;
	}

	public String getReviewRemark() {
		return reviewRemark;
	}

	public void setReviewRemark(String reviewRemark) {
		this.reviewRemark = reviewRemark;
	}

	public List<Integer> getShareInfoIdList() {
		return shareInfoIdList;
	}

	public void setShareInfoIdList(List<Integer> shareInfoIdList) {
		this.shareInfoIdList = shareInfoIdList;
	}
	
}
