package cn.conac.rc.gateway.common.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.conac.rc.gateway.common.entity.AreaEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 地区信息树形结构
 */
@ApiModel("地区信息")
public class AreaTreeVo implements Serializable {
    
    private static final long serialVersionUID = 8030023983021201711L;
    
    @ApiModelProperty("主键id")
    private String id;
    
    @ApiModelProperty("上级pid")
    private String pid;
    
    @ApiModelProperty("地区名称")
    private String name;
    
    @ApiModelProperty("地区code")
    private String code;
    
    @ApiModelProperty("地区权重")
    private Integer weight;
    
    @ApiModelProperty("等级lvl")
    private Integer lvl;
    
    @ApiModelProperty("节点leaf")
    private Integer leaf;
    
    @ApiModelProperty("行政区划编码")
    private String adminCode;
    
    @ApiModelProperty("试点1：试点0：非试点")
    private String type;
    
	@ApiModelProperty("1: 有公开办法 0：无公开办法")
	private Integer isPublic;
	
	@ApiModelProperty("1:已共享 0：未共享")
	private Integer isShared = 0;
	
    @ApiModelProperty("下级地区")
    private List<AreaTreeVo> child = new ArrayList<AreaTreeVo>();
    
    public AreaTreeVo() {
    }
    
    public AreaTreeVo (AreaEntity areaEntity){
    	this.id = areaEntity.getId();
    	this.pid = areaEntity.getPid();
    	this.name = areaEntity.getName();
    	this.code = areaEntity.getCode();
    	this.weight = areaEntity.getWeight();
    	this.lvl = areaEntity.getLvl();
    	this.leaf = areaEntity.getLeaf();
    	this.adminCode = areaEntity.getAdminCode();
    	this.isPublic = areaEntity.getIsPublic();
    	this.isShared = areaEntity.getIsShared();
    }
	
    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPid() {
        return pid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setLvl(Integer lvl) {
        this.lvl = lvl;
    }

    public Integer getLvl() {
        return lvl;
    }

    public void setLeaf(Integer leaf) {
        this.leaf = leaf;
    }

    public Integer getLeaf() {
        return leaf;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdminCode() {
        return adminCode;
    }

    public void setAdminCode(String adminCode) {
        this.adminCode = adminCode;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

	public Integer getIsPublic() {
		return isPublic;
	}
	
	public void setIsPublic(Integer isPublic) {
		this.isPublic = isPublic;
	}
	
	public Integer getIsShared() {
		return isShared;
	}

	public void setIsShared(Integer isShared) {
		this.isShared = isShared;
	}
	
	public void addChild(AreaTreeVo areaTreeVo){
		this.child.add(areaTreeVo);
	}

	public List<AreaTreeVo> getChild() {
		return child;
	}

	public void setChild(List<AreaTreeVo> child) {
		this.child = child;
	}
}
