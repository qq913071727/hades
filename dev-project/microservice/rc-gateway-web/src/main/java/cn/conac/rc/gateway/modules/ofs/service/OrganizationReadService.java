package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.gateway.modules.ofs.vo.OrganizationVo;

@FeignClient("RC-SERVICE-OFS-R")
@CacheConfig(keyGenerator = "keyGenerator", cacheNames = "ofs_cache")
public interface OrganizationReadService {

	@RequestMapping(value = "organization/recentOrgs", method = RequestMethod.GET)
	public JSONObject findRecentOrgs4App(@RequestParam("num") int num, @RequestParam("areaCode") String areaCode);
    
	@Cacheable
	@RequestMapping(value = "organization/{id}", method = RequestMethod.GET)
	public JSONObject findById(@PathVariable("id") String id);

	@RequestMapping(value = "organization/count", method = RequestMethod.POST)
	public JSONObject count(@RequestBody OrganizationVo vo);

	@RequestMapping(value = "organization/list", method = RequestMethod.POST)
	public JSONObject list(@RequestBody OrganizationVo vo);

	@RequestMapping(value = "organization/{id}/delete", method = RequestMethod.POST)
	public JSONObject delete(@PathVariable("id") String id);

	// 根据参数查询部门库
	@RequestMapping(value = "organization/Org", method = RequestMethod.GET)
	public JSONObject findOrgByParam(@RequestBody OrganizationVo vo);

	// 根据父部门标识查询第一级子部门库
	@RequestMapping(value = "organization/ChildOrgs/{parentId}", method = RequestMethod.GET)
	public JSONObject findChildOrgs(@PathVariable("parentId") String parentId);


//
//	// 查询九大系统
//	@RequestMapping(value = "organization/findOwnSys", method = RequestMethod.POST)
//	public JSONObject findOwnSys();

	// 统计地区的事业单位和行政机关
	@Cacheable
	@RequestMapping(value = "organization/CountXZJGAndSYDW/{areaCode}", method = RequestMethod.GET)
	public JSONObject CountXZJGAndSYDW(@PathVariable("areaCode") String areaCode);

	// 统计地区的事业单位和行政机关
	@Cacheable
	@RequestMapping(value = "organization/orgTree/{areaCode}", method = RequestMethod.GET)
	public JSONObject findOrgTree(@PathVariable("areaCode") String areaCode);
	
//	// 根据id获取机构树信息
//	@RequestMapping(value = "organization/mapTree/{id}", method = RequestMethod.GET)
//	 public JSONObject findMapTreeById(@PathVariable("id") String id);
//
//
//	// 统计地区的事业单位和行政机关
//	@RequestMapping(value = "organizationBase/orgnization/{orgId}", method = RequestMethod.GET)
//	public JSONObject findOrgDetailByOrgId(@PathVariable("orgId") String orgId);
//	
//	// 根据用户Id查询部门树
//	@RequestMapping(value = "organization/{userId}/orgTree", method = RequestMethod.GET)
//	public JSONObject findOrgTreeByUserId(@PathVariable("userId") Integer userId);
	
}
