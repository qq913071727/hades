package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-OFS")
public interface VHistApprovalService {
	
	@RequestMapping(method = RequestMethod.GET, value = "/history/list/{orgId}")
	 public JSONObject historyList(@ApiParam(value = "部门OrgId", required = true) @PathVariable("orgId") String orgId);

}
