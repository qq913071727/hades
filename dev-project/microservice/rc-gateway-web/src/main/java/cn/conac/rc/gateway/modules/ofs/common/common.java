package cn.conac.rc.gateway.modules.ofs.common;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.ResultPojo;
import cn.conac.rc.gateway.modules.ofs.contsant.Contsants;
import cn.conac.rc.gateway.modules.ofs.entity.OrganizationBaseEntity;
import cn.conac.rc.gateway.modules.ofs.entity.OrganizationEntity;
import cn.conac.rc.gateway.modules.ofs.service.OrganizationBaseService;
import cn.conac.rc.gateway.modules.ofs.service.OrganizationService;

public class common {

	/**
	 *主管部门变更所属系统(使用场景：编办部门直接提交和编办部门审核通过时调用)
	 * @param OrganizationService  部门库service
	 * @param OrganizationBaseService  部门基本信息service
	 * @param orgEntity  部门库实体
	 * @param resultInfo  
	 * @return 0：正常 -1：失败
	 */
    public static int compOrgUpdateOwnSys(OrganizationService orgService,OrganizationBaseService orgBaseService, OrganizationEntity orgEntity,ResultPojo resultInfo) {
    	int ret =0 ;
    	Map<String,String> idOwnSysMap = new HashMap<String, String>();
    	// 1、 当前部门是主管部门更新场合
		// 主管部门的场合并且更改了所属系统场合，递归更新子部门的所属系统
		if(StringUtils.isEmpty(orgEntity.getParentId()) && Contsants.OFS_ORG_STR_IS_COMP.equals(orgEntity.getIsComp())) {
			// 判断是不是更改了所属系统
			JSONObject dbOrgInfoJson =  orgService.findById(orgEntity.getId());
			if (ResultPojo.CODE_SUCCESS.equals(dbOrgInfoJson.getString("code")) ) {
				// 部门库信息取得成功场合
				OrganizationEntity dbOrgEntity = new OrganizationEntity();
				dbOrgEntity = JSONObject.toJavaObject(dbOrgInfoJson.getJSONObject("result"), OrganizationEntity.class);
				// 当前的所属系统与db中的所属系统不同
				if(!orgEntity.getOwnSys().equals(dbOrgEntity.getOwnSys())) {
					// 递归更新子部门的所属系统(部门库和部门基本信息表两张表)
					ret =comRecUpdateOwnSys(orgService,orgBaseService,orgEntity.getId(), orgEntity.getOwnSys(),idOwnSysMap,resultInfo);
					return ret;
				}
			}
		}
    	return 0;
    }
    
	/**
	 *  使用递归为下设机构更新所属系统(使用场景：编办部门直接提交和编办部门审核通过时调用)
	 * @param OrganizationService  部门库service
	 * @param OrganizationBaseService  部门基本信息service
	 * @param orgId  部门库Id
	 * @param resultInfo  
	 * @return 0：正常 -1：失败
	 */
	public static int  comRecUpdateOwnSys(OrganizationService orgService, OrganizationBaseService orgBaseService, String orgId, String ownSys, Map<String, String> idOwnSysMap, ResultPojo resultInfo ) {
		
		int ret = 0;
		OrganizationEntity subOrgEntity = new OrganizationEntity();
		JSONObject  childOrgJsonObj = null;
		JSONObject  subOrgInfoJsonObj = null;
		String subOrgId = null;
		JSONArray childOrgList  = new JSONArray();
		
		// 通过orgId查找下设机构
		JSONObject childOrgInfoJson   = orgService.findChildOrgs(orgId);
		if (ResultPojo.CODE_SUCCESS.equals(childOrgInfoJson.getString("code")) ) {
			childOrgList = childOrgInfoJson.getJSONArray("result");
		} else {
			// 取得下设机构失败场合
			resultInfo.setCode(childOrgInfoJson.getString("code"));
			resultInfo.setMsg("[orgService.findChildOrgs] in function of comRecUpdateOwnSys (currentOrgId="  + orgId+
									" error occured) " + ResultPojo.MSG_FAILURE + "【"  + childOrgInfoJson.getString("msg") + "】" );
			resultInfo.setResult(idOwnSysMap);
			return -1;
		}
		if(childOrgList.isEmpty() == true){
			return 0;
		} else {
			for (int i=0; i < childOrgList.size(); i++) {
				
				childOrgJsonObj =  (JSONObject)childOrgList.get(i);
				
				// 更新下设机构的所属系统
				subOrgEntity = JSONObject.toJavaObject(childOrgJsonObj, OrganizationEntity.class);
				subOrgEntity.setOwnSys(ownSys);
				subOrgInfoJsonObj = (JSONObject)JSONObject.toJSON(subOrgEntity);
				JSONObject updateOrgInfoJson = orgService.saveOrUpdate(subOrgInfoJsonObj);
				 // 判断保存是否成功失败
				 if (!ResultPojo.CODE_SUCCESS.equals(updateOrgInfoJson.getString("code")) ) {
					  // 保存失败场合
					  resultInfo.setCode(updateOrgInfoJson.getString("code"));
					  resultInfo.setMsg("[orgService.saveOrUpdate] in function of comRecUpdateOwnSys " + ResultPojo.MSG_FAILURE + "【"  + updateOrgInfoJson.getString("msg") + "】" );
					  resultInfo.setResult(idOwnSysMap);
					  return  -1;
				 } 
				subOrgId = childOrgJsonObj.getString("id");
				idOwnSysMap.put(subOrgId, ownSys);
				// 如果部门库中baseID不为空场合下，将部门基本信息表中的所属系统也一并更新
				if(StringUtils.isNotEmpty(subOrgEntity.getBaseId())) {
					// 部门基本信息表中的所属系统也一并更新
					 OrganizationBaseEntity baseEntityInfo = new OrganizationBaseEntity();
				     JSONObject baseInfoJson = orgBaseService.findById(new Integer(subOrgEntity.getBaseId()));
				     if (ResultPojo.CODE_SUCCESS.equals(baseInfoJson.getString("code"))) {
				    	 baseEntityInfo =   JSONObject.toJavaObject(baseInfoJson.getJSONObject("result"), OrganizationBaseEntity.class);
				    	 baseEntityInfo.setOwnSys(ownSys);
				      } else {
				    	  resultInfo.setCode(baseInfoJson.getString("code"));
				    	  resultInfo.setMsg("[orgBaseService.findById] in function of comRecUpdateOwnSys " + ResultPojo.MSG_FAILURE + "【"  + baseInfoJson.getString("msg") + "】" );
				    	  resultInfo.setResult(idOwnSysMap);
				          return -1;
				     }
					 // 执行保存部门基本信息表
				     JSONObject updateBaseInfoJson = orgBaseService.saveOrUpdate(baseEntityInfo, "1");
			    	 // 判断保存是否成功失败
					 if (!ResultPojo.CODE_SUCCESS.equals(updateBaseInfoJson.getString("code")) ) {
						  // 保存失败场合
						  resultInfo.setCode(updateBaseInfoJson.getString("code"));
						  resultInfo.setMsg("[orgBaseService.saveOrUpdate] in function of recUpdateOwnSys " + ResultPojo.MSG_FAILURE + "【"  + updateBaseInfoJson.getString("msg") + "】" );
						  resultInfo.setResult(idOwnSysMap);
						  return  -1;
					 }
				}
				// 递归更新下设机构的下设机构的所属系统
				ret = comRecUpdateOwnSys(orgService, orgBaseService, subOrgId, ownSys, idOwnSysMap, resultInfo);
				if(ret != 0) {
					return -1;
				}
			}
		}
		return 0;
	}
	
	
}
