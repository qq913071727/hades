package cn.conac.rc.gateway.modules.share.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ShareAreaInfoVo类
 *
 * @author voCreator
 * @date 2017-07-21
 * @version 1.0
 */
@ApiModel
public class ShareAreaInfoMsgVo implements Serializable {

	private static final long serialVersionUID = -3233146995845240878L;


	@ApiModelProperty("地区名称")
	private String areaName;

	@ApiModelProperty("地区代码")
	private String areaCode;
	
	@ApiModelProperty("提醒用户的信息")
	private String msg;

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
