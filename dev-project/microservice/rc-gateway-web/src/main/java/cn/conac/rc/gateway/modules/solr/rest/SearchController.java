package cn.conac.rc.gateway.modules.solr.rest;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.RestClientException;
import cn.conac.rc.gateway.modules.solr.service.SearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="solr/searchInfo/")
@Api(tags="搜索接口", description="三定信息")
public class SearchController {

    @Autowired
    SearchService service;
    
    @ApiOperation(value = "检索机构信息", httpMethod = "POST", response = JSONObject.class, notes = "根据输入关键字和区域ID检索机构信息")
	@RequestMapping(value = "org/", method = RequestMethod.POST)
	 public JSONObject searchOrgInfo(HttpServletRequest request, HttpServletResponse response,
	    		@ApiParam(value = "查询条件对象", required = true)  @RequestBody Map<String, String> condMap)
	            throws RestClientException, Exception {
	        return service.searchOrgInfo(condMap);
	  }
	
	@ApiOperation(value = "检索历史沿革机构信息", httpMethod = "POST", response = JSONObject.class, notes = "根据输入关键字和区域ID检索历史沿革机构信息")
	@RequestMapping(value = "histOrg/", method = RequestMethod.POST)
	public JSONObject searchHistOrgInfo(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam(value = "查询条件对象", required = true) @RequestBody Map<String, String> condMap)
            throws RestClientException, Exception {
        return service.searchHistOrgInfo(condMap);
    }
	
	@ApiOperation(value = "检索机构三定分析信息", httpMethod = "POST", response = JSONObject.class, notes = "根据输入关键字、区域ID、是否是历史标示、批文开始时间和批文结束时间检索机构三定分析信息")
	@RequestMapping(value = "orgAnaly/", method = RequestMethod.POST)
	public JSONObject searchOrgAnalyInfo(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam(value = "查询条件对象", required = true)  @RequestBody Map<String, String> condMap)
            throws RestClientException, Exception {
        return service.searchOrgAnalyInfo(condMap);
  }
	
	@ApiOperation(value = "检索综合和批文提示词信息", httpMethod = "POST", response = JSONObject.class, notes = "根据输入关键字综合和批文提示词信息")
	@RequestMapping(value = "suggest/org/", method = RequestMethod.POST)
	public JSONObject searchOrgSuggInfo(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true)  @RequestBody Map<String, String> condMap)
            throws RestClientException, Exception {
        return service.searchOrgSuggInfo(condMap);
    }
	
	@ApiOperation(value = "检索历史沿革机构提示词信息", httpMethod = "GET", response = JSONObject.class, notes = "根据输入关键字检索历史沿革机构提示词信息")
	@RequestMapping(value = "suggest/histOrg/{queryWord}", method = RequestMethod.GET)
	public JSONObject searchHistOrgSuggInfo(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "queryWord", required = true) @PathVariable("queryWord") String queryWord)
            throws RestClientException, Exception {
        return service.searchHistOrgSuggInfo(queryWord);
    }
	
	@ApiOperation(value = "综合查询", httpMethod = "POST", response = JSONObject.class, notes = "根据输入组合条件进行综合查询")
	@RequestMapping(value = "comp/org/", method = RequestMethod.POST)
	public JSONObject searchOrgCompInfo(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam(value = "查询条件对象", required = true) @RequestBody Map<String, String> condMap)
            throws RestClientException, Exception {
        return service.searchOrgCompInfo(condMap);
    }
	
    @ApiOperation(value = "检索机构批文信息", httpMethod = "POST", response = JSONObject.class, notes = "根据输入关键字（批文名称或批文号）、区域ID、批文开始时间和批文结束时间检索机构信息")
	@RequestMapping(value = "orgApproval/", method = RequestMethod.POST)
	 public JSONObject searchOrgApprovalInfo(HttpServletRequest request, HttpServletResponse response,
	    		@ApiParam(value = "查询条件对象", required = true)  @RequestBody Map<String, String> condMap)
	            throws RestClientException, Exception {
	        return service.searchOrgApprovalInfo(condMap);
	  }
}
