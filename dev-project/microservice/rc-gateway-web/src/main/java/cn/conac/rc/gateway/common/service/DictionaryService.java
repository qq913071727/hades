package cn.conac.rc.gateway.common.service;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSONObject;

import io.swagger.annotations.ApiParam;


@FeignClient("RC-SERVICE-COMMON")
public interface DictionaryService {
    @RequestMapping(value = "/comm/saveDic", method = RequestMethod.POST)
    public JSONObject saveDic(@RequestParam(value = "key", required = false) String key,
            @RequestParam(value = "value", required = false) String value,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "sort", required = false) Integer sort,
            @RequestParam(value = "parentId", required = false) String parentId);

    @RequestMapping(value = "/comm/updateDic", method = RequestMethod.POST)
    public JSONObject updateDic(@RequestParam(value = "id", required = false) String id,
            @RequestParam(value = "key", required = false) String key,
            @RequestParam(value = "value", required = false) String value,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "sort", required = false) Integer sort,
            @ApiParam(value = "value的父value的id", required = false) @RequestParam(value = "parentId", required = false) String parentId);

    @RequestMapping(value = "/comm/updateDicKey", method = RequestMethod.POST)
    public JSONObject updateDicKey(@RequestParam(value = "id", required = false) String id,
            @RequestParam(value = "key", required = false) String key,
            @RequestParam(value = "name", required = false) String name);

    @RequestMapping(value = "/comm/deleteDic", method = RequestMethod.POST)
    public JSONObject deleteDic(@RequestParam(value = "id", required = false) String id);

    @RequestMapping(value = "/comm/deleteDicByKey", method = RequestMethod.POST)
    public JSONObject deleteDicByKey(@RequestParam(value = "id", required = false) String key);

    @RequestMapping(value = "/comm/getDicByKey/{key}", method = RequestMethod.GET)
    public JSONObject getDicByKey(@PathVariable("key") String key);

    @RequestMapping(value = "/comm/getDicById/{id}", method = RequestMethod.GET)
    public JSONObject getDicById(@PathVariable("id") String id);

    @RequestMapping(value = "/comm/getAllDics", method = RequestMethod.GET)
    public JSONObject getAllDics();

    @RequestMapping(value = "/industryDict/getAllIndustry", method = RequestMethod.GET)
    public JSONObject getAllIndustry();

    @RequestMapping(value = "/categoryDict/getAllCategory", method = RequestMethod.GET)
    public JSONObject getAllCategory();
}
