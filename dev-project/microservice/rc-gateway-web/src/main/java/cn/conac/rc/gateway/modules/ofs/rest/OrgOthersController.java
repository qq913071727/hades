package cn.conac.rc.gateway.modules.ofs.rest;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.BeanMapper;
import cn.conac.rc.framework.utils.RestClientException;
import cn.conac.rc.framework.utils.ResultPojo;
import cn.conac.rc.gateway.modules.ofs.entity.OrgOthersEntity;
import cn.conac.rc.gateway.modules.ofs.entity.OrgStatusEntity;
import cn.conac.rc.gateway.modules.ofs.service.OrgOthersService;
import cn.conac.rc.gateway.modules.ofs.service.OrgStatusService;
import cn.conac.rc.gateway.security.UserUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="ofs/")
@Api(tags="部门的其它信息", description="三定信息")
public class OrgOthersController {

    @Autowired
    OrgOthersService orgOthersService;
    
    @Autowired
    OrgStatusService  orgStatusService;
    
	@ApiOperation(value = "其他事项及附则资源信息", httpMethod = "GET", response = JSONObject.class, notes = "根据id获取其他事项及附则资源信息")
    @RequestMapping(value = "bases/{id}/orgOthers", method = RequestMethod.GET)
    public JSONObject findListById(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "id", required = true) @PathVariable("id") String id)
            throws RestClientException, Exception {
        return orgOthersService.findById(id);
    }
	
	@ApiOperation(value = "部门其他事项及附则信息保存", httpMethod = "POST", response = JSONObject.class, notes = "部门其他事项及附则信息保存")
    @RequestMapping(value = "others/c", method = RequestMethod.POST)
    public Object cudOtherSave(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "部门其他事项及附则信息和状态信息的entity", required = true)  @RequestBody Map<String, Map<String, Object>> entityMap  )
            throws RestClientException, Exception {
    	
    	 ResultPojo resultInfo = new ResultPojo();
    	 Map<String,String> idMap = new HashMap<String, String>();
    	 
    	 //******************第一步从页面侧获取部门其他事项及附则信息和部门状态实体对象***********
         // 获得部门其他事项及附则信息
    	 OrgOthersEntity orgOthersEntityInfo =new OrgOthersEntity();
    	 BeanMapper.copy(entityMap.get("otherEntity"), orgOthersEntityInfo);
    	
    	 // 确保部门其他事项及附则信息中的ID不为null
    	 String baseId = orgOthersEntityInfo.getId();
    	 if(null == baseId){
    		 //部门其他事项及附则信息中的ID为null场合返回错误信息
	       	  resultInfo.setCode(ResultPojo.CODE_FAILURE);
	          resultInfo.setMsg("[ the id of orgOthersEntityInfo is null] in function of otherSave " + ResultPojo.MSG_FAILURE);
	       	  resultInfo.setResult(null);
	       	  return resultInfo;
    	 }
   
         // 获得部门状态信息
         OrgStatusEntity statusEntityInfo=  new OrgStatusEntity();
         BeanMapper.copy(entityMap.get("statusEntity"), statusEntityInfo);
       
         // 取得完整度掩码
         String stepMask = statusEntityInfo.getContentMask();
         // 判断是否是暂存[0]还是正常保存[1]，默认为正常保存 
         String submitType = "1";
         if(null  != stepMask ) {
        	 // 完整度掩码的格式为【111111】，当暂存场合 完整度掩码的第一个字符为[0],正常保存时为【1】
        	 submitType = stepMask.substring(5, 6);
         }
         
         //******************第二步保存部门其他事项及附则信息表**************************************
         // 执行保存部门其他事项及附则信息表
    	 JSONObject otherInfoJson =  orgOthersService.saveOrUpdate(submitType, orgOthersEntityInfo);
    	 
    	 // 判断保存是否成功失败
    	  if (ResultPojo.CODE_SUCCESS.equals(otherInfoJson.getString("code"))) {
    		  // 保存成功场合
    		  idMap.put("id", otherInfoJson.getJSONObject("result").getString("id"));
    		  resultInfo.setResult(idMap);
          } else {
        	 // 保存失败场合
        	  resultInfo.setCode(otherInfoJson.getString("code"));
        	  resultInfo.setMsg("[orgOthersService.saveOrUpdate] in function of otherSave " 
        			  					+ ResultPojo.MSG_FAILURE + "【"  + otherInfoJson.getString("msg") + "】");
        	  resultInfo.setResult(null);
         	  return resultInfo;
          }
    	  
    	 //*******************第三步保存部门的状态表******************************************
    	 // 设置状态表ID
    	 statusEntityInfo.setId(idMap.get("id"));
    	  // 设置登录用户ID
		 statusEntityInfo.setUpdateUserId(UserUtils.getCurrentUser().getId());
		  // 执行保存部门的状态表
		  JSONObject statusInfoJson = orgStatusService.saveOrUpdate(statusEntityInfo);
		 
		  // 判断保存是否成功失败
		  if (ResultPojo.CODE_SUCCESS.equals(statusInfoJson.getString("code")) ) {
			  // 保存成功场合
			  resultInfo.setCode(ResultPojo.CODE_SUCCESS);
			  resultInfo.setMsg(ResultPojo.MSG_SUCCESS);
		  } else {
			 // 保存失败场合
			  resultInfo.setCode(statusInfoJson.getString("code"));
			  resultInfo.setMsg("[orgStatusService.saveOrUpdate] in function of otherSave " 
					  				+ ResultPojo.MSG_FAILURE + "【"  + statusInfoJson.getString("msg") + "】");
			  resultInfo.setResult(null);
		      // 程序事务控制
		  	  // 删除保存的部门基本信息 TODO
			  return  resultInfo;
		  }
    	 
         // 成功保存部门信息表和部门状态表返回
         return  resultInfo;
    }
    
    @ApiOperation(value = "部门其他事项及附则信息更新", httpMethod = "POST", response = JSONObject.class, notes = "部门其他事项及附则信息更新")
    @RequestMapping(value = "others/{baseId}/u", method = RequestMethod.POST)
    public Object cudOtherUpdate(HttpServletRequest request, HttpServletResponse response,
    		@ApiParam("部门基本信息Id") @PathVariable("baseId") String baseId,
            @ApiParam(value = "部门其他事项及附则信息和状态信息的entity", required = true)  @RequestBody Map<String, Map<String, Object>> entityMap  )
            throws RestClientException, Exception {
    	
    	 ResultPojo resultInfo = new ResultPojo();
    	 if(null == baseId) {
    		  // 参数baseId为null场合
	       	  resultInfo.setCode(ResultPojo.CODE_FAILURE);
	          resultInfo.setMsg("[ baseId of parameter is null ] in function of otherUpdate " + ResultPojo.MSG_FAILURE);
	       	  resultInfo.setResult(null);
	       	  return resultInfo;
    	 }
    	 
    	 Map<String,String> idMap = new HashMap<String, String>();
    	 
    	 //******************第一步从页面侧获取部门其他事项及附则信息和部门状态实体对象***********
         // 获得部门其他事项及附则信息
    	 OrgOthersEntity orgOthersEntityInfo =new OrgOthersEntity();
    	 BeanMapper.copy(entityMap.get("otherEntity"), orgOthersEntityInfo);
    	 
    	 // 确保部门其他事项及附则信息中的ID不为null
    	 orgOthersEntityInfo.setId(baseId);
    	 
         // 获得部门状态信息
         OrgStatusEntity statusEntityInfo=  new OrgStatusEntity();
         BeanMapper.copy(entityMap.get("statusEntity"), statusEntityInfo);
     
         // 取得完整度掩码
         String stepMask = statusEntityInfo.getContentMask();
         // 判断是否是暂存[0]还是正常保存[1]，默认为正常保存 
         String submitType = "1";
         if(null  != stepMask ) {
        	 // 完整度掩码的格式为【111111】，当暂存场合 完整度掩码的第一个字符为[0],正常保存时为【1】
        	 submitType =  stepMask.substring(5, 6);
         }
         
         //******************第二步保存部门其他事项及附则信息表**************************************
         // 执行保存部门其他事项及附则信息表
    	 JSONObject otherInfoJson =  orgOthersService.saveOrUpdate(submitType,orgOthersEntityInfo);
    	 
    	 // 判断保存是否成功失败
    	  if (ResultPojo.CODE_SUCCESS.equals(otherInfoJson.getString("code"))) {
    		  // 保存成功场合
    		  idMap.put("id", otherInfoJson.getJSONObject("result").getString("id"));
    		  resultInfo.setResult(idMap);
          } else {
        	 // 保存失败场合
        	  resultInfo.setCode(otherInfoJson.getString("code"));
        	  resultInfo.setMsg("[orgOthersService.saveOrUpdate] in function of otherUpdate " + 
        			  						ResultPojo.MSG_FAILURE + "【"  + otherInfoJson.getString("msg") + "】");
        	  resultInfo.setResult(null);
         	  return resultInfo;
          }
    	 
    	 //*******************第三步保存部门的状态表******************************************
    	 // 设置状态表ID
    	 statusEntityInfo.setId(idMap.get("id"));
    	  // 设置登录用户ID
		 statusEntityInfo.setUpdateUserId(UserUtils.getCurrentUser().getId());
		  // 执行保存部门的状态表
		  JSONObject statusInfoJson = orgStatusService.saveOrUpdate(statusEntityInfo);
		 
		  // 判断保存是否成功失败
		  if (ResultPojo.CODE_SUCCESS.equals(statusInfoJson.getString("code")) ) {
			  // 保存成功场合
			  resultInfo.setCode(ResultPojo.CODE_SUCCESS);
			  resultInfo.setMsg(ResultPojo.MSG_SUCCESS);
		  } else {
			 // 保存失败场合
			  resultInfo.setCode(statusInfoJson.getString("code"));
			  resultInfo.setMsg("[orgStatusService.saveOrUpdate] in function of otherUpdate " 
					  						+ ResultPojo.MSG_FAILURE + "【"   + statusInfoJson.getString("msg") + "】");
			  resultInfo.setResult(null);
		      // 程序事务控制
		  	  // 删除保存的部门基本信息 TODO
			  return  resultInfo;
		  }
    	 
         // 成功保存部门信息表和部门状态表返回
         return  resultInfo;
    }
}
