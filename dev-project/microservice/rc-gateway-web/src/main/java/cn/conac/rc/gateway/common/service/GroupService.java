package cn.conac.rc.gateway.common.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-COMMON")
public interface GroupService {
    
    @RequestMapping(value = "/group/{groupId}", method = RequestMethod.GET)
    public JSONObject getFileTypes(@ApiParam(value = "groupId", required = false) @PathVariable("groupId") Integer groupId);

}
