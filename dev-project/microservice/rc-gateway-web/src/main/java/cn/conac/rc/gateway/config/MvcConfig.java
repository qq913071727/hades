
package cn.conac.rc.gateway.config;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import cn.conac.rc.gateway.servlet.ValidateCodeServlet;

/**
 * ClassName: MvcConfig
 * @author wangmeng
 * @version 1.0
 */
//@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addRedirectViewController("/login", "/static/bpp/index.html");
        // registry.addRedirectViewController("/", "/static/bpp/index.html");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        super.addResourceHandlers(registry);
        registry.addResourceHandler("/static/**").addResourceLocations("file:static/", "classpath:/static/");
    }

//    /**
//     * @return 验证码图片servlet
//     */
//    @Bean
//    public ServletRegistrationBean jcaptcha() {
//        ServletRegistrationBean bean = new ServletRegistrationBean();
//        bean.setServlet(new ValidateCodeServlet());
//        bean.addUrlMappings("/public/jcaptcha");
//        return bean;
//    }

    /**
     * characterEncodingFilter.
     * @return FilterRegistrationBean
     * @author wangmeng
     */
    @Bean(name = "characterEncodingFilter")
    public FilterRegistrationBean characterEncodingFilter() {
        FilterRegistrationBean bean = new FilterRegistrationBean();
        bean.addInitParameter("encoding", "UTF-8");
        bean.addInitParameter("forceEncoding", "true");
        bean.setFilter(new CharacterEncodingFilter());
        bean.addUrlPatterns("/*");
        return bean;
    }

}