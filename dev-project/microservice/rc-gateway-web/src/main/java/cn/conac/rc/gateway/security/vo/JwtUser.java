package cn.conac.rc.gateway.security.vo;

import java.util.Collection;
import java.util.Date;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cn.conac.rc.gateway.modules.common.vo.UserVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by stephan on 20.03.16.
 */
@ApiModel("token中的用户信息")
public class JwtUser implements UserDetails
{
    private static final long serialVersionUID = 5325384096141053834L;

    @ApiModelProperty("用户id")
    private String id;

    @ApiModelProperty("用户名称")
    private String username;

    @ApiModelProperty("用户密码")
    private String password;

    @ApiModelProperty("用户类型 0为系统管理员 1为编办 2为法制办 3为委办局")
    private int userType;

    @ApiModelProperty("用户所属地区id")
    private String areaId;

    @ApiModelProperty("用户所属地区名称")
    private String areaName;

    @ApiModelProperty("用户所属地区行政区划")
    private String areaAdminCode;

    @ApiModelProperty("用户所属地区code")
    private String areaCode;

    @ApiModelProperty("用户所属地区层级")
    private String areaLvl;

    @ApiModelProperty("电话号码")
    private String mobile;

    @ApiModelProperty("用户所属部门库的部门id")
    private String orgId;

    @ApiModelProperty("用户所属部门库的部门BaseId，关联部门最新详情")
    private String orgBaseId;

    @ApiModelProperty("用户所属部门库的部门名称")
    private String orgName;

    @ApiModelProperty("用户所属部门的系统，9大系统")
    private String orgSysType;

    @ApiModelProperty("用户所属部门的类别，1为机关或2为事业单位")
    private String orgType;

    @ApiModelProperty("区分机构域名和编制域名（1：机构域名  0：编制域名）")
    private String isAdmin;

    @ApiModelProperty("真实姓名")
    private String fullName;
    
    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("用户角色")
    private Collection<? extends GrantedAuthority> authorities;

    @ApiModelProperty("是否激活")
    private boolean enabled;

    @ApiModelProperty("上次密码重置时间")
    private Date lastPasswordResetDate;

    @ApiModelProperty("是否是试点用户;1-是 0-非")
    private String isTrialPoints;

    @ApiModelProperty("是否手机登录;1-是 0-非")
    private String isMobileLogin;

    @ApiModelProperty("是否需要完成信息完善；1-是 0-否")
    private String isComplete;

    @ApiModelProperty("登录编制域名是否重复；1-是 0-否")
    private String isRepeat;
    
    @ApiModelProperty("SI code")
    private String siCode;
    
	@ApiModelProperty("imgUrl")
	private String imgUrl;
	
	@ApiModelProperty("取得编制域名二维码的Key")
	private String qrCodeKey;
	
	@ApiModelProperty("机构域名")
	private String orgDomainName;

    public JwtUser()
    {
    }

    public JwtUser(UserVo user)
    {
        this.id = user.getId();
        this.username = user.getLoginName();
        this.fullName = user.getFullName();
        this.password = user.getPassword();
        this.orgId = user.getOrgId();
        this.areaId = user.getAreaId();
        this.userType = user.getUserType();
        this.isAdmin = user.getIsAdmin();
        this.nickName = user.getNickName();
        this.siCode = user.getSiCode();
        if (user.getIsDisabled() == 1) {
            this.enabled = false;
        } else {
            this.enabled = true;
        }
        
        this.imgUrl=user.getImgUrl();
        this.qrCodeKey=user.getQrCodeKey();
        this.orgDomainName = user.getOrgDomainName();
    }

    public String getId()
    {
        return id;
    }

    @Override
    public String getUsername()
    {
        return username;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked()
    {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    @JsonIgnore
    @Override
    public String getPassword()
    {
        return password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        return authorities;
    }

    @Override
    public boolean isEnabled()
    {
        return enabled;
    }

    @JsonIgnore
    public Date getLastPasswordResetDate()
    {
        return lastPasswordResetDate;
    }

    public String getAreaId()
    {
        return areaId;
    }

    public void setAreaId(String areaId)
    {
        this.areaId = areaId;
    }

    public String getAreaName()
    {
        return areaName;
    }

    public void setAreaName(String areaName)
    {
        this.areaName = areaName;
    }

    public String getAreaAdminCode()
    {
        return areaAdminCode;
    }

    public void setAreaAdminCode(String areaAdminCode)
    {
        this.areaAdminCode = areaAdminCode;
    }

    public String getAreaCode()
    {
        return areaCode;
    }

    public void setAreaCode(String areaCode)
    {
        this.areaCode = areaCode;
    }

    public String getOrgId()
    {
        return orgId;
    }

    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }

    public String getOrgName()
    {
        return orgName;
    }

    public void setOrgName(String orgName)
    {
        this.orgName = orgName;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities)
    {
        this.authorities = authorities;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public void setLastPasswordResetDate(Date lastPasswordResetDate)
    {
        this.lastPasswordResetDate = lastPasswordResetDate;
    }

    public String getAreaLvl()
    {
        return areaLvl;
    }

    public void setAreaLvl(String areaLvl)
    {
        this.areaLvl = areaLvl;
    }

    public String getOrgSysType()
    {
        return orgSysType;
    }

    public void setOrgSysType(String orgSysType)
    {
        this.orgSysType = orgSysType;
    }

    public String getOrgType()
    {
        return orgType;
    }

    public void setOrgType(String orgType)
    {
        this.orgType = orgType;
    }

    public String getOrgBaseId()
    {
        return orgBaseId;
    }

    public void setOrgBaseId(String orgBaseId)
    {
        this.orgBaseId = orgBaseId;
    }

    public int getUserType()
    {
        return userType;
    }

    public void setUserType(int userType)
    {
        this.userType = userType;
    }

    public String getIsTrialPoints()
    {
        return isTrialPoints;
    }

    public void setIsTrialPoints(String isTrialPoints)
    {
        this.isTrialPoints = isTrialPoints;
    }

    public String getIsMobileLogin()
    {
        return isMobileLogin;
    }

    public void setIsMobileLogin(String isMobileLogin)
    {
        this.isMobileLogin = isMobileLogin;
    }

    public String getIsAdmin()
    {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin)
    {
        this.isAdmin = isAdmin;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    // 是否需要完善编制域名信息 1表示需要完善 
    public String getIsComplete() {
         return isComplete;
    }

    public String getIsRepeat()
    {
        return isRepeat;
    }

    public void setIsRepeat(String isRepeat)
    {
        this.isRepeat = isRepeat;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSiCode() {
        return siCode;
    }

    public void setSiCode(String siCode) {
        this.siCode = siCode;
    }

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getQrCodeKey() {
		return qrCodeKey;
	}

	public void setQrCodeKey(String qrCodeKey) {
		this.qrCodeKey = qrCodeKey;
	}

    public void setIsComplete(String isComplete) {
        this.isComplete = isComplete;
    }

	public String getOrgDomainName() {
		return orgDomainName;
	}

	public void setOrgDomainName(String orgDomainName) {
		this.orgDomainName = orgDomainName;
	}

}
