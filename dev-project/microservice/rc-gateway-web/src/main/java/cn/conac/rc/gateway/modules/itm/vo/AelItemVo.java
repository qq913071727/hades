package cn.conac.rc.gateway.modules.itm.vo;

import java.io.Serializable;
import java.util.Date;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class AelItemVo extends AelItemCatalogueEntity implements Serializable {
	public static enum Operation{
		/**新增  ADD*/
		A,
		/**变更 CHANGE*/
		C,
		/**下放 DECENTRALIZATION*/
		D,
		/**转移 TRANSFER*/
		T,
		/**废止 WASTE*/
		W,
		/**取消 PAUSE*/
		P;
	}
	public static enum Status{
		/**未提交*/
		UNCOMMITTED,

		/**已提交*/
		COMMITTED,

		/**已通过*/
		AUDIT_PASS,

		/**驳回*/
		AUDIT_UNPASS,

		/**挂起*/
		HANGUP	;

		public static Status valueOf(int value) {
			switch (value) {
				case 0:
					return UNCOMMITTED;
				case 1:
					return COMMITTED;
				case 2:
					return AUDIT_PASS;
				case 3:
					return AUDIT_UNPASS;
				case 4:
					return HANGUP;
				default:
					return null;
			}
		}

		public String getStringValue(){
			return String.valueOf(this.ordinal());
		}
	}

	private static final long serialVersionUID = 1472903587588453800L;

	@ApiModelProperty("当前分页")
	private Integer page;

	@ApiModelProperty("每页个数")
	private Integer size;
	String itemStatus;
	String operation;
	@ApiModelProperty("创建 开始时间")
	private Date createDateStart;

	@ApiModelProperty("1:已发布，2:待发布，3:历史")
	private String tab;

	@ApiModelProperty("创建 结束时间")
	private Date createDateEnd;

	@ApiModelProperty("更新 开始时间")
	private Date updateDateStart;

	@ApiModelProperty("更新 结束时间")
	private Date updateDateEnd;

	@ApiModelProperty("清单子id号")
	private String childItemId;

	@ApiModelProperty("系统类别")
	private String sysType;
	public void setPage(Integer page){
		this.page=page;
	}

	public Integer getPage(){
		return page;
	}

	public void setSize(Integer size){
		this.size=size;
	}

	public Integer getSize(){
		return size;
	}

	public void setCreateDateStart(Date createDateStart){
		this.createDateStart=createDateStart;
	}

	public Date getCreateDateStart(){
		return createDateStart;
	}

	public void setCreateDateEnd(Date createDateEnd){
		this.createDateEnd=createDateEnd;
	}

	public Date getCreateDateEnd(){
		return createDateEnd;
	}

	public void setUpdateDateStart(Date updateDateStart){
		this.updateDateStart=updateDateStart;
	}

	public Date getUpdateDateStart(){
		return updateDateStart;
	}

	public void setUpdateDateEnd(Date updateDateEnd){
		this.updateDateEnd=updateDateEnd;
	}

	public Date getUpdateDateEnd(){
		return updateDateEnd;
	}

	public String getItemStatus()
	{
		return itemStatus;
	}

	public void setItemStatus(String itemStatus)
	{
		this.itemStatus = itemStatus;
	}

	public String getOperation()
	{
		return operation;
	}

	public void setOperation(String operation)
	{
		this.operation = operation;
	}

	public String getTab()
	{
		return tab;
	}

	public void setTab(String tab)
	{
		this.tab = tab;
	}

	public String getChildItemId()
	{
		return childItemId;
	}

	public void setChildItemId(String childItemId)
	{
		this.childItemId = childItemId;
	}

	public String getSysType()
	{
		return sysType;
	}

	public void setSysType(String sysType)
	{
		this.sysType = sysType;
	}
}
