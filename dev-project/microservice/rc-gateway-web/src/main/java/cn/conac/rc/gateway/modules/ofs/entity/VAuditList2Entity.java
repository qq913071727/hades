package cn.conac.rc.gateway.modules.ofs.entity;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * VAuditList2Entity类
 *
 * @author beanCreator
 * @date 2016-12-26
 * @version 1.0
 */
@ApiModel
public class VAuditList2Entity implements Serializable {

	private static final long serialVersionUID = -6498053556077327689L;

	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("baseId")
	private Integer baseId;

	@ApiModelProperty("orgName")
	private String orgName;

	@ApiModelProperty("ownSys")
	private String ownSys;

	@ApiModelProperty("compOrgName")
	private String compOrgName;

	@ApiModelProperty("type")
	private String type;

	@ApiModelProperty("userOrgName")
	private String userOrgName;

	@ApiModelProperty("updateType")
	private String updateType;

	@ApiModelProperty("status")
	private String status;

	@ApiModelProperty("updateTime")
	private Date updateTime;

	@ApiModelProperty("userId")
	private Integer userId;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setBaseId(Integer baseId){
		this.baseId=baseId;
	}

	public Integer getBaseId(){
		return baseId;
	}

	public void setOrgName(String orgName){
		this.orgName=orgName;
	}

	public String getOrgName(){
		return orgName;
	}

	public void setOwnSys(String ownSys){
		this.ownSys=ownSys;
	}

	public String getOwnSys(){
		return ownSys;
	}

	public void setCompOrgName(String compOrgName){
		this.compOrgName=compOrgName;
	}

	public String getCompOrgName(){
		return compOrgName;
	}

	public void setType(String type){
		this.type=type;
	}

	public String getType(){
		return type;
	}

	public void setUserOrgName(String userOrgName){
		this.userOrgName=userOrgName;
	}

	public String getUserOrgName(){
		return userOrgName;
	}

	public void setUpdateType(String updateType){
		this.updateType=updateType;
	}

	public String getUpdateType(){
		return updateType;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public void setStatus(String status){
		this.status=status;
	}

	public String getStatus(){
		return status;
	}

	public void setUserId(Integer userId){
		this.userId=userId;
	}

	public Integer getUserId(){
		return userId;
	}

}
