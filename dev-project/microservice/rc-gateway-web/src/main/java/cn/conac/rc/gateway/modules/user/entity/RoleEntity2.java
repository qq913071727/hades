package cn.conac.rc.gateway.modules.user.entity;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * RoleEntity类
 *
 * @author beanCreator
 * @date 2017-04-10
 * @version 1.0
 */
@ApiModel
public class RoleEntity2 implements Serializable {

	private static final long serialVersionUID = 7664676387177945889L;

	@ApiModelProperty("roleId")
	private Integer roleId;

	@ApiModelProperty("siteId")
	private Integer siteId;

	@ApiModelProperty("角色名字")
	private String roleName;

	@ApiModelProperty("顺序号")
	private Integer priority;

	@ApiModelProperty("拥有所用权限")
	private String isSuper;

	@ApiModelProperty("角色类型")
	private String roleType;
	
	@ApiModelProperty("注册用户ID")
	private Integer registerUserId;

	@ApiModelProperty("注册时间")
	private Date registerDate;

	@ApiModelProperty("更新用户ID")
	private Integer updateUserId;

	@ApiModelProperty("更新时间")
	private Date updateDate;

	public void setRoleId(Integer roleId){
		this.roleId=roleId;
	}

	public Integer getRoleId(){
		return roleId;
	}

	public void setSiteId(Integer siteId){
		this.siteId=siteId;
	}

	public Integer getSiteId(){
		return siteId;
	}

	public void setRoleName(String roleName){
		this.roleName=roleName;
	}

	public String getRoleName(){
		return roleName;
	}

	public void setPriority(Integer priority){
		this.priority=priority;
	}

	public Integer getPriority(){
		return priority;
	}

	public void setIsSuper(String isSuper){
		this.isSuper=isSuper;
	}

	public String getIsSuper(){
		return isSuper;
	}

	public void setRoleType(String roleType){
		this.roleType=roleType;
	}

	public String getRoleType(){
		return roleType;
	}
	
	public Integer getRegisterUserId() {
		return registerUserId;
	}

	public void setRegisterUserId(Integer registerUserId) {
		this.registerUserId = registerUserId;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}
