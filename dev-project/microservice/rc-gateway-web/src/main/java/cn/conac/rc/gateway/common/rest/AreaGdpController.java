package cn.conac.rc.gateway.common.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.utils.ResultPojo;
import cn.conac.rc.gateway.common.service.AreaGdpService;
import cn.conac.rc.gateway.common.vo.AreaGdpVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags="地区GDP接口", description="共通")
@RequestMapping("comm/")
public class AreaGdpController {
	@Autowired
	AreaGdpService service;
	
	@ApiOperation(value = "查询地区的GDP和面积等信息", httpMethod = "GET", response = AreaGdpVo.class, notes = "查询地区的GDP和面积等信息")
    @RequestMapping(value = "/areaGdp/{areaId}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "areaId", required = true) @PathVariable("areaId") String areaId) {
		if (StringUtils.isBlank(areaId)) {
            return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_FAILURE, "参数错误！"), HttpStatus.OK);
        }
        return new ResponseEntity<ResultPojo>(
                new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS, service.detail(areaId)),
                HttpStatus.OK);
	}
}
