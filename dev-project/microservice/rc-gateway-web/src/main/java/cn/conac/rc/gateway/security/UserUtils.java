package cn.conac.rc.gateway.security;

import java.util.List;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import cn.conac.rc.gateway.modules.common.vo.RoleVo;
import cn.conac.rc.gateway.security.vo.JwtUser;

public class UserUtils {

    /**
     * @return 当前认证的用户信息
     */
    public static JwtUser getCurrentUser() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return (JwtUser) userDetails;
    }

    /**
     * @return 当前用户是否管理员
     */
    public static boolean isAdmin() {
        return isRole(RoleVo.ROLE_ADMIN_SYS);
    }

    /**
     * @return 当前用户是否编办
     */
    public static boolean isBianBan() {
        if (getCurrentUser().getUserType() == 1) {
            return true;
        } else {
            return false;
        }
         //return isRole(RoleVo.ROLE_BB);
    }

    /**
     * @return 当前用户是否法制办
     */
    public static boolean isFazhiBan() {
        return isRole(RoleVo.ROLE_FZB);
    }

    /**
     * @return 当前用户是否普通用户委办局
     */
    public static boolean isWeiBanJu() {
    	 if (getCurrentUser().getUserType() == 3) {
             return true;
         } else {
             return false;
         }
       // return isRole(RoleVo.ROLE_WBJ);
    }

    private static boolean isRole(String role) {
        @SuppressWarnings("unchecked")
        List<SimpleGrantedAuthority> list = (List<SimpleGrantedAuthority>) getCurrentUser().getAuthorities();
        for (SimpleGrantedAuthority simpleGrantedAuthority : list) {
            if (simpleGrantedAuthority.getAuthority().equals(role))
                return true;
        }
        return false;
    }

}
