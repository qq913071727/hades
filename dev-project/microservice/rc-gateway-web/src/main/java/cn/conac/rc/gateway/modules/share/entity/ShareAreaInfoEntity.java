package cn.conac.rc.gateway.modules.share.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ShareAreaInfoEntity类
 *
 * @author beanCreator
 * @date 2017-07-21
 * @version 1.0
 */
@ApiModel
public class ShareAreaInfoEntity implements Serializable {

	private static final long serialVersionUID = 1500615504950624326L;
	
	/**
	 * 地区共享状态审核中
	 */
    public static final int SHARE_AREA_INFO_STATUS_AUDITING = 1; 
    
    /**
	 * 地区共享状态通过
	 */
    public static final int SHARE_AREA_INFO_STATUS_PASSED = 2; 
    
    /**
 	 * 地区共享状态拒绝
 	 */
     public static final int SHARE_AREA_INFO_STATUS_REFUSE = 3; 

	@Id
	@ApiModelProperty("主键")
	private Integer id;

	@ApiModelProperty("申请用户ID")
	private Integer userId;

	@ApiModelProperty("申请发起方(该用户绑定的机构对应的机构域名)")
	private String orgDomainNameRe;

	@ApiModelProperty("地区代码申请方")
	private String areaCodeSource;
	
	@ApiModelProperty("地区代码申请方名称")
	private String areaSourceNameRe;

	@ApiModelProperty("地区被申请方名称")
	private String areaDestNameRe;

	@ApiModelProperty("地区代码被申请方")
	private String areaCodeDest;

	@ApiModelProperty("申请人姓名")
	private String fullName;

	@ApiModelProperty("申请人电话")
	private String mobile;

	@ApiModelProperty("申请时间")
	private Date createTime;

	@ApiModelProperty("申请状态(1-审核中 2-通过  3-拒绝)")
	private Integer status;

	@ApiModelProperty("失效时间")
	private Date endTime;

	@ApiModelProperty("生效时间")
	private Date startTime;

	@ApiModelProperty("备注（可能后台操作用）")
	private String remark;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setUserId(Integer userId){
		this.userId=userId;
	}

	public Integer getUserId(){
		return userId;
	}

	public void setOrgDomainNameRe(String orgDomainNameRe){
		this.orgDomainNameRe=orgDomainNameRe;
	}

	public String getOrgDomainNameRe(){
		return orgDomainNameRe;
	}

	public void setAreaCodeSource(String areaCodeSource){
		this.areaCodeSource=areaCodeSource;
	}

	public String getAreaCodeSource(){
		return areaCodeSource;
	}

	public String getAreaSourceNameRe() {
		return areaSourceNameRe;
	}

	public void setAreaSourceNameRe(String areaSourceNameRe) {
		this.areaSourceNameRe = areaSourceNameRe;
	}

	public void setAreaDestNameRe(String areaDestNameRe){
		this.areaDestNameRe=areaDestNameRe;
	}

	public String getAreaDestNameRe(){
		return areaDestNameRe;
	}

	public void setAreaCodeDest(String areaCodeDest){
		this.areaCodeDest=areaCodeDest;
	}

	public String getAreaCodeDest(){
		return areaCodeDest;
	}

	public void setFullName(String fullName){
		this.fullName=fullName;
	}

	public String getFullName(){
		return fullName;
	}

	public void setMobile(String mobile){
		this.mobile=mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setCreateTime(Date createTime){
		this.createTime=createTime;
	}

	public Date getCreateTime(){
		return createTime;
	}

	public void setStatus(Integer status){
		this.status=status;
	}

	public Integer getStatus(){
		return status;
	}

	public void setEndTime(Date endTime){
		this.endTime=endTime;
	}

	public Date getEndTime(){
		return endTime;
	}

	public void setStartTime(Date startTime){
		this.startTime=startTime;
	}

	public Date getStartTime(){
		return startTime;
	}

	public void setRemark(String remark){
		this.remark=remark;
	}

	public String getRemark(){
		return remark;
	}
}
