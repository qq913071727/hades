package cn.conac.rc.gateway.common.vo;

import cn.conac.rc.gateway.base.BaseVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel
public class AreaGdpVo extends BaseVo {
	
	private static final long serialVersionUID = 5792171343640428052L;
	@ApiModelProperty("数据字典展示名称")
	private String areaId;
	@ApiModelProperty("数据字典实际存储值")
	private String totalGDP;
	@ApiModelProperty("数据字典值的类型")
	private String perSpeedGDP;
	@ApiModelProperty("排序号")
	private Integer totalPeople;
	@ApiModelProperty("父")
	private String areaOfZone;
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getTotalGDP() {
		return totalGDP;
	}
	public void setTotalGDP(String totalGDP) {
		this.totalGDP = totalGDP;
	}
	public String getPerSpeedGDP() {
		return perSpeedGDP;
	}
	public void setPerSpeedGDP(String perSpeedGDP) {
		this.perSpeedGDP = perSpeedGDP;
	}
	public Integer getTotalPeople() {
		return totalPeople;
	}
	public void setTotalPeople(Integer totalPeople) {
		this.totalPeople = totalPeople;
	}
	public String getAreaOfZone() {
		return areaOfZone;
	}
	public void setAreaOfZone(String areaOfZone) {
		this.areaOfZone = areaOfZone;
	}
}
