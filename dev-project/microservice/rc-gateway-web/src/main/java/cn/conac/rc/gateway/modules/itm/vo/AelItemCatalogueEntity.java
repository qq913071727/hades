package cn.conac.rc.gateway.modules.itm.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class AelItemCatalogueEntity implements Serializable {

	private static final long serialVersionUID = -378537713158084824L;
	protected Integer id;
	@ApiModelProperty("通用事项编码")
	private String genaralCode;
	@ApiModelProperty("事项名称")
	private String itemName;
	@ApiModelProperty("事项类型")
	private String itemType;
	@ApiModelProperty("实施对象")
	private String objectIm;
	@ApiModelProperty("所属部门")
	private String ownOrgId;
	@ApiModelProperty("创建时间")
	private Date createTime;
	@ApiModelProperty("备注")
	private String remarks;
	@ApiModelProperty("关联用户ID")
	private String users;
	@ApiModelProperty("是否有下级")
	private String levels;
	@ApiModelProperty("ItemsId")
	private String itemsId;
	@ApiModelProperty("areaId")
	private String areaId;
	@ApiModelProperty("机构名称")
	private String orgName;
	@ApiModelProperty("aelItemDeploiedEntityList")

	public String getGenaralCode() {
		return genaralCode;
	}

	public void setGenaralCode(String genaralCode) {
		this.genaralCode = genaralCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getObjectIm() {
		return objectIm;
	}

	public void setObjectIm(String objectIm) {
		this.objectIm = objectIm;
	}

	public String getOwnOrgId() {
		return ownOrgId;
	}

	public void setOwnOrgId(String ownOrgId) {
		this.ownOrgId = ownOrgId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getUsers() {
		return users;
	}

	public void setUsers(String users) {
		this.users = users;
	}

	public String getLevels() {
		return levels;
	}

	public void setLevels(String levels) {
		this.levels = levels;
	}

	public String getItemsId() {
		return itemsId;
	}

	public void setItemsId(String itemsId) {
		this.itemsId = itemsId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}
}
