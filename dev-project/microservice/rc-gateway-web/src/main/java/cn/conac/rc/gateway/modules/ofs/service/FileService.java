package cn.conac.rc.gateway.modules.ofs.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import com.alibaba.fastjson.JSONObject;

import io.swagger.annotations.ApiParam;

@FeignClient("RC-SERVICE-FILE")
public interface FileService {
	
	/**
	 * 文件信息存储
	 * @param appFile
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/saveFile")
	 public JSONObject saveFile(@ApiParam(value = "文件信息对象", required = true) @RequestBody JSONObject appFile);
	
	/**
	 * 文件下载
	 * @param fileId
	 * @return
	 */
    @RequestMapping(method = RequestMethod.GET, value = "/download/{fileId}")
    JSONObject download(@PathVariable("fileId") @ApiParam(value = "文件ID", required = true) String fileId);

	/**
	 * 文件检索
	 * @param fileId
	 * @return
	 */
    @RequestMapping(method = RequestMethod.GET, value = "/search/{fileId}")
    JSONObject searchFile(@PathVariable("fileId") @ApiParam(value = "文件ID", required = true) String fileId);

    /**
	 * 文件删除(逻辑删除)
	 * @param fileId
	 * @return
	 */
    @RequestMapping(method = RequestMethod.POST, value = "/deleteInLogic")
    JSONObject deleteFileInlogic(@RequestParam("fileId") @ApiParam(value = "文件ID", required = true) String fileId);

    /**
	 * 文件删除(物理删除)
	 * @param fileId
	 * @return
	 */
    @RequestMapping(method = RequestMethod.POST, value = "/deleteInPhysics")
    JSONObject deleteFileInPhysics(@RequestParam("fileId") @ApiParam(value = "文件ID", required = true) String fileId);

}
