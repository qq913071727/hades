package cn.conac.rc.gateway.modules.common.vo;

import java.util.Map;

/**
 * 邮件发送封装实体
 * @author haocm
 * @version 1.0
 */
public class SmsBean
{
    /**
     * 电话号码
     */
    private String mobile;
    /**
     * 短信模板
     */
    private String smsTemplate;

    /**
     * 单位名称
     */
    private String orgName;
    /**
     * 模板内容键值对map 用于替换模版中的变量
     */
    private Map<String, String> map;

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSmsTemplate() {
        return smsTemplate;
    }

    public void setSmsTemplate(String smsTemplate) {
        this.smsTemplate = smsTemplate;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

}
