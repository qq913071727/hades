package cn.conac.rc.gateway.modules.itm.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.conac.rc.framework.utils.StatusUtil;
import cn.conac.rc.framework.utils.StringUtils;
import cn.conac.rc.gateway.base.ResultPojo;
import cn.conac.rc.gateway.modules.itm.manager.AelProcessMng;
import cn.conac.rc.gateway.modules.itm.service.ItemService;
import cn.conac.rc.gateway.modules.itm.vo.AelItemVo;
import cn.conac.rc.gateway.modules.ofs.vo.AnalzWarning;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags = "三定分析/权责相关接口", description = "清单信息")
@RequestMapping(value = "itm/")
public class ItmController
{
    @Autowired
    ItemService service;
    @Autowired
    AelProcessMng aelProcessMng;

    @ApiOperation(value = "根据职能职责标识查询权力清单", httpMethod = "GET", response = AnalzWarning.class, notes = "根据职能职责标识查询权力清单")
    @RequestMapping(value = "/duty/{dutyId}/aelItems", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> findAelItemByDutyCode(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "dutyId", required = true) @PathVariable("dutyId") String dutyId)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                service.findAelItemByDutyId(dutyId)), HttpStatus.OK);
    }

    @ApiOperation(value = "根据清单id查询权力清单", httpMethod = "GET", response = AnalzWarning.class, notes = "根据清单id查询权力清单")
    @RequestMapping(value = "/itm/aelItem/{id}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> AelItemDetail(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "id", required = true) @PathVariable("id") String id)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                service.AelItemDetail(id)), HttpStatus.OK);
    }

    @ApiOperation(value = "根据清单id查询责任清单详情(APP)", httpMethod = "GET", response = AnalzWarning.class, notes = "根据清单id查询责任清单详情(APP)")
    @RequestMapping(value = "/itm/delItemApp/{id}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> DelItemDetailApp(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "id", required = true) @PathVariable("id") String id)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                service.DelItemDetailApp(id)), HttpStatus.OK);
    }

    @ApiOperation(value = "根据清单id查询责任清单", httpMethod = "GET", response = AnalzWarning.class, notes = "根据清单id查询责任清单")
    @RequestMapping(value = "/itm/delItem/{id}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> DelItemDetail(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "id", required = true) @PathVariable("id") String id)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                service.DelItemDetail(id)), HttpStatus.OK);
    }

    @ApiOperation(value = "根据职能职责标识查询权力清单", httpMethod = "GET", response = AnalzWarning.class, notes = "根据职能职责标识查询权力清单")
    @RequestMapping(value = "/duty/{dutyId}/delItems", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> findDelItemByDutyId(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "dutyId", required = true) @PathVariable("dutyId") String dutyId)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                service.findDelItemByDutyId(dutyId)), HttpStatus.OK);
    }

    @ApiOperation(value = "根据机构ID以及权力类别查询权力清单列表，分页(APP)", httpMethod = "POST", response = AnalzWarning.class, notes = "根据机构ID查询权力清单列表，分页(APP)")
    @RequestMapping(value = "/itm/aelItem/list", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> findAelItemByOrgId(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody JSONObject aelItemVo)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                service.findAelItemByOrgId(aelItemVo)), HttpStatus.OK);
    }

    @ApiOperation(value = "根据清单id查询权力清单详情(APP)", httpMethod = "GET", response = AnalzWarning.class, notes = "根据清单id查询权力清单详情(APP)")
    @RequestMapping(value = "/itm/aelItemDeploied/{id}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> aelItemDeploiedDetail(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "id", required = true) @PathVariable("id") String id)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                service.aelItemDeploiedDetail(id)), HttpStatus.OK);
    }

    @ApiOperation(value = "根据机构ID查询责任清单列表，分页(APP)", httpMethod = "POST", response = AnalzWarning.class, notes = "根据机构ID查询责任清单列表，分页(APP)")
    @RequestMapping(value = "/itm/delItem/list", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> findDelItemByOrgId(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody JSONObject aelItemVo)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                service.findDelItemByOrgId(aelItemVo)), HttpStatus.OK);
    }

    @ApiOperation(value = "权利事项新增", httpMethod = "POST", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/ael", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> aelSave(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "实体", required = true) @RequestBody AelItemVo aelItemVo)
    {
        //TODO 整理需要插入的数据
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                service.addAelItemsInfo(aelItemVo)), HttpStatus.OK);
    }

    @ApiOperation(value = "权利事项详情", httpMethod = "GET", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/ael/{id}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> aelId(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "清单id", required = true) @PathVariable String id)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                service.AelItemDetail(id)), HttpStatus.OK);
    }

    @ApiOperation(value = "我的事项", httpMethod = "POST", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/ael/list", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> aelList(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody AelItemVo aelItemVo)
    {
        if (aelItemVo.getPage() == null || aelItemVo.getSize() == null || StringUtils.isBlank(aelItemVo.getTab())) {
            return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_FAILURE, ResultPojo.MSG_FAILURE, null),
                    HttpStatus.OK);
        }
        //判断状态
        String itemStatus = StatusUtil.getQueryStatus(aelItemVo.getItemStatus(), aelItemVo.getTab());
        String operation = StatusUtil.getQueryReviewStatus(aelItemVo.getOperation(), aelItemVo.getTab());
        if (AelItemVo.Status.AUDIT_PASS.getStringValue().equals(itemStatus)
                && (AelItemVo.Operation.A.name() + "," + AelItemVo.Operation.C.name()).equals(operation))
        {
            return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                    service.findAelItemList(aelItemVo)), HttpStatus.OK);
        }
        aelItemVo.setItemStatus(itemStatus);
        aelItemVo.setOperation(operation);
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                service.findAelItemInfoList(aelItemVo)), HttpStatus.OK);
    }

    @ApiOperation(value = "主项目录新增修改", httpMethod = "POST", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/ael/catalogue", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> aelCatalogue(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody JSONObject aelItemVo)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "主项目录新增修改", httpMethod = "GET", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/ael/catalogue/{id}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> aelCatalogueById(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "目录id", required = true) @RequestBody JSONObject aelItemVo)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "删除权力清单", httpMethod = "GET", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/ael/items/{id}/delete", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> deLAelItems(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "目录id", required = true) @RequestBody JSONObject aelItemVo)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "主项目录新增修改", httpMethod = "GET", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/ael/{id}/catalogue", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> aelIdCatalogue(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "事项id", required = true) @RequestBody JSONObject aelItemVo)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "事项背景", httpMethod = "GET", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/ael/{id}/background", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> aelIdBackground(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "清单id", required = true) @PathVariable String id)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "基本信息", httpMethod = "GET", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/ael/{id}/itemInfo", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> aelIdItemInfo(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "清单id", required = true) @PathVariable String id)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "会同处室", httpMethod = "GET", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/{id}/org/{orgId}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> aelIdOrgId(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "事项id", required = true) @PathVariable String id,
            @ApiParam(value = "部门id", required = true) @PathVariable String orgId)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "审核列表", httpMethod = "POST", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/ael/review/list", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> aelReviewList(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody JSONObject aelItemVo)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "审核详情", httpMethod = "GET", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/ael/review/{id}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> aelReviewId(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody JSONObject aelItemVo)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "审核流程", httpMethod = "POST", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/ael/review/process", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> aelReviewProcess(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody AelItemVo aelItemVo)
    {
        //1.并返回流程是否结束标志
        boolean isTaskExist = aelProcessMng.isTaskExist(aelItemVo.getChildItemId(), aelItemVo.getSysType());
        if(!isTaskExist){
            //startProcess
        }
        //2.审批流水记录表添加审批记录



        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "申请材料库详情", httpMethod = "GET", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/material/{id}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> aelSave(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "申请材料库id", required = true) @PathVariable String id)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "申请材料库列表", httpMethod = "POST", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/material/list", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> materialList(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody JSONObject aelItemVo)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "申请材料库编辑", httpMethod = "POST", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/material", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> materialSave(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody JSONObject aelItemVo)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "办理规范标准库详情", httpMethod = "GET", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/standard/{id}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> standardId(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "办理规范标准库id", required = true) @PathVariable String id)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "办理规范标准库列表", httpMethod = "POST", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/standard/list", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> standardList(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody JSONObject aelItemVo)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }

    @ApiOperation(value = "办理规范标准库编辑", httpMethod = "POST", response = AnalzWarning.class, notes = "")
    @RequestMapping(value = "/standard", method = RequestMethod.POST)
    public ResponseEntity<ResultPojo> standard(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "查询条件对象", required = true) @RequestBody JSONObject aelItemVo)
    {
        return new ResponseEntity<ResultPojo>(new ResultPojo(ResultPojo.CODE_SUCCESS, ResultPojo.MSG_SUCCESS,
                null), HttpStatus.OK);
    }
}
