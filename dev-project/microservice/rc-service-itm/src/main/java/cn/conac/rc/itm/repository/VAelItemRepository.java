package cn.conac.rc.itm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.itm.entity.VAelItemEntity;
@Repository
public interface VAelItemRepository extends GenericDao<VAelItemEntity, Integer>{
	
	@Query(value = "select id, genaral_code, item_name,item_type, main_items, dept_code, subject_id,object_im_ids, own_org_id, basis_type, item_code,implement_basis,main_item_name, own_org_name, dept_name, subject_name, duty_code,remarks, duty_id from rcd_v_ael_item where duty_id =:duty_id", nativeQuery = true)
	List<VAelItemEntity> findByDutyCode(@Param("duty_id") String dutyId);
}
