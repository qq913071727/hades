package cn.conac.rc.itm.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.itm.entity.AelGeneralItemEntity;
import cn.conac.rc.framework.repository.GenericDao;

/**
 * AelGeneralItemRepository类
 *
 * @author repositoryCreator
 * @date 2017-01-09
 * @version 1.0
 */
@Repository
public interface AelGeneralItemRepository extends GenericDao<AelGeneralItemEntity, Integer> {
}
