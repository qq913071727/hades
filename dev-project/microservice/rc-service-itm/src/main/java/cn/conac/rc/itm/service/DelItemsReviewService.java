package cn.conac.rc.itm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.itm.entity.DelItemsReviewEntity;
import cn.conac.rc.itm.repository.DelItemsReviewRepository;
import cn.conac.rc.itm.vo.DelItemsReviewVo;
import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;

/**
 * DelItemsReviewService类
 *
 * @author serviceCreator
 * @date 2017-01-06
 * @version 1.0
 */
@Service
public class DelItemsReviewService extends GenericService<DelItemsReviewEntity, Integer> {

	@Autowired
	private DelItemsReviewRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(DelItemsReviewVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<DelItemsReviewEntity> list(DelItemsReviewVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<DelItemsReviewEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<DelItemsReviewEntity> createCriteria(DelItemsReviewVo param) {
		Criteria<DelItemsReviewEntity> dc = new Criteria<DelItemsReviewEntity>();
		// TODO 具体条件赋值

		return dc;
	}

}
