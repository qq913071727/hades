package cn.conac.rc.itm.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.itm.entity.VDelItemEntity;
import cn.conac.rc.itm.repository.VDelItemRepository;
@Service
public class VDelItemService extends GenericService<VDelItemEntity, Integer> {

	@Autowired
	private VDelItemRepository repository;

	public List<VDelItemEntity> findItemsByDutyId(String dutyId) {
		Criteria<VDelItemEntity> dc = new Criteria<VDelItemEntity>();
		if (StringUtils.isNotBlank(dutyId)) {
			dc.add(Restrictions.eq("dutyId", dutyId, true));
		}
		return repository.findAll(dc);
	}
}
