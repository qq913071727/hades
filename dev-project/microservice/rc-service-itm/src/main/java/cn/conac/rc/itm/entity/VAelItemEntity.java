package cn.conac.rc.itm.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.rc.framework.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@Entity
@Table(name="rcd_v_ael_item")
public class VAelItemEntity extends BaseEntity<VAelItemEntity> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -378537713158084824L;
	@ApiModelProperty("通用事项编码")
	
	private String genaralCode; // 通用事项编码
	@ApiModelProperty("事项名称")
	private String itemName; // 事项名称
	@ApiModelProperty("事项类型")
	private String itemType; // 事项类型
	@ApiModelProperty("主项标识")
	private String mainItems; // 主项标识
	@ApiModelProperty("责任处室")
	private String deptCode;  // 责任处室
	@ApiModelProperty("职责id")
	private String dutyId;  // 责任处室
	@ApiModelProperty("实施主体")
	private String subjectId;  // 实施主体
	@ApiModelProperty("实施对象")
	private String objectImIds;// 实施对象
	@ApiModelProperty("所属部门")
	private String ownOrgId;// 所属部门
	@ApiModelProperty("实施依据类型")
	private String basisType;// 实施依据类型
	@ApiModelProperty("行政权力编号")
	private String itemCode;// 行政权力编号
	@ApiModelProperty("实施依据 ")
	private String implementBasis;// 实施依据 
//	@ApiModelProperty("区划")
//	private String areaCode; // 区划
//	@ApiModelProperty("区划")
//	private String areaId; // 区划
	@ApiModelProperty("职责编码")
	private String dutyCode; // 区划
	@ApiModelProperty("职责编码")
	private String mainItemName; // 区划
	@ApiModelProperty("职责编码")
	private String ownOrgName; // 区划
	@ApiModelProperty("职责编码")
	private String deptName; // 区划
	@ApiModelProperty("职责编码")
	private String subjectName; // 区划
	@ApiModelProperty("备注")
	private String remarks; // 区划
	@ApiModelProperty("承诺时限")
	private String promiseTotalTerm;
	
	public String getPromiseTotalTerm() {
		return promiseTotalTerm;
	}
	public void setPromiseTotalTerm(String promiseTotalTerm) {
		this.promiseTotalTerm = promiseTotalTerm;
	}
	public String getDutyId() {
		return dutyId;
	}
	public void setDutyId(String dutyId) {
		this.dutyId = dutyId;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getMainItems() {
		return mainItems;
	}
	public void setMainItems(String mainItems) {
		this.mainItems = mainItems;
	}
	public String getMainItemName() {
		return mainItemName;
	}
	public void setMainItemName(String mainItemName) {
		this.mainItemName = mainItemName;
	}
	public String getOwnOrgName() {
		return ownOrgName;
	}
	public void setOwnOrgName(String ownOrgName) {
		this.ownOrgName = ownOrgName;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getDutyCode() {
		return dutyCode;
	}
	public void setDutyCode(String dutyCode) {
		this.dutyCode = dutyCode;
	}
	public String getDeptCode() {
		return deptCode;
	}
	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}
//	public String getAreaCode() {
//		return areaCode;
//	}
//	public void setAreaCode(String areaCode) {
//		this.areaCode = areaCode;
//	}
//	public String getAreaId() {
//		return areaId;
//	}
//	public void setAreaId(String areaId) {
//		this.areaId = areaId;
//	}
	public String getGenaralCode() {
		return genaralCode;
	}
	public void setGenaralCode(String genaralCode) {
		this.genaralCode = genaralCode;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public String getMainItemId() {
		return mainItems;
	}
	public void setMainItemId(String mainItems) {
		this.mainItems = mainItems;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getObjectImIds() {
		return objectImIds;
	}
	public void setObjectImIds(String objectImIds) {
		this.objectImIds = objectImIds;
	}
	public String getOwnOrgId() {
		return ownOrgId;
	}
	public void setOwnOrgId(String ownOrgId) {
		this.ownOrgId = ownOrgId;
	}
	public String getBasisType() {
		return basisType;
	}
	public void setBasisType(String basisType) {
		this.basisType = basisType;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getImplementBasis() {
		return implementBasis;
	}
	public void setImplementBasis(String implementBasis) {
		this.implementBasis = implementBasis;
	}
}
