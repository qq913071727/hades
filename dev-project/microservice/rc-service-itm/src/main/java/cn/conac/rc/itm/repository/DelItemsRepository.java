package cn.conac.rc.itm.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.itm.entity.DelItemsEntity;
import cn.conac.rc.framework.repository.GenericDao;

/**
 * DelItemsRepository类
 *
 * @author repositoryCreator
 * @date 2017-01-06
 * @version 1.0
 */
@Repository
public interface DelItemsRepository extends GenericDao<DelItemsEntity, Integer> {
}
