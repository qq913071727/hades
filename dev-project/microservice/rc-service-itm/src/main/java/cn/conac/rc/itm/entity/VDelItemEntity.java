package cn.conac.rc.itm.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.rc.framework.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@Entity
@Table(name="rcd_v_del_item")
public class VDelItemEntity extends BaseEntity<VDelItemEntity> implements Serializable {
	private static final long serialVersionUID = 3535112255934939732L;
	
	@ApiModelProperty("行政责任编码")
	private String dutyCode;
	@ApiModelProperty("行政责任名称")
	private String dutyContent;
	@ApiModelProperty("权力标识")
	private String itemsInfoId;
	@ApiModelProperty("责任依据")
	private String dutyTerms;
	@ApiModelProperty("追责情形")
	private String traceObjects ;
	@ApiModelProperty("关联权力名称")
	private String aelName;
	@ApiModelProperty("所属单位名称")
	private String orgName;
	@ApiModelProperty("职能职责标识")
	private String dutyId;
	
	public String getDutyId() {
		return dutyId;
	}
	public void setDutyId(String dutyId) {
		this.dutyId = dutyId;
	}
	public String getDutyCode() {
		return dutyCode;
	}
	public void setDutyCode(String dutyCode) {
		this.dutyCode = dutyCode;
	}
	public String getDutyContent() {
		return dutyContent;
	}
	public void setDutyContent(String dutyContent) {
		this.dutyContent = dutyContent;
	}
	public String getItemsInfoId() {
		return itemsInfoId;
	}
	public void setItemsInfoId(String itemsInfoId) {
		this.itemsInfoId = itemsInfoId;
	}
	public String getDutyTerms() {
		return dutyTerms;
	}
	public void setDutyTerms(String dutyTerms) {
		this.dutyTerms = dutyTerms;
	}
	public String getTraceObjects() {
		return traceObjects;
	}
	public void setTraceObjects(String traceObjects) {
		this.traceObjects = traceObjects;
	}
	public String getAelName() {
		return aelName;
	}
	public void setAelName(String aelName) {
		this.aelName = aelName;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

}
