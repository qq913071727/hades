package cn.conac.rc.itm.entity;

import java.io.Serializable;
import javax.persistence.Id;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DelItemsDeptDutyEntity类
 *
 * @author beanCreator
 * @date 2017-01-06
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="DEL_ITEMS_DEPT_DUTY")
public class DelItemsDeptDutyEntity implements Serializable {

	private static final long serialVersionUID = 1483683626896400375L;
	@Id
	@ApiModelProperty("dutyItemsBaseId")
	private Integer dutyItemsBaseId;

	@ApiModelProperty("deptDutyId")
	private Integer deptDutyId;

	public void setDutyItemsBaseId(Integer dutyItemsBaseId){
		this.dutyItemsBaseId=dutyItemsBaseId;
	}

	public Integer getDutyItemsBaseId(){
		return dutyItemsBaseId;
	}

	public void setDeptDutyId(Integer deptDutyId){
		this.deptDutyId=deptDutyId;
	}

	public Integer getDeptDutyId(){
		return deptDutyId;
	}

}
