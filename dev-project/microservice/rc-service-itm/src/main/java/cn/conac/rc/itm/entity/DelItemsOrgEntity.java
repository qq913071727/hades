package cn.conac.rc.itm.entity;

import java.io.Serializable;
import javax.persistence.Id;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DelItemsOrgEntity类
 *
 * @author beanCreator
 * @date 2017-01-06
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="DEL_ITEMS_ORG")
public class DelItemsOrgEntity implements Serializable {

	private static final long serialVersionUID = 1483683626935932711L;
	@Id
	@ApiModelProperty("dutyItemsBaseId")
	private Integer dutyItemsBaseId;

	@ApiModelProperty("orgId")
	private Integer orgId;

	public void setDutyItemsBaseId(Integer dutyItemsBaseId){
		this.dutyItemsBaseId=dutyItemsBaseId;
	}

	public Integer getDutyItemsBaseId(){
		return dutyItemsBaseId;
	}

	public void setOrgId(Integer orgId){
		this.orgId=orgId;
	}

	public Integer getOrgId(){
		return orgId;
	}

}
