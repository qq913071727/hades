package cn.conac.rc.itm.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.itm.entity.AelItemBasisEntity;

/**
 * AelItemBasisRepository类
 *
 * @author repositoryCreator
 * @date 2017-06-19
 * @version 1.0
 */
@Repository
public interface AelItemBasisRepository extends GenericDao<AelItemBasisEntity, Integer> {
	
	/**
	 * findByPid
	 * @param pid
	 * @return
	 */
	List<AelItemBasisEntity> findByPid(Integer pid);
}
