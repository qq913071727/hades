package cn.conac.rc.itm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AelItemsOrgEntity类
 *
 * @author beanCreator
 * @date 2017-01-09
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="AEL_ITEMS_ORG")
public class AelItemsOrgEntity implements Serializable {

	private static final long serialVersionUID = 148392736294070372L;
	@Id
	@ApiModelProperty("itemId")
	private Integer itemId;

	@ApiModelProperty("orgId")
	private Integer orgId;

	public void setItemId(Integer itemId){
		this.itemId=itemId;
	}

	public Integer getItemId(){
		return itemId;
	}

	public void setOrgId(Integer orgId){
		this.orgId=orgId;
	}

	public Integer getOrgId(){
		return orgId;
	}

}
