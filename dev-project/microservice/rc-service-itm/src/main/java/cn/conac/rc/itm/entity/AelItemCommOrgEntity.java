package cn.conac.rc.itm.entity;

import java.io.Serializable;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import cn.conac.rc.framework.entity.DataEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 共同实施主体
 * @author wanghm
 *
 */
@ApiModel
@Immutable
@Subselect("select item_id,org_Name, org_id from(select t1.item_id, t4.name org_name, t4.id org_id from ael_items_info  t3 inner join ael_items_org t1 on t3.id = t1.item_id inner join ofs_organization t2 on t1.org_id = t2.id inner join ofs_organization_base t4 on t2.base_id = t4.id)")
public class AelItemCommOrgEntity  extends DataEntity<AelItemCommOrgEntity> implements Serializable{

	private static final long serialVersionUID = 8618039374003470194L;
	@ApiModelProperty("事项标识")
	private String itemId;
	@ApiModelProperty("部门标识")
	private String orgId;
	@ApiModelProperty("部门名称")
	private String orgName;
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
}
