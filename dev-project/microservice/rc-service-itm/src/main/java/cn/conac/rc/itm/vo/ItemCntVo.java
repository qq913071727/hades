package cn.conac.rc.itm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel
public class ItemCntVo implements Serializable {

	private static final long serialVersionUID = 1472903587588453800L;

	@ApiModelProperty("权力清单-行政许可-数量")
	private long aelItemCnt0;

	@ApiModelProperty("权力清单-非行政许可(非'9+1'项目)-数量")
	private long aelItemCnt1;

	@ApiModelProperty("权力清单-行政处罚-数量")
	private long aelItemCnt2;

	@ApiModelProperty("权力清单-行政强制-数量")
	private long aelItemCnt3;

	@ApiModelProperty("权力清单-行政征收-数量")
	private long aelItemCnt4;

	@ApiModelProperty("权力清单-行政给付-数量")
	private long aelItemCnt5;

	@ApiModelProperty("权力清单-行政检查-数量")
	private long aelItemCnt6;

	@ApiModelProperty("权力清单-行政确认-数量")
	private long aelItemCnt7;

	@ApiModelProperty("权力清单-行政奖励-数量")
	private long aelItemCnt8;

	@ApiModelProperty("权力清单-行政裁决-数量")
	private long aelItemCnt9;

	@ApiModelProperty("权力清单-其他类别-数量")
	private long aelItemCnt10;

	@ApiModelProperty("权力清单-总数量")
	private long aelItemTotalCnt;

	@ApiModelProperty("权力清单-子项目的总数量")
	private long aelItemTotalChildCnt;

	@ApiModelProperty("责任清单-总数量")
	private long delItemTotalCnt;

	public long getAelItemCnt0() {
		return aelItemCnt0;
	}

	public void setAelItemCnt0(long aelItemCnt0) {
		this.aelItemCnt0 = aelItemCnt0;
	}

	public long getAelItemCnt1() {
		return aelItemCnt1;
	}

	public void setAelItemCnt1(long aelItemCnt1) {
		this.aelItemCnt1 = aelItemCnt1;
	}

	public long getAelItemCnt2() {
		return aelItemCnt2;
	}

	public void setAelItemCnt2(long aelItemCnt2) {
		this.aelItemCnt2 = aelItemCnt2;
	}

	public long getAelItemCnt3() {
		return aelItemCnt3;
	}

	public void setAelItemCnt3(long aelItemCnt3) {
		this.aelItemCnt3 = aelItemCnt3;
	}

	public long getAelItemCnt4() {
		return aelItemCnt4;
	}

	public void setAelItemCnt4(long aelItemCnt4) {
		this.aelItemCnt4 = aelItemCnt4;
	}

	public long getAelItemCnt5() {
		return aelItemCnt5;
	}

	public void setAelItemCnt5(long aelItemCnt5) {
		this.aelItemCnt5 = aelItemCnt5;
	}

	public long getAelItemCnt6() {
		return aelItemCnt6;
	}

	public void setAelItemCnt6(long aelItemCnt6) {
		this.aelItemCnt6 = aelItemCnt6;
	}

	public long getAelItemCnt7() {
		return aelItemCnt7;
	}

	public void setAelItemCnt7(long aelItemCnt7) {
		this.aelItemCnt7 = aelItemCnt7;
	}

	public long getAelItemCnt8() {
		return aelItemCnt8;
	}

	public void setAelItemCnt8(long aelItemCnt8) {
		this.aelItemCnt8 = aelItemCnt8;
	}

	public long getAelItemCnt9() {
		return aelItemCnt9;
	}

	public void setAelItemCnt9(long aelItemCnt9) {
		this.aelItemCnt9 = aelItemCnt9;
	}

	public long getAelItemCnt10() {
		return aelItemCnt10;
	}

	public void setAelItemCnt10(long aelItemCnt10) {
		this.aelItemCnt10 = aelItemCnt10;
	}

	public long getAelItemTotalCnt() {
		return aelItemTotalCnt;
	}

	public void setAelItemTotalCnt(long aelItemTotalCnt) {
		this.aelItemTotalCnt = aelItemTotalCnt;
	}

	public long getAelItemTotalChildCnt() {
		return aelItemTotalChildCnt;
	}

	public void setAelItemTotalChildCnt(long aelItemTotalChildCnt) {
		this.aelItemTotalChildCnt = aelItemTotalChildCnt;
	}

	public long getDelItemTotalCnt() {
		return delItemTotalCnt;
	}

	public void setDelItemTotalCnt(long delItemTotalCnt) {
		this.delItemTotalCnt = delItemTotalCnt;
	}
}
