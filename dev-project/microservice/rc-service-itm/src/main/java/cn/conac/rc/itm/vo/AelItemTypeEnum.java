package cn.conac.rc.itm.vo;
/** * 
 * 软件著作权：	政府和公益机构域名注册管理中心  
 * 功能： 事项9+x类型
 * @author  wanghm 
 * @date  2016年5月25日 上午9:08:49  
 * @version 1.0   
 */
public enum AelItemTypeEnum {
	FXZXK(1, "非行政许可"), XZXK(0, "行政许可"), XZCF(2,"行政处罚"), XZQZ(3,"行政强制"),
	XZZS(4,"行政征收"), XZJF(5,"行政给付"),XZJC(6,"行政检查"), XZQR(7,"行政确认"),
	XZJL(8,"行政奖励"), XZCJ(9,"行政裁决"),QTLB(10, "其他类别");
	private AelItemTypeEnum(int typeValue, String name){
		this.typeValue = typeValue;
		this.name = name;
	}
	
	public int typeValue;
	public String name;
	
//	public static void main(String args[]){
//		System.out.println(FXZXK.name+":"+escape(FXZXK.name));
//		System.out.println(XZXK.name+":"+escape(XZXK.name));
//		System.out.println(XZCF.name+":"+escape(XZCF.name));
//		System.out.println(XZQZ.name+":"+escape(XZQZ.name));
//		System.out.println(XZZS.name+":"+escape(XZZS.name));
//		System.out.println(XZJF.name+":"+escape(XZJF.name));
//		System.out.println(XZJC.name+":"+escape(XZJC.name));
//		System.out.println(XZQR.name+":"+escape(XZQR.name));
//		System.out.println(XZJL.name+":"+escape(XZJL.name));
//		System.out.println(XZCJ.name+":"+escape(XZCJ.name));
//		System.out.println(QTLB.name+":"+escape(QTLB.name));
//	}
	
	public static String escape(String s)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i ++)
        {
        	 char c = s.charAt(i);
             
             // 转换为unicode
             sb.append("\\u" + Integer.toHexString(c));
        }
        return sb.toString();
    }
}
