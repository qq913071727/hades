package cn.conac.rc.itm.entity;

import java.io.Serializable;
import javax.persistence.Id;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DelItemsEntity类
 *
 * @author beanCreator
 * @date 2017-01-06
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="DEL_ITEMS")
public class DelItemsEntity implements Serializable {

	private static final long serialVersionUID = 1483683626650721334L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("baseId")
	private Integer baseId;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setBaseId(Integer baseId){
		this.baseId=baseId;
	}

	public Integer getBaseId(){
		return baseId;
	}

}
