package cn.conac.rc.itm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AelItemUpdateEntity类
 *
 * @author beanCreator
 * @date 2017-01-09
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="AEL_ITEM_UPDATE")
public class AelItemUpdateEntity implements Serializable {

	private static final long serialVersionUID = 1483927362845402999L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("updateType")
	private String updateType;

	@ApiModelProperty("cancelType")
	private String cancelType;

	@ApiModelProperty("description")
	private String description;

	@ApiModelProperty("fileUrl")
	private String fileUrl;

	@ApiModelProperty("createDate")
	private Date createDate;

	@ApiModelProperty("itemId")
	private Integer itemId;

	@ApiModelProperty("cancelDate")
	private Date cancelDate;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setUpdateType(String updateType){
		this.updateType=updateType;
	}

	public String getUpdateType(){
		return updateType;
	}

	public void setCancelType(String cancelType){
		this.cancelType=cancelType;
	}

	public String getCancelType(){
		return cancelType;
	}

	public void setDescription(String description){
		this.description=description;
	}

	public String getDescription(){
		return description;
	}

	public void setFileUrl(String fileUrl){
		this.fileUrl=fileUrl;
	}

	public String getFileUrl(){
		return fileUrl;
	}

	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}

	public Date getCreateDate(){
		return createDate;
	}

	public void setItemId(Integer itemId){
		this.itemId=itemId;
	}

	public Integer getItemId(){
		return itemId;
	}

	public void setCancelDate(Date cancelDate){
		this.cancelDate=cancelDate;
	}

	public Date getCancelDate(){
		return cancelDate;
	}

}
