package cn.conac.rc.itm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DelItemsAttachmentsEntity类
 *
 * @author beanCreator
 * @date 2017-01-06
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="DEL_ITEMS_ATTACHMENTS")
public class DelItemsAttachmentsEntity implements Serializable {

	private static final long serialVersionUID = 1483683626857529174L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("masterId")
	private Integer masterId;

	@ApiModelProperty("masterTableName")
	private String masterTableName;

	@ApiModelProperty("fileUrl")
	private String fileUrl;

	@ApiModelProperty("position")
	private Integer position;

	@ApiModelProperty("fileName")
	private String fileName;

	@ApiModelProperty("fileExtName")
	private String fileExtName;

	@ApiModelProperty("fileSize")
	private Integer fileSize;

	@ApiModelProperty("createdDate")
	private Date createdDate;

	@ApiModelProperty("valid")
	private Integer valid;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setMasterId(Integer masterId){
		this.masterId=masterId;
	}

	public Integer getMasterId(){
		return masterId;
	}

	public void setMasterTableName(String masterTableName){
		this.masterTableName=masterTableName;
	}

	public String getMasterTableName(){
		return masterTableName;
	}

	public void setFileUrl(String fileUrl){
		this.fileUrl=fileUrl;
	}

	public String getFileUrl(){
		return fileUrl;
	}

	public void setPosition(Integer position){
		this.position=position;
	}

	public Integer getPosition(){
		return position;
	}

	public void setFileName(String fileName){
		this.fileName=fileName;
	}

	public String getFileName(){
		return fileName;
	}

	public void setFileExtName(String fileExtName){
		this.fileExtName=fileExtName;
	}

	public String getFileExtName(){
		return fileExtName;
	}

	public void setFileSize(Integer fileSize){
		this.fileSize=fileSize;
	}

	public Integer getFileSize(){
		return fileSize;
	}

	public void setCreatedDate(Date createdDate){
		this.createdDate=createdDate;
	}

	public Date getCreatedDate(){
		return createdDate;
	}

	public void setValid(Integer valid){
		this.valid=valid;
	}

	public Integer getValid(){
		return valid;
	}

}
