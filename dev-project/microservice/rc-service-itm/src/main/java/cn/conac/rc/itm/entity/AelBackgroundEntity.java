package cn.conac.rc.itm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AelBackgroundEntity类
 *
 * @author beanCreator
 * @date 2017-01-09
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="AEL_BACKGROUND")
public class AelBackgroundEntity implements Serializable {

	private static final long serialVersionUID = 148392736264017635L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("description")
	private String description;

	@ApiModelProperty("type")
	private String type;

	@ApiModelProperty("fileUrl")
	private String fileUrl;

	@ApiModelProperty("codeCombin")
	private String codeCombin;

	@ApiModelProperty("codeSplit")
	private String codeSplit;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setDescription(String description){
		this.description=description;
	}

	public String getDescription(){
		return description;
	}

	public void setType(String type){
		this.type=type;
	}

	public String getType(){
		return type;
	}

	public void setFileUrl(String fileUrl){
		this.fileUrl=fileUrl;
	}

	public String getFileUrl(){
		return fileUrl;
	}

	public void setCodeCombin(String codeCombin){
		this.codeCombin=codeCombin;
	}

	public String getCodeCombin(){
		return codeCombin;
	}

	public void setCodeSplit(String codeSplit){
		this.codeSplit=codeSplit;
	}

	public String getCodeSplit(){
		return codeSplit;
	}

}
