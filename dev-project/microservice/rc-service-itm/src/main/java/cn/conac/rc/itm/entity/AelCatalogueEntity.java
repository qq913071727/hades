package cn.conac.rc.itm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AelCatalogueEntity类
 *
 * @author beanCreator
 * @date 2017-01-09
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="AEL_CATALOGUE")
public class AelCatalogueEntity implements Serializable {

	private static final long serialVersionUID = 1483927362772256836L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("genaralCode")
	private String genaralCode;

	@ApiModelProperty("itemName")
	private String itemName;

	@ApiModelProperty("itemType")
	private String itemType;

	@ApiModelProperty("objectIm")
	private Integer objectIm;

	@ApiModelProperty("ownOrgId")
	private Integer ownOrgId;

	@ApiModelProperty("createTime")
	private Date createTime;

	@ApiModelProperty("remarks")
	private String remarks;

	@ApiModelProperty("users")
	private Integer users;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setGenaralCode(String genaralCode){
		this.genaralCode=genaralCode;
	}

	public String getGenaralCode(){
		return genaralCode;
	}

	public void setItemName(String itemName){
		this.itemName=itemName;
	}

	public String getItemName(){
		return itemName;
	}

	public void setItemType(String itemType){
		this.itemType=itemType;
	}

	public String getItemType(){
		return itemType;
	}

	public void setObjectIm(Integer objectIm){
		this.objectIm=objectIm;
	}

	public Integer getObjectIm(){
		return objectIm;
	}

	public void setOwnOrgId(Integer ownOrgId){
		this.ownOrgId=ownOrgId;
	}

	public Integer getOwnOrgId(){
		return ownOrgId;
	}

	public void setCreateTime(Date createTime){
		this.createTime=createTime;
	}

	public Date getCreateTime(){
		return createTime;
	}

	public void setRemarks(String remarks){
		this.remarks=remarks;
	}

	public String getRemarks(){
		return remarks;
	}

	public void setUsers(Integer users){
		this.users=users;
	}

	public Integer getUsers(){
		return users;
	}

}
