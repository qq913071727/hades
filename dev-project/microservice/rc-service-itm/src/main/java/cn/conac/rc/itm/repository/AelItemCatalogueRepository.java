package cn.conac.rc.itm.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.itm.entity.AelItemCatalogueEntity;

@Repository
public interface AelItemCatalogueRepository extends GenericDao<AelItemCatalogueEntity, Integer>{
}
