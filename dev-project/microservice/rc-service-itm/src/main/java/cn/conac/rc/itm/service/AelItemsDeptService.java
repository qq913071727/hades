package cn.conac.rc.itm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.itm.entity.AelItemsDeptEntity;
import cn.conac.rc.itm.repository.AelItemsDeptRepository;
import cn.conac.rc.itm.vo.AelItemsDeptVo;
import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;

/**
 * AelItemsDeptService类
 *
 * @author serviceCreator
 * @date 2017-01-09
 * @version 1.0
 */
@Service
public class AelItemsDeptService extends GenericService<AelItemsDeptEntity, Integer> {

	@Autowired
	private AelItemsDeptRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(AelItemsDeptVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<AelItemsDeptEntity> list(AelItemsDeptVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<AelItemsDeptEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<AelItemsDeptEntity> createCriteria(AelItemsDeptVo param) {
		Criteria<AelItemsDeptEntity> dc = new Criteria<AelItemsDeptEntity>();
		// TODO 具体条件赋值

		return dc;
	}

}
