package cn.conac.rc.itm.entity;

import java.io.Serializable;
import javax.persistence.Id;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DelItemsReviewEntity类
 *
 * @author beanCreator
 * @date 2017-01-06
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="DEL_ITEMS_REVIEW")
public class DelItemsReviewEntity implements Serializable {

	private static final long serialVersionUID = 1483683626950428384L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("dutyItemsId")
	private Integer dutyItemsId;

	@ApiModelProperty("reviewUser")
	private Integer reviewUser;

	@ApiModelProperty("reviewDate")
	private Date reviewDate;

	@ApiModelProperty("reviewStatus")
	private Integer reviewStatus;

	@ApiModelProperty("reviewOpinion")
	private String reviewOpinion;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setDutyItemsId(Integer dutyItemsId){
		this.dutyItemsId=dutyItemsId;
	}

	public Integer getDutyItemsId(){
		return dutyItemsId;
	}

	public void setReviewUser(Integer reviewUser){
		this.reviewUser=reviewUser;
	}

	public Integer getReviewUser(){
		return reviewUser;
	}

	public void setReviewDate(Date reviewDate){
		this.reviewDate=reviewDate;
	}

	public Date getReviewDate(){
		return reviewDate;
	}

	public void setReviewStatus(Integer reviewStatus){
		this.reviewStatus=reviewStatus;
	}

	public Integer getReviewStatus(){
		return reviewStatus;
	}

	public void setReviewOpinion(String reviewOpinion){
		this.reviewOpinion=reviewOpinion;
	}

	public String getReviewOpinion(){
		return reviewOpinion;
	}

}
