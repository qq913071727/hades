package cn.conac.rc.itm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AelGeneralItemEntity类
 *
 * @author beanCreator
 * @date 2017-01-09
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="AEL_GENERAL_ITEM")
public class AelGeneralItemEntity implements Serializable {

	private static final long serialVersionUID = 1483927362794291869L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("generalCode")
	private String generalCode;

	@ApiModelProperty("itemCode")
	private String itemCode;

	@ApiModelProperty("itemBasisType")
	private String itemBasisType;

	@ApiModelProperty("itemBasis")
	private String itemBasis;

	@ApiModelProperty("generalName")
	private String generalName;

	@ApiModelProperty("appObject")
	private String appObject;

	@ApiModelProperty("directOrg")
	private String directOrg;

	@ApiModelProperty("appOrg")
	private String appOrg;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setGeneralCode(String generalCode){
		this.generalCode=generalCode;
	}

	public String getGeneralCode(){
		return generalCode;
	}

	public void setItemCode(String itemCode){
		this.itemCode=itemCode;
	}

	public String getItemCode(){
		return itemCode;
	}

	public void setItemBasisType(String itemBasisType){
		this.itemBasisType=itemBasisType;
	}

	public String getItemBasisType(){
		return itemBasisType;
	}

	public void setItemBasis(String itemBasis){
		this.itemBasis=itemBasis;
	}

	public String getItemBasis(){
		return itemBasis;
	}

	public void setGeneralName(String generalName){
		this.generalName=generalName;
	}

	public String getGeneralName(){
		return generalName;
	}

	public void setAppObject(String appObject){
		this.appObject=appObject;
	}

	public String getAppObject(){
		return appObject;
	}

	public void setDirectOrg(String directOrg){
		this.directOrg=directOrg;
	}

	public String getDirectOrg(){
		return directOrg;
	}

	public void setAppOrg(String appOrg){
		this.appOrg=appOrg;
	}

	public String getAppOrg(){
		return appOrg;
	}

}
