package cn.conac.rc.itm.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import cn.conac.rc.framework.entity.DataEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * 责任清单责任处室
 * @author wanghm
 *
 */
@ApiModel
@Immutable
@Subselect("select item_id, dept_name, dept_id from(select t3.id item_id, t2.id dept_id, t2.dept_Name dept_name from del_items_info  t3 inner join del_items_dept t1 on t3.id = t1.duty_items_base_id inner join ofs_department t2 on t1.dept_id = t2.id)")
public class DelItemDutyDeptEntity extends DataEntity<DelItemDutyDeptEntity> implements Serializable {

	private static final long serialVersionUID = 52459478244927731L;
	@ApiModelProperty("内设标识")
	private String deptId;
	@ApiModelProperty("内设名称")
	private String deptName;
	@ApiModelProperty("事项标识")
	private String itemId;
	
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
}
