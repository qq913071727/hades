package cn.conac.rc.itm.entity;

import java.io.Serializable;
import javax.persistence.Id;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DelItemsChangeReasonEntity类
 *
 * @author beanCreator
 * @date 2017-01-06
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="DEL_ITEMS_CHANGE_REASON")
public class DelItemsChangeReasonEntity implements Serializable {

	private static final long serialVersionUID = 1483683626871633421L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("changeReason")
	private String changeReason;

	@ApiModelProperty("changeDate")
	private Date changeDate;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setChangeReason(String changeReason){
		this.changeReason=changeReason;
	}

	public String getChangeReason(){
		return changeReason;
	}

	public void setChangeDate(Date changeDate){
		this.changeDate=changeDate;
	}

	public Date getChangeDate(){
		return changeDate;
	}

}
