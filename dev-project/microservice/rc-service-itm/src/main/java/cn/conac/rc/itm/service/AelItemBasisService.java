package cn.conac.rc.itm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.itm.entity.AelItemBasisEntity;
import cn.conac.rc.itm.repository.AelItemBasisRepository;

/**
 * AelItemBasisService类
 *
 * @author serviceCreator
 * @date 2017-06-19
 * @version 1.0
 */
@Service
public class AelItemBasisService extends GenericService<AelItemBasisEntity, Integer> {

	@Autowired
	private AelItemBasisRepository repository;

	/**
	 * 查询所用实施依据类型信息
	 * @return List<AelItemBasisEntity>
	 * @throws Exception
	 */
	public List<AelItemBasisEntity> findAllList() throws Exception {
		return repository.findAll();
	}
	
	/**
	 * 根据Pid查询实施依据类型信息
	 * @return List<AelItemBasisEntity>
	 * @throws Exception
	 */
	public List<AelItemBasisEntity> findByPid(Integer pid) throws Exception {
		return repository.findByPid(pid);
	}
	
}
