package cn.conac.rc.itm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.itm.entity.AelItemsDutyEntity;
import cn.conac.rc.itm.repository.AelItemsDutyRepository;
import cn.conac.rc.itm.vo.AelItemsDutyVo;
import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;

/**
 * AelItemsDutyService类
 *
 * @author serviceCreator
 * @date 2017-01-09
 * @version 1.0
 */
@Service
public class AelItemsDutyService extends GenericService<AelItemsDutyEntity, Integer> {

	@Autowired
	private AelItemsDutyRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(AelItemsDutyVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<AelItemsDutyEntity> list(AelItemsDutyVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<AelItemsDutyEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<AelItemsDutyEntity> createCriteria(AelItemsDutyVo param) {
		Criteria<AelItemsDutyEntity> dc = new Criteria<AelItemsDutyEntity>();
		// TODO 具体条件赋值

		return dc;
	}

}
