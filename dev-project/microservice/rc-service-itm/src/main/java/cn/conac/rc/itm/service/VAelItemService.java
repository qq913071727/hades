package cn.conac.rc.itm.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.itm.entity.VAelItemEntity;
import cn.conac.rc.itm.repository.VAelItemRepository;
@Service
public class VAelItemService extends GenericService<VAelItemEntity, Integer> {

	@Autowired
	private VAelItemRepository repository;

	public List<VAelItemEntity> findItemsByDutyId(String dutyId) {
		Criteria<VAelItemEntity> dc = new Criteria<VAelItemEntity>();
		if (StringUtils.isNotBlank(dutyId)) {
			dc.add(Restrictions.eq("dutyId", dutyId, true));
		}
		return repository.findAll(dc);
	}
}
