package cn.conac.rc.itm.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AelItemBasisEntity类
 *
 * @author beanCreator
 * @date 2017-06-19
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="AEL_ITEM_BASIS")
public class AelItemBasisEntity implements Serializable {

	private static final long serialVersionUID = 149785910616916835L;

	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("pid")
	private Integer pid;

	@ApiModelProperty("name")
	private String name;

	@ApiModelProperty("open")
	private String open;

	@ApiModelProperty("perm")
	private String perm;

	@ApiModelProperty("leaf")
	private Integer leaf;

	@ApiModelProperty("imgpath")
	private String imgpath;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setPid(Integer pid){
		this.pid=pid;
	}

	public Integer getPid(){
		return pid;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setOpen(String open){
		this.open=open;
	}

	public String getOpen(){
		return open;
	}

	public void setPerm(String perm){
		this.perm=perm;
	}

	public String getPerm(){
		return perm;
	}

	public void setLeaf(Integer leaf){
		this.leaf=leaf;
	}

	public Integer getLeaf(){
		return leaf;
	}

	public void setImgpath(String imgpath){
		this.imgpath=imgpath;
	}

	public String getImgpath(){
		return imgpath;
	}

}
