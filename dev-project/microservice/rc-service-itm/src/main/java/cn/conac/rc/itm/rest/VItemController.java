package cn.conac.rc.itm.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.utils.StringUtils;
import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.itm.entity.AelItemCatalogueEntity;
import cn.conac.rc.itm.entity.AelItemDeploiedEntity;
import cn.conac.rc.itm.entity.DelItemAppEntity;
import cn.conac.rc.itm.entity.VAelItemEntity;
import cn.conac.rc.itm.entity.VDelItemEntity;
import cn.conac.rc.itm.service.AelItemCatalogueService;
import cn.conac.rc.itm.service.AelItemDeploiedService;
import cn.conac.rc.itm.service.DelItemAppService;
import cn.conac.rc.itm.service.VAelItemService;
import cn.conac.rc.itm.service.VDelItemService;
import cn.conac.rc.itm.vo.AelItemVo;
import cn.conac.rc.itm.vo.DelItemVo;
import cn.conac.rc.itm.vo.ItemCntVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
public class VItemController {
	@Autowired
	VAelItemService aelService;

	@Autowired
	VDelItemService delService;

	@Autowired
	DelItemAppService delItemAppService;

	@Autowired
	AelItemDeploiedService aelDeploiedService;

	@Autowired
	AelItemCatalogueService aelCatalogueService;

	@ApiOperation(value = "个数", httpMethod = "GET", response = AelItemCatalogueEntity.class, notes = "根据条件获得资源数量")
	@RequestMapping(value = "/{areaId}/item/counts", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> count(HttpServletRequest request, HttpServletResponse response,
		    @ApiParam(value = "查询地区ID", required = true) @PathVariable("areaId") String areaId) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		ItemCntVo itemCntVo = new ItemCntVo();
		// service调用
		if(StringUtils.isNotBlank(areaId)){
			AelItemVo aelVo = new AelItemVo();
			DelItemVo delVo = new DelItemVo();
			aelVo.setAreaId(areaId);
			delVo.setAreaId(areaId);
			// 权力清单
			long aelItemCnt = aelCatalogueService.count(aelVo);
			long childAelItemCnt = aelDeploiedService.findChildAelItemCnt(areaId);
			long delItemCnt = delItemAppService.count(delVo);
			itemCntVo.setAelItemTotalCnt(aelItemCnt);// 权力清单总数（不含子项）
			itemCntVo.setAelItemTotalChildCnt(childAelItemCnt);// 权力清单子事项数量
			itemCntVo.setDelItemTotalCnt(delItemCnt);
			// 结果集设定
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setResult(itemCntVo);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		} else {
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg(ResultPojo.MSG_FAILURE);
		}

		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据职责编码查询权力清单列表，不分页", httpMethod = "GET", response = VAelItemEntity.class, notes = "根据职责编码查询权力清单列表，不分页")
	@RequestMapping(value = "/duty/{dutyId}/aelItems", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findAelItemByDutyCode(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "dutyId", required = true) @PathVariable("dutyId") String dutyId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<VAelItemEntity> items = aelService.findItemsByDutyId(dutyId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(items);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据机构ID查询权力清单列表，分页(APP)", httpMethod = "POST", response = AelItemCatalogueEntity.class, notes = "根据机构ID查询权力清单列表，分页(APP)")
	@RequestMapping(value = "aelItem/list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> findAelItemByOrgId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody AelItemVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page items = aelCatalogueService.findItemsByOrgId(vo);

		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(items);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "根据清单id查询权力清单", httpMethod = "GET", response = VAelItemEntity.class, notes = "根据清单id查询权力清单")
	@RequestMapping(value = "aelItem/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> aelItemDetail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		VAelItemEntity item = aelService.findById(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(item);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据清单id查询权力清单详情(APP)", httpMethod = "GET", response = AelItemDeploiedEntity.class, notes = "根据清单id查询权力清单详情(APP)")
	@RequestMapping(value = "aelItemDeploied/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> aelItemDeploiedDetail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		AelItemDeploiedEntity item = aelDeploiedService.findById(id);
		if(null != item){
			// 获取 通用事项名称
			if(StringUtils.isNotBlank(item.getGenaralCode())){
				item.setGenaralName(aelDeploiedService.findGeneralName(item.getGenaralCode()));
			}
			// 获取 权力类别名称
			if(StringUtils.isNotBlank(item.getItemType())){
				item.setItemType(this.getItemTypeName(item.getItemType()));
			}
			// 设置 所属职责范围
			if(StringUtils.isNotBlank(item.getOwnOrgId())){
				String dutyContents = aelDeploiedService.findDutyContentByOrgIdAndItemId(item.getOwnOrgId(),item.getId());
				item.setDutyContent(dutyContents);
			}
			// 设置 实施主体
			if(StringUtils.isNotBlank(item.getSubjectId())){
				String subjectName = aelDeploiedService.findOrganizationName(item.getSubjectId());
				item.setSubjectName(subjectName);
			}
			// 设置 所属部门
			if(StringUtils.isNotBlank(item.getOwnOrgId())){
				String orgName = aelDeploiedService.findOrganizationName(item.getOwnOrgId());
				item.setOwnOrgName(orgName);
			}
			// 设置 实施对象
			String objectImName = aelDeploiedService.findObjectImName(item.getId());
			item.setObjectImName(objectImName);
			// 设置 责任处室
			if (StringUtils.isNotBlank(item.getDeptCode())) {
				String deptType = aelDeploiedService.findDeptType(item.getId());
				if("1".equals(deptType)){
					item.setDeptName(aelDeploiedService.findDeptName(item.getDeptCode()));
				} else if("2".equals(deptType)){
					item.setDeptName(aelDeploiedService.findOrganizationName(item.getDeptCode()));
				}
			}
			// 设置 会同处室
			if(StringUtils.isNotBlank(item.getOwnOrgId())){
				item.setDeptContent(aelDeploiedService.findDeptContentByOrgIdAndItemId(item.getOwnOrgId(),item.getId()));
			}
			// 设置 其他共同实施主体
			item.setOrganizations(aelDeploiedService.findOrganizations(item.getId()));
			// 设置 事实依据类型
			item.setBasisName(this.getBasisName(item.getBasisType()));
			// 如果是子项，则要获取对应主项的权力名称
			if(StringUtils.isNotBlank(item.getMainItems())){
				AelItemCatalogueEntity acEntity = aelCatalogueService.findById(new Integer(item.getMainItems()));
				if(null != acEntity){
					item.setMainItemName(acEntity.getItemName());
				}
			}
			// 获取关联责任事项名称以及关联责任事项依据
			List<DelItemAppEntity> delInfos = delItemAppService.findDelInfoByItemsInfoId(item.getId());
			String delItemsName = "";
			String delItemsTerm = "";
			if(delInfos != null && delInfos.size() > 0){
				for(DelItemAppEntity delInfo : delInfos){
					delItemsName += "\r\n" + delInfo.getDutyContent();
					delItemsTerm += "\r\n" + delInfo.getDutyTerms();
				}
				delItemsName = delItemsName.substring(2);
				delItemsTerm = delItemsTerm.substring(2);
			}
			item.setDelItemsName(delItemsName);
			item.setDelItemsTerm(delItemsTerm);
		}
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(item);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据职责编码查询权力清单列表，不分页", httpMethod = "GET", response = VDelItemEntity.class, notes = "根据职责编码查询权力清单列表，不分页")
	@RequestMapping(value = "/duty/{dutyId}/delItems", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findDelItemByDutyCode(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "dutyId", required = true) @PathVariable("dutyId") String dutyId) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		List<VDelItemEntity> items = delService.findItemsByDutyId(dutyId);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(items);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据清单id查询责任清单详情(APP)", httpMethod = "GET", response = VAelItemEntity.class, notes = "根据清单id查询责任清单详情(APP)")
	@RequestMapping(value = "delItemApp/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> delItemDetailApp(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		DelItemAppEntity item = delItemAppService.findById(id);

		if(null != item){
			// 权力对应责任处室
			item.setDeptNames(delItemAppService.findDeptNames(item.getId()));
			// 权力类型获取名称
			item.setAelItemType(this.getItemTypeName(item.getAelItemType()));
		}
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(item);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据清单id查询责任清单", httpMethod = "GET", response = VAelItemEntity.class, notes = "根据清单id查询责任清单")
	@RequestMapping(value = "delItem/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> delItemDetail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		VDelItemEntity item = delService.findById(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(item);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据机构ID查询责任清单列表，分页(APP)", httpMethod = "POST", response = VDelItemEntity.class, notes = "根据机构ID查询责任清单列表，分页(APP)")
	@RequestMapping(value = "delItem/list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> findDelItemByOrgId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody DelItemVo vo) throws Exception{
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<DelItemAppEntity> items = delItemAppService.findItemsByOrgId(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(items);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "根据机构ID查询对应的各项权责清单数量", httpMethod = "GET", response = VDelItemEntity.class, notes = "根据机构ID查询对应的各项权责清单数量")
	@RequestMapping(value = "org/{orgId}/itemCnts", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> findItemsCnt(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "orgId", required = true) @PathVariable("orgId") String orgId) throws Exception{
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		ItemCntVo cntVo = new ItemCntVo();
		// service调用
		AelItemVo vo = new AelItemVo();
		vo.setOwnOrgId(orgId);
		vo.setItemType("0");
		long aelItemCnt0 = aelCatalogueService.count(vo);
		vo.setItemType("1");
		long aelItemCnt1 = aelCatalogueService.count(vo);
		vo.setItemType("2");
		long aelItemCnt2 = aelCatalogueService.count(vo);
		vo.setItemType("3");
		long aelItemCnt3 = aelCatalogueService.count(vo);
		vo.setItemType("4");
		long aelItemCnt4 = aelCatalogueService.count(vo);
		vo.setItemType("5");
		long aelItemCnt5 = aelCatalogueService.count(vo);
		vo.setItemType("6");
		long aelItemCnt6 = aelCatalogueService.count(vo);
		vo.setItemType("7");
		long aelItemCnt7 = aelCatalogueService.count(vo);
		vo.setItemType("8");
		long aelItemCnt8 = aelCatalogueService.count(vo);
		vo.setItemType("9");
		long aelItemCnt9 = aelCatalogueService.count(vo);
		vo.setItemType("10");
		long aelItemCnt10 = aelCatalogueService.count(vo);

		long aelItemTotalCnt = aelItemCnt0 + aelItemCnt2 + aelItemCnt3 + aelItemCnt4
				+ aelItemCnt5 + aelItemCnt6 + aelItemCnt7 + aelItemCnt8 + aelItemCnt9 + aelItemCnt10;

		DelItemVo vo2 = new DelItemVo();
		vo2.setOrgId(orgId);
		long delItemTotalCnt = delItemAppService.count(vo2);

		cntVo.setAelItemCnt0(aelItemCnt0);
		cntVo.setAelItemCnt1(aelItemCnt1);
		cntVo.setAelItemCnt2(aelItemCnt2);
		cntVo.setAelItemCnt3(aelItemCnt3);
		cntVo.setAelItemCnt4(aelItemCnt4);
		cntVo.setAelItemCnt5(aelItemCnt5);
		cntVo.setAelItemCnt6(aelItemCnt6);
		cntVo.setAelItemCnt7(aelItemCnt7);
		cntVo.setAelItemCnt8(aelItemCnt8);
		cntVo.setAelItemCnt9(aelItemCnt9);
		cntVo.setAelItemCnt10(aelItemCnt10);
		cntVo.setAelItemTotalCnt(aelItemTotalCnt);
		cntVo.setDelItemTotalCnt(delItemTotalCnt);

		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(cntVo);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	/**
	 * 权力类型获取名称
	 * @param itemType
	 * @return itemTypeName
	 */
	private String getItemTypeName(String itemType){
		String itemTypeName = "";
		if(StringUtils.isNotEmpty(itemType)){
			switch(itemType){
				case "0":
					itemTypeName = "行政许可";
					break;
				case "1":
					itemTypeName = "非行政许可";
					break;
				case "2":
					itemTypeName = "行政处罚";
					break;
				case "3":
					itemTypeName = "行政强制";
					break;
				case "4":
					itemTypeName = "行政征收";
					break;
				case "5":
					itemTypeName = "行政给付";
					break;
				case "6":
					itemTypeName = "行政检查";
					break;
				case "7":
					itemTypeName = "行政确认";
					break;
				case "8":
					itemTypeName = "行政奖励";
					break;
				case "9":
					itemTypeName = "行政裁决";
					break;
				case "10":
					itemTypeName = "其他类别";
					break;
				default:
					break;
			}
		}
		return itemTypeName;
	}
	
	/**
	 * 获取事实依据类型
	 * @param basisType
	 * @return getBasisTypeName
	 */
	private String getBasisName(String basisType){
		String basisNames = "";
		if(StringUtils.isNotEmpty(basisType)){
			String[] basisTypes = basisType.split(",");
			String basisName = "";
			for(String basisTp : basisTypes){
				switch(basisTp){
					case "1":
						basisName = "国家法律法规";
						break;
					case "2":
						basisName = "省级法规";
						break;
					case "3":
						basisName = "市级法规";
						break;
					case "4":
						basisName = "区县级法律法规";
						break;
					default:
						break;
				}
				basisNames = basisNames + "," + basisName;
			}
			basisNames = basisNames.substring(1);
		}
		return basisNames;
	}
}
