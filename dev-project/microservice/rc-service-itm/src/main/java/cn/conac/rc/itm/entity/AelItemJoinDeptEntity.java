package cn.conac.rc.itm.entity;

import java.io.Serializable;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import cn.conac.rc.framework.entity.DataEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 权力清单会同处室
 * @author wanghm
 *
 */
@ApiModel
@Immutable
@Subselect("select item_Id, dept_name, dept_id from(select t3.id item_Id, t2.dept_Name dept_name t2.id dept_id from ael_items_info  t3 inner join ael_items_dept t1 on t3.id = t1.item_id inner join ofs_department t2 on t1.dept_code = t2.dep_code)")
public class AelItemJoinDeptEntity extends DataEntity<AelItemJoinDeptEntity> implements Serializable {
	
	private static final long serialVersionUID = -3358127121836131577L;
	@ApiModelProperty("内设标识")
	private String deptId;
	
	@ApiModelProperty("内设名称")
	private String deptName;  // 处室名称
	
	@ApiModelProperty("事项标识")
	private String itemId;
	
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
}
