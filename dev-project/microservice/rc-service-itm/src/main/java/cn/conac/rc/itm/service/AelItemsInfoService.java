package cn.conac.rc.itm.service;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.itm.entity.AelItemsInfoEntity;
import cn.conac.rc.itm.repository.AelItemsInfoRepository;
import cn.conac.rc.itm.vo.AelItemsInfoVo;

/**
 * AelItemsInfoService类
 *
 * @author serviceCreator
 * @version 1.0
 * @date 2017-01-09
 */
@Service
public class AelItemsInfoService extends GenericService<AelItemsInfoEntity, Integer>
{
    @Autowired
    private AelItemsInfoRepository repository;

    /**
     * 计数查询
     *
     * @return 计数结果
     */
    public long count(AelItemsInfoVo vo)
    {
        return super.count(this.createCriteria(vo));
    }

    /**
     * 动态查询，分页，排序查询
     *
     * @return Page
     */
    public Page<AelItemsInfoEntity> list(AelItemsInfoVo vo) throws Exception
    {
        try {
            Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
            Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
            Criteria<AelItemsInfoEntity> dc = this.createCriteria(vo);
            return repository.findAll(dc, pageable);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * 动态查询条件
     *
     * @return Criteria
     */
    private Criteria<AelItemsInfoEntity> createCriteria(AelItemsInfoVo param)
    {
        Criteria<AelItemsInfoEntity> dc = new Criteria<AelItemsInfoEntity>();
        // TODO 具体条件赋值
        if (param.getUsers() != null) {
            dc.add(Restrictions.eq("users", param.getUsers(), true));
        }
        if (StringUtils.isNotBlank(param.getItemName())) {
            dc.add(Restrictions.eq("itemName", param.getItemName(), true));
        }
        if (StringUtils.isNotBlank(param.getItemCode())) {
            dc.add(Restrictions.eq("itemCode", param.getItemCode(), true));
        }
        if (StringUtils.isNotBlank(param.getItemCoding())) {
            dc.add(Restrictions.eq("itemCoding", param.getItemCoding(), true));
        }
        if (StringUtils.isNotBlank(param.getItemType())) {
            dc.add(Restrictions.eq("itemType", param.getItemType(), true));
        }
        if (StringUtils.isNotBlank(param.getItemStatus())) {
            String[] ss = param.getItemStatus().split(",");
            if (ss.length == 1) {
                dc.add(Restrictions.eq("itemStatus", param.getItemStatus(), true));
            } else {
                dc.add(Restrictions.in("itemStatus", Arrays.asList(ss), true));
            }
        }
        if (StringUtils.isNotBlank(param.getOperation())) {
            String[] ss = param.getOperation().split(",");
            if (ss.length == 1) {
                dc.add(Restrictions.eq("operation", param.getOperation(), true));
            } else {
                dc.add(Restrictions.in("operation", Arrays.asList(ss), true));
            }
        }
//				beginDate
//		beginDate
        return dc;
    }
}
