package cn.conac.rc.itm.entity;

import java.io.Serializable;
import javax.persistence.Id;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DelItemsDeptEntity类
 *
 * @author beanCreator
 * @date 2017-01-06
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="DEL_ITEMS_DEPT")
public class DelItemsDeptEntity implements Serializable {

	private static final long serialVersionUID = 1483683626883917216L;
	@Id
	@ApiModelProperty("dutyItemsBaseId")
	private Integer dutyItemsBaseId;

	@ApiModelProperty("deptId")
	private Integer deptId;

	public void setDutyItemsBaseId(Integer dutyItemsBaseId){
		this.dutyItemsBaseId=dutyItemsBaseId;
	}

	public Integer getDutyItemsBaseId(){
		return dutyItemsBaseId;
	}

	public void setDeptId(Integer deptId){
		this.deptId=deptId;
	}

	public Integer getDeptId(){
		return deptId;
	}

}
