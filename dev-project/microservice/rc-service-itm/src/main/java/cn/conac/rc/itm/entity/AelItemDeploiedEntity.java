package cn.conac.rc.itm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import cn.conac.rc.framework.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@Entity
@Table(name="AEL_ITEMS_DEPLOIED")
public class AelItemDeploiedEntity extends BaseEntity<AelItemDeploiedEntity> implements Serializable {

	private static final long serialVersionUID = -378537713158084824L;
	
	@ApiModelProperty("通用事项编码")
	private String genaralCode;
	@ApiModelProperty("事项名称")
	private String itemName;
	@ApiModelProperty("事项类型")
	private String itemType;
	@ApiModelProperty("是否有下级")
	private String levels;
	@ApiModelProperty("主项标识")
	private String mainItems;
	@ApiModelProperty("实施主体")
	private String subjectId;
	@ApiModelProperty("实施对象")
	private String objectIm;
	@ApiModelProperty("所属机构ID")
	private String ownOrgId;
	@ApiModelProperty("责任处室")
	private String deptCode;
	@ApiModelProperty("实施依据")
	private String implementBasis;
	@ApiModelProperty("创建时间")
	private Date createTime;
	@ApiModelProperty("备注")
	private String remarks;
	@ApiModelProperty("isAgency")
	private String isAgency;
	@ApiModelProperty("行政权力编号")
	private String itemCode;
	@ApiModelProperty("实施依据类型")
	private String basisType;
	@ApiModelProperty("关联用户ID")
	private String users;
	@ApiModelProperty("agencyRemarks")
	private String agencyRemarks;
	@ApiModelProperty("itemsId")
	private String itemsId;
	@ApiModelProperty("事项状态")
	private String itemStatus;
	@ApiModelProperty("operation")
	private String operation;

	@Transient
	@ApiModelProperty("所属职责范围")
	private String dutyContent;
	@Transient
	@ApiModelProperty("通用事项名称")
	private String genaralName;
	@Transient
	@ApiModelProperty("实施主体名称")
	private String subjectName;
	@Transient
	@ApiModelProperty("所属部门名称")
	private String ownOrgName;
	@Transient
	@ApiModelProperty("实施对象")
	private String objectImName;
	@Transient
	@ApiModelProperty("责任处室名称")
	private String deptName;
	@Transient
	@ApiModelProperty("会同处室名称")
	private String deptContent;
	@Transient
	@ApiModelProperty("其他共同实施主体")
	private String organizations;
	@Transient
	@ApiModelProperty("实施依据类型名称")
	private String basisName;
	@Transient
	@ApiModelProperty("如果是子项，则主项的名称")
	private String mainItemName;
	@Transient
	@ApiModelProperty("关联责任事项名称")
	private String delItemsName;
	@Transient
	@ApiModelProperty("关联责任事项依据")
	private String delItemsTerm;

	public String getGenaralCode() {
		return genaralCode;
	}

	public void setGenaralCode(String genaralCode) {
		this.genaralCode = genaralCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getLevels() {
		return levels;
	}

	public void setLevels(String levels) {
		this.levels = levels;
	}

	public String getMainItems() {
		return mainItems;
	}

	public void setMainItems(String mainItems) {
		this.mainItems = mainItems;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getObjectIm() {
		return objectIm;
	}

	public void setObjectIm(String objectIm) {
		this.objectIm = objectIm;
	}

	public String getOwnOrgId() {
		return ownOrgId;
	}

	public void setOwnOrgId(String ownOrgId) {
		this.ownOrgId = ownOrgId;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getImplementBasis() {
		return implementBasis;
	}

	public void setImplementBasis(String implementBasis) {
		this.implementBasis = implementBasis;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getIsAgency() {
		return isAgency;
	}

	public void setIsAgency(String isAgency) {
		this.isAgency = isAgency;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getBasisType() {
		return basisType;
	}

	public void setBasisType(String basisType) {
		this.basisType = basisType;
	}

	public String getUsers() {
		return users;
	}

	public void setUsers(String users) {
		this.users = users;
	}

	public String getAgencyRemarks() {
		return agencyRemarks;
	}

	public void setAgencyRemarks(String agencyRemarks) {
		this.agencyRemarks = agencyRemarks;
	}

	public String getItemsId() {
		return itemsId;
	}

	public void setItemsId(String itemsId) {
		this.itemsId = itemsId;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getDutyContent() {
		return dutyContent;
	}

	public void setDutyContent(String dutyContent) {
		this.dutyContent = dutyContent;
	}

	public String getGenaralName() {
		return genaralName;
	}

	public void setGenaralName(String genaralName) {
		this.genaralName = genaralName;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getObjectImName() {
		return objectImName;
	}

	public void setObjectImName(String objectImName) {
		this.objectImName = objectImName;
	}

	public String getOwnOrgName() {
		return ownOrgName;
	}

	public void setOwnOrgName(String ownOrgName) {
		this.ownOrgName = ownOrgName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptContent() {
		return deptContent;
	}

	public void setDeptContent(String deptContent) {
		this.deptContent = deptContent;
	}

	public String getOrganizations() {
		return organizations;
	}

	public void setOrganizations(String organizations) {
		this.organizations = organizations;
	}

	public String getBasisName() {
		return basisName;
	}

	public void setBasisName(String basisName) {
		this.basisName = basisName;
	}

	public String getMainItemName() {
		return mainItemName;
	}

	public void setMainItemName(String mainItemName) {
		this.mainItemName = mainItemName;
	}

	public String getDelItemsName() {
		return delItemsName;
	}

	public void setDelItemsName(String delItemsName) {
		this.delItemsName = delItemsName;
	}

	public String getDelItemsTerm() {
		return delItemsTerm;
	}

	public void setDelItemsTerm(String delItemsTerm) {
		this.delItemsTerm = delItemsTerm;
	}
}
