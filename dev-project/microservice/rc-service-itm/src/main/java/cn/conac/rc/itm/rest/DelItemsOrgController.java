package cn.conac.rc.itm.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.itm.entity.DelItemsOrgEntity;
import cn.conac.rc.itm.service.DelItemsOrgService;
import cn.conac.rc.itm.vo.DelItemsOrgVo;
import cn.conac.rc.framework.vo.ResultPojo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * DelItemsOrgController类
 *
 * @author controllerCreator
 * @date 2017-01-06
 * @version 1.0
 */
@RestController
@RequestMapping(value="delItemsOrg/")
public class DelItemsOrgController {

	@Autowired
	DelItemsOrgService service;

	@ApiOperation(value = "详情", httpMethod = "GET", response = DelItemsOrgEntity.class, notes = "根据id获取资源详情")
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> detail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		DelItemsOrgEntity entity = service.findById(id);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "个数", httpMethod = "POST", response = Long.class, notes = "根据条件获得资源数量")
	@RequestMapping(value = "count", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> count(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody DelItemsOrgVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		long cnt = service.count(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(cnt);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "列表", httpMethod = "POST", response = DelItemsOrgEntity.class, notes = "根据条件获得资源列表")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> list(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody DelItemsOrgVo vo) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		Page<DelItemsOrgEntity> list = service.list(vo);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(list);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "新增", httpMethod = "POST", response = DelItemsOrgEntity.class, notes = "保存资源到数据库")
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> save(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "保存对象", required = true) @RequestBody DelItemsOrgEntity entity) throws Exception {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();

		//TODO 1.用户权限校验

		//TODO 2.数据格式校验
		String valMsg = service.validate(entity);
		if (valMsg != null) {
			result.setCode(ResultPojo.CODE_FORMAT_ERR);
			result.setMsg(valMsg);
			return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
		}

		//TODO 3.业务逻辑校验

		//TODO 4.业务操作

		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);// TODO 根据具体需求配置返回消息
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "更新", httpMethod = "POST", response = DelItemsOrgEntity.class, notes = "根据id更新数据库资源")
	@RequestMapping(value = "{id}", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> update(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "更新对象", required = true) @RequestBody DelItemsOrgEntity entity) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		service.save(entity);
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(entity);
		result.setMsg(ResultPojo.MSG_SUCCESS);// TODO 根据具体需求配置返回消息
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "物理删除", httpMethod = "POST", response = DelItemsOrgEntity.class, notes = "根据id物理删除资源")
	@RequestMapping(value = "{id}/delete", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> delete(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "id", required = true) @PathVariable("id") Integer id) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		// service调用
		if(service.exists(id)){
			service.delete(id);
			// 结果集设定
			result.setCode(ResultPojo.CODE_SUCCESS);
			result.setMsg(ResultPojo.MSG_SUCCESS);
		} else {
			// 结果集设定
			result.setCode(ResultPojo.CODE_FAILURE);
			result.setMsg(ResultPojo.MSG_FAILURE);
		}
		result.setResult(id);

		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}

}
