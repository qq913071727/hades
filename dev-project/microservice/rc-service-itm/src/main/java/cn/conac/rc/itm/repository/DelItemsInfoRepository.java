package cn.conac.rc.itm.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.itm.entity.DelItemsInfoEntity;
import cn.conac.rc.framework.repository.GenericDao;

/**
 * DelItemsInfoRepository类
 *
 * @author repositoryCreator
 * @date 2017-01-06
 * @version 1.0
 */
@Repository
public interface DelItemsInfoRepository extends GenericDao<DelItemsInfoEntity, Integer> {
}
