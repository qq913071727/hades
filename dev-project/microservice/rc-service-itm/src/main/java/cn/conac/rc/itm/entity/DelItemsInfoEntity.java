package cn.conac.rc.itm.entity;

import java.io.Serializable;
import javax.persistence.Id;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DelItemsInfoEntity类
 *
 * @author beanCreator
 * @date 2017-01-06
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="DEL_ITEMS_INFO")
public class DelItemsInfoEntity implements Serializable {

	private static final long serialVersionUID = 1483683626920820003L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("dutyCode")
	private String dutyCode;

	@ApiModelProperty("itemsInfoId")
	private Integer itemsInfoId;

	@ApiModelProperty("dutyItemsId")
	private Integer dutyItemsId;

	@ApiModelProperty("dutyBody")
	private Integer dutyBody;

	@ApiModelProperty("notes")
	private String notes;

	@ApiModelProperty("createdUser")
	private Integer createdUser;

	@ApiModelProperty("createdDate")
	private Date createdDate;

	@ApiModelProperty("operation")
	private String operation;

	@ApiModelProperty("status")
	private Integer status;

	@ApiModelProperty("sourceId")
	private Integer sourceId;

	@ApiModelProperty("dutyItemsExtend")
	private String dutyItemsExtend;

	@ApiModelProperty("deptIds")
	private String deptIds;

	@ApiModelProperty("isColAel")
	private String isColAel;

	@ApiModelProperty("dutyContent")
	private String dutyContent;

	@ApiModelProperty("dutyTerms")
	private String dutyTerms;

	@ApiModelProperty("traceObjects")
	private String traceObjects;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setDutyCode(String dutyCode){
		this.dutyCode=dutyCode;
	}

	public String getDutyCode(){
		return dutyCode;
	}

	public void setItemsInfoId(Integer itemsInfoId){
		this.itemsInfoId=itemsInfoId;
	}

	public Integer getItemsInfoId(){
		return itemsInfoId;
	}

	public void setDutyItemsId(Integer dutyItemsId){
		this.dutyItemsId=dutyItemsId;
	}

	public Integer getDutyItemsId(){
		return dutyItemsId;
	}

	public void setDutyBody(Integer dutyBody){
		this.dutyBody=dutyBody;
	}

	public Integer getDutyBody(){
		return dutyBody;
	}

	public void setNotes(String notes){
		this.notes=notes;
	}

	public String getNotes(){
		return notes;
	}

	public void setCreatedUser(Integer createdUser){
		this.createdUser=createdUser;
	}

	public Integer getCreatedUser(){
		return createdUser;
	}

	public void setCreatedDate(Date createdDate){
		this.createdDate=createdDate;
	}

	public Date getCreatedDate(){
		return createdDate;
	}

	public void setOperation(String operation){
		this.operation=operation;
	}

	public String getOperation(){
		return operation;
	}

	public void setStatus(Integer status){
		this.status=status;
	}

	public Integer getStatus(){
		return status;
	}

	public void setSourceId(Integer sourceId){
		this.sourceId=sourceId;
	}

	public Integer getSourceId(){
		return sourceId;
	}

	public void setDutyItemsExtend(String dutyItemsExtend){
		this.dutyItemsExtend=dutyItemsExtend;
	}

	public String getDutyItemsExtend(){
		return dutyItemsExtend;
	}

	public void setDeptIds(String deptIds){
		this.deptIds=deptIds;
	}

	public String getDeptIds(){
		return deptIds;
	}

	public void setIsColAel(String isColAel){
		this.isColAel=isColAel;
	}

	public String getIsColAel(){
		return isColAel;
	}

	public void setDutyContent(String dutyContent){
		this.dutyContent=dutyContent;
	}

	public String getDutyContent(){
		return dutyContent;
	}

	public void setDutyTerms(String dutyTerms){
		this.dutyTerms=dutyTerms;
	}

	public String getDutyTerms(){
		return dutyTerms;
	}

	public void setTraceObjects(String traceObjects){
		this.traceObjects=traceObjects;
	}

	public String getTraceObjects(){
		return traceObjects;
	}

}
