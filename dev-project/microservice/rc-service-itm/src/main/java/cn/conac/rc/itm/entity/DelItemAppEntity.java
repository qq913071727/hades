package cn.conac.rc.itm.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import cn.conac.rc.framework.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@Entity
@Table(name="rcd_v_del_item_app")
public class DelItemAppEntity extends BaseEntity<DelItemAppEntity> implements Serializable {
	private static final long serialVersionUID = 3535112255934939732L;
	
	@ApiModelProperty("行政责任编码")
	private String dutyCode;
	@ApiModelProperty("行政责任名称")
	private String dutyContent;
	@ApiModelProperty("权力标识")
	private Integer itemsInfoId;
	@ApiModelProperty("责任依据")
	private String dutyTerms;
	@ApiModelProperty("追责情形")
	private String traceObjects ;
	@ApiModelProperty("关联权力名称")
	private String aelName;
	@ApiModelProperty("关联权力类型")
	private String aelItemType;
	@ApiModelProperty("所属单位名称")
	private String orgName;
	@ApiModelProperty("所属机构ID")
	private String orgId;
	@ApiModelProperty("操作类型")
	private String operation;
	@ApiModelProperty("关联权力实施依据")
	private String aelImplementBasis;
	@ApiModelProperty("地区ID")
	private String areaId;
	@ApiModelProperty("责任处室")
	@Transient
	private String deptNames;

	public String getDutyCode() {
		return dutyCode;
	}
	public void setDutyCode(String dutyCode) {
		this.dutyCode = dutyCode;
	}
	public String getDutyContent() {
		return dutyContent;
	}
	public void setDutyContent(String dutyContent) {
		this.dutyContent = dutyContent;
	}
	public Integer getItemsInfoId() {
		return itemsInfoId;
	}
	public void setItemsInfoId(Integer itemsInfoId) {
		this.itemsInfoId = itemsInfoId;
	}
	public String getDutyTerms() {
		return dutyTerms;
	}
	public void setDutyTerms(String dutyTerms) {
		this.dutyTerms = dutyTerms;
	}
	public String getTraceObjects() {
		return traceObjects;
	}
	public void setTraceObjects(String traceObjects) {
		this.traceObjects = traceObjects;
	}
	public String getAelName() {
		return aelName;
	}
	public void setAelName(String aelName) {
		this.aelName = aelName;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getAelItemType() {
		return aelItemType;
	}
	public void setAelItemType(String aelItemType) {
		this.aelItemType = aelItemType;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getDeptNames() {
		return deptNames;
	}
	public void setDeptNames(String deptNames) {
		this.deptNames = deptNames;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getAelImplementBasis() {
		return aelImplementBasis;
	}

	public void setAelImplementBasis(String aelImplementBasis) {
		this.aelImplementBasis = aelImplementBasis;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
}
