package cn.conac.rc.itm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AelItemsEntity类
 *
 * @author beanCreator
 * @date 2017-01-09
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="AEL_ITEMS")
public class AelItemsEntity implements Serializable {

	private static final long serialVersionUID = 1483927362859243439L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("itemInfoId")
	private Integer itemInfoId;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setItemInfoId(Integer itemInfoId){
		this.itemInfoId=itemInfoId;
	}

	public Integer getItemInfoId(){
		return itemInfoId;
	}

}
