package cn.conac.rc.itm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AelItemReviewEntity类
 *
 * @author beanCreator
 * @date 2017-01-09
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="AEL_ITEM_REVIEW")
public class AelItemReviewEntity implements Serializable {

	private static final long serialVersionUID = 1483927362820431943L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("itemId")
	private Integer itemId;

	@ApiModelProperty("userId")
	private Integer userId;

	@ApiModelProperty("reviewType")
	private String reviewType;

	@ApiModelProperty("description")
	private String description;

	@ApiModelProperty("createDate")
	private Date createDate;

	@ApiModelProperty("itemStatus")
	private String itemStatus;

	@ApiModelProperty("itemType")
	private String itemType;

	@ApiModelProperty("itemCode")
	private String itemCode;

	@ApiModelProperty("itemName")
	private String itemName;

	@ApiModelProperty("mentionTime")
	private Date mentionTime;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setItemId(Integer itemId){
		this.itemId=itemId;
	}

	public Integer getItemId(){
		return itemId;
	}

	public void setUserId(Integer userId){
		this.userId=userId;
	}

	public Integer getUserId(){
		return userId;
	}

	public void setReviewType(String reviewType){
		this.reviewType=reviewType;
	}

	public String getReviewType(){
		return reviewType;
	}

	public void setDescription(String description){
		this.description=description;
	}

	public String getDescription(){
		return description;
	}

	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}

	public Date getCreateDate(){
		return createDate;
	}

	public void setItemStatus(String itemStatus){
		this.itemStatus=itemStatus;
	}

	public String getItemStatus(){
		return itemStatus;
	}

	public void setItemType(String itemType){
		this.itemType=itemType;
	}

	public String getItemType(){
		return itemType;
	}

	public void setItemCode(String itemCode){
		this.itemCode=itemCode;
	}

	public String getItemCode(){
		return itemCode;
	}

	public void setItemName(String itemName){
		this.itemName=itemName;
	}

	public String getItemName(){
		return itemName;
	}

	public void setMentionTime(Date mentionTime){
		this.mentionTime=mentionTime;
	}

	public Date getMentionTime(){
		return mentionTime;
	}

}
