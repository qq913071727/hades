package cn.conac.rc.itm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.itm.entity.DelItemsChangeReasonEntity;
import cn.conac.rc.itm.repository.DelItemsChangeReasonRepository;
import cn.conac.rc.itm.vo.DelItemsChangeReasonVo;
import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;

/**
 * DelItemsChangeReasonService类
 *
 * @author serviceCreator
 * @date 2017-01-06
 * @version 1.0
 */
@Service
public class DelItemsChangeReasonService extends GenericService<DelItemsChangeReasonEntity, Integer> {

	@Autowired
	private DelItemsChangeReasonRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(DelItemsChangeReasonVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<DelItemsChangeReasonEntity> list(DelItemsChangeReasonVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<DelItemsChangeReasonEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<DelItemsChangeReasonEntity> createCriteria(DelItemsChangeReasonVo param) {
		Criteria<DelItemsChangeReasonEntity> dc = new Criteria<DelItemsChangeReasonEntity>();
		// TODO 具体条件赋值

		return dc;
	}

}
