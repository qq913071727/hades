package cn.conac.rc.itm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.conac.rc.itm.entity.DelItemsOrgEntity;
import cn.conac.rc.itm.repository.DelItemsOrgRepository;
import cn.conac.rc.itm.vo.DelItemsOrgVo;
import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;

/**
 * DelItemsOrgService类
 *
 * @author serviceCreator
 * @date 2017-01-06
 * @version 1.0
 */
@Service
public class DelItemsOrgService extends GenericService<DelItemsOrgEntity, Integer> {

	@Autowired
	private DelItemsOrgRepository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(DelItemsOrgVo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<DelItemsOrgEntity> list(DelItemsOrgVo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<DelItemsOrgEntity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<DelItemsOrgEntity> createCriteria(DelItemsOrgVo param) {
		Criteria<DelItemsOrgEntity> dc = new Criteria<DelItemsOrgEntity>();
		// TODO 具体条件赋值

		return dc;
	}

}
