package cn.conac.rc.itm.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.itm.entity.AelItemBasisEntity;
import cn.conac.rc.itm.service.AelItemBasisService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * AelItemBasisController类
 *
 * @author controllerCreator
 * @date 2017-06-19
 * @version 1.0
 */
@RestController
@RequestMapping(value="aelItemBasis/")
public class AelItemBasisController {

	@Autowired
	AelItemBasisService service;
	
	@ApiOperation(value = "获得实施依据类型列表信息", httpMethod = "GET", response = AelItemBasisEntity.class, notes = "获得实施依据类型列表信息")
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public List<AelItemBasisEntity> findAllList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// service调用
		List<AelItemBasisEntity> list = service.findAllList();
		return list;
	}
	
	@ApiOperation(value = "根据Pid获得实施依据类型列表信息", httpMethod = "GET", response = AelItemBasisEntity.class, notes = "根据Pid获得实施依据类型列表信息")
	@RequestMapping(value = "list/{pid}", method = RequestMethod.GET)
	public List<AelItemBasisEntity> findByPidList(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "地区pid", required = true) @PathVariable("pid") Integer pid) throws Exception {
		// service调用
		List<AelItemBasisEntity> list = service.findByPid(pid);
		return list;
	}
}
