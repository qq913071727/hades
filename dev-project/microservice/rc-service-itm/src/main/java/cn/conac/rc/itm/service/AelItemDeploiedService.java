package cn.conac.rc.itm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.jpa.Restrictions;
import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.framework.utils.StringUtils;
import cn.conac.rc.itm.entity.AelItemDeploiedEntity;
import cn.conac.rc.itm.repository.AelItemDeploiedRepository;
import cn.conac.rc.itm.vo.AelItemVo;

@Service
public class AelItemDeploiedService extends GenericService<AelItemDeploiedEntity, Integer> {

    @Autowired
    AelItemDeploiedRepository repository;
    /**
     * 获取部门相关权利对应的所属职责范围
     * @param itemId
     * @param orgId
     * @return 所属职责范围
     */
    public String findDutyContentByOrgIdAndItemId(String orgId, Integer itemId){
        String dutyContents = "";
        List<String> dutyContentList = repository.findDutyContents(itemId,orgId);
        if (null != dutyContentList && dutyContentList.size() > 0) {
            for (String dutyContent : dutyContentList) {
                dutyContents = dutyContents + " " + dutyContent;
            }
        }
        return dutyContents;
    }
    
    /**
     * 获取 通用事项名称
     * @param generalCode
     * @return String
     */
    public String findGeneralName(String generalCode){
    	return repository.findGeneralName(generalCode);
    }
    
    /**
     * 通过OrgId获取对应机构名称
     * @param orgId
     * @return String
     */
    public String findOrganizationName(String orgId){
    	return repository.findOrganizationName(orgId);
    }
    
    /**
     * 通过objectIm获取对应实施对象
     * @param itemId
     * @return String
     */
    public String findObjectImName(Integer itemId){
        String objectImIds = repository.findObjectImIds(itemId);
        String objectNames = "";
        if(StringUtils.isNotBlank(objectImIds)){
            String[] objectIms = objectImIds.split(",");
            for(String objectIm : objectIms){
                objectNames = objectNames + "," +  repository.findObjectImName(objectIm);
            }
            objectNames = objectNames.substring(1);
        }

    	return objectNames;
    }
    
    /**
     * 通过itemId获取对应机构名称
     * @param itemId
     * @return String
     */
    public String findDeptType(Integer itemId){
    	return repository.findDeptType(itemId);
    }
    
    /**
     * 通过deptId获取对应机构名称
     * @param deptId
     * @return String
     */
    public String findDeptName(String deptId){
    	return repository.findDeptName(deptId);
    }
    
    /**
     * 获取部门相关权利对应的会同处室
     * @param itemId
     * @param orgId
     * @return 会同处室
     */
    public String findDeptContentByOrgIdAndItemId(String orgId, Integer itemId){
        String deptContents = "";
        List<String> deptContentList = repository.findDeptContents(itemId,orgId);
        if (null != deptContentList && deptContentList.size() > 0) {
            for (String deptContent : deptContentList) {
                deptContents = deptContents + "," + deptContent;
            }
            deptContents = deptContents.substring(1);
        }
        return deptContents;
    }
    
    /**
     * 通过itemId获取对应其他共同实施主体
     * @param itemId
     * @return String
     */
    public String findOrganizations(Integer itemId){
    	String orgs = "";
    	List<String> orgList = repository.findOrganizations(itemId);
    	if(null != orgList && orgList.size() > 0){
    		for(String org : orgList){
    			orgs = orgs + "," + org;
    		}
    		orgs = orgs.substring(1);
    	}
    	return orgs;
    }


    public Page<AelItemDeploiedEntity> findAelItemDeploiedList(AelItemVo vo) throws Exception {
        try {
            Sort sort = new Sort(Sort.Direction.DESC,"createTime");
            Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
            Criteria<AelItemDeploiedEntity> dc = this.createCriteria(vo);
            return repository.findAll(dc,pageable);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * 动态查询条件
     *
     * @param vo
     * @return Criteria
     */
    private Criteria<AelItemDeploiedEntity> createCriteria(AelItemVo vo) {
        Criteria<AelItemDeploiedEntity> dc = new Criteria<>();
        // 权力事项名称
        if (StringUtils.isNotBlank(vo.getItemName())) {
            dc.add(Restrictions.like("itemName", vo.getItemName(), true));
        }
        return dc;
    }

    /**
     * 查询对应地区ID的子权力事项的数量
     * @param areaId
     * @return Integer
     */
    public long findChildAelItemCnt(String areaId){
        return repository.findChildAelItemCnt(areaId);
    }
}
