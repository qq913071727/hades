package cn.conac.rc.itm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AelItemsDutyEntity类
 *
 * @author beanCreator
 * @date 2017-01-09
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="AEL_ITEMS_DUTY")
public class AelItemsDutyEntity implements Serializable {

	private static final long serialVersionUID = 1483927362889171458L;
	@Id
	@ApiModelProperty("itemId")
	private Integer itemId;

	@ApiModelProperty("dutyCode")
	private String dutyCode;

	@ApiModelProperty("id")
	private Integer id;

	public void setItemId(Integer itemId){
		this.itemId=itemId;
	}

	public Integer getItemId(){
		return itemId;
	}

	public void setDutyCode(String dutyCode){
		this.dutyCode=dutyCode;
	}

	public String getDutyCode(){
		return dutyCode;
	}

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

}
