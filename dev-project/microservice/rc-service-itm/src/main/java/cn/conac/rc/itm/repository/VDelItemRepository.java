package cn.conac.rc.itm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.itm.entity.VDelItemEntity;
@Repository
public interface VDelItemRepository extends GenericDao<VDelItemEntity, Integer>{
	
	@Query(value = "select * from rcd_v_del_item where duty_id =:dutyId", nativeQuery = true)
	List<VDelItemEntity> findByDutyCode(@Param("dutyId") String dutyId);
}
