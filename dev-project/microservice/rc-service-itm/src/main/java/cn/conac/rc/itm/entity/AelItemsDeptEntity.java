package cn.conac.rc.itm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AelItemsDeptEntity类
 *
 * @author beanCreator
 * @date 2017-01-09
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="AEL_ITEMS_DEPT")
public class AelItemsDeptEntity implements Serializable {

	private static final long serialVersionUID = 1483927362874354079L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("itemId")
	private Integer itemId;

	@ApiModelProperty("deptCode")
	private String deptCode;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setItemId(Integer itemId){
		this.itemId=itemId;
	}

	public Integer getItemId(){
		return itemId;
	}

	public void setDeptCode(String deptCode){
		this.deptCode=deptCode;
	}

	public String getDeptCode(){
		return deptCode;
	}

}
