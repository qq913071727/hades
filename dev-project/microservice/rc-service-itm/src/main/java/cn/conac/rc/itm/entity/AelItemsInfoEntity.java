package cn.conac.rc.itm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AelItemsInfoEntity类
 *
 * @author beanCreator
 * @date 2017-01-09
 * @version 1.0
 */
@ApiModel
@Entity
@Table(name="AEL_ITEMS_INFO")
public class AelItemsInfoEntity implements Serializable {

	private static final long serialVersionUID = 148392736292362370L;
	@Id
	@ApiModelProperty("id")
	private Integer id;

	@ApiModelProperty("genaralCode")
	private String genaralCode;

	@ApiModelProperty("itemName")
	private String itemName;

	@ApiModelProperty("itemType")
	private String itemType;

	@ApiModelProperty("levels")
	private String levels;

	@ApiModelProperty("mainItems")
	private Integer mainItems;

	@ApiModelProperty("subjectId")
	private Integer subjectId;

	@ApiModelProperty("itemCoding")
	private String itemCoding;

	@ApiModelProperty("objectIm")
	private Integer objectIm;

	@ApiModelProperty("ownOrgId")
	private Integer ownOrgId;

	@ApiModelProperty("deptCode")
	private String deptCode;

	@ApiModelProperty("createTime")
	private Date createTime;

	@ApiModelProperty("remarks")
	private String remarks;

	@ApiModelProperty("isAgency")
	private String isAgency;

	@ApiModelProperty("itemCode")
	private String itemCode;

	@ApiModelProperty("basisType")
	private String basisType;

	@ApiModelProperty("users")
	private Integer users;

	@ApiModelProperty("agencyRemarks")
	private String agencyRemarks;

	@ApiModelProperty("itemsId")
	private Integer itemsId;

	@ApiModelProperty("extContent")
	private String extContent;

	@ApiModelProperty("itemStatus")
	private String itemStatus;

	@ApiModelProperty("operation")
	private String operation;

	@ApiModelProperty("objectImIds")
	private String objectImIds;

	@ApiModelProperty("implementBasis")
	private String implementBasis;

	@ApiModelProperty("deptType")
	private String deptType;

	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return id;
	}

	public void setGenaralCode(String genaralCode){
		this.genaralCode=genaralCode;
	}

	public String getGenaralCode(){
		return genaralCode;
	}

	public void setItemName(String itemName){
		this.itemName=itemName;
	}

	public String getItemName(){
		return itemName;
	}

	public void setItemType(String itemType){
		this.itemType=itemType;
	}

	public String getItemType(){
		return itemType;
	}

	public void setLevels(String levels){
		this.levels=levels;
	}

	public String getLevels(){
		return levels;
	}

	public void setMainItems(Integer mainItems){
		this.mainItems=mainItems;
	}

	public Integer getMainItems(){
		return mainItems;
	}

	public void setSubjectId(Integer subjectId){
		this.subjectId=subjectId;
	}

	public Integer getSubjectId(){
		return subjectId;
	}

	public void setObjectIm(Integer objectIm){
		this.objectIm=objectIm;
	}

	public Integer getObjectIm(){
		return objectIm;
	}

	public void setOwnOrgId(Integer ownOrgId){
		this.ownOrgId=ownOrgId;
	}

	public Integer getOwnOrgId(){
		return ownOrgId;
	}

	public void setDeptCode(String deptCode){
		this.deptCode=deptCode;
	}

	public String getDeptCode(){
		return deptCode;
	}

	public void setCreateTime(Date createTime){
		this.createTime=createTime;
	}

	public Date getCreateTime(){
		return createTime;
	}

	public void setRemarks(String remarks){
		this.remarks=remarks;
	}

	public String getRemarks(){
		return remarks;
	}

	public void setIsAgency(String isAgency){
		this.isAgency=isAgency;
	}

	public String getIsAgency(){
		return isAgency;
	}

	public void setItemCode(String itemCode){
		this.itemCode=itemCode;
	}

	public String getItemCode(){
		return itemCode;
	}

	public void setBasisType(String basisType){
		this.basisType=basisType;
	}

	public String getBasisType(){
		return basisType;
	}

	public void setUsers(Integer users){
		this.users=users;
	}

	public Integer getUsers(){
		return users;
	}

	public void setAgencyRemarks(String agencyRemarks){
		this.agencyRemarks=agencyRemarks;
	}

	public String getAgencyRemarks(){
		return agencyRemarks;
	}

	public void setItemsId(Integer itemsId){
		this.itemsId=itemsId;
	}

	public Integer getItemsId(){
		return itemsId;
	}

	public void setExtContent(String extContent){
		this.extContent=extContent;
	}

	public String getExtContent(){
		return extContent;
	}


	public void setOperation(String operation){
		this.operation=operation;
	}

	public String getOperation(){
		return operation;
	}

	public void setObjectImIds(String objectImIds){
		this.objectImIds=objectImIds;
	}

	public String getObjectImIds(){
		return objectImIds;
	}

	public void setImplementBasis(String implementBasis){
		this.implementBasis=implementBasis;
	}

	public String getImplementBasis(){
		return implementBasis;
	}

	public void setDeptType(String deptType){
		this.deptType=deptType;
	}

	public String getDeptType(){
		return deptType;
	}

	public String getItemCoding()
	{
		return itemCoding;
	}

	public void setItemCoding(String itemCoding)
	{
		this.itemCoding = itemCoding;
	}

	public String getItemStatus()
	{
		return itemStatus;
	}

	public void setItemStatus(String itemStatus)
	{
		this.itemStatus = itemStatus;
	}
}
