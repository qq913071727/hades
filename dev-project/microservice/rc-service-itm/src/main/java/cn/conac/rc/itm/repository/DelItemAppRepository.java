package cn.conac.rc.itm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.itm.entity.DelItemAppEntity;

@Repository
public interface DelItemAppRepository extends GenericDao<DelItemAppEntity, Integer>{
	
	/**
	 * 获取权力对应的责任处室
	 * @param itemId
	 * @return List<String>
	 */
	@Query(value = "select od.dept_name from ofs_department od, del_items_dept did, del_items_info dii where dii.ID = did.DUTY_ITEMS_BASE_ID and did.DEPT_ID = od.ID and dii.ID = :itemId", nativeQuery = true)
	List<String> findDeptNames(@Param("itemId") Integer itemId);

	/**
	 * 通过权力ID获取对应的责任信息
	 * @param itemsInfoId
	 * @param operations
	 * @return List<DelItemAppEntity>
	 */
	List<DelItemAppEntity> findByItemsInfoIdAndOperationInOrderById(@Param("itemsInfoId") Integer itemsInfoId, @Param("operations") List<String>  operations);
}
