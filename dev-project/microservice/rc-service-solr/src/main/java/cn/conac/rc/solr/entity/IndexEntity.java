package cn.conac.rc.solr.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * solr相关索引Entity类
 */
public class IndexEntity {

	/**
	 * 需要索引的数据
	 */
	private List<Map<String,Object>> indexData = new ArrayList<Map<String,Object>>();
	
	/**
	 * 删除索引时，传入id组
	 */
	private List<String> ids = new ArrayList<String>();
	
	/**
	 * 删除索引时，传入id
	 */ 
	private String id;
	
	/**
	 * 指定solr中要索引的仓库名
	 */
	private String collectionName = null;
	
	/**
	 * 指定solr中要删除索引的条件
	 */
	private String delCondtion = "*:*";
	
	
	public List<String> getIds() {
		return ids;
	}

	public void setIds(List<String> ids) {
		this.ids = ids;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Map<String, Object>> getIndexData() {
		return indexData;
	}

	public void setIndexData(List<Map<String, Object>> indexData) {
		this.indexData = indexData;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

	public String getDelCondtion() {
		return delCondtion;
	}

	public void setDelCondtion(String delCondtion) {
		this.delCondtion = delCondtion;
	}
	
}
