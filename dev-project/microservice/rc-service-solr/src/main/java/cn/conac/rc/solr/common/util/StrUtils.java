package cn.conac.rc.solr.common.util;

import java.util.ArrayList;
import java.util.List;

import cn.conac.rc.framework.utils.StringUtils;

/**
 * 字符串的帮助类，提供静态方法，不可以实例化。
 * 
 */
public class StrUtils {
	/**
	 * 禁止实例化
	 */
	private StrUtils() {
	}
	
	/**
	 * 通过字符串list转换为适用于solr的检索条件
	 * @param condStr 字符列表
	 * @param field  solr字段
	 * @param oper  操作符(例如 OR、AND等)
	 * @return    返回值如下例
	 *   比如查询多个ID场合 areaId:XXXX01 OR areaId:XXXX02 OR areaId:XXXX03
	 *   Map中key与value的值:
	 *   key:areaId
	 *   value: XXXX01 OR areaId:XXXX02 OR areaId:XXXX03
	 */
	public static String convfilterFieldStr(List<String> condStrs, String field, String oper){		
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < condStrs.size(); i++){
			if(i == 0){
				sb.append(condStrs.get(i));
			} else {
				sb.append(field + " : " + condStrs.get(i));
			}
			if(i != (condStrs.size() -1)){
				sb.append(" " + oper + " ");
			}
		}
		return sb.toString();
	}
	
	/**
	 * 通过字符串list转换为适用于solr的检索条件
	 * @param condStr 条件范围列表中间以分割符进行区分（比如分割符是@@ -> 2@@8）
	 * @param field  solr字段
	 * @param oper  操作符(例如 OR、AND等)
	 * @param splitFlag  分割符
	 * @return    返回值如下例
	 *   比如查询多个ID场合 empNumStr : [1 TO 5] OR empNumStr : [7 TO 10 ] OR empNumStr : [15 TO 16 ] 
	 *   Map中key与value的值:
	 *   key:empNumStr
	 *   value: [1 TO 5] OR empNumStr : [7 TO 10 ] OR empNumStr : [15 TO 16 ] 
	 */
	public static String convfilterFieldRangeStr(List<String> condStrs, String field, String oper, String splitFlag) {
		StringBuffer sb = new StringBuffer();
		String[] condStrRange = null; 
		for(int i = 0; i < condStrs.size(); i++) {
			condStrRange = condStrs.get(i).split(splitFlag);
			if(i == 0){
				sb.append( " [" + condStrRange[0] + " TO " + condStrRange[1] + "] " );
			} else {
				sb.append(field + " : " + " [" + condStrRange[0] + " TO " + condStrRange[1] + "] ");
			}
			if(i != (condStrs.size() -1)){
				sb.append(" " + oper + " ");
			}
		}
		return sb.toString();
	}
	
	/**
	 * 去掉字符串list中的重复值
	 * @param listStr 字符串list
	 * @return 返回去重后的字符串List
	 */
	public static List<String> remDupliStrList(List<String> listStr) {
		if(StringUtils.isEmpty(listStr) ){
			return new ArrayList<String>();
		}
		String tmpStr = null;
		List<String> newListStr = new ArrayList<String>(); 
	    for(int i = 0; i < listStr.size(); i++){ 
	    	tmpStr = listStr.get(i);
	        if(!newListStr.contains(tmpStr)){
	        	newListStr.add(tmpStr);
	        }
	    }
		return newListStr;
	}
	
	/**
	 * 将用分割符区分的字符串转换成List
	 * @param str 用分割符区分的字符
	 * @param splitFlag 分割符
	 * @return 返回转换成的List
	 */
	public static List<String> convStr2List(String str, String splitFlag) {
		if(StringUtils.isEmpty(str)){
			return new ArrayList<String>();
		}
		List<String> newListStr = new ArrayList<String>(); 
		String[] strs  = str.split(",");
		for(int i=0; i<strs.length; i++){
			newListStr.add(strs[i]);
		}
		return newListStr;
	}
	
	/**
	 * solr查询中query检索词特殊字符的处理
	 * @param queryWord 查询关键字
	 */
	public static String escapeQueryChars(String queryWord) {
		if(null == queryWord){
			return null;
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < queryWord.length(); i++) {
			char c = queryWord.charAt(i);
	        //These characters are part of the query syntax and must be escaped
			if (c == '\\' || c == '+' || c == '-' || c == '!' || c == '(' || c == ')' || c == ':'
//		        || c == '^' || c == '[' || c == ']' || c == '\"' || c == '{' || c == '}' || c == '~'
				|| c == '^' || c == '[' || c == ']' || c == '{' || c == '}' || c == '~'
		        || c == '*' || c == '?' || c == '|' || c == '&' || c == ';' || c == '/'
		        || Character.isWhitespace(c)) {
		    	sb.append('\\');
		    }
			sb.append(c);
	    }
	    return sb.toString();
	}
}
