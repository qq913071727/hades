package cn.conac.rc.solr.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.service.GenericService;
import cn.conac.rc.solr.entity.VOrgsCompQueryEntity;
import cn.conac.rc.solr.repository.VOrgsCompQueryRepository;

@Service
public class VOrgsCompQueryService extends GenericService<VOrgsCompQueryEntity, String> {

	@Autowired
	private VOrgsCompQueryRepository repository;

	/**
	 * 根据部门基本ID获取综合查询所有信息（包含职能职责和内设机构名称）
	 * @return List<VOrgsCompQueryEntity>
	 */
	public List<VOrgsCompQueryEntity> findAllInfo(){
//		Sort sort = new Sort(Direction.ASC, "organName");
		List<VOrgsCompQueryEntity> vOrgsCompQueryEntityList =  repository.findAll();
		return vOrgsCompQueryEntityList;
	}
	
}
