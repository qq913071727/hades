package cn.conac.rc.solr.vo;

import org.apache.solr.common.SolrDocumentList;

public class DutyCorrAnalyVo {
	
	/**
	 * 机构主表Id
	 */
	private String orgId;
	/**
	 * 机构名称
	 */
	private String  orgName;
	
	/**
	 * 机构类别
	 */
	private String  orgType;
	/**
	 * 机构所属系统
	 */
	private String  ownSys;
	/**
	 * 机构规格
	 */
	private String  mechStandard;
	
	/**
	 * 三定分析的部门职能职责列表
	 */
	private SolrDocumentList dutyList;
	
	/**
	 * 三定分析的内设机构职能职责
	 */
	private SolrDocumentList deptList;
	
	/**
	 * 三定分析的权力责任事项
	 */
	private SolrDocumentList itemList;
	
	public DutyCorrAnalyVo(){
		dutyList = new SolrDocumentList();
		deptList = new SolrDocumentList();
		itemList = new SolrDocumentList();
	}
		
	/**
	 * 取得机构主表Id
	 */
	public String getOrgId() {
		return orgId;
	}
	/**
	 * 设置机构主表Id
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	/**
	 * 取得机构名称
	 */
	public String getOrgName() {
		return orgName;
	}
	/**
	 * 设置机构名称
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public SolrDocumentList getDutyList() {
		return dutyList;
	}

	public void setDutyList(SolrDocumentList dutyList) {
		this.dutyList = dutyList;
	}

	public SolrDocumentList getDeptList() {
		return deptList;
	}

	public void setDeptList(SolrDocumentList deptList) {
		this.deptList = deptList;
	}

	public SolrDocumentList getItemList() {
		return itemList;
	}

	public void setItemList(SolrDocumentList itemList) {
		this.itemList = itemList;
	}

	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	public String getOwnSys() {
		return ownSys;
	}

	public void setOwnSys(String ownSys) {
		this.ownSys = ownSys;
	}

	public String getMechStandard() {
		return mechStandard;
	}

	public void setMechStandard(String mechStandard) {
		this.mechStandard = mechStandard;
	}
}
