package cn.conac.rc.solr.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.solr.common.util.StrUtils;
import cn.conac.rc.solr.config.SolrConfig;
import cn.conac.rc.solr.entity.ResultEntity;
import cn.conac.rc.solr.service.SearchService;
import cn.conac.rc.solr.vo.DutyCorrAnalyVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="solr/searchInfo/") 
public class SearchController {

	@Autowired
	SearchService searchService;
	
	@Autowired
	private SolrConfig solrConfig;

	@ApiOperation(value = "检索机构信息", httpMethod = "POST", response = ResultEntity.class, notes = "根据输入关键字和区域ID检索机构信息")
	@RequestMapping(value = "org/", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> searchOrgInfo(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody Map<String, String> condMap) {
		// 声明返回结果集SearchCondVo
		ResultPojo result = new ResultPojo();
		String areaIds  = condMap.get("areaIds");
		String parentId  = condMap.get("parentId");
		List<String> areaIdList = new ArrayList<String>();
	    areaIdList = StrUtils.convStr2List(areaIds,",");
		// service调用
		ResultEntity searchedResult = searchService.doOrgQuery(StrUtils.escapeQueryChars(condMap.get("queryName")),
										areaIdList, 
										parentId,
										condMap.get("startNum"),
										condMap.get("appFlag"));
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(searchedResult);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "检索历史沿革机构信息", httpMethod = "POST", response = ResultEntity.class, notes = "根据输入关键字和区域ID检索历史沿革机构信息")
	@RequestMapping(value = "histOrg/", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> searchHistOrgInfo(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody Map<String, String> condMap) {
		// 声明返回结果集SearchCondVo
		ResultPojo result = new ResultPojo();
		String areaIds  = condMap.get("areaIds");
		String parentId  = condMap.get("parentId");
		List<String> areaIdList = new ArrayList<String>();
		areaIdList = StrUtils.convStr2List(areaIds,",");
		// service调用
		ResultEntity searchedResult = searchService.doHistOrgQuery(StrUtils.escapeQueryChars(condMap.get("queryName")),
										areaIdList, 
										parentId,
										condMap.get("startNum"));
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(searchedResult);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "检索机构三定分析信息", httpMethod = "POST", response = ResultEntity.class, notes = "根据输入关键字、区域ID、是否是历史标示、批文开始时间和批文结束时间检索机构三定分析信息")
	@RequestMapping(value = "orgAnaly/", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> searchOrgAnalyInfo(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody Map<String, String> condMap) {
		// 声明返回结果集SearchCondVo
		ResultPojo result = new ResultPojo();
		String areaIds  = condMap.get("areaIds");
		List<String> areaIdList = new ArrayList<String>();
		areaIdList = StrUtils.convStr2List(areaIds,",");
		// service调用
		List<DutyCorrAnalyVo> resultList = searchService.doOrgAnalyQuery(StrUtils.escapeQueryChars(condMap.get("queryName")),
										areaIdList, 
										condMap.get("currFlag"),
										condMap.get("beginDateStr"),
										condMap.get("endDateStr"));
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(resultList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "检索机构提示词信息", httpMethod = "POST", response = ResultEntity.class, notes = "根据输入关键字检索机构提示词信息")
	@RequestMapping(value = "suggest/org/", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> searchOrgSuggInfo(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "条件对象", required = true) @RequestBody Map<String, String> condMap) {
		// 声明返回结果集SearchCondVo
		ResultPojo result = new ResultPojo();
		
		String areaIds  = condMap.get("areaIds");
		List<String> areaIdList = new ArrayList<String>();
		areaIdList = StrUtils.convStr2List(areaIds,",");
		
		String typeFlags  = condMap.get("typeFlags");
		List<String> typeFlagsList = new ArrayList<String>();
		typeFlagsList = StrUtils.convStr2List(typeFlags,",");
		
		// service调用
		List<String> resutList = searchService.doSuggestQuery(StrUtils.escapeQueryChars(condMap.get("queryWord")),  areaIdList, typeFlagsList);
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(resutList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "检索历史沿革机构提示词信息", httpMethod = "GET", response = ResultEntity.class, notes = "根据输入关键字检索历史沿革机构提示词信息")
	@RequestMapping(value = "suggest/histOrg/{queryWord}", method = RequestMethod.GET)
	public ResponseEntity<ResultPojo> searchHistOrgSuggInfo(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "queryWord", required = true) @PathVariable("queryWord") String queryWord) {
		// 声明返回结果集SearchCondVo
		ResultPojo result = new ResultPojo();
		
		// service调用
		List<String> resutList = searchService.doCommSuggestQuery(StrUtils.escapeQueryChars(queryWord), solrConfig.getHistOrgSuggColl());
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(resutList);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "检索机构批文信息", httpMethod = "POST", response = ResultEntity.class, notes = "根据输入关键字（批文名或者批文号）、区域IDs、批文开始时间和批文结束时间检索机构批文信息")
	@RequestMapping(value = "orgApproval/", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> searchOrgApproInfo(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody Map<String, String> condMap) {
		// 声明返回结果集SearchCondVo
		ResultPojo result = new ResultPojo();
		String areaIds  = condMap.get("areaIds");
		List<String> areaIdList = new ArrayList<String>();
		areaIdList = StrUtils.convStr2List(areaIds,",");
		// service调用
		ResultEntity searchedResult = searchService.doOrgApproQuery(StrUtils.escapeQueryChars(condMap.get("queryWord")),
										areaIdList, 
										condMap.get("startNum"),
										condMap.get("beginDateStr"),
										condMap.get("endDateStr"));
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(searchedResult);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
