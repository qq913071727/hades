package cn.conac.rc.solr.service;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import cn.conac.rc.ofs.mq.vo.MqMessageVo;

@Service
public class MqReceiveService{

    @JmsListener(destination = "testQuene", containerFactory = "myJmsContainerFactory")
    public void receiveMessage(String message) {
    	// TODO 封装维护索引的业务逻辑
        System.out.println("Received <" + message + ">");
    }
    
   @JmsListener(destination = "ofsOrgQuene", containerFactory = "myJmsContainerFactory")
    public void receiveOfsMessage(MqMessageVo mqMsgVo) {
	    // TODO 封装维护索引的业务逻辑
        System.out.println("Received orgID<" + mqMsgVo.getId() + ">");
        System.out.println("Received flag<" + mqMsgVo.getFlag() + ">");
    }
}
