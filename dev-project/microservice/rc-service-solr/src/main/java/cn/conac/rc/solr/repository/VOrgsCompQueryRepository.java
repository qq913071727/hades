package cn.conac.rc.solr.repository;

import org.springframework.stereotype.Repository;

import cn.conac.rc.framework.repository.GenericDao;
import cn.conac.rc.solr.entity.VOrgsCompQueryEntity;

@Repository
public interface VOrgsCompQueryRepository extends GenericDao<VOrgsCompQueryEntity, String> {
	
}
