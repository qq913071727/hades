package cn.conac.rc.solr.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="spring.solr")
public class SolrConfig {
	
	/**
	 * solr的http地址配置
	 */
	private String host;
	
	/**
	 * solr的ZK地址配置
	 */
	private String zkHost;
	
	/**
	 * 机构检索solr端配置的Collection名
	 */
	private String orgColl;
	
	/**
	 * 机构三定分析solr端配置的Collection名
	 */
	private String orgAnalyColl;
	
	/**
	 * 机构检索提示词配置的Collection名
	 */
	private String orgSuggColl;
	
	
	/**
	 * 历史沿革机构检索solr端配置的Collection名
	 */
	private String histOrgColl;
	
	/**
	 * 历史沿革机构检索提示词配置的Collection名
	 */
	private String histOrgSuggColl;
	
	/**
	 * 机构检索分页每页显示的页数配置
	 */
	private int pageCount;
	
	/**
	 * app机构检索分页每页显示的页数配置
	 */
	private int appPageCount;
	
	
	/**
	 * 手机端设置需要检索的字段
	 */
	private String phoneOrgViewFields;
	
	/**
	 * 高亮显示设置的前段样式
	 */
	private String preHighlight;
	
	/**
	 * 高亮显示设置的后段样式
	 */
	private String postHighlight;
	
	/**
	 * 三定分析查询每页显示的页数配置
	 */
	private int orgAnalyPageCount;
	
	/**
	 * 三定信息综合查询配置的Collection名
	 */
	private String orgCompQueryColl;
	
	/**
	 * 三定批文信息查询配置的Collection名
	 */
	private String approQueryColl;
	
	/**
	 * 综合和批文查询提示词显示个数
	 */
	private int suggCount;
	
		
	public String getHost() {
	    return host;
	}
	
	public void setHost(String host) {
	    this.host = host;
	}
	
	public String getZkHost() {
	    return zkHost;
	}
	
	public void setZkHost(String zkHost) {
	    this.zkHost = zkHost;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public int getAppPageCount() {
		return appPageCount;
	}

	public void setAppPageCount(int appPageCount) {
		this.appPageCount = appPageCount;
	}

	public String getPhoneOrgViewFields() {
		return phoneOrgViewFields;
	}

	public void setPhoneOrgViewFields(String phoneOrgViewFields) {
		this.phoneOrgViewFields = phoneOrgViewFields;
	}

	public String getPreHighlight() {
		return preHighlight;
	}

	public void setPreHighlight(String preHighlight) {
		this.preHighlight = preHighlight;
	}

	public String getPostHighlight() {
		return postHighlight;
	}

	public void setPostHighlight(String postHighlight) {
		this.postHighlight = postHighlight;
	}

	public String getOrgColl() {
		return orgColl;
	}

	public void setOrgColl(String orgColl) {
		this.orgColl = orgColl;
	}

	public String getOrgAnalyColl() {
		return orgAnalyColl;
	}

	public void setOrgAnalyColl(String orgAnalyColl) {
		this.orgAnalyColl = orgAnalyColl;
	}

	public String getOrgSuggColl() {
		return orgSuggColl;
	}

	public void setOrgSuggColl(String orgSuggColl) {
		this.orgSuggColl = orgSuggColl;
	}

	public String getHistOrgColl() {
		return histOrgColl;
	}

	public void setHistOrgColl(String histOrgColl) {
		this.histOrgColl = histOrgColl;
	}

	public String getHistOrgSuggColl() {
		return histOrgSuggColl;
	}

	public void setHistOrgSuggColl(String histOrgSuggColl) {
		this.histOrgSuggColl = histOrgSuggColl;
	}

	public int getOrgAnalyPageCount() {
		return orgAnalyPageCount;
	}

	public void setOrgAnalyPageCount(int orgAnalyPageCount) {
		this.orgAnalyPageCount = orgAnalyPageCount;
	}

	public String getOrgCompQueryColl() {
		return orgCompQueryColl;
	}

	public void setOrgCompQueryColl(String orgCompQueryColl) {
		this.orgCompQueryColl = orgCompQueryColl;
	}

	public String getApproQueryColl() {
		return approQueryColl;
	}

	public void setApproQueryColl(String approQueryColl) {
		this.approQueryColl = approQueryColl;
	}

	public int getSuggCount() {
		return suggCount;
	}

	public void setSuggCount(int suggCount) {
		this.suggCount = suggCount;
	}
}