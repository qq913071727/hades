package cn.conac.rc.solr.service;

import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.solr.common.util.SearchUtils;
import cn.conac.rc.solr.entity.IndexEntity;

@Service
public class IndexService {
	
	@Autowired
	private CloudSolrClient cloudSolrClient;
	
	/**
	 * 保存索引
	 * @param indexEntity 索引相关数据O
	 * @return  ret (0: 正常  -1：异常)
	 */
	public int saveIndex(IndexEntity indexEntity) {
		// 先将collection中索引的index清除
		int ret = SearchUtils.deleteIndexByCondition(indexEntity, cloudSolrClient);
		if(ret == -1){
			return ret;
		}
		// 保存要索引的index数据
		ret = SearchUtils.saveIndex(indexEntity, cloudSolrClient);
		return ret;
	}
	
	/**
	 * 更新索引
	 * @param indexEntity 索引相关数据
	 * @return  ret (0: 正常  -1：异常)
	 */ 
	public int updateIndex(IndexEntity indexEntity){
		
		int ret = SearchUtils.updateIndexByIdInfo(indexEntity, cloudSolrClient);
		return ret;
	}

	/**
	 * 根据索引数据ID相关信息删除索引
	 * @param indexEntity 索引相关数据
	 * @return  ret (0: 正常  -1：异常)
	 */
	public int deleteIndex(IndexEntity indexEntity){
		
		int ret = SearchUtils.deleteIndexByIdInfo(indexEntity, cloudSolrClient);
		return ret;
	}

}
