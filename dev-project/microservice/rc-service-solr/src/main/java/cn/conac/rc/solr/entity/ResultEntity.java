package cn.conac.rc.solr.entity;

/**
 * solr搜索结果Entity类
 */
public class ResultEntity {
	
	/**
	 * 检索到的总数量
	 */ 
	public long numFound = 0;
	
	/**
	 * 检索结果
	 */ 
	public Object searchedResult = null;

	public long getNumFound() {
		return numFound;
	}

	public void setNumFound(long numFound) {
		this.numFound = numFound;
	}

	public Object getSearchedResult() {
		return searchedResult;
	}

	public void setSearchedResult(Object searchedResult) {
		this.searchedResult = searchedResult;
	}
}
