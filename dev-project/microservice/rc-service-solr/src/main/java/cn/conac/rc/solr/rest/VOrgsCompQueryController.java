package cn.conac.rc.solr.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.conac.rc.framework.vo.ResultPojo;
import cn.conac.rc.solr.common.util.BeanUtils;
import cn.conac.rc.solr.common.util.StrUtils;
import cn.conac.rc.solr.config.SolrConfig;
import cn.conac.rc.solr.entity.IndexEntity;
import cn.conac.rc.solr.entity.ResultEntity;
import cn.conac.rc.solr.entity.VOrgsCompQueryEntity;
import cn.conac.rc.solr.service.IndexService;
import cn.conac.rc.solr.service.SearchService;
import cn.conac.rc.solr.service.VOrgsCompQueryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value="solr/compInfo/")
public class VOrgsCompQueryController {

	@Autowired
	VOrgsCompQueryService service;
	
	@Autowired
	IndexService  indexService;
	
	@Autowired
	SearchService searchService;
	
	@Autowired
	private SolrConfig solrConfig;

	@ApiOperation(value = "综合索引信息", httpMethod = "POST", response = Integer.class, notes = "生成三定综合查询的索引信息")
	@RequestMapping(value = "index/", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> indexInfo(HttpServletRequest request, HttpServletResponse response) {
		// 声明返回结果集
		ResultPojo result = new ResultPojo();
		int ret = 0;
		// service调用
		List<VOrgsCompQueryEntity> vOrgsCompQueryList = service.findAllInfo();
		IndexEntity indexEntity = new IndexEntity();
		Map<String, Object> map = null;
		for(VOrgsCompQueryEntity vOrgsCompQueryEntity : vOrgsCompQueryList){
			map = BeanUtils.transBean2Map(vOrgsCompQueryEntity);
			indexEntity.getIndexData().add(map);
		}
		indexEntity.setCollectionName(solrConfig.getOrgCompQueryColl());
		ret = indexService.saveIndex(indexEntity);
		
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(Integer.valueOf(ret));
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "综合查询信息", httpMethod = "POST", response = ResultEntity.class, notes = "根据输入组合条件进行综合查询")
	@RequestMapping(value = "org/", method = RequestMethod.POST)
	public ResponseEntity<ResultPojo> searchOrgCompInfo(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "查询条件对象", required = true) @RequestBody Map<String, String> condMap) {
		// 声明返回结果集SearchCondVo
		ResultPojo result = new ResultPojo();
		
		List<String> areaIdList = new ArrayList<String>();
		List<String> ownSysList = new ArrayList<String>();
		List<String> orgTypeList = new ArrayList<String>();
		List<String> mechStandardList = new ArrayList<String>();
		List<String> empNumStrList = new ArrayList<String>();
		List<String> orgLeaderNumStrList = new ArrayList<String>();
		
		String areaIds  = condMap.get("areaIds");
		String ownSyss  = condMap.get("ownSyss");
		String orgTypes  = condMap.get("orgTypes");
		String mechStandards  = condMap.get("mechStandards");
		String empNumStrs  = condMap.get("empNumStrs");
		String orgLeaderNumStrs  = condMap.get("orgLeaderNumStrs");
		
		areaIdList = StrUtils.convStr2List(areaIds,",");
		ownSysList = StrUtils.convStr2List(ownSyss,",");
		orgTypeList = StrUtils.convStr2List(orgTypes,",");
		mechStandardList = StrUtils.convStr2List(mechStandards,",");
		empNumStrList = StrUtils.convStr2List(empNumStrs,",");
		orgLeaderNumStrList = StrUtils.convStr2List(orgLeaderNumStrs,",");
		
		// service调用
		ResultEntity searchedResult = searchService.doOrgCompQuery(
													areaIdList,
													ownSysList,
													orgTypeList,
													mechStandardList,
													empNumStrList,
													orgLeaderNumStrList,
													StrUtils.escapeQueryChars(condMap.get("organName")),
													StrUtils.escapeQueryChars(condMap.get("deptNames")),
													StrUtils.escapeQueryChars(condMap.get("dutyContents")),
													condMap.get("startNum")
											 );
		// 结果集设定
		result.setCode(ResultPojo.CODE_SUCCESS);
		result.setResult(searchedResult);
		result.setMsg(ResultPojo.MSG_SUCCESS);
		return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
	}
}
