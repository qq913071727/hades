package cn.conac.rc.solr;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.SimpleJmsListenerContainerFactory;

@SpringBootApplication
@EnableDiscoveryClient
//@PropertySource({"classpath:zkHost.properties"})
public class Application {

	@Bean
    JmsListenerContainerFactory<?> myJmsContainerFactory(ActiveMQConnectionFactory connectionFactory) {
        SimpleJmsListenerContainerFactory factory = new SimpleJmsListenerContainerFactory();
        connectionFactory.setTrustAllPackages(true);
        factory.setConnectionFactory(connectionFactory);
        return factory;
    }
	
	public static void main(String[] args) {
		
		SpringApplication.run(Application.class, args);
	}
}
