package cn.conac.rc.solr.config;

import java.io.IOException;

import javax.annotation.PreDestroy;

import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

@Configuration
@EnableConfigurationProperties(SolrConfig.class)
public class SolrClientConfig {
	
	@Autowired
	private SolrConfig solrConfig;
	
	private CloudSolrClient cloudSolrClient;
	
	@PreDestroy
	public void close() {
	    if (this.cloudSolrClient != null) {
	        try {
	            this.cloudSolrClient.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	}
	@Bean 
	public CloudSolrClient cloudSolrClient(){
	    if (StringUtils.hasText(this.solrConfig.getZkHost())) {
	    	cloudSolrClient = new CloudSolrClient(this.solrConfig.getZkHost());
	    }
	    return this.cloudSolrClient;
	}
}