package cn.conac.rc.solr.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.rc.framework.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@Entity
@Table(name="RCO_ORGS_COMP_QUERY")
public class VOrgsCompQueryEntity extends BaseEntity<VOrgsCompQueryEntity> implements Serializable {

	private static final long serialVersionUID = 1473752895339804544L;

	@ApiModelProperty("部门基本表ID")
	private String baseId;

	@ApiModelProperty("区域ID")
	private String areaId;

	@ApiModelProperty("部门名称")
	private String organName;

	@ApiModelProperty("所属系统")
	private String ownSys;

	@ApiModelProperty("部门类型")
	private String orgType;

	@ApiModelProperty("机构规格")
	private String mechStandard;

	@ApiModelProperty("编制总数")
	private String empNumStr;

	@ApiModelProperty("领导职数")
	private String orgLeaderNumStr;

	@ApiModelProperty("内设机构名称(名称间用<br>进行分割)")
	private String deptNames;

	@ApiModelProperty("职能职责内容（内容间用<br>进行分割）")
	private String dutyContents;
	
	@ApiModelProperty("内设机构编码（编码间用<br>进行分割）")
	private String deptCodes;

	@ApiModelProperty("职能职责编码（编码间用<br>进行分割）")
	private String dutyCodes;

	public void setBaseId(String baseId){
		this.baseId=baseId;
	}

	public String getBaseId(){
		return baseId;
	}

	public void setAreaId(String areaId){
		this.areaId=areaId;
	}

	public String getAreaId(){
		return areaId;
	}

	public void setOrganName(String organName){
		this.organName=organName;
	}

	public String getOrganName(){
		return organName;
	}

	public void setOwnSys(String ownSys){
		this.ownSys=ownSys;
	}

	public String getOwnSys(){
		return ownSys;
	}

	public void setOrgType(String orgType){
		this.orgType=orgType;
	}

	public String getOrgType(){
		return orgType;
	}

	public void setMechStandard(String mechStandard){
		this.mechStandard=mechStandard;
	}

	public String getMechStandard(){
		return mechStandard;
	}

	public void setEmpNumStr(String empNumStr){
		this.empNumStr=empNumStr;
	}

	public String getEmpNumStr(){
		return empNumStr;
	}

	public void setOrgLeaderNumStr(String orgLeaderNumStr){
		this.orgLeaderNumStr=orgLeaderNumStr;
	}

	public String getOrgLeaderNumStr(){
		return orgLeaderNumStr;
	}

	public void setDeptNames(String deptNames){
		this.deptNames=deptNames;
	}

	public String getDeptNames(){
		return deptNames;
	}

	public void setDutyContents(String dutyContents){
		this.dutyContents=dutyContents;
	}

	public String getDutyContents(){
		return dutyContents;
	}

	public String getDeptCodes() {
		return deptCodes;
	}

	public void setDeptCodes(String deptCodes) {
		this.deptCodes = deptCodes;
	}

	public String getDutyCodes() {
		return dutyCodes;
	}

	public void setDutyCodes(String dutyCodes) {
		this.dutyCodes = dutyCodes;
	}

}
