package cn.conac.rc.solr.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * solr相关检索条件Entity类
 */
public class SearchCondEntity {

	/**
	 * 搜索关键词
	 */
	private String queryWord = "*:*";
	
	/**
	 * 指定搜索域，并且默认的关系是OR
	 */
	private List<String> assignmentFields = new ArrayList<String>();
	
	/**
	 * 指定搜索域与搜索域之间的关系
	 * Map<String,String>
	 *         第一个String是域名，比如：name
	 *         第二个String是关系，写法：或者：OR 与：AND
	 */
	private List<Map<String,String>> assignFields = new ArrayList<Map<String,String>>();
	
	/**
	 * 显示域
	 */
	private String[] viewFields;
	
	/**
	 * 是否高亮
	 */
	private Boolean isHighlight = true;
	
	/**
	 * 高亮域
	 */
	private List<String> highlightFields = new ArrayList<String>();
	
	/**
	 * 高亮前缀
	 */
	private String preHighlight = "<font color='red'>";
	
	/**
	 * 高亮后缀
	 */
	private String postHighlight = "</font>";
	
	/**
	 * 排序域 String:需要排序的域名，Boolean：true 升序 false 降序
	 */
	private Map<String,Boolean> sortField = new HashMap<String,Boolean>();
	
	/**
	 * 过滤域
	 *   注意点：
	 *   比如查询多个ID场合 areaID:XXXX01 OR areaID:XXXX02 OR areaID:XXXX03
	 *   Map中key与value的值:
	 *   key:areaID
	 *   value: XXXX01 OR areaID:XXXX02 OR areaID:XXXX03
	 */
	private Map<String,String> filterField = new HashMap<String,String>();
	
	/**
	 * 开始行
	 */
	private int startNum = 0;
	
	/**
	 * 一页显示多少行
	 */
	private int pageCount = 10;
	
	/**
	 * 显示结果的字数
	 */
	private int viewNums = 200;
	
	/**
	 * 指定solr中要查询的仓库名
	 */
	private String collectionName = null;
	
	/**
	 * 指定solr高亮显示是是否匹配字段(默认为false)
	 */
	private boolean hlRequireFieldMatch = false;
	
	
	public String getQueryWord() {
		return queryWord;
	}

	public void setQueryWord(String queryWord) {
		this.queryWord = queryWord;
	}

	public List<String> getAssignmentFields() {
		return assignmentFields;
	}

	public void setAssignmentFields(List<String> assignmentFields) {
		this.assignmentFields = assignmentFields;
	}

	public String[] getViewFields() {
		return viewFields;
	}

	public void setViewFields(String[] viewFields) {
		this.viewFields = viewFields;
	}

	public List<String> getHighlightFields() {
		return highlightFields;
	}

	public void setHighlightFields(List<String> highlightFields) {
		this.highlightFields = highlightFields;
	}

	public String getPreHighlight() {
		return preHighlight;
	}

	public void setPreHighlight(String preHighlight) {
		this.preHighlight = preHighlight;
	}

	public String getPostHighlight() {
		return postHighlight;
	}

	public void setPostHighlight(String postHighlight) {
		this.postHighlight = postHighlight;
	}

	public Map<String, Boolean> getSortField() {
		return sortField;
	}

	public void setSortField(Map<String, Boolean> sortField) {
		this.sortField = sortField;
	}

	public Map<String, String> getFilterField() {
		return filterField;
	}

	public void setFilterField(Map<String, String> filterField) {
		this.filterField = filterField;
	}

	public int getStartNum() {
		return startNum;
	}

	public void setStartNum(int startNum) {
		this.startNum = startNum;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public int getViewNums() {
		return viewNums;
	}

	public void setViewNums(int viewNums) {
		this.viewNums = viewNums;
	}

	public List<Map<String, String>> getAssignFields() {
		return assignFields;
	}

	public void setAssignFields(List<Map<String, String>> assignFields) {
		this.assignFields = assignFields;
	}

	public Boolean getIsHighlight() {
		return isHighlight;
	}

	public void setIsHighlight(Boolean isHighlight) {
		this.isHighlight = isHighlight;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

	public boolean getHlRequireFieldMatch() {
		return hlRequireFieldMatch;
	}

	public void setHlRequireFieldMatch(boolean hlRequireFieldMatch) {
		this.hlRequireFieldMatch = hlRequireFieldMatch;
	}
}
