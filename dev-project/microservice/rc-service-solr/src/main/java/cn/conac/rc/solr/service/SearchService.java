package cn.conac.rc.solr.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.conac.rc.framework.utils.StringUtils;
import cn.conac.rc.solr.common.util.SearchUtils;
import cn.conac.rc.solr.common.util.StrUtils;
import cn.conac.rc.solr.config.SolrConfig;
import cn.conac.rc.solr.entity.ResultEntity;
import cn.conac.rc.solr.entity.SearchCondEntity;
import cn.conac.rc.solr.vo.DutyCorrAnalyVo;

@Service
public class SearchService {
	
	@Autowired
	private CloudSolrClient cloudSolrClient;
	
	@Autowired
	private SolrConfig solrConfig;
	
	/**
	 * 根据检索关键字和区域IDS，使用Solr分布式集群对三定信息进行综合查询
	 * 
	 * @param areaIds：用户所在的地区IDs（比如市级可以查看所有的区县，此时为ID的list）
	 * @param ownSyss：所属系统的List
	 * @param orgTypes：机构类别的List
	 * 	@param mechStandards：机构规格的List
	 * 	@param empNumStrs：编制总数的List(格式为： 开始值@@结束值)
	 * @param orgLeaderNumStrs：领导职数的List(格式为： 开始值@@结束值)
	 * @param organName：页面中输入的部门名称的组合（直接字符串连接）
	 * @param deptName：页面中输入的内设机构名称的组合（直接字符串连接）
	 * @param dutyContent：页面中输入的职责内容的组合（直接字符串连接）
	 * @param startNum：不分页场合默认为0，分页场合值为当前页的开始行数
	 * @return  返回查询结果的机构信息并高亮化机构名称
	 */
	public ResultEntity  doOrgCompQuery(List<String> areaIds, List<String> ownSyss, List<String> orgTypes,  
								List<String> mechStandards, List<String> empNumStrs,  List<String> orgLeaderNumStrs, 
								String organName, String deptName, String dutyContent, String startNum){
		// 没有找到完全匹配结果时的返回的对象结果集
		ResultEntity resultEntity = new ResultEntity();
		
		// 封装检索相关的条件bean
		// 设置查询关键字
		SearchCondEntity searchCondEntity = new SearchCondEntity();
		
		StringBuffer sbQueryWords =new StringBuffer();
		
		// 拼接组合查询条件
		if(null !=organName && null ==deptName &&  null == dutyContent) {
			sbQueryWords.append("organName:" + organName);
		} else if(null !=organName && null ==deptName && null != dutyContent ){
			sbQueryWords.append("organName:" + organName);
			sbQueryWords.append("#AND#");
			sbQueryWords.append("dutyContents:" + dutyContent);
		} else if(null !=organName && null !=deptName && null == dutyContent ){
			sbQueryWords.append("organName:" + organName);
			sbQueryWords.append("#AND#");
			sbQueryWords.append("deptNames:" + deptName);
		} else if(null !=organName && null !=deptName && null != dutyContent ){
			sbQueryWords.append("organName:" + organName);
			sbQueryWords.append("#AND#");
			sbQueryWords.append("deptNames:" + deptName);
			sbQueryWords.append("#AND#");
			sbQueryWords.append("dutyContents:" + dutyContent);
		} else if(null ==organName && null ==deptName && null != dutyContent ){
			sbQueryWords.append("dutyContents:" + dutyContent);
		} else if(null ==organName && null !=deptName && null == dutyContent ){
			sbQueryWords.append("deptNames:" + deptName);
		} else if(null ==organName && null !=deptName && null != dutyContent ){
			sbQueryWords.append("deptNames:" + deptName);
			sbQueryWords.append("#AND#");
			sbQueryWords.append("dutyContents:" + dutyContent);
		} else {
			sbQueryWords.append("*:*");
		}
		
		searchCondEntity.setQueryWord(sbQueryWords.toString());
		
		if(sbQueryWords.indexOf("organName:") == -1 && 
				(sbQueryWords.indexOf("deptNames:") == -1) &&
				(sbQueryWords.indexOf("dutyContents:") == -1)){
			searchCondEntity.setIsHighlight(false);
		} else {
			//设置是否高亮显示
			searchCondEntity.setIsHighlight(true);
			// 设置高亮显示的样式
			searchCondEntity.setPreHighlight(solrConfig.getPreHighlight());
			searchCondEntity.setPostHighlight(solrConfig.getPostHighlight());
			searchCondEntity.setHlRequireFieldMatch(true);
		}
		// 设置高亮显示的字段
		if(sbQueryWords.indexOf("organName:") != -1){
			searchCondEntity.getHighlightFields().add("organName");
		}
		if(sbQueryWords.indexOf("deptNames:") != -1){
			searchCondEntity.getHighlightFields().add("deptNames");
		}
		if(sbQueryWords.indexOf("dutyContents:") != -1){
			searchCondEntity.getHighlightFields().add("dutyContents");
		}
		
		// 设置过滤域
		if(  null != areaIds && areaIds.size() > 0){
			searchCondEntity.getFilterField().put("areaId", StrUtils.convfilterFieldStr(areaIds,"areaId","OR"));
		}
		if(  null != ownSyss && ownSyss.size() > 0){
			searchCondEntity.getFilterField().put("ownSys", StrUtils.convfilterFieldStr(ownSyss,"ownSys","OR"));
		}
		if(  null != orgTypes && orgTypes.size() > 0){
			searchCondEntity.getFilterField().put("orgType", StrUtils.convfilterFieldStr(orgTypes,"orgType","OR"));
		}
		if(  null != mechStandards && mechStandards.size() > 0){
			searchCondEntity.getFilterField().put("mechStandard", StrUtils.convfilterFieldStr(mechStandards,"mechStandard","OR"));
		}
		if(  null != empNumStrs && empNumStrs.size() > 0){
			searchCondEntity.getFilterField().put("empNumStr", StrUtils.convfilterFieldRangeStr(empNumStrs,"empNumStr","OR", "@@"));
		}
		if(  null != orgLeaderNumStrs && orgLeaderNumStrs.size() > 0){
			searchCondEntity.getFilterField().put("orgLeaderNumStr", StrUtils.convfilterFieldRangeStr(orgLeaderNumStrs,"orgLeaderNumStr","OR", "@@"));
		}
		// 设置开始行号
		if(!StringUtils.isEmpty(startNum)){
			searchCondEntity.setStartNum(Integer.parseInt(startNum));
		}
		
		// 设置显示每页显示行数
		searchCondEntity.setPageCount(solrConfig.getPageCount());
		
		// 设置机构检索要查询的仓库名
		searchCondEntity.setCollectionName(solrConfig.getOrgCompQueryColl());
		// 执行检索操作
	    resultEntity = SearchUtils.doCommQuery(searchCondEntity, cloudSolrClient);
		
		return resultEntity;
	}
	
	/**
	 * 根据检索关键字和区域IDS，使用Solr分布式集群对机构信息进行检索
	 * 
	 * @param queryWord：从前台页面输入的要查询的关键字
	 * @param areaIds：用户所在的地区IDs（比如市级可以查看所有的区县，此时为ID的list）
	 * @param parentId 主管部门ID
	 * @param startNum：不分页场合默认为0，分页场合值为当前页的开始行数
	 * @param appFlag：手机端场合为1，电脑端场合为2
	 * @return  返回查询结果的机构信息并高亮化机构名称
	 */
	public ResultEntity  doOrgQuery(String queryWord, List<String> areaIds, String parentId, String startNum, String appFlag){
		// 没有找到完全匹配结果时的返回的对象结果集
		ResultEntity resultEntity = new ResultEntity();
		// 异常场合返回空的结果集
		if(null == queryWord){
			return resultEntity;
		}
		// 封装检索相关的条件bean
		// 设置查询关键字
		SearchCondEntity searchCondEntity = new SearchCondEntity();
		searchCondEntity.setQueryWord(queryWord);
		
		// 设置查询关键字对应的solr侧字段名称 
		searchCondEntity.getAssignmentFields().add("fullName");
		
		if("2".equals(appFlag)) {
			//设置是否高亮显示
			searchCondEntity.setIsHighlight(true);
			// 设置高亮显示的字段
			searchCondEntity.getHighlightFields().add("name");
			// 设置高亮显示的样式
			searchCondEntity.setPreHighlight(solrConfig.getPreHighlight());
			searchCondEntity.setPostHighlight(solrConfig.getPostHighlight());
		}
		
		// 设置过滤域
		if(  null != areaIds && areaIds.size() > 0){
			searchCondEntity.getFilterField().put("areaId", StrUtils.convfilterFieldStr(areaIds,"areaId","OR"));
		}
		
		// 委办局场合通过parentId进行过滤（委办局只能搜索到自己的下设机构部门）
		if(StringUtils.isNotBlank(parentId)){
			searchCondEntity.getFilterField().put("parentId", parentId);
		}
		
		// 设置开始行号
		if(!StringUtils.isEmpty(startNum)){
			searchCondEntity.setStartNum(Integer.parseInt(startNum));
		}
		
		// 设置显示每页显示行数
		if("2".equals(appFlag)) {
			searchCondEntity.setPageCount(solrConfig.getPageCount());
		} else{
			searchCondEntity.setPageCount(solrConfig.getAppPageCount());
		}
		
		// 当手机端使用时，设置需要显示的字段，以提高检索性能
		if("1".equals(appFlag)){
			String viewFieldsStr =solrConfig.getPhoneOrgViewFields();
			if(null != viewFieldsStr){
				searchCondEntity.setViewFields(viewFieldsStr.split(","));
			}
		}
		// 设置机构检索要查询的仓库名
		searchCondEntity.setCollectionName(solrConfig.getOrgColl());
		// 执行检索操作
	    resultEntity = SearchUtils.doCommQuery(searchCondEntity, cloudSolrClient);
		
		return resultEntity;
	}
	
	/**
	 * 根据检索关键字和区域IDS，使用Solr分布式集群对历史沿革机构信息进行检索
	 * 
	 * @param queryWord：从前台页面输入的要查询的关键字
	 * @param areaIds：用户所在的地区IDs（比如市级可以查看所有的区县，此时为ID的list）
	 * @param parentId 主管部门ID
	 * @param startNum：不分页场合默认为0，分页场合值为当前页的开始行数
	 * @return  返回查询结果的机构ID和高亮化机构名称的信息
	 */
	public ResultEntity  doHistOrgQuery(String queryWord, List<String> areaIds, String parentId, String startNum){
		// 没有找到完全匹配结果时的返回的对象结果集
		ResultEntity resultEntity = new ResultEntity();
		// 异常场合返回空的结果集
		if(null == queryWord){
			return resultEntity;
		}
		// 封装检索相关的条件bean
		// 设置查询关键字
		SearchCondEntity searchCondEntity = new SearchCondEntity();
		searchCondEntity.setQueryWord(queryWord);
		
		// 设置查询关键字对应的solr侧字段名称 
		searchCondEntity.getAssignmentFields().add("organName");
		
//		//设置是否高亮显示
//		searchCondEntity.setIsHighlight(true);
//		// 设置高亮显示的字段
//		searchCondEntity.getHighlightFields().add("organName");
//		// 设置高亮显示的样式
//		searchCondEntity.setPreHighlight(solrConfig.getPreHighlight());
//		searchCondEntity.setPostHighlight(solrConfig.getPostHighlight());
		
		// 设置过滤域
		if(  null != areaIds && areaIds.size() > 0){
			searchCondEntity.getFilterField().put("areaId", StrUtils.convfilterFieldStr(areaIds,"areaId","OR"));
		}
		// 委办局场合通过parentId进行过滤（委办局只能搜索到自己的下设机构部门）
		if(StringUtils.isNotBlank(parentId)){
			searchCondEntity.getFilterField().put("parentId", parentId);
		}
		// 设置开始行号
		if(!StringUtils.isEmpty(startNum)){
			searchCondEntity.setStartNum(Integer.parseInt(startNum));
		}
		
		// 设置显示每页显示行数
		searchCondEntity.setPageCount(solrConfig.getAppPageCount());
	
		// 设置机构检索要查询的仓库名
		searchCondEntity.setCollectionName(solrConfig.getHistOrgColl());
		// 执行检索操作
	    resultEntity = SearchUtils.doCommQuery(searchCondEntity, cloudSolrClient);
		
		return resultEntity;
	}
	
	/**
	 * 根据检索关键字和区域IDS，使用Solr分布式集群对机构三定分析信息进行检索
	 * 
	 * @param queryWord：从前台页面输入的要查询的关键字
	 * @param areaIds：用户所在的地区IDs（比如市级可以查看所有的区县，此时为ID的list）
	 * @param currFlag (1: 当前   0：历史)
	 * @param beginDateStr 批文开始时间(currFlag=0时有效)
	 * @param endDateStr 批文结束时间(currFlag=0时有效)
	 * @return  返回查询结果的机构ID和高亮化机构名称的信息
	 */
	public List<DutyCorrAnalyVo>  doOrgAnalyQuery(String queryWord, List<String> areaIds,
						String currFlag, String beginDateStr, String endDateStr){
		// 没有找到完全匹配结果时的返回的对象结果集
		ResultEntity resultEntity = new ResultEntity();
		List<DutyCorrAnalyVo> resultList = new ArrayList<DutyCorrAnalyVo>();
		// 异常场合返回空的结果集
		
		
		// 封装检索相关的条件bean
		// 设置查询关键字
		SearchCondEntity searchCondEntity = new SearchCondEntity();
		
		if(null != queryWord) {
			searchCondEntity.setQueryWord(queryWord);
			// 设置查询关键字对应的solr侧字段名称 
			searchCondEntity.getAssignmentFields().add("contentDesc");
			
			//设置是否高亮显示
			searchCondEntity.setIsHighlight(true);
			// 设置高亮显示的字段
			searchCondEntity.getHighlightFields().add("contentDesc");
			// 设置高亮显示的样式
			searchCondEntity.setPreHighlight(solrConfig.getPreHighlight());
			searchCondEntity.setPostHighlight(solrConfig.getPostHighlight());
			
		}
		// 设置显示每页显示行数
		searchCondEntity.setPageCount(solrConfig.getOrgAnalyPageCount());
		
		// 设置过滤域
		searchCondEntity.getFilterField().put("currFlag", currFlag);
		if(  null != areaIds && areaIds.size() > 0){
			searchCondEntity.getFilterField().put("areaId", StrUtils.convfilterFieldStr(areaIds,"areaId","OR"));
		}
		// 批文时间的查询条件
		if(StringUtils.isNotEmpty(beginDateStr) && StringUtils.isNotEmpty(endDateStr) 
				&& "0".equals(currFlag)){
			searchCondEntity.getFilterField().put ("appDateStr", "[" + beginDateStr + " TO " + endDateStr + "]");
		}
//		// 设置显示顺序（排序规则：按照orgName升序排，如果名称相同按照orgid升序排）
//		searchCondEntity.getSortField().put("orgName", true);
//		searchCondEntity.getSortField().put("orgId", true);
		
		// 设置机构检索要查询的仓库名
		searchCondEntity.setCollectionName(solrConfig.getOrgAnalyColl());
		// 执行检索操作
	    resultEntity = SearchUtils.doCommQuery(searchCondEntity, cloudSolrClient);
	    
	   resultList = convDutyCorrAnaly((SolrDocumentList)resultEntity.getSearchedResult());
		
		return resultList;
	}
	
	/**
	 * 根据检索关键字和区域IDS，使用Solr分布式集群对综合查询和批文查询提示词信息进行检索
	 * 
	 * @param queryWord：从前台页面输入的要提示词的关键字
	 * @param areaIds：用户所在的地区IDs（比如市级可以查看所有的区县，此时为ID的list）
	 * @param typeFlags：综合查询提示词场合为0，批文查询提示词场合为1和2
	 * @return  返回查询结果的机构提示词相关信息
	 */
	public List<String>  doSuggestQuery(String queryWord, List<String> areaIds,  List<String> typeFlags){
		// 没有找到完全匹配结果时的返回的对象结果集
		List<String> resultList = new ArrayList<String>();
		// 异常场合返回空的结果集
		if(null == queryWord){
			return resultList;
		}
		ResultEntity resultEntity = new ResultEntity();
		// 封装检索相关的条件bean
		// 设置查询关键字
		SearchCondEntity searchCondEntity = new SearchCondEntity();
		searchCondEntity.setQueryWord(queryWord);
		
		// 设置查询关键字对应的solr侧字段名称 
		searchCondEntity.getAssignmentFields().add("cureCard");
		
		// 设置区域过滤域
		if(  null != areaIds && areaIds.size() > 0){
			searchCondEntity.getFilterField().put("areaId", StrUtils.convfilterFieldStr(areaIds,"areaId","OR"));
		}
		
		// 设置类别过滤域
		if(  null != typeFlags && typeFlags.size() > 0){
			searchCondEntity.getFilterField().put("typeFlag", StrUtils.convfilterFieldStr(typeFlags,"typeFlag","OR"));
		}
		
		// 设置显示每页显示行数
		searchCondEntity.setPageCount(solrConfig.getSuggCount());
		
		// 设置机构检索要查询的仓库名
		searchCondEntity.setCollectionName(solrConfig.getOrgSuggColl());
		// 执行检索操作
	    resultEntity = SearchUtils.doCommQuery(searchCondEntity, cloudSolrClient);
	    
	    SolrDocumentList   solrDocList = (SolrDocumentList)resultEntity.getSearchedResult();
	    
	    for (SolrDocument doc : solrDocList) {
	    	resultList.add(doc.getFieldValue("cureCard").toString());
	    }
	    
		return resultList;
	}
	/**
	 * 根据检索关键字使用Solr分布式集群对机构提示词信息检索
	 * 
	 * @param queryWord：从前台页面输入的要查询的关键字
	 * @return  返回与输入的关键字匹配的机构名称的信息
	 */
	public List<String> doCommSuggestQuery(String queryWord, String collectionName ){
		// 没有找到完全匹配结果时的返回的对象结果集
		List<String> resultList = new ArrayList<String>();
		// 异常场合返回空的结果集
		if(StringUtils.isEmpty(queryWord)){
			return resultList;
		}
		// 封装检索相关的条件bean
		// 设置查询关键字
		SearchCondEntity searchCondEntity = new SearchCondEntity();
		searchCondEntity.setQueryWord(queryWord);
	
		// 设置机构检索要查询的仓库名
		searchCondEntity.setCollectionName(collectionName);
		// 执行检索操作
		resultList = SearchUtils.doSuggestQuery(searchCondEntity, cloudSolrClient);
		
		return resultList;
	}
	
	/**
	 * 根据检索关键字和区域IDS，使用Solr分布式集群对机构批文信息进行检索
	 * 
	 * @param queryWord：从前台页面输入的要查询的关键字（批文名称或者批文号）
	 * @param areaIds：用户所在的地区IDs（比如市级可以查看所有的区县，此时为ID的list）
	 * @param startNum：不分页场合默认为0，分页场合值为当前页的开始行数
	 * @param beginDateStr 批文开始时间
	 * @param endDateStr 批文结束时间
	 * @return  返回查询结果的机构批文信息并高亮化批文名称或批文号
	 */
	public ResultEntity  doOrgApproQuery(String queryWord, List<String> areaIds, String startNum, String beginDateStr, String endDateStr){
		// 没有找到完全匹配结果时的返回的对象结果集
		ResultEntity resultEntity = new ResultEntity();
		
		// 封装检索相关的条件bean
		// 设置查询关键字
		SearchCondEntity searchCondEntity = new SearchCondEntity();
		if(null != queryWord){
			searchCondEntity.setQueryWord(queryWord);
			// 设置查询关键字对应的solr侧字段名称 
			searchCondEntity.getAssignmentFields().add("appNameOrNum");
			
			//设置是否高亮显示
			searchCondEntity.setIsHighlight(true);
			// 设置高亮显示的字段
			searchCondEntity.getHighlightFields().add("appName");
			searchCondEntity.getHighlightFields().add("appNumStr");
			// 设置高亮显示的样式
			searchCondEntity.setPreHighlight(solrConfig.getPreHighlight());
			searchCondEntity.setPostHighlight(solrConfig.getPostHighlight());
		} 
		
		// 设置过滤域
		if(  null != areaIds && areaIds.size() > 0){
			searchCondEntity.getFilterField().put("areaId", StrUtils.convfilterFieldStr(areaIds,"areaId","OR"));
		}
		
		// 设置开始行号
		if(!StringUtils.isEmpty(startNum)){
			searchCondEntity.setStartNum(Integer.parseInt(startNum));
		}
		
		// 批文时间的查询条件
		if(StringUtils.isNotEmpty(beginDateStr) && StringUtils.isNotEmpty(endDateStr) ){
			searchCondEntity.getFilterField().put ("appDateStr", "[" + beginDateStr + " TO " + endDateStr + "]");
		}
				
		searchCondEntity.setPageCount(solrConfig.getPageCount());
		
		// 设置机构检索要查询的仓库名
		searchCondEntity.setCollectionName(solrConfig.getApproQueryColl());
		// 执行检索操作
	    resultEntity = SearchUtils.doCommQuery(searchCondEntity, cloudSolrClient);
		
		return resultEntity;
	}
	
	/**
	 * 将同一个部门的三定分析查询的内设机构，职能职责及权责职责汇总到一起
	 * 
	 * @param searchedDocs：搜索的结果列表
	 * @return  返回汇总后的查询结果（包含高亮显示信息）
	 */
	private List<DutyCorrAnalyVo>  convDutyCorrAnaly(SolrDocumentList searchedDocs){
		if(null == searchedDocs || searchedDocs.isEmpty() == true){
			return new ArrayList<DutyCorrAnalyVo>();
		}
		List<DutyCorrAnalyVo> resultList = new ArrayList<DutyCorrAnalyVo>();
		
		String orgID = null;
		for (SolrDocument doc : searchedDocs) {
			
			DutyCorrAnalyVo dutyCorrAnalyVo = null;
			
			for(DutyCorrAnalyVo dutyResult : resultList) {
				orgID = doc.getFieldValue("orgId").toString();
				if( orgID.equals(dutyResult.getOrgId())){
					dutyCorrAnalyVo = dutyResult;
					break;
				}
			}
			if(null == dutyCorrAnalyVo) {
				dutyCorrAnalyVo = new DutyCorrAnalyVo();
				dutyCorrAnalyVo.setOrgId(doc.getFieldValue("orgId").toString());
				dutyCorrAnalyVo.setOrgName(doc.getFieldValue("orgName").toString());
				dutyCorrAnalyVo.setOrgType(doc.getFieldValue("orgType").toString());
				dutyCorrAnalyVo.setOwnSys(doc.getFieldValue("ownSys").toString());
				dutyCorrAnalyVo.setMechStandard(doc.getFieldValue("mechStandard").toString());
				if ("1".equals(doc.getFieldValue("typeFlag").toString())) {
					dutyCorrAnalyVo.getDutyList().add(doc);
				} else if ("2".equals(doc.getFieldValue("typeFlag").toString())) {
					dutyCorrAnalyVo.getDeptList().add(doc);
				} else {
					dutyCorrAnalyVo.getItemList().add(doc);
				}
				resultList.add(dutyCorrAnalyVo);
			} else {
				if ("1".equals(doc.getFieldValue("typeFlag").toString())) {
					dutyCorrAnalyVo.getDutyList().add(doc);
				} else if ("2".equals(doc.getFieldValue("typeFlag").toString())) {
					dutyCorrAnalyVo.getDeptList().add(doc);
				}else{
					dutyCorrAnalyVo.getItemList().add(doc);
				}
			}
		}
		return resultList;
	}
	
	
}
