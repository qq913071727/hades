package cn.conac.rc.framework.utils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 *Bean的帮助类，提供静态方法，不可以实例化。
 * 
 */
public class BeanUtils {
	/**
	 * 禁止实例化
	 */
	private BeanUtils() {
	}
	
	/**
	 * 将Map转换成bean
	 * @param map 要转换的Map
	 * @param obj 要转成的bean
	 * @return   (0: 正常  -1: 异常)
	 */
	 public static int transMap2Bean(Map<String, Object> map, Object obj) {
		   int ret = 0;
	        try {
	            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
	            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
	            
	            for (PropertyDescriptor property : propertyDescriptors) {
	                String key = property.getName();  
	                if (map.containsKey(key)) {
	                    Object value = map.get(key);
	                    // 得到property对应的setter方法  
	                    Method setter = property.getWriteMethod();
	                    setter.invoke(obj, value);
	                }
	            }
	        } catch (Exception e) {
	            System.out.println( "BeanUtils class : transMap2Bean Error " + e);
	            ret = -1;
	        }
	        return ret;
	    }
	  
	    // Bean --> Map 1: 利用Introspector和PropertyDescriptor 将Bean --> Map  
	    public static Map<String, Object> transBean2Map(Object obj) {
	        if(null == obj) {
	            return null;
	        }
	        Map<String, Object> map = new HashMap<String, Object>();
	        try {
	            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
	            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
	            for (PropertyDescriptor property : propertyDescriptors) {
	                String key = property.getName();
	                // 过滤class属性
	                if (!key.equals("class")) {
	                    // 得到property对应的getter方法
	                    Method getter = property.getReadMethod();
	                    Object value = getter.invoke(obj);
	                    map.put(key, value);
	                }
	            }
	        } catch (Exception e) {
	            System.out.println("BeanUtils class : transBean2Map Error " + e);
	        }
	        return map;
	    }
}
