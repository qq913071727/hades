package cn.conac.rc.framework;

import org.springframework.boot.SpringApplication;

//@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
