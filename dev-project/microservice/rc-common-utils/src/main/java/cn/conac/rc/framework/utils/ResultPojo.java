package cn.conac.rc.framework.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 返回结果封装类
 */
@ApiModel(value = "返回结果封装类")
public class ResultPojo implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * CODE_SUCCESS
     */
    public static final String CODE_SUCCESS = "1000";
    /**
     * 用户名重复提示输入手机后四位
     */
    public static final String CODE_SUCCESS_USER_REPEAT = "1111";
    /**
     * CODE_FAILURE
     */
    public static final String CODE_FAILURE = "2000";
    /**
     * CODE_FAILURE
     */
    public static final String CODE_FAILURE_USER_BINDED = "2001";
    /**
     * MSG_SUCCESS
     */
    public static final String MSG_SUCCESS = "SUCCESS";
    /**
     * MSG_FAILURE
     */
    public static final String MSG_FAILURE = "FAIL";
    
    /**
     * Code_数据不存在
     */
    public static final String CODE_NULL_ERR = "2503 ";
    /**
     * MSG_数据不存在
     */
    public static final String MSG_NULL_ERR  = "数据不存在";
    

    @ApiModelProperty(value = "结果code。1000：成功，2000：失败", required = true)
    private String code = CODE_SUCCESS;

    @ApiModelProperty(value = "消息提示", required = false)
    private String msg = MSG_SUCCESS;

    @ApiModelProperty(value = "真正返回结果数据", required = false)
    private Object result = null;

    /**
     * 自定义返回内容
     */
    public ResultPojo(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 自定义返回内容
     */
    public ResultPojo(String code, String msg, Object result) {
        this.code = code;
        this.msg = msg;
        this.result = result;
    }

    public ResultPojo() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ResultPojo{" + "code='" + code + '\'' + ", msg='" + msg + '\'' + ", result=" + result + '}';
    }
}
