/**
 * Copyright (c) 2005-2012 springside.org.cn
 */
package cn.conac.rc.framework.utils;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.SecureRandom;

import org.apache.commons.lang3.Validate;
//import org.apache.shiro.crypto.hash.Md5Hash;

/**
 * 支持SHA-1/MD5消息摘要的工具类. 返回ByteSource，可进一步被编码为Hex, Base64或UrlSafeBase64
 * @author calvin
 * @version 1.0
 */
public class Digests {

    /**
     * SHA1
     */
    private static final String SHA1 = "SHA-1";
    /**
     * MD5
     */
    private static final String MD5 = "MD5";
    /**
     * RANDOM
     */
    private static SecureRandom RANDOM = new SecureRandom();

    /**
     * 对输入字符串进行md5散列.
     * @param input byte[]
     * @return byte[]
     */
    public static byte[] md5(byte[] input) {
        return digest(input, MD5, null, 1);
    }

    /*
     * public static String md5(String input) { return new
     * Md5Hash(input).toString(); }
     */

    /**
     * md5.
     * @param input byte[]
     * @param iterations int
     * @return byte[]
     */
    public static byte[] md5(byte[] input, int iterations) {
        return digest(input, MD5, null, iterations);
    }

    /**
     * 对输入字符串进行sha1散列.
     * @param input byte[]
     * @return byte[]
     */
    public static byte[] sha1(byte[] input) {
        return digest(input, SHA1, null, 1);
    }

    /**
     * sha1.
     * @param input byte[]
     * @param salt byte[]
     * @return byte[]
     */
    public static byte[] sha1(byte[] input, byte[] salt) {
        return digest(input, SHA1, salt, 1);
    }

    /**
     * sha1.
     * @param input byte[]
     * @param salt byte[]
     * @param iterations int
     * @return byte[]
     */
    public static byte[] sha1(byte[] input, byte[] salt, int iterations) {
        return digest(input, SHA1, salt, iterations);
    }

    /**
     * 对字符串进行散列, 支持md5与sha1算法.
     * @param input byte[]
     * @param algorithm String
     * @param salt byte[]
     * @param iterations int
     * @return byte[]
     */
    private static byte[] digest(byte[] input, String algorithm, byte[] salt, int iterations) {
        try {
            MessageDigest digest = MessageDigest.getInstance(algorithm);

            if (salt != null) {
                digest.update(salt);
            }

            byte[] result = digest.digest(input);

            for (int i = 1; i < iterations; i++) {
                digest.reset();
                result = digest.digest(result);
            }
            return result;
        } catch (GeneralSecurityException e) {
            throw Exceptions.unchecked(e);
        }
    }

    /**
     * 生成随机的Byte[]作为salt.
     * @param numBytes byte数组的大小
     * @return byte[]
     */
    public static byte[] generateSalt(int numBytes) {
        Validate.isTrue(numBytes > 0, "numBytes argument must be a positive integer (1 or larger)", numBytes);

        byte[] bytes = new byte[numBytes];
        RANDOM.nextBytes(bytes);
        return bytes;
    }

    /**
     * 对文件进行md5散列.
     * @param input InputStream
     * @return byte[]
     * @throws IOException IO异常
     */
    public static byte[] md5(InputStream input) throws IOException {
        return digest(input, MD5);
    }

    /**
     * 对文件进行sha1散列.
     * @param input InputStream
     * @return byte[]
     * @throws IOException IO异常
     */
    public static byte[] sha1(InputStream input) throws IOException {
        return digest(input, SHA1);
    }

    /**
     * digest.
     * @param input InputStream
     * @param algorithm String
     * @return byte[] 
     * @throws IOException 异常
     */
    private static byte[] digest(InputStream input, String algorithm) throws IOException {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
            int bufferLength = 8 * 1024;
            byte[] buffer = new byte[bufferLength];
            int read = input.read(buffer, 0, bufferLength);

            while (read > -1) {
                messageDigest.update(buffer, 0, read);
                read = input.read(buffer, 0, bufferLength);
            }

            return messageDigest.digest();
        } catch (GeneralSecurityException e) {
            throw Exceptions.unchecked(e);
        }
    }

    /**
     * 主函数.
     * @param args String[]
     */
    public static void main(String[] args) {
        // System.out.println(md5("bPp@2015Go1000005820150405"));
        // System.out.println(new Md5Hash("1000005820150405",
        // "bPp@2015Go").toString());
    }
}
