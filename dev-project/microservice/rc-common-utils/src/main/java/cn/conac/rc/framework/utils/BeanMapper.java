/**
 * Copyright (c) 2005-2012 springside.org.cn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package cn.conac.rc.framework.utils;

import java.util.Collection;
import java.util.List;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;

import com.google.common.collect.Lists;

/**
 * 简单封装Dozer, 实现深度转换Bean<->Bean的Mapper.实现: 1. 持有Mapper的单例. 2. 返回值类型转换. 3.
 * 批量转换Collection中的所有对象. 4. 区分创建新的B对象与将对象A值复制到已存在的B对象两种函数.
 * @author calvin
 * @version 2013-01-15
 */
public class BeanMapper {

    /**
     * 持有Dozer单例, 避免重复创建DozerMapper消耗资源.
     */
    private static DozerBeanMapper DOZER = new DozerBeanMapper();
    
    /**
     * NOT_NULL
     */
    private static BeanUtilsBean NOT_NULL = new NullAwareBeanUtilsBean();

    /*
     * public static final BeanMappingBuilder builder = new BeanMappingBuilder()
     * {
     * 
     * @Override protected void configure() { String dateFormat = "yyyy-MM-dd";
     * mapping(Map.class, Search.class,
     * TypeMappingOptions.dateFormat(dateFormat)).fields("startDate",
     * "startDate").fields("endDate", "endDate"); } }; static{
     * BeanMappingBuilder builder = new BeanMappingBuilder() {
     * 
     * @Override protected void configure() { String dateFormat = "yyyy-MM-dd";
     * mapping(Map.class, Search.class,
     * TypeMappingOptions.dateFormat(dateFormat)).fields("startDate",
     * "startDate").fields("endDate", "endDate"); } };
     * dozer.addMapping(builder); }
     */
    /**
     * 基于Dozer转换对象的类型.
     * @param source Object
     * @param <T> 泛型对象
     * @param destinationClass Class
     * @return T
     */
    public static <T> T map(Object source, Class<T> destinationClass) {
        return DOZER.map(source, destinationClass);
    }

    /**
     * 基于Dozer转换对象的类型.
     * @param source Object
     * @param <T> 泛型对象
     * @param destinationClass Class<T>
     * @param builder BeanMappingBuilder
     * @return T
     */
    public static <T> T map(Object source, Class<T> destinationClass, BeanMappingBuilder builder) {
        DozerBeanMapper dozer1 = new DozerBeanMapper();
        dozer1.addMapping(builder);
        return dozer1.map(source, destinationClass);
    }

    /**
     * 基于Dozer转换Collection中对象的类型.
     * @param sourceList Collection
     * @param <T> 泛型对象
     * @param destinationClass Class<T>
     * @return List<T>
     */
    @SuppressWarnings("rawtypes")
    public static <T> List<T> mapList(Collection sourceList, Class<T> destinationClass) {
        List<T> destinationList = Lists.newArrayList();
        for (Object sourceObject : sourceList) {
            T destinationObject = DOZER.map(sourceObject, destinationClass);
            destinationList.add(destinationObject);
        }
        return destinationList;
    }

    /**
     * 基于Dozer将对象A的值拷贝到对象B中.
     * @param source Object
     * @param destinationObject Object
     */
    public static void copy(Object source, Object destinationObject) {
        DOZER.map(source, destinationObject);
    }

    /**
     * 基于BeanUtils将对象A的值拷贝到对象B中. 忽略A的null值
     * @param source Object
     * @param destinationObject Object
     */
    public static void copyIgnorNull(Object source, Object destinationObject) {
        try {
            NOT_NULL.copyProperties(destinationObject, source);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}