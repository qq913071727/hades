package cn.conac.rc.framework.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * FastJson工具类
 * @author wangmeng
 * @version 1.0
 */
public class FastJsonUtil {
    /**
     *  生成json.
     * @param obj Object
     * @return String
     */
    public static String getJson(Object obj) {
        return JSON.toJSONString(obj, SerializerFeature.WriteMapNullValue);
    }

    /**
     *  解析Object
     * @param <T> 泛型对象
     * @param json String
     * @param c Class<T>
     * @return T
     */
    public static <T> T getObject(String json, Class<T> c) {
        T t = null;
        try {
            t = JSON.parseObject(json, c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    /**
     *  解析List.
     * @param <T> 泛型对象
     * @param json String
     * @param c Class<T>
     * @return List<T>
     */
    public static <T> List<T> getList(String json, Class<T> c) {
        List<T> list = new ArrayList<T>();
        try {
            list = JSON.parseArray(json, c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     *  解析List.
     * @param json String
     * @return List<String>
     */
    public static List<String> getList(String json) {
        List<String> list = new ArrayList<String>();
        try {
            list = JSON.parseArray(json, String.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     *  解析List.
     * @param json String
     * @return List<Map<String, Object>>
     */
    public static List<Map<String, Object>> listKeyMaps(String json) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        try {
            list = JSON.parseObject(json, new TypeReference<List<Map<String, Object>>>() {
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
