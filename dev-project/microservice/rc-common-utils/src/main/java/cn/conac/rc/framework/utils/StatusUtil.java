package cn.conac.rc.framework.utils;

import org.apache.commons.lang.StringUtils;

public class StatusUtil {
	/***
	 * 
	 * @param status 事项的状态
	 * @param tab	 tab页
	 * @param status 1[已发布] 2[待提交] 3[审核中] 4[驳回] 5[取消] 6[转移] 7[下放] 8[上调]目前没有  9[合并]目前没有 10[拆分] 11[废置] 12[变更]
	 * @param tab 1[已发布] 2[待发布事项] 3[历史事项]
	 * @return
	 */
	public static String getQueryStatus(String status,String tab){
		if(! StringUtils.isEmpty(status)){
			return status;
		}
		//已发布：查询已发布事项
		if(tab == null || "".equals(tab) || tab.equals("1")){
			if(StringUtils.isBlank(status)){
				return "2";
			}
		}else if(tab.equals("2")) {//代发布事项：2[待提交] 3[审核中] 4[驳回] 12[变更]
			return "0,1,3,4";
		}else if(tab.equals("3")){//历史事项：5[取消] 6[转移] 7[下放] 11[废置] 15[下放中] 16[转移中] 17[取消中] 18[废置中]
			return "2";
		}
		return "2";
	}
	/***
	 * 
	 * @param status 事项的状态
	 * @param tab	 tab页
	 * @param reviewStatus 审核状态 0[未审核] 1[审核通过] 2[待提交] 3[挂起] 
	 * @return
	 */
	public static String getQueryReviewStatus(String operation,String tab){
		if(! StringUtils.isEmpty(operation)){
			return operation;
		}
		//已发布：查询的所以已发布的事项
		if(tab == null || "".equals(tab) || tab.equals("1")){
			 return "A,C";
		}else if(tab.equals("2")){//待发布事项：查询待提交，审核中，驳回，变更
			 return "A,C,W,P";
		}else{//历史：查询取消、转移、下放、废置、取消中、转移中、下放中、废置中
			return "W,P";
		}
	}
}
