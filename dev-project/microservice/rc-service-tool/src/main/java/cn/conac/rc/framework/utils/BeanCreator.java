package cn.conac.rc.framework.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 制造Bean工具类
 * 
 * @author wangmeng
 */
public class BeanCreator {

	/**
	 * 包名
	 */
	private String packageName = "";
	
	/**
	 * 关联表名
	 */
	private String tableName = "";
	
	/**
	 * Entity实体名称
	 */
	private String className = "";
	
	/**
	 * serialVersionUID
	 */
	private String serialVersionUID = "0";
	
	/**
	 * 制造日期
	 */
	private String date = "";
	
	/**
	 * version
	 */
	private String version = "";
	
	/**
	 * Field类集合
	 */
	private List<Field> fieldList = new ArrayList<Field>();
	
	/**
	 * 表类别（DATA_TABLE：1，BASE_TABLE：2）
	 */
	private String tableType = "";

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getSerialVersionUID() {
		return serialVersionUID;
	}

	public void setSerialVersionUID(String serialVersionUID) {
		this.serialVersionUID = serialVersionUID;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<Field> getFieldList() {
		return fieldList;
	}

	public void setFieldList(List<Field> fieldList) {
		this.fieldList = fieldList;
	}

	public String getTableType() {
		return tableType;
	}

	public void setTableType(String tableType) {
		this.tableType = tableType;
	}

}
