package cn.conac.rc.framework.utils;

/**
 * 制造Field工具类
 * 
 * @author wangmeng
 */
public class Field {

	/**
	 * 属性注解
	 */
	private String comment = "";
	
	/**
	 * 属性类型
	 */
	private String type = "";
	
	/**
	 * 属性名称
	 */
	private String name = "";

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
