package cn.conac.rc.framework.utils;

/**
 * 制造Service工具类
 * 
 * @author wangmeng
 */
public class ServiceCreator {

	/**
	 * 包名
	 */
	private String packageName = "";
	
	/**
	 * 实体名称
	 */
	private String className = "";
	
	/**
	 * 制造日期
	 */
	private String date = "";
	
	/**
	 * version
	 */
	private String version = "";
	
	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}