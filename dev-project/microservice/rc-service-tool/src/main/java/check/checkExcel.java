package check;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class checkExcel {

	public static void main(String[] args) throws BiffException, IOException {

		String filePath = "D:/1.xls";
		InputStream is = new FileInputStream(filePath);
		Workbook rwb = Workbook.getWorkbook(is);
		rwb.getNumberOfSheets();
		Sheet sheet = rwb.getSheet(0);
		int rows = sheet.getRows();
		String error = "";


		// 验证部门名称长度
		for (int i = 2; i < rows; i++) {
			if (sheet.getCell(0, i).getContents().length() > 35) {
				error = error + (i + 1) + "行部门名称长度不能大于35字符" + "\n";
			}
		}
		// 部门状态输入验证
		for (int i = 2; i < rows; i++) {
			if (!sheet.getCell(1, i).getContents().equals("正常")
					&& !sheet.getCell(1, i).getContents().equals("已撤销")) {
				error = error + (i + 1) + "行部门状态输入错误" + "\n";
			}
		}
		// 是否涉密
		for (int i = 2; i < rows; i++) {
			if (!sheet.getCell(2, i).getContents().trim().equals("是")
					&& !sheet.getCell(2, i).getContents().trim().equals("否")) {
				error = error + (i + 1) + "行是否涉密输入错误" + "\n";
			}
		}
		// 是否主管
		for (int i = 2; i < rows; i++) {
			if (!sheet.getCell(3, i).getContents().trim().equals("是")
					&& !sheet.getCell(3, i).getContents().trim().equals("否")) {
				error = error + (i + 1) + "行是否主管输入错误" + "\n";
			}
		}
		// 主管部门
		for (int i = 2; i < rows; i++) {
			if (sheet.getCell(4, i).getContents().length() > 35) {
				error = error + (i + 1) + "行主管部门名称长度不能大于35字符" + "\n";
			}
		}
		// 所属系统
		for (int i = 2; i < rows; i++) {
			if (!sheet.getCell(5, i).getContents().equals("党委")
					&& !sheet.getCell(5, i).getContents().trim().equals("人大")
					&& !sheet.getCell(5, i).getContents().trim().equals("政协")
					&& !sheet.getCell(5, i).getContents().trim().equals("政府")
					&& !sheet.getCell(5, i).getContents().trim().equals("民主党派")
					&& !sheet.getCell(5, i).getContents().trim().equals("群众团体")
					&& !sheet.getCell(5, i).getContents().trim().equals("法院")
					&& !sheet.getCell(5, i).getContents().trim().equals("检察院")
					&& !sheet.getCell(5, i).getContents().trim().equals("经济实体")
					&& !sheet.getCell(5, i).getContents().trim().equals("其他")
					&& !sheet.getCell(5, i).getContents().trim().equals("街")
					&& !sheet.getCell(5, i).getContents().trim().equals("街道")
					&& !sheet.getCell(5, i).getContents().trim().equals("乡")
					&& !sheet.getCell(5, i).getContents().trim().equals("镇")) {
				error = error + (i + 1) + "行所属系统输入错误" + "\n";
			}
		}
		//机构类型
		for (int i = 2; i < rows; i++) {
			if (!sheet.getCell(6, i).getContents().equals("行政机关")
					&& !sheet.getCell(6, i).getContents().equals("事业单位")) {
				error = error + (i + 1) + "行机构类型输入错误" + "\n";
			}
		}
		//主管选是-主管部门名称不为空
		for (int i = 2; i < rows; i++) {
			if (sheet.getCell(3, i).getContents().equals("是") && !sheet.getCell(4, i).getContents().equals("")) {
				error = error + (i + 1) + "行为主管单位，请勿填写主管单位名称" + "\n";
			}
		}
		//主管选否-主管部门名称为空
				for (int i = 2; i < rows; i++) {
					if (sheet.getCell(3, i).getContents().equals("否") && sheet.getCell(4, i).getContents().equals("")) {
						error = error + (i + 1) + "行为非主管单位，请填写主管单位名称" + "\n";
					}
				}
		//部门重复
		for (int i = 2; i < rows; i++) {
			for (int j = 2; j < rows; j++) {
				if (sheet.getCell(0, j).getContents().equals(sheet.getCell(0, i).getContents()) && i!=j) {
					error = error + (i + 1) + "行部门名称重复" + "\n";
				}
			}
		}
		//分割线
		error = error + "--------警告-------" + "\n";
		//主管存在与否
		int count = 0;
		for (int i = 2; i < rows; i++) {
			if(!sheet.getCell(4, i).getContents().trim().equals("")){
				for (int j = 2; j < rows; j++) {
					if (sheet.getCell(4, i).getContents().trim().equals(sheet.getCell(0, j).getContents().trim())) {
						count = count + 1;
					}
				}
				if(count == 0){
					error = error + (i + 1) + "行主管部门不存在，请确认系统中是否存在" + "\n";
				}
				count = 0;
			}
			
		}
		
//		error = error + "\n" + "\n" + "\n";
		File file = new File("D:/1.txt");
        FileWriter fw = new FileWriter(file,true); 
        fw.write(error);
        fw.close();
        System.out.println(error);
	}

}
