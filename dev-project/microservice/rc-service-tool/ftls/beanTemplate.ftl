package ${beanCreator.packageName}.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ${beanCreator.className?cap_first}Entity类
 *
 * @author beanCreator
 * @date ${beanCreator.date}
 * @version ${beanCreator.version}
 */
@ApiModel
@Entity
@Table(name="${beanCreator.tableName}")
public class ${beanCreator.className?cap_first}Entity implements Serializable {

	private static final long serialVersionUID = ${beanCreator.serialVersionUID}L;

	<#list beanCreator.fieldList as field>
	@ApiModelProperty("${field.comment!field.name}")
	private ${field.type} ${field.name};

	</#list>
	<#list beanCreator.fieldList as field>
	public void set${field.name?cap_first}(${field.type} ${field.name}){
		this.${field.name}=${field.name};
	}

	public ${field.type} get${field.name?cap_first}(){
		return ${field.name};
	}

	</#list>
}
