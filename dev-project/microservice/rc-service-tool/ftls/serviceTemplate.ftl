package ${serviceCreator.packageName}.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import ${serviceCreator.packageName}.entity.${serviceCreator.className?cap_first}Entity;
import ${serviceCreator.packageName}.repository.${serviceCreator.className?cap_first}Repository;
import ${serviceCreator.packageName}.vo.${serviceCreator.className?cap_first}Vo;
import cn.conac.rc.framework.jpa.Criteria;
import cn.conac.rc.framework.service.GenericService;

/**
 * ${serviceCreator.className?cap_first}Service类
 *
 * @author serviceCreator
 * @date ${serviceCreator.date}
 * @version ${serviceCreator.version}
 */
@Service
public class ${serviceCreator.className?cap_first}Service extends GenericService<${serviceCreator.className?cap_first}Entity, Integer> {

	@Autowired
	private ${serviceCreator.className?cap_first}Repository repository;

	/**
	 * 计数查询
	 * @param vo
	 * @return 计数结果
	 */
	public long count(${serviceCreator.className?cap_first}Vo vo){
		return super.count(this.createCriteria(vo));
	}

	/**
	 * 动态查询，分页，排序查询
	 * @param vo
	 * @return Page
	 * @throws Exception
	 */
	public Page<${serviceCreator.className?cap_first}Entity> list(${serviceCreator.className?cap_first}Vo vo) throws Exception {
		try {
			Sort sort = new Sort(Direction.ASC, "id");// TODO 可选的排序
			Pageable pageable = new PageRequest(vo.getPage(), vo.getSize(), sort);
			Criteria<${serviceCreator.className?cap_first}Entity> dc = this.createCriteria(vo);
			return repository.findAll(dc, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 动态查询条件
	 * @param param
	 * @return Criteria
	 */
	private Criteria<${serviceCreator.className?cap_first}Entity> createCriteria(${serviceCreator.className?cap_first}Vo param) {
		Criteria<${serviceCreator.className?cap_first}Entity> dc = new Criteria<${serviceCreator.className?cap_first}Entity>();
		// TODO 具体条件赋值

		return dc;
	}

}
