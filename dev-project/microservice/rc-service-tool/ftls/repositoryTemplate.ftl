package ${repositoryCreator.packageName}.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ${repositoryCreator.packageName}.entity.${repositoryCreator.className?cap_first}Entity;
import cn.conac.rc.framework.repository.GenericDao;

/**
 * ${repositoryCreator.className?cap_first}Repository类
 *
 * @author repositoryCreator
 * @date ${repositoryCreator.date}
 * @version ${repositoryCreator.version}
 */
@Repository
public interface ${repositoryCreator.className?cap_first}Repository extends GenericDao<${repositoryCreator.className?cap_first}Entity, Integer> {
}
