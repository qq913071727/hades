import KeyValueInput from './key_value_input/index';
import calculation from './calculation/index';

//存储组件列表
const components = [
  KeyValueInput,
  calculation,
];

//install是让业务代码在main.js 引入之后 use(myUi)  注册到全局用的
const install = (app, opts = {}) => {
  components.forEach(item => {
    app.component(item.name, item)
  })
}

const exportObject = {
  // 全局导出
  version: '1.0.0',
  install,
  KeyValueInput,
  calculation,
};

export default exportObject;
