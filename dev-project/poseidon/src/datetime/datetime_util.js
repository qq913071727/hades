/**
 * 根据日期返回星期几。参数_date可以是Date类型，也可以是string类型
 * @param {*} _date 
 * @returns 
 */
export function getDay(_date) {
  if(typeof(_date) == "string"){
    _date = new Date(_date);
  }
  let weekDay = ["星期天", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
    return weekDay[_date.getDay()];
};