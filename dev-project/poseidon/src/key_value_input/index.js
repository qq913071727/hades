/**
 * 键值对数组，每个元素是一个对象，属性为：key和value、keyWarning和valueWarning、deleteDisableStyle
 */
// let this.keyValueArray = [];

const KeyValueInput = {
  name: 'KeyValueInput',
  props: {
    /**
     * ant design vue的a-input组件的样式
     */
    class_: '',
    /**
     * 样式
     */
    style_: '',
  },
  data: function () {
    return {
      /**
       * 键值对数组，每个元素是一个对象，属性为：key和value、keyWarning和valueWarning、deleteDisableStyle
       */
      keyValueArray: [],
    };
  },
  created() {
    console.debug('created方法被调用');
    this.keyValueArray = [];
    this.keyValueArray.push({
      key: '',
      value: '',
      keyWarning: '',
      valueWarning: '',
      deleteDisableStyle: 'pointer-events: none; opacity: 0.2;'
    });
  },
  mounted() {
    console.debug('mounted方法被调用');
  },
  methods: {
    /**
     * 点击“添加”按钮
     */
    clickAdd() {
      console.debug('点击“添加”按钮');
      if (null != this.keyValueArray && this.keyValueArray.length > 0) {
        for (let i = 0; i < this.keyValueArray.length; i++) {
          if ('' === this.keyValueArray[i].key || undefined == this.keyValueArray[i].key) {
            this.keyValueArray[i].keyWarning = 'key不能为空';
            return;
          } else {
            this.keyValueArray[i].keyWarning = '';
          }
          if ('' === this.keyValueArray[i].value || undefined == this.keyValueArray[i].value) {
            this.keyValueArray[i].valueWarning = 'value不能为空';
            return;
          } else {
            this.keyValueArray[i].valueWarning = '';
          }
        }
      }
      this.keyValueArray[this.keyValueArray.length - 1].deleteDisableStyle = '';
      this.keyValueArray.unshift({ key: '', value: '', keyWarning: '', valueWarning: '', deleteDisableStyle: '' });
    },
    /**
     * 点击“删除”按钮
     */
    clickDelete(event) {
      console.debug('点击“删除”按钮');
      let index = event.target.attributes.index.value;
      // console.debug(index);
      // console.debug('before', this.keyValueArray.length);
      this.keyValueArray.splice(index, 1);
      // console.debug('after', this.keyValueArray.length);
      if (null != this.keyValueArray && this.keyValueArray.length == 1) {
        this.keyValueArray[0].deleteDisableStyle = 'pointer-events: none; opacity: 0.2;';
      }
    },
  },
  computed: {

  },
  render: function (h) {
    console.debug('render方法被调用');
    let that = this;
    return h('div', [
      h('table', [
        this.keyValueArray.map(function (item, index) {
          return h('tr', [
            h('td', [
              h('input', {
                attrs: {
                  class: that.class_,
                  style: that.style_,
                  type: 'text',
                },
                domProps: {
                  value: item.key
                },
                on: {
                  input: function (event) {
                    item.key = event.target.value
                  }
                }
              }),
              h('span', {
                attrs: {
                  style: 'color: red'
                }
              }, item.keyWarning),
            ]),
            h('td', [
              h('input', {
                attrs: {
                  class: that.class_,
                  style: that.style_,
                  type: 'text',
                },
                domProps: {
                  value: item.value
                },
                on: {
                  input: function (event) {
                    item.value = event.target.value
                  }
                }
              }),
              h('span', {
                attrs: {
                  style: 'color: red'
                }
              }, item.valueWarning),
            ]),
            h('td', [
              h('span', {
                on: {
                  click: that.clickAdd
                }
              }, '添加')
            ]),
            h('td', [
              h('span', {
                on: {
                  click: that.clickDelete
                },
                attrs: {
                  style: item.deleteDisableStyle,
                  index: index
                }
              }, '删除')
            ]),
          ])
        }),
      ])
    ]);
  }
}
export default KeyValueInput;