import add from './add'
import subtract from './subtract'
import multiply from './multiply'
import divide from './divide'

const calculation = {
  add,
  subtract,
  multiply,
  divide
}

export default calculation
export { add, subtract, multiply, divide }
