function subtract(...args) {
  return args.reduce((ac, cur) => ac - cur)
}

export default subtract
