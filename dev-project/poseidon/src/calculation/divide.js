function divide(...args) {
  return args.reduce((ac, cur) => ac / cur)
}

export default divide

