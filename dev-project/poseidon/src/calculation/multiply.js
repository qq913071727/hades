function multiply(...args) {
  return args.reduce((ac, cur) => ac * cur)
}

export default multiply
