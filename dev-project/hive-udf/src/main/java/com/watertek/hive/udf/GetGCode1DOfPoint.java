package com.watertek.hive.udf;

import com.watertek.geosot.GCode1D;
import com.watertek.geosot.GeoSOT;
import com.watertek.hive.util.PropertiesUtil;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.JavaStringObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.io.Text;

/**
 * hive自定义函数类
 */
public class GetGCode1DOfPoint extends UDF {

    static {
        try {
            String os = System.getProperty("os.name");
            if(os.toLowerCase().startsWith("win")){
                System.loadLibrary("GeoSOT");
            }else{
                System.load("/opt/libgeosot-2.0.so");
            }
        } catch (UnsatisfiedLinkError e) {
            System.err.println("Native code library failed to load.\n" + e);
            System.exit(1);
        }
    }

    public GetGCode1DOfPoint() {

    }

    /**
     * 将经纬度转换为北斗网格码时的层级
     */
    public static final int POINT_TO_CODE_LAYER = Integer.parseInt(PropertiesUtil.getValue("config/config.properties", "point.to.code.layer"));

    /**
     * namenode节点的IP
     */
    public static final String NAMENODE_IP = PropertiesUtil.getValue("config/config.properties", "namenode.ip");

    /**
     * namenode节点的端口
     */
    public static final String NAMENODE_PORT = PropertiesUtil.getValue("config/config.properties", "namenode.port");

    /**
     * 根据经度、纬度和层级，进行网格编码
     *
     * @param longitude
     * @param latitude
     * @return
     */
    public long evaluate(final String longitude, final String latitude, final String layer) {
        System.out.println("udf method GetGCode1DOfPoint.evaluate start");
//        JavaStringObjectInspector stringInspector;
//        stringInspector = PrimitiveObjectInspectorFactory.javaStringObjectInspector;
//        String longitudeStr = stringInspector.getPrimitiveJavaObject(longitude);
//        String latitudeStr = stringInspector.getPrimitiveJavaObject(latitude);
        System.out.println("java.library.path:"+System.getProperty("java.library.path"));
        try {
            GCode1D orgGCode1D = GeoSOT.getGCode1DOfPoint(Double.parseDouble(longitude), Double.parseDouble(latitude), Integer.parseInt(layer));
            return orgGCode1D.getCode().longValue();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

//    /**
//     * 返回需要导入的库文件数组，必须是绝对路径
//     * @return
//     */
//    @Override
//    public String[] getRequiredJars() {
//        String[] jarFilePath = new String[]{"hdfs://"+NAMENODE_IP+":"+NAMENODE_PORT+"/libgeosot-2.0.so",
//                "hdfs://"+NAMENODE_IP+":"+NAMENODE_PORT+"/hive-udf-jar-with-dependencies.jar",
//                "hdfs://"+NAMENODE_IP+":"+NAMENODE_PORT+"/geosot-2.0.jar"};
//        return jarFilePath;
//    }
//
//    @Override
//    public String[] getRequiredFiles() {
//        String[] jarFilePath = new String[]{"hdfs://"+NAMENODE_IP+":"+NAMENODE_PORT+"/libgeosot-2.0.so",
//                "hdfs://"+NAMENODE_IP+":"+NAMENODE_PORT+"/hive-udf-jar-with-dependencies.jar",
//                "hdfs://"+NAMENODE_IP+":"+NAMENODE_PORT+"/geosot-2.0.jar"};
//        return jarFilePath;
//    }

    public static void main(String[] args) {
        GetGCode1DOfPoint g=new GetGCode1DOfPoint();
        System.out.println(g.evaluate("1.1", "2.2", "15"));
    }
}
