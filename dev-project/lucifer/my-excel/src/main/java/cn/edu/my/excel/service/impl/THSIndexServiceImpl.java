package cn.edu.my.excel.service.impl;

import cn.edu.my.excel.model.THSIndex;
import cn.edu.my.excel.service.THSIndexService;
import cn.edu.my.excel.dao.THSIndexDao;
import cn.edu.my.excel.dao.impl.THSIndexDaoImpl;

import java.util.List;

public class THSIndexServiceImpl implements THSIndexService{
	private THSIndexDao thsIndexDao=new THSIndexDaoImpl();
	
	public void save(List<THSIndex> thsIndexList){
		thsIndexDao.save(thsIndexList);
	}
}