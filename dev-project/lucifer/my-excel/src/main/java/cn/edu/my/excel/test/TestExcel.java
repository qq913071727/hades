package cn.edu.my.excel.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import cn.edu.my.excel.service.impl.ExcelReaderImpl;
import cn.edu.my.excel.service.ExcelReader;

public class TestExcel{
	public static void main(String[] args) throws Exception {
		HSSFWorkbook wb = null;
	    POIFSFileSystem fs = null;
	    ExcelReader excelReader=ExcelReaderImpl.getInstance();
	    try {
	      //fs = new POIFSFileSystem(new FileInputStream(args[0]));
	      //wb = new HSSFWorkbook(fs);
	      excelReader.readExcel(args[0]);
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	
	    /*HSSFSheet sheet = wb.getSheetAt(0);
	    HSSFRow row = sheet.getRow(0);
	    HSSFCell cell = row.getCell(0);
	    String msg = cell.getStringCellValue();
	    Sytem.out.println(msg);*/
	}
}