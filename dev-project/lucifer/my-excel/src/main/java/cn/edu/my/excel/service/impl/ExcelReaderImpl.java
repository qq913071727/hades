package cn.edu.my.excel.service.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.text.DecimalFormat;

import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import cn.edu.my.excel.service.ExcelReader;
import cn.edu.my.excel.model.THSIndex;

public class ExcelReaderImpl implements ExcelReader{
	private static ExcelReaderImpl excelReader;
	private List<THSIndex> thsIndexList=new ArrayList<THSIndex>();
	
	private ExcelReaderImpl(){
	}
	
	public static ExcelReader getInstance(){
		if(null==excelReader){
			excelReader=new ExcelReaderImpl();	
		}
		return new ExcelReaderImpl();
	}

	public void method2() throws Exception {
	
	    InputStream is = new FileInputStream("D:\\同花顺指数_20140226.xls");
	    HSSFWorkbook wb = new HSSFWorkbook(new POIFSFileSystem(is));
	
	    ExcelExtractor extractor = new ExcelExtractor(wb);
	    extractor.setIncludeSheetNames(false);
	    extractor.setFormulasNotResults(false);
	    extractor.setIncludeCellComments(true);
	
	    String text = extractor.getText();
	    //System.out.println(text);
	}

	public List<THSIndex> readExcel(String file) throws Exception {
	    HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(file));
	    HSSFSheet sheet = wb.getSheetAt(0);
	    DecimalFormat df = new DecimalFormat("#.00");//保留小数点后两位
	
		List<THSIndex> thsIndexList=new ArrayList<THSIndex>();
	    for(int i=0; i< sheet.getLastRowNum()+1; i++){
	    	if (i == 0) {
    			continue;
    		}
    		Row row = sheet.getRow(i);
		    THSIndex thsIndex=new THSIndex();
		    List temp=new ArrayList();
		    for (Iterator<Cell> iter2 = (Iterator<Cell>) row.cellIterator(); iter2.hasNext();) {
				Cell cell = iter2.next();
			    if(Cell.CELL_TYPE_NUMERIC==cell.getCellType()){
			    	double content = cell.getNumericCellValue();// 除非是numeric类型，否则这样迭代读取会有错误
			      	temp.add(content);
			      	//System.out.println(content);	
			    }
			    if(Cell.CELL_TYPE_STRING==cell.getCellType()){
			    	/*if(cell.getStringCellValue().equals("--")){
						continue;
					}*/
			      	String content = cell.getStringCellValue();// 除非是sring类型，否则这样迭代读取会有错误
			       	temp.add(content);
			       	//System.out.println(content);	
			    }
		    }
		    
			thsIndex.setIndexDate(new Date());
			thsIndex.setSectionName((String)temp.get(0));
	    	thsIndex.setRiseExtent(Double.parseDouble(df.format(Double.parseDouble(temp.get(1).toString())*100)));//由于使用百分比表示，所以最后还要乘以100
	    	thsIndex.setRiseSpeed(Double.parseDouble(df.format(Double.parseDouble(temp.get(2).toString())*100)));//df.format方法表示保留小数点后两位
	    	thsIndex.setDdeNetAmount(Double.parseDouble(df.format(Double.parseDouble(temp.get(3).toString()))));
	    	thsIndex.setDdeSum(Long.parseLong(temp.get(4).toString().substring(0,temp.get(4).toString().length()-2)));
	    	thsIndex.setAmountRatio(Double.parseDouble(df.format(Double.parseDouble(temp.get(5).toString()))));
	    	thsIndex.setRiseNumber(Integer.parseInt(temp.get(6).toString().substring(0,temp.get(6).toString().length()-2)));
	    	thsIndex.setDropNumber(Integer.parseInt(temp.get(7).toString().substring(0,temp.get(7).toString().length()-2)));
	    	thsIndex.setLeadingRiseShare(temp.get(8).toString());
	    	thsIndex.setLastFivedayRiseExtent(Double.parseDouble(df.format(Double.parseDouble(temp.get(9).toString())*100)));
	    	thsIndex.setLastTendayRiseExtent(Double.parseDouble(df.format(Double.parseDouble(temp.get(10).toString())*100)));
	    	thsIndex.setLastTwentydayRiseExtent(Double.parseDouble(df.format(Double.parseDouble(temp.get(11).toString())*100)));
	    	//System.out.println(Integer.parseInt(temp.get(6).toString().replaceAll("0+?$","").replaceAll("[.]$","")));	    	
	    	
	    	if(temp.get(12).toString().contains("E")){
	    		String[] str=temp.get(12).toString().split("E");
	    		double d=Double.parseDouble(str[0])*10*Long.parseLong(str[1]);
	    		System.out.println(d);
	    		thsIndex.setCirculatingMarketValue(Long.parseLong(String.valueOf(d)));
	    	}else{
	    		thsIndex.setCirculatingMarketValue(Long.parseLong(temp.get(12).toString()));
	    	}
	    	thsIndex.setTransactionVolume(Long.parseLong(temp.get(12).toString().substring(0,temp.get(12).toString().length()-2)));
	    	thsIndex.setTransactionSum(Long.parseLong(temp.get(13).toString().substring(0,temp.get(13).toString().length()-2)));
	    	thsIndex.setTotalMarketValue(Long.parseLong(temp.get(14).toString().substring(0,temp.get(14).toString().length()-2)));
	    	
	    	thsIndex.setCirculatingMarketValue(Long.parseLong(temp.get(15).toString().substring(0,temp.get(15).toString().length()-2)));
	    	thsIndexList.add(thsIndex);
	    }
	    return thsIndexList;
	}
}