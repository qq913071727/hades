package cn.edu.my.excel.service;

import cn.edu.my.excel.model.THSIndex;

import java.util.List;

public interface THSIndexService {
	void save(List<THSIndex> thsIndexList);
}