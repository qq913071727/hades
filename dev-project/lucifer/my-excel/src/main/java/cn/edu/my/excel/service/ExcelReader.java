package cn.edu.my.excel.service;

import java.util.List;
import cn.edu.my.excel.model.THSIndex;

public interface ExcelReader {
	void method2() throws Exception;
	List<THSIndex> readExcel(String file) throws Exception;
}