package cn.edu.my.excel.dao;

import cn.edu.my.excel.model.THSIndex;

import java.util.List;

public interface THSIndexDao{
	void save(List<THSIndex> thsIndexList);
}