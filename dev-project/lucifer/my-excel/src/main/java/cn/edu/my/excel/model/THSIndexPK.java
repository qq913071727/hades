package cn.edu.my.excel.model;

import java.io.Serializable;
import java.util.Date;

public class THSIndexPK implements Serializable{
	private Date indexDate;
	private String sectionName;
	
	public THSIndexPK(){
	}
	
	public THSIndexPK(Date indexDate,String sectionName){
		this.indexDate=indexDate;
		this.sectionName=sectionName;
	}
	
	public Date getIndexDate(){
		return this.indexDate;	
	}
	
	public void setIndexDate(Date indexDate){
		this.indexDate=indexDate;	
	}
	
	public String getSectionName(){
		return this.sectionName;	
	}
	
	public void setSectionName(String sectionName){
		this.sectionName=sectionName;	
	}
	
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
        result = PRIME * result + ((indexDate == null) ? 0 : indexDate.hashCode());
        result = PRIME * result + ((sectionName == null) ? 0 : sectionName.hashCode());
        return result;
	}

	@Override
    public boolean equals(Object obj) {
	    if (this == obj){
			return true;    
	    }
		if (obj == null){
			return false;		
		}
		if (getClass() != obj.getClass()){
			return false;		
		}
		final THSIndexPK thsIndexPk = (THSIndexPK) obj;
		if (indexDate == null) {
			if (thsIndexPk.indexDate != null){
				return false;			
			}
		} else if (!indexDate.equals(thsIndexPk.indexDate)){
			return false;		
		}
		if (sectionName == null) {
			if (thsIndexPk.sectionName != null){
				return false;			
			}
		} else if (!sectionName.equals(thsIndexPk.sectionName)){
			return false;		
		}
		return true;
	}
}