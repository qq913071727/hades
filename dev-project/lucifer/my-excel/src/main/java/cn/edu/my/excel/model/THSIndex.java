package cn.edu.my.excel.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.IdClass;

import cn.edu.my.excel.model.THSIndexPK;

@Entity
@Table(name = "THS_INDEX")
@IdClass(THSIndexPK.class)
public class THSIndex implements Serializable{
	private static final long serialVersionUID = 4943396187256597131L;
	
	@Id
	private Date indexDate;
	@Id
	private String sectionName;
	@Column(name = "RISE_EXTENT")
	private double riseExtent;
	@Column(name = "RISE_SPEED")
	private double riseSpeed;
	@Column(name = "DDE_NET_AMOUNT")
	private double ddeNetAmount;
	@Column(name = "DDE_SUM")
	private long ddeSum;
	@Column(name = "AMOUNT_RATIO")
	private double amountRatio;
	@Column(name = "RISE_NUMBER")
	private int riseNumber;
	@Column(name = "DROP_NUMBER")
	private int dropNumber;
	@Column(name = "LEADING_RISE_SHARE")
	private String leadingRiseShare;
	@Column(name = "LAST_FIVEDAY_RISE_EXTENT")
	private double lastFivedayRiseExtent;
	@Column(name = "LAST_TENDAY_RISE_EXTENT")
	private double lastTendayRiseExtent;
	@Column(name = "LAST_TWENTYDAY_RISE_EXTENT")
	private double lastTwentydayRiseExtent;
	@Column(name = "TRANSACTION_VOLUME")
	private long transactionVolume;
	@Column(name = "TRANSACTION_SUM")
	private long transactionSum;
	@Column(name = "TOTAL_MARKET_VALUE")
	private long totalMarketValue;
	@Column(name = "CIRCULATING_MARKET_VALUE")
	private long circulatingMarketValue;
	
	public Date getIndexDate(){
		return this.indexDate;	
	}
	
	public void setIndexDate(Date indexDate){
		this.indexDate=indexDate;	
	}
	
	public String getSectionName(){
		return this.sectionName;	
	}
	
	public void setSectionName(String sectionName){
		this.sectionName=sectionName;	
	}
	
	public double getRiseExtent(){
		return this.riseExtent;	
	}
	
	public void setRiseExtent(double riseExtent){
		this.riseExtent=riseExtent;	
	}
	
	public double getRiseSpeed(){
		return this.riseSpeed;	
	}
	
	public void setRiseSpeed(double riseSpeed){
		this.riseSpeed=riseSpeed;	
	}
	
	public double getDdeNetAmount(){
		return this.ddeNetAmount;	
	}
	
	public void setDdeNetAmount(double ddeNetAmount){
		this.ddeNetAmount=ddeNetAmount;	
	}
	
	public long getDdeSum(){
		return this.ddeSum;	
	}
	
	public void setDdeSum(long ddeSum){
		this.ddeSum=ddeSum;	
	}
	
	public double getAmountRatio(){
		return this.amountRatio;	
	}
	
	public void setAmountRatio(double amountRatio){
		this.amountRatio=amountRatio;	
	}
	
	public int getRiseNumber(){
		return this.riseNumber;	
	}
	
	public void setRiseNumber(int riseNumber){
		this.riseNumber=riseNumber;	
	}
	
	public int getDropNumber(){
		return this.dropNumber;	
	}
	
	public void setDropNumber(int dropNumber){
		this.dropNumber=dropNumber;	
	}
	
	public String getLeadingRiseShare(){
		return this.leadingRiseShare;	
	}
	
	public void setLeadingRiseShare(String leadingRiseShare){
		this.leadingRiseShare=leadingRiseShare;	
	}
	
	public double getLastFivedayRiseExtent(){
		return this.lastFivedayRiseExtent;	
	}
	
	public void setLastFivedayRiseExtent(double lastFivedayRiseExtent){
		this.lastFivedayRiseExtent=lastFivedayRiseExtent;	
	}
	
	public double getLastTendayRiseExtent(){
		return this.lastTendayRiseExtent;	
	}
	
	public void setLastTendayRiseExtent(double lastTendayRiseExtent){
		this.lastTendayRiseExtent=lastTendayRiseExtent;	
	}
	
	public double getLastTwentydayRiseExtent(){
		return this.lastTwentydayRiseExtent;	
	}
	
	public void setLastTwentydayRiseExtent(double lastTwentydayRiseExtent){
		this.lastTwentydayRiseExtent=lastTwentydayRiseExtent;	
	}
	
	public long getTransactionVolume(){
		return this.transactionVolume;	
	}
	
	public void setTransactionVolume(long transactionVolume){
		this.transactionVolume=transactionVolume;	
	}
	
	public long getTransactionSum(){
		return this.transactionSum;	
	}
	
	public void setTransactionSum(long transactionSum){
		this.transactionSum=transactionSum;	
	}
	
	public long getTotalMarketValue(){
		return this.totalMarketValue;	
	}
	
	public void setTotalMarketValue(long totalMarketValue){
		this.totalMarketValue=totalMarketValue;	
	}
	
	public long getCirculatingMarketValue(){
		return this.circulatingMarketValue;	
	}
	
	public void setCirculatingMarketValue(long circulatingMarketValue){
		this.circulatingMarketValue=circulatingMarketValue;	
	}
}