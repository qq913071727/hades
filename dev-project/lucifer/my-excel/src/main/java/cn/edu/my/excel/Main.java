package cn.edu.my.excel;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import cn.edu.my.excel.service.THSIndexService;
import cn.edu.my.excel.service.ExcelReader;
import cn.edu.my.excel.service.impl.THSIndexServiceImpl;
import cn.edu.my.excel.service.impl.ExcelReaderImpl;
import cn.edu.my.excel.model.THSIndex;

public class Main{
	public static void main(String[] args) throws Exception{
		THSIndexService thsIndexService=new THSIndexServiceImpl();
		ExcelReader excelReader=ExcelReaderImpl.getInstance();
		
    	List<THSIndex> thsIndexList=new ArrayList<THSIndex>();
    	
        try {
	    	thsIndexList=excelReader.readExcel(args[0]);
	    	System.out.println(thsIndexList.get(0));
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
        
        thsIndexService.save(thsIndexList);
    }
}