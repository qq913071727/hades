package cn.com.acca.s2s3h4.action;

import java.util.Date;
import java.util.UUID;

import org.apache.log4j.Logger;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import cn.com.acca.s2s3h4.model.Tuser;
import cn.com.acca.s2s3h4.service.UserService;

@ParentPackage(value="basePackage")
@Namespace(value="/")
@Action(value="userAction")
public class UserAction {
	
	private static final Logger logger = Logger.getLogger(UserAction.class);
	
	private UserService userService;
	
	public UserService getUserService() {
		return userService;
	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void test(){
		logger.info("32222222222");
//		ApplicationContext ac=WebApplicationContextUtils.getWebApplicationContext(ServletActionContext.getServletContext());
//		UserService userService=(UserService) ac.getBean("userService");
		userService.test();
	}
	
	public void addUser(){
		Tuser t=new Tuser();
		t.setId(UUID.randomUUID().toString());
		t.setName("张三");
		t.setPwd("222");
		t.setCreateDateTime(new Date());
		userService.save(t);
	}
}
