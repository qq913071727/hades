package cn.com.acca.s2s3h4.dao;

import java.io.Serializable;

import cn.com.acca.s2s3h4.model.Tuser;

public interface UserDao {
	Serializable save(Tuser t);
}
