package cn.edu.my.jfreechart.test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.awt.Color;
import java.awt.Font;
import java.util.List;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import cn.edu.my.jfreechart.model.TradeFundFlowDirection;

public class TestLineChart {
    public static void main(String[] args) {
        StandardChartTheme mChartTheme = new StandardChartTheme("CN");
        mChartTheme.setLargeFont(new Font("黑体", Font.BOLD, 20));
        mChartTheme.setExtraLargeFont(new Font("宋体", Font.PLAIN, 15));
        mChartTheme.setRegularFont(new Font("宋体", Font.PLAIN, 15));
        ChartFactory.setChartTheme(mChartTheme);
        CategoryDataset mDataset = GetDataset();
        JFreeChart mChart = ChartFactory.createLineChart(
                "折线图",
                "年份",
                "数量",
                mDataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                false);
                
        try {
			FileOutputStream fos = new FileOutputStream("D:\\123.jpeg");
			// 将统计图标输出成JPG文件
			ChartUtilities.writeChartAsJPEG(fos, // 输出到哪个输出流
					1, // JPEG图片的质量，0~1之间
					mChart, // 统计图标对象
					2800, // 宽
					3600,// 宽
					null // ChartRenderingInfo 信息
					);
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

        /*CategoryPlot mPlot = (CategoryPlot)mChart.getPlot();
        mPlot.setBackgroundPaint(Color.LIGHT_GRAY);
        mPlot.setRangeGridlinePaint(Color.BLUE);//背景底部横虚线
        mPlot.setOutlinePaint(Color.RED);//边界线

        ChartFrame mChartFrame = new ChartFrame("折线图", mChart);
        mChartFrame.pack();
        mChartFrame.setVisible(true);*/
    }
    
    public static CategoryDataset GetDataset(){
        DefaultCategoryDataset mDataset = new DefaultCategoryDataset();
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("tradeFundFlowDirection");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		Query tradeNameQuery=em.createQuery("select distinct t.tradeName from TradeFundFlowDirection t");
		List<TradeFundFlowDirection> tradeNameList=(List<TradeFundFlowDirection>)tradeNameQuery.getResultList();
		
		for(int i=0;i<tradeNameList.size();i++){
			System.out.println(tradeNameList.get(i));
			Query query=em.createQuery("select t from TradeFundFlowDirection t where t.tradeName='"+String.valueOf(tradeNameList.get(i))+"'");
			List<TradeFundFlowDirection> tradeFundFlowDirectionList=(List<TradeFundFlowDirection>)query.getResultList();
			
			String[] x = new String[tradeFundFlowDirectionList.size()];
			Date[] y = new Date[tradeFundFlowDirectionList.size()];
			for(int k=0; k<tradeFundFlowDirectionList.size(); k++){
				x[k]=String.valueOf(tradeFundFlowDirectionList.get(k).getMainForceNetInflowAmount());
				y[k]=tradeFundFlowDirectionList.get(k).getTradeDate();
			}
			
			for(int j=0;j<x.length;j++){
				mDataset.addValue(Double.parseDouble(x[j]),String.valueOf(tradeNameList.get(i)),y[j]);
			}
		}
		em.getTransaction().commit();
		emf.close();
		
		return mDataset;
    }
}