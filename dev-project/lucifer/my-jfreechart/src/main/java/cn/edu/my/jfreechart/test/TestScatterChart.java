package cn.edu.my.jfreechart.test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.DefaultXYDataset;

import cn.edu.my.jfreechart.model.TradeFundFlowDirection;

public class TestScatterChart {
	public static void data(String title, String[] x, String[] y) {
		DefaultXYDataset xydataset = new DefaultXYDataset();

		double[][] data = new double[2][x.length];
		for (int i = 0; i < x.length; i++) {
			data[0][i] = Double.parseDouble(x[i]);
			data[1][i] = Double.parseDouble(y[i]);
		}
		xydataset.addSeries("牛的无线定位", data);

		XYTextAnnotation text1 = new XYTextAnnotation("1sss", 2, 2);
		XYTextAnnotation text2 = new XYTextAnnotation("2aaa", 4, 4);
		XYTextAnnotation text3 = new XYTextAnnotation("3bbb", 7, 5);

		final JFreeChart chart = ChartFactory.createScatterPlot("", "", "",
				xydataset, PlotOrientation.VERTICAL, false, false, false);

		XYPlot xyplot = (XYPlot) chart.getPlot();
		xyplot.addAnnotation(text1);
		xyplot.addAnnotation(text2);
		xyplot.addAnnotation(text3);

		try {
			FileOutputStream fos = new FileOutputStream("D:\\123.jpeg");
			// 将统计图标输出成JPG文件
			ChartUtilities.writeChartAsJPEG(fos, // 输出到哪个输出流
					1, // JPEG图片的质量，0~1之间
					chart, // 统计图标对象
					800, // 宽
					600,// 宽
					null // ChartRenderingInfo 信息
					);
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		List<TradeFundFlowDirection> tradeFundFlowDirectionList=getData();
		String[] x = new String[tradeFundFlowDirectionList.size()];
		String[] y = new String[tradeFundFlowDirectionList.size()];
		for(int i=0; i<tradeFundFlowDirectionList.size(); i++){
			x[i]=String.valueOf(tradeFundFlowDirectionList.get(i).getMainForceNetInflowPercentage());
			y[i]=String.valueOf(tradeFundFlowDirectionList.get(i).getUpDownRange());
		}
		data("title", x, y);
	}
	
	private static List<TradeFundFlowDirection> getData(){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("tradeFundFlowDirection");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		Query query=em.createQuery("select t from TradeFundFlowDirection t");
		List<TradeFundFlowDirection> tradeFundFlowDirectionList=(List<TradeFundFlowDirection>)query.getResultList();
		em.getTransaction().commit();
		emf.close();
		
		return tradeFundFlowDirectionList;
	}
}
