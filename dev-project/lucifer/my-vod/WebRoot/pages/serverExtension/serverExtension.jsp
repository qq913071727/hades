<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'serverExtension.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">

		<link rel="stylesheet" type="text/css"
			href="../css/serverExtension.css">
		<link rel="stylesheet" type="text/css" href="../css/index.css">
		<link rel="stylesheet" type="text/css" href="../css/share.css">

		<script language="javascript" src="../js/serverExtension.js"></script>
		<script language="javascript" src="../js/allOnLoadFunction.js"></script>


	</head>

	<body>
		<!-- serverExtension model -->
		<c:import
			url="../pages/portletHeader/serverExtensionPortletHeader.jsp" />
		<div id="serverExtensionView" class="view">
			<br>
			<bean:message key="serverExtension.selectModeOfStartingServer" />

			<html:form action="/serverSelect">

				<html:radio value="mainServer" property="serverSelect" onclick="closeServerIP()">
					<bean:message key="serverExtension.mainServer" />
				</html:radio>
				<br>
				<html:radio value="subsidiaryServer" property="serverSelect" onclick="showServerIP()">
					<bean:message key="serverExtension.subsidiaryServer" />
				</html:radio>
				<br>

				<span id="serverIP" class="serverIP"> 
					<bean:message key="serverExtension.mainServerIP" /> 
					<html:text property="inputIP" />
					<br> 
				</span>
				<c:if test="${requestScope.alreadyRegister == true && requestScope.success == false && requestScope.inputIpErr == false && requestScope.fail == false}">
					<bean:message key="serverExtension.alreadyRegister" />
				</c:if>
				<c:if test="${requestScope.alreadyRegister == false && requestScope.success == true && requestScope.inputIpErr == false && requestScope.fail == false}">
					<bean:message key="serverExtension.success" />
				</c:if>
				<c:if test="${requestScope.alreadyRegister == false && requestScope.success == false && requestScope.inputIpErr == true && requestScope.fail == false}">
					<bean:message key="serverExtension.inputIpErr" />
				</c:if>
				<c:if test="${requestScope.alreadyRegister == false && requestScope.success == false && requestScope.inputIpErr == false && requestScope.fail == true}">
					<bean:message key="serverExtension.fail" />
				</c:if>
				<br>
				<html:submit property="submit" value="确定" />

			</html:form>
		</div>
		<div id="serverExtensionEdit" class="edit">
			serverExtensionEdit div
		</div>
		<div id="serverExtensionHelp" class="help">
			serverExtensionHelp div
		</div>
	</body>
</html>
