<%@ page language="java" pageEncoding="utf-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<html>

	<head>
		<base href="<%=basePath%>">

		<title>JSP for CalendarForm form</title>

		<script language="javascript" src="<%=path%>/js/calendar.js"></script>

		<jsp:useBean id="calendarForm" scope="session"
			class="edu.ncut.vod.struts.form.CalendarForm" />	
			
		<link rel="stylesheet" type="text/css" href="../css/calendar.css">
	</head>
	
	<body>			
		<!-- calendar model -->
		<c:import url="../pages/portletHeader/calendarPortletHeader.jsp" />
		<div id="calendarView" class="view">				
		<html:form action="/calendar" focus="month">
					${calendarForm.year}<bean:message key="calendar.year" />${calendarForm.monthInteger + 1} <bean:message
				key="calendar.month" />
			<table class="tableOfCalendarMessage" align="center">
				<tr>
					<th class="tableHeaderOfCalendarMessage">
						<font color="red"><bean:message key="calendar.day" /> </font>
					</th>
					<th class="tableHeaderOfCalendarMessage">
						<bean:message key="calendar.one" />
					</th>
					<th class="tableHeaderOfCalendarMessage">
						<bean:message key="calendar.two" />
					</th>
					<th class="tableHeaderOfCalendarMessage">
						<bean:message key="calendar.three" />
					</th>
					<th class="tableHeaderOfCalendarMessage">
						<bean:message key="calendar.four" />
					</th>
					<th class="tableHeaderOfCalendarMessage">
						<bean:message key="calendar.five" />
					</th>
					<th class="tableHeaderOfCalendarMessage">
						<font color="green"> <bean:message key="calendar.six" /> </font>
					</th>
				</tr>
				<c:forEach var="j" begin="0" end="5">
					<tr>
						<c:forEach var="i" begin="${j * 7}" end="${(j + 1) * 7 - 1}">
							<td class="tableOfCalendar">
								<a
									href="/VideoOnDemand_v7/calendar.do?year=${calendarForm.year}&month=${calendarForm.monthInteger + 1}&date=${calendarForm.days[i]}"
									target="_self">${calendarForm.days[i]}</a>
								<!-- 
								target为_self时，在本窗口中显示；target为_blank时，在新窗口中显示； 
								-->
							</td>
						</c:forEach>
					</tr>
				</c:forEach>
			</table>
			<table border="0" width="168" height="20">
				<tr>
					<td width=30%>
						<select name="month" size="1" onchange="changeMonth()">
							<option value="0">
								<bean:message key="calendar.january" />
							</option>
							<option value="1">
								<bean:message key="calendar.february" />
							</option>
							<option value="2">
								<bean:message key="calendar.march" />
							</option>
							<option value="3">
								<bean:message key="calendar.april" />
							</option>
							<option value="4">
								<bean:message key="calendar.may" />
							</option>
							<option value="5">
								<bean:message key="calendar.june" />
							</option>
							<option value="6">
								<bean:message key="calendar.july" />
							</option>
							<option value="7">
								<bean:message key="calendar.august" />
							</option>
							<option value="8">
								<bean:message key="calendar.september" />
							</option>
							<option value="9">
								<bean:message key="calendar.october" />
							</option>
							<option value="10">
								<bean:message key="calendar.november" />
							</option>
							<option value="11">
								<bean:message key="calendar.december" />
							</option>
						</select>
					</td>
					<td width=28%>
						<input type=text name="year" value="${calendarForm.year}" size=4
							maxlength=4>
					</td>
					<td>
						<bean:message key="calendar.year" />
					</td>
					<td width=28%>
						<input type=submit value="查看">
					</td>
				</tr>
			</table>
		</html:form>
		</div>
		<div id="calendarEdit" class="edit">
			calendarEdit div
		</div>
		<div id="calendarHelp" class="help">
			calendarHelp div
		</div>
	</body>
</html>

