<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%@ page
	import="java.util.*,com.jspsmart.upload.*,edu.ncut.vod.commonbeans.*"
	errorPage=""%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'fileUpload.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

		<link rel="stylesheet" type="text/css" href="<%=path%>/css/share.css">
		<link rel="stylesheet" type="text/css"
			href="<%=path%>/css/fileUpload.css">
	</head>

	<body>
		<!-- fileUpload model -->
		<c:import url="../pages/portletHeader/fileUploadPortletHeader.jsp" />
		<div id="fileUploadView" class="view">
			<br>
			<center class="pleaseChooseUploadFile">
				<bean:message key="fileUpload.pleaseChooseUploadFile" />
			</center>
			<form action="<%=path%>/servlet/FileUploadServlet" method="post"
				enctype="multipart/form-data">
				<table class="fileUploadTable">
					<tr>
						<td class="fileUploadTd">
							1、
							<input type="file" name="file1">
						</td>
					</tr>
					<tr>
						<td class="fileUploadTd">
							2、
							<input type="file" name="file2">
						</td>
					</tr>

					<tr>
						<td class="fileUploadTd">
							<html:submit property="submit" value="上传" />
						</td>
					</tr>
				</table>
			</form>
			<br>

			<!-- print information about current file on browser. -->
			<center>
				<logic:present name="uploadedFile" scope="session">
					<bean:message key="fileUpload.informationInDetail" />
					<c:forEach var="i" begin="0" end="1">
						<TABLE class="uploadFileInformation">
							<TR>
								<TD>
									表单项名（FieldName）
								</TD>
								<TD>
									${sessionScope.uploadedFile[i].fieldName}
								</TD>
							</TR>
							<TR>
								<TD>
									文件长度（Size）
								</TD>
								<TD>
									${sessionScope.uploadedFile[i].size}
								</TD>
							</TR>
							<TR>
								<TD>
									文件名（FileName）
								</TD>
								<TD>
									${sessionScope.uploadedFile[i].fileName}
								</TD>
							</TR>
							<TR>
								<TD>
									文件扩展名（FileExt）
								</TD>
								<TD>
									${sessionScope.uploadedFile[i].fileExt}
								</TD>
							</TR>
							<TR>
								<TD>
									文件全名（FilePathName）
								</TD>
								<TD>
									${sessionScope.uploadedFile[i].filePathName}
								</TD>
							</TR>
						</TABLE>
					</c:forEach>
				</logic:present>
			</center>
			<BR>
		</div>
		<div id="fileUploadEdit" class="edit">
			fileUploadEdit div
		</div>
		<div id="fileUploadHelp" class="help">
			fileUploadHelp div
		</div>
	</body>
</html>
