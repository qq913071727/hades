<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<%@ taglib uri="control" prefix="control"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'searchAndPagingShoe.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

		<link rel="stylesheet" type="text/css" href="../css/index.css">
		<link rel="stylesheet" type="text/css" href="../css/share.css">
		
		<script language="javascript" src="../js/searchAndPagingShow.js"></script>
	</head>

	<body>
		<!-- searchAndPagingShow model -->
		<c:import url="../pages/portletHeader/searchAndPagingShowPortletHeader.jsp" />
		<div id="searchAndPagingShowView" class="view">
		<html:form action="/searchInformationAboutVideo" focus="keyword"
			onsubmit="return searchValidate();">

			<bean:message key="index.pleaseEnterKeyword" />
			<html:text property="keyword" styleClass=""></html:text>
			<input type="hidden" name="searchAndPagingShow.keyword"
				value="<bean:message key='searchAndPagingShow.keyword'/>" />

			<bean:message key="index.byCategory" />
			<html:select property="searchBy">
				<html:option value="allCategories">
					<bean:message key="index.allCategories" />
				</html:option>
				<html:option value="computer">
					<bean:message key="index.computer" />
				</html:option>
			</html:select>

			<html:submit property="submit" value="搜索"></html:submit>
			<br />

			<!-- This is self-defined tag.
					 To know in details,look at four files: web.xml,SelfDefinedTag.tld,
					 VideoInformationTag.java and Pageable.java -->
			<control:VideoInformation />

		</html:form>
		</div>
		<div id="searchAndPagingShowEdit" class="edit">
			searchAndPagingShowEdit div
		</div>
		<div id="searchAndPagingShowHelp" class="help">
			searchAndPagingShowHelp div
		</div>
	</body>
</html>
