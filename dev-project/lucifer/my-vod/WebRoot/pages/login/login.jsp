<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'login.jsp' starting page</title>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">

		<jsp:useBean id="loginForm" scope="session"
			class="edu.ncut.vod.struts.form.LoginForm" />

		<script type="text/javascript" charset="UTF-8" language="javascript"
			src="<%=path%>/js/login.js"></script>
		<script language="javascript" src="../js/allOnLoadFunction.js"></script>

		<link rel="stylesheet" type="text/css" href="<%=path%>/css/login.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/css/share.css">

	</head>

	<body>
		<P>
			<!-- It dose not need to determine attribute 'name' in tag 'html:form'.
				When jsp file runs,it is added automatically.Attribute 'name' is defined in 
				attribute 'action' of tag 'action-mapping' of file 'struts-config.xml' -->
			<html:form action="/login?method=executeLogin" focus="username"
				onsubmit="return loginValidate();">
				<table align="center" class="loginFormTable">
					<tr>
						<td>
							<font class="login_ch"> <bean:message key="login.userName" />:
							</font>
						</td>
						<td>
							<html:text property="userName" styleClass="login_input"></html:text>
							<input type="hidden" name="login.userNameCanNotBeEmpty"
								value="<bean:message key='login.userNameCanNotBeEmpty'/>" />
						</td>
					<tr>
						<td>
							<font class="login_ch"> <bean:message key="login.password" />:
							</font>
						</td>
						<td>
							<html:password property="password" redisplay="false"
								styleClass="login_input"></html:password>
							<input type="hidden" name="login.passwordCanNotBeEmpty"
								value="<bean:message key='login.passwordCanNotBeEmpty'/>" />
						</td>
					<tr>
						<td>
							<font class="login_ch"> <bean:message
									key="login.determinePassword" />: </font>
						</td>
						<td>
							<html:password property="determinePassword" redisplay="false"
								styleClass="login_input"></html:password>
							<input type="hidden" name="login.determinePasswordCanNotBeEmpty"
								value="<bean:message key='login.determinePasswordCanNotBeEmpty'/>" />
							<input type="hidden"
								name="login.passwordIsNotEqualWithDeterminepassword"
								value="<bean:message key='login.passwordIsNotEqualWithDeterminepassword'/>" />
						</td>
					<tr>
						<td>
							<font class="login_ch"> <bean:message
									key="login.authentication" />: </font>
						</td>
						<td>
							<html:text property="authentication"
								styleClass="login_authentication"></html:text>
							<img border=0 src="<%=path%>/pages/login/image.jsp">
							<input type="hidden" name="login.authenticationCanNotBeEmpty"
								value="<bean:message key='login.authenticationCanNotBeEmpty'/>" />
						</td>
					</tr>
					<c:if test="${loginForm.authenticationIsWrong == true}">
						<tr>
							<td colspan="2" class="authenticationIsWrong">
								<bean:message key='login.authenticationIsWrong' />
							</td>
						</tr>
					</c:if>
					<tr>
						<td colspan="2">
							<html:submit property="submit" value="登录"></html:submit>
							<html:reset value="重置"></html:reset>
							<html:link forward="forgetPassword" styleClass="">
								<bean:message key="login.forgetPassword" />？
							</html:link>
						</td>
					</tr>
				</table>
				<!-- <input type="button" value="查看" onclick="test();"/> -->
			</html:form>
		</P>
	</body>
</html>
