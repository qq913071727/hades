<%@ page language="java" import="java.util.*" pageEncoding="utf-8"
	errorPage="exception.jsp"%>

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>===在线考试系统===</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">

		<link href="<%=path%>/css/examOnline.css" rel="stylesheet"
			type="text/css">
		<script type='text/javascript' src='js/tree.js'></script>

	</head>

	<body>
		<f:view>
			<f:loadBundle basename="edu.ncut.vod.jsf.examonline.MessageBundle"
				var="bundle" />
			<div>
				<h:form id="listStudent" onsubmit="return check();">
					<h:panelGrid columnClasses="addStudent" styleClass="addStudent"
						headerClass="header" columns="1">

						<f:facet name="header">
							<h:graphicImage url="/images/200.jpg" styleClass="headerPicture" />
						</f:facet>

						<h:panelGroup>
							<h:panelGrid columns="2">

								<h:panelGroup styleClass="left">
									<jsp:include page="left.jsp" />
								</h:panelGroup>

								<h:panelGroup>

									<h:panelGroup styleClass="titleImage">
										<h:outputText value="#{bundle['listStudent']}" />
									</h:panelGroup>

									<h:panelGroup>
										<center>
											<h:outputText value="#{bundle['allExamStudentInformation']}" />
										</center>
									</h:panelGroup>

									<h:dataTable value="#{listStudentBean.studentList}" var="items">
										<h:column>
											<f:facet name="header">
												<h:outputText value="#{bundle['identificationNumber']}" styleClass="tableTitle" />
											</f:facet>
											<h:outputText value="#{items.humanId}" />
										</h:column>
										<h:column>
											<f:facet name="header">
												<h:outputText value="#{bundle['name']}" styleClass="tableTitle" />
											</f:facet>
											<h:outputText value="#{items.stuName}" />
										</h:column>
										<h:column>
											<f:facet name="header">
												<h:outputText value="#{bundle['class']}" styleClass="tableTitle" />
											</f:facet>
											<h:outputText value="#{items.className}" />
										</h:column>
										<h:column>
											<f:facet name="header">
												<h:outputText value="#{bundle['numberOfStudent']}" styleClass="tableTitle" />
											</f:facet>
											<h:outputText value="#{items.stuNumber}" />
										</h:column>
										<h:column>
											<f:facet name="header">
												<h:outputText value="#{bundle['Email']}" styleClass="tableTitle" />
											</f:facet>
											<h:outputText value="#{items.stuMail}" />
										</h:column>
										<h:column>
											<f:facet name="header">
												<h:outputText value="#{bundle['isDelete']}" styleClass="tableTitle" />
											</f:facet>
											<h:outputText value="删除" />
										</h:column>
									</h:dataTable>

									<h:panelGroup>

										第<h:outputText value="#{listStudentBean.currentPage}" />页&nbsp
										共<h:outputText value="#{listStudentBean.pageCount}" />页&nbsp
										<h:commandLink id="firstPage" action="#{listStudentBean.first}">
											<h:outputText value="首页" escape="false" />
											<f:attribute name="pageNo" value="1" />
										</h:commandLink>

										<h:panelGroup rendered="#{listStudentBean.currentPage>1}">
											<h:commandLink id="previousPage" action="#{listStudentBean.prev}">
												<h:outputText value="上一页" escape="false" />
												<f:attribute name="pageNo" value="#{listStudentBean.currentPage - 1}" />
											</h:commandLink>
										</h:panelGroup>

										<h:panelGroup
											rendered="#{listStudentBean.currentPage<listStudentBean.pageCount}">
											<h:commandLink id="nextPage" action="#{listStudentBean.next}">
												<h:outputText value="下一页" escape="false" />
												<f:attribute name="pageNo" value="#{listStudentBean.currentPage + 1}" />
											</h:commandLink>
										</h:panelGroup>

										<h:panelGroup>
											<h:commandLink id="lastPage" action="#{listStudentBean.last}">
												<h:outputText value="尾页" escape="false" />
												<f:attribute name="pageNo" value="#{listStudentBean.pageCount}" />
											</h:commandLink>
										</h:panelGroup>

									</h:panelGroup>

								</h:panelGroup>

							</h:panelGrid>

						</h:panelGroup>

					</h:panelGrid>


				</h:form>
			</div>
		</f:view>
	</body>
</html>
