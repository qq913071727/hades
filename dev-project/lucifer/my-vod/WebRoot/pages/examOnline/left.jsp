<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>===在线考试系统===</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">

		<script type="text/javascript" src="<%=path%>/js/tree.js"></script>

	</head>

	<body>
			<h:panelGrid columns="1">

				<h:commandLink id="questionDataManagement" actionListener="#{leftBean.questionDataManagementActionListener}">
					<h:outputText value="#{bundle['questionDataManagement']}" />
				</h:commandLink>

				<h:panelGroup id="questionChild" rendered="#{leftBean.questionChildDisplay}" styleClass="child"> 
					<h:panelGrid columns="1">
						<h:commandLink id="addExamType" action="addTest">
							<h:outputText value="#{bundle['addExamType']}" /> 
						</h:commandLink>
						<h:commandLink id="browseAllQuestion" action="listQuestion">
							<h:outputText value="#{bundle['browseAllQuestion']}" />
						</h:commandLink>
						<h:commandLink id="addQuestion" action="addQuestion">
							<h:outputText value="#{bundle['addQuestion']}" />
						</h:commandLink>
					</h:panelGrid>
				</h:panelGroup>
				<br/>

				<h:commandLink id="studentDataManagement" action="self" actionListener="#{leftBean.studentDataManagementActionListener}">
					<h:outputText value="#{bundle['studentDataManagement']}" />
				</h:commandLink>

				<h:panelGroup id="studentChild" rendered="#{leftBean.studentChildDisplay}" styleClass="child">
					<h:panelGrid columns="1">
						<h:commandLink id="browseAllStudent" action="listStudent">
							<h:outputText value="#{bundle['browseAllStudent']}" />
						</h:commandLink>
						<h:commandLink id="addStudentData" action="addStudent">
							<h:outputText value="#{bundle['addStudentData']}" />
						</h:commandLink>
					</h:panelGrid>
				</h:panelGroup>				

			</h:panelGrid>

	</body>
</html>