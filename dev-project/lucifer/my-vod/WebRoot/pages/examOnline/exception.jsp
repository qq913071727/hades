<%@ page language="java" import="java.util.*" pageEncoding="utf-8"
	isErrorPage="true"%>

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'exception.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">

		<link href="<%=path%>/css/examOnline.css" rel="stylesheet"
			type="text/css">
	</head>

	<body>
		<f:view>
			<div align="center">

				<h:panelGrid columnClasses="exception" styleClass="exception"
					headerClass="header" columns="1">

					<f:facet name="header">
						<h:graphicImage url="/images/200.jpg" styleClass="headerPicture" />
					</f:facet>

					<h:outputText escape="false" value="<br><br><br><br><br><br>系统处理过程中发生了异常，信息如下：" />
					<h:outputText escape="false" value="#{exception.message}" />
					<h:outputText escape="false"
						value="请您先核对输入，如果再次出现该错误，请与站长联系。kongyeeku@163.com 谢谢。<br><br><br><br><br><br>" />

				</h:panelGrid>

				<jsp:include page="footer.jsp" />
			</div>
		</f:view>
	</body>
</html>
