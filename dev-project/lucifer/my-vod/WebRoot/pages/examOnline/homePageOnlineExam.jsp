<%@ page language="java" import="java.util.*" pageEncoding="utf-8" errorPage="exception.jsp"%>

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
	<head>
		<base href="<%=basePath%>">

		<title>===在线考试系统===</title>
 
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">

		<link href="<%=path%>/css/examOnline.css" rel="stylesheet"
			type="text/css">

	</head>

	<body>
		<f:view>
			<f:loadBundle basename="edu.ncut.vod.jsf.examonline.MessageBundle"
				var="bundle" />
			<div>
				<h:form id="selectLink">
					<h:panelGrid footerClass="footer" styleClass="selectLink" headerClass="header"
						columns="1" columnClasses="selectLink">
						
						<f:facet name="header">
							<h:graphicImage url="/images/200.jpg" styleClass="headerPicture" />
						</f:facet>
						
						<h:panelGroup>
							<h:commandLink action="startExamOnline">
								<h:outputText value="#{bundle['startExamOnline']}" />
							</h:commandLink>
							<br>
							<h:commandLink action="enterManagementBackground">
								<h:outputText value="#{bundle['enterManagementBackground']}" />
							</h:commandLink>
						</h:panelGroup>
							
						<f:facet name="footer">
							<h:panelGrid styleClass="footer">
								<h:outputText value="All Rights Reserved."/>
								<h:outputText value="版权所有 Copyright@2006: Yeeku.H.Lee" />
								<h:panelGroup>
									<h:outputText escape="false" value="如有任何问题和建议，" />
									<h:outputLink value="mailto:kongyeeku@163.com">
										<h:outputText escape="false" value="请E-mail to me！" />
									</h:outputLink>
								</h:panelGroup>								
								<h:outputText escape="false"
									value="建议您使用1024*768分辨率，IE5.0以上版本浏览本站!" />
							</h:panelGrid>
						</f:facet>
						
					</h:panelGrid>
				</h:form>
			</div>
		</f:view>
	</body>
</html>

