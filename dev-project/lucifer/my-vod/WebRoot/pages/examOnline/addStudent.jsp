<%@ page language="java" import="java.util.*" pageEncoding="utf-8" errorPage="exception.jsp"%>

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>===在线考试系统===</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">

		<link href="<%=path%>/css/examOnline.css" rel="stylesheet"
			type="text/css">

		<script language="javascript" src="<%=path%>/js/addStudent.js"></script>

	</head>

	<body>
		<f:view>
			<f:loadBundle basename="edu.ncut.vod.jsf.examonline.MessageBundle"
				var="bundle" />
			<div>
				<h:form id="addStudent" onsubmit="return check();">
					<h:panelGrid columnClasses="addStudent" styleClass="addStudent"
						headerClass="header" columns="1">

						<f:facet name="header">
							<h:graphicImage url="/images/200.jpg" styleClass="headerPicture" />
						</f:facet>

						<h:panelGroup>
							<h:panelGrid columns="2">

								<h:panelGroup styleClass="left">
									<jsp:include page="left.jsp" />
								</h:panelGroup>

								<h:panelGroup>

									<h:panelGroup styleClass="titleImage">
										<h:outputText value="#{bundle['addStudent']}" />
									</h:panelGroup>
									
									<h:panelGroup>
										<h:selectBooleanCheckbox id="showNote" value="#{addStudentBean.showNote}" onclick="submit();"/>
										<h:outputText value="#{bundle['whetherShowNote']}" />											
									</h:panelGroup>
									
									<h:panelGrid columnClasses="addStudent" styleClass="addStudent"
										columns="4">
										<h:outputText value="#{bundle['identificationNumber']}:" />
										<h:inputText id="identificationNumber" label="identificationNumber"
											binding="#{addStudentBean.identificationNumber}">
											<f:validator validatorId="identificationNumberValidator"/>	
										</h:inputText>
										<h:outputText value="#{bundle['name']}:" />
										<h:inputText id="name" label="name" binding="#{addStudentBean.name}">
											<f:validator validatorId="nameValidator"/>	
										</h:inputText>
									</h:panelGrid>

									<h:panelGrid columns="2">
										<h:panelGroup styleClass="notice">
											<h:outputText rendered="#{addStudentBean.identificationNumberNotice == false}" value="#{bundle['identificationNumberLengthMustBe15Or18AndMustBeNumberOrEnglish']}" />
											<h:message for="identificationNumber" rendered="#{addStudentBean.identificationNumberMessage == true}" styleClass="errorMessage" />											
										</h:panelGroup>
										<h:panelGroup styleClass="notice">
											<h:outputText rendered="#{addStudentBean.nameNotice == false}" value="#{bundle['nameMustBeChinese']}" />
											<h:message for="name" rendered="#{addStudentBean.nameMessage == true}" styleClass="errorMessage" />
										</h:panelGroup>
									</h:panelGrid>

									<h:panelGrid columnClasses="addStudent" styleClass="addStudent"
										columns="4">
										<h:outputText value="#{bundle['numberOfStudent']}:" />
										<h:inputText id="numberOfStudent" label="numberOfStudent"
											binding="#{addStudentBean.numberOfStudent}">
											<f:validator validatorId="numberOfStudentValidator"/>	
										</h:inputText>
										<h:outputText value="#{bundle['class']}:" />
										<h:inputText id="class" label="class" binding="#{addStudentBean.theClass}">
											<f:validator validatorId="classValidator"/>
										</h:inputText>
									</h:panelGrid>
									
									<h:panelGrid columns="2">
										<h:panelGroup styleClass="notice">
											<h:outputText rendered="#{addStudentBean.numberOfStudentNotice == false}" value="#{bundle['numberOfStudentMustBeNumberOrEnglish']}" />
											<h:message for="numberOfStudent" rendered="#{addStudentBean.numberOfStudentMessage == true}" styleClass="errorMessage" />
										</h:panelGroup>
										<h:panelGroup styleClass="notice">
											<h:outputText rendered="#{addStudentBean.classNotice == false}" value="#{bundle['classCanNotBeLongerThan12']}" />
											<h:message for="class" rendered="#{addStudentBean.classMessage == true}" styleClass="errorMessage" />
										</h:panelGroup>
									</h:panelGrid> 
									
									<h:panelGrid columnClasses="addStudent" styleClass="addStudent"
										columns="4">
										<h:outputText value="#{bundle['address']}:" />
										<h:inputText id="address" label="address" binding="#{addStudentBean.address}">
											<f:validator validatorId="addressValidator"/>	
										</h:inputText>
										<h:outputText value="#{bundle['telephone']}:" />
										<h:inputText id="telephone" label="telephone"
											binding="#{addStudentBean.telephone}">
											<f:validator validatorId="telephoneValidator"/>	
										</h:inputText>
									</h:panelGrid>
									
									<h:panelGrid columns="2">
										<h:panelGroup styleClass="notice">
											<h:outputText rendered="#{addStudentBean.addressNotice == false}" value="#{bundle['addressLenghtCanNotBeLongerThan20']}" />
											<h:message for="address" rendered="#{addStudentBean.addressMessage == true}" styleClass="errorMessage" />
										</h:panelGroup>
										<h:panelGroup styleClass="notice">
											<h:outputText rendered="#{addStudentBean.telephoneNotice == false}" value="#{bundle['telephoneContentMustBeNumberInCountry']}" />
											<h:message for="telephone" rendered="#{addStudentBean.telephoneMessage == true}" styleClass="errorMessage" />
										</h:panelGroup>
									</h:panelGrid>
									
									<h:panelGrid columnClasses="addStudent" styleClass="addStudent"
										columns="4">
										<h:outputText value="#{bundle['Email']}:" />
										<h:inputText id="Email" label="Email" binding="#{addStudentBean.EMail}">
											<f:validator validatorId="emailValidator"/>
										</h:inputText>
										<h:outputText value="#{bundle['humanId']}:" />
										<h:inputText id="humanId" label="humanId" binding="#{addStudentBean.humanId}">
											<f:validator validatorId="humanIdValidator"/>
										</h:inputText>
									</h:panelGrid>
									
									<h:panelGrid columns="2">
										<h:panelGroup styleClass="notice">
											<h:outputText rendered="#{addStudentBean.emailNotice == false}" value="#{bundle['EmailContentMustBeFormatOfEmail']}" />
											<h:message for="Email" rendered="#{addStudentBean.emailMessage == true}" styleClass="errorMessage" />
										</h:panelGroup>
										<h:panelGroup styleClass="notice">
											<h:outputText rendered="#{addStudentBean.humanIdNotice == false}" value="#{bundle['humanIdLengthMustBe15Or18AndMustBeNumberOrEnglish']}" />
											<h:message for="humanId" rendered="#{addStudentBean.humanIdMessage == true}" styleClass="errorMessage" />
										</h:panelGroup>
									</h:panelGrid> 

									<h:panelGroup>
										<h:panelGrid columns="2" styleClass="login">
											<h:commandButton type="submit" value="#{bundle['submit']}"
												action="#{addStudentBean.getRedirect}"
												actionListener="#{addStudentBean.addStudentActionListener}" />
											<h:commandButton type="reset" value="#{bundle['reset']}" />
										</h:panelGrid>
									</h:panelGroup>
								</h:panelGroup>

							</h:panelGrid>
						</h:panelGroup>

					</h:panelGrid>
				</h:form>

				<h:panelGrid styleClass="footer">
					<h:outputText value="All Rights Reserved." />
					<h:outputText value="版权所有 Copyright@2006: Yeeku.H.Lee" />
					<h:panelGroup>
						<h:outputText escape="false" value="如有任何问题和建议，" />
						<h:outputLink value="mailto:kongyeeku@163.com">
							<h:outputText escape="false" value="请E-mail to me！" />
						</h:outputLink>
					</h:panelGroup>
					<h:outputText escape="false"
						value="建议您使用1024*768分辨率，IE5.0以上版本浏览本站!" />
				</h:panelGrid>

			</div>
		</f:view>
	</body>
</html>
