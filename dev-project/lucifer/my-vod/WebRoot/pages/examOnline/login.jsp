<%@ page language="java" import="java.util.*" pageEncoding="utf-8"
	errorPage="exception.jsp"%>

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'login.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">

		<link href="<%=path%>/css/examOnline.css" rel="stylesheet"
			type="text/css">

		<script language="javascript" src="<%=path%>/js/loginExamOnline.js"></script>

	</head>

	<body>
		<f:view>
			<f:loadBundle basename="edu.ncut.vod.jsf.examonline.MessageBundle"
				var="bundle" />
			<div>
				<h:form id="login" onsubmit="return check();">
					<h:panelGrid styleClass="login" headerClass="header" columns="1"
						columnClasses="login">

						<f:facet name="header">
							<h:graphicImage url="/images/200.jpg" styleClass="headerPicture" />
						</f:facet>

						<h:panelGroup>
							<h:outputText value="#{requestScope.msg}" />
						</h:panelGroup>
  
						<!-- 
						这个panelGroup中用到了自定义验证器,下面是标准做法 
						-->
						<h:panelGroup>
							<h:outputText value="#{sessionScope.msg}"/>
							
							<h:panelGrid columns="4" styleClass="login">
								
								<h:outputText value="#{bundle['manager']}:" />
								<h:inputText id="managerName" label="managerName"
									binding="#{loginBean.managerName}">
									<f:validator validatorId="managerNameValidator" />
								</h:inputText>
								
								<h:outputText value="#{bundle['password']}:" />
								<h:inputSecret id="password" label="password"
									binding="#{loginBean.password}">
									<f:validator validatorId="passwordValidator" />
								</h:inputSecret>
								
							</h:panelGrid>
							
							<h:panelGrid columns="2">
								
								<h:panelGroup styleClass="errorMessage">
									<h:message for="managerName" />
								</h:panelGroup>
								
								<h:panelGroup styleClass="errorMessage">
									<h:message for="password" />
								</h:panelGroup>
								
							</h:panelGrid>
							
						</h:panelGroup>

						<h:panelGroup>
							<h:panelGrid columns="2" styleClass="login">
								<h:commandButton type="submit" value="#{bundle['submit']}"
									actionListener="#{loginBean.loginActionListener}"
									action="#{loginBean.getRedirect}" />
								<h:commandButton type="reset" value="#{bundle['reset']}" />
							</h:panelGrid>
						</h:panelGroup>

					</h:panelGrid>
				</h:form>

				<h:panelGrid styleClass="footer">
					<h:outputText value="All Rights Reserved." />
					<h:outputText value="版权所有 Copyright@2006: Yeeku.H.Lee" />
					<h:panelGroup>
						<h:outputText escape="false" value="如有任何问题和建议，" />
						<h:outputLink value="mailto:kongyeeku@163.com">
							<h:outputText escape="false" value="请E-mail to me！" />
						</h:outputLink>
					</h:panelGroup>
					<h:outputText escape="false"
						value="建议您使用1024*768分辨率，IE5.0以上版本浏览本站!" />
				</h:panelGrid>

			</div>
		</f:view>
	</body>
</html>
