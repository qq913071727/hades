<%@ page language="java" import="java.util.*" pageEncoding="utf-8"
	errorPage="exception.jsp"%>

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>===在线考试系统===</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">

		<SCRIPT language="javascript">
			function check() {
				//判断是否填写试卷名称
				if (document.addTestForm.testName.value == "") {
					alert("试卷名称不能为空");
					document.addTestForm.testName.focus();
					return false;
				}
				//判断是否填写考试时间
				if (document.addTestForm.testTime.value == "") {
					alert("考试时间不能为空");
					document.addTestForm.testTime.focus();
					return false;
				}
		
			}
		</SCRIPT>

		<script type='text/javascript' src='<%=path%>js/tree.js'></script>
		<link href="<%=path%>/css/examOnline.css" rel="stylesheet"
			type="text/css">
	</head>

	<body>
		<f:view>
			<f:loadBundle basename="edu.ncut.vod.jsf.examonline.MessageBundle"
				var="bundle" />
			<div>
				<h:form id="addTest" onsubmit="return check();">
					<h:panelGrid columnClasses="addTest" styleClass="addTest"
						headerClass="header" columns="1">

						<f:facet name="header">
							<h:graphicImage url="/images/200.jpg" styleClass="headerPicture" />
						</f:facet>

						<h:panelGroup>
							<h:panelGrid columns="2">

								<h:panelGroup styleClass="left">
									<jsp:include page="left.jsp" />
								</h:panelGroup>

								<h:panelGroup>

									<h:panelGroup styleClass="titleImage">
										<h:outputText value="#{bundle['addTest']}" />
									</h:panelGroup>

									<h:panelGrid columns="2">

										<h:outputText value="#{bundle['examPaperName']}" />
										<h:inputText id="examPaperName" value="" />

										<h:outputText value="#{bundle['examTime']}" />
										<h:inputText id="examTime" value="" />

									</h:panelGrid>

									<h:commandButton value="" type="submit" />
									<h:commandButton value="" type="reset" />

								</h:panelGroup>
							</h:panelGrid>
						</h:panelGroup>
					</h:panelGrid>
				</h:form>

				<h:panelGrid styleClass="footer">
					<h:outputText value="All Rights Reserved." />
					<h:outputText value="版权所有 Copyright@2006: Yeeku.H.Lee" />
					<h:panelGroup>
						<h:outputText escape="false" value="如有任何问题和建议，" />
						<h:outputLink value="mailto:kongyeeku@163.com">
							<h:outputText escape="false" value="请E-mail to me！" />
						</h:outputLink>
					</h:panelGroup>
					<h:outputText escape="false"
						value="建议您使用1024*768分辨率，IE5.0以上版本浏览本站!" />
				</h:panelGrid>
				
			</div>
		</f:view>
	</body>
</html>
