<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html"%>
<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean"%>
<%@ taglib prefix="logic" uri="http://struts.apache.org/tags-logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>????</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
		<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<jsp:useBean id="adminServiceForm"
			class="edu.ncut.vod.struts.form.AdminServiceForm" scope="session" />
	</head>

	<body>
		<div>
			<html:form action="/adminService.do">
				<html:select property="tableSelection" onchange="submit();">
					<c:forEach var="selectedTableName"
						items="${adminServiceForm.tableName}" begin="0"
						end="${fn:length(adminServiceForm.tableName)}" step="1">
						<html:option value="${selectedTableName}">
							<c:out value="${selectedTableName}" />
						</html:option>
					</c:forEach>
				</html:select>
				<html:submit property="button" />
			</html:form>
			<br>
			<%-- if the content of the selected table is empty,display  the following message --%>
			<c:if test="${adminServiceForm.contentEmpty == true}">
				<bean:message key="adminService.emptyMessage" />
			</c:if>
			<%-- the content of the selected table --%>
			<c:if test="${adminServiceForm.showTable == true}">
				<table border="1">
					
					<c:forEach var="shownTableRows"
						items="${adminServiceForm.shownTableContent}" begin="0"
						end="${fn:length(adminServiceForm.shownTableContent)-1}" step="1">
						<tr>
							<c:forEach var="shownTableFields" items="${shownTableRows}"
								begin="0" end="${fn:length(shownTableRows)-1}" step="1">
								<td>
									<%--key:<c:out value="${shownTableFields.key}" />--%>
									<c:out value="${shownTableFields.value}" />
									<%--value:<c:out value="${shownTableRows}" /> --%>
								</td>
							</c:forEach>
						</tr>
					</c:forEach>
				</table>
			</c:if>
		</div>
	</body>
</html>
