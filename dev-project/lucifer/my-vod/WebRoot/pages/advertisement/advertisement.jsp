<%@ page language="java" pageEncoding="utf-8"%>
<%@ page import="java.util.*"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html:html>
<head>
	<base href="<%=basePath%>">

	<!-- refresh the page automatically by using a element 'meta' with attribute 'http-equiv'
			whose value is refresh.the attribute 'content' indicates the time of refreshing whose
			unity is minute,attribute 'url' and attribute 'charset' -->
	<!-- <meta http-equiv="refresh"
			content="10;url=http://localhost:8088/VideoOnDemand_v7/advertisement.do; charset=UTF-8"> -->
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">

	<jsp:useBean id="adBean" scope="request"
		class="edu.ncut.vod.commonbeans.ADBean" />

	<link rel="stylesheet" type="text/css"
		href="<%=path%>/css/advertisement.css">

	<script language="javascript" src="<%=path%>/js/advertisement.js"></script>
</head>

<body>
	<html:form action="/advertisement">
		<c:choose>
			<c:when test="${!empty requestScope.adBean.imgName}">
				<html:image
					page="${sessionScope.IMG_PATH}/${requestScope.adBean.imgName}"
					property="advertisementImage" styleClass="imageLink"
					onclick="OpenAD('${requestScope.adBean.url}')" />
			</c:when>
			<c:when test="${empty requestScope.adBean.imgName}">
				<html:image page="/images/1.gif" property="advertisementImage"
					styleClass="imageLink"
					onclick="OpenAD('http://sun.csdn.net/solaris/')" />
			</c:when>
			<c:otherwise>
				<html:image page="/images/1.gif" property="advertisementImage"
					styleClass="imageLink"
					onclick="OpenAD('http://sun.csdn.net/solaris/')" />
			</c:otherwise>
		</c:choose>
		<!-- when refreshed,the value of tag 'html:hidden' is null;when submitted,link -->				
		<html:hidden property="style" value="link" />
		<html:hidden property="id" value="${adBean.adid}" />
		<br />
	</html:form>

</body>
</html:html>
