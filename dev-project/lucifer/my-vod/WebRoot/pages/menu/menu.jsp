<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'menu.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		
		<link rel="stylesheet" type="text/css" href="<%=path%>/css/share.css">	
	</head>

	<body>
		<table align="center">
			<tr>
				<td>
					<html:link forward="homePage" styleClass="menuLink">
						<bean:message key="menu.homePage" />
					</html:link>
				</td>
				<td>
					<html:link forward="chatRoom" styleClass="menuLink">
						<bean:message key="menu.chatRoom" />
					</html:link>
				</td>
				<td>
					<html:link forward="onlineExamSystem" styleClass="menuLink">
						<bean:message key="menu.onlineExamSystem" />
					</html:link>
				</td>
				<td>
					<html:link forward="adminService" styleClass="menuLink">
						<bean:message key="menu.adminService" />
					</html:link>
				</td>
			</tr>
		</table>
	</body>
</html>
