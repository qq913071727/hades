<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'mediaplayer.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
		<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<link rel="stylesheet" type="text/css"
			href="<%=path%>/css/player.css">
		<link rel="stylesheet" type="text/css" href="<%=path%>/css/share.css">
	</head>

	<body>
		<!-- 左边空白 -->
		<div class="leftMargin"></div>

		<!-- 右边空白 -->
		<div class="rightMargin"></div>

		<!-- 中间部分 -->
		<div class="center">
			
			<!-- 图标和日期模块 -->
			<div class="top">

				<!-- 图标模块 -->
				<div class="picture">
					<img src="<%=path%>/images/banner.gif" align="middle" />
				</div>

				<!-- 日期模块 -->
				<div class="date">

					<!-- 显示年，月，日，星期和时间 -->
					<c:import url="../pages/dayAndTime/dayAndTime.jsp" />

					<!-- 显示Java时钟 -->
					<c:import url="../pages/applet/clockApplet.jsp" />
				</div>

			</div>
			<br>

			<!-- 菜单模块 -->
			<div class="menu">
				<c:import url="../pages/menu/menu.jsp" />
			</div>

			<!-- 中间部分的左边部分:视频播放器部分 -->
			<div class="playerMainLeft">
				<center>
					<object align=middle
						classid="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95" class=OBJECT
						id=MediaPlayer width=600 height=500>
						<param name=ShowStatusBar value=0>
						<param name=Filename value="<%=path%>/video/b.wmv">
						<embed type=application/x-oleobject
							codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701">
						</embed>
					</object>
				</center>
			</div>

			<!-- 中间部分的右边部分:视频相关信息部分 -->
			<div class="playerMainRight">
				<center>
					相关视频
				</center>
			</div>
		</div>
	</body>
</html>
