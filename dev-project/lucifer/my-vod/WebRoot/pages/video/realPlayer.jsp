<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'realplayer.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">

		<jsp:useBean id="videoManagerForm" scope="session"
			class="edu.ncut.vod.struts.form.VideoManagerForm" />
		
		<link rel="stylesheet" type="text/css" href="<%=path%>/css/player.css">
		<link rel="stylesheet" type="text/css" href="<%=path%>/css/share.css">
		
		<script language="javascript" src="<%=path%>/js/closeRealPlayer.js"></script>	
	</head>

	<body onUnload="javascript:closeVideo('${videoManagerForm.demandedVideoId}');">

		<!-- 左边空白 -->
		<div class="leftMargin"></div>

		<!-- 右边空白 -->
		<div class="rightMargin"></div>

		<!-- 中间部分 -->
		<div class="center">

			<!-- 图标和日期模块 -->
			<div class="top">

				<!-- 图标模块 -->
				<div class="picture">
					<img src="<%=path%>/images/banner.gif" align="middle" />
				</div>

				<!-- 日期模块 -->
				<div class="date">

					<!-- 显示年，月，日，星期和时间 -->
					<c:import url="../dayAndTime/dayAndTime.jsp" />

					<!-- 显示Java时钟 -->
					<c:import url="../applet/clockApplet.jsp" />
				</div>

			</div>
			<br>

			<!-- 菜单模块 -->
			<div class="menu">
				<c:import url="../menu/menu.jsp" />
			</div>

			<!-- 中间部分的左边部分:视频播放器部分 -->
			<div class="playerMainLeft">
				<center>
					<object id="vid"
						classid="clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA" width=700
						height=500>
						<param name="_ExtentX" value="11298">
						<param name="_ExtentY" value="7938">
						<param name="AUTOSTART" value="-1">
						<param name="SHUFFLE" value="0">
						<param name="PREFETCH" value="0">
						<param name="NOLABELS" value="-1">
						<param name="SRC" value="rtsp://${videoManagerForm.videoURL}:554/TeachingVideo/${videoManagerForm.informationName}">
						<param name="CONTROLS" value="Imagewindow">
						<param name="CONSOLE" value="clip1">
						<param name="LOOP" value="0">
						<param name="NUMLOOP" value="0">
						<param name="CENTER" value="0">
						<param name="MAINTAINASPECT" value="0">
						<param name="BACKGROUNDCOLOR" value="#000000">
					</object>
					<br />
					<object id="vid2"
						classid="clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA" width=700
						height=30>
						<param name="_ExtentX" value="11298">
						<param name="_ExtentY" value="794">
						<param name="AUTOSTART" value="-1">
						<param name="SHUFFLE" value="0">
						<param name="PREFETCH" value="0">
						<param name="NOLABELS" value="-1">
						<param name="SRC" value="rtsp://${videoManagerForm.videoURL}:554/TeachingVideo/${videoManagerForm.informationName}">
						<param name="CONTROLS" value="ControlPanel">
						<param name="CONSOLE" value="clip1">
						<param name="LOOP" value="0">
						<param name="NUMLOOP" value="0">
						<param name="CENTER" value="0">
						<param name="MAINTAINASPECT" value="0">
						<param name="BACKGROUNDCOLOR" value="#000000">
					</object>
				</center>
			</div>			
			
			<!-- 中间部分的右边部分:视频相关信息部分 -->
			<div class="playerMainRight">
				<center>
					相关视频
				</center>
			</div>
		</div>
	</body>
</html>
