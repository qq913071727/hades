<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<%
    String path = request.getContextPath();
			String basePath = request.getScheme() + "://"
					+ request.getServerName() + ":" + request.getServerPort()
					+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'loginAndRegister.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">

		<link rel="stylesheet" type="text/css" href="../css/index.css">
		<link rel="stylesheet" type="text/css" href="../css/share.css">
		<link rel="stylesheet" type="text/css"
			href="../css/loginAndRegister.css">

		<script language="javascript" src="../js/index.js"></script>
		<script language="javascript" src="../js/loginAndRegister.js"></script>

		<jsp:useBean id="loginForm" scope="session"
			class="edu.ncut.vod.struts.form.LoginForm" />

	</head>

	<body>
		<!-- loginAndRegister model -->
		<c:import url="../pages/portletHeader/loginAndRegisterPortletHeader.jsp" />
		<div id="loginAndRegisterView" class="view">
			<div id="loginAndRegister" class="loginAndRegister">
				<c:if test="${loginForm.success == true}">
					<H3>
						<bean:message key="index.hello" />
						,
						<!-- pay attention:look at the attribute 'scope' of 'action of login model' 
					in file 'struts-config.xml' carefully -->
						<bean:write name="loginForm" property="userName" scope="request" />
					</H3>
				</c:if>
				<c:if test="${loginForm.success == false}">
					<H3>
						<bean:message key="index.hello" />
						,
						<bean:message key="index.pleaseSelectLoginOrRegister" />
					</H3>
				</c:if>
				<c:if test="${loginForm.success == false}">
					<button onclick="loginLinkClick();">
						<bean:message key="index.login" />
					</button>
					<button onclick="registerLinkClick();">
						<bean:message key="index.register" />
					</button>
				</c:if>
				<c:if test="${loginForm.success == true}">
					<html:form action="/logoff?method=executeLogoff">
						<html:submit onclick="logoffLinkClick()" property="logoff"
							value="logoff">
							<bean:message key="index.logoff" />
						</html:submit>
					</html:form>
				</c:if>
			</div>
			<div id="login" class="login">
				<c:import url="../pages/login/login.jsp" />
			</div>
			<div id="register" class="register">
				<c:import url="../pages/register/register.jsp" />
			</div>
		</div>
		<div id="loginAndRegisterEdit" class="edit">
			loginAndRegisterEdit div
		</div>
		<div id="loginAndRegisterHelp" class="help">
			loginAndRegisterHelp div
		</div>
	</body>
</html>
