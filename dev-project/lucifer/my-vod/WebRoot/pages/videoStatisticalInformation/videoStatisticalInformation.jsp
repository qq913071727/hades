<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'videoStatisticalInformation.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
		<link rel="stylesheet" type="text/css" href="styles.css">
		-->

		<link rel="stylesheet" type="text/css" href="../css/index.css">
		<link rel="stylesheet" type="text/css" href="../css/share.css">

		<script language="javascript"
			src="<%=path%>/js/videoStatisticalInformation.js"></script>
		
		<jsp:useBean id="videoStatisticalInformationForm" scope="request"
			class="edu.ncut.vod.struts.form.VideoStatisticalInformationForm" />					
	</head>

	<body>
		<!-- video statistical information model -->
		<c:import
			url="../pages/portletHeader/videoStatisticalInformationPortletHeader.jsp" />
		<div id="videoStatisticalInformationView" class="view">
			<c:if test="${!empty requestScope.selectNotice || requestScope.selectNotice != '可以继续选择'}">
				<c:out value="${requestScope.selectNotice}" />				
			</c:if>
			
			<html:form action="/videoStatisticalInformation">
				<bean:message key="videoStatisticalInformation.selectedObject" />
				<html:select property="selectedObject">
					<c:forEach var="selectedObjectItems" items="${videoStatisticalInformationForm.selectedObjectItems}" begin="0" end="${fn:length(videoStatisticalInformationForm.selectedObjectItems)-1}" step="1"> 
						<html:option value="${selectedObjectItems.key}">
							<c:out value="${selectedObjectItems.value}" />
						</html:option>
					</c:forEach>					
				</html:select>
				
				<bean:message key="videoStatisticalInformation.chart" />
				<html:select property="chart">
					<c:forEach var="chartItems" items="${videoStatisticalInformationForm.chartItems}" begin="0" end="${fn:length(videoStatisticalInformationForm.chartItems)-1}" step="1">
						<html:option value="${chartItems.key}">
							<c:out value="${chartItems.value}" />
						</html:option>
					</c:forEach>
				</html:select>
				<html:submit property="submit" value="确定" />
			</html:form>
			<center>
				<c:if test="${requestScope.selectNotice == '可以继续选择'}">
					<img id="pieChart" src="<%=path%>/images/statisticalchart/${requestScope.chartName}${requestScope.randomInt}.jpg"/> 
				</c:if>
			</center>

		</div>
		<div id="videoStatisticalInformationEdit" class="edit">
			videoStatisticalInformationEdit div
		</div>
		<div id="videoStatisticalInformationHelp" class="help">
			videoStatisticalInformationHelp div
		</div>
	</body>
</html>
