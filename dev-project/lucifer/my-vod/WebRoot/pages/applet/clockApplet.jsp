<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'clockApplet.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">		
	</head>

	<body>
		<h1 align=center>
			<b><bean:message key="clockApplet.javaClock" />
			</b>
		</h1>
		<hr>
		<center>
			<FORM>
				<SELECT name="lbSelect1">
					<OPTION>
						当地时间
					<OPTION>
						夏威夷
					<OPTION>
						阿拉斯加
					<OPTION>
						墨西哥
					<OPTION>
						格林威治
					<OPTION>
						巴黎
					<OPTION>
						莫斯科
					<OPTION>
						雅加达
					<OPTION>
						香港
					<OPTION>
						东京
					<OPTION>
						悉尼
				</SELECT>
				<INPUT type="button" value="刷新"
					onClick=timezone(document.forms[0].lbSelect1.options[document.forms[0].lbSelect1.selectedIndex].text);>
			</FORM>
			<jsp:plugin type="applet" code="ClockApplet.class" codebase="/VideoOnDemand_v7/pages/applet" jreversion="1.2" height="200" width="200"> 
				<jsp:params>
					<jsp:param name="name" value="jsp"/>
				</jsp:params>

				<jsp:fallback>
            		Plugin tag OBJECT or EMBED not supported by browser. 
        		</jsp:fallback>
			</jsp:plugin>					
		</center>
	</body>
</html>
