<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'fileUploadPortletHeader.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	<link rel="stylesheet" type="text/css"
			href="<%=path%>/css/portletHeaderStyle.css">
			
		<script language="javascript" src="../js/fileUploadPortletHeader.js"></script>
  </head>
  
  <body>
    <div class="portlet-header" >
			<label class="portlet-title" >
				<bean:message key="fileUpload.title"/>
			</label>
			<ul class="portlet-options">
				<!-- there are some style problems to solve in file 'portletStyle.css' -->
				<li id="fileUploadMinimizeButton">
					<img src="<%=path%>/images/minimize_button.gif" alt="最小化" onclick="fileUploadMinimizeButtonClick()" title="最小化" />
				</li>
				<li id="fileUploadUnminimizeButton" class="unminimizeButton">
					<img src="<%=path%>/images/unminimize_button.gif" alt="取消最小化" onclick="fileUploadUnminimizeButtonClick()" title="取消最小化" />
				</li>
				<li id="fileUploadMaximizeButton">
					<img src="<%=path%>/images/maximize_button.gif" alt="最大化"
						title="最大化" />
				</li>
				<li id="fileUploadViewButton">
					<img src="<%=path%>/images/view_button.gif" alt="查看" onclick="fileUploadViewButtonClick()" title="查看" />
				</li>
				<li id="fileUploadEditButton">
					<img src="<%=path%>/images/edit_button.gif" alt="编辑" onclick="fileUploadEditButtonClick()" title="编辑" />
				</li>
				<li id="fileUploadHelpButton">
					<img src="<%=path%>/images/help_button.gif" alt="帮助" onclick="fileUploadHelpButtonnClick()" title="帮助" />
				</li>
				<li id="fileUploadRemoveButton">
					<img src="<%=path%>/images/remove_button.gif" alt="删除" onclick="fileUploadRemoveButtonClick()" title="删除" />
				</li>
			</ul>
		</div>
  </body>
</html>
