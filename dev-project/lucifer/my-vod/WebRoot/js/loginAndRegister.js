/* show div element whose id is 'login',when button whose content is '登录' is clicked */
function loginLinkClick(){
	var loginAndRegister = document.getElementById("loginAndRegister");
	var register = document.getElementById("register");
	var login = document.getElementById("login");
	loginAndRegister.style.display = "none";
	register.style.display = "none";
	login.style.display = "block";
	focusOnUserName();
}

/* show div element whose id is 'register',when button whose content is '注册' is clicked */
function registerLinkClick(){
	var loginAndRegister = document.getElementById("loginAndRegister");
	var register = document.getElementById("register");
	var login = document.getElementById("login");
	loginAndRegister.style.display = "none";
	register.style.display = "block";
	login.style.display = "none";
}

/* show div element whose id is 'loginAndRegister',when button whose value is '退出' is clicked */
function logoffLinkClick(){
	var loginAndRegister = document.getElementById("loginAndRegister");
	var register = document.getElementById("register");
	var login = document.getElementById("login");
	loginAndRegister.style.display = "";
	register.style.display = "none";
	login.style.display = "none";
}

/**
 * put focus on tag 'html:text' whose attribute 'property' is 'userName'
 * @return null
 */
function focusOnUserName(){
	document.forms["loginForm"].userName.focus();
}