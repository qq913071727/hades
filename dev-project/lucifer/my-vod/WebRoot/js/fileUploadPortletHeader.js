/* these three variables indicate which div element is being operated */
var fileUploadViewIsOpen = true;
var fileUploadEditIsOpen = false;
var fileUploadHelpIsOpen = false;


/* hide div element */
function fileUploadMinimizeButtonClick(){
	
	var fileUploadView = document.getElementById("fileUploadView");
	var fileUploadEdit = document.getElementById("fileUploadEdit");
	var fileUploadHelp = document.getElementById("fileUploadHelp");
	
	if(fileUploadViewIsOpen == true && fileUploadEditIsOpen == false && fileUploadHelpIsOpen == false){
		//alert("222222222222222222");
		fileUploadView.style.display = 'none';
		//alert("333333333333333333");
	}else
	if(fileUploadViewIsOpen == false && fileUploadEditIsOpen == true && fileUploadHelpIsOpen == false){		
		fileUploadEdit.style.display = 'none';
	}else
	if(fileUploadViewIsOpen == false && fileUploadEditIsOpen == false && fileUploadHelpIsOpen == true){		
		fileUploadHelp.style.display = 'none';
	}
	var fileUploadMinimizeButton = document.all["fileUploadMinimizeButton"];
	fileUploadMinimizeButton.style.display='none';
	//alert("111111111111111");
	var fileUploadUnminimizeButton = document.all["fileUploadUnminimizeButton"];
	fileUploadUnminimizeButton.style.display='block';
	//alert("111111111111111");
}

/* show div element */
function fileUploadUnminimizeButtonClick(){
	var fileUploadView = document.getElementById("fileUploadView");
	var fileUploadEdit = document.getElementById("fileUploadEdit");
	var fileUploadHelp = document.getElementById("fileUploadHelp");
	
	if(fileUploadViewIsOpen == true && fileUploadEditIsOpen == false && fileUploadHelpIsOpen == false){		
		fileUploadView.style.display = '';
	}else
	if(fileUploadViewIsOpen == false && fileUploadEditIsOpen == true && fileUploadHelpIsOpen == false){		
		fileUploadEdit.style.display = 'block';
	}else
	if(fileUploadViewIsOpen == false && fileUploadEditIsOpen == false && fileUploadHelpIsOpen == true) {		
		fileUploadHelp.style.display = 'block';
	}
	
	var fileUploadMinimizeButton = document.all["fileUploadMinimizeButton"];
	fileUploadMinimizeButton.style.display='';
	var fileUploadUnminimizeButton = document.all["fileUploadUnminimizeButton"];
	fileUploadUnminimizeButton.style.display='none';
}

/* show div element 'view' */
function fileUploadViewButtonClick(){
	if(fileUploadViewIsOpen != true){
		fileUploadViewIsOpen = true;
		fileUploadEditIsOpen = false;
		fileUploadHelpIsOpen = false;
	}
	var fileUploadView = document.getElementById("fileUploadView");
	fileUploadView.style.display = 'block';
	var fileUploadMinimizeButton = document.all["fileUploadMinimizeButton"];
	fileUploadMinimizeButton.style.display='';
	var fileUploadUnminimizeButton = document.all["fileUploadUnminimizeButton"];
	fileUploadUnminimizeButton.style.display='none';
	var fileUploadEdit = document.getElementById("fileUploadEdit");
	fileUploadEdit.style.display = 'none';
	var fileUploadHelp = document.getElementById("fileUploadHelp");
	fileUploadHelp.style.display = 'none';
}

/* show div element 'edit' */
function fileUploadEditButtonClick(){
	if(fileUploadEditIsOpen != true){
		fileUploadViewIsOpen = false;
		fileUploadEditIsOpen = true;
		fileUploadHelpIsOpen = false;
	}
	var fileUploadView = document.getElementById("fileUploadView");
	fileUploadView.style.display = 'none';
	var fileUploadMinimizeButton = document.all["fileUploadMinimizeButton"];
	fileUploadMinimizeButton.style.display='';
	var fileUploadUnminimizeButton = document.all["fileUploadUnminimizeButton"];
	fileUploadUnminimizeButton.style.display='none';
	var fileUploadEdit = document.getElementById("fileUploadEdit");
	fileUploadEdit.style.display = 'block';
	var fileUploadHelp = document.getElementById("fileUploadHelp");
	fileUploadHelp.style.display = 'none';
}

/* show div element 'help' */
function fileUploadHelpButtonnClick(){
	if(fileUploadHelpIsOpen != true){
		fileUploadViewIsOpen = false;
		fileUploadEditIsOpen = false;
		fileUploadHelpIsOpen = true;
	}
	var fileUploadView = document.getElementById("fileUploadView");
	fileUploadView.style.display = 'none';
	var fileUploadMinimizeButton = document.all["fileUploadMinimizeButton"];
	fileUploadMinimizeButton.style.display='';
	var fileUploadUnminimizeButton = document.all["fileUploadUnminimizeButton"];
	fileUploadUnminimizeButton.style.display='none';
	var fileUploadEdit = document.getElementById("fileUploadEdit");
	fileUploadEdit.style.display = 'none';
	var fileUploadHelp = document.getElementById("fileUploadHelp");
	fileUploadHelp.style.display = 'block';
}


