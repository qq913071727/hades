/* these three variables indicate which div element is being operated */
var loginAndRegisterViewIsOpen = true;
var loginAndRegisterEditIsOpen = false;
var loginAndRegisterHelpIsOpen = false;


/* hide div element */
function loginAndRegisterMinimizeButtonClick(){
	
	var loginAndRegisterView = document.getElementById("loginAndRegisterView");
	var loginAndRegisterEdit = document.getElementById("loginAndRegisterEdit");
	var loginAndRegisterHelp = document.getElementById("loginAndRegisterHelp");
	
	if(loginAndRegisterViewIsOpen == true && loginAndRegisterEditIsOpen == false && loginAndRegisterHelpIsOpen == false){
		loginAndRegisterView.style.display = 'none';
	}else
	if(loginAndRegisterViewIsOpen == false && loginAndRegisterEditIsOpen == true && loginAndRegisterHelpIsOpen == false){		
		loginAndRegisterEdit.style.display = 'none';
	}else
	if(loginAndRegisterViewIsOpen == false && loginAndRegisterEditIsOpen == false && loginAndRegisterHelpIsOpen == true){		
		loginAndRegisterHelp.style.display = 'none';
	}
	var loginAndRegisterMinimizeButton = document.all["loginAndRegisterMinimizeButton"];
	loginAndRegisterMinimizeButton.style.display='none';
	var loginAndRegisterUnminimizeButton = document.all["loginAndRegisterUnminimizeButton"];
	loginAndRegisterUnminimizeButton.style.display='block';
}

/* show div element */
function loginAndRegisterUnminimizeButtonClick(){
	var loginAndRegisterView = document.getElementById("loginAndRegisterView");
	var loginAndRegisterEdit = document.getElementById("loginAndRegisterEdit");
	var loginAndRegisterHelp = document.getElementById("loginAndRegisterHelp");
	
	if(loginAndRegisterViewIsOpen == true && loginAndRegisterEditIsOpen == false && loginAndRegisterHelpIsOpen == false){		
		loginAndRegisterView.style.display = '';
	}else
	if(loginAndRegisterViewIsOpen == false && loginAndRegisterEditIsOpen == true && loginAndRegisterHelpIsOpen == false){		
		loginAndRegisterEdit.style.display = 'block';
	}else
	if(loginAndRegisterViewIsOpen == false && loginAndRegisterEditIsOpen == false && loginAndRegisterHelpIsOpen == true) {		
		loginAndRegisterHelp.style.display = 'block';
	}
	
	var loginAndRegisterMinimizeButton = document.all["loginAndRegisterMinimizeButton"];
	loginAndRegisterMinimizeButton.style.display='';
	var loginAndRegisterUnminimizeButton = document.all["loginAndRegisterUnminimizeButton"];
	loginAndRegisterUnminimizeButton.style.display='none';
}

/* show div element 'view' */
function loginAndRegisterViewButtonClick(){
	if(loginAndRegisterViewIsOpen != true){
		loginAndRegisterViewIsOpen = true;
		loginAndRegisterEditIsOpen = false;
		loginAndRegisterHelpIsOpen = false;
	}
	var loginAndRegisterView = document.getElementById("loginAndRegisterView");
	loginAndRegisterView.style.display = 'block';
	var loginAndRegisterMinimizeButton = document.all["loginAndRegisterMinimizeButton"];
	loginAndRegisterMinimizeButton.style.display='';
	var loginAndRegisterUnminimizeButton = document.all["loginAndRegisterUnminimizeButton"];
	loginAndRegisterUnminimizeButton.style.display='none';
	var loginAndRegisterEdit = document.getElementById("loginAndRegisterEdit");
	loginAndRegisterEdit.style.display = 'none';
	var loginAndRegisterHelp = document.getElementById("loginAndRegisterHelp");
	loginAndRegisterHelp.style.display = 'none';
}

/* show div element 'edit' */
function loginAndRegisterEditButtonClick(){
	if(loginAndRegisterEditIsOpen != true){
		loginAndRegisterViewIsOpen = false;
		loginAndRegisterEditIsOpen = true;
		loginAndRegisterHelpIsOpen = false;
	}
	var loginAndRegisterView = document.getElementById("loginAndRegisterView");
	loginAndRegisterView.style.display = 'none';
	var loginAndRegisterMinimizeButton = document.all["loginAndRegisterMinimizeButton"];
	loginAndRegisterMinimizeButton.style.display='';
	var loginAndRegisterUnminimizeButton = document.all["loginAndRegisterUnminimizeButton"];
	loginAndRegisterUnminimizeButton.style.display='none';
	var loginAndRegisterEdit = document.getElementById("loginAndRegisterEdit");
	loginAndRegisterEdit.style.display = 'block';
	var loginAndRegisterHelp = document.getElementById("loginAndRegisterHelp");
	loginAndRegisterHelp.style.display = 'none';
}

/* show div element 'help' */
function loginAndRegisterHelpButtonnClick(){
	if(loginAndRegisterHelpIsOpen != true){
		loginAndRegisterViewIsOpen = false;
		loginAndRegisterEditIsOpen = false;
		loginAndRegisterHelpIsOpen = true;
	}
	var loginAndRegisterView = document.getElementById("loginAndRegisterView");
	loginAndRegisterView.style.display = 'none';
	var loginAndRegisterMinimizeButton = document.all["loginAndRegisterMinimizeButton"];
	loginAndRegisterMinimizeButton.style.display='';
	var loginAndRegisterUnminimizeButton = document.all["loginAndRegisterUnminimizeButton"];
	loginAndRegisterUnminimizeButton.style.display='none';
	var loginAndRegisterEdit = document.getElementById("loginAndRegisterEdit");
	loginAndRegisterEdit.style.display = 'none';
	var loginAndRegisterHelp = document.getElementById("loginAndRegisterHelp");
	loginAndRegisterHelp.style.display = 'block';
}


