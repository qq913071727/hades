/*
 * checking before submitting this form 
 */
function check() {
	
	// 判断是否填写身份证号
	if (document.forms["addStudent"].elements["addStudent:identificationNumber"].value == "") {
		alert("身份证号不能为空");
		document.forms["addStudent"].elements["addStudent:identificationNumber"].focus();
		return false;
	}
	
	// 判断是否填写考生姓名
	if (document.forms["addStudent"].elements["addStudent:name"].value == "") {
		alert("考生姓名不能为空");
		document.forms["addStudent"].elements["addStudent:name"].focus();
		return false;
	}
	
	// 判断是否填写考生学号
	if (document.forms["addStudent"].elements["addStudent:numberOfStudent"].value == "") {
		alert("考生学号不能为空");
		document.forms["addStudent"].elements["addStudent:numberOfStudent"].focus();
		return false;
	}
	
	// 判断是否填写考生班级
	if (document.forms["addStudent"].elements["addStudent:class"].value == "") {
		alert("班级不能为空");
		document.forms["addStudent"].elements["addStudent:class"].focus();
		return false;
	}

	// 判断是否填写考生地址
	if (document.forms["addStudent"].elements["addStudent:address"].value == "") {
		alert("考生地址不能为空");
		document.forms["addStudent"].elements["addStudent:address"].focus();
		return false;
	}
	
	// 判断是否填写考生电话
	if (document.forms["addStudent"].elements["addStudent:telephone"].value == "") {
		alert("考生电话不能为空");
		document.forms["addStudent"].elements["addStudent:telephone"].focus();
		return false;
	}
	
	// 判断是否填写email
	if (document.forms["addStudent"].elements["addStudent:Email"].value == "") {
		alert("email不能为空");
		document.forms["addStudent"].elements["addStudent:Email"].focus();
		return false;
	} else if (document.forms["addStudent"].elements["addStudent:Email"].value.indexOf("@") == -1) {
		alert("email不规范");
		document.forms["addStudent"].elements["addStudent:Email"].focus();
		return false;
	}

}