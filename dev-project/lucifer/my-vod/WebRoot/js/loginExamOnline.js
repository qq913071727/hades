/**
 * used in file 'pages/examOnline/login.jsp' to make sure if information for login is correct
 * @return
 */
function check() {
	// pay attention:the following codes are specially used in javascript with jsf
	// 判断是否填写用户名
	if (document.forms["login"].elements["login:managerName"].value == "") {
		alert("请输入用户名！");
		document.forms["login"].elements["login:managerName"].focus();
		return false;
	}
	// 判断是否填写密码
	if (document.forms["login"].elements["login:password"].value == "") {
		alert("请输入密码！");
		document.forms["login"].elements["login:password"].focus();
		return false;
	}
	return true;
}