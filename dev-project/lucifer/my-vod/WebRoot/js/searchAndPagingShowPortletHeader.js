/* these three variables indicate which div element is being operated */
var searchAndPagingShowViewIsOpen = true;
var searchAndPagingShowEditIsOpen = false;
var searchAndPagingShowHelpIsOpen = false;


/* hide div element */
function searchAndPagingShowMinimizeButtonClick(){
	var searchAndPagingShowView = document.getElementById("searchAndPagingShowView");
	var searchAndPagingShowEdit = document.getElementById("searchAndPagingShowEdit");
	var searchAndPagingShowHelp = document.getElementById("searchAndPagingShowHelp");
	
	if(searchAndPagingShowViewIsOpen == true && searchAndPagingShowEditIsOpen == false && searchAndPagingShowHelpIsOpen == false){
		searchAndPagingShowView.style.display = 'none';
	}else
	if(searchAndPagingShowViewIsOpen == false && searchAndPagingShowEditIsOpen == true && searchAndPagingShowHelpIsOpen == false){		
		searchAndPagingShowEdit.style.display = 'none';
	}else
	if(searchAndPagingShowViewIsOpen == false && searchAndPagingShowEditIsOpen == false && searchAndPagingShowHelpIsOpen == true){		
		searchAndPagingShowHelp.style.display = 'none';
	}
	var searchAndPagingShowMinimizeButton = document.all["searchAndPagingShowMinimizeButton"];
	searchAndPagingShowMinimizeButton.style.display='none';
	var searchAndPagingShowUnminimizeButton = document.all["searchAndPagingShowUnminimizeButton"];
	searchAndPagingShowUnminimizeButton.style.display='block';
}

/* show div element */
function searchAndPagingShowUnminimizeButtonClick(){
	var searchAndPagingShowView = document.getElementById("searchAndPagingShowView");
	var searchAndPagingShowEdit = document.getElementById("searchAndPagingShowEdit");
	var searchAndPagingShowHelp = document.getElementById("searchAndPagingShowHelp");
	
	if(searchAndPagingShowViewIsOpen == true && searchAndPagingShowEditIsOpen == false && searchAndPagingShowHelpIsOpen == false){		
		searchAndPagingShowView.style.display = '';
	}else
	if(searchAndPagingShowViewIsOpen == false && searchAndPagingShowEditIsOpen == true && searchAndPagingShowHelpIsOpen == false){		
		searchAndPagingShowEdit.style.display = 'block';
	}else
	if(searchAndPagingShowViewIsOpen == false && searchAndPagingShowEditIsOpen == false && searchAndPagingShowHelpIsOpen == true) {		
		searchAndPagingShowHelp.style.display = 'block';
	}
	
	var searchAndPagingShowMinimizeButton = document.all["searchAndPagingShowMinimizeButton"];
	searchAndPagingShowMinimizeButton.style.display='';
	var searchAndPagingShowUnminimizeButton = document.all["searchAndPagingShowUnminimizeButton"];
	searchAndPagingShowUnminimizeButton.style.display='none';
}

/* show div element 'view' */
function searchAndPagingShowViewButtonClick(){
	if(searchAndPagingShowViewIsOpen != true){
		searchAndPagingShowViewIsOpen = true;
		searchAndPagingShowEditIsOpen = false;
		searchAndPagingShowHelpIsOpen = false;
	}
	var searchAndPagingShowView = document.getElementById("searchAndPagingShowView");
	searchAndPagingShowView.style.display = 'block';
	var searchAndPagingShowMinimizeButton = document.all["searchAndPagingShowMinimizeButton"];
	searchAndPagingShowMinimizeButton.style.display='';
	var searchAndPagingShowUnminimizeButton = document.all["searchAndPagingShowUnminimizeButton"];
	searchAndPagingShowUnminimizeButton.style.display='none';
	var searchAndPagingShowEdit = document.getElementById("searchAndPagingShowEdit");
	searchAndPagingShowEdit.style.display = 'none';
	var searchAndPagingShowHelp = document.getElementById("searchAndPagingShowHelp");
	searchAndPagingShowHelp.style.display = 'none';
}

/* show div element 'edit' */
function searchAndPagingShowEditButtonClick(){
	if(searchAndPagingShowEditIsOpen != true){
		searchAndPagingShowViewIsOpen = false;
		searchAndPagingShowEditIsOpen = true;
		searchAndPagingShowHelpIsOpen = false;
	}
	var searchAndPagingShowView = document.getElementById("searchAndPagingShowView");
	searchAndPagingShowView.style.display = 'none';
	var searchAndPagingShowMinimizeButton = document.all["searchAndPagingShowMinimizeButton"];
	searchAndPagingShowMinimizeButton.style.display='';
	var searchAndPagingShowUnminimizeButton = document.all["searchAndPagingShowUnminimizeButton"];
	searchAndPagingShowUnminimizeButton.style.display='none';
	var searchAndPagingShowEdit = document.getElementById("searchAndPagingShowEdit");
	searchAndPagingShowEdit.style.display = 'block';
	var searchAndPagingShowHelp = document.getElementById("searchAndPagingShowHelp");
	searchAndPagingShowHelp.style.display = 'none';
}

/* show div element 'help' */
function searchAndPagingShowHelpButtonClick(){
	if(searchAndPagingShowHelpIsOpen != true){
		searchAndPagingShowViewIsOpen = false;
		searchAndPagingShowEditIsOpen = false;
		searchAndPagingShowHelpIsOpen = true;
	}
	var searchAndPagingShowView = document.getElementById("searchAndPagingShowView");
	searchAndPagingShowView.style.display = 'none';
	var searchAndPagingShowMinimizeButton = document.all["searchAndPagingShowMinimizeButton"];
	searchAndPagingShowMinimizeButton.style.display='';
	var searchAndPagingShowUnminimizeButton = document.all["searchAndPagingShowUnminimizeButton"];
	searchAndPagingShowUnminimizeButton.style.display='none';
	var searchAndPagingShowEdit = document.getElementById("searchAndPagingShowEdit");
	searchAndPagingShowEdit.style.display = 'none';
	var searchAndPagingShowHelp = document.getElementById("searchAndPagingShowHelp");
	searchAndPagingShowHelp.style.display = 'block';
}


