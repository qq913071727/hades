/* these three variables indicate which div element is being operated */
var videoStatisticalInformationViewIsOpen = true;
var videoStatisticalInformationEditIsOpen = false;
var videoStatisticalInformationHelpIsOpen = false;


/* hide div element */
function videoStatisticalInformationMinimizeButtonClick(){
	
	var videoStatisticalInformationView = document.getElementById("videoStatisticalInformationView");
	var videoStatisticalInformationEdit = document.getElementById("videoStatisticalInformationEdit");
	var videoStatisticalInformationHelp = document.getElementById("videoStatisticalInformationHelp");
	
	if(videoStatisticalInformationViewIsOpen == true && videoStatisticalInformationEditIsOpen == false && videoStatisticalInformationHelpIsOpen == false){
		//alert("222222222222222222");
		videoStatisticalInformationView.style.display = 'none';
		//alert("333333333333333333");
	}else
	if(videoStatisticalInformationViewIsOpen == false && videoStatisticalInformationEditIsOpen == true && videoStatisticalInformationHelpIsOpen == false){		
		videoStatisticalInformationEdit.style.display = 'none';
	}else
	if(videoStatisticalInformationViewIsOpen == false && videoStatisticalInformationEditIsOpen == false && videoStatisticalInformationHelpIsOpen == true){		
		videoStatisticalInformationHelp.style.display = 'none';
	}
	var videoStatisticalInformationMinimizeButton = document.all["videoStatisticalInformationMinimizeButton"];
	videoStatisticalInformationMinimizeButton.style.display='none';
	//alert("111111111111111");
	var videoStatisticalInformationUnminimizeButton = document.all["videoStatisticalInformationUnminimizeButton"];
	videoStatisticalInformationUnminimizeButton.style.display='block';
	//alert("111111111111111");
}

/* show div element */
function videoStatisticalInformationUnminimizeButtonClick(){
	var videoStatisticalInformationView = document.getElementById("videoStatisticalInformationView");
	var videoStatisticalInformationEdit = document.getElementById("videoStatisticalInformationEdit");
	var videoStatisticalInformationHelp = document.getElementById("videoStatisticalInformationHelp");
	
	if(videoStatisticalInformationViewIsOpen == true && videoStatisticalInformationEditIsOpen == false && videoStatisticalInformationHelpIsOpen == false){		
		videoStatisticalInformationView.style.display = '';
	}else
	if(videoStatisticalInformationViewIsOpen == false && videoStatisticalInformationEditIsOpen == true && videoStatisticalInformationHelpIsOpen == false){		
		videoStatisticalInformationEdit.style.display = 'block';
	}else
	if(videoStatisticalInformationViewIsOpen == false && videoStatisticalInformationEditIsOpen == false && videoStatisticalInformationHelpIsOpen == true) {		
		videoStatisticalInformationHelp.style.display = 'block';
	}
	
	var videoStatisticalInformationMinimizeButton = document.all["videoStatisticalInformationMinimizeButton"];
	videoStatisticalInformationMinimizeButton.style.display='';
	var videoStatisticalInformationUnminimizeButton = document.all["videoStatisticalInformationUnminimizeButton"];
	videoStatisticalInformationUnminimizeButton.style.display='none';
}

/* show div element 'view' */
function videoStatisticalInformationViewButtonClick(){
	if(videoStatisticalInformationViewIsOpen != true){
		videoStatisticalInformationViewIsOpen = true;
		videoStatisticalInformationEditIsOpen = false;
		videoStatisticalInformationHelpIsOpen = false;
	}
	var videoStatisticalInformationView = document.getElementById("videoStatisticalInformationView");
	videoStatisticalInformationView.style.display = 'block';
	var videoStatisticalInformationMinimizeButton = document.all["videoStatisticalInformationMinimizeButton"];
	videoStatisticalInformationMinimizeButton.style.display='';
	var videoStatisticalInformationUnminimizeButton = document.all["videoStatisticalInformationUnminimizeButton"];
	videoStatisticalInformationUnminimizeButton.style.display='none';
	var videoStatisticalInformationEdit = document.getElementById("videoStatisticalInformationEdit");
	videoStatisticalInformationEdit.style.display = 'none';
	var videoStatisticalInformationHelp = document.getElementById("videoStatisticalInformationHelp");
	videoStatisticalInformationHelp.style.display = 'none';
}

/* show div element 'edit' */
function videoStatisticalInformationEditButtonClick(){
	if(videoStatisticalInformationEditIsOpen != true){
		videoStatisticalInformationViewIsOpen = false;
		videoStatisticalInformationEditIsOpen = true;
		videoStatisticalInformationHelpIsOpen = false;
	}
	var videoStatisticalInformationView = document.getElementById("videoStatisticalInformationView");
	videoStatisticalInformationView.style.display = 'none';
	var videoStatisticalInformationMinimizeButton = document.all["videoStatisticalInformationMinimizeButton"];
	videoStatisticalInformationMinimizeButton.style.display='';
	var videoStatisticalInformationUnminimizeButton = document.all["videoStatisticalInformationUnminimizeButton"];
	videoStatisticalInformationUnminimizeButton.style.display='none';
	var videoStatisticalInformationEdit = document.getElementById("videoStatisticalInformationEdit");
	videoStatisticalInformationEdit.style.display = 'block';
	var videoStatisticalInformationHelp = document.getElementById("videoStatisticalInformationHelp");
	videoStatisticalInformationHelp.style.display = 'none';
}

/* show div element 'help' */
function videoStatisticalInformationHelpButtonnClick(){
	if(videoStatisticalInformationHelpIsOpen != true){
		videoStatisticalInformationViewIsOpen = false;
		videoStatisticalInformationEditIsOpen = false;
		videoStatisticalInformationHelpIsOpen = true;
	}
	var videoStatisticalInformationView = document.getElementById("videoStatisticalInformationView");
	videoStatisticalInformationView.style.display = 'none';
	var videoStatisticalInformationMinimizeButton = document.all["videoStatisticalInformationMinimizeButton"];
	videoStatisticalInformationMinimizeButton.style.display='';
	var videoStatisticalInformationUnminimizeButton = document.all["videoStatisticalInformationUnminimizeButton"];
	videoStatisticalInformationUnminimizeButton.style.display='none';
	var videoStatisticalInformationEdit = document.getElementById("videoStatisticalInformationEdit");
	videoStatisticalInformationEdit.style.display = 'none';
	var videoStatisticalInformationHelp = document.getElementById("videoStatisticalInformationHelp");
	videoStatisticalInformationHelp.style.display = 'block';
}

