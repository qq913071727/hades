/**
 * validate the form of file 'login.jsp' 
 * @return 'true' indicates validation passes;'false' indicates validation does not pass
 */
function loginValidate() {
	/*
	 * get fields from tag 'html:form' whose name is 'loginForm' 
	 */
	var name = document.forms["loginForm"].userName.value;
	var pwd = document.forms["loginForm"].password.value;
	var dtmpwd = document.forms["loginForm"].determinePassword.value;
	var authentication = document.forms["loginForm"].authentication.value;
	
	/*
	 * get hidden information about each field in tag 'html:form'
	 */
	var hiddenName = document.forms["loginForm"].elements["login.userNameCanNotBeEmpty"].value;
	var hiddenPwd = document.forms["loginForm"].elements["login.passwordCanNotBeEmpty"].value;
	var hiddenDeterminePassword = document.forms["loginForm"].elements["login.determinePasswordCanNotBeEmpty"].value;
	var hiddenPasswordIsNotEqualWithDeterminepassword = document.forms["loginForm"].elements["login.passwordIsNotEqualWithDeterminepassword"].value;
	var hiddenAuthentication = document.forms["loginForm"].elements["login.authenticationCanNotBeEmpty"].value;
	
	if (name == "") {
		alert(hiddenName);
		document.forms["loginForm"].userName.focus();
		return false;
	}
	if (pwd == "") {
		alert(hiddenPwd);
		document.forms["loginForm"].password.focus();
		return false;
	}
	if (dtmpwd == "") {
		alert(hiddenDeterminePassword);
		document.forms["loginForm"].determinePassword.focus();
		return false;
	}
	if (pwd != dtmpwd){
		alert(hiddenPasswordIsNotEqualWithDeterminepassword);
		document.forms["loginForm"].password.value="";
		document.forms["loginForm"].determinePassword.value="";
		document.forms["loginForm"].password.focus();
		return false;
	}
	if (authentication == "") {
		alert(hiddenAuthentication);
		document.forms["loginForm"].authentication.focus();
		return false;
	}
	return true;
}



