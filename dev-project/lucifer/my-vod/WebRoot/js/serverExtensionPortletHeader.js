/* these three variables indicate which div element is being operated */
var serverExtensionViewIsOpen = true;
var serverExtensionEditIsOpen = false;
var serverExtensionHelpIsOpen = false;


/* hide div element */
function serverExtensionMinimizeButtonClick(){
	var serverExtensionView = document.getElementById("serverExtensionView");
	var serverExtensionEdit = document.getElementById("serverExtensionEdit");
	var serverExtensionHelp = document.getElementById("serverExtensionHelp");
	
	if(serverExtensionViewIsOpen == true && serverExtensionEditIsOpen == false && serverExtensionHelpIsOpen == false){
		serverExtensionView.style.display = 'none';
	}else
	if(serverExtensionViewIsOpen == false && serverExtensionEditIsOpen == true && serverExtensionHelpIsOpen == false){		
		serverExtensionEdit.style.display = 'none';
	}else
	if(serverExtensionViewIsOpen == false && serverExtensionEditIsOpen == false && serverExtensionHelpIsOpen == true){		
		serverExtensionHelp.style.display = 'none';
	}
	var serverExtensionMinimizeButton = document.all["serverExtensionMinimizeButton"];
	serverExtensionMinimizeButton.style.display='none';
	var serverExtensionUnminimizeButton = document.all["serverExtensionUnminimizeButton"];
	serverExtensionUnminimizeButton.style.display='block';
}

/* show div element */
function serverExtensionUnminimizeButtonClick(){
	var serverExtensionView = document.getElementById("serverExtensionView");
	var serverExtensionEdit = document.getElementById("serverExtensionEdit");
	var serverExtensionHelp = document.getElementById("serverExtensionHelp");
	
	if(serverExtensionViewIsOpen == true && serverExtensionEditIsOpen == false && serverExtensionHelpIsOpen == false){		
		serverExtensionView.style.display = '';
	}else
	if(serverExtensionViewIsOpen == false && serverExtensionEditIsOpen == true && serverExtensionHelpIsOpen == false){		
		serverExtensionEdit.style.display = 'block';
	}else
	if(serverExtensionViewIsOpen == false && serverExtensionEditIsOpen == false && serverExtensionHelpIsOpen == true) {		
		serverExtensionHelp.style.display = 'block';
	}
	
	var serverExtensionMinimizeButton = document.all["serverExtensionMinimizeButton"];
	serverExtensionMinimizeButton.style.display='';
	var serverExtensionUnminimizeButton = document.all["serverExtensionUnminimizeButton"];
	serverExtensionUnminimizeButton.style.display='none';
}

/* show div element 'view' */
function serverExtensionViewButtonClick(){
	if(serverExtensionViewIsOpen != true){
		serverExtensionViewIsOpen = true;
		serverExtensionEditIsOpen = false;
		serverExtensionHelpIsOpen = false;
	}
	var serverExtensionView = document.getElementById("serverExtensionView");
	serverExtensionView.style.display = 'block';
	var serverExtensionMinimizeButton = document.all["serverExtensionMinimizeButton"];
	serverExtensionMinimizeButton.style.display='';
	var serverExtensionUnminimizeButton = document.all["serverExtensionUnminimizeButton"];
	serverExtensionUnminimizeButton.style.display='none';
	var serverExtensionEdit = document.getElementById("serverExtensionEdit");
	serverExtensionEdit.style.display = 'none';
	var serverExtensionHelp = document.getElementById("serverExtensionHelp");
	serverExtensionHelp.style.display = 'none';
}

/* show div element 'edit' */
function serverExtensionEditButtonClick(){
	if(serverExtensionEditIsOpen != true){
		serverExtensionViewIsOpen = false;
		serverExtensionEditIsOpen = true;
		serverExtensionHelpIsOpen = false;
	}
	var serverExtensionView = document.getElementById("serverExtensionView");
	serverExtensionView.style.display = 'none';
	var serverExtensionMinimizeButton = document.all["serverExtensionMinimizeButton"];
	serverExtensionMinimizeButton.style.display='';
	var serverExtensionUnminimizeButton = document.all["serverExtensionUnminimizeButton"];
	serverExtensionUnminimizeButton.style.display='none';
	var serverExtensionEdit = document.getElementById("serverExtensionEdit");
	serverExtensionEdit.style.display = 'block';
	var serverExtensionHelp = document.getElementById("serverExtensionHelp");
	serverExtensionHelp.style.display = 'none';
}

/* show div element 'help' */
function serverExtensionHelpButtonClick(){
	if(serverExtensionHelpIsOpen != true){
		serverExtensionViewIsOpen = false;
		serverExtensionEditIsOpen = false;
		serverExtensionHelpIsOpen = true;
	}
	var serverExtensionView = document.getElementById("serverExtensionView");
	serverExtensionView.style.display = 'none';
	var serverExtensionMinimizeButton = document.all["serverExtensionMinimizeButton"];
	serverExtensionMinimizeButton.style.display='';
	var serverExtensionUnminimizeButton = document.all["serverExtensionUnminimizeButton"];
	serverExtensionUnminimizeButton.style.display='none';
	var serverExtension = document.getElementById("serverExtensionEdit");
	serverExtensionEdit.style.display = 'none';
	var serverExtensionHelp = document.getElementById("serverExtensionHelp");
	serverExtensionHelp.style.display = 'block';
}


