/* these three variables indicate which div element is being operated */
var calendarViewIsOpen = true;
var calendarEditIsOpen = false;
var calendarHelpIsOpen = false;


/* hide div element */
function calendarMinimizeButtonClick(){
	
	var calendarView = document.getElementById("calendarView");
	var calendarEdit = document.getElementById("calendarEdit");
	var calendarHelp = document.getElementById("calendarHelp");
	
	if(calendarViewIsOpen == true && calendarEditIsOpen == false && calendarHelpIsOpen == false){
		//alert("222222222222222222");
		calendarView.style.display = 'none';
		//alert("333333333333333333");
	}else
	if(calendarViewIsOpen == false && calendarEditIsOpen == true && calendarHelpIsOpen == false){		
		calendarEdit.style.display = 'none';
	}else
	if(calendarViewIsOpen == false && calendarEditIsOpen == false && calendarHelpIsOpen == true){		
		calendarHelp.style.display = 'none';
	}
	var calendarMinimizeButton = document.all["calendarMinimizeButton"];
	calendarMinimizeButton.style.display='none';
	//alert("111111111111111");
	var calendarUnminimizeButton = document.all["calendarUnminimizeButton"];
	calendarUnminimizeButton.style.display='block';
	//alert("111111111111111");
}

/* show div element */
function calendarUnminimizeButtonClick(){
	var calendarView = document.getElementById("calendarView");
	var calendarEdit = document.getElementById("calendarEdit");
	var calendarHelp = document.getElementById("calendarHelp");
	
	if(calendarViewIsOpen == true && calendarEditIsOpen == false && calendarHelpIsOpen == false){		
		calendarView.style.display = '';
	}else
	if(calendarViewIsOpen == false && calendarEditIsOpen == true && calendarHelpIsOpen == false){		
		calendarEdit.style.display = 'block';
	}else
	if(calendarViewIsOpen == false && calendarEditIsOpen == false && calendarHelpIsOpen == true) {		
		calendarHelp.style.display = 'block';
	}
	
	var calendarMinimizeButton = document.all["calendarMinimizeButton"];
	calendarMinimizeButton.style.display='';
	var calendarUnminimizeButton = document.all["calendarUnminimizeButton"];
	calendarUnminimizeButton.style.display='none';
}

/* show div element 'view' */
function calendarViewButtonClick(){
	if(calendarViewIsOpen != true){
		calendarViewIsOpen = true;
		calendarEditIsOpen = false;
		calendarHelpIsOpen = false;
	}
	var calendarView = document.getElementById("calendarView");
	calendarView.style.display = 'block';
	var calendarMinimizeButton = document.all["calendarMinimizeButton"];
	calendarMinimizeButton.style.display='';
	var calendarUnminimizeButton = document.all["calendarUnminimizeButton"];
	calendarUnminimizeButton.style.display='none';
	var calendarEdit = document.getElementById("calendarEdit");
	calendarEdit.style.display = 'none';
	var calendarHelp = document.getElementById("calendarHelp");
	calendarHelp.style.display = 'none';
}

/* show div element 'edit' */
function calendarEditButtonClick(){
	if(calendarEditIsOpen != true){
		calendarViewIsOpen = false;
		calendarEditIsOpen = true;
		calendarHelpIsOpen = false;
	}
	var calendarView = document.getElementById("calendarView");
	calendarView.style.display = 'none';
	var calendarMinimizeButton = document.all["calendarMinimizeButton"];
	calendarMinimizeButton.style.display='';
	var calendarUnminimizeButton = document.all["calendarUnminimizeButton"];
	calendarUnminimizeButton.style.display='none';
	var calendarEdit = document.getElementById("calendarEdit");
	calendarEdit.style.display = 'block';
	var calendarHelp = document.getElementById("calendarHelp");
	calendarHelp.style.display = 'none';
}

/* show div element 'help' */
function calendarHelpButtonnClick(){
	if(calendarHelpIsOpen != true){
		calendarViewIsOpen = false;
		calendarEditIsOpen = false;
		calendarHelpIsOpen = true;
	}
	var calendarView = document.getElementById("calendarView");
	calendarView.style.display = 'none';
	var calendarMinimizeButton = document.all["calendarMinimizeButton"];
	calendarMinimizeButton.style.display='';
	var calendarUnminimizeButton = document.all["calendarUnminimizeButton"];
	calendarUnminimizeButton.style.display='none';
	var calendarEdit = document.getElementById("calendarEdit");
	calendarEdit.style.display = 'none';
	var calendarHelp = document.getElementById("calendarHelp");
	calendarHelp.style.display = 'block';
}


