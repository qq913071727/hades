package edu.ncut.vod.hibernate.model;

import java.io.Serializable;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class ExamUser implements Serializable,BeanPostProcessor {

    // Fields
    private static final long serialVersionUID = -7365244139383859141L;
    private Integer admId;
    private String admName;
    private String admPass;

    // Constructors
    /** default constructor */
    public ExamUser() {
    }

    /** full constructor */
    public ExamUser(String admName, String admPass) {
        this.admName = admName;
        this.admPass = admPass;
    }

    /**
     * overloading method 'toString'
     */
     public String toString(){
		return admId.toString().trim()+"#"+admName.trim()+"#"+admPass.trim();
	 }

    // Property accessors
    public Integer getAdmId() {
        return this.admId;
    }

    public void setAdmId(Integer admId) {
        this.admId = admId;
    }

    public String getAdmName() {
        return this.admName;
    }

    public void setAdmName(String admName) {
        this.admName = admName;
    }

    public String getAdmPass() {
        return this.admPass;
    }

    public void setAdmPass(String admPass) {
        this.admPass = admPass;
    }

    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("method postProcessAfterInitialization in class ExamUser is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("method postProcessBeforeInitialization in class ExamUser is called");
        return null;
    }

    /**
     * print message on console
     * @param message
     */
    public void debug(String message){
        System.out.println(message);
    }

}