package edu.ncut.vod.hibernate.model;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class VideoInfo implements java.io.Serializable,BeanPostProcessor {

    //Fields
    private static final long serialVersionUID = 4435201681782880594L;
    private Integer videoid;
    private String informationname;
    private String informationdescription;
    private String secondlevelcolumnid;
    private String firstlevelcolumnid;
    private String filesize;
    private Integer browsenumber;

    // Constructors
    /** default constructor */
    public VideoInfo() {
    }

    /** full constructor */
    public VideoInfo(Integer videoid,String informationname, String informationdescription,
            String secondlevelcolumnid, String firstlevelcolumnid,
            String filesize, Integer browsenumber) {
        this.videoid = videoid;
        this.informationname = informationname;
        this.informationdescription = informationdescription;
        this.secondlevelcolumnid = secondlevelcolumnid;
        this.firstlevelcolumnid = firstlevelcolumnid;
        this.filesize = filesize;
        this.browsenumber = browsenumber;
    }

    /**
     * overloading method 'toString'
     */
    public String toString(){
		return videoid.toString().trim()+"#"+informationname.trim()+"#"+informationdescription.trim()+
				"#"+secondlevelcolumnid.trim()+"#"+firstlevelcolumnid.trim()+"#"+filesize.trim()+"#"+
				browsenumber.toString().trim();
	}

    // Property accessors
    public Integer getVideoid() {
        return this.videoid;
    }

    public void setVideoid(Integer videoid) {
        this.videoid = videoid;
    }

    public String getInformationname() {
        return this.informationname;
    }

    public void setInformationname(String informationname) {
        this.informationname = informationname;
    }

    public String getInformationdescription() {
        return this.informationdescription;
    }

    public void setInformationdescription(String informationdescription) {
        this.informationdescription = informationdescription;
    }

    public String getSecondlevelcolumnid() {
        return this.secondlevelcolumnid;
    }

    public void setSecondlevelcolumnid(String secondlevelcolumnid) {
        this.secondlevelcolumnid = secondlevelcolumnid;
    }

    public String getFirstlevelcolumnid() {
        return this.firstlevelcolumnid;
    }

    public void setFirstlevelcolumnid(String firstlevelcolumnid) {
        this.firstlevelcolumnid = firstlevelcolumnid;
    }

    public String getFilesize() {
        return this.filesize;
    }

    public void setFilesize(String filesize) {
        this.filesize = filesize;
    }

    public Integer getBrowsenumber() {
        return this.browsenumber;
    }

    public void setBrowsenumber(Integer browsenumber) {
        this.browsenumber = browsenumber;
    }

    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("method postProcessAfterInitialization in class Videoinfo is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("method postProcessBeforeInitialization in class Videoinfo is called");
        return null;
    }

    /**
     * print message on console
     * @param message
     */
    public void debug(String message){
        System.out.println(message);
    }
}