package edu.ncut.vod.hibernate.service;

import javax.servlet.http.HttpServletRequest;

public interface ServerInfoService {
    
    /**
     * output a sentence in console for debugging
     * @param message
     */
    public void debug(String message);
    
    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup();
    
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy();

    /**
     * reduce one at column 'curShowNum' in table 'serverinfo'
     * @param demandedVideoId
     */
    public void reduceCurShowNumByVideoURL(Integer demandedVideoId);
    
    /**
     * set zero to column 'isbroadcast' at table 'videoinfo'
     * @param demandedVideoId
     */
    public void setZeroToColumnIsBroadcast(Integer demandedVideoId);
    
    /**
     * set current time to column 'endtime' at table 'demandedvideoinfo'
     * @param demandedVideoId
     */
    public void setEndTime(Integer demandedVideoId);
    
    /**
     * set state of server
     * @param videoURL
     * @param videoId
     */
    public void setState(Integer demandedVideoId);
    
    /**
     * try to find ip of server whose curShowNum is minimum
     * @return
     */
    public Integer findCurShowNumMinServer();
    
    /**
     * increase one at row curShowNum
     * @param serverId
     */
    public void increaseCurShowNum(Integer serverId);
    
    /**
     * distribute ServerIp To VideoInformation
     * @param serverId
     * @param videoId
     */
    public void distributeServerIpToVideoInformation(Integer serverId,String videoId,HttpServletRequest request);
    
    /**
     * first find a row whose column is minimum, and then increase one at column curShowNum
     * @param videoId
     */
    public Integer updateState(String videoId,HttpServletRequest request);
}
