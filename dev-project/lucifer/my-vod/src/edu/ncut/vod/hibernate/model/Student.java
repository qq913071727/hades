package edu.ncut.vod.hibernate.model;

import java.io.Serializable;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class Student implements Serializable,BeanPostProcessor {

    // Fields
    private static final long serialVersionUID = 1032802196928552176L;
    private Integer stuId;
    private String stuNumber;
    private String stuName;
    private String className;
    private String stuMail;
    private String humanId;
    private String address;
    private String stuPhone;

    // Constructors
    /** default constructor */
    public Student() {
    }

    /** minimal constructor */
    public Student(String stuNumber, String stuName, String className,
            String humanId, String address, String stuPhone) {
        this.stuNumber = stuNumber;
        this.stuName = stuName;
        this.className = className;
        this.humanId = humanId;
        this.address = address;
        this.stuPhone = stuPhone;
    }

    /** full constructor */
    public Student(String stuNumber, String stuName, String className,
            String stuMail, String humanId, String address, String stuPhone) {
        this.stuNumber = stuNumber;
        this.stuName = stuName;
        this.className = className;
        this.stuMail = stuMail;
        this.humanId = humanId;
        this.address = address;
        this.stuPhone = stuPhone;
    }

    /**
     * overloading method 'toString'
     */
    public String toString(){
		return stuId.toString().trim()+"#"+stuNumber.trim()+"#"+stuName.trim()+"#"+className.trim()+
				"#"+stuMail.trim()+"#"+humanId.trim()+"#"+address.trim()+"#"+stuPhone.trim();
	}

    // Property accessors
    public Integer getStuId() {
        return this.stuId;
    }

    public void setStuId(Integer stuId) {
        this.stuId = stuId;
    }

    public String getStuNumber() {
        return this.stuNumber;
    }

    public void setStuNumber(String stuNumber) {
        this.stuNumber = stuNumber;
    }

    public String getStuName() {
        return this.stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getClassName() {
        return this.className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getStuMail() {
        return this.stuMail;
    }

    public void setStuMail(String stuMail) {
        this.stuMail = stuMail;
    }

    public String getHumanId() {
        return this.humanId;
    }

    public void setHumanId(String humanId) {
        this.humanId = humanId;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStuPhone() {
        return this.stuPhone;
    }

    public void setStuPhone(String stuPhone) {
        this.stuPhone = stuPhone;
    }

    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("method postProcessAfterInitialization in class Student is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("method postProcessBeforeInitialization in class Student is called");
        return null;
    }

    /**
     * print message on console
     * @param message
     */
    public void debug(String message){
        System.out.println(message);
    }

}