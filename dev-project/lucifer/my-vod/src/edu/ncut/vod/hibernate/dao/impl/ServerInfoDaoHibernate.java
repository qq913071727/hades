package edu.ncut.vod.hibernate.dao.impl;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import edu.ncut.vod.hibernate.dao.ServerInfoDao;
import edu.ncut.vod.hibernate.model.Serverinfo;
import edu.ncut.vod.hibernate.model.Visitor;

public class ServerInfoDaoHibernate extends HibernateDaoSupport
        implements ServerInfoDao, BeanPostProcessor {

    /**
     * add a row information about server
     * @param ip
     * @param role
     */
    public void addServerinfo(Serverinfo serverInfo) {
        debug(serverInfo.toString());
        getHibernateTemplate().saveOrUpdate(serverInfo);
    }

    /**
     * delete a row information about server
     * @param ip
     * @param role
     */
    public void deleteServerinfo(Integer id) {
        getHibernateTemplate().delete(getServerinfo(id));
    }
    
    /**
     * save or update object 'Serverinfo'
     * @param serverInfo
     */
    public void save(Serverinfo serverInfo) {
        getHibernateTemplate().saveOrUpdate(serverInfo);
    }
    
    /**
     * get a row information according to id
     * @param id
     * @return
     */
    public Serverinfo getServerinfo(Integer id) {
        return (Serverinfo) getHibernateTemplate().get(edu.ncut.vod.hibernate.model.Serverinfo.class, id);
    }
    
    /**
     * get a row information according to videoName
     * @param videoName
     * @return
     */
    public List<Serverinfo> getServerinfoByVideoURL(String videoURL) {
        return getHibernateTemplate().find("from Serverinfo where ip=?",videoURL);
    }
    
    /**
     * get a row information according to ip
     * @param ip
     * @return
     */
    public List<Serverinfo> getServerinfo(String ip) {
        return getHibernateTemplate().find("from Serverinfo where ip=?",ip); 
    }
    
    /**
     * get rows about table 'serverinfo'
     * @return
     */
    public List getServerinfos() {
        return getHibernateTemplate().find("from Serverinfo order by id"); 
    }

    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy() {
        debug("destroy mothed in ServerInfoDaoHibernate object is called");
    }

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup() {
        debug("setup mothed in ServerInfoDaoHibernate object is called");
    }
    
    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in ServerInfoDaoHibernate object is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in ServerInfoDaoHibernate object is called");
        return null;
    }
    
    /**
     * print message on console
     * @param message
     */
    public void debug(String message){
        System.out.println(message);
    }

}
