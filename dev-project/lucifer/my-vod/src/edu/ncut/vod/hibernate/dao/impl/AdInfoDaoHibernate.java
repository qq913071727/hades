package edu.ncut.vod.hibernate.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import edu.ncut.vod.hibernate.dao.AdInfoDao;
import edu.ncut.vod.hibernate.model.AdInfo;

public class AdInfoDaoHibernate extends HibernateDaoSupport implements AdInfoDao,BeanPostProcessor {

    /* (non-Javadoc)
     * @see edu.ncut.vod.hibernate.dao.AdinfoDao#delete(java.lang.Integer)
     */
    public void delete(Integer id) {
        getHibernateTemplate().delete(getAdInfo(id));
    }

    /* (non-Javadoc)
     * @see edu.ncut.vod.hibernate.dao.AdinfoDao#getAdinfo(org.hibernate.Session, java.lang.Integer)
     */
    public AdInfo getAdInfo(Integer id) {
        return (AdInfo) getHibernateTemplate().get(edu.ncut.vod.hibernate.model.AdInfo.class, id);
    }

    /* (non-Javadoc)
     * @see edu.ncut.vod.hibernate.dao.AdinfoDao#getAdinfos(org.hibernate.Session)
     */
    public List getAdInfos() {
        return getHibernateTemplate().find("from AdInfo order by adId desc");
    }

    /* (non-Javadoc)
     * @see edu.ncut.vod.hibernate.dao.AdinfoDao#save(edu.ncut.vod.hibernate.model.Adinfo)
     */
    public void save(AdInfo ad) {
        getHibernateTemplate().saveOrUpdate(ad);
    }

    /* (non-Javadoc)
     * @see edu.ncut.vod.hibernate.dao.AdinfoDao#searchAmountOfResult(org.hibernate.Session)
     */
    public int searchAmountOfResult(Session session) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy() {
        debug("destroy mothed in AdInfoDaoHibernate object is called");
    }

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup() {
        debug("setup mothed in AdInfoDaoHibernate object is called");
    }
    
    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in AdInfoDaoHibernate object is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in AdInfoDaoHibernate object is called");
        return null;
    }
    
    /*
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }

}
