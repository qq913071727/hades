package edu.ncut.vod.hibernate.service.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import edu.ncut.vod.hibernate.dao.ServerInfoDao;
import edu.ncut.vod.hibernate.model.Serverinfo;
import edu.ncut.vod.hibernate.service.ServerSelectService;

public class ServerSelectServiceImpl implements ServerSelectService,BeanPostProcessor  {

    /*
     * dao layer object named 'serverInfoDao'
     */
    ServerInfoDao serverInfoDao;

    public ServerInfoDao getServerInfoDao() {
        return serverInfoDao;
    }

    public void setServerInfoDao(ServerInfoDao serverInfoDao) {
        this.serverInfoDao = serverInfoDao;
    }

    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy() {
        debug("destroy mothed in ServerSelectServiceImpl object is called");
    }

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup() {
        debug("setup mothed in ServerSelectServiceImpl object is called");
    }
    
    /**
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in ServerSelectServiceImpl object is called");
        return null;
    }

    /**
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in ServerSelectServiceImpl object is called");
        return null;
    }
    
    /**
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }

    /**
     * add subsidiary server information to database
     */
    public String addSubsidiaryServer(String inputIp,String localServerIp,Integer role,Integer num) {
        if(findServerInfoByIp(inputIp) && !findServerInfoByIp(localServerIp)){
            Serverinfo serverinfo = new Serverinfo(localServerIp,role,num);
            serverInfoDao.addServerinfo(serverinfo);
            return "success";//adding condition is valid
        }else if(!findServerInfoByIp(inputIp)){            
            return "inputIpErr";//inputIp err.main server does not exist
        }else if(findServerInfoByIp(localServerIp)){
            return "alreadyRegister";//local server has been registered
        }else{
            return "fail";//other failure situation
        }
    }

    /**
     * add main server information to database
     */
    public String addMainServer(String localServerIp,Integer role,Integer num) {  
        if(findServerInfoByIp(localServerIp)){
            return "alreadyRegister";//in database there is the row whose ip is the same as localServerIp
        }else{
            Serverinfo serverinfo = new Serverinfo(localServerIp,role,num);
            serverInfoDao.addServerinfo(serverinfo);
            return "success";//in database there is not the row whose ip is the same as localServerIp
        }
    }
    
    /**
     * according to inputIp,try to find row in database
     * @param inputIp
     * @return
     */
    public boolean findServerInfoByIp(String ip){
        List<Serverinfo> list = serverInfoDao.getServerinfo(ip);
        for(Serverinfo serverInfo : list){
            if(null == serverInfo){
                return false;//there is not row in database according to ip
            }else{
                return true;//there is a row in database according to ip 
            }
        }        
        return false;
    }

}
