package edu.ncut.vod.hibernate.dao;

import java.util.List;
import java.util.HashMap;

import edu.ncut.vod.hibernate.model.TableList;

public interface TableListDao {
	
	/**
	 * get all table information from table 'tablelist'
	 * @return
	 */
	public List<TableList> getAllTables();
	
	/**
	 * according to parameter 'tableName',get content of table
	 * @param tableName
	 * @return
	 */
	public List getTableContent(String tableName);
	
	/**
	 * get the Class name of table
	 * @param tableName
	 * @return
	 */
	public String getTableClassName(String tableName);
}
