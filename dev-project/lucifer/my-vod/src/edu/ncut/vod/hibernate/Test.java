package edu.ncut.vod.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.ncut.vod.exception.ExamException;
import edu.ncut.vod.hibernate.Test;
import edu.ncut.vod.hibernate.dao.ExamTypeDao;
import edu.ncut.vod.hibernate.dao.VisitorDao;
import edu.ncut.vod.hibernate.dao.impl.ExamTypeDaoHibernate;
import edu.ncut.vod.hibernate.model.ExamType;
import edu.ncut.vod.hibernate.model.Visitor;
import edu.ncut.vod.hibernate.service.ExamService;

public class Test {

    public static void main(String[] args) {
        
    }
    
    public void testSpring(){
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        /*VisitorDao dao = (VisitorDao)ctx.getBean("visitorDao");
        debug(dao.getVisitors().toString());
        ExamTypeDao dao = (ExamTypeDao)ctx.getBean("examTypeDao");
        debug(dao.findAll().toString());*/
        ExamService service = (ExamService) ctx.getBean("examService");
        try {
            debug(service.getAllExamType().toString());
        } catch (ExamException e) {
            e.printStackTrace();
        }
        
        /*ExamType et = new ExamType("abc","123");
        debug(et.getTypeId().toString());
        debug(et.getTestTitle().toString());*/
    }
    
    public void testHibernate(){
        Session session = HibernateSessionFactory.getSession();
        Transaction tx = session.beginTransaction();        
        Test tu = new Test();
        tu.add(session,tx);       
        session.close();
    }

    // ��ѯ��ݿ��
    public void search(Session session) {
        //debug("nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
        Query q = session.createQuery("select v from Visitor v");
        //debug("nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
        List list = q.list();
        for (Object o : list) {
            System.out.println("id:" + ((Visitor) o).getVisitorid() + ",Visitorname:"
                    + ((Visitor) o).getVisitorname() + ",Password:"
                    + ((Visitor) o).getPassword());
        }
    }

    // ����ݿ������Ӽ�¼
    public void add(Session session,Transaction tx) {
        Visitor v = new Visitor();
        
        //v.setUserid(10);
        v.setVisitorname("zhangsan");
        v.setPassword("456");
        System.out.println(v.getVisitorname() + v.getPassword());
        tx.begin();
        session.save(v);
        System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        tx.commit();
    }

    // ��ļ�¼
    public void update(Session session,Transaction tx) {
        Visitor v2 = (Visitor) session.get(Visitor.class, new Integer(10));
        v2.setVisitorname("lisi");
        tx.begin();
        session.saveOrUpdate(v2);
        tx.commit();
    }

    // ɾ���¼
    public void delete(Session session,Transaction tx) {
        Visitor v3 = (Visitor) session.get(Visitor.class, new Integer(10));
        tx.begin();
        session.delete(v3);
        tx.commit();
    }

    // ��������4�ڿ���̨����,�Ա�ȷ�������д����������
    public static void debug(String message) {
        System.out.println(message);
    }
}
