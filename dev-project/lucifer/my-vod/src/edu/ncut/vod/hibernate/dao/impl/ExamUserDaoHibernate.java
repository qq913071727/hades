package edu.ncut.vod.hibernate.dao.impl;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import edu.ncut.vod.hibernate.dao.ExamUserDao;
import edu.ncut.vod.hibernate.enhance.HibernateDaoSupportEnhance;
import edu.ncut.vod.hibernate.model.ExamUser;

public class ExamUserDaoHibernate extends HibernateDaoSupportEnhance implements
        ExamUserDao,BeanPostProcessor {

    /* 
     * 删除考试管理员实体，删除一条考试管理员记录
     * @param examUser 删除的考试管理员实例
     * @see edu.ncut.vod.hibernate.dao.ExamUserDao#delete(edu.ncut.vod.hibernate.model.ExamUser)
     */
    public void delete(ExamUser examUser) {
        getHibernateTemplate().delete(examUser);
    }

    /* 
     * 根据主键删除考试管理员实体，删除一条考试管理员记录
     * @param id 删除考试管理员的主键
     * @see edu.ncut.vod.hibernate.dao.ExamUserDao#delete(int)
     */
    public void delete(int id) {
        getHibernateTemplate().delete(get(id));
    }

    /* 
     * 根据用户名和密码查找考试管理员
     * @param name 用户名
     * @param pass 密码
     * @return 根据用户名和密码查找到的考试管理员列表
     * @see edu.ncut.vod.hibernate.dao.ExamUserDao#findExamUserByNameAndPass(java.lang.String, java.lang.String)
     */
    public List<ExamUser> findExamUserByNameAndPass(String name, String pass) {
        String[] args = {name , pass};
        return getHibernateTemplate().find("from ExamUser eu where eu.admName=? and eu.admPass=? " , args);
    }

    /* 
     * 根据主键加载考试管理员
     * @param id 需要加载的考试管理员的主键值
     * @return 加载的考试管理员PO
     * @see edu.ncut.vod.hibernate.dao.ExamUserDao#get(int)
     */
    public ExamUser get(int id) {
        return (ExamUser)getHibernateTemplate().load(ExamUser.class , new Integer(id));
    }

    /* 
     * 保存考试管理员实体，新增一条考试管理员记录
     * @param examUser 保存的考试管理员实例
     * @see edu.ncut.vod.hibernate.dao.ExamUserDao#save(edu.ncut.vod.hibernate.model.ExamUser)
     */
    public void save(ExamUser examUser) {
        getHibernateTemplate().save(examUser);
    }

    /* 
     * 更新一条考试管理员记录
     * @param examUser 需要更新的考试管理员
     * @see edu.ncut.vod.hibernate.dao.ExamUserDao#update(edu.ncut.vod.hibernate.model.ExamUser)
     */
    public void update(ExamUser examUser) {
        getHibernateTemplate().update(examUser);
    }
    
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy() {
        debug("destroy mothed in ExamUserDaoHibernate object is called");
    }

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup() {
        debug("setup mothed in ExamUserDaoHibernate object is called");
    }
    
    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in ExamUserDaoHibernate object is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in ExamUserDaoHibernate object is called");
        return null;
    }
    
    /*
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }

}
