package edu.ncut.vod.hibernate.dao;

import java.util.List;

import org.hibernate.Session;

import edu.ncut.vod.hibernate.model.AdInfo;

public interface AdInfoDao {
    public void save(AdInfo ad);

    public void delete(Integer id);
    
    public AdInfo getAdInfo(Integer id);

    public List getAdInfos();
    
    public int searchAmountOfResult(Session session);
}
