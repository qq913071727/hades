package edu.ncut.vod.hibernate.model;

public class SecondLevelColumnInfo implements java.io.Serializable {

    // Fields
    private static final long serialVersionUID = 3046172494262140261L;
    private Integer nextclassid;
    private String columnname;
    private Integer firstlevelcolumnid;

    // Constructors
    /** default constructor */
    public SecondLevelColumnInfo() {
    }

    /** full constructor */
    public SecondLevelColumnInfo(String columnname, Integer firstlevelcolumnid) {
        this.columnname = columnname;
        this.firstlevelcolumnid = firstlevelcolumnid;
    }

    /**
     * overloading method 'toString'
     */
     public String toString(){
	 	return nextclassid.toString().trim()+"#"+columnname.trim()+"#"+firstlevelcolumnid.toString().trim();
	 }

    // Property accessors
    public Integer getNextclassid() {
        return this.nextclassid;
    }

    public void setNextclassid(Integer nextclassid) {
        this.nextclassid = nextclassid;
    }

    public String getColumnname() {
        return this.columnname;
    }

    public void setColumnname(String columnname) {
        this.columnname = columnname;
    }

    public Integer getFirstlevelcolumnid() {
        return this.firstlevelcolumnid;
    }

    public void setFirstlevelcolumnid(Integer firstlevelcolumnid) {
        this.firstlevelcolumnid = firstlevelcolumnid;
    }

}