package edu.ncut.vod.hibernate.dao;

import java.util.List;

import org.hibernate.Session;

import edu.ncut.vod.hibernate.model.AdClickInfo;

public interface AdClickInfoDao {
    public void save(AdClickInfo ad);

    public void delete(Integer id);
    
    public AdClickInfo getAdClickInfo(Integer id);

    public List getAdClickInfos();
    
    public int searchAmountOfResult(Session session);
    
    /**
     * get ADid and count by group from table 'adclickinfo'
     * @return
     */
    public List<AdClickInfo> getAdinfoAndCount();
    
    /**
     * according to adId,get ADid and count by group from table 'adclickinfo'
     * @param adId
     * @return
     */
    public List<AdClickInfo> getAdinfoAndCount(Integer adId);
    
    /**
     * get ADid from table 'adclickinfo'
     * @return
     */
    public List<AdClickInfo> getADid();
    
    /**
     * get clientIp from table 'adclickinfo'
     * @return
     */
    public List<AdClickInfo> getClientIp();
}
