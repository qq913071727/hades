package edu.ncut.vod.hibernate.model;

import java.io.Serializable;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class Question implements Serializable,BeanPostProcessor {

    // Fields
    private static final long serialVersionUID = -4565304309387143026L;
    private Integer quId;
    private Integer typeId;
    private String quTitle;
    private String quHard;
    private String quType;
    private Integer quScore;
    private String quAnswer;
    private String selectOption;

    // Constructors
    /** default constructor */
    public Question() {
    }

    /** full constructor */
    public Question(Integer typeId, String quTitle, String quHard,
            String quType, Integer quScore, String quAnswer, String selectOption) {
        this.typeId = typeId;
        this.quTitle = quTitle;
        this.quHard = quHard;
        this.quType = quType;
        this.quScore = quScore;
        this.quAnswer = quAnswer;
        this.selectOption = selectOption;
    }

    /**
     * overloading method 'toString'
     */
    public String toString(){
		return quId.toString().trim()+"#"+typeId.toString().trim()+"#"+quTitle.trim()+"#"+quHard.trim()+"#"+
				quType.trim()+"#"+quScore.toString().trim()+"#"+quAnswer.trim()+"#"+selectOption.trim();
	}

    // Property accessors
    public Integer getQuId() {
        return this.quId;
    }

    public void setQuId(Integer quId) {
        this.quId = quId;
    }

    public Integer getTypeId() {
        return this.typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getQuTitle() {
        return this.quTitle;
    }

    public void setQuTitle(String quTitle) {
        this.quTitle = quTitle;
    }

    public String getQuHard() {
        return this.quHard;
    }

    public void setQuHard(String quHard) {
        this.quHard = quHard;
    }

    public String getQuType() {
        return this.quType;
    }

    public void setQuType(String quType) {
        this.quType = quType;
    }

    public Integer getQuScore() {
        return this.quScore;
    }

    public void setQuScore(Integer quScore) {
        this.quScore = quScore;
    }

    public String getQuAnswer() {
        return this.quAnswer;
    }

    public void setQuAnswer(String quAnswer) {
        this.quAnswer = quAnswer;
    }

    public String getSelectOption() {
        return selectOption;
    }

    public void setSelectOption(String selectOption) {
        this.selectOption = selectOption;
    }

    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("method postProcessAfterInitialization in class Question is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("method postProcessBeforeInitialization in class Question is called");
        return null;
    }

    /**
     * print message on console
     * @param message
     */
    public void debug(String message){
        System.out.println(message);
    }

}