package edu.ncut.vod.hibernate.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import edu.ncut.vod.hibernate.dao.DemandedVideoInfoDao;
import edu.ncut.vod.hibernate.model.DemandedVideoInfo;

public class DemandedVideoInfoDaoHibernate extends HibernateDaoSupport implements DemandedVideoInfoDao,BeanPostProcessor {

    public void delete(Integer id) {
        getHibernateTemplate().delete(getDemandedvideoinfo(id));
    }

    public DemandedVideoInfo getDemandedvideoinfo(Integer id) {
        List<DemandedVideoInfo> list = getHibernateTemplate().find("from DemandedVideoInfo where id=?", id);
        DemandedVideoInfo demandedvideoinfo = list.get(0);
        return demandedvideoinfo;
        //return (Demandedvideoinfo) getHibernateTemplate().get(edu.ncut.vod.hibernate.model.Demandedvideoinfo.class, id);
    }

    public List<?> getDemandedvideoinfos() {
        return getHibernateTemplate().find("from DemandedVideoInfo order by id");
    }

    public void save(DemandedVideoInfo dvi) {
        getHibernateTemplate().saveOrUpdate(dvi);
    }

    /**
     * get videoid and count by group from table 'demandedvideoinfo'
     * @return
     */
    public List<DemandedVideoInfo> getVideoIdAndCount(){
        List<DemandedVideoInfo> list =  getHibernateTemplate().find("select dvi.videoId,count(*) from DemandedVideoInfo as dvi group by dvi.videoId");
        return list;
    }
    
    /**
     * get an array about videoid in table 'demandedvideoinfo'
     * @return
     */
    public List getVideoId(){
        List list =  getHibernateTemplate().find("select distinct dvi.videoId from DemandedVideoInfo as dvi");
        return list;
    }

    /**
     * get an array about videoURL in table 'demandedvideoinfo'
     * @param videoId
     * @return
     */
    public List getVideoUrl(String videoId){
        List list =  getHibernateTemplate().find("select distinct dvi.videoUrl from DemandedVideoInfo as dvi where dvi.videoId=?",videoId);
        return list;
    }
    
    /**
     * get count at table 'demandedvideoInfo',according to videoid and videoURL
     * @param parameters
     * @return
     */
    public int getCountByVideoIdAndVideoUrl(String[] parameters){
        int count =  Integer.parseInt(getHibernateTemplate().find("select count(*) from DemandedVideoInfo as dvi where dvi.videoId=? and dvi.videoUrl=?",parameters).get(0).toString());
        return count;
    }
    
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy() {
        debug("destroy mothed in DemandedVideoInfoDaoHibernate object is called");
    }

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup() {
        debug("setup mothed in DemandedVideoInfoDaoHibernate object is called");
    }
    
    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in DemandedVideoInfoDaoHibernate object is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in DemandedVideoInfoDaoHibernate object is called");
        return null;
    }

    /**
     * print message on console
     * @param message
     */
    public void debug(String message){
        System.out.println(message);
    }
}
