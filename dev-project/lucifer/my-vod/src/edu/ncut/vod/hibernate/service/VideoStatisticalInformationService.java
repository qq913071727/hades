package edu.ncut.vod.hibernate.service;

public interface VideoStatisticalInformationService {
    
    /**
     * get chart about statistical information
     * @param selectedObject
     * @param chart
     */
    public void getStatisticalChart(String selectedObject,String chart,int randomInt);
    
    /**
     * create bar chart about demandedvideoinfo table
     * @param randomInt
     */
    public void createDemandedVideoInfoPie(int randomInt);
    
    /**
     * create pie chart about adclickinfo table
     * @param randomInt
     */
    public void createAdClickInfoPie(int randomInt);
    
    /**
     * create bar chart about demandedvideoinfo table
     * @param randomInt
     */
    public void createDemandedVideoInfoBar(int randomInt);
    
    /**
     * create bar chart about adclickinfo table
     * @param randomInt
     */
    public void createAdClickInfoBar(int randomInt);
    
}
