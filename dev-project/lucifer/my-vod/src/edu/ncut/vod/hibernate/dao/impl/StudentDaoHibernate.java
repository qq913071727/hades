package edu.ncut.vod.hibernate.dao.impl;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import edu.ncut.vod.hibernate.dao.StudentDao;
import edu.ncut.vod.hibernate.enhance.HibernateDaoSupportEnhance;
import edu.ncut.vod.hibernate.model.Student;

public class StudentDaoHibernate extends HibernateDaoSupportEnhance implements
        StudentDao,BeanPostProcessor {

    /*
     * 删除学生实体，删除一条学生记录
     * @param student 删除的学生实例
     * @see edu.ncut.vod.hibernate.dao.StudentDao#delete(edu.ncut.vod.hibernate.model.Student)
     */
    public void delete(Student student) {
        getHibernateTemplate().delete(student);
    }

    /*
     * 根据主键删除学生实体，删除一条学生记录
     * @param id 删除学生的主键
     * @see edu.ncut.vod.hibernate.dao.StudentDao#delete(int)
     */
    public void delete(int id) {
        getHibernateTemplate().delete(get(id));
    }

    /*
     * 根据页码查询学生列表
     * @param pageNo 查询的页码
     * @param pageSize 每页显示的学生数
     * @return 指定页的学生列表
     * @see edu.ncut.vod.hibernate.dao.StudentDao#findAllByPage(int, int)
     */
    public List<Student> findAllByPage(int pageNo, int pageSize) {
        if (pageNo < 1) {
            return null;
        }
        int offset = (pageNo - 1) * pageSize;
        return findByPage("from Student", offset, pageSize);
    }

    /*
     * 根据学号和姓名查询学生
     * @param name 需要更新的学生
     * @param stuNumber 学号
     * @return 符合名字和学号查询条件的学生列表
     * @see edu.ncut.vod.hibernate.dao.StudentDao#findStudentByNameAndStuNumber(java.lang.String, java.lang.String)
     */
    public List<Student> findStudentByNameAndStuNumber(String name,
            String stuNumber) {
        return getHibernateTemplate().find(
                        "from Student as student where student.stuNumber = ? and student.name=?",
                        new String[] { stuNumber, name });
    }

    /*
     * 根据主键加载学生
     * @param id 需要加载的学生的主键值
     * @return 加载的学生PO
     * @see edu.ncut.vod.hibernate.dao.StudentDao#get(int)
     */
    public Student get(int id) {
        return (Student) getHibernateTemplate().load(Student.class,
                new Integer(id));
    }

    /*
     * 查询学生的数量
     * @return 学生的数量
     * @see edu.ncut.vod.hibernate.dao.StudentDao#getStudentCount()
     */
    public long getStudentCount() {
        List studentList = getHibernateTemplate().find(
                "select count(student.id) from Student as student");
        if (studentList == null) {
            return 0;
        }
        return (Long) studentList.get(0);
    }

    /*
     * 保存学生实体，新增一条学生记录
     * @param student 保存的学生实例
     * @see edu.ncut.vod.hibernate.dao.StudentDao#save(edu.ncut.vod.hibernate.model .Student)
     */
    public void save(Student student) {
        getHibernateTemplate().save(student);
    }

    /*
     * 更新一条学生记录
     * @param student 需要更新的学生
     * @see edu.ncut.vod.hibernate.dao.StudentDao#update(edu.ncut.vod.hibernate.model.Student)
     */
    public void update(Student student) {
        getHibernateTemplate().update(student);
    }
    
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy() {
        debug("destroy mothed in StudentDaoHibernate object is called");
    }

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup() {
        debug("setup mothed in StudentDaoHibernate object is called");
    }
    
    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in StudentDaoHibernate object is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in StudentDaoHibernate object is called");
        return null;
    }
    
    /*
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }

}
