package edu.ncut.vod.hibernate.service;

import java.util.List;
import java.util.HashMap;

import edu.ncut.vod.hibernate.model.TableList;

public interface TableListService {

    /**
     * get all the name of table from table 'tablelist' 
     * @return
     */
	public List<TableList> getAllTables();
	
	/**
	 * according to parameter 'tableName' ,get all content in table
	 * @param tableName
	 * @return
	 */
	public List getTableInformation(String tableName);
}
