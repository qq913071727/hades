package edu.ncut.vod.hibernate.service;

import java.util.List;

import edu.ncut.vod.hibernate.model.AdClickInfo;

public interface AdClickInfoService {
	public List<AdClickInfo> getAllAdClickInfo();

}
