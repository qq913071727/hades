package edu.ncut.vod.hibernate.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface VideoInformationService {
    
    /**
     * output a sentence in console for debugging
     * @param message
     */
    public void debug(String message);

    /**
     * return number of rows searched in database
     * @return 
     */
    public int getResultSize();
    
    /**
     * return a List object about Videoinfo objects
     * @return
     */
    public List getVideoinfos();

    /**
     * according to 'videoId',search database and get a String object of videoURL and 
     * a String object of informationname.encapsulate them in a Map object and return.
     */
    public Map demandVideo(Integer videoId,Integer serverId,HttpServletRequest request);
    
    /**
     * get column 'browsenumber' of table 'videoinfo',increase one and update table 
     * @param videoId
     */
    public void increaseOneOnColumnBrowsenumber(Integer videoId);
    
    /**
     * get parameter used in file 'realplayer.jsp'
     * @param videoId
     * @param request
     * @return
     */
    public Map getParameterUsedInFileRealplayer(Integer videoId,HttpServletRequest request);
    
    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup();
    
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy();

}
