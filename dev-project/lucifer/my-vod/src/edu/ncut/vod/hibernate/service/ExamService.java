package edu.ncut.vod.hibernate.service;

import java.util.List;

import edu.ncut.vod.exception.ExamException;
import edu.ncut.vod.hibernate.model.ExamType;
import edu.ncut.vod.hibernate.model.Question;
import edu.ncut.vod.hibernate.model.Student;

public interface ExamService
{
    int STUDENT_PAGE_SIZE = 10;
    int QUESTION_PAGE_SIZE = 3;

    /**
     * 增加一个学生实例，对应为增加一条学生的记录
     * @param stuNumber 学生学号。
     * @param name 学生学号。
     * @param classname 学生学号。
     * @param humanId 学生学号。
     * @param email 学生学号。
     * @param address 学生地址。
     * @param phone 学生电话。
     * @return 新增学生的主键
     */
    int addStudent(String stuNumber , String name , 
        String classname , String humanId , String email ,
        String address , String phone) throws ExamException;

    /**
     * 根据学生Id删除学生
     * @param id 需要删除的学生的主键。
     */
    void deleteStudent(int id) throws ExamException;

    /**
     * 根据页码列出学生列表
     * @param pageNo 页码
     * return 列出的学生列表
     */
    List<Student> listStudent(int pageNo) throws ExamException;


    /**
     * 增加一个试题，增加一条试题记录
     * @param quTitle 试题题目。
     * @param quHard 试题难度。
     * @param quScore 试题分数。
     * @param quAnswer 试题答案。
     * @param quType 试题类型。
     * @param selectOption 试题选项。
     * @param examTypeId 试题对应的考试类型
     * @return 新增试题的主键
     */
    int addQuestion(String quTitle,String quHard,String quScore,
        String quAnswer,String quType,String selectOption , int examTypeId) throws ExamException;

    /**
     * 根据试题ID删除试题
     * @param id 需要删除的试题的主键。
     */
    void deleteQuestion(int id) throws ExamException;   

    /**
     * 根据页码列出试题列表
     * @param pageNo 页码
     * return 列出的试题列表
     */
    List<Question> listQuestion(int pageNo) throws ExamException;

    /**
     * 根据考试类型ID删除考试类型
     * @param id 需要删除的考试类型的主键。
     */
    void deleteExamType(int id) throws ExamException;

    /**
     * 新增考试类型
     * @param testName 新增的考试名称。
     * @param testTime 新增的考试时间
     * @return 新增的考试类型的ID
     */
    int addExamType(String testName , String testTime) throws ExamException;

    /**
     * 获取所有考试类型
     * @return 所有考试类型
     */
    List<ExamType> getAllExamType() throws ExamException;

    /**
     * 根据用户名和密码判断用户是否可以成功登录
     * @param user 登录用的用户名
     * @param pass 登录用的密码
     */
    boolean adminLogin(String user , String pass) throws ExamException;

    /**
     * 获取学生数量
     * @return 学生的个数
     */
    int getStudentCount()throws ExamException;

    /**
     * 获取试题数量
     * @return 试题的个数
     */
    int getQuestionCount()throws ExamException;

    /**
     * 根据每页记录数，总记录数获取总页数
     * @param count 总记录数
     * @param pageSize 每页显示的记录数
     * @return 计算得到的总页数
     */
    int getPageCount(int count , int pageSize);

    /**
     * 判断学生是否可以成功登录。
     * @param name 登录用的学生姓名
     * @param stuNumber 登录用的学号
     */
    String studentLogin(String name , String stuNumber)throws ExamException;

    /**
     * 根据考试类型ID获取下一个试题
     * @param already 已经回答的试题ID
     * @param examTypeId 考试类型ID
     * return 该考试类型的下一个试题
     */
    Question getNextQuestion(List<Integer> alreadys , int examTypeId)throws ExamException;

    /**
     * 根据试题ID获取实体
     * @param id 试题ID
     * return 该ID对应的试题
     */
    Question getQuestionById(int id)throws ExamException;

    /**
     * 根据考试类型ID获取考试类型
     * @param typeId 考试类型ID
     * return 该ID对应的考试类型名
     */
    String getExamTypeName(int typeId)throws ExamException;

}
