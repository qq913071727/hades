package edu.ncut.vod.hibernate.dao;

import java.util.List;

import edu.ncut.vod.hibernate.model.Serverinfo;

public interface ServerInfoDao {
    
    /**
     * add a row information about server
     * @param ip
     * @param role
     */
    public void addServerinfo(Serverinfo serverInfo);
    
    /**
     * save or update object 'Serverinfo'
     * @param serverInfo
     */
    public void save(Serverinfo serverInfo);
    
    /**
     * delete a row information about server
     * @param ip
     * @param role
     */
    public void deleteServerinfo(Integer id);
    
    /**
     * get a row information according to id
     * @param id
     * @return
     */
    public Serverinfo getServerinfo(Integer id);
    
    /**
     * get a row information according to videoURL
     * @param videoURL
     * @return
     */
    public List<Serverinfo> getServerinfoByVideoURL(String videoURL);
    
    /**
     * get a row information according to ip
     * @param ip
     * @return
     */
    public List<Serverinfo> getServerinfo(String ip);
    
    /**
     * get rows about table 'serverinfo'
     * @return
     */
    public List getServerinfos();
}
