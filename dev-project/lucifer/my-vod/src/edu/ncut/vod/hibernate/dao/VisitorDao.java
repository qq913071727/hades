package edu.ncut.vod.hibernate.dao;

import java.util.List;

import edu.ncut.vod.hibernate.model.Visitor;

public interface VisitorDao {
    public void save(Visitor v);

    public void delete(Integer id);

    public Visitor getVisitor(Integer id);

    public List getVisitors();
    
    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup();
    
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy();
}
