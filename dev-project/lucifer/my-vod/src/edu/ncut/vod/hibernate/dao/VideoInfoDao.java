package edu.ncut.vod.hibernate.dao;

import java.util.List;

import edu.ncut.vod.hibernate.model.VideoInfo;

public interface VideoInfoDao {
    
    public void save(VideoInfo v);

    public void delete(Integer id);
    
    public VideoInfo getVideoinfo(Integer id);

    public List getVideoinfos();      
    
    /**
     * according to videoId and browseNumber,get this row and increase browsenumber column of this row
     */
    public void updateBrowseNumber(Integer videoId,Integer browseNumber);
    
    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup();
    
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy();

}
