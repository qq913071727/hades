package edu.ncut.vod.hibernate.dao.impl;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import edu.ncut.vod.hibernate.dao.VisitorDao;
import edu.ncut.vod.hibernate.model.Visitor;

public class VisitorDaoHibernate extends HibernateDaoSupport implements VisitorDao,BeanPostProcessor {

    public void delete(Integer id) {
        getHibernateTemplate().delete(getVisitor(id));
    }

    public Visitor getVisitor(Integer id) {
        return (Visitor) getHibernateTemplate().get(edu.ncut.vod.hibernate.model.Visitor.class, id);
    }

    public List getVisitors() {
        return getHibernateTemplate().find("from Visitor order by visitorid"); 
    }

    public void save(Visitor v) {
        getHibernateTemplate().saveOrUpdate(v);
    }
    
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy() {
        debug("destroy mothed in VisitorDaoHibernate object is called");
    }

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup() {
        debug("setup mothed in VisitorDaoHibernate object is called");
    }
    
    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in VisitorDaoHibernate object is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in VisitorDaoHibernate object is called");
        return null;
    }
    
    /*
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }
}
