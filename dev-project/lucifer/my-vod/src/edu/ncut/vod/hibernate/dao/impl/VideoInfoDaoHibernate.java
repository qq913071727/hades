package edu.ncut.vod.hibernate.dao.impl;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import edu.ncut.vod.hibernate.dao.VideoInfoDao;
import edu.ncut.vod.hibernate.model.VideoInfo;

public class VideoInfoDaoHibernate extends HibernateDaoSupport implements VideoInfoDao,BeanPostProcessor {

    /*
     * according to field 'id',delete corresponding row in database
     * @see edu.ncut.vod.hibernate.dao.VideoinfoDao#delete(java.lang.Integer)
     */
    public void delete(Integer id) {
        //debug("delete in class 'VideoinfoDaoImpl' begins");

        /**
         * public abstract class HibernateDaoSupport extends DaoSupport:
         *      Convenient super class for Hibernate data access objects. 
         * public final HibernateTemplate getHibernateTemplate(): 
         *      Return the HibernateTemplate for this DAO, pre-initialized with the
         *      SessionFactory or set explicitly.
         * 
         * public class HibernateTemplate extends HibernateAccessor implements HibernateOperations:
         *      Helper class that simplifies Hibernate data access code, and converts checked 
         *      HibernateExceptions into unchecked DataAccessExceptions, following 
         *      the org.springframework.dao exception hierarchy. 
         * public void delete(Object entity) throws DataAccessException: 
         *      Delete the given persistent instance.
         */
        getHibernateTemplate().delete(getVideoinfo(id));

        //debug("delete in class 'VideoinfoDaoImpl' ends");
    }

    /*
     * according to field 'id',get corresponding row in database
     * @see edu.ncut.vod.hibernate.dao.VideoinfoDao#getVideoinfo(java.lang.Integer)
     */
    public VideoInfo getVideoinfo(Integer id) {
        /**
         * public Object get(Class entityClass,Serializable id) throws DataAccessException:
         *      Return the persistent instance of the given entity class with the given 
         *      identifier, or null if not found. 
         */
        return (VideoInfo) getHibernateTemplate().get(edu.ncut.vod.hibernate.model.VideoInfo.class, id);       
    }

    /*
     * return all rows of a table in database
     * @see edu.ncut.vod.hibernate.dao.VideoinfoDao#getVideoinfos(org.hibernate.Session)
     */
    public List<?> getVideoinfos() {
        return getHibernateTemplate().find("from VideoInfo order by videoid"); 
    }

    /**
     * save VideoInfo object
     */
    public void save(VideoInfo v) {
        getHibernateTemplate().saveOrUpdate(v);
    }
    
    /**
     * according to videoId and browseNumber,get this row and increase browsenumber column of this row
     */
    public void updateBrowseNumber(Integer videoId,Integer browseNumber){
        VideoInfo videoInfo = (VideoInfo)getHibernateTemplate().get(edu.ncut.vod.hibernate.model.VideoInfo.class, videoId);
        videoInfo.setBrowsenumber(browseNumber);
        save(videoInfo);
    }
    
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy() {
        debug("destroy mothed in VideoInfoDaoHibernate object is called");
    }

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup() {
        debug("setup mothed in VideoInfoDaoHibernate object is called");
    }
    
    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in VideoInfoDaoHibernate object is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in VideoInfoDaoHibernate object is called");
        return null;
    }
    
    /**
     * print message on console
     * @param message
     */
    public void debug(String message){
        System.out.println(message);
    }
}
