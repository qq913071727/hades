package edu.ncut.vod.hibernate.model;

import java.sql.Timestamp;

public class DemandedVideoInfo implements java.io.Serializable {

    // Fields
    private static final long serialVersionUID = -316197831924412599L;
    private Integer id;
    private String videoId;
    private String videoUrl;
    private Timestamp beginTime;
    private Timestamp endTime;
    private Integer isBroadcast;

    // Constructors
    /** default constructor */
    public DemandedVideoInfo() {
    }

    /** full constructor */
    public DemandedVideoInfo(String videoId, String videoUrl,
            Timestamp beginTime, Timestamp endTime, Integer isBroadcast) {
        this.videoId = videoId;
        this.videoUrl = videoUrl;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.isBroadcast = isBroadcast;
    }

    /**
     * overloading method 'toString'
     */
    public String toString() {
        String broadcast;

        if (isBroadcast == 1) {
            broadcast = "????";
        } else{
            broadcast = "????";
        }

        return id.toString().trim() + "#" + videoId.trim() + "#"
                + videoUrl.trim() + "#" + beginTime==null ? "null":beginTime.toString().trim() + "#"
                + endTime==null ? "null":endTime.toString().trim() + "#" + broadcast.trim();
    }

    // Property accessors
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Timestamp getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Timestamp beginTime) {
        this.beginTime = beginTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Integer getIsBroadcast() {
        return isBroadcast;
    }

    public void setIsBroadcast(Integer isBroadcast) {
        this.isBroadcast = isBroadcast;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

}