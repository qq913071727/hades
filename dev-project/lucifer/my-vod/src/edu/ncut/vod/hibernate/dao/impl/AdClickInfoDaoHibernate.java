package edu.ncut.vod.hibernate.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import edu.ncut.vod.hibernate.dao.AdClickInfoDao;
import edu.ncut.vod.hibernate.model.AdClickInfo;

public class AdClickInfoDaoHibernate extends HibernateDaoSupport implements AdClickInfoDao,BeanPostProcessor {

    /* (non-Javadoc)
     * @see edu.ncut.vod.hibernate.dao.AdClickInfoDao#delete(java.lang.Integer)
     */
    public void delete(Integer id) {
        getHibernateTemplate().delete(getAdClickInfo(id));
    }

    /* (non-Javadoc)
     * @see edu.ncut.vod.hibernate.dao.AdClickInfoDao#getAdClickInfo(java.lang.Integer)
     */
    public AdClickInfo getAdClickInfo(Integer id) {
        return (AdClickInfo) getHibernateTemplate().get(edu.ncut.vod.hibernate.model.AdClickInfo.class, id);
    }

    /* (non-Javadoc)
     * @see edu.ncut.vod.hibernate.dao.AdClickInfoDao#getAdClickInfos(org.hibernate.Session)
     */
    public List<AdClickInfo> getAdClickInfos() {
        return getHibernateTemplate().find("from AdClickInfo order by cid"); 
    }

    /* (non-Javadoc)
     * @see edu.ncut.vod.hibernate.dao.AdClickInfoDao#save(edu.ncut.vod.hibernate.model.AdClickInfo)
     */
    public void save(AdClickInfo ad) {
        getHibernateTemplate().saveOrUpdate(ad);
    }

    /* (non-Javadoc)
     * @see edu.ncut.vod.hibernate.dao.AdClickInfoDao#searchAmountOfResult(org.hibernate.Session)
     */
    public int searchAmountOfResult(Session session) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    /**
     * get ADid and count by group from table 'adclickinfo'
     * @return
     */
    public List<AdClickInfo> getAdinfoAndCount(){
        return getHibernateTemplate().find("select aci.adinfo.adId,count(*) from AdClickInfo as aci group by aci.adinfo.adId");
    }
    
    /**
     * according to adId,get ADid and count by group from table 'adclickinfo'
     * @param adId
     * @return
     */
    public List<AdClickInfo> getAdinfoAndCount(Integer adId){
        return getHibernateTemplate().find("select aci.adinfo.adId,count(*) from AdClickInfo as aci group by aci.adinfo.adId having aci.adinfo.adId=?",adId);
    }
    
    /**
     * get ADid from table 'adclickinfo'
     * @return
     */
    public List<AdClickInfo> getADid(){
        return getHibernateTemplate().find("select distinct adClickInfo.adinfo.adId from AdClickInfo as adClickInfo");
    }
    
    /**
     * get clientIp from table 'adclickinfo'
     * @return
     */
    public List<AdClickInfo> getClientIp(){
        return getHibernateTemplate().find("select distinct adClickInfo.clientIp from AdClickInfo as adClickInfo");
    }
    
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy() {
        debug("destroy mothed in AdClickInfoDaoHibernate object is called");
    }

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup() {
        debug("setup mothed in AdClickInfoDaoHibernate object is called");
    }
    
    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in AdClickInfoDaoHibernate object is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in AdClickInfoDaoHibernate object is called");
        return null;
    }
    
    /*
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }

}
