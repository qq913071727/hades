package edu.ncut.vod.hibernate.service.impl;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import edu.ncut.vod.hibernate.dao.AdClickInfoDao;
import edu.ncut.vod.hibernate.dao.AdInfoDao;
import edu.ncut.vod.hibernate.dao.DemandedVideoInfoDao;
import edu.ncut.vod.hibernate.dao.VideoInfoDao;
import edu.ncut.vod.hibernate.model.DemandedVideoInfo;
import edu.ncut.vod.hibernate.model.VideoInfo;
import edu.ncut.vod.hibernate.service.VideoStatisticalInformationService;
import edu.ncut.vod.smart.jrsoft.chart.DrawBar;
import edu.ncut.vod.smart.jrsoft.chart.DrawPie;

public class VideoStatisticalInformationServiceImpl implements VideoStatisticalInformationService,BeanPostProcessor {

    /*
     * dao layer object named 'adClickInfoDao'
     */
    AdClickInfoDao adClickInfoDao;    
    
    public AdClickInfoDao getAdClickInfoDao() {
        return adClickInfoDao;
    }

    public void setAdClickInfoDao(AdClickInfoDao adClickInfoDao) {
        this.adClickInfoDao = adClickInfoDao;
    }
    
    /*
     * dao layer object named 'demandedVideoInfoDao'
     */
    DemandedVideoInfoDao demandedVideoInfoDao;    
    
    public DemandedVideoInfoDao getDemandedVideoInfoDao() {
        return demandedVideoInfoDao;
    }

    public void setDemandedVideoInfoDao(DemandedVideoInfoDao demandedVideoInfoDao) {
        this.demandedVideoInfoDao = demandedVideoInfoDao;
    }
    
    /*
     * dao layer object named 'videoInfoDao'
     */
    VideoInfoDao videoInfoDao;    
    
    public VideoInfoDao getVideoInfoDao() {
        return videoInfoDao;
    }

    public void setVideoInfoDao(VideoInfoDao videoInfoDao) {
        this.videoInfoDao = videoInfoDao;
    }
    
    /*
     * dao layer object named 'adInfoDao'
     */
    AdInfoDao adInfoDao;        

    public AdInfoDao getAdInfoDao() {
        return adInfoDao;
    }

    public void setAdInfoDao(AdInfoDao adInfoDao) {
        this.adInfoDao = adInfoDao;
    }

    /**
     * get chart about statistical information
     * @param selectedObject
     * @param chart
     */
    public void getStatisticalChart(String selectedObject,String chart,int randomInt){
        debug("method getStatisticalChart in class VideoStatisticalInformationServiceImpl is called");
        
        if("demandedVideoInfo".equals(selectedObject) && "pie".equals(chart)){
            createDemandedVideoInfoPie(randomInt);
        }else if("adClickInfo".equals(selectedObject) && "pie".equals(chart)){
            createAdClickInfoPie(randomInt);
        }else if("demandedVideoInfo".equals(selectedObject) && "bar".equals(chart)){
            createDemandedVideoInfoBar(randomInt);
        }else if("adClickInfo".equals(selectedObject) && "bar".equals(chart)){
            createAdClickInfoBar(randomInt);
        }
    }
    
    /**
     * create pie chart about demandedvideoinfo table
     * @param randomInt
     */
    public void createDemandedVideoInfoPie(int randomInt){
        debug("method createDemandedVideoInfoPie in class VideoStatisticalInformationServiceImpl is called");
        
        //1.生成画饼图的类对象
        DrawPie pie=new DrawPie();
        
        Iterator it = demandedVideoInfoDao.getVideoIdAndCount().iterator();
        while(it.hasNext()) { 
            Object[] obj=(Object[])it.next();
            
            debug("videoId is "+obj[0]);//这个是videoId 
            debug("count(*) is "+obj[1]);//这个是count(*) 
                        
            //2.添加绘图的数据集
            pie.addData(videoInfoDao.getVideoinfo(Integer.valueOf(obj[0].toString())).getInformationname(), Integer.valueOf(obj[1].toString()));
        }
                        
        //3.设置图片属性
        //test.init();
        pie.setBgcolor(Color.white);
        pie.setTitle("饼图标题:关于被点播的视频的信息");
        pie.setWidth(600);
        pie.setHeight(350);
        pie.setLabelFontSize(10);
        pie.setFactor(0.2);
        //4.保存图片
        pie.saveAbs("C:\\Program Files\\Apache Software Foundation\\Tomcat 6.0\\webapps\\VideoOnDemand_v7\\images\\statisticalchart\\demandedVideoInfoPie"+randomInt+".jpg");
        //5.同时保存图片文件和地图文件
        //test.saveMap("E:\\pie.jpg","E:\\pie.map","http://localhost:8080/test/MyJsp.jsp");
        //test.saveAbs("http://localhost:8088/VideoOnDemand_v7/images/statisticalchart/pie.jpg");
    }

    /**
     * create pie chart about adclickinfo table
     * @param randomInt
     */
    public void createAdClickInfoPie(int randomInt){
        debug("method createAdClickInfoPie in class VideoStatisticalInformationServiceImpl is called");
        
        //1.生成画饼图的类对象
        DrawPie pie=new DrawPie();
        
        Iterator it = adClickInfoDao.getAdinfoAndCount().iterator();
        
        while(it.hasNext()) { 
            Object[] obj=(Object[])it.next();
            
            debug("ADid is "+obj[0]);//这个是ADid 
            debug("count(*) is "+obj[1]);//这个是count(*) 
                        
            //2.添加绘图的数据集
            pie.addData(videoInfoDao.getVideoinfo(Integer.valueOf(obj[0].toString())).getInformationname(), Integer.valueOf(obj[1].toString()));
        }
        
        //3.设置图片属性
        //test.init();
        pie.setBgcolor(Color.white);
        pie.setTitle("饼图标题:关于被选择的广告的信息");
        pie.setWidth(600);
        pie.setHeight(350);
        pie.setLabelFontSize(10);
        pie.setFactor(0.2);
        //4.保存图片
        pie.saveAbs("C:\\Program Files\\Apache Software Foundation\\Tomcat 6.0\\webapps\\VideoOnDemand_v7\\images\\statisticalchart\\adClickInfoPie"+randomInt+".jpg");
    }
    
    /**
     * create bar chart about demandedvideoinfo table
     * @param randomInt
     */
    public void createDemandedVideoInfoBar(int randomInt){
        debug("method createDemandedVideoInfoBar in class VideoStatisticalInformationServiceImpl is called");
        
        //1.生成画饼图的类对象
        DrawBar bar=new DrawBar();
        
        Iterator itVideoId = demandedVideoInfoDao.getVideoId().iterator();
        //debug((String)demandedVideoInfoDao.getVideoId().toString());
        
        while(itVideoId.hasNext()) { 
            String videoId=itVideoId.next().toString();
            String videoInformationName = videoInfoDao.getVideoinfo(Integer.valueOf(videoId)).getInformationname();
            Iterator itVideoUrl = demandedVideoInfoDao.getVideoUrl(videoId).iterator();
            debug((String)demandedVideoInfoDao.getVideoUrl(videoId).toString());
            
            while(itVideoUrl.hasNext()){
                String videoUrl=itVideoUrl.next().toString();
                String[] parameters={videoId,videoUrl};
                int count = demandedVideoInfoDao.getCountByVideoIdAndVideoUrl(parameters);
                debug("count:"+count);
                
                //2.添加绘图的数据集
                bar.addData(videoInformationName, count,videoUrl);
            }                                  
        }
        
        //3.设置图片属性
        //test.init();        
        bar.setTitle("饼图标题:关于被点播的视频的信息");
        bar.setYTitle("被点播次数");
        bar.setXTitle("视频名称");
        bar.setBgColor("white");
        bar.setIsV(true);
        bar.setWidth(600);
        bar.setMargin(0.1);
        bar.setHeight(500);
        bar.setXFontSize(10);
        bar.setYFontSize(10);
        //4.保存图片
        bar.saveAbs("C:\\Program Files\\Apache Software Foundation\\Tomcat 6.0\\webapps\\VideoOnDemand_v7\\images\\statisticalchart\\demandedVideoInfoBar"+randomInt+".jpg");
    }
    
    /**
     * create bar chart about adclickinfo table
     * @param randomInt
     */
    public void createAdClickInfoBar(int randomInt){
        debug("method createAdClickInfoBar in class VideoStatisticalInformationServiceImpl is called");
        
        //1.生成画饼图的类对象
        DrawBar bar=new DrawBar();
        
        Iterator itClientIp = adClickInfoDao.getClientIp().iterator();        
        
        while(itClientIp.hasNext()) {
            String objClientIp=itClientIp.next().toString(); 
            debug("clientIp is "+objClientIp);//这个是clientIp 
            
            Iterator itADid = adClickInfoDao.getADid().iterator();
            
            while(itADid.hasNext()) { 
                String adId=itADid.next().toString();
                String url = adInfoDao.getAdInfo(Integer.valueOf(adId)).getUrl();
                
                Iterator it = adClickInfoDao.getAdinfoAndCount(Integer.valueOf(adId)).iterator();
                
                while(it.hasNext()) { 
                    Object[] obj=(Object[])it.next();
                    
                    debug("ADid is "+obj[0]);//这个是ADid 
                    debug("count(*) is "+Integer.parseInt(obj[1].toString()));//这个是count(*) 
                    
                    bar.addData(url,Integer.parseInt(obj[1].toString()),objClientIp);                            
                }
            }
        }
        
        //3.设置图片属性
        //test.init();        
        bar.setTitle("饼图标题:关于被选择的广告的信息");
        bar.setYTitle("被选择次数");
        bar.setXTitle("广告名称");
        bar.setBgColor("white");
        bar.setIsV(true);
        bar.setWidth(600);
        bar.setMargin(0.1);
        bar.setHeight(500);
        bar.setXFontSize(10);
        bar.setYFontSize(10);
        //4.保存图片
        bar.saveAbs("C:\\Program Files\\Apache Software Foundation\\Tomcat 6.0\\webapps\\VideoOnDemand_v7\\images\\statisticalchart\\adClickInfoBar"+randomInt+".jpg");
    }
    
    /* output a sentence in console for debugging
     * @see edu.ncut.vod.hibernate.service.ServerInfoService#debug(java.lang.String)
     */
    public void debug(String message) {
        System.out.println(message);
    }
    
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy() {
        debug("destroy mothed in VideoStatisticalInformationServiceImpl object is called");
    }

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup() {
        debug("setup mothed in VideoStatisticalInformationServiceImpl object is called");
    }
    
    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in VideoStatisticalInformationServiceImpl object is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in VideoStatisticalInformationServiceImpl object is called");
        return null;
    }

}
