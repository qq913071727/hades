package edu.ncut.vod.hibernate.dao.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import edu.ncut.vod.hibernate.dao.TableListDao;
import edu.ncut.vod.hibernate.model.TableList;

public class TableListDaoHibernate extends HibernateDaoSupport implements TableListDao,BeanPostProcessor {

    private static final long serialVersionUID = 2651336021122641414L;

	/**
     * this method is called on the bean just before this bean is removed from the container
     */
	public void destroy(){
		debug("destroy method in TableListDaoHibernate object is invoked");	
	}
	
	/**
     * this method is called on the bean immediately upon instantiation
     */
	public void setup(){
		debug("setup method in TableLIstDaoHibernate object is invoked");
	}

	/**
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization method in TableListDaoHibernate object is invoked");	
        return null;
    }

	/**
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean??s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization method in TableLIstDaoHibernate object is invoked");
        return null;
    }
    
    /**
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }

	/**
	 * get all table information from table 'tablelist'
	 * @return
	 */
    public List<TableList> getAllTables() {
    	debug("getAllTables method in TableLIstDaoHibernate object is invoked");
        return getHibernateTemplate().find("from TableList order by id");
    }

    /**
     * according to parameter 'tableName',get content of table
     * @param tableName
     * @return
     */
    public List getTableContent(String tableName) {
    	debug("getTableContent method in TableLIstDaoHibernate object is invoked");
        String tableClassName = getTableClassName(tableName);
        return getHibernateTemplate().find("from "+tableClassName+" order by id");
    }

    /**
	 * get the Class name of table
	 * @param tableName
	 * @return
	 */
	public String getTableClassName(String tableName) {
		debug("getTableClassName method in TableLIstDaoHibernate object is invoked");
		return getHibernateTemplate().find("select tableclassname from TableList where tablename=? order by id",tableName).get(0).toString();
    }

}
