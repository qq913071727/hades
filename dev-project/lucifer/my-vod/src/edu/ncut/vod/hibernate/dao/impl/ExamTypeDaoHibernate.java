package edu.ncut.vod.hibernate.dao.impl;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import edu.ncut.vod.hibernate.dao.ExamTypeDao;
import edu.ncut.vod.hibernate.enhance.HibernateDaoSupportEnhance;
import edu.ncut.vod.hibernate.model.ExamType;

public class ExamTypeDaoHibernate extends HibernateDaoSupportEnhance implements
        ExamTypeDao,BeanPostProcessor {

    /* 
     * 删除考试类型实体，删除一条考试类型记录
     * @param examType 删除的考试类型实例
     * @see edu.ncut.vod.hibernate.dao.ExamTypeDao#delete(edu.ncut.vod.hibernate.model.ExamType)
     */
    public void delete(ExamType examType) {
        getHibernateTemplate().delete(examType);
    }

    /* 
     * 根据主键删除考试类型实体，删除一条考试类型记录
     * @param id 删除考试类型的主键
     * @see edu.ncut.vod.hibernate.dao.ExamTypeDao#delete(int)
     */
    public void delete(int id) {
        getHibernateTemplate().delete(get(id));
    }

    /* 
     * 查询全部的考试类型
     * @param examType 需要更新的考试管理员
     * @see edu.ncut.vod.hibernate.dao.ExamTypeDao#findAll()
     */
    public List<ExamType> findAll() {
        return getHibernateTemplate().find("from ExamType");
    }

    /* 
     * 根据主键加载考试类型
     * @param id 需要加载的考试类型的主键值
     * @return 加载的考试类型PO
     * @see edu.ncut.vod.hibernate.dao.ExamTypeDao#get(int)
     */
    public ExamType get(int id) {
        return (ExamType)getHibernateTemplate().load(ExamType.class , new Integer(id));
    }

    /* 
     * 保存考试类型实体，新增一条考试类型记录
     * @param examType 保存的考试类型实例
     * @see edu.ncut.vod.hibernate.dao.ExamTypeDao#save(edu.ncut.vod.hibernate.model.ExamType)
     */
    public void save(ExamType examType) {
        getHibernateTemplate().save(examType);
    }

    /* 
     * 更新一条考试类型记录
     * @param examType 需要更新的考试类型
     * @see edu.ncut.vod.hibernate.dao.ExamTypeDao#update(edu.ncut.vod.hibernate.model.ExamType)
     */
    public void update(ExamType examType) {
        getHibernateTemplate().update(examType);
    }
    
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy() {
        debug("destroy mothed in ExamTypeDaoHibernate object is called");
    }

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup() {
        debug("setup mothed in ExamTypeDaoHibernate object is called");
    }
    
    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in ExamTypeDaoHibernate object is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in ExamTypeDaoHibernate object is called");
        return null;
    }
    
    /*
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }

}
