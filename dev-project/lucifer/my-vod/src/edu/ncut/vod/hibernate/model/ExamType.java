package edu.ncut.vod.hibernate.model;

import java.io.Serializable;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class ExamType implements Serializable, BeanPostProcessor {

    private static final long serialVersionUID = 8605320451507068931L;

    // Fields
    private Integer typeId;
    private String testTitle;
    private String testTime;

    // Constructors
    /** default constructor */
    public ExamType() {
    }

    /** full constructor */
    public ExamType(String testTitle, String testTime) {
        this.testTitle = testTitle;
        this.testTime = testTime;
    }

    /**
     * overloading method 'toString'
     */
    public String toString(){
	 	return typeId.toString().trim()+"#"+testTitle.trim()+"#"+testTime.trim();
	 }

    // Property accessors
    public Integer getTypeId() {
        return this.typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTestTitle() {
        return this.testTitle;
    }

    public void setTestTitle(String testTitle) {
        this.testTitle = testTitle;
    }

    public String getTestTime() {
        return this.testTime;
    }

    public void setTestTime(String testTime) {
        this.testTime = testTime;
    }

    /*
     * the postProcessAfterInitialization() method is called immediately after
     * initialization.
     * 
     * @seeorg.springframework.beans.factory.config.BeanPostProcessor#
     * postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("method postProcessAfterInitialization in class ExamType is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior
     * to bean initialization (the call to afterPropertiesSet() and the bean’s
     * custom initmethod).
     * 
     * @seeorg.springframework.beans.factory.config.BeanPostProcessor#
     * postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("method postProcessBeforeInitialization in class ExamType is called");
        return null;
    }

    /**
     * print message on console
     * 
     * @param message
     */
    public void debug(String message) {
        System.out.println(message);
    }

}