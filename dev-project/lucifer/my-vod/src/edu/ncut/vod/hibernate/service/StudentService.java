package edu.ncut.vod.hibernate.service;

import java.util.List;

import edu.ncut.vod.hibernate.model.Student;

public interface StudentService {
    
    /**
     * find all records about student in table 'student'
     * @return
     */
    List<Student> findAllStudent();
}
