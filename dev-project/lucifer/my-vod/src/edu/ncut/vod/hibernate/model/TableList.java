package edu.ncut.vod.hibernate.model;

public class TableList implements java.io.Serializable {

	// Fields
    private static final long serialVersionUID = 8931156314769722562L;
    private Integer id;
    private String tablename;
    private String tableclassname;

    // Constructors
    /** default constructor */
    public TableList() {
    }

    /** full constructor */
    public TableList(String tablename, String tableclassname) {
        this.tablename = tablename;
        this.tableclassname = tableclassname;
    }

    /**
     * overloading method 'toString'
     */
    public String toString(){
		return id.toString().trim()+"#"+tablename.trim()+"#"+tableclassname.trim();
	}

    // Property accessors
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTablename() {
        return this.tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getTableclassname() {
        return this.tableclassname;
    }

    public void setTableclassname(String tableclassname) {
        this.tableclassname = tableclassname;
    }

}