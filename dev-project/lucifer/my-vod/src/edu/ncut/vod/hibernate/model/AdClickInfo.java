﻿package edu.ncut.vod.hibernate.model;

import java.io.Serializable;
import java.sql.Timestamp;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class AdClickInfo implements Serializable,BeanPostProcessor {

    private static final long serialVersionUID = -3465954076905974193L;
    private Integer cid;
    private AdInfo adinfo;
    private String clientIp;
    private Timestamp clicktime;

    /** default constructor */
    public AdClickInfo() {
    }

    /** full constructor */
    public AdClickInfo(AdInfo adinfo, String clientIp, Timestamp clicktime) {
        this.adinfo = adinfo;
        this.clientIp = clientIp;
        this.clicktime = clicktime;
    }

    /**
     * overloading method 'toString'
     */
    public String toString() {
        return cid.toString().trim() + "#" + adinfo.toString().trim() + "#"
                + clientIp.trim()+"#"+clicktime.toString().trim();
    }

    public Integer getCid() {
        return this.cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public AdInfo getAdinfo() {
        return this.adinfo;
    }

    public void setAdinfo(AdInfo adinfo) {
        this.adinfo = adinfo;
    }

    public String getClientIp() {
        return this.clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public Timestamp getClicktime() {
        return this.clicktime;
    }

    public void setClicktime(Timestamp clicktime) {
        this.clicktime = clicktime;
    }

    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization is called");
        return null;
    }

    /*
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }

}