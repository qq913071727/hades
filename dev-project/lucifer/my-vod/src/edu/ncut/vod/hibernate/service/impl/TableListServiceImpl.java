package edu.ncut.vod.hibernate.service.impl;

import java.util.List; 

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import edu.ncut.vod.hibernate.model.TableList;
import edu.ncut.vod.hibernate.service.TableListService;
import edu.ncut.vod.hibernate.dao.TableListDao;

public class TableListServiceImpl implements TableListService,
        BeanPostProcessor {
         	
    /**
     * dependency injection object named 'tableListDao'
     */
    private TableListDao tableListDao;    
    
    public TableListDao getTableListDao() {
        return tableListDao;
    }

    public void setTableListDao(TableListDao tableListDao) {
        this.tableListDao = tableListDao;
    }
     
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
	public void destroy(){
		debug("destroy method in TableListServiceImpl object is invoked");		
	}
	
	/**
     * this method is called on the bean immediately upon instantiation
     */
	public void setup(){
		debug("setup mothed in TableListServiceImpl object is invoked");
	}

    /**
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object bean, String beanName)
            throws BeansException {
        debug("postProcessAfterInitialization method in TableListServiceImpl object is invoked");
        return null;
    }

    /**
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean��s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object bean, String beanName)
            throws BeansException {
        debug("postBeanBeforeInitialization method in TableListServiceImpl object is invoked");
        return null;
    }
    
    /**
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }

	/**
     * get all the name of table from table 'tablelist' 
     * @return
     */
    public List<TableList> getAllTables() {
    	debug("method getAllTable in class TableListServiceImpl is invoked");
        return tableListDao.getAllTables();
    }       

    /**
     * according to parameter 'tableName' ,get all content in table
     * @param tableName
     * @return
     */
    public List getTableInformation(String tableName) {
     	debug("method getTableInformation in class TableListServiceImpl is invoked");
        List tableContent = tableListDao.getTableContent(tableName);                        
        return tableContent;
    }

}
