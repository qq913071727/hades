package edu.ncut.vod.hibernate.dao;

import java.util.List;

import edu.ncut.vod.hibernate.model.DemandedVideoInfo;

public interface DemandedVideoInfoDao {
    public void save(DemandedVideoInfo dvi);

    public void delete(Integer id);
    
    public DemandedVideoInfo getDemandedvideoinfo(Integer id);

    public List<?> getDemandedvideoinfos();
    
    /**
     * get videoid and count by group from table 'demandedvideoinfo'
     * @return
     */
    public List<DemandedVideoInfo> getVideoIdAndCount();
    
    /**
     * get an array about videoid in table 'demandedvideoinfo'
     * @return
     */
    public List getVideoId();
    
    /**
     * get an array about videoURL in table 'demandedvideoinfo'
     * @param videoId
     * @return
     */
    public List getVideoUrl(String videoId);
    
    /**
     * get count at table 'demandedvideoInfo',according to videoid and videoURL
     * @param parameters
     * @return
     */
    public int getCountByVideoIdAndVideoUrl(String[] parameters);

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup();
    
    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy();
}
