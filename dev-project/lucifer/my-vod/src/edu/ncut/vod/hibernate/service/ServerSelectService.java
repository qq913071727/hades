package edu.ncut.vod.hibernate.service;

public interface ServerSelectService {
    
    /**
     * add main server information to database
     * @param localServerIp
     * @param role
     * @param num
     */
    public String addMainServer(String localServerIp,Integer role,Integer num);
    
    /**
     * add subsidiary server information to database
     * @param inputIp
     * @param mainServerIp
     * @param role
     * @param num
     */
    public String addSubsidiaryServer(String inputIp,String localServerIp,Integer role,Integer num);
    
    /**
     * according to inputIp,try to find row in database
     * @param inputIp
     * @return
     */
    public boolean findServerInfoByIp(String ip);
    
    /**
     * print message on console
     * @param message
     */
    public void debug(String message);
}
