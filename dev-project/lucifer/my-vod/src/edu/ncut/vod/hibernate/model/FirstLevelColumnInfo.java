package edu.ncut.vod.hibernate.model;

public class FirstLevelColumnInfo implements java.io.Serializable {

    // Fields
    private static final long serialVersionUID = 5466135840423812263L;
    private Integer classid;
    private String columnname;

    // Constructors
    /** default constructor */
    public FirstLevelColumnInfo() {
    }

    /** full constructor */
    public FirstLevelColumnInfo(String columnname) {
        this.columnname = columnname;
    }

    /**
     * overloading method 'toString'
     */
     public String toString(){
	 	return classid.toString().trim()+"#"+columnname.trim();
	 }

    // Property accessors
    public Integer getClassid() {
        return this.classid;
    }

    public void setClassid(Integer classid) {
        this.classid = classid;
    }

    public String getColumnname() {
        return this.columnname;
    }

    public void setColumnname(String columnname) {
        this.columnname = columnname;
    }

}