package edu.ncut.vod.hibernate.service.impl;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import edu.ncut.vod.hibernate.dao.StudentDao;
import edu.ncut.vod.hibernate.model.Student;
import edu.ncut.vod.hibernate.service.StudentService;

public class StudentServiceImpl implements StudentService,BeanPostProcessor {
    
    /*
     * dao layer object 'studentDao'
     */
    StudentDao studentDao;    
    
    public StudentDao getStudentDao() {
        return studentDao;
    }

    public void setStudentDao(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    /**
     * 
     */
    public List<Student> findAllStudent() {
        
        return null;
    }

    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy() {
        debug("destroy mothed in StudentServiceImpl object is called");
    }

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup() {
        debug("setup mothed in StudentServiceImpl object is called");
    }
    
    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in StudentServiceImpl object is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in StudentServiceImpl object is called");
        return null;
    }
    
    /**
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }
}
