package edu.ncut.vod.hibernate;

import edu.ncut.vod.hibernate.dao.VideoInfoDao;
import edu.ncut.vod.hibernate.dao.VisitorDao;
import edu.ncut.vod.hibernate.dao.impl.VideoInfoDaoHibernate;
import edu.ncut.vod.hibernate.dao.impl.VisitorDaoHibernate;

/**
 * factory class 'DaoFactory'
 * @author lishenlishen
 */
public class DaoFactory {

    /**
     * @return object 'VideoinfoDao'
     */
    public static VideoInfoDao getVideoinfoDaoInstance() {
        return new VideoInfoDaoHibernate();
    }

    /**
     * @return object 'VisitorDao'
     */
    public static VisitorDao getVisitorDaoInstance() {
        return new VisitorDaoHibernate();
    }    
}
