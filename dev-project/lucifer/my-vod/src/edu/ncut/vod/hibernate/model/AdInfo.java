﻿package edu.ncut.vod.hibernate.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class AdInfo implements Serializable,BeanPostProcessor {

    private static final long serialVersionUID = -7797839083115230110L;
    private Integer adId;
    private String url;
    private String imgName;
    private Set adClickInfos = new HashSet(0);

    /** default constructor */
    public AdInfo() {
    }

    /** minimal constructor */
    public AdInfo(String url, String imgName) {
        this.url = url;
        this.imgName = imgName;
    }

    /** full constructor */
    public AdInfo(String url, String imgName, Set adClickInfos) {
        this.url = url;
        this.imgName = imgName;
        this.adClickInfos = adClickInfos;
    }

    /**
     * overloading method 'toString'
     */
    public String toString(){
		return adId.toString().trim()+"#"+url.trim()+"#"+imgName.trim();
	}

    // Property accessors

    public Integer getAdId() {
        return this.adId;
    }

    public void setAdId(Integer adId) {
        this.adId = adId;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImgName() {
        return this.imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public Set getAdClickInfos() {
        return this.adClickInfos;
    }

    public void setAdClickInfos(Set adClickInfos) {
        this.adClickInfos = adClickInfos;
    }

    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("method postProcessAfterInitialization in class AdInfo is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("method postProcessBeforeInitialization in class AdInfo is called");
        return null;
    }

    /**
     * print message on console
     * @param message
     */
    public void debug(String message){
        System.out.println(message);
    }

}