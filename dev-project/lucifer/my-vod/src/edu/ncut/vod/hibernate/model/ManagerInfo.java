package edu.ncut.vod.hibernate.model;

/**
 * Managerinfo entity. @author MyEclipse Persistence Tools
 */

public class ManagerInfo implements java.io.Serializable {

    // Fields
    private static final long serialVersionUID = -3251879974532929862L;
    private Integer managerid;
    private String managername;
    private String managerpassword;
    private String managerlevel;

    // Constructors
    /** default constructor */
    public ManagerInfo() {
    }

    /** full constructor */
    public ManagerInfo(String managername, String managerpassword,
            String managerlevel) {
        this.managername = managername;
        this.managerpassword = managerpassword;
        this.managerlevel = managerlevel;
    }

    /**
     * overloading method 'toString'
     */
     public String toString(){
	 	return managerid.toString().trim()+"#"+managername.trim()+"#"+managerpassword.trim()+"#"+managerlevel.trim();
	 }

    // Property accessors
    public Integer getManagerid() {
        return this.managerid;
    }

    public void setManagerid(Integer managerid) {
        this.managerid = managerid;
    }

    public String getManagername() {
        return this.managername;
    }

    public void setManagername(String managername) {
        this.managername = managername;
    }

    public String getManagerpassword() {
        return this.managerpassword;
    }

    public void setManagerpassword(String managerpassword) {
        this.managerpassword = managerpassword;
    }

    public String getManagerlevel() {
        return this.managerlevel;
    }

    public void setManagerlevel(String managerlevel) {
        this.managerlevel = managerlevel;
    }

}