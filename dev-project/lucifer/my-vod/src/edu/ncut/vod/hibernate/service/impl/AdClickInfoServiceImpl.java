package edu.ncut.vod.hibernate.service.impl;

import java.util.List;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.BeansException;

import edu.ncut.vod.hibernate.model.AdClickInfo;
import edu.ncut.vod.hibernate.service.AdClickInfoService;
import edu.ncut.vod.hibernate.dao.AdClickInfoDao;

public class AdClickInfoServiceImpl implements AdClickInfoService,BeanPostProcessor {
    
    /**
     * dependency injection object 'adClickInfoDao'
     */
	private AdClickInfoDao adClickInfoDao;	
	
    public AdClickInfoDao getAdClickInfoDao() {
        return adClickInfoDao;
    }

    public void setAdClickInfoDao(AdClickInfoDao adClickInfoDao) {
        this.adClickInfoDao = adClickInfoDao;
    }

    /**
     * get all AdClickInfo objects from database
     */
    public List<AdClickInfo> getAllAdClickInfo() {        
        return (List<AdClickInfo>)adClickInfoDao.getAdClickInfos();
    }

	/**
     * this method is called on the bean just before this bean is removed from the container
     */
	public void destroy(){
		debug("destroy method in AdClickInfoServiceImpl object is invoked");		
	}
	
	/**
     * this method is called on the bean immediately upon instantiation
     */
	public void setup(){
		debug("setup mothed in AdClickInfoServiceImpl object is invoked");
	}
	
	/**
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
	public Object postProcessAfterInitialization(Object arg0,String srg1)
			throws BeansException{
		debug("postProcessAfterInitialization method in AdClickInfoServiceImpl object is invoked");
		return null;
	}
	
	/**
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean��s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
	public Object postProcessBeforeInitialization(Object arg0,String arg1)
			throws BeansException{
		debug("postBeanBeforeInitialization method in AdClickInfoServiceImpl object is invoked");
		return null;		
	}
	
	/**
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }
}
