package edu.ncut.vod.hibernate.model;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class Serverinfo implements java.io.Serializable,BeanPostProcessor {

    // Fields
    private static final long serialVersionUID = -8106493479882150618L;
    private Integer id;
    private String ip;
    private Integer role;
    private Integer curShowNum;

    // Constructors
    /** default constructor */
    public Serverinfo() {
    }

    /** full constructor */
    public Serverinfo(String ip, Integer role, Integer curShowNum) {
        this.ip = ip;
        this.role = role;
        this.curShowNum = curShowNum;
    }

    /**
     * overloading method 'toString'
     */
     public String toString(){
	 	return id.toString().trim()+"#"+ip.trim()+"#"+role.toString().trim()+"#"+curShowNum.toString().trim();
	 }

    // Property accessors
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getRole() {
        return this.role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Integer getCurShowNum() {
        return this.curShowNum;
    }

    public void setCurShowNum(Integer curShowNum) {
        this.curShowNum = curShowNum;
    }

    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("method postProcessAfterInitialization in class Serverinfo is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("method postProcessBeforeInitialization in class Serverinfo is called");
        return null;
    }

    /**
     * print message on console
     * @param message
     */
    public void debug(String message){
        System.out.println(message);
    }
}