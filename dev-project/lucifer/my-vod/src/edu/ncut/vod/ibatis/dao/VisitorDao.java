package edu.ncut.vod.ibatis.dao;

import java.util.List;

import edu.ncut.vod.hibernate.model.Visitor;

public interface VisitorDao {
    public void save(Visitor v);

    public void delete(Integer id);

    public Visitor getVisitor(Integer id);

    public List getVisitors();
}
