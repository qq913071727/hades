package edu.ncut.vod.ibatis.dao.impl;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import edu.ncut.vod.hibernate.model.Visitor;
import edu.ncut.vod.ibatis.dao.VisitorDao;

public class VisitorDaoIbatis extends SqlMapClientDaoSupport implements VisitorDao,BeanPostProcessor {

    public void delete(Integer id) {
        this.getSqlMapClientTemplate().delete("delete",id);
    }

    public Visitor getVisitor(Integer id) {
        Visitor visitor  =(Visitor) this.getSqlMapClientTemplate().queryForObject("getVisitor",id);
        return visitor;
    }

    public List getVisitors() {
        return getSqlMapClientTemplate().queryForList("getVisitors");
    }

    public void save(Visitor v) {
        this.getSqlMapClientTemplate().update("save",v);
    }

    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object bean, String beanName)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in VisitorDaoIbatis object is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object bean, String beanName)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in VisitorDaoIbatis object is called");
        return null;
    }

    /*
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }
}
