package edu.ncut.vod.ibatis.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import com.ibatis.sqlmap.client.SqlMapClient;

import edu.ncut.vod.ibatis.DatabaseManager;
import edu.ncut.vod.ibatis.dao.impl.VisitorDaoIbatis;
import edu.ncut.vod.ibatis.model.Visitor;
import edu.ncut.vod.ibatis.service.LoginAndLogoffService;

public class LoginAndLogoffServiceImpl implements LoginAndLogoffService,BeanPostProcessor {

    /*
     * service object 'VisitorDaoIbatis'
     */
    private VisitorDaoIbatis visitorDaoIbatis;           

    public VisitorDaoIbatis getVisitorDaoIbatis() {
        return visitorDaoIbatis;
    }

    public void setVisitorDaoIbatis(VisitorDaoIbatis visitorDaoIbatis) {
        this.visitorDaoIbatis = visitorDaoIbatis;
    }

    /**
     * store returning results
     */
    private Map<String,String> resultMap;                

    public Map<String, String> getResultMap() {              
        return resultMap;
    }

    public void setResultMap(Map<String, String> resultMap) {
        this.resultMap = resultMap;
    }

    /**
     * validate if login is valid
     * @return
     */
    public Map<String,String> loginValidation(Map<String,Object> loginParameter){
        if(null == resultMap){
            resultMap = new HashMap<String,String>();
        } 
        resultMap.clear();
        try {
            /*
             * judge whether inputRand and username and password is equal with corresponding
             * object 'forward'
             */            
            if (loginParameter.get("rand").equals(loginParameter.get("authentication"))) {//authentication is the same as rand
                /* 
                 * reach database and compare fields from database with fields from form to
                 * determine whether login is legal
                 */
                
                List<Visitor> list = visitorDaoIbatis.getVisitors();
                for (Visitor v : list) {
                    String visitorName = v.getVisitorname().toString().trim();
                    String visitorpassword = v.getPassword().toString().trim();
                    if (loginParameter.get("username").equals(visitorName) && loginParameter.get("password").equals(visitorpassword)) {                        
                        getResultMap().put("authenticationIsWrong","false");
                        getResultMap().put("success","true");
                        getResultMap().put("forward","goToIndex");
                        
                        return resultMap;//validate succeeds
                    }
                }
                getResultMap().put("authenticationIsWrong","false");
                getResultMap().put("success","false");
                getResultMap().put("forward","goToIndex");
                
                return resultMap;//validate fails.There are not any data in database which suits condition from form
            } else {//authentication is not the same as rand
                getResultMap().put("authenticationIsWrong","true");
                getResultMap().put("success","false");
                getResultMap().put("forward","authenticationIsWrong");

                return resultMap;//validate fails.Authentication is not the same as rand which stores in object 'session'
            }
        } catch (Exception e) {
            return resultMap;//validate fails.Exception happens
        }
    }
    
    /**
     * validate if logoff is valid
     * @param logoff
     * @return
     */
    public boolean logoffValidation(String logoff) {
        boolean logoffIsValidated = false;
        if(null != logoff){
            if("logoff".equals(logoff)){
                logoffIsValidated = true;
                return logoffIsValidated;
            }
        }
        return logoffIsValidated;
    }
    
    /*
     * print message on console
     */
    public void debug(String message) {
        System.out.println(message);
    }

    /**
     * this method is called on the bean just before this bean is removed from the container
     */
    public void destroy() {
        debug("destroy mothed in LoginAndLogoffServiceImpl object is called");
    }

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup() {
        debug("setup mothed in LoginAndLogoffServiceImpl object is called");
    }

    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in LoginAndLogoffServiceImpl object is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in LoginAndLogoffServiceImpl object is called");
        return null;
    }
    

}
