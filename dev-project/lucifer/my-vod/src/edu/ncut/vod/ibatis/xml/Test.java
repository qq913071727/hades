package edu.ncut.vod.ibatis.xml;

import com.ibatis.sqlmap.client.SqlMapClient;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import edu.ncut.vod.ibatis.DatabaseManager;

public class Test {

    public static void main(String[] args) throws SQLException {
        test();
    }

    private static void test() {
        try {
            //Map parameter_map = new HashMap();

            //parameter_map.put("nd", "2009");
            //parameter_map.put("uid", "08101090215");
            //debug("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
            SqlMapClient sqlMapper = DatabaseManager.getClient();
            //debug("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
            List<HashMap> list = sqlMapper.queryForList("Test.test");
            debug("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
            debug(list.toString());
            debug("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
            for (HashMap item : list) {
                
            }
        } catch (Exception e) {            
            throw new RuntimeException("Something bad happened while building the SqlMapClient instance." + e, e);
        }
    }

    /**
     * output message on console
     * @param msg
     */
    public static void debug(String msg) {
        System.out.println(msg);
    }
}