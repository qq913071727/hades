package edu.ncut.vod.ibatis.service;

import java.util.Map;

public interface LoginAndLogoffService {

    /**
     * output a sentence in console for debugging
     * 
     * @param message
     */
    public void debug(String message);

    /**
     * this method is called on the bean immediately upon instantiation
     */
    public void setup();

    /**
     * this method is called on the bean just before this bean is removed from
     * the container
     */
    public void destroy();

    /**
     * validate if login is valid
     * @return
     */
    public Map<String,String> loginValidation(Map<String,Object> loginParameter);
    
    /**
     * validate if logoff is valid
     * @param logoff
     * @return
     */
    public boolean logoffValidation(String logoff);
    
    
}
