package edu.ncut.vod.ibatis.model;

import java.io.Serializable;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class Visitor implements Serializable,BeanPostProcessor {

    private static final long serialVersionUID = 1465791087108637535L;
    // Fields
    private Integer visitorid;
    private String visitorname;
    private String password;

    // Constructors
    /** default constructor */
    public Visitor() {
    }

    /** full constructor */
    public Visitor(String visitorname, String password) {
        this.visitorname = visitorname;
        this.password = password;
    }

    // Property accessors
    public Integer getVisitorid() {
        return this.visitorid;
    }

    public void setVisitorid(Integer visitorid) {
        this.visitorid = visitorid;
    }

    public String getVisitorname() {
        return this.visitorname;
    }

    public void setVisitorname(String visitorname) {
        this.visitorname = visitorname;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /*
     * the postProcessAfterInitialization() method is called immediately after initialization.
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessAfterInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessAfterInitialization mothed in Visitor object is called");
        return null;
    }

    /*
     * The postProcessBeforeInitialization() method is called immediately prior to bean 
     * initialization (the call to afterPropertiesSet() and the bean’s custom initmethod).
     * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
     */
    public Object postProcessBeforeInitialization(Object arg0, String arg1)
            throws BeansException {
        debug("postProcessBeforeInitialization mothed in Visitor object is called");
        return null;
    }
    
    /*
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }
}