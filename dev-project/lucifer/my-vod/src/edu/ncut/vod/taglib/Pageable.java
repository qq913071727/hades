package edu.ncut.vod.taglib;

import java.util.List;

public class Pageable {

	private int pageSize;//the number of rows of every page
	private int totalRows;//the number of all rows
	private int totalPages;//the number of all pages
	private static int currentPage;//it indicates the number of current page
	private int rowsCount;//the number of first row in this page or the number of last row in this page

	/**
	 * move to last row.
	 * set attribute 'totalRows' with last row.
	 * return to before first row
	 * @param rs: a set of result searched in database 
	 */
	public Pageable(List lists) {			    
		//set field 'totalRows',according to function 'size()' of object 'lists'
	    this.setTotalRows(lists.size());			
	}

	/**
	 * set the amount of rows of this page,at last one page
	 * @param pageSize:size of every page
	 */
	public void setPageSize(int pageSize) {
		if (pageSize >= 0) {//paging 
			this.pageSize = pageSize;//set pageSize
		} else {//no paging
			this.pageSize = 1;
		}
		//after the size of every page is known,we can set the amount of all pages
		this.setTotalPages();
	}

	/**
	 * according to parameter 'page',go to corresponding page.
	 * @param page:it is in URL rewrited in jsp file which uses this tag.
	 *     this parameter indicates number of page which shows
	 */
	public void gotoPage(int page) {

		switch (page) {
		case -1://first page
			this.setCurrentPage(1);
			break;
		case -2://previous page
			int t = this.getCurrentPage();
			this.setCurrentPage(t - 1);
			break;
		case -3://next page
			int n = this.getCurrentPage();
			debug("n:"+n);
			this.setCurrentPage(n + 1);
			break;
		case -4://last page
			this.setCurrentPage(this.getTotalPages());
			break;
		default:
			this.setCurrentPage(page);
		}
	}

	/**
	 * set currentPage
	 * @param page:page number which we will return to
	 */
	public void setCurrentPage(int page) {

		if (page <= 0){//when current page is first page,'previous link' is clicked
			this.currentPage = 1;
		}else if (page > this.getTotalPages()){//when current page is last page,'next link' is clicked
			this.currentPage = this.getTotalPages();
		}else{//other situation
			this.currentPage = page;
		}
		this.setRowsCount((this.currentPage - 1) * this.getPageSize() + 1);//???
	}

	/**
	 * get the amount of rows of current page
	 * @return the amount of rows of current page
	 */
	public int getCurrentPageRowsCount() {
		if (this.getPageSize() == 0){//no paging
			return this.getTotalRows();
		}
		if (this.getTotalRows() == 0){//no row,no paging
			return 0;
		}
		if (this.getCurrentPage() != this.getTotalPages()){//if current page is not last page
			return this.getPageSize();
		}
		return this.getTotalRows() - (this.getTotalPages() - 1)
				* this.getPageSize();//the amount of rows of current page
	}

	/**
	 * get pageSize
	 * @return pageSize
	 */
	public int getPageSize() {
		return this.pageSize;
	}

	/**
	 * get totalRows
	 * @return totalRows
	 */
	public int getTotalRows() {
		return totalRows;
	}

	/**
	 * set totalRows
	 * @param totalRows:the amount of all rows
	 */
	public void setTotalRows(int totalRows) {
		this.totalRows = totalRows;
	}

	/**
	 * get rowsCount
	 * @return rowsCount
	 */
	public int getRowsCount() {
		return rowsCount;
	}

	/**
	 * set rowsCount
	 * @param rowsCount
	 */
	public void setRowsCount(int rowsCount) {
		this.rowsCount = rowsCount;
	}

	/**
	 * get currentPage
	 * @return currentPage
	 */
	public int getCurrentPage() {
		return currentPage;
	}

	/**
	 * get totalPages
	 * @return totalPages
	 */
	public int getTotalPages() {
		return this.totalPages;
	}

	/**
	 * set the amount of all pages
	 */
	public void setTotalPages() {
		if (this.getTotalRows() == 0) {//if not any row,the number of page is 0 and no paging
			this.totalPages = 0;
		} else if (this.getPageSize() == 0) {//if the size of page is 0,no paging
			this.totalPages = 1;
		} else {
			if (this.getTotalRows() % this.getPageSize() != 0){//last page is not full
				this.totalPages = this.getTotalRows() / this.getPageSize() + 1;
			}
			else{//last page is full
				this.totalPages = this.getTotalRows() / this.getPageSize();
			}
		}
	}

	/**
	 * set the number of first row in this page
	 * @throws java.sql.SQLException
	 */
	public void pageFirst() throws java.sql.SQLException {
		this.setRowsCount((this.getCurrentPage() - 1) * this.getPageSize() + 1);
	}

	/**
	 * set the number of last row in this page
	 * @throws java.sql.SQLException
	 */
	public void pageLast() throws java.sql.SQLException {
		this.setRowsCount((this.getCurrentPage() - 1) * this.getPageSize()
				+ this.getCurrentPageRowsCount());
	}
	
	/**
     * print mesage in console
     * @param message
     */
    public static void debug(String message) {
        System.out.println(message);
    }
}
