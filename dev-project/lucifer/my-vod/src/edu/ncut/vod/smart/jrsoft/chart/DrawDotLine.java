package edu.ncut.vod.smart.jrsoft.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.io.FileOutputStream;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import javax.servlet.http.*;

/**
 * @author oracle
 */
public class DrawDotLine {
	private static DrawDotLine instance = null;	//������ͼ��ʵ��
	private String title="����ͼʵ��";			//����ͼ����
	private String XTitle="��������";			//����ͼ�������Ŀ
	private String YTitle="��������";			//����ͼ�������Ŀ
	private int width=800;						//ͼƬ���
	private int height=450;						//ͼƬ�߶�
	private String fileName="d:\\temp.jpg";		//��ʱ�����ͼƬ���
	private boolean isV=true;					//�Ƿ�ֱ����ʾ
	private Color bgColor=Color.WHITE;			//ͼ�񱳾���ɫ
	private Color foreColor=Color.lightGray;	//�����ͼ���򱳾���ɫ
	private Color spaceLineColor=Color.white;	//�������ɫ

	FileOutputStream fosJpg = null;				//��ɵ���ͼ�������
	DefaultCategoryDataset defaultcategorydataset = new DefaultCategoryDataset();


	/**
	 * ��ȡ���൥һ����
	 * @return ��������
	 */
	public static synchronized DrawDotLine getInstance() {
		if (instance==null)
		instance=new DrawDotLine();
		return instance;
	}


	/**
	 * ����ͼ��������
	 * @param title ͼ��������
	 */
	public void setTitle(String title){
		this.title=title;
	}


	/**
	 * ����ͼ���ˮƽ������
	 * @param title ˮƽ������
	 */
	public void setXTitle(String title){
		this.XTitle=title;
	}


	/**
	 * ����ͼ��Ĵ�ֱ�������
	 * @param title ͼ��Ĵ�ֱ�������
	 */
	public void setYTitle(String title){
		this.YTitle=title;
	}


	/**
	 * ����ͼ��Ŀ��
	 * @param width ͼ��Ŀ��
	 */
	public void setWidth(int width){
		this.width=width;
	}


	/**
	 * ����ͼ��Ŀ��
	 * @param width ͼ��Ŀ��
	 */
	public void setHeight(int height){
		this.height=height;
	}





	/**
	 * ���ñ����ļ����
	 * @param fileName �����ļ����
	 */
	private void setFileName(String fileName){
		this.fileName=fileName;
	}


	/**
	 * ���û�ˮƽͼ���Ǵ�ֱͼ
	 * @param isV ˮƽͼ���Ǵ�ֱͼ
	 */
	public void setIsV(boolean isV){
		this.isV=isV;
	}


	/**
	 * ���ñ�����ɫ
	 * @param color ������ɫ
	 */
	public void setBgColor(Color color){
		this.bgColor=color;
	}
	/**
	 * ���ñ�����ɫ
	 * @param red   redɫ��ֵ
	 * @param green greenɫ��ֵ
	 * @param blue  blueɫ��ֵ
	 */
	public void setBgColor(int red,int green,int blue){
		this.bgColor=new Color(red,green,blue);
	}



	/**
	 * �ı䱳����ɫ
	 * @param str  ������ɫ���� ����:BLACK black blue Blue ��
	 */
	public void setBgcolor(String str){
		this.bgColor=ChangeColor.getColor(str);
	}


	/**
	 * ���û����ͼ����ı�����ɫ
	 * @param color �����ͼ����ı�����ɫ
	 */
	public void setForeColor(Color color){
		this.foreColor=color;
	}


	/**
	 * ���û����ͼ����ı�����ɫ
	 * @param red   redɫ��ֵ
	 * @param green greenɫ��ֵ
	 * @param blue  blueɫ��ֵ
	 */
	public void setForeColor(int red,int green,int blue){
		this.foreColor=new Color(red,green,blue);
	}


	/**
	 * ���û����ͼ����ı�����ɫ
	 * @param str  �����ͼ����ı�����ɫ ����:BLACK black blue Blue ��
	 */
	public void setForeColor(String str){
		this.foreColor=ChangeColor.getColor(str);
	}


	/**
	 * ���ü���ߵ���ɫ
	 * @param red   redɫ��ֵ
	 * @param green greenɫ��ֵ
	 * @param blue  blueɫ��ֵ
	 */
	public void setSpaceLineColor(int red,int green,int blue){
		this.spaceLineColor=new Color(red,green,blue);
	}


	/**
	 * ���ü���ߵ���ɫ
	 * @param color ����ߵ���ɫ
	 */
	public void setSpaceLineColor(Color color){
		this.spaceLineColor=color;
	}


	/**
	 * ���ü���ߵ���ɫ
	 * @param str  ����ߵ���ɫ ����:BLACK black blue Blue ��
	 */
	public void setSpaceLineColor(String str){
		this.spaceLineColor=ChangeColor.getColor(str);
	}


	/**
	 *��ʼ����ز���
	 */
	public void init(){
		setTitle("����ͼʵ��");
		setXTitle("��������");
		setYTitle("��������");
		setWidth(800);
		setHeight(450);
		setIsV(true);
		setBgColor(255,255,255);
		setFileName("d:\\temp.jpg");
		setForeColor(Color.lightGray);
		setSpaceLineColor(Color.white);
	}


	/**
	 *�ָ�����ʼ��״̬
	 */
	public void reset(){
		init();
	}


	/**
	 * ���Ҫ���Ƶ���ͼ�����
	 * @param typeName  ����������
	 * @param value     ���ֵ
	 * @param groupName �����
	 */
	public void addValue(String typeName,double value,String groupName){
		defaultcategorydataset.addValue(value,groupName,typeName);
	}


	/**
	 * ��ɵ���ͼ��
	 * @param fileName �����ļ���� �ļ����Ϊ��ʹ��·��Ϊ��: d:\\web\test.jpg
	 * @return �ɹ�:true ʧ��:false
	 */
        public boolean saveAbs(String fileName) {
          JFreeChart jfreechart = null;
          if (fileName != null) {
            this.setFileName(fileName);
          }
          if (isV == true) {
            jfreechart = ChartFactory.createLineChart(this.title, this.XTitle,
                                                      this.YTitle,
                                                      defaultcategorydataset,
                                                      PlotOrientation.VERTICAL, true, true, false);
          }
          else {
            jfreechart = ChartFactory.createLineChart(this.title, this.XTitle,
                                                      this.YTitle,
                                                      defaultcategorydataset,
                                                      PlotOrientation.HORIZONTAL, true, true, false);
          }

          jfreechart.setBackgroundPaint(this.bgColor);
          CategoryPlot categoryplot = (CategoryPlot) jfreechart.getPlot();
          categoryplot.setBackgroundPaint(this.foreColor);
          categoryplot.setRangeGridlinePaint(this.spaceLineColor);
          NumberAxis numberaxis = (NumberAxis) categoryplot.getRangeAxis();
          numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
          numberaxis.setAutoRangeIncludesZero(true);
          LineAndShapeRenderer lineandshaperenderer = (LineAndShapeRenderer)
              categoryplot.getRenderer();
          lineandshaperenderer.setShapesVisible(true);
          lineandshaperenderer.setSeriesStroke(0, new BasicStroke(2.0F, 1, 1, 1.0F, new float[] {
              10F, 6F
          }
              , 0.0F));
          lineandshaperenderer.setSeriesStroke(1, new BasicStroke(2.0F, 1, 1, 1.0F, new float[] {
              6F, 6F
          }
              , 0.0F));
          lineandshaperenderer.setSeriesStroke(2, new BasicStroke(2.0F, 1, 1, 1.0F, new float[] {
              2.0F, 6F
          }
              , 0.0F));
          FileOutputStream fosJpg = null;
          try {
            fosJpg = new FileOutputStream(fileName);
            ChartUtilities.writeChartAsJPEG(fosJpg, 100, jfreechart, this.width, this.height, null);

          }
          catch (Exception e) {
            return false;
          }
          finally {
            this.defaultcategorydataset.clear();
            try {
              fosJpg.close();
            }
            catch (Exception e) {}
          }
          return true;
        }



        /**
        * ������ɵĵ���ͼ
        * @param request   ��jspҳ���е�request���� ���ڻ�ȡ�ļ�·��
        * @param fileName   �����ļ���� �ļ���Ʊ���ʹ��վ��ľ��·�� �� : ��/admin/test.jpg"
        * @return true �ɹ�  false ʧ��
        */
        public boolean saveWebFile(HttpServletRequest request, String fileName) {
          JFreeChart jfreechart = null;
          if (fileName != null) {
            this.setFileName(fileName);
          }
          fileName = request.getRealPath("") + fileName;
          if (isV == true) {
            jfreechart = ChartFactory.createLineChart(this.title, this.XTitle,
                                                      this.YTitle,
                                                      defaultcategorydataset,
                                                      PlotOrientation.VERTICAL, true, true, false);
          }
          else {
            jfreechart = ChartFactory.createLineChart(this.title, this.XTitle,
                                                      this.YTitle,
                                                      defaultcategorydataset,
                                                      PlotOrientation.HORIZONTAL, true, true, false);
          }

          jfreechart.setBackgroundPaint(this.bgColor);
          CategoryPlot categoryplot = (CategoryPlot) jfreechart.getPlot();
          categoryplot.setBackgroundPaint(this.foreColor);
          categoryplot.setRangeGridlinePaint(this.spaceLineColor);
          NumberAxis numberaxis = (NumberAxis) categoryplot.getRangeAxis();
          numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
          numberaxis.setAutoRangeIncludesZero(true);
          LineAndShapeRenderer lineandshaperenderer = (LineAndShapeRenderer)
              categoryplot.getRenderer();
          lineandshaperenderer.setShapesVisible(true);
          lineandshaperenderer.setSeriesStroke(0, new BasicStroke(2.0F, 1, 1, 1.0F, new float[] {
              10F, 6F
          }
              , 0.0F));
          lineandshaperenderer.setSeriesStroke(1, new BasicStroke(2.0F, 1, 1, 1.0F, new float[] {
              6F, 6F
          }
              , 0.0F));
          lineandshaperenderer.setSeriesStroke(2, new BasicStroke(2.0F, 1, 1, 1.0F, new float[] {
              2.0F, 6F
          }
              , 0.0F));
          FileOutputStream fosJpg = null;
          try {
            fosJpg = new FileOutputStream(fileName);
            ChartUtilities.writeChartAsJPEG(fosJpg, 100, jfreechart, this.width, this.height, null);

          }
          catch (Exception e) {
            return false;
          }
          finally {
             this.defaultcategorydataset.clear();
            try {
              fosJpg.close();
            }
            catch (Exception e) {}
          }
          return true;

        }
	public static void main(String[] args) {

		//1.��ɻ����ͼ�Ķ���
		DrawDotLine drawDotLine =new  DrawDotLine();
		//2.��ӻ�ͼ�����
		drawDotLine.addValue("s1",2.0,"group1");
		drawDotLine.addValue("s2",3.0,"group1");
		drawDotLine.addValue("s3",3.0,"group1");
		drawDotLine.addValue("s4",2.0,"group1");
		drawDotLine.addValue("s5",1.0,"group1");
		drawDotLine.addValue("s6",4.0,"group1");
		drawDotLine.addValue("s7",3.0,"group1");
		drawDotLine.addValue("s8",2.0,"group1");
		drawDotLine.addValue("s9",5.0,"group1");
		drawDotLine.addValue("s10",1.0,"group1");

		drawDotLine.addValue("s1",3.0,"group2");
		drawDotLine.addValue("s2",1.0,"group2");
		drawDotLine.addValue("s3",5.0,"group2");
		drawDotLine.addValue("s4",1.0,"group2");
		drawDotLine.addValue("s5",3.0,"group2");
		drawDotLine.addValue("s6",2.0,"group2");
		drawDotLine.addValue("s7",5.0,"group2");
		drawDotLine.addValue("s8",2.0,"group2");
		drawDotLine.addValue("s9",1.0,"group2");
		drawDotLine.addValue("s10",4.0,"group2");
		//3.����ͼƬ����
		drawDotLine.init();
		drawDotLine.setTitle("����ͼ��ʾ");
		drawDotLine.setXTitle("����ͼ��������");
		drawDotLine.setYTitle("����ͼ��������");

		drawDotLine.setBgColor(Color.white);
		drawDotLine.setForeColor(Color.lightGray);
		drawDotLine.setSpaceLineColor(Color.white);
		drawDotLine.setWidth(800);
		drawDotLine.setHeight(450);
		//4.����ͼƬ
		drawDotLine.saveAbs("e:\\DrawDotLine.jpg");

	}
}
