package edu.ncut.vod.smart.jrsoft.chart;
import java.awt.Color;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.gantt.TaskSeriesCollection;
import javax.servlet.http.*;
/**
 * @author sychallenger
 *
 */
public class DrawGant
{

	private static DrawGant instance = null;	//������ͼ��ʵ��
	private String title="����ͼʵ��";			//����ͼ����
	private String XTitle="������";				//����ͼ�������Ŀ
	private String YTitle="����";				//����ͼ�������Ŀ
	private int width=800;						//ͼƬ���
	private int height=450;						//ͼƬ�߶�
	private String fileName="d:\\temp.jpg";		//��ʱ�����ͼƬ���
	private Color bgColor=Color.white;			//ͼƬ������ɫ
	private Color foreColor=Color.lightGray;	//ͼƬ������ɫ
	private Color spaceLineColor=Color.white;	//ͼƬ������ɫ

	private TaskList taskList=null;				//�������м�
	private TaskObject taskObject=null;			//�����������
	private TaskObject taskChildObject=null;	//���������
	FileOutputStream fosJpg = null;				//��ɸ���ͼ�������

	TaskSeriesCollection tc = new TaskSeriesCollection(); //�������м���


	/**
	 * ���ó�ʼ������
	 */
	public void init(){
		setTitle("����ͼ��ʾ");
		setXTitle("������");
		setYTitle("����");
		setWidth(800);
		setHeight(450);
		setFileName("d:\\temp.jpg");
		setBgColor(Color.white);
		setForeColor(Color.lightGray);
		setSpaceLineColor(Color.white);
	}


	/**
	 * �ָ�Ĭ��ֵ
	 */
	public void reset(){
		init();
	}


	/**
	 * ���ø���ͼ����
	 * @param ��ɵĸ���ͼ�ı���
	 */
	public void setTitle(String title){
		this.title=title;
	}


	/**
	 * ��ɸ���ͼ�ĺ�������
	 * @param xTitle ��������
	 */
	public void setXTitle(String xTitle){
		this.XTitle=xTitle;
	}


	/**
	 * ��ɸ���ͼ����������
	 * @param yTitle ��������
	 */
	public void setYTitle(String yTitle){
		this.YTitle=yTitle;
	}


	/**
	 * ��ɸ���ͼ�Ŀ��
	 * @param width	����ͼ�Ŀ��
	 */
	public void setWidth(int width){
		this.width=width;
	}


	/**
	 * ��ɸ���ͼ�ĸ߶�
	 * @param height����ͼ�ĸ߶�
	 */
	public void setHeight(int height){
		this.height=height;
	}


	/**
	 * ��ɸ���ͼ���ļ����
	 * @param fileName ����ͼ���ļ����
	 */
	private void setFileName(String fileName){
		this.fileName=fileName;
	}


	/**
	 * ��̬ģʽ��������
	 * @return �����һ�����
	 */

	public static synchronized DrawGant getInstance() {
		if (instance==null)
		instance=new DrawGant();
		return instance;
	}


	/**
	 * ���ñ�����ɫ
	 * @param color ������ɫ
	 */
	public void setBgColor(Color color){
		this.bgColor=color;
	}


	/**
	 * ���ñ�����ɫ
	 * @param red   redɫ��ֵ
	 * @param green greenɫ��ֵ
	 * @param blue  blueɫ��ֵ
	 */
	public void setBgColor(int red,int green,int blue){
		this.bgColor=new Color(red,green,blue);
	}


	/**
	 * �ı䱳����ɫ
	 * @param str  ������ɫ���� ����:BLACK black blue Blue ��
	 */
	public void setBgcolor(String str){
		this.bgColor=ChangeColor.getColor(str);
	}


	/**
	 * ���û����ͼ����ı�����ɫ
	 * @param color �����ͼ����ı�����ɫ
	 */
	public void setForeColor(Color color){
		this.foreColor=color;
	}


	/**
	 * ���û����ͼ����ı�����ɫ
	 * @param red   redɫ��ֵ
	 * @param green greenɫ��ֵ
	 * @param blue  blueɫ��ֵ
	 */
	public void setForeColor(int red,int green,int blue){
		this.foreColor=new Color(red,green,blue);
	}

	/**
	 * ���û����ͼ����ı�����ɫ
	 * @param str  �����ͼ����ı�����ɫ ����:BLACK black blue Blue ��
	 */
	public void setForeColor(String str){
		this.foreColor=ChangeColor.getColor(str);
	}


	/**
	 * ���û����ͼ����ߵ���ɫ
	 * @param red   redɫ��ֵ
	 * @param green greenɫ��ֵ
	 * @param blue  blueɫ��ֵ
	 */
	public void setSpaceLineColor(int red,int green,int blue){
		this.spaceLineColor=new Color(red,green,blue);
	}


	/**
	 * ���ü���ߵ���ɫ
	 * @param color ����ߵ���ɫ
	 */
	public void setSpaceLineColor(Color color){
		this.spaceLineColor=color;
	}

	/**
	 * ���ü���ߵ���ɫ
	 * @param str  ����ߵ���ɫ ����:BLACK black blue Blue ��
	 */
	public void setSpaceLineColor(String str){
		this.spaceLineColor=ChangeColor.getColor(str);
	}


	/**
	 * �������
	 * @param i ��
	 * @param j ��
	 * @param k ��
	 * @return ���� Date����
	 */
	public static Date date(int i, int j, int k)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(k, j, i);
        Date date1 = calendar.getTime();
        return date1;
    }
	/**
	 * ��������
	 * @param str �������
	 */
	public void setTaskList(String str){
		if(this.taskList!=null){
			this.taskList=null;
		}
		this.taskList=new TaskList(str);
	}
	/**
	 * �����������
	 * @param str       �������
	 * @param startDate ����ʼʱ��
	 * @param endDate   �������ʱ��
	 * @param value     ������ɰٷֱ� (��0.8��ʾ�����80%)
	 */
	public void setTaskObject(String str,Date startDate,Date endDate,double value){
		if(this.taskObject!=null){
			this.taskObject=null;
		}
		this.taskObject=new TaskObject(str,startDate, endDate);
		this.taskObject.setPercentComplete(value);
	}
	/**
	 * ��ǰ����������������
	 * @param str		���������
	 * @param startDate ������ʼʱ��
	 * @param endDate   ���������ʱ��
	 * @param value		��������ɰٷֱ� (��0.8��ʾ�����80%)
	 */
	public void setChildTask(String str ,Date startDate ,Date endDate, double value){
		if(this.taskChildObject!=null){
			this.taskChildObject=null;
		}
		this.taskChildObject=new TaskObject(str,startDate,endDate);
		this.taskChildObject.setPercentComplete(value);
	}
	/**
	 * �����������
	 * @param str       �������
	 * @param startDate ����ʼʱ��
	 * @param endDate   �������ʱ��
	 */
	public void setTaskObject(String str,Date startDate,Date endDate){
		if(this.taskObject!=null){
			this.taskObject=null;
		}
		this.taskObject=new TaskObject(str,startDate, endDate);

	}
	/**
	 * ��ǰ����������������
	 * @param str		���������
	 * @param startDate ������ʼʱ��
	 * @param endDate   ���������ʱ��
	 */
	public void setChildTask(String str ,Date startDate ,Date endDate){
		if(this.taskChildObject!=null){
			this.taskChildObject=null;
		}
		this.taskChildObject=new TaskObject(str,startDate,endDate);
	}
	/**
	 * ���Ҫ�����ͼ�������б�
	 */
	public void addTaskList(){
		tc.add(taskList.getTaskSeries());
	}

	/**
	 * ����������
	 * @param str       �������
	 * @param startDate ����ʼʱ��
	 * @param endDate   �������ʱ��
	 * @param value     ������ɰٷֱ� (��0.8��ʾ�����80%)
	 * @return �ɹ� true ʧ�� false
	 */

	public boolean addTask(String str,Date startDate,Date endDate,double value){
		if(this.taskList==null){
			return false;
		}
		this.setTaskObject(str,startDate,endDate,value);
		this.taskList.add(this.taskObject);
		return true;
	}
	/**
	 * ����������
	 * @param str       �������
	 * @param startDate ����ʼʱ��
	 * @param endDate   �������ʱ��
	 * @return �ɹ� true ʧ�� false
	 */
	public boolean addTask(String str,Date startDate,Date endDate){
		if(this.taskList==null){
			return false;
		}
		this.setTaskObject(str,startDate,endDate);
		this.taskList.add(this.taskObject);
		return true;
	}
	/**
	 * ��ǰ����������������
	 * @param str		���������
	 * @param startDate ������ʼʱ��
	 * @param endDate   ���������ʱ��
	 * @param value		��������ɰٷֱ� (��0.8��ʾ�����80%)
	 */
	public boolean addChildTask(String str,Date startDate,Date endDate,double value){
		if(this.taskObject==null){
			return false;
		}
		this.setChildTask(str,startDate,endDate,value);
		this.taskObject.addSubtask(this.taskChildObject);
		return true;
	}
	/**
	 * ��ǰ����������������
	 * @param str		���������
	 * @param startDate ������ʼʱ��
	 * @param endDate   ���������ʱ��
	 */
	public boolean addChildTask(String str,Date startDate,Date endDate){
		if(this.taskObject==null){
			return false;
		}
		this.setChildTask(str,startDate,endDate);
		this.taskObject.addSubtask(this.taskChildObject);
		return true;
	}
	public boolean remove(TaskList taskList){
		try{
			tc.remove(taskList.getTaskSeries());
		}catch(Exception e){
			return false;
		}
		return true;
	}


	/**
	 * ��ɸ���ͼ
	 * @param fileName  �������ͼ���ļ����  �ļ����Ϊ��ʹ��·��Ϊ��: d:\\web\test.jpg
	 * @return �ɹ�:true ʧ��:false
	 */
        public boolean saveAbs(String fileName) {
          if (fileName != null) {
            this.setFileName(fileName);
          }

          JFreeChart chart = ChartFactory.createGanttChart(this.title, this.YTitle,
              this.XTitle, tc, true, true, false);
          chart.setBackgroundPaint(this.bgColor); //���ñ�����ɫ
          CategoryPlot categoryplot = (CategoryPlot) chart.getPlot();
          categoryplot.setBackgroundPaint(this.foreColor);
          categoryplot.setRangeGridlinePaint(this.spaceLineColor);
          categoryplot.getDomainAxis().setMaximumCategoryLabelWidthRatio(10F);
          categoryplot.getDomainAxis().setMaximumCategoryLabelLines(1);
          categoryplot.getDomainAxis().setCategoryMargin(0.1);

          CategoryItemRenderer categoryitemrenderer = categoryplot.getRenderer();
          categoryitemrenderer.setSeriesPaint(0, Color.blue);
          try {
            fosJpg = new FileOutputStream(fileName);
            ChartUtilities.writeChartAsJPEG(fosJpg, 100, chart, this.width,
                                            this.height, null);

          }
          catch (Exception e) {}
          finally {
            this.tc.removeAll();
            try {
              fosJpg.close();
            }
            catch (Exception e) {}
          }
          return true;
        }


        /**
        * ������ɵĸ���ͼ
        * @param request   ��jspҳ���е�request���� ���ڻ�ȡ�ļ�·��
        * @param fileName   �����ļ���� �ļ���Ʊ���ʹ��վ��ľ��·�� �� : ��/admin/test.jpg"
        * @return true �ɹ�  false ʧ��
        */
        public boolean saveWebFile(HttpServletRequest request, String fileName) {
          if (fileName != null) {
            this.setFileName(fileName);
          }
           fileName = request.getRealPath("") + fileName;
          JFreeChart chart = ChartFactory.createGanttChart(this.title, this.YTitle,
              this.XTitle, tc, true, true, false);
          chart.setBackgroundPaint(this.bgColor); //���ñ�����ɫ
          CategoryPlot categoryplot = (CategoryPlot) chart.getPlot();
          categoryplot.setBackgroundPaint(this.foreColor);
          categoryplot.setRangeGridlinePaint(this.spaceLineColor);
          categoryplot.getDomainAxis().setMaximumCategoryLabelWidthRatio(10F);
          categoryplot.getDomainAxis().setMaximumCategoryLabelLines(1);
          categoryplot.getDomainAxis().setCategoryMargin(0.1);

          CategoryItemRenderer categoryitemrenderer = categoryplot.getRenderer();
          categoryitemrenderer.setSeriesPaint(0, Color.blue);
          try {
            fosJpg = new FileOutputStream(fileName);
            ChartUtilities.writeChartAsJPEG(fosJpg, 100, chart, this.width,
                                            this.height, null);

          }
          catch (Exception e) {}
          finally {
            this.tc.removeAll();
            try {
              fosJpg.close();
            }
            catch (Exception e) {}
          }
          return true;

        }

    public static void main(String args[])
    {
    	//1.��ɻ����ͼ�������
    	DrawGant gant=DrawGant.getInstance();
    	//2.�������
    	gant.setTaskList("��������1");
    	gant.addTask("����1",DrawGant.date(1,4,2005),DrawGant.date(3,4,2005),0.8);
    	gant.addTask("����2",DrawGant.date(3,4,2005),DrawGant.date(5,4,2005),1.0);
    	gant.addTask("����3",DrawGant.date(5,4,2005),DrawGant.date(8,4,2005),0.8);
    	gant.addChildTask("������1",DrawGant.date(5,4,2005),DrawGant.date(6,4,2005),0.8);
    	gant.addChildTask("������2",DrawGant.date(7,4,2005),DrawGant.date(8,4,2005),0.8);

    	gant.addTaskList();

    	gant.setTaskList("��������2");
    	gant.addTask("����11",DrawGant.date(1,4,2005),DrawGant.date(3,4,2005),0.8);
    	gant.addTask("����22",DrawGant.date(3,4,2005),DrawGant.date(5,4,2005),1.0);
    	gant.addTask("����33",DrawGant.date(5,4,2005),DrawGant.date(8,4,2005),0.8);
    	gant.addChildTask("������11",DrawGant.date(5,4,2005),DrawGant.date(6,4,2005),0.8);
    	gant.addChildTask("������22",DrawGant.date(7,4,2005),DrawGant.date(8,4,2005),0.8);
      	gant.addTaskList();
      	//3.����ͼƬ����
    	gant.init();
    	gant.setBgColor(Color.white);
    	gant.setForeColor(Color.lightGray);
    	gant.setSpaceLineColor(Color.white);
    	gant.setHeight(800);
    	gant.setHeight(450);
    	gant.setTitle("����ͼ��ʾ");
    	gant.setXTitle("����ͼ��������");
    	gant.setYTitle("����ͼ��������");
    	//4.����ͼƬ
    	gant.saveAbs("e:\\gant.jpg");

	}
}


