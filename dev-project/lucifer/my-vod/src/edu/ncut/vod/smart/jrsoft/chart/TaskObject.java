package edu.ncut.vod.smart.jrsoft.chart;

import java.util.Date;

import org.jfree.data.gantt.Task;

/**
 * @author sychallenger
 */
public class TaskObject {
	private  Task task=null;
	/**
	 * ��ȡһ������
	 * @return ����
	 */
	public Task getTask(){
		return this.task;
	}
	/**
	 * ����һ�������������
	 * @param taskName �������
	 * @param startDate ����ʼʱ��
	 * @param endDate   �������ʱ��
	 */
	public TaskObject(String taskName,Date startDate,Date endDate){
		this.task=new Task(taskName,startDate,endDate);
	}
	/**
	 * �Ը�����������������
	 * @param taskObject
	 */
	public void addSubtask(TaskObject taskObject){
		this.task.addSubtask(taskObject.task);
	}
	/**
	 * ���ø�������������� 0--1��Χ��
	 * @param value ���������
	 */
	public void setPercentComplete(double value){
		this.task.setPercentComplete(value);
	}
}
