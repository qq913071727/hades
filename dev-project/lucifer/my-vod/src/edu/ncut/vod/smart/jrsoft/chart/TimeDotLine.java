package edu.ncut.vod.smart.jrsoft.chart;

import java.awt.Color;
import java.io.FileOutputStream;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.RectangleInsets;
import javax.servlet.http.*;
/**
 * @author oracle
 *
 * TODO ��ʱ�仭����ͼ
 * ������:
 * 1.��������
 * 2.��ӻ�ͼ����ݼ�
 * 3.����ͼƬ�������
 * 4.���ͼƬ
 * 5.��ȡ����ʾͼƬ
 */
public class TimeDotLine {
	private static TimeDotLine instance = null;	//��ʱ��˳������ͼ��ʵ��
	private String title="ʱ�����ͼʵ��";			//��ʱ��˳������ͼ�ı���
	private String XTitle="��������";			//��ʱ��˳������ͼ�ĺ������Ŀ
	private String YTitle="��������";			//��ʱ��˳������ͼ���������Ŀ
	private int width=800;						//ͼƬ���
	private int height=450;						//ͼƬ�߶�
	private String fileName="d:\\TimeDotLine.jpg";		//��ʱ�����ͼƬ���
	private Color bgColor=Color.WHITE;			//ͼ�񱳾���ɫ
	private Color foreColor=Color.lightGray;	//��ʱ��˳������ͼ�����򱳾���ɫ
	private Color spaceLineColor=Color.white;	//�������ɫ


	private TimeSeriesCollection timesc=null;	//��ʱ������ͼ��ʱ�伯��
	private TimeList timeList=null;
	private JFreeChart jfreechart=null;
	private XYPlot xyplot=null;


	/**
	 * ��ȡ���൥һ����
	 * @return ��������
	 */
	public static synchronized TimeDotLine getInstance() {
		if (instance==null)
		instance=new TimeDotLine();
		return instance;
	}
	/**
	 * ����ʱ�伯
	 * @param str ʱ�伯���
	 * @return �ɹ� true ʧ�� false
	 */
	public boolean setTimeList(String str){
		if(this.timeList!=null){
			this.timeList=null;
		}
		this.timeList=new TimeList(str);
		return true;
	}
	/**
	 * ��ӵ���Ϣ
	 * @param year   //��
	 * @param month  //��
	 * @param date   //��
	 * @param hour   //Сʱ
	 * @param minute //��
	 * @param value  //ֵ
	 * @return �ɹ� true ʧ�� false
	 */
	public boolean add(int year,int month,int date,int hour,int minute,double value){
		if(this.timeList==null){
			return false;
		}
		this.timeList.add(year,month,date,hour,minute,value);
		return true;
	}
	/**
	 * ��ӵ���Ϣ
	 * @param year   //��
	 * @param month  //��
	 * @param date   //��
	 * @param value  //ֵ
	 * @return �ɹ� true ʧ�� false
	 */
	public boolean add(int year,int month,int date,double value){
		if(this.timeList==null){
			this.timeList=null;
		}
		this.timeList.add(year,month,date,00,00,value);
		return true;
	}
	/**
	 * ���ʱ�伯
	 * @return  �ɹ� true ʧ�� false
	 */
	public boolean addTimeList(){
		if(this.timeList==null){
			return false;
		}
		if(this.timesc==null){
			this.timesc = new TimeSeriesCollection();
		}
		try{
			this.timesc.addSeries(this.timeList.getTimeSeries());
		}
		catch(Exception e){
			System.out.print(e);
		}

		return true;
	}
	/**
	 * ����ͼ��������
	 * @param title ͼ��������
	 */
	public void setTitle(String title){
		this.title=title;
	}


	/**
	 * ����ͼ���ˮƽ������
	 * @param title ˮƽ������
	 */
	public void setXTitle(String title){
		this.XTitle=title;
	}


	/**
	 * ����ͼ��Ĵ�ֱ�������
	 * @param title ͼ��Ĵ�ֱ�������
	 */
	public void setYTitle(String title){
		this.YTitle=title;
	}


	/**
	 * ����ͼ��Ŀ��
	 * @param width ͼ��Ŀ��
	 */
	public void setWidth(int width){
		this.width=width;
	}


	/**
	 * ����ͼ��Ŀ��
	 * @param width ͼ��Ŀ��
	 */
	public void setHeight(int width){
		this.width=width;
	}


	/**
	 * ����ͼ��߶�
	 * @param height
	 */
	public void setXTitle(int height){
		this.height=height;
	}


	/**
	 * ���ñ����ļ����
	 * @param fileName �����ļ����
	 */
	public void setFileName(String fileName){
		this.fileName=fileName;
	}



	/**
	 * ���ñ�����ɫ
	 * @param color ������ɫ
	 */
	public void setBgColor(Color color){
		this.bgColor=color;
	}


	/**
	 * ���ñ�����ɫ
	 * @param red   redɫ��ֵ
	 * @param green greenɫ��ֵ
	 * @param blue  blueɫ��ֵ
	 */
	public void setBgColor(int red,int green,int blue){
		this.bgColor=new Color(red,green,blue);
	}


	/**
	 * ���ð�ʱ��˳������ͼ������ı�����ɫ
	 * @param color ��ʱ��˳������ͼ����ı�����ɫ
	 */
	public void setForeColor(Color color){
		this.foreColor=color;
	}
	/**
	 * ���ð�ʱ��˳������ͼ����ı�����ɫ
	 * @param red   redɫ��ֵ
	 * @param green greenɫ��ֵ
	 * @param blue  blueɫ��ֵ
	 */


	public void setForeColor(int red,int green,int blue){
		this.foreColor=new Color(red,green,blue);
	}
	/**
	 * ���ð�ʱ��˳������ͼ����ı�����ɫ
	 * @param red   redɫ��ֵ
	 * @param green greenɫ��ֵ
	 * @param blue  blueɫ��ֵ
	 */


	public void setSpaceLineColor(int red,int green,int blue){
		this.spaceLineColor=new Color(red,green,blue);
	}


	/**
	 * ���ü���ߵ���ɫ
	 * @param color ����ߵ���ɫ
	 */
	public void setSpaceLineColor(Color color){
		this.spaceLineColor=color;
	}


	/**
	 *��ʼ����ز���
	 */
        public void init() {
          setTitle("��ʱ��˳������ͼ��ʵ��");
          setXTitle("��������");
          setYTitle("��������");
          setWidth(800);
          setHeight(450);
          setBgColor(255, 255, 255);
          setFileName("d:\\temp.jpg");
          setForeColor(Color.lightGray);
          setSpaceLineColor(Color.white);
        }


	/**
	 *�ָ�����ʼ��״̬
	 */
	public void reset(){
		init();
	}


	/**
	 * ��Ӱ�ʱ��˳������ͼ��ʱ�伯
	 * @param timeList ��ʱ��˳������ͼ��ʱ�伯
	 */
	public void addTimeList(TimeList timeList){
		if(this.timesc==null){
			this.timesc = new TimeSeriesCollection();
		}
		this.timesc.addSeries(this.timeList.getTimeSeries());
	}

	/**
	 * ��ɰ�ʱ��˳�������ͼ
	 * @param fileName �����ļ���� �ļ����Ϊ��ʹ��·��Ϊ��: d:\\web\test.jpg
         * @return true �ɹ�  false ʧ��
	 */
        public boolean saveAbs(String fileName) {
          if (fileName != null) {
            this.fileName = fileName;
          }
          this.jfreechart = ChartFactory.createTimeSeriesChart(this.title,
              this.XTitle,
              YTitle, this.timesc, true, true, false);
          this.jfreechart.setBackgroundPaint(this.bgColor);
          this.xyplot = (XYPlot) jfreechart.getPlot();
          this.xyplot.setBackgroundPaint(this.foreColor);
          this.xyplot.setDomainGridlinePaint(this.spaceLineColor);
          this.xyplot.setRangeGridlinePaint(Color.white);
          this.xyplot.setAxisOffset(new RectangleInsets(5D, 5D, 5D, 5D));
          this.xyplot.setDomainCrosshairVisible(true);
          this.xyplot.setRangeCrosshairVisible(true);
          org.jfree.chart.renderer.xy.XYItemRenderer xyitemrenderer = xyplot
              .getRenderer();
          if (xyitemrenderer instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer)
                xyitemrenderer;
            xylineandshaperenderer.setDefaultShapesVisible(true);
            xylineandshaperenderer.setDefaultShapesFilled(true);
          }
          DateAxis dateaxis = (DateAxis) xyplot.getDomainAxis();
          FileOutputStream fosJpg = null;
          try {
            fosJpg = new FileOutputStream(fileName);
            ChartUtilities.writeChartAsJPEG(fosJpg, 100, this.jfreechart, this.width,
                                            this.height, null);

          }
          catch (Exception e) {
            return false;
          }
          finally {
            this.timesc.removeAllSeries();
            try {
              fosJpg.close();
            }
            catch (Exception e) {
              return false;
            }

          }
          return true;
        }


        /**
        * ��ɰ�ʱ��˳�������ͼ
        * @param request   ��jspҳ���е�request���� ���ڻ�ȡ�ļ�·��
        * @param fileName   �����ļ���� �ļ���Ʊ���ʹ��վ��ľ��·�� �� : ��/admin/test.jpg"
        * @return true �ɹ�  false ʧ��
        */

        public boolean saveWebFile(HttpServletRequest request, String fileName) {
          if (fileName != null) {
            this.fileName = fileName;
          }
          //fileName=fileName.replaceFirst("\\","/");
          fileName = request.getRealPath("") + fileName;
          this.jfreechart = ChartFactory.createTimeSeriesChart(this.title,
              this.XTitle,
              YTitle, this.timesc, true, true, false);
          this.jfreechart.setBackgroundPaint(this.bgColor);
          this.xyplot = (XYPlot) jfreechart.getPlot();
          this.xyplot.setBackgroundPaint(this.foreColor);
          this.xyplot.setDomainGridlinePaint(this.spaceLineColor);
          this.xyplot.setRangeGridlinePaint(Color.white);
          this.xyplot.setAxisOffset(new RectangleInsets(5D, 5D, 5D, 5D));
          this.xyplot.setDomainCrosshairVisible(true);
          this.xyplot.setRangeCrosshairVisible(true);
          org.jfree.chart.renderer.xy.XYItemRenderer xyitemrenderer = xyplot
              .getRenderer();
          if (xyitemrenderer instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer)
                xyitemrenderer;
            xylineandshaperenderer.setDefaultShapesVisible(true);
            xylineandshaperenderer.setDefaultShapesFilled(true);
          }
          DateAxis dateaxis = (DateAxis) xyplot.getDomainAxis();
          FileOutputStream fosJpg = null;
          try {
            fosJpg = new FileOutputStream(fileName);
            ChartUtilities.writeChartAsJPEG(fosJpg, 100, this.jfreechart, this.width,
                                            this.height, null);

          }
          catch (Exception e) {
            return false;
          }
          finally {
            this.timesc.removeAllSeries();
            try {
              fosJpg.close();
            }
            catch (Exception e) {
              return false;
            }
          }
          return true;
        }
	public static void main(String args[]) {
		//1.��ɻ�ʱ�����ͼ�������
		TimeDotLine test=new TimeDotLine();
		//2.������
		//A ���ʱ�伯һ
		test.setTimeList("ʱ������1");
		test.add(2005,2,3,4);
		test.add(2005,2,4,2);
		test.add(2005,2,5,4);
		test.add(2005,2,6,3);
		test.add(2005,2,7,2);
		test.add(2005,2,8,1);
		test.add(2005,2,9,4);
		test.add(2005,2,10,1);
		test.add(2005,2,11,3);
		//B ���ʱ�伯һ
		test.addTimeList();

		test.setTimeList("ʱ������2");
		//test.add(2005,2,3,1);
                test.add(2005,2,3,4);
		test.add(2005,2,4,3);
		test.add(2005,2,5,2);
		test.add(2005,2,6,4);
		test.add(2005,2,7,3);
		test.add(2005,2,8,2);
		test.add(2005,2,9,3);
		test.add(2005,2,10,4);
		test.add(2005,2,11,2);
		test.addTimeList();

		//3.����ͼƬ����
		test.setBgColor(Color.WHITE);
		test.setWidth(800);
		test.setHeight(500);
		test.setForeColor(221,221,221);
		test.setTitle("��ʱ��˳������ͼ");
		test.setXTitle("��������");
		test.setYTitle("��������");
		//4.��ͼƬ
		test.saveAbs("e:\\TimeDotLine.jpg");

	}
}
