package edu.ncut.vod.exception;

public class ExamException extends Exception {

    private static final long serialVersionUID = -8402970906118596014L;

    public ExamException() {
    }

    public ExamException(String x) {
        super(x);
    }
}
