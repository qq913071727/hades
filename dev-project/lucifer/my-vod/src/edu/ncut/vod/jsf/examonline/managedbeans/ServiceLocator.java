package edu.ncut.vod.jsf.examonline.managedbeans;

import edu.ncut.vod.hibernate.service.ExamService;
import edu.ncut.vod.hibernate.service.StudentService;

public interface ServiceLocator {
    
    /**
     * get instance of object 'ExamService'
     * @return
     */
    public ExamService getExamService();
    
    /**
     * get instance of object 'StudentService'
     * @return
     */
    public StudentService getStudentService();
}
