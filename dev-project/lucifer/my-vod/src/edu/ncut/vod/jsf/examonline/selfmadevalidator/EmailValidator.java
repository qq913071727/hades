package edu.ncut.vod.jsf.examonline.selfmadevalidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.sun.faces.util.MessageFactory;

import edu.ncut.vod.jsf.examonline.managedbeans.AddStudentBean;

public class EmailValidator implements Validator {

    /**
     * validate field 'Email':it must be the format of e-mail
     */
    public void validate(FacesContext context, UIComponent component,
            Object obj) throws ValidatorException {
        debug("method validate in class EmailValidator is called");

        String label = ((HtmlInputText) component).getLabel();

        if ("Email".equals(label)) {
            String fieldValue = (String) obj; //get the value of field which needs validating

            // validate field 'Email':it must be the format of e-mail
            Pattern pattern = Pattern.compile("(\\w)*@([a-zA-Z]*(\\.))*[a-zA-Z]*");
            Matcher matcher = pattern.matcher(fieldValue);
            boolean b = matcher.matches();

            if (!b) {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                FacesMessage msg = MessageFactory.getMessage(facesContext,
                        "EmailContent_mustBeFormatOfEmail",
                        "EmailContent_mustBeFormatOfEmail");
                msg.setSeverity(FacesMessage.SEVERITY_WARN);
                facesContext.addMessage("addStudent:Email", msg);
                setErrorMessageVisible();
                throw new ValidatorException(msg);
            }else{
				setErrorMessageInvisible();
			}
        }
    }

    /**
     * set error message tag of tag 'h:inputText' visible
     */
    public void setErrorMessageVisible(){
        debug("method setErrorMessageVisible in class EmailValidator is called");

        FacesContext context =FacesContext.getCurrentInstance();
        Application application=context.getApplication();
        AddStudentBean addStudentBean=(AddStudentBean)application.evaluateExpressionGet(context,"#{addStudentBean}",AddStudentBean.class);

        addStudentBean.setEmailNotice(true);
        addStudentBean.setEmailMessage(true);
    }

    /**
	 * set error message tag of tag 'h:inputText' invisible
	 */
    public void setErrorMessageInvisible(){
		debug("method setErrorMessageInvisible in class EmailValidator is called");

		FacesContext context =FacesContext.getCurrentInstance();
        Application application=context.getApplication();
        AddStudentBean addStudentBean=(AddStudentBean)application.evaluateExpressionGet(context,"#{addStudentBean}",AddStudentBean.class);

        addStudentBean.setEmailNotice(false);
        addStudentBean.setEmailMessage(false);
	}

    /**
     * print message on console
     * @param message
     */
    public void debug(String message) {
        System.out.println(message);
    }
}
