package edu.ncut.vod.jsf.examonline.managedbeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlInputText;

import edu.ncut.vod.exception.ExamException;
import edu.ncut.vod.hibernate.model.Student;
import edu.ncut.vod.jsf.examonline.utils.PagingInfo;

public class ListStudentBean extends BaseBean {

    private static final long serialVersionUID = -9079618048294366809L;

    /**
     * show researched data in table 'student'
     */
    private List<Student> studentList;

    public List<Student> getStudentList() {
        debug("method getStudentList in class ListStudent is called");

        if (null == studentList) {
            studentList = new ArrayList<Student>();
        }
        if (null == pageNo) {
            pageNo = new String("1");
        }
        if (null == pageNo || pageNo.trim().equals("")){
            pageNo = "1";
        }
        if(null == pageCount){
            pageCount = new Integer(0);
        }
        pagingInfo = getPagingInfo();

        try {
            studentList = serviceLocator.getExamService().listStudent(Integer.parseInt(pageNo));
            debug(studentList.toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (ExamException e) {
            e.printStackTrace();
        }
        //reset(true);
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    /**
     * the count of student
     */
    int studentCount = 0;

    public int getStudentCount() {
        try {
            studentCount = serviceLocator.getExamService().getStudentCount();
            if (studentCount < 1) {
                parameter.put("msg", "????????");
                return studentCount;
            }
        } catch (ExamException e) {
            e.printStackTrace();
        }
        return studentCount;
    }

    public void setStudentCount(int studentCount) {
        this.studentCount = studentCount;
    }

    /**
     * the count of page
     */
    private Integer pageCount;

    public Integer getPageCount() {
        pageCount = serviceLocator.getExamService().getPageCount(getStudentCount() , serviceLocator.getExamService().STUDENT_PAGE_SIZE);
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    /**
     * the number of page
     */
    private String pageNo;

    public String getPageNo() {
        return pageNo;
    }

    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

    /**
     * current page
     */
    private String currentPage;

    public String getCurrentPage() {
        debug("method getCurrentPage in class ListStudent is called");

        if(null == currentPage){
            currentPage = new String();
        }
        //?????????????
        if (Integer.valueOf(pageNo) > getPageCount())
        {
            pageNo = String.valueOf(getPageCount());
        }
        currentPage = pageNo;

        debug("pageCount is"+pageCount);
        debug("pageNo is"+pageNo);
        debug("currentPage is"+currentPage);
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * act as attribute which is used in jsp file
     */
    Map<String, Object> parameter;

    public Map<String, Object> getParameter() {
        return parameter;
    }

    public void setParameter(Map<String, Object> parameter) {
        this.parameter = parameter;
    }

    /**
     *
     */
    private PagingInfo pagingInfo = new PagingInfo();

    public PagingInfo getPagingInfo() {
        if (pagingInfo.getItemCount() == -1) {
            Integer count;
            try {
                count = serviceLocator.getExamService().getStudentCount();
                pagingInfo.setItemCount(count.intValue());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (ExamException e) {
                e.printStackTrace();
            }
        }
        return pagingInfo;
    }

    public void setPagingInfo(PagingInfo pagingInfo) {
        this.pagingInfo = pagingInfo;
    }

    /**
     *
     */
    private HtmlInputText toPage;

    public HtmlInputText getToPage() {
        return toPage;
    }

    public void setToPage(HtmlInputText toPage) {
        this.toPage = toPage;
    }

    /**
     *
     * @return
     */
    public String next() {
        reset(false);
        getPagingInfo().nextPage();
        return "";
    }

    public String first() {
        reset(true);
        return "";
    }

    public String last() {
        reset(false);
        getPagingInfo().lastPage();
        return "";
    }

    private void reset(boolean resetFirstItem) {
        studentList = null;
        pagingInfo.setItemCount(-1);
        if (resetFirstItem) {
            pagingInfo.setFirstItem(0);
        }
    }

    public String prev() {
        reset(false);
        getPagingInfo().previousPage();
        return "";
    }
}
