package edu.ncut.vod.jsf.examonline.managedbeans;

import javax.servlet.ServletContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import edu.ncut.vod.hibernate.service.ExamService;
import edu.ncut.vod.hibernate.service.StudentService;
import edu.ncut.vod.jsf.examonline.utils.FacesUtils;

public class ServiceLocatorBean extends BaseBean implements ServiceLocator {

    private static final long serialVersionUID = -4701668733699976262L;

    /**
     * the exam service bean name
     */
    private static final String EXAM_SERVICE_BEAN_NAME = "examService";
    
    /**
     * the student service bean name
     */
    private static final String STUDENT_SERVICE_BEAN_NAME = "studentService";
    
    /**
     * the logger for this class
     */
    private Log logger = LogFactory.getLog(this.getClass());
    
    /**
     * the Spring application context
     */
    private ApplicationContext appContext;
    
    /**
     * the cached exam service
     */
    private ExamService examService;

    public ExamService getExamService() {
        if(null == examService){
            debug("examService is null");
        }
        return examService;
    }

    public void setExamService(ExamService examService) {
        this.examService = examService;
    }
    
    /**
     * the cached student service
     */
    private StudentService studentService;    

    public StudentService getStudentService() {
        return studentService;
    }

    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    /**
     * Constructor.
     * <p>
     * The following steps being done:
     * <ul>
     * <li>retrieve Spring application context from servlet context.
     * <li>look up <code>CatalogService</code> from Spring application context.
     * <li>look up <code>UserService</code> from Spring applicatino context.
     * </ul>
     */
    public ServiceLocatorBean() {
        ServletContext context = FacesUtils.getServletContext();
        this.appContext = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
        this.examService = (ExamService)this.lookupService(EXAM_SERVICE_BEAN_NAME);
        this.studentService = (StudentService)this.lookupService(STUDENT_SERVICE_BEAN_NAME);
        
        this.logger.info("Service locator bean is initialized");
    }
    
    /**
     * Initializes the ApplicationBean.load all the categories.
     * @see BaseBean#init()
     */
    protected void init() {             
        this.logger.debug("Application bean is initialized");
    }
    
    /**
     * Lookup service based on service bean name.
     * @param serviceBeanName the service bean name
     * @return the service bean
     */
    public Object lookupService(String serviceBeanName) {
        return appContext.getBean(serviceBeanName);
    }
    
    /*
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }
            
}
