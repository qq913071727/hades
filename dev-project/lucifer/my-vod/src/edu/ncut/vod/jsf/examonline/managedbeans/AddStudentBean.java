package edu.ncut.vod.jsf.examonline.managedbeans;

import javax.faces.component.html.HtmlInputText;
import javax.faces.event.ActionEvent;

import edu.ncut.vod.exception.ExamException;

/**
 * public interface PhaseListener extends java.util.EventListener, java.io.Serializable:
 * An interface implemented by objects that wish to be notified at the beginning and ending of
 * processing for each standard phase of the request processing lifecycle.
 */
public class AddStudentBean extends BaseBean {

    private static final long serialVersionUID = -1358514672946286722L;

    /**
     * binding with jsf tag whose id is 'showNote'
     */
    protected boolean showNote;

    public boolean isShowNote() {
		if(showNote == true){//when jsf tag whose id is 'showNote' is selected ,execute the following
			//show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'identificationNumber'
			setIdentificationNumberNotice(false);
			//show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'name'
			setNameNotice(false);
			//show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'numberOfStudent'
			setNumberOfStudentNotice(false);
			//show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'class'
			setClassNotice(false);
			//show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'address'
			setAddressNotice(false);
			//show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'telephone'
			setTelephoneNotice(false);
			//show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'Email'
			setEmailNotice(false);
			//show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'humanId'
			setHumanIdNotice(false);
		}else{//when jsf tag whose id is 'showNote' is not selected ,execute the following
			//do not show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'identificationNumber'
			setIdentificationNumberNotice(true);
			//do not show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'name'
			setNameNotice(true);
			//do not show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'numberOfStudent'
			setNumberOfStudentNotice(true);
			//do not show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'class'
			setClassNotice(true);
			//do not show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'address'
			setAddressNotice(true);
			//do not show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'telephone'
			setTelephoneNotice(true);
			//do not show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'Email'
			setEmailNotice(true);
			//do not show 'h:outputText' tag which is note message tag for 'h:inputText' tag whose id is 'humanId'
			setHumanIdNotice(true);
		}
        return showNote;
    }

    public void setShowNote(boolean showNote) {
        this.showNote = showNote;
    }

    /**
     * binding with jsf tag whose id is 'identificationNumber'
     */
    protected HtmlInputText identificationNumber;

    public HtmlInputText getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(HtmlInputText identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    /**
     * binding with jsf tag whose id is 'name'
     */
    protected HtmlInputText name;

    public HtmlInputText getName() {
        return name;
    }

    public void setName(HtmlInputText name) {
        this.name = name;
    }

    /**
     * binding with jsf tag whose id is 'numberOfStudent'
     */
    protected HtmlInputText numberOfStudent;

    public HtmlInputText getNumberOfStudent() {
        return numberOfStudent;
    }

    public void setNumberOfStudent(HtmlInputText numberOfStudent) {
        this.numberOfStudent = numberOfStudent;
    }

    /**
     * binding with jsf tag whose id is 'class'
     */
    protected HtmlInputText theClass;

    public HtmlInputText getTheClass() {
        return theClass;
    }

    public void setTheClass(HtmlInputText theClass) {
        this.theClass = theClass;
    }

    /**
     * binding with jsf tag whose id is 'address'
     */
    protected HtmlInputText address;

    public HtmlInputText getAddress() {
        return address;
    }

    public void setAddress(HtmlInputText address) {
        this.address = address;
    }

    /**
     * binding with jsf tag whose id is 'telephone'
     */
    protected HtmlInputText telephone;

    public HtmlInputText getTelephone() {
        return telephone;
    }

    public void setTelephone(HtmlInputText telephone) {
        this.telephone = telephone;
    }

    /**
     * binding with jsf tag whose id is 'Email'
     */
    protected HtmlInputText eMail;

    public HtmlInputText getEMail() {
        return eMail;
    }

    public void setEMail(HtmlInputText mail) {
        eMail = mail;
    }

    /**
     * binding with jsf tag whose id is 'humanId'
     */
    protected HtmlInputText humanId;

    public HtmlInputText getHumanId() {
        return humanId;
    }

    public void setHumanId(HtmlInputText humanId) {
        this.humanId = humanId;
    }

    /**
     * specify whether 'h:outputText' tag shows note message for 'h:inputText' tag whose id is
     * identificationNumber
     */
    protected boolean identificationNumberNotice;

    public boolean isIdentificationNumberNotice() {
        return identificationNumberNotice;
    }

    public void setIdentificationNumberNotice(boolean identificationNumberNotice) {
        this.identificationNumberNotice = identificationNumberNotice;
    }

    /**
     * specify whether 'h:message' tag shows error message for 'h:inputText' tag whose id is
     * identificationNumber
     */
    protected boolean identificationNumberMessage;

    public boolean isIdentificationNumberMessage() {
        return identificationNumberMessage;
    }

    public void setIdentificationNumberMessage(boolean identificationNumberMessage) {
        this.identificationNumberMessage = identificationNumberMessage;
    }

    /**
     * specify whether 'h:outputText' tag shows note message for 'h:inputText' tag whose id is name
     */
    protected boolean nameNotice;

    public boolean isNameNotice() {
        return nameNotice;
    }

    public void setNameNotice(boolean nameNotice) {
        this.nameNotice = nameNotice;
    }

    /**
     * specify whether 'h:message' tag shows error message for 'h:inputText' tag whose id is name
     */
    protected boolean nameMessage;

    public boolean isNameMessage() {
        return nameMessage;
    }

    public void setNameMessage(boolean nameMessage) {
        this.nameMessage = nameMessage;
    }

	/**
     * specify whether 'h:outputText' tag shows note message for 'h:inputText' tag whose id is numberOfStudent
     */
    protected boolean numberOfStudentNotice;

    public boolean isNumberOfStudentNotice() {
        return numberOfStudentNotice;
    }

    public void setNumberOfStudentNotice(boolean numberOfStudentNotice) {
        this.numberOfStudentNotice = numberOfStudentNotice;
    }

	/**
     * specify whether 'h:message' tag shows error message for 'h:inputText' tag whose id is numberOfStudent
     */
    protected boolean numberOfStudentMessage;

    public boolean isNumberOfStudentMessage() {
        return numberOfStudentMessage;
    }

    public void setNumberOfStudentMessage(boolean numberOfStudentMessage) {
        this.numberOfStudentMessage = numberOfStudentMessage;
    }

	/**
     * specify whether 'h:outputText' tag shows note message for 'h:inputText' tag whose id is class
     */
	protected boolean classNotice;

    public boolean isClassNotice() {
        return classNotice;
    }

    public void setClassNotice(boolean classNotice) {
        this.classNotice = classNotice;
    }

	/**
     * specify whether 'h:message' tag shows error message for 'h:inputText' tag whose id is class
     */
	protected boolean classMessage;

    public boolean isClassMessage() {
        return classMessage;
    }

    public void setClassMessage(boolean classMessage) {
        this.classMessage = classMessage;
    }

	/**
     * specify whether 'h:outputText' tag shows note message for 'h:inputText' tag whose id is address
     */
	protected boolean addressNotice;

    public boolean isAddressNotice() {
        return addressNotice;
    }

    public void setAddressNotice(boolean addressNotice) {
        this.addressNotice = addressNotice;
    }

	/**
     * specify whether 'h:message' tag shows error message for 'h:inputText' tag whose id is address
     */
	protected boolean addressMessage;

    public boolean isAddressMessage() {
        return addressMessage;
    }

    public void setAddressMessage(boolean addressMessage) {
        this.addressMessage = addressMessage;
    }

	/**
     * specify whether 'h:outputText' tag shows note message for 'h:inputText' tag whose id is telephone
     */
	protected boolean telephoneNotice;

	public boolean isTelephoneNotice() {
        return telephoneNotice;
    }

    public void setTelephoneNotice(boolean telephoneNotice) {
        this.telephoneNotice = telephoneNotice;
    }

    /**
     * specify whether 'h:message' tag shows error message for 'h:inputText' tag whose id is telephone
     */
	protected boolean telephoneMessage;

    public boolean isTelephoneMessage() {
        return telephoneMessage;
    }

    public void setTelephoneMessage(boolean telephoneMessage) {
        this.telephoneMessage = telephoneMessage;
    }

	/**
     * specify whether 'h:outputText' tag shows note message for 'h:inputText' tag whose id is Email
     */
	protected boolean EmailNotice;

    public boolean isEmailNotice() {
        return EmailNotice;
    }

    public void setEmailNotice(boolean emailNotice) {
        EmailNotice = emailNotice;
    }

    /**
     * specify whether 'h:message' tag shows error message for 'h:inputText' tag whose id is Email
     */
	protected boolean EmailMessage;

    public boolean isEmailMessage() {
        return EmailMessage;
    }

    public void setEmailMessage(boolean emailMessage) {
        EmailMessage = emailMessage;
    }

	/**
     * specify whether 'h:outputText' tag shows note message for 'h:inputText' tag whose id is humanId
     */
	protected boolean humanIdNotice;

    public boolean isHumanIdNotice() {
        return humanIdNotice;
    }

    public void setHumanIdNotice(boolean humanIdNotice) {
        this.humanIdNotice = humanIdNotice;
    }

    /**
     * specify whether 'h:message' tag shows error message for 'h:inputText' tag whose id is humanId
     */
	protected boolean humanIdMessage;

    public boolean isHumanIdMessage() {
        return humanIdMessage;
    }

    public void setHumanIdMessage(boolean humanIdMessage) {
        this.humanIdMessage = humanIdMessage;
    }

    /**
     * indicates what page to direct to
     */
    private String redirect;

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    /**
     * insert information about student into table 'student'
     * @param actionEvent
     */
    public void addStudentActionListener(ActionEvent actionEvent){
        debug("method addStudentActionListener in class AddStudentBean is called");

        String strIdentificationNumber = (String)getIdentificationNumber().getValue();
        String strName = (String)getName().getValue();
        String strNumberOfStudent = (String)getNumberOfStudent().getValue();
        String strTheClass = (String)getTheClass().getValue();
        String strAddress = (String)getAddress().getValue();
        String strTelephone = (String)getTelephone().getValue();
        String strEMail = (String)getEMail().getValue();
        String strHumanId = (String)getHumanId().getValue();

        debug(strIdentificationNumber);
        debug(strName);
        debug(strNumberOfStudent);
        debug(strTheClass);
        debug(strAddress);
        debug(strTelephone);
        debug(strEMail);
        debug(strHumanId);

        try {
            serviceLocator.getExamService().addStudent(strIdentificationNumber , strName ,
                    strNumberOfStudent , strTheClass , strAddress ,
                    strTelephone , strEMail);
        } catch (ExamException e) {
            e.printStackTrace();
        }
        setRedirect("success");
    }
}
