package edu.ncut.vod.jsf.examonline.selfmadevalidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.sun.faces.util.MessageFactory;

import edu.ncut.vod.jsf.examonline.managedbeans.AddStudentBean;

public class NumberOfStudentValidator implements Validator {

    /**
     * validate field 'numberOfStudent':it must be number or English
     */
    public void validate(FacesContext context, UIComponent component, Object obj)
            throws ValidatorException {
        debug("method validate in class NumberOfStudentValidator is called");

        String label = ((HtmlInputText) component).getLabel();

        if ("numberOfStudent".equals(label)) {
            String fieldValue = (String) obj; //get the value of field which needs validating

            // validate field 'numberOfStudent':it must be number or English
            Pattern pattern = Pattern.compile("^[0-9a-zA-Z]*$");
            Matcher matcher = pattern.matcher(fieldValue);
            boolean match = matcher.matches();

            if (!match) {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                FacesMessage msg = MessageFactory.getMessage(facesContext,
                        "numberOfStudentContent_mustBeNumberOrEnglish",
                        "numberOfStudentContent_mustBeNumberOrEnglish");
                msg.setSeverity(FacesMessage.SEVERITY_WARN);
                facesContext.addMessage("addStudent:numberOfStudent", msg);
				setErrorMessageVisible();
				throw new ValidatorException(msg);
            }else{
				setErrorMessageInvisible();
			}
        }
    }

    /**
     * set error message tag of tag 'h:inputText' visible
     */
    public void setErrorMessageVisible(){
        debug("method setErrorMessageVisible in class NumberOfStudentValidator is called");

        FacesContext context =FacesContext.getCurrentInstance();
        Application application=context.getApplication();
        AddStudentBean addStudentBean=(AddStudentBean)application.evaluateExpressionGet(context,"#{addStudentBean}",AddStudentBean.class);

        addStudentBean.setNumberOfStudentNotice(true);
        addStudentBean.setNumberOfStudentMessage(true);
    }

    /**
	 * set error message tag of tag 'h:inputText' invisible
	 */
    public void setErrorMessageInvisible(){
		debug("method setErrorMessageInvisible in class NumberOfStudentValidator is called");

		FacesContext context =FacesContext.getCurrentInstance();
        Application application=context.getApplication();
        AddStudentBean addStudentBean=(AddStudentBean)application.evaluateExpressionGet(context,"#{addStudentBean}",AddStudentBean.class);

        addStudentBean.setNumberOfStudentNotice(false);
        addStudentBean.setNumberOfStudentMessage(false);
	}

    /**
     * print message on console
     * @param message
     */
    public void debug(String message) {
        System.out.println(message);
    }
}
