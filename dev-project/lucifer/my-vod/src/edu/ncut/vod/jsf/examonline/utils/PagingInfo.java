package edu.ncut.vod.jsf.examonline.utils;

public class PagingInfo
{
    private int batchSize = 10;
    private int firstItem = 0;
    private int itemCount = -1;
    private int currentpage=0;
    private int pagecount=-1;

    public int getCurrentpage()
    {
        getFirstItem();
        currentpage=firstItem/batchSize;
        return currentpage;
    }

    public void setCurrentpage(int currentpage) {
        this.currentpage = currentpage;
    }

    public int getPagecount() {
        int i=itemCount%batchSize;
        double count=itemCount/batchSize;
        if(i==0)
        {

           pagecount=(int)Math.floor(count);
        }
        else
        {
            pagecount=(int)Math.floor(count)+1;
        }

        return pagecount;
    }

    public void setPagecount(int pagecount) {
        this.pagecount = pagecount;
    }
    public int getBatchSize()
    {
        return batchSize;
    }

    public int getItemCount()
    {
        return itemCount;
    }

    public void setItemCount(int itemCount)
    {
        this.itemCount = itemCount;
    }

    public int getFirstItem()
    {
        if (itemCount == -1)
        {
            throw new IllegalStateException("itemCount must be set before invoking getFirstItem");
        }
        if (firstItem >= itemCount)
        {
            if (itemCount == 0)
            {
                firstItem = 0;
            }
            else
            {
                int zeroBasedItemCount = itemCount - 1;
                double pageDouble = zeroBasedItemCount / batchSize;

                int page = (int) Math.floor(pageDouble);
                firstItem = page * batchSize;
            }
        }
        return firstItem;
    }

    public void setFirstItem(int firstItem)
    {
        this.firstItem = firstItem;
    }

    public int getLastItem()
    {
        getFirstItem();
        return firstItem + batchSize > itemCount ? itemCount : firstItem + batchSize;
    }

    public void nextPage()
    {
        getFirstItem();
        if (firstItem + batchSize <itemCount)
        {
            firstItem += batchSize;
        }
    }

    public void previousPage()
    {
        getFirstItem();
        firstItem -= batchSize;
        if (firstItem < 0)
        {
            firstItem = 0;
        }
    }
    public void lastPage()
    {
        getFirstItem();
        int remaing=itemCount%batchSize;
        firstItem=itemCount-remaing;
    }
}
