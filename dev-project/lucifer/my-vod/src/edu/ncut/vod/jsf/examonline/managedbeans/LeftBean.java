package edu.ncut.vod.jsf.examonline.managedbeans;

import javax.faces.event.ActionEvent;

public class LeftBean extends BaseBean {

    private static final long serialVersionUID = 3194995923956666548L;

    /**
     * this field is binding with jsf tag 'h:panelGroup' whose id is
     * 'questionChild'
     */
    private boolean questionChildDisplay;

    public boolean isQuestionChildDisplay() {
        return questionChildDisplay;
    }

    public void setQuestionChildDisplay(boolean questionChildDisplay) {
        this.questionChildDisplay = questionChildDisplay;
    }

    /**
     * this field is binding with jsf tag 'h:panelGroup' whose id is
     * 'studentChild'
     */
    private boolean studentChildDisplay;

    public boolean isStudentChildDisplay() {
        return studentChildDisplay;
    }

    public void setStudentChildDisplay(boolean studentChildDisplay) {
        this.studentChildDisplay = studentChildDisplay;
    }

    /**
     * this actionListener is binding with jsf tag 'h:commandLink' whose id is
     * 'questionDataManagement'
     * 
     * @param ActionEvent
     */
    public void questionDataManagementActionListener(ActionEvent ActionEvent) {
			if(false == questionChildDisplay){
					setQuestionChildDisplay(true);
			}else if(true == questionChildDisplay){
					setQuestionChildDisplay(false);
			}
    }
    
    /**
     * this actionListener is binding with jsf tag 'h:commandLink' whose id is
     * 'studentDataManagement'
     * 
     * @param ActionEvent
     */
    public void studentDataManagementActionListener(ActionEvent ActionEvent) {
			if(false == studentChildDisplay){
					setStudentChildDisplay(true);
			}else if(true == studentChildDisplay){
					setStudentChildDisplay(false);
			}
    }
}
