package edu.ncut.vod.jsf.examonline.managedbeans;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * public interface PhaseListener extends java.util.EventListener, java.io.Serializable:
 * An interface implemented by objects that wish to be notified at the beginning and ending of 
 * processing for each standard phase of the request processing lifecycle.
 */
public class BaseBean implements PhaseListener{

    private static final long serialVersionUID = 4714784366489132109L;
    
    /**
     * attribute 'ServiceLocatorBean' is dependable of injection from file 'faces-config.xml'
     */
    protected ServiceLocatorBean serviceLocator;

    public ServiceLocatorBean getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(ServiceLocatorBean serviceLocator) {
        this.logger.debug("service locator is set");
        this.serviceLocator = serviceLocator;
        
        this.init();
    }
    
    public BaseBean() {
    }
    
    /**
     * attribute 'ExamBean' is dependable of injection from file 'applicationContext.xml'
     */
    /*protected ExamService examService;

    public void setExamService(ExamService examService) {
        this.examService = examService;
    }

    public ExamService getExamService() {
        return examService;
    }*/
    
    /**
     * the logger for this class
     */
    protected final Log logger = LogFactory.getLog(this.getClass());

    /**
     * Used to initialize the managed bean.
     * <p>
     * Called after the service locator is set.
     * It is a workaround.
     * <p>
     * Once the JSF bean management facility can support init method,
     * the init method can be configured and called from the JSF implementation directly.
     */ 
    protected void init() {
    }

    /*
     * print message on console
     */
    public void debug(String message) {
        System.out.println(message);
    }
    
    /*
     * method implemented by this class,because of interface PhaseListener
     * @see javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
     */
    public void afterPhase(PhaseEvent phaseEvent) {
        System.out.println("在" + phaseEvent.getPhaseId().toString() + "之后");
        if (phaseEvent.getPhaseId() == PhaseId.RENDER_RESPONSE) {
            System.out.println("用户请求响应完毕");
        }
    }

    /*
     * method implemented by this class,because of interface PhaseListener
     * @see javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
     */
    public void beforePhase(PhaseEvent phaseEvent) {
        if (phaseEvent.getPhaseId() == PhaseId.RESTORE_VIEW) {
            System.out.println("正在处理新的请求");
        }
        System.out.println("在" + phaseEvent.getPhaseId().toString() + "进入之前");
    }

    /*
     * method implemented by this class,because of interface PhaseListener
     * @see javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
     */
    public PhaseId getPhaseId() {
        return PhaseId.ANY_PHASE;
    }
}
