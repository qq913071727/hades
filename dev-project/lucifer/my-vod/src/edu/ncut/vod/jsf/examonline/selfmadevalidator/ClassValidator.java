package edu.ncut.vod.jsf.examonline.selfmadevalidator;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.sun.faces.util.MessageFactory;

import edu.ncut.vod.jsf.examonline.managedbeans.AddStudentBean;

public class ClassValidator implements Validator {

    /**
     * validate field 'class':its length can not be longer than 12
     */
    public void validate(FacesContext context, UIComponent component,
            Object obj) throws ValidatorException {
        debug("method validate in class ClassValidator is called");

        String label = ((HtmlInputText) component).getLabel();

        if ("class".equals(label)) {
            String fieldValue = (String) obj; // get the value of field which needs validating

            // validate field 'class':its length can not be longer than 12
            if(fieldValue.length() > 12){
                FacesContext facesContext = FacesContext.getCurrentInstance();
                FacesMessage msg = MessageFactory.getMessage(facesContext,
                        "classLength_canNotBeLongerThan12",
                        "classLength_canNotBeLongerThan12");
                msg.setSeverity(FacesMessage.SEVERITY_WARN);
                facesContext.addMessage("addStudent:class", msg);
                setErrorMessageVisible();
                throw new ValidatorException(msg);
            }else{
				setErrorMessageInvisible();
			}
        }
    }

    /**
     * set error message tag of tag 'h:inputText' visible
     */
    public void setErrorMessageVisible(){
        debug("method setErrorMessageVisible in class ClassValidator is called");

        FacesContext context =FacesContext.getCurrentInstance();
        Application application=context.getApplication();
        AddStudentBean addStudentBean=(AddStudentBean)application.evaluateExpressionGet(context,"#{addStudentBean}",AddStudentBean.class);

        addStudentBean.setClassNotice(true);
        addStudentBean.setClassMessage(true);
    }

    /**
	 * set error message tag of tag 'h:inputText' invisible
	 */
    public void setErrorMessageInvisible(){
		debug("method setErrorMessageInvisible in class ClassValidator is called");

		FacesContext context =FacesContext.getCurrentInstance();
        Application application=context.getApplication();
        AddStudentBean addStudentBean=(AddStudentBean)application.evaluateExpressionGet(context,"#{addStudentBean}",AddStudentBean.class);

        addStudentBean.setClassNotice(false);
        addStudentBean.setClassMessage(false);
	}

    /**
     * print message on console
     * @param message
     */
    public void debug(String message) {
        System.out.println(message);
    }
}
