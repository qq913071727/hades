package edu.ncut.vod.jsf.examonline.selfmadevalidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.sun.faces.util.MessageFactory;

import edu.ncut.vod.jsf.examonline.managedbeans.AddStudentBean;

public class TelephoneValidator implements Validator {

    /**
     * validate field 'telephone':it must be number in country
     */
    public void validate(FacesContext context, UIComponent component,
            Object obj) throws ValidatorException {
        debug("method validate in class TelephoneValidator is called");

        String label = ((HtmlInputText) component).getLabel();

        if ("telephone".equals(label)) {
            String fieldValue = (String) obj; // get the value of field which needs validating

            // validate field 'telephone':it must be number in country
            Pattern pattern = Pattern.compile("(^(\\d{3,4}-)?\\d{7,8})$|(13[0-9]{9})");
            Matcher matcher = pattern.matcher(fieldValue);
            boolean match = matcher.matches();

            if (!match) {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                FacesMessage msg = MessageFactory.getMessage(facesContext,
                        "telephoneContent_mustBeNumberInCountry",
                        "telephoneContent_mustBeNumberInCountry");
                msg.setSeverity(FacesMessage.SEVERITY_WARN);
                facesContext.addMessage("addStudent:telephone", msg);
                setErrorMessageVisible();
                throw new ValidatorException(msg);
            }else{
				setErrorMessageInvisible();
			}
        }
    }

    /**
     * set error message tag of tag 'h:inputText' visible
     */
    public void setErrorMessageVisible(){
        debug("method setErrorMessageVisible in class TelephoneValidator is called");

        FacesContext context =FacesContext.getCurrentInstance();
        Application application=context.getApplication();
        AddStudentBean addStudentBean=(AddStudentBean)application.evaluateExpressionGet(context,"#{addStudentBean}",AddStudentBean.class);

        addStudentBean.setTelephoneNotice(true);
        addStudentBean.setTelephoneMessage(true);
    }

    /**
	 * set error message tag of tag 'h:inputText' invisible
	 */
    public void setErrorMessageInvisible(){
		debug("method setErrorMessageInvisible in class TelephoneValidator is called");

		FacesContext context =FacesContext.getCurrentInstance();
        Application application=context.getApplication();
        AddStudentBean addStudentBean=(AddStudentBean)application.evaluateExpressionGet(context,"#{addStudentBean}",AddStudentBean.class);

        addStudentBean.setTelephoneNotice(false);
        addStudentBean.setTelephoneMessage(false);
	}

    /**
     * print message on console
     * @param message
     */
    public void debug(String message) {
        System.out.println(message);
    }
}
