package edu.ncut.vod.commonbeans;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * delegate an advertisement
 * @author lishenlishen
 */
public class ADBean implements java.io.Serializable {

    private static final long serialVersionUID = 7171092138733841164L;
    private String url = "";
    private String imgName = "";
    private int adid = 1;

    /** Creates a new instance of ADBean */
    public ADBean() {
    }

    /**
     * get object 'ADBean' whose id is given
     * @param id
     */
    public ADBean(int id) {
        adid = id;
        populate(id);
    }     
   
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public int getAdid() {
        return adid;
    }

    public void setAdid(int adid) {
        this.adid = adid;
    }

    /**
     * according to content of database,initialize an object on random
     * @param id
     */
    public void populate(int id) {
        DBConnection db = new DBConnection();

        PreparedStatement preparedStmt;
        ResultSet sqlRst;

        try {
            preparedStmt = db.getConnection().prepareStatement(
                    "select * from adinfo where adid = ?");
            preparedStmt.setInt(1, id);

            sqlRst = preparedStmt.executeQuery();

            while (sqlRst.next()) {
                url = sqlRst.getString("url");
                imgName = sqlRst.getString("ImgName");
                //debug(imgName);
                adid = sqlRst.getInt("adid");
            }
        } catch (Exception e) {
            System.out.print(e.toString());
        }
    }
    
    /**
     * console input for the purpose of debugging code
     * @param msg
     */
    public void debug(String msg) {
        System.out.println(msg);
    }
    
}
