package edu.ncut.vod.commonbeans;

public class CurrentDateTest {

    public static void main(String[] args) {
        CurrentDate cd = new CurrentDate();
        debug(cd.getDate().toString());        
        debug(cd.getDay());
        debug(cd.getColor());
    }

    private static void debug(String message) {
        System.out.println(message);
    }
}
