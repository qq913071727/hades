package edu.ncut.vod.commonbeans;

import java.util.Date;

public class CurrentDate extends Object implements java.io.Serializable {
    
    private static final long serialVersionUID = 1L;
    private Date date;    
    private String day;
    private String color;
        
    public CurrentDate() {        
                                             
    }

    public Date getDate() {
        date = new Date();
        //debug(date.toString());
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }       

    public void setDay(String day) {
        this.day = day;
    }
    
    public String getDay() {
        //debug("tttttttttttttt"+day);
        //day = new String("1");
        day = Integer.toString(getDate().getDay());
        //debug("tttttttttttttt"+day);
        return day;
    }
    
    public void setColor(String color) {
        this.color = color;
    }
    
    public String getColor() {
        color = new String("black");
        if(getDay().equals("0")|getDay().equals("6")){
            color="red";
        }
        //debug(color);
        return color;
    }     
    
    private void debug(String message) {
        System.out.println(message);
    }
}
