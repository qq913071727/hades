package edu.ncut.vod.commonbeans;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
    
    /**
     * creates a new instance of DBConnection
     */
    public DBConnection() {
    }
    
    /**
     * get database connection
     * @return
     */
    public Connection getConnection(){
        Connection conn;
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn= DriverManager.getConnection("jdbc:mysql://localhost/myvod","root","root");
            
        } catch (java.sql.SQLException e){
            System.err.println(e.toString());
            return null;
        }catch (Exception ea){
            System.err.println(ea.toString());
            return null;
        }
        return conn;
    }
}
