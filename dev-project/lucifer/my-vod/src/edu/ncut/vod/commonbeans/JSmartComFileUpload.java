package edu.ncut.vod.commonbeans;

public class JSmartComFileUpload {
    
    private String fieldName;
    private int size;
    private String fileName;
    private String fileExt;
    private String filePathName;
              
    /**
     * constructor method
     */
    public JSmartComFileUpload() {
        super();
    }

    /**
     * @param fieldName
     * @param size
     * @param fileName
     * @param fieldExt
     * @param filePathName
     */
    public JSmartComFileUpload(String fieldName, int size, String fileName,
            String fileExt, String filePathName) {
        this.fieldName = fieldName;
        this.size = size;
        this.fileName = fileName;
        this.fileExt = fileExt;
        this.filePathName = filePathName;
    }
    
    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }
    
    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
           
    /**
     * @return the size
     */
    public int getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }
    
    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    /**
     * @return the fileExt
     */
    public String getFileExt() {
        return fileExt;
    }

    /**
     * @param fileExt the fileExt to set
     */
    public void setFileExt(String fileExt) {
        this.fileExt = fileExt;
    }

    /**
     * @return the filePathName
     */
    public String getFilePathName() {
        return filePathName;
    }
    
    /**
     * @param filePathName the filePathName to set
     */
    public void setFilePathName(String filePathName) {
        this.filePathName = filePathName;
    }        
    
    /**
     * print mesage in console
     * @param message
     */
    public static void debug(String message) {
        System.out.println(message);
    }
}
