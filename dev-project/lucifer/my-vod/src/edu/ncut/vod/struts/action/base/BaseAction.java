package edu.ncut.vod.struts.action.base;

import org.apache.struts.action.Action;

import edu.ncut.vod.hibernate.service.ServerInfoService;
import edu.ncut.vod.hibernate.service.ServerSelectService;
import edu.ncut.vod.hibernate.service.TableListService;
import edu.ncut.vod.hibernate.service.VideoStatisticalInformationService;
import edu.ncut.vod.hibernate.service.AdClickInfoService;

public class BaseAction extends Action {
    
    /**
     * service layer object named 'serverSelectService'
     */
    protected ServerSelectService serverSelectService;

    public ServerSelectService getServerSelectService() {
        return serverSelectService;
    }

    public void setServerSelectService(ServerSelectService serverSelectService) {
        this.serverSelectService = serverSelectService;
    }
    
    /**
     * service layer object named 'serverInfoService'
     */
    protected ServerInfoService serverInfoService;

    public ServerInfoService getServerInfoService() {
        return serverInfoService;
    }

    public void setServerInfoService(ServerInfoService serverInfoService) {
        this.serverInfoService = serverInfoService;
    }
    
    /**
     * service layer object named 'videoStatisticalInformationService'
     */
    protected VideoStatisticalInformationService videoStatisticalInformationService;    
    
    public VideoStatisticalInformationService getVideoStatisticalInformationService() {
        return videoStatisticalInformationService;
    }

    public void setVideoStatisticalInformationService(
            VideoStatisticalInformationService videoStatisticalInformationService) {
        this.videoStatisticalInformationService = videoStatisticalInformationService;
    }
    
    /**
     * service layer object named 'adClickInfoService'
     */
    protected AdClickInfoService adClickInfoService;    

    public AdClickInfoService getAdClickInfoService() {
        return adClickInfoService;
    }

    public void setAdClickInfoService(AdClickInfoService adClickInfoService) {
        this.adClickInfoService = adClickInfoService;
    }        

    /**
     * output message on console 
     * @param message
     */
    public void debug(String message){
        System.out.println(message);
    }
}
