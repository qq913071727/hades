﻿package edu.ncut.vod.struts.form;

import org.apache.struts.action.ActionForm;

import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

public class AdminServiceForm extends ActionForm {

    private static final long serialVersionUID = -7448001105382629131L;

    /**
     * determine to select which table is shown
     */
    private String tableSelection;

    public String getTableSelection() {
        return tableSelection;
    }

    public void setTableSelection(String tableSelection) {
        this.tableSelection = tableSelection;
    }

    /**
     * for showing the name of table in file 'home.jsp'
     */
    private List<String> tableName;

    public List<String> getTableName() {
        return tableName;
    }

    public void setTableName(List<String> tableName) {
        this.tableName = tableName;
    }

    /**
     * the content of the table shown in page 'home.jsp'
     */
	private List<HashMap<String,Object>> shownTableContent;

    public List<HashMap<String, Object>> getShownTableContent() {
		if(null == shownTableContent){
			shownTableContent = new ArrayList<HashMap<String,Object>>();
		}
		//shownTableContent = new ArrayList<HashMap<String,Object>>();
        return shownTableContent;
    }

    public void setShownTableContent(List<HashMap<String, Object>> shownTableContent) {
        this.shownTableContent = shownTableContent;
    }

    /**
     * specify if table is shown
     */
    private boolean showTable;

    public boolean isShowTable() {
        return showTable;
    }

    public void setShowTable(boolean showTable) {
        this.showTable = showTable;
    }

    /**
     * specify if the content of selected table is empty
     */
    private boolean contentEmpty;

    public boolean isContentEmpty() {
        return contentEmpty;
    }

    public void setContentEmpty(boolean contentEmpty) {
        this.contentEmpty = contentEmpty;
    }

    /**
     * store every name of each field
     */
    private List<String> fieldName;

    public List<String> getFieldName() {
		if(null == fieldName){
			fieldName = new ArrayList<String>();
		}
        return fieldName;
    }

    public void setFieldName(List<String> fieldName) {
        this.fieldName = fieldName;
    }

    /*private Set fieldName;

    public Set getFieldName() {
		if(null == fieldName){
			fieldName = new TreeSet();
		}
        return fieldName;
    }

    public void setFieldName(Set fieldName) {
        this.fieldName = fieldName;
    }*/

}
