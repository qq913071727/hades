package edu.ncut.vod.struts.action.base;

import org.apache.struts.actions.DispatchAction;

import edu.ncut.vod.ibatis.service.LoginAndLogoffService;

public class BaseDispatchAction extends DispatchAction {
    
    /*
     * service object and provide login and logoff service
     */
    protected LoginAndLogoffService loginAndLogoffService;

    /**
     * @return the loginAndLogoffService
     */
    public LoginAndLogoffService getLoginAndLogoffService() {
        return loginAndLogoffService;
    }

    /**
     * @param loginAndLogoffService the loginAndLogoffService to set
     */
    public void setLoginAndLogoffService(LoginAndLogoffService loginAndLogoffService) {
        this.loginAndLogoffService = loginAndLogoffService;
    }

    /*
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }
}
