package edu.ncut.vod.struts.action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import edu.ncut.vod.struts.action.base.BaseDispatchAction;
import edu.ncut.vod.struts.form.LoginForm;
import edu.ncut.vod.struts.form.LogoffForm;

public class LoginAction extends BaseDispatchAction {
    
    public ActionForward executeLogin(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        
        /*
         * firstly create object of LoginForm and ActionForward as loginForm and 
         * forward,and then initialize forward as 'fail' string
         */
        LoginForm loginForm = (LoginForm) form;        
        ActionForward forward = new ActionForward();
        forward = mapping.findForward("goToIndex");

        /*
         * loginForm.validate();get username,password,determinepassword and authentication 
         * in theloginform
         */
        String username = (String) loginForm.getUserName().trim();
        String password = (String) loginForm.getPassword().trim();
        String determinepassword = (String) loginForm.getDeterminePassword().trim();
        String authentication = (String) loginForm.getAuthentication().trim();               

        /*
         * create session and get authentication as rand in session and authentication 
         * as inputRand in request
         */
        HttpSession session = request.getSession();
        String rand = (String) session.getAttribute("authentication");

        // console output
        debug("rand:" + rand);
        debug("authentication:" + authentication);
        debug("username:" + username);
        debug("password:" + password);
        debug("determinepassword:" + determinepassword);        
        debug("session:" + session);
        
        getLoginParameter().put("rand",rand);
        getLoginParameter().put("authentication",authentication);
        getLoginParameter().put("username",username);
        getLoginParameter().put("password",password);
        getLoginParameter().put("determinepassword",determinepassword);
        
        Map<String,String> result = loginAndLogoffService.loginValidation(getLoginParameter());
        
        loginForm.setAuthenticationIsWrong(Boolean.valueOf(result.get("authenticationIsWrong")));
        loginForm.setSuccess(Boolean.valueOf(result.get("success")));
        forward = mapping.findForward(result.get("forward"));
        
        return forward;
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
    public ActionForward executeLogoff(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        LogoffForm logoffForm = (LogoffForm)form;
        ActionForward forward = new ActionForward();        
        forward = mapping.findForward("gotoIndex");
        
        String logoff = (String)logoffForm.getLogoff().trim();
        
        boolean b = loginAndLogoffService.logoffValidation(logoff);
               
        if(true == b){
            forward = mapping.findForward("gotoIndex");
            return (forward);
        }
       
        return (forward);
    }

    /*
     * transfer parameters to service object 
     */
    private Map<String,Object> loginParameter;            

    public Map<String, Object> getLoginParameter() {
        if(null == loginParameter){
            loginParameter = new HashMap<String,Object>();
        }
        return loginParameter;
    }

    public void setLoginParameter(Map<String, Object> loginParameter) {
        this.loginParameter = loginParameter;
    }
  
    /*
     * print message on console
     */
    public void debug(String message){
        System.out.println(message);
    }
}