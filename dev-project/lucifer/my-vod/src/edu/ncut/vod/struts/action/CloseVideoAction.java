package edu.ncut.vod.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import edu.ncut.vod.struts.action.base.BaseAction;
import edu.ncut.vod.struts.form.ServerSelectForm;

public class CloseVideoAction extends BaseAction {
    /*
     * (non-Javadoc)
     * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        debug("execute method in class CloseVideoAction is called");
        
        Integer demandedVideoId = Integer.valueOf(request.getParameter("demandedVideoId"));
        debug("demandedVideoId is "+demandedVideoId);
        
        serverInfoService.setState(demandedVideoId);
        
        return mapping.findForward("goBack");
    }
}
