package edu.ncut.vod.struts.form;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class LoginForm extends ActionForm {

    private static final long serialVersionUID = 3002171838646632677L;
    private String password = null;
    private String userName = null;
    private String determinePassword = null;
    private String authentication = null;
    private boolean authenticationIsWrong;
    private boolean success;
    
    /**
     * 
     * @return
     */
    public String getAuthentication() {
        return authentication;
    }

    /**
     * 
     * @param authentication
     */
    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    /**
     * 
     */
    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        debug("validate method in file 'LoginForm.java' is called");
        return null;
    }

    /**
     * 
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {

    }

    /**
     * 
     * @return
     */
    public String getUserName() {
        //debug("getUsername is invoked!");
        return userName;
    }

    /**
     * 
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * 
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 
     * @return
     */
    public String getDeterminePassword() {
        return determinePassword;
    }

    /**
     * 
     * @param determinePassword
     */
    public void setDeterminePassword(String determinePassword) {
        this.determinePassword = determinePassword;
    }
    
    /**
     * @return the authenticationIsWrong
     */
    public boolean isAuthenticationIsWrong() {
        return authenticationIsWrong;
    }

    /**
     * @param authenticationIsWrong the authenticationIsWrong to set
     */
    public void setAuthenticationIsWrong(boolean authenticationIsWrong) {
        this.authenticationIsWrong = authenticationIsWrong;
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        if(false == success){
            success = false;
        }
        return success;
    }

    /**
     * @param success the success to set
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * console input for the purpose of debugging code
     * @param msg
     */
    public void debug(String msg){
        System.out.println(msg);
    }
}

