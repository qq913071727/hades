package edu.ncut.vod.struts.form;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class SearchInformationAboutVideoForm extends ActionForm {

    private static final long serialVersionUID = 7105648544873861137L;
    private String keyword;
    private String searchBy;

    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        debug("validate method in file 'SearchInformationAboutVideoForm.java' is called");
        return null;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        // TODO Auto-generated method stub
    }

    public ActionErrors validate(ActionMapping mapping, ServletRequest request) {
        // TODO Auto-generated method stub
        return null;
    }

    public void reset(ActionMapping mapping, ServletRequest request) {
        // TODO Auto-generated method stub
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getSearchBy() {
        return searchBy;
    }

    public void setSearchBy(String searchBy) {
        this.searchBy = searchBy;
    }
    
    /*
     * print message on console
     */
    public void debug(String message) {
        System.out.println(message);
    }
}