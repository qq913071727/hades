package edu.ncut.vod.struts.form;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class VideoStatisticalInformationForm extends ActionForm {

    private static final long serialVersionUID = 3034688131522027671L;

    /** chart property */
    private String chart;

    /** chartItems property */
    Map<String,String> chartItems;
    
    /** selectedObject property */
    private String selectedObject;
    
    /** selectedObjectItems property */
    Map<String,String> selectedObjectItems;
    
    /** randomInt property */
    int randomInt;

    /** 
     * Method validate
     * @param mapping
     * @param request
     * @return ActionErrors
     */
    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        // TODO Auto-generated method stub
        return null;
    }

    /** 
     * Method reset
     * @param mapping
     * @param request
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        // TODO Auto-generated method stub
    }

    /** 
     * Returns the chart.
     * @return String
     */
    public String getChart() {
        return chart;
    }

    /** 
     * Set the chart.
     * @param chart The chart to set
     */
    public void setChart(String chart) {
        this.chart = chart;
    }

    /** 
     * Returns the selectedObject.
     * @return String
     */
    public String getSelectedObject() {
        return selectedObject;
    }

    /** 
     * Set the selectedObject.
     * @param selectedObject The selectedObject to set
     */
    public void setSelectedObject(String selectedObject) {
        this.selectedObject = selectedObject;
    }

    /**
     * @return the chartItems
     */
    public Map<String, String> getChartItems() {
        if(null == chartItems){
            chartItems = new HashMap<String, String>();
        }
        chartItems.put("pleaseSelect","请选择");
        chartItems.put("bar","柱状图");
        chartItems.put("pie","饼状图");
        return chartItems;
    }

    /**
     * @param chartItems the chartItems to set
     */
    public void setChartItems(Map<String, String> chartItems) {
        this.chartItems = chartItems;
    }

    /**
     * @return the selectedObjectItems
     */
    public Map<String, String> getSelectedObjectItems() {
        if(null == selectedObjectItems){
            selectedObjectItems = new HashMap<String, String>();
        }
        selectedObjectItems.put("pleaseSelect","请选择");
        selectedObjectItems.put("demandedVideoInfo","关于被点播的视频的信息");
        selectedObjectItems.put("adClickInfo","关于被选择的广告的信息");
        return selectedObjectItems;
    }

    /**
     * @param selectedObjectItems the selectedObjectItems to set
     */
    public void setSelectedObjectItems(Map<String, String> selectedObjectItems) {
        this.selectedObjectItems = selectedObjectItems;
    }

    /**
     * @return the randomInt
     */
    public int getRandomInt() {
        Random randomNumber = new Random();
        randomInt=Math.abs(randomNumber.nextInt(1000000));
        //randomInt=randomNumber.nextInt(1000);
        return randomInt;
    }

    /**
     * @param randomInt the randomInt to set
     */
    public void setRandomInt(int randomInt) {
        this.randomInt = randomInt;
    }
        
}