package edu.ncut.vod.struts.form;

import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class CalendarForm extends ActionForm {

    private static final long serialVersionUID = 5033645488804539630L;

    HttpServletRequest request;
    String year;// = request.getParameter("year");
    String month;// = request.getParameter("month");
    int monthInteger;
    String days[] = new String[42];
    Calendar thisMonth = Calendar.getInstance();

    public Calendar getThisMonth() {
        return thisMonth;
    }

    public void setThisMonth(Calendar thisMonth) {
        this.thisMonth = thisMonth;
    }

    public String getYear() {
        Date d = new Date();
        year = String.valueOf(d.getYear()+1900);
        //debug("ssssssssssssssssssssssssssssssssssssssss"+year);
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getMonthInteger() {
        Date d = new Date();
        int month = d.getMonth() ;//Calendar.get(Calendar.MONTH);
        //debug("ssssssssssssssssssssssssssssssssssssssss"+month+"\n"+thisMonth);
        return month;//Integer.parseInt(month);
    }

    public void setMonthInteger(int monthInteger) {
        this.monthInteger = monthInteger;
    }

    public String[] getDays() {
        for (int i = 0; i < 42; i++) {
            days[i] = "";
        }
        
        if (month != null && (!month.equals("null")))
            thisMonth.set(Calendar.MONTH, Integer.parseInt(month));
        if (year != null && (!year.equals("null")))
            thisMonth.set(Calendar.YEAR, Integer.parseInt(year));
        year = String.valueOf(thisMonth.get(Calendar.YEAR));
        month = String.valueOf(thisMonth.get(Calendar.MONTH));
        thisMonth.setFirstDayOfWeek(Calendar.SUNDAY);
        thisMonth.set(Calendar.DAY_OF_MONTH, 1);
        int firstIndex = thisMonth.get(Calendar.DAY_OF_WEEK) - 1;
        int maxIndex = thisMonth.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < maxIndex; i++) {
            days[firstIndex + i] = String.valueOf(i + 1);
        }
        //debug("sssssssssssss"+days[0]);
        return days;
    }

    public void setDays(String[] days) {
        this.days = days;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        debug("validate method in file 'CalendarForm.java' is called");
        return null;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {

    }
    
    /**
     * output message on console
     * @param msg
     */
    public void debug(String msg) {
        System.out.println(msg);
    }
}