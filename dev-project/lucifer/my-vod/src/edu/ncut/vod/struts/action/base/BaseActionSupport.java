package edu.ncut.vod.struts.action.base;

import org.springframework.web.struts.ActionSupport;

import edu.ncut.vod.hibernate.service.TableListService;

/**
 * Implementing Spring-aware Struts actions
 * One way to integrate Struts and Spring is to write all of your Struts action classes
 * to extend a common base class that has access to the Spring application context.
 * The good news is that you won’t have to write this Spring-aware base action class
 * because Spring comes with org.springframework.web.struts.ActionSupport, an
 * abstract implementation of the org.apache.struts.action.Action that overrides
 * the setServlet() method to retrieve the WebApplicationContext from the Context-
 * LoaderPlugIn. Then, anytime your action needs to access a bean from the Spring
 * context, it just needs to call the getBean() method.
 * This version extends ActionSupport so that it has access to the Spring application context.
 * @author lishenlishen
 *
 */
public class BaseActionSupport extends ActionSupport {	   

    /**
     * output message on console
     * @param message
     */
    public void debug(String message){
        System.out.println(message);
    }
}