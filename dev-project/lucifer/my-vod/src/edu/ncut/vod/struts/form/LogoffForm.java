package edu.ncut.vod.struts.form;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class LogoffForm extends ActionForm {

    private static final long serialVersionUID = 2939452524916449617L;
    /** logoff property */
    private String logoff;

    /** 
     * Method validate
     * @param mapping
     * @param request
     * @return ActionErrors
     */
    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        debug("validate method in file 'LogoffForm.java' is called");
        return null;
    }

    /** 
     * Method reset
     * @param mapping
     * @param request
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        // TODO Auto-generated method stub
    }

    /** 
     * Returns the logoff.
     * @return String
     */
    public String getLogoff() {
        return logoff;
    }

    /** 
     * Set the logoff.
     * @param logoff The logoff to set
     */
    public void setLogoff(String logoff) {
        this.logoff = logoff;
    }
    
    /*
     * print message on console
     */
    public void debug(String message) {
        System.out.println(message);
    }
}