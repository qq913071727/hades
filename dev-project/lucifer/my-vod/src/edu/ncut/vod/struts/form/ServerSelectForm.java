package edu.ncut.vod.struts.form;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class ServerSelectForm extends ActionForm {

    private static final long serialVersionUID = -6099807114379718184L;

    /** server property */
    private String serverSelect;
    private String inputIP;

    /**
     * Method validate
     * @param mapping
     * @param request
     * @return ActionErrors
     */
    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        debug("validate method in file 'ServerSelectForm.java' is called");
        /*ActionErrors errors = new ActionErrors();
        if (IP == null || IP.equals("")) {
            errors.add("IP", new ActionMessage(
                    "hello.no.user_name.error"));
            return errors;
        }*/
        return null;
    }

    /**
     * Method reset:Reset bean properties to their default state, as needed.
     * This method is called before the properties are repopulated by the
     * controller.
     * 
     * @param mapping
     * @param request
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        // initiate selected radio input
        serverSelect = new String("mainServer");
    }

    /**
     * @return the serverSelect
     */
    public String getServerSelect() {
        return serverSelect;
    }

    /**
     * @param serverSelect
     * the serverSelect to set
     */
    public void setServerSelect(String serverSelect) {
        this.serverSelect = serverSelect;
    }

    /**
     * @return the inputIP
     */
    public String getInputIP() {
        return inputIP;
    }

    /**
     * @param inputIP the inputIP to set
     */
    public void setInputIP(String inputIP) {
        this.inputIP = inputIP;
    }

    /**
     * print message on console
     * @param message
     */
    public void debug(String message) {
        System.out.println(message);
    }

}