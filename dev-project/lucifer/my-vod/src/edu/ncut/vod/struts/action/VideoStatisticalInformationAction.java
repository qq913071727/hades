package edu.ncut.vod.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import edu.ncut.vod.struts.action.base.BaseAction;
import edu.ncut.vod.struts.form.VideoStatisticalInformationForm;

public class VideoStatisticalInformationAction extends BaseAction {

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        debug("method execute on class VideoStatisticalInformationAction is called");
        
        VideoStatisticalInformationForm videoStatisticalInformationForm = (VideoStatisticalInformationForm) form;        
        ActionForward forward = new ActionForward();
        forward = mapping.findForward("goBack");
        
        String selectedObject = videoStatisticalInformationForm.getSelectedObject();
        String chart = videoStatisticalInformationForm.getChart();
        int randomInt = videoStatisticalInformationForm.getRandomInt();
        
        request.setAttribute("randomInt",randomInt);
        
        debug("selectedObject is "+selectedObject);
        debug("chart is "+chart);
        debug("randomInt is "+randomInt);
        
        if("pleaseSelect".equals(selectedObject) && "pleaseSelect".equals(chart)){
            request.setAttribute("selectNotice","请选择对象和图表");
            return forward;//do not select anything
        }else if("pleaseSelect".equals(selectedObject) && !"pleaseSelect".equals(chart)){
            request.setAttribute("selectNotice","请选择对象");
            return forward;//select object,do not select chart
        }else if(!"pleaseSelect".equals(selectedObject) && "pleaseSelect".equals(chart)){
            request.setAttribute("selectNotice","请选择图表");
            return forward;//select chart,do not select object
        }else{
            request.setAttribute("selectNotice","可以继续选择");
            
            if("demandedVideoInfo".equals(selectedObject) && "pie".equals(chart)){
                request.setAttribute("chartName","demandedVideoInfoPie");
            }else if("demandedVideoInfo".equals(selectedObject) && "bar".equals(chart)){
                request.setAttribute("chartName","demandedVideoInfoBar");
            }else if("adClickInfo".equals(selectedObject) && "pie".equals(chart)){
                request.setAttribute("chartName","adClickInfoPie");
            }else if("adClickInfo".equals(selectedObject) && "bar".equals(chart)){
                request.setAttribute("chartName","adClickInfoBar");
            }
            
            videoStatisticalInformationService.getStatisticalChart(selectedObject,chart,randomInt);
            return forward;//select object and chart
        }        
    }
}
