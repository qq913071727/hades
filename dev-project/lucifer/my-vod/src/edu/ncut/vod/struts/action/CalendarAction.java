package edu.ncut.vod.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import edu.ncut.vod.struts.action.base.BaseAction;
import edu.ncut.vod.struts.form.CalendarForm;

/**
 * Delegating actions
 * Another approach to Struts-Spring integration is to write a Struts action that is
 * nothing more than a proxy to the real Struts action that is contained in the Spring
 * application context. The proxy action will retrieve the application context from
 * the ContextLoaderPlugIn, look up the real Struts action from the context, then
 * delegate responsibility to the real Struts action.
 * One nice thing about this approach is that the only action that does anything
 * Spring-specific is the proxy action. The real actions can be written as just
 * plain subclasses of org.apache.struts.Action.
 * @author lishenlishen
 *
 */
public class CalendarAction extends BaseAction {

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        CalendarForm calendarForm = (CalendarForm) form;
        debug("CalendarAction is involved");
        ActionForward forward = new ActionForward();
        forward = mapping.findForward("gotoCalendarPerformance");
        return forward;
    }

}