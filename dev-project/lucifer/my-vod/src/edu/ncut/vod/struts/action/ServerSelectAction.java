package edu.ncut.vod.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import edu.ncut.vod.struts.action.base.BaseAction;
import edu.ncut.vod.struts.form.ServerSelectForm;

public class ServerSelectAction extends BaseAction {

    /**
     * Method execute
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        ServerSelectForm serverSelectForm = (ServerSelectForm) form;
        ActionForward forward = new ActionForward();
        forward = mapping.findForward("goBack");

        String serverSelect = serverSelectForm.getServerSelect();
        String inputIp = serverSelectForm.getInputIP();
        String localServerIp;
        //经过代理以后，由于在客户端和服务之间增加了中间层，因此服务器无法直接拿到客户端的IP，
        //服务器端应用也无法直接通过转发请求的地址返回给客户端。但是在转发请求的HTTP头信息中，
        //增加了X－FORWARDED－FOR信息。
        if (request.getHeader("x-forwarded-for") == null) {
            localServerIp = request.getRemoteAddr();
        }else{
            localServerIp = request.getHeader("x-forwarded-for");
        }
        
        debug(serverSelect);
        debug(inputIp);
        debug(localServerIp);                

        // judge if input is valid
        if (null != serverSelect && "subsidiaryServer".equals(serverSelect)
                && null != inputIp && null != localServerIp) {
            String outCome = serverSelectService.addSubsidiaryServer(inputIp,localServerIp, 0, 0);
            if("success".equals(outCome)){
                request.setAttribute("alreadyRegister",false);
                request.setAttribute("success",true);
                request.setAttribute("inputIpErr",false);
                request.setAttribute("fail",false);
            }else if("alreadyRegister".equals(outCome)){
                request.setAttribute("alreadyRegister",true);
                request.setAttribute("success",false);
                request.setAttribute("inputIpErr",false);
                request.setAttribute("fail",false);
            }else if("inputIpErr".equals(outCome)){
                request.setAttribute("alreadyRegister",false);
                request.setAttribute("success",false);
                request.setAttribute("inputIpErr",true);
                request.setAttribute("fail",false);
            }else{
                request.setAttribute("alreadyRegister",false);
                request.setAttribute("success",false);
                request.setAttribute("inputIpErr",false);
                request.setAttribute("fail",true);
            }
        } else if (null != serverSelect && "mainServer".equals(serverSelect)
                && null != localServerIp) {
            String outCome = serverSelectService.addMainServer(localServerIp, 1, 0);
            if("alreadyRegister".equals(outCome)){
                request.setAttribute("alreadyRegister",true);
                request.setAttribute("success",false);
                request.setAttribute("inputIpErr",false);
                request.setAttribute("fail",false);
            }else{
                request.setAttribute("alreadyRegister",false);
                request.setAttribute("success",true);
                request.setAttribute("inputIpErr",false);
                request.setAttribute("fail",false);
            }
        }
        return forward;
    }
}