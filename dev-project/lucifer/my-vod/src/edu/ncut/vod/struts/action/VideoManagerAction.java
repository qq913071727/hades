package edu.ncut.vod.struts.action;

import java.sql.Timestamp;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.context.ApplicationContext;

import edu.ncut.vod.hibernate.service.ServerInfoService;
import edu.ncut.vod.hibernate.service.VideoInformationService;
import edu.ncut.vod.struts.action.base.BaseActionSupport;
import edu.ncut.vod.struts.form.VideoManagerForm;

/**
 * When ListCourseAction needs a CourseService, it starts by calling
 * getWebApplicationContext() to get a reference to the Spring application
 * context. Then it calls the getBean() method to retrieve a reference to the
 * Spring-managed course-Service bean.
 * @author lishenlishen
 */
public class VideoManagerAction extends BaseActionSupport {
    
    /**
     * Method execute
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        debug("execute method in object VideoManagerAction is called");
        
        VideoManagerForm videoManagerForm = (VideoManagerForm) form;
        
        //get video id requested
        Integer videoId = Integer.parseInt(request.getParameter("videoid"));
        debug("videoId is " + videoId);
        
        /*try to find ip of server whose curShowNum is minimum,
        and then increase one at column curShowNum,
        at last insert a row in table 'demandedvideoinfo'*/
        Integer serverId = updateState(videoId,request);//id of server whose column 'curShowNum' is minimum 
        
        //
        demandVideo(videoId,serverId, videoManagerForm,request);        

        return mapping.findForward("gotoRealPlayer");
    }

    /**
     * do something about demanding video 
     * @param videoId
     * @param serverId:id of server whose column 'curShowNum' is minimum 
     * @param videoManagerForm
     */
    public void demandVideo(Integer videoId,Integer serverId, VideoManagerForm videoManagerForm,HttpServletRequest request) {
        debug("method demandVideo in object VideoManagerAction is called");
        
        // Get Spring context
        ApplicationContext context = getWebApplicationContext();
        // Get videoInformationService bean
        VideoInformationService videoInformationService = (VideoInformationService) context.getBean("videoInformationService");

        //do something about demanding video
        Map<?, ?> map = videoInformationService.demandVideo(videoId,serverId,request);

        //encapsulate videoManagerForm for using in file 'realplayer.jsp'
        videoManagerForm.setDemandedVideoId(Integer.valueOf(map.get("demandedVideoId").toString()));
        videoManagerForm.setVideoURL(map.get("VideoURL").toString());
        videoManagerForm.setInformationName(map.get("informationName").toString());
        videoManagerForm.setVideoId(map.get("videoId").toString());
        videoManagerForm.setBeginTime(Timestamp.valueOf(map.get("beginTime").toString()));
        if(null == map.get("endTime")){
            videoManagerForm.setEndTime(null);
        }else{
            videoManagerForm.setEndTime(Timestamp.valueOf((String)map.get("endTime")));
        }        
        videoManagerForm.setIsBroadcast(Integer.valueOf(map.get("isBroadcast").toString()));
        
        debug("VideoURL is " + videoManagerForm.getVideoURL());
        debug("informationName is " + videoManagerForm.getInformationName());
        debug("videoId is " + videoManagerForm.getVideoId());
        debug("beginTime is " + videoManagerForm.getBeginTime());
        debug("endTime is " + videoManagerForm.getEndTime());
        debug("isBroadcast is " + videoManagerForm.getIsBroadcast());
    }

    /**
     * try to find ip of server whose curShowNum is minimum,
     * and then increase one at column curShowNum,at last insert a row in table 'demandedvideoinfo'
     * @param videoId
     */
    public Integer updateState(Integer videoId,HttpServletRequest request){
        debug("method updateState in class VideoManagerAction is called");
        
        // Get Spring context
        ApplicationContext context = getWebApplicationContext();
        // Get serverInfoService bean
        ServerInfoService serverInfoService = (ServerInfoService) context.getBean("serverInfoService");        
        return serverInfoService.updateState(String.valueOf(videoId),request);   
    }
}