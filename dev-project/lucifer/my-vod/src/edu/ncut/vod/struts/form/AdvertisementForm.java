package edu.ncut.vod.struts.form;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import edu.ncut.vod.commonbeans.ADBean;

public class AdvertisementForm extends ActionForm {

    private static final long serialVersionUID = -2165735995529178095L;

    /** image property */
    private ADBean advertisementImage;

    /** style property */
    private String style;
    
    /** id property */
    private String id;

    /** 
     * Method validate
     * @param mapping
     * @param request
     * @return ActionErrors
     */
    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        debug("validate method in file 'ServerSelectForm.java' is called");
        return null;
    }

    /** 
     * Method reset
     * @param mapping
     * @param request
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        
    }     

    /**
     * @return the advertisementImage
     */
    public ADBean getAdvertisementImage() {
        if(null == advertisementImage){
            advertisementImage = new ADBean();
        }
        return advertisementImage;
    }

    /**
     * @param advertisement the advertisementImage to set
     */
    public void setAdvertisementImage(ADBean advertisementImage) {
        this.advertisementImage = advertisementImage;
    }

    /**
     * @return the style
     */
    public String getStyle() {
        return style;
    }

    /**
     * @param style the style to set
     */
    public void setStyle(String style) {
        this.style = style;
    }
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
    /**
     * console input for the purpose of debugging code
     * @param msg
     */
    public void debug(String msg) {
        System.out.println(msg);
    }
    
}