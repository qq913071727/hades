package edu.ncut.vod.struts.form;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class VideoManagerForm extends ActionForm {

    private static final long serialVersionUID = 1058084820726564388L;

    /** videoId property */
    private String videoId;

    /** videoURL property */
    private String videoURL;
    
    /** informationName property */
    private String informationName;
    
    /** beginTime property */
    private Timestamp beginTime;
    
    /** endTime property */
    private Timestamp endTime;
    
    /** isBroadcast property */
    private Integer isBroadcast;
    
    /** demandedVideoId property */
    private Integer demandedVideoId;

    /** 
     * Method validate
     * @param mapping
     * @param request
     * @return ActionErrors
     */
    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        debug("validate method in file 'VideoManagerForm.java' is called");
        return null;
    }

    /** 
     * Method reset
     * @param mapping
     * @param request
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }

    /**
     * @return the videoId
     */
    public String getVideoId() {
        return videoId;
    }

    /**
     * @param videoId the videoId to set
     */
    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    /**
     * @return the videoURL
     */
    public String getVideoURL() {
        return videoURL;
    }

    /**
     * @param videoURL the videoURL to set
     */
    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    /**
     * Returns the informationName.
     * @return the informationName
     */
    public String getInformationName() {
        return informationName;
    }

    /**
     * Set the informationName.
     * @param informationName the informationName to set
     */
    public void setInformationName(String informationName) {
        this.informationName = informationName;
    }
    
    /**
     * @return the beginTime
     */
    public Timestamp getBeginTime() {
        return beginTime;
    }

    /**
     * @param beginTime the beginTime to set
     */
    public void setBeginTime(Timestamp beginTime) {
        this.beginTime = beginTime;
    }

    /**
     * @return the endTime
     */
    public Timestamp getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the isBroadcast
     */
    public Integer getIsBroadcast() {
        return isBroadcast;
    }

    /**
     * @param isBroadcast the isBroadcast to set
     */
    public void setIsBroadcast(Integer isBroadcast) {
        this.isBroadcast = isBroadcast;
    }

    /**
     * @return the demandedVideoId
     */
    public Integer getDemandedVideoId() {
        return demandedVideoId;
    }

    /**
     * @param demandedVideoId the demandedVideoId to set
     */
    public void setDemandedVideoId(Integer demandedVideoId) {
        this.demandedVideoId = demandedVideoId;
    }

    /**
     * print message on console
     * @param message
     */
    public void debug(String message) {
        System.out.println(message);
    }
}