var blank=".   ";  
var blankTimes=1;  
var blankSize=30;  
var idIndex=0;  
var totalWidth=0;  
var imgpath="/MoonNighTags/images/";  

/* 
 *  加载xml字符串返回XMLDocument对象 
 */  
function loadXML(xmlStr){  
	if(!window.DOMParser && window.ActiveXObject){   //support IE explorer  
		alert("11111111111111");
		var xmlDomVersions = ['MSXML.2.DOMDocument.6.0','MSXML.2.DOMDocument.3.0','Microsoft.XMLDOM'];  
		for(var i=0;i<xmlDomVersions.length;i++){  
			try{  
	      		xmlDoc = new ActiveXObject(xmlDomVersions[i]);
	      	
	      		/*async:规定 XML 文件的下载是否应当被同步处理。*/
	        	xmlDoc.async = false;
	        	/*loadXML():通过解析XML标签字符串来组成文档。*/
	        	xmlDoc.loadXML(xmlStr);
	        	break;
	    	}catch(e){
      		}  
    	}
  	}else if(window.DOMParser && document.implementation && document.implementation.createDocument){  //support Mozilla explorer
	  	try{
	  		/*DOMParser 对象解析 XML 文本并返回一个 XML Document 对象。要使用 DOMParser，使用不带参数的构造函数来实例化它，然后调用其 parseFromString() 方法*/
	    	domParser = new DOMParser();
	    	
	    	/*DOMParser.parseFromString():解析 XML 标记
			语法
			parseFromString(text, contentType)
			text 参数是要解析的 XML 标记。
			contentType 是文本的内容类型。可能是 "text/xml" 、"application/xml" 或 "application/xhtml+xml" 中的一个。注意，不支持 "text/html"。
			返回值
			保存 text 解析后表示的一个 Document 对象。*/ 
	     	xmlDoc = domParser.parseFromString(xmlStr,"text/xml");
	    }catch(e){  
	    }  
	}else{  
		return null;  
	}  
  	return xmlDoc;  
}

/* 
 * 解析xml字符串生成属性结构 
 */  
function xmlParser(xmlStr,textId){
	var childs=null;
	var root=null;
	var xmlDoc=loadXML(xmlStr);
	/*documentElement:返回文档的根节点*/
	root=xmlDoc.documentElement;
	
	/*childNodes:返回元素的子节点的 NodeList*/
	childs=root.childNodes;
	
	/*createElement():创建元素节点。*/
	var topDiv=document.createElement("div");
	alert("topDiv   "+topDiv);
	topDiv.id='topdiv';
	document.body.appendChild(topDiv);
	
	dealWithNode(root,0,topDiv,textId);
	topDiv.style.width='100%';
}

/* 
 * 根据节点及其子节点生成树 
 */  
function dealWithNode(node,blankTimes,container,textId){  
	var children=null;  
	var childCount=0;  
	var isLeaf=false;  //表示节点是否是叶子节点
	              
	var htmlnode=null;  
	var textnode=null;  
	              
	var bl_times=blankTimes;  
	var bl_str="";  
	              
	var node_value="";  
	var node_id="";  
              
	if(node){
	    isLeaf=isLeafNode(node);
	    var str="";
	    var spaceLength=0;
	}
	
    if(idIndex>0){
    	str=getSpaceString(1);
    }
    
    if(bl_times>0){
    	spaceLength=bl_times*blankSize;
    }
    
    node_id=node_value=getAttributValue(node,"id");
    if(!node_id||node_id==""){  
    	node_id=idIndex+1;  
    }
    
    if(isLeaf){
    	if(node.nodeType==3){
        	node_value=node.nodeValue;
        }else{
        	node_value=node.firstChild.nodeValue;
        }
        idIndex++;
        node_value=str+node_value;
        htmlnode=createItem(node_value,node_id,container,isLeaf,textId);
                      
        htmlnode.style.paddingLeft=spaceLength+'px';
    }else{
       	node_value=getAttributValue(node,"value");
        idIndex++;
        node_value=str+node_value;
        htmlnode=createItem(node_value,node_id,container,isLeaf,textId);
        htmlnode.style.paddingLeft=spaceLength+'px';
        var cContainer=createChildContainer("div"+node_id,container);
        cContainer.style.paddingLeft=bl_times*blankSize;
        cContainer.style.width='100%';
        children=node.childNodes;
        if(getBrowser()=="MSIE"){
          	bl_times=1;
        }else{
          	bl_times+=1;
        }
                      
        if(children&&children.length>0){
          	for(var i=0;i<children.length;i++){
               	dealWithNode(children[i],bl_times,cContainer,textId);
            }
        }
    }
}
          
/*
 * 判断节点是否是叶子节点
 */
function isLeafNode(node){
	var children=null;
	if(node){
		/*nodeType:属性可返回节点的节点类型。3:TEXT_NODE*/
		if(node.nodeType==3){
			return true;
		}else{
			children=node.childNodes;
			if(children.length==1&&children[0].nodeType==3){
				return true;
			}else{  
				return false;
			}
		}
	}
	return true;
}
          
/* 
 * 获取自定节点的指定属性值 
 */  
function getAttributValue(node,attrName){
	var attributes=null;
	var attr=null;
	if(node){
		attributes=node.attributes;
		if(attributes&&attributes.length>0){
			for(var attrI=0;attrI<attributes.length;attrI++){
				attr=attributes[attrI];
				if(attr&&attr.nodeName==attrName){
					return attr.nodeValue;
				}
			}
		}
	}
	return null;
}
          
/* 
 * 创建树形结构中每个节点的项 
 */  
function createItem(itemText,idStr,parentContainer,isLeaf,textId){
	var htmlnode=null;
	var textnode=null;
	var imgnode=null;
	var titlenode=null;
	var imgsrc=imgpath+"bg09.png";
	
	if(isLeaf){
		imgsrc=imgpath+"bg10.png";
	}
	
	htmlnode=document.createElement("div");
	htmlnode.id="span"+idStr;
	//htmlnode.href="#";
	//htmlnode.style.paddingLeft="200";
	//htmlnode.expanded="fales";
  
	//图片
	imgnode=document.createElement("img");
	imgnode.setAttribute("src",imgsrc);
	imgnode.setAttribute("flag",isLeaf);
	imgnode.setAttribute("isLeaf",isLeaf);
	myAttachEvent(imgnode,'click',open_closeClick);

	//文字–  
	titlenode=document.createElement("div");  
	titlenode.style.display="inline";  
	titlenode.style.cursor="pointer";  
	titlenode.id=idStr;
	  
	textnode=document.createTextNode(itemText);
	titlenode.appendChild(textnode);
	myAttachEvent(titlenode,'click',function(){
		var srcObj;
		if(typeof(event)!="undefined"){
			srcObj=event.srcElement;
		}else{
			srcObj=this;
		}
		itemClick(srcObj,textId);
	});
              
	htmlnode.appendChild(imgnode);
	htmlnode.appendChild(titlenode);
	              
	parentContainer.appendChild(htmlnode);
	return htmlnode;
}
          
/* 
 * 创建包含节点下子树的容器 
 */  
function createChildContainer(idStr,parentContainer){  
	var cContainer=null;  
	cContainer=document.createElement("div");  
	cContainer.id=idStr;  
	cContainer.style.display="none";  
	cContainer.style.whiteSpace='nowrap';  
	parentContainer.appendChild(cContainer);  
	return cContainer;  
}  
          
/* 
 * 单击图片时触发的事件 
 * 展开或收起该节点的子树 
 */  
function open_closeClick(arg){  
	var srcObj;  
	if(typeof(event)!="undefined"){  
		srcObj=event.srcElement;  
	}else{  
		srcObj=this;  
	}  
	var flag=srcObj.getAttribute("flag");  
	if(srcObj.getAttribute("isLeaf")=="true"){  
		return;
	}
	if(flag=="true"){  
		srcObj.src=imgpath+"bg09.png";  
		srcObj.setAttribute("flag","false")  
	}else{  
		srcObj.src=imgpath+"bg10.png";  
		srcObj.setAttribute("flag","true");  
	}  
             
	var conId=srcObj.parentNode.id.replace("span","div");  
	//alert(conId);  
	var container=document.getElementById(conId);  
	if(container){  
		if(container.style.display=="block"){
			container.style.display="none";
		}else{
			container.style.display="block"; 
		}
	}  
}  
          
/* 
 * 点击树中节点上选项时触发事件时调用该方法 
 * 关闭下拉框 
 */  
function itemClick(srcObj,pid){
	if(window.parent){
	    window.parent.document.getElementById(pid).value=srcObj.innerHTML;
	    window.parent.document.getElementById(pid+'_value').value=srcObj.id;
	    if(window.parent["selector_"+pid]){
	    	window.parent["selector_"+pid].hide();
	    }
    }
}
          
/* 
 * 绑定事件 
 */ 
var myAttachEvent = function(obj, evt, fn){
                    if (obj.addEventListener){
                    	obj.addEventListener(evt, fn, false);
                    }else if (obj.attachEvent){
                    	obj.attachEvent("on" + evt, fn);
                    }
                }
          
function Integration_InterceptCellEvent(id, evt, func){
	if (func.constructor == Function){
    	document.write('<scr' + 'ipt for="' + id + '" event="' + evt + '">'
        	+ 'Array.prototype.push.call(arguments, "' + evt + '");'
            + func.toString().match(/\s*Function\s+(\S*)\s*\(/i)[1]
        	+ '.apply(this, arguments);'
        	+ '</sc' + 'ript>');
    }
}

/* 
 * 返回空格字符串
 */ 
function getSpaceString(strlength){
	var str="";
	for(var strIndex=0;strIndex<strlength;strIndex++){
		str+=" ";
	}
	return str;
}

function getShortFileName(fileName){
	var name="";
	var index=-1;
}
          
/* 
 * 获取浏览器类型 
 */  
function getBrowser(){  
	var OsObject = "";
	if(navigator.userAgent.indexOf("MSIE")>0) {
		return "MSIE";
	}
	if(isFirefox=navigator.userAgent.indexOf("Firefox")>0){ 
		return "Firefox";
	}
	if(isSafari=navigator.userAgent.indexOf("Safari")>0) {
		return "Safari";
	}
	if(isCamino=navigator.userAgent.indexOf("Camino")>0){
		return "Camino";
	}
	if(isMozilla=navigator.userAgent.indexOf("Gecko/")>0){
		return "Gecko";
	}
	return "others";
}