    function closeHandler(cal) {  
      cal.hide();                        // hide the selector  
    }  
    /* 
    function selected(cal, date) { 
      
    } 
    */  
      
    /* 
     * 显示下拉框 
     */  
    function showSelector(id,path,ref) {  
      var selector=window["selector_"+id];  
      var el = document.getElementById(id);  
      if (selector) {  
        // we already have some selector created  
        if(ref&&ref===true)  
        {  
            selector.frame.src=path;  
        }  
        //selector.hide();                 // so we hide it first.  
      } else {  
        // first-time call, create the selector.  
        var selec = new Selector();  
        selec.textId=id;  
        selec.id="selector_"+id;  
        selec.valueId=id+"_value";  
        selec.path=path;  
        // uncomment the following line to hide the week numbers  
        // cal.weekNumbers = false;  
        selector = selec;                  // remember it in the global var  
        selec.create();  
      }  
      selector.sel = el;                 // inform it what input field we use  
      
      // the reference element that we pass to showAtElement is the button that  
      // triggers the selector.  In this example we align the selector bottom-right  
      // to the button.  
      selector.showAtElement(el, "Bl");        // show the selector  
      return false;  
    }  
      
    Selector = function () {  
        // member variables  
        this.activeDiv = null;  
        this.timeout = null;  
        this.dragging = false;  
        this.hidden = false;  
        this.isPopup = true;  
        // HTML elements  
        this.element = null;  
        this.containFrame=null;  
        this.textId=null;  
        this.id=null;  
        this.valueId=null;  
        this.path=null;  
        this.frame=null;  
      
    }  
    Selector.is_ie = ( /msie/i.test(navigator.userAgent) &&!/opera/i.test(navigator.userAgent) );  
    Selector.getAbsolutePos = function(el) {  
        var r = { x: el.offsetLeft, y: el.offsetTop };  
        if (el.offsetParent) {  
            var tmp = Selector.getAbsolutePos(el.offsetParent);  
            r.x += tmp.x;  
            r.y += tmp.y;  
        }  
        //alert(r.x+":"+r.y);  
          
        return r;  
    };  
    Selector.isRelated = function (el, evt) {  
        var related = evt.relatedTarget;  
        if (!related) {  
            var type = evt.type;  
            if (type == "mouseover") {  
                related = evt.fromElement;  
            } else if (type == "mouseout") {  
                related = evt.toElement;  
            }  
        }  
        while (related) {  
            if (related == el) {  
                return true;  
            }  
            related = related.parentNode;  
        }  
        return false;  
    };  
    Selector.removeClass = function(el, className) {  
        if (!(el && el.className)) {  
            return;  
        }  
        var cls = el.className.split(" ");  
        var ar = new Array();  
        for (var i = cls.length; i > 0;) {  
            if (cls[--i] != className) {  
                ar[ar.length] = cls[i];  
            }  
        }  
        el.className = ar.join(" ");  
    };  
    Selector.addClass = function(el, className) {  
        Selector.removeClass(el, className);  
        el.className += " " + className;  
    };  
    Selector.createElement = function(type, parent) {  
        var el = null;  
        if (document.createElementNS) {  
            // use the XHTML namespace; IE won't normally get here unless  
            // _they_ "fix" the DOM2 implementation.  
            el = document.createElementNS("http://www.w3.org/1999/xhtml", type);  
        } else {  
            el = document.createElement(type);  
        }  
        if (typeof parent != "undefined") {  
            parent.appendChild(el);  
        }  
        return el;  
    };  
    /** Calls the second user handler (closeHandler). 
     * 添加事件 
     *  */  
      
    Selector.addEvent = function(el, evname, func) {  
        if (el.attachEvent) { // IE  
            el.attachEvent("on" + evname, func);  
        } else if (el.addEventListener) { // Gecko / W3C  
            el.addEventListener(evname, func, true);  
        } else { // Opera (or old browsers)  
            el["on" + evname] = func;  
        }  
    };  
      
    /* 
     * 删除事件 
     */  
    Selector.removeEvent = function(el, evname, func) {  
        //alert(el.detachEvent);  
        if (el.detachEvent) { // IE  
            el.detachEvent("on" + evname, func);  
        } else if (el.removeEventListener) { // Gecko / W3C  
            el.removeEventListener(evname, func, true);  
        } else { // Opera (or old browsers)  
            el["on" + evname] = null;  
        }  
    };  
    Selector.prototype.create = function (_par) {  
        var parent = null;  
        if (! _par) {  
            // default parent is the document body, in which case we create  
            // a popup selector.  
            parent = document.getElementsByTagName("body")[0];  
            this.isPopup = true;  
        } else {  
            parent = _par;  
            this.isPopup = false;  
        }  
        var div = document.createElement("div");  
        var selectIframe = Selector.createElement("iframe");  
        selectIframe.style.width="100%";  
        selectIframe.style.height="100%";  
        selectIframe.style.display="block";  
        selectIframe.scrolling="auto";  
        selectIframe.style.border="0px";  
        selectIframe.id=this.textId+"_frm";  
        this.containFrame=selectIframe;  
        //selectIframe.style.border="1px";  
        selectIframe.src=this.path;  
        var span=document.createElement("span");  
        span.innerHTML="test";  
        //div.appendChild(span);  
        this.frame=selectIframe;  
        div.appendChild(selectIframe);  
        this.element = div;  
        div.className = "selector";  
        if (this.isPopup) {  
            div.style.position = "absolute";  
            div.style.display = "none";  
        }  
        parent.appendChild(this.element);  
    };  
    Selector.prototype.destroy = function () {  
        var el = this.element.parentNode;  
        el.removeChild(this.element);  
        Selector._C = null;  
    };  
      
    /** 
     *  Hides the Selector.  Also removes any "hilite" from the class of any TD 
     *  element. 
     *  隐藏下拉框 
     */  
    Selector.prototype.hide = function () {  
        if (this.isPopup) {  
            //Selector.removeEvent(document, "keydown", Selector._keyEvent);  
            //Selector.removeEvent(document, "keypress", Selector._keyEvent);  
            Selector.removeEvent(document, "mousedown", function(){window[this.id].hide();});  
        }  
        this.element.style.display = "none";  
        this.hidden = true;  
        this.hideShowCovered();  
    };  
      
    Selector.prototype.showAt = function (x, y) {  
        var s = this.element.style;  
        s.left = x + "px";  
        s.top = y + "px";  
        this.show();  
    };  
    /** 下拉框相对指定控件的显示位置. */  
    Selector.prototype.showAtElement = function (el, opts) {  
        var p = Selector.getAbsolutePos(el);  
        //alert(p.x+'-'+p.y);  
        if (!opts || typeof opts != "string") {  
            this.showAt(p.x, p.y + el.offsetHeight);  
            return true;  
        }  
        this.show();  
        var w = this.element.offsetWidth;  
        var h = this.element.offsetHeight;  
        this.hide();  
        var valign = opts.substr(0, 1);  
        var halign = "l";  
        if (opts.length > 1) {  
            halign = opts.substr(1, 1);  
        }  
        // 垂直对齐方式  
            switch (valign) {  
            case "T": p.y -= h; break;  
            case "B": p.y += el.offsetHeight; break;  
            case "C": p.y += (el.offsetHeight - h) / 2; break;  
            case "t": p.y += el.offsetHeight - h; break;  
            case "b": break; // already there  
            }  
        // 水平对齐方式  
        switch (halign) {  
            case "L": p.x -= w; break;  
            case "R": p.x += el.offsetWidth; break;  
            case "C": p.x += (el.offsetWidth - w) / 2; break;  
            case "r": p.x += el.offsetWidth - w; break;  
            case "l": break; // already there  
        }  
        this.showAt(p.x, p.y);  
    };  
      
    Selector.prototype.show = function () {  
        this.element.style.display = "block";  
        this.hidden = false;  
          
        if (this.isPopup) {  
            window[this.id] = this;  
            var tId=this.id;  
            //Calendar.addEvent(document, "keydown", Calendar._keyEvent);  
            //Calendar.addEvent(document, "keypress", Calendar._keyEvent);  
            Selector.addEvent(document, "mousedown", function(){window[tId].hide();});  
        }  
          
        this.hideShowCovered();  
    };  
      
    Selector.prototype.hideShowCovered = function () {  
        function getStyleProp(obj, style){  
            var value = obj.style[style];  
            if (!value) {  
                if (document.defaultView && typeof (document.defaultView.getComputedStyle) == "function") { // Gecko, W3C  
                    value = document.defaultView.  
                        getComputedStyle(obj, "").getPropertyValue(style);  
                } else if (obj.currentStyle) { // IE  
                    value = obj.currentStyle[style];  
                } else {  
                    value = obj.style[style];  
                }  
            }  
            return value;  
        };  
      
        var tags = new Array("applet", "select");  
        var el = this.element;  
      
        var p = Selector.getAbsolutePos(el);  
        var EX1 = p.x;  
        var EX2 = el.offsetWidth + EX1;  
        var EY1 = p.y;  
        var EY2 = el.offsetHeight + EY1;  
      
        for (var k = tags.length; k > 0; ) {  
            var ar = document.getElementsByTagName(tags[--k]);  
            var cc = null;  
      
            for (var i = ar.length; i > 0;) {  
                cc = ar[--i];  
      
                p = Selector.getAbsolutePos(cc);  
                var CX1 = p.x;  
                var CX2 = cc.offsetWidth + CX1;  
                var CY1 = p.y;  
                var CY2 = cc.offsetHeight + CY1;  
      
                if (this.hidden || (CX1 > EX2) || (CX2 < EX1) || (CY1 > EY2) || (CY2 < EY1)) {  
                    if (!cc.__msh_save_visibility) {  
                        cc.__msh_save_visibility = getStyleProp(cc, "visibility");  
                    }  
                    cc.style.visibility = cc.__msh_save_visibility;  
                } else {  
                    if (!cc.__msh_save_visibility) {  
                        cc.__msh_save_visibility = getStyleProp(cc, "visibility");  
                    }  
                    cc.style.visibility = "hidden";  
                }  
            }  
        }  
    };  