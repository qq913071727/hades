<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
	
<%@ taglib prefix="nest" uri="acca"%>
<%@ taglib prefix="n" uri="acca"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title></title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	</head>
<body>
	<nest:parent id="1">
		<nest:child key="key1" value="value1" />
		<nest:child key="key2" value="value2" />
	</nest:parent>

	<n:parent id="2">
		<n:child key="key3" value="value3" />
		<n:child key="key4" value="value4" />
	</n:parent>
</body>
</html>
