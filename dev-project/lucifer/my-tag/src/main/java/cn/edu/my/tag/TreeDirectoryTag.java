package cn.edu.my.tag;  
  
import java.io.IOException;  
  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.jsp.JspException;  
import javax.servlet.jsp.JspWriter;  
import javax.servlet.jsp.tagext.TagSupport;  
  
/** 
 * 
 * @author lishen 
 * 
 * 树形下拉选择控件 
 * 
 */  
  
public class TreeDirectoryTag extends BaseTag {  
		
		private static final long serialVersionUID = 9878861374414215L;  
      
    //标签name属性  
    private String name;  
      
    //所需图片的路径  
    private String imgPath;  
      
    //所需javascript文件的路径  
    private String scriptPaht;  
      
    //所需css文件的路径  
    private String cssPath;  
      
    //项目的根路径  
    private String rootPath;  
      
    //标签的value属性  
    private String value;  
    private String text;  
    private String path;  
      
    /* 
     * 标签的actionUrl属性 
     * 联想查询结果数据通过向actionUrl属性指定的url请求得到 
     */  
    private String actionUrl;  
      
    private HttpServletRequest request=null;  
      
      
    public String getActionUrl() {  
        return actionUrl;  
    }  
    public void setActionUrl(String actionUrl) {  
        this.actionUrl = actionUrl;  
    }  
    public String getValue() {  
        return value;  
    }  
    public void setValue(String value) {  
        this.value = value;  
    }  
    public String getImgPath() {  
        return imgPath;  
    }  
    public void setImgPath(String imgPath) {  
        this.imgPath = imgPath;  
    }  
    public String getScriptPaht() {  
        return scriptPaht;  
    }  
    public void setScriptPaht(String scriptPaht) {  
        this.scriptPaht = scriptPaht;  
    }  
    public String getCssPath() {  
        return cssPath;  
    }  
    public void setCssPath(String cssPath) {  
        this.cssPath = cssPath;  
    }  
    public String getText() {  
        return text;  
    }  
    public void setText(String text) {  
        this.text = text;  
    }  
    public String getName() {  
        return name;  
    }  
    public void setName(String name) {  
        this.name = name;  
    }  
    
    public TreeDirectoryTag(){  
    } 
    
    /** 
     * 初始化变量 
     */  
    private void initAbttributes(){  
        request=(HttpServletRequest)this.pageContext.getRequest();  
        rootPath=request.getContextPath();  
        this.imgPath="/images/";  
        this.scriptPaht="/js/";  
        this.cssPath="/css/";  
    }  
    
    @Override  
    public int doStartTag() throws JspException {  
        initAbttributes();  
        path=rootPath+"/jsp/tags/treeSelectorPage.jsp?id="+id+"&actionUrl="+actionUrl;  
        JspWriter out=pageContext.getOut();  
        try {  
            String tName=name;  
            //引入javascript文件  
            out.println("<script type='text/javascript' charset='GB2312' src='"+rootPath+scriptPaht+"selector.js'></script>");  
              
            //引入css文件  
            out.println("<link rel='stylesheet' href='"+rootPath+cssPath+"selector.css' type='text/css' />");  
              
            StringBuilder tag=new StringBuilder("<input type='text' ");  
            tag.append("id='").append(id).append("'");  
            tag.append(" value='").append(text==null?"":text).append("'");  
            tag.append(" onclick='return showSelector(\"");  
            tag.append(id).append("\",\"").append(path).append("\")' readonly>");  
            tag.append("<input type='hidden' name='")  
            .append(tName).append("' id='").append(id).append("_value")  
            .append("' value='").append(value==null?"":value).append("'>");  
            out.println(tag.toString());  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return SKIP_BODY;  
    }  
}  