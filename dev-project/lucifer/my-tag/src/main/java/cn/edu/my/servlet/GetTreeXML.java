    package cn.edu.my.servlet;  
      
    import java.io.IOException;  
    import java.io.PrintWriter;  
      
    import javax.servlet.ServletException;  
    import javax.servlet.http.HttpServlet;  
    import javax.servlet.http.HttpServletRequest;  
    import javax.servlet.http.HttpServletResponse;  
      
    public class GetTreeXML extends HttpServlet {  
      
        /** 
         * 
         */  
        private static final long serialVersionUID = 1L;  
      
        @Override  
        protected void doGet(HttpServletRequest req, HttpServletResponse resp)  
                throws ServletException, IOException {  
            this.doPost(req, resp);  
        }  
      
        @Override  
        protected void doPost(HttpServletRequest req, HttpServletResponse resp)  
                throws ServletException, IOException {
            
            resp.setCharacterEncoding("UTF-8");  
            resp.setContentType("UTF-8");  
            PrintWriter out=resp.getWriter();  
            StringBuilder sb=new StringBuilder();  
            sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");  
            sb.append("<xmlRoot value=\"中国\" id=\"china\">");  
            sb.append("<child value=\"四川\" id=\"sc\">");  
            sb.append("<child id=\"ctu\">成都</child>");  
            sb.append("</child><child id=\"gz\">贵州</child></xmlRoot>");  
            out.write(sb.toString());
            //System.out.println(sb.toString());
            out.flush();  
        }  
    }  