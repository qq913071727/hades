package cn.edu.my.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class MenuTag extends BaseTag {
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String classStyle;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClassStyle() {
		return classStyle;
	}

	public void setClassStyle(String classStyle) {
		this.classStyle = classStyle;
	}

	public int doStartTag() throws JspException {
		logger.debug("MenuTag.doStartTag is invoked");
		JspWriter out=pageContext.getOut();
		try {
			out.write("<nav id='"+id+"'><ul class='"+classStyle+"'>");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return EVAL_BODY_INCLUDE;
	}

	public int doEndTag() throws JspException {
		JspWriter out=pageContext.getOut();
		try {
			out.write("</ul></nav>");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return EVAL_PAGE;
	}
}
