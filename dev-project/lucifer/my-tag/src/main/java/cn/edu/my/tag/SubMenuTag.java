package cn.edu.my.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class SubMenuTag extends BaseTag {
	private static final long serialVersionUID = 1L;
	
	private String classStyle;
	private String href;
	private String text;
	
	public String getClassStyle() {
		return classStyle;
	}

	public void setClassStyle(String classStyle) {
		this.classStyle = classStyle;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getText() {
		return text;
	} 

	public void setText(String text) {
		this.text = text;
	}

	public int doStartTag() throws JspException {
		System.out.println("111111111111111111111111111");
		JspWriter out=pageContext.getOut();
		try {
			out.write("<li><span class='"+classStyle+"'></span><a href='"+href+"'>"+text+"</a><dl>");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return EVAL_BODY_INCLUDE;
	}

	public int doEndTag() throws JspException {
		System.out.println("0000000000000000000000000000000000");
		JspWriter out=pageContext.getOut();
		try {
			out.write("</dl></li>");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return EVAL_PAGE;
	}
}
