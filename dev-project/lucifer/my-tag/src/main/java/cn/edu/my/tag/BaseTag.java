package cn.edu.my.tag;

import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

public class BaseTag extends TagSupport {
	protected final Logger logger = Logger.getLogger(getClass());
}
