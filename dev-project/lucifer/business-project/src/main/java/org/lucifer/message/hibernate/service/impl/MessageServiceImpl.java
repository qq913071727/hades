package org.lucifer.message.hibernate.service.impl;

import java.util.List;

import org.lucifer.message.hibernate.DaoFactory;
import org.lucifer.message.hibernate.dao.MessageDao;
import org.lucifer.message.hibernate.model.Message;
import org.lucifer.message.hibernate.service.MessageService;


public class MessageServiceImpl implements MessageService {
	private MessageDao message;

	public void deleteMessage(int id) {
		message=DaoFactory.getMessageDao();
		message.delete(id);
	}

	public int addMessage(Message m) {
		message=DaoFactory.getMessageDao();
		message.save(m);
		return m.getId();
	}

	public List<Message> findAllMessage() {
		message=DaoFactory.getMessageDao();
		List<Message> l=message.getMessages();
		return l;
	}

	public List<Message> getMessagesByKeyWord(String keyWord) {
		message=DaoFactory.getMessageDao();
		List<Message> l=message.getMessagesByKeyWord(keyWord);
		return l;
	}
	
	public void update(Message m){
		message=DaoFactory.getMessageDao();	
		message.update(m);
	}

}
