package org.lucifer.message.hibernate.service;

import java.util.List;

import org.lucifer.message.hibernate.model.Message;


public interface MessageService {
	void deleteMessage(int id);

	int addMessage(Message m);
	
	List<Message> findAllMessage();
	
	List<Message> getMessagesByKeyWord(String keyWord);
	
	void update(Message m);
}
