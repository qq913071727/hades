package org.lucifer.message.hibernate;

import org.lucifer.message.hibernate.dao.MessageDao;
import org.lucifer.message.hibernate.dao.impl.MessageDaoHibernate;

public class DaoFactory {
	private static MessageDao messageDao;

	public static MessageDao getMessageDao() {
		if(null==messageDao){
			messageDao=new MessageDaoHibernate();
		}
		return messageDao;
	}

	public static void setMessageDao(MessageDao messageDao) {
		DaoFactory.messageDao = messageDao;
	}
   
}
