package org.lucifer.task.ibatis.dao.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;


import org.lucifer.task.ibatis.dao.TaskDao;
import org.lucifer.task.ibatis.model.Task;

import com.ibatis.sqlmap.client.SqlMapClient;

public class TaskDaoIbatis extends SqlMapClientDaoSupport implements TaskDao/*,
		BeanPostProcessor*/ {
	private SimpleJdbcTemplate simpleJdbcTemplate;

//	public Object postProcessAfterInitialization(Object arg0, String arg1)
//			throws BeansException {
//		return null;
//	}
//
//	public Object postProcessBeforeInitialization(Object arg0, String arg1)
//			throws BeansException {
//		return null;
//	}

	/**
	 * Spring面向切面编程：<aop:config>，<aop:aspect>和<aop:pointcut>
	 */
	public void save(Task v) {
		this.getSqlMapClientTemplate().insert("insert", v);
	}

	public void delete(Integer id) {		
		this.getSqlMapClientTemplate().delete("delete", id);
	}

	public Task getTask(Integer id) {
		Task task = (Task) this.getSqlMapClientTemplate().queryForObject("getTask",id);
		return task;
	}

	@SuppressWarnings("unchecked")
	public List<Task> getTasks() {
		return getSqlMapClientTemplate().queryForList("getTasks");
	}
	
	/** 
	 * 使用类ClassPathXmlApplicationContext注入bean
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<HashMap> getTasksByKeyWord(String keyWord) {
		ApplicationContext context=new ClassPathXmlApplicationContext("taskBean.xml");
		SqlMapClient sqlMapClient=(SqlMapClient) context.getBean("sqlMapClient");
		List<HashMap> list=null;
		try {
			list=sqlMapClient.queryForList("getTasksByKeyWord",keyWord);
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
		
		//return getSqlMapClientTemplate().queryForList("getTasksByKeyWord",keyWord);
	}

	public void update(Task task) {
		String sql="update TASK t set t.RECORD_TIME=? ,t.BEGINNING_TIME=? ,t.ENDING_TIME=? ,t.DESCRIPTION=? ,t.TYPE=? ,t.SOLVED=? where t.ID=?";
		simpleJdbcTemplate.update(sql,task.getRecordTime(),task.getBeginningTime(),task.getEndingTime(),task.getDescription(),task.getType(),task.getSolved(),task.getId());
		
		//getSqlMapClientTemplate().update("update",task);
	}
	
	public SimpleJdbcTemplate getSimpleJdbcTemplate() {
		return simpleJdbcTemplate;
	}

	//@Autowired
	public void setSimpleJdbcTemplate(SimpleJdbcTemplate simpleJdbcTemplate) {
		this.simpleJdbcTemplate = simpleJdbcTemplate;
	}
}