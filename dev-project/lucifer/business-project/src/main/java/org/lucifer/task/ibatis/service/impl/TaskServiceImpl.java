package org.lucifer.task.ibatis.service.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.lucifer.task.ibatis.dao.TaskDao;
import org.lucifer.task.ibatis.model.Task;
import org.lucifer.task.ibatis.service.TaskService;

import javax.inject.Inject;
import javax.inject.Named;

public class TaskServiceImpl implements TaskService/*,BeanPostProcessor*/,InitializingBean,BeanNameAware,BeanFactoryAware,ApplicationContextAware,DisposableBean  {
	
	/*@Autowired(required=false)
	@Qualifier("taskDao")*/
	/*@Inject
	@Named("taskDao")*/
	private TaskDao taskDao;
	private String beanName;
	private BeanFactory beanFactory;
	private ApplicationContext applicationContext;
	
	private TaskServiceImpl(){	  
		//System.out.println("*****************************TaskServiceImpl");
	}

	private static class TaskServiceImplSingletonHolder{
		static TaskServiceImpl instance=new TaskServiceImpl();
	}

	public static TaskServiceImpl getInstance(){
		//System.out.println("*********************************getInstance"+TaskServiceImplSingletonHolder.instance);
		return TaskServiceImplSingletonHolder.instance;
	}
	
	public TaskDao getTaskDao() {
		//System.out.println(getClass()+"****************************getTaskDao");
		return taskDao;
	}

	
	public void setTaskDao(TaskDao taskDao) {
		//System.out.println("****************************************"+taskDao.getClass());
		this.taskDao = taskDao;
	}

	public Task getTask(Integer id) {
		return taskDao.getTask(id);
	}

	public void save(Task t) {
		taskDao.save(t);
	}

	public List<Task> getTasks() {
		return taskDao.getTasks();
	}

	@SuppressWarnings("rawtypes")
	public List<HashMap> getTasksByKeyWord(String keyWord) {
		return taskDao.getTasksByKeyWord(keyWord);
	}

	public void delete(Integer id) {
		taskDao.delete(id);
	}

	public void update(Task task) {
		taskDao.update(task);
	}
	
	public void initMethod(){
		//System.out.println(getClass()+"*************************************initMethod");
	}
	
	public void destroyMethod(){
		//System.out.println(getClass()+"************************************destroyMethod");
	}

	@Override
	public void setBeanName(String beanName) {
		this.beanName=beanName;
		System.out.println("*************setBeanName	"+beanName);
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory=beanFactory;
		//System.out.println(getClass()+"****************************************setBeanFactory");
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)throws BeansException {
		this.applicationContext=applicationContext;
		//System.out.println(getClass()+"****************************************setApplicationContext");
	}
	
//	public Object postProcessAfterInitialization(Object arg0, String arg1)
//		throws BeansException {
//		System.out.println(getClass()+"***********************************postProcessAfterInitialization");
//		return null;
//	}
//	
//	public Object postProcessBeforeInitialization(Object arg0, String arg1)
//		throws BeansException {
//		System.out.println(getClass()+"*************************************postProcessBeforeInitialization");
//		return null;
//	}

	@Override
	public void afterPropertiesSet() throws Exception {
		//System.out.println(getClass()+"******************************************afterPropertiesSet");
	}

	@Override
	public void destroy() throws Exception {
		//System.out.println(getClass()+"*****************************************destroy");
	}

}
