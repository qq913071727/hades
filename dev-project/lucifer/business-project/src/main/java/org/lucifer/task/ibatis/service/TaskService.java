package org.lucifer.task.ibatis.service;

import java.util.HashMap;
import java.util.List;

import org.lucifer.task.ibatis.model.Task;

public interface TaskService {
	Task getTask(Integer id);
	void save(Task t);
	List<Task> getTasks();
	List<HashMap> getTasksByKeyWord(String keyWord);
	void delete(Integer id);
	void update(Task task);
}
