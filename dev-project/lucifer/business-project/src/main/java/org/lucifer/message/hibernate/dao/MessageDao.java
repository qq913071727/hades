package org.lucifer.message.hibernate.dao;

import java.util.List;

import org.lucifer.message.hibernate.model.Message;

public interface MessageDao {
	void save(Message m);

    void delete(int id);
    
    Message getMessage(Integer id);

    @SuppressWarnings("rawtypes")
	List getMessages();
    
    @SuppressWarnings("rawtypes")
	List getMessagesByKeyWord(String keyWord);
    
    void update(Message m);
}
