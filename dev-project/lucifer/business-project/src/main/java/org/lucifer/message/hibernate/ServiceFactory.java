package org.lucifer.message.hibernate;

import org.lucifer.message.hibernate.service.MessageService;
import org.lucifer.message.hibernate.service.impl.MessageServiceImpl;

public class ServiceFactory {
	private static MessageService messageService;

	public static MessageService getMessageService() {
		if(null==messageService){
			messageService=new MessageServiceImpl();
		}
		return messageService;
	}

	@SuppressWarnings("static-access")
	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	
	
}
