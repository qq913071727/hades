package org.lucifer.task.ibatis.dao;

import java.util.HashMap;
import java.util.List;

import org.lucifer.task.ibatis.model.Task;

public interface TaskDao {
	void save(Task v);

    void delete(Integer id);

    Task getTask(Integer id);

    List<Task> getTasks();
    
    @SuppressWarnings("rawtypes")
	List<HashMap> getTasksByKeyWord(String keyWord);
    
    void update(Task task);
}
