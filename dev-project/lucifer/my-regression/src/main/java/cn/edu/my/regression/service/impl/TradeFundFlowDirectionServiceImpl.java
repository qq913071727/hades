package cn.edu.my.regression.service.impl;

import cn.edu.my.regression.model.TradeFundFlowDirection;
import cn.edu.my.regression.service.TradeFundFlowDirectionService;
import cn.edu.my.regression.dao.TradeFundFlowDirectionDao;
import cn.edu.my.regression.dao.impl.TradeFundFlowDirectionDaoImpl;

import java.util.List;

public class TradeFundFlowDirectionServiceImpl implements TradeFundFlowDirectionService{
	private TradeFundFlowDirectionDao tradeFundFlowDirectionDao=new TradeFundFlowDirectionDaoImpl();
	
	public List<TradeFundFlowDirection> readTradeFundFlowDirectionList(){
		return tradeFundFlowDirectionDao.readTradeFundFlowDirectionList();
	}
}