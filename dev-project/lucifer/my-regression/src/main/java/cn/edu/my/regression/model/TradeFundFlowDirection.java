package cn.edu.my.regression.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TRADE_FUND_FLOW_DIRECTION")
public class TradeFundFlowDirection implements Serializable{
	@Id
	@Column(name="TRADE_DATE")
	private Date tradeDate;
	@Id
	@Column(name="TRADE_NAME")
	private String tradeName;
	@Column(name="UDR")
	private double upDownRange;
	@Column(name="MFNIA")
	private double mainForceNetInflowAmount;
	@Column(name="MFNIP")
	private double mainForceNetInflowPercentage;
	@Column(name="SLSNIA")
	private double superLargeSheetNetInflowAmount;
	@Column(name="SLSNIP")
	private double superLargeSheetNetInflowPercentage;
	@Column(name="LSNIA")
	private double largeSheetNetInflowAmount;
	@Column(name="LSNIP")
	private double largeSheetNetInflowPercentage;
	@Column(name="MSNIA")
	private double middleSheetNetInflowAmount;
	@Column(name="MSNIP")
	private double middleSheetNetInflowPercentage;
	@Column(name="SSNIA")
	private double smallSheetNetInflowAmount;
	@Column(name="SSNIP")
	private double smallSheetNetInflowPercentage;
	@Column(name="MFNILS")
	private String mainForceNetInflowLargestStock;
	
	public Date getTradeDate() {
		return tradeDate;
	}
	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}
	public String getTradeName() {
		return tradeName;
	}
	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}
	public double getUpDownRange() {
		return upDownRange;
	}
	public void setUpDownRange(double upDownRange) {
		this.upDownRange = upDownRange;
	}
	public double getMainForceNetInflowAmount() {
		return mainForceNetInflowAmount;
	}
	public void setMainForceNetInflowAmount(double mainForceNetInflowAmount) {
		this.mainForceNetInflowAmount = mainForceNetInflowAmount;
	}
	public double getMainForceNetInflowPercentage() {
		return mainForceNetInflowPercentage;
	}
	public void setMainForceNetInflowPercentage(double mainForceNetInflowPercentage) {
		this.mainForceNetInflowPercentage = mainForceNetInflowPercentage;
	}
	public double getSuperLargeSheetNetInflowAmount() {
		return superLargeSheetNetInflowAmount;
	}
	public void setSuperLargeSheetNetInflowAmount(
			double superLargeSheetNetInflowAmount) {
		this.superLargeSheetNetInflowAmount = superLargeSheetNetInflowAmount;
	}
	public double getSuperLargeSheetNetInflowPercentage() {
		return superLargeSheetNetInflowPercentage;
	}
	public void setSuperLargeSheetNetInflowPercentage(
			double superLargeSheetNetInflowPercentage) {
		this.superLargeSheetNetInflowPercentage = superLargeSheetNetInflowPercentage;
	}
	public double getLargeSheetNetInflowAmount() {
		return largeSheetNetInflowAmount;
	}
	public void setLargeSheetNetInflowAmount(double largeSheetNetInflowAmount) {
		this.largeSheetNetInflowAmount = largeSheetNetInflowAmount;
	}
	public double getLargeSheetNetInflowPercentage() {
		return largeSheetNetInflowPercentage;
	}
	public void setLargeSheetNetInflowPercentage(
			double largeSheetNetInflowPercentage) {
		this.largeSheetNetInflowPercentage = largeSheetNetInflowPercentage;
	}
	public double getMiddleSheetNetInflowAmount() {
		return middleSheetNetInflowAmount;
	}
	public void setMiddleSheetNetInflowAmount(double middleSheetNetInflowAmount) {
		this.middleSheetNetInflowAmount = middleSheetNetInflowAmount;
	}
	public double getMiddleSheetNetInflowPercentage() {
		return middleSheetNetInflowPercentage;
	}
	public void setMiddleSheetNetInflowPercentage(
			double middleSheetNetInflowPercentage) {
		this.middleSheetNetInflowPercentage = middleSheetNetInflowPercentage;
	}
	public double getSmallSheetNetInflowAmount() {
		return smallSheetNetInflowAmount;
	}
	public void setSmallSheetNetInflowAmount(double smallSheetNetInflowAmount) {
		this.smallSheetNetInflowAmount = smallSheetNetInflowAmount;
	}
	public double getSmallSheetNetInflowPercentage() {
		return smallSheetNetInflowPercentage;
	}
	public void setSmallSheetNetInflowPercentage(
			double smallSheetNetInflowPercentage) {
		this.smallSheetNetInflowPercentage = smallSheetNetInflowPercentage;
	}
	public String getMainForceNetInflowLargestStock() {
		return mainForceNetInflowLargestStock;
	}
	public void setMainForceNetInflowLargestStock(
			String mainForceNetInflowLargestStock) {
		this.mainForceNetInflowLargestStock = mainForceNetInflowLargestStock;
	}
}