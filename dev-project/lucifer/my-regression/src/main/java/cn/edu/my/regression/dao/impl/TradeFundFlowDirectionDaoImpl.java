package cn.edu.my.regression.dao.impl;

import cn.edu.my.regression.dao.TradeFundFlowDirectionDao;
import cn.edu.my.regression.model.TradeFundFlowDirection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import java.util.List;

public class TradeFundFlowDirectionDaoImpl implements TradeFundFlowDirectionDao{
	public List<TradeFundFlowDirection> readTradeFundFlowDirectionList(){
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("tradeFundFlowDirection");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();
		Query query = em.createQuery("select t from TradeFundFlowDirection t");
		List<TradeFundFlowDirection> tradeFundFlowDirectionList = (List<TradeFundFlowDirection>) query
				.getResultList();
		em.getTransaction().commit();
		emf.close();

		return tradeFundFlowDirectionList;
	}
}