package cn.edu.my.regression.nonlinear;

public class NonlinearRegressionDataPoint{
	private double x;						//x的值
	private double y;						//y的值
	
	public NonlinearRegressionDataPoint(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public NonlinearRegressionDataPoint() {
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
}