package cn.edu.my.regression.linear.unary;

public class DataPoint {
	private double x;// the x value
	private double y;// the y value

	/**
	 * Constructor.
	 * 
	 * @param x
	 *            the x value
	 * @param y
	 *            the y value
	 */
	public DataPoint(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public DataPoint() {
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
}
