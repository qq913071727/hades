package cn.edu.my.regression.dao;

import java.util.List;

import cn.edu.my.regression.model.TradeFundFlowDirection;

public interface TradeFundFlowDirectionDao{
	List<TradeFundFlowDirection> readTradeFundFlowDirectionList();
}