package cn.edu.my.regression.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import cn.edu.my.regression.linear.multivariate.MultivariateDataPoint;
import cn.edu.my.regression.linear.multivariate.MultivariateLinearRegression;
import cn.edu.my.regression.linear.unary.UnaryDataPoint;
import cn.edu.my.regression.linear.unary.UnaryLinearRegression;
import cn.edu.my.regression.nonlinear.ExponentialCurveModel;
import cn.edu.my.regression.nonlinear.LogarithmicCurveModel;
import cn.edu.my.regression.nonlinear.HyperbolaModel;
import cn.edu.my.regression.nonlinear.PowerFunctionCurveModel;
import cn.edu.my.regression.nonlinear.ParabolaModel;

public class TestRegression {
	public static void main(String[] args) {
		//testUnaryLinearRegression();
		//testMultivariateLinearRegression();
		//testExponentialCurveModel();
		//testLogarithmicCurveModel();
		//testHyperbolaModel();
		//testPowerFunctionCurveModel();
		testParabolaModel();
	}
	
	private static void testParabolaModel(){
		ParabolaModel pm=new ParabolaModel();
		pm.obtainParabolaModel();
	}
	
	private static void testPowerFunctionCurveModel(){
		PowerFunctionCurveModel pfc=new PowerFunctionCurveModel();
		pfc.obtainPowerFunctionCurveModel();
	}
	
	private static void testHyperbolaModel(){
		HyperbolaModel hm=new HyperbolaModel();
		hm.obtainHyperbolaModel();
	}
	
	private static void testLogarithmicCurveModel(){
		LogarithmicCurveModel lcm=new LogarithmicCurveModel();
		lcm.obtainLogarithmicCurveModel();
	}
	
	private static void testExponentialCurveModel(){
		ExponentialCurveModel ec=new ExponentialCurveModel();
		ec.obtainExponentialCurveModel();
	}
	
	private static void testMultivariateLinearRegression(){
		MultivariateLinearRegression mlr=new MultivariateLinearRegression();
		mlr.obtainMultivariateLinearRegressionModel();
	}
	
	private static void testUnaryLinearRegression(){
		UnaryLinearRegression ulr=new UnaryLinearRegression();
		ulr.obtainUnaryLinearRegression();
	}
}
