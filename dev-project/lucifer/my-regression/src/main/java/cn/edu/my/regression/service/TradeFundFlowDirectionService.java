package cn.edu.my.regression.service;

import cn.edu.my.regression.model.TradeFundFlowDirection;

import java.util.List;

public interface TradeFundFlowDirectionService{
	List<TradeFundFlowDirection> readTradeFundFlowDirectionList();
}