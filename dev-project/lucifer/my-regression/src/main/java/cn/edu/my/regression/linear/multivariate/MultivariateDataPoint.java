package cn.edu.my.regression.linear.multivariate;

public class MultivariateDataPoint {
	private double dependentVariable;	//因变量
	private double[] argument;			//自变量
	private double[] coefficient;		//系数
	
	public MultivariateDataPoint() {
	}
	public MultivariateDataPoint(double dependentVariable, double[] argument,
			double[] coefficient) {
		super();
		this.dependentVariable = dependentVariable;
		this.argument = argument;
		this.coefficient = coefficient;
	}
	
	public double getDependentVariable() {
		return dependentVariable;
	}
	public void setDependentVariable(double dependentVariable) {
		this.dependentVariable = dependentVariable;
	}
	public double[] getArgument() {
		return argument;
	}
	public void setArgument(double[] argument) {
		this.argument = argument;
	}
	public double[] getCoefficient() {
		return coefficient;
	}
	public void setCoefficient(double[] coefficient) {
		this.coefficient = coefficient;
	}
}
