package cn.edu.my.jsoup.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import cn.edu.my.jsoup.dao.TradeFundFlowDirectionWriterDao;
import cn.edu.my.jsoup.dao.impl.TradeFundFlowDirectionWriterDaoImpl;
import cn.edu.my.jsoup.model.TradeFundFlowDirection;
import cn.edu.my.jsoup.service.TradeFundFlowDirectionWriterService;

public class TradeFundFlowDirectionWriterServiceImpl implements TradeFundFlowDirectionWriterService {
	private TradeFundFlowDirectionWriterDao dataWriterDao=new TradeFundFlowDirectionWriterDaoImpl();
	
	public void writeData(List<TradeFundFlowDirection> tradeFundFlowDirectionList) {
		dataWriterDao.writeData(tradeFundFlowDirectionList);
	}

}
