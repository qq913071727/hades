package cn.edu.my.jsoup.dao;

import java.util.List;

import cn.edu.my.jsoup.model.base.BaseTrade;

public interface TradeWriterDao {
	void writeData(List<BaseTrade> baseTradeList);
}
