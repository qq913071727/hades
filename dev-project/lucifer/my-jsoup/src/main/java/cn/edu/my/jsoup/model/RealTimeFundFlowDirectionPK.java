package cn.edu.my.jsoup.model;

import java.util.Date;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Id;

public class RealTimeFundFlowDirectionPK implements Serializable{
	@Id
	private String stockCode;
	@Id
	private Date stockDate;
	
	public RealTimeFundFlowDirectionPK() {
	}
	
	public RealTimeFundFlowDirectionPK(String stockCode, Date stockDate) {
		this.stockCode = stockCode;
		this.stockDate = stockDate;
	}
	
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public Date getStockDate() {
		return stockDate;
	}
	public void setStockDate(Date stockDate) {
		this.stockDate = stockDate;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((stockDate == null) ? 0 : stockDate.hashCode());
		result = prime * result
				+ ((stockCode == null) ? 0 : stockCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RealTimeFundFlowDirectionPK other = (RealTimeFundFlowDirectionPK) obj;
		if (stockDate == null) {
			if (other.stockDate != null)
				return false;
		} else if (!stockDate.equals(other.stockDate))
			return false;
		if (stockCode == null) {
			if (other.stockCode != null)
				return false;
		} else if (!stockCode.equals(other.stockCode))
			return false;
		return true;
	}
}