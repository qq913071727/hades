package cn.edu.my.jsoup.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import cn.edu.my.jsoup.dao.RealTimeFundFlowDirectionWriterDao;
import cn.edu.my.jsoup.dao.impl.RealTimeFundFlowDirectionWriterDaoImpl;
import cn.edu.my.jsoup.model.RealTimeFundFlowDirection;
import cn.edu.my.jsoup.service.RealTimeFundFlowDirectionWriterService;

public class RealTimeFundFlowDirectionWriterServiceImpl implements RealTimeFundFlowDirectionWriterService {
	private RealTimeFundFlowDirectionWriterDao dataWriterDao=new RealTimeFundFlowDirectionWriterDaoImpl();
	
	public void writeData(List<RealTimeFundFlowDirection> realTimeFundFlowDirection) {
		dataWriterDao.writeData(realTimeFundFlowDirection);
	}

}