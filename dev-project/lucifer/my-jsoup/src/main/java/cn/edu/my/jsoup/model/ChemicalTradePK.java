package cn.edu.my.jsoup.model;

import java.util.Date;

import javax.persistence.Id;

public class ChemicalTradePK {
	@Id
	private int stockCode;
	@Id
	private Date stockDate;
	
	public ChemicalTradePK(){
		
	}

	public ChemicalTradePK(int stockCode, Date stockDate) {
		super();
		this.stockCode = stockCode;
		this.stockDate = stockDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + stockCode;
		result = prime * result
				+ ((stockDate == null) ? 0 : stockDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChemicalTradePK other = (ChemicalTradePK) obj;
		if (stockCode != other.stockCode)
			return false;
		if (stockDate == null) {
			if (other.stockDate != null)
				return false;
		} else if (!stockDate.equals(other.stockDate))
			return false;
		return true;
	}
}
