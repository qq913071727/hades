package cn.edu.my.jsoup.service;

import java.util.List;

import cn.edu.my.jsoup.model.RealTimeFundFlowDirection;

public interface RealTimeFundFlowDirectionWriterService {
	void writeData(List<RealTimeFundFlowDirection> realTimeFundFlowDirection);
}