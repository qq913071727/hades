package cn.edu.my.jsoup;

/*
 * http://outofmemory.cn/code-snippet/2823/usage-Jsoup-zhuaqu-web-site-data-example
 * http://jsoup.org/apidocs/
 * http://blog.csdn.net/geekjoker/article/details/6446472
 * http://stackoverflow.com/questions/16672939/hibernate-jpa-maven
 * http://tomrose.iteye.com/blog/212427
 * http://blog.csdn.net/tjcyjd/article/details/6925105
 * */

import java.util.List;

import cn.edu.my.jsoup.service.TradeFundFlowDirectionReaderService;
import cn.edu.my.jsoup.service.TradeFundFlowDirectionWriterService;
import cn.edu.my.jsoup.service.TradeReaderService;
import cn.edu.my.jsoup.service.TradeWriterService;
import cn.edu.my.jsoup.service.RealTimeFundFlowDirectionReaderService;
import cn.edu.my.jsoup.service.RealTimeFundFlowDirectionWriterService;
import cn.edu.my.jsoup.service.impl.TradeFundFlowDirectionWriterServiceImpl;
import cn.edu.my.jsoup.service.impl.TradeFundFlowDirectionReaderServiceImpl;
import cn.edu.my.jsoup.service.impl.TradeReaderServiceImpl;
import cn.edu.my.jsoup.service.impl.TradeWriterServiceImpl;
import cn.edu.my.jsoup.service.impl.RealTimeFundFlowDirectionReaderServiceImpl;
import cn.edu.my.jsoup.service.impl.RealTimeFundFlowDirectionWriterServiceImpl;
import cn.edu.my.jsoup.model.ChemicalTrade;
import cn.edu.my.jsoup.model.TradeFundFlowDirection;
import cn.edu.my.jsoup.model.RealTimeFundFlowDirection;

public class Main {
	public static void main(String[] args) {
		//insertAllTrade();
		insertTradeFundFlowDirection();
		insertRealTimeFundFlowDirection();
	}

	private static void insertTradeFundFlowDirection() {
		System.out.println("*********************************************************** 插入行业资金流向数据");
		TradeFundFlowDirectionReaderService tradeFundFlowDirectionReaderService = new TradeFundFlowDirectionReaderServiceImpl();
		TradeFundFlowDirectionWriterService tradeFundFlowDirectionWriterService = new TradeFundFlowDirectionWriterServiceImpl();

		List<TradeFundFlowDirection> tradeFundFlowDirection = tradeFundFlowDirectionReaderService
				.readHttp("http://data.eastmoney.com/bkzj/hy.html");
		tradeFundFlowDirectionWriterService.writeData(tradeFundFlowDirection);
	}

	private static void insertAllTrade() {
		TradeReaderService tradeReaderService = new TradeReaderServiceImpl();
		TradeWriterService tradeWriterService=new TradeWriterServiceImpl();
		
		List tradeHrefList = tradeReaderService.getTradeHref("http://data.eastmoney.com/bkzj/hy.html");
		for(int i=0; i<tradeHrefList.size(); i++){
			tradeWriterService.writeData(tradeReaderService.readHttp("http://data.eastmoney.com/Bkzj/data.aspx?type=stockFundflow&bkCode="+Integer.parseInt(tradeHrefList.get(i).toString())+"&page=2&pageSize=50&sortType=D&sortRule=-1&jsname=vXWSdhBxdJ&day=1&bkType=2&rt=46507855"));
		}
	}
	
	private static void insertRealTimeFundFlowDirection(){
		System.out.println("*********************************************************** 插入实时资金流向数据");
		RealTimeFundFlowDirectionReaderService realTimeFundFlowDirectionReaderService=new RealTimeFundFlowDirectionReaderServiceImpl();
		List<RealTimeFundFlowDirection> realTimeFundFlowDirectionList=realTimeFundFlowDirectionReaderService.readRealTimeFundFlowDirection("http://data.eastmoney.com/zjlx/detail.html");
	
		RealTimeFundFlowDirectionWriterService realTimeFundFlowDirectionWriterService=new RealTimeFundFlowDirectionWriterServiceImpl();
		realTimeFundFlowDirectionWriterService.writeData(realTimeFundFlowDirectionList);
	}
}
