package cn.edu.my.jsoup.service;

import cn.edu.my.jsoup.model.TradeFundFlowDirection;

import java.util.List;

public interface TradeFundFlowDirectionReaderService {
	List<TradeFundFlowDirection> readHttp(String url);
}
