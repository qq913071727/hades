package cn.edu.my.jsoup.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import cn.edu.my.jsoup.dao.TradeFundFlowDirectionWriterDao;
import cn.edu.my.jsoup.model.TradeFundFlowDirection;

public class TradeFundFlowDirectionWriterDaoImpl implements TradeFundFlowDirectionWriterDao {
	private TradeFundFlowDirection tradeFundFlowDirection;
	
	public void writeData(List<TradeFundFlowDirection> tradeFundFlowDirectionList) {
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("tradeFundFlowDirection");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		
		/*if(true==isDuplicate(tradeFundFlowDirectionList.get(0).getTradeDate())){
			return;
		}*/
		
		for(int i=0; i<tradeFundFlowDirectionList.size(); i++){
			tradeFundFlowDirection=new TradeFundFlowDirection();
			
			tradeFundFlowDirection.setTradeDate(tradeFundFlowDirectionList.get(i).getTradeDate());
			tradeFundFlowDirection.setTradeName(tradeFundFlowDirectionList.get(i).getTradeName());
			tradeFundFlowDirection.setUpDownRange(tradeFundFlowDirectionList.get(i).getUpDownRange());
			tradeFundFlowDirection.setMainForceNetInflowAmount(tradeFundFlowDirectionList.get(i).getMainForceNetInflowAmount());
			tradeFundFlowDirection.setMainForceNetInflowPercentage(tradeFundFlowDirectionList.get(i).getMainForceNetInflowPercentage());
			tradeFundFlowDirection.setSuperLargeSheetNetInflowAmount(tradeFundFlowDirectionList.get(i).getSuperLargeSheetNetInflowAmount());
			tradeFundFlowDirection.setSuperLargeSheetNetInflowPercentage(tradeFundFlowDirectionList.get(i).getSuperLargeSheetNetInflowPercentage());
			tradeFundFlowDirection.setLargeSheetNetInflowAmount(tradeFundFlowDirectionList.get(i).getLargeSheetNetInflowAmount());
			tradeFundFlowDirection.setLargeSheetNetInflowPercentage(tradeFundFlowDirectionList.get(i).getLargeSheetNetInflowPercentage());
			tradeFundFlowDirection.setMiddleSheetNetInflowAmount(tradeFundFlowDirectionList.get(i).getMiddleSheetNetInflowAmount());
			tradeFundFlowDirection.setMiddleSheetNetInflowPercentage(tradeFundFlowDirectionList.get(i).getMiddleSheetNetInflowPercentage());
			tradeFundFlowDirection.setSmallSheetNetInflowAmount(tradeFundFlowDirectionList.get(i).getSmallSheetNetInflowAmount());
			tradeFundFlowDirection.setSmallSheetNetInflowPercentage(tradeFundFlowDirectionList.get(i).getSmallSheetNetInflowPercentage());
			tradeFundFlowDirection.setMainForceNetInflowLargestStock(tradeFundFlowDirectionList.get(i).getMainForceNetInflowLargestStock());
			
			em.persist(tradeFundFlowDirection);
		}
		
		em.getTransaction().commit();
		emf.close();
	}
	
	private boolean isDuplicate(Date date){
		Date currentDate=new Date();
		if(currentDate.getYear()==date.getYear()&&currentDate.getMonth()==date.getMonth()&&currentDate.getDay()==date.getDay()){
			return true;
		}
		return false;
	}
}
