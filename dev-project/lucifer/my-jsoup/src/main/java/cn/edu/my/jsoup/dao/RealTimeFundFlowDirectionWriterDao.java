package cn.edu.my.jsoup.dao;

import java.util.List;

import cn.edu.my.jsoup.model.RealTimeFundFlowDirection;

public interface RealTimeFundFlowDirectionWriterDao {
	void writeData(List<RealTimeFundFlowDirection> realTimeFundFlowDirection);
}