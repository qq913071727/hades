package cn.edu.my.jsoup.model.base;

import cn.edu.my.jsoup.model.ChemicalTrade;

public class InstanceFactory {
	public static BaseTrade getInstance(String instanceName){
		if("ChemicalTrade".equals(instanceName)){
			return ChemicalTrade.getInstance();
		}else{
			return null;
		}
	}
}
