package cn.edu.my.jsoup.service;

import java.util.List;

import cn.edu.my.jsoup.model.TradeFundFlowDirection;

public interface TradeFundFlowDirectionWriterService {
	void writeData(List<TradeFundFlowDirection> tradeFundFlowDirectionList);
}
