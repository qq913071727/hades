package cn.edu.my.jsoup.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.math.BigDecimal;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cn.edu.my.jsoup.service.RealTimeFundFlowDirectionReaderService;
import cn.edu.my.jsoup.model.RealTimeFundFlowDirection;

public class RealTimeFundFlowDirectionReaderServiceImpl implements RealTimeFundFlowDirectionReaderService {
	public List<RealTimeFundFlowDirection> readRealTimeFundFlowDirection(String url){
		List<RealTimeFundFlowDirection> realTimeFundFlowDirectionList=new ArrayList<RealTimeFundFlowDirection>();
		
		System.getProperties().setProperty("proxySet", "true"); 
        //用的代理服务器  
        System.getProperties().setProperty("http.proxyHost", "10.1.31.133"); 
        //代理端口  
        System.getProperties().setProperty("http.proxyPort", "8080"); 
		
		try {
            Document doc = Jsoup.connect(url).get();
            Elements tbodies = doc.getElementsByTag("tbody");
            for (Element tbody : tbodies) {
            	Elements trs = tbody.getElementsByTag("tr");
            	for(Element tr:trs){
            		Elements tds=tr.getElementsByTag("td");
            		RealTimeFundFlowDirection reaTimeFundDirection=new RealTimeFundFlowDirection();
            		List temp=new ArrayList();
            		for(Element td:tds){
            			temp.add(td.children().text());
            		}
            		
            		Date currentDate=new Date();
            		int year=currentDate.getYear();
            		int month=currentDate.getMonth();
            		int date=currentDate.getDate();
            		
            		reaTimeFundDirection.setStockCode(temp.get(1).toString());
            		reaTimeFundDirection.setStockDate(new Date(year,month,date));
            		reaTimeFundDirection.setStockName(temp.get(2).toString());
            		reaTimeFundDirection.setNewPrice(new BigDecimal(temp.get(4).toString()));
            		reaTimeFundDirection.setUpDownRange(new BigDecimal(temp.get(5).toString().replace("%","")));
            		
            		if(temp.get(6).toString().endsWith("亿")){
            			reaTimeFundDirection.setMainForceNetInflowAmount(new BigDecimal(Double.parseDouble(temp.get(6).toString().replace("亿",""))*10000));
            		}else{
            			reaTimeFundDirection.setMainForceNetInflowAmount(new BigDecimal(temp.get(6).toString().replace("万","")));	
            		}
            		reaTimeFundDirection.setMainForceNetInflowPercentage(new BigDecimal(temp.get(7).toString().replace("%","")));
            		
            		if(temp.get(8).toString().endsWith("亿")){
            			reaTimeFundDirection.setSuperLargeSheetNetInflowAmount(new BigDecimal(Double.parseDouble(temp.get(8).toString().replace("亿",""))*10000));
            		}else{
            			reaTimeFundDirection.setSuperLargeSheetNetInflowAmount(new BigDecimal(temp.get(8).toString().replace("万","")));
            		}
            		reaTimeFundDirection.setSuperLargeSheetNetInflowPercentage(new BigDecimal(temp.get(9).toString().replace("%","")));
            		
            		if(temp.get(10).toString().endsWith("亿")){
            			reaTimeFundDirection.setLargeSheetNetInflowAmount(new BigDecimal(Double.parseDouble(temp.get(10).toString().replace("亿",""))*10000));
            		}else{
            			reaTimeFundDirection.setLargeSheetNetInflowAmount(new BigDecimal(temp.get(10).toString().replace("万","")));
            		}
           			reaTimeFundDirection.setLargeSheetNetInflowPercentage(new BigDecimal(temp.get(11).toString().replace("%","")));
            		
            		if(temp.get(12).toString().endsWith("亿")){
            			reaTimeFundDirection.setMiddleSheetNetInflowAmount(new BigDecimal(Double.parseDouble(temp.get(12).toString().replace("亿",""))*10000));
            		}else{
            			reaTimeFundDirection.setMiddleSheetNetInflowAmount(new BigDecimal(temp.get(12).toString().replace("万","")));
            		}
            		reaTimeFundDirection.setMiddleSheetNetInflowPercentage(new BigDecimal(temp.get(13).toString().replace("%","")));
            		
            		if(temp.get(14).toString().endsWith("亿")){
            			reaTimeFundDirection.setSmallSheetNetInflowAmount(new BigDecimal(Double.parseDouble(temp.get(14).toString().replace("亿",""))*10000));
            		}else{
            			reaTimeFundDirection.setSmallSheetNetInflowAmount(new BigDecimal(temp.get(14).toString().replace("万","")));
            		}
            		
            		reaTimeFundDirection.setSmallSheetNetInflowPercentage(new BigDecimal(temp.get(15).toString().replace("%","")));
            		realTimeFundFlowDirectionList.add(reaTimeFundDirection);
            	}
            }
		} catch (IOException e) {
            e.printStackTrace();
        }
		return realTimeFundFlowDirectionList;
	}
}