package cn.edu.my.jsoup.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import cn.edu.my.jsoup.dao.RealTimeFundFlowDirectionWriterDao;
import cn.edu.my.jsoup.model.RealTimeFundFlowDirection;

public class RealTimeFundFlowDirectionWriterDaoImpl implements RealTimeFundFlowDirectionWriterDao {
	private RealTimeFundFlowDirection realTimeFundFlowDirection;
	
	public void writeData(List<RealTimeFundFlowDirection> realTimeFundFlowDirectionList) {
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("tradeFundFlowDirection");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		
		for(int i=0; i<realTimeFundFlowDirectionList.size(); i++){
			realTimeFundFlowDirection=new RealTimeFundFlowDirection();
			
			realTimeFundFlowDirection.setStockCode(realTimeFundFlowDirectionList.get(i).getStockCode());
			realTimeFundFlowDirection.setStockDate(realTimeFundFlowDirectionList.get(i).getStockDate());
			realTimeFundFlowDirection.setStockName(realTimeFundFlowDirectionList.get(i).getStockName().toString());
			realTimeFundFlowDirection.setNewPrice(realTimeFundFlowDirectionList.get(i).getNewPrice());
			realTimeFundFlowDirection.setUpDownRange(realTimeFundFlowDirectionList.get(i).getUpDownRange());
			realTimeFundFlowDirection.setMainForceNetInflowAmount(realTimeFundFlowDirectionList.get(i).getMainForceNetInflowAmount());
			realTimeFundFlowDirection.setMainForceNetInflowPercentage(realTimeFundFlowDirectionList.get(i).getMainForceNetInflowPercentage());
			realTimeFundFlowDirection.setSuperLargeSheetNetInflowAmount(realTimeFundFlowDirectionList.get(i).getSuperLargeSheetNetInflowAmount());
			realTimeFundFlowDirection.setSuperLargeSheetNetInflowPercentage(realTimeFundFlowDirectionList.get(i).getSuperLargeSheetNetInflowPercentage());
			realTimeFundFlowDirection.setLargeSheetNetInflowAmount(realTimeFundFlowDirectionList.get(i).getLargeSheetNetInflowAmount());
			realTimeFundFlowDirection.setLargeSheetNetInflowPercentage(realTimeFundFlowDirectionList.get(i).getLargeSheetNetInflowPercentage());
			realTimeFundFlowDirection.setMiddleSheetNetInflowAmount(realTimeFundFlowDirectionList.get(i).getMiddleSheetNetInflowAmount());
			realTimeFundFlowDirection.setMiddleSheetNetInflowPercentage(realTimeFundFlowDirectionList.get(i).getMiddleSheetNetInflowPercentage());
			realTimeFundFlowDirection.setSmallSheetNetInflowAmount(realTimeFundFlowDirectionList.get(i).getSmallSheetNetInflowAmount());
			realTimeFundFlowDirection.setSmallSheetNetInflowPercentage(realTimeFundFlowDirectionList.get(i).getSmallSheetNetInflowPercentage());
			
			em.persist(realTimeFundFlowDirection);
		}
		
		em.getTransaction().commit();
		emf.close();
	}
}