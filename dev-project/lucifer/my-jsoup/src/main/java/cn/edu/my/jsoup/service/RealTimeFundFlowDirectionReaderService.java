package cn.edu.my.jsoup.service;

import java.util.List;

import cn.edu.my.jsoup.model.RealTimeFundFlowDirection;

public interface RealTimeFundFlowDirectionReaderService {
	List<RealTimeFundFlowDirection> readRealTimeFundFlowDirection(String url);
}