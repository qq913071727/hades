package cn.edu.my.jsoup.service;

import java.util.List;

import cn.edu.my.jsoup.model.TradeFundFlowDirection;
import cn.edu.my.jsoup.model.base.BaseTrade;

public interface TradeReaderService {
	List<BaseTrade> readHttp(String url);
	List getTradeHref(String url);
}
