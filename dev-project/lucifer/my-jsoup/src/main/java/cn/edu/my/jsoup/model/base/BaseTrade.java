package cn.edu.my.jsoup.model.base;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)  
public class BaseTrade implements Serializable {
	@Id
	@Column(name="STOCK_CODE")
	private String stockCode;
	@Id
	@Column(name="STOCK_DATE")
	private Date stockDate;
	@Column(name="STOCK_NAME")
	private String stockName;
	@Column(name="PRICE")
	private BigDecimal price;
	@Column(name="UDR")
	private BigDecimal upDownRange;
	@Column(name="MFNIA")
	private BigDecimal mainForceNetInflowAmount;
	@Column(name="MFNIP")
	private BigDecimal mainForceNetInflowPercentage;
	@Column(name="SLSNIA")
	private BigDecimal superLargeSheetNetInflowAmount;
	@Column(name="SLSNIP")
	private BigDecimal superLargeSheetNetInflowPercentage;
	@Column(name="LSNIA")
	private BigDecimal largeSheetNetInflowAmount;
	@Column(name="LSNIP")
	private BigDecimal largeSheetNetInflowPercentage;
	@Column(name="MSNIA")
	private BigDecimal middleSheetNetInflowAmount;
	@Column(name="MSNIP")
	private BigDecimal middleSheetNetInflowPercentage;
	@Column(name="SSNIA")
	private BigDecimal smallSheetNetInflowAmount;
	@Column(name="SSNIP")
	private BigDecimal smallSheetNetInflowPercentage;
	
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public Date getStockDate() {
		return stockDate;
	}
	public void setStockDate(Date stockDate) {
		this.stockDate = stockDate;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getUpDownRange() {
		return upDownRange;
	}
	public void setUpDownRange(BigDecimal upDownRange) {
		this.upDownRange = upDownRange;
	}
	public BigDecimal getMainForceNetInflowAmount() {
		return mainForceNetInflowAmount;
	}
	public void setMainForceNetInflowAmount(BigDecimal mainForceNetInflowAmount) {
		this.mainForceNetInflowAmount = mainForceNetInflowAmount;
	}
	public BigDecimal getMainForceNetInflowPercentage() {
		return mainForceNetInflowPercentage;
	}
	public void setMainForceNetInflowPercentage(BigDecimal mainForceNetInflowPercentage) {
		this.mainForceNetInflowPercentage = mainForceNetInflowPercentage;
	}
	public BigDecimal getSuperLargeSheetNetInflowAmount() {
		return superLargeSheetNetInflowAmount;
	}
	public void setSuperLargeSheetNetInflowAmount(
			BigDecimal superLargeSheetNetInflowAmount) {
		this.superLargeSheetNetInflowAmount = superLargeSheetNetInflowAmount;
	}
	public BigDecimal getSuperLargeSheetNetInflowPercentage() {
		return superLargeSheetNetInflowPercentage;
	}
	public void setSuperLargeSheetNetInflowPercentage(
			BigDecimal superLargeSheetNetInflowPercentage) {
		this.superLargeSheetNetInflowPercentage = superLargeSheetNetInflowPercentage;
	}
	public BigDecimal getLargeSheetNetInflowAmount() {
		return largeSheetNetInflowAmount;
	}
	public void setLargeSheetNetInflowAmount(BigDecimal largeSheetNetInflowAmount) {
		this.largeSheetNetInflowAmount = largeSheetNetInflowAmount;
	}
	public BigDecimal getLargeSheetNetInflowPercentage() {
		return largeSheetNetInflowPercentage;
	}
	public void setLargeSheetNetInflowPercentage(
			BigDecimal largeSheetNetInflowPercentage) {
		this.largeSheetNetInflowPercentage = largeSheetNetInflowPercentage;
	}
	public BigDecimal getMiddleSheetNetInflowAmount() {
		return middleSheetNetInflowAmount;
	}
	public void setMiddleSheetNetInflowAmount(BigDecimal middleSheetNetInflowAmount) {
		this.middleSheetNetInflowAmount = middleSheetNetInflowAmount;
	}
	public BigDecimal getMiddleSheetNetInflowPercentage() {
		return middleSheetNetInflowPercentage;
	}
	public void setMiddleSheetNetInflowPercentage(
			BigDecimal middleSheetNetInflowPercentage) {
		this.middleSheetNetInflowPercentage = middleSheetNetInflowPercentage;
	}
	public BigDecimal getSmallSheetNetInflowAmount() {
		return smallSheetNetInflowAmount;
	}
	public void setSmallSheetNetInflowAmount(BigDecimal smallSheetNetInflowAmount) {
		this.smallSheetNetInflowAmount = smallSheetNetInflowAmount;
	}
	public BigDecimal getSmallSheetNetInflowPercentage() {
		return smallSheetNetInflowPercentage;
	}
	public void setSmallSheetNetInflowPercentage(
			BigDecimal smallSheetNetInflowPercentage) {
		this.smallSheetNetInflowPercentage = smallSheetNetInflowPercentage;
	}
}
