package cn.edu.my.jsoup.dao;

import java.util.List;

import cn.edu.my.jsoup.model.TradeFundFlowDirection;

public interface TradeFundFlowDirectionWriterDao {
	void writeData(List<TradeFundFlowDirection> tradeFundFlowDirection);
}
