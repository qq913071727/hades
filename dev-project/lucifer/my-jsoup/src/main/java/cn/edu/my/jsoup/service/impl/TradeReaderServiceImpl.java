package cn.edu.my.jsoup.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;

import org.json.JSONObject;
import org.json.JSONArray;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cn.edu.my.jsoup.model.base.BaseTrade;
import cn.edu.my.jsoup.model.base.InstanceFactory;
import cn.edu.my.jsoup.service.TradeReaderService;

public class TradeReaderServiceImpl implements TradeReaderService {

	public List<BaseTrade> readHttp(String url) {
		List<BaseTrade> baseTradeList = new ArrayList<BaseTrade>();
		
		System.getProperties().setProperty("proxySet", "true"); 
        //用的代理服务器  
        System.getProperties().setProperty("http.proxyHost", "10.1.31.133"); 
        //代理端口  
        System.getProperties().setProperty("http.proxyPort", "8080"); 
		
		try {
			Document doc = Jsoup.connect(url).get();
			Elements tbodies = doc.getElementsByTag("body");
			JSONObject clientJSONObj = new JSONObject(tbodies.text().split("=")[1]);
			JSONArray dataArray = clientJSONObj.getJSONArray("data");
			for (int i = 0; i < dataArray.length(); i++) {
				String[] chemicalTradeArray=dataArray.get(i).toString().split(",");
				BaseTrade baseTrade=InstanceFactory.getInstance("ChemicalTrade");
				baseTrade.setStockCode(chemicalTradeArray[0]);
				baseTrade.setStockName(chemicalTradeArray[1]);
				baseTrade.setPrice(new BigDecimal(chemicalTradeArray[2]));
				baseTrade.setUpDownRange(new BigDecimal(chemicalTradeArray[3].replace("%", "")));
				baseTrade.setMainForceNetInflowAmount(new BigDecimal(chemicalTradeArray[4].replace("万", "").replace("亿", "")));
				baseTrade.setMainForceNetInflowPercentage(new BigDecimal(chemicalTradeArray[5].replace("%", "")));
				baseTrade.setSmallSheetNetInflowAmount(new BigDecimal(chemicalTradeArray[6].replace("万", "").replace("亿", "")));
				baseTrade.setSmallSheetNetInflowPercentage(new BigDecimal(chemicalTradeArray[7].replace("%", "")));
				baseTrade.setLargeSheetNetInflowAmount(new BigDecimal(chemicalTradeArray[8].replace("万", "").replace("亿", "")));
				baseTrade.setLargeSheetNetInflowPercentage(new BigDecimal(chemicalTradeArray[9].replace("%", "")));
				baseTrade.setMiddleSheetNetInflowAmount(new BigDecimal(chemicalTradeArray[10].replace("万", "").replace("亿", "")));
				baseTrade.setMiddleSheetNetInflowPercentage(new BigDecimal(chemicalTradeArray[11].replace("%", "")));
				baseTrade.setSmallSheetNetInflowAmount(new BigDecimal(chemicalTradeArray[12].replace("万", "").replace("亿", "")));
				baseTrade.setSmallSheetNetInflowPercentage(new BigDecimal(chemicalTradeArray[13].replace("%", "")));
				baseTradeList.add(baseTrade);
			}
			
			/*Object[] tbodyArray = tbodies.toArray();
			Elements trs = ((Element) tbodyArray[2]).getElementsByTag("tr");
			for (Element tr : trs) {
				Elements tds = tr.getElementsByTag("td");
				BaseTrade baseTrade=InstanceFactory.getInstance("ChemicalTrade");
				List temp=new ArrayList(); 
				for(Element td:tds){
					temp.add(td.children().text());
				}
				baseTrade.setStockDate(new Date());
				baseTrade.setStockCode(temp.get(1).toString());
				baseTrade.setStockName(temp.get(2).toString());
				baseTrade.setPrice(new BigDecimal(temp.get(4).toString())); 
				baseTrade.setUpDownRange(new BigDecimal(temp.get(5).toString().replace("%","")));
				baseTrade.setMainForceNetInflowAmount(new BigDecimal(temp.get(6).toString().replace("万", "").replace("亿", ""))*1000);
				baseTrade.setMainForceNetInflowPercentage(new BigDecimal(temp.get(7).toString().replace("%","")));
				baseTrade.setSuperLargeSheetNetInflowAmount(new BigDecimal(temp.get(8).toString().replace("万", "").replace("亿", ""))*1000);
				baseTrade.setSuperLargeSheetNetInflowPercentage(new BigDecimal(temp.get(9).toString().replace("%","")));
				baseTrade.setLargeSheetNetInflowAmount(new BigDecimal(temp.get(10).toString().replace("万", "").replace("亿", ""))*1000);
				baseTrade.setLargeSheetNetInflowPercentage(new BigDecimal(temp.get(11).toString().replace("%","")));
				baseTrade.setMiddleSheetNetInflowAmount(new BigDecimal(temp.get(12).toString().replace("万", "").replace("亿", ""))*1000);
				baseTrade.setMiddleSheetNetInflowPercentage(new BigDecimal(temp.get(13).toString().replace("%","")));
				baseTrade.setSmallSheetNetInflowAmount(new BigDecimal(temp.get(14).toString().replace("万", "").replace("亿", ""))*1000);
				baseTrade.setSmallSheetNetInflowPercentage(new BigDecimal(temp.get(15).toString().replace("%",""))); 
				baseTradeList.add(baseTrade);
			}*/
		} catch (IOException e) {
			e.printStackTrace();
		}
		return baseTradeList;
	}

	public List getTradeHref(String url) {
		List tradeHrefList = new ArrayList();
		try {
			Document doc = Jsoup.connect(url).get();
			Elements tbodies = doc.getElementsByTag("tbody");
			Object[] tbodyArray = tbodies.toArray();
			Elements trs = ((Element) tbodyArray[0]).getElementsByTag("tr");

			for (Element tr : trs) {
				Elements tds = tr.getElementsByTag("td");
				Object[] tdsArray = tds.toArray();
				tradeHrefList.add(((Element) tdsArray[2]).child(0).select("a")
						.attr("href"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return tradeHrefList;
	}
}
