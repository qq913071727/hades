package cn.edu.my.jsoup.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cn.edu.my.jsoup.model.TradeFundFlowDirection;
import cn.edu.my.jsoup.service.TradeFundFlowDirectionReaderService;

public class TradeFundFlowDirectionReaderServiceImpl implements TradeFundFlowDirectionReaderService {

	public List<TradeFundFlowDirection> readHttp(String url) {
		List<TradeFundFlowDirection> tradeFundFlowDirectionList=new ArrayList<TradeFundFlowDirection>();
		
		System.getProperties().setProperty("proxySet", "true"); 
        //用的代理服务器  
        System.getProperties().setProperty("http.proxyHost", "10.1.31.133"); 
        //代理端口  
        System.getProperties().setProperty("http.proxyPort", "8080"); 
		
		try {
            Document doc = Jsoup.connect(url).get();
            Elements tbodies = doc.getElementsByTag("tbody");

            for (Element tbody : tbodies) {
            	if(tbody.id().equals("dt_body")){
            		Elements trs = tbody.getElementsByTag("tr");
            		for(Element tr:trs){
            			Elements tds=tr.getElementsByTag("td");
            			TradeFundFlowDirection tradeFundFlowDirection=new TradeFundFlowDirection();
            			List temp=new ArrayList();
            			for(Element td:tds){
            				temp.add(td.children().text());
            			}
            			
            			Date currentDate=new Date();
	            		int year=currentDate.getYear();
	            		int month=currentDate.getMonth();
	            		int date=currentDate.getDate();
            			
            			tradeFundFlowDirection.setTradeDate(new Date(year,month,date));
            			tradeFundFlowDirection.setTradeName(temp.get(1).toString());
            			tradeFundFlowDirection.setUpDownRange(new BigDecimal(temp.get(3).toString().replace("%", "")));
            			
            			if(temp.get(4).toString().endsWith("亿")){
            				tradeFundFlowDirection.setMainForceNetInflowAmount(new BigDecimal(Double.parseDouble(temp.get(4).toString().replace("亿", ""))*10000));
            			}else{
            				tradeFundFlowDirection.setMainForceNetInflowAmount(new BigDecimal(temp.get(4).toString().replace("万", "")));
            			}
            			tradeFundFlowDirection.setMainForceNetInflowPercentage(new BigDecimal(temp.get(5).toString().replace("%", "")));
            			
            			if(temp.get(6).toString().endsWith("亿")){
            				tradeFundFlowDirection.setSuperLargeSheetNetInflowAmount(new BigDecimal(Double.parseDouble(temp.get(6).toString().replace("亿", ""))*10000));
            			}else{
            				tradeFundFlowDirection.setSuperLargeSheetNetInflowAmount(new BigDecimal(temp.get(6).toString().replace("万", "")));
            			}
            			tradeFundFlowDirection.setSuperLargeSheetNetInflowPercentage(new BigDecimal(temp.get(7).toString().replace("%", "")));
            			
            			if(temp.get(8).toString().endsWith("亿")){
            				tradeFundFlowDirection.setLargeSheetNetInflowAmount(new BigDecimal(Double.parseDouble(temp.get(8).toString().replace("亿", ""))*10000));
            			}else{
            				tradeFundFlowDirection.setLargeSheetNetInflowAmount(new BigDecimal(temp.get(8).toString().replace("万", "")));
            			}
            			tradeFundFlowDirection.setLargeSheetNetInflowPercentage(new BigDecimal(temp.get(9).toString().replace("%", "")));
            			
            			if(temp.get(10).toString().endsWith("亿")){
            				tradeFundFlowDirection.setMiddleSheetNetInflowAmount(new BigDecimal(Double.parseDouble(temp.get(10).toString().replace("亿", ""))*10000));
            			}else{
            				tradeFundFlowDirection.setMiddleSheetNetInflowAmount(new BigDecimal(temp.get(10).toString().replace("万", "")));
            			}
            			tradeFundFlowDirection.setMiddleSheetNetInflowPercentage(new BigDecimal(temp.get(11).toString().replace("%", "")));
            			
            			if(temp.get(12).toString().endsWith("亿")){
            				tradeFundFlowDirection.setSmallSheetNetInflowAmount(new BigDecimal(Double.parseDouble(temp.get(12).toString().replace("亿", ""))*10000));
            			}else{
            				tradeFundFlowDirection.setSmallSheetNetInflowAmount(new BigDecimal(temp.get(12).toString().replace("万", "")));
            			}
            			tradeFundFlowDirection.setSmallSheetNetInflowPercentage(new BigDecimal(temp.get(13).toString().replace("%", "")));
            			
            			tradeFundFlowDirection.setMainForceNetInflowLargestStock(temp.get(14).toString());
            			tradeFundFlowDirectionList.add(tradeFundFlowDirection);
            		}
            	}
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
		return tradeFundFlowDirectionList;
	}

}
