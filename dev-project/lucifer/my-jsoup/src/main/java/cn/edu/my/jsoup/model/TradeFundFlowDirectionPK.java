package cn.edu.my.jsoup.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;

public class TradeFundFlowDirectionPK implements Serializable{
	@Id
	private Date tradeDate;
	@Id
	private String tradeName;
	
	public TradeFundFlowDirectionPK() {
	}
	
	public TradeFundFlowDirectionPK(Date tradeDate, String tradeName) {
		this.tradeDate = tradeDate;
		this.tradeName = tradeName;
	}

	public Date getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((tradeDate == null) ? 0 : tradeDate.hashCode());
		result = prime * result
				+ ((tradeName == null) ? 0 : tradeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TradeFundFlowDirectionPK other = (TradeFundFlowDirectionPK) obj;
		if (tradeDate == null) {
			if (other.tradeDate != null)
				return false;
		} else if (!tradeDate.equals(other.tradeDate))
			return false;
		if (tradeName == null) {
			if (other.tradeName != null)
				return false;
		} else if (!tradeName.equals(other.tradeName))
			return false;
		return true;
	}
}
