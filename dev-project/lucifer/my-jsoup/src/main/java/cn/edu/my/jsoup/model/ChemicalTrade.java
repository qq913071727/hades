package cn.edu.my.jsoup.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.edu.my.jsoup.model.base.BaseTrade;

@Entity
@Table(name="CHEMICAL_TRADE")
public class ChemicalTrade extends BaseTrade implements Serializable {
	private static ChemicalTrade chemicalTrade;
	public static int pageNumber=4;
	
	private ChemicalTrade(){
		
	}
	
	public static ChemicalTrade getInstance(){
		if(null==chemicalTrade){
			return new ChemicalTrade();
		}
		return chemicalTrade;
	}
}
