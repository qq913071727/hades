package cn.edu.my.jsoup.model;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TRADE_FUND_FLOW_DIRECTION")
public class TradeFundFlowDirection implements Serializable{
	@Id
	@Column(name="TRADE_DATE")
	private Date tradeDate;
	@Id
	@Column(name="TRADE_NAME")
	private String tradeName;
	@Column(name="UDR")
	private BigDecimal upDownRange;
	@Column(name="MFNIA")
	private BigDecimal mainForceNetInflowAmount;
	@Column(name="MFNIP")
	private BigDecimal mainForceNetInflowPercentage;
	@Column(name="SLSNIA")
	private BigDecimal superLargeSheetNetInflowAmount;
	@Column(name="SLSNIP")
	private BigDecimal superLargeSheetNetInflowPercentage;
	@Column(name="LSNIA")
	private BigDecimal largeSheetNetInflowAmount;
	@Column(name="LSNIP")
	private BigDecimal largeSheetNetInflowPercentage;
	@Column(name="MSNIA")
	private BigDecimal middleSheetNetInflowAmount;
	@Column(name="MSNIP")
	private BigDecimal middleSheetNetInflowPercentage;
	@Column(name="SSNIA")
	private BigDecimal smallSheetNetInflowAmount;
	@Column(name="SSNIP")
	private BigDecimal smallSheetNetInflowPercentage;
	@Column(name="MFNILS")
	private String mainForceNetInflowLargestStock;
	
	public Date getTradeDate() {
		return tradeDate;
	}
	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}
	public String getTradeName() {
		return tradeName;
	}
	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}
	public BigDecimal getUpDownRange() {
		return upDownRange;
	}
	public void setUpDownRange(BigDecimal upDownRange) {
		this.upDownRange = upDownRange;
	}
	public BigDecimal getMainForceNetInflowAmount() {
		return mainForceNetInflowAmount;
	}
	public void setMainForceNetInflowAmount(BigDecimal mainForceNetInflowAmount) {
		this.mainForceNetInflowAmount = mainForceNetInflowAmount;
	}
	public BigDecimal getMainForceNetInflowPercentage() {
		return mainForceNetInflowPercentage;
	}
	public void setMainForceNetInflowPercentage(BigDecimal mainForceNetInflowPercentage) {
		this.mainForceNetInflowPercentage = mainForceNetInflowPercentage;
	}
	public BigDecimal getSuperLargeSheetNetInflowAmount() {
		return superLargeSheetNetInflowAmount;
	}
	public void setSuperLargeSheetNetInflowAmount(
			BigDecimal superLargeSheetNetInflowAmount) {
		this.superLargeSheetNetInflowAmount = superLargeSheetNetInflowAmount;
	}
	public BigDecimal getSuperLargeSheetNetInflowPercentage() {
		return superLargeSheetNetInflowPercentage;
	}
	public void setSuperLargeSheetNetInflowPercentage(
			BigDecimal superLargeSheetNetInflowPercentage) {
		this.superLargeSheetNetInflowPercentage = superLargeSheetNetInflowPercentage;
	}
	public BigDecimal getLargeSheetNetInflowAmount() {
		return largeSheetNetInflowAmount;
	}
	public void setLargeSheetNetInflowAmount(BigDecimal largeSheetNetInflowAmount) {
		this.largeSheetNetInflowAmount = largeSheetNetInflowAmount;
	}
	public BigDecimal getLargeSheetNetInflowPercentage() {
		return largeSheetNetInflowPercentage;
	}
	public void setLargeSheetNetInflowPercentage(
			BigDecimal largeSheetNetInflowPercentage) {
		this.largeSheetNetInflowPercentage = largeSheetNetInflowPercentage;
	}
	public BigDecimal getMiddleSheetNetInflowAmount() {
		return middleSheetNetInflowAmount;
	}
	public void setMiddleSheetNetInflowAmount(BigDecimal middleSheetNetInflowAmount) {
		this.middleSheetNetInflowAmount = middleSheetNetInflowAmount;
	}
	public BigDecimal getMiddleSheetNetInflowPercentage() {
		return middleSheetNetInflowPercentage;
	}
	public void setMiddleSheetNetInflowPercentage(
			BigDecimal middleSheetNetInflowPercentage) {
		this.middleSheetNetInflowPercentage = middleSheetNetInflowPercentage;
	}
	public BigDecimal getSmallSheetNetInflowAmount() {
		return smallSheetNetInflowAmount;
	}
	public void setSmallSheetNetInflowAmount(BigDecimal smallSheetNetInflowAmount) {
		this.smallSheetNetInflowAmount = smallSheetNetInflowAmount;
	}
	public BigDecimal getSmallSheetNetInflowPercentage() {
		return smallSheetNetInflowPercentage;
	}
	public void setSmallSheetNetInflowPercentage(
			BigDecimal smallSheetNetInflowPercentage) {
		this.smallSheetNetInflowPercentage = smallSheetNetInflowPercentage;
	}
	public String getMainForceNetInflowLargestStock() {
		return mainForceNetInflowLargestStock;
	}
	public void setMainForceNetInflowLargestStock(
			String mainForceNetInflowLargestStock) {
		this.mainForceNetInflowLargestStock = mainForceNetInflowLargestStock;
	}
}