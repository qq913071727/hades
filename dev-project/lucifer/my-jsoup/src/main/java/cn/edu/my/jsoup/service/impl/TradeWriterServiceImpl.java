package cn.edu.my.jsoup.service.impl;

import java.util.List;

import cn.edu.my.jsoup.dao.TradeWriterDao;
import cn.edu.my.jsoup.dao.impl.TradeWriterDaoImpl;
import cn.edu.my.jsoup.model.base.BaseTrade;
import cn.edu.my.jsoup.service.TradeWriterService;

public class TradeWriterServiceImpl implements TradeWriterService {
	private TradeWriterDao tradeWriterDao=new TradeWriterDaoImpl();

	public void writeData(List<BaseTrade> baseTradeList) {
		tradeWriterDao.writeData(baseTradeList);
	}

}
