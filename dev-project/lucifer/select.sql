# 同花顺指数
select * from ths_index t where t.indexdate=to_date('2014-6-9','yyyy-mm-dd') order by t.last_fiveday_rise_extent desc

# 行业资金流向
select * from trade_fund_flow_direction t where t.trade_date=to_date('2014-6-3','yyyy-mm-dd') order by t.mfnia desc

# 实时资金流向
select * from real_time_fund_flow_direction t where t.stock_date=to_date('2014-6-3','yyyy-mm-dd') order by t.mfnia desc