package cn.com.acca.hbase.dao;

import java.util.List;

import cn.com.acca.hbase.dao.base.BaseDao;
import cn.com.acca.hbase.model.StockMovingAverage;

public interface StockMovingAverageDao extends BaseDao<StockMovingAverage> {
	List<StockMovingAverage> getAllStockMovingAverages(String stockCode);
	List<StockMovingAverage> getAllStockCodes();
}
