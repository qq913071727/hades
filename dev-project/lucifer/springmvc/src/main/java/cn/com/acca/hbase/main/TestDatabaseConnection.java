package cn.com.acca.hbase.main;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import cn.com.acca.hbase.model.StockMovingAverage;
import cn.com.acca.hbase.service.StockMovingAverageService;

public class TestDatabaseConnection {

	private static StockMovingAverageService stockMovingAverageService;//=new StockMovingAverageServiceImpl();
	
	public static void setStockMovingAverageService(StockMovingAverageService stockMovingAverageService) {
		stockMovingAverageService = stockMovingAverageService;
	}
	
	public static void main(String[] args) {
		ApplicationContext context =new FileSystemXmlApplicationContext("applicationContext.xml");
		stockMovingAverageService=(StockMovingAverageService) context.getBean("stockMovingAverageService");
		
		List<StockMovingAverage> stockCodeList=new ArrayList<StockMovingAverage>();
		List<StockMovingAverage> allStockList=new ArrayList<StockMovingAverage>();
		stockCodeList=stockMovingAverageService.getAllStockCodes();
		for(int i=0;i<stockCodeList.size();i++){
			allStockList=stockMovingAverageService.getAllStockMovingAverages(stockCodeList.get(i).toString());
			//System.out.println(allStockList);
			//System.out.println(allStockList.get(i).getStockDate());
			stockMovingAverageService.writeStock(allStockList);
			allStockList.clear();
		}
	}
}
