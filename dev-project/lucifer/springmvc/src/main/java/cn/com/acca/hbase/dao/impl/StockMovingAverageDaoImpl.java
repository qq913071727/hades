package cn.com.acca.hbase.dao.impl;

import java.util.List;
import javax.annotation.Resource;

import org.springframework.stereotype.Repository;
import cn.com.acca.hbase.dao.StockMovingAverageDao;
import cn.com.acca.hbase.dao.base.AbstractBaseDao;
import cn.com.acca.hbase.model.StockMovingAverage;

@Repository("stockMovingAverageDao")
public class StockMovingAverageDaoImpl extends AbstractBaseDao<StockMovingAverage> implements StockMovingAverageDao{
	public StockMovingAverageDaoImpl(){
		super();
		//setClazz(StockMovingAverage.class);
	}
	
	public List<StockMovingAverage> getAllStockMovingAverages(String stockCode) {
		return this.querySQL("select * from Stock_Moving_Average t where t.stock_code='"+stockCode+"'");
	}

	public List<StockMovingAverage> getAllStockCodes(){
		return this.querySQL("select distinct t.stock_code from Stock_Moving_Average t");
	}
}
