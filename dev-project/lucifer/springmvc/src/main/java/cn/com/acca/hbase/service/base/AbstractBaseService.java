package cn.com.acca.hbase.service.base;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import cn.com.acca.hbase.dao.base.BaseDao;

@Transactional
public abstract class AbstractBaseService<T extends Serializable> implements BaseDao<T> {
	protected abstract BaseDao<T> getDao();
	
	public List<T> query(String hql) {
        return getDao().query(hql);
	}
	
	public List<T> querySQL(String sql) {
        return getDao().querySQL(sql);
	}

	public void delete(T t) {
		getDao().delete(t);
	}

	public void update(T t) {
		getDao().update(t);
	}

	public void save(T t) {
		getDao().save(t);
	}
}
