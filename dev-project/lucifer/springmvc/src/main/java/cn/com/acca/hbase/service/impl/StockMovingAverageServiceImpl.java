package cn.com.acca.hbase.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.com.acca.hbase.dao.StockMovingAverageDao;
import cn.com.acca.hbase.dao.base.BaseDao;
import cn.com.acca.hbase.model.StockMovingAverage;
import cn.com.acca.hbase.service.StockMovingAverageService;
import cn.com.acca.hbase.service.base.AbstractBaseService;

@Service("stockMovingAverageService") 
public class StockMovingAverageServiceImpl extends AbstractBaseService<StockMovingAverage> implements StockMovingAverageService{
	@Resource(name="stockMovingAverageDao")
	private StockMovingAverageDao stockMovingAverageDao;
	
	public StockMovingAverageServiceImpl() {
        super();
    }
	
	protected BaseDao<StockMovingAverage> getDao() {
        return this.stockMovingAverageDao;
    }

	public List<StockMovingAverage> getAllStockMovingAverages(String stockCode) {
		return stockMovingAverageDao.getAllStockMovingAverages(stockCode);
	}

	public List<StockMovingAverage> getAllStockCodes(){
		return stockMovingAverageDao.getAllStockCodes();
	}
	
	public void writeStock(List<StockMovingAverage> stockList){
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(new File("E:\\eclipse-workspace\\springmvc\\src\\main\\resources\\stock.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		for(int i=0;i<stockList.size();i++){
			try {
				//System.out.println(stockList.get(i).getClass().getName());
				out.write((stockList.get(i).toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getDea().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getDif().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getEma12().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getEma26().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getFive().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getOneHundredTwenty().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getSixty().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getStockAmount().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getStockClose().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getStockCode().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getStockHigh().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getStockLow().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getStockOpen().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getTen().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getTwenty().toString()+" ").getBytes());
				out.write((((StockMovingAverage)stockList.get(i)).getUpDown().toString()+" ").getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
