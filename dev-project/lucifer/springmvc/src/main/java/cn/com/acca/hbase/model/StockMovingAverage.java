package cn.com.acca.hbase.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="STOCK_MOVING_AVERAGE")
public class StockMovingAverage implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="STOCK_DATE")
	private Date stockDate;
	@Id
	@Column(name="STOCK_CODE")
	private String stockCode;
	@Column(name="STOCK_OPEN")
	private BigDecimal stockOpen;
	@Column(name="STOCK_HIGH")
	private BigDecimal stockHigh;
	@Column(name="STOCK_CLOSE")
	private BigDecimal stockClose;
	@Column(name="STOCK_LOW")
	private BigDecimal stockLow;
	@Column(name="STOCK_AMOUNT")
	private BigDecimal stockAmount;
	@Column(name="FIVE")
	private BigDecimal five;
	@Column(name="TEN")
	private BigDecimal ten;
	@Column(name="TWENTY")
	private BigDecimal twenty;
	@Column(name="SIXTY")
	private BigDecimal sixty;
	@Column(name="ONE_HUNDRED_TWENTY")
	private BigDecimal oneHundredTwenty;
	@Column(name="UPDOWN")
	private BigDecimal upDown;
	@Column(name="EMA12")
	private BigDecimal ema12;
	@Column(name="EMA26")
	private BigDecimal ema26;
	@Column(name="DIF")
	private BigDecimal dif;
	@Column(name="DEA")
	private BigDecimal dea;
	
	public StockMovingAverage() {
		super();
	}
	public Date getStockDate() {
		return stockDate;
	}
	public void setStockDate(Date stockDate) {
		this.stockDate = stockDate;
	}
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public BigDecimal getStockOpen() {
		return stockOpen;
	}
	public void setStockOpen(BigDecimal stockOpen) {
		this.stockOpen = stockOpen;
	}
	public BigDecimal getStockHigh() {
		return stockHigh;
	}
	public void setStockHigh(BigDecimal stockHigh) {
		this.stockHigh = stockHigh;
	}
	public BigDecimal getStockClose() {
		return stockClose;
	}
	public void setStockClose(BigDecimal stockClose) {
		this.stockClose = stockClose;
	}
	public BigDecimal getStockLow() {
		return stockLow;
	}
	public void setStockLow(BigDecimal stockLow) {
		this.stockLow = stockLow;
	}
	public BigDecimal getStockAmount() {
		return stockAmount;
	}
	public void setStockAmount(BigDecimal stockAmount) {
		this.stockAmount = stockAmount;
	}
	public BigDecimal getFive() {
		return five;
	}
	public void setFive(BigDecimal five) {
		this.five = five;
	}
	public BigDecimal getTen() {
		return ten;
	}
	public void setTen(BigDecimal ten) {
		this.ten = ten;
	}
	public BigDecimal getTwenty() {
		return twenty;
	}
	public void setTwenty(BigDecimal twenty) {
		this.twenty = twenty;
	}
	public BigDecimal getSixty() {
		return sixty;
	}
	public void setSixty(BigDecimal sixty) {
		this.sixty = sixty;
	}
	public BigDecimal getOneHundredTwenty() {
		return oneHundredTwenty;
	}
	public void setOneHundredTwenty(BigDecimal oneHundredTwenty) {
		this.oneHundredTwenty = oneHundredTwenty;
	}
	public BigDecimal getUpDown() {
		return upDown;
	}
	public void setUpDown(BigDecimal upDown) {
		this.upDown = upDown;
	}
	public BigDecimal getEma12() {
		return ema12;
	}
	public void setEma12(BigDecimal ema12) {
		this.ema12 = ema12;
	}
	public BigDecimal getEma26() {
		return ema26;
	}
	public void setEma26(BigDecimal ema26) {
		this.ema26 = ema26;
	}
	public BigDecimal getDif() {
		return dif;
	}
	public void setDif(BigDecimal dif) {
		this.dif = dif;
	}
	public BigDecimal getDea() {
		return dea;
	}
	public void setDea(BigDecimal dea) {
		this.dea = dea;
	}
}
