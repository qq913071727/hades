package cn.com.acca.hbase.dao.base;

import java.io.Serializable;
import java.util.List;

public interface BaseDao<T extends Serializable> {
	public List<T> query(String hql);
	public List<T> querySQL(String sql);
	public void delete(T t);
	public void update(T t);
	public void save(T t);
}
