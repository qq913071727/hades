package cn.com.acca.hbase.service;

import java.util.List;

import cn.com.acca.hbase.dao.base.BaseDao;
import cn.com.acca.hbase.model.StockMovingAverage;

public interface StockMovingAverageService extends BaseDao<StockMovingAverage> {
	List<StockMovingAverage> getAllStockMovingAverages(String stockCode);
	List<StockMovingAverage> getAllStockCodes();
	void writeStock(List<StockMovingAverage> stockList);
}
