package cn.com.acca.hbase.dao.base;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.google.common.base.Preconditions;

@SuppressWarnings("unchecked")
public abstract class AbstractBaseDao<T extends Serializable> implements BaseDao<T> {
	private Class<T> clazz;
	
	@Resource(name="sessionFactory")
	private SessionFactory sessionFactory;
	
	protected final void setClazz(final Class<T> clazzToSet) {
        this.clazz = Preconditions.checkNotNull(clazzToSet);
    }
	
	protected final Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

	public List<T> query(String hql) {
		Preconditions.checkNotNull(hql);
		Query query = getCurrentSession().createQuery(hql);
        return query.list();
	}
	
	public List<T> querySQL(String sql) {
		Preconditions.checkNotNull(sql);
		Query query = getCurrentSession().createSQLQuery(sql);
        return query.list();
	}

	public void delete(T t) {
		Preconditions.checkNotNull(t);
		getCurrentSession().delete(t);
	}

	public void update(T t) {
		Preconditions.checkNotNull(t);
		getCurrentSession().update(t);
	}

	public void save(T t) {
		Preconditions.checkNotNull(t);
		getCurrentSession().save(t);
	}
}
