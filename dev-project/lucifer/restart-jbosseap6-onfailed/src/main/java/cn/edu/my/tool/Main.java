package cn.edu.my.tool;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class Main{
	private static String XML_FILE="/nbs/tools/config.xml";
	
    public static void main( String[] args ){
    	File file = null;
    	try {
			SAXReader reader = new SAXReader();
		    Document doc = reader.read(new File(XML_FILE));
			Element rootElt = doc.getRootElement();
			for(Iterator it=rootElt.elementIterator();it.hasNext();){     
		        Element element = (Element) it.next();
		        if(element.attribute("key").getValue().equals("jbosseap6.web.dev.deployment.path")){
		        	System.out.println(element.attributeValue("value"));
		        	file=new File(element.attributeValue("value")+"seurat-web-ci.ear.failed");
		        	if(file.exists()){
		        		System.out.println("exist");
		        		file.delete();
		        		try{
		        			Runtime.getRuntime().exec("/sbin/services jbosseap6-web-dev restart");
		        		}catch(IOException e){
		        			e.printStackTrace();	
		        		}
		        	}
		        }
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
    }
}
