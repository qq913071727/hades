import java.io.IOException;
import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;

public class ZipPdf{
	
	public static void main(String[] args){

		String path=System.getProperty("user.dir");
		File file = new File(path);
		
		copyFolder(file);
		deleteFolder(file);
	}
	
	private static void copyFolder(File file){
		File[] childFiles = file.listFiles();
		String dest=new String();
		for (File childFile : childFiles) {
            if (childFile.isDirectory()) {
				copyFolder(childFile);
				//System.out.println(childFile.getName());
				//System.out.println(childFile.getPath());
			//}else if(childFile.isFile){
            }else if((childFile.isFile()&&childFile.getName().endsWith(".PDF"))||childFile.isFile()&&childFile.getName().endsWith(".pdf")){
            	//System.out.println(childFile.getName());
            	dest=childFile.getPath().substring(0,childFile.getPath().substring(0,
            		childFile.getPath().substring(0, childFile.getPath().lastIndexOf("\\")).lastIndexOf("\\")).lastIndexOf("\\"));
            	//System.out.println(dest);
            	copyPdfFile(childFile,new File(dest+"\\"+childFile.getName()));
            	
            }
            //deleteFolder(new File(dest));
        }
	}
	
	private static void copyPdfFile(File src,File dest){
		dest.setWritable(true);
		BufferedInputStream inBuff = null;
        BufferedOutputStream outBuff = null;
        try {
            // 新建文件输入流并对它进行缓冲
            inBuff = new BufferedInputStream(new FileInputStream(src));
            // 新建文件输出流并对它进行缓冲
            outBuff = new BufferedOutputStream(new FileOutputStream(dest));

            // 缓冲数组
            byte[] b = new byte[1024 * 5];
            int len;
            while ((len = inBuff.read(b)) != -1) {
                outBuff.write(b, 0, len);
            }
            // 刷新此缓冲的输出流
            outBuff.flush();
            if (inBuff != null)
                inBuff.close();
            if (outBuff != null)
                outBuff.close();
        }catch(IOException e){
        	e.printStackTrace();
        }
    }
    
    private static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }
    
    private static void deleteFolder(File file){
    	//System.out.println(file.getName());
    	File[] childFiles = file.listFiles();
		for (File rootDir : childFiles) {
			//System.out.println(rootDir.getName());
            if (rootDir.isDirectory()) {
            	File[] childFile = rootDir.listFiles();
				for (File child : childFile) {
					//System.out.println(child.getName());
		            if (child.isDirectory()) {
		            	deleteDir(child);
		            	//System.out.println(child.getAbsolutePath());
						//new File(child.getAbsolutePath()).delete();
						//child.deleteOnExit();
		            }
		        }
            }
        }
    }
}