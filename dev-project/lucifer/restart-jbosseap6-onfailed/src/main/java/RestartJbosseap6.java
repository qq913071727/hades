import java.io.IOException;
import java.io.File;
import java.lang.InterruptedException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class RestartJbosseap6{
	
	/********************************************* 156 and 151 testing environment **********************************************/
	//web test node 
	private static final String[] STOP_WEB_TEST={"/etc/init.d/jbosseap6-web-test", "stop"};
	private static final String[] START_WEB_TEST={"/etc/init.d/jbosseap6-web-test", "start"};
	private static final String WEB_TEST_DEPLOYED="/usr/app/jboss-eap-6.2/standalone-web-test/deployments/seurat-web-ci.ear.deployed";
	
	//web dev node 
	private static final String[] STOP_WEB_DEV={"/etc/init.d/jbosseap6-web-dev", "stop"};
	private static final String[] START_WEB_DEV={"/etc/init.d/jbosseap6-web-dev", "start"};
	private static final String WEB_DEV_DEPLOYED="/usr/app/jboss-eap-6.2/standalone-web-dev/deployments/seurat-web-ci.ear.deployed";
	
	//sch test node 
	private static final String[] STOP_SCH_TEST={"/etc/init.d/jbosseap6-sch-test", "stop"};
	private static final String[] START_SCH_TEST={"/etc/init.d/jbosseap6-sch-test", "start"};
	private static final String SCH_TEST_DEPLOYED="/usr/app/jboss-eap-6.2/standalone-sch-test/deployments/job-schedule-ci.ear.deployed";
	
	//sch dev node 
	private static final String[] STOP_SCH_DEV={"/etc/init.d/jbosseap6-sch-dev", "stop"};
	private static final String[] START_SCH_DEV={"/etc/init.d/jbosseap6-sch-dev", "start"};
	private static final String SCH_DEV_DEPLOYED="/usr/app/jboss-eap-6.2/standalone-sch-dev/deployments/job-schedule-ci.ear.deployed";
	
	//exe test node 
	private static final String[] STOP_EXE_TEST={"/etc/init.d/jbosseap6-exe-test", "stop"};
	private static final String[] START_EXE_TEST={"/etc/init.d/jbosseap6-exe-test", "start"};
	private static final String EXE_TEST_DEPLOYED="/usr/app/jboss-eap-6.2/standalone-exe-test/deployments/job-execution-ci.ear.deployed";
	
	//exe dev node 
	private static final String[] STOP_EXE_DEV={"/etc/init.d/jbosseap6-exe-dev", "stop"};
	private static final String[] START_EXE_DEV={"/etc/init.d/jbosseap6-exe-dev", "start"};
	private static final String EXE_DEV_DEPLOYED="/usr/app/jboss-eap-6.2/standalone-exe-dev/deployments/job-execution-ci.ear.deployed";
	
	/************************************************ 119 testing environment *************************************************/
	//web node 
	private static final String[] STOP_WEB={"/etc/init.d/jbosseap6-web", "stop"};
	private static final String[] START_WEB={"/etc/init.d/jbosseap6-web", "start"};
	private static final String WEB_DEPLOYED="/usr/app/jboss-eap-6.1/standalone-web/deployments/seurat-web-ci.ear.deployed";
	
	//sch node 
	private static final String[] STOP_SCH={"/etc/init.d/jbosseap6-sch", "stop"};
	private static final String[] START_SCH={"/etc/init.d/jbosseap6-sch", "start"};
	private static final String SCH_DEPLOYED="/usr/app/jboss-eap-6.1/standalone-sch/deployments/job-schedule-ci.ear.deployed";
	
	//exe1 node 
	private static final String[] STOP_EXE1={"/etc/init.d/jbosseap6-exe", "stop"};
	private static final String[] START_EXE1={"/etc/init.d/jbosseap6-exe", "start"};
	private static final String EXE1_DEPLOYED="/usr/app/jboss-eap-6.1/standalone-exe/deployments/job-execution-ci.ear.deployed";
	
	//exe2 node 
	private static final String[] STOP_EXE2={"/etc/init.d/jbosseap6-exe", "stop"};
	private static final String[] START_EXE2={"/etc/init.d/jbosseap6-exe", "start"};
	private static final String EXE2_DEPLOYED="/usr/app/jboss-eap-6.1/standalone-exe/deployments/job-execution-ci.ear.deployed";
	
	public static void main( String[] args ){
		InetAddress addr=null;
		String localhost=null;
		File file=null;
		
		try{
			Thread.sleep(60000);
		}catch(InterruptedException e1){
			e1.printStackTrace();
		}
		
		//获取本机IP
		try {
			addr = InetAddress.getLocalHost();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		localhost=addr.getHostAddress().toString();
		System.out.println("localhost is "+localhost);
		
		/******************************************* 156 and 151 testing environment *********************************************/
		//本机是10.1.15.156 WEB node
		if(localhost.equals(NodeIP.WEB.getIp())){
			System.out.println("it is 10.1.15.156 WEB node");
			
			//判断WEB TEST node部署是否成功
			file=new File(WEB_TEST_DEPLOYED);
			while(!file.exists()){//部署失败，重启jboss
				System.out.println("deployment of 10.1.15.156 WEB TEST node failed, it needs to restart");
				try {
					Runtime.getRuntime().exec(STOP_WEB_TEST);
					System.out.println("WEB TEST node is stopping");
					Thread.sleep(30000);
					Runtime.getRuntime().exec(START_WEB_TEST);
					System.out.println("WEB TEST node is starting");
					Thread.sleep(30000);
				}catch(InterruptedException e1){
					e1.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(file.exists()){//部署成功
				System.out.println("deployment of 10.1.15.156 WEB TEST node finishes");
			}
			
			//判断WEB DEV node部署是否成功
			file=new File(WEB_DEV_DEPLOYED);
			while(!file.exists()){//部署失败，重启jboss
				System.out.println("deployment of 10.1.15.156 WEB DEV node failed, it needs to restart");
				try {
					Runtime.getRuntime().exec(STOP_WEB_DEV);
					System.out.println("WEB DEV node is stopping");
					Thread.sleep(30000);
					Runtime.getRuntime().exec(START_WEB_DEV);
					System.out.println("WEB DEV node is starting");
					Thread.sleep(30000);
				}catch(InterruptedException e1){
					e1.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(file.exists()){//部署成功
				System.out.println("deployment of 10.1.15.156 WEB DEV node finishes");
			}
		}
		
		//本机是10.1.15.157 SCH node
		if(localhost.equals(NodeIP.SCH.getIp())){
			System.out.println("it is 10.1.15.157 SCH node");
			
			//判断SCH TEST node部署是否成功
			file=new File(SCH_TEST_DEPLOYED);
			while(!file.exists()){//部署失败，重启jboss
				System.out.println("deployment of 10.1.15.157 SCH TEST node failed, it needs to restart");
				try {
					Runtime.getRuntime().exec(STOP_SCH_TEST);
					System.out.println("SCH TEST node is stopping");
					Thread.sleep(30000);
					Runtime.getRuntime().exec(START_SCH_TEST);
					System.out.println("SCH TEST node is starting");
					Thread.sleep(30000);
				}catch(InterruptedException e1){
					e1.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(file.exists()){//部署成功
				System.out.println("deployment of 10.1.15.157 SCH TEST node finishes");
			}
			
			//判断SCH DEV node部署是否成功
			file=new File(SCH_DEV_DEPLOYED);
			while(!file.exists()){//部署失败，重启jboss
				System.out.println("deployment of 10.1.15.157 SCH DEV node failed, it needs to restart");
				try {
					Runtime.getRuntime().exec(STOP_SCH_DEV);
					System.out.println("SCH DEV node is stopping");
					Thread.sleep(30000);
					Runtime.getRuntime().exec(START_SCH_DEV);
					System.out.println("SCH DEV nodet is starting");
					Thread.sleep(30000);
				}catch(InterruptedException e1){
					e1.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(file.exists()){//部署成功
				System.out.println("deployment of 10.1.15.157 SCH DEV node finishes");
			}
		}
		
		//本机是10.1.15.158 EXE node
		if(localhost.equals(NodeIP.EXE.getIp())){
			System.out.println("it is 10.1.15.158 EXE node");
			
			//判断EXE TEST node部署是否成功
			file=new File(EXE_TEST_DEPLOYED);
			while(!file.exists()){//部署失败，重启jboss
				System.out.println("deployment of 10.1.15.158 EXE TEST node failed, it needs to restart");
				try {
					Runtime.getRuntime().exec(STOP_EXE_TEST);
					System.out.println("EXE TEST node is stopping");
					Thread.sleep(30000);
					Runtime.getRuntime().exec(START_EXE_TEST);
					System.out.println("EXE TEST node is starting");
					Thread.sleep(30000);
				}catch(InterruptedException e1){
					e1.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(file.exists()){//部署成功
				System.out.println("deployment of 10.1.15.158 EXE TEST node finishes");
			}
			
			//判断EXE DEV node部署是否成功
			file=new File(EXE_DEV_DEPLOYED);
			while(!file.exists()){//部署失败，重启jboss
				System.out.println("deployment of 10.1.15.158 EXE DEV node failed, it needs to restart");
				try {
					Runtime.getRuntime().exec(STOP_EXE_DEV);
					System.out.println("EXE DEV node is stopping");
					Thread.sleep(30000);
					Runtime.getRuntime().exec(START_EXE_DEV);
					System.out.println("EXE DEV node is starting");
					Thread.sleep(30000);
				}catch(InterruptedException e1){
					e1.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(file.exists()){//部署成功
				System.out.println("deployment of 10.1.15.158 EXE DEV node finishes");
			}
		}
		
		/************************************************ 119 testing environment *************************************************/
		//本机是10.1.15.119 WEB node
		if(localhost.equals(NodeIP.WEB_119.getIp())){
			System.out.println("it is 10.1.15.119 WEB node");
			
			//判断WEB node部署是否成功
			file=new File(WEB_DEPLOYED);
			while(!file.exists()){//部署失败，重启jboss
				System.out.println("deployment of 10.1.15.119 WEB node failed, it needs to restart");
				try {
					Runtime.getRuntime().exec(STOP_WEB);
					System.out.println("WEB node is stopping");
					Thread.sleep(120000);
					Runtime.getRuntime().exec(START_WEB);
					System.out.println("WEB node is starting");
					Thread.sleep(120000);
				}catch(InterruptedException e1){
					e1.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(file.exists()){//部署成功
				System.out.println("deployment of 10.1.15.119 WEB node finishes");
			}
		}
		
		//本机是10.1.15.123 SCH node
		if(localhost.equals(NodeIP.SCH_119.getIp())){
			System.out.println("it is 10.1.15.123 SCH node");
			
			//判断SCH node部署是否成功
			file=new File(SCH_DEPLOYED);
			while(!file.exists()){//部署失败，重启jboss
				System.out.println("deployment of 10.1.15.123 SCH node failed, it needs to restart");
				try {
					Runtime.getRuntime().exec(STOP_SCH);
					System.out.println("SCH node is stopping");
					Thread.sleep(120000);
					Runtime.getRuntime().exec(START_SCH);
					System.out.println("SCH node is starting");
					Thread.sleep(120000);
				}catch(InterruptedException e1){
					e1.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(file.exists()){//部署成功
				System.out.println("deployment of 10.1.15.123 SCH node finishes");
			}
		}
		
		//本机是10.1.15.124 EXE node
		if(localhost.equals(NodeIP.EXE1_119.getIp())){
			System.out.println("it is 10.1.15.124 EXE1 node");
			
			//重启服务器的次数
			int restartNumber=0;
			
			//判断EXE1 node部署是否成功
			file=new File(EXE1_DEPLOYED);
			while(!file.exists()&&restartNumber!=5){//部署失败，重启jboss
				System.out.println("deployment of 10.1.15.124 EXE1 node failed, it needs to restart");
				try {
					Runtime.getRuntime().exec(STOP_EXE1);
					System.out.println("EXE1 node is stopping");
					Thread.sleep(120000);
					Runtime.getRuntime().exec(START_EXE1);
					System.out.println("EXE1 node is starting");
					Thread.sleep(120000);
				}catch(InterruptedException e1){
					e1.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
				restartNumber++;
			}
			
			//如果重启5次Jboss都是失败的话，则不再启动Jboss
			if(restartNumber==5){
				try {
					Runtime.getRuntime().exec(STOP_EXE1);
					System.out.println("EXE1 node is stopping");
					Thread.sleep(120000);
				}catch(InterruptedException e1){
					e1.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			if(file.exists()){//部署成功
				System.out.println("deployment of 10.1.15.124 EXE1 node finishes");
			}else{//部署失败
				System.out.println("deployment of 10.1.15.124 EXE1 node fail");
			}
		}
		
		//本机是10.1.15.149 EXE node
		if(localhost.equals(NodeIP.EXE2_119.getIp())){
			System.out.println("it is 10.1.15.149 EXE2 node");
			
			//判断EXE1 node部署是否成功
			file=new File(EXE2_DEPLOYED);
			while(!file.exists()){//部署失败，重启jboss
				System.out.println("deployment of 10.1.15.149 EXE2 node failed, it needs to restart");
				try {
					Runtime.getRuntime().exec(STOP_EXE2);
					System.out.println("EXE2 node is stopping");
					Thread.sleep(120000);
					Runtime.getRuntime().exec(START_EXE2);
					System.out.println("EXE2 node is starting");
					Thread.sleep(120000);
				}catch(InterruptedException e1){
					e1.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(file.exists()){//部署成功
				System.out.println("deployment of 10.1.15.149 EXE2 node finishes");
			}
		}
    }
    
    enum NodeIP{
    	// 151 and 156 testing environment
    	WEB("10.1.15.156"),SCH("10.1.15.157"),EXE("10.1.15.158"),
    	// 119 testing environment
    	WEB_119("10.1.15.119"),SCH_119("10.1.15.123"),EXE1_119("10.1.15.124"),EXE2_119("10.1.15.149");
    	
    	private NodeIP(String ip){
    		this.ip=ip;	
    	}
    	
    	private String ip;
    	
    	public String getIp(){
    		return this.ip;	
    	}
    	
    	public void setIp(String ip){
    		this.ip=ip;	
    	}
    }
}