<%@ page language="java" pageEncoding="utf-8" isELIgnored="false"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<html>
<head>
<base href="<%=basePath%>">

<!-- refresh the page automatically by using a element 'meta' with attribute 'http-equiv'
			whose value is refresh.the attribute 'content' indicates the time of refreshing whose
			unity is minute,attribute 'url' and attribute 'charset' -->
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
</head>
<body>
	<s:form action="showMessage.action">
		<s:submit name="showMessage" value="showMessage" />
	</s:form>
	
	<c:if test="${!empty requestScope.messages}">
		<table border="1">
			<c:forEach var="messages" items="${requestScope.messages}" begin="0"
				end="${fn:length(requestScope.messages)-1}" step="1">
				<tr>
					<td>
						<c:out value="${messages.id}" />
					</td>	
					<td>
						<c:out value="${messages.title}" /> 
					</td>
					<td>
						<c:out value="${messages.description}" /> 
					</td>
					<td>
						<c:out value="${messages.creationTime}" />
					</td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
</body>
</html>