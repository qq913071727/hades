<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Insert title here</title>
</head>
<body>
	<center>
		<h1>Message Management System</h1>
		<s:a href="index.jsp">home</s:a>
		<s:a href="addMessage.jsp">add message</s:a>
		<s:a href="deleteMessage.jsp">delete message</s:a>
		<s:a href="updateMessage.jsp">update message</s:a>
		<s:a href="searchMessage.jsp">search message</s:a>
	</center>
</body>
</html>