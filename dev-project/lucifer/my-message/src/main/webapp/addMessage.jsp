<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<h1>Message Management System</h1>
		<s:a href="index.jsp">home</s:a>
		<s:a href="addMessage.jsp">add message</s:a>
		<s:a href="deleteMessage.jsp">delete message</s:a>
		<s:a href="updateMessage.jsp">update message</s:a>
		<s:a href="searchMessage.jsp">search message</s:a>
		
		<br />
		<s:if test="result==true">adding is successful</s:if>
		<s:elseif test="result==false">adding failed</s:elseif>
		
		<s:form action="addMessage.action">
			<s:textfield name="title" label="标题" size="60" />
			<s:textarea label="描述" name="description" cols="100"	rows="12" />
			<s:submit name="submit" value="submit" />
			<s:actionerror />
			<s:fielderror>
				<s:param>title</s:param>
			</s:fielderror>
			<s:actionmessage/>
		</s:form>
	</center>
</body>
</html>