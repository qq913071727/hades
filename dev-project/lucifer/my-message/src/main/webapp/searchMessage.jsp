<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>  
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<h1>Message Management System</h1>
		<s:a href="index.jsp">home</s:a>
		<s:a href="addMessage.jsp">add message</s:a>
		<s:a href="deleteMessage.jsp">delete message</s:a>
		<s:a href="updateMessage.jsp">update message</s:a>
		<s:a href="searchMessage.jsp">search message</s:a>
		<s:form action="searchMessage.action">
			<s:textfield name="keyWord" label="关键字" size="60"/>
			<s:submit name="submit" value="submit" />		
		
			<c:if test="${!empty requestScope.messages}">
				<table border="1">
					<c:forEach var="i" begin="0" end="${fn:length(requestScope.messages)-1}">
						<tr>
							<td>
								<c:out value="${requestScope.messages[i].title}" /> 
							</td>
							<td>
								<c:out value="${requestScope.messages[i].description}" /> 								
							</td>
							<td>
								<c:out value="${requestScope.messages[i].creationTime}" /> 
							</td>
						</tr>
					</c:forEach>
				</table>
				</c:if>
		</s:form>
	</center>
</body>
</html>