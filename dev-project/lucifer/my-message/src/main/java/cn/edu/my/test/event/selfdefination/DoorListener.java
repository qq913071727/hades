package cn.edu.my.test.event.selfdefination;

import java.util.EventListener;

/**
 * 
 * @author user
 * public interface EventListener:
 * A tagging interface that all event listener interfaces must extend. 
 */
public interface DoorListener extends EventListener {
	public void doorEvent(DoorEvent event);
}
