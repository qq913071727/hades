package cn.edu.my.test.swing;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class TestSwing1 extends JFrame {

	private static final long serialVersionUID = -5082982067437464587L;
	private JLabel jLabel;
	private JTextField jTextField;
	private JButton jButton;

	public TestSwing1() {
		super();
		this.setSize(300, 200);//Resizes this component so that it has width width and height height
		/*public Container getContentPane():Returns the contentPane object for this frame
		  public void setLayout(LayoutManager mgr):Sets the layout manager for this container*/
		this.getContentPane().setLayout(null);
		/*public void add(Component comp,Object constraints):Adds the specified component to the end of this container. 
		  Also notifies the layout manager to add the component to this container's layout using the specified constraints object*/
		this.add(getJLabel(), null);
		this.add(getJTextField(), null);
		this.add(getJButton(), null);
		this.setTitle("HelloWorld");
	}

	private javax.swing.JLabel getJLabel() {
		if (jLabel == null) {
			jLabel = new javax.swing.JLabel();
			/*public void setBounds(int x,int y,int width,int height):
			  Moves and resizes this component. The new location of the top-left corner is specified by x and y, 
			  and the new size is specified by width and height*/
			jLabel.setBounds(34, 49, 53, 18);
			jLabel.setText("Name:");
		}
		return jLabel;
	}

	private javax.swing.JTextField getJTextField() {
		if (jTextField == null) {
			jTextField = new javax.swing.JTextField();
			jTextField.setBounds(96, 49, 160, 20);
		}
		return jTextField;
	}

	private javax.swing.JButton getJButton() {
		if (jButton == null) {
			jButton = new javax.swing.JButton();
			jButton.setBounds(103, 110, 71, 27);
			jButton.setText("OK");
		}
		return jButton;
	}

	public static void main(String[] args) {
		TestSwing1 t = new TestSwing1();
		//public void setVisible(boolean b):Shows or hides this component depending on the value of parameter b
		t.setVisible(true);
	}
}
