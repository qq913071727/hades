package cn.edu.my.test.event.selfdefination;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 * �¼�Դ��������������԰��������һ�����ƿ��Ź��ŵ�ң������(�������swing�У�������һ��button)
 * @author user
 *
 */
public class DoorManager {

	private Collection<DoorListener> listeners;

	/**
	 * ����¼�
	 * @param listener
	 */
	public void addDoorListener(DoorListener listener) {
		if (listeners == null) {
			listeners = new HashSet<DoorListener>();
		}
		listeners.add(listener);
	}

	/**
	 * �Ƴ��¼�
	 * @param listener
	 */
	public void removeDoorListener(DoorListener listener) {
		if (listeners == null){
			return;
		}
		listeners.remove(listener);
	}

	/**
	 * ���������¼�
	 */
	protected void fireWorkspaceOpened() {
		if (listeners == null){
			return;
		}
		DoorEvent event = new DoorEvent(this, "open");
		notifyListeners(event);
	}

	/**
	 * ���������¼�
	 */
	protected void fireWorkspaceClosed() {
		if (listeners == null){
			return;
		}
		DoorEvent event = new DoorEvent(this, "close");
		notifyListeners(event);
	}

	/**
	 * ֪ͨ���е�DoorListener
	 * @param event
	 */
	private void notifyListeners(DoorEvent event) {
		Iterator<DoorListener> iter = listeners.iterator();
		while (iter.hasNext()) {
			DoorListener listener = (DoorListener) iter.next();
			listener.doorEvent(event);
		}
	}
}