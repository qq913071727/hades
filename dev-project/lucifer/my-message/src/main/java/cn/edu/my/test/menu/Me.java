package cn.edu.my.test.menu;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Me extends JFrame implements ActionListener {

	private static final long serialVersionUID = -2507870942776374296L;
	
	/*public class MenuBar extends MenuComponent implements MenuContainer, Accessible:
	The MenuBar class encapsulates the platform's concept of a menu bar bound to a frame. 
	In order to associate the menu bar with a Frame object, call the frame's setMenuBar method. */
	MenuBar mb;
	/*public class Menu extends MenuItem implements MenuContainer, Accessible:
	A Menu object is a pull-down menu component that is deployed from a menu bar. */
	Menu m1,m2;
	/*public class MenuItem extends MenuComponent implements Accessible:
	All items in a menu must belong to the class MenuItem, or one of its subclasses. */
	MenuItem mi1, mi2, mi3;

	public Me() {
	
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		mb = new MenuBar();
		m1 = new Menu("�ļ�");
		mi1 = new MenuItem("���ܣ�");
		mi2 = new MenuItem("���ܣ�");
		mi3 = new MenuItem("�˳�");
		
		m1.add(mi1);
		m1.add(mi2);
		m1.addSeparator();
		m1.add(mi3);
		mb.add(m1);
		
		mi1.addActionListener(this);
		mi2.addActionListener(this);
		mi3.addActionListener(this);
		
		m2=new Menu("test"); 
		m2.add(new MenuItem("a"));
		m2.addSeparator();
		m2.add(new MenuItem("b"));		
		m2.add(new MenuItem("c"));
		mb.add(m2);
		
		setMenuBar(mb);
		setSize(320, 240);
		setVisible(true);
	}

	public static void main(String args[]) {
		new Me();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == mi1)
			setTitle("���ܣ�");
		else if (e.getSource() == mi2)
			setTitle("���ܣ�");
		else
			setTitle("�˳�");
	}
}
