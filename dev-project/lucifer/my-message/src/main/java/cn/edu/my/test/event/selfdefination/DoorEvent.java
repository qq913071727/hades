package cn.edu.my.test.event.selfdefination;

import java.util.EventObject;

/**
 * �¼���
 * @author user
 * public class EventObject extends Object implements Serializable:
 * The root class from which all event state objects shall be derived.
 * All Events are constructed with a reference to the object, the "source", 
 * that is logically deemed to be the object upon which the Event in question initially occurred upon. 
 */
public class DoorEvent extends EventObject {

	private static final long serialVersionUID = -6786626989430405572L;
	private String doorState = "";// ��ʾ�ŵ�״̬���С������͡��ء�����

	public DoorEvent(Object source, String doorState) {
		super(source);
		this.doorState = doorState;
	}

	public void setDoorState(String doorState) {
		this.doorState = doorState;
	}

	public String getDoorState() {
		return this.doorState;
	}
}
