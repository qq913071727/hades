package cn.edu.my.test.swing;

import java.awt.BorderLayout;  
import java.awt.Container;  
  
import javax.swing.JButton;  
import javax.swing.JFrame;  
  
public class TestSwing2 {  
      
    public static void main(String[] args) {  
		JFrame jf= new JFrame("test");  
		Container c= new Container();  
		jf.setSize(200,200);  
		jf.setLocation(100, 200);  
		jf.setLayout(new BorderLayout());  
		JButton b= new JButton("go");  
		c=jf.getContentPane();  
		c.add(b,BorderLayout.SOUTH);  
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
		jf.setVisible(true);    
    }    
} 
