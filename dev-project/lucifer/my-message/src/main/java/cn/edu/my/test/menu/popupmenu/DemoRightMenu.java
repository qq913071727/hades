package cn.edu.my.test.menu.popupmenu;

import javax.swing.*;
import java.awt.event.*;

public class DemoRightMenu extends JFrame {

	private static final long serialVersionUID = 1993431375070770081L;
	JMenu fileMenu;
	JPopupMenu jPopupMenuOne;
	JMenuItem openFile, closeFile, exit;// �ֱ��ÿ����ť����¼������OK��..
	JRadioButtonMenuItem copyFile, pasteFile;
	ButtonGroup buttonGroupOne;

	public DemoRightMenu() {
		jPopupMenuOne = new JPopupMenu();// ����jPopupMenuOne����
		buttonGroupOne = new ButtonGroup();
		
		// �����ļ��˵����Ӳ˵��������Ӳ˵���ӵ��ļ��˵���
		fileMenu = new JMenu("�ļ�");
		openFile = new JMenuItem("��");// �ֱ��ÿ����ť����¼������OK��..
		closeFile = new JMenuItem("�ر�");
		
		fileMenu.add(openFile);
		fileMenu.add(closeFile);
		
		jPopupMenuOne.add(fileMenu);// ��fileMenu�˵���ӵ�����ʽ�˵���
		jPopupMenuOne.addSeparator();// ��ӷָ��
		
		// ������ѡ�˵������ӵ�ButtonGroup������
		copyFile = new JRadioButtonMenuItem("����");
		pasteFile = new JRadioButtonMenuItem("ճ��");
		
		// group ֻ����ά����֮����߼���ϵ
		buttonGroupOne.add(copyFile);
		buttonGroupOne.add(pasteFile);
		
		jPopupMenuOne.add(copyFile);// ��copyFile��ӵ�jPopupMenuOne��
		jPopupMenuOne.add(pasteFile);// ��pasteFile��ӵ�jPopupMenuOne��
		jPopupMenuOne.addSeparator();
		
		exit = new JMenuItem("�˳�");
		jPopupMenuOne.add(exit);// ��exit��ӵ�jPopupMenuOne��
		
		// ��������������
		MouseListener popupListener = new PopupListener(jPopupMenuOne);
		this.addMouseListener(popupListener);// ��������ע�������
		this.setTitle("����ʽ�˵��ļ�ʹ��");
		this.setBounds(100, 100, 250, 150);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String args[]) {
		new DemoRightMenu();
	}

	// ����ڲ��࣬����չ��MouseAdapter�࣬������������¼�
	class PopupListener extends MouseAdapter {
		JPopupMenu popupMenu;

		PopupListener(JPopupMenu popupMenu) {
			this.popupMenu = popupMenu;
		}

		public void mousePressed(MouseEvent e) {
			showPopupMenu(e);
		}

		public void mouseReleased(MouseEvent e) {
			showPopupMenu(e);
		}

		private void showPopupMenu(MouseEvent e) {
			if (e.isPopupTrigger()) {// ���ǰ�¼�������¼���أ��򵯳��˵�
				// �ؼ������䣬������ʾ�ġ���
				popupMenu.show(e.getComponent(), e.getX(), e.getY());
			}// ����if
		}// ����showPopupMenu
	} // �����ڲ���PopupListener
}// ����DemoJPopupMenu