package cn.edu.my.test.enumeration;

public class TestEnum {
	/* ����ͨ��ö�� */
	public enum ColorSelect {
		red, green, yellow, blue;
	}

	// public static void main(String[] args){
	// // ColorSelect m=ColorSelect.red;
	// // if(m.ordinal()==0)
	// // System.out.println("1");
	// System.out.println(Season.getBest());
	// }

	/* ö��Ҳ������һ�����һ����ӷ���������,�����Ϊ����Ӿ�̬�ͷǾ�̬�����Ի򷽷�,��һ�ж�������һ���������������. */
	public enum Season {
		// ö���б����д����ǰ�棬����������
		winter, spring, summer, fall;
		private final static String location = "Phoenix";

		public static Season getBest() {
			if (location.equals(/* "Phoenix" */"1"))
				return winter;
			else
				return summer;
		}
	}

	/* �������й��췽�� */
	public enum Temp {
		/*
		 * ͨ�����Ÿ�ֵ,���ұ����д�ι�������һ���Ը������������� ��ֵ�����Ƕ���ֵ�򶼲���ֵ������һ���ָ�ֵһ���ֲ���ֵ
		 * ���ֵ����д����������ֵ����Ҳ����
		 */
		absoluteZero(-459), freezing(32), boiling(212), paperBurns(451);
		private final int value;
		
		public int getValue() {
			return value;
		}

		// ������Ĭ��Ҳֻ����private, �Ӷ�֤���캯��ֻ�����ڲ�ʹ��
		Temp(int value) {
			this.value = value;
		}
	}

	public static void main(String[] args) {
		/*
		 * ö��������һ�����ͣ����ڶ�������������Ʊ����ĸ�ֵ ��ֵʱͨ��"ö����.ֵ"��ȡ�����ö���е�ֵ
		 */
		ColorSelect m = ColorSelect.blue;
		switch (m) {
		/*
		 * ע��:ö����д��ToString(),˵��ö�ٱ�����ֵ�ǲ���ǰ׺������Ϊblue���ColorSelect.blue
		 */
		case red:
			System.out.println("color is red");
			break;
		case green:
			System.out.println("color is green");
			break;
		case yellow:
			System.out.println("color is yellow");
			break;
		case blue:
			System.out.println("color is blue");
			break;
		}

		System.out.println("����ColorSelect�е�ֵ");
		/* ͨ��values()���ö��ֵ������ */
		for (ColorSelect c : ColorSelect.values()) {
			System.out.println(c);
		}
		System.out.println("ö��ColorSelect�е�ֵ�У�" + ColorSelect.values().length
				+ "��");
	}
}