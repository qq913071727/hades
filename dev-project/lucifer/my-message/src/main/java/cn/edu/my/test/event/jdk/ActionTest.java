package cn.edu.my.test.event.jdk;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ActionTest {
	private static JFrame frame; // ����Ϊ��̬�����Ա�mainʹ��
	private static JPanel myPanel; // ������������ð�ť���
	private JButton button1; // ���ﶨ�尴ť���

	public ActionTest() { // ������, ����ͼ�ν���		
		myPanel = new JPanel();// �½����
		button1 = new JButton("��ť1"); // �½���ť1		
		SimpleListener ourListener = new SimpleListener();// ����һ��actionlistener�ð�ť1ע�ᣬ�Ա���Ӧ�¼�
		
		button1.addActionListener(ourListener);
		myPanel.add(button1); // ��Ӱ�ť�����
	}

	public static void main(String s[]) {
		ActionTest gui = new ActionTest(); // �½�Simple1���
		frame = new JFrame("Simple1"); // �½�JFrame
		
		// ����ر��¼���ͨ������
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		frame.getContentPane().add(myPanel);
		frame.pack();
		frame.setVisible(true);
	}
}
