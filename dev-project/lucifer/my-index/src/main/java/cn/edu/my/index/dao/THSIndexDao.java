package cn.edu.my.index.dao;

import cn.edu.my.index.model.THSIndex;

import java.util.List;

public interface THSIndexDao{
	void save(List<THSIndex> thsIndexList);
}