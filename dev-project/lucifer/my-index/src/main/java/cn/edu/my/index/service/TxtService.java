package cn.edu.my.index.service;

import java.util.List;

import cn.edu.my.index.model.THSIndex;

public interface TxtService{
	/**
	* 读取txt文件
	*/
	List<THSIndex> readTxt(String txtFilePath);
	String validateTxtLine(String[] txtLine);
	int getYear(String txtFilePath);
	int getMonth(String txtFilePath);
	int getDay(String txtFilePath);
}