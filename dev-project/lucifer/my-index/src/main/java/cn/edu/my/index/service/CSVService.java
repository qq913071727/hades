package cn.edu.my.index.service;

import java.util.List;

import cn.edu.my.index.model.THSIndex;

public interface CSVService{
	/**
	* 读取CSV文件
	*/
	List<THSIndex> readCsv(String csvFilePath);
           
	/**
	* 写入CSV文件
	*/
	void writeCsv(List<THSIndex> thsIndexList);
}