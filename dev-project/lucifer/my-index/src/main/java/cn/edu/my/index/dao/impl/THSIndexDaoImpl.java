package cn.edu.my.index.dao.impl;

import cn.edu.my.index.dao.THSIndexDao;
import cn.edu.my.index.model.THSIndex;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.List;

public class THSIndexDaoImpl implements THSIndexDao{
	EntityManagerFactory factory = Persistence.createEntityManagerFactory("mytool");
    EntityManager em = factory.createEntityManager();
    
	public void save(List<THSIndex> thsIndexList){
		try{
			em.getTransaction().begin();
			for(int i=0; i<thsIndexList.size(); i++){
				em.persist(thsIndexList.get(i)); //持久化实体
			}
	        //em.flush();
	        em.getTransaction().commit();
	    }finally{
            em.close();
	        factory.close();
        }
	}
}