package cn.edu.my.index.service.impl;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.math.BigDecimal;

import cn.edu.my.index.service.TxtService;
import cn.edu.my.index.model.THSIndex;

public class TxtServiceImpl implements TxtService{
	
	public List<THSIndex> readTxt(String txtFilePath){
		File file = new File(txtFilePath);
		String encoding = "GBK";
		List<THSIndex> thsIndexList=new ArrayList<THSIndex>();
		if(file.isFile() && file.exists()) {
			try{
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), encoding);
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTXT = null;//表示每一行
				bufferedReader.readLine();//跳过表头
				
				while ((lineTXT = bufferedReader.readLine()) != null) {
					String[] str=lineTXT.toString().split("\t");
					THSIndex thsIndex=new THSIndex();
					
					//验证数据
					String result=validateTxtLine(str);
					if(result!=null){
						System.out.println(result);
						continue;
					}
					
					thsIndex.setIndexDate(new Date(this.getYear(txtFilePath),this.getMonth(txtFilePath),this.getDay(txtFilePath)));
					if(!str[0].equals("--")){
						thsIndex.setSectionName((str[0]));	
					}
					if(!str[2].equals("--")){
						thsIndex.setRiseExtent(new BigDecimal(str[2].replace("%","")));
					}
					if(!str[3].equals("--")){
						thsIndex.setRiseSpeed(new BigDecimal(str[3]));
					}
					if(!str[4].equals("--")){
						thsIndex.setDdeNetAmount(new BigDecimal(str[4]));
					}
					if(!str[5].equals("--")){
						thsIndex.setDdeSum(new BigDecimal(str[5].replace("+","")));
					}
					if(!str[6].equals("--")){
						thsIndex.setAmountRatio(new BigDecimal(str[6]));
					}
					if(!str[7].equals("--")){
						thsIndex.setRiseNumber(new BigDecimal(str[7]));
					}
					if(!str[8].equals("--")){
						thsIndex.setDropNumber(new BigDecimal(str[8]));
					}
					if(!str[9].equals("--")){
						thsIndex.setLeadingRiseShare(str[9]);
					}
					if(!str[10].equals("--")){
						thsIndex.setLastFivedayRiseExtent(new BigDecimal(str[10].replace("%","")));
					}
					if(!str[11].equals("--")){
						thsIndex.setLastTendayRiseExtent(new BigDecimal(str[11].replace("%","")));
					}
					if(!str[12].equals("--")){
						thsIndex.setLastTwentydayRiseExtent(new BigDecimal(str[12].replace("%","")));
					}
					if(!str[13].equals("--")){
						thsIndex.setTransactionVolume(new BigDecimal(str[13]));
					}
					if(!str[14].equals("--")){
						thsIndex.setTransactionSum(new BigDecimal(str[14].replace(".00","")));
					}
			    	if(!str[15].equals("--")){
			    		thsIndex.setTotalMarketValue(new BigDecimal(str[15].replace(".00","")));
			    	}
			    	if(!str[16].equals("--")){
			    		thsIndex.setCirculatingMarketValue(new BigDecimal(str[16]));
			    	}
			    	thsIndexList.add(thsIndex);
				}
				read.close();
			}catch(Exception e){
				e.printStackTrace();	
			}
		}else{
			System.out.println("找不到指定的文件！");
		}
		return thsIndexList;
	}
	
	public String validateTxtLine(String[] str){
		if(str==null){
			return "此行无数据!";
		}
		int total=0;
		for(int i=0; i<str.length; i++){
			if(str[i].equals("--")){
				total++;
			}
		}
		if(total==str.length){
			return "此行无有效数据!";
		}
		if(str[0].equals("--")){
			return "板块名称不能为空!";
		}
		if(str.length!=17){
			return "此行数据不足17列!";
		}
		return null;
	}
	
	public int getYear(String txtFilePath){
		return Integer.parseInt(txtFilePath.split("_")[1].replace(".xls","").substring(0,4))-1900;
	}
	
	public int getMonth(String txtFilePath){
		//1至9月
		if(txtFilePath.split("_")[1].replace(".xls","").substring(4,5).equals("0")){
			return Integer.parseInt(txtFilePath.split("_")[1].replace(".xls","").substring(5,6))-1;
		}else{//10至12月
			return Integer.parseInt(txtFilePath.split("_")[1].replace(".xls","").substring(4,6))-1;
		}
	}
	
	public int getDay(String txtFilePath){
		//1至9日
		if(txtFilePath.split("_")[1].replace(".xls","").substring(6,7).equals("0")){
			return Integer.parseInt(txtFilePath.split("_")[1].replace(".xls","").substring(7,8));
		}else{//10至31日
			return Integer.parseInt(txtFilePath.split("_")[1].replace(".xls","").substring(6,8));
		}
	}
}