package cn.edu.my.index.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Date;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.math.BigDecimal;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

import cn.edu.my.index.service.CSVService;
import cn.edu.my.index.model.THSIndex;

public class CSVServiceImpl implements CSVService{
	
	private static CSVServiceImpl csvService;
	private List<THSIndex> thsIndexList=new ArrayList<THSIndex>();
	
	private CSVServiceImpl(){
	}
	
	public static CSVService getInstance(){
		if(null==csvService){
			csvService=new CSVServiceImpl();	
		}
		return new CSVServiceImpl();
	}
	
	/**
	* 读取CSV文件
	*/
	public List<THSIndex> readCsv(String csvFilePath){
		try {
			String indexDate=csvFilePath.split("_")[1].substring(0,csvFilePath.split("_")[1].length()-4);
			int year=Integer.parseInt(indexDate.substring(0,indexDate.length()-4))-1900;
			int month=Integer.parseInt(indexDate.substring(4,indexDate.length()-2))-1;
			int day=Integer.parseInt(indexDate.substring(6,indexDate.length()));
			
			CsvReader reader = new CsvReader(csvFilePath,',',Charset.forName("GBK"));    //中文编码读
	        reader.readHeaders(); // 跳过表头,如果需要表头的话,不要写这句。
	        
	        while(reader.readRecord()){ //逐行读入除表头的数据
	        	THSIndex thsIndex=new THSIndex();
	        	
	        	thsIndex.setIndexDate(new Date(year,month,day));
	        	thsIndex.setSectionName(reader.getValues()[0]);
	        	if(!reader.getValues()[1].toString().equals("--")){
	        		thsIndex.setRiseExtent(new BigDecimal(reader.getValues()[1].replace("%","")));
	        	}
	        	if(!reader.getValues()[2].toString().equals("--")){
	        		thsIndex.setRiseSpeed(new BigDecimal(reader.getValues()[2].replace("%","")));
	        	}
	        	if(!reader.getValues()[3].toString().equals("--")){
	        		thsIndex.setDdeNetAmount(new BigDecimal(reader.getValues()[3]));
	        	}
	        	if(!reader.getValues()[4].toString().equals("--")){
	        		thsIndex.setDdeSum(new BigDecimal(reader.getValues()[4]));
	        	}
	        	if(!reader.getValues()[5].toString().equals("--")){
	        		thsIndex.setAmountRatio(new BigDecimal(reader.getValues()[5]));
	        	}
	        	if(!reader.getValues()[6].toString().equals("--")){
	        		thsIndex.setRiseNumber(new BigDecimal(reader.getValues()[6]));
	        	}
	        	if(!reader.getValues()[7].toString().equals("--")){
	        		thsIndex.setDropNumber(new BigDecimal(reader.getValues()[7]));
	        	}
	        	if(!reader.getValues()[8].toString().equals("--")){
	        		thsIndex.setLeadingRiseShare(reader.getValues()[8].toString());
	        	}
	        	if(!reader.getValues()[9].toString().equals("--")){
	        		thsIndex.setLastFivedayRiseExtent(new BigDecimal(reader.getValues()[9].replace("%","")));
	        	}
	        	if(!reader.getValues()[10].toString().equals("--")){
	        		thsIndex.setLastTendayRiseExtent(new BigDecimal(reader.getValues()[10].replace("%","")));
	        	}
	        	if(!reader.getValues()[11].toString().equals("--")){
	        		thsIndex.setLastTwentydayRiseExtent(new BigDecimal(reader.getValues()[11].replace("%","")));
	        	}
	        	if(!reader.getValues()[12].toString().equals("--")){
	        		thsIndex.setTransactionVolume(new BigDecimal(reader.getValues()[12]));
	        	}
	        	if(!reader.getValues()[13].toString().equals("--")){
	        		thsIndex.setTransactionSum(new BigDecimal(reader.getValues()[13]));
	        	}
	        	if(!reader.getValues()[14].toString().equals("--")){
	        		thsIndex.setTotalMarketValue(new BigDecimal(reader.getValues()[14]));
	        	}
	        	if(!reader.getValues()[15].toString().equals("--")){
	        		thsIndex.setCirculatingMarketValue(new BigDecimal(reader.getValues()[15]));
	        	}
	        	thsIndexList.add(thsIndex);
	        }            
	        reader.close();
	    }catch (IOException e) {
           	e.printStackTrace();
	    }catch(Exception ex){
	       	ex.printStackTrace();
	    }
	    return thsIndexList;
	}
           
	/**
	* 写入CSV文件
	*/
	public void writeCsv(List<THSIndex> dayList){
		
	}
}
