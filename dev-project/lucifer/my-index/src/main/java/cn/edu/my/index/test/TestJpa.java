package cn.edu.my.index.test;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import cn.edu.my.index.model.THSIndex;
import cn.edu.my.index.model.THSIndexPK;

public class TestJpa{
	public static void main(String[] args){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("mytool");
        EntityManager em = factory.createEntityManager();
        
        em.getTransaction().begin();
        THSIndex thsIndex=new THSIndex();
        thsIndex.setIndexDate(new Date());
        thsIndex.setSectionName("321");
        em.persist(thsIndex); //持久化实体
        em.getTransaction().commit();
        
        em.close();
        factory.close();
    }
}