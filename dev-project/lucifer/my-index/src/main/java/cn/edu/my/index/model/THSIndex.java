package cn.edu.my.index.model;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.IdClass;

import cn.edu.my.index.model.THSIndexPK;

@Entity
@Table(name = "THS_INDEX")
@IdClass(THSIndexPK.class)
public class THSIndex implements Serializable{
	private static final long serialVersionUID = 4943396187256597131L;
	
	@Id
	private Date indexDate;
	@Id
	private String sectionName;
	@Column(name = "RISE_EXTENT")
	private BigDecimal riseExtent;
	@Column(name = "RISE_SPEED")
	private BigDecimal riseSpeed;
	@Column(name = "DDE_NET_AMOUNT")
	private BigDecimal ddeNetAmount;
	@Column(name = "DDE_SUM")
	private BigDecimal ddeSum;
	@Column(name = "AMOUNT_RATIO")
	private BigDecimal amountRatio;
	@Column(name = "RISE_NUMBER")
	private BigDecimal riseNumber;
	@Column(name = "DROP_NUMBER")
	private BigDecimal dropNumber;
	@Column(name = "LEADING_RISE_SHARE")
	private String leadingRiseShare;
	@Column(name = "LAST_FIVEDAY_RISE_EXTENT")
	private BigDecimal lastFivedayRiseExtent;
	@Column(name = "LAST_TENDAY_RISE_EXTENT")
	private BigDecimal lastTendayRiseExtent;
	@Column(name = "LAST_TWENTYDAY_RISE_EXTENT")
	private BigDecimal lastTwentydayRiseExtent;
	@Column(name = "TRANSACTION_VOLUME")
	private BigDecimal transactionVolume;
	@Column(name = "TRANSACTION_SUM")
	private BigDecimal transactionSum;
	@Column(name = "TOTAL_MARKET_VALUE")
	private BigDecimal totalMarketValue;
	@Column(name = "CIRCULATING_MARKET_VALUE")
	private BigDecimal circulatingMarketValue;
	
	public Date getIndexDate(){
		return this.indexDate;	
	}
	
	public void setIndexDate(Date indexDate){
		this.indexDate=indexDate;	
	}
	
	public String getSectionName(){
		return this.sectionName;	
	}
	
	public void setSectionName(String sectionName){
		this.sectionName=sectionName;	
	}
	
	public BigDecimal getRiseExtent(){
		return this.riseExtent;	
	}
	
	public void setRiseExtent(BigDecimal riseExtent){
		this.riseExtent=riseExtent;	
	}
	
	public BigDecimal getRiseSpeed(){
		return this.riseSpeed;	
	}
	
	public void setRiseSpeed(BigDecimal riseSpeed){
		this.riseSpeed=riseSpeed;	
	}
	
	public BigDecimal getDdeNetAmount(){
		return this.ddeNetAmount;	
	}
	
	public void setDdeNetAmount(BigDecimal ddeNetAmount){
		this.ddeNetAmount=ddeNetAmount;	
	}
	
	public BigDecimal getDdeSum(){
		return this.ddeSum;	
	}
	
	public void setDdeSum(BigDecimal ddeSum){
		this.ddeSum=ddeSum;	
	}
	
	public BigDecimal getAmountRatio(){
		return this.amountRatio;	
	}
	
	public void setAmountRatio(BigDecimal amountRatio){
		this.amountRatio=amountRatio;	
	}
	
	public BigDecimal getRiseNumber(){
		return this.riseNumber;	
	}
	
	public void setRiseNumber(BigDecimal riseNumber){
		this.riseNumber=riseNumber;	
	}
	
	public BigDecimal getDropNumber(){
		return this.dropNumber;	
	}
	
	public void setDropNumber(BigDecimal dropNumber){
		this.dropNumber=dropNumber;	
	}
	
	public String getLeadingRiseShare(){
		return this.leadingRiseShare;	
	}
	
	public void setLeadingRiseShare(String leadingRiseShare){
		this.leadingRiseShare=leadingRiseShare;	
	}
	
	public BigDecimal getLastFivedayRiseExtent(){
		return this.lastFivedayRiseExtent;	
	}
	
	public void setLastFivedayRiseExtent(BigDecimal lastFivedayRiseExtent){
		this.lastFivedayRiseExtent=lastFivedayRiseExtent;	
	}
	
	public BigDecimal getLastTendayRiseExtent(){
		return this.lastTendayRiseExtent;	
	}
	
	public void setLastTendayRiseExtent(BigDecimal lastTendayRiseExtent){
		this.lastTendayRiseExtent=lastTendayRiseExtent;	
	}
	
	public BigDecimal getLastTwentydayRiseExtent(){
		return this.lastTwentydayRiseExtent;	
	}
	
	public void setLastTwentydayRiseExtent(BigDecimal lastTwentydayRiseExtent){
		this.lastTwentydayRiseExtent=lastTwentydayRiseExtent;	
	}
	
	public BigDecimal getTransactionVolume(){
		return this.transactionVolume;	
	}
	
	public void setTransactionVolume(BigDecimal transactionVolume){
		this.transactionVolume=transactionVolume;	
	}
	
	public BigDecimal getTransactionSum(){
		return this.transactionSum;	
	}
	
	public void setTransactionSum(BigDecimal transactionSum){
		this.transactionSum=transactionSum;	
	}
	
	public BigDecimal getTotalMarketValue(){
		return this.totalMarketValue;	
	}
	
	public void setTotalMarketValue(BigDecimal totalMarketValue){
		this.totalMarketValue=totalMarketValue;	
	}
	
	public BigDecimal getCirculatingMarketValue(){
		return this.circulatingMarketValue;	
	}
	
	public void setCirculatingMarketValue(BigDecimal circulatingMarketValue){
		this.circulatingMarketValue=circulatingMarketValue;	
	}
}