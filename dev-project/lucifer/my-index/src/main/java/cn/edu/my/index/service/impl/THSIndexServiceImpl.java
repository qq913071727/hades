package cn.edu.my.index.service.impl;

import cn.edu.my.index.model.THSIndex;
import cn.edu.my.index.service.THSIndexService;
import cn.edu.my.index.dao.THSIndexDao;
import cn.edu.my.index.dao.impl.THSIndexDaoImpl;

import java.util.List;

public class THSIndexServiceImpl implements THSIndexService{
	private THSIndexDao thsIndexDao=new THSIndexDaoImpl();
	
	public void save(List<THSIndex> thsIndexList){
		thsIndexDao.save(thsIndexList);
	}
}