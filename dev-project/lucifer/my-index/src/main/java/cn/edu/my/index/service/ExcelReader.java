package cn.edu.my.index.service;

import java.util.List;
import cn.edu.my.index.model.THSIndex;

public interface ExcelReader {
	void method2() throws Exception;
	List<THSIndex> readExcel(String file) throws Exception;
	void removeColumn(String file) throws Exception;
}