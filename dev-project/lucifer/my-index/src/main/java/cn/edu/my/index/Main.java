package cn.edu.my.index;

import java.util.List;
import java.util.ArrayList;
import java.io.File;

import cn.edu.my.index.service.THSIndexService;
import cn.edu.my.index.service.ExcelReader;
import cn.edu.my.index.service.CSVService;
import cn.edu.my.index.service.TxtService;
import cn.edu.my.index.service.impl.THSIndexServiceImpl;
import cn.edu.my.index.service.impl.CSVServiceImpl;
import cn.edu.my.index.service.impl.ExcelReaderImpl;
import cn.edu.my.index.service.impl.TxtServiceImpl;
import cn.edu.my.index.model.THSIndex;

public class Main{
	public static void main(String[] args) throws Exception{
		//insertAllFiles(args[0]);
		insertFile(args[0]);
    }
    
    //路径：D:\\云盘同步文件夹\\sync-folder\\investment\\同花顺指数
    private static void insertAllFiles(String args){
    	File dir=new File(args);
		String[] content=dir.list();
		List<File> fileList=new ArrayList<File>();
		for(int i=0;i<content.length;i++){
			File file=new File(dir+"\\"+content[i]);
			if(file.isFile()){
				fileList.add(file);
			}
		}
		
		for(int i=0;i<fileList.size();i++){
			TxtService txtService=new TxtServiceImpl();
			List<THSIndex> thsIndexList=txtService.readTxt(fileList.get(i).getPath());
			System.out.println("****************************"+fileList.get(i).getPath());
			THSIndexService thsIndexService=new THSIndexServiceImpl();
			thsIndexService.save(thsIndexList);
		}
    }
    
    //文件：D:\\同花顺指数_20140409.xls
    private static void insertFile(String args){
    	TxtService txtService=new TxtServiceImpl();
		List<THSIndex> thsIndexList=txtService.readTxt(args);
		
		THSIndexService thsIndexService=new THSIndexServiceImpl();
		thsIndexService.save(thsIndexList);
    	
    	/*ExcelReader excelReader=ExcelReaderImpl.getInstance();
		excelReader.removeColumn(args[0]);
		excelReader.readExcel(args[0]);
		
		THSIndexService thsIndexService=new THSIndexServiceImpl();
		CSVService csvService=CSVServiceImpl.getInstance();
		
    	List<THSIndex> thsIndexList=new ArrayList<THSIndex>();
    	
        try {
	    	thsIndexList=csvService.readCsv(args[0]);
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
        thsIndexService.save(thsIndexList);*/
    }
}