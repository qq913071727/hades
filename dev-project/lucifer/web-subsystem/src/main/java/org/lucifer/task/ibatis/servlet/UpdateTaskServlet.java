package org.lucifer.task.ibatis.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import org.lucifer.task.ibatis.model.Task;
import org.lucifer.task.ibatis.service.TaskService;
import org.lucifer.task.ibatis.util.DateUtil;

public class UpdateTaskServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("rawtypes")
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");

		WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		String keyWord = request.getParameter("keyWord");

		TaskService taskService = (TaskService) wac.getBean("taskService");
		
		if("searchSubmit".equals(request.getParameter("searchSubmit"))){
			List<HashMap> task=taskService.getTasksByKeyWord(keyWord);
			request.setAttribute("task", task);	
			
		}else if("updateSubmit".equals(request.getParameter("updateSubmit"))){
			Task task=new Task();
			task.setId(Long.parseLong(request.getParameter("id")));
			task.setRecordTime(DateUtil.stringToDate(request.getParameter("recordingTime")));
			task.setBeginningTime(DateUtil.stringToDate(request.getParameter("beginningTime")));
			task.setEndingTime(DateUtil.stringToDate(request.getParameter("endingTime")));
			task.setDescription(request.getParameter("descripion"));
			task.setType((request.getParameter("type")));
			task.setSolved(request.getParameter("solved").equals("true")?new Boolean(true):new Boolean(false));
			taskService.update(task);
		}
				
		request.getRequestDispatcher("/updateTask.jsp").forward(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}
}
