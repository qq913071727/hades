package org.lucifer.message.struts.action;

import java.util.Date;

import org.lucifer.message.hibernate.ServiceFactory;
import org.lucifer.message.hibernate.model.Message;
import org.lucifer.message.hibernate.service.MessageService;

import com.opensymphony.xwork2.ActionSupport;

public class AddMessageAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private MessageService messageService;

	private String title;
	private String description;
	private boolean result;

	public void validate(){
		System.out.println("**************** validate function in class AddMessageAction is invoked! **********************");	    
		
		if("".equals(title)||null==title){
			try{
				this.clearErrorsAndMessages();
				
				this.addFieldError("title","鏍囬涓嶈兘涓虹┖锛�");;
				this.addActionError("澶勭悊鍔ㄤ綔澶辫触!");
				this.addActionMessage("鎻愪氦澶辫触"); 
			}catch(Exception e){
				e.printStackTrace();
			}
		}	
	}

	@Override
	public String execute() throws Exception {
		Message m = new Message();
		m.setTitle(title);
		m.setDescription(description);
		m.setCreationTime(new Date());

		messageService = ServiceFactory.getMessageService();
		messageService.addMessage(m);
		result=true;
		return SUCCESS;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

}

