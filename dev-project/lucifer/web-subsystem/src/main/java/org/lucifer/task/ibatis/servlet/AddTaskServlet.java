package org.lucifer.task.ibatis.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import org.lucifer.task.ibatis.model.Task;
import org.lucifer.task.ibatis.service.TaskService;
import org.lucifer.task.ibatis.service.impl.TaskServiceImpl;

public class AddTaskServlet extends HttpServlet  {

	private static final long serialVersionUID = 1L;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		
		WebApplicationContext wac = 
		    WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		
		Task t=new Task();
//		t.setRecordTime(new SimpleDateFormat("yyyyMMdd").format(new Date()));
//		t.setDescription(request.getParameter("description"));
//		if("solved".equals(request.getParameter("solved"))){
//			t.setSolved("1");
//		}else{
//			t.setSolved("0");
//		}
//		if("urgentTask".equals(request.getParameter("type"))){
//			t.setType("1");
//			t.setBeginningTime("");
//			t.setEndingTime("");
//		}else if("routineTask".equals(request.getParameter("type"))){
//			t.setType("2");
//			t.setBeginningTime(request.getParameter("beginningTime"));
//			t.setEndingTime(request.getParameter("endingTime"));
//		}else{
//			t.setType("3");
//			t.setBeginningTime("");
//			t.setEndingTime("");
//		}
		
		TaskService taskService=(TaskService) wac.getBean("taskService");
		//System.out.println("!!!!!!!!!!!!!!!!!!!!:	"+taskService);
		taskService.save(t);
		
		request.getRequestDispatcher("/addTask.jsp").forward(request,response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}
}
