package org.lucifer.task.ibatis.util;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.lucifer.task.ibatis.dao.impl.TaskDaoIbatis;
import org.lucifer.task.ibatis.service.TaskService;
import org.lucifer.task.ibatis.service.impl.TaskServiceImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ibatis.sqlmap.client.SqlMapClient;

public class TaskLog implements ApplicationContextAware{
	
	//@Value("import org.springframework.beans.factory.annotation.Value")
	private String message;
	private List list;
	private Set set;
	private Object[] array;
	private Map map;
	private ApplicationContext applicationContext;
	private Properties prop;
	private double multiplier;
	private double randomNumber;
	
	public TaskLog(){
	
	}
	
	public TaskLog(String message) {
		super();
		this.message = message;
		//System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"+message);
	}

	public void printBeforeEnterMethod(){
		//System.out.println("enter the method!"+message);
	}
	
	public void printAfterLeaveMethod(){
		System.out.println("leave the method!"+message);
		
		/*System.out.println("list:	"+((TaskService)list.get(0)).getClass());
		System.out.println("list:	"+list.get(1).toString());
		System.out.println("list:	"+((List)list.get(2)).get(0).toString());
		
		Iterator iterator=set.iterator();
		System.out.println("set:	"+iterator.next().getClass());
		System.out.println("set:	"+iterator.next().toString());
		System.out.println("set:	"+((Set)iterator.next()).iterator().next().toString());
		
		System.out.println("array:	"+((TaskService)array[0]).getClass());
		System.out.println("array:	"+((String)array[1]).toString());
		System.out.println("array:	"+((List)array[2]).get(0).toString());
		
		System.out.println("map:	"+map.get("123").getClass());
		System.out.println("map:	"+map.get("testValue").toString());
		System.out.println("map:	"+map.get(applicationContext.getBean("taskDao")).getClass());
		System.out.println("!!!!!!!!!!!!!!!!!!!!:	"+TaskServiceImpl.getInstance());
		System.out.println("map:	"+map.get(TaskServiceImpl.getInstance()).getClass());
		
		System.out.println("properties:	"+prop.getProperty("123"));
		
		System.out.println("T():	"+multiplier);
		System.out.println("randomNumber:	"+randomNumber);*/
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List getList() {
		//System.out.println("(((((((((((((((((((((((((((((((((((((((((((((((("+list);
		return list;
	}

	public void setList(List list) {
		//System.out.println("))))))))))))))))))))))))))))))))))))))))))))))))))"+list);
		this.list = list;
	}

	public Set getSet() {
		return set;
	}

	public void setSet(Set set) {
		this.set = set;
	}

	public Object[] getArray() {
		return array;
	}

	public void setArray(Object[] array) {
		this.array = array;
	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}
	
	public Properties getProp() {
		return prop;
	}

	public void setProp(Properties prop) {
		this.prop = prop;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)throws BeansException {
		this.applicationContext=applicationContext;
	}

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public double getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(double multiplier) {
		this.multiplier = multiplier;
	}

	public double getRandomNumber() {
		return randomNumber;
	}

	public void setRandomNumber(double randomNumber) {
		this.randomNumber = randomNumber;
	}

}