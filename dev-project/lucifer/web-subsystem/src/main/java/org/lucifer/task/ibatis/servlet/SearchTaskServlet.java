package org.lucifer.task.ibatis.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import org.lucifer.task.ibatis.service.TaskService;
import org.lucifer.task.ibatis.util.TaskLog;

public class SearchTaskServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("rawtypes")
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ApplicationContext context;
		try{
			//context=new FileSystemXmlApplicationContext("D:/development-environment/server/apache-tomcat-6.0.29/webapps/web-subsystem/WEB-INF/taskLog.xml");
			context=new FileSystemXmlApplicationContext("D:/dev/Tomcat/apache-tomcat-6.0.29/webapps/web-subsystem/WEB-INF/taskLog.xml");
		}catch(Exception e){
			context=new FileSystemXmlApplicationContext("D:/development-environment/server/apache-tomcat-6.0.29/webapps/web-subsystem/WEB-INF/taskLog.xml");
			//context=new FileSystemXmlApplicationContext("D:/dev/Tomcat/apache-tomcat-6.0.29/webapps/web-subsystem/WEB-INF/taskLog.xml");
		}
		TaskLog taskLog= (TaskLog) context.getBean("taskLog");
		System.out.println(taskLog.getMessage());
				
		request.setCharacterEncoding("utf-8");

		WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		String keyWord = request.getParameter("keyWord");

		TaskService taskService = (TaskService) wac.getBean("taskService");

		List<HashMap> task = taskService.getTasksByKeyWord(keyWord);
		request.setAttribute("task", task);

		request.getRequestDispatcher("/searchTask.jsp").forward(request,response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}
}
