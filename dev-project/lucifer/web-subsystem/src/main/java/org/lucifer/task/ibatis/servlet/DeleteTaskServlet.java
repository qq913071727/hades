package org.lucifer.task.ibatis.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import org.lucifer.task.ibatis.model.Task;
import org.lucifer.task.ibatis.service.TaskService;

public class DeleteTaskServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		
		WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		String keyWord=request.getParameter("keyWord");
		
		TaskService taskService=(TaskService) wac.getBean("taskService");
		
		if("searchSubmit".equals(request.getParameter("searchSubmit"))){
			
			List<HashMap> task=taskService.getTasksByKeyWord(keyWord);
			request.setAttribute("task", task);		
		}else if("deleteSubmit".equals(request.getParameter("deleteSubmit"))){
			
			String[] item=request.getParameterValues("deleteItem");
			System.out.println("**********************************"+item);
			for(int i=0;i<item.length;i++){
				taskService.delete(Integer.parseInt(item[i]));
			}
		}
		request.getRequestDispatcher("/deleteTask.jsp").forward(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}
}
