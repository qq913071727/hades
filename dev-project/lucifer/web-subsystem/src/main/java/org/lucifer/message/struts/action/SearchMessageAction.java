package org.lucifer.message.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import org.lucifer.message.hibernate.ServiceFactory;
import org.lucifer.message.hibernate.model.Message;
import org.lucifer.message.hibernate.service.MessageService;

import com.opensymphony.xwork2.ActionSupport;

public class SearchMessageAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	
	private String keyWord;
	
	private MessageService messageService;

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	@Override
	public String execute() throws Exception {
		messageService = ServiceFactory.getMessageService();
		List<Message> l=messageService.getMessagesByKeyWord(keyWord);
		HttpServletRequest request = ServletActionContext.getRequest();
		request.setAttribute("messages", l);
		return SUCCESS;
	}
}
