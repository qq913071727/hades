package cn.edu.my.xml.model;

import java.util.List;

public class UrgentTask {
	private List<Task> tasks;

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
}
