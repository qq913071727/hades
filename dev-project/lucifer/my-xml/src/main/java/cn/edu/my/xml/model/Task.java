package cn.edu.my.xml.model;

import java.util.Date;

public class Task {
	private int id;
	private Date recordTime;
	private String beginningTime;
	private String endingTime;
	private String description;
	private String type;
	private String solved;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getRecordTime() {
		return recordTime;
	}
	public void setRecordTime(Date recordTime) {
		this.recordTime = recordTime;
	}
	public String getBeginningTime() {
		return beginningTime;
	}
	public void setBeginningTime(String beginningTime) {
		this.beginningTime = beginningTime;
	}
	public String getEndingTime() {
		return endingTime;
	}
	public void setEndingTime(String endingTime) {
		this.endingTime = endingTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSolved() {
		return solved;
	}
	public void setSolved(String solved) {
		this.solved = solved;
	}
}
