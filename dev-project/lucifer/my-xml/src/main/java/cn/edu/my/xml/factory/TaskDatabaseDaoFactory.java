package cn.edu.my.xml.factory;

import cn.edu.my.xml.dao.TaskDatabaseDao;
import cn.edu.my.xml.dao.impl.TaskDatabaseDaoImpl;
import cn.edu.my.xml.service.TaskDatabaseService;
import cn.edu.my.xml.service.impl.TaskDatabaseServiceImpl;

public class TaskDatabaseDaoFactory {
	private static TaskDatabaseDao taskDatabaseDao;
	
	public static TaskDatabaseDao getTaskDatabaseDao(){
		if(null==taskDatabaseDao){
			return TaskDatabaseDaoImpl.newInstance();
		}
		return taskDatabaseDao;
	}
}
