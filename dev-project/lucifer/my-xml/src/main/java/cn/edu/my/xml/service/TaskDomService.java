package cn.edu.my.xml.service;

import cn.edu.my.xml.model.CompletedTask;
import cn.edu.my.xml.model.RoutineTask;
import cn.edu.my.xml.model.UrgentTask;

public interface TaskDomService {
	UrgentTask getUrgentTask();
	void setUrgentTask(UrgentTask urgentTask);
	RoutineTask getRoutineTask();
	void setRoutineTask(RoutineTask routineTask);
	CompletedTask getCompletedTask();
	void setCompletedTask(CompletedTask completedTask);
}
