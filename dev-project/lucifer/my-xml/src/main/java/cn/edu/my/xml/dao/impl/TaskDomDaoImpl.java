package cn.edu.my.xml.dao.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import cn.edu.my.xml.dao.TaskDomDao;
import cn.edu.my.xml.model.CompletedTask;
import cn.edu.my.xml.model.RoutineTask;
import cn.edu.my.xml.model.Task;
import cn.edu.my.xml.model.UrgentTask;
import cn.edu.my.xml.test.TestTaskDom;

public class TaskDomDaoImpl implements TaskDomDao {
	private UrgentTask urgentTask;
	private RoutineTask routineTask;
	private CompletedTask completedTask;
	
	InputStream input;
	DocumentBuilderFactory factory;
	DocumentBuilder builder;
	Document document;
	
	private TaskDomDaoImpl(String inputStream) throws ParserConfigurationException, SAXException, IOException {
		input = new TestTaskDom().getClass().getClassLoader().getResourceAsStream(inputStream);
		factory = DocumentBuilderFactory.newInstance();  
        builder = factory.newDocumentBuilder();  
        document = builder.parse(input);
	}
	
	public static TaskDomDao newInstance(String inputStream) throws ParserConfigurationException, SAXException, IOException{
		return new TaskDomDaoImpl(inputStream);
	}

	@SuppressWarnings("deprecation")
	public UrgentTask getUrgentTask() {
		Element documentElement = document.getDocumentElement();

		NodeList nodeList = documentElement
				.getElementsByTagName("urgent-tasks");
		Element nodeElement = (Element) nodeList.item(0);
		NodeList nodes = nodeElement.getChildNodes();
		urgentTask = new UrgentTask();
		List<Task> tasks = new ArrayList<Task>();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			Task task = new Task();
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if ("task".equals(node.getNodeName())) {
					if("id".equals(node.getAttributes().item(0).getNodeName())){
						task.setId(Integer.parseInt(node.getAttributes().item(0).getNodeValue()));
						task.setRecordTime(new Date(Integer.parseInt(node.getAttributes().item(1).getNodeValue().substring(0, 4)),
								Integer.parseInt(node.getAttributes().item(1).getNodeValue().substring(4, 6)),
								Integer.parseInt(node.getAttributes().item(1).getNodeValue().substring(6, 8))));
					}else{
						task.setRecordTime(new Date(Integer.parseInt(node.getAttributes().item(0).getNodeValue().substring(0, 4)),
								Integer.parseInt(node.getAttributes().item(0).getNodeValue().substring(4, 6)),
								Integer.parseInt(node.getAttributes().item(0).getNodeValue().substring(6, 8))));
					}
					task.setDescription(node.getTextContent());
					task.setType("1");
					task.setSolved("0");
				}
				tasks.add(task);
			}
		}
		urgentTask.setTasks(tasks);
		return urgentTask;
	}

	public RoutineTask getRoutineTask() {
		Element documentElement = document.getDocumentElement();

		NodeList nodeList = documentElement
				.getElementsByTagName("routine-tasks");
		Element nodeElement = (Element) nodeList.item(0);
		NodeList nodes = nodeElement.getChildNodes();
		routineTask = new RoutineTask();
		List<Task> tasks = new ArrayList<Task>();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			Task task = new Task();
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if ("task".equals(node.getNodeName())) {
					task.setRecordTime(new Date());
					task.setDescription(node.getTextContent());
					task.setBeginningTime(node.getAttributes().item(0)
							.getNodeValue());
					task.setEndingTime(node.getAttributes().item(1)
							.getNodeValue());
					task.setType("2");
				}
				tasks.add(task);
			}
		}
		routineTask.setTasks(tasks);
		return routineTask;
	}

	public CompletedTask getCompletedTask() {
		Element documentElement = document.getDocumentElement();

		NodeList nodeList = documentElement
				.getElementsByTagName("completed-tasks");
		Element nodeElement = (Element) nodeList.item(0);
		NodeList nodes = nodeElement.getChildNodes();
		completedTask = new CompletedTask();
		List<Task> tasks = new ArrayList<Task>();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			Task task = new Task();
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if ("task".equals(node.getNodeName())) {
					task.setRecordTime(new Date());
					task.setDescription(node.getTextContent());
					task.setType("3");
					task.setSolved("1");
				}
				tasks.add(task);
			}
		}
		completedTask.setTasks(tasks);
		return completedTask;
	}
}
