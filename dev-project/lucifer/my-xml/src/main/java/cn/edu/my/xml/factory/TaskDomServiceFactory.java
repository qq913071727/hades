package cn.edu.my.xml.factory;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import cn.edu.my.xml.service.TaskDomService;
import cn.edu.my.xml.service.impl.TaskDomServiceImpl;

public class TaskDomServiceFactory {
	private static TaskDomService taskDomService;
	
	public static TaskDomService getTaskDomService(String inputStream) throws ParserConfigurationException, SAXException, IOException{
		if(null==taskDomService){
			return TaskDomServiceImpl.newInstance(inputStream);
		}
		return taskDomService;
	}
}
