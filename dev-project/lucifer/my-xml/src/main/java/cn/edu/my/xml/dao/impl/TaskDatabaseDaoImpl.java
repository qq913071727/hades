package cn.edu.my.xml.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import cn.edu.my.xml.dao.TaskDatabaseDao;
import cn.edu.my.xml.model.CompletedTask;
import cn.edu.my.xml.model.RoutineTask;
import cn.edu.my.xml.model.Task;
import cn.edu.my.xml.model.UrgentTask;

public class TaskDatabaseDaoImpl implements TaskDatabaseDao {
	private UrgentTask urgentTask;
	private RoutineTask routineTask;
	private CompletedTask completedTask;

	private Connection conn;
	private Statement stmt;
	private ResultSet rs;

	private TaskDatabaseDaoImpl() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			try {
				conn = DriverManager.getConnection(
						"jdbc:oracle:thin:@127.0.0.1:1521:myhometool", "scott",
						"tiger");
				stmt = conn.createStatement();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static TaskDatabaseDao newInstance() {
		return new TaskDatabaseDaoImpl();
	}

	public UrgentTask getUrgentTask() {
		urgentTask = new UrgentTask();
		urgentTask.setTasks(new ArrayList<Task>());

		try {
			rs = stmt.executeQuery("SELECT * FROM task where type=3");
			while (rs.next()) {
				urgentTask.getTasks().add(getTask());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return urgentTask;
	}

	public RoutineTask getRoutineTask() {
		routineTask = new RoutineTask();
		routineTask.setTasks(new ArrayList<Task>());

		try {
			rs = stmt.executeQuery("SELECT * FROM task where type=2");
			while (rs.next()) {
				routineTask.getTasks().add(getTask());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return routineTask;
	}

	public CompletedTask getCompletedTask() {
		completedTask = new CompletedTask();
		completedTask.setTasks(new ArrayList<Task>());

		try {
			rs = stmt.executeQuery("SELECT * FROM task where type=3");
			while (rs.next()) {
				completedTask.getTasks().add(getTask());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return completedTask;
	}

	public void setUrgentTask(UrgentTask urgentTask) {

		List<Task> list = urgentTask.getTasks();
		for (int i = 0; i < list.size(); i++) {
			Task task = new Task();
			task.setId(list.get(i).getId());
			task.setRecordTime(list.get(i).getRecordTime());
			task.setDescription(list.get(i).getDescription());
			task.setBeginningTime(list.get(i).getBeginningTime());
			task.setEndingTime(list.get(i).getEndingTime());
			task.setSolved(list.get(i).getSolved());
			task.setType(list.get(i).getType());

			setTask(task);
		}
	}

	public void setRoutineTask(RoutineTask routineTask) {
		// TODO Auto-generated method stub

	}

	public void setCompletedTask(CompletedTask completedTask) {
		// TODO Auto-generated method stub

	}

	public Task getTask() {
		Task task = new Task();
		try {
			task.setId(rs.getInt("id"));
			task.setDescription(rs.getString("description"));
			task.setBeginningTime(rs.getString("beginning_time"));
			task.setEndingTime(rs.getString("ending_time"));
			task.setRecordTime(rs.getDate("record_time"));
			task.setType(rs.getString("type"));
			task.setSolved(rs.getString("solved"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return task;
	}

	public void setTask(Task task) {
		String sql = "insert into task set 1=1 ";
		if (0!=task.getId()) {
			sql += "id=" + task.getId()+" and";
		}
		if (!"".equals(task.getDescription())) {
			sql += "id=" + task.getDescription()+" and";
		}
		if (!"".equals(task.getBeginningTime())) {

		}
		if (!"".equals(task.getEndingTime())) {

		}
		if (!"".equals(task.getRecordTime())) {

		}
		if (!"".equals(task.getType())) {

		}
		if (!"".equals(task.getSolved())) {

		}
	}

}
