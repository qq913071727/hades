package cn.edu.my.xml.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
/*import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;*/
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cn.edu.my.xml.factory.TaskDomDaoFactory;
import cn.edu.my.xml.factory.TaskDomServiceFactory;
import cn.edu.my.xml.model.CompletedTask;
import cn.edu.my.xml.model.RoutineTask;
import cn.edu.my.xml.model.Task;
import cn.edu.my.xml.model.UrgentTask;
import cn.edu.my.xml.service.TaskDomService;

public class TestTaskDom {
	public static void main(String[] args) throws Exception {
		TaskDomService dom = TaskDomServiceFactory
				.getTaskDomService("my-home-tasks.xml");

		try {
			Process p = Runtime.getRuntime().exec(
					"cmd /C dir D:\\bookmark.htm /tc");
			InputStream is = p.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String str;
			int i = 0;
			while ((str = br.readLine()) != null) {
				i++;
				if (i == 6) {
					System.out.println(str.substring(0, 17));
				}
			}

		} catch (java.io.IOException exc) {
			exc.printStackTrace();
		}

		UrgentTask urgentTask = dom.getUrgentTask();
		List<Task> urgentTasks = urgentTask.getTasks();
		System.out
				.println("******************* urgent task ******************************");
		for (int i = 0; i < urgentTasks.size(); i++) {
			Task task = (Task) urgentTasks.get(i);
			System.out.println(task.getId());
			System.out.println(task.getRecordTime());
			System.out.println(task.getDescription());
			System.out.println(task.getType());
			System.out.println(task.getSolved());
		}

		RoutineTask routineTask = dom.getRoutineTask();
		List<Task> routineTasks = routineTask.getTasks();
		System.out
				.println("******************* routine task ******************************");
		for (int i = 0; i < routineTasks.size(); i++) {
			Task task = (Task) routineTasks.get(i);
			System.out.println(task.getRecordTime());
			System.out.println(task.getDescription());
			System.out.println(task.getBeginningTime());
			System.out.println(task.getEndingTime());
			System.out.println(task.getType());
			System.out.println(task.getSolved());
		}

		CompletedTask completedTask = dom.getCompletedTask();
		List<Task> completedTasks = completedTask.getTasks();
		System.out
				.println("******************* completed task ******************************");
		for (int i = 0; i < completedTasks.size(); i++) {
			Task task = (Task) completedTasks.get(i);
			System.out.println(task.getRecordTime());
			System.out.println(task.getDescription());
			System.out.println(task.getType());
			System.out.println(task.getSolved());
		}

	}
}
