package cn.edu.my.xml.service.impl;

import cn.edu.my.xml.dao.TaskDatabaseDao;
import cn.edu.my.xml.factory.TaskDatabaseDaoFactory;
import cn.edu.my.xml.model.CompletedTask;
import cn.edu.my.xml.model.RoutineTask;
import cn.edu.my.xml.model.UrgentTask;
import cn.edu.my.xml.service.TaskDatabaseService;

public class TaskDatabaseServiceImpl implements TaskDatabaseService {
	private TaskDatabaseDao taskDatabaseDao;

	private TaskDatabaseServiceImpl(){
		taskDatabaseDao=TaskDatabaseDaoFactory.getTaskDatabaseDao();
	}
	
	public static TaskDatabaseService newInstance(){
		return new TaskDatabaseServiceImpl();
	}
	
	public UrgentTask getUrgentTask() {
		return taskDatabaseDao.getUrgentTask();
	}

	public RoutineTask getRoutineTask() {
		return taskDatabaseDao.getRoutineTask();
	}

	public CompletedTask getCompletedTask() {
		return taskDatabaseDao.getCompletedTask();
	}

	public void setUrgentTask(UrgentTask urgentTask) {
		taskDatabaseDao.setUrgentTask(urgentTask);
	}

	public void setRoutineTask(RoutineTask routineTask) {
		taskDatabaseDao.setRoutineTask(routineTask);
	}

	public void setCompletedTask(CompletedTask completedTask) {
		taskDatabaseDao.setCompletedTask(completedTask);
	}
}
