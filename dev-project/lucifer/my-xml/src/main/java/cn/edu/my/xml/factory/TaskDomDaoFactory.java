package cn.edu.my.xml.factory;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import cn.edu.my.xml.dao.TaskDomDao;
import cn.edu.my.xml.dao.impl.TaskDomDaoImpl;
import cn.edu.my.xml.service.TaskDomService;
import cn.edu.my.xml.service.impl.TaskDomServiceImpl;

public class TaskDomDaoFactory {
	private static TaskDomDao taskDomDao;
	
	public static TaskDomDao getTaskDomDao(String inputStream) throws ParserConfigurationException, SAXException, IOException{
		if(null==taskDomDao){
			return TaskDomDaoImpl.newInstance(inputStream);
		}
		return taskDomDao;
	}
}
