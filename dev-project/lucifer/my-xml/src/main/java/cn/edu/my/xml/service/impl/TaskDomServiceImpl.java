package cn.edu.my.xml.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;  
import javax.xml.parsers.DocumentBuilderFactory;  
import javax.xml.parsers.ParserConfigurationException;
  
import org.w3c.dom.Document;  
import org.w3c.dom.Element;  
import org.w3c.dom.NodeList;  
import org.w3c.dom.Node;  
import org.xml.sax.SAXException;

import cn.edu.my.xml.dao.TaskDomDao;
import cn.edu.my.xml.dao.impl.TaskDomDaoImpl;
import cn.edu.my.xml.factory.TaskDomDaoFactory;
import cn.edu.my.xml.model.CompletedTask;
import cn.edu.my.xml.model.RoutineTask;
import cn.edu.my.xml.model.Task;
import cn.edu.my.xml.model.UrgentTask;
import cn.edu.my.xml.service.TaskDomService;
import cn.edu.my.xml.test.TestTaskDom;

public class TaskDomServiceImpl implements TaskDomService {
	private TaskDomDao taskDomDao;
	
	private TaskDomServiceImpl(String inputStream) throws ParserConfigurationException, SAXException, IOException{
		taskDomDao=TaskDomDaoFactory.getTaskDomDao(inputStream);
	}
	
	public static TaskDomService newInstance(String inputStream) throws ParserConfigurationException, SAXException, IOException{
		return new TaskDomServiceImpl(inputStream);
	}
	
	public UrgentTask getUrgentTask() {
		return taskDomDao.getUrgentTask();
	}

	public RoutineTask getRoutineTask() {
		return taskDomDao.getRoutineTask();
	}

	public CompletedTask getCompletedTask() {
		return taskDomDao.getCompletedTask();
	}

	public void setUrgentTask(UrgentTask urgentTask) {
		// TODO Auto-generated method stub
		
	}

	public void setRoutineTask(RoutineTask routineTask) {
		// TODO Auto-generated method stub
		
	}

	public void setCompletedTask(CompletedTask completedTask) {
		// TODO Auto-generated method stub
		
	}
}
