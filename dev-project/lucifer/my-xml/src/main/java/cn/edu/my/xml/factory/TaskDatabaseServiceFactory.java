package cn.edu.my.xml.factory;

import cn.edu.my.xml.service.TaskDatabaseService;
import cn.edu.my.xml.service.impl.TaskDatabaseServiceImpl;

public class TaskDatabaseServiceFactory {
	private static TaskDatabaseService taskDatabaseService;
	
	public static TaskDatabaseService getTaskDatabaseService(){
		if(null==taskDatabaseService){
			return TaskDatabaseServiceImpl.newInstance();
		}
		return taskDatabaseService;
	}
}
