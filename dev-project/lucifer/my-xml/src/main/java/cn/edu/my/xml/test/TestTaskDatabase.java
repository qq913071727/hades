package cn.edu.my.xml.test;

import java.util.List;

import cn.edu.my.xml.factory.TaskDatabaseServiceFactory;
import cn.edu.my.xml.model.Task;
import cn.edu.my.xml.model.UrgentTask;
import cn.edu.my.xml.service.TaskDatabaseService;

public class TestTaskDatabase {
	public static void main(String[] args){
		TaskDatabaseService tds=TaskDatabaseServiceFactory.getTaskDatabaseService();
		UrgentTask urgentTask = tds.getUrgentTask();
		List<Task> urgentTasks=urgentTask.getTasks();
		for(int i=0;i<urgentTasks.size();i++){
        	Task task=(Task)urgentTasks.get(i);
        	System.out.println(task.getId());
        	System.out.println(task.getRecordTime());
        	System.out.println(task.getDescription());
        	System.out.println(task.getType());
        	System.out.println(task.getSolved());
        }
	}
}
