package cn.edu.my.xml.dao;

import cn.edu.my.xml.model.CompletedTask;
import cn.edu.my.xml.model.RoutineTask;
import cn.edu.my.xml.model.UrgentTask;

public interface TaskDomDao {
	UrgentTask getUrgentTask();
	RoutineTask getRoutineTask();
	CompletedTask getCompletedTask();
}
