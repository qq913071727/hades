package cn.edu.my.xml.service;

import cn.edu.my.xml.model.CompletedTask;
import cn.edu.my.xml.model.RoutineTask;
import cn.edu.my.xml.model.UrgentTask;

public interface TaskDatabaseService {
	UrgentTask getUrgentTask();
	RoutineTask getRoutineTask();
	CompletedTask getCompletedTask();
	
	void setUrgentTask(UrgentTask urgentTask);
	void setRoutineTask(RoutineTask routineTask);
	void setCompletedTask(CompletedTask completedTask);
}
