package cn.edu.my.xml;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import cn.edu.my.xml.factory.TaskDatabaseServiceFactory;
import cn.edu.my.xml.factory.TaskDomServiceFactory;
import cn.edu.my.xml.model.CompletedTask;
import cn.edu.my.xml.model.RoutineTask;
import cn.edu.my.xml.model.Task;
import cn.edu.my.xml.model.UrgentTask;
import cn.edu.my.xml.service.TaskDatabaseService;
import cn.edu.my.xml.service.TaskDomService;

public class TaskSynchronization {
	TaskDomService dom;
	UrgentTask xmlUrgentTask;
	List<Task> xmlUrgentTasks;
	
	TaskDatabaseService tds;
	UrgentTask databaseUrgentTask;
	List<Task> databaseUrgentTasks;
	
	public void synchronizeUrgentTask(UrgentTask xmlUrgentTask,UrgentTask databaseUrgentTask) {
		try {
			dom = TaskDomServiceFactory.getTaskDomService("my-home-tasks.xml");
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		xmlUrgentTask = dom.getUrgentTask();
		xmlUrgentTasks = xmlUrgentTask.getTasks();
		
		tds=TaskDatabaseServiceFactory.getTaskDatabaseService();
		databaseUrgentTask=tds.getUrgentTask();
		databaseUrgentTasks=databaseUrgentTask.getTasks();
		
		for(int i=0;i<xmlUrgentTasks.size();i++){
			if(xmlUrgentTasks.get(0).getId()==0){
				
			}else{
				//compare with database record
			}
		}
	}

	public void synchronizeRoutineTask(RoutineTask xmlRoutineTask,
			RoutineTask databaseRoutineTask) {

	}

	public void synchronizeCompletedTask(CompletedTask xmlCompletedTask,
			CompletedTask databaseCompletedTask) {

	}
}