package cn.edu.my.xml.dao;

import cn.edu.my.xml.model.CompletedTask;
import cn.edu.my.xml.model.RoutineTask;
import cn.edu.my.xml.model.Task;
import cn.edu.my.xml.model.UrgentTask;

public interface TaskDatabaseDao {
	UrgentTask getUrgentTask();
	RoutineTask getRoutineTask();
	CompletedTask getCompletedTask();
	Task getTask();
	
	void setUrgentTask(UrgentTask urgentTask);
	void setRoutineTask(RoutineTask routineTask);
	void setCompletedTask(CompletedTask completedTask);
	void setTask(Task task);
}
