package org.lucifer.task.ibatis.model;

import java.util.Date;

public class Task {
	private Long id;
	private Date recordTime;
	private Date beginningTime;
	private Date endingTime;
	private String description;
	private String type;
	private Boolean solved;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getRecordTime() {
		return recordTime;
	}
	public void setRecordTime(Date recordTime) {
		this.recordTime = recordTime;
	}
	public Date getBeginningTime() {
		return beginningTime;
	}
	public void setBeginningTime(Date beginningTime) {
		this.beginningTime = beginningTime;
	}
	public Date getEndingTime() {
		return endingTime;
	}
	public void setEndingTime(Date endingTime) {
		this.endingTime = endingTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Boolean getSolved() {
		return solved;
	}
	public void setSolved(Boolean solved) {
		this.solved = solved;
	}

}
