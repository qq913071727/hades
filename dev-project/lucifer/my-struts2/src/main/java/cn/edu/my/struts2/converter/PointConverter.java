package cn.edu.my.struts2.converter;

import java.util.Map;

import cn.edu.my.struts2.bean.Point;

import com.opensymphony.xwork2.conversion.impl.DefaultTypeConverter;

public class PointConverter extends DefaultTypeConverter {

	@Override
	public Object convertValue(Map<String, Object> context, Object value,
			Class toType) {
		if(Point.class==toType){
			Point point=new Point();
			
			String[] str=(String[])value;
			String[] paramValues=str[0].split(",");
			
			int x=Integer.valueOf(paramValues[0]);
			int y=Integer.valueOf(paramValues[1]);
			
			point.setX(x);
			point.setY(y);
			
			return point;
		}
		if(String.class==toType){
			Point point=(Point)value;
			
			int x=point.getX();
			int y=point.getY();
			
			return "[x="+x+",y="+y+"]";
		}
		return null;
	}

}
