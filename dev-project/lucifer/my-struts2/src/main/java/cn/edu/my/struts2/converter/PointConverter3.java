package cn.edu.my.struts2.converter;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import cn.edu.my.struts2.bean.Point;

import org.apache.struts2.util.StrutsTypeConverter;

public class PointConverter3 extends StrutsTypeConverter {

	@Override
	public Object convertFromString(Map arg0, String[] arg1, Class arg2) {
		List<Point> list=new ArrayList<Point>();
		
		for(String value : arg1){
			String[] paramValues=value.split(",");
			
			int x=Integer.parseInt(paramValues[0]);
			int y=Integer.parseInt(paramValues[1]);
			
			Point point=new Point();
			point.setX(x);
			point.setY(y);
			
			list.add(point);
		}
		
		return list;
	}

	@Override
	public String convertToString(Map arg0, Object arg1) {
		List<Point> list=(List<Point>)arg1;
		
		StringBuilder sb=new StringBuilder();
		
		int number=0;
		
		sb.append("[");
		
		for (Point point : list){
			++number;
			
			int x=point.getX();
			int y=point.getY();
			
			sb.append("number="+number).append(" x = ").append(x).append(" y = ").append(y);
		}
		sb.append("]");
		
		return sb.toString();
	}

}
