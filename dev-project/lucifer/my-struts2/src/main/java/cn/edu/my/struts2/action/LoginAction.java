package cn.edu.my.struts2.action;

import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport {
	private String username;
	private String password;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String execute() throws Exception{
		debug("222222222222222222222222222222222222222222222");
		if("hello".equals(this.username.trim())&&"world".equals(this.password.trim())){
			return SUCCESS;
		}else{
			this.addFieldError("username", "username or password error");
			return "failure";
		}
	}
	@Override
	public void validate(){
		if(null==this.username||"".equals(this.username.trim())){
			this.addFieldError("username", "username required");
		}
		if(null==this.password||"".equals(this.password.trim())){
			this.addFieldError("password", "password required");
		}
	}
	
	private void debug(String message){
		System.out.println(message);
	}
}
