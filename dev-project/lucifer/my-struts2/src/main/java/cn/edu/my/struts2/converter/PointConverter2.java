package cn.edu.my.struts2.converter;

import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

import cn.edu.my.struts2.bean.Point;

public class PointConverter2 extends StrutsTypeConverter {

	@Override
	public Object convertFromString(Map arg0, String[] arg1, Class arg2) {
		Point point=new Point();
		
		String[] paramValues=arg1[0].split(",");
		
		int x=Integer.parseInt(paramValues[0]);
		int y=Integer.parseInt(paramValues[1]);
		
		point.setX(x);
		point.setY(y);
		
		return point;
	}

	@Override
	public String convertToString(Map arg0, Object arg1) {
		Point point=(Point)arg1;
		
		int x=point.getX();
		int y=point.getY();
		
		String result="[ x = " + x + " , y = " + y + " ]";
		
		return result;
	}

}
