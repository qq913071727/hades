<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<s:form action="register">
		<s:textfield name="username" label="username"></s:textfield>
		<s:password name="password" label="password"></s:password>
		<s:password name="repassword" label="repassword"></s:password>
		<s:textfield name="age" label="age"></s:textfield>
		<s:textfield name="birthday" label="birthday"></s:textfield>
		<s:textfield name="graduation" label="graduation"></s:textfield>
		<s:submit label="submit"></s:submit>
	</s:form>
</body>
</html>