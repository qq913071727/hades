<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>

	<s:form action="pointConverter3">
		<s:textfield name="point.x" label="x"></s:textfield>
		<s:textfield name="point.y" label="y"></s:textfield>
		<s:textfield name="age" label="age"></s:textfield>
		<s:textfield name="username" label="username"></s:textfield>
		<s:textfield name="date" label="birthday"></s:textfield>
		<s:submit name="submit"></s:submit>
	</s:form>

</body>
</html>