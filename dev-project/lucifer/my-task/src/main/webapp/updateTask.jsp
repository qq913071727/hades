<%@ page language="java" contentType="text/html; charset=utf-8" isELIgnored="false"
    pageEncoding="utf-8"%>
    
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<h1>Task Management System</h1>
		<a href="index.jsp">home</a>
		<a href="addTask.jsp">add task</a>
		<a href="deleteTask.jsp">delete task</a>
		<a href="updateTask.jsp">update task</a>
		<a href="searchTask.jsp">search task</a>
		
		<form action="updateTask" method="post">
			key word<input type="text" name="keyWord" /> 
			<input type="submit" name="searchSubmit" value="searchSubmit" /><br />
			<c:if test="${!empty requestScope.task}">
				<table border="1">
					<tr>
						<td></td>
						<td>recording time</td>
						<td>beginning time</td>
						<td>ending time</td>
						<td>description</td>
						<td>type</td>
						<td>sovled</td>
					</tr>
					<c:forEach var="i" begin="0" end="${fn:length(requestScope.task)-1}">
						<tr>
							<td>
								<input type="hidden" name="id" value="${requestScope.task[i]['ID']}" />
							</td>
							<td>
								<input type="text" name="recordingTime" value="${requestScope.task[i]['RECORD_TIME']}" />
							</td>
							<td>
								<input type="text" name="beginningTime" value="${requestScope.task[i]['BEGINNING_TIME']}" />
							</td>
							<td>
								<input type="text" name="endingTime" value="${requestScope.task[i]['ENDING_TIME']}" />
							</td>
							<td>
								<textarea rows="5" cols="50" name="descripion">${requestScope.task[i]['DESCRIPTION']}</textarea>
							</td>
							<td>
								<input type="text" name="type" value="${requestScope.task[i]['TYPE']}" />
							</td>
							<td>
								<input type="text" name="solved" value="${requestScope.task[i]['SOLVED']}" />
							</td>
							<td>
								<input type="submit" name="updateSubmit" value="updateSubmit" />
							</td>
						</tr>
					</c:forEach>
				</table>
				<br />
				</c:if>
		</form>
	</center>
</body>
</html>