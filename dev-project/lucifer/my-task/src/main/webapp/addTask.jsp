<%@ page language="java" pageEncoding="utf-8"%>
<%@ page import="java.util.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script language="javascript" src="js/addTask.js"></script>
</head>
<body>
	<center>
		<h1>Task Management System</h1>
		<a href="index.jsp">home</a>
		<a href="addTask.jsp">add task</a>
		<a href="deleteTask.jsp">delete task</a>
		<a href="updateTask.jsp">update task</a>
		<a href="searchTask.jsp">search task</a>
		
		<form action="addTask" method="post">
			<table>
				<tr>
					<td>type</td>
					<td><select onchange="change();" name="type">
							<option id="urgentTask" value="urgentTask" selected="selected">urgent task</option>
							<option id="routineTask" value="routineTask">routine task</option>
							<option id="completedTask" value="completedTask">completed task</option>
						</select>
					</td>
				</tr>
				<tr id="beginningTimeLine">
					<td>beginning time</td>
					<td><input type="text" name="beginningTime" /></td>
				</tr>
				<tr id="endingTimeLine">
					<td>ending time</td>
					<td><input type="text" name="endingTime" /></td>
				</tr>
				<tr>
					<td>description</td>
					<td><textarea name="description" rows="10" cols="100"></textarea></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="radio" name="solved" value="solved" checked />solved
						<input type="radio" name="solved" value="unsolved" />unsolved
					</td>
				</tr>
			</table>
			<input type="submit" name="submit" value="submit">
		</form>
	</center>
</body>
</html>