<%@ page language="java" contentType="text/html; charset=utf-8" isELIgnored="false"
    pageEncoding="utf-8"%>
    
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<h1>Task Management System</h1>
		<a href="index.jsp">home</a>
		<a href="addTask.jsp">add task</a>
		<a href="deleteTask.jsp">delete task</a>
		<a href="updateTask.jsp">update task</a>
		<a href="searchTask.jsp">search task</a>
		
		<form action="searchTask" method="post">
			key word<input type="text" name="keyWord" /> 
			<input type="submit" name="searchSubmit" value="searchSubmit" /><br />
			<c:if test="${!empty requestScope.task}">
				<table border="1">
					<tr>
						<td>recording time</td>
						<td>beginning time</td>
						<td>ending time</td>
						<td>description</td>
						<td>type</td>
						<td>sovled</td>
					</tr>
					<c:forEach var="i" begin="0" end="${fn:length(requestScope.task)-1}">
						<tr>
							<td>
								<c:out value="${requestScope.task[i]['RECORD_TIME']}" />
							</td>
							<td>
								<c:out value="${requestScope.task[i]['BEGINNING_TIME']}" />
							</td>
							<td>
								<c:out value="${requestScope.task[i]['ENDING_TIME']}" />
							</td>
							<td>
								<c:out value="${requestScope.task[i]['DESCRIPTION']}" />
							</td>
							<td>
								<c:out value="${requestScope.task[i]['TYPE']}" /></td>
							<td>
								<c:out value="${requestScope.task[i]['SOLVED']}" />
							</td>
						</tr>
					</c:forEach>
				</table>
			</c:if>
		</form>
	</center>
</body>
</html>