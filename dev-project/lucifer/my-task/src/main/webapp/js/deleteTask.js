/*
 * When Checkbox whose id is deleteAll is selected, all of other Checkboxes
 * named deleteItem will also be selected. Otherwise, they are not selected
 */
function controlCheckbox() {
	var deleteAll = document.getElementById("deleteAll");
	var deleteItem = document.getElementsByName("deleteItem");
	for (i = 0; i < deleteItem.length; i++) {
		if (deleteAll.checked == true) {
			deleteItem[i].checked = true;
		} else {
			deleteItem[i].checked = false;
		}
	}
}
