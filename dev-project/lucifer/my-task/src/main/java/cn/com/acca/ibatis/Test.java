package cn.com.acca.ibatis;

import cn.com.acca.ibatis.dao.impl.TaskDaoIbatis;

import com.ibatis.sqlmap.client.SqlMapClient;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) throws SQLException {
		test();
	}

	private static void test() {
		/*
		 * try { SqlMapClient sqlMapper = DatabaseManager.getClient(); List list
		 * = sqlMapper.queryForList("Task.getTask",new Integer(1));
		 * debug(list.toString()); } catch (Exception e) { throw new
		 * RuntimeException
		 * ("Something bad happened while building the SqlMapClient instance." +
		 * e, e); }
		 */

		ApplicationContext context = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		TaskDaoIbatis tdi = (TaskDaoIbatis) context.getBean("taskDaoIbatis");
		System.out.println("@@@@@@@@@@@@@"+tdi.getTasks());
	}

	/**
	 * output message on console
	 * 
	 * @param msg
	 */
	public static void debug(String msg) {
		System.out.println(msg);
	}
}