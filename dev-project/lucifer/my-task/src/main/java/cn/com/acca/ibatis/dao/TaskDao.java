package cn.com.acca.ibatis.dao;

import java.util.HashMap;
import java.util.List;

import cn.com.acca.ibatis.model.Task;

public interface TaskDao {
	void save(Task v);

    void delete(Integer id);

    Task getTask(Integer id);

    List<Task> getTasks();
    
    List<HashMap> getTasksByKeyWord(String keyWord);
    
    void update(Task task);
}
