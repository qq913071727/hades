package cn.com.acca.ibatis.dao.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import cn.com.acca.ibatis.dao.TaskDao;
import cn.com.acca.ibatis.model.Task;

public class TaskDaoIbatis extends SqlMapClientDaoSupport implements TaskDao,
		BeanPostProcessor {

	public Object postProcessAfterInitialization(Object arg0, String arg1)
			throws BeansException {
		return null;
	}

	public Object postProcessBeforeInitialization(Object arg0, String arg1)
			throws BeansException {
		return null;
	}

	public void save(Task v) {
		this.getSqlMapClientTemplate().insert("insert", v);
	}

	public void delete(Integer id) {		
		this.getSqlMapClientTemplate().delete("delete", id);
	}

	public Task getTask(Integer id) {
		Task task = (Task) this.getSqlMapClientTemplate().queryForObject(
				"getTask",id);
		return task;
	}

	public List<Task> getTasks() {
		return getSqlMapClientTemplate().queryForList("getTasks");
	}

	public List<HashMap> getTasksByKeyWord(String keyWord) {
		return getSqlMapClientTemplate().queryForList("getTasksByKeyWord",keyWord);
	}

	public void update(Task task) {
		getSqlMapClientTemplate().update("update",task);
	}

}