package cn.com.acca.ibatis.service.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import cn.com.acca.ibatis.dao.TaskDao;
import cn.com.acca.ibatis.model.Task;
import cn.com.acca.ibatis.service.TaskService;

public class TaskServiceImpl implements TaskService,BeanPostProcessor  {
	
	private TaskDao taskDao;
	
	public TaskDao getTaskDao() {
		return taskDao;
	}

	public void setTaskDao(TaskDao taskDao) {
		this.taskDao = taskDao;
	}

	public Task getTask(Integer id) {
		return taskDao.getTask(id);
	}

	public Object postProcessAfterInitialization(Object arg0, String arg1)
			throws BeansException {
		return null;
	}

	public Object postProcessBeforeInitialization(Object arg0, String arg1)
			throws BeansException {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(Task t) {
		taskDao.save(t);
	}

	public List<Task> getTasks() {
		
		return taskDao.getTasks();
	}

	public List<HashMap> getTasksByKeyWord(String keyWord) {
		return taskDao.getTasksByKeyWord(keyWord);
	}

	public void delete(Integer id) {
		taskDao.delete(id);
	}

	public void update(Task task) {
		taskDao.update(task);
	}

}
