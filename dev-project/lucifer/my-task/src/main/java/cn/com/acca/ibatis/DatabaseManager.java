package cn.com.acca.ibatis;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import java.io.Reader;

public class DatabaseManager {
    private static SqlMapClient sqlMapClient = null;

    @SuppressWarnings("empty-statement")
    public static SqlMapClient getClient() {
        if (sqlMapClient == null) {
            Reader reader = null;
            try {
                reader = Resources.getResourceAsReader("cn/com/acca/ibatis/SqlMapConfig.xml");
                sqlMapClient = SqlMapClientBuilder.buildSqlMapClient(reader);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    reader.close();
                } catch (Exception e) {};
            }
        }

        return sqlMapClient;
    }
    
    /**
     * output message on console
     * @param msg
     */
    public static void debug(String msg) {
        System.out.println(msg);
    }
}
