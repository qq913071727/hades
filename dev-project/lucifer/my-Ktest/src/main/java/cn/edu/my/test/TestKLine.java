package cn.edu.my.test;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.Container;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;

import java.util.Date;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.border.BevelBorder;
import javax.swing.event.MouseInputAdapter;
import javax.swing.event.MouseInputListener;

import cn.edu.my.component.KLine;
import cn.edu.my.component.KLineChart;
import cn.edu.my.component.MainPanel;
import cn.edu.my.component.LeftPrice;
import cn.edu.my.component.BottomTime;

import org.apache.log4j.Logger;

public class TestKLine{
	static Logger logger = Logger.getLogger(TestKLine.class.getName());
	
	public static void main(String[] args){
		JFrame frame = new JFrame();
		frame.setTitle("K Line Test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);//达到全屏的效果
		
		MainPanel mainPanel=new MainPanel(new BorderLayout());
		KLineChart kLineChart=new KLineChart();
		LeftPrice leftPrice=new LeftPrice();
		BottomTime bottomTime=new BottomTime();
		
		mainPanel.add(kLineChart,BorderLayout.CENTER);
		mainPanel.add(leftPrice,BorderLayout.WEST);
		mainPanel.add(bottomTime,BorderLayout.SOUTH);
		
		logger.info("frame: "+frame.getWidth());
		
		frame.setContentPane(mainPanel);
		
		frame.setVisible(true);
		
		
		/***************************************************GridLayout**************************************************/
		/*JFrame frame = new JFrame();
		frame.setSize(300,300);//frame的大小
		frame.setLocation(400,200);//frame在显示器上的位置，相对于显示器的左上角
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//frame一关闭就退出应用程序
		JButton jbs[] = new JButton[9];
		for(int i = 0; i<9; i++){ 
			jbs[i] = new JButton (String.valueOf(i)); 
		} 
		//设置网格布局(3,3)表示3行3列;(10,10)表示单元格之间的水平间隔和垂直间隔
		frame.setLayout(new GridLayout(3,3,10,10)); 
		//添加组件
		for(int i = 0; i < 9; i++){ 
			frame.add(jbs[i]); 
		} 
		frame.setVisible(true);*/
		
		/***************************************************FlowLayout**************************************************/
		/*JFrame frame = new JFrame();
		frame.setSize(300,300);//frame的大小
		frame.setLocation(400,200);//frame在显示器上的位置，相对于显示器的左上角
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//frame一关闭就退出应用程序
		JButton jb1,jb2,jb3,jb4,jb5,jb6;
		jb1=new JButton("关羽");
		jb2=new JButton("张飞");
		jb3=new JButton("赵云");
		jb4=new JButton("马超");
		jb5=new JButton("黄忠");
		jb6=new JButton("魏延");
		//添加组件
		frame.add(jb1);
		frame.add(jb2);
		frame.add(jb3);
		frame.add(jb4);
		frame.add(jb5);
		frame.add(jb6);
		//设置布局管理器
		//frame.setLayout(new FlowLayout(FlowLayout.LEFT));
		//frame.setLayout(new FlowLayout(FlowLayout.TRAILING));
		//frame.setLayout(new FlowLayout(FlowLayout.RIGHT));
		frame.setLayout(new FlowLayout(FlowLayout.CENTER));
		//frame.setLayout(new FlowLayout(FlowLayout.LEADING));
		//设置窗体属性
		frame.setTitle("边界布局");
		frame.setVisible(true);//默认为false，即不显示*/
		
		/**************************************************BorderLayout*************************************************/
		/*JFrame frame = new JFrame();
		frame.setSize(200,200);//frame的大小
		frame.setLocation(400,200);//frame在显示器上的位置，相对于显示器的左上角
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//frame一关闭就退出应用程序
		JButton jb1,jb2,jb3,jb4,jb5;
		jb1 = new JButton("中");
		jb2 = new JButton("北");
		jb3 = new JButton("东");
		jb4 = new JButton("南");
		jb5 = new JButton("西");
		frame.add(jb1, BorderLayout.CENTER);
		frame.add(jb2, BorderLayout.NORTH);
		frame.add(jb3, BorderLayout.EAST);
		frame.add(jb4, BorderLayout.SOUTH);
		frame.add(jb5, BorderLayout.WEST);
		frame.setVisible(true);//默认为false，即不显示*/
		
		/*************************************************默认不添加Layout************************************************/
		/*JFrame frame = new JFrame();
		JButton jButton=new JButton("button");
		frame.setSize(200,200);//frame的大小
		frame.setLocation(400,200);//frame在显示器上的位置，相对于显示器的左上角
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//frame一关闭就退出应用程序
		frame.add(jButton);//不设置panel，直接将button加入到frame中
		frame.setVisible(true);//默认为false，即不显示*/
	}
}