package cn.edu.my.test;

import java.io.*;
import java.util.Properties;
import java.net.Authenticator;  
import java.net.InetSocketAddress;  
import java.net.PasswordAuthentication;  
import java.net.Proxy;  
import java.net.URL; 
import java.net.URL;
import java.net.URLConnection;

public class TestURL {
	public static void main(String[] args) throws IOException {
		Properties prop = System.getProperties();
		prop.put("http.proxyHost","10.1.31.133");
		prop.put("http.proxyPort","8080"); 
		
    	test4();
        /*test3();
        test2();
        test();*/
    }
    
    public static void test4() throws IOException {
		URL url = new URL("http://ichart.finance.yahoo.com/table.csv?s=300072.sz&d=7&e=23&f=2010&a=5&b=11&c=2010");
		//URL url = new URL("http://www.baidu.com");
		
		URLConnection conn = url.openConnection(); 
		InputStream is = url.openStream();
		
		byte buffer[] = new byte[1024];
		int i;
		while ((i = is.read(buffer)) != -1) {
			System.out.write(buffer, 0, i);
		}
		
		is.close();
    }
    
    public static void test3() throws IOException {
                URL url = new URL("http://ichart.finance.yahoo.com/table.csv?s=300072.sz&d=7&e=23&f=2010&a=5&b=11&c=2010");
                
                URLConnection uc = url.openConnection();
                
                InputStream in = uc.getInputStream();
                int c;
                while ((c = in.read()) != -1)
                        System.out.print(c);
                in.close();
    }
    
    public static void test2() throws IOException {
                URL url = new URL("http://ichart.finance.yahoo.com/table.csv?s=300072.sz&d=7&e=23&f=2010&a=5&b=11&c=2010");
                
                Reader reader = new InputStreamReader(new BufferedInputStream(url.openStream()));
                int c;
                while ((c = reader.read()) != -1) {
                        System.out.print((char) c);
                }
                reader.close();
    }
    
    public static void test() throws IOException {
                URL url = new URL("http://ichart.finance.yahoo.com/table.csv?s=300072.sz&d=7&e=23&f=2010&a=5&b=11&c=2010");
                
                InputStream in = url.openStream();
                int c;
                while ((c = in.read()) != -1)
                        System.out.print(c);
                in.close();
        }  
} 