package cn.edu.my.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Date;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

import cn.edu.my.component.KLine;

public class CSVService{
	private static CSVService csvService;
	private List<KLine> csvList;//用来保存csv文件的所有数据
	private KLine kLine;//用来存储csv文件的每行记录
	
	private CSVService(){
	}
	
	public static CSVService getInstance(){
		if(null==csvService){
			csvService=new CSVService();	
		}
		return new CSVService();
	}
	
	/**
	* 读取CSV文件
	*/
	public List<KLine> readCsv(){
		csvList=new ArrayList<KLine>();
		
		try {
			Properties prop = new Properties();
			InputStream in = Object.class.getResourceAsStream("/csv-filename.properties");
           	prop.load(in);
            String csvFilePath = prop.getProperty("AUDUSD.2012year.1month").trim();        	
	        CsvReader reader = new CsvReader(csvFilePath,',',Charset.forName("SJIS"));    //一般用这编码读就可以了    
	        reader.readHeaders(); // 跳过表头,如果需要表头的话,不要写这句。
	        
	        while(reader.readRecord()){ //逐行读入除表头的数据
	        	kLine=new KLine();
	        	String date=reader.getValues()[0].split(" ")[0];
	        	String time=reader.getValues()[0].split(" ")[1];
	        	
	        	int year=Integer.parseInt(date.split("\\.")[2]);
	        	int month=Integer.parseInt(date.split("\\.")[1]);
	        	int day=Integer.parseInt(date.split("\\.")[0]);
	        	int hour=Integer.parseInt(time.split(":")[0]);
	        	int minute=Integer.parseInt(time.split(":")[1]);
	        	int second=Integer.parseInt(time.split(":")[2].split("\\.")[0]);
	        	
	        	//Time,Open,High,Low,Close,Volume
	        	kLine.setTime(new Date(year,month,day,hour,minute,second));
	        	kLine.setOpen(Double.parseDouble(reader.getValues()[1]));
	        	kLine.setHigh(Double.parseDouble(reader.getValues()[2]));
	        	kLine.setLow(Double.parseDouble(reader.getValues()[3]));
	        	kLine.setClose(Double.parseDouble(reader.getValues()[4]));
	        	kLine.setVolume(Long.parseLong(reader.getValues()[5].split("\\.")[0]));
	            csvList.add(kLine);
	        }            
	        reader.close();
	        
	        //用于打印出csv文件的内容
	        /*for(int row=0;row<csvList.size();row++){
	        	kLine = csvList.get(row); //取得第row行第col列的数据
	            System.out.println(kLine.getTime());
	            System.out.println(kLine.getOpen());
	            System.out.println(kLine.getHigh());
	            System.out.println(kLine.getLow());
	            System.out.println(kLine.getClose());
	            System.out.println(kLine.getVolume());
	        }
	        System.out.println("-----------------------------------------------------------------");*/
	    }catch (IOException e) {
           	e.printStackTrace();
	    }catch(Exception ex){
	       System.out.println(ex);
	    }
	    
	    return csvList;
	}
           
	/**
	* 写入CSV文件
	*/
	public void writeCsv(){
	   try {
			String csvFilePath = "d:/test.csv";
	        CsvWriter wr =new CsvWriter(csvFilePath,',',Charset.forName("SJIS"));
	        String[] contents = {"aaaaa","bbbbb","cccccc","ddddddddd"};                    
	        wr.writeRecord(contents);
	        wr.close();
	    } catch (IOException e) {
	       e.printStackTrace();
	    }
	}
}
