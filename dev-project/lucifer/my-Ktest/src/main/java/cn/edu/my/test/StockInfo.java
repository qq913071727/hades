package cn.edu.my.test;

public class StockInfo{
	private int num;
	private String name;
	
	public int getNum(){
		return this.num;	
	}
	
	public void setNum(int num){
		this.num=num;	
	}
	
	public String getName(){
		return this.name;	
	}
	
	public void setName(String name){
		this.name=name;	
	}
}
