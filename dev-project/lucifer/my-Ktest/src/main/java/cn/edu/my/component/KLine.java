package cn.edu.my.component;

import java.util.Date;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Point;

import org.apache.log4j.Logger;

public class KLine extends Canvas {
	static Logger logger = Logger.getLogger(KLineChart.class.getName());
	
	private Date time;
	
	/*交易信息*/
	private double open;
	private double high;
	private double low;
	private double close;
	private long volume;
	
	/*K线信息*/
	private int wide;
	private double convertPercentage;//面板高度与K线价格之间换算的百分比
	private double allHeight;//包括影线高度
	private double entityHeight;//只包括实体高度
	private KLineStatus kLineStatus;//UP:阳线.DOWN:阴线. MIDDLE:十字星
	private Point entityLocation;//K线实体部分的x坐标和y坐标
	private Point allLocation;//K线影线部分的x坐标和y坐标
	
	public KLine(){
	}
	
	public KLine(Date time,double open,double high,double low,double close,long volume){
		this.time=time;
		this.open=open;
		this.high=high;
		this.low=low;
		this.close=close;
		this.volume=volume;
	}
	
	/**
	* 将交易信息转换为K线信息
	*/
	public void convert(int panelWidth,int panelHeight,int kLineCount,double maxPrice,double minPrice){
		this.wide=(panelWidth-kLineCount)/kLineCount+1;//加1是因为要考虑到K线有上影线和下影线
		this.convertPercentage=panelHeight/(maxPrice-minPrice);
		logger.info("wide: "+wide);
		logger.info("convertPercentage: "+convertPercentage);
		
		if(this.close>this.open){//UP:阳线
			this.kLineStatus=KLineStatus.UP;
			this.entityHeight=this.close-this.open;
			this.setBackground(Color.RED);
		}else if(this.close==this.open){//MIDDLE:十字星
			this.kLineStatus=KLineStatus.MIDDLE;
			this.entityHeight=this.close=this.open;
			this.setBackground(Color.GRAY);
		}else if(this.close<this.open){//DOWN:阴线
			this.kLineStatus=KLineStatus.DOWN;
			this.entityHeight=this.open-this.close;
			this.setBackground(Color.GREEN);
		}
		
		//System.out.println("JPanel:"+panelWidth);
		this.allHeight=this.high-this.low;
		this.setSize(this.wide,(int)this.entityHeight);
		
		//this.entityLocation=new Point(x,y);//K线的x坐标和y坐标是以实体部分的左上角为基础的
		//this.allLocation=new Point(x+((this.wide-1)/2+1),);//影线的x坐标和y坐标是以影线的顶点为标准的
	}
	
	public void paint(Graphics g){
		super.paint(g);
		
		//g.setColor(Color.BLUE);
		//g.drawRect((int)this.getLocation().getX(),(int)this.getLocation().getY(),this.wide,(int)this.entityHeight);
		//System.out.println("4444444444444444444444444444444444444444444444444");
	}
	
	public Date getTime(){
		return this.time;	
	}
	public void setTime(Date time){
		this.time=time;
	}
	
	public double getClose(){
		return this.close;	
	}
	public void setClose(double close){
		this.close=close;
	}
	
	public double getOpen(){
		return this.open;	
	}
	public void setOpen(double open){
		this.open=open;
	}
	
	public double getHigh(){
		return this.high;	
	}
	public void setHigh(double high){
		this.high=high;
	}
	
	public double getLow(){
		return this.low;	
	}
	public void setLow(double low){
		this.low=low;
	}
	
	public long getVolume(){
		return this.volume;	
	}
	public void setVolume(long volume){
		this.volume=volume;
	}
	
	public double getWide(){
		return this.wide;	
	}
	public void setWide(int wide){
		this.wide=wide;
	}
	
	public double getEntityHeight(){
		return this.entityHeight;	
	}
	
	public void setEntityLocation(int x,int y){
		this.entityLocation=new Point(x,y);
	}
	
	public Point getEntityLocation(){
		return this.entityLocation;
	}
	
	public KLineStatus getKLineStatus(){
		return this.kLineStatus;
	}
}