package cn.edu.my.component;

import java.awt.Color;

import java.util.List;

import javax.swing.JPanel;
import java.awt.Graphics;

import cn.edu.my.component.KLine;
import cn.edu.my.service.CSVService;

import org.apache.log4j.Logger;

public class LeftPrice extends JPanel{
	static Logger logger = Logger.getLogger(LeftPrice.class.getName());
	
	private List<KLine> kLineList;//表示一个csv文件中的所有数据，每一行是一个KLine对象
	private double maxPrice;//K线图表中的最高价格，可以变化
	private double minPrice;//K线图表中的最低价格，可以变化
	
	public LeftPrice(){
		this.setBackground(Color.BLUE);
		kLineList=CSVService.getInstance().readCsv();
		this.setMaxPrice(kLineList);
		this.setMinPrice(kLineList);
		//logger.info("maxPrice: "+maxPrice);
		//logger.info("minPrice: "+minPrice);
	}
	
	public void paint(Graphics g) {
		super.paint(g);
	}
	
	/**
	* 计算K线图表中的最高价格
	*/
	private double setMaxPrice(List<KLine> kLineList){
		maxPrice=0;
		for(int row=0; row<kLineList.size(); row++){
			KLine kLine=kLineList.get(row);
			if(maxPrice<kLine.getHigh()){
				maxPrice=kLine.getHigh();
			}
		}
		return maxPrice;
	}
	
	/**
	* 计算K线图表中的最低价格
	*/
	private double setMinPrice(List<KLine> kLineList){
		minPrice=0;
		for(int row=0; row<kLineList.size(); row++){
			KLine kLine=kLineList.get(row);
			if(minPrice>kLine.getLow()){
				minPrice=kLine.getLow();
				logger.info("low price: "+minPrice);
			}
		}
		return minPrice;
	}
}
