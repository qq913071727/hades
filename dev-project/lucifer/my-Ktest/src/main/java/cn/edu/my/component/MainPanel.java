package cn.edu.my.component;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.LayoutManager;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;
import javax.swing.event.MouseInputListener;

import cn.edu.my.service.CSVService;
import cn.edu.my.component.KLine;
import cn.edu.my.component.KLineChart;

import org.apache.log4j.Logger;

public class MainPanel extends JPanel {

	ArrayList<Rectangle> list;
	Rectangle current;
	
	static Logger logger = Logger.getLogger(MainPanel.class.getName());
	/*private KLineChart kLineChart;*/
	private int panelWidth;//主面板的宽度，可以变化
	private int panelHeight;//主面板的高度，可以变化

	public MainPanel() {		
		list = new ArrayList<Rectangle>();
		addMouseMotionListener(mouseHandler);
		addMouseListener(mouseHandler);
		
		this.init();
	}
	
	public MainPanel(LayoutManager layout) {
		super(layout);
		
		this.init();
	}
	
	private void init(){
		this.setBackground(Color.WHITE);
		/*kLineChart=new KLineChart();*/
		panelWidth=(int)this.getSize().getWidth();
		panelHeight=(int)this.getSize().getHeight();
		logger.info("width: "+panelWidth);
		logger.info("height: "+panelHeight);
	}

	MouseInputListener mouseHandler = new MouseInputAdapter() {
		Point startPoint;
		
		public void mousePressed(MouseEvent e) {
			startPoint = e.getPoint();
			current = new Rectangle();
		}
	
		public void mouseReleased(MouseEvent e) {
			makeRectangle(startPoint, e.getPoint());
			if (current.width > 0 && current.height > 0) {
				list.add(current);
				current = null;
				repaint();
			}
		}
	
		public void mouseDragged(MouseEvent e) {
			if (current != null) {
				makeRectangle(startPoint, e.getPoint());
				repaint();
			}
		}
	
		private void makeRectangle(Point p1, Point p2) {
			int x = Math.min(p1.x, p2.x);
			int y = Math.min(p1.y, p2.y);
			int w = Math.abs(p1.x - p2.x);
			int h = Math.abs(p1.y - p2.y);
			current.setBounds(x, y, w, h);
		}
	};

	public void paint(Graphics g) {
		super.paint(g);
		
		/*for(int i=0; i<kLineChart.getKLineList().size(); i++){
			//kLineChart.getKLineList().get(i).convert(panelWidth,panelHeight,kLineChart.getKLineList().size(),kLineChart.getMaxPrice(),kLineChart.getMinPrice());
			//g.setColor(kLineChart.getKLineList().get(i).getBackground());
			//g.fillRect((int)kLine.getEntityLocation().getX(),(int)kLine.getEntityLocation().getY(),(int)kLine.getWide(),(int)kLine.getEntityHeight());
			//g.drawLine();
		}*/
		
		/*KLine kLine=kLineList.get(0);
		kLine.convert(panelWidth,panelHeight,160,280);*/
		
		/*System.out.println(kLine.getLocation().getX());
		System.out.println(kLine.getLocation().getY());
		System.out.println(kLine.getWidth());
		System.out.println((int)kLine.getEntityHeight());*/
		
		//g.setColor(Color.BLACK);
		//g.fillRect(70,40,76,20);
		/*KLine kLine=new KLine(new Date(),100,120,90,110,500);
		//kLine.setBackground(Color.BLUE);
		//kLine.setSize(200,300);
		kLine.setLocation(60,80);
		this.add(kLine);*/
		
		/*TestCSV tCSV=new TestCSV();
		ArrayList<String[]> csvList=tCSV.readeCsv();*/
		
		/*for (Rectangle rect : list) {
			g.fillRect(rect.x, rect.y, rect.width, rect.height);
		}

		if (current != null) {
			g.drawRect(current.x, current.y, current.width, current.height);
		}*/
		
		//System.out.println("22222222222222222222222222222222222");
	}
}