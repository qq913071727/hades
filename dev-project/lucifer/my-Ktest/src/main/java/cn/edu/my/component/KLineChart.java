package cn.edu.my.component;

import java.util.List;

import cn.edu.my.component.KLine;
import cn.edu.my.service.CSVService;

import org.apache.log4j.Logger;

import javax.swing.JPanel;
import java.awt.Graphics;

public class KLineChart extends JPanel{
	static Logger logger = Logger.getLogger(KLineChart.class.getName());
	
	private List<KLine> kLineList;//表示一个csv文件中的所有数据，每一行是一个KLine对象
	private KLine kLine;//csv文件中的每一行用一个KLine对象表示
	private double maxPrice;//K线图表中的最高价格，可以变化
	private double minPrice;//K线图表中的最低价格，可以变化
	private int panelWidth;//K线图表的宽度，可以变化
	private int panelHeight;//K线图表的高度，可以变化
	
	public KLineChart(){		
		kLineList=CSVService.getInstance().readCsv();
		this.setMaxPrice(kLineList);
		this.setMinPrice(kLineList);
		panelWidth=(int)getBounds().width;
		panelHeight=(int)getBounds().height;
		
		logger.info("maxPrice: "+maxPrice);
		logger.info("minPrice: "+minPrice);
		logger.info("width: "+panelWidth);
		logger.info("height: "+panelHeight);
	}
	
	public void paint(Graphics g) {
		super.paint(g);
	}
	
	/**
	* 计算K线图表中的最高价格
	*/
	private double setMaxPrice(List<KLine> kLineList){
		maxPrice=kLineList.get(0).getHigh();
		for(int row=0; row<kLineList.size(); row++){
			KLine kLine=kLineList.get(row);
			if(maxPrice<kLine.getHigh()){
				maxPrice=kLine.getHigh();
			}
		}
		return maxPrice;
	}
	
	/**
	* 计算K线图表中的最低价格
	*/
	private double setMinPrice(List<KLine> kLineList){
		minPrice=kLineList.get(0).getLow();
		for(int row=0; row<kLineList.size(); row++){
			KLine kLine=kLineList.get(row);
			if(minPrice>kLine.getLow()){
				minPrice=kLine.getLow();
			}
		}
		return minPrice;
	}

	public double getMaxPrice(){
		return this.maxPrice;
	}
	
	public double getMinPrice(){
		return this.minPrice;
	}
	
	public List<KLine> getKLineList(){
		return this.kLineList;	
	}
	
	public void setKLineList(List<KLine> kLineList){
		this.kLineList=kLineList;	
	}
}