package cn.edu.my.test;

import java.io.File;
import java.util.List;
import java.util.ArrayList;

public class SameNameComparator{
	public static void main(String[] args){		
		String basePath=new String("E:\\seurat\\trunk\\seurat-crystal-report");
		File filePath=new File(basePath);
		String[] allMemberList=filePath.list();
		List<RptFile> fileList=new ArrayList<RptFile>();
		
		fileList.clear();	
		
		for(int i=0; i<allMemberList.length; i++){
			if(allMemberList[i].endsWith(".rpt")){// is file
				if(fileList.isEmpty()){
					fileList.add(new RptFile(allMemberList[i],basePath,false));
				}else{
					boolean flag=false;
					for(int j=0; j<fileList.size(); j++){
						if(fileList.get(j).getFileName().equals(allMemberList[i])){
							flag=true;
							fileList.get(j).setSameName(true);
							System.out.println("same rpt file is "+basePath+"\\"+allMemberList[i]);
						}
					}
					if(flag==false){
						fileList.add(new RptFile(allMemberList[i],basePath,false));
					}
				}
			}else{ // is directory
				File directory=new File(basePath+"\\"+allMemberList[i]);
				String[] files=directory.list();
				for(int j=0; j<files.length; j++){
					if(fileList.isEmpty()){
						fileList.add(new RptFile(files[j],basePath+"\\"+allMemberList[i],false));
					}else{
						boolean flag=false;
						for(int x=0; x<fileList.size(); x++){
							if(fileList.get(x).getFileName().equals(files[j])){
								flag=true;
								fileList.get(x).setSameName(true);
								System.out.println("same rpt file is "+basePath+"\\"+allMemberList[i]+"\\"+files[j]);
							}
						}
						if(flag==false){
							fileList.add(new RptFile(files[j],basePath+"\\"+allMemberList[i],false));
						}
					}
				}
			}
		}
		
		for(int i=0; i<fileList.size(); i++){
			if(true==fileList.get(i).getSameName()){
				System.out.println("same rpt file is "+fileList.get(i).getPath()+"\\"+fileList.get(i).getFileName());
			}	
		}
	}
}
