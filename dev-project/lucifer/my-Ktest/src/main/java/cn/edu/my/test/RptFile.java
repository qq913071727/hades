package cn.edu.my.test;

public class RptFile{
	private String fileName;
	private String path;
	private boolean sameName;

	public RptFile(String fileName,String path,boolean sameName){
		this.fileName=fileName;
		this.path=path;
		this.sameName=sameName;
	}

	public String getFileName(){
		return this.fileName;	
	}

	public String getPath(){
		return this.path;	
	}

	public void setSameName(boolean sameName){
		this.sameName=sameName;	
	}

	public boolean getSameName(){
		return this.sameName;
	}
}