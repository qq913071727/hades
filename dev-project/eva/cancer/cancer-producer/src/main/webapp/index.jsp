<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title></title>
		
		<script type="text/javascript" src="jslib/jquery-easyui-1.4/jquery.min.js"></script>
		<script type="text/javascript" src="jslib/jquery-easyui-1.4/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="jslib/jquery-easyui-1.4/locale/easyui-lang-zh_CN.js"></script>
		<link rel="stylesheet" href="jslib/jquery-easyui-1.4/themes/default/easyui.css" type="text/css"></link>
		<link rel="stylesheet" href="jslib/jquery-easyui-1.4/themes/icon.css" type="text/css"></link>
		<script type="text/javascript" src="jslib/util.js"></script>
		
		<script type="text/javascript">
			${function(){
				/*$('#index_regform').form({
				    url:,
				    onSubmit: function(){
				        
				    },
				    success:function(data){
					    
				});*/
			}};
		</script>
		
	</head>
	<body class="easyui-layout">
		<div data-options="region:'north',title:'North Title',split:true" style="height:100px;"></div>
    	<div data-options="region:'south',title:'South Title',split:true" style="height:100px;"></div>
    	<div data-options="region:'east',title:'East',split:true" style="width:100px;"></div>
    	<div data-options="region:'west',split:true" style="width:100px;">
    		<div class="easyui-panel" data-options="title:'sss',border:false,fit:true"></div>
    	</div>
    	<div data-options="region:'center',title:'center title'" style="padding:5px;background:#eee;"></div>
    	
    	<div id="index_logDialog" class="easyui-dialog" data-options="title:'登录',modal:true,closable:false,
			buttons:[{
				text:'注册',
				iconCls:'icon-edit',
				handler:function(){
					$('#index_regDialog').dialog('open');
				}
			},{
				text:'登录',
				iconCls:'icon-help',
				handler:function(){
					alert('11111111111');
					$('#index_logForm').form('submit',{
						url:'userAction!test.action',
					
						onsubmit:function () {
							alert('22222222222222');
	                    	return true;
	                	},
                	
						success:function(result){
							$.messager.alert('提示信息', '操作成功');
						}
					});
				}
			}]">
			<form id="index_logForm" method="post">
	    		<table>
	    			<tr>
	    				<th>登录名</th>
	    				<td><input /></td>
	    			</tr>
	    			<tr>
	    				<th>密码</th>
	    				<td><input /></td>
	    			</tr>
	    		</table>
	    	</form>
    	</div>
    	
    	<div id="index_regDialog" class="easyui-dialog" data-options="title:'注册',closed:true,modal:true,
			buttons:[{
				text:'注册',
				iconCls:'icon-edit',
				handler:function(){
					$('#index_regform').form('submit',{
				    url:'',
				    success:function(data){
					    
					}
				});
				}
			}]">
			<form id="index_regform" method="post">
	    		<table>
	    			<tr>
	    				<th>登录名</th>
	    				<td><input name="name" class="easyui-validatebox" data-options="required:true,missingMessage:'登录名称必填'" /></td>
	    			</tr>
	    			<tr>
	    				<th>密码</th>
	    				<td><input name="pwd" type="password" class="easyui-validatebox" data-options="required:true" /></td>
	    			</tr>
	    			<tr>
	    				<th>重复密码</th>
	    				<td><input name="rePwd" type="password" class="easyui-validatebox" data-options="required:true,validType:'eqPwd[\'#index_reform input[name=pwd]\']'" /></td>
	    			</tr>
	    		</table>
    		</form>
    	</div>
	</body>
</html>