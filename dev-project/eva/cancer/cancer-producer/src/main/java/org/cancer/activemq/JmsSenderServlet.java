package org.cancer.activemq;

import java.io.IOException;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
 
public class JmsSenderServlet extends HttpServlet {
     
    private ConnectionFactory connectionFactory = null;
    private Connection connection = null;
    private Session session = null;
    private Destination destination = null;
    private MessageProducer producer = null;

    private static final long serialVersionUID = 1L;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("2222222222222222222222");
		request.setCharacterEncoding("utf-8");
		send();
		request.getRequestDispatcher("/addTask.jsp").forward(request,response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}
     
    public void send(){
        connectionFactory = new ActiveMQConnectionFactory(
                ActiveMQConnection.DEFAULT_USER,
                ActiveMQConnection.DEFAULT_PASSWORD,"tcp://localhost:61616");
         
        try{
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(Boolean.TRUE.booleanValue(), 
                    Session.AUTO_ACKNOWLEDGE);
            //Queue
            destination = session.createQueue("xkey");
            producer = session.createProducer(destination);
            //Topic
            /**
             * Topic topic = session.createTopic("xkey.Topic");
             * producer = session.createProducer(topic);
            */
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            sendMessage(session,producer);
            session.commit();
             
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try {
                connection.close();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }
     
    private void sendMessage(Session session,MessageProducer producer) throws JMSException{
        for (int i = 1; i <= 5; i ++) { 
            TextMessage message = session.createTextMessage("First ActiveMQ Test:::: " + i); 
            // ������Ϣ
            System.out.println("Sender��" + "First ActiveMQ Test::: " + i); 
            producer.send(message); 
        } 
    }
}
