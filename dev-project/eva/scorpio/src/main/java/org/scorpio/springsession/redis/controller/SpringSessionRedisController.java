package org.scorpio.springsession.redis.controller;

import javax.servlet.http.HttpServletRequest;

import org.scorpio.springsession.redis.domain.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
//@RequestMapping(value = "index")
public class SpringSessionRedisController {
	private final Gson gson = new GsonBuilder().setDateFormat("yyyyMMddHHmmss").create();
	
	@RequestMapping(value = "login")
	public String login(HttpServletRequest request, String username) {
		request.getSession().setAttribute("user", gson.toJson(new User("zhangsan", "123456")));
		return "stock";
	}

	@RequestMapping(value = "index")
	public String index(HttpServletRequest request, Model model) {
		User user = gson.fromJson(request.getSession().getAttribute("user").toString(), User.class);
		System.out.println(user.getUsername());
		model.addAttribute("user", user);
		return "stock";
	}
}
