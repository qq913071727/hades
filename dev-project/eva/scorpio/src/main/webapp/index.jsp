<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<script type="text/javascript" src="jslib/jquery-easyui-1.4/jquery.min.js"></script>
		<script type="text/javascript" src="jslib/jquery-easyui-1.4/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="jslib/jquery-easyui-1.4/locale/easyui-lang-zh_CN.js"></script>
		<link rel="stylesheet" href="jslib/jquery-easyui-1.4/themes/default/easyui.css" type="text/css"></link>
		<link rel="stylesheet" href="jslib/jquery-easyui-1.4/themes/icon.css" type="text/css"></link>
		
		<title>Insert title here</title>
	</head>
	<body>
		<h2>Basic Calendar</h2>
		<p>Click to select date.</p>
		<div style="margin:20px 0"></div>
		<div class="easyui-calendar" style="width:250px;height:250px;"></div>
	</body>
</html>