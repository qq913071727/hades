package org.gemini.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.gemini.linear.multivariate.MultivariateDataPoint;
import org.gemini.linear.multivariate.MultivariateLinearRegression;
import org.gemini.linear.unary.UnaryDataPoint;
import org.gemini.linear.unary.UnaryLinearRegression;
import org.gemini.nonlinear.ExponentialCurveModel;
import org.gemini.nonlinear.LogarithmicCurveModel;
import org.gemini.nonlinear.HyperbolaModel;
import org.gemini.nonlinear.PowerFunctionCurveModel;
import org.gemini.nonlinear.ParabolaModel;

public class TestRegression {
	public static void main(String[] args) {
		//testUnaryLinearRegression();
		//testMultivariateLinearRegression();
		//testExponentialCurveModel();
		//testLogarithmicCurveModel();
		//testHyperbolaModel();
		//testPowerFunctionCurveModel();
		testParabolaModel();
	}
	
	private static void testParabolaModel(){
		ParabolaModel pm=new ParabolaModel();
		pm.obtainParabolaModel();
	}
	
	private static void testPowerFunctionCurveModel(){
		PowerFunctionCurveModel pfc=new PowerFunctionCurveModel();
		pfc.obtainPowerFunctionCurveModel();
	}
	
	private static void testHyperbolaModel(){
		HyperbolaModel hm=new HyperbolaModel();
		hm.obtainHyperbolaModel();
	}
	
	private static void testLogarithmicCurveModel(){
		LogarithmicCurveModel lcm=new LogarithmicCurveModel();
		lcm.obtainLogarithmicCurveModel();
	}
	
	private static void testExponentialCurveModel(){
		ExponentialCurveModel ec=new ExponentialCurveModel();
		ec.obtainExponentialCurveModel();
	}
	
	private static void testMultivariateLinearRegression(){
		MultivariateLinearRegression mlr=new MultivariateLinearRegression();
		mlr.obtainMultivariateLinearRegressionModel();
	}
	
	private static void testUnaryLinearRegression(){
		UnaryLinearRegression ulr=new UnaryLinearRegression();
		ulr.obtainUnaryLinearRegression();
	}
}
