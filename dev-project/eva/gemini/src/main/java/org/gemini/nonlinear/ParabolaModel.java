package org.gemini.nonlinear;

import java.util.List;
import java.util.ArrayList;

import Jama.Matrix;

import org.gemini.model.TradeFundFlowDirection;
import org.gemini.service.TradeFundFlowDirectionService;
import org.gemini.service.impl.TradeFundFlowDirectionServiceImpl;

public class ParabolaModel {
	private int argumentNumber=2;
	private TradeFundFlowDirectionService tradeFundFlowDirectionService;
	//private List<MultivariateDataPoint> multivariateDataPointList;
	private List<TradeFundFlowDirection> tradeFundFlowDirectionList;
	
	public ParabolaModel() {
	}
	
	// 抛物线模型
	public void obtainParabolaModel(){
		tradeFundFlowDirectionService=new TradeFundFlowDirectionServiceImpl();
		tradeFundFlowDirectionList = tradeFundFlowDirectionService.readTradeFundFlowDirectionList();
		
		double[][] arrayArgument=new double[tradeFundFlowDirectionList.size()][argumentNumber+1];
		double[] arrayDependent=new double[tradeFundFlowDirectionList.size()];
		for(int i=0;i<tradeFundFlowDirectionList.size();i++){
			arrayArgument[i]=new double[]{1,
					tradeFundFlowDirectionList.get(i).getLargeSheetNetInflowAmount(),
					tradeFundFlowDirectionList.get(i).getMainForceNetInflowAmount()};
			arrayDependent[i]=tradeFundFlowDirectionList.get(i).getUpDownRange();
		}
		
      	Matrix matrixArgument = new Matrix(arrayArgument);
      	Matrix matrixArgumentTranspose=matrixArgument.transpose();
      	Matrix matrixDependent = new Matrix(arrayDependent,tradeFundFlowDirectionList.size());
      	Matrix result=matrixArgumentTranspose.times(matrixArgument).inverse().times(matrixArgumentTranspose).times(matrixDependent);
      	result.print(result.getRowDimension(),2);
		for(int i=0;i<result.getRowDimension();i++){
			System.out.println("constant"+i+"  :  "+result.get(i,0));
		}
	}
}