package org.gemini.service.impl;

import org.gemini.model.TradeFundFlowDirection;
import org.gemini.service.TradeFundFlowDirectionService;
import org.gemini.dao.TradeFundFlowDirectionDao;
import org.gemini.dao.impl.TradeFundFlowDirectionDaoImpl;

import java.util.List;

public class TradeFundFlowDirectionServiceImpl implements TradeFundFlowDirectionService{
	private TradeFundFlowDirectionDao tradeFundFlowDirectionDao=new TradeFundFlowDirectionDaoImpl();
	
	public List<TradeFundFlowDirection> readTradeFundFlowDirectionList(){
		return tradeFundFlowDirectionDao.readTradeFundFlowDirectionList();
	}
}