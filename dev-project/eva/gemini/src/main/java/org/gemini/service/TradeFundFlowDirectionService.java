package org.gemini.service;

import org.gemini.model.TradeFundFlowDirection;

import java.util.List;

public interface TradeFundFlowDirectionService{
	List<TradeFundFlowDirection> readTradeFundFlowDirectionList();
}