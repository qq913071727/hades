package org.gemini.dao;

import java.util.List;

import org.gemini.model.TradeFundFlowDirection;

public interface TradeFundFlowDirectionDao{
	List<TradeFundFlowDirection> readTradeFundFlowDirectionList();
}