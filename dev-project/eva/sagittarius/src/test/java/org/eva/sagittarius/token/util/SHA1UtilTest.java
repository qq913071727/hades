package org.eva.sagittarius.token.util;

import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;
import org.eva.sagittarius.token.constant.TokenType;
import org.eva.sagittarius.token.domain.Token;
import org.eva.sagittarius.token.exception.TokenException;
import org.eva.sagittarius.token.service.TokenService;
import org.junit.Test;
import org.eva.sagittarius.token.domain.User;
import org.eva.sagittarius.token.service.impl.TokenServiceImpl;

public class SHA1UtilTest {
	private TokenService tokenService=new TokenServiceImpl();

	@Test
	public void testSha1Encrypt(){
		Date date=new Date();
		
		Token token=new Token();
		token.setVersion(TokenType.CODE);
		token.setLoginTime(date);
		token.setExpirationTime(new Date(2020,02,02));
		User user=new User();
		user.setUsername("zhangsan");
		user.setPassword("123");
		token.setUser(user);
		String tokenString=null;
		try {
			tokenString = tokenService.createTokenString(token);
			System.out.println(tokenString);
		} catch (TokenException e) {
			e.printStackTrace();
		}
//		System.out.println(SHA1Util.sha1Encrypt(tokenString));
		System.out.println(DigestUtils.shaHex(tokenString));
		
		Token token1=new Token();
		token1.setVersion(TokenType.CODE);
		token1.setLoginTime(date);
		token1.setExpirationTime(new Date(2020,02,02));
		User user1=new User();
		user1.setUsername("zhangsan");
		user1.setPassword("123");
		token1.setUser(user1);
		String tokenString1=null;
		try {
			tokenString1 = tokenService.createTokenString(token1);
			System.out.println(tokenString1);
		} catch (TokenException e) {
			e.printStackTrace();
		}
//		System.out.println(SHA1Util.sha1Encrypt(tokenString1));
		System.out.println(DigestUtils.shaHex(tokenString1));
		
//		String s1=new String("123");
//		String s2=new String("123");
//		System.out.println(DigestUtils.shaHex(s1));
//		System.out.println(DigestUtils.shaHex(s2));
		
//		User u1=new User();
//		u1.setUsername("zhangsan");
//		u1.setPassword("123");
//		User u2=new User();
//		u2.setUsername("zhangsan");
//		u2.setPassword("123");
//		System.out.println(DigestUtils.shaHex(u1.toString()));
//		System.out.println(DigestUtils.shaHex(u2.toString()));
	}
}
