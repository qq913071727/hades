package org.eva.sagittarius.token.service.impl;

import java.util.Date;

import org.eva.sagittarius.token.util.SHA1Util;
import org.junit.Test;
import org.eva.sagittarius.token.constant.TokenType;
import org.eva.sagittarius.token.domain.Token;
import org.eva.sagittarius.token.domain.User;
import org.eva.sagittarius.token.exception.TokenException;
import org.eva.sagittarius.token.service.TokenService;

public class SHA1UtilTest {

private TokenService tokenService=new TokenServiceImpl();
	
	Token token=new Token();
	String tokenString=null;
	{
		token.setVersion(TokenType.CODE);
		token.setLoginTime(new Date());
		token.setExpirationTime(new Date(2020,02,02));
		User user=new User();
		user.setUsername("zhangsan");
		user.setPassword("123");
		token.setUser(user);
		try {
			tokenString = tokenService.createTokenString(token);
			System.out.println("tokenString:   "+tokenString);
		} catch (TokenException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSha1Encrypt(){
		String str= SHA1Util.sha1Encrypt(tokenString);
		System.out.println(str);
	}
}
