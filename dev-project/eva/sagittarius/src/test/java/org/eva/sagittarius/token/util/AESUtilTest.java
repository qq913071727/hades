package org.eva.sagittarius.token.util;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.eva.sagittarius.token.service.TokenService;
import org.junit.Test;
import org.eva.sagittarius.token.constant.TokenType;
import org.eva.sagittarius.token.domain.Token;
import org.eva.sagittarius.token.domain.User;
import org.eva.sagittarius.token.exception.TokenException;
import org.eva.sagittarius.token.service.impl.TokenServiceImpl;

import com.alibaba.fastjson.JSON;

public class AESUtilTest {
	/**
	 * AES加密算法的秘钥
	 */
	private static final String aesPassword = PropertiesUtil.getValue("config.properties", "aes.password");

	private TokenService tokenService=new TokenServiceImpl();
	
	Token token=new Token();
	String tokenString=null;
	{
		token.setVersion(TokenType.CODE);
		token.setLoginTime(new Date());
		token.setExpirationTime(new Date(2020,02,02));
		User user=new User();
		user.setUsername("zhangsan");
		user.setPassword("123");
		token.setUser(user);
		try {
			tokenString = tokenService.createTokenString(token);
			System.out.println("tokenString:   "+tokenString);
		} catch (TokenException e) {
			e.printStackTrace();
		}
	}

//	@Test
	public void testAesEncrypt(){
		System.out.println("AESUtil.aesEncrypt:   "+AESUtil.aesEncrypt(tokenString, aesPassword));
	}
	
//	@Test
	public void testAesDecrypt() throws UnsupportedEncodingException{
		System.out.println("加密之前："+tokenString);
		byte[] b=AESUtil.aesEncrypt(tokenString, aesPassword);
		System.out.println("加密之后");
		System.out.println(b);//此处打印出来的值不准，只能在debug模式下查看b
		System.out.println("AESUtil.aesDecrypt:   "+AESUtil.aesDecrypt(b, aesPassword));
	}
	
	@Test
	public void testDoubleAesEncrypt(){
		User user=new User();
		user.setUsername("zhangsan");
		user.setPassword("123");
		String userString=JSON.toJSONString(user);
		StringBuffer sb=new StringBuffer();
		StringBuffer sb1=new StringBuffer();
		String str1=sb.append(AESUtil.aesEncrypt(userString, aesPassword)).toString();
		System.out.println(str1);
		String str2=sb.append(AESUtil.aesEncrypt(userString, aesPassword)).toString();
		System.out.println(str2);
		
//		User user1=new User();
//		user1.setUsername("zhangsan");
//		user1.setPassword("123");
//		String userString1=JSON.toJSONString(user1);
//		StringBuffer sb1=new StringBuffer();
//		userString1=sb1.append(AESUtil.aesEncrypt(userString1, aesPassword)).toString();
//		System.out.println(userString1);
	}
	
	
}
