package org.eva.sagittarius.token.util;

import java.util.Date;

import org.eva.sagittarius.token.constant.TokenType;
import org.eva.sagittarius.token.domain.Token;
import org.eva.sagittarius.token.domain.User;
import org.eva.sagittarius.token.exception.TokenException;
import org.eva.sagittarius.token.service.TokenService;
import org.eva.sagittarius.token.service.impl.TokenServiceImpl;
import org.junit.Test;

public class Base64UtilTest {
	
	private TokenService tokenService=new TokenServiceImpl();
	
	Token token=new Token();
	String tokenString=null;
	{
		token.setVersion(TokenType.CODE);
		token.setLoginTime(new Date());
		token.setExpirationTime(new Date(2020,02,02));
		User user=new User();
		user.setUsername("zhangsan");
		user.setPassword("123");
		token.setUser(user);
		try {
			tokenString = tokenService.createTokenString(token);
			System.out.println("tokenString:   "+tokenString);
		} catch (TokenException e) {
			e.printStackTrace();
		}
	}

//	@Test
	public void testEncode(){
		String str=Base64Util.encode(tokenString.getBytes());
		System.out.println(str);
	}
	
	@Test
	public void testDecode(){
		System.out.println("加密前："+tokenString);
		String str=Base64Util.encode(tokenString.getBytes());
		System.out.println("加密后："+str);
		byte[] s=Base64Util.decode(str.toCharArray());//在debug模式中看s
		System.out.println("解密后："+new String(s));
	}
	
	
	
	
}
