package org.eva.sagittarius.token.service.impl;

import java.util.Date;

import org.junit.Test;
import org.eva.sagittarius.token.constant.TokenType;
import org.eva.sagittarius.token.domain.Token;
import org.eva.sagittarius.token.domain.User;
import org.eva.sagittarius.token.exception.TokenException;
import org.eva.sagittarius.token.service.TokenService;
import org.eva.sagittarius.token.util.PropertiesUtil;

public class TokenServiceImplTest {
	
	/**
	 * AES加密算法的秘钥
	 */
	private static final String aesPassword = PropertiesUtil.getValue("config.properties", "aes.password");

	private TokenService tokenService=new TokenServiceImpl();
	
	public TokenServiceImplTest() {
		super();
	}
	
	@Test
	public void testToTokenString(){
		System.out.println("========================= testToTokenString =============================");
		
		Token token=new Token();
		token.setVersion(TokenType.CODE);
		token.setLoginTime(new Date());
		token.setExpirationTime(new Date(2020,02,02));
		User user=new User();
		user.setUsername("zhangsan");
		user.setPassword("123");
		token.setUser(user);
		String tokenString=null;
		try {
			tokenString = tokenService.createTokenString(token);
			System.out.println(tokenString);
		} catch (TokenException e) {
			e.printStackTrace();
		}
		
		System.out.println("=======================================================================");
	}
	
	@Test
	public void testEncrypt(){
		System.out.println("========================= testEncrypt =============================");
		
		Token token=new Token();
		token.setVersion(TokenType.CODE);
		token.setLoginTime(new Date());
		token.setExpirationTime(new Date(2020,02,02));
		User user=new User();
		user.setUsername("zhangsan");
		user.setPassword("123");
		token.setUser(user);
		String tokenString=null;
		try {
			tokenString = tokenService.createTokenString(token);
//			System.out.println(tokenString);
		} catch (TokenException e) {
			e.printStackTrace();
		}
		String tokenStringEncrypt=tokenService.encrypt(tokenString);
		System.out.println(tokenStringEncrypt);
		
		System.out.println("=======================================================================");
	}
	
	@Test
	public void testDecrypt(){
		System.out.println("========================= testDecrypt =============================");
		
		Token token=new Token();
		token.setVersion(TokenType.CODE);
		token.setLoginTime(new Date());
		token.setExpirationTime(new Date(2020,02,02));
		User user=new User();
		user.setUsername("lisi");
		user.setPassword("abc");
		token.setUser(user);
		String tokenString=null;
		try {
			tokenString = tokenService.createTokenString(token);
		} catch (TokenException e) {
			e.printStackTrace();
		}
		String tokenStringEncrypt=tokenService.encrypt(tokenString);
//		System.out.println(tokenStringEncrypt);
		boolean b=tokenService.decrypt(tokenStringEncrypt);
		System.out.println(b);
		
		System.out.println("=======================================================================");
	}
	
	@Test
	public void testIsExpiration(){
		Token token=new Token();
		token.setVersion(TokenType.CODE);
		token.setLoginTime(new Date());
		token.setExpirationTime(new Date(118,11,10));
		System.out.println(token.getExpirationTime());
		User user=new User();
		user.setUsername("lisi");
		user.setPassword("abc");
		token.setUser(user);
		String tokenString=null;
		try {
			tokenString = tokenService.createTokenString(token);
		} catch (TokenException e) {
			e.printStackTrace();
		}
		String tokenStringEncrypt=tokenService.encrypt(tokenString);
		boolean b=tokenService.isExpiration(tokenStringEncrypt);
		System.out.println(b);
	}
	
//	@Test
//	public void testEncodeBase64(){
//		System.out.println("========================= testEncodeBase64 =============================");
//		
//		try {
//			System.out.println(System.getProperty("file.encoding"));
//			System.out.println(Base64Util.encode("[B@a1cdc6d"));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		System.out.println("=======================================================================");
//	}
	
//	@Test
//	public void testDecodeBase64(){
//		System.out.println("========================= testDecodeBase64 =============================");
//		
//		try {
//			System.out.println(Base64Util.decode("W0JAYTFjZGM2ZA=="));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		System.out.println("=======================================================================");
//	}
}
