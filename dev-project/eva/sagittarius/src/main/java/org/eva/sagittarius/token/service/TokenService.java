package org.eva.sagittarius.token.service;

import org.eva.sagittarius.token.domain.Token;
import org.eva.sagittarius.token.exception.TokenException;

/**
 * 令牌服务接口
 * @author lishen
 *
 */
public interface TokenService {

	/**
	 * 将token对象转换为令牌（String格式）
	 * @param token
	 * @return
	 */
	String createTokenString(Token token) throws TokenException;
	
	/**
	 * 对令牌进行加密
	 * @param tokenString
	 * @return
	 */
	String encrypt(String tokenString);
	
	/**
	 * 对令牌进行解密，并判断令牌是否被篡改。true：表示解密成功，令牌没有被篡改；false：表示解密失败，令牌被篡改了。
	 * @param tokenString
	 * @return
	 */
	boolean decrypt(String tokenString);
	
	/**
	 * 判断令牌是否过期。true：过期；false：没过期
	 * @param tokenString
	 * @return
	 */
	boolean isExpiration(String tokenString);
}
