package org.eva.sagittarius.token.util;

import java.util.Date;

/**
 * 16进制工具类
 * @author lishen
 *
 */
public class HexUtil {

	/**
	 * 将date对象所代表的时间转换为16进制的字符串（不包括0x）
	 * @param date
	 * @return
	 */
	public static String dateToHex(Date date){
		long timer = date.getTime() / 1000;
		return Long.toHexString(timer);
	}
	
	/**
	 * 将16进制的字符串（不包括0x）转换为Date类型的对象
	 * @param hex
	 * @return
	 */
	public static Date hexToDate(String hex){
		long i=Integer.parseInt(hex, 16);
		Date date=new Date(i*1000);
		System.out.println(System.currentTimeMillis());
		return date;
	}
	
	
}
