package org.eva.sagittarius.token.domain;

import java.util.Date;

/**
 * 令牌
 * @author lishen
 *
 */
public class Token {
	/**
	 * 版本信息
	 */
	private String version;
	
	/**
	 * 登录时间
	 */
	private Date loginTime;
	
	/**
	 * 会话失效时间
	 */
	private Date expirationTime;
	
	/**
	 * 登录用户信息，或其他信息
	 */
	private User user;
	
	/**
	 * 校验和
	 */
	private String checkSum;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getCheckSum() {
		return checkSum;
	}

	public void setCheckSum(String checkSum) {
		this.checkSum = checkSum;
	}

}
