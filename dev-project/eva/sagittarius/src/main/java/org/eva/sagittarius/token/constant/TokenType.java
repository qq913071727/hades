package org.eva.sagittarius.token.constant;

/**
 * 表示token的类型。0x01表示code，0x02表示access_token
 * @author lishen
 *
 */
public class TokenType {

	/**
	 * 表示code
	 */
	public static final String CODE="0x01";
	
	/**
	 * 表示access_token
	 */
	public static final String ACCESS_TOKEN="0x02";
}
