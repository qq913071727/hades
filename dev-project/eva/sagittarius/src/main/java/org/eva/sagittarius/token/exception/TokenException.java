package org.eva.sagittarius.token.exception;

/**
 * 令牌异常雷
 * @author lishen
 *
 */
public class TokenException extends Exception {

	private static final long serialVersionUID = 1L;

	public TokenException(String message){
		super(message);
	}
}
