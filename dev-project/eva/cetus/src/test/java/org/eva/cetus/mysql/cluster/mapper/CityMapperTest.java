package org.eva.cetus.mysql.cluster.mapper;

import junit.framework.Assert;
import org.eva.cetus.mysql.cluster.domain.City;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CityMapperTest {
    @Autowired
    private CityMapper cityMapper;

    @Test
    public void testFindById(){
        City city=cityMapper.findById(new Long(111111));
        Assert.assertEquals(city.getName(), "aaa");
    }

    @Test
    public void testFindAll(){
        List<City> cityList=cityMapper.findAll();
        Assert.assertEquals(cityList.size(), 6);
    }

    @Test
    public void testInsert() throws Exception {
        City city=new City();
        city.setId(new Long(111111));
        city.setName("aaa");
        cityMapper.insert(city);
        city=new City();
        city.setId(new Long(222222));
        city.setName("bbb");
        cityMapper.insert(city);
        city=new City();
        city.setId(new Long(333333));
        city.setName("ccc");
        cityMapper.insert(city);

        Assert.assertEquals(6, cityMapper.findAll().size());
    }

    @Test
    public void testUpdate(){
        City city=new City();
        city.setId(new Long(111111));
        city.setName("aaaaaa");
        cityMapper.update(city);

        Assert.assertEquals("aaaaaa", cityMapper.findById(new Long(111111)).getName());
    }

    @Test
    public void TestDelete(){
        cityMapper.delete(new Long(333333));

        Assert.assertEquals(5, cityMapper.findAll().size());
    }
}
