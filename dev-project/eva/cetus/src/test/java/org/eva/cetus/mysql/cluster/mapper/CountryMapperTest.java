package org.eva.cetus.mysql.cluster.mapper;

import org.eva.cetus.mysql.cluster.domain.Country;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CountryMapperTest {
    @Autowired
    private CountryMapper countryMapper;

    @Test
    public void testFindAll(){
        List<Country> countryList=countryMapper.findAll();
        Assert.assertEquals(0, countryList.size());
    }

    @Test
    public void TestFindById(){
        Country country=countryMapper.findById(new Long("111"));
        Assert.assertEquals("China", country.getName());
    }

    @Test
    public void TestInsert(){
        Country country=new Country();
        country.setId(new Long("111"));
        country.setName("China");
        countryMapper.insert(country);

        Assert.assertEquals(1, countryMapper.findAll().size());
    }

    @Test
    public void TestUpdate(){
        Country country=new Country();
        country.setId(new Long("111"));
        country.setName("USA");
        countryMapper.update(country);

        Assert.assertEquals("USA", countryMapper.findById(new Long("111")).getName());
    }

    @Test
    public void TestDelete(){
        countryMapper.delete(new Long("111"));

        Assert.assertEquals(0, countryMapper.findAll().size());
    }
}
