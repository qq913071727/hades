package org.eva.cetus.mysql.cluster.service.impl;

import org.eva.cetus.mysql.cluster.domain.Country;
import org.eva.cetus.mysql.cluster.mapper.CityMapper;
import org.eva.cetus.mysql.cluster.mapper.CountryMapper;
import org.eva.cetus.mysql.cluster.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("countryService")
public class CountryServiceImpl implements CountryService {
    @Autowired
    private CountryMapper countryMapper;

    @Override
    public List<Country> findAll() {
        return countryMapper.findAll();
    }

    @Override
    public Country findById(Long id) {
        return countryMapper.findById(id);
    }

    @Override
    public void insert(Country country) {
        countryMapper.insert(country);
    }

    @Override
    public void update(Country country) {
        countryMapper.update(country);
    }

    @Override
    public void delete(Long id){
        countryMapper.delete(id);
    }
}
