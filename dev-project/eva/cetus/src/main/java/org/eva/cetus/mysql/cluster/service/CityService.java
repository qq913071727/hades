package org.eva.cetus.mysql.cluster.service;

import org.eva.cetus.mysql.cluster.domain.City;

import java.util.List;

public interface CityService {

    City findById(Long id);

    List<City> findAll();

    void insert(City city);

    void update(City city);

    void delete(Long id);
}
