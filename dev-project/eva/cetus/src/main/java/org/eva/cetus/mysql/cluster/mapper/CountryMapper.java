package org.eva.cetus.mysql.cluster.mapper;

import org.eva.cetus.mysql.cluster.domain.Country;

import java.util.List;

public interface CountryMapper {
    List<Country> findAll();

    Country findById(Long id);

    void insert(Country country);

    void update(Country country);

    void delete(Long id);
}
