package org.eva.cetus.mysql.cluster.service.impl;

import org.eva.cetus.mysql.cluster.domain.City;
import org.eva.cetus.mysql.cluster.mapper.CityMapper;
import org.eva.cetus.mysql.cluster.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service("cityService")
public class CityServiceImpl implements CityService {

    @Autowired
    private CityMapper cityMapper;

    public City findById(Long id){
        return cityMapper.findById(id);
    }

    public List<City> findAll(){
        return cityMapper.findAll();
    }

    public void insert(City city){
        cityMapper.insert(city);
    }

    public void update(City city){
        cityMapper.update(city);
    }

    public void delete(Long id){
        cityMapper.delete(id);
    }
}
