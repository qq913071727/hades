package org.eva.cetus.mysql.cluster;

import org.eva.cetus.mysql.cluster.domain.City;
import org.eva.cetus.mysql.cluster.mapper.CityMapper;
import org.eva.cetus.mysql.cluster.service.CityService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@MapperScan("org.eva.cetus.mysql.cluster.mapper")
public class Application {

    public static void main(String[] args){
        SpringApplication.run(Application.class, args);
    }

}
