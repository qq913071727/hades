package org.eva.cetus.mysql.cluster.web;

import org.eva.cetus.mysql.cluster.domain.City;
import org.eva.cetus.mysql.cluster.service.CityService;
import org.eva.cetus.mysql.cluster.vo.CityVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class CityController {

    @Autowired
    private CityService cityService;

    @RequestMapping(value="/city/{id}", method= RequestMethod.GET)
    public void findById(@PathVariable Long id){
        City city=cityService.findById(id);
        System.out.println(city.getName());
    }

    @RequestMapping(value = "/cities", method = RequestMethod.GET)
    public void findAll(){
        List<City> cityList=cityService.findAll();
        if(null!=cityList && cityList.size()>0){
            for(City city:cityList){
                System.out.println("city id:"+city.getId()+"   city name:"+city.getName());
            }
        }
    }

    @RequestMapping(value = "/city/add", method = RequestMethod.POST)
    public void insert(@RequestBody CityVO cityVO){
        if(null!=cityVO){
            City city=new City();
            city.setId(cityVO.getId());
            city.setName(cityVO.getName());
            cityService.insert(city);
        }
    }

    @RequestMapping(value = "/city/update", method = RequestMethod.POST)
    public void update(@RequestBody CityVO cityVO){
        if(null!=cityVO){
            City city=new City();
            city.setId(cityVO.getId());
            city.setName(cityVO.getName());
            cityService.update(city);
        }
    }

    @RequestMapping(value = "/city/delete/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id){
        if(null!=id){
            cityService.delete(id);
        }
    }
}
