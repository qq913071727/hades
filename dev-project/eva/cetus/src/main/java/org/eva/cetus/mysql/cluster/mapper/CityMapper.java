package org.eva.cetus.mysql.cluster.mapper;

import org.apache.ibatis.annotations.*;
import org.eva.cetus.mysql.cluster.domain.City;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface CityMapper {

    @Select("SELECT * FROM tb_city WHERE id = #{id}")
    City findById(Long id);

    @Select("SELECT * FROM tb_city")
    List<City> findAll();

    @Insert("INSERT INTO tb_city(id, name) VALUES(#{id}, #{name})")
    void insert(City city);

    @Update("UPDATE tb_city SET name=#{name} WHERE id =#{id}")
    void update(City city);

    @Delete("DELETE FROM tb_city WHERE id =#{id}")
    void delete(Long id);
}
