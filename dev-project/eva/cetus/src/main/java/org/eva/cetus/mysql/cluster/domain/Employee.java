//package org.eva.cetus.mysql.cluster.domain;
//
//import javax.persistence.*;
//
//@Entity
//public class Employee {
//    @Id
//    @GeneratedValue
//    private Long id;
//
//    private String name;
//
//    private String password;
//
//    private String email;
//
//    private String role;
//
//    @ManyToOne(targetEntity = Employee.class)
//    @JoinColumn(name="manager_id")
//    private Employee manager;
//
//    public Employee() {
//        super();
//    }
//
//    public Employee(Long id, String name, String password, String email, String role, Employee manager) {
//        super();
//        this.id = id;
//        this.name = name;
//        this.password = password;
//        this.email = email;
//        this.role = role;
//        this.manager = manager;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public String getRole() {
//        return role;
//    }
//
//    public void setRole(String role) {
//        this.role = role;
//    }
//
//    public Employee getManager() {
//        return manager;
//    }
//
//    public void setManager(Employee manager) {
//        this.manager = manager;
//    }
//}
