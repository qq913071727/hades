package org.eva.cetus.mysql.cluster.web;

import org.eva.cetus.mysql.cluster.domain.Country;
import org.eva.cetus.mysql.cluster.service.CountryService;
import org.eva.cetus.mysql.cluster.vo.CountryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CountryController {
    @Autowired
    private CountryService countryService;

    @RequestMapping(value="/country/{id}", method= RequestMethod.GET)
    public void findById(@PathVariable Long id){
        Country country=countryService.findById(id);
        System.out.println(country.getName());
    }

    @RequestMapping(value = "/countries", method = RequestMethod.GET)
    public void findAll(){
        List<Country> countryList=countryService.findAll();
        if(null!=countryList && countryList.size()>0){
            for(Country country:countryList){
                System.out.println("city id:"+country.getId()+"   city name:"+country.getName());
            }
        }
    }

    @RequestMapping(value = "/country/add", method = RequestMethod.POST)
    public void insert(@RequestBody CountryVO countryVO){
        if(null!=countryVO){
            Country country=new Country();
            country.setId(countryVO.getId());
            country.setName(countryVO.getName());
            countryService.insert(country);
        }
    }

    @RequestMapping(value = "/country/update", method = RequestMethod.POST)
    public void update(@RequestBody CountryVO countryVO){
        if(null!=countryVO){
            Country country=new Country();
            country.setId(countryVO.getId());
            country.setName(countryVO.getName());
            countryService.update(country);
        }
    }

    @RequestMapping(value = "/country/delete/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id){
        if(null!=id){
            countryService.delete(id);
        }
    }
}
