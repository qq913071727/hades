//package org.eva.cetus.mysql.cluster.domain;
//
//import javax.persistence.*;
//import java.util.Date;
//
//@Entity
//public class LeaveBill {
//    @Id
//    @GeneratedValue
//    private Long id;
//    private Integer days;
//    private String content;
//    private Date leaveDate=new Date();
//    private String remark;
//    @ManyToOne(targetEntity = Employee.class)
//    @JoinColumn(name="user_id")
//    private Employee user;
//    private Integer state=0;
//
//    public LeaveBill() {
//        super();
//    }
//
//    public LeaveBill(Long id, Integer days, String content, Date leaveDate, String remark, Employee user,
//                     Integer state) {
//        super();
//        this.id = id;
//        this.days = days;
//        this.content = content;
//        this.leaveDate = leaveDate;
//        this.remark = remark;
//        this.user = user;
//        this.state = state;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public Integer getDays() {
//        return days;
//    }
//
//    public void setDays(Integer days) {
//        this.days = days;
//    }
//
//    public String getContent() {
//        return content;
//    }
//
//    public void setContent(String content) {
//        this.content = content;
//    }
//
//    public Date getLeaveDate() {
//        return leaveDate;
//    }
//
//    public void setLeaveDate(Date leaveDate) {
//        this.leaveDate = leaveDate;
//    }
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    public Employee getUser() {
//        return user;
//    }
//
//    public void setUser(Employee user) {
//        this.user = user;
//    }
//
//    public Integer getState() {
//        return state;
//    }
//
//    public void setState(Integer state) {
//        this.state = state;
//    }
//}
