package org.eva.cetus.mysql.cluster.service;

import org.eva.cetus.mysql.cluster.domain.City;
import org.eva.cetus.mysql.cluster.domain.Country;

import java.util.List;

public interface CountryService {

    List<Country> findAll();

    Country findById(Long id);

    void insert(Country country);

    void update(Country country);

    void delete(Long id);
}
