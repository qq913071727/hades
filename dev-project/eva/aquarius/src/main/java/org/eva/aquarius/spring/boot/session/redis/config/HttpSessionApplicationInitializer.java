package org.eva.aquarius.spring.boot.session.redis.config;

import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;

/**
* 加载RedisHttpSessionConfiguration配置文件
* 定义springSessionRepositoryFilter拦截所有的请求将session封装为spring session
*
* @author lishen
*/
public class HttpSessionApplicationInitializer extends AbstractHttpSessionApplicationInitializer {
	public HttpSessionApplicationInitializer() {
        super(SpringSessionConfig.class);
    }
}
