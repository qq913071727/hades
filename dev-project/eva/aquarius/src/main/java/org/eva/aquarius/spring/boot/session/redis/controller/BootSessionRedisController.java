package org.eva.aquarius.spring.boot.session.redis.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class BootSessionRedisController {
	
	@RequestMapping(value = "redis")
	@ResponseBody
	public String login(HttpServletRequest request, String username) {
		request.getSession().setAttribute("user", "123");
		String a = (String) request.getSession().getAttribute("user");
		return a;
	}
}
