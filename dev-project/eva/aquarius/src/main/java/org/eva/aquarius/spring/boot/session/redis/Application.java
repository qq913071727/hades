package org.eva.aquarius.spring.boot.session.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
	// 自动配置Spring框架
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
