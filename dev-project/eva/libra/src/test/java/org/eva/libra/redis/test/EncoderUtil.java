package org.eva.libra.redis.test;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.junit.Test;

public class EncoderUtil {

	@Test
	public void decode(){
		String decode=null;
		try {
			decode = URLEncoder.encode("\\xAC\\xED\\x00\\x05t\\x00P", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}  
        System.out.println("编码之后的内容："+decode);  
	}
}
