package org.eva.libra.springboot.web;

import java.util.ArrayList;
import java.util.List;

import org.eva.libra.springboot.domain.InterfaceMonitorEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api")
public class InterfaceMonitorController2 {
	@RequestMapping("toInterfaceMonitor")
    public String toInterfaceMonitor(Model m){
        List<InterfaceMonitorEntity> list = new ArrayList<InterfaceMonitorEntity>();
        InterfaceMonitorEntity u1 = new InterfaceMonitorEntity("0001", "hello1", "11111111111111111");
        InterfaceMonitorEntity u2 = new InterfaceMonitorEntity("0002", "hello2", "22222222222222222");
        InterfaceMonitorEntity u3 = new InterfaceMonitorEntity("0003", "hello3", "33333333333333333");
        list.add(u1);
        list.add(u2);
        list.add(u3);
        m.addAttribute("imList", list);
        m.addAttribute("sysUser", "SysUser");
        return "interfaceMonitor/list";
    }
}
