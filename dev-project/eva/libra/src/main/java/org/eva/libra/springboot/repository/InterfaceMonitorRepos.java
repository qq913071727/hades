package org.eva.libra.springboot.repository;

import java.util.List;

import org.eva.libra.springboot.domain.InterfaceMonitorEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InterfaceMonitorRepos extends JpaRepository<InterfaceMonitorEntity, String> {
	List<InterfaceMonitorEntity> findByErrorInfo(String errorInfo);
}
