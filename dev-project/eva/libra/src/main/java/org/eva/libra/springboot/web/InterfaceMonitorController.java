package org.eva.libra.springboot.web;

import java.util.ArrayList;
import java.util.List;

import org.eva.libra.springboot.domain.InterfaceMonitorEntity;
import org.eva.libra.springboot.repository.InterfaceMonitorRepos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InterfaceMonitorController {
	@Autowired
    private InterfaceMonitorRepos imRepos;
	
	@RequestMapping(value="findByErrorInfo")
    public List<InterfaceMonitorEntity> findByErrorInfo(String errorInfo){
        return imRepos.findByErrorInfo(errorInfo);
    }
	
	
}
