package org.eva.libra.spring.boot.security.oauth.repository;

import org.eva.libra.spring.boot.security.oauth.domain.OauthAuthority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OauthAuthorityRepository extends JpaRepository<OauthAuthority, String> {

}
