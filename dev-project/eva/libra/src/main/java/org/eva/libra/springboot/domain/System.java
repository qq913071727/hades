package org.eva.libra.springboot.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="INTERFACE_MONITOR")
public class System {
	@Id
	@Column(name="ID")
	private String id;
	
	@Column(name="SYSTEM_NAME")
	private String systemName;

	public System() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	
	
}
