package org.eva.libra.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.net.URLDecoder;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONObject;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {

//	public static void main(String[] args) {
//		SpringApplication.run(Application.class, args);
//
//		test();
//	}

	public static void test() {
		String url = "http://localhost:8080/user/1";
		DefaultHttpClient httpClient = new DefaultHttpClient();
		JSONObject jsonResult = null;
		HttpPost method = new HttpPost(url);

		StringEntity entity = new StringEntity(url, "utf-8");
		entity.setContentEncoding("UTF-8");
		entity.setContentType("application/json");
		method.setEntity(entity);

		try {
			HttpResponse result = httpClient.execute(method);
			url = URLDecoder.decode(url, "UTF-8");
			if (result.getStatusLine().getStatusCode() == 200) {
				String str = "";
				str = EntityUtils.toString(result.getEntity());
				System.out.println("entity:   "+str);
			}
			Header headers[] = result.getAllHeaders();
			for(int i=0; i<headers.length; i++){
				System.out.println(headers[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
