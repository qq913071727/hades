package org.eva.libra.spring.boot.security.oauth.repository;

import org.eva.libra.spring.boot.security.oauth.domain.OauthUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OauthUserRepository extends JpaRepository<OauthUser, String> {

	@Query("select u from OauthUser u where lower(u.username)=lower(:username)")
	OauthUser findByUsernameCaseInsensitive(@Param("username") String username);
}
