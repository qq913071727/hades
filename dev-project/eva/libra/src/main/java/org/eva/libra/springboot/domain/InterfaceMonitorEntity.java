package org.eva.libra.springboot.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="INTERFACE_MONITOR")
public class InterfaceMonitorEntity implements Serializable{

	private static final long serialVersionUID = -7346107729362452376L;

	@Id
	@Column(name="ID")
	private String id;
	
	@Column(name="SYSTEM_NAME")
	private String systemName;
	
	@Column(name="ERROR_INFO")
	private String errorInfo;

	public InterfaceMonitorEntity() {
		super();
	}

	public InterfaceMonitorEntity(String id, String systemName, String errorInfo) {
		super();
		this.id = id;
		this.systemName = systemName;
		this.errorInfo = errorInfo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}

	
	
}