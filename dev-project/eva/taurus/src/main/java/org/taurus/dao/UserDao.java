package org.taurus.dao;

import java.io.Serializable;

import org.taurus.model.Tuser;


public interface UserDao {
	Serializable save(Tuser t);
}
