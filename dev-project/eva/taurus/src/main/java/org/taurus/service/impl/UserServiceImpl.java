package org.taurus.service.impl;

import java.io.Serializable;

import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.taurus.dao.UserDao;
import org.taurus.model.Tuser;
import org.taurus.service.UserService;


@Service(value="userService")
//@Transactional
public class UserServiceImpl implements UserService {
	
	private static final Logger logger = Logger.getLogger(UserServiceImpl.class);
	
	private UserDao userDao;

	public UserDao getUserDao() {
		return userDao;
	}

	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
	public Serializable save(Tuser t){
		return userDao.save(t);
	}

	public void test() {
		logger.info("dddddddddddddd");
	}
}
