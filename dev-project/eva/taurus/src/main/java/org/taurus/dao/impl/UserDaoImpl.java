package org.taurus.dao.impl;

import java.io.Serializable;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.taurus.dao.UserDao;
import org.taurus.model.Tuser;


@Repository("userDao")
public class UserDaoImpl implements UserDao {
	
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Autowired	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Serializable save(Tuser t) {
		return this.sessionFactory.getCurrentSession().save(t);
		//return this.sessionFactory.openSession().save(t);
	}

}
