package org.taurus.service;

import java.io.Serializable;

import org.taurus.model.Tuser;


public interface UserService{
	void test();
	Serializable save(Tuser t);
}
