package org.taurus.test;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.taurus.service.UserService;

public class TestSpring {
	
	@Test
	public void test(){
		ApplicationContext ac=new ClassPathXmlApplicationContext(new String[]{"classpath:applicationContext.xml","classpath:spring-hibernate.xml"});
		UserService userService=(UserService) ac.getBean("userService");
		userService.test();
	}
}
