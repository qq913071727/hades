package org.eva.cynosure.controller;

import org.eva.cynosure.model.Msg;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
    @RequestMapping({"/index", "/"})
    public String index(Model model) {
        Msg msg = new Msg("测试标题", "测试内容", "额外信息，只对管理员显示");
        model.addAttribute("msg", msg);
        return "index";
    }

    @RequestMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @RequestMapping("/admin/admin")
    public String admin(Model model) {
        return "admin/admin";
    }

    @RequestMapping("/user/user")
    public String user(Model model) {
        return "user/user";
    }
}
