<!DOCTYPE html>
<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />
<head>
    <meta charset="UTF-8"/>
    <title>
        <@security.authentication property="principal.username" />
    </title>

    <link rel="stylesheet" th:href="@{css/bootstrap.min.css}"/>
    <link rel="stylesheet" th:href="@{css/signin.css}"/>

    <style type="text/css">
        body {
            padding-top: 50px;
        }

        .starter-template {
            padding: 40px 15px;
            text-align: center;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Spring Security演示</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/">首页</a></li>
                </li>
                <li>
                    <a href="http://www.baidu.com">百度</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
        <@security.authorize access="isAuthenticated()">
            <p class="bg-info">登录成功</p>
        </@security.authorize>
        <h1>${msg.title}</h1>
        <p class="bg-primary">${msg.content}</p>
        <@security.authorize access="hasRole('ROLE_ADMIN')">
            <p class="bg-info">${msg.extraInfo}</p>
        </@security.authorize>
        <@security.authorize access="hasRole('ROLE_USER')">
            <p class="bg-info">无更多显示信息</p>
        </@security.authorize>
        <form action="/logout" method="post">
            <input type="submit" class="btn btn-primary" value="注销"/>
        </form>
    </div>
</div>
</body>
</html>