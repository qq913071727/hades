package org.eve.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class UrlTest {
	private static final String YAHOO_URL = "http://download.finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s=USDCAD=x";

	public static void main(String[] args) throws IOException {
		System.getProperties().setProperty("socksProxySet", "true");
		System.getProperties().setProperty("http.proxyHost", "10.1.31.133");
		System.getProperties().setProperty("http.proxyPort", "8080");
		
		URL resourceUrl = new URL(YAHOO_URL);
		InputStream content = (InputStream) resourceUrl.getContent();
		BufferedReader in = new BufferedReader(new InputStreamReader(content));
		String line;

		while ((line = in.readLine()) != null) {
			System.out.println(line);
		}
	}
}
