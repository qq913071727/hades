package org.eve.test;

import org.aries.model.StockRecord;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateTest {
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		// 读取配置文件
		Configuration cfg = new Configuration().configure();
		SessionFactory factory = cfg.buildSessionFactory();
		Session session = null;
		try {
			session = factory.openSession();
			// 开启事务
			session.beginTransaction();
			StockRecord str=new StockRecord();
			Query qList = session.createQuery("from StockRecord t where t.stockCode='sh600000' and t.stockDate=to_Date('20150115','yyyy-mm-dd')");
			str=(StockRecord) qList.uniqueResult();
			System.out.println(str);
			// 提交事务
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			// 回滚事务
			session.getTransaction().rollback();
		} finally {
			if (session != null) {
				if (session.isOpen()) {
					// 关闭session
					session.close();
				}
			}
		}
	}
}
