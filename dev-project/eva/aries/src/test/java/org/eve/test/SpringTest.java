package org.eve.test;

import org.aries.config.EveConfig;
import org.springframework.config.java.context.JavaConfigApplicationContext;
import org.springframework.context.ApplicationContext;

public class SpringTest {
	public static void main(String[] args){
		ApplicationContext ctx = new JavaConfigApplicationContext(EveConfig.class);
		System.out.println(ctx.getBean("stockRecordService"));
	}
}
