<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Insert title here</title>
		
		<link rel="stylesheet" type="text/css" href="jquery-easyui-1.4/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="jquery-easyui-1.4/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="jquery-easyui-1.4/demo/demo.css">
		
		<script src="jquery-easyui-1.4/jquery.min.js" type="text/javascript"></script>
		<script src="jquery-easyui-1.4/jquery.easyui.min.js" type="text/javascript"></script>
		
		<script>
	        function submitForm(){
	            $('#ff').form('submit');
	        }
	        function clearForm(){
	            $('#ff').form('clear');
	        }
	    </script>
	</head>
	<body>
		<div class="easyui-panel" title="New Topic" style="width: 400px">
			<div style="padding: 10px 60px 20px 60px">
				<form id="ff" action="parameterConfiguration.action" method="post">
					<table cellpadding="5">
						<tr>
							<td><div class="easyui-calendar" style="width: 250px; height: 250px;"></div>
							</td>
						</tr>
					</table>
				</form>
				<div style="text-align:center;padding:5px">
		            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()">Submit</a>
		            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()">Clear</a>
		        </div>
			</div>
		</div>
		
		<!-- <div id="container" style="min-width:400px;height:400px"></div> -->
		<button id="button1">button</button>
		<div class="easyui-calendar" style="width: 250px; height: 250px;"></div>
	</body>
</html>