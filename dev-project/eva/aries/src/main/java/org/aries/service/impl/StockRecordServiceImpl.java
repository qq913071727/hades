package org.aries.service.impl;

import java.io.Serializable;
import java.util.List;

import org.aries.dao.StockRecordDao;
import org.aries.model.StockRecord;
import org.aries.service.StockRecordService;
import org.springframework.stereotype.Component;

@Component("stockRecordService")
public class StockRecordServiceImpl implements StockRecordService {
	private StockRecordDao stockRecordDao;

	public StockRecordDao getStockRecordDao() {
		return stockRecordDao;
	}

	public void setStockRecordDao(StockRecordDao stockRecordDao) {
		this.stockRecordDao = stockRecordDao;
	}

	public Serializable save(StockRecord s) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<StockRecord> findStockRecord(String hql){
		return null;
	}

}
