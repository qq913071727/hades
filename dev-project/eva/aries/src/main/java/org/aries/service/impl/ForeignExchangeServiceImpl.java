package org.aries.service.impl;

import org.springframework.stereotype.Component;

import org.aries.service.ForeignExchangeService;

import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.HttpURLConnection;

@Component("stockRecordService")
public class ForeignExchangeServiceImpl implements ForeignExchangeService {
	public void test(){
		long begintime = System.currentTimeMillis();
		URL url=null;
		HttpURLConnection urlcon=null;
		BufferedReader buffer=null;
		StringBuffer bs=null;
		String l;

		System.getProperties().setProperty("socksProxySet", "true");
		System.getProperties().setProperty("http.proxyHost", "10.1.31.133");
		System.getProperties().setProperty("http.proxyPort", "8080");

		try{
			url = new URL("http://download.finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s=AUDCNY=x");
			urlcon = (HttpURLConnection)url.openConnection();
			urlcon.connect();         //获取连接
			InputStream is = urlcon.getInputStream();
			buffer = new BufferedReader(new InputStreamReader(is));
			bs = new StringBuffer();
			l = null;
			while((l=buffer.readLine())!=null){
				bs.append(l).append("/n");
			}
		}catch(MalformedURLException e){
			e.printStackTrace();	
		}catch(IOException e){
			e.printStackTrace();	
		}
		System.out.println(bs.toString());
            
		System.out.println("总共执行时间为："+(System.currentTimeMillis()-begintime)+"毫秒");	
	}

}
