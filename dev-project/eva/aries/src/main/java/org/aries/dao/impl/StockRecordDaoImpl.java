package org.aries.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.aries.dao.StockRecordDao;
import org.aries.model.StockRecord;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("stockRecordDao")
public class StockRecordDaoImpl implements StockRecordDao {
	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Serializable save(StockRecord s) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<StockRecord> findStockRecord(String hql) {
		Session session=sessionFactory.getCurrentSession();
		List<StockRecord> stockRecordList = null;
		try {
			session.beginTransaction();
			Query query = session.createQuery("from StockRecord t where t.stockCode='sh600000' and t.stockDate=to_Date('20150115','yyyy-mm-dd')");
			stockRecordList = query.list();
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session != null) {
				if (session.isOpen()) {
					session.close();
				}
			}
		}
		return stockRecordList;
	}

}
