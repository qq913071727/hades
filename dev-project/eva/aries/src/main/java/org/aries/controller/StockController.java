package org.aries.controller;

import org.noahsark.core.action.BaseAction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import java.net.URL;
import java.net.URLConnection;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.io.IOException;

@Controller
public class StockController extends BaseAction {	
	@RequestMapping(value="/stock")
    public String index() {    	
    	try{
    		System.out.println("################");
	    	URL url = new URL("http://download.finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s=AUDUSD=x");
	    	URLConnection uc = url.openConnection();
	        InputStream in = uc.getInputStream();
	        int c;
            while ((c = in.read()) != -1){
            	System.out.print((char)c);
            }
            in.close();
		}catch(MalformedURLException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
    	
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@		welcome to stock");
		/*ForeignExchangeService fes=new ForeignExchangeServiceImpl();
		fes.test();*/
        return "home";
    }
}
