package org.aries.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ForeignExchangeController {
	@RequestMapping(value="/foreign_exchange")
    public String index() {
		System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%		welcome to foreign_exchange");
        return "foreign_exchange";
    }
}
