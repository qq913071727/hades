package org.aries.dao;

import java.io.Serializable;
import java.util.List;

import org.aries.model.StockRecord;

public interface StockRecordDao {
	Serializable save(StockRecord s);
	List<StockRecord> findStockRecord(String hql);
}
