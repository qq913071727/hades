package org.aries.service;

import java.io.Serializable;
import java.util.List;

import org.aries.model.StockRecord;

public interface StockRecordService {
	Serializable save(StockRecord s);
	List<StockRecord> findStockRecord(String hql);
}
