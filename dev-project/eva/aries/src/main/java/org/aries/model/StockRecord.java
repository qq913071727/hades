package org.aries.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class StockRecord implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date stockDate;
	private String stockCode;
	private BigDecimal stockOpen;
	private BigDecimal stockHigh;
	private BigDecimal stockClose;
	private BigDecimal stockLow;
	private BigDecimal stockAmount;
	private BigDecimal five;
	private BigDecimal ten;
	private BigDecimal twenty;
	private BigDecimal sixty;
	private BigDecimal oneHundredTwenty;
	private BigDecimal twoHundredFifty;
	private BigDecimal upDown;
	private BigDecimal ema12;
	private BigDecimal ema26;
	private BigDecimal dif;
	private BigDecimal dea;
	private BigDecimal todayUpDownPercentage;
	private BigDecimal fiveDayVolatility;
	private BigDecimal tenDayVolatility;
	private BigDecimal twentyDayVolatility;
	
	public Date getStockDate() {
		return stockDate;
	}
	public void setStockDate(Date stockDate) {
		this.stockDate = stockDate;
	}
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public BigDecimal getStockOpen() {
		return stockOpen;
	}
	public void setStockOpen(BigDecimal stockOpen) {
		this.stockOpen = stockOpen;
	}
	public BigDecimal getStockHigh() {
		return stockHigh;
	}
	public void setStockHigh(BigDecimal stockHigh) {
		this.stockHigh = stockHigh;
	}
	public BigDecimal getStockClose() {
		return stockClose;
	}
	public void setStockClose(BigDecimal stockClose) {
		this.stockClose = stockClose;
	}
	public BigDecimal getStockLow() {
		return stockLow;
	}
	public void setStockLow(BigDecimal stockLow) {
		this.stockLow = stockLow;
	}
	public BigDecimal getStockAmount() {
		return stockAmount;
	}
	public void setStockAmount(BigDecimal stockAmount) {
		this.stockAmount = stockAmount;
	}
	public BigDecimal getFive() {
		return five;
	}
	public void setFive(BigDecimal five) {
		this.five = five;
	}
	public BigDecimal getTen() {
		return ten;
	}
	public void setTen(BigDecimal ten) {
		this.ten = ten;
	}
	public BigDecimal getTwenty() {
		return twenty;
	}
	public void setTwenty(BigDecimal twenty) {
		this.twenty = twenty;
	}
	public BigDecimal getSixty() {
		return sixty;
	}
	public void setSixty(BigDecimal sixty) {
		this.sixty = sixty;
	}
	public BigDecimal getOneHundredTwenty() {
		return oneHundredTwenty;
	}
	public void setOneHundredTwenty(BigDecimal oneHundredTwenty) {
		this.oneHundredTwenty = oneHundredTwenty;
	}
	public BigDecimal getTwoHundredFifty() {
		return twoHundredFifty;
	}
	public void setTwoHundredFifty(BigDecimal twoHundredFifty) {
		this.twoHundredFifty = twoHundredFifty;
	}
	public BigDecimal getUpDown() {
		return upDown;
	}
	public void setUpDown(BigDecimal upDown) {
		this.upDown = upDown;
	}
	public BigDecimal getEma12() {
		return ema12;
	}
	public void setEma12(BigDecimal ema12) {
		this.ema12 = ema12;
	}
	public BigDecimal getEma26() {
		return ema26;
	}
	public void setEma26(BigDecimal ema26) {
		this.ema26 = ema26;
	}
	public BigDecimal getDif() {
		return dif;
	}
	public void setDif(BigDecimal dif) {
		this.dif = dif;
	}
	public BigDecimal getDea() {
		return dea;
	}
	public void setDea(BigDecimal dea) {
		this.dea = dea;
	}
	public BigDecimal getTodayUpDownPercentage() {
		return todayUpDownPercentage;
	}
	public void setTodayUpDownPercentage(BigDecimal todayUpDownPercentage) {
		this.todayUpDownPercentage = todayUpDownPercentage;
	}
	public BigDecimal getFiveDayVolatility() {
		return fiveDayVolatility;
	}
	public void setFiveDayVolatility(BigDecimal fiveDayVolatility) {
		this.fiveDayVolatility = fiveDayVolatility;
	}
	public BigDecimal getTenDayVolatility() {
		return tenDayVolatility;
	}
	public void setTenDayVolatility(BigDecimal tenDayVolatility) {
		this.tenDayVolatility = tenDayVolatility;
	}
	public BigDecimal getTwentyDayVolatility() {
		return twentyDayVolatility;
	}
	public void setTwentyDayVolatility(BigDecimal twentyDayVolatility) {
		this.twentyDayVolatility = twentyDayVolatility;
	}
}
