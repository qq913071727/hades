package org.aries.config;

import org.aries.dao.impl.StockRecordDaoImpl;
import org.aries.model.StockRecord;
import org.aries.service.impl.StockRecordServiceImpl;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EveConfig {
	
	@Bean(autowire=Autowire.BY_TYPE)
	public StockRecordDaoImpl stockRecordDao(){
		return new StockRecordDaoImpl();
	}
	
	@Bean(autowire=Autowire.BY_TYPE)
	public StockRecordServiceImpl stockRecordService(){
//		StockRecordServiceImpl srsi=new StockRecordServiceImpl();
//		srsi.setStockRecordDao(stockRecordDao());
//		return srsi;
		return new StockRecordServiceImpl();
	}
	
	@Bean(autowire=Autowire.BY_TYPE)
	public StockRecord stockRecord(){
		return new StockRecord();
	}
}
