package org.eva.hydra.redis.controller;

import org.eva.hydra.redis.domain.InterfaceMonitor;
import org.eva.hydra.redis.service.InterfaceMonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InterfaceMonitorController {
	@Autowired
	private InterfaceMonitorService interfaceMonitorService;

	@RequestMapping("/test")
	public @ResponseBody String test() {
		interfaceMonitorService.test();
		return "ok";
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping("/save")
	public @ResponseBody String save() {
		InterfaceMonitor interfaceMonitor = new InterfaceMonitor();
		interfaceMonitor.setId("1");
		interfaceMonitor.setErrorInfo("1");
		interfaceMonitor.setSystemName("1");
		interfaceMonitorService.save(interfaceMonitor);
		return "ok";
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("/delete/{id}")
	public @ResponseBody String delete(@PathVariable("id") String id) {
		InterfaceMonitor interfaceMonitor = new InterfaceMonitor();
		interfaceMonitor.setId(id);
		interfaceMonitorService.delete(interfaceMonitor);
		return "ok";
	}

	/**
	 * 将数据存储在redis中，可持久化
	 * @return
	 */
	@RequestMapping("/update/{id}")
	public @ResponseBody String update(@PathVariable("id") String id) {
		InterfaceMonitor interfaceMonitor = new InterfaceMonitor();
		interfaceMonitor.setId(id);
		interfaceMonitor.setErrorInfo("11");
		interfaceMonitor.setSystemName("11");
		interfaceMonitorService.save(interfaceMonitor);
		return "ok";
	}
	
	@RequestMapping("/find/{id}")
	public @ResponseBody String find(@PathVariable("id") String id) {
		InterfaceMonitor interfaceMonitor = interfaceMonitorService.findById(id);
		System.out.println(interfaceMonitor.getId());
		System.out.println(interfaceMonitor.getErrorInfo());
		System.out.println(interfaceMonitor.getSystemName());
		return "ok";
	}
}
