package org.eva.hydra.redis.service;

import org.eva.hydra.redis.domain.InterfaceMonitor;
import org.eva.hydra.redis.repository.InterfaceMonitorRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class InterfaceMonitorService {

	@Resource
	private InterfaceMonitorRepository interfaceMonitorRepository;

	@Resource
    private RedisTemplate<String,String> redisTemplate;

	/**
	 * 将数据存储在redis中，可持久化
	 */
    public void test(){
       ValueOperations<String,String> valueOperations = redisTemplate.opsForValue();
       valueOperations.set("mykey4", "random1="+Math.random());
       System.out.println(valueOperations.get("mykey4"));
    }

    //keyGenerator="myKeyGenerator"
    @Cacheable(value="interfaceMonitor",keyGenerator = "wiselyKeyGenerator") //缓存,这里没有指定key.
    public void save(InterfaceMonitor interfaceMonitor) {
    	interfaceMonitorRepository.save(interfaceMonitor);
    }

    @CacheEvict(value="interfaceMonitor",keyGenerator = "wiselyKeyGenerator")
    public void delete(InterfaceMonitor interfaceMonitor) {
    	interfaceMonitorRepository.delete(interfaceMonitor);
    }

	@Cacheable(value = "interfaceMonitor", keyGenerator = "wiselyKeyGenerator")
	public void update(InterfaceMonitor interfaceMonitor) {
		interfaceMonitorRepository.save(interfaceMonitor);
	}

	@Cacheable(value = "interfaceMonitor", keyGenerator = "wiselyKeyGenerator")
	public InterfaceMonitor findById(String id) {
		return interfaceMonitorRepository.findById(id);
//		return null;
	}
}
