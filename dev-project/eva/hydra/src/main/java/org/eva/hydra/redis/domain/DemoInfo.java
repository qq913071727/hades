package org.eva.hydra.redis.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;

/**
 * 测试实体类，这个随便;
 *
 * @author Angel(QQ:412887952)
 * @version v.0.1
 */
@Entity
public class DemoInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private String id;
    private String name;
    private String pwd;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DemoInfo demoInfo = (DemoInfo) o;

        if (id != null ? !id.equals(demoInfo.id) : demoInfo.id != null) return false;
        if (name != null ? !name.equals(demoInfo.name) : demoInfo.name != null) return false;
        return pwd != null ? pwd.equals(demoInfo.pwd) : demoInfo.pwd == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (pwd != null ? pwd.hashCode() : 0);
        return result;
    }

    public String getPwd() {

        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
//        return "DemoInfo [id=" + id + ", name=" + name + ", pwd=" + pwd + "]";
        return "123";
    }
}
