package org.eva.hydra.redis.controller;

import org.eva.hydra.redis.domain.DemoInfo;
import org.eva.hydra.redis.service.DemoInfoService;
import org.eva.hydra.redis.util.SerializeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import redis.clients.jedis.Jedis;

/**
 * 测试类.
 *
 * @author Angel(QQ:412887952)
 * @version v.0.1
 */
@Controller
public class DemoInfoController {
    @Autowired
    DemoInfoService demoInfoService;

    @RequestMapping("/demoinfo/test")
    public @ResponseBody String test() {
        DemoInfo loaded = demoInfoService.findById("1");
        System.out.println("loaded=" + loaded);
        DemoInfo cached = demoInfoService.findById("1");
        System.out.println("cached=" + cached);
        loaded = demoInfoService.findById("2");
        System.out.println("loaded2=" + loaded);

        Jedis jedis = new Jedis("127.0.0.1", 6379);
        byte[] data100= jedis.get(("org.eva.hydra.redis.service.DemoInfoServicefindById1").getBytes());
        DemoInfo demoInfo = (DemoInfo) SerializeUtil.unserialize(data100);
        System.out.println(String.format("person:100->id=%s,name=%s", demoInfo.getId(), demoInfo.getName()));

        return "ok";
    }

    @RequestMapping("/demoinfo/delete")
    public @ResponseBody String delete(String id) {
        demoInfoService.deleteFromCache(id);
        return "ok";
    }

    @RequestMapping("/demoinfo/test1")
    public @ResponseBody String test1() {
        demoInfoService.test();
        System.out.println("DemoInfoController.test1()");
        return "ok";
    }
}
