package org.eva.hydra.redis.repository;

import org.eva.hydra.redis.domain.InterfaceMonitor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("interfaceMonitorRepository")
public interface InterfaceMonitorRepository extends CrudRepository<InterfaceMonitor, String> {

	InterfaceMonitor findById(String id);
}
