package org.eva.hydra.redis.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class InterfaceMonitor implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;
	
	private String errorInfo;
	
	private String systemName;

	public InterfaceMonitor() {
		super();
	}

	public InterfaceMonitor(String id, String errorInfo, String systemName) {
		super();
		this.id = id;
		this.errorInfo = errorInfo;
		this.systemName = systemName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	
	@Override
	public String toString() {
		return "DemoInfo [id=" + id + ", errorInfo=" + errorInfo + ", systemName=" + systemName + "]";
	}
}
