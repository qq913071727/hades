<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
>
    <head>
        <meta content="text/html;charset=utf-8"></meta>
        <title>Hello World!</title>
        <script sec="static/js/jquery-3.2.1.js"></script>
        <link rel="stylesheet" href="static/css/bootstrap.min.css"></link>
        <script sec="static/js/bootstrap.min.js"></script>
    </head>
<body> 
<div class="container">
        <table class="table">
            <caption>${sysUser}</caption>
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>User Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>aehyok</td>
                    <td>leo</td>
                    <td>@aehyok</td>
                </tr>
                <tr>
                    <td>lynn</td>
                    <td>thl</td>
                    <td>@lynn</td>
                </tr>
                <#list imList as im>
                <tr>
                    <td>${im.id}</td>
                    <td>${im.systemName}</td>
                    <td>${im.errorInfo}</td>
                </tr>
                   </#list>
                
            </tbody>
        </table>
    </div>
</body>
</html>