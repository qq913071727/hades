package com.watertek.g3is.bdcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
//@EnableDiscoveryClient
@ImportResource(locations = {"classpath:/config/hbase-spring.xml"})
public class BdcodeApplication {

    // 自动配置Spring框架
    public static void main(String[] args) {
        SpringApplication.run(BdcodeApplication.class, args);
    }
}
