package com.watertek.g3is.bdcode.rest;

import com.alibaba.fastjson.JSON;
import com.watertek.g3is.bdcode.common.Range;
import com.watertek.g3is.bdcode.vo.ResultPojo;
import com.watertek.geosot.GCode1D;
import com.watertek.geosot.GeoRange;
import com.watertek.geosot.GeoSOT;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;

/**
 * 网格的REST接口类
 */
@RestController
@RequestMapping(value="/geoSOT-API")
public class GridController extends AbstractController {

    /**
     * 将经纬度转换为北斗网格码
     *
     * @param request
     * @param response
     * @param longitude 经度
     * @param latitude  纬度
     * @param layer     层级
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "Point2Code/{longitude}/{latitude}/{layer}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> point2Code(HttpServletRequest request, HttpServletResponse response,
                                                 @PathVariable("longitude") String longitude, @PathVariable("latitude") String latitude,
                                                 @PathVariable("layer") String layer) {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();

        GCode1D orgGCode1D = GeoSOT.getGCode1DOfPoint(Double.parseDouble(longitude), Double.parseDouble(latitude), Integer.parseInt(layer));
        BigInteger code = orgGCode1D.getCode();

        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(JSON.toJSONString(code));
        result.setMessage(ResultPojo.MSG_SUCCESS);

        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }

    /**
     * 根据北斗网格码获取矩形
     *
     * @param request
     * @param response
     * @param code     北斗网格码
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "Code2Range/{code}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> code2Range(HttpServletRequest request, HttpServletResponse response,
                                                 @PathVariable("code") String code) {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();

        GCode1D gCode1D = GeoSOT.toGCode1DFromGCode(new BigInteger(code));
        GeoRange geoRange = GeoSOT.toGeoRangeFromGCode1D(gCode1D);
        Range range = new Range();
        range.setMinLat(geoRange.getMinLat());
        range.setMaxLat(geoRange.getMaxLat());
        range.setMinLon(geoRange.getMinLon());
        range.setMaxLon(geoRange.getMaxLon());

        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(JSON.toJSONString(range));
        result.setMessage(ResultPojo.MSG_SUCCESS);

        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }

    /**
     * 验证北斗码
     *
     * @param request
     * @param response
     * @param code     北斗网格码
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "VerifyCode/{code}", method = RequestMethod.GET)
    public ResponseEntity<ResultPojo> verifyCode(HttpServletRequest request, HttpServletResponse response,
                                                 @PathVariable("code") String code) {
        // 声明返回结果集
        ResultPojo result = new ResultPojo();

        GCode1D gCode1D = GeoSOT.toGCode1DFromGCode(new BigInteger(code));
        boolean b = GeoSOT.isGCode1DExistent(gCode1D);

        // 结果集设定
        if (b) {
            result.setCode(ResultPojo.CODE_SUCCESS);
            result.setMessage(ResultPojo.MSG_SUCCESS);
        } else {
            result.setCode(ResultPojo.CODE_FAILURE);
            result.setMessage("北斗码验证失败！");
        }

        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }

}
