package com.watertek.g3is.bdcode.common;

/**
 * Envelop对象
 */
public class Envelop {
    private String code;
    private Range range;

    public Range getRange() {
        return range;
    }

    public void setRange(Range range) {
        this.range = range;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
