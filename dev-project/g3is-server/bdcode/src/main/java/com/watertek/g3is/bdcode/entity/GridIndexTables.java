package com.watertek.g3is.bdcode.entity;

/**
 * 网格索引大表
 */
public class GridIndexTables {

    /**
     * 主键
     */
    private String id;

    /**
     * 数据对象id
     */
    private String dataObjectId;

    /**
     * 数据类型
     */
    private String dataCategory;

    /**
     * 北斗网格码
     */
    private String bdCode;

    public String getBdCode() {
        return bdCode;
    }

    public void setBdCode(String bdCode) {
        this.bdCode = bdCode;
    }

    public String getDataCategory() {

        return dataCategory;
    }

    public void setDataCategory(String dataCategory) {
        this.dataCategory = dataCategory;
    }

    public String getDataObjectId() {

        return dataObjectId;
    }

    public void setDataObjectId(String dataObjectId) {
        this.dataObjectId = dataObjectId;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
