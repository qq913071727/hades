package com.watertek.g3is.bdcode.service;

public interface GridIndexTablesService {

    /**
     * 添加GridIndexTables对象
     * @param rowName
     * @param familyName
     * @param qualifier
     * @param value
     */
    void put(String rowName, String familyName, String qualifier, byte[] value);

}
