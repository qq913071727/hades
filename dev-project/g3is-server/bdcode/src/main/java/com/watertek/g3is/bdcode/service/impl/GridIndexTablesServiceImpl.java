package com.watertek.g3is.bdcode.service.impl;

import com.watertek.g3is.bdcode.dao.GridIndexTablesDao;
import com.watertek.g3is.bdcode.service.GridIndexTablesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GridIndexTablesServiceImpl extends AbstractService implements GridIndexTablesService {
    @Autowired
    private GridIndexTablesDao gridIndexTablesDao;

    /**
     * 添加GridIndexTables对象
     * @param rowName
     * @param familyName
     * @param qualifier
     * @param value
     */
    public void put(String rowName, String familyName, String qualifier, byte[] value) {
        gridIndexTablesDao.put(rowName, familyName, qualifier, value);
    }

}
