package com.watertek.g3is.bdcode.util;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class PropertiesUtil {

	/**
	 * 根据指定的配置文件名称和key，获取value值
	 * @param file
	 * @param key
	 * @return
	 */
	public static String getValue(String file, String key){
		PropertiesConfiguration config=null;
		try {
			config=new PropertiesConfiguration(file);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		return config.getProperty(key).toString();
	}
}
