package com.watertek.g3is.bdcode.dao.impl;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.BufferedMutator;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Mutation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.hadoop.hbase.HbaseTemplate;

import java.io.IOException;
import java.util.List;

/**
 * DAO抽象类，提供了通用的功能
 */
public abstract class AbstractDao {
    @Autowired
    public HbaseTemplate hbaseTemplate;

    /**
     * 插入GridIndexTables对象
     * @param rowName
     * @param familyName
     * @param qualifier
     * @param value
     */
    public void putGridIndexTables(String rowName, String familyName, String qualifier, byte[] value) {
        hbaseTemplate.put(com.watertek.g3is.bdcode.constant.TableName.GRID_INDEX_TABLES_NAME, rowName, familyName, qualifier, value);
    }

    /**
     * 根据表格名称tableName，批量加载数据到HBase
     * @param mutationList
     * @param tableName
     */
    public void batchPut(List<Mutation> mutationList, String tableName) {
        Configuration config = hbaseTemplate.getConfiguration();
        Connection connection = null;
        BufferedMutator table = null;
        try {
            connection = ConnectionFactory.createConnection(config);
            table = connection.getBufferedMutator(TableName.valueOf(tableName));
            table.mutate(mutationList);
            table.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                table.close();
                connection.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
