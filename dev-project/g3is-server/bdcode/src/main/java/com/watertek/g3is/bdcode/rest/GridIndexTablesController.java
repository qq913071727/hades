package com.watertek.g3is.bdcode.rest;

import com.watertek.g3is.bdcode.vo.ResultPojo;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 网格索引大表的controller类
 */
@RestController
@RequestMapping(value="/bdcode")
public class GridIndexTablesController extends AbstractController {

    /**
     * 测试方法
     * @param request
     * @param response
     * @return
     */
    @ResponseBody
    @RequestMapping(value="test", method= RequestMethod.GET)
    public ResponseEntity<ResultPojo> test(HttpServletRequest request, HttpServletResponse response){
        // 声明返回结果集
        ResultPojo result = new ResultPojo();

        gridIndexTablesService.put("t1", "f1", "re11", Bytes.toBytes("ree22"));

        // 结果集设定
        result.setCode(ResultPojo.CODE_SUCCESS);
        result.setResult(null);
        result.setMessage(ResultPojo.MSG_SUCCESS);

        return new ResponseEntity<ResultPojo>(result, HttpStatus.OK);
    }
}
