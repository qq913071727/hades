package com.watertek.g3is.bdcode.vo;

import java.io.Serializable;

/**
 * 封装请求Envelop的接口的参数
 */
public class EnvelopVo implements Serializable {

    private static final long serialVersionUID = 2343691076058721144L;

    private String x1;
    private long y1;
    private long x2;
    private long y2;
    private int level;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getY2() {

        return y2;
    }

    public void setY2(long y2) {
        this.y2 = y2;
    }

    public long getX2() {

        return x2;
    }

    public void setX2(long x2) {
        this.x2 = x2;
    }

    public long getY1() {

        return y1;
    }

    public void setY1(long y1) {
        this.y1 = y1;
    }

    public String getX1() {
        return x1;
    }

    public void setX1(String x1) {
        this.x1 = x1;
    }
}
