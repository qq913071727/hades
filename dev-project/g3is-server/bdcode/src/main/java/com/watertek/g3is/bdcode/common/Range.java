package com.watertek.g3is.bdcode.common;

/**
 * 封装了经度和纬度
 */
public class Range {
    private double minLon;
    private double maxLon;
    private double minLat;
    private double maxLat;

    public double getMaxLat() {
        return maxLat;
    }

    public void setMaxLat(double maxLat) {
        this.maxLat = maxLat;
    }

    public double getMinLat() {

        return minLat;
    }

    public void setMinLat(double minLat) {
        this.minLat = minLat;
    }

    public double getMaxLon() {

        return maxLon;
    }

    public void setMaxLon(double maxLon) {
        this.maxLon = maxLon;
    }

    public double getMinLon() {

        return minLon;
    }

    public void setMinLon(double minLon) {
        this.minLon = minLon;
    }
}
