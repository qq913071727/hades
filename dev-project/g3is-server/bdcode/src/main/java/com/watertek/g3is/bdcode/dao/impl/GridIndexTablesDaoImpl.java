package com.watertek.g3is.bdcode.dao.impl;

import com.watertek.g3is.bdcode.dao.GridIndexTablesDao;
import org.apache.hadoop.hbase.client.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GridIndexTablesDaoImpl extends AbstractDao implements GridIndexTablesDao {

    /**
     * 插入GridIndexTables对象
     * @param rowName
     * @param familyName
     * @param qualifer
     * @param value
     */
    public void put(String rowName, String familyName, String qualifer, byte[] value) {
        super.putGridIndexTables(rowName, familyName, qualifer, value);
    }

    /**
     * 批量加载北斗网格码
     * @param mutationList
     */
    public void batchPut(List<Mutation> mutationList) {
        super.batchPut(mutationList, com.watertek.g3is.bdcode.constant.TableName.GRID_INDEX_TABLES_NAME);
    }


}
