package com.watertek.g3is.bdcode.util;

/**
 * HBase工具类
 */
public class HBaseUtil {

    /**
     * 创建rowKey，将当前的时间戳字符串反转
     * @return
     */
    public static String generateRowKey(){
        return new StringBuilder(String.valueOf(System.currentTimeMillis())).reverse().toString();
    }
}
