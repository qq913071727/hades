package com.watertek.g3is.bdcode.rest;

import com.watertek.g3is.bdcode.service.GridIndexTablesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public abstract class AbstractController {

    @Autowired
    protected GridIndexTablesService gridIndexTablesService;

    /**
     * 初始化时，在windows环境，加载GeoSOT.dll库；在linux环境，加载libgeosot库
     */
    static {
        try {
            String os = System.getProperty("os.name");
            if(os.toLowerCase().startsWith("win")){
                System.loadLibrary("GeoSOT");
            }else{
                System.load("/opt/libgeosot-2.0.so");
            }
        } catch (UnsatisfiedLinkError e) {
            System.err.println("Native code library failed to load.\n" + e);
            System.exit(1);
        }
    }
}













