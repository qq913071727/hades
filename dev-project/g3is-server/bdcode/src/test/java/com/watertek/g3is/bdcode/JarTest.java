package com.watertek.g3is.bdcode;

import com.watertek.geosot.*;

public class JarTest {
    static {
        try {
            System.loadLibrary("GeoSOT");
        } catch (UnsatisfiedLinkError e) {
            System.err.println("Native code library failed to load.\n" + e);
            System.exit(1);
        }
    }

    public static void main(String argv[]) {
        GCode1D orgGCode1D=GeoSOT.getGCode1DOfPoint(1.1, 2.2, 12);
        GCode1D newGCode1D = GeoSOT.displaceGCode1D(orgGCode1D, 10, 10);
        System.out.println(newGCode1D.getCode());
        System.out.println(newGCode1D.getLayer());

        System.out.println("Hello");
        GeoPoint pt = new GeoPoint();
        pt.setLatitude(40);
        pt.setLongitude(120);

        int layer = 12;

        GCode2D code = GeoSOT.getGCode2DOfPoint(pt.getLongitude(), pt.getLatitude(), layer);

        System.out.println("layer:" + code.getLayer() + " Lat:" + code.getLatCode() + " Lon:" + code.getLonCode());

        GeoPoint pt2 = new GeoPoint();
        pt2.setLatitude(39);
        pt2.setLongitude(121);

        GeoPoint ps[] = {pt, pt2};

        GCode2DList list2 = GeoSOT.getGCode2DOfPolyline(ps, 2, layer);
        System.out.println(list2.getCount());
        GCode1DList list21 = GeoSOT.toGCode1DListFromGCode2DList(list2);
        System.out.println(list21.getCount());
        GCodeList list20 = GeoSOT.toGCodeListFromGCode2DList(list2);
        System.out.println(list20.getCount());
        GCodeNode nodes = list20.getList();
        while (nodes != null) {
            System.out.println("code:" + nodes.getCode());
            nodes = nodes.getNext();
        }
    }

}
