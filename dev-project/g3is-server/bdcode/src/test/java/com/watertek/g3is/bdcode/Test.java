package com.watertek.g3is.bdcode;

import com.watertek.geosot.GCode1D;
import com.watertek.geosot.GeoSOT;

import java.math.BigInteger;

public class Test {
    static {
        try {
            String os = System.getProperty("os.name");
            System.out.println(os);
            if(os.toLowerCase().startsWith("win")) {
                System.loadLibrary("GeoSOT");
            }else {
                System.load("/opt/libgeosot-2.0.so");
            }
        }catch(UnsatisfiedLinkError e) {
            System.err.println("Native code library failed to load.\n"+e);
            System.exit(1);
        }
    }
    public static void main(String[] args) {
        System.out.println(getGeoCode(118.2356,23.5258,10));
    }

    /**
     *
     * 生成二维网格码的算法
     * @param longitude
     * @param latitude
     * @param layer
     * @return
     */
    public static BigInteger getGeoCode(double longitude, double latitude, int layer) {
        GCode1D orgGCode = GeoSOT.getGCode1DOfPoint(longitude, latitude, layer);
        BigInteger code = orgGCode.getCode();
        return code;
    }
}
