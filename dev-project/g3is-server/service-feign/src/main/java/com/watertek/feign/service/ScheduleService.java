package com.watertek.feign.service;

import com.watertek.feign.service.impl.ScheduleServiceHystric;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "eureka-client",fallback = ScheduleServiceHystric.class)
public interface ScheduleService {

    @RequestMapping(value = "/hi",method = RequestMethod.POST)
    String sayHiFromClientOne(@RequestParam(value = "name") String name);
}
