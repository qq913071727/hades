package cn.tomoya.common.enums;

/**
 * Created by liuwei on 2017-3-24.
 */
public enum TopicTypeEnum{
    COMMON(1,"普通留言"),
    PROMIST(2,"公约留言"),
    LIVESHOW(3,"直播留言"),
    SUBJECT(4,"专题留言");

    public int typeValue;
    public String name;

    private TopicTypeEnum(int typeValue, String name){
        this.typeValue = typeValue;
        this.name = name;
    }

    public int getTypeValue(){
        return typeValue;
    }

    public void setTypeValue(int typeValue){
        this.typeValue = typeValue;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public static TopicTypeEnum stateOf(int index){
        for (TopicTypeEnum typeValue : values()) {
            if (typeValue.getTypeValue() == index) {
                return typeValue;
            }
        }
        return null;
    }

}
