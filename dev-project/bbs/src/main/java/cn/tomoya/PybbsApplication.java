package cn.tomoya;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RegexRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import cn.tomoya.common.config.SiteConfig;
import cn.tomoya.interceptor.CommonInterceptor;
import cn.tomoya.module.security.core.MyFilterSecurityInterceptor;
import cn.tomoya.module.security.core.MyUserDetailService;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by tomoya.
 * Copyright (c) 2016, All Rights Reserved.
 * http://tomoya.cn
 */

@EnableSwagger2
@EnableCaching

@SpringBootApplication
@EnableConfigurationProperties(SiteConfig.class)
@EnableRedisHttpSession
@ServletComponentScan
public class PybbsApplication extends WebMvcConfigurerAdapter {

    @Autowired
    private CommonInterceptor commonInterceptor;
    @Autowired
    private MyUserDetailService myUserDetailService;
    @Autowired
    private MyFilterSecurityInterceptor myFilterSecurityInterceptor;
    @Autowired
    private SiteConfig siteConfig;


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    /**
     * 添加viewController
     *
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        super.addViewControllers(registry);
        registry.addViewController("/donate").setViewName(siteConfig.getTheme() + "/donate");
        registry.addViewController("/about").setViewName(siteConfig.getTheme() + "/about");
        registry.addViewController("/apidoc").setViewName(siteConfig.getTheme() + "/api");
    }

    /**
     * 添加拦截器
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);
        registry.addInterceptor(commonInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/api/**");
    }

    /**
     * 配置权限
     */
    @Configuration
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    @Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
    class SecurityConfig extends WebSecurityConfigurerAdapter {



        @Override
        protected void configure(HttpSecurity http) throws Exception {



            // Build the request matcher for CSFR
            RequestMatcher csrfRequestMatcher = new RequestMatcher() {

                private RegexRequestMatcher requestMatcher =
                        new RegexRequestMatcher("/api/\\*\\*", null);

                @Override
                public boolean matches(HttpServletRequest request) {

                    // Enable the CSRF
                    if(requestMatcher.matches(request))
                        return true;

                    // You can add here any other rule on the request object, returning
                    // true if the CSRF must be enabled, false otherwise
                    // ....
                    // No CSRF for other requests
                    return false;
                }
            };



            http
                    .csrf()
                        .requireCsrfProtectionMatcher(csrfRequestMatcher)
                        .and()
                    .authorizeRequests()
                    /*.antMatchers("/api*//**").hasAnyRole("user","admin","banzhu")*/
                    /*.antMatchers("/api*//**").hasAnyAuthority("topic:edit","reply:delete","reply:edit","topic:delete")*/
                    .antMatchers("/static/**").permitAll()
                    .antMatchers(
                            "/admin/**",
                            /*"/topic*//**",*/
                            "/reply/save",
                            "/reply/replies/*",
                            "/replyReply/**",
                           "/reply/delete",
                            "/reply/edit",
                            "/collect/**",
                            "/notification/**",
                            "/user/setting"

                    ).authenticated().and()
                    .formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/login")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .failureUrl("/login?error")
                    .defaultSuccessUrl("/")
                    .permitAll()
                    .and()
                    .logout()
                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                    .logoutSuccessUrl("/");
            http.addFilterBefore(myFilterSecurityInterceptor, FilterSecurityInterceptor.class)/*.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class)*/;
        }

        @Autowired
        public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(myUserDetailService).passwordEncoder(new BCryptPasswordEncoder());
        }



        @Override
        @Bean
        public AuthenticationManager authenticationManager() throws Exception {
            return super.authenticationManager();
        }
    }

    class SecurityInitializer extends AbstractSecurityWebApplicationInitializer
    {
        public SecurityInitializer() {
            super(SecurityConfig.class, Config.class);
        }
    }
   /* class Initializer extends AbstractHttpSessionApplicationInitializer
    {
        public Initializer()
        {
            super(Config.class);
        }
    }*/

    public static void main(String[] args) {
        SpringApplication.run(PybbsApplication.class, args);
    }
}
