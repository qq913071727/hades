package cn.tomoya.module.security;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserVo  implements Serializable
{

    private static final long serialVersionUID = 1L;

    private String id;

    private String loginName;

    private String realName;
    private String orgname;
    private String nickname;
    @JsonIgnore
    private String password;

    private int userType;

    private String phoneNum;

    private String email;

    private String address;


    private String imgId;

    private String areaId;

    private String orgId;

    public String getLoginName() {
        return loginName;
    }

    public String getRealName() {
        return realName;
    }

    public String getPassword() {
        return password;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }



    public String getImgId() {
        return imgId;
    }

    public String getAreaId() {
        return areaId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setImgId(String imgId) {
        this.imgId = imgId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * 密码盐.
     * @return
     */
    public String getCredentialsSalt() {
        return this.getLoginName() + "8d78869f470951332959580424d4bf4f";

    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getOrgname()
    {
        return orgname;
    }

    public void setOrgname(String orgname)
    {
        this.orgname = orgname;
    }

    public String getNickname()
    {
        return nickname;
    }

    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }
}
