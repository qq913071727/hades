package cn.tomoya.module.api.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.tomoya.common.BaseController;
import cn.tomoya.exception.Result;
import cn.tomoya.module.reply.entity.Reply;
import cn.tomoya.module.reply.service.ReplyService;
import cn.tomoya.module.replyToReply.entity.ReplyReply;
import cn.tomoya.module.replyToReply.service.ReplyReplyService;
import cn.tomoya.module.security.JwtTokenUtil;
import cn.tomoya.module.security.JwtUser;
import cn.tomoya.module.topic.service.TopicService;
import cn.tomoya.module.user.entity.User;
import cn.tomoya.module.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Created by tomoya.
 * Copyright (c) 2016, All Rights Reserved.
 * http://tom.cn
 */
@RestController
@Api(tags = "发表评论", description = "APP")
@RequestMapping("/api/replyReply")
public class ReplyReplyApiController extends BaseController {

    @Autowired
    private ReplyService replyService;
    @Autowired
    private TopicService topicService;
    @Autowired
    private ReplyReplyService replyReplyService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UserService userService;
    @Autowired
    private UserDetailsService userDetailsService;



    /**
     * 保存回答
     * * @return
     */
    @ApiOperation(value = "保存评论",notes = "保存回答的评论内容",httpMethod = "POST")
    @RequestMapping(value = "/save" ,method = RequestMethod.POST,produces = "application/json;charset=utf-8")
    public Result save(HttpServletRequest request, HttpServletResponse response,
            @ApiParam(value = "提交条件对象replyId:Integer,content:String,token:String", required = true) @RequestBody JSONObject replyvo) {
        Result result = new Result();
        Integer  replyId =(Integer) replyvo.get("replyId");
        String content = (String)replyvo.get("content");
        String token =(String) replyvo.get("token");
        JwtUser jwtUsr = jwtTokenUtil.getUserFromToken(token);
        String tokenUserName = jwtUsr.getUsername();
        String errors = null;
        if(tokenUserName== null || "".equals(tokenUserName)){
//            跳转登录
            /*return render("/login");*/

            errors="没有登录";
            return result.error(201,errors);
        }
        if (replyId != null) {
            Reply reply = replyService.findById(replyId);
            if (reply != null) {
//                User user = getUser();
                User user=userService.findByUsername(tokenUserName);
                ReplyReply replyReply = new ReplyReply();
                replyReply.setUser(user);
                replyReply.setReply(reply);
                replyReply.setInTime(new Date());
                replyReply.setContent(content);
                replyReply.setTopic(reply.getTopic());
                replyReplyService.save(replyReply);
                //回复+1
                reply.setReplyReplyCount(reply.getReplyReplyCount() + 1);
                replyService.save(reply);

               result.setDetail(replyReply);
            }
        }
        return result;
    }


    @ApiOperation(value = "回答的评论页面",notes = "根据回答的id查询评论" ,httpMethod = "GET")
    @RequestMapping(value = "/{replyId}/{size}/{p}",method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    public Result replyReplies(HttpServletRequest request,@PathVariable("replyId")@ApiParam(value = "回答的id") Integer replyId,@PathVariable("size")@ApiParam(value = "每页条数") Integer size,@PathVariable("p")@ApiParam(value = "当前页数") Integer p){

        Page<ReplyReply> page =replyReplyService.findByReplyId(p == null ? 1 : p,size,replyId);
        Result result = new Result();
        String authorization = request.getHeader("Authorization");
        if(authorization!=null){
            JwtUser jwtUsr = jwtTokenUtil.getUserFromToken(authorization);
            String tokenUserName = jwtUsr.getUsername();
            UserDetails userDetails = this.userDetailsService.loadUserByUsername(tokenUserName);
            User user = userService.findByUsername(tokenUserName);
            String username = user.getUsername();
            if(username.equals(tokenUserName)){
                return result.success(page);
            } else{
                return result.error(400,"没有权限");
            }

        }else{
            return result.error(400,"没有权限");
        }


    }
}
