package cn.tomoya.module.replyToReply.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.tomoya.module.replyToReply.dao.ReplyReplyDao;
import cn.tomoya.module.replyToReply.entity.ReplyReply;

/**
 * Created by liuwei on 2017-1-13.
 */
@Service
@Transactional
public class ReplyReplyService
{
    @Autowired
    private ReplyReplyDao replyReplyDao ;

    public void save(ReplyReply replyReply) {
        replyReplyDao.save(replyReply);
    }

    /**
     * 查询评论
     * @param p
     * @param size
     * @return
     */
    public Page<ReplyReply> findByReplyId(int p ,int size,int replyId){
        Sort sort  = new Sort(new Sort.Order(Sort.Direction.DESC, "inTime"));
        Pageable pageable = new PageRequest(p - 1, size,sort);
        return replyReplyDao.findByReplyId(replyId,pageable);

    }

    public List<ReplyReply> findByUserIdAndInTimeLike(int userId,String inTime){

        return  replyReplyDao.findByUserAndInTimeLike(userId,inTime);


    }

    /**
     * 根据话题删除回复
     *
     * @param topicId
     */
    public void deleteByTopic(int topicId) {
        replyReplyDao.deleteByTopicId(topicId);
    }
}
