package cn.tomoya.module.security;

import java.util.Collection;
import java.util.Date;

import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * Created by stephan on 20.03.16.
 */
public class JwtUser
{

    private static final long serialVersionUID = 5325384096141053834L;
    private String id;
    private String username;
    private String password;
    private int userType;

    private String areaId;
    private String areaName;
    private String areaAdminCode;
    private String areaCode;
    private String areaLvl;

    private String orgId;
    private String orgBaseId;
    private String orgName;
    private String orgSysType;
    private String orgType;

    /**
     * 新增昵称
     */
    private String nickName;


    private Collection<? extends GrantedAuthority> authorities;
    private boolean enabled;
    private Date lastPasswordResetDate;

    public JwtUser() {
    }

    public JwtUser(UserVo user) {
        this.id = user.getId();
        this.username = user.getLoginName();
        this.password = user.getPassword();
        this.orgId = user.getOrgId();
        this.areaId = user.getAreaId();
        this.orgName=user.getOrgname();
        this.nickName=user.getNickname();

    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public boolean isEnabled() {
        return enabled;
    }

    @JsonIgnore
    public Date getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaAdminCode() {
        return areaAdminCode;
    }

    public void setAreaAdminCode(String areaAdminCode) {
        this.areaAdminCode = areaAdminCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setLastPasswordResetDate(Date lastPasswordResetDate) {
        this.lastPasswordResetDate = lastPasswordResetDate;
    }

    public String getAreaLvl() {
        return areaLvl;
    }

    public void setAreaLvl(String areaLvl) {
        this.areaLvl = areaLvl;
    }

    public String getOrgSysType() {
        return orgSysType;
    }

    public void setOrgSysType(String orgSysType) {
        this.orgSysType = orgSysType;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public String getOrgBaseId() {
        return orgBaseId;
    }

    public void setOrgBaseId(String orgBaseId) {
        this.orgBaseId = orgBaseId;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getNickName()
    {
        return nickName;
    }

    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }
}
