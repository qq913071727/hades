package cn.tomoya.module.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cn.tomoya.module.user.entity.User;

/**
 * Created by tomoya.
 * Copyright (c) 2016, All Rights Reserved.
 * http://tomoya.cn
 */
@Repository
//@CacheConfig(cacheNames = "users")
public interface UserDao extends JpaRepository<User, Integer> {

//    @Cacheable
    User findOne(int id);

//    @Cacheable
    User findByUsername(String username);

//    @CacheEvict
    void delete(int id);

}
