package cn.tomoya.module.replyToReply.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.tomoya.common.BaseController;
import cn.tomoya.common.config.SiteConfig;
import cn.tomoya.exception.ApiException;
import cn.tomoya.module.reply.entity.Reply;
import cn.tomoya.module.reply.service.ReplyService;
import cn.tomoya.module.replyToReply.entity.ReplyReply;
import cn.tomoya.module.replyToReply.service.ReplyReplyService;
import cn.tomoya.module.user.entity.User;

/**
 * Created by liuwei on 2017-1-13.
 */
@Controller
@RestController
@RequestMapping("/replyReply")
public class ReplyReplyController extends BaseController{

    @Autowired
    private ReplyService replyService;
    @Autowired
    private ReplyReplyService replyReplyService;
    @Autowired
    private SiteConfig siteConfig;
    /**
     * 保存回复
     *
     * @param replyId
     * @param content
     * @return
     */
    @ResponseBody
    @PostMapping("/save")
    public Map<String ,Object> save(Integer replyId, String content, HttpServletResponse response) {
        Map<String, Object> result = new HashMap<String,Object>();
        if (replyId != null) {
            Reply reply = replyService.findById(replyId);
            if (reply != null) {
                User user = getUser();

                ReplyReply replyReply = new ReplyReply();

                replyReply.setUser(user);
                replyReply.setReply(reply);
                replyReply.setInTime(new Date());
                replyReply.setContent(content);
                replyReply.setTopic(reply.getTopic());
                replyReplyService.save(replyReply);

                //回复+1
                reply.setReplyReplyCount(reply.getReplyReplyCount() + 1);
                replyService.save(reply);

                Page<ReplyReply> page =replyReplyService.findByReplyId(1 ,10,replyId);

                if(null==page){
                    result.put("status",400);
                }else{
                    result.put("pages",page.getTotalPages());
                    result.put("status",200);
                }
//        return render("/components/reply_to_reply.ftl");
                return result;
            }
        }
        return result;
    }



    /**
     * 查询一个小时之内评论次数
     * @param replyId
     * @param content
     * @param response
     * @return
     */
    @ResponseBody
    @PostMapping("/findTimes")
    public Map<String ,Object> findTimes(Integer replyId, String content, HttpServletResponse response) {
        Map<String, Object> result = new HashMap<String,Object>();
        User user = getUser();
        int userId = user.getId();
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String formatDate = sdf.format(date);
        ReplyReply res = new ReplyReply();

        List<ReplyReply> replyReplyList = replyReplyService.findByUserIdAndInTimeLike(userId, formatDate);
        if(replyReplyList.size()>10){
            result.put("status",400);
        }else {
            result.put("pages",replyReplyList);
            result.put("status",200);
        }

               /* if(null==page){
                    result.put("status",400);
                }else{
                    result.put("pages",page.getTotalPages());
                    result.put("status",200);
                }*/
//        return render("/components/reply_to_reply.ftl");



        return result;
    }





    @ResponseBody
    @GetMapping("/reply/{replyId}")
    public Map<String ,Object> replyReplies(@PathVariable Integer replyId,Integer p,Model model)throws
            ApiException  {

        Page<ReplyReply> page =replyReplyService.findByReplyId(p == null ? 1 : p,10,replyId);
        Map<String, Object> result = new HashMap<String,Object>();
        model.addAttribute("reply_to_reply",page.getContent());
        if(null==page){
            result.put("status",400);
        }else{
            result.put("data",page);
            result.put("status",200);
        }
//        return render("/components/reply_to_reply.ftl");
        return result;


    }






}
