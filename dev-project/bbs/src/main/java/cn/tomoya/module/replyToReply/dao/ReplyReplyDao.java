package cn.tomoya.module.replyToReply.dao;

import java.util.List;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.tomoya.module.replyToReply.entity.ReplyReply;
import cn.tomoya.module.user.entity.User;

/**
 * Created by liuwei on 2017-1-13.
 */
@Repository
@CacheConfig(cacheNames = "replyreplies")
public interface ReplyReplyDao extends JpaRepository<ReplyReply, Integer>
{

    /*@Cacheable*/
    public Page<ReplyReply> findByReplyId(int replyId,Pageable pageable);

   /* @Cacheable*/
    @Query(value = "select t FROM ReplyReply t where t.user.id = :userId and t.inTime like CONCAT(:inTime,'%')")
    List<ReplyReply> findByUserAndInTimeLike(@Param("userId") int userId,@Param("inTime")String inTime);
    void deleteByTopicId(int topicId);

}
