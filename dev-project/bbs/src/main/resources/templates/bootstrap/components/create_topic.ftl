<#macro create_topic>

    <#--<#if _roles?seq_contains("admin:index") >-->
    <#if user?? && _roles?seq_contains("admin:index")>

        <a  id="tanchu" class="btn btn-info btn-liuyan col-md-2 col-md-offset-1" data-toggle="modal" data-target="#myModal2" >
            <span class="glyphicon glyphicon-pencil"></span>&nbsp;留言
        </a>

    <#else >
    <a  id="tanchu1" class="btn btn-info btn-liuyan col-md-2 col-md-offset-1" data-toggle="modal" data-target="#myModal1" >
        <span class="glyphicon glyphicon-pencil"></span>&nbsp;留言
    </a>

    </#if>
<div style="display:none;">
    <a  id="tanchu11" class="btn btn-info btn-liuyan col-md-2 col-md-offset-1" data-toggle="modal" data-target="#myModal1" >
        <span class="glyphicon glyphicon-pencil"></span>&nbsp;留言
    </a>
</div>



    <!-- 点击添加模态框（Modal） -->
<div class="modal fade"  id="myModal1"  tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog " >
        <div class="modal-content"  >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">添加留言</h4>
            </div>
        <div class="modal-body">
        <div class="row">
            <form method="post" action="/topic/save" id="topicForm">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <div class="form-group">

                    <input type="text" class="form-control" id="title" name="title" style="margin-left:0px;float: none;border: 1px solid #ccc;" placeholder="写下你的留言标题…">
                </div>
                <div class="form-group">
                    <h5 for="title">留言内容（可选）：</h5>
                    <textarea name="content" id="content" rows="10"  style="height: 250px;" class="form-control" placeholder="描述留言细节和具体讨论内容"></textarea>
                </div>
              <#--  <div class="form-group">
                    <label for="title">版块</label>
                    <select name="tab" id="tab" class="form-control">
                        <#list sections as section>
                            <option value="${section}">${section}</option>
                        </#list>
                    </select>
                </div>-->
                <#--<button type="button" onclick="publishTopic();" class="btn btn-default">发布</button>-->
                <span id="error_message"></span>
            </form>

            </div>
            </div>
                <div class="modal-footer bg-default">
                    <input type="hidden" name="sessionId" id="sessionId" />
                    <button type="button" class="btn btn-primary mright3 mt-10" data-dismiss="modal" onclick="goTopic();">取消</button>
                    <#--<button type="button"  class="btn btn-primary mright3 mt-10" onclick="submitToCheck(this);">确定</button>-->
                    <button type="button" class="btn btn-primary mright3 mt-10"  onclick="publishTopic();"  <#if user??> <#else >disabled="disabled"</#if> >确定</button>
                </div>
                <!-- </form> -->

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->








<!-- 点击添加留言方式模态框（Modal） -->
<div class="modal fade"  id="myModal2"  tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog " >
        <div class="modal-content"  >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">添加方式</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <p>请选择以下三种留言方式的一种：</p>
                    <label><input name="topicWay" type="radio" value="1" />普通留言 </label><br/>
                    <label><input name="topicWay" type="radio" value="2" />展示留言 </label><br/>
                    <label><input name="topicWay" type="radio" value="3" />直播留言 </label><br/>
                    <label><input name="topicWay" type="radio" value="4" />专题留言 </label><br/>
                </div>
            </div>
            <div class="modal-footer bg-default">
                <input type="hidden" name="sessionId" id="sessionId" />
                <button type="button" class="btn btn-primary mright3 mt-10" data-dismiss="modal" onclick="goTopic();">取消</button>
            <#--<button type="button"  class="btn btn-primary mright3 mt-10" onclick="submitToCheck(this);">确定</button>-->
                <button type="button" class="btn btn-primary mright3 mt-10"  onclick="publishWay();" >确定</button>
            </div>
            <!-- </form> -->

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- 点击添加展示留言模态框（Modal） -->
<div class="modal fade"  id="myModal3"  tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog " >
        <div class="modal-content"  >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">添加展示留言</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form method="post" action="/topic/promise/save" id="topicForm2">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <div class="form-group">

                            <input type="text" class="form-control" id="title2" name="title" style="margin-left:0px;float: none;border: 1px solid #ccc;" placeholder="写下你的留言标题…">
                        </div>
                        <div class="form-group">
                            <h5 for="title">留言内容（可选）：</h5>
                            <textarea name="content" id="content2" rows="10"  style="height: 250px;"  class="form-control" placeholder="描述留言细节和具体讨论内容"></textarea>
                        </div>
                        <span style="color: red;" id="error_message2"></span>
                    </form>

                </div>
            </div>
            <div class="modal-footer bg-default">
                <input type="hidden" name="sessionId" id="sessionId" />
                <button type="button" class="btn btn-primary mright3 mt-10" data-dismiss="modal" onclick="goTopic();">取消</button>
            <#--<button type="button"  class="btn btn-primary mright3 mt-10" onclick="submitToCheck(this);">确定</button>-->
                <button type="button" class="btn btn-primary mright3 mt-10"  onclick="promiseTopic();" >确定</button>
            </div>
            <!-- </form> -->

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- 点击添加直播留言模态框（Modal） -->
<div class="modal fade"  id="myModal4"  tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog " >
        <div class="modal-content"  >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">添加直播留言</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form method="post" action="/topic/liveshow/save" id="topicForm3">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <div class="form-group">

                            <input type="text" class="form-control" id="title3" name="title" style="margin-left:0px;float: none;border: 1px solid #ccc;" placeholder="写下你的留言标题…">
                        </div>
                        <div class="form-group">
                            <h5 for="title">留言内容（可选）：</h5>
                            <textarea name="content" id="content3" rows="10" style="height: 250px;"  class="form-control" placeholder="描述留言细节和具体讨论内容"></textarea>
                        </div>
                        <span style="color: red;" id="error_message3"></span>
                    </form>

                </div>
            </div>
            <div class="modal-footer bg-default">
                <input type="hidden" name="sessionId" id="sessionId" />
                <button type="button" class="btn btn-primary mright3 mt-10" data-dismiss="modal" onclick="goTopic();">取消</button>
            <#--<button type="button"  class="btn btn-primary mright3 mt-10" onclick="submitToCheck(this);">确定</button>-->
                <button type="button" class="btn btn-primary mright3 mt-10"  onclick="liveShowTopic();" >确定</button>
            </div>
            <!-- </form> -->

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- 主题留言模态框（Modal） -->
<div class="modal fade"  id="myModal5"  tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog " >
        <div class="modal-content"  >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">关闭</span></button>
                <h4 class="modal-title">添加主题留言</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form method="post" action="/topic/subject/save" id="topicForm4">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <div class="form-group">

                            <input type="text" class="form-control" id="title4" name="title" style="margin-left:0px;float: none;border: 1px solid #ccc;" placeholder="写下你的留言标题…">
                        </div>
                        <div class="form-group">
                            <h5 for="title">留言内容（可选）：</h5>
                            <textarea name="content" id="content4" rows="10"  style="height: 250px;" class="form-control" placeholder="描述留言细节和具体讨论内容"></textarea>
                        </div>
                        <span style="color: red;" id="error_message4"></span>
                    </form>

                </div>
            </div>
            <div class="modal-footer bg-default">
                <input type="hidden" name="sessionId" id="sessionId" />
                <button type="button" class="btn btn-primary mright3 mt-10" data-dismiss="modal" onclick="goTopic();">取消</button>
            <#--<button type="button"  class="btn btn-primary mright3 mt-10" onclick="submitToCheck(this);">确定</button>-->
                <button type="button" class="btn btn-primary mright3 mt-10"  onclick="subjectShowTopic();" >确定</button>
            </div>
            <!-- </form> -->

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->





<#--<link rel="stylesheet" href="/static/bootstrap/libs/editor/editor.css"/>
<script type="text/javascript" src="/static/bootstrap/libs/webuploader/webuploader.withoutimage.js"></script>
<script type="text/javascript" src="/static/bootstrap/libs/markdownit.js"></script>
<script type="text/javascript" src="/static/bootstrap/libs/editor/editor.js"></script>-->
<#--<script type="text/javascript" src="/static/bootstrap/libs/editor/ext.js"></script>-->
<link rel="stylesheet" href="/static/bootstrap/css/wangEditor.min.css" />
<script type="text/javascript" src="/static/bootstrap/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="/static/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/static/bootstrap/js/wangEditor.min.js"></script>


<script type="text/javascript">

        //模态框垂直居中
        function centerModals(){
            $('.modal').each(function(i){
                var $clone = $(this).clone().css('display', 'block').appendTo('body');    var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
                top = top > 0 ? top : 0;
                $clone.remove();
                $(this).find('.modal-content').css("margin-top", top);
            });
        }
        $('.modal').on('show.bs.modal', centerModals);
        $(window).on('resize', centerModals);
       /* var editor = new Editor({element: $("#content")[0], status: []});
        editor.render();
        var editor2 = new Editor({element: $("#content2")[0], status: []});
        editor2.render();
        var editor3 = new Editor({element: $("#content3")[0], status: []});
        editor3.render();
        var editor4 = new Editor({element: $("#content4")[0], status: []});
        editor4.render();*/

        function publishWay(){
            var wayValue = $("input[type='radio']:checked").val();
            if(wayValue==1){
                $('#myModal2').modal('hide');
                $('#myModal1').modal('show');
            }
            if(wayValue==2){
                $('#myModal2').modal('hide');
                $('#myModal3').modal('show');
            }
            if(wayValue==3){
                $('#myModal2').modal('hide');
                $('#myModal4').modal('show');
            }
            if(wayValue==4){
                $('#myModal2').modal('hide');
                $('#myModal5').modal('show');
            }


        }

        var editor = new wangEditor('content');
        var editor2 = new wangEditor('content2');
        var editor3 = new wangEditor('content3');
        var editor4 = new wangEditor('content4');
        editor.config.uploadImgUrl = '/upload';


        // 普通的自定义菜单
        editor.config.menus = [

            'bold',
            'underline',
            'italic',
            'strikethrough',
            'eraser',
           /* 'forecolor',
            'bgcolor',*/
            '|',
            'quote',
            /*'fontfamily',
            'fontsize',*/
            'head',
            'unorderlist',
            'orderlist'

            /*'link',
            'unlink',
            'table',
            '|',
            'img',
            'insertcode'*/

        ];
        editor.create();
        // 普通的自定义菜单
        editor2.config.menus = [

            'bold',
            'underline',
            'italic',
            'strikethrough',
            'eraser',
            /* 'forecolor',
             'bgcolor',*/
            '|',
            'quote',
            /*'fontfamily',
            'fontsize',*/
            'head',
            'unorderlist',
            'orderlist'

            /*'link',
            'unlink',
            'table',
            '|',
            'img',
            'insertcode'*/
        ];
        editor2.create();
        // 普通的自定义菜单
        editor3.config.menus = [

            'bold',
            'underline',
            'italic',
            'strikethrough',
            'eraser',
            /* 'forecolor',
             'bgcolor',*/
            '|',
            'quote',
            /*'fontfamily',
            'fontsize',*/
            'head',
            'unorderlist',
            'orderlist'

            /*'link',
            'unlink',
            'table',
            '|',
            'img',
            'insertcode'*/
        ];
        editor3.create();
        // 普通的自定义菜单
        editor4.config.menus = [

            'bold',
            'underline',
            'italic',
            'strikethrough',
            'eraser',
            /* 'forecolor',
             'bgcolor',*/
            '|',
            'quote',
            /*'fontfamily',
            'fontsize',*/
            'head',
            'unorderlist',
            'orderlist'

            /*'link',
            'unlink',
            'table',
            '|',
            'img',
            'insertcode'*/
        ];
        editor4.create();
    </script>
</p>

</#macro>