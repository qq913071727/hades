<#macro searchresult topics>
<#--<div class="liuyan-title clearfix">
    <span class="glyphicon glyphicon-list-alt"></span>留言搜索
</div>-->

    <#list topics as topic>
    <div class="media">
        <!--<div class="media-left">
      <a href="/user/${topic.user.username!}"><img src="${topic.user.avatar}" class="avatar" alt=""></a>
    </div>-->
        <div class="media-body">
            <div class="title">
                <#--<a href="/topic/${topic.id!}">${topic.title!}</a>-->
                <a href="/topic/${topic.id!}">${topic.lightTitle(q, topic.title)!}</a>
                <p class="gray">${topic.formatDate2China(topic.inTime)}</p>
            </div>
            <div class="media-info">
                <p class="media-user"><#--<a href="/user/${topic.user.username!}">-->
                <#if (topic.user.nickname)?? && (topic.user.nickname) !="">
                ${topic.user.nickname!}
                <#else>
                ${topic.user.orgname!}
                </#if><#--</a>--></p>
                <p class="media-content">
                    <span id="reply${topic.id!}"></span>
                    <script type="text/javascript">
                        (function () {
                            $.ajax({
                                url: "/reply/replies/"+${topic.id!},
                                async: false,
                                cache: false,
                                type: "GET",
                                dataType: "json",
                                success: function (data) {
                                    if(data.status == 200 ) {
                                        $("#reply${topic.id!}").html(data.data[0].content);
                                    }
                                }
                            })
                        })();
                    </script>
                </p>
                <p class="gray">
                    <span class="hidden-sm hidden-xs">${topic.replyCount!0}个回答</span>
                <#--<span class="hidden-sm hidden-xs">•</span>
                  <span class="hidden-sm hidden-xs">${topic.view!0}次浏览</span>-->
                </p>
            </div>
        </div>
    </div>
        <#--<#if topic_has_next>
        <div class="divide mar-top-5"></div>
        </#if>-->
    </#list>

<#--<script type="text/javascript">
    layer.confirm('搜索不到相关留言问题，请您留言！', {
        btn: ['取消','留言'] //按钮
    }, function(){
        window.location.href="/";
    }, function(){
        $("#tanchu").trigger("click");
    });
</script>-->

</#macro>