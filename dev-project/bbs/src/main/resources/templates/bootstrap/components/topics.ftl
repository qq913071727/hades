<#macro topics>
  <div class="liuyan-title clearfix">
    <span class="glyphicon glyphicon-list-alt"></span> 留言动态
    <select id="myTab" onchange="tabChange();">
          <option  value="1">默认排序</option>
          <option  value="2">时间排序</option>
      </select>
      <script type="text/JavaScript">
          (function () {
              var tabSelect=${tab};
              if(tabSelect=="2"){
                  $("#myTab option[value='2']").attr("selected","selected");
              }
          })();
          function tabChange(){
              var objS = document.getElementById("myTab");
              var grade = objS.options[objS.selectedIndex].value;
              if(grade==2){
                  window.location.href="/?tab="+grade;
              }else{
                  window.location.href="/?tab=1";
              }
          }
      </script>


  </div>
  <#list page.getContent() as topic>
    <div class="media">
      <!--<div class="media-left">
      <a href="/user/${topic.user.username!}"><img src="${topic.user.avatar}" class="avatar" alt=""></a>
    </div>-->
      <div class="media-body" style="display: block;width: 100%">
        <div class="title">
          <a class="col-md-8" href="/topic/${topic.id!}">${topic.title!}</a>
          <p class="gray col-md-3">${topic.formatDate2China(topic.inTime)}</p>
        </div>
        <div class="media-info">
          <p class="media-user"><#--<a href="/user/${topic.user.username!}">-->
                <#if (topic.user.nickname)?? && (topic.user.nickname) !="">
                  ${topic.user.nickname!}
                <#else>
                  ${topic.user.orgname!}
                </#if>
<#--</a>--></p>
          <p <#--class="media-content"-->>
            <span id="reply${topic.id!}"></span>
            <script type="text/javascript">
            (function () {
                $.ajax({
                    url: "/reply/replies/"+${topic.id!},
                    async: false,
                    cache: false,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        if(data.status == 200 ) {
                            $("#reply${topic.id!}").html(data.data[0].content);
                        }

                    }
                })
            })();
          </script>
          </p>
          <p class="gray">
            <span class="hidden-sm hidden-xs">${topic.replyCount!0}个回答</span>
            <#--<span class="hidden-sm hidden-xs">•</span>
              <span class="hidden-sm hidden-xs">${topic.view!0}次浏览</span>-->
          </p>
        </div>
      </div>
    </div>
    <#if topic_has_next>
      <div class="divide mar-top-5"></div>
    </#if>
  </#list>
</#macro>