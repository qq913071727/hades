<#macro reply_to_reply>


    <#list reply_to_reply as replyToReply>
    <div class="media" id="replyToReply${replyToReply.id}">
        <div class="media-left">
            <#--<a href="/user/${replyToReply.user.username}"><img src="${replyToReply.user.avatar}" class="avatar" alt=""/></a>-->
        </div>
        <div class="media-body reply-content">
            <div class="media-heading gray">

            ${reply.formatDate(replyToReply.inTime)}
x
            </div>
            <p>
            ${reply.marked(replyToReply.content)}
            </p>
        </div>
    </div>

        <#if reply_has_next>
        <div class="divide mar-top-5"></div>
        </#if>
    </#list>
</#macro>