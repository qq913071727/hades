<#macro reply replies  >
  <#list replies as reply>
  <div class="media" id="reply${reply.id}">
    <!--<div class="media-left">
      <a href="/user/${reply.user.username}"> </a>
    </div>-->
    <div class="media-body reply-content">
      <div class="media-heading gray">
        <#--<a href="/user/${reply.user.username!}">--><#--${reply.user.username!}--> <#--</a>-->
         <#if (reply.user.nickname)?? && (reply.user.nickname) !="">
        ${reply.user.nickname!}
        <#else>
        ${reply.user.orgname!}
        </#if>



      </div>
            <!--<div id="userinfo" style="display: none">
                <div id="replyTOreply" >

               </div>

            </div>
         <div id="example"></div>-->
      <p>
      ${reply.marked(reply.content)}
      </p>
        <p style="color: #999999" >发布于 ${reply.formatDate(reply.inTime)}&nbsp;&nbsp;&nbsp;&nbsp;
            <#if user??>
                <a class="pinglun-btn" style="font-size: 13px" id="replythisReply${reply.id!}"  href="javascript:replythisReply('${reply.id!}');">${reply.replyReplyCount!}&nbsp;条评论  </a>
                <#--<#if (_roles?seq_contains("reply:edit")) && (user.username=reply.user.username) ||(_roles?seq_contains("permission:add"))>
                    <a href="/reply/${reply.id}/edit">编辑</a>
                </#if>-->
                <#if (_roles?seq_contains("reply:delete") ) && (user.username=reply.user.username)||(_roles?seq_contains("permission:add"))>
                    <a href="javascript:if(confirm('确定要删除吗？'))location.href='/reply/${reply.id!}/delete'">删除</a>
                </#if>
            </#if>

        </p>
        <div class="commit commit${reply.id}" >
            <div class="replyTOreply" id="replyTOreply${reply.id}"> </div>
            <div class="example" id="example${reply.id}"></div>
            <div class="conmment" id="conmment${reply.id}"  >

                <div class="panel-body">
                    <#--<form action="/replyReply/save" method="post" id="replyForm${reply.id}">-->
                        <form  id="replyForm${reply.id}">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <input type="hidden" value="${reply.id}" name="replyId"/>
                        <div class="form-group">
                            <textarea name="content" id="content${reply.id}" rows="3" class="form-control"></textarea>
                        </div>
                        <button type="button" onclick="replyReplySubmit(${reply.id})" class="btn btn-default">评&nbsp;&nbsp;论</button>
                        <span id="error_message${reply.id}" style="color:red;"></span>
                    </form>
                </div>
            </div>
        </div>

    </div>
  </div>


  <script type="text/javascript">



  </script>


    <#if reply_has_next>
    <div class="divide mar-top-5"></div>
    </#if>
  </#list>

</#macro>