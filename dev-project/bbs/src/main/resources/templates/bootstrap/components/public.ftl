<#macro public>
<div class="panel panel-default gonggao">
    <div class="panel-heading">最新公告</div>
    <div class="panel-body text-center">
        <ul>
            <#list publishTopicList as topic>
               <li style="position: relative ;padding-bottom: 14px;">
                   <#if topic.tab=="2" >
                      <#if topic.publishTopic(topic.inTime)> <img style="width: 28px;margin-right: 4px" src="../../../static/images/avatar/u125.png"></#if><a href="/topic/promise/${topic.id}">${topic.title}</a>
                    </#if>
                   <#if topic.tab=="3" >
                       <#if topic.publishTopic(topic.inTime)> <img style="width: 28px;margin-right: 4px"  src="../../../static/images/avatar/u125.png"></#if> <a href="/topic/liveshow/${topic.id}">${topic.title}</a>
                   </#if>
                   <#if topic.tab=="4" >
                       <#if topic.publishTopic(topic.inTime)> <img style="width: 28px;margin-right: 4px"  src="../../../static/images/avatar/u125.png"> </#if><a href="/topic/${topic.id}">${topic.title}</a>
                   </#if>
                   <span class="gray" style="font-size: 10px;position: absolute;bottom: -3px;right: 0;">${topic.formatDate2China(topic.inTime)}</span>
               </li>

            </#list>

            <li><a href="http://jgbzy.conac.cn/#/function" target="_blank" >查询共享资料，请点此跳转到资料共享信息系统中查询</a>
            </li>
            
            <li>若有问题，请拨打电话010-69001467进入询问
            </li>
            
            <li>提示：如用360浏览器访问此网站建议使用极速模式
            </li>
        </ul>

    </div>
</div>
</#macro>