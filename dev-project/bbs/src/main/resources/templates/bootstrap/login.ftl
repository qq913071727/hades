<#include "./common/layout.ftl">
<@html page_title="登录" page_tab="login">
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="panel panel-default">
      <div class="panel-heading">
        <a href="/">主页</a> / 登录
      </div>
      <div class="panel-body">
        <#if SPRING_SECURITY_LAST_EXCEPTION?? >
         <#-- <div class="alert alert-danger">用户名或密码错误</div>-->
         <div class="alert alert-danger">用户名或密码或验证码错误</div>
          <#--  <div class="alert alert-danger">${SPRING_SECURITY_LAST_EXCEPTION.message}</div>-->




        </#if>
        <#if s?? && s == "reg">
          <div class="alert alert-success">注册成功，快快登录吧！</div>
        </#if>
        <form role="form" id="loginForm" action="/login" method="post">
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <#--<input type="text" name="${piccode!}" id="codeHidden" value="${piccode!}"/>-->
          <div class="form-group">
            <label for="username">用户名</label>
            <input type="text" class="form-control" id="username" name="username" placeholder="用户名">
          </div>
          <div class="form-group">
            <label for="password">密码</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="密码">
          </div>
           <div class="form-group">
                <label for="imagecodeText">验证码:</label><img alt="验证码" id="imagecode" src="/login/imgCode"/><a href="javascript:reloadedCode();">换一张</a><br>
                <input type="text" class="form-control" id="imagecodeText" name="imagecodeText" placeholder="验证码"/>

            </div>
       <#--   <button &lt;#&ndash;type="submit"&ndash;&gt; onclick="readyImageCode();"  class="btn btn-default">登录</button>-->
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    function reloadedCode() {
        var time = new Date().getTime(); //防止由于缓存而不更新图片
        document.getElementById("imagecode").src="/login/imgCode?d="+time;
    }
    /*$(document).ready(function(){
        document.getElementById("imagecode").src="/login/imgCode";
    });*/
    function readyImageCode(){
        var imagecodeText = $("#imagecodeText").val();
        if(imagecodeText.length ==0 || imagecodeText==null){
            alert("请输入验证码！");
            return false;
        }else{
            document.getElementById("loginForm").submit();
        }
    }
</script>
</@html>