<#include "common/layout.ftl"/>
<@html page_tab="api" page_title="出错了~~">
<div class="row">
  <div class="col-md-9">
    <div class="panel panel-default">
      <div class="panel-body">
        <h1>☹</h1>
        <#--<h3>${errorCode}</h3>-->
        <h3>服务器发生未知错误</h3>
        <h4>如因您非法操作请改正，正常访问我们的网站！</h4>

      </div>
    </div>
  </div>
  <div class="col-md-3 hidden-sm hidden-xs"></div>
</div>
</@html>
