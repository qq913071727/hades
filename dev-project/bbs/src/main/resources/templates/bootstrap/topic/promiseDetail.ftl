<#include "../common/layout.ftl">
  <@html page_title="${topic.title!}">
  <div class="fix-layout">
    <div class="fix-btn" id="gototop" style="display: none">
      <div class="fix-img glyphicon glyphicon-arrow-up"></div>
      <div class="fix-word">返回顶部</div>
    </div>

  </div>
  <script>
    $(document).ready(function(){

        $(window).scroll(function(){
          var ling = $(document).scrollTop();
          if(ling>400){
            $("#gototop").show();
          }else{
            $("#gototop").hide();
          }

        })

      $('#editmessage').click(function(){
        var h = $(document).height()-$(window).height();
        $(document).scrollTop(h);
      });
      $('#gototop').click(function(){
        $(document).scrollTop(0);
      });
    })
  </script>
    <div class="row">
      <div class="">
        <div class="panel panel-default">
          <div class="panel-body topic-detail-header">
            <div class="media" style="position: relative">
              <div class="media-body">
                <h4 class="topic-detail-title" style="word-break: break-all;background: url('/static/bootstrap/imgs/right.png')  6px 11px no-repeat #F2F2F2">${topic.title!}</h4>
               <#-- <#if user??>
                  <div class="panel-collect" style="">
                   <#if collect??>
                       <a class="uncollect" style="" href="/collect/${topic.id!}/delete">取消收藏</a>
                      <#else>
                        <a class="collect"  style="" href="/collect/${topic.id!}/add">添加收藏</a>
                    </#if>
                  </div>
                </#if>-->
                <p class="gray">
                  <#if user??>
                 <#--   &lt;#&ndash;<#if _roles?seq_contains( "topic:edit")>&ndash;&gt;
                  <#if user.username=topic.user.username>
                      <span>•</span>
                      <span><a href="/topic/${topic.id}/edit">编辑</a></span>
                   </#if>-->
                  <#if (_roles?seq_contains( "topic:edit")) && (user.username=topic.user.username)||(_roles?seq_contains("permission:add"))>
                        <span>•</span>
                        <span><a href="/topic/${topic.id!}/edit">编辑</a></span>
                    </#if>
                    <#if (_roles?seq_contains( "topic:delete")) && (user.username=topic.user.username) ||(_roles?seq_contains("permission:add"))>

                      <span>•</span>
                      <span><a href="javascript:if(confirm('确定要删除吗？'))location.href='/topic/${topic.id!}/delete'">删除</a></span>
                    </#if>

                  </#if>
                </p>
              </div>
              <!--<div class="media-right">
            <img src="${topic.user.avatar!}" class="avatar-lg"/>
          </div>-->
            </div>
          </div>
      <#if topic.content?? && topic.content != "">
        <div class="divide"></div>
        <div class="topic-user" style="margin-top: 10px;padding: 0 35px;color: #003399;font-size: 14px;">
          <#--<a href="/user/${topic.user.username!}">-->${topic.user.username!}<#--</a>-->
        </div>
        <div class="panel-body topic-detail-content">
        ${topic.markedNotAt(topic.content!)}
        </div>
      </#if>
      <#if user??>
        <div class="panel-footer">
         <!-- <a
            href="javascript:window.open('http://service.weibo.com/share/share.php?url=${baseUrl!}/topic/${topic.id!}?r=${user.username!}&title=${topic.title!}', '_blank', 'width=550,height=370'); recordOutboundLink(this, 'Share', 'weibo.com');">分享微博</a>&nbsp;-->
          <!--<#if collect??>
            <a href="/collect/${topic.id!}/delete">取消收藏</a>
          <#else>
            <a href="/collect/${topic.id!}/add">加入收藏</a>
          </#if>-->
              <span>•</span>
              <span>${topic.formatDate(topic.inTime!)}</span>
              <span>•</span>
              <span>${topic.view!1}次点击</span>
              <!--<span>•</span>
              <span>来自 <a href="/user/${topic.user.username!}">${topic.user.username!}</a></span>-->
          <#--<span class="pull-right">${collectCount!0}个收藏</span>-->
        </div>
      </#if>
    </div>


  </div>
  <#--<div class="col-md-3 hidden-sm hidden-xs">
    <#include "../components/author_info.ftl"/>
        <@info/>
        <#include "../components/other_topics.ftl"/>
        <@othertopics/>
      </div>-->
    </div>
    <link rel="stylesheet" href="/static/bootstrap/css/jquery.atwho.min.css" />
    <script type="text/javascript" src="/static/bootstrap/js/jquery.atwho.min.js"></script>
    <script type="text/javascript" src="/static/bootstrap/js/lodash.min.js"></script>
    <#--<link rel="stylesheet" href="/static/bootstrap/libs/editor/editor.css" />-->
  <link rel="stylesheet" href="/static/bootstrap/css/wangEditor.min.css" />
    <style>
      .CodeMirror {
        height: 150px;
      }
    </style>
    <#--<script type="text/javascript" src="/static/bootstrap/js/highlight.min.js"></script>
    <script type="text/javascript" src="/static/bootstrap/libs/webuploader/webuploader.withoutimage.js"></script>
    <script type="text/javascript" src="/static/bootstrap/libs/markdownit.js"></script>
    <script type="text/javascript" src="/static/bootstrap/libs/editor/editor.js"></script>
    <script type="text/javascript" src="/static/bootstrap/libs/editor/ext.js"></script>-->
    <script type="text/javascript" src="/static/bootstrap/js/wangEditor.min.js"></script>
    <script type="text/javascript">

  $('pre code').each(function (i, block) {
    hljs.highlightBlock(block);
  });

 /* var editor = new Editor({element: $("#content1")[0], status: [],toolbar:false});
  editor.render();*/
  var editor = new wangEditor('content1');

  // 普通的自定义菜单
  editor.config.menus = [

      'bold',
      'underline',
      'italic',
      'strikethrough',
      'eraser',
      /* 'forecolor',
       'bgcolor',*/
      '|',
      'quote',
      /*'fontfamily',
      'fontsize',*/
      'head',
      'unorderlist',
      'orderlist',

      /*'link',
      'unlink',
      'table',
      '|',
      'img',
      'insertcode'*/
  ];
  // 上传图片（举例）
  editor.config.uploadImgUrl = '/wangEditorUpload';
  // 配置自定义参数（举例）
  editor.config.uploadParams = {
      '${_csrf.parameterName}': '${_csrf.token}'
  };
  editor.config.uploadImgFileName = 'file';
  editor.create();

  var $input = $(editor.codemirror.display.input);


  // at.js 配置
  var codeMirrorGoLineUp = CodeMirror.commands.goLineUp;
  var codeMirrorGoLineDown = CodeMirror.commands.goLineDown;
  var codeMirrorNewlineAndIndent = CodeMirror.commands.newlineAndIndent;
  var data = [];
    <#list replies as reply>
    data.push('${reply.user.username!}');
    </#list>
  data = _.unique(data);
  $input.atwho({
    at: "@",
    data: data
  }).on('shown.atwho', function () {
    CodeMirror.commands.goLineUp = _.noop;
    CodeMirror.commands.goLineDown = _.noop;
    CodeMirror.commands.newlineAndIndent = _.noop;
  })
    .on('hidden.atwho', function () {
      CodeMirror.commands.goLineUp = codeMirrorGoLineUp;
      CodeMirror.commands.goLineDown = codeMirrorGoLineDown;
      CodeMirror.commands.newlineAndIndent = codeMirrorNewlineAndIndent;
    });
  // END at.js 配置
</script>
  </@html>