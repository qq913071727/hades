<#include "./common/layout.ftl"/>
<@html page_tab="" page_title="搜索结果">
<div class="row">
  <div class="col-md-9">
    <div class="panel panel-default bbs-con">

        <div class="panel-heading">
            <ul class="nav nav-pills">

              <#include "./components/search.ftl"/>
          <@search/>
          <#include "./components/create_topic.ftl"/>
          <@create_topic/>
            </ul>
        </div>

      <div class="panel-body paginate-bot">
          <div class="liuyan-title clearfix">
              <span class="glyphicon glyphicon-list-alt"> </span> 留言搜索
          </div>
      <#if topics ??>
      <#include "./components/search_result.ftl"/>
      <@searchresult topics=page.getContent()/>
      <#else >
          <div class="media">

              <script type="text/javascript">
                  layer.confirm('搜索不到相关留言问题，请您留言！', {
                      btn: ['留言','取消'] //按钮
                  },function(){
                    $("#tanchu11").trigger("click");
                      layer.closeAll();
                  } ,function(){
                    window.location.href="/";
                  } );
              </script>

          </div>
      </#if>
      <#--<div class="panel-body" style="padding: 0 15px;">-->
        <#include "./components/paginate.ftl"/>
        <@paginate currentPage=(page.getNumber() + 1) totalPage=page.getTotalPages() actionUrl="/topic/search" urlParas="&q=${q!}" showdivide="no"/>
      </div>
    </div>
  </div>
  <div class="col-md-3 hidden-sm hidden-xs">
    <#include "components/public.ftl">
       <@public/>

  </div>
</div>
</@html>
