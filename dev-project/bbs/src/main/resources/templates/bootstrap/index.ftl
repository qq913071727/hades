<#include "./common/layout.ftl">
<@html page_title="首页 - ${siteTitle!}">
<div class="row">
  <div class="col-md-9">
    <div class="panel panel-default bbs-con">
      <div class="panel-heading clearfix">
        <ul class="nav nav-pills col-md-10 col-md-offset-1">

          <#include "./components/search.ftl"/>
          <@search/>
          <#include "./components/create_topic.ftl"/>
          <@create_topic/>
        </ul>
      </div>
      <div class="panel-body paginate-bot">

        <#include "./components/topics.ftl"/>
        <@topics/>
        <#include "./components/paginate.ftl"/>
        <@paginate currentPage=(page.getNumber() + 1) totalPage=page.getTotalPages() actionUrl="/" urlParas="&tab=${tab!}"/>
      </div>
    </div>
  </div>
  <div class="col-md-3 hidden-sm hidden-xs">


    <#include "components/public.ftl">
       <@public/>

      <#--<#include "./components/qrcode.ftl"/>-->
    <#--<@qrcode/>-->
  </div>
</div>

</@html>