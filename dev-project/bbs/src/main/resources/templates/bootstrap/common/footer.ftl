<#macro footer>
<div class="container">
  <br>
  <div class="text-center">
    &copy;2017 Powered by <a href="//www.conac.cn" target="_blank">政务和公益机构域名注册管理中心</a>
    <a href="http://www.miitbeian.gov.cn/">${beianName!}</a>
  ${tongjiJs!}
  </div>
    <script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1261333331'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s4.cnzz.com/z_stat.php%3Fid%3D1261333331' type='text/javascript'%3E%3C/script%3E"));</script>

    <br>
</div>
</#macro>