<#include "../common/layout.ftl"/>
<@html page_title="我的留言" page_tab="user">
<div class="row">
  <div class="col-md-9">
    <div class="panel panel-default">
      <div class="panel-heading">
      我的留言
      </div>
      <div class="panel-body">
        <#include "../components/user_topics.ftl"/>
        <@user_topics topics=page.getContent()/>
        <#include "../components/paginate.ftl"/>
        <@paginate currentPage=(page.getNumber() + 1) totalPage=page.getTotalPages() actionUrl="/user/${currentUser.username}/topics" urlParas=""/>
      </div>
    </div>
  </div>
  <div class="col-md-3 hidden-sm hidden-xs"></div>
</div>
</@html>