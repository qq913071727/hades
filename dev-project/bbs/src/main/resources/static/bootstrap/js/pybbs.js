//用户信息
var usr = window.localStorage.getItem('usr');
//token信息
var token = window.localStorage.getItem('token');

var usrJson={};
if(usr){
    usrJson.user = JSON.parse(usr);
    usrJson.token = token;
    $('#user_name').html(usrJson.user.username);
}
var moveEnd = function(obj){
    obj.focus();
    obj = obj.get(0);
    var len = obj.value.length;
    if (document.selection) {
        var sel = obj.createTextRange();
        sel.moveStart('character',len);
        sel.collapse();
        sel.select();
    } else if (typeof obj.selectionStart == 'number' && typeof obj.selectionEnd == 'number') {
        obj.selectionStart = obj.selectionEnd = len;
    }
};
$(function(){
    var n = $("#goTop");
    n.click(function () {
        return $("html,body").animate({
            scrollTop: 0
        });
    });
});
function goTopic(){
    window.location.href="/";
}
function publishTopic() {
    var em = $("#error_message");
    var errors = 0;
    var title = $("#title").val();
    if(title.length == 0) {
        errors++;
        em.html("标题不能为空");
    }
   /* var content = editor.value();*/
    var content = editor.$txt.html();
    content = filterXSS(content);

    var contentLenth = editor.$txt.text();
    if(title.length > 0){
        errors++;
        $.ajax({
            url: "/topic/topics/"+title,
            async: false,
            cache: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if(data.status == 200 && data!=null) {
                    em.html("留言主题已经存在");
                }
                if(data.status==400){
                    errors=0;
                }

            }
        })

    }
    if(title.length > 50) {
        errors++;
        em.html("标题不能超过50个字")
    }
    if(contentLenth.length > 2000) {
        errors++;
        em.html("留言内容不能超过2000个字")
    }
    if(errors == 0) {
        var form = $("#topicForm");
        form.submit();
    }
}



function promiseTopic(){
    var em = $("#error_message2");
    var errors = 0;
    var title = $("#title2").val();
    if(title.length == 0) {
        errors++;
        em.html("标题不能为空");
    }
    /*var content = editor2.value();*/
    var content = editor2.$txt.html();
    var contentLenth = editor2.$txt.text();
    if(title.length > 0){
        errors++;
        $.ajax({
            url: "/topic/topics/"+title,
            async: false,
            cache: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if(data.status == 200 && data!=null) {
                    em.html("留言主题已经存在");
                }
                if(data.status==400){
                    errors=0;
                }

            }
        })

    }
    if(title.length > 50) {
        errors++;
        em.html("标题不能超过50个字")
    }
    if(contentLenth.length > 2000) {
        errors++;
        em.html("留言内容不能超过2000个字")
    }
    if(errors == 0) {
        var form = $("#topicForm2");
        form.submit();
    }
}
function liveShowTopic(){
    var em = $("#error_message3");
    var errors = 0;
    var title = $("#title3").val();
    if(title.length == 0) {
        errors++;
        em.html("标题不能为空");
    }
    /*var content = editor3.value();*/
    var content = editor3.$txt.html();
    var contentLenth = editor3.$txt.text();

    if(title.length > 0){
        errors++;
        $.ajax({
            url: "/topic/topics/"+title,
            async: false,
            cache: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if(data.status == 200 && data!=null) {
                    em.html("留言主题已经存在");
                }
                if(data.status==400){
                    errors=0;
                }

            }
        })

    }
    if(title.length > 50) {
        errors++;
        em.html("标题不能超过50个字")
    }
    if(contentLenth.length > 2000) {
        errors++;
        em.html("留言内容不能超过2000个字")
    }
    if(errors == 0) {
        var form = $("#topicForm3");
        form.submit();
    }
}

function subjectShowTopic(){
    var em = $("#error_message4");
    var errors = 0;
    var title = $("#title4").val();
    if(title.length == 0) {
        errors++;
        em.html("标题不能为空");
    }
    /*var content = editor4.value();*/
    var content = editor4.$txt.html();
    var contentLenth = editor4.$txt.text();

    if(title.length > 0){
        errors++;
        $.ajax({
            url: "/topic/topics/"+title,
            async: false,
            cache: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if(data.status == 200 && data!=null) {
                    em.html("留言主题已经存在");
                }
                if(data.status==400){
                    errors=0;
                }

            }
        })

    }
    if(title.length > 50) {
        errors++;
        em.html("标题不能超过50个字")
    }
    if(contentLenth.length > 2000) {
        errors++;
        em.html("留言内容不能超过2000个字")
    }
    if(errors == 0) {
        var form = $("#topicForm4");
        form.submit();
    }
}


function previewContent() {
    var content = $("#content").val();
    if(content.length > 0) {
        $("#preview").html("<hr>" + marked(content));
    }
}
function replySubmit() {
    var errors = 0;
    var em = $("#error_message");
    /*var content = editor.value();*/
    var content = editor.$txt.html();

    if(content.length == 0) {
        errors++;
        em.html("回复内容不能为空");
    }
    if(errors == 0) {
        var form = $("#replyForm");
        form.submit();
    }
}



function replyReplySubmit(replyId) {

/*
    var replyReplyCount = $("#replythisReply"+replyId).text();
    var mapper=replyReplyCount.split("条评论")[0];
    var countInt = parseInt(mapper,10);
    ajaxCount = countInt+1;
    $("#replythisReply"+replyId).text(ajaxCount+" 条评论");*/



    var errors = 0;
    var em = $("#error_message"+replyId);
    var content = $("#content"+replyId).val();
    $.ajax({
        cache: false,
        type: "POST",
        url: "/replyReply/findTimes",
        async: false,
        error: function (request)
        {
            alert("Connection error");
        },
        success: function (data5)
        {
            if(data5.status==400){
                errors++;
                em.html("一分钟之内只能发送10条评论！");
            }
        }
    });
    if(content.length == 0) {
        errors++;
        em.html("评论内容不能为空");
    }
    if(errors == 0) {

        /*var form = $("#replyForm"+replyId);
        form.submit();
        replythisReply(replyId);*/
        $.ajax({
            cache: false,
            type: "POST",
            url:"/replyReply/save",
            data:$("#replyForm"+replyId).serialize(),
            async: false,
            error: function(request) {
                alert("Connection error");
            },
            success: function(data3) {
                em.html("");
                $("#content"+replyId).val("");
                $(".commit"+replyId).show();
                var p ="";
                if(data3.status==200){
                    p=data3.pages;
                }
                $.ajax({
                    url: "/replyReply/reply/"+replyId+"?p="+p,
                    async: false,
                    cache: false,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        if(data.status == 200 ) {
//                            $("#reply").text(data.detail);
//                 $("#replyTOreply").html(data);
//                 $("#reply"+replyId).html('');

                            /*评论成功，评论数+1*/
                            var replyReplyCount = $("#replythisReply"+replyId).text();
                            var mapper=replyReplyCount.split("条评论")[0];
                            var countInt = parseInt(mapper,10);
                            ajaxCount = countInt+1;
                            $("#replythisReply"+replyId).text(ajaxCount+" 条评论");


                            $("#replyTOreply"+replyId).html('');
                            var html="";
                            var replyReplies = data.data.content;
                            replyReplies.forEach(function(e){
                                if(e.user.nickname!=null && e.user.nickname !=""){
                                    html+=
                                        "<p style='color: #013299;'>"+e.user.nickname+"</p>"+
                                        "<p style='color: #666666;'>"+e.content+"</p>" +
                                        "<p style='color: #999999;'>评论于"+e.inTime+"</p>"
                                    ;
                                }else{
                                    html+=
                                        "<p style='color: #013299;'>"+e.user.orgname+"</p>"+
                                        "<p style='color: #666666;'>"+e.content+"</p>" +
                                        "<p style='color: #999999;'>评论于"+e.inTime+"</p>"
                                    ;
                                }



                            })
                            var pageCount=data.data.totalPages;
                            var currentPage=1;
                            if(data3.status==200){
                                currentPage=pageCount;
                            }
                            var options = {
                                bootstrapMajorVersion: 2, //版本
                                currentPage: currentPage, //当前页数
                                totalPages: pageCount, //总页数
                                itemTexts: function (type, page, current) {
                                    switch (type) {
                                        case "first":
                                            return "首页";
                                        case "prev":
                                            return "上一页";
                                        case "next":
                                            return "下一页";
                                        case "last":
                                            return "末页";
                                        case "page":
                                            return page;
                                    }
                                },//点击事件，用于通过Ajax来刷新整个list列表

                                onPageClicked: function (event, originalEvent, type, page) {

                                    $.ajax({
                                        url: "/replyReply/reply/" + replyId + "?p=" + page,
                                        type: "GET",
                                        data: "page=" + page,
                                        success: function (data1)
                                        {
                                            data3="";
                                            if (data1 != null) {
                                                // $("#reply"+replyId).html('');
                                                $("#replyTOreply"+replyId).html('');
                                                var html="";
                                                var replyReplies = data1.data.content;
                                                replyReplies.forEach(function(e){
                                                    if(e.user.nickname!=null && e.user.nickname !=""){
                                                        html+=
                                                            "<p style='color: #013299;'>"+e.user.nickname+"</p>"+
                                                            "<p style='color: #666666;'>"+e.content+"</p>" +
                                                            "<p style='color: #999999;'>评论于"+e.inTime+"</p>"
                                                        ;
                                                    }else{
                                                        html+=
                                                            "<p style='color: #013299;'>"+e.user.orgname+"</p>"+
                                                            "<p style='color: #666666;'>"+e.content+"</p>" +
                                                            "<p style='color: #999999;'>评论于"+e.inTime+"</p>"
                                                        ;
                                                    }

                                                })
                                            }
                                            // $("#reply"+replyId).append("<div>" +html+"</div>");
                                            $("#replyTOreply"+replyId).append("<div>" +html+"</div>")
                                        }
                                    });
                                }
                            };

                            $("#example"+replyId).bootstrapPaginator(options);

                            // $("#reply"+replyId).append("<div>" +html+"</div>");
                            $("#replyTOreply"+replyId).append("<div>" +html+"</div>")
                        }

                    }
                })
            }
        });


    }

}

function replythis(author) {
    var content = $(editor.codemirror.display.input);
    var oldContent = editor.value();
    var prefix = "@" + author + " ";
    var newContent = '';
    console.log(oldContent, prefix);
    if(oldContent.length > 0){
        console.log(oldContent == prefix);
        if (oldContent != prefix) {
            console.log(1);
            newContent = oldContent + '\n' + prefix;
        }
    } else {
        console.log(2);
        newContent = prefix
    }
    editor.value(newContent);
    CodeMirror.commands.goDocEnd(editor.codemirror);
    content.focus();
    moveEnd(content);
}
function updateUserProfile() {
    var errors = 0;
    var em = $("#error_message");
    var signature = $("#signature").val();
    if(signature.length > 1000) {
        errors++;
        em.html("个性签名不能超过1000个字");
    }
    if(errors == 0) {
        var form = $("#userProfileForm");
        $("#userProfileUpdateBtn").button("保存中...");
        form.submit();
    }
}
function permissionSubmit() {
    var errors = 0;
    var em = $("#error_message");
    var pid = $("#pid").val();
    var name = $("#name").val();
    var url = $("#url").val();
    var description = $("#description").val();
    if(name.length == 0) {
        errors++;
        em.html("权限标识不能为空");
    }
    if(pid > 0 && url.length == 0) {
        errors++;
        em.html("授权路径不能为空");
    }
    if(description.length == 0) {
        errors++;
        em.html("权限描述不能为空");
    }
    if(errors == 0) {
        var form = $("#permissionForm");
        $("#permissionBtn").button("保存中...");
        form.submit();
    }
}
function roleSubmit() {
    var errors = 0;
    var em = $("#error_message");
    var name = $("#name").val();
    var description = $("#description").val();
    if(name.length == 0) {
        errors++;
        em.html("角色标识不能为空");
    }
    if(description.length == 0) {
        errors++;
        em.html("角色描述不能为空");
    }
    if(errors == 0) {
        var form = $("#roleForm");
        $("#roleBtn").button("保存中...");
        form.submit();
    }
}
function saveSection() {
    var errors = 0;
    var em = $("error_message");
    var name = $("name").val();
    var tab = $("tab").val();
    if(name.length == 0) {
        errors++;
        em.html("名称不能为空");
    }
    if(tab.length == 0) {
        errors++;
        em.html("tab不能为空");
    }
    if(errors == 0) {
        var form = $("sectionForm");
        $("#savesectionbtn").button("保存中...");
        form.submit();
    }
}

function replythisReply(replyId){
    /*alert("sdfadaf");*/
    // $("#conmment"+replyId).style.display='block';
    var showStatus = $(".commit"+replyId).is(":visible");
    var hiddenStatus = $(".commit"+replyId).is(":hidden");
    if(showStatus){
        $(".commit"+replyId).hide();
        $("#error_message"+replyId).html('');
        return ;
    }


    $(".commit"+replyId).show();

    $.ajax({
        url: "/replyReply/reply/"+replyId,
        async: false,
        cache: false,
        type: "GET",
        dataType: "json",
        success: function (data) {
            if(data.status == 200 ) {
//                            $("#reply").text(data.detail);
//                 $("#replyTOreply").html(data);
//                 $("#reply"+replyId).html('');
                $("#replyTOreply"+replyId).html('');
                var html="";
                var replyReplies = data.data.content;
                replyReplies.forEach(function(e){
                    /*html+=
                            "<p style='color: #013299;'>"+e.user.username+"</p>"+
                        "<p style='color: #666666;'>"+e.content+"</p>" +
                            "<p style='color: #999999;'>评论于"+e.inTime+"</p>"
                        ;*/


                    if(e.user.nickname!=null && e.user.nickname !=""){
                        html+=
                            "<p style='color: #013299;'>"+e.user.nickname+"</p>"+
                            "<p style='color: #666666;'>"+e.content+"</p>" +
                            "<p style='color: #999999;'>评论于"+e.inTime+"</p>"
                        ;
                    }else{
                        html+=
                            "<p style='color: #013299;'>"+e.user.orgname+"</p>"+
                            "<p style='color: #666666;'>"+e.content+"</p>" +
                            "<p style='color: #999999;'>评论于"+e.inTime+"</p>"
                        ;
                    }
                })
                var pageCount=data.data.totalPages;
                var currentPage=1;
                var options = {
                    bootstrapMajorVersion: 2, //版本
                    currentPage: currentPage, //当前页数
                    totalPages: pageCount, //总页数
                    itemTexts: function (type, page, current) {
                        switch (type) {
                            case "first":
                                return "首页";
                            case "prev":
                                return "上一页";
                            case "next":
                                return "下一页";
                            case "last":
                                return "末页";
                            case "page":
                                return page;
                        }
                    },//点击事件，用于通过Ajax来刷新整个list列表
                    onPageClicked: function (event, originalEvent, type, page) {
                        $.ajax({
                            url: "/replyReply/reply/" + replyId + "?p=" + page,
                            type: "GET",
                            data: "page=" + page,
                            success: function (data1)
                            {
                                if (data1 != null) {
                                    // $("#reply"+replyId).html('');
                                    $("#replyTOreply"+replyId).html('');
                                    var html="";
                                    var replyReplies = data1.data.content;
                                    replyReplies.forEach(function(e){
                                       /* html+=
                                            "<p style='color: #013299;'>"+e.user.username+"</p>"+
                                            "<p style='color: #666666;'>"+e.content+"</p>" +
                                            "<p style='color: #999999;'>评论于"+e.inTime+"</p>"
                                        ;*/
                                        if(e.user.nickname!=null && e.user.nickname !=""){
                                            html+=
                                                "<p style='color: #013299;'>"+e.user.nickname+"</p>"+
                                                "<p style='color: #666666;'>"+e.content+"</p>" +
                                                "<p style='color: #999999;'>评论于"+e.inTime+"</p>"
                                            ;
                                        }else{
                                            html+=
                                                "<p style='color: #013299;'>"+e.user.orgname+"</p>"+
                                                "<p style='color: #666666;'>"+e.content+"</p>" +
                                                "<p style='color: #999999;'>评论于"+e.inTime+"</p>"
                                            ;
                                        }

                                    })
                                }
                                // $("#reply"+replyId).append("<div>" +html+"</div>");
                                $("#replyTOreply"+replyId).append("<div>" +html+"</div>")
                            }
                        });
                    }
                };
                $("#example"+replyId).bootstrapPaginator(options);

                // $("#reply"+replyId).append("<div>" +html+"</div>");
                $("#replyTOreply"+replyId).append("<div>" +html+"</div>")
            }

        }
    })
    // $("#reply"+replyId).append($("#replyTOreply").html());





   /* $.ajax({
        url: "/replyReply/reply/"+replyId,
        async: false,
        cache: false,
        type: "GET",
        dataType: "json",
        success: function (data) {
            if(data.status == 200 && data!=null) {
                $("#reply${reply.id}").append(data);
            }
        }
    })*/

}