package cn.tomoya;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import cn.tomoya.common.enums.TopicTypeEnum;
import redis.clients.jedis.Jedis;

/**
 * Created by tomoya.
 * Copyright (c) 2016, All Rights Reserved.
 * http://tomoya.cn
 */
public class TestDemo
{
    Logger log = Logger.getLogger(TestDemo.class);

    @Test
    public void test1()
    {
        String dh = ",";
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 10000000; i++) {
            sb.append(i).append(dh);
        }
        String str = sb.toString();
        long start = System.currentTimeMillis();
        String[] strs = str.split(dh);
        List list = Arrays.asList(strs);
        boolean b = list.contains("57192");
        System.out.println(System.currentTimeMillis() - start);
        start = System.currentTimeMillis();
        str = str.replace("3123,", "") + "123123123,";
        System.out.println(System.currentTimeMillis() - start);
    }

    @Test
    public void test2()
    {
        Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
        log.info(md5PasswordEncoder.encodePassword("123123", "tomoya"));
    }

    @Test
    public void test3()
    {
        log.info(UUID.randomUUID().toString());
        log.info(new BCryptPasswordEncoder().encode("123123"));
    }

    @Test
    public void test4()
    {

        for (int x = 1; x <= 9; x++) {
            for (int y = 1; y <= x; y++) {
                System.out.print(y + "*" + x + "=" + y * x + "\t");
            }
            System.out.println();
        }

        for (int x = 1; x <= 5; x++) {
            for (int y = 1; y <= x; y++) {
                System.out.print(" ");
            }
            for (int z = x; z <= 5; z++) {
                System.out.print("* ");
            }
            System.out.println("");
        }
    }

    public static void printAll(HashMap<String, String> mapData)
    {
        //获取key的set
        Iterator iter = mapData.keySet().iterator();

        while (iter.hasNext()) {
            Object key = iter.next();
            Object val = mapData.get(key);
        }
    }

    public static int removeItems(List<Object> listItems, int[] itemInddexs)
    {
        int removeNum = 0;
        for (int i = 0; i < itemInddexs.length; i++) {
            for (Iterator it = listItems.iterator(); it.hasNext(); ) {
                String str = (String) it.next();

                if (str.equals((String) listItems.get(itemInddexs[i]))) {
                    it.remove();
                    removeNum++;
                }
            }
        }
        return removeNum;
    }

    @Test
    public void select1()
    {
        List<Object> listadd = new ArrayList();

        listadd.add("a");

        listadd.add("b");

        listadd.add("b");

        listadd.add("c");

        listadd.add("d");
        int[] ints = { 1, 2, 3 };
        int s = removeItems(listadd, ints);
        System.out.println(s);
    }



    /*@Test
    public void select(){
        int[] arr1={4,2,1,3,5,9};
        int[] arr2={3,5,8,7};
        Arrays.sort(arr1);
        for (int i = 0; i <arr1.length ; i++) {
            int temp=0;
            if(arr1[i]>arr1[i+1]){
                temp=arr1[i];


            }

        }

    }*/


    public static void printArray(String[] arr){
        System.out.print("[");
        for (int i = 0; i <arr.length ; i++) {
            if (i!=arr.length-1){
                System.out.print(arr[i]+",");
            }else{
                System.out.print(arr[i]+"]");
            }
        }
    }
    
    public static void sortString(String[] arr){
        for (int i = 0; i < arr.length-1; i++) {
            for (int j = i+1; j <arr.length ; j++) {
                if(arr[i].compareTo(arr[j])>0){
                    swap(arr,i,j);

                }

            }
            
        }
        
    }

    public static void swap(String[] arr,int i,int j){
        String temp = arr[i];
        arr[i]=arr[j];
        arr[j]=temp;

    }
    @Test
    public void test5(){
        String[] arr ={"asd","dfgwe","sfe","se"};
        printArray(arr);
        sortString(arr);
        printArray(arr);
    }
    @Test
    public void test6()throws IOException{
        StringBuffer sb = new StringBuffer("abcd");
        sb.replace(1,3,"nba");
        System.out.println(sb);
        boolean s = Boolean.parseBoolean("te");
        System.out.println(s);
        int sm = Integer.parseInt("5");
        System.out.println(sm);
        Integer.toString(60,4);
        System.currentTimeMillis();
        Calendar c = Calendar.getInstance();
        System.out.println(c.get(Calendar.YEAR));




        FileWriter fw = new FileWriter("demo.txt");
        fw.write("slfw");
      /*  fw.flush();
        fw.close();*/

        BufferedWriter bufw = new BufferedWriter(fw);
        for (int i = 0; i <4 ; i++) {
            bufw.write("dsf"+i);
            bufw.newLine();
            bufw.flush();


        }
        bufw.flush();
        fw.close();
    }

    public static void readKey() throws IOException
    {
        InputStream in = System.in;
        int ch = in.read();
        System.out.println(ch);

        ch = in.read();
        System.out.println(ch);
        ch = in.read();
        System.out.println(ch);
        in.close();
    }
    @Test
    public void test7() throws IOException
    {
        readUp() ;
    }


    public void readUp() throws IOException
    {
        StringBuilder sb = new StringBuilder();
        InputStream in = System.in;
        int chr = 0;
        while((chr=in.read())!=-1){
            if(chr == '\r'){
                continue;

            }
            if (chr =='\n'){
                String temp = sb.toString();
                if ("over".equals(temp)){
                    break;
                }
                System.out.println(temp.toUpperCase());
                sb.delete(0,sb.length());
            }else {
                sb.append(chr);

            }


        }
    }
    @Test
    public void test9(){
        String str = "zzhaangsanttttxiaoqiangmmmmmzzhaoliu";
        str = str.replaceAll("(.)\\1+","$1");
        System.out.println(str);
        String s = "15800001111";
        s.replaceAll("(\\d{3})(\\d{1})(\\d{1})","$1****$2");
        System.out.println(s);
    }

    @Test
    public void test10(){
        Jedis jedis = new Jedis("172.17.80.166",6379);
        jedis.auth("rc");
        Map<String,String> hash = new HashMap<String,String>();
        hash.put("username","xiaoming");
        hash.put("sex","nan");
        hash.put("age","28");
        jedis.hmset("user_01",hash);
        System.out.println("age:"+jedis.hmget("user_01","age"));

         hash = jedis.hgetAll("user_01");
        System.out.println(hash);
        jedis.close();

    }

    @Test
    public void test11(){
        int i=0,j=0;
        System.out.println(j==i);
        System.out.println(i);

        Integer i1 = 1;
        Integer i3 = 1;
        int i2 = 1;
        System.out.println(i1==i3);
    }

    @Test
    public void test12(){
        int num = 245000006;
        String s = formatInteger(num);
        System.out.println(s);

        String numstr = "24504000";
        String s1 = get(numstr);
        System.out.println(s1);
    }
    static String[] util ={ "", "十", "百", "千", "万", "十万", "百万", "千万", "亿",
            "十亿", "百亿", "千亿", "万亿" };
    static char[] numArray= { '零', '一', '二', '三', '四', '五', '六', '七', '八', '九' };
    public static String formatInteger(int num){
        char[] val = String.valueOf(num).toCharArray();
        int len = val.length;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++) {
            String m = val[i]+"";
            int n = Integer.valueOf(m);
            boolean isZero = n ==0;
            String unit = util[(len - 1) - i];
            if(isZero){
                if('0'==val[i-1]){
                    continue;
                }else{
                    sb.append(numArray[n]);
                }

            }else{
                sb.append(numArray[n]);
                sb.append(unit);
            }
        }

        return sb.toString();
    }

    private static String units[] = {"","十","百","千","万","十","百","千","亿"};

    private static String nums[] = {"零","一","二","三","四","五","六","七","八","九","十"};

    private static String result[] ;
    public static String get(String input) {
        String out = "";
        result = new String[input.length()];
        for(int i=0;i<result.length;i++) {
            result[i] = String.valueOf(input.charAt(i));
        }
        int back = 0;
        for(int i=0;i<result.length;i++) {
            if(!result[i].equals("0")) {
                back = result.length-i-1;
                out += nums[Integer.parseInt(result[i])];
                out += units[back];
            }else {
                if(i==result.length-1) {

                }else {
                    if(!result[i+1].equals("0")) {
                        out += nums[0];
                    }
                }
            }
        }
        return out;
    }
    @Test
    public void test8(){
        String s = String.valueOf(TopicTypeEnum.LIVESHOW.getName());

        int live = TopicTypeEnum.LIVESHOW.getTypeValue();
        int promist = TopicTypeEnum.PROMIST.getTypeValue();
        System.out.println(s);
        System.out.println(live);
        System.out.println(promist);
    }

}

