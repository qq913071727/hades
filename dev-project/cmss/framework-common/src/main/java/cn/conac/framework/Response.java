/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.framework;

import cn.conac.framework.common.utils.JsonUtil;

/**
 * 接口调用应答体
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public class Response {
	/** 应答结果 */
	private Result result;
	/** 数据 */
	private Object data;

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public Object getData() {
		return data;
	}
	
	public void setData(Object data) {
		this.data = data;
	}
	
	public String toJson(){
		return JsonUtil.toJson(this);
	}
	
}
