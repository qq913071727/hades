/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.framework;

import cn.conac.framework.common.utils.JsonUtil;

/**
 * 接口调用应答结果
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public class Result {
	
	/** 应答码 对外部系统有效  约定1000成功,其余都属于失败*/
	private Integer code;
	
	/** 应答消息描述 */
	private String message;
	
	/** 应答状态  对系统内部有效 */
	private Status status;
	
	public enum Status{
		/** 失败*/
		ERROR,
		/** 成功*/
		SUCCESS
	}
	
	public Integer getCode() {
		return code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String toJson(){
		return JsonUtil.toJson(this);
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
}
