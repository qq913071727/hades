/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.framework.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.annotations.GenericGenerator;

import cn.conac.framework.common.utils.JsonUtil;
import cn.conac.framework.listener.EntityListener;

/**
 * 基础实体类
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT 
 */
@EntityListeners(EntityListener.class)
@MappedSuperclass
public abstract class BaseEntity  implements Serializable {

	private static final long serialVersionUID = -7444351194789389245L;
	/** "ID"属性名称 */
	public static final String ID_PROPERTY_NAME = "id";

	/** "创建日期"属性名称 */
	public static final String CREATE_DATE_PROPERTY_NAME = "createDate";

	/** "修改日期"属性名称 */
	public static final String MODIFY_DATE_PROPERTY_NAME = "modifyDate";
	
	@Id
	@GeneratedValue(generator = "entityIdGenerator")
	@GenericGenerator(name = "entityIdGenerator", strategy = "uuid")
	private String id;

	/** 创建时间 */
	@Column(nullable = false, updatable = false)
	private Date createDate;

	/** 修改时间 */
	@Column(nullable = false, updatable = false)
	private Date modifyDate; 
	
	
	public BaseEntity() {
		super();
	}

	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 获取创建时间
	 * @return the createDate 创建时间
	 */
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 获取修改时间
	 * @return the modifyDate 修改时间
	 */
	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	@Transient
	public boolean isNew() {
		return null == this.getId();
	}

	@Override
	public boolean equals(Object obj) {
		 return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
	
	public String toJson(){
		return JsonUtil.toJson(this);
	}
	
}
