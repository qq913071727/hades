# CONAC资料共享系统 （简称CMSS）
 


| 名称    |    岗位    |       职责      |
| :------:| :---------:| :-------------: |
| 姜殿祥  | 项目经理   |  设计、开发  |
| 刘伟  | JAVA EE |  设计、开发 |
| 李珅  | JAVA EE   |  设计、开发         |


# Git 版本管理说明
* master: 主分支，主要用来版本发布，这个分支只能从其他分支合并，不能在这个分支直接修改
* develop：日常开发分支，该分支正常保存了开发的最新代码
* feature：具体的功能开发分支，只与 develop 分支交互，一旦开发完成，我们合并回develop分支进入下一个Release
* release：release 分支可以认为是 master 分支的未测试版。比如说某一期的功能全部开发完成，那么就将 develop 分支合并到 release 分支，测试没有问题并且到了发布日期就合并到 master 分支，进行发布。
        (你需要一个发布一个新release的时候，我们基于Develop分支创建一个Release分支，完成Release后，我们合并到Master和Develop分支)
        
* hotfix：线上 bug 修复分支(当我们发现新的Bug时候，我们需要创建一个Hotfix, 完成Hotfix后，我们合并回master和develop分支，所以gotfix的改动会进入下一个Release)

      参考：http://www.cnblogs.com/cnblogsfans/p/5075073.html 

# Git 常用命令

*  git拉取远程分支并创建本地分支 ： http://blog.csdn.net/tterminator/article/details/52225720
*  Git 远程分支的删除与同步 ：http://blog.csdn.net/lxbwolf/article/details/53168066
*  远程同步到本地：http://www.360doc.com/content/14/1008/15/474846_415260141.shtml
*  

1. 添加到暂存区 ：git add .
2. 提交到本地分支 ：git commit -m “注释”
3. 提交到远程分支 ：git push origin 要提交到远程分支的名字
4. 将远程分支代码同步到本地（自动merge,不建议） ：git pull origin 远程分支的名字
    
    git fetch origin 远程分支:本地分支
    
    git diff 本地分支 

    git merge 本地分支
    
5. 标准流程 https://my.oschina.net/nyankosama/blog/270546

    git支持很多种工作流程，我们采用的一般是这样，远程创建一个主分支，本地每人创建功能分支，日常工作流程如下：

    去自己的工作分支
    $ git checkout work

    工作
    ....

    提交工作分支的修改
    $ git commit -a

    回到主分支
    $ git checkout master

    获取远程最新的修改，此时不会产生冲突
    $ git pull

    回到工作分支
    $ git checkout work

    用rebase合并主干的修改，如果有冲突在此时解决
    $ git rebase master

    回到主分支
    $ git checkout master

    合并工作分支的修改，此时不会产生冲突。
    $ git merge work

    提交到远程主干
    $ git push

    这样做的好处是，远程主干上的历史永远是线性的。每个人在本地分支解决冲突，不会在主干上产生冲突。
    
