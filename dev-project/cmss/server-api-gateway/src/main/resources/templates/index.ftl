<#include "./common/layout.ftl">
<@html page_title="首页">

<div class="container-center fl" style="margin-left: 236px; width: 700px;">
	<div class="container-center-small">	
		<div class="container-center-search clearfix">
			<input type="" name="" id="" value="请输入想要搜索的内容" class="search-input"/>
			<a href="" class="search-btn">搜索</a>
		</div>
		
		<div class="container-center-scroll">
			<div id="slideBox" class="slideBox">
				<div class="hd">
					<ul>
						<li></li>
						<li></li>
						<li></li>
					</ul>
				</div>
				<div class="bd">
					<ul>
						<li><a href="" target="_blank"><img src="img/scroll-img1.jpg" /></a></li>
						<li><a href="" target="_blank"><img src="img/scroll-img1.jpg" /></a></li>
						<li><a href="" target="_blank"><img src="img/scroll-img1.jpg" /></a></li>
					</ul>
				</div>
	
				<!-- 下面是前/后按钮代码，如果不需要删除即可 -->
				<a class="prev" href="javascript:void(0)"></a>
				<a class="next" href="javascript:void(0)"></a>
	
			</div>
	
			<script type="text/javascript">
				jQuery(".slideBox").slide({mainCell:".bd ul",autoPlay:true});
			</script>
		</div>
		
		<div class="container-center-newsupload">
			<div class="newsupload-tab-title clearfix">
				<div class="newsupload-tab fl clearfix">
					<ul>
						<li id="two1" onclick="setTab('two',1,2)" class="active">最新上传</li>
						<li class="li-line2"></li>
                		<li id="two2" onclick="setTab('two',2,2)"  >月度最热</li>      
					</ul>								
				</div>
				<a href="" class="newsupload-tabchange fr">换一换</a>
			</div>
			<div class="newsupload-tab-info">
				<div id="tab_two_1"  style="display: block" class="newsupload-tab-infocon">
	                <ul>
	                	<li>
	                		<h6>贵州省机构编制服务经济开发区等园区建设的经验和启示</h6>
	                		<p>
	                			<em>行政体制改革</em>
	                			<span class="newsupload-tab-infocon-icon1"><i>政策法规</i></span>
	                			<span class="newsupload-tab-infocon-icon2">适用地区：<i>贵州省</i></span>
	                			<span>上传时间：<i>2017-07-18</i></span>
	                		</p>
	                	</li>
	                	<li>
	                		<h6>贵州省机构编制服务经济开发区等园区建设的经验和启示</h6>
	                		<p>
	                			<em>行政体制改革</em>
	                			<span class="newsupload-tab-infocon-icon1"><i>政策法规</i></span>
	                			<span class="newsupload-tab-infocon-icon2">适用地区：<i>贵州省</i></span>
	                			<span>上传时间：<i>2017-07-18</i></span>
	                		</p>
	                	</li>
	                	<li>
	                		<h6>贵州省机构编制服务经济开发区等园区建设的经验和启示</h6>
	                		<p>
	                			<em>行政体制改革</em>
	                			<span class="newsupload-tab-infocon-icon1"><i>政策法规</i></span>
	                			<span class="newsupload-tab-infocon-icon2">适用地区：<i>贵州省</i></span>
	                			<span>上传时间：<i>2017-07-18</i></span>
	                		</p>
	                	</li>
	                	<li>
	                		<h6>贵州省机构编制服务经济开发区等园区建设的经验和启示</h6>
	                		<p>
	                			<em>行政体制改革</em>
	                			<span class="newsupload-tab-infocon-icon1"><i>政策法规</i></span>
	                			<span class="newsupload-tab-infocon-icon2">适用地区：<i>贵州省</i></span>
	                			<span>上传时间：<i>2017-07-18</i></span>
	                		</p>
	                	</li>
	                </ul>
	            </div>
	            <div id="tab_two_2" style="display:none" class="newsupload-tab-infocon">
	                <ul>
	                	<li>
	                		<h6>贵州省机构编制服务经济开发区等园区建设的经验和启示</h6>
	                		<p>
	                			<em>行政体制改革</em>
	                			<span class="newsupload-tab-infocon-icon1"><i>政策法规</i></span>
	                			<span class="newsupload-tab-infocon-icon2">适用地区：<i>贵州省</i></span>
	                			<span>浏览次数：<i>200</i></span>
	                			<span>下载次数：<i>30</i></span>
	                		</p>
	                	</li>
	                	<li>
	                		<h6>贵州省机构编制服务经济开发区等园区建设的经验和启示</h6>
	                		<p>
	                			<em>行政体制改革</em>
	                			<span class="newsupload-tab-infocon-icon1"><i>政策法规</i></span>
	                			<span class="newsupload-tab-infocon-icon2">适用地区：<i>贵州省</i></span>
	                			<span>浏览次数：<i>200</i></span>
	                			<span>下载次数：<i>30</i></span>
	                		</p>
	                	</li>
	                	<li>
	                		<h6>贵州省机构编制服务经济开发区等园区建设的经验和启示</h6>
	                		<p>
	                			<em>行政体制改革</em>
	                			<span class="newsupload-tab-infocon-icon1"><i>政策法规</i></span>
	                			<span class="newsupload-tab-infocon-icon2">适用地区：<i>贵州省</i></span>
	                			<span>浏览次数：<i>200</i></span>
	                			<span>下载次数：<i>30</i></span>
	                		</p>
	                	</li>
	                	<li>
	                		<h6>贵州省机构编制服务经济开发区等园区建设的经验和启示</h6>
	                		<p>
	                			<em>行政体制改革</em>
	                			<span class="newsupload-tab-infocon-icon1"><i>政策法规</i></span>
	                			<span class="newsupload-tab-infocon-icon2">适用地区：<i>贵州省</i></span>
	                			<span>浏览次数：<i>200</i></span>
	                			<span>下载次数：<i>30</i></span>
	                		</p>
	                	</li>
	                </ul>
	            </div>
			</div>
		</div>
	</div>
</div>

</@html>