<#include "../common/layout.ftl">
<@html page_title="主题分类">
	
	<head>
		<!--<script type="text/javascript" src="/bdtlaw/monitor/static/js/client_monitor/client_monitor.js"></script>-->
		<!--<link rel="stylesheet" href="/static/css/default.css">-->
	</head>

<div class="container-center-big">
	<div class="crumbs-nav"><a href="">首页</a>>主题分类</div>
		<div class="container-center-big-search">
			<div class="container-center-search clearfix fl">
				<input type="" name="" id="" value="请输入想要搜索的内容" class="search-input"/>
				<a href="" class="search-btn">搜索</a>
			</div>
			<div class="search-select1 fl search-select">
				<input type="radio" name="scarchRange" id="" value=""/><label for="scarchRange">精准搜索</label>
				<input type="radio" name="scarchRange" id="" value="" checked="checked"/><label for="scarchRange">全文搜素</label>
			</div>
			<div class="search-select2 search-select">
				<input type="radio" name="scarchFile" id="" value="" checked="checked"/><label for="scarchFile">全部</label>
				<input type="radio" name="scarchFile" id="" value=""/><label for="scarchFile">DOC</label>
				<input type="radio" name="scarchFile" id="" value=""/><label for="scarchFile">PDF</label>
				<input type="radio" name="scarchFile" id="" value=""/><label for="scarchFile">PPT</label>
			</div>
		</div>	
		<div class="child-items clearfix">
			<h2>子项分类：</h2>
			<ul class="child-items-ul">
				<li class="active">全部</li>
				<li>权责清单</li>
				<li>综合行政执法</li>
				<li>经济发达镇行政管理体制</li>
				<li>领域体制改革</li>
			</ul>
		</div>
		<div class="document-filter">
			<input type="text" id="" value="" class="document-filter-number document-filter-input" placeholder="请输入发文字号"/>
			<input type="text" id="" value="" class="document-filter-starttime document-filter-input" placeholder="发文起始时间"/>&nbsp;-&nbsp;<input type="text" id="" value="" class="document-filter-endtime document-filter-input" placeholder="发文截止时间"/>						
			<select name="" class="document-filter-select">
				<option value="请选择适用地区">请选择适用地区</option>
				<option value="全国">全国</option>
				<option value="北京">北京</option>
				<option value="上海">上海</option>
			</select>
			<select name="" class="document-filter-select">
				<option value="全部">全部</option>
				<option value="全部">全部</option>
			</select>
			<a href="" class="document-filter-btn">筛选</a>
		</div>
		<div class="newsupload-tab-info">
				<div class="newsupload-tab-infocon">
	                <ul>
	                	<#list materialList! as material>
		                	<li>
		                		<h6 class="clearfix"><div class="h6-title">${material.name!}</div><span class="score">评分：<i>${material.score!}</i>分</span></h6>
		                		<p>
		                			<em>权责清单</em>
		                			<span>国办发【2015】92号</span>
		                			<span class="newsupload-tab-infocon-icon2">适用地区：<i>贵州省·贵阳市</i></span>
		                			<span>上传时间：<i>2017-07-18</i></span>
		                			<span>发文时间：<i>2017-07-18</i></span>
		                			<span>下载次数：<i>200</i></span>
		                		</p>
		                	</li>
	                	</#list>
	                </ul>
	            </div>
		</div>
		<div class="bottom-page">
			<ul>
				<li><a href="">首页</a></li>
				<li><a href="">上一页</a></li>
				<li class="active"><a href="">1</a></li>
				<li><a href="">2</a></li>
				<li><a href="">3</a></li>
				<li><a href="">4</a></li>
				<li><a href="">5</a></li>
				<li><a href="">6</a></li>
				<li><a href="">7</a></li>
				<li><a href="">8</a></li>
				<li><a href="">9</a></li>
				<li><a href="">10</a></li>
				<li><a href="">下一页</a></li>
				<li><a href="">尾页</a></li>
			</ul>
		</div>
</div>

<script type="application/javascript">
	$(".child-items .child-items-ul li").click(function(){
		$(this).siblings().removeClass("active");
		$(this).addClass("active");
	})
</script>
</@html>
