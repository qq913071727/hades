<#macro header page_tab="">

<div class="header">
	<div class="row clearfix">
		<div class="header-logo fl">
			<img src="img/header-logo.jpg"/>
		</div>
		<div class="header-nav fl clearfix">
			<ul>
				<li class="active"><a href="">首页</a></li>
				<li class="li-line">|</li>
				<li><a href="">我的主页</a></li>
				<li class="li-line">|</li>
				<li><a href="">法律法规库</a></li>
			</ul>
		</div>
		<div class="header-upload fl">
			<a href="">我要上传</a>
		</div>
		<div class="header-login fr">
			<div class="header-login-user">
				张三
			</div>
			<ul class="login-drop-down">
				<li><a href="">审核</a></li>
				<li><a href="">退出</a></li>
			</ul>
		</div>
	</div>			
</div>

</#macro>