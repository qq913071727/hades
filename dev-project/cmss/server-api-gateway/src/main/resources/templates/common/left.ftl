<#macro left page_tab="">

<div class="container-left fl">

	<div class="tab clearfix">
        <ul>
            <li id="one1" onclick="setTab('one',1,2)" class="active">主题分类</li>
            <li id="one2" onclick="common.showCommonCategory();"  >一般分类</li>                  
        </ul>
    </div>
    
    <!-- 主题分类 -->
    <div id="tab_one_1" class="table-ol" style="display: block">
        <ul>
        	<#list themeCategoryList as themeCategory>
        		<li><a href="/cmss/themeCategoryMaterial/${themeCategory.id!}">${themeCategory.name!}</a></li>
        	</#list>
        </ul>
    </div>
    
    <!-- 一般分类 -->
    <div id="tab_one_2" class="table-ol" style="display: none">

    </div>		            
</div>

</#macro>