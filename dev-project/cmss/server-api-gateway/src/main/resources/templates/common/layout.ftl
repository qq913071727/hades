<#macro html page_title page_tab="">

<!doctype html>
<html lang="zh-CN">

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="renderer" content="webkit">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		${weibometa!}
		<title>${page_title}</title>
		
		<link rel="stylesheet" href="/cmss/static/css/bootstrap.min.css">
		<link rel="stylesheet" href="/cmss/static/css/flat-ui.min.css">
		<link rel="stylesheet" href="/cmss/static/css/normalize.css">
		<link rel="stylesheet" href="/cmss/static/css/style.css">
		<link rel="stylesheet" type="text/css" href="css/home-theme-search.css"/>
		
		<script src="/cmss/static/js/jquery-2.2.3.min.js"></script>
		<script src="/cmss/static/js/jquery.SuperSlide.2.1.1.js"></script>
		<script src="/cmss/static/js/bootstrap.min.js"></script>
		<script src="/cmss/static/js/main.js"></script>
		
		<script src="/cmss/static/js/util.js"></script>
		<script src="/cmss/static/js/common/common.js"></script>
	</head>
	
	<body>
	
		<#include "./header.ftl">
		<@header page_tab=page_tab/>
  
  		<div class="container">
			<div class="row clearfix">
				<#include "./left.ftl">
				<@left page_tab=page_tab/>
				
				
				<#nested />
				
				<#if (withoutRight!"false")!="true">
					<#include "./right.ftl">
					<@right page_tab=page_tab/>
				</#if>
			</div>
		</div>

		<#include "./footer.ftl">
		<@footer/>
	
	</body>
</html>

</#macro>