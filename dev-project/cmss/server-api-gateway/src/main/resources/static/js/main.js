
/*选项卡切换*/
function setTab(name, cursel, n) {
	for(i = 1; i <= n; i++) {
		var menu = document.getElementById(name + i);
		var con = document.getElementById("tab_" + name + "_" + i);
		menu.className = i == cursel ? "active" : "";
		con.style.display = i == cursel ? "block" : "none";
	}
}

function delRow(e){
	var parentElement = e.parentNode.parentNode;
    if(parentElement){
      	parentElement.removeChild(e.parentNode); 
    } 
}
$(function(){
				//  主题分类
				$(".upload-page-theme-tab ul li").click(function() {
					var this_index = $(this).index();
					$(this).siblings().removeClass("active");
					$(this).addClass("active");
					
					$(this).parents(".upload-page-theme-info").children(".tab_three-themecon").css("display","none");
					$(this).parents(".upload-page-theme-info").children(".tab_three-themecon").eq(this_index).css("display","block");
					
					$("#upload-page-childrentheme").show();
				})
				//  子项主题
				$(".tab_three-themecon table tr td").click(function(){
					var this_parentsId = $(this).parents(".tab_three-themecon").attr('id');
					console.log(this_parentsId);
					var new_parentsId ="#three" + this_parentsId.substr(10,1);
					
					var this_text = $(this).html();
					var this_parentsText = $(new_parentsId).html();					
					var new_text ="<span>" + this_parentsText +"-"+this_text+"<em onclick='delRow(this)'></em></span>";
					var themeAdd = $("#upload-page-theme-add").append(new_text);									
					
					
					$("#upload-page-childrentheme").hide();
					$(this).parents(".tab_three-themecon").hide();										
															
				})
					
				// 输入字数统计
				var lenInput1 = $('.upload-biaoti-input').val().length;
	            $(".upload-biaoti-input").keyup(function(){
	                lenInput1 = $(this).val().length;
	                if(lenInput1>0 && lenInput1<=50){
	                    $('.upload-biaoti-input-count').html(lenInput1);
	                    $('.upload-pagecon-submit').attr('disabled',false);
	                }else{
	                    $('.upload-pagecon-submit').attr('disabled',true);
	                }
	            });
				
				var lenInput2 = $('.fawenbianhao-input').val().length;
	            $(".fawenbianhao-input").keyup(function(){
	                lenInput2 = $(this).val().length;
	                if(lenInput2>0 && lenInput2<=50){
	                    $('.fawenbianhao-input-count').html(lenInput2);
	                    $('.upload-pagecon-submit').attr('disabled',false);
	                }else{
	                    $('.upload-pagecon-submit').attr('disabled',true);
	                }
	            });
				
				var lenInput3 = $('.upload-page-abstract-textarea').val().length;
	            $(".upload-page-abstract-textarea").keyup(function(){
	                lenInput3 = $(this).val().length;
	                if(lenInput3>0 && lenInput3<=200){
	                    $('.textareaInput').html(lenInput3);
	                    $('.upload-pagecon-submit').attr('disabled',false);
	                }else{
	                    $('.upload-pagecon-submit').attr('disabled',true);
	                }
	            });
						
				$(".upload-page-input .upload-page-input-radio").click(function(){
					$(this).toggleClass("upload-page-input-radio-change");
				})
				
				
})