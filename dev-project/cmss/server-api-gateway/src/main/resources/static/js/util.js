/**
 * 工具类
 */
var util={
	/**
	 * 判断是否是空数组
	 */
	isEmptyArray : function(value) {
		return (Array.isArray(value) && value.length === 0)
				|| (Object.prototype.isPrototypeOf(value) && Object.keys(value).length === 0);
	},

	/**
	 * 空的校验
	 */
	isNull : function(param) {
		if (typeof (param) == "undefined" || param == null || param == "null"
				|| param == "") {
			return true;
		} else {
			return false;
		}
	},
	
	/**
	 * 将时间戳（以毫秒为单位）转换为时间格式字符串
	 */
	timestampToDate : function(timestamp){
		var newDate = new Date();
		newDate.setTime(timestamp);
		return newDate.toLocaleString();
	}
}