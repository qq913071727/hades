var common={
		/**
		 * 单击一般分类选项卡
		 */
		showCommonCategory : function(){
			// 调用main.js文件中的setTab方法，切换到一般分类选项卡
			setTab('one',2,2);
			
			$.ajax({
	             type: "get",
	             url: "commonCategory",
	             dataType: "json",
	             success: function(data){
	            	 if(!util.isEmptyArray(data)){
	            		 var html="<ul>";
	            		 for(var i=0; i<data.length; i++){
	            			 html+="<li><a href=''>"+data[i].name+"</a></li>";
	            		 }
	            		 html+="</ul>";
	            		 $("#tab_one_2").append(html);
	            	 }
	             }
	         });
		},
		
		/**
		 * 绑定事件处理函数
		 */
		bindEventHandler : function(){
			
		}
};

/**
 * 入口函数
 */
$(function() {
	// 绑定事件处理函数
	common.bindEventHandler();
	
	
	
	
	
	
	
	
	
	
	
});