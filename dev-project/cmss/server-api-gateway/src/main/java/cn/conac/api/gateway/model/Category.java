/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.api.gateway.model;

import java.util.Set;

/**
 * 资料分类
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public class Category {

	private static final long serialVersionUID = -6427000865726076017L;
	
	/**
	 * 一般分类
	 */
	public static final String COMMON_CATEGORY_MATERIALS_ID="573e9c3fbe401ca6e053125011ac1a3e";
	
	/**
	 * 主题分类
	 */
	public static final String THEME_CATEGORY_MATERIALS_ID="573e9c3fbe411ca6e053125011ac1a3e";
	
	/** id */
	private String id;
	
	/** 资料分类名称 */
	private String name;
	
	/** 资料分类编码 */
	private String code;
	
	/** 层级 */
	private Integer level;
	
	/** 是否叶子节点 */
	private boolean leaf;
	
	/** 一般分类下的资料 */
	private Set<Material> commonCategoryMaterials;
	
	/** 主题分类下的资料 */
	private Set<Material> themeCategoryMaterials;
	
	/** 上级分类 */
	private Category parent;
	
	/** 下级分类 */
	private Set<Category> children;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public Set<Material> getCommonCategoryMaterials() {
		return commonCategoryMaterials;
	}

	public void setCommonCategoryMaterials(Set<Material> commonCategoryMaterials) {
		this.commonCategoryMaterials = commonCategoryMaterials;
	}

	public Set<Material> getThemeCategoryMaterials() {
		return themeCategoryMaterials;
	}

	public void setThemeCategoryMaterials(Set<Material> themeCategoryMaterials) {
		this.themeCategoryMaterials = themeCategoryMaterials;
	}

	public Category getParent() {
		return parent;
	}

	public void setParent(Category parent) {
		this.parent = parent;
	}

	public Set<Category> getChildren() {
		return children;
	}

	public void setChildren(Set<Category> children) {
		this.children = children;
	}
}
