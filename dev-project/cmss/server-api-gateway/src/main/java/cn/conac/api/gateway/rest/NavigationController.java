package cn.conac.api.gateway.rest;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.conac.api.gateway.model.Category;
import cn.conac.api.gateway.service.CmssService;

/**
 * 导航控制器类
 * @author lishen
 * @date	2017年8月24日下午4:54:08
 * @version 1.0
 */
@RestController
public class NavigationController {
	/**
     * 日志记录器
     */
    private static Logger logger = LoggerFactory.getLogger(NavigationController.class);
    
    @Autowired
	private CmssService cmssService;
    
    /**
     * 进入首页，加载主题分类的所有一级子类
     * @param model
     * @return
     */
	@RequestMapping(value="", method = RequestMethod.GET)  
    public ModelAndView index(Model model){
		logger.info("method index begin");
		
		JSONObject themeCategoryJSONOjbect=cmssService.findThemeCategoryByParentId(Category.THEME_CATEGORY_MATERIALS_ID);
		List<Category> themeCategoryList=JSONArray.parseArray((String)themeCategoryJSONOjbect.get("message"), cn.conac.api.gateway.model.Category.class);
		model.addAttribute("themeCategoryList", themeCategoryList);
		
		ModelAndView mv = new ModelAndView("index");
		
		logger.info("进入首页，加载主题分类的所有一级子类");
		return mv;
    }

}
