package cn.conac.api.gateway.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

/**
 * 本地数据库操作用Service
 * @author lishen
 * @date	2017年8月25日上午10:15:40
 * @version 1.0
 */
@FeignClient("service-cmss")
public interface CmssService {

	/**
	 * 查找主题分类的所有一级子类
	 * @param parentId
	 * @return
	 */
    @RequestMapping(method = RequestMethod.GET, value = "/themeCategory/{parentId}")
    public JSONObject findThemeCategoryByParentId(@PathVariable("parentId") String parentId);
    
    /**
     *   查找一般分类的所有一级子类
     * @param parentId
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/commonCategory/{parentId}")
    public JSONObject findCommonCategoryByParentId(@PathVariable("parentId") String parentId);
    
    /**
     * 查找某个主题分类相关的材料
     * @param parentId
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/themeCategoryMaterial/{themeCategoryId}")
    public JSONObject findThemeCategoryMaterial(@PathVariable("themeCategoryId") String themeCategoryId);

}
