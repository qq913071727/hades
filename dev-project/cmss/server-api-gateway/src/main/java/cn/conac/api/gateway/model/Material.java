/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.api.gateway.model;

import java.util.Date;
import java.util.Set;

/**
 * 资料信息
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public class Material {

	private static final long serialVersionUID = -2978307311442772004L;
	
	/** 资料名称 */
	private String name;
	
	/** 标题 */
	private String title;
	
	/** 发文编号 */
	private String materialNumber;
	
	/** 摘要 */
	private String summary;
	
	/** 资料存放路径（全路径） */
	private String path;
	
	/** 扩展名 */
	private String extension;
	
	/** 点击量、浏览次数 */
	private Long hits = 0L;
	
	/** 下载量 */
	private Long downloads = 0L;
	
	/** 上传时间 */
	private Date uploadDate;
	
	/** 是否是个人资料 */
	private boolean privateFlag = false;
	
	/** 是否匿名上传 */
	private boolean anonymous = false;
	
	/** 是否已建立全文索引 */
	private boolean indexFlag = true;
	
	/** 是否需要审核（新上传的都需要审核） */
	private boolean needCheck = true;
	
	/** 是否已审核 */
	private boolean checked = false;
	
	/** 是否需要纠错（非用户选择的主题需要显示纠错按钮） */
	private boolean needMistake = false;
	
	/** 资料评分 */
	private double score = 1.0;
	
	/** 上传人 */
//	private User uploadUser;
//	
//	/** 最后修改人、审核人 */
//	private User updateUser;
	
	/** 分类的标记（不明确分类时给出的猜测的分类） */
	private String categoryMark;
	
//	/** 试用地区 */
//	private Area suitableArea;
	
	/** 一般分类 */
	private Set<Category> commonCategorys;
	
	/** 主题分类 */
	private Set<Category> themeCategorys;
	
//	/** 意见反馈 */
//	private Set<Suggestion> suggestions;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMaterialNumber() {
		return materialNumber;
	}

	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public Long getHits() {
		return hits;
	}

	public void setHits(Long hits) {
		this.hits = hits;
	}

	public Long getDownloads() {
		return downloads;
	}

	public void setDownloads(Long downloads) {
		this.downloads = downloads;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public boolean isPrivateFlag() {
		return privateFlag;
	}

	public void setPrivateFlag(boolean privateFlag) {
		this.privateFlag = privateFlag;
	}

	public boolean isAnonymous() {
		return anonymous;
	}

	public void setAnonymous(boolean anonymous) {
		this.anonymous = anonymous;
	}

	public boolean isIndexFlag() {
		return indexFlag;
	}

	public void setIndexFlag(boolean indexFlag) {
		this.indexFlag = indexFlag;
	}

	public boolean isNeedCheck() {
		return needCheck;
	}

	public void setNeedCheck(boolean needCheck) {
		this.needCheck = needCheck;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	public boolean isNeedMistake() {
		return needMistake;
	}

	public void setNeedMistake(boolean needMistake) {
		this.needMistake = needMistake;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

//	public User getUploadUser() {
//		return uploadUser;
//	}
//
//	public void setUploadUser(User uploadUser) {
//		this.uploadUser = uploadUser;
//	}
//
//	public User getUpdateUser() {
//		return updateUser;
//	}
	
	public String getCategoryMark() {
		return categoryMark;
	}

	public void setCategoryMark(String categoryMark) {
		this.categoryMark = categoryMark;
	}

//	public void setUpdateUser(User updateUser) {
//		this.updateUser = updateUser;
//	}
//
//	public Area getSuitableArea() {
//		return suitableArea;
//	}
//
//	public void setSuitableArea(Area suitableArea) {
//		this.suitableArea = suitableArea;
//	}

	public Set<Category> getCommonCategorys() {
		return commonCategorys;
	}

	public void setCommonCategorys(Set<Category> commonCategorys) {
		this.commonCategorys = commonCategorys;
	}

	public Set<Category> getThemeCategorys() {
		return themeCategorys;
	}

	public void setThemeCategorys(Set<Category> themeCategorys) {
		this.themeCategorys = themeCategorys;
	}

//	public Set<Suggestion> getSuggestions() {
//		return suggestions;
//	}
//
//	public void setSuggestions(Set<Suggestion> suggestions) {
//		this.suggestions = suggestions;
//	}

}
