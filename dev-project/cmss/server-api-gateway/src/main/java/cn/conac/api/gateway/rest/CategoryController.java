package cn.conac.api.gateway.rest;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

import cn.conac.api.gateway.model.Category;
import cn.conac.api.gateway.service.CmssService;

/**
 * Category类的控制器
 * @author lishen
 * @date	2017年8月25日下午2:19:59
 * @version 1.0
 */
@RestController
public class CategoryController {
	/**
     * 日志记录器
     */
    private static Logger logger = LoggerFactory.getLogger(CategoryController.class);
    
    @Autowired
	private CmssService cmssService;

    /**
     * 查询一般分类所有一级子类
     */
	@RequestMapping(value = "/commonCategory",method = RequestMethod.GET)
	public String commonCategory(Model model) {
		logger.info("method commonCategory begin");
		
		JSONObject conmnonCategoryJSONOjbect=cmssService.findCommonCategoryByParentId(Category.COMMON_CATEGORY_MATERIALS_ID);
		String message=(String) conmnonCategoryJSONOjbect.get("message");
		String jsonString=JSON.toJSONString(message, SerializerFeature.DisableCircularReferenceDetect).replace("\\", "");
		String result=jsonString.substring(1).substring(0, jsonString.length()-2);
		
		logger.info("查询一般分类所有一级子类");
		return result;
    }
	
}
