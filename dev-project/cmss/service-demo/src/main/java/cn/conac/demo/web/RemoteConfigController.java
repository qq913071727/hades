/*
 * Copyright 2002-2020 the original author or authors.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.conac.demo.web;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author jiangdx
 * @version 2017年5月25日    v 1.0  
 */
@RefreshScope
@Controller
public class RemoteConfigController {
	@Value("${mysqldb.datasource.url}")
	private String value;
	
	/**
	 * 从远程git库取得配置信息
	 */
	@Value("${logging.level.org.springframework.web}")
	private String value1;
	
	@ResponseBody
	@RequestMapping("/greating")
	public String index() {
		return "Greetings from Spring Boot!";
	}
	
	@ResponseBody
	@RequestMapping("/hello/{param}")
	public String hello(@PathVariable String param){
		return "I got the param : " + param;
	}
	
	@ResponseBody
	@RequestMapping("/config")
	public String getConfig(){
		return value + "----" + value1;
	}
	
	/**
	 * Map必须放到参数中Freemarker才识别？
	 * @param data
	 * @return
	 */
	@RequestMapping("/hello")
	public String hello(Map<String,Object> data){
		data.put("name", "姜殿祥");
		return "tmp-hello";
	}
}
