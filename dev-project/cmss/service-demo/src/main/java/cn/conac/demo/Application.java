/*
 * Copyright 2002-2020 the original author or authors.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.conac.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import cn.conac.framework.domain.impl.BaseRepositoryImpl;

/**
 * spring boot 服务启动入口
 * SpringBootApplication 注解等价于Configuration、EnableAutoConfiguration、ComponentScan三者之和
 * @author jiangdx
 * @version 2017年5月25日    v 1.0  
 */
@EnableDiscoveryClient
@SpringBootApplication
@EnableJpaRepositories(repositoryBaseClass = BaseRepositoryImpl.class)
public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
