/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.demo.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.framework.domain.BaseEntity;

/**
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
@Entity
@Table(name = "BDT_USER")
public class User extends BaseEntity {

	private static final long serialVersionUID = -3897748044997748484L;
	
	private String name;
	
	private Integer age;
	
	
	/**
	 * @param name
	 * @param age
	 */
	public User(String name, Integer age) {
		super();
		this.name = name;
		this.age = age;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the age
	 */
	public Integer getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(Integer age) {
		this.age = age;
	}

}
