/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.demo.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import cn.conac.framework.domain.BaseEntity;

/**
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
@Entity
@Table(name = "BDT_ROLE")
public class Role extends BaseEntity {

	private static final long serialVersionUID = -3897748044997748484L;
	
	private String name;
	
	
	
	public Role() {
		super();
	}

	/**
	 * @param name
	 */
	public Role(String name) {
		super();
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}
