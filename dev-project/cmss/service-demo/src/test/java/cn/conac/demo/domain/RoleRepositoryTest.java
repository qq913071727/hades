/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.demo.domain;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import cn.conac.framework.Filter;

/**
 * 
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RoleRepositoryTest {
	@Resource(name = "roleRepository")
	private RoleRepository roleRepository;
	
	/**
	 * 加上@Transactional可回滚事务,保证测试数据不污染数据库
	 */
	@Test
	@Transactional  
	public void test() {
		Role r1 = new Role("admin");
		Role r2 = new Role("customer");
		roleRepository.save(r1);
		roleRepository.save(r2);
		
		long count = roleRepository.count(new Filter());
		
		long count1 = roleRepository.count();
		
		System.out.println(count);
		System.out.println(count1);
		
		r1 = roleRepository.findOne(r1.getId());
		r2 = roleRepository.findOne(r2.getId());
		
		System.out.println(r1.toJson());
		System.out.println(r2.toJson());
		
	}

}
