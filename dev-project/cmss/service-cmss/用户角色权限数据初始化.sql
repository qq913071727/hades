/**
 * 初始化角色
 */
insert into CMSS_ROLE (ID, CODE, NAME, CREATE_DATE, MODIFY_DATE)
values ('574036ad5566210be053125011ac514e', 'ROLE_ADMIN', '系统管理员', to_date('21-08-2017 16:37:28', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 16:37:28', 'dd-mm-yyyy hh24:mi:ss'));

insert into CMSS_ROLE (ID, CODE, NAME, CREATE_DATE, MODIFY_DATE)
values ('574036ad5567210be053125011ac514e', 'ROLE_LOCAL', '地方编办用户', to_date('21-08-2017 16:37:28', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 16:37:28', 'dd-mm-yyyy hh24:mi:ss'));

insert into CMSS_ROLE (ID, CODE, NAME, CREATE_DATE, MODIFY_DATE)
values ('574036ad5568210be053125011ac514e', 'ROLE_CONAC', 'CONAC管理员', to_date('21-08-2017 16:37:28', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 16:37:28', 'dd-mm-yyyy hh24:mi:ss'));

/**
 * 初始化权限
 */
insert into CMSS_AUTHORITY (ID, CODE, NAME, CREATE_DATE, MODIFY_DATE)
values ('57406e06ad3721c1e053125011ac4645', 'AUTH_AUDIT', '审核权限', to_date('21-08-2017 16:40:24', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 16:40:24', 'dd-mm-yyyy hh24:mi:ss'));

insert into CMSS_AUTHORITY (ID, CODE, NAME, CREATE_DATE, MODIFY_DATE)
values ('57406e06ad3821c1e053125011ac4645', 'AUTH_UPLOAD', '资料上传权限', to_date('21-08-2017 16:40:24', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 16:40:24', 'dd-mm-yyyy hh24:mi:ss'));

/**
 * 初始化角色权限中间表
 */
insert into CMSS_ROLE_AUTHORITY (ID, ROLE_ID, AUTHORITY_ID)
values ('574077e726ea21d7e053125011ac8f45', '574036ad5566210be053125011ac514e', '57406e06ad3721c1e053125011ac4645');

insert into CMSS_ROLE_AUTHORITY (ID, ROLE_ID, AUTHORITY_ID)
values ('574077e726eb21d7e053125011ac8f45', '574036ad5566210be053125011ac514e', '57406e06ad3821c1e053125011ac4645');

insert into CMSS_ROLE_AUTHORITY (ID, ROLE_ID, AUTHORITY_ID)
values ('574077e726ec21d7e053125011ac8f45', '574036ad5568210be053125011ac514e', '57406e06ad3721c1e053125011ac4645');

insert into CMSS_ROLE_AUTHORITY (ID, ROLE_ID, AUTHORITY_ID)
values ('574077e726ed21d7e053125011ac8f45', '574036ad5568210be053125011ac514e', '57406e06ad3821c1e053125011ac4645');

insert into CMSS_ROLE_AUTHORITY (ID, ROLE_ID, AUTHORITY_ID)
values ('574077e726ee21d7e053125011ac8f45', '574036ad5567210be053125011ac514e', '57406e06ad3821c1e053125011ac4645');



