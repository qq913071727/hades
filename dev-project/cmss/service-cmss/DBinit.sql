
/*==============================================================*/
/* Table: CMSS_USER                                             */
/*==============================================================*/

drop index IDX_USER_USER_ID;

drop table CMSS_USER cascade constraints;

create table CMSS_USER 
(
   ID                   VARCHAR2(32)         not null,
   USER_ID              NUMBER               not null,
   NAME                 VARCHAR2(50),
   AREA_ID              VARCHAR2(32),
   MATERIAL_AMOUNT      NUMBER,
   MATERIAL_HITS        NUMBER,
   MATERIAL_DOWNLOADS   NUMBER,
   IS_DELETE        NUMBER					 not null,
   IS_LOCKED        NUMBER					 not null,
   IS_ENABLED        NUMBER					 not null,
   CREATE_DATE          DATE				 not null,
   MODIFY_DATE          DATE				 not null,
   constraint PK_CMSS_USER primary key (ID)
);

comment on table CMSS_USER is
'用户表';

comment on column CMSS_USER.USER_ID is
'红云用户ID';

comment on column CMSS_USER.NAME is
'用户昵称';

comment on column CMSS_USER.AREA_ID is
'地区ID';

comment on column CMSS_USER.MATERIAL_AMOUNT is
'文档数量';

comment on column CMSS_USER.MATERIAL_HITS is
'资料被浏览次数';

comment on column CMSS_USER.MATERIAL_DOWNLOADS is
'资料被下载次数';

comment on column CMSS_USER.IS_DELETE is
'是否删除';

comment on column CMSS_USER.IS_LOCKED is
'是否锁定';

comment on column CMSS_USER.IS_ENABLED is
'是否启用';

/*==============================================================*/
/* Index: IDX_USER_USER_ID                                      */
/*==============================================================*/
create unique index IDX_USER_USER_ID on CMSS_USER (
   USER_ID ASC
);


/*==============================================================*/
/* Table: CMSS_MATERIAL                                         */
/*==============================================================*/
drop index IDX_MATERIAL_SUITABLE_AREA_ID;

drop index IDX_MATERIAL_MATERIAL_NUMBER;

drop table CMSS_MATERIAL cascade constraints;

create table CMSS_MATERIAL 
(
   ID                   VARCHAR(32)          not null,
   NAME                 VARCHAR(200)         not null,
   TITLE                VARCHAR(500)         not null,
   MATERIAL_NUMBER      VARCHAR(50),
   SUMMARY              VARCHAR(1000),
   SUITABLE_AREA_ID     VARCHAR(32),
   PATH                 VARCHAR(300)         not null,
   EXTENSION            VARCHAR(5)           not null,
   HITS                 NUMBER               not null,
   DOWNLOADS            NUMBER               not null,
   UPLOAD_USER          VARCHAR(32),
   UPLOAD_DATE          DATE,
   UPDATE_USER          VARCHAR(32),
   UPDATE_DATE          DATE,
   CREATE_DATE          DATE				 not null,
   MODIFY_DATE          DATE				 not null,
   IS_PRIVATE_FLAG      NUMBER               not null,
   IS_ANONYMOUS         NUMBER               not null,
   IS_INDEX_FLAG        NUMBER               not null,
   IS_NEED_MISTAKE      NUMBER               not null,
   IS_NEED_CHECK        NUMBER               not null,
   IS_CHECKED           NUMBER               not null,
   SCORE                NUMBER(2,1),
   CATEGORY_MARK        VARCHAR(100),
   constraint PK_CMSS_MATERIAL primary key (ID)
);

comment on table CMSS_MATERIAL is
'资料';

comment on column CMSS_MATERIAL.NAME is
'资料名称';

comment on column CMSS_MATERIAL.TITLE is
'资料标题';

comment on column CMSS_MATERIAL.MATERIAL_NUMBER is
'发文编号';

comment on column CMSS_MATERIAL.SUMMARY is
'摘要';

comment on column CMSS_MATERIAL.SUITABLE_AREA_ID is
'适用地区ID';

comment on column CMSS_MATERIAL.PATH is
'文件存放全路径';

comment on column CMSS_MATERIAL.EXTENSION is
'扩展名';

comment on column CMSS_MATERIAL.HITS is
'点击量（浏览次数）';

comment on column CMSS_MATERIAL.DOWNLOADS is
'下载次数';

comment on column CMSS_MATERIAL.UPLOAD_USER is
'上传人';

comment on column CMSS_MATERIAL.UPLOAD_DATE is
'上传日期';

comment on column CMSS_MATERIAL.UPDATE_USER is
'最后修改人';

comment on column CMSS_MATERIAL.UPDATE_DATE is
'最后修改时间';

comment on column CMSS_MATERIAL.IS_PRIVATE_FLAG is
'是否是个人资料';

comment on column CMSS_MATERIAL.IS_ANONYMOUS is
'是否匿名';

comment on column CMSS_MATERIAL.IS_INDEX_FLAG is
'是否已建立全文索引';

comment on column CMSS_MATERIAL.IS_NEED_MISTAKE is
'是否需要纠错';

comment on column CMSS_MATERIAL.IS_NEED_CHECK is
'是否需要审核（新发布的需要审核）';

comment on column CMSS_MATERIAL.IS_CHECKED is
'是否已审核';

comment on column CMSS_MATERIAL.SCORE is
'评分';

comment on column CMSS_MATERIAL.CATEGORY_MARK is
'分类的标记（不明确分类时给出的猜测的分类）';

/*==============================================================*/
/* Index: IDX_MATERIAL_MATERIAL_NUMBER                          */
/*==============================================================*/
create unique index IDX_MATERIAL_MATERIAL_NUMBER on CMSS_MATERIAL (
   MATERIAL_NUMBER ASC
);

/*==============================================================*/
/* Index: IDX_MATERIAL_SUITABLE_AREA_ID                         */
/*==============================================================*/
create index IDX_MATERIAL_SUITABLE_AREA_ID on CMSS_MATERIAL (
   SUITABLE_AREA_ID ASC
);



/*==============================================================*/
/* Table: CMSS_AREA                                             */
/*==============================================================*/
drop index IDX_AREA_CODE;

drop index IDX_AREA_NAME;

drop table CMSS_AREA cascade constraints;

create table CMSS_AREA 
(
   ID                   VARCHAR(32)          not null,
   PARENT_ID            VARCHAR(32)          not null,
   CODE                 VARCHAR(10)          not null,
   NAME                 VARCHAR(50)          not null,
   WEIGHT               NUMBER,
   LVL                  NUMBER               not null,
   IS_LEAF              NUMBER               not null,
   CREATE_DATE          DATE				 not null,
   MODIFY_DATE          DATE				 not null,
   constraint PK_CMSS_AREA primary key (ID)
);

comment on table CMSS_AREA is
'地区表';

comment on column CMSS_AREA.PARENT_ID is
'上级地区';

comment on column CMSS_AREA.CODE is
'编码';

comment on column CMSS_AREA.NAME is
'地区名称';

comment on column CMSS_AREA.WEIGHT is
'权重';

comment on column CMSS_AREA.LVL is
'级别';

comment on column CMSS_AREA.IS_LEAF is
'是否叶子节点';

/*==============================================================*/
/* Index: IDX_AREA_NAME                                         */
/*==============================================================*/
create index IDX_AREA_NAME on CMSS_AREA (
   NAME ASC
);

/*==============================================================*/
/* Index: IDX_AREA_CODE                                         */
/*==============================================================*/
create unique index IDX_AREA_CODE on CMSS_AREA (
   CODE ASC
);


/*==============================================================*/
/* Table: CMSS_FAVORITE                                         */
/*==============================================================*/
drop table CMSS_FAVORITE cascade constraints;

create table CMSS_FAVORITE 
(
   ID                   VARCHAR(32)          not null,
   USER_ID              VARCHAR(32)          not null,
   MATERIAL_ID          VARCHAR(32)          not null,
   CREATE_DATE          DATE				 not null,
   MODIFY_DATE          DATE				 not null,
   constraint PK_CMSS_FAVORITE primary key (ID)
);

comment on table CMSS_FAVORITE is
'收藏夹';

comment on column CMSS_FAVORITE.USER_ID is
'用户ID';

comment on column CMSS_FAVORITE.MATERIAL_ID is
'资料ID';

comment on column CMSS_FAVORITE.CREATE_DATE is
'创建时间';


/*==============================================================*/
/* Table: CMSS_USER_ROLE                                        */
/*==============================================================*/
drop table CMSS_USER_ROLE cascade constraints;

create table CMSS_USER_ROLE 
(
   ID                   VARCHAR(32)          not null,
   USER_ID              VARCHAR(32)          not null,
   ROLE_ID              VARCHAR(32)          not null,
   constraint PK_CMSS_USER_ROLE primary key (ID)
);

comment on table CMSS_USER_ROLE is
'用户角色中间表';


/*==============================================================*/
/* Table: CMSS_ROLE                                             */
/*==============================================================*/
drop table CMSS_ROLE cascade constraints;

create table CMSS_ROLE 
(
   ID                   VARCHAR(32)          not null,
   CODE                 VARCHAR(20)          not null,
   NAME                 VARCHAR(45)          not null,
   CREATE_DATE          DATE				 not null,
   MODIFY_DATE          DATE				 not null,
   constraint PK_CMSS_ROLE primary key (ID),
   constraint UK_CMSS_ROLE_CODE unique (CODE)
);

comment on table CMSS_ROLE is
'角色';

comment on column CMSS_ROLE.CODE is
'角色编码';

comment on column CMSS_ROLE.NAME is
'角色名称';


/*==============================================================*/
/* Table: CMSS_ROLE_AUTHORITY                                   */
/*==============================================================*/

drop table CMSS_ROLE_AUTHORITY cascade constraints;

create table CMSS_ROLE_AUTHORITY 
(
   ID                   VARCHAR(32)          not null,
   ROLE_ID              VARCHAR(32)          not null,
   AUTHORITY_ID         VARCHAR(32)          not null,
   constraint PK_CMSS_ROLE_AUTHORITY primary key (ID)
);

comment on table CMSS_ROLE_AUTHORITY is
'角色权限中间表';



/*==============================================================*/
/* Table: CMSS_AUTHORITY                                        */
/*==============================================================*/
drop table CMSS_AUTHORITY cascade constraints;

create table CMSS_AUTHORITY 
(
   ID                   VARCHAR(32)          not null,
   CODE                 VARCHAR(20)          not null,
   NAME                 VARCHAR(45)          not null,
   CREATE_DATE          DATE				 not null,
   MODIFY_DATE          DATE				 not null,
   constraint PK_CMSS_AUTHORITY primary key (ID),
    constraint UK_CMSS_AUTHORITY_CODE unique (CODE)
);

comment on table CMSS_AUTHORITY is
'权限';

comment on column CMSS_AUTHORITY.CODE is
'编码';

comment on column CMSS_AUTHORITY.NAME is
'权限名称';



/*==============================================================*/
/* Table: CMSS_SUGGESTION                                       */
/*==============================================================*/
drop table CMSS_SUGGESTION cascade constraints;

create table CMSS_SUGGESTION 
(
   ID                   VARCHAR(32)          not null,
   USER_ID              VARCHAR(32)          not null,
   MATERIAL_ID          VARCHAR(32)          not null,
   SUGGESTION           VARCHAR(500)         not null,
   IS_CONFIM            NUMBER               not null,
   CREATE_DATE          DATE				 not null,
   MODIFY_DATE          DATE				 not null,
   constraint PK_CMSS_SUGGESTION primary key (ID)
);

comment on table CMSS_SUGGESTION is
'意见表（纠错反馈）';

comment on column CMSS_SUGGESTION.USER_ID is
'用户ID';

comment on column CMSS_SUGGESTION.MATERIAL_ID is
'资料ID';

comment on column CMSS_SUGGESTION.SUGGESTION is
'意见反馈';

comment on column CMSS_SUGGESTION.CREATE_DATE is
'反馈时间';

comment on column CMSS_SUGGESTION.IS_CONFIM is
'是否确认';


/*==============================================================*/
/* Table: CMSS_SCORE                                            */
/*==============================================================*/
drop table CMSS_SCORE cascade constraints;

create table CMSS_SCORE 
(
   ID                   VARCHAR(32)          not null,
   MATERIAL_ID          VARCHAR(32)          not null,
   USER_ID              VARCHAR(32)          not null,
   SCORE                NUMBER(1,1)          not null,
   CREATE_DATE          DATE				 not null,
   MODIFY_DATE          DATE				 not null,
   constraint PK_CMSS_SCORE primary key (ID)
);

comment on table CMSS_SCORE is
'评分表';

comment on column CMSS_SCORE.MATERIAL_ID is
'资料ID';

comment on column CMSS_SCORE.USER_ID is
'用户ID';

comment on column CMSS_SCORE.SCORE is
'评分';

comment on column CMSS_SCORE.CREATE_DATE is
'创建时间';


/*==============================================================*/
/* Table: CMSS_CATEGORY                                         */
/*==============================================================*/
drop table CMSS_CATEGORY cascade constraints;

create table CMSS_CATEGORY 
(
   ID                   VARCHAR(32)          not null,
   PARENT_ID            VARCHAR(32),
   NAME                 VARCHAR(45)          not null,
   CODE                 VARCHAR(20)          not null,
   LVL                  NUMBER               not null,
   IS_LEAF              NUMBER               not null,
   ORDERS               NUMBER,
   CREATE_DATE          DATE				 not null,
   MODIFY_DATE          DATE				 not null,
   constraint PK_CMSS_CATEGORY primary key (ID)
);

comment on table CMSS_CATEGORY is
'资料分类';

comment on column CMSS_CATEGORY.PARENT_ID is
'父级节点ID';

comment on column CMSS_CATEGORY.NAME is
'分类名称';

comment on column CMSS_CATEGORY.CODE is
'分类编码';

comment on column CMSS_CATEGORY.LVL is
'级别';

comment on column CMSS_CATEGORY.IS_LEAF is
'是否是叶子节点';
comment on column CMSS_CATEGORY.ORDERS is
'排序';


/*==============================================================*/
/* Table: CMSS_MATERIAL_CATEGORY                                */
/*==============================================================*/
drop table CMSS_MATERIAL_CATEGORY cascade constraints;

create table CMSS_MATERIAL_CATEGORY 
(
   MATERIAL_ID          VARCHAR(32)          not null,
   CATEGORY_ID          VARCHAR(32)          not null,
   ID                   VARCHAR(32)          not null,
   constraint PK_CMSS_MATERIAL_CATEGORY primary key (MATERIAL_ID)
);

comment on table CMSS_MATERIAL_CATEGORY is
'资料主题中间表';

