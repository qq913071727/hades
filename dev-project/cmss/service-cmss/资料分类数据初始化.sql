insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc73d1d8ee053125011ac733f', '573e9c3fbe401ca6e053125011ac1a3e', '三定规定', '100-10', 1, 1, to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), 10);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc73e1d8ee053125011ac733f', '573e9c3fbe401ca6e053125011ac1a3e', '政策法规', '100-20', 1, 1, to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), 20);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7401d8ee053125011ac733f', '573e9c3fbe401ca6e053125011ac1a3e', '经验材料', '100-30', 1, 0, to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), 30);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7421d8ee053125011ac733f', '573e9c3fbe401ca6e053125011ac1a3e', '理论研究', '100-40', 1, 1, to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), 40);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7431d8ee053125011ac733f', '573e9c3fbe401ca6e053125011ac1a3e', '编制常识', '100-50', 1, 1, to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), 50);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7461d8ee053125011ac733f', '573e9c3fbe401ca6e053125011ac1a3e', '政策解读', '100-60', 1, 1, to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), 60);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7471d8ee053125011ac733f', '573e9c3fbe401ca6e053125011ac1a3e', '历史资料', '100-70', 1, 1, to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), 70);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc74a1d8ee053125011ac733f', '573e9c3fbe401ca6e053125011ac1a3e', '统计数据', '100-80', 1, 1, to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), 80);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc74b1d8ee053125011ac733f', '573e9c3fbe401ca6e053125011ac1a3e', '其他资料', '100-90', 1, 1, to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 14:56:39', 'dd-mm-yyyy hh24:mi:ss'), 90);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc74c1d8ee053125011ac733f', '573e9c3fbe411ca6e053125011ac1a3e', '行政体制改革', '200-10', 1, 0, to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), 10);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc74d1d8ee053125011ac733f', '573e9c3fbe411ca6e053125011ac1a3e', '事业单位改革', '200-20', 1, 0, to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), 20);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc74e1d8ee053125011ac733f', '573e9c3fbe411ca6e053125011ac1a3e', '放管服', '200-30', 1, 0, to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), 30);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc74f1d8ee053125011ac733f', '573e9c3fbe411ca6e053125011ac1a3e', '机构编制管理', '200-40', 1, 0, to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), 40);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7501d8ee053125011ac733f', '573e9c3fbe411ca6e053125011ac1a3e', '监督检查', '200-50', 1, 0, to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), 50);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7511d8ee053125011ac733f', '573e9c3fbe411ca6e053125011ac1a3e', '电子政务', '200-60', 1, 0, to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), 60);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7521d8ee053125011ac733f', '573e9c3fbe411ca6e053125011ac1a3e', '综合改革', '200-70', 1, 0, to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), 70);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7531d8ee053125011ac733f', '573e9c3fbe411ca6e053125011ac1a3e', '事业单位登记管理', '200-80', 1, 0, to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), 80);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7541d8ee053125011ac733f', '573e9c3fbe411ca6e053125011ac1a3e', '自身建设', '200-90', 1, 0, to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), 90);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7551d8ee053125011ac733f', '573e9c3fbe411ca6e053125011ac1a3e', '其他', '200-100', 1, 1, to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:06:46', 'dd-mm-yyyy hh24:mi:ss'), 100);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7561d8ee053125011ac733f', '573efaffc74c1d8ee053125011ac733f', '权责清单', '200-10-10', 2, 1, to_date('21-08-2017 15:22:34', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:22:34', 'dd-mm-yyyy hh24:mi:ss'), 10);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7571d8ee053125011ac733f', '573efaffc74c1d8ee053125011ac733f', '综合行政执法', '200-10-20', 2, 1, to_date('21-08-2017 15:22:34', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:22:34', 'dd-mm-yyyy hh24:mi:ss'), 20);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7581d8ee053125011ac733f', '573efaffc74c1d8ee053125011ac733f', '经济发达镇行政管理体制', '200-10-30', 2, 1, to_date('21-08-2017 15:22:34', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:22:34', 'dd-mm-yyyy hh24:mi:ss'), 30);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7591d8ee053125011ac733f', '573efaffc74c1d8ee053125011ac733f', '领域体制改革', '200-10-40', 2, 1, to_date('21-08-2017 15:22:34', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:22:34', 'dd-mm-yyyy hh24:mi:ss'), 40);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc75a1d8ee053125011ac733f', '573efaffc74c1d8ee053125011ac733f', '其他', '200-10-50', 2, 1, to_date('21-08-2017 15:22:34', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:22:34', 'dd-mm-yyyy hh24:mi:ss'), 50);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc75b1d8ee053125011ac733f', '573efaffc74d1d8ee053125011ac733f', '承担行政职能事业单位改革', '200-20-10', 2, 1, to_date('21-08-2017 15:26:06', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:26:06', 'dd-mm-yyyy hh24:mi:ss'), 10);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc75c1d8ee053125011ac733f', '573efaffc74d1d8ee053125011ac733f', '生产经营事业单位改革', '200-20-20', 2, 1, to_date('21-08-2017 15:26:06', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:26:06', 'dd-mm-yyyy hh24:mi:ss'), 20);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc75d1d8ee053125011ac733f', '573efaffc74d1d8ee053125011ac733f', '公益类事业单位体制改革', '200-20-30', 2, 1, to_date('21-08-2017 15:26:06', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:26:06', 'dd-mm-yyyy hh24:mi:ss'), 30);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc75e1d8ee053125011ac733f', '573efaffc74d1d8ee053125011ac733f', '其他', '200-20-40', 2, 1, to_date('21-08-2017 15:26:06', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:26:06', 'dd-mm-yyyy hh24:mi:ss'), 40);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc75f1d8ee053125011ac733f', '573efaffc74e1d8ee053125011ac733f', '简政放权', '200-30-10', 2, 1, to_date('21-08-2017 15:28:36', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:28:36', 'dd-mm-yyyy hh24:mi:ss'), 10);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7601d8ee053125011ac733f', '573efaffc74e1d8ee053125011ac733f', '放管结合', '200-30-20', 2, 1, to_date('21-08-2017 15:28:36', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:28:36', 'dd-mm-yyyy hh24:mi:ss'), 20);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7611d8ee053125011ac733f', '573efaffc74e1d8ee053125011ac733f', '优化服务', '200-30-30', 2, 1, to_date('21-08-2017 15:28:36', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:28:36', 'dd-mm-yyyy hh24:mi:ss'), 30);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7621d8ee053125011ac733f', '573efaffc74e1d8ee053125011ac733f', '其他', '200-30-40', 2, 1, to_date('21-08-2017 15:28:36', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:28:36', 'dd-mm-yyyy hh24:mi:ss'), 40);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7631d8ee053125011ac733f', '573efaffc74f1d8ee053125011ac733f', '减编控编', '200-40-10', 2, 1, to_date('21-08-2017 15:31:11', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:31:11', 'dd-mm-yyyy hh24:mi:ss'), 10);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7641d8ee053125011ac733f', '573efaffc74f1d8ee053125011ac733f', '事业单位挖潜创新', '200-40-20', 2, 1, to_date('21-08-2017 15:31:11', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:31:11', 'dd-mm-yyyy hh24:mi:ss'), 20);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7651d8ee053125011ac733f', '573efaffc74f1d8ee053125011ac733f', '职能交叉', '200-40-30', 2, 1, to_date('21-08-2017 15:31:11', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:31:11', 'dd-mm-yyyy hh24:mi:ss'), 30);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7661d8ee053125011ac733f', '573efaffc74f1d8ee053125011ac733f', '机构履职', '200-40-40', 2, 1, to_date('21-08-2017 15:31:11', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:31:11', 'dd-mm-yyyy hh24:mi:ss'), 40);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7671d8ee053125011ac733f', '573efaffc74f1d8ee053125011ac733f', '职能转变', '200-40-50', 2, 1, to_date('21-08-2017 15:31:11', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:31:11', 'dd-mm-yyyy hh24:mi:ss'), 50);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7681d8ee053125011ac733f', '573efaffc74f1d8ee053125011ac733f', '其他', '200-40-60', 2, 1, to_date('21-08-2017 15:31:11', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:31:11', 'dd-mm-yyyy hh24:mi:ss'), 60);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7691d8ee053125011ac733f', '573efaffc7501d8ee053125011ac733f', '绩效管理', '200-50-10', 2, 1, to_date('21-08-2017 15:32:43', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:32:43', 'dd-mm-yyyy hh24:mi:ss'), 10);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc76a1d8ee053125011ac733f', '573efaffc7501d8ee053125011ac733f', '机构编制审计', '200-50-20', 2, 1, to_date('21-08-2017 15:32:43', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:32:43', 'dd-mm-yyyy hh24:mi:ss'), 20);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc76b1d8ee053125011ac733f', '573efaffc7501d8ee053125011ac733f', '减编控编督察', '200-50-30', 2, 1, to_date('21-08-2017 15:32:43', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:32:43', 'dd-mm-yyyy hh24:mi:ss'), 30);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc76c1d8ee053125011ac733f', '573efaffc7501d8ee053125011ac733f', '其他', '200-50-40', 2, 1, to_date('21-08-2017 15:32:43', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:32:43', 'dd-mm-yyyy hh24:mi:ss'), 40);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc76d1d8ee053125011ac733f', '573efaffc7511d8ee053125011ac733f', '机构编制信息化', '200-60-10', 2, 1, to_date('21-08-2017 15:34:16', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:34:16', 'dd-mm-yyyy hh24:mi:ss'), 10);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc76e1d8ee053125011ac733f', '573efaffc7511d8ee053125011ac733f', '机构编制云平台', '200-60-20', 2, 1, to_date('21-08-2017 15:34:16', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:34:16', 'dd-mm-yyyy hh24:mi:ss'), 20);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc76f1d8ee053125011ac733f', '573efaffc7511d8ee053125011ac733f', '其他', '200-60-30', 2, 1, to_date('21-08-2017 15:34:16', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:34:16', 'dd-mm-yyyy hh24:mi:ss'), 30);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7701d8ee053125011ac733f', '573efaffc7521d8ee053125011ac733f', '国企国资改革', '200-70-10', 2, 1, to_date('21-08-2017 15:35:40', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:35:40', 'dd-mm-yyyy hh24:mi:ss'), 10);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7711d8ee053125011ac733f', '573efaffc7521d8ee053125011ac733f', '其他', '200-70-20', 2, 1, to_date('21-08-2017 15:35:40', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:35:40', 'dd-mm-yyyy hh24:mi:ss'), 20);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7721d8ee053125011ac733f', '573efaffc7531d8ee053125011ac733f', '登记管理', '200-80-10', 2, 1, to_date('21-08-2017 15:37:14', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:37:14', 'dd-mm-yyyy hh24:mi:ss'), 10);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7731d8ee053125011ac733f', '573efaffc7531d8ee053125011ac733f', '监督管理', '200-80-20', 2, 1, to_date('21-08-2017 15:37:14', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:37:14', 'dd-mm-yyyy hh24:mi:ss'), 20);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7741d8ee053125011ac733f', '573efaffc7531d8ee053125011ac733f', '事业单位员额管理', '200-80-30', 2, 1, to_date('21-08-2017 15:37:14', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:37:14', 'dd-mm-yyyy hh24:mi:ss'), 30);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7751d8ee053125011ac733f', '573efaffc7531d8ee053125011ac733f', '其他', '200-80-40', 2, 1, to_date('21-08-2017 15:37:14', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:37:14', 'dd-mm-yyyy hh24:mi:ss'), 40);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7761d8ee053125011ac733f', '573efaffc7541d8ee053125011ac733f', '党的建设', '200-90-10', 2, 1, to_date('21-08-2017 15:39:03', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:39:03', 'dd-mm-yyyy hh24:mi:ss'), 10);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7771d8ee053125011ac733f', '573efaffc7541d8ee053125011ac733f', '作风建设', '200-90-20', 2, 1, to_date('21-08-2017 15:39:03', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:39:03', 'dd-mm-yyyy hh24:mi:ss'), 20);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7781d8ee053125011ac733f', '573efaffc7541d8ee053125011ac733f', '文化建设', '200-90-30', 2, 1, to_date('21-08-2017 15:39:03', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:39:03', 'dd-mm-yyyy hh24:mi:ss'), 30);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573efaffc7791d8ee053125011ac733f', '573efaffc7541d8ee053125011ac733f', '制度建设', '200-90-40', 2, 1, to_date('21-08-2017 15:39:03', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:39:03', 'dd-mm-yyyy hh24:mi:ss'), 40);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573e9c3fbe401ca6e053125011ac1a3e', '-1', '一般分类', '100', 0, 0, to_date('21-08-2017 14:33:15', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 14:33:15', 'dd-mm-yyyy hh24:mi:ss'), null);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573e9c3fbe411ca6e053125011ac1a3e', '-2', '主题分类', '200', 0, 0, to_date('21-08-2017 15:00:36', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:00:36', 'dd-mm-yyyy hh24:mi:ss'), null);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573e9c3fbe421ca6e053125011ac1a3e', '573efaffc7401d8ee053125011ac733f', '会议材料', '100-30-10', 2, 1, to_date('21-08-2017 15:54:05', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:54:05', 'dd-mm-yyyy hh24:mi:ss'), 10);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573e9c3fbe431ca6e053125011ac1a3e', '573efaffc7401d8ee053125011ac733f', '工作材料', '100-30-20', 2, 1, to_date('21-08-2017 15:54:05', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:54:05', 'dd-mm-yyyy hh24:mi:ss'), 20);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573e9c3fbe441ca6e053125011ac1a3e', '573efaffc7401d8ee053125011ac733f', '调研报告', '100-30-30', 2, 1, to_date('21-08-2017 15:54:05', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:54:05', 'dd-mm-yyyy hh24:mi:ss'), 30);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573e9c3fbe451ca6e053125011ac1a3e', '573efaffc7401d8ee053125011ac733f', '业务培训', '100-30-40', 2, 1, to_date('21-08-2017 15:54:05', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:54:05', 'dd-mm-yyyy hh24:mi:ss'), 40);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573e9c3fbe461ca6e053125011ac1a3e', '573efaffc7401d8ee053125011ac733f', '年度报告', '100-30-50', 2, 1, to_date('21-08-2017 15:54:05', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:54:05', 'dd-mm-yyyy hh24:mi:ss'), 50);

insert into CMSS_CATEGORY (ID, PARENT_ID, NAME, CODE, LVL, IS_LEAF, CREATE_DATE, MODIFY_DATE, ORDERS)
values ('573e9c3fbe471ca6e053125011ac1a3e', '573efaffc7401d8ee053125011ac733f', '网站模版', '100-30-60', 2, 1, to_date('21-08-2017 15:54:05', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-08-2017 15:54:05', 'dd-mm-yyyy hh24:mi:ss'), 60);

