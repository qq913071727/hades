/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.domain.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.conac.cmss.entity.User;
import cn.conac.cmss.repository.UserRepository;

/**
 * 
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryImplTest {
	
	

	@Resource(name = "userRepository")
	private UserRepository userRepository;
	

	@Test
	@Transactional
	public void save() {
		User u = new User();
		u.setUserId(1L);
		u.setName("jiangdx");
		userRepository.save(u);
		User u1 = userRepository.findOne(u.getId());
		System.out.println(u1.toJson());
		
		List<User> list = userRepository.findList(null, null, null, null);
		
		for(User u2 : list){
			System.out.println(u2.toJson());
		}
	}

}
