package cn.conac.cmss.service.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.conac.cmss.entity.Category;
import cn.conac.cmss.service.CategoryService;

/**
 * CategoryServiceImpl的测试类
 * @author lishen
 * @date	2017年8月24日下午4:48:13
 * @version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryServiceImplTest {
	
	@Resource(name = "categoryService")
	private CategoryService categoryService;

	/**
	 * 测试findByParentId方法
	 */
	@Test
	@Transactional
	public void testFindByParentId() {
		List<Category> themeCategoryList=categoryService.findByParentId(Category.THEME_CATEGORY_MATERIALS_ID);
		for(Category category : themeCategoryList){
			System.out.println(category.getName());
		}
	}

}
