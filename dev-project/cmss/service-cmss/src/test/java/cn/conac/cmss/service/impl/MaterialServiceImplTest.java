/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.service.impl;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.conac.cmss.entity.Material;
import cn.conac.cmss.service.MaterialService;

/**
 * 
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MaterialServiceImplTest {
	@Resource(name = "materialService")
	private MaterialService  materialService;

	@Test
	@Transactional
	public void save() {
		Material m = new Material();
		m.setName("name");
		m.setTitle("title");
		m.setPath("path");
		m.setExtension("ext");
		
		materialService.save(m);
		Material m1 = materialService.find(m.getId());
		System.out.println("[to json:]" + m1.toJson());
	}

}
