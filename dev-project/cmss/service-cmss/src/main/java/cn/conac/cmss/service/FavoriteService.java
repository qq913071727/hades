/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.service;

import cn.conac.cmss.entity.Favorite;
import cn.conac.framework.Page;
import cn.conac.framework.service.BaseService;

/**
 * 
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public interface FavoriteService extends BaseService<Favorite, String> {
	/**
	 * 查询用户收藏文件
	 * @param userId
	 * 		用户ID,主键
	 * @return
	 */
	public Page<Favorite> findByUserId(String userId);
	
	/**
	 * 将资料设置为收藏
	 * @param userId
	 * 		用户ID,用户表主键
	 * @param materialId
	 * 		资料ID
	 */
	public void setFavorite(String userId, String materialId);
	
	/**
	 * 取消资料收藏
	 * @param userId
	 * 		用户ID,用户表主键
	 * @param materialId
	 * 		资料ID
	 */
	public void removeFavorite(String userId, String materialId);
}
