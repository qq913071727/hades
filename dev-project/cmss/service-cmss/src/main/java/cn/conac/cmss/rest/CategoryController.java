package cn.conac.cmss.rest;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;

import cn.conac.cmss.entity.Category;
import cn.conac.cmss.service.CategoryService;
import cn.conac.framework.Result;

/**
 * 
 * @author lishen
 * @date	2017年8月25日下午2:19:51
 * @version 1.0
 */
@RestController
public class CategoryController {
	/**
     * 日志记录器
     */
    private static Logger logger = LoggerFactory.getLogger(CategoryController.class);
    
    @Resource(name = "categoryService")
	private CategoryService categoryService;
    
    /**
     * 查找主题分类的所有一级子类
     * @param model
     * @return
     */
	@RequestMapping(value="/themeCategory/{parentId}", method = RequestMethod.GET)  
    public ResponseEntity<Result> themeCategory(HttpServletRequest request, HttpServletResponse response, 
    		@PathVariable("parentId") String parentId){
		logger.info("interface /themeCategory/{parentId} begin");
		
		Result result = new Result();
		
		List<Category> themeCategoryList=categoryService.findByParentId(parentId);
		
		result.setCode(1000);
		result.setMessage(JSON.toJSONString(themeCategoryList));
		result.setStatus(Result.Status.SUCCESS);
		
		logger.info("查找主题分类的所有一级子类");
		return new ResponseEntity<Result>(result, HttpStatus.OK);
    }

	/**
	 * 查找一般分类的所有一级子类
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/commonCategory/{parentId}",method = RequestMethod.GET)
	public ResponseEntity<Result> commonCategory(HttpServletRequest request, HttpServletResponse response, 
    		@PathVariable("parentId") String parentId) {
		logger.info("interface /commonCategory/{parentId} begin");
		
		Result result = new Result();
		
		List<Category> conmnonCategoryList=categoryService.findByParentId(parentId);
		
		result.setCode(1000);
		result.setMessage(JSON.toJSONString(conmnonCategoryList));
		result.setStatus(Result.Status.SUCCESS);
		
		logger.info("查找一般分类的所有一级子类");
		return new ResponseEntity<Result>(result, HttpStatus.OK);
    }
	
	

}
