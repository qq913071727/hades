/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.service;

import java.util.List;

import cn.conac.cmss.entity.Area;
import cn.conac.framework.service.BaseService;
/**
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT
 */
public interface AreaService extends BaseService<Area, String> {
	/**
	 * 查找地区
	 * @param parentId
	 * 		上级地区ID
	 * @return
	 * 		返回地区对象,未查到返回null
	 */
	public List<Area> findByParentId(String parentId);
	
	/**
	 * 查找地区
	 * @param code
	 * 		地区编码
	 * @return
	 * 		返回地区对象,未查到返回null
	 */
	public Area findByCode(Integer code);

}
