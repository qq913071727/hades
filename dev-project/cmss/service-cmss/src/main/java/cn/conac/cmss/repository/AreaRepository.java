/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.repository;

import cn.conac.cmss.entity.Area;
import cn.conac.framework.domain.BaseRepository;

/**
 * 
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public interface AreaRepository extends BaseRepository<Area, String>{

}
