/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.service;

import java.io.File;
import java.util.List;

import cn.conac.cmss.entity.Material;
import cn.conac.framework.service.BaseService;

/**
 * 
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public interface MaterialService extends BaseService<Material, String> {
	
	/**
	 * 上传资料<br>
	 * 
	 * @param material 
	 * 			资料对象,游离态
	 * @param file 
	 * 			资料文件
	 * @param userId
	 * 			用户ID	
	 * @param themeCategoryIds
	 * 			主题分类ID
	 */
	public void addMaterial(Material material, File file, long userId, String... themeCategoryIds);
	
	/**
	 * 修改资料
	 * @param material
	 * 			资料对象,游离态
	 */
	public void editMaterial(Material material);
	
	List<Material> findThemeCategoryMaterial(String categoryId);

}
