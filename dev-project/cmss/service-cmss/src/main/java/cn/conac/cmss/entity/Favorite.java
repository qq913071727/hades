/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import cn.conac.framework.domain.BaseEntity;

/**
 * 用户收藏的资料
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
@Entity
@Table(name = "CMSS_FAVORITE") 
public class Favorite extends BaseEntity {

	private static final long serialVersionUID = -8662723461620249840L;
	
	/** 用户 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(updatable = false)
	private User user;
	
	/** 资料 */
	@OneToOne
	@JoinColumn(name = "MATERIAL_ID")
	private Material naterial;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Material getNaterial() {
		return naterial;
	}

	public void setNaterial(Material naterial) {
		this.naterial = naterial;
	}

}
