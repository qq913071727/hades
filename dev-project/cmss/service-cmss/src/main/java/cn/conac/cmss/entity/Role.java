/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import cn.conac.framework.domain.BaseEntity;

/**
 * 角色
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
@Entity
@Table(name = "CMSS_ROLE")
public class Role extends BaseEntity {

	private static final long serialVersionUID = -3897748044997748484L;
	
	@Column(name = "NAME", nullable = false, unique = true)
	private String name;
	
	@Column(name = "CODE", nullable = false, unique = true)
	private String code;
	
	@ManyToMany(mappedBy = "roles")
	private Set<User> users;
	
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "CMSS_ROLE_AUTHORITY", 
	joinColumns = {@JoinColumn(name = "ROLE_ID", referencedColumnName = "id")},
	inverseJoinColumns = {@JoinColumn(name="AUTHORITY_ID", referencedColumnName = "id")}
	)
	private Set<Authority> authoritys;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Set<Authority> getAuthoritys() {
		return authoritys;
	}

	public void setAuthoritys(Set<Authority> authoritys) {
		this.authoritys = authoritys;
	}
	
}
