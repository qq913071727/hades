/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.service;

import java.io.File;

import cn.conac.cmss.FileInfo;
import cn.conac.framework.Result;

/**
 * 文件上传
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public interface FileService {

	/**
	 * 上传文件
	 * @param fileInfo
	 * @return
	 */
	public Result upload(FileInfo fileInfo);
	
	/**
	 * 下载文件
	 * @param fileInfo
	 * @return
	 */
	public File download(FileInfo fileInfo);
	
	/**
	 * 删除文件
	 * @param fileInfo
	 * @return
	 */
	public Result delete(FileInfo fileInfo);
	
	/**
	 * 验证文件大小、类型
	 * @param fileInfo
	 * @return
	 */
	public Result isValid(FileInfo fileInfo);
	
	/**
	 * 根据一定规则、算法计算文件存储路径
	 * @param fileInfo
	 * @return
	 * 		文件实际存储到磁盘的路径
	 */
	public String calculateFilePath(FileInfo fileInfo);
	
}
