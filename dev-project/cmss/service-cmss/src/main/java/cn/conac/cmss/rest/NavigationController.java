package cn.conac.cmss.rest;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import cn.conac.cmss.entity.Category;
import cn.conac.cmss.service.CategoryService;

/**
 * 导航控制器类
 * @author lishen
 * @date	2017年8月24日下午4:54:08
 * @version 1.0
 */
@RestController
public class NavigationController {
	/**
     * 日志记录器
     */
    private static Logger logger = LoggerFactory.getLogger(NavigationController.class);
    
    @Resource(name = "categoryService")
	private CategoryService categoryService;
    
	@RequestMapping(value="/layout", method = RequestMethod.GET)  
    public ModelAndView layout(Model model){
		logger.info("layout");
		ModelAndView mv = new ModelAndView("/common/layout");
		return mv;
    }

}
