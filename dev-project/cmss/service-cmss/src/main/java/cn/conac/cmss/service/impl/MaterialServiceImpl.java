/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.service.impl;

import java.io.File;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.conac.cmss.entity.Material;
import cn.conac.cmss.repository.CategoryRepository;
import cn.conac.cmss.repository.MaterialRepository;
import cn.conac.cmss.repository.UserRepository;
import cn.conac.cmss.service.MaterialService;
import cn.conac.framework.service.impl.BaseServiceImpl;

/**
 * 
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
@Service("materialService")
public class MaterialServiceImpl extends BaseServiceImpl<Material, String> implements MaterialService {
	
	@Resource(name = "materialRepository")
	private MaterialRepository materialRepository;
	
	@Resource(name = "userRepository")
	private UserRepository userRepository;
	
	@Resource(name = "categoryRepository")
	private CategoryRepository categoryRepository;
	
	@Resource(name = "materialRepository")
	public void setBaseDao(MaterialRepository materialRepository) {
		super.setBaseDao(materialRepository);
	}

	@Override
	public void addMaterial(Material material, File file, long userId, String... themeCategoryIds) {
		// 1.保存资料信息
		// 2.存储资料文件
		// 3.建立全文检索
	}

	@Override
	public void editMaterial(Material material) {
		
	}

	public List<Material> findThemeCategoryMaterial(String categoryId){
		return materialRepository.findThemeCategoryMaterial(categoryId);
	}
}
