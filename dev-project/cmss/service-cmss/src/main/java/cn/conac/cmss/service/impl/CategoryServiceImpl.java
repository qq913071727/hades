package cn.conac.cmss.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.conac.cmss.entity.Category;
import cn.conac.cmss.repository.CategoryRepository;
import cn.conac.cmss.service.CategoryService;
import cn.conac.framework.service.impl.BaseServiceImpl;

/**
 * 
 * @author lishen
 * @date	2017年8月24日下午8:19:38
 * @version 1.0
 */
@Service("categoryService")
public class CategoryServiceImpl extends BaseServiceImpl<Category, String> implements CategoryService {
	
	@Resource(name = "categoryRepository")
	private CategoryRepository categoryRepository;
	
	@Resource(name = "categoryRepository")
	public void setBaseDao(CategoryRepository categoryRepository) {
		super.setBaseDao(categoryRepository);
	}

	/**
	 * 根据某个节点的parent_id字段查找他的所有子节点
	 * @param parentId
	 * 		上级分类ID
	 * @return
	 * 		返回分类,未查到返回null
	 */
	@Override
	public List<Category> findByParentId(String parentId) {
		List<Category> categoryList=null;
		try{
			categoryList=categoryRepository.findByParentId(parentId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return categoryList;
	}

}
