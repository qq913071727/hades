/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import cn.conac.framework.domain.BaseEntity;

/**
 * 资料信息
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
@Entity
@Table(name = "CMSS_MATERIAL") 
public class Material extends BaseEntity {

	private static final long serialVersionUID = -2978307311442772004L;
	
	/** 资料名称 */
	@Column(name = "NAME", nullable = false)
	private String name;
	
	/** 标题 */
	@Column(name = "TITLE", nullable = false)
	private String title;
	
	/** 发文编号 */
	@Column(name = "MATERIAL_NUMBER")
	private String materialNumber;
	
	/** 摘要 */
	@Column(name = "SUMMARY")
	private String summary;
	
	/** 资料存放路径（全路径） */
	@Column(name = "PATH", nullable = false)
	private String path;
	
	/** 扩展名 */
	@Column(name = "EXTENSION", nullable = false)
	private String extension;
	
	/** 点击量、浏览次数 */
	@Column(name = "HITS", nullable = false)
	private Long hits = 0L;
	
	/** 下载量 */
	@Column(name = "DOWNLOADS", nullable = false)
	private Long downloads = 0L;
	
	/** 上传时间 */
	@Column(name = "UPLOAD_DATE", updatable = false)
	private Date uploadDate;
	
	/** 是否是个人资料 */
	@Column(name = "IS_PRIVATE_FLAG", nullable = false)
	private boolean privateFlag = false;
	
	/** 是否匿名上传 */
	@Column(name = "IS_ANONYMOUS", nullable = false)
	private boolean anonymous = false;
	
	/** 是否已建立全文索引 */
	@Column(name = "IS_INDEX_FLAG", nullable = false)
	private boolean indexFlag = true;
	
	/** 是否需要审核（新上传的都需要审核） */
	@Column(name = "IS_NEED_CHECK", nullable = false)
	private boolean needCheck = true;
	
	/** 是否已审核 */
	@Column(name = "IS_CHECKED", nullable = false)
	private boolean checked = false;
	
	/** 是否需要纠错（非用户选择的主题需要显示纠错按钮） */
	@Column(name = "IS_NEED_MISTAKE", nullable = false)
	private boolean needMistake = false;
	
	/** 资料评分 */
	@Column(name = "SCORE")
	private double score = 1.0;
	
	/** 上传人 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(updatable = false, name = "UPLOAD_USER")
	private User uploadUser;
	
	/** 最后修改人、审核人 */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UPDATE_USER")
	private User updateUser;
	
	/** 分类的标记（不明确分类时给出的猜测的分类） */
	@Column(name = "CATEGORY_MARK")
	private String categoryMark;
	
	/** 试用地区 */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SUITABLE_AREA_ID")
	private Area suitableArea;
	
	/** 一般分类 */
	@ManyToMany(mappedBy = "commonCategoryMaterials")
	private Set<Category> commonCategorys;
	
	/** 主题分类 */
	@ManyToMany(mappedBy = "themeCategoryMaterials")
	private Set<Category> themeCategorys;
	
	/** 意见反馈 */
	@OneToMany(mappedBy = "material", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<Suggestion> suggestions;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMaterialNumber() {
		return materialNumber;
	}

	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public Long getHits() {
		return hits;
	}

	public void setHits(Long hits) {
		this.hits = hits;
	}

	public Long getDownloads() {
		return downloads;
	}

	public void setDownloads(Long downloads) {
		this.downloads = downloads;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public boolean isPrivateFlag() {
		return privateFlag;
	}

	public void setPrivateFlag(boolean privateFlag) {
		this.privateFlag = privateFlag;
	}

	public boolean isAnonymous() {
		return anonymous;
	}

	public void setAnonymous(boolean anonymous) {
		this.anonymous = anonymous;
	}

	public boolean isIndexFlag() {
		return indexFlag;
	}

	public void setIndexFlag(boolean indexFlag) {
		this.indexFlag = indexFlag;
	}

	public boolean isNeedCheck() {
		return needCheck;
	}

	public void setNeedCheck(boolean needCheck) {
		this.needCheck = needCheck;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	public boolean isNeedMistake() {
		return needMistake;
	}

	public void setNeedMistake(boolean needMistake) {
		this.needMistake = needMistake;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public User getUploadUser() {
		return uploadUser;
	}

	public void setUploadUser(User uploadUser) {
		this.uploadUser = uploadUser;
	}

	public User getUpdateUser() {
		return updateUser;
	}
	
	public String getCategoryMark() {
		return categoryMark;
	}

	public void setCategoryMark(String categoryMark) {
		this.categoryMark = categoryMark;
	}

	public void setUpdateUser(User updateUser) {
		this.updateUser = updateUser;
	}

	public Area getSuitableArea() {
		return suitableArea;
	}

	public void setSuitableArea(Area suitableArea) {
		this.suitableArea = suitableArea;
	}

	public Set<Category> getCommonCategorys() {
		return commonCategorys;
	}

	public void setCommonCategorys(Set<Category> commonCategorys) {
		this.commonCategorys = commonCategorys;
	}

	public Set<Category> getThemeCategorys() {
		return themeCategorys;
	}

	public void setThemeCategorys(Set<Category> themeCategorys) {
		this.themeCategorys = themeCategorys;
	}

	public Set<Suggestion> getSuggestions() {
		return suggestions;
	}

	public void setSuggestions(Set<Suggestion> suggestions) {
		this.suggestions = suggestions;
	}

}
