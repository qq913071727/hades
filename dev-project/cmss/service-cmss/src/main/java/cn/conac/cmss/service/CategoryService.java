/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.service;

import java.util.List;

import cn.conac.cmss.entity.Category;
import cn.conac.framework.service.BaseService;

/**
 * 
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public interface CategoryService extends BaseService<Category, String> {
	/**
	 * 根据某个节点的parent_id字段查找他的所有子节点
	 * @param parentId
	 * 		上级分类ID
	 * @return
	 * 		返回分类,未查到返回null
	 */
	public List<Category> findByParentId(String parentId);

}
