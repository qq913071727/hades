/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.service;

import cn.conac.cmss.entity.Score;
import cn.conac.framework.service.BaseService;

/**
 * 
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public interface ScoreService extends BaseService<Score, String> {

	/**
	 * 为资料评分
	 * @param score
	 * @param userId
	 * @param materialId
	 */
	public void addScore(double score, long userId, String materialId );
}
