/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cn.conac.framework.domain.BaseEntity;

/**
 * 意见反馈（纠错）
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
@Entity
@Table(name = "CMSS_SUGGESTION")
public class Suggestion extends BaseEntity {

	private static final long serialVersionUID = -2109896601108718209L;
	
	/** 反馈人 */
	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User suggestionUser;
	
	/** 资料 */
	@ManyToOne
	@JoinColumn(name = "MATERIAL_ID")
	private Material material;
	
	/** 反馈意见 */
	@Column(name = "SUGGESTION")
	private String suggestion;
	
	/** 是否已确认 */
	@Column(name = "IS_CONFIM", nullable = false)
	private boolean confirm = false;

	public User getSuggestionUser() {
		return suggestionUser;
	}

	public void setSuggestionUser(User suggestionUser) {
		this.suggestionUser = suggestionUser;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public String getSuggestion() {
		return suggestion;
	}

	public void setSuggestion(String suggestion) {
		this.suggestion = suggestion;
	}

	public boolean isConfirm() {
		return confirm;
	}

	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}

}
