/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import cn.conac.framework.domain.BaseEntity;

/**
 * 评分
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
@Entity
@Table(name = "CMSS_SCORE")
public class Score extends BaseEntity {

	private static final long serialVersionUID = 1981080489087355860L;
	
	/** 评分人 */
	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User scorer;
	
	/** 资料 */
	@OneToOne
    @JoinColumn(name = "MATERIAL_ID")
	private Material magerial;
	
	/** 评分 */
	@Column(name = "SCORE", nullable = false)
	private double score = 0;

	public User getScorer() {
		return scorer;
	}

	public void setScorer(User scorer) {
		this.scorer = scorer;
	}

	public Material getMagerial() {
		return magerial;
	}

	public void setMagerial(Material magerial) {
		this.magerial = magerial;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

}
