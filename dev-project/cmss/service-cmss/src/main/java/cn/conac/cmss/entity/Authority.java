/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import cn.conac.framework.domain.BaseEntity;

/**
 * 权限
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
@Entity
@Table(name = "CMSS_AUTHORITY")
public class Authority extends BaseEntity {

	private static final long serialVersionUID = -9217393177110927241L;
	
	/** 权限名称 */
	@Column(name = "NAME", nullable = false, unique = true)
	private String name;
	
	/** 权限编码  */
	@Column(name = "CODE", nullable = false, unique = true)
	private String code;
	
	/** 对应的角色 */
	@ManyToMany(mappedBy = "authoritys")
	private Set<Role> roles;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
}
