/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.service;

import cn.conac.cmss.entity.Suggestion;
import cn.conac.framework.service.BaseService;

/**
 * 
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public interface SuggestionService extends BaseService<Suggestion, String> {

	/**
	 * 保存意见反馈（资料纠错）<br>
	 * @param suggestion
	 * @param userId
	 * @param materialId
	 */
	public void addSuggestion(String suggestion, long userId, String materialId);
}
