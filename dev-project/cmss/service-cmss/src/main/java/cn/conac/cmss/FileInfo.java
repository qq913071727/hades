/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss;

import java.io.File;

/**
 * 
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public class FileInfo {
	
	/**文件对象*/
	private File file;
	
	/**文件全路径*/
	private File path;
	
	/**文件大小*/
	private long size;
	
	/**扩展名*/
	private String extension;
	
	
	public File getFile() {
		return file;
	}
	
	public void setFile(File file) {
		this.file = file;
	}
	
	public File getPath() {
		return path;
	}
	
	public void setPath(File path) {
		this.path = path;
	}
	
	public long getSize() {
		return size;
	}
	
	public void setSize(long size) {
		this.size = size;
	}
	
	public String getExtension() {
		return extension;
	}
	
	public void setExtension(String extension) {
		this.extension = extension;
	}
	
}
