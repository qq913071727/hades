/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cn.conac.cmss.entity.Material;
import cn.conac.framework.domain.BaseRepository;

/**
 * 
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public interface MaterialRepository extends BaseRepository<Material, String>{
	
	@Query(value="select m.* from cmss_category c, cmss_material m, cmss_material_category mc where mc.material_id=m.id and mc.category_id=c.id and c.id=:categoryId", nativeQuery = true)
	List<Material> findThemeCategoryMaterial(@Param("categoryId") String categoryId);
}
