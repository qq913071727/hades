/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import cn.conac.framework.domain.BaseEntity;

/**
 * 地区
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
@Entity
@Table(name = "CMSS_AREA") 
public class Area extends BaseEntity {

	private static final long serialVersionUID = -7905193275088539671L;
	
	/** 地区名称 */
	@Column(name = "NAME", nullable = false)
	private String name;
	
	/** 地区码 */
	@Column(name = "CODE", nullable = false, unique = true)
	private Integer code;
	
	/** 层级*/
	@Column(name = "WEIGHT", nullable = false)
	private Long weight;
	
	/** 层级*/
	@Column(name = "LVL", nullable = false)
	private Integer level;
	
	/** 是否叶子节点 */
	@Column(name = "IS_LEAF", nullable = false)
	private boolean leaf;
	
	/** 上级地区 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	private Area parent;
	
	/** 下级地区 */
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "parent")
	private Set<Area> children;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Long getWeight() {
		return weight;
	}

	public void setWeight(Long weight) {
		this.weight = weight;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}
	
	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public Area getParent() {
		return parent;
	}

	public void setParent(Area parent) {
		this.parent = parent;
	}

	public Set<Area> getChildren() {
		return children;
	}

	public void setChildren(Set<Area> children) {
		this.children = children;
	}

}
