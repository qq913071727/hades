package cn.conac.cmss.rest;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;

import cn.conac.cmss.entity.Category;
import cn.conac.cmss.entity.Material;
import cn.conac.cmss.service.CategoryService;
import cn.conac.cmss.service.MaterialService;
import cn.conac.framework.Result;

/**
 * Material类的控制亲
 * @author lishen
 * @date	2017年8月25日下午8:20:22
 * @version 1.0
 */
@RestController
public class MaterialController {

	/**
     * 日志记录器
     */
    private static Logger logger = LoggerFactory.getLogger(MaterialController.class);
    
    @Resource(name = "materialService")
	private MaterialService materialService;
    
    @Resource(name = "categoryService")
	private CategoryService categoryService;
    
    /**
     * 查找某个主题分类相关的材料
     * @param model
     * @return
     */
	@RequestMapping(value="/themeCategoryMaterial/{themeCategoryId}", method = RequestMethod.GET)  
    public ResponseEntity<Result> themeCategoryMaterial(HttpServletRequest request, HttpServletResponse response, 
    		@PathVariable("themeCategoryId") String themeCategoryId){
		logger.info("interface /themeCategoryMaterial/{themeCategoryId} begin");
		
		Result result = new Result();
		
		List<Material> materialList=materialService.findThemeCategoryMaterial(themeCategoryId);
		
		result.setCode(1000);
		System.out.println(materialList.size());
		result.setMessage(JSON.toJSONString(materialList));
		result.setStatus(Result.Status.SUCCESS);
		
		logger.info("查找某个主题分类相关的材料");
		return new ResponseEntity<Result>(result, HttpStatus.OK);
    }

}
