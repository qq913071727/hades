/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cn.conac.cmss.entity.Category;
import cn.conac.framework.domain.BaseRepository;

/**
 * 
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public interface CategoryRepository extends BaseRepository<Category, String>{
	/**
	 * 查找分类
	 * @param parentId
	 * 		上级分类ID
	 * @return
	 * 		返回分类,未查到返回null
	 */
//	@Query(value = "select t from Category t where t.parent.id=(select c.id from Category c where c.parent.id=:parentId)", nativeQuery = false)
	@Query(value = "select t from Category t where t.parent.id=:parentId", nativeQuery = false)
	List<Category> findByParentId(@Param("parentId") String parentId);
}
