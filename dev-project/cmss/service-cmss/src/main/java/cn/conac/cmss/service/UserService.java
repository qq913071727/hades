/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.service;

import cn.conac.cmss.entity.User;
import cn.conac.framework.service.BaseService;

/**
 * 
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public interface UserService extends BaseService<User, String> {

	/**
	 * 根据红云后台的userId查询用户信息(非主键)
	 * @param userId
	 * @return
	 */
	public User findByUserId(Long userId);
}
