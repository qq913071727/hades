/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import cn.conac.framework.domain.BaseEntity;

/**
 * 用户,userID、name、area 字段信息来自红云
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
@Entity
@Table(name = "CMSS_USER")
public class User extends BaseEntity {

	private static final long serialVersionUID = -3897748044997748484L;
	
	/** 用户ID（红云用户数据库主键） */
	@Column(name = "USER_ID", nullable = false, unique = true)
	private Long userId;
	
	/** 用户昵称，没用昵称使用用户名 */
	@Column(name = "NAME")
	private String name;
	
	/** 文档数量 */
	@Column(name = "MATERIAL_AMOUNT")
	private Integer materialAmount = 0;
	
	/** 文档被浏览次数（所有文档总点击量） */
	@Column(name = "MATERIAL_HITS")
	private Integer materialHits = 0;
	
	/** 文档被下载次数（所有文档总下载量） */
	@Column(name = "MATERIAL_DOWNLOADS")
	private Integer materialDownloads = 0;
	
	/** 是否启用 */
	@Column(name = "IS_ENABLED", nullable = false)
	private boolean enabled = true;

	/** 是否锁定 */
	@Column(name = "IS_LOCKED", nullable = false)
	private boolean locked = false;
	
	/** 是否删除 */
	@Column(name = "IS_DELETE", nullable = false)
	private boolean delete = false;
	
	/** 用户所属地区 */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AREA_ID")
	private Area area;
	
	/** 纠错、意见反馈 */
	@OneToMany(mappedBy = "suggestionUser", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<Suggestion> suggestion;
	
	/** 评分 */
	@OneToMany(mappedBy = "scorer", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<Score> scores;
	
	/** 收藏 */
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<Favorite> favorites;
	
	/** 资料 */
	@OneToMany(mappedBy = "uploadUser", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<Material> materials;
	
	/** 角色 */
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "CMSS_USER_ROLE", 
	joinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName = "id")},
	inverseJoinColumns = {@JoinColumn(name="ROLE_ID", referencedColumnName = "id")}
	)
	private Set<Role> roles;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getMaterialAmount() {
		return materialAmount;
	}

	public void setMaterialAmount(Integer materialAmount) {
		this.materialAmount = materialAmount;
	}

	public Integer getMaterialHits() {
		return materialHits;
	}

	public void setMaterialHits(Integer materialHits) {
		this.materialHits = materialHits;
	}

	public Integer getMaterialDownloads() {
		return materialDownloads;
	}

	public void setMaterialDownloads(Integer materialDownloads) {
		this.materialDownloads = materialDownloads;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Set<Suggestion> getSuggestion() {
		return suggestion;
	}

	public void setSuggestion(Set<Suggestion> suggestion) {
		this.suggestion = suggestion;
	}

	public Set<Score> getScores() {
		return scores;
	}

	public void setScores(Set<Score> scores) {
		this.scores = scores;
	}

	public Set<Favorite> getFavorites() {
		return favorites;
	}

	public void setFavorites(Set<Favorite> favorites) {
		this.favorites = favorites;
	}

	public Set<Material> getMaterials() {
		return materials;
	}

	public void setMaterials(Set<Material> materials) {
		this.materials = materials;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
}
