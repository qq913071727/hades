/*
 * Copyright 2006-2014 conac.cn. All rights reserved.
 * Support: http://www.conac.cn
 * License: http://www.conac.cn/license
 */
package cn.conac.cmss.solr;

import cn.conac.cmss.entity.Material;

/**
 * 全文检索相关工具
 * @author jiangdx
 * @version 0.0.1-SNAPSHOT  
 */
public interface FullIndexService {
	/**
	 * 创建全文检索索引
	 * @param material
	 */
	public void index(Material material);
	
	/**
	 * 全量创建全文检索索引
	 */
	public void fullIndex();
}
