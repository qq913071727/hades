package cn.conac.server.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * 配置信息管理服务端
 * @author jiangdx
 * @version 2017年6月2日    v 1.0
 */
@EnableConfigServer
@EnableDiscoveryClient
@SpringBootApplication
public class ConfigServerApplication {

    public static void main( String[] args ){
    	SpringApplication.run(ConfigServerApplication.class, args);
    }
}
