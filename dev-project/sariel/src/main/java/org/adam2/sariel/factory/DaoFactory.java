package org.adam2.sariel.factory;

import org.adam2.sariel.dao.BoardDao;
import org.adam2.sariel.dao.StockDao;
import org.adam2.sariel.dao.impl.BoardDaoImpl;
import org.adam2.sariel.dao.impl.StockDaoImpl;

/**
 * Dao工厂类
 */
public class DaoFactory {
    /**
     * BoardDao类型实例
     */
    public static BoardDao boardDao;

    /**
     * StockDao类型实例
     */
    public static StockDao stockDao;

    /**
     * 返回dao类的实例
     * @param clazz
     * @return
     */
    public static Object getInstance(Class clazz){
        try {
            // 返回BoardDao类的实例
            if(clazz.getName().equals(BoardDaoImpl.class.getName())){
                if(null!=boardDao){
                    return boardDao;
                }else{
                    boardDao= (BoardDao) clazz.newInstance();
                    return boardDao;
                }
            }

            // 返回StockDao类的实例
            if(clazz.getName().equals(StockDaoImpl.class.getName())){
                if(null!=stockDao){
                    return stockDao;
                }else{
                    stockDao= (StockDao) clazz.newInstance();
                    return stockDao;
                }
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
