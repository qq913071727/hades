package org.adam2.sariel.service.impl;

import org.adam2.sariel.dao.BoardDao;
import org.adam2.sariel.dao.impl.AbstractDao;
import org.adam2.sariel.dao.impl.BoardDaoImpl;
import org.adam2.sariel.service.BoardService;
import org.apache.hadoop.hbase.client.Mutation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("boardService")
public class BoardServiceImpl extends AbstractService implements BoardService {

    /**
     * 插入Board对象
     * @param rowName
     * @param familyName
     * @param qualifier
     * @param value
     */
    @Override
    public void put(String rowName, String familyName, String qualifier, byte[] value) {
        boardDao.put(rowName, familyName, qualifier, value);
    }

    /**
     * 批量加载Board对象
     * @param mutationList
     */
    @Override
    public void batchPut(List<Mutation> mutationList) {
        boardDao.batchPut(mutationList);
    }

    /**
     * 根据rowName, familyName, qualifier返回board表中的一个value值
     * @param rowName
     * @param familyName
     * @param qualifier
     * @return
     */
    public String getValueByRowNameAndFamilyAndQualifier(String rowName, String familyName, String qualifier) {
        return boardDao.getValueByRowNameAndFamilyAndQualifier(rowName, familyName, qualifier);
    }

    /**
     * 通过开始行键和结束行键获取board表中的数据
     *
     * @param startRow
     * @param stopRow
     * @return
     */
    @Override
    public List<Map<String, Object>> find(String startRow, String stopRow){
        return boardDao.find(startRow, stopRow);
    }

    /**
     * 根据qualifier的value，返回rowName(行键)
     * @param qualifier
     * @param value
     * @return
     */
    @Override
    public String getRowNameByQualifier(String qualifier, String value) {
        return boardDao.getRowNameByQualifier(qualifier, value);
    }
}
