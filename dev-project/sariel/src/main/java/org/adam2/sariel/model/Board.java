package org.adam2.sariel.model;

/**
 * 板块基本信息
 */
public class Board {
    /**
     * 主键
     */
    private String id;

    /**
     * 板块名称
     */
    private String name;

    /**
     * 股票数量（这个板块中有多少股票）
     */
    private Long amount;

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
