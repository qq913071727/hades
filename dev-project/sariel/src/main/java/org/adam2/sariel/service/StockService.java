package org.adam2.sariel.service;

import org.apache.hadoop.hbase.client.Mutation;

import java.util.List;

public interface StockService {
    /**
     * 批量加载Stock对象
     * @param mutationList
     */
    void batchPut(List<Mutation> mutationList);
}
