package org.adam2.sariel.dao.impl;

import org.adam2.sariel.constant.TableName;
import org.adam2.sariel.dao.StockDao;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.client.Mutation;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.data.hadoop.hbase.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class StockDaoImpl extends AbstractDao implements StockDao {
    /**
     * 批量加载Stock对象
     * @param mutationList
     */
    @Override
    public void batchPut(List<Mutation> mutationList) {
        super.batchPut(mutationList, TableName.STOCK);
    }

    public Map scan(){
        return hbaseTemplate.get(TableName.STOCK, "SH600035_20180515",new RowMapper<Map<String,Object>>(){
            public Map<String,Object> mapRow(Result result, int rowNum) throws Exception {

                List<Cell> ceList =   result.listCells();
                Map<String,Object> map = new HashMap<String, Object>();
                if(ceList!=null&&ceList.size()>0){
                    for(Cell cell:ceList){
                        System.out.println(Bytes.toString(cell.getFamilyArray()));
                        System.out.println(Bytes.toString(cell.getQualifierArray()));
                        System.out.println(Bytes.toString(cell.getValueArray()));
                        System.out.println(Bytes.toString(cell.getValue()));

                        map.put(Bytes.toString(cell.getFamilyArray(),cell.getFamilyOffset(),cell.getFamilyLength())+
                                        "_"+Bytes.toString( cell.getQualifierArray(),cell.getQualifierOffset(),cell.getQualifierLength()),
                                Bytes.toString( cell.getValueArray(), cell.getValueOffset(), cell.getValueLength()));
                    }
                }
                return  map;
            }
        });
    }
}
