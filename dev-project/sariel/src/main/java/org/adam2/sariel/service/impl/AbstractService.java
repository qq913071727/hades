package org.adam2.sariel.service.impl;

import org.adam2.sariel.dao.BoardDao;
import org.adam2.sariel.dao.StockDao;
import org.adam2.sariel.dao.impl.AbstractDao;
import org.adam2.sariel.dao.impl.BoardDaoImpl;
import org.adam2.sariel.dao.impl.StockDaoImpl;
import org.adam2.sariel.factory.DaoFactory;
import org.adam2.sariel.service.BoardService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 抽象service类
 */
public abstract class AbstractService {
    @Autowired
    public BoardDao boardDao;

    @Autowired
    public StockDao stockDao;

    /**
     * 在运行mapreduce时，spring依赖注入无法使用，只能用硬编码的方式创建对象
     */
    {
        if(null==boardDao){
            boardDao= (BoardDao) DaoFactory.getInstance(BoardDaoImpl.class);
        }
        if(null==stockDao){
            stockDao= (StockDao) DaoFactory.getInstance(StockDaoImpl.class);
        }
    }


}
