package org.adam2.sariel.controller;

import org.adam2.sariel.constant.TableName;
import org.adam2.sariel.job.LoadBoardJob;
import org.adam2.sariel.job.LoadStockJob;
import org.adam2.sariel.service.BoardService;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.ToolRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.swing.text.TabableView;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value="/sariel")
public class MapReduceController extends AbstractController {
    @Autowired
    public LoadBoardJob loadBoardJob;

    @Autowired
    public BoardService boardService;

    @RequestMapping(value = "test", method = RequestMethod.GET)
    public void test(Model model) {

        Configuration conf = new Configuration();
        int loadBoardJobStatus = 0;
        int loadStockJobStatus = 0;
        try {
//            loadBoardJobStatus = ToolRunner.run(conf, new LoadBoardJob(), args);
            loadStockJobStatus = ToolRunner.run(conf, new LoadStockJob(), args);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        System.exit(loadBoardJobStatus);
        return;
    }

    @RequestMapping(value = "find", method = RequestMethod.GET)
    public void find(Model model){
        List<Map<String, Object>> l=boardService.find(null, null);
    }
}
