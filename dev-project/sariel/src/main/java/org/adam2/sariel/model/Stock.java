package org.adam2.sariel.model;

import java.math.BigInteger;

/**
 * 股票基本信息
 */
public class Stock {
    /**
     * 主键
     */
    private String id;

    /**
     * 股票代码
     */
    private String code;

    /**
     * 股票名称
     */
    private String name;

    /**
     * TTM市盈率
     */
    private Double ttmPeRatio;

    /**
     * 净利润
     */
    private BigInteger netProfits;

    /**
     * 市净率
     */
    private Double priceBookRatio;

    /**
     * 总市值
     */
    private BigInteger totalMarketCapitalization;

    /**
     * 流通市值
     */
    private BigInteger circulationMarketValue;

    /**
     * 板块id
     */
    private String boardId;

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public BigInteger getCirculationMarketValue() {

        return circulationMarketValue;
    }

    public void setCirculationMarketValue(BigInteger circulationMarketValue) {
        this.circulationMarketValue = circulationMarketValue;
    }

    public BigInteger getTotalMarketCapitalization() {

        return totalMarketCapitalization;
    }

    public void setTotalMarketCapitalization(BigInteger totalMarketCapitalization) {
        this.totalMarketCapitalization = totalMarketCapitalization;
    }

    public Double getPriceBookRatio() {

        return priceBookRatio;
    }

    public void setPriceBookRatio(Double priceBookRatio) {
        this.priceBookRatio = priceBookRatio;
    }

    public BigInteger getNetProfits() {

        return netProfits;
    }

    public void setNetProfits(BigInteger netProfits) {
        this.netProfits = netProfits;
    }

    public Double getTtmPeRatio() {

        return ttmPeRatio;
    }

    public void setTtmPeRatio(Double ttmPeRatio) {
        this.ttmPeRatio = ttmPeRatio;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
