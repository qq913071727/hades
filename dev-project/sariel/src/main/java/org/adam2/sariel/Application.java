package org.adam2.sariel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * mapreduce入口类
 */
@SpringBootApplication
//@ComponentScan("org.adam2.sariel.job")
@ImportResource(locations = {"classpath:/config/hbase-spring.xml"})
public class Application {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}