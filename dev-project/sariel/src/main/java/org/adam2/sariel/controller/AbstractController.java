package org.adam2.sariel.controller;

import org.adam2.sariel.util.PropertiesUtil;

public abstract class AbstractController {
    /**
     * hadoop的输入路径和输出路径
     */
    protected String[] args={PropertiesUtil.getValue("config/config.properties", "hadoop.input.path"),
            PropertiesUtil.getValue("config/config.properties", "hadoop.output.path")};
}
