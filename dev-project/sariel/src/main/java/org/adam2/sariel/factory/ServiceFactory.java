package org.adam2.sariel.factory;

import org.adam2.sariel.service.BoardService;
import org.adam2.sariel.service.StockService;
import org.adam2.sariel.service.impl.BoardServiceImpl;
import org.adam2.sariel.service.impl.StockServiceImpl;

/**
 * service的工厂类
 */
public class ServiceFactory {
    /**
     * BoardService类型对象
     */
    private static BoardService boardService;

    /**
     * StockService类型对象
     */
    private static StockService stockService;

    /**
     * 返回service类的实例
     * @param clazz
     * @return
     */
    public static Object getInstance(Class clazz){
        try {
            // 返回BoardService类的实例
            if(clazz.getName().equals(BoardServiceImpl.class.getName())){
                if(null!=boardService){
                    return boardService;
                }else{
                    boardService= (BoardService) clazz.newInstance();
                    return boardService;
                }
            }

            // 返回StockService类的实例
            if(clazz.getName().equals(StockServiceImpl.class.getName())){
                if(null!=stockService){
                    return stockService;
                }else{
                    stockService= (StockService) clazz.newInstance();
                    return stockService;
                }
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
