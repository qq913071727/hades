package org.adam2.sariel.dao;

import org.apache.hadoop.hbase.client.Mutation;

import java.util.List;

public interface StockDao {
    /**
     * 批量加载Stock对象
     * @param mutationList
     */
    void batchPut(List<Mutation> mutationList);
}
