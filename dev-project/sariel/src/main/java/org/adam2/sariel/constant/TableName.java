package org.adam2.sariel.constant;

/**
 * 表格名称常量类
 */
public class TableName {
    /**
     * board表
     */
    public static final String BOARD="board";

    /**
     * stock表
     */
    public static final String STOCK="stock";
}
