package org.adam2.sariel.service.impl;

import org.adam2.sariel.service.StockService;
import org.apache.hadoop.hbase.client.Mutation;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("stockService")
public class StockServiceImpl extends AbstractService implements StockService {

    /**
     * 批量加载Stock对象
     * @param mutationList
     */
    @Override
    public void batchPut(List<Mutation> mutationList) {
        stockDao.batchPut(mutationList);
    }
}
