package org.adam2.sariel.dao;

import org.apache.hadoop.hbase.client.Mutation;

import java.util.List;
import java.util.Map;

public interface BoardDao {

    /**
     * 插入Board对象
     * @param rowName
     * @param familyName
     * @param qualifier
     * @param value
     */
    void put(String rowName, String familyName, String qualifier, byte[] value);

    /**
     * 批量加载Board对象
     * @param mutationList
     */
    void batchPut(List<Mutation> mutationList);

    /**
     * 根据rowName, familyName, qualifier返回board表中的一个value值
     * @param rowName
     * @param familyName
     * @param qualifier
     * @return
     */
    String getValueByRowNameAndFamilyAndQualifier(String rowName, String familyName, String qualifier);

    /**
     * 通过开始行键和结束行键获取board表中的数据
     * @param startRow
     * @param stopRow
     * @return
     */
    List<Map<String, Object>> find(String startRow, String stopRow);

    /**
     * 根据qualifier的value，返回rowName(行键)
     * @param qualifier
     * @param value
     * @return
     */
    String getRowNameByQualifier(String qualifier, String value);
}
