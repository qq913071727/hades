package org.adam2.chamuel.entity;

import com.alibaba.fastjson.JSONObject;
import org.adam2.chamuel.util.DateUtil;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 股票交易记录类
 */
public class StockRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 日期
     */
    private Date date;

    /**
     * 股票代码
     */
    private String code;

    /**
     * 开盘价
     */
    private BigDecimal open;

    /**
     * 最高价
     */
    private BigDecimal high;

    /**
     * 收盘价
     */
    private BigDecimal close;

    /**
     * 最低价
     */
    private BigDecimal low;

    /**
     * 前收盘
     */
    private BigDecimal lastClose;

    /**
     * 涨跌
     */
    private BigDecimal upDown;

    /**
     * 涨跌额
     */
    private BigDecimal upDownAmount;

    /**
     * 涨跌幅
     */
    private BigDecimal upDownPercentage;

    /**
     * 换手率
     */
    private BigDecimal turnoverRate;

    /**
     * 成交量
     */
    private BigDecimal volume;

    /**
     * 成交金额
     */
    private BigDecimal amount;

    /**
     * 总市值
     */
    private BigDecimal totalMarketValue;

    /**
     * 流通市值
     */
    private BigDecimal circulationMarketValue;

    /**
     * 五日均值
     */
    private BigDecimal five;

    /**
     * 十日均值
     */
    private BigDecimal ten;

    /**
     * 二十日均值
     */
    private BigDecimal twenty;

    /**
     * 六十日均值
     */
    private BigDecimal sixty;

    /**
     * 一百二十日均值
     */
    private BigDecimal oneHundredAndTwenty;

    /**
     * 二百四十日均值
     */
    private BigDecimal twoHundredAndForty;

    /**
     * MACD的ema12
     */
    private BigDecimal ema12;

    /**
     * MACD的ema26
     */
    private BigDecimal ema26;

    /**
     * MACD的dif
     */
    private BigDecimal dif;

    /**
     * MACD的dea
     */
    private BigDecimal dea;

    public static StockRecord toEntity(JSONObject jsonObject){
        if(null==jsonObject){
            return null;
        }
        StockRecord stockRecord=new StockRecord();
        if(null!=jsonObject.get("id") && !"None".equals(jsonObject.get("id"))){
            stockRecord.setId(jsonObject.get("id").toString());
        }
        if(null!=jsonObject.get("amount") && !"None".equals(jsonObject.get("amount"))){
            stockRecord.setAmount(new BigDecimal(jsonObject.get("amount").toString()));
        }
        if(null!=jsonObject.get("circulation_market_value") && !"None".equals(jsonObject.get("circulation_market_value"))){
            String str=jsonObject.get("circulation_market_value").toString();
            String subStr=str.substring(0, str.length()-1);
//            System.out.println("str:"+str+"     subStr:"+subStr);
            stockRecord.setCirculationMarketValue(new BigDecimal(subStr));
        }
        if(null!=jsonObject.get("close") && !"None".equals(jsonObject.get("close"))){
            stockRecord.setClose(new BigDecimal(jsonObject.get("close").toString()));
        }
        if(null!=jsonObject.get("code") && !"None".equals(jsonObject.get("code"))){
            stockRecord.setCode(jsonObject.get("code").toString());
        }
        if(null!=jsonObject.get("date") && !"None".equals(jsonObject.get("date"))){
            stockRecord.setDate(DateUtil.stringToDate(jsonObject.get("date").toString().replace("-","")));
        }
        if(null!=jsonObject.get("high") && !"None".equals(jsonObject.get("high"))){
            stockRecord.setHigh(new BigDecimal(jsonObject.get("high").toString()));
        }
        if(null!=jsonObject.get("last_close") && !"None".equals(jsonObject.get("last_close"))){
            stockRecord.setLastClose(new BigDecimal(jsonObject.get("last_close").toString()));
        }
        if(null!=jsonObject.get("low") && !"None".equals(jsonObject.get("low"))){
            stockRecord.setLow(new BigDecimal(jsonObject.get("low").toString()));
        }
        if(null!=jsonObject.get("open") && !"None".equals(jsonObject.get("open"))){
            stockRecord.setOpen(new BigDecimal(jsonObject.get("open").toString()));
        }
        if(null!=jsonObject.get("total_market_value") && !"None".equals(jsonObject.get("total_market_value"))){
            stockRecord.setTotalMarketValue(new BigDecimal(jsonObject.get("total_market_value").toString()));
        }
        if(null!=jsonObject.get("up_down") && !"None".equals(jsonObject.get("up_down"))){
            stockRecord.setUpDown(new BigDecimal(jsonObject.get("up_down").toString()));
        }
        if(null!=jsonObject.get("turnover_rate") && !"None".equals(jsonObject.get("turnover_rate"))){
            stockRecord.setTurnoverRate(new BigDecimal(jsonObject.get("turnover_rate").toString()));
        }
        if(null!=jsonObject.get("up_down_amount") && !"None".equals(jsonObject.get("up_down_amount"))){
            stockRecord.setUpDownAmount(new BigDecimal(jsonObject.get("up_down_amount").toString()));
        }
        if(null!=jsonObject.get("up_down_percentage") && !"None".equals(jsonObject.get("up_down_percentage"))){
            stockRecord.setUpDownPercentage(new BigDecimal(jsonObject.get("up_down_percentage").toString()));
        }
        if(null!=jsonObject.get("volume") && !"None".equals(jsonObject.get("volume"))){
            stockRecord.setVolume(new BigDecimal(jsonObject.get("volume").toString()));
        }

        return stockRecord;
    }

    public BigDecimal getDea() {
        return dea;
    }

    public void setDea(BigDecimal dea) {
        this.dea = dea;
    }

    public BigDecimal getDif() {
        return dif;
    }

    public void setDif(BigDecimal dif) {
        this.dif = dif;
    }

    public BigDecimal getEma26() {

        return ema26;
    }

    public void setEma26(BigDecimal ema26) {
        this.ema26 = ema26;
    }

    public void setEma12(BigDecimal ema12) {

        this.ema12 = ema12;
    }

    public BigDecimal getEma12() {

        return ema12;
    }

    public BigDecimal getTwoHundredAndForty() {
        return twoHundredAndForty;
    }

    public void setTwoHundredAndForty(BigDecimal twoHundredAndForty) {
        this.twoHundredAndForty = twoHundredAndForty;
    }

    public BigDecimal getOneHundredAndTwenty() {

        return oneHundredAndTwenty;
    }

    public void setOneHundredAndTwenty(BigDecimal oneHundredAndTwenty) {
        this.oneHundredAndTwenty = oneHundredAndTwenty;
    }

    public BigDecimal getSixty() {

        return sixty;
    }

    public void setSixty(BigDecimal sixty) {
        this.sixty = sixty;
    }

    public BigDecimal getTwenty() {

        return twenty;
    }

    public void setTwenty(BigDecimal twenty) {
        this.twenty = twenty;
    }

    public BigDecimal getTen() {

        return ten;
    }

    public void setTen(BigDecimal ten) {
        this.ten = ten;
    }

    public BigDecimal getFive() {

        return five;
    }

    public void setFive(BigDecimal five) {
        this.five = five;
    }

    public BigDecimal getCirculationMarketValue() {

        return circulationMarketValue;
    }

    public void setCirculationMarketValue(BigDecimal circulationMarketValue) {
        this.circulationMarketValue = circulationMarketValue;
    }

    public BigDecimal getTotalMarketValue() {

        return totalMarketValue;
    }

    public void setTotalMarketValue(BigDecimal totalMarketValue) {
        this.totalMarketValue = totalMarketValue;
    }

    public BigDecimal getAmount() {

        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getVolume() {

        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public BigDecimal getTurnoverRate() {

        return turnoverRate;
    }

    public void setTurnoverRate(BigDecimal turnoverRate) {
        this.turnoverRate = turnoverRate;
    }

    public BigDecimal getUpDownPercentage() {

        return upDownPercentage;
    }

    public void setUpDownPercentage(BigDecimal upDownPercentage) {
        this.upDownPercentage = upDownPercentage;
    }

    public BigDecimal getUpDownAmount() {

        return upDownAmount;
    }

    public void setUpDownAmount(BigDecimal upDownAmount) {
        this.upDownAmount = upDownAmount;
    }

    public BigDecimal getUpDown() {

        return upDown;
    }

    public void setUpDown(BigDecimal upDown) {
        this.upDown = upDown;
    }

    public BigDecimal getLastClose() {

        return lastClose;
    }

    public void setLastClose(BigDecimal lastClose) {
        this.lastClose = lastClose;
    }

    public BigDecimal getLow() {

        return low;
    }

    public void setLow(BigDecimal low) {
        this.low = low;
    }

    public BigDecimal getClose() {

        return close;
    }

    public void setClose(BigDecimal close) {
        this.close = close;
    }

    public BigDecimal getHigh() {

        return high;
    }

    public void setHigh(BigDecimal high) {
        this.high = high;
    }

    public BigDecimal getOpen() {

        return open;
    }

    public void setOpen(BigDecimal open) {
        this.open = open;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDate() {

        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
