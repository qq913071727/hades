package org.adam2.chamuel.interceptor;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;

import java.util.List;

public class StockRecordInterceptor implements Interceptor {
    public void initialize() {

    }

    public Event intercept(Event event) {
        return null;
    }

    public List<Event> intercept(List<Event> list) {
        return null;
    }

    public void close() {

    }

    public static class Builder implements Interceptor.Builder {
        //使用Builder初始化Interceptor
        public Interceptor build() {
            return new StockRecordInterceptor();
        }

        public void configure(Context context) {

        }
    }
}
