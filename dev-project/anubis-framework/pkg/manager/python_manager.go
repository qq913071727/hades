package manager

//
//import (
//	"fmt"
//	"github.com/DataDog/go-python3"
//	"os"
//)
//
type PythonManager struct {
}

//
//func (_pythonManager PythonManager) Init() {
//	// 初始化python环境
//	python3.Py_Initialize()
//	if !python3.Py_IsInitialized() {
//		fmt.Println("Error initializing the python interpreter")
//		os.Exit(1)
//	}
//
//}
//
//// InsertBeforeSysPath
//// @Description: 添加site-packages路径即包的查找路径
//// @param p
//func (_pythonManager PythonManager) InsertBeforeSysPath(p string) {
//	sysModule := python3.PyImport_ImportModule("sys")
//	path := sysModule.GetAttrString("path")
//	python3.PyList_Append(path, python3.PyUnicode_FromString(p))
//}
//
//// ImportModule
//// @Description: 倒入一个包
//// @param dir
//// @param name
//// @return *python3.PyObject
//func (_pythonManager PythonManager) ImportModule(dir, name string) *python3.PyObject {
//	sysModule := python3.PyImport_ImportModule("sys")                 // import sys
//	path := sysModule.GetAttrString("path")                           // path = sys.path
//	python3.PyList_Insert(path, 0, python3.PyUnicode_FromString(dir)) // path.insert(0, dir)
//	return python3.PyImport_ImportModule(name)                        // return __import__(name)
//}
//
//// pythonRepr
//// @Description: PyObject转换为string
//// @param o
//// @return string
//// @return error
//func (_pythonManager PythonManager) PythonRepr(o *python3.PyObject) (string, error) {
//	if o == nil {
//		return "", fmt.Errorf("object is nil")
//	}
//	s := o.Repr()
//	if s == nil {
//		python3.PyErr_Clear()
//		return "", fmt.Errorf("failed to call Repr object method")
//	}
//	defer s.DecRef()
//
//	return python3.PyUnicode_AsUTF8(s), nil
//}
//
//// PrintList
//// @Description: 输出一个List
//// @param list
//// @return error
//func (_pythonManager PythonManager) PrintList(list *python3.PyObject) error {
//	if exc := python3.PyErr_Occurred(); list == nil && exc != nil {
//		return fmt.Errorf("Fail to create python list object")
//	}
//	defer list.DecRef()
//	repr, err := _pythonManager.PythonRepr(list)
//	if err != nil {
//		return fmt.Errorf("fail to get representation of object list")
//	}
//	fmt.Printf("python list: %s\n", repr)
//	return nil
//}
