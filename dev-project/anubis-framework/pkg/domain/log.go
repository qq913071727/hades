package domain

import "github.com/sirupsen/logrus"

type Log struct {
	Logger       *logrus.Logger
	LogFormatter LogFormatter
}

var Log_ = &Log{}
