package domain

// 应用程序的结构体
type Application struct {
	LogPath            string
	CoroutinePoolCount int
}

var Application_ = &Application{}
