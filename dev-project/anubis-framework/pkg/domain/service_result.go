package domain

type ServiceResult struct {
	// 返回代码
	Code int
	// 返回消息
	Message string
	// 返回结果
	Result interface{}
	// 接口调用是否成功
	Success bool
}
