package domain

import "time"

// 表示数据库的结构体
type Database struct {
	/*********************** oracle使用 *********************/
	Username string
	Password string
	Host     string
	Schema   string

	/*********************** mysql使用 *********************/
	DriveSourceName string

	/*********************** 通用 *********************/
	MaxOpenConnections    int
	MaxIdleConnections    int
	MaxConnectionLifetime time.Duration
}

var Database_ = &Database{}
