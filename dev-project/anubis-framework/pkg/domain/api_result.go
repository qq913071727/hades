package domain

// 返回值
type ApiResult struct {
	// 返回代码
	Code int
	// 返回消息
	Message string
	// 返回结果
	Result interface{}
	// 接口调用是否成功
	Success bool
}
