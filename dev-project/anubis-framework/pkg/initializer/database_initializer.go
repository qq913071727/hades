package initializer

import (
	pDomain "anubis-framework/pkg/domain"
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/util"
	"github.com/go-ini/ini"
)

// 读取配置文件，初始化数据库
func InitDatabaseProperties() {
	io.Infoln("开始读取配置文件app.ini，初始化数据库")

	var cfg *ini.File
	var err error
	cfg, err = ini.Load("internal/config/app.ini")
	if err != nil {
		io.Fatalf("读取配置文件失败: %v", err.Error())
	} else {
		util.MapTo(cfg, "database", pDomain.Database_)
	}
}
