package initializer

import (
	"anubis-framework/pkg/domain"
	"github.com/sirupsen/logrus"
	"io"
	"os"
)

// 初始化日志配置
func InitLog(filename string) {
	domain.Log_.Logger = logrus.New() //新建一个实例
	file, _ := os.OpenFile(filename, os.O_CREATE|os.O_RDWR|os.O_APPEND, 0755)
	domain.Log_.Logger.SetOutput(io.MultiWriter(os.Stdout, file)) //设置输出类型
	domain.Log_.Logger.SetReportCaller(true)                      //开启返回函数名和行号
	//mLog.SetFormatter(&logrus.JSONFormatter{})//设置自定义的Formatter
	domain.Log_.Logger.SetFormatter(domain.Log_.LogFormatter) //设置自定义的Formatter
	domain.Log_.Logger.SetLevel(logrus.DebugLevel)            //设置最低等级
}
