package ui

import (
	"syscall"
	"unsafe"
)

// 弹出提示对话框
func MessageBox(message string, title string) {
	user32dll, _ := syscall.LoadLibrary("user32.dll")
	user32 := syscall.NewLazyDLL("user32.dll")
	MessageBoxW := user32.NewProc("MessageBoxW")
	MessageBoxW.Call(intPtr(0), strPtr(message), strPtr(title), intPtr(0))
	defer syscall.FreeLibrary(user32dll)
}

func intPtr(n int) uintptr {
	return uintptr(n)
}

func strPtr(s string) uintptr {
	return uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr(s)))
}
