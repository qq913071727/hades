package io

import (
	"anubis-framework/pkg/domain"
	"fmt"
)

// 打印info级别的日志
func Infoln(message string, argArray ...interface{}) {
	var formatString string
	if len(argArray) == 0 {
		domain.Log_.Logger.Infoln(message)
	}
	if len(argArray) == 1 {
		formatString = fmt.Sprintf(message, argArray[0])
		domain.Log_.Logger.Infoln(formatString)
	}
	if len(argArray) == 2 {
		formatString = fmt.Sprintf(message, argArray[0], argArray[1])
		domain.Log_.Logger.Infoln(formatString)
	}
	if len(argArray) == 3 {
		formatString = fmt.Sprintf(message, argArray[0], argArray[1], argArray[2])
		domain.Log_.Logger.Infoln(formatString)
	}
	if len(argArray) == 4 {
		formatString = fmt.Sprintf(message, argArray[0], argArray[1], argArray[2], argArray[3])
		domain.Log_.Logger.Infoln(formatString)
	}
	if len(argArray) == 5 {
		formatString = fmt.Sprintf(message, argArray[0], argArray[1], argArray[2], argArray[3], argArray[4])
		domain.Log_.Logger.Infoln(formatString)
	}
}

// 打印fatal级别的日志
func Fatalf(message string, argArray ...interface{}) {
	var formatString string
	if len(argArray) == 0 {
		domain.Log_.Logger.Fatalf(message)
	}
	if len(argArray) == 1 {
		formatString = fmt.Sprintf(message, argArray[0])
		domain.Log_.Logger.Fatalf(formatString)
	}
	if len(argArray) == 2 {
		formatString = fmt.Sprintf(message, argArray[0], argArray[1])
		domain.Log_.Logger.Fatalf(formatString)
	}
	if len(argArray) == 3 {
		formatString = fmt.Sprintf(message, argArray[0], argArray[1], argArray[2])
		domain.Log_.Logger.Fatalf(formatString)
	}
	if len(argArray) == 4 {
		formatString = fmt.Sprintf(message, argArray[0], argArray[1], argArray[2], argArray[3])
		domain.Log_.Logger.Fatalf(formatString)
	}
	if len(argArray) == 5 {
		formatString = fmt.Sprintf(message, argArray[0], argArray[1], argArray[2], argArray[3], argArray[4])
		domain.Log_.Logger.Fatalf(formatString)
	}
}
