package util

import (
	"crypto/md5"
	"fmt"
	"io"
)

// 通过salt，加密密码。32位小写
func Md5Password(password string, salt string) string {
	h := md5.New()
	io.WriteString(h, password+salt)
	return fmt.Sprintf("%x", h.Sum(nil))
}
