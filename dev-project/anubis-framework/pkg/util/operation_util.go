package util

// 三元预算，例如：
// a, b := 2, 3
// max := If(a > b, a, b).(int)
// println(max)
func If(condition bool, trueVal interface{}, falseVal interface{}) interface{} {
	if condition {
		return trueVal
	}
	return falseVal
}
