package util

import (
	"fmt"
	"strconv"
	"time"
)

// 格式
const DATE_LAYOUT string = "2006-01-02"
const TIME_LAYOUT string = "15:04:05"

// 将time.Time转换为string类型
func DateTimeToString(date time.Time) string {
	return date.Format(DATE_LAYOUT + " " + TIME_LAYOUT)
}

// 将time.Time转换为string类型
func DateToString(date time.Time) string {
	return date.Format(DATE_LAYOUT)
}

// 将string转换为time.Time类型
func StringToDate(date string) time.Time {
	var _date, _ = time.ParseInLocation(DATE_LAYOUT, date, time.Local)
	return _date
}

func StringToDateTime(date string) time.Time {
	var _date, _ = time.ParseInLocation(DATE_LAYOUT+" "+TIME_LAYOUT, date, time.Local)
	return _date
}

// 返回15:04:05格式的时间字符串
func StringToTime(_time string) time.Time {
	var time_, _ = time.ParseInLocation(TIME_LAYOUT, _time, time.Local)
	return time_
}

// 在给定日期的基础上，向后n天
func AfterNDay(date string, n int) string {
	loc, _ := time.LoadLocation("Local")
	theTime, _ := time.ParseInLocation(DATE_LAYOUT, date, loc)
	return theTime.AddDate(0, 0, n).Format(DATE_LAYOUT)
}

// 在给定日期的基础上，向前n天
func BeforeNDay(date string, n int) string {
	loc, _ := time.LoadLocation("Local")
	theTime, _ := time.ParseInLocation(DATE_LAYOUT, date, loc)
	return theTime.AddDate(0, 0, -n).Format(DATE_LAYOUT)
}

// 如果source的日期在target的日期之前的话，则返回true，否则返回false
func Before(source string, target string) bool {
	loc, _ := time.LoadLocation("Local")
	theSource, _ := time.ParseInLocation(DATE_LAYOUT, source, loc)
	theTarget, _ := time.ParseInLocation(DATE_LAYOUT, target, loc)
	return theSource.Before(theTarget)
}

// 如果source的日期在target的日期之后的话，则返回true，否则返回false
func After(source string, target string) bool {
	loc, _ := time.LoadLocation("Local")
	theSource, _ := time.ParseInLocation(DATE_LAYOUT, source, loc)
	theTarget, _ := time.ParseInLocation(DATE_LAYOUT, target, loc)
	return theSource.After(theTarget)
}

// 如果source的日期在target的日期之后或者相等的话，则返回true，否则返回false
func AfterOrEqual(source string, target string) bool {
	loc, _ := time.LoadLocation("Local")
	theSource, _ := time.ParseInLocation(DATE_LAYOUT, source, loc)
	theTarget, _ := time.ParseInLocation(DATE_LAYOUT, target, loc)
	if theSource.After(theTarget) {
		return true
	}
	if theSource.Equal(theTarget) {
		return true
	}
	return false
}

// 如果source的日期在target的日期相同的话，则返回true，否则返回false
func Equal(source string, target string) bool {
	loc, _ := time.LoadLocation("Local")
	theSource, _ := time.ParseInLocation(DATE_LAYOUT, source, loc)
	theTarget, _ := time.ParseInLocation(DATE_LAYOUT, target, loc)
	return theSource.Equal(theTarget)
}

// 把time.time类型的数据转换为YYMMDDHHMMSS的格式(float64)
func DateTimeToFloat64(date time.Time) float64 {
	timeString := strconv.Itoa(date.Year()) + fmt.Sprintf("%02d", date.Month()) + fmt.Sprintf("%02d", date.Day()) + fmt.Sprintf("%02d", date.Hour()) + fmt.Sprintf("%02d", date.Minute()) + fmt.Sprintf("%02d", date.Second())
	timeFloat64, _ := strconv.ParseFloat(timeString, 64)
	return timeFloat64
}

// 把time.time类型的数据转换为YYMMDD的格式(float64)
func DateToFloat64(date time.Time) float64 {
	timeString := strconv.Itoa(date.Year()) + fmt.Sprintf("%02d", date.Month()) + fmt.Sprintf("%02d", date.Day())
	timeFloat64, _ := strconv.ParseFloat(timeString, 64)
	return timeFloat64
}
