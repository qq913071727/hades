package util

import (
	"anubis-framework/pkg/domain"
	"net/http"
)

// 将ServiceResult类型转换为ApiResult
func ServiceResultToApiResult(serviceResult *domain.ServiceResult) (int, *domain.ApiResult) {
	var apiResult *domain.ApiResult = &domain.ApiResult{}
	code := If(serviceResult.Success, http.StatusOK, http.StatusBadRequest).(int)
	apiResult.Code = serviceResult.Code
	apiResult.Message = serviceResult.Message
	apiResult.Result = serviceResult.Result
	apiResult.Success = serviceResult.Success
	return code, apiResult
}
