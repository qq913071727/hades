package util

import "math"

// 计算sigmoid的函数
func Sigmoid(x float64) float64 {
	return 1 / (1 + math.Pow(math.E, -x))
}
