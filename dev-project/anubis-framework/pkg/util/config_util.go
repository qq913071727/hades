package util

import (
	"github.com/go-ini/ini"
)

// 从配置文件中，根据key取value
func MapTo(cfg *ini.File, section string, v interface{}) error {
	err := cfg.Section(section).MapTo(v)
	return err
}
