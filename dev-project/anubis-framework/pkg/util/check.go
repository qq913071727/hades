package util

import "reflect"

// 判断结构体是否为空
func IsEmptyStruct(s interface{}) bool {
	val := reflect.ValueOf(s)
	for i := 0; i < val.NumField(); i++ {
		field := val.Field(i)
		zero := reflect.Zero(field.Type())
		if field.Interface() != zero.Interface() {
			return false
		}
	}
	return true
}
