package constants

/********************************* 操作成功 ************************************/
// 操作成功code
const OperationSuccessCode = 200

// 操作成功message
const OperationSuccessMessage = "操作成功"

/********************************* 操作失败 ************************************/
// 操作失败code
const OperationFailCode = 200

// 操作失败message
const OperationFailMessage = "操作失败"

/********************************* 数据已经存在 ************************************/
// 数据已经存在code
const ExistsCode = 4001

// 数据已经存在message
const ExistsMessage = "数据已经存在"

/********************************* 数据不存在 ************************************/
// 数据不存在code
const NotExistsCode = 4002

// 数据不存在message
const NotExistsMessage = "数据不存在"
