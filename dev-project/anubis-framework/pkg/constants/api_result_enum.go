package constants

// 登录成功code
const LoginSuccessCode int = 200

// 登录成功message
const LoginSuccessMessage string = "登录成功"

// 登录失败code
const LoginFailCode int = 3001

// 登录失败message
const LoginFailMessage string = "登录失败"

// 参数不能为空code
const ParameterCannotBeNilCode int = 3002

// 参数不能为空message
const ParameterCannotBeNilMessage string = "参数不能为空"
