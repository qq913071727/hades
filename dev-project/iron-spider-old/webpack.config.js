//声明变量
var webpack = require('webpack');
var PATH = require('path');//这是nodejs的核心模块之一
var SRC_PATH = PATH.resolve(__dirname, './src');
var DIST_PATH = PATH.resolve(__dirname, './dist');


module.exports = {
  entry: SRC_PATH + '/main.js',
  output: {
    path: DIST_PATH,
    filename: 'bundle.js'
  },
  //loader
  module: {
  },
  //插件
  plugins: [
  ],
  devServer: {//开发服务器
    hot: true,//热更新
    inline: true,//
    open: true,//是否自动打开默认浏览器
    contentBase: DIST_PATH,//发布目录
    port: '3996',//控制端口
    host: '0.0.0.0',//host地址
    historyApiFallback: true,
    useLocalIp: true,//是否用自己的IP
    proxy: {
      '/action': 'http://127.0.0.1:8080/'
    }
  }
}