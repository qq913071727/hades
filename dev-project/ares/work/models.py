from django.db import models
import django.utils.timezone as timezone

'''消息类'''


class Message(models.Model):
    # 主键
    id = models.IntegerField

    # 标题
    title = models.CharField(max_length=50)

    # 描述
    description = models.CharField(max_length=200)

    # 创建时间
    create_time = models.DateTimeField('保存日期',default = timezone.now)

    def __str__(self):
        return "id=" + str(self.id) + ", title=" + self.title + ", description=" + self.description + ", create_time=" + str(self.create_time)
