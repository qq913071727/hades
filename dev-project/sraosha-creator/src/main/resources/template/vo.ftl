package ${vo.packageName};

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * ${vo.classComment}
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "${vo.className}", description = "${vo.classComment}")
public class ${vo.className} {
<#list model.columnOrAttributeList as column>
    @ApiModelProperty(value = "${column.comment}", dataType = "${column.attributeType}")
    private ${column.attributeType} ${column.attributeName};

</#list>
    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;
}