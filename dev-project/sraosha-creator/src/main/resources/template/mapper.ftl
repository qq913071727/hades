package ${mapper.packageName};

import ${model.packageName}.${model.className};
import org.springframework.stereotype.Repository;

/**
 * ${mapper.classComment}
 */
@Repository
public interface ${mapper.className} extends EasyBaseMapper<${model.className}> {

}