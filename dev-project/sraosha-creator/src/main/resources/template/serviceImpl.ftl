package ${serviceImpl.packageName};

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ${mapper.packageName}.${mapper.className};
import ${model.packageName}.${model.className};
import ${service.packageName}.${service.className};
import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.framework.enumeration.ServiceResultEnum;

import java.io.Serializable;
import java.util.List;

/**
 * ${serviceImpl.classComment}
 */
@Slf4j
@Service
public class ${serviceImpl.className} extends ServiceImpl<${mapper.className}, ${model.className}> implements ${service.className} {

    /**
     * 新增
     * @param ${model.instanceName} ${model.tableComment}
     * @return 添加成功返回1，添加失败返回-1
     */
    @Override
    public ServiceResult<Boolean> add(${model.className} ${model.instanceName}) {
        baseMapper.insert(${model.instanceName});
        return new ServiceResult<>(ServiceResultEnum.INSERT_SUCCESS.getCode(), Boolean.TRUE, Boolean.TRUE, ServiceResultEnum.INSERT_SUCCESS.getMessage());
    }

    /**
     * 批量导入
     * @param ${model.instanceName}List ${model.tableComment}（列表）
     * @return 批量导入成功返回1，批量导入失败返回-1
     */
    @Override
    public ServiceResult<Boolean> batchAdd(List<${model.className}> ${model.instanceName}List) {
        baseMapper.insertBatchSomeColumn(${model.instanceName}List);
        return new ServiceResult<>(ServiceResultEnum.INSERT_SUCCESS.getCode(), Boolean.TRUE, Boolean.TRUE, ServiceResultEnum.INSERT_SUCCESS.getMessage());
    }

    /**
     * 根据id更新
     * @param ${model.instanceName} ${model.tableComment}
     * @return 更新成功返回1，更新失败返回-1
     */
    @Override
    public ServiceResult<Boolean> updateForId(${model.className} ${model.instanceName}) {
        baseMapper.updateById(${model.instanceName});
        return new ServiceResult<>(ServiceResultEnum.UPDATE_SUCCESS.getCode(), Boolean.TRUE, Boolean.TRUE, ServiceResultEnum.UPDATE_SUCCESS.getMessage());
    }

    /**
     * 根据id删除
     * @param id 主键
     * @return 删除成功返回1，删除失败返回-1
     */
    @Override
    public ServiceResult<Boolean> deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return new ServiceResult<>(ServiceResultEnum.DELETE_SUCCESS.getCode(), Boolean.TRUE, Boolean.TRUE, ServiceResultEnum.DELETE_SUCCESS.getMessage());
    }

    /**
     * 根据id查询
     * @param id 主键
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<${model.className}> selectById(Serializable id) {
        ${model.className} ${model.instanceName} = baseMapper.selectById(id);
        return new ServiceResult<>(ServiceResultEnum.SELECT_SUCCESS.getCode(), ${model.instanceName}, Boolean.TRUE, ServiceResultEnum.SELECT_SUCCESS.getMessage());
    }

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param ${model.instanceName} ${model.tableComment}
     * @param pageNo 页号
     * @param pageSize 每页行数
     * @return 返回查询结果列表
     */
    @Override
    public ServiceResult<PageInfo<${model.className}>> page(${model.className} ${model.instanceName}, int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        LambdaQueryWrapper<${model.className}> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        List<${model.className}> ${model.instanceName}List = baseMapper.selectList(lambdaQueryWrapper);
        return new ServiceResult<>(ServiceResultEnum.SELECT_SUCCESS.getCode(), new PageInfo<>(${model.instanceName}List), Boolean.TRUE, ServiceResultEnum.SELECT_SUCCESS.getMessage());
    }

    /**
     * 查询所有记录
     * @return
     */
    @Override
    public ServiceResult<List<${model.className}>> findAll() {
        List<${model.className}> ${model.instanceName}List = baseMapper.selectList(null);
        return new ServiceResult<>(ServiceResultEnum.SELECT_SUCCESS.getCode(), ${model.instanceName}List, Boolean.TRUE, ServiceResultEnum.SELECT_SUCCESS.getMessage());
    }
}
