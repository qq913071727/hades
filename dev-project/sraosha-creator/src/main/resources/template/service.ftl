package ${service.packageName};

import org.sraosha.framework.dto.ServiceResult;
import ${model.packageName}.${model.className};

import com.github.pagehelper.PageInfo;

import java.io.Serializable;
import java.util.List;

/**
 * ${service.classComment}
 */
public interface ${service.className} {

    /**
     * 新增
     * @param ${model.instanceName} ${model.tableComment}
     * @return 添加成功返回1，添加失败返回-1
     */
    ServiceResult<Boolean> add(${model.className} ${model.instanceName});

    /**
     * 批量导入
     * @param ${model.instanceName}List ${model.tableComment}（列表）
     * @return 批量导入成功返回1，批量导入失败返回-1
     */
    ServiceResult<Boolean> batchAdd(List<${model.className}> ${model.instanceName}List);

    /**
     * 更新
     * @param ${model.instanceName} ${model.tableComment}
     * @return 更新成功返回1，更新失败返回-1
     */
    ServiceResult<Boolean> updateForId(${model.className} ${model.instanceName});

    /**
     * 删除
     * @param id 主键
     * @return 删除成功返回1，删除失败返回-1
     */
    ServiceResult<Boolean> deleteById(Serializable id);

    /**
     * 根据id查询
     * @param id 主键
     * @return 返回查询结果
     */
    ServiceResult<${model.className}> selectById(Serializable id);

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param ${model.instanceName} ${model.tableComment}
     * @param pageNo 页号
     * @param pageSize 每页行数
     * @return 返回查询结果列表
     */
    ServiceResult<PageInfo<${model.className}>> page(${model.className} ${model.instanceName}, int pageNo, int pageSize);

    /**
     * 查询所有记录
     * @return
     */
    ServiceResult<List<${model.className}>> findAll();
}