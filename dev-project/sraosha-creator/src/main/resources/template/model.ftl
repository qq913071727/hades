package ${model.packageName};

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.sql.Blob;
import java.sql.Clob;

/**
 * ${model.tableComment}
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "${model.className}", description = "${model.tableComment}")
@TableName("${model.tableName}")
public class ${model.className} {

<#list model.columnOrAttributeList as column>
    @ApiModelProperty(value = "${column.comment}", dataType = "${column.attributeType}")
    <#if column.identityColumn="PRI">
    @TableId(value = "${column.columnName}", type = IdType.AUTO)
    <#else>
    <#if column.attributeType="Date">
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    </#if>
    @TableField(value = "${column.columnName}")
    </#if>
    private ${column.attributeType} ${column.attributeName};

</#list>
}
