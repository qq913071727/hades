package ${controller.packageName};

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ${model.packageName}.${model.className};
import org.sraosha.framework.dto.ApiResult;
import org.sraosha.framework.dto.ServiceResult;
import ${vo.packageName}.${vo.className};

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * ${controller.classComment}
 */
@Api(value = "${controller.classComment}", tags = "${controller.className}")
@RestController
@RequestMapping("api/v1/${model.instanceName}")
@Slf4j
public class ${controller.className} extends AbstractController {

    /**
     * 新增
     * @param ${vo.instanceName} ${vo.classComment}
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("新增")
    @ApiImplicitParam(name = "${vo.instanceName}", value = "${vo.classComment}", required = true, dataType = "${vo.className}")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> add(@RequestBody ${vo.className} ${vo.instanceName}, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == ${vo.instanceName}) {
            String message = "调用接口【/api/v1/${model.instanceName}/add时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ${model.className} ${model.instanceName} = new ${model.className}();
        BeanUtils.copyProperties(${vo.instanceName}, ${model.instanceName});
        ServiceResult<Boolean> serviceResult = ${service.variableName}.add(${model.instanceName});

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 批量导入
     * @param ${vo.instanceName}List ${vo.classComment}列表
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("批量导入")
    @ApiImplicitParam(name = "${vo.instanceName}List", value = "${vo.classComment}列表", required = true, dataType = "List")
    @RequestMapping(value = "/batchAdd", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> batchAdd(@RequestBody List<${vo.className}> ${vo.instanceName}List, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == ${vo.instanceName}List) {
            String message = "调用接口【/api/v1/${model.instanceName}/batchAdd时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        List<${model.className}> ${model.instanceName}List = new ArrayList<>();
        BeanUtils.copyProperties(${vo.instanceName}List, ${model.instanceName}List);
        ServiceResult<Boolean> serviceResult = ${service.variableName}.batchAdd(${model.instanceName}List);

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id更新
     * @param ${vo.instanceName} ${vo.classComment}
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id更新")
    @ApiImplicitParam(name = "${vo.instanceName}", value = "${vo.classComment}", required = true, dataType = "${vo.className}")
    @RequestMapping(value = "/updateForId", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> updateForId(@RequestBody ${vo.className} ${vo.instanceName}, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == ${vo.instanceName} || null == ${vo.instanceName}.getId()) {
            String message = "调用接口【/api/v1/${model.instanceName}/updateForId时，参数不合法，id不能为空";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ${model.className} ${model.instanceName} = new ${model.className}();
        BeanUtils.copyProperties(${vo.instanceName}, ${model.instanceName});
        ServiceResult<Boolean> serviceResult = ${service.variableName}.updateForId(${model.instanceName});

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id删除
     * @param id 主键
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id删除")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Integer")
    @RequestMapping(value = "/deleteById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Boolean>> deleteById(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == id) {
            String message = "调用接口【/api/v1/${model.instanceName}/deleteById时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Boolean> serviceResult = ${service.variableName}.deleteById(id);

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id查询
     * @param id 主键
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id查询")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Integer")
    @RequestMapping(value = "/selectById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<${model.className}>> selectById(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == id) {
            String message = "调用接口【/api/v1/${model.instanceName}/selectById时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<${model.className}> serviceResult = ${service.variableName}.selectById(id);

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param ${vo.instanceName} ${vo.classComment}
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据条件，分页查询，升序/降序排列")
    @ApiImplicitParam(name = "${vo.instanceName}", value = "${vo.classComment}", required = true, dataType = "${vo.className}")
    @RequestMapping(value = "/page", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<PageInfo<${model.className}>>> page(@RequestBody ${vo.className} ${vo.instanceName}, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == ${vo.instanceName}) {
            String message = "调用接口【/api/v1/${model.instanceName}/page时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (null == ${vo.instanceName}.getPageNo() || null == ${vo.instanceName}.getPageSize()) {
            String message = "调用接口【/api/v1/${model.instanceName}/page时，参数pageNo和pageSize不能为空";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ${model.className} ${model.instanceName} = new ${model.className}();
        BeanUtils.copyProperties(${vo.instanceName}, ${model.instanceName});
        ServiceResult<PageInfo<${model.className}>> serviceResult = ${service.variableName}.page(${model.instanceName}, ${vo.instanceName}.getPageNo(), ${vo.instanceName}.getPageSize());

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 查询所有记录
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("查询所有记录")
    @ApiImplicitParam(name = "", value = "", required = true, dataType = "")
    @RequestMapping(value = "/findAll", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<List<${model.className}>>> findAll(HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();

        ServiceResult<List<${model.className}>> serviceResult = ${service.variableName}.findAll();

        apiResult.setSuccess(serviceResult.getSuccess());
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
