package org.sraosha.creator.manager;

import java.sql.ResultSet;

/**
 * 数据库管理（接口）
 */
public interface IDatabaseManager {

    /**
     * 关闭preparedStatement和connection
     */
    void close();

    /**
     * 返回当前用户的所有表的信息
     *
     * @return
     */
    ResultSet findUserTable();

    /**
     * 根据表名，返回表的所有列的信息
     *
     * @param tableName
     * @return
     */
    ResultSet findTableColumnByTableName(String tableName);

    /**
     * 根据表名称返回表的注释
     *
     * @param tableName
     * @return
     */
    String findCommentByTableName(String tableName);

    /**
     * 根据表名和列名，返回注解
     *
     * @param tableName
     * @param columnName
     * @return
     */
    String findCommentByTableNameAndColumnName(String tableName, String columnName);

    /**
     * 返回表名
     * @param resultSet
     * @return
     */
    String getTableName(ResultSet resultSet);

    /**
     * 返回列名
     * @param resultSet
     * @return
     */
    String getColumnName(ResultSet resultSet);

    /**
     * 返回列类型
     * @param resultSet
     * @return
     */
    String getColumnType(ResultSet resultSet);

    /**
     * 返回主键
     * @param resultSet
     * @return
     */
    String getIdentityColumn(ResultSet resultSet);
}
