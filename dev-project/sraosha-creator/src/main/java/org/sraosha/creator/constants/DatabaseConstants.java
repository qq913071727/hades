package org.sraosha.creator.constants;

import org.sraosha.framework.util.PropertiesUtil;

/**
 * 数据库类型
 */
public class DatabaseConstants {

    /**
     * 数据库
     */
    public static final String DATABASE_TYPE = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.databaseType");

    /**
     * 表格名称列表
     */
    public static final String GENERATE_TABLE_NAME = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.tableName");

    /**
     * oracle数据库
     */
    public static final String ORACLE = "oracle";

    /**
     * mysql数据库
     */
    public static final String MYSQL = "mysql";

    /**
     * oracle数据库url
     */
    public static final String ORACLE_URL = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.oracle.url");

    /**
     * oracle数据库驱动类
     */
    public static final String ORACLE_DRIVER_CLASS_NAME = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.oracle.driverClassName");

    /**
     * oracle数据库用户名
     */
    public static final String ORACLE_USERNAME = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.oracle.username");

    /**
     * oracle数据库密码
     */
    public static final String ORACLE_PASSWORD = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.oracle.password");

    /**
     * mysql数据库url
     */
    public static final String MYSQL_URL = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.mysql.url");

    /**
     * mysql数据库驱动类
     */
    public static final String MYSQL_DRIVER_CLASS_NAME = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.mysql.driverClassName");

    /**
     * mysql数据库名
     */
    public static final String MYSQL_DATABASE_NAME = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.mysql.databaseName");

    /**
     * mysql数据库用户名
     */
    public static final String MYSQL_USERNAME = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.mysql.username");

    /**
     * mysql数据库密码
     */
    public static final String MYSQL_PASSWORD = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.mysql.password");
}
