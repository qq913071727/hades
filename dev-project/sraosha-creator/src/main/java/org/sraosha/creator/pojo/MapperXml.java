package org.sraosha.creator.pojo;

import lombok.Data;

/**
 * mapper类对应的xml文件
 */
@Data
public class MapperXml {
    
    /**
     * 包名
     */
    private String packageName;
    
    /**
     * 类名
     */
    private String className;
    
}
