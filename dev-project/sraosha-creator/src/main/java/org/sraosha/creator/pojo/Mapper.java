package org.sraosha.creator.pojo;

import lombok.Data;

/**
 * mapper类
 */
@Data
public class Mapper {

    /**
     * 基础包名
     */
    private String basePackageName;
    
    /**
     * 包名
     */
    private String packageName;
    
    /**
     * 类名
     */
    private String className;
    
    /**
     * 类注释
     */
    private String classComment;
}
