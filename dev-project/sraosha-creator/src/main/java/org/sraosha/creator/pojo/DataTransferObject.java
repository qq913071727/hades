package org.sraosha.creator.pojo;

import lombok.Data;

/**
 * 视图类
 */
@Data
public class DataTransferObject {

    /**
     * 包名
     */
    private String packageName;

    /**
     * 类名
     */
    private String className;

    /**
     * 类的注解
     */
    private String classComment;

    /**
     * 实例名称
     */
    private String instanceName;
}
