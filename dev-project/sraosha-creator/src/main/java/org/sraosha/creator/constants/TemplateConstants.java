package org.sraosha.creator.constants;

import org.sraosha.framework.util.PropertiesUtil;

/**
 * 模板信息
 *
 */
public class TemplateConstants {

    /*------------------------------- model -------------------------------*/
    public static final String MODEL = "model";

    public static final String MODEL_PACKAGE_NAME = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.model.packageName");

    public static final String MODEL_FILENAME = "model.ftl";

    /*------------------------------- mapper -------------------------------*/
    public static final String MAPPER = "mapper";

    public static final String MAPPER_BASE_PACKAGE_NAME = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.mapper.basePackageName");

    public static final String MAPPER_PACKAGE_NAME = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.mapper.packageName");

    public static final String MAPPER_FILENAME = "mapper.ftl";
    
    public static final String MAPPER_CLASS_SUFFIX = "Mapper";

    public static final String MAPPER_CLASS_COMMENT_SUFFIX = "的mapper类";

    /*------------------------------- mapperXml -------------------------------*/
    public static final String MAPPER_XML = "mapperXml";

    public static final String MAPPER_XML_PACKAGE_NAME = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.mapper.packageName");

    public static final String MAPPER_XML_FILENAME = "mapper.xml.ftl";

    /*------------------------------- service -------------------------------*/
    public static final String SERVICE = "service";

    public static final String SERVICE_PACKAGE_NAME = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.service.packageName");
    
    public static final String SERVICE_FILENAME = "service.ftl";
    
    public static final String SERVICE_INTERFACE_SUFFIX = "Service";
    
    public static final String SERVICE_INTERFACE_COMMENT_SUFFIX = "的service接口";

    /*------------------------------- serviceImpl -------------------------------*/
    public static final String SERVICE_IMPL = "serviceImpl";

    public static final String SERVICE_IMPL_PACKAGE_NAME = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.serviceImpl.packageName");

    public static final String SERVICE_IMPL_FILENAME = "serviceImpl.ftl";

    public static final String SERVICE_IMPL_CLASS_SUFFIX = "ServiceImpl";

    public static final String SERVICE_IMPL_CLASS_COMMENT_SUFFIX = "Service接口的实现类";

    /*------------------------------- controller -------------------------------*/
    public static final String CONTROLLER = "controller";

    public static final String CONTROLLER_PACKAGE_NAME = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.controller.packageName");

    public static final String CONTROLLER_FILENAME = "controller.ftl";

    public static final String CONTROLLER_CLASS_SUFFIX = "Controller";

    public static final String CONTROLLER_CLASS_COMMENT_SUFFIX = "的controller类";

    /*------------------------------- vo -------------------------------*/
    public static final String VIEW_OBJECT = "vo";

    public static final String VIEW_OBJECT_PACKAGE_NAME = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.vo.packageName");

    public static final String VIEW_OBJECT_FILENAME = "vo.ftl";

    public static final String VIEW_OBJECT_CLASS_SUFFIX = "Vo";

    public static final String VIEW_OBJECT_CLASS_COMMENT_SUFFIX = "的vo类";
}
