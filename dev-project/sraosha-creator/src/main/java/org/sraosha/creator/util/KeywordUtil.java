package org.sraosha.creator.util;

import java.util.ArrayList;
import java.util.List;

/**
 * java关键字工具类
 */
public class KeywordUtil {

    public static final List KEYWORD = new ArrayList();

    static {
        KEYWORD.add("abstract");
        KEYWORD.add("boolean");
        KEYWORD.add("break");
        KEYWORD.add("byte");
        KEYWORD.add("case");
        KEYWORD.add("catch");
        KEYWORD.add("char");
        KEYWORD.add("class");
        KEYWORD.add("continue");
        KEYWORD.add("default");
        KEYWORD.add("do");
        KEYWORD.add("double");
        KEYWORD.add("else");
        KEYWORD.add("enum");
        KEYWORD.add("extends");
        KEYWORD.add("final");
        KEYWORD.add("finally");
        KEYWORD.add("float");
        KEYWORD.add("for");
        KEYWORD.add("if");
        KEYWORD.add("implements");
        KEYWORD.add("import");
        KEYWORD.add("instanceof");
        KEYWORD.add("int");
        KEYWORD.add("interface");
        KEYWORD.add("long");
        KEYWORD.add("native");
        KEYWORD.add("new");
        KEYWORD.add("null");
        KEYWORD.add("package");
        KEYWORD.add("private");
        KEYWORD.add("protected");
        KEYWORD.add("public");
        KEYWORD.add("return");
        KEYWORD.add("short");
        KEYWORD.add("static");
        KEYWORD.add("strictfp");
        KEYWORD.add("super");
        KEYWORD.add("switch");
        KEYWORD.add("synchronized");
        KEYWORD.add("this");
        KEYWORD.add("throw");
        KEYWORD.add("throws");
        KEYWORD.add("try");
        KEYWORD.add("void");
        KEYWORD.add("volatile");
        KEYWORD.add("while");
    }

    /**
     * 如果参数是关键字，则在前面添加下划线
     * @param str
     * @return
     */
    public static String underlineKeyword(String str){
        if (KEYWORD.contains(str)){
            return "_" + str;
        } else {
            return str;
        }
    }
}
