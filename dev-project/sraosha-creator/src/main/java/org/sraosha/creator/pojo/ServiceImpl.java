package org.sraosha.creator.pojo;

import lombok.Data;

/**
 * service接口的实现类
 */
@Data
public class ServiceImpl {

    /**
     * 包名
     */
    private String packageName;

    /**
     * 类名
     */
    private String className;

    /**
     * 类的注解
     */
    private String classComment;
}
