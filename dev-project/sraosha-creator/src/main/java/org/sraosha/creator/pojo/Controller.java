package org.sraosha.creator.pojo;

import lombok.Data;

/**
 * 控制器类
 */
@Data
public class Controller {

    /**
     * 包名
     */
    private String packageName;

    /**
     * 类名
     */
    private String className;

    /**
     * 类的注解
     */
    private String classComment;
}
