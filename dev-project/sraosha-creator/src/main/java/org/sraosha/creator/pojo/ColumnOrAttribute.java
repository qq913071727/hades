package org.sraosha.creator.pojo;

import lombok.Data;

/**
 * 表的列/类的属性
 *
 */
@Data
public class ColumnOrAttribute {

    /**
     * 名称
     */
    private String columnName;

    /**
     * 类型
     */
    private String columnType;

    /**
     * 是否为主键。YES表示是，NO表示否
     */
    private String identityColumn;

    /**
     * 注释
     */
    private String comment;

    /**
     * 属性名
     */
    private String attributeName;

    /**
     * 属性类型
     */
    private String attributeType;

    public ColumnOrAttribute() {
    }

    public ColumnOrAttribute(String columnName, String columnType, String comment,
            String identityColumn, String attributeName, String attributeType) {
        this.columnName = columnName;
        this.columnType = columnType;
        this.comment = comment;
        this.identityColumn = identityColumn;
        this.attributeName = attributeName;
        this.attributeType = attributeType;
    }

}
