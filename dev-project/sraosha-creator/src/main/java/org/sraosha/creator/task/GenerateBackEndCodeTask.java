package org.sraosha.creator.task;

import org.sraosha.creator.handler.GenerateBackEndCodeHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GenerateBackEndCodeTask {

    public static void main(String[] arg){
        log.info("程序GenerateBackEndCodeTask开始运行");
        
        GenerateBackEndCodeHandler generateBackEndCodeHandler = new GenerateBackEndCodeHandler();
        generateBackEndCodeHandler.handle();
        
        log.info("程序GenerateBackEndCodeTask运行完毕");
    }
}
