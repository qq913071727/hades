package org.sraosha.creator.util;

import org.sraosha.creator.constants.TemplateConstants;
import org.sraosha.framework.util.StringUtils;

/**
 * oracle jdbc工具类
 *
 */
public class JdbcUtil {

    /**
     * 表名称转换为model类名称
     *
     * @return
     */
    public static String tableNameToModelClassName(String tableName) {
        if (null != tableName) {
            String[] wordArray = tableName.toLowerCase().split("_");
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < wordArray.length; i++) {
                stringBuffer.append(StringUtils.toUpperCase(wordArray[i]));
            }
            return stringBuffer.toString();
        } else {
            return null;
        }
    }

    /**
     * 表名称转换为model类的实例名
     *
     * @param tableName
     * @return
     */
    public static String tableNameToModelInstanceName(String tableName) {
        if (null != tableName) {
            String[] wordArray = tableName.toLowerCase().split("_");
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < wordArray.length; i++) {
                if (i == 0) {
                    stringBuffer.append(wordArray[i]);
                } else {
                    stringBuffer.append(StringUtils.toUpperCase(wordArray[i]));
                }
            }
            return KeywordUtil.underlineKeyword(stringBuffer.toString());
        } else {
            return null;
        }
    }

    /**
     * 表名称转换为mapper类名称
     *
     * @return
     */
    public static String tableNameToMapperClassName(String tableName) {
        if (null != tableName) {
            String[] wordArray = tableName.toLowerCase().split("_");
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < wordArray.length; i++) {
                stringBuffer.append(StringUtils.toUpperCase(wordArray[i]));
            }
            stringBuffer.append(TemplateConstants.MAPPER_CLASS_SUFFIX);
            return stringBuffer.toString();
        } else {
            return null;
        }
    }

    /**
     * 表名称转换为service接口名称
     *
     * @return
     */
    public static String tableNameToServiceInterfaceName(String tableName) {
        if (null != tableName) {
            String[] wordArray = tableName.toLowerCase().split("_");
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < wordArray.length; i++) {
                stringBuffer.append(StringUtils.toUpperCase(wordArray[i]));
            }
            stringBuffer.append(TemplateConstants.SERVICE_INTERFACE_SUFFIX);
            return stringBuffer.toString();
        } else {
            return null;
        }
    }

    /**
     * 表名称转换为service接口实现类的名称
     *
     * @return
     */
    public static String tableNameToServiceImplName(String tableName) {
        if (null != tableName) {
            String[] wordArray = tableName.toLowerCase().split("_");
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < wordArray.length; i++) {
                stringBuffer.append(StringUtils.toUpperCase(wordArray[i]));
            }
            stringBuffer.append(TemplateConstants.SERVICE_IMPL_CLASS_SUFFIX);
            return stringBuffer.toString();
        } else {
            return null;
        }
    }

    /**
     * 表名称转换为controller类的名称
     *
     * @return
     */
    public static String tableNameToControllerName(String tableName) {
        if (null != tableName) {
            String[] wordArray = tableName.toLowerCase().split("_");
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < wordArray.length; i++) {
                stringBuffer.append(StringUtils.toUpperCase(wordArray[i]));
            }
            stringBuffer.append(TemplateConstants.CONTROLLER_CLASS_SUFFIX);
            return stringBuffer.toString();
        } else {
            return null;
        }
    }

    /**
     * 表名称转换为dto类的名称
     *
     * @return
     */
    public static String tableNameToViewObjectName(String tableName) {
        if (null != tableName) {
            String[] wordArray = tableName.toLowerCase().split("_");
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < wordArray.length; i++) {
                stringBuffer.append(StringUtils.toUpperCase(wordArray[i]));
            }
            stringBuffer.append(TemplateConstants.VIEW_OBJECT_CLASS_SUFFIX);
            return stringBuffer.toString();
        } else {
            return null;
        }
    }

    /**
     * 表名称转换为dto实例的名称
     *
     * @return
     */
    public static String tableNameToViewObjectInstanceName(String tableName) {
        if (null != tableName) {
            String[] wordArray = tableName.toLowerCase().split("_");
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < wordArray.length; i++) {
                if (i == 0) {
                    stringBuffer.append(wordArray[i]);
                } else {
                    stringBuffer.append(StringUtils.toUpperCase(wordArray[i]));
                }
            }
            stringBuffer.append(TemplateConstants.VIEW_OBJECT_CLASS_SUFFIX);
            return stringBuffer.toString();
        } else {
            return null;
        }
    }

    /**
     * 表名称转换为service接口的变量名称
     *
     * @param tableName
     * @return
     */
    public static String tableNameToServiceVariableName(String tableName) {
        if (null != tableName) {
            String[] wordArray = tableName.toLowerCase().split("_");
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < wordArray.length; i++) {
                if (i == 0) {
                    stringBuffer.append(wordArray[i]);
                } else {
                    stringBuffer.append(StringUtils.toUpperCase(wordArray[i]));
                }
            }
            stringBuffer.append(TemplateConstants.SERVICE_INTERFACE_SUFFIX);
            return stringBuffer.toString();
        } else {
            return null;
        }
    }

    /**
     * 将表的列名转换为类的属性名
     *
     * @param columnName
     * @return
     */
    public static String tableColumnNameToClassAttributeName(String columnName) {
        if (null != columnName) {
            String[] wordArray = columnName.toLowerCase().split("_");
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < wordArray.length; i++) {
                if (i == 0) {
                    stringBuffer.append(wordArray[i]);
                } else {
                    stringBuffer.append(StringUtils.toUpperCase(wordArray[i]));
                }
            }
            return stringBuffer.toString();
        } else {
            return null;
        }
    }

    /**
     * 将表的列类型转换为类的属性类型，包括oracle、mysql
     *
     * @param tableColumn
     * @return
     */
    public static String tableColumnTypeToClassAttributeType(String tableColumn) {
        if ("CHAR".equalsIgnoreCase(tableColumn)
                || "VARCHAR2".equalsIgnoreCase(tableColumn)
                || "LONG".equalsIgnoreCase(tableColumn)
                || "VARCHAR".equalsIgnoreCase(tableColumn)
                || "TEXT".equalsIgnoreCase(tableColumn)) {
            return "String";
        }
        if ("NUMBER".equalsIgnoreCase(tableColumn)
                || "DECIMAL".equalsIgnoreCase(tableColumn)) {
            return "BigDecimal";
        }
        if ("RAW".equalsIgnoreCase(tableColumn)
                || "LONGRAW".equalsIgnoreCase(tableColumn)) {
            return "byte[]";
        }
        if ("DATE".equalsIgnoreCase(tableColumn)) {
            return "Date";
        }
        if ("TIME".equalsIgnoreCase(tableColumn)) {
            return "Time";
        }
        if ("TIMESTAMP".equalsIgnoreCase(tableColumn)
                || "DATETIME".equalsIgnoreCase(tableColumn)) {
            return "Date";
        }
        if ("BLOB".equalsIgnoreCase(tableColumn)) {
            return "Blob";
        }
        if ("CLOB".equalsIgnoreCase(tableColumn)) {
            return "Clob";
        }
        if ("INT".equalsIgnoreCase(tableColumn)) {
            return "Integer";
        }
        if ("BIGINT".equalsIgnoreCase(tableColumn)) {
            return "Long";
        }
        if ("FLOAT".equalsIgnoreCase(tableColumn)) {
            return "Float";
        }
        if ("DOUBLE".equalsIgnoreCase(tableColumn)) {
            return "Double";
        }
        return null;
    }
}
