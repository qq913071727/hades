package org.sraosha.creator.constants;

import org.sraosha.framework.util.PropertiesUtil;

import java.io.File;

/**
 * 文件信息
 */
public class FileConstants {

    /**
     * 获取程序当前路径，即根目录
     */
    protected static final String DIR = System.getProperty("user.dir");

    /**
     * 主配置文件
     */
//    public static final String PROPERTIES_PATH = DIR + "/sraosha-creator/src/main/resources/config.properties";
    public static final String PROPERTIES_PATH = "E:/github-repository/hades/dev-project/sraosha-creator/src/main/resources/config.properties";

    /**
     * 模板文件路径
     */
    public static final String TEMPLATE_FILE_PATH = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.templateFilePath");

    /**
     * 输出文件路径
     */
    public static final String OUTPUT_PATH = PropertiesUtil.getValue(FileConstants.PROPERTIES_PATH, "generate.outputPath");

    /**
     * 路径分隔符
     */
    public static final String SEPARATOR = "/";

    /**
     * java文件后缀
     */
    public static final String JAVA_FILE_SUFFIX = ".java";

    /**
     * xml文件后缀
     */
    public static final String XML_FILE_SUFFIX = ".xml";
}
