package org.sraosha.creator.pojo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * 模型类
 *
 */
@Data
public class Model {

    /*---------------------------- 表相关 ----------------------------*/
    /**
     * 表的名称
     */
    private String tableName;

    /**
     * 表注释
     */
    private String tableComment;

    /**
     * 列的列表/属性的列表
     */
    private List<ColumnOrAttribute> columnOrAttributeList = new ArrayList();

    /*-------------------------------- 类相关 ----------------------------*/
    /**
     * 包名
     */
    private String packageName;

    /**
     * 类名
     */
    private String className;
    
    /**
     * 实例名
     */
    private String instanceName;
}
