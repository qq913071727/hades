package org.sraosha.creator.pojo;

import lombok.Data;

/**
 * service接口
 *
 */
@Data
public class Service {

    /**
     * 包名
     */
    private String packageName;
    
    /**
     * 类名
     */
    private String className;
    
    /**
     * 接口的注解
     */
    private String interfaceComment;
    
    /**
     * 接口的变量的名字
     */
    private String variableName;
    
    /**
     * 类注释
     */
    private String classComment;

}
