'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

/**
 * 开发环境配置
 */
module.exports = merge(prodEnv, {
  // 环境
  NODE_ENV: '"development"',
  // 后端接口url
  BASE_API: "'http://localhost:18568'"
})
