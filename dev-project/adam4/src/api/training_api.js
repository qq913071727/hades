import request from "@/utils/request.js";
import qs from "qs";

const baseUrl = '/adam4/api/v1/trainingInfo'

/**
 * 添加
 */
function _add(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/add",
			method: "POST",
			data: params
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function add(params) {
	let res = await _add(params);
	return res;
};

/**
 * 根据name查询
 */
function _findByName(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/findByName/" + params,
			method: "GET"
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function findByName(params) {
	let res = await _findByName(params);
	return res;
};

/**
 * 根据name，更新status
 */
function _updateStatusByName(trainingName, status) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/updateStatusByName/" + trainingName + "/" + status,
			method: "GET"
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function updateStatusByName(trainingName, status) {
	let res = await _updateStatusByName(trainingName, status);
	return res;
};

/**
 * 分页显示
 */
function _page(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/page",
			method: "POST",
			data: params
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function page(params) {
	let res = await _page(params);
	return res;
};

/**
 * 结束训练
 */
function _stopTraining(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/stopTraining/" + params,
			method: "GET"
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function stopTraining(params) {
	let res = await _stopTraining(params);
	return res;
};

/**
 * 删除训练
 */
function _deleteTraining(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/deleteTraining/" + params,
			method: "GET",
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function deleteTraining(params) {
	let res = await _deleteTraining(params);
	return res;
};

/**
 * 根据训练名称查询日线级别交易记录
 * @param {*} name 
 * @returns 
 */
function _findDateDataByTrainingName(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/findDateDataByTrainingName/" + name,
			method: "get",
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function findDateDataByTrainingName(params) {
	let res = await _findDateDataByTrainingName(params);
	return res;
};

/**
 * 根据训练名称查询周线级别交易记录
 * @param {*} name 
 * @returns 
 */
function _findWeekDataByTrainingName(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/findWeekDataByTrainingName/" + name,
			method: "get",
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function findWeekDataByTrainingName(params) {
	let res = await _findWeekDataByTrainingName(params);
	return res;
};

/**
 * 下一日
 */
function _nextDate(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/nextDate/" + params,
			method: "GET",
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function nextDate(params) {
	let res = await _nextDate(params);
	return res;
};

//其实，也不一定就是params，也可以是 query 还有 data 的呀！
//params是添加到url的请求字符串中的，用于get请求。会将参数加到 url后面。所以，传递的都是字符串。无法传递参数中含有json格式的数据
//而data是添加到请求体（body）中的， 用于post请求。添加到请求体（body）中，json 格式也是可以的。