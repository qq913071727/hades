import request from "@/utils/request.js";
import qs from "qs";

const baseUrl = '/adam4/api/v1/trainingAnalysis'

/**
 * 返回收益率折线图数据
 */
function _findProfitAndLossRatePictureData(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/findProfitAndLossRatePictureData/" + params,
			method: "get",
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function findProfitAndLossRatePictureData(params) {
	let res = await _findProfitAndLossRatePictureData(params);
	return res;
};
// function _findProfitAndLossRatePictureData(params) {
// 	// return request({
// 	//     url: baseUrl + "/findProfitAndLossRatePictureData/" + params,
// 	//     method: "get",
// 	// });
// 	return new Promise((resolve, reject) => {
// 		request({
// 			url: baseUrl + "/findProfitAndLossRatePictureData/" + params,
// 			method: "get",
// 		}).then((res) => {
// 			resolve(res)
// 		}).catch((err) => {
// 			reject(err)
// 		})
// 	});
// }
// async function findProfitAndLossRatePictureData(params) {
// 	try {
// 		let res = await _findProfitAndLossRatePictureData()
// 		return res;
// 	} catch (err) {
// 		console.err('请求出错', err);
// 	}
// }

// export { findProfitAndLossRatePictureData };