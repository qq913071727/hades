import request from "@/utils/request.js";
import qs from "qs";

const baseUrl = '/adam4/api/v1/trainingTransactionData'

/**
 * 根据trg_info_id，判断是否是持仓状态
 */
function _isHoldingShareByTrainingInfoId(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/isHoldingShareByTrainingInfoId/" + params,
			method: "GET",
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function isHoldingShareByTrainingInfoId(params) {
	let res = await _isHoldingShareByTrainingInfoId(params);
	return res;
};

/**
 * 开多仓
 */
function _openLongPosition(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/openLongPosition/" + params,
			method: "GET",
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function openLongPosition(params) {
	let res = await _openLongPosition(params);
	return res;
};

/**
 * 开空仓
 */
function _openShortPosition(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/openShortPosition/" + params,
			method: "GET",
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function openShortPosition(params) {
	let res = await _openShortPosition(params);
	return res;
};

/**
 * 平多仓
 */
function _closeLongPosition(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/closeLongPosition/" + params,
			method: "GET",
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function closeLongPosition(params) {
	let res = await _closeLongPosition(params);
	return res;
};

/**
 * 平空仓
 */
function _closeShortPosition(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/closeShortPosition/" + params,
			method: "GET",
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function closeShortPosition(params) {
	let res = await _closeShortPosition(params);
	return res;
};

/**
 * 买入
 */
function _buy(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/buy/" + params,
			method: "GET",
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function buy(params) {
	let res = await _buy(params);
	return res;
};

/**
 * 卖出
 */
function _sell(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/sell/" + params,
			method: "GET",
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function sell(params) {
	let res = await _sell(params);
	return res;
};

/**
 * 计算收益率
 */
function _calculateProfitAndLossRate(params) {
	return new Promise((resolve, reject) => {
		request({
			url: baseUrl + "/calculateProfitAndLossRate/" + params,
			method: "GET",
		}).then((res) => {
			resolve(res)
		}).catch((err) => {
			reject(err)
		})
	})
};
export async function calculateProfitAndLossRate(params) {
	let res = await _calculateProfitAndLossRate(params);
	return res;
};