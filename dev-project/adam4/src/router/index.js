import Vue from 'vue'
import Router from 'vue-router'

import Training from '@/views/training'
// import Transaction from '@/views/transaction/index.vue'
import Transaction from '@/views/transaction/echarts-kline.vue'
import Analysis from '@/views/analysis'
import Reply from '@/views/reply'

Vue.use(Router);

// 此Router是自己自定义引入暴露出来的，即是自定义的，以下的Router同样是这样
// 解决两次访问相同路由地址报错
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}

export default new Router({
    mode: 'history',
    routes: [
        {
            path: "/",
            name: "Home",
            component: Training
        },
        {
            path: "/training",
            name: "Training",
            component: Training
        },
        {
            path: "/transaction",
            name: "Transaction",
            component: Transaction
        },
        {
            path: "/analysis",
            name: "Analysis",
            component: Analysis
        },
        {
            path: "/reply",
            name: "Reply",
            component: Reply
        }
    ]
});