import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	//数据，相当于data
	state: {
		/**
		 * 被选中的菜单项
		 */
		activeIndexInNavigationBar: '1',
	},
	getters: {
		/**
		 * 返回被选中的菜单项
		 * @param {*} state 
		 * @returns 
		 */
		getActiveIndexInNavigationBar(state) {
      return state.activeIndexInNavigationBar
    },
	},
	//里面定义方法，操作state方发
	mutations: {
		/**
		 * 更新activeIndexInNavigationBar
		 * @param {*} state 
		 * @param {*} activeIndex 
		 */
		changeActiveIndexInNavigationBar(state, activeIndex) {
			state.activeIndexInNavigationBar = activeIndex;
		},
	},
	// 操作异步操作mutation
	// actions: {

	// },
	// modules: {

	// },
})