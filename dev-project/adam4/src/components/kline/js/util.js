export class Util {

    static fromFloat(v, fractionDigits) {
        let text = v.toFixed(fractionDigits);
        for (let i = text.length - 1; i >= 0; i--) {
            if (text[i] === '.')
                return text.substring(0, i);
            if (text[i] !== '0')
                return text.substring(0, i + 1);
        }
    };

    static formatTime(v) {
        return (v < 10) ? "0" + v.toString() : v.toString();
    }

    static isInstance(obj, clazz) {
        if (obj === null || obj === undefined) {
            return false;
        }
        return obj instanceof clazz;
    }

    static getPixelRatio(context) {
        let backingStore = context.backingStorePixelRatio ||
                context.webkitBackingStorePixelRatio ||
                context.mozBackingStorePixelRatio ||
                context.msBackingStorePixelRatio ||
                context.oBackingStorePixelRatio ||
                context.backingStorePixelRatio || 1;
        return (window.devicePixelRatio || 1) / backingStore;
        // return 1;
    };

    static fixPos(context, a) {
        return a * this.getPixelRatio(context);
    }

    static fixFontSize(context) {
        let match = context.font.match(/(?<size>\d+)(?<other>.*)/);
        context.font = Number(match.groups.size) * this.getPixelRatio(context) + match.groups.other;
    }
}
