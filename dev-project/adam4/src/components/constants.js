/**
 * 市场类型
 */
export const MarketType = {
  /**
   * 股票市场
   */
  STOCK_MARKET: 1,
  /**
   * 期货市场
   */
  COMMODITY_FUTURE_MARKET: 2,
};

/**
 * K线级别
 */
export const KLineLevel = {
  /**
   * 日线级别
   */
  DATE_LEVEL: 1,
  /**
   * 周线级别
   */
  WEEK_LEVEL: 2,
};

/**
 * Http状态
 */
export const HttpStatus = {
  /**
   * 数据不存在
   */
  NOT_EXIST: 4002
};

/**
 * 持仓情况
 */
export const SharePosition = {
  /**
   * 没有持仓
   */
  SHARE_NONE: 0,
  /**
   * 股票持有多仓
   */
  STOCK_SHARE: 1,
  /**
   * 期货持有多仓
   */
  COMMODITY_FUTURE_SHARE_LONG: 2,
  /**
   * 期货持有空仓
   */
  COMMODITY_FUTURE_SHARE_SHORT: 3,
};

/**
 * 训练状态
 */
export const TrainingStatus = {
  /**
   * 未开始
   */
  NO_START: 1,
  /**
   * 已经开始
   */
  STARTING: 2,
  /**
   * 已经结束
   */
  END: 3,
};