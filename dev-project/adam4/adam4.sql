﻿------------------------------------------------- trng_info ------------------------------------------
select * from trng_info for update;

select * from stock_transaction_data_all t where t.code_='600655'
and t.date_ between to_date('1993-09-02','yyyy-mm-dd') and to_date('1994-09-02','yyyy-mm-dd') order by t.date_ desc;

select min(t.date_)+365, max(t.date_) from stock_transaction_data_all t where t.code_='000001';

SELECT id, name,
(CASE WHEN market = 1 THEN '��Ʊ' WHEN market = 2 THEN '�ڻ�' ELSE 'δ֪' END),
code, to_char(begin_date, 'yyyy-mm-dd') AS market, to_char(end_date, 'yyyy-mm-dd'),
(CASE WHEN status = 1 THEN 'δ��ʼ' WHEN status = 2 THEN '�Ѿ���ʼ' WHEN status = 3 THEN '�Ѿ�����' ELSE 'δ֪' END),
to_char(create_time, 'yyyy-mm-dd'), to_char(update_time, 'yyyy-mm-dd')
from trng_info;

-- truncate table trng_info;

------------------------------------------------- trng_transaction_data ------------------------------------------

select * from trng_transaction_data t;

select * from trng_transaction_data t where t.trng_info_id=264 order by t.BUY_DATE asc;

select * from stock_transaction_data_all t
where t.code_='300364' and t.date_ between to_date('2016-09-05', 'yyyy-mm-dd') and to_date('2017-09-05', 'yyyy-mm-dd') order by t.date_ desc;

select * from commodity_future_date_data t
where t.code='RS' and t.transaction_date between to_date('2014-12-27', 'yyyy-mm-dd') and to_date('2014-12-28', 'yyyy-mm-dd')
order by t.transaction_date asc;

select * from (
select * from trng_transaction_data t where t.trng_info_id=215 order by t.create_time desc) t1 where rownum<=1;

select * from COMMODITY_FUTURE_WEEK_DATA t;

-- truncate table trng_transaction_data;

------------------------------------------ STRAIGHT_LINE_INCR_DECR ------------------------------------------

select * from STRAIGHT_LINE_INCR_DECR t order by t.CREATE_TIME desc;

/*delete from STRAIGHT_LINE_INCR_DECR t where t.NAME is null;
commit;*/

------------------------------------------ monitor_current_price ------------------------------------------

select * from monitor_current_price;

/*insert into MONITOR_CURRENT_PRICE(ID, TRANSACTION_TYPE, code, NAME, COMPARISON, MONITOR_PRICE, create_time, available)
VALUES(1, 3, 'M2505', '豆粕', 2, 2855, SYSDATE, 1);
commit;*/

/*update MONITOR_CURRENT_PRICE t set t.COMPARISON=1, t.MONITOR_PRICE=2866 where t.ID=1;
commit;*/

-- truncate table MONITOR_CURRENT_PRICE;
