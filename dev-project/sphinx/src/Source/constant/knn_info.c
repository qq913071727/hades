/**
 * 文件有多少列（不包括最后一列的标签）
 */
//int KNN_FILE_COLUMN_SIZE = 1024 * 100;
int KNN_FILE_COLUMN_SIZE = 16;

/**
 * 文件有多少行
 */
int KNN_FILE_ROW_SIZE = 13611;

/**
 * 单元格有多大
 */
int KNN_FILE_CELL_SIZE = 30;

/**
 * 数据文件的路径
 */
char *KNN_DATA_FILE_AT_HOME = "F:\\github-repository\\dataset\\DryBeanDataset\\DryBeanDataset\\Dry_Bean_Dataset.csv";
char *KNN_DATA_FILE_AT_COMPANY = "C:\\github-repository\\dataset\\DryBeanDataset\\DryBeanDataset\\Dry_Bean_Dataset.csv";

/**
 * 训练数据和测试数据比例
 */
double TRAINING_DATASET_PERCENTAGE = 0.8;
double TESTING_DATASET_PERCENTAGE = 0.2;