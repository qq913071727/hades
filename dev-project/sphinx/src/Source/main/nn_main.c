#include <stdio.h>
#include "../../Include/manager/log_manager.h"
#include "../../Include/task/nn_task.h"
#include "../../Include/task/knn_task.h"
#include "../../Include/test/test.h"

void Test();

int main() {
    Info("main函数开始执行");

//    Test();

    // 执行反向传播算法
	DoBP();

    Info("main函数执行完毕");
    return 0;
}

void Test() {
    TestStructPointMember();
}