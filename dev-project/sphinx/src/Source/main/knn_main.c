#include "../../Include/manager/log_manager.h"
#include "../../Include/task/knn_task.h"

int main(void) {
    Info("main函数开始执行");

    // 执行k近邻算法
    DoKnn();

    Info("main函数执行完毕");
    return 0;
}