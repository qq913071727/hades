#include <stdio.h>
#include <stdlib.h>
#include "../../Include/domain/stock_transaction_data_list_result.h"
#include "../../Include/constant/knn_info.h"

/**
 * 特征值数量
 */
int CHARACTERISTIC_VALUE_NUMBER;

/**
 * 将StockTransactionData结构体类型转换为二维数组
 * @param trainDataList
 * @param count
 * @return
 */
double **StockTransactionDataListToArray(struct StockTransactionData *trainDataList, int count) {
    double _array[count][CHARACTERISTIC_VALUE_NUMBER];
    double (*array)[CHARACTERISTIC_VALUE_NUMBER] = _array;
    for (int i = 0; i < count; ++i) {
        if (trainDataList->date_ == NULL) {
            continue;
        }
        *(*(array + i) + 0) = trainDataList->openPrice;
//        *(*(array + i) + 1) = trainDataList->closePrice;
        *(*(array + i) + 1) = trainDataList->highestPrice;
        *(*(array + i) + 2) = trainDataList->lowestPrice;
        *(*(array + i) + 3) = trainDataList->lastClosePrice;
        *(*(array + i) + 4) = trainDataList->changeAmount;
        *(*(array + i) + 5) = trainDataList->changeRange;
        *(*(array + i) + 6) = trainDataList->changeRangeExRight;
        *(*(array + i) + 7) = trainDataList->upDown;
        *(*(array + i) + 8) = trainDataList->turnoverRate;
//        *(*(array + i) + 10) = trainDataList->volume;
//        *(*(array + i) + 11) = trainDataList->turnover;
//        *(*(array + i) + 12) = trainDataList->totalMarketValue;
//        *(*(array + i) + 13) = trainDataList->circulationMarketValue;
        *(*(array + i) + 9) = trainDataList->ma5;
        *(*(array + i) + 10) = trainDataList->ma10;
        *(*(array + i) + 11) = trainDataList->ma20;
        *(*(array + i) + 12) = trainDataList->ma60;
        *(*(array + i) + 13) = trainDataList->ma120;
        *(*(array + i) + 14) = trainDataList->ma250;
        *(*(array + i) + 15) = trainDataList->ema12;
        *(*(array + i) + 16) = trainDataList->ema26;
        *(*(array + i) + 17) = trainDataList->dif;
        *(*(array + i) + 18) = trainDataList->dea;
        *(*(array + i) + 19) = trainDataList->rsv;
        *(*(array + i) + 20) = trainDataList->k;
        *(*(array + i) + 21) = trainDataList->d;
        *(*(array + i) + 22) = trainDataList->haOpenPrice;
        *(*(array + i) + 23) = trainDataList->haClosePrice;
        *(*(array + i) + 24) = trainDataList->haHighestPrice;
        *(*(array + i) + 25) = trainDataList->haLowestPrice;
        trainDataList++;
    }
    return array;
}

/**
 * 行列互换
 * @param dataArray
 * @return
 */
double **RowColumnInterchange(double (*dataArray)[KNN_FILE_COLUMN_SIZE]) {
    double (*reverseArray)[KNN_FILE_COLUMN_SIZE] = (double *) malloc(
            KNN_FILE_ROW_SIZE * KNN_FILE_COLUMN_SIZE * sizeof(double));
    for (int i = 0; i < KNN_FILE_COLUMN_SIZE; ++i) {
        for (int j = 0; j < KNN_FILE_ROW_SIZE; ++j) {
            reverseArray[j][i] = dataArray[i][j];
        }
    }
    return reverseArray;
}
