#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../Include/constant/path.h"

/**
 * 连接字符串a和b，并生成新的字符串
 */
char *JoinChar(char *a, char *b) {
    char *c = (char *) malloc(strlen(a) + strlen(b) + 1);
    if (c == NULL) {
        exit(1);
    }
    strcpy(c, a);
    strcat(c, b);
    return c;
}

/**
 * 生成绝对路径
 */
char *MakeAbsolutePath(char *b) {
    char *result = (char *) malloc(strlen(PROJECT_ROOT_PATH) + strlen(b));
    result[0] = '\0';
    strcat(result, PROJECT_ROOT_PATH);
    strcat(result, b);
    return result;
}

/**
 * 给定一个C-String的变量(非字面值常量)，去除首尾两端Blank的字符
 * 输入参数: char *src   --->  带有'\0'的C-String
 * 输出参数: char *src       --->  格式化后(去除首尾两端Blank的字符)
 */
void Trim(char *src) {
    char *begin = src;
    char *end = src;

    while (*end++) { ;  // Statement is null
    }

    if (begin == end)
        return;

    while (*begin == ' ' || *begin == '\t')
        ++begin;
    while ((*end) == '\0' || *end == ' ' || *end == '\t')
        --end;

    if (begin > end) {
        *src = '\0';
        return;
    }
    //printf("begin=%1.1s; end=%1.1s\n", begin, end);
    while (begin != end) {
        *src++ = *begin++;
    }

    *src++ = *end;
    *src = '\0';

    return;
}

/**
 * 除空白字符。空白字符指空格、水平制表、垂直制表、换页、回车和换行符。
 * 此方法返回的是已经修改的字串，而且字串的长度小于等于原字串的长度。
 */
char *Rtrim(char *str) {
    char *p = str;
    while (*p == ' ' || *p == '/t' || *p == '/r' || *p == '/n')
        p++;
    str = p;
    p = str + strlen(str) - 1;
    while (*p == ' ' || *p == '/t' || *p == '/r' || *p == '/n')
        --p;
    *(p + 1) = 0;
    return str;
}

/**
 * 分隔字符串
 * @param src 要进行分割的字符串地址
 * @param separator 分隔符，设置为常量字符串,这里的分隔符可以为字符串
 * @param dest 分割结束后数据存储的地址，二维数组
 * @param num 返回的是字符串中存在的分隔符的数量
 */
void split(char *src, const char *separator, char **dest, int *num) {
    char *pNext;
    //记录分隔符数量
    int count = 0;
    //原字符串为空
    if (src == NULL || strlen(src) == 0)
        return;
    //未输入分隔符
    if (separator == NULL || strlen(separator) == 0)
        return;
    /*
        c语言string库中函数，
        声明：
        char *strtok(char *str, const char *delim)
        参数：
        str -- 要被分解成一组小字符串的字符串。
        delim -- 包含分隔符的 C 字符串。
        返回值：
        该函数返回被分解的第一个子字符串，如果没有可检索的字符串，则返回一个空指针。

    */
    char *strtok(char *str, const char *delim);
    //获得第一个由分隔符分割的字符串
    pNext = strtok(src, separator);
    while (pNext != NULL) {
        //存入到目的字符串数组中
        *dest++ = pNext;
        ++count;
        /*
			strtok()用来将字符串分割成一个个片段。参数s指向欲分割的字符串，参数delim则为分割字符串中包含的所有字符。
			当strtok()在参数s的字符串中发现参数delim中包涵的分割字符时,则会将该字符改为\0 字符。
			在第一次调用时，strtok()必需给予参数s字符串，往后的调用则将参数s设置成NULL。
			每次调用成功则返回指向被分割出片段的指针。

		*/
        pNext = strtok(NULL, separator);
    }
    *num = count;
}
