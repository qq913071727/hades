#include <stdio.h>
#include <windows.h>

/**
 * 打包程序。先编译为dll，再编译为exe
 */
//int main()
//{
//  // CompileDll();
//  CompileExe();
//  Run();
//
//  return 0;
//}

/**
 * 编译为dll
 */
void CompileDll()
{
    InfoChar("开始编译为dll文件");

  // 命令和参数
  char *command = "gcc -w -shared -o ";
  // char *command = "gcc -o ";
  // 输出的dll文件
  char *output = "E:/github-repository/hades/dev-project/sphinx/Lib/sphinx.dll ";

  // 输入的文件
  const int INPUT_LENGTH = 5;
  char *input[] = {
      // "E:/github-repository/hades/dev-project/sphinx/Config/*.conf ",
      "E:/github-repository/hades/dev-project/sphinx/Include/constant/*.h ",
      "E:/github-repository/hades/dev-project/sphinx/Include/util/*.h ",
      // "E:/github-repository/hades/dev-project/sphinx/Lib/*.dll ",
      // "E:/github-repository/hades/dev-project/sphinx/Lib/11.2.0.1.0_Production/*.dll ",
      "E:/github-repository/hades/dev-project/sphinx/Source/*.c ",
      "E:/github-repository/hades/dev-project/sphinx/Source/constant/*.c ",
      // "E:/github-repository/hades/dev-project/sphinx/Source/proc/*.c ",
      "E:/github-repository/hades/dev-project/sphinx/Source/util/*.c "};

  command = JoinChar(command, output);

  int i = 0;
  for (i = 0; i < INPUT_LENGTH; i++)
  {
    command = JoinChar(command, input[i]);
  }
    InfoChar(command);

  // 编译为dll
  system(command);
}

/**
 * 编译为exe
 */
void CompileExe()
{
    InfoChar("开始编译为exe文件");

  // 命令和参数
  char *command = "gcc -w -o ";
  // 输出的exe文件
  char *output = "E:/github-repository/hades/dev-project/sphinx/Bin/sphinx.exe ";

  // 输入的文件
  const int INPUT_LENGTH = 7;
  char *input[] = {
      "E:/github-repository/hades/dev-project/sphinx/Include/constant/*.h ",
      "E:/github-repository/hades/dev-project/sphinx/Include/util/*.h ",
      "E:/github-repository/hades/dev-project/sphinx/Lib/11.2.0.1.0_Production/*.dll ",
      "E:/github-repository/hades/dev-project/sphinx/Source/*.c ",
      "E:/github-repository/hades/dev-project/sphinx/Source/constant/*.c ",
      "E:/github-repository/hades/dev-project/sphinx/Source/proc/*.c ",
      "E:/github-repository/hades/dev-project/sphinx/Source/util/*.c "};

  command = JoinChar(command, output);

  int i = 0;
  for (i = 0; i < INPUT_LENGTH; i++)
  {
    command = JoinChar(command, input[i]);
  }
    InfoChar(command);

  // 编译为exe
  system(command);
}

void Run()
{
    InfoChar("开始执行sphinx.exe文件");

  system("sphinx.exe");
  system("cd ..");
}
