//
// Created by Administrator on 2022/2/19.
//

#include <stdio.h>
#include "../../Include/manager/matrix_manager.h"
#include "../../Include/domain/database_info.h"
#include "../../Include/model/stock_transaction_data.h"

/**
 * 测试指向结构体的指针，使用字符数组和指向数组的指针
 */
void TestStructPointMember() {
    struct Student {
        char *name;
    };
    struct Teacher{
        char address[10];
    };
    struct DatabaseInfo *databaseInfo = (struct DatabaseInfo *) malloc(sizeof(struct DatabaseInfo));
    databaseInfo->password = "zhangsan";
    struct StockTransactionData *trainDataList = (struct StockTransactionData *) malloc(sizeof(struct StockTransactionData));
    printf("123");
}

/**
 * 测试矩阵相乘
 */
void Multiply_Test() {
    A_ROW = 3;
    A_COLUMN = 2;
    B_ROW = 2;
    B_COLUMN = 3;
    double a[3][2] = {{2.3, 3.5},
                      {4.8, 5.8},
                      {3.1, 5.2}};
    double b[2][3] = {{1.9, 2.3, 3.5},
                      {4.4, 5.6, 6.8}};
    double c[3][3];
    Multiply(a, b, c);
    printf("矩阵a与矩阵b相乘的结果为:\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%f\t", c[i][j]);
        }
        printf("\n");
    }
}