#include <stdio.h>
#include <time.h>

/**
 * 打印INFO级别的日志
 * @param message
 */
void Info(char message[]) {
	time_t t;
	struct tm *lt;
	time(&t);
	lt = localtime(&t);
	printf("[%d-%d-%d %d:%d:%d] [INFO] %s\n", lt->tm_year + 1900,
			lt->tm_mon + 1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec,
			message);
}

/**
 * 打印INFO级别的日志
 * @param key
 * @param value
 */
void InfoChar(char *key, char *value) {
	time_t t;
	struct tm *lt;
	time(&t);
	lt = localtime(&t);
	printf("[%d-%d-%d %d:%d:%d] [INFO] %s: %s\n", lt->tm_year + 1900,
			lt->tm_mon + 1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec,
           key, value);
}

/**
 * 打印INFO级别的日志
 * @param key
 * @param value
 */
void InfoDouble(char *key, double value) {
    time_t t;
    struct tm *lt;
    time(&t);
    lt = localtime(&t);
    printf("[%d-%d-%d %d:%d:%d] [INFO] %s: %f\n", lt->tm_year + 1900,
           lt->tm_mon + 1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec,
           key, value);
}

/**
 * 打印INFO级别的日志
 * @param key
 * @param value
 */
void InfoInt(char *key, int value) {
    time_t t;
    struct tm *lt;
    time(&t);
    lt = localtime(&t);
    printf("[%d-%d-%d %d:%d:%d] [INFO] %s: %d\n", lt->tm_year + 1900,
           lt->tm_mon + 1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec,
           key, value);
}
