#include <stdio.h>
#include <string.h>
#include "../../Include/model/stock_transaction_data.h"
#include "../../Include/proc/find_stock_transaction_data.h"
#include "../../Include/domain/database_info.h"
#include "../../Include/domain/stock_transaction_data_list_result.h"
#include "../../Include/util/char_util.h"
#include "../../Include/manager/log_manager.h"
#include "../../Include/manager/config_manager.h"

/**
 * ��ȡx����ĳֻ��Ʊ�Ļ��������������ֵ
 * @param beginDate
 * @param endDate
 * @return
 */
struct StockTransactionDataListResult FindStockTransactionData(char *beginDate, char *endDate) {
    // ��ȡ���ݿ�������Ϣ
    struct DatabaseInfo *databaseInfo = GetDatabaseInfo();

    // ����StockTransactionData���͵Ľṹ������
    // �����С
    STOCK_TRANSACTION_DATA_LIST_SIZE = 8000;
    struct StockTransactionData *trainDataList = (struct StockTransactionData *) malloc(
            STOCK_TRANSACTION_DATA_LIST_SIZE * sizeof(struct StockTransactionData));
    double (*trainLabelList)[1] = (double *) malloc(
            STOCK_TRANSACTION_DATA_LIST_SIZE * sizeof(double));
    // ���ص����ݵĴ�С
    int _count = 0;
    int *count = &_count;
    GetStockTransactionData(databaseInfo, "000004", count, trainDataList, trainLabelList, beginDate, endDate);
//    for (int i = 0; i < *count; ++i) {
//        printf("%f\n", trainDataList->openPrice);
//        trainDataList++;
//    }

    struct StockTransactionDataListResult structStockTransactionDataListResult = {trainDataList, *count, trainLabelList};
    return structStockTransactionDataListResult;
}
