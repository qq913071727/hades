#include <stdio.h>
#include <time.h>
#include <stdlib.h>

/****************************************************************************************************/
/**
 * a和b两个二维矩阵的行数和列数
 */
int A_ROW;
int A_COLUMN;
int B_ROW;
int B_COLUMN;

/**
 * 矩阵相乘
 */
void Multiply(double (*a)[A_COLUMN], double (*b)[B_COLUMN], double c[A_ROW][B_COLUMN]) {
	for (int i = 0; i < A_ROW; i++) {
		for (int j = 0; j < B_COLUMN; j++) {
			c[i][j] = 0;
			for (int k = 0; k < A_COLUMN; k++) {
                c[i][j] += *(*(a + i) + k) * *(*(b + k) + j);
            }
		}
	}

//	printf("矩阵a与矩阵b相乘的结果为:\n");
//	for (int i = 0; i < 3; i++) {
//		for (int j = 0; j < 3; j++) {
//			printf("%f\t", c[i][j]);
//		}
//		printf("\n");
//	}
}

/****************************************************************************************************/

/**
 * 矩阵相加
 * @param a 
 * @param b 
 * @param c 
 */
void Add(double (*a)[A_COLUMN], double (*b)[B_COLUMN], double c[A_ROW][B_COLUMN]){
    for (int i = 0; i < A_ROW; ++i) {
        for (int j = 0; j < B_COLUMN; ++j) {
            c[i][j] = *(*(a + i) + j) + *(*(b + i) + j);
        }
    }
}

/****************************************************************************************************/
/**
 * 二维矩阵的列数
 */
int MATRIX_COLUMN;

/**
 * 创建矩阵。使用之前要先给全局变量MATRIX_COLUMN赋值
 * arrayRandomLimit：表示需要生成的随机数为0~arrayRandomLimit之间
 * row：矩阵的行
 * column：矩阵的列
 * (*weight)[]：返回值。二维数组（矩阵）
 */
void CreateMatrix(int arrayRandomLimit, int row, int column, double (*weight)[MATRIX_COLUMN]) {
	srand((unsigned) time(NULL));
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < column; j++) {
			*(*(weight+i)+j) = (double) rand() / RAND_MAX * arrayRandomLimit;
//			printf("%4f\n", *(*(weight+i)+j));
		}
	}
}
/****************************************************************************************************/
