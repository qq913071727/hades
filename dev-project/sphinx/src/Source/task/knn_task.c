#include <stdlib.h>
#include "../../Include/handler/knn_handler.h"
#include "../../Include/manager/log_manager.h"
#include "../../Include/manager/file_manager.h"
#include "../../Include/util/char_util.h"
#include "../../Include/constant/knn_info.h"
#include "../../Include/domain/knn_dataset.h"

/**
 * k近邻算法
 */
void DoKnn() {
    Info("k近邻算法开始执行");

    // 返回数据文件的路径
    char *filePath = GetDataFilePathByEnvUSERDOMAIN();

    // 从文件中读取全部数据
    struct KnnDataset knnDataset = ReadFile(filePath, "r");

    // 归一化
    double (*normalizedArray)[KNN_FILE_COLUMN_SIZE] = Normalize(knnDataset.allDataset);

    struct KnnDataset knnDataset1 = DifferentiateTrainingDatasetAndTestingDataset(knnDataset.allDatasetLabel, normalizedArray);

    Info("k近邻算法执行完毕");
}