
/* Result Sets Interface */
#ifndef SQL_CRSR
#  define SQL_CRSR
  struct sql_cursor
  {
    unsigned int curocn;
    void *ptr1;
    void *ptr2;
    unsigned int magic;
  };
  typedef struct sql_cursor sql_cursor;
  typedef struct sql_cursor SQL_CURSOR;
#endif /* SQL_CRSR */

/* Thread Safety */
typedef void * sql_context;
typedef void * SQL_CONTEXT;

/* Object support */
struct sqltvn
{
  unsigned char *tvnvsn; 
  unsigned short tvnvsnl; 
  unsigned char *tvnnm;
  unsigned short tvnnml; 
  unsigned char *tvnsnm;
  unsigned short tvnsnml;
};
typedef struct sqltvn sqltvn;

struct sqladts
{
  unsigned int adtvsn; 
  unsigned short adtmode; 
  unsigned short adtnum;  
  sqltvn adttvn[1];       
};
typedef struct sqladts sqladts;

static struct sqladts sqladt = {
  1,1,0,
};

/* Binding to PL/SQL Records */
struct sqltdss
{
  unsigned int tdsvsn; 
  unsigned short tdsnum; 
  unsigned char *tdsval[1]; 
};
typedef struct sqltdss sqltdss;
static struct sqltdss sqltds =
{
  1,
  0,
};

/* File name & Package Name */
struct sqlcxp
{
  unsigned short fillen;
           char  filnam[93];
};
static const struct sqlcxp sqlfpn =
{
    92,
    "E:\\github-repository\\hades\\dev-project\\sphinx\\src\\Source\\proc\\find_stock_transaction_data.pc"
};


static unsigned int sqlctx = 1320545917;


static struct sqlexd {
   unsigned int   sqlvsn;
   unsigned int   arrsiz;
   unsigned int   iters;
   unsigned int   offset;
   unsigned short selerr;
   unsigned short sqlety;
   unsigned int   occurs;
      const short *cud;
   unsigned char  *sqlest;
      const char  *stmt;
   sqladts *sqladtp;
   sqltdss *sqltdsp;
            void  **sqphsv;
   unsigned int   *sqphsl;
            int   *sqphss;
            void  **sqpind;
            int   *sqpins;
   unsigned int   *sqparm;
   unsigned int   **sqparc;
   unsigned short  *sqpadto;
   unsigned short  *sqptdso;
   unsigned int   sqlcmax;
   unsigned int   sqlcmin;
   unsigned int   sqlcincr;
   unsigned int   sqlctimeout;
   unsigned int   sqlcnowait;
              int   sqfoff;
   unsigned int   sqcmod;
   unsigned int   sqfmod;
   unsigned int   sqlpfmem;
            void  *sqhstv[40];
   unsigned int   sqhstl[40];
            int   sqhsts[40];
            void  *sqindv[40];
            int   sqinds[40];
   unsigned int   sqharm[40];
   unsigned int   *sqharc[40];
   unsigned short  sqadto[40];
   unsigned short  sqtdso[40];
} sqlstm = {13,40};

/* SQLLIB Prototypes */
extern void sqlcxt (void **, unsigned int *,
                    struct sqlexd *, const struct sqlcxp *);
extern void sqlcx2t(void **, unsigned int *,
                    struct sqlexd *, const struct sqlcxp *);
extern void sqlbuft(void **, char *);
extern void sqlgs2t(void **, char *);
extern void sqlorat(void **, unsigned int *, void *);

/* Forms Interface */
static const int IAPSUCC = 0;
static const int IAPFAIL = 1403;
static const int IAPFTL  = 535;
extern void sqliem(unsigned char *, signed int *);

 static const char *sq0003 = 
"select to_char(t.date_,'yyyy-mm-dd') ,t.*  from stock_transaction_data_all \
t where ((((((((((((((((((((((t.code_=:b0 and t.date_ between to_date(:b1,'yy\
yy-mm-dd') and to_date(:b2,'yyyy-mm-dd')) and t.change_amount is  not null ) \
and t.change_range is  not null ) and t.change_range_ex_right is  not null ) \
and t.up_down is  not null ) and t.ma5 is  not null ) and t.ma10 is  not null\
 ) and t.ma20 is  not null ) and t.ma60 is  not null ) and t.ma120 is  not nu\
ll ) and t.ma250 is  not null ) and t.ema12 is  not null ) and t.ema26 is  no\
t null ) and t.dif is  not null ) and t.dea is  not null ) and t.rsv is  not \
null ) and t.k is  not null ) and t.d is  not null ) and t.ha_open_price is  \
not null ) and t.ha_close_price is  not null ) and t.ha_highest_price is  not\
 null ) and t.ha_lowest_price is  not null ) order by t.date_ desc           \
  ";

typedef struct { unsigned short len; unsigned char arr[1]; } VARCHAR;
typedef struct { unsigned short len; unsigned char arr[1]; } varchar;

/* cud (compilation unit data) array */
static const short sqlcud0[] =
{13,4130,852,0,0,
5,0,0,1,0,0,32,14,0,0,0,0,0,1,0,
20,0,0,0,0,0,539,107,0,0,4,4,0,1,0,1,97,0,0,1,10,0,0,1,10,0,0,1,10,0,0,
51,0,0,3,847,0,521,128,0,0,3,3,0,1,0,1,9,0,0,1,9,0,0,1,9,0,0,
78,0,0,3,0,0,525,131,0,0,40,0,0,1,0,2,97,0,0,2,3,0,0,2,97,0,0,2,97,0,0,2,4,0,0,
2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,
0,2,4,0,0,2,4,0,0,2,4,0,0,2,3,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,
0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,
4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,2,4,0,0,
253,0,0,3,0,0,527,198,0,0,0,0,0,1,0,
268,0,0,4,0,0,542,199,0,0,0,0,0,1,0,
};


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../../Include/model/stock_transaction_data.h"
#include "../../Include/domain/database_info.h"
#include "../../Include/manager/log_manager.h"
#include "../../Include/util/char_util.h"

/* EXEC SQL INCLUDE SQLCA;
 */ 
/*
 * $Header: precomp/public/sqlca.h /osds/1 2011/08/02 23:12:16 pnayak Exp $ sqlca.h 
 */

/* Copyright (c) 1985, 2003, Oracle Corporation.  All rights reserved.  */
 
/*
NAME
  SQLCA : SQL Communications Area.
FUNCTION
  Contains no code. Oracle fills in the SQLCA with status info
  during the execution of a SQL stmt.
NOTES
  **************************************************************
  ***                                                        ***
  *** This file is SOSD.  Porters must change the data types ***
  *** appropriately on their platform.  See notes/pcport.doc ***
  *** for more information.                                  ***
  ***                                                        ***
  **************************************************************

  If the symbol SQLCA_STORAGE_CLASS is defined, then the SQLCA
  will be defined to have this storage class. For example:
 
    #define SQLCA_STORAGE_CLASS extern
 
  will define the SQLCA as an extern.
 
  If the symbol SQLCA_INIT is defined, then the SQLCA will be
  statically initialized. Although this is not necessary in order
  to use the SQLCA, it is a good pgming practice not to have
  unitialized variables. However, some C compilers/OS's don't
  allow automatic variables to be init'd in this manner. Therefore,
  if you are INCLUDE'ing the SQLCA in a place where it would be
  an automatic AND your C compiler/OS doesn't allow this style
  of initialization, then SQLCA_INIT should be left undefined --
  all others can define SQLCA_INIT if they wish.

  If the symbol SQLCA_NONE is defined, then the SQLCA variable will
  not be defined at all.  The symbol SQLCA_NONE should not be defined
  in source modules that have embedded SQL.  However, source modules
  that have no embedded SQL, but need to manipulate a sqlca model
  passed in as a parameter, can set the SQLCA_NONE symbol to avoid
  creation of an extraneous sqlca variable.
 
MODIFIED
    lvbcheng   07/31/98 -  long to int
    jbasu      12/12/94 -  Bug 217878: note this is an SOSD file
    losborne   08/11/92 -  No sqlca var if SQLCA_NONE macro set 
  Clare      12/06/84 - Ch SQLCA to not be an extern.
  Clare      10/21/85 - Add initialization.
  Bradbury   01/05/86 - Only initialize when SQLCA_INIT set
  Clare      06/12/86 - Add SQLCA_STORAGE_CLASS option.
*/
 
#ifndef SQLCA
#define SQLCA 1
 
struct   sqlca
         {
         /* ub1 */ char    sqlcaid[8];
         /* b4  */ int     sqlabc;
         /* b4  */ int     sqlcode;
         struct
           {
           /* ub2 */ unsigned short sqlerrml;
           /* ub1 */ char           sqlerrmc[70];
           } sqlerrm;
         /* ub1 */ char    sqlerrp[8];
         /* b4  */ int     sqlerrd[6];
         /* ub1 */ char    sqlwarn[8];
         /* ub1 */ char    sqlext[8];
         };

#ifndef SQLCA_NONE 
#ifdef   SQLCA_STORAGE_CLASS
SQLCA_STORAGE_CLASS model sqlca sqlca
#else
         struct sqlca sqlca
#endif
 
#ifdef  SQLCA_INIT
         = {
         {'S', 'Q', 'L', 'C', 'A', ' ', ' ', ' '},
         sizeof(model sqlca),
         0,
         { 0, {0}},
         {'N', 'O', 'T', ' ', 'S', 'E', 'T', ' '},
         {0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0}
         }
#endif
         ;
#endif
 
#endif
 
/* end SQLCA */


void SqlError(char *msg)
{
  printf("\n%s %s\n", msg,(char *)sqlca.sqlerrm.sqlerrmc);
  /* EXEC SQL ROLLBACK RELEASE; */ 

{
  struct sqlexd sqlstm;
  sqlstm.sqlvsn = 13;
  sqlstm.arrsiz = 0;
  sqlstm.sqladtp = &sqladt;
  sqlstm.sqltdsp = &sqltds;
  sqlstm.iters = (unsigned int  )1;
  sqlstm.offset = (unsigned int  )5;
  sqlstm.cud = sqlcud0;
  sqlstm.sqlest = (unsigned char  *)&sqlca;
  sqlstm.sqlety = (unsigned short)4352;
  sqlstm.occurs = (unsigned int  )0;
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
}


  exit(0);
}

void GetStockTransactionData(struct DatabaseInfo *databaseInfo, char code[], int *count,
                             struct StockTransactionData *trainDataList, double (*trainLabelList)[1],
                                     char *beginDate, char *endDate)
{
	system("chcp 65001");
	
	/* EXEC ORACLE OPTION (RELEASE_CURSOR = YES); */ 

	/* EXEC SQL WHENEVER SQLERROR DO SqlError(" <ERROR> "); */ 


	/* 开始定义变量 */
	/* EXEC SQL BEGIN DECLARE SECTION; */ 

	/* 用户名 */
	/* VARCHAR v_username[128]; */ 
struct { unsigned short len; unsigned char arr[128]; } v_username;

	/* 密码 */
	/* VARCHAR v_password[128]; */ 
struct { unsigned short len; unsigned char arr[128]; } v_password;

	/* 数据库服务名 */
	/* VARCHAR v_schema[256]; */ 
struct { unsigned short len; unsigned char arr[256]; } v_schema;

	/* 数据库连接字符串 */
	/* VARCHAR v_url[256]; */ 
struct { unsigned short len; unsigned char arr[256]; } v_url;

	/* 股票代码 */
	/* VARCHAR v_code[256]; */ 
struct { unsigned short len; unsigned char arr[256]; } v_code;

    /* 开始时间 */
    /* VARCHAR v_begin_date[256]; */ 
struct { unsigned short len; unsigned char arr[256]; } v_begin_date;

    /* 结束时间 */
    /* VARCHAR v_end_date[256]; */ 
struct { unsigned short len; unsigned char arr[256]; } v_end_date;

	/* sql返回的列 */
    char date[20];
    long id_;
    char date_[20];
    char code_[20];
	double openPrice;
    double closePrice;
    double highestPrice;
    double lowestPrice;
    double lastClosePrice;
    double changeAmount;
    double changeRange;
    double changeRangeExRight;
    double upDown;
    double turnoverRate;
    double volume;
    double turnover;
    double totalMarketValue;
    double circulationMarketValue;
    long transactionNumber;
    short transactionNumber_flagVariable;
    double ma5;
    double ma10;
    double ma20;
    double ma60;
    double ma120;
    double ma250;
    double ema12;
    double ema26;
    double dif;
    double dea;
    double rsv;
    double fiveDayVolatility;
    short fiveDayVolatility_flagVariable;
    double tenDayVolatility;
    short tenDayVolatility_flagVariable;
    double twentyDayVolatility;
    short twentyDayVolatility_flagVariable;
    double twoHundredFifty;
    short twoHundredFifty_flagVariable;
    double k;
    double d;
    double haOpenPrice;
    double haClosePrice;
    double haHighestPrice;
    double haLowestPrice;
	/* EXEC SQL END DECLARE SECTION; */ 


	// 变量赋值
	/*strcpy(v_username.arr, databaseInfo->username);
	v_username.len = (unsigned short)strlen((char *)v_username.arr);
	strcpy(v_password.arr, databaseInfo->password);
	v_password.len = (unsigned short)strlen((char *)v_password.arr);
	strcpy(v_schema.arr, databaseInfo->schema);
	v_schema.len = (unsigned short)strlen((char *)v_schema.arr);*/
	strcpy(v_url.arr, databaseInfo->url);
	v_url.len = (unsigned short)strlen((char *)v_url.arr);
	strcpy(v_code.arr, code);
	v_code.len = (unsigned short)strlen((char *)v_code.arr);
    strcpy(v_begin_date.arr, beginDate);
    v_begin_date.len = (unsigned short)strlen((char *)v_begin_date.arr);
    strcpy(v_end_date.arr, endDate);
    v_end_date.len = (unsigned short)strlen((char *)v_end_date.arr);

	//EXEC SQL CONNECT :v_username IDENTIFIED BY :v_password USING :v_schema;
	/* EXEC SQL CONNECT :"scott/tiger@adam"; */ 

{
 struct sqlexd sqlstm;
 sqlstm.sqlvsn = 13;
 sqlstm.arrsiz = 4;
 sqlstm.sqladtp = &sqladt;
 sqlstm.sqltdsp = &sqltds;
 sqlstm.iters = (unsigned int  )10;
 sqlstm.offset = (unsigned int  )20;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)4352;
 sqlstm.occurs = (unsigned int  )0;
 sqlstm.sqhstv[0] = (         void  *)"scott/tiger@adam";
 sqlstm.sqhstl[0] = (unsigned int  )0;
 sqlstm.sqhsts[0] = (         int  )0;
 sqlstm.sqindv[0] = (         void  *)0;
 sqlstm.sqinds[0] = (         int  )0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqadto[0] = (unsigned short )0;
 sqlstm.sqtdso[0] = (unsigned short )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqphss = sqlstm.sqhsts;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqpins = sqlstm.sqinds;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlstm.sqpadto = sqlstm.sqadto;
 sqlstm.sqptdso = sqlstm.sqtdso;
 sqlstm.sqlcmax = (unsigned int )100;
 sqlstm.sqlcmin = (unsigned int )2;
 sqlstm.sqlcincr = (unsigned int )1;
 sqlstm.sqlctimeout = (unsigned int )0;
 sqlstm.sqlcnowait = (unsigned int )0;
 sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) SqlError(" <ERROR> ");
}


	
	//判断是否连接到数据库
	if (sqlca.sqlcode)
	{
		printf("ORA-ERROR: sqlca.sqlcode=%d\n", sqlca.sqlcode);
	    exit(0);
	}
	printf("数据库【%s】连接成功\n", databaseInfo->schema);
	
	/* EXEC SQL declare cur_stock_transaction_data cursor for
	    select to_char(t.date_, 'yyyy-mm-dd'), t.*
	    from stock_transaction_data_all t
	    where t.code_=:v_code and t.date_ between to_date(:v_begin_date, 'yyyy-mm-dd') and to_date(:v_end_date, 'yyyy-mm-dd')
	        and t.change_amount is not null and t.change_range is not null and t.change_range_ex_right is not null
            and t.up_down is not null and t.ma5 is not null and t.ma10 is not null and t.ma20 is not null
            and t.ma60 is not null and t.ma120 is not null and t.ma250 is not null and t.ema12 is not null
            and t.ema26 is not null and t.dif is not null and t.dea is not null and t.rsv is not null
            and t.k is not null and t.d is not null and t.ha_open_price is not null and t.ha_close_price is not null
            and t.ha_highest_price is not null and t.ha_lowest_price is not null
	    order by t.date_ desc; */ 

	/* EXEC SQL open cur_stock_transaction_data; */ 

{
 struct sqlexd sqlstm;
 sqlstm.sqlvsn = 13;
 sqlstm.arrsiz = 4;
 sqlstm.sqladtp = &sqladt;
 sqlstm.sqltdsp = &sqltds;
 sqlstm.stmt = sq0003;
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )51;
 sqlstm.selerr = (unsigned short)1;
 sqlstm.sqlpfmem = (unsigned int  )0;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)4352;
 sqlstm.occurs = (unsigned int  )0;
 sqlstm.sqcmod = (unsigned int )0;
 sqlstm.sqhstv[0] = (         void  *)&v_code;
 sqlstm.sqhstl[0] = (unsigned int  )258;
 sqlstm.sqhsts[0] = (         int  )0;
 sqlstm.sqindv[0] = (         void  *)0;
 sqlstm.sqinds[0] = (         int  )0;
 sqlstm.sqharm[0] = (unsigned int  )0;
 sqlstm.sqadto[0] = (unsigned short )0;
 sqlstm.sqtdso[0] = (unsigned short )0;
 sqlstm.sqhstv[1] = (         void  *)&v_begin_date;
 sqlstm.sqhstl[1] = (unsigned int  )258;
 sqlstm.sqhsts[1] = (         int  )0;
 sqlstm.sqindv[1] = (         void  *)0;
 sqlstm.sqinds[1] = (         int  )0;
 sqlstm.sqharm[1] = (unsigned int  )0;
 sqlstm.sqadto[1] = (unsigned short )0;
 sqlstm.sqtdso[1] = (unsigned short )0;
 sqlstm.sqhstv[2] = (         void  *)&v_end_date;
 sqlstm.sqhstl[2] = (unsigned int  )258;
 sqlstm.sqhsts[2] = (         int  )0;
 sqlstm.sqindv[2] = (         void  *)0;
 sqlstm.sqinds[2] = (         int  )0;
 sqlstm.sqharm[2] = (unsigned int  )0;
 sqlstm.sqadto[2] = (unsigned short )0;
 sqlstm.sqtdso[2] = (unsigned short )0;
 sqlstm.sqphsv = sqlstm.sqhstv;
 sqlstm.sqphsl = sqlstm.sqhstl;
 sqlstm.sqphss = sqlstm.sqhsts;
 sqlstm.sqpind = sqlstm.sqindv;
 sqlstm.sqpins = sqlstm.sqinds;
 sqlstm.sqparm = sqlstm.sqharm;
 sqlstm.sqparc = sqlstm.sqharc;
 sqlstm.sqpadto = sqlstm.sqadto;
 sqlstm.sqptdso = sqlstm.sqtdso;
 sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) SqlError(" <ERROR> ");
}


    /* EXEC SQL WHENEVER NOT FOUND DO break; */ 

    do {
        /* EXEC SQL fetch cur_stock_transaction_data into :date, :id_, :date_, :code_, :openPrice, :closePrice,
            :highestPrice, :lowestPrice, :lastClosePrice, :changeAmount, :changeRange, :changeRangeExRight,
            :upDown, :turnoverRate, :volume, :turnover, :totalMarketValue, :circulationMarketValue,
            :transactionNumber :transactionNumber_flagVariable, :ma5, :ma10, :ma20, :ma60, :ma120, :ma250,
            :ema12, :ema26, :dif, :dea, :fiveDayVolatility :fiveDayVolatility_flagVariable, :tenDayVolatility
            :tenDayVolatility_flagVariable, :twentyDayVolatility :twentyDayVolatility_flagVariable,
            :twoHundredFifty :twoHundredFifty_flagVariable, :rsv, :k, :d, :haOpenPrice, :haClosePrice,
            :haHighestPrice, :haLowestPrice; */ 

{
        struct sqlexd sqlstm;
        sqlstm.sqlvsn = 13;
        sqlstm.arrsiz = 40;
        sqlstm.sqladtp = &sqladt;
        sqlstm.sqltdsp = &sqltds;
        sqlstm.iters = (unsigned int  )1;
        sqlstm.offset = (unsigned int  )78;
        sqlstm.selerr = (unsigned short)1;
        sqlstm.sqlpfmem = (unsigned int  )0;
        sqlstm.cud = sqlcud0;
        sqlstm.sqlest = (unsigned char  *)&sqlca;
        sqlstm.sqlety = (unsigned short)4352;
        sqlstm.occurs = (unsigned int  )0;
        sqlstm.sqfoff = (           int )0;
        sqlstm.sqfmod = (unsigned int )2;
        sqlstm.sqhstv[0] = (         void  *)date;
        sqlstm.sqhstl[0] = (unsigned int  )20;
        sqlstm.sqhsts[0] = (         int  )0;
        sqlstm.sqindv[0] = (         void  *)0;
        sqlstm.sqinds[0] = (         int  )0;
        sqlstm.sqharm[0] = (unsigned int  )0;
        sqlstm.sqadto[0] = (unsigned short )0;
        sqlstm.sqtdso[0] = (unsigned short )0;
        sqlstm.sqhstv[1] = (         void  *)&id_;
        sqlstm.sqhstl[1] = (unsigned int  )sizeof(long);
        sqlstm.sqhsts[1] = (         int  )0;
        sqlstm.sqindv[1] = (         void  *)0;
        sqlstm.sqinds[1] = (         int  )0;
        sqlstm.sqharm[1] = (unsigned int  )0;
        sqlstm.sqadto[1] = (unsigned short )0;
        sqlstm.sqtdso[1] = (unsigned short )0;
        sqlstm.sqhstv[2] = (         void  *)date_;
        sqlstm.sqhstl[2] = (unsigned int  )20;
        sqlstm.sqhsts[2] = (         int  )0;
        sqlstm.sqindv[2] = (         void  *)0;
        sqlstm.sqinds[2] = (         int  )0;
        sqlstm.sqharm[2] = (unsigned int  )0;
        sqlstm.sqadto[2] = (unsigned short )0;
        sqlstm.sqtdso[2] = (unsigned short )0;
        sqlstm.sqhstv[3] = (         void  *)code_;
        sqlstm.sqhstl[3] = (unsigned int  )20;
        sqlstm.sqhsts[3] = (         int  )0;
        sqlstm.sqindv[3] = (         void  *)0;
        sqlstm.sqinds[3] = (         int  )0;
        sqlstm.sqharm[3] = (unsigned int  )0;
        sqlstm.sqadto[3] = (unsigned short )0;
        sqlstm.sqtdso[3] = (unsigned short )0;
        sqlstm.sqhstv[4] = (         void  *)&openPrice;
        sqlstm.sqhstl[4] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[4] = (         int  )0;
        sqlstm.sqindv[4] = (         void  *)0;
        sqlstm.sqinds[4] = (         int  )0;
        sqlstm.sqharm[4] = (unsigned int  )0;
        sqlstm.sqadto[4] = (unsigned short )0;
        sqlstm.sqtdso[4] = (unsigned short )0;
        sqlstm.sqhstv[5] = (         void  *)&closePrice;
        sqlstm.sqhstl[5] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[5] = (         int  )0;
        sqlstm.sqindv[5] = (         void  *)0;
        sqlstm.sqinds[5] = (         int  )0;
        sqlstm.sqharm[5] = (unsigned int  )0;
        sqlstm.sqadto[5] = (unsigned short )0;
        sqlstm.sqtdso[5] = (unsigned short )0;
        sqlstm.sqhstv[6] = (         void  *)&highestPrice;
        sqlstm.sqhstl[6] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[6] = (         int  )0;
        sqlstm.sqindv[6] = (         void  *)0;
        sqlstm.sqinds[6] = (         int  )0;
        sqlstm.sqharm[6] = (unsigned int  )0;
        sqlstm.sqadto[6] = (unsigned short )0;
        sqlstm.sqtdso[6] = (unsigned short )0;
        sqlstm.sqhstv[7] = (         void  *)&lowestPrice;
        sqlstm.sqhstl[7] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[7] = (         int  )0;
        sqlstm.sqindv[7] = (         void  *)0;
        sqlstm.sqinds[7] = (         int  )0;
        sqlstm.sqharm[7] = (unsigned int  )0;
        sqlstm.sqadto[7] = (unsigned short )0;
        sqlstm.sqtdso[7] = (unsigned short )0;
        sqlstm.sqhstv[8] = (         void  *)&lastClosePrice;
        sqlstm.sqhstl[8] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[8] = (         int  )0;
        sqlstm.sqindv[8] = (         void  *)0;
        sqlstm.sqinds[8] = (         int  )0;
        sqlstm.sqharm[8] = (unsigned int  )0;
        sqlstm.sqadto[8] = (unsigned short )0;
        sqlstm.sqtdso[8] = (unsigned short )0;
        sqlstm.sqhstv[9] = (         void  *)&changeAmount;
        sqlstm.sqhstl[9] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[9] = (         int  )0;
        sqlstm.sqindv[9] = (         void  *)0;
        sqlstm.sqinds[9] = (         int  )0;
        sqlstm.sqharm[9] = (unsigned int  )0;
        sqlstm.sqadto[9] = (unsigned short )0;
        sqlstm.sqtdso[9] = (unsigned short )0;
        sqlstm.sqhstv[10] = (         void  *)&changeRange;
        sqlstm.sqhstl[10] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[10] = (         int  )0;
        sqlstm.sqindv[10] = (         void  *)0;
        sqlstm.sqinds[10] = (         int  )0;
        sqlstm.sqharm[10] = (unsigned int  )0;
        sqlstm.sqadto[10] = (unsigned short )0;
        sqlstm.sqtdso[10] = (unsigned short )0;
        sqlstm.sqhstv[11] = (         void  *)&changeRangeExRight;
        sqlstm.sqhstl[11] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[11] = (         int  )0;
        sqlstm.sqindv[11] = (         void  *)0;
        sqlstm.sqinds[11] = (         int  )0;
        sqlstm.sqharm[11] = (unsigned int  )0;
        sqlstm.sqadto[11] = (unsigned short )0;
        sqlstm.sqtdso[11] = (unsigned short )0;
        sqlstm.sqhstv[12] = (         void  *)&upDown;
        sqlstm.sqhstl[12] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[12] = (         int  )0;
        sqlstm.sqindv[12] = (         void  *)0;
        sqlstm.sqinds[12] = (         int  )0;
        sqlstm.sqharm[12] = (unsigned int  )0;
        sqlstm.sqadto[12] = (unsigned short )0;
        sqlstm.sqtdso[12] = (unsigned short )0;
        sqlstm.sqhstv[13] = (         void  *)&turnoverRate;
        sqlstm.sqhstl[13] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[13] = (         int  )0;
        sqlstm.sqindv[13] = (         void  *)0;
        sqlstm.sqinds[13] = (         int  )0;
        sqlstm.sqharm[13] = (unsigned int  )0;
        sqlstm.sqadto[13] = (unsigned short )0;
        sqlstm.sqtdso[13] = (unsigned short )0;
        sqlstm.sqhstv[14] = (         void  *)&volume;
        sqlstm.sqhstl[14] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[14] = (         int  )0;
        sqlstm.sqindv[14] = (         void  *)0;
        sqlstm.sqinds[14] = (         int  )0;
        sqlstm.sqharm[14] = (unsigned int  )0;
        sqlstm.sqadto[14] = (unsigned short )0;
        sqlstm.sqtdso[14] = (unsigned short )0;
        sqlstm.sqhstv[15] = (         void  *)&turnover;
        sqlstm.sqhstl[15] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[15] = (         int  )0;
        sqlstm.sqindv[15] = (         void  *)0;
        sqlstm.sqinds[15] = (         int  )0;
        sqlstm.sqharm[15] = (unsigned int  )0;
        sqlstm.sqadto[15] = (unsigned short )0;
        sqlstm.sqtdso[15] = (unsigned short )0;
        sqlstm.sqhstv[16] = (         void  *)&totalMarketValue;
        sqlstm.sqhstl[16] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[16] = (         int  )0;
        sqlstm.sqindv[16] = (         void  *)0;
        sqlstm.sqinds[16] = (         int  )0;
        sqlstm.sqharm[16] = (unsigned int  )0;
        sqlstm.sqadto[16] = (unsigned short )0;
        sqlstm.sqtdso[16] = (unsigned short )0;
        sqlstm.sqhstv[17] = (         void  *)&circulationMarketValue;
        sqlstm.sqhstl[17] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[17] = (         int  )0;
        sqlstm.sqindv[17] = (         void  *)0;
        sqlstm.sqinds[17] = (         int  )0;
        sqlstm.sqharm[17] = (unsigned int  )0;
        sqlstm.sqadto[17] = (unsigned short )0;
        sqlstm.sqtdso[17] = (unsigned short )0;
        sqlstm.sqhstv[18] = (         void  *)&transactionNumber;
        sqlstm.sqhstl[18] = (unsigned int  )sizeof(long);
        sqlstm.sqhsts[18] = (         int  )0;
        sqlstm.sqindv[18] = (         void  *)&transactionNumber_flagVariable;
        sqlstm.sqinds[18] = (         int  )0;
        sqlstm.sqharm[18] = (unsigned int  )0;
        sqlstm.sqadto[18] = (unsigned short )0;
        sqlstm.sqtdso[18] = (unsigned short )0;
        sqlstm.sqhstv[19] = (         void  *)&ma5;
        sqlstm.sqhstl[19] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[19] = (         int  )0;
        sqlstm.sqindv[19] = (         void  *)0;
        sqlstm.sqinds[19] = (         int  )0;
        sqlstm.sqharm[19] = (unsigned int  )0;
        sqlstm.sqadto[19] = (unsigned short )0;
        sqlstm.sqtdso[19] = (unsigned short )0;
        sqlstm.sqhstv[20] = (         void  *)&ma10;
        sqlstm.sqhstl[20] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[20] = (         int  )0;
        sqlstm.sqindv[20] = (         void  *)0;
        sqlstm.sqinds[20] = (         int  )0;
        sqlstm.sqharm[20] = (unsigned int  )0;
        sqlstm.sqadto[20] = (unsigned short )0;
        sqlstm.sqtdso[20] = (unsigned short )0;
        sqlstm.sqhstv[21] = (         void  *)&ma20;
        sqlstm.sqhstl[21] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[21] = (         int  )0;
        sqlstm.sqindv[21] = (         void  *)0;
        sqlstm.sqinds[21] = (         int  )0;
        sqlstm.sqharm[21] = (unsigned int  )0;
        sqlstm.sqadto[21] = (unsigned short )0;
        sqlstm.sqtdso[21] = (unsigned short )0;
        sqlstm.sqhstv[22] = (         void  *)&ma60;
        sqlstm.sqhstl[22] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[22] = (         int  )0;
        sqlstm.sqindv[22] = (         void  *)0;
        sqlstm.sqinds[22] = (         int  )0;
        sqlstm.sqharm[22] = (unsigned int  )0;
        sqlstm.sqadto[22] = (unsigned short )0;
        sqlstm.sqtdso[22] = (unsigned short )0;
        sqlstm.sqhstv[23] = (         void  *)&ma120;
        sqlstm.sqhstl[23] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[23] = (         int  )0;
        sqlstm.sqindv[23] = (         void  *)0;
        sqlstm.sqinds[23] = (         int  )0;
        sqlstm.sqharm[23] = (unsigned int  )0;
        sqlstm.sqadto[23] = (unsigned short )0;
        sqlstm.sqtdso[23] = (unsigned short )0;
        sqlstm.sqhstv[24] = (         void  *)&ma250;
        sqlstm.sqhstl[24] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[24] = (         int  )0;
        sqlstm.sqindv[24] = (         void  *)0;
        sqlstm.sqinds[24] = (         int  )0;
        sqlstm.sqharm[24] = (unsigned int  )0;
        sqlstm.sqadto[24] = (unsigned short )0;
        sqlstm.sqtdso[24] = (unsigned short )0;
        sqlstm.sqhstv[25] = (         void  *)&ema12;
        sqlstm.sqhstl[25] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[25] = (         int  )0;
        sqlstm.sqindv[25] = (         void  *)0;
        sqlstm.sqinds[25] = (         int  )0;
        sqlstm.sqharm[25] = (unsigned int  )0;
        sqlstm.sqadto[25] = (unsigned short )0;
        sqlstm.sqtdso[25] = (unsigned short )0;
        sqlstm.sqhstv[26] = (         void  *)&ema26;
        sqlstm.sqhstl[26] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[26] = (         int  )0;
        sqlstm.sqindv[26] = (         void  *)0;
        sqlstm.sqinds[26] = (         int  )0;
        sqlstm.sqharm[26] = (unsigned int  )0;
        sqlstm.sqadto[26] = (unsigned short )0;
        sqlstm.sqtdso[26] = (unsigned short )0;
        sqlstm.sqhstv[27] = (         void  *)&dif;
        sqlstm.sqhstl[27] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[27] = (         int  )0;
        sqlstm.sqindv[27] = (         void  *)0;
        sqlstm.sqinds[27] = (         int  )0;
        sqlstm.sqharm[27] = (unsigned int  )0;
        sqlstm.sqadto[27] = (unsigned short )0;
        sqlstm.sqtdso[27] = (unsigned short )0;
        sqlstm.sqhstv[28] = (         void  *)&dea;
        sqlstm.sqhstl[28] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[28] = (         int  )0;
        sqlstm.sqindv[28] = (         void  *)0;
        sqlstm.sqinds[28] = (         int  )0;
        sqlstm.sqharm[28] = (unsigned int  )0;
        sqlstm.sqadto[28] = (unsigned short )0;
        sqlstm.sqtdso[28] = (unsigned short )0;
        sqlstm.sqhstv[29] = (         void  *)&fiveDayVolatility;
        sqlstm.sqhstl[29] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[29] = (         int  )0;
        sqlstm.sqindv[29] = (         void  *)&fiveDayVolatility_flagVariable;
        sqlstm.sqinds[29] = (         int  )0;
        sqlstm.sqharm[29] = (unsigned int  )0;
        sqlstm.sqadto[29] = (unsigned short )0;
        sqlstm.sqtdso[29] = (unsigned short )0;
        sqlstm.sqhstv[30] = (         void  *)&tenDayVolatility;
        sqlstm.sqhstl[30] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[30] = (         int  )0;
        sqlstm.sqindv[30] = (         void  *)&tenDayVolatility_flagVariable;
        sqlstm.sqinds[30] = (         int  )0;
        sqlstm.sqharm[30] = (unsigned int  )0;
        sqlstm.sqadto[30] = (unsigned short )0;
        sqlstm.sqtdso[30] = (unsigned short )0;
        sqlstm.sqhstv[31] = (         void  *)&twentyDayVolatility;
        sqlstm.sqhstl[31] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[31] = (         int  )0;
        sqlstm.sqindv[31] = (         void  *)&twentyDayVolatility_flagVariable;
        sqlstm.sqinds[31] = (         int  )0;
        sqlstm.sqharm[31] = (unsigned int  )0;
        sqlstm.sqadto[31] = (unsigned short )0;
        sqlstm.sqtdso[31] = (unsigned short )0;
        sqlstm.sqhstv[32] = (         void  *)&twoHundredFifty;
        sqlstm.sqhstl[32] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[32] = (         int  )0;
        sqlstm.sqindv[32] = (         void  *)&twoHundredFifty_flagVariable;
        sqlstm.sqinds[32] = (         int  )0;
        sqlstm.sqharm[32] = (unsigned int  )0;
        sqlstm.sqadto[32] = (unsigned short )0;
        sqlstm.sqtdso[32] = (unsigned short )0;
        sqlstm.sqhstv[33] = (         void  *)&rsv;
        sqlstm.sqhstl[33] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[33] = (         int  )0;
        sqlstm.sqindv[33] = (         void  *)0;
        sqlstm.sqinds[33] = (         int  )0;
        sqlstm.sqharm[33] = (unsigned int  )0;
        sqlstm.sqadto[33] = (unsigned short )0;
        sqlstm.sqtdso[33] = (unsigned short )0;
        sqlstm.sqhstv[34] = (         void  *)&k;
        sqlstm.sqhstl[34] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[34] = (         int  )0;
        sqlstm.sqindv[34] = (         void  *)0;
        sqlstm.sqinds[34] = (         int  )0;
        sqlstm.sqharm[34] = (unsigned int  )0;
        sqlstm.sqadto[34] = (unsigned short )0;
        sqlstm.sqtdso[34] = (unsigned short )0;
        sqlstm.sqhstv[35] = (         void  *)&d;
        sqlstm.sqhstl[35] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[35] = (         int  )0;
        sqlstm.sqindv[35] = (         void  *)0;
        sqlstm.sqinds[35] = (         int  )0;
        sqlstm.sqharm[35] = (unsigned int  )0;
        sqlstm.sqadto[35] = (unsigned short )0;
        sqlstm.sqtdso[35] = (unsigned short )0;
        sqlstm.sqhstv[36] = (         void  *)&haOpenPrice;
        sqlstm.sqhstl[36] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[36] = (         int  )0;
        sqlstm.sqindv[36] = (         void  *)0;
        sqlstm.sqinds[36] = (         int  )0;
        sqlstm.sqharm[36] = (unsigned int  )0;
        sqlstm.sqadto[36] = (unsigned short )0;
        sqlstm.sqtdso[36] = (unsigned short )0;
        sqlstm.sqhstv[37] = (         void  *)&haClosePrice;
        sqlstm.sqhstl[37] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[37] = (         int  )0;
        sqlstm.sqindv[37] = (         void  *)0;
        sqlstm.sqinds[37] = (         int  )0;
        sqlstm.sqharm[37] = (unsigned int  )0;
        sqlstm.sqadto[37] = (unsigned short )0;
        sqlstm.sqtdso[37] = (unsigned short )0;
        sqlstm.sqhstv[38] = (         void  *)&haHighestPrice;
        sqlstm.sqhstl[38] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[38] = (         int  )0;
        sqlstm.sqindv[38] = (         void  *)0;
        sqlstm.sqinds[38] = (         int  )0;
        sqlstm.sqharm[38] = (unsigned int  )0;
        sqlstm.sqadto[38] = (unsigned short )0;
        sqlstm.sqtdso[38] = (unsigned short )0;
        sqlstm.sqhstv[39] = (         void  *)&haLowestPrice;
        sqlstm.sqhstl[39] = (unsigned int  )sizeof(double);
        sqlstm.sqhsts[39] = (         int  )0;
        sqlstm.sqindv[39] = (         void  *)0;
        sqlstm.sqinds[39] = (         int  )0;
        sqlstm.sqharm[39] = (unsigned int  )0;
        sqlstm.sqadto[39] = (unsigned short )0;
        sqlstm.sqtdso[39] = (unsigned short )0;
        sqlstm.sqphsv = sqlstm.sqhstv;
        sqlstm.sqphsl = sqlstm.sqhstl;
        sqlstm.sqphss = sqlstm.sqhsts;
        sqlstm.sqpind = sqlstm.sqindv;
        sqlstm.sqpins = sqlstm.sqinds;
        sqlstm.sqparm = sqlstm.sqharm;
        sqlstm.sqparc = sqlstm.sqharc;
        sqlstm.sqpadto = sqlstm.sqadto;
        sqlstm.sqptdso = sqlstm.sqtdso;
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
        if (sqlca.sqlcode == 1403) break;
        if (sqlca.sqlcode < 0) SqlError(" <ERROR> ");
}


        trainDataList->id_ = id_;
        Trim(&code_);
        trainDataList->code_ = &code_;
        Trim(&date);
        trainDataList->date_ = &date;
        trainDataList->openPrice = openPrice;
        (*trainLabelList)[0] = closePrice;
        trainDataList->highestPrice = highestPrice;
        trainDataList->lowestPrice = lowestPrice;
        trainDataList->lastClosePrice = lastClosePrice;
        trainDataList->changeAmount = changeAmount;
        trainDataList->changeRange = changeRange;
        trainDataList->changeRangeExRight = changeRangeExRight;
        trainDataList->upDown = upDown;
        trainDataList->turnoverRate = turnoverRate;
        trainDataList->volume = volume;
        trainDataList->turnover = turnover;
        trainDataList->totalMarketValue = totalMarketValue;
        trainDataList->circulationMarketValue = circulationMarketValue;
        if (transactionNumber_flagVariable == 0){
            trainDataList->transactionNumber = transactionNumber;
        }
        trainDataList->ma5 = ma5;
        trainDataList->ma10 = ma10;
        trainDataList->ma20 = ma20;
        trainDataList->ma60 = ma60;
        trainDataList->ma120 = ma120;
        trainDataList->ma250 = ma250;
        trainDataList->ema12 = ema12;
        trainDataList->ema26 = ema26;
        trainDataList->dif = dif;
        trainDataList->dea = dea;
        if (fiveDayVolatility_flagVariable == 0) {
            trainDataList->fiveDayVolatility = fiveDayVolatility;
        }
        if (tenDayVolatility_flagVariable == 0) {
            trainDataList->tenDayVolatility = tenDayVolatility;
        }
        if (twentyDayVolatility_flagVariable == 0) {
            trainDataList->twentyDayVolatility = twentyDayVolatility;
        }
        if (twoHundredFifty_flagVariable == 0) {
            trainDataList->twoHundredFifty = twoHundredFifty;
        }
        trainDataList->rsv = rsv;
        trainDataList->k = k;
        trainDataList->d = d;
        trainDataList->haOpenPrice = haOpenPrice;
        trainDataList->haClosePrice = haClosePrice;
        trainDataList->haHighestPrice = haHighestPrice;
        trainDataList->haLowestPrice = haLowestPrice;
        trainDataList++;
        trainLabelList++;
        (*count)++;
    } while (1);

//    for (int i = 0; i < 200; ++i) {
//        printf("_date：【%s】\n", Rtrim(stockTransactionDataList[i].date_));
//    }
	/* EXEC SQL close cur_stock_transaction_data; */ 

{
 struct sqlexd sqlstm;
 sqlstm.sqlvsn = 13;
 sqlstm.arrsiz = 40;
 sqlstm.sqladtp = &sqladt;
 sqlstm.sqltdsp = &sqltds;
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )253;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)4352;
 sqlstm.occurs = (unsigned int  )0;
 sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) SqlError(" <ERROR> ");
}


	/* EXEC SQL commit work release; */ 

{
 struct sqlexd sqlstm;
 sqlstm.sqlvsn = 13;
 sqlstm.arrsiz = 40;
 sqlstm.sqladtp = &sqladt;
 sqlstm.sqltdsp = &sqltds;
 sqlstm.iters = (unsigned int  )1;
 sqlstm.offset = (unsigned int  )268;
 sqlstm.cud = sqlcud0;
 sqlstm.sqlest = (unsigned char  *)&sqlca;
 sqlstm.sqlety = (unsigned short)4352;
 sqlstm.occurs = (unsigned int  )0;
 sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
 if (sqlca.sqlcode < 0) SqlError(" <ERROR> ");
}


	printf("数据库【%s】关闭连接\n", databaseInfo->schema);

//    return stockTransactionDataList;
}