#include <stdio.h>
#include <stdlib.h>

#include "../../Include/kNN2_test_function.h"

int main() {
    double normalTrainingDataset[TRAINING_DATASET_ROW_SIZE][TRAINING_DATASET_COLUMN_SIZE];
    for (int i = 0; i < TRAINING_DATASET_ROW_SIZE; ++i) {
        for (int j = 0; j < TRAINING_DATASET_COLUMN_SIZE; ++j) {
            (normalTrainingDataset[i][j]) = i + j;
        }
    }

    double normalTrainingDatasetLabel[2] = {1, 2};

    double normalTestingDataset[TESTING_DATASET_ROW_SIZE][TESTING_DATASET_COLUMN_SIZE];
    for (int i = 0; i < TESTING_DATASET_ROW_SIZE; ++i) {
        for (int j = 0; j < TESTING_DATASET_COLUMN_SIZE; ++j) {
            normalTestingDataset[i][j] = -(i + j);
        }
    }

    double normalTestingDatasetLabel[2] = {1, 2};

//    struct Point *allDistanceList;
//    for (int i = 0; i < TESTING_DATASET_ROW_SIZE; ++i) {
//        for (int j = 0; j < TRAINING_DATASET_ROW_SIZE; ++j) {
//            (*(*allDistanceList + sizeof(struct Point *))+j) = (struct Point *) malloc(sizeof(struct Point *));
//        }
//    }

    struct Point *p[TESTING_DATASET_ROW_SIZE][TRAINING_DATASET_ROW_SIZE];
//    p[0][0]=(struct Point *)malloc(sizeof(struct Point *));
//    p[0][1]=(struct Point *)malloc(sizeof(struct Point *));
//    struct Point *pp[1][2];
//    pp[0][0]=(struct Point *)malloc(sizeof(struct Point *));
//    pp[0][1]=(struct Point *)malloc(sizeof(struct Point *));
    /***pp =*/ Classify(normalTrainingDataset,
                               normalTrainingDatasetLabel,
                               normalTestingDataset,
                               normalTestingDatasetLabel, *p);
    printf("main");
}