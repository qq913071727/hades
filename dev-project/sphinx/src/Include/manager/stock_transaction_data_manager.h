/*
 * stock_transaction_data_handler.h
 *
 *  Created on: 2021年10月28日
 *      Author: lishen
 */

#ifndef STOCK_TRANSACTION_DATA_HANDLER_H_
#define STOCK_TRANSACTION_DATA_HANDLER_H_

/**
 * 获取x矩阵。某只股票的基础数据组成特征值
 * @param beginDate
 * @param endDate
 * @return
 */
struct StockTransactionDataListResult FindStockTransactionData(char *beginDate, char *endDate) ;

#endif /* STOCK_TRANSACTION_DATA_HANDLER_H_ */
