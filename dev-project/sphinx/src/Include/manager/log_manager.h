#ifndef LOG_MANAGER_H_
#define LOG_MANAGER_H_

/**
 * 打印INFO级别的日志
 * @param message
 */
void Info(char message[]);

/**
 * 打印INFO级别的日志
 * @param key
 * @param value
 */
void InfoChar(char *key, char *value);

/**
 * 打印INFO级别的日志
 * @param key
 * @param value
 */
void InfoDouble(char *key, double value);

/**
 * 打印INFO级别的日志
 * @param key
 * @param value
 */
void InfoInt(char *key, int value);


#endif /* LOG_MANAGER_H_ */
