/*
 * matrix_handler.h
 *
 *  Created on: 2021年10月21日
 *      Author: lishen
 */

#ifndef MATRIX_HANDLER_H_
#define MATRIX_HANDLER_H_

/****************************************************************************************************/
/**
 * a和b两个二维矩阵的行数和列数
 */
extern int A_ROW;
extern int A_COLUMN;
extern int B_ROW;
extern int B_COLUMN;

/**
 * 矩阵相乘
 */
void Multiply(double (*a)[A_ROW], double (*b)[B_COLUMN], double c[A_ROW][B_COLUMN]);
/****************************************************************************************************/

/**
 * 矩阵相加
 * @param a
 * @param b
 * @param c
 */
void Add(double (*a)[A_COLUMN], double (*b)[B_COLUMN], double c[A_ROW][B_COLUMN]);
/****************************************************************************************************/

/**
 * 二维矩阵的列数
 */
extern int MATRIX_COLUMN;

/**
 * 创建矩阵
 * arrayRandomLimit：表示需要生成的随机数为0~arrayRandomLimit之间
 * row：矩阵的行
 * column：矩阵的列
 * (*weight)[]：返回值。二维数组（矩阵）
 */
void CreateMatrix(int arrayRandomLimit, int row, int column, double (*weight)[MATRIX_COLUMN]);
/****************************************************************************************************/

#endif /* MATRIX_HANDLER_H_ */
