#ifndef SPHINX_FILE_MANAGER_H
#define SPHINX_FILE_MANAGER_H

/**
 * 读取文件
 * @param path
 * @param r
 */
struct KnnDataset ReadFile(char *path, char *r);

#endif //SPHINX_FILE_MANAGER_H
