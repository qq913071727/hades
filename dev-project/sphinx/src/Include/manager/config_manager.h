/*
 * config_handler.h
 *
 *  Created on: 2021年10月13日
 *      Author: lishen
 */

#ifndef CONFIG_HANDLER_H_
#define CONFIG_HANDLER_H_

/**
 * 字符数组的长度
 */
extern int CONFIG_HANDLER_ARRAY_LENGTH;

/**
 * 从配置文件中获取数据库的配置
 * @return
 */
struct DatabaseInfo *GetDatabaseInfo();

/**
 * 从配置文件中获取反向传播网络算法的配置
 * @return
 */
struct BPInfo *GetBPInfo();

#endif /* CONFIG_HANDLER_H_ */
