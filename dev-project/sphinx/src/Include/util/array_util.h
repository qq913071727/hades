/*
 * vector_handler.h
 *
 *  Created on: 2021年10月27日
 *      Author: lishen
 */

#ifndef VECTOR_HANDLER_H_
#define VECTOR_HANDLER_H_

/**
 * 特征值数量
 */
extern int CHARACTERISTIC_VALUE_NUMBER;

/**
 * 将StockTransactionData结构体类型转换为二维数组
 * @param trainDataList
 * @param count
 * @return
 */
double **StockTransactionDataListToArray(struct StockTransactionData *trainDataList, int count);

/**
 * 行列互换
 * @param sourceArray
 * @return
 */
double **RowColumnInterchange(double (*dataArray)[KNN_FILE_COLUMN_SIZE]);

#endif /* VECTOR_HANDLER_H_ */
