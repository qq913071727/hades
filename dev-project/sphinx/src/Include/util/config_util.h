/*
 * config_util.h
 *
 *  Created on: 2021年10月12日
 *      Author: lishen
 */

#ifndef CONFIG_UTIL_H_
#define CONFIG_UTIL_H_

/**
 * 初始化环境，成功返回0，失败返回非0值
 */
int init(const char *filepath, void **handle);

/**
 * 根据KEY获取值，找到返回0，如果未找到返回非0值
 */
int getValue(void *handle, const char *key, char *value);

#endif /* CONFIG_UTIL_H_ */
