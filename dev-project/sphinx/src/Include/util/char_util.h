#ifndef CHAR_UTIL_H_
#define CHAR_UTIL_H_

/**
 * 连接字符串a和b，并生成新的字符串
 */
char *JoinChar(char *a, char *b);

/**
 * 生成绝对路径
 */
char *MakeAbsolutePath(char *b);

/**
 * 给定一个C-String的变量(非字面值常量)，去除首尾两端Blank的字符
 * 输入参数: char *src   --->  带有'\0'的C-String
 * 输出参数: char *src       --->  格式化后(去除首尾两端Blank的字符)
 */
void Trim(char *src);

/**
 * 除空白字符。空白字符指空格、水平制表、垂直制表、换页、回车和换行符。
 * 此方法返回的是已经修改的字串，而且字串的长度小于等于原字串的长度。
 */
char *Rtrim(char *str);

/**
 * 分隔字符串
 * @param src 要进行分割的字符串地址
 * @param separator 分隔符，设置为常量字符串,这里的分隔符可以为字符串
 * @param dest 分割结束后数据存储的地址，二维数组
 * @param num 返回的是字符串中存在的分隔符的数量
 */
void split(char *src, const char *separator, char **dest, int *num);

#endif
