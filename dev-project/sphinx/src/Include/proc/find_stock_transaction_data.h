/*
 * find_stock_transaction_data.h
 *
 *  Created on: 2021年10月12日
 *      Author: lishen
 */

#ifndef FIND_STOCK_TRANSACTION_DATA_H_
#define FIND_STOCK_TRANSACTION_DATA_H_

void GetStockTransactionData(struct DatabaseInfo *databaseInfo, char code[], int *count,
                             struct StockTransactionData *trainDataList, double (*trainLabelList)[1],
                             char *beginDate, char *endDate);

#endif /* FIND_STOCK_TRANSACTION_DATA_H_ */
