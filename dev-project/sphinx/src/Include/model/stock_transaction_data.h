/*
 * stock_transaction_data.h
 *
 *  Created on: 2021年10月12日
 *      Author: lishen
 */

#ifndef STOCK_TRANSACTION_DATA_H_
#define STOCK_TRANSACTION_DATA_H_

/**
 * 结构体StockTransactionData的数组的大小
 */
extern int STOCK_TRANSACTION_DATA_LIST_SIZE;

/**
 * stock_transaction_data表
 */
struct StockTransactionData {

    int id_;
    char *date_;
    char *code_;
    double openPrice;
    double closePrice;
    double highestPrice;
    double lowestPrice;
    double lastClosePrice;
    double changeAmount;
    double changeRange;
    double changeRangeExRight;
    double upDown;
    double turnoverRate;
    double volume;
    double turnover;
    double totalMarketValue;
    double circulationMarketValue;
    long transactionNumber;
    double ma5;
    double ma10;
    double ma20;
    double ma60;
    double ma120;
    double ma250;
    double ema12;
    double ema26;
    double dif;
    double dea;
    double rsv;
    double fiveDayVolatility;
    double tenDayVolatility;
    double twentyDayVolatility;
    double twoHundredFifty;
    double k;
    double d;
    double haOpenPrice;
    double haClosePrice;
    double haHighestPrice;
    double haLowestPrice;
};

#endif /* STOCK_TRANSACTION_DATA_H_ */
