//
// Created by Administrator on 2022/2/19.
//

#ifndef SPHINX_TEST_H
#define SPHINX_TEST_H

/**
 * 测试指向结构体的指针，使用字符数组和指向数组的指针
 */
void TestStructPointMember();

/**
 * 测试矩阵相乘
 */
void Multiply_Test();

#endif //SPHINX_TEST_H
