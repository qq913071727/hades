#include "../constant/knn_info.h"

#ifndef SPHINX_KNN_DATASET_H
#define SPHINX_KNN_DATASET_H

/**
 * knn算法的数据集，包括所有数据集、所有数据集标签、训练数据集、训练数据集标签、测试数据集、测试数据集标签
 */
struct KnnDataset {
    /**
     * 所有数据集
     */
    double (*allDataset)[];

    /**
     * 所有数据集标签
     */
    char **allDatasetLabel;

    /**
     * 训练数据集
     */
    double (*trainingDataset)[];

    /**
     * 训练数据集标签
     */
    char **trainingDatasetLabel;

    /**
     * 测试数据集
     */
    double (*testingDataset)[];

    /**
     * 测试数据集标签
     */
    char **testingDatasetLabel;
};

#endif
