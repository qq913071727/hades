#include "../model/stock_transaction_data.h"

#ifndef SPHINX_STOCK_TRANSACTION_DATA_LIST_RESULT_H
#define SPHINX_STOCK_TRANSACTION_DATA_LIST_RESULT_H

/**
 * 结构体StockTransactionData类型的数组的返回值
 */
struct StockTransactionDataListResult {
    /**
     * 训练数据。结构体StockTransactionData类型的数组
     */
    struct StockTransactionData *trainDataList;

    /**
     * 数组大小
     */
    int count;

    /**
     * 训练数据（标签）
     */
    double (*trainLabelList)[1];
};

#endif //SPHINX_STOCK_TRANSACTION_DATA_LIST_RESULT_H
