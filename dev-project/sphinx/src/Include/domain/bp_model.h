//
// Created by Administrator on 2022/2/8.
//

#ifndef SPHINX_BP_MODEL_H
#define SPHINX_BP_MODEL_H

/**
 * 反向传播网络算法的模型
 */
struct BPModel {
    /**
     * 参数列表（指针）
     */
    double (*parameter_array)[];
};

#endif //SPHINX_BP_MODEL_H
