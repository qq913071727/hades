/*
 * database_info.h
 *
 *  Created on: 2021年10月13日
 *      Author: lishen
 */

#ifndef DATABASE_INFO_H_
#define DATABASE_INFO_H_

/**
 * 数据库信息
 */
struct DatabaseInfo {
	/**
	 * 账号
	 */
	char *username;

	/**
	 * 密码
	 */
	char *password;

	/**
	 * 数据库名
	 */
	char *schema;

	/**
	 * 数据库连接字符串
	 */
	char *url;
};

#endif /* DATABASE_INFO_H_ */
