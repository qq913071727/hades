/*
 * bp_info.h
 *
 *  Created on: 2021年10月27日
 *      Author: lishen
 */

#ifndef BP_INFO_H_
#define BP_INFO_H_

/**
 * 反向传播网络算法的信息
 */
struct BPInfo {
	/**
	 * 反向传播网络算法--w1的行数
	 */
	int w1RowNumber;

	/**
	 * 反向传播网络算法--w1的列数
	 */
	int w1ColumnNumber;

	/**
	 * 反向传播网络算法--w2的行数
	 */
	int w2RowNumber;

	/**
	 * 反向传播网络算法--w2的列数
	 */
	int w2ColumnNumber;

	/**
	 * 反向传播网络算法--创建一维数组时每个元素的取值范围为0~limit
	 */
    double arrayRandomLimit;

    /**
     * 反向传播网络算法--初始学习率
     */
    double initLearningRate;

    /**
     * 反向传播网络算法--衰减系数
     */
    double decayRate;

    /**
     * 反向传播网络算法--衰减速度
     */
    double decaySteps;

    /**
     * 损失值。当损失值小于这个值时，就可以结束梯度下降算法的迭代了
     */
    double lossValue;

    /**
     * 正则化时（解决过拟合问题），模型复杂损失在总损失中的比例
     */
    double l2RegularizationRate;

    /**
     * 训练数据集开始时间
     */
    char *trainDataSetBeginDate;

    /**
     * 训练数据集结束时间
     */
    char *trainDataSetEndDate;

    /**
     * 测试数据集开始时间
     */
    char *testDataSetBeginDate;

    /**
     * 测试数据集结束时间
     */
    char *testDataSetEndDate;
};

#endif /* BP_INFO_H_ */
