#ifndef BIMON_KNN2_TEST_FUNCTION_H
#define BIMON_KNN2_TEST_FUNCTION_H

/**
 * 训练数据集的行数、列数
 */
extern int TRAINING_DATASET_ROW_SIZE;
extern int TRAINING_DATASET_COLUMN_SIZE;

/**
 * 测试数据集的行数、列数
 */
extern int TESTING_DATASET_ROW_SIZE;
extern int TESTING_DATASET_COLUMN_SIZE;

void Classify(double normalTrainingDataset[][TRAINING_DATASET_COLUMN_SIZE],
              double normalTrainingDatasetLabel[TESTING_DATASET_COLUMN_SIZE],
              double normalTestingDataset[][TESTING_DATASET_COLUMN_SIZE],
              double normalTestingDatasetLabel[TESTING_DATASET_COLUMN_SIZE], struct Point *p[TESTING_DATASET_ROW_SIZE][TRAINING_DATASET_ROW_SIZE]);

#endif
