#ifndef SPHINX_KNN_INFO_H
#define SPHINX_KNN_INFO_H

/**
 * 文件有多少列
 */
extern int KNN_FILE_COLUMN_SIZE;

/**
 * 文件有多少行
 */
extern int KNN_FILE_ROW_SIZE;

/**
 * 单元格有多大
 */
extern int KNN_FILE_CELL_SIZE;

/**
 * 数据文件的路径
 */
extern char *KNN_DATA_FILE_AT_HOME;
extern char *KNN_DATA_FILE_AT_COMPANY;

/**
 * 训练数据和测试数据比例
 */
extern double TRAINING_DATASET_PERCENTAGE;
extern double TESTING_DATASET_PERCENTAGE;

#endif
