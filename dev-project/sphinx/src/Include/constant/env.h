#ifndef SPHINX_ENV_H
#define SPHINX_ENV_H

/**
 * 环境变量USERDOMAIN
 */
extern char *USERDOMAIN;
extern char *USERDOMAIN_AT_HOME;
extern char *USERDOMAIN_AT_COMPANY;

#endif
