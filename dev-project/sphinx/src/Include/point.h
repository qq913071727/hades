#ifndef BIMON_POINT_H
#define BIMON_POINT_H

struct Point{
    int testingDatasetIndex;
    int trainingDatasetIndex;
    int distance;
};

#endif //BIMON_POINT_H
