#include "../constant/knn_info.h"

#ifndef SPHINX_KNN_HANDLER_H
#define SPHINX_KNN_HANDLER_H

/**
 * 返回数据文件的路径
 * @return
 */
char *GetDataFilePathByEnvUSERDOMAIN();

/**
 * 归一化。公式：newValue = (oldValue - min) / (max - min)
 * @param dataArray
 * @return
 */
char **Normalize(double (*dataArray)[KNN_FILE_COLUMN_SIZE]);

/**
 * 返回训练数据集、训练数据集标签、测试数据集、测试数据集标签
 * @param allDatasetLabel
 * @param normalizedArray
 * @return
 */
struct KnnDataset DifferentiateTrainingDatasetAndTestingDataset(double (*allDatasetLabel)[KNN_FILE_COLUMN_SIZE],
                                                                double (*normalizedArray)[KNN_FILE_COLUMN_SIZE]);

#endif
