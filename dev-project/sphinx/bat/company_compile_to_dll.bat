@echo off

rem utf-8
chcp 65001

echo 将c源程序编译为dll

echo on

gcc -shared -o C:\github-repository\hades\dev-project\bimon\output\libbimon.dll ^
 C:\github-repository\hades\dev-project\bimon\src\Function\*.c ^
 C:\github-repository\hades\dev-project\bimon\src\Include\*.h ^
 C:\github-repository\hades\dev-project\bimon\src\Test\Function\*.c ^
 C:\github-repository\hades\dev-project\bimon\src\Struct\*.c -fno-builtin

