package dto

// 分页显示的DTO
type PageDto struct {
	Data      interface{}
	Total     int
	PageCount int
}
