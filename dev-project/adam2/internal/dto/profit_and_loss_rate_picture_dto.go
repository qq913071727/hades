package dto

// 收益率图表需要的数据
type ProfitAndLossRatePictureDto struct {
	ProfitAndLossRateArray []float64
	DateArray              []string
}
