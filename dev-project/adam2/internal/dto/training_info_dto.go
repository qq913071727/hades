package dto

type TrainingInfoDto struct {
	ID         int
	Name       string
	Market     string
	Code       string
	BeginDate  string
	EndDate    string
	Status     string
	CreateTime string
	UpdateTime string
}

type TrainingInfoDtoArray []*TrainingInfoDto
