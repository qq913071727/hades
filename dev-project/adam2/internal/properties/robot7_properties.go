package properties

// robot7的配置文件的结构体
type Robot7Properties struct {
	BeginDate                  string
	EndDate                    string
	NotDecrementDateNumber     int
	ClosePriceBegin            float64
	ClosePriceEnd              float64
	UpPercentageDateNunber     int
	XrDateNumber               int
	BiasThresholdTop           float64
	BiasThresholdBottom        float64
	MacdGoldDateNumber         int
	TransactionStrategyInBull  int
	TransactionStrategyInShort int
	StockSortStrategyInBull    int
	StockSortStrategyInShort   int
	//EnableDynamicHoldStockNumberLimit bool
	//EnableAverageClosePriceUpBullShortLine bool
	EnableTransactionHoldStockNumberLimit int
	MaxHoldStockNumberInBull              int
	MaxHoldStockNumberInShort             int
	MandatoryStopLoss                     bool
	MandatoryStopLossRate                 float64
	EnableAddPosition                     bool
	AddOnePositionPerProfitRate           float64
	MaxAddPositionNumber                  int
	InitAssets                            int
	ModelId                               int
	StampDutyRate                         float64
	RegistrationFeeRate                   float64
	CommissionRate                        float64
	PrintMaximumDrawdownModelId           int
}

var Robot7Properties_ = &Robot7Properties{}
