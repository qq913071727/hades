package properties

// 绘图的配置文件的结构体
type ChartProperties struct {
	/***************** 根据表his_robot_account_log中每个账号的总资金，生成折线图 **************/
	// 图片的存储位置
	HisRobotAccountLogLineChartPath string
	// 图片文件的后缀
	HisRobotAccountLogLineChartSuffix string
	// model表中的id字段（chart使用）
	HisRobotAccountLogLineChartModelId int
	// 初始化资产
	HisRobotAccountLogLineChartInitAssets int

	/***************** 创建平均收盘价和各个平均均线的折线图 **************/
	// 开始时间
	AverageClosePriceAndMABeginDate string
	// 结束时间
	AverageClosePriceAndMAEndDate string
	// 图片的存储位置
	AverageClosePriceAndMALineChartPath string
	// 图片文件的后缀
	AverageClosePriceAndMAgLineChartSuffix string

	/***************** 创建平均收盘价和牛熊线（bias决定）的折线图 **************/
	// 开始时间
	AverageClosePriceAndMABullShortLineByBiasBeginDate string
	// 结束时间
	AverageClosePriceAndMABullShortLineByBiasEndDate string
	// 图片的存储位置
	AverageClosePriceAndMABullShortLineByBiasLineChartPath string
	// 图片文件的后缀
	AverageClosePriceAndMABullShortLineByBiasLineChartSuffix string
	// bias上限
	AverageClosePriceAndMABullShortLineByBiasTopLimit float64
	// bias下限
	AverageClosePriceAndMABullShortLineByBiasBottomLimit float64

	/***************** 创建收益率(his_robot_transact_record)和相关系数关系的散点图 **************/
	// 开始时间
	ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterBeginDateByHisRobotTransactRecord string
	// 结束时间
	ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterEndDateByHisRobotTransactRecord string
	// 图片的存储位置
	ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterPathByHisRobotTransactRecord string
	// 图片文件的后缀
	ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterSuffixByHisRobotTransactRecord string
	// model表中的id字段（chart使用）
	ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterModelIdByHisRobotTransactRecord int

	/***************** 创建收益率(mdl_etf_bull_short_line_up)和相关系数关系的散点图 **************/
	// 开始时间
	ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterBeginDateByMdlEtfBullShortLineUp string
	// 结束时间
	ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterEndDateByMdlEtfBullShortLineUp string
	// 图片的存储位置
	ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterPathByMdlEtfBullShortLineUp string
	// 图片文件的后缀
	ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterSuffixByMdlEtfBullShortLineUp string
}

var ChartProperties_ = &ChartProperties{}
