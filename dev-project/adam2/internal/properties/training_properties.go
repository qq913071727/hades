package properties

// 训练配置
type TrainingProperties struct {
	Port         string
	EncryptKey   string
	IV           string
	DateInterval int
}

var TrainingProperties_ = &TrainingProperties{}
