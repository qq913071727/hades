package properties

// 调用Python的配置文件的结构体
type PythonProperties struct {
	ManagePath  string
	PackageName string
	// 期货
	WatchCommodityFutureMinuteDataStraightLineIncreaseDecreaseEnable bool
	WatchCommodityFutureMinuteDataUpOrDownPriceEnable                bool
	WatchCommodityFutureMinuteDataMethodName                         string
	WatchCommodityFutureMinuteDataEnableMessageBox                   bool
	WatchCommodityFutureMinuteDataEnableSoundAudio                   bool
	WatchCommodityFutureMinuteDataEnableSaveInDatabase               bool
	WatchCommodityFutureMinuteDataRiseOrFallThreshold                float64
	WatchCommodityFutureMinuteDataAudioFilePath                      string
	// 股票
	WatchStockTransactionMinuteDataStraightLineIncreaseDecreaseEnable bool
	WatchStockTransactionMinuteDataUpOrDownPriceEnable                bool
	WatchStockTransactionMinuteDataMethodName                         string
	WatchStockTransactionMinuteDataEnableMessageBox                   bool
	WatchStockTransactionMinuteDataEnableSoundAudio                   bool
	WatchStockTransactionMinuteDataEnableSaveInDatabase               bool
	WatchStockTransactionMinuteDataRiseOrFallThreshold                float64
	WatchStockTransactionMinuteDataAudioFilePath                      string
}

var PythonProperties_ = &PythonProperties{}
