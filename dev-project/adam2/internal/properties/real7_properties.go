package properties

// real7的配置文件的结构体
type Real7Properties struct {
	TransactionDate        string
	Debug                  bool
	ClosePriceStart        float64
	ClosePriceEnd          float64
	XrDateNumber           int
	Url                    string
	BiasThresholdTop       float64
	BiasThresholdBottom    float64
	NotDecrementDateNumber int
	ScheduleBeginTime      string
	ScheduleEndTime        string
	MaxHoldStockNumber     int
	EmailFrom              string
	EmailTo                string
	EmailSubject           string
	EmailAddress           string
	EmailUsername          string
	EmailPassword          string
	EmailHost              string
}

var Real7Properties_ = &Real7Properties{}
