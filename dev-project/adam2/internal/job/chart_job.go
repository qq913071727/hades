package job

import (
	"adam2/internal/domain"
	"anubis-framework/pkg/io"
)

// 开始生成图表
func CreateChart() {
	io.Infoln("开始生成图表")

	/****************** 根据表his_robot_account_log中每个账号的总资金，生成折线图 ******************/
	//domain.BaseJob_.HisRobotAccountLogServiceImpl_.CreateHisRobotAccountLogLineChart()

	/********************* 创建平均收盘价和各个平均均线的折线图（经过平滑处理后的） *********************/
	//domain.BaseJob_.StockTransactionDataAllServiceImpl_.CreateAverageClosePriceAndMALineChart_smooth()

	/**************************** 创建平均收盘价和牛熊线（bias决定）的折线图 ****************************/
	domain.BaseJob_.StockTransactionDataAllServiceImpl_.CreateAverageClosePriceAndMABullShortLineByBiasLineChart()

	/*********************** 创建收益率(his_robot_transact_record)和相关系数关系的散点图 ***********************/
	//domain.BaseJob_.HisRobotTransactionRecordServiceImpl_.CreateProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterByHisRobotTransactRecord()

	/******************** 创建收益率(mdl_etf_bull_short_line_up)和相关系数关系的散点图 ********************/
	//domain.BaseJob_.MdlEtfBullShortLineUpServiceImpl_.CreateProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterByMdlEtfBullShortLineUp()

	/*************************************** 使操作系统睡眠 *************************************/
	//domain.BaseJob_.ProjectServiceImpl_.MakeOperationSystemSleep()

	io.Infoln("生成图表完成")
}
