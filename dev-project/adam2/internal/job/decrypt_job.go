package job

import (
	"adam2/internal/properties"
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/util"
)

// 解密
func Decrypt() {
	io.Infoln("解密开始")

	var result, err = util.Decrypt("mrYFIeTencxrSEzQOg31tA==", properties.TrainingProperties_.EncryptKey, properties.TrainingProperties_.IV)
	if err == nil {
		io.Infoln(result)
	}

	io.Infoln("解密完成")
}
