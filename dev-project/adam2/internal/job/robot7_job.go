package job

import (
	"adam2/internal/domain"
	"anubis-framework/pkg/io"
)

// 开始robot7测试
func StartRobot7() {
	io.Infoln("robot7测试开始")

	/*************** 执行某段时间内的股票买卖交易（根据bias来确定牛熊线的算法） ****************/
	domain.BaseJob_.Robot7StockTransactRecordServiceImpl_.DoBuyAndSellByBeginDateAndEndDate_Bias()

	/******************************* 将测试数据导入到历史数据表中 *****************************/
	//domain.BaseJob_.ModelServiceImpl_.ImportIntoHistoryDataTableFromRobot7()

	/******************************* 打印最大回撤 *****************************/
	//domain.BaseJob_.HisRobotAccountLogServiceImpl_.PrintMaximumDrawdown()

	/******************************* 使操作系统睡眠 *****************************/
	//domain.BaseJob_.ProjectServiceImpl_.MakeOperationSystemSleep()

	io.Infoln("robot7测试完成")
}
