package job

import (
	"adam2/internal/domain"
	"anubis-framework/pkg/io"
)

// 开始real7交易
func StartReal7() {
	io.Infoln("real7交易开始")

	/********************************** 预先计算股票的买卖条件 ***********************************/
	domain.BaseJob_.Real7TransactionConditionServiceImpl_.PreCalculateTransactionCondition()

	/***************************** 实时地判断买卖条件，给出交易建议 ******************************/
	domain.BaseJob_.Real7StockTransactionRecordServiceImpl_.RealTimeJudgeConditionAndGiveSuggestion()

	/******************************* 使操作系统睡眠 *****************************/
	//domain.BaseJob_.ProjectServiceImpl_.MakeOperationSystemSleep()

	io.Infoln("real7交易完成")
}
