package job

import (
	"adam2/internal/domain"
	"adam2/internal/properties"
	"anubis-framework/pkg/io"
	"fmt"
)

// 训练
func StartTraining() {
	io.Infoln("训练开始")

	// 启动服务器
	io.Infoln("启动服务器，端口%s", properties.TrainingProperties_.Port)
	domain.Router_.Engine_.Run(fmt.Sprintf(":%s", properties.TrainingProperties_.Port))

	io.Infoln("训练完成")
}
