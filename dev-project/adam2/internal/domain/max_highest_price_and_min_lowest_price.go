package domain

type MaxHighestPriceAndMinLowestPrice struct {
	MaxHighestPrice float64 `gorm:"column:MAX_HIGHEST_PRICE"`
	MinLowestPrice  float64 `gorm:"column:MIN_LOWEST_PRICE"`
}

type MaxHighestPriceAndMinLowestPriceArray []*MaxHighestPriceAndMinLowestPrice
