package domain

type LastWeekAverageK struct {
	LastWeekAverageK    float64 `json:"LAST_WEEK_AVERAGE_K"`
	LastTwoWeekAverageK float64 `json:"LAST_TWO_WEEK_AVERAGE_K"`
}
