package domain

import (
	"anubis-framework/pkg/manager"
)

type Manager struct {
	AudioManager_ manager.AudioManager
}

var Manager_ = &Manager{}
