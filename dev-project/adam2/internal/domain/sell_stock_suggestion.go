package domain

type SellStockSuggestion struct {
	StockCode  string
	Name       string
	FilterType int
	Direction  int
}

type SellStockSuggestionArray []*SellStockSuggestion
