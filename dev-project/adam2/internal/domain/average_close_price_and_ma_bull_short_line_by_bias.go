package domain

type AverageClosePriceAndMaBullShortLineByBias struct {
	Date              string  `gorm:"column:DATE_"`
	AverageClosePrice float64 `gorm:"column:AVERAGE_CLOSE_PRICE"`
	AverageMa5        float64 `gorm:"column:AVERAGE_MA5"`
	AverageMa10       float64 `gorm:"column:AVERAGE_MA10"`
	AverageMa20       float64 `gorm:"column:AVERAGE_MA20"`
	AverageMa60       float64 `gorm:"column:AVERAGE_MA60"`
	AverageMa120      float64 `gorm:"column:AVERAGE_MA120"`
	AverageMa250      float64 `gorm:"column:AVERAGE_MA250"`
	AverageBias5      float64 `gorm:"column:AVERAGE_BIAS5"`
	AverageBias10     float64 `gorm:"column:AVERAGE_BIAS10"`
	AverageBias20     float64 `gorm:"column:AVERAGE_BIAS20"`
	AverageBias60     float64 `gorm:"column:AVERAGE_BIAS60"`
	AverageBias120    float64 `gorm:"column:AVERAGE_BIAS120"`
	AverageBias250    float64 `gorm:"column:AVERAGE_BIAS250"`
}

type AverageClosePriceAndMaBullShortLineByBiasArray []*AverageClosePriceAndMaBullShortLineByBias
