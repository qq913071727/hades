package domain

import (
	"github.com/go-echarts/go-echarts/v2/opts"
)

type HisRobotAccountLogLineChart struct {
	RobotName               string
	HisRobotAccountLogArray []opts.LineData
}

type HisRobotAccountLogLineChartArray []HisRobotAccountLogLineChart
