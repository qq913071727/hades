package domain

type StockTransactionDataAllAverage struct {
	Date_      string  `gorm:"column:DATE_"`
	Dif        float64 `gorm:"column:DIF"`
	Dea        float64 `gorm:"column:DEA"`
	K          float64 `gorm:"column:K"`
	D          float64 `gorm:"column:D"`
	ClosePrice float64 `gorm:"column:CLOSE_PRICE"`
	Ma5        float64 `gorm:"column:MA5"`
}
