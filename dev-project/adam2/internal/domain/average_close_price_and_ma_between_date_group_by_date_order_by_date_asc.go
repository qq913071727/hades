package domain

type AverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc struct {
	Date              string  `gorm:"column:DATE_"`
	AverageClosePrice float64 `gorm:"column:AVERAGE_CLOSE_PRICE"`
	AverageMa5        float64 `gorm:"column:AVERAGE_MA5"`
	AverageMa10       float64 `gorm:"column:AVERAGE_MA10"`
	AverageMa20       float64 `gorm:"column:AVERAGE_MA20"`
	AverageMa60       float64 `gorm:"column:AVERAGE_MA60"`
	AverageMa120      float64 `gorm:"column:AVERAGE_MA120"`
	AverageMa250      float64 `gorm:"column:AVERAGE_MA250"`
}

type AverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray []*AverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc
