package domain

import "time"

type MaxDataAndMinData struct {
	MaxDate time.Time `gorm:"column:MAX_DATE"`
	MinDate time.Time `gorm:"column:MIN_DATE"`
}

type MaxDataAndMinDataArray []*MaxDataAndMinData
