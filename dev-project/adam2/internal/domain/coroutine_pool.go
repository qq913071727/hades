package domain

import (
	"github.com/panjf2000/ants/v2"
	"sync"
)

// 协程池
type CoroutinePool struct {
	WaitGroup_ sync.WaitGroup
	Pool_      *ants.Pool
}

var CoroutinePool_ = &CoroutinePool{}
