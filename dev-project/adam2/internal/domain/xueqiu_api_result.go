package domain

type XueQiuApiResult struct {
	DataArray        []*Data `json:"data"`
	ErrorCode        int     `json:"error_code"`
	ErrorDescription string  `json:"error_description"`
}

type Data struct {
	StockCode          string  `json:"symbol"`
	CurrentPrice       float64 `json:"current"`
	Percent            float64 `json:"percent"`
	Change             float64 `json:"chg"`
	Timestamp          int     `json:"timestamp"`
	Volume             int     `json:"volume"`
	Amount             float64 `json:"amount"`
	MarketCapital      float64 `json:"market_capital"`
	FloatMarketCapital float64 `json:"float_market_capital"`
	TurnoverRate       float64 `json:"turnover_rate"`
	Amplitude          float64 `json:"amplitude"`
	OpenPrice          float64 `json:"open"`
	LastClosePrice     float64 `json:"last_close"`
	HighestPrice       float64 `json:"high"`
	LowestPrice        float64 `json:"low"`
	AverageClosePrice  float64 `json:"avg_price"`
	TradeVolume        int     `json:"trade_volume"`
	Side               int     `json:"side"`
	IsTrade            bool    `json:"is_trade"`
	Level              int     `json:"level"`
	TradeSession       string  `json:"trade_session"`
	TradeType          int     `json:"trade_type"`
	CurrentYearPercent float64 `json:"current_year_percent"`
	TradeUniqueId      string  `json:"trade_unique_id"`
	Type               int     `json:"type"`
	BidApplSeqNum      int     `json:"bid_appl_seq_num"`
	OfferApplSeqNum    int     `json:"offer_appl_seq_num"`
	VolumeExt          int     `json:"volume_ext"`
	TradedAmountExt    float64 `json:"traded_amount_ext"`
	TradeTypeV2        int     `json:"trade_type_v2"`
	YieldToMaturity    int     `json:"yield_to_maturity"`
}
