package domain

type BuyStockSuggestion struct {
	StockCode  string
	Name       string
	FilterType int
	Direction  int
	BuyPrice   float64
	BuyAmount  int
}

type BuyStockSuggestionArray []*BuyStockSuggestion
