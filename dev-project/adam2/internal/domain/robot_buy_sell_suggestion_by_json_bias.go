package domain

import (
	"adam2/internal/model"
)

type RobotBuySellSuggestionByJson_bias struct {

	// 做多卖出建议列表
	BullSellSuggestionArray []*model.Robot7StockTransactRecord

	// 做多买入建议列表
	BullBuySuggestionArray []*model.Robot7StockTransactRecord
}
