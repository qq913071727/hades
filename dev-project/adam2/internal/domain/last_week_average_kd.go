package domain

type LastWeekAverageKD struct {
	LastWeekAverageK float64 `json:"LAST_WEEK_AVERAGE_K"`
	LastWeekAverageD float64 `json:"LAST_WEEK_AVERAGE_D"`
}
