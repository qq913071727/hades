package domain

import (
	"adam2/internal/service"
)

type BaseJob struct {
	CommodityFutureInfoService_             service.CommodityFutureInfoService
	CommodityFutureMinuteDataService_       service.CommodityFutureMinuteDataService
	Robot7StockTransactRecordServiceImpl_   service.Robot7StockTransactRecordService
	ModelServiceImpl_                       service.ModelService
	MdlEtfBullShortLineUpServiceImpl_       service.MdlEtfBullShortLineUpService
	HisRobotAccountLogServiceImpl_          service.HisRobotAccountLogService
	HisRobotTransactionRecordServiceImpl_   service.HisRobotTransactionRecordService
	StockTransactionDataAllServiceImpl_     service.StockTransactionDataAllService
	ProjectServiceImpl_                     service.ProjectService
	Real7StockTransactionRecordServiceImpl_ service.Real7StockTransactionRecordService
	Real7TransactionConditionServiceImpl_   service.Real7TransactionConditionService
	TrainingInfoServiceImpl_                service.TrainingInfoService
	TrainingTransactionDataService_         service.TrainingTransactionDataService
}

var BaseJob_ = &BaseJob{}
