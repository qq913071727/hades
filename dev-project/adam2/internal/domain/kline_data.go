package domain

// K线数据
type KLineData struct {
	// 数据
	Lines [][]interface{}
	// 代码
	Code string
	// 名称
	Name string
}
