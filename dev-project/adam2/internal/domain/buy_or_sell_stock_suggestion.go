package domain

type BuyOrSellStockSuggestion struct {
	BuyStockSuggestionArray  BuyStockSuggestionArray
	SellStockSuggestionArray SellStockSuggestionArray
}

type BuyOrSellStockSuggestionArray []*BuyOrSellStockSuggestionArray
