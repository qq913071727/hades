package domain

import "gorm.io/gorm"

type DataSource struct {
	GormDb *gorm.DB
}

var DataSource_ = &DataSource{}
