package domain

import "time"

type MaximumDrawdown struct {
	MaximumDrawdown float64   `json:"MAXIMUM_DRAWDOWN"`
	Date1           time.Time `json:"DATE1"`
	Date2           time.Time `json:"DATE2"`
	RobotName       string    `json:"ROBOT_NAME"`
	Total_Assets1   float64   `json:"TOTAL_ASSETS1"`
	Total_Assets2   float64   `json:"TOTAL_ASSETS2"`
}

type MaximumDrawdownArray []*MaximumDrawdown
