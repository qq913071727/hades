package domain

import "time"

type ProfitAndLossRateAndCorrelation250WithAvgClosePrice struct {
	StockCode                       string    `gorm:"column:STOCK_CODE"`
	BuyDate                         time.Time `gorm:"column:BUY_DATE"`
	ProfitAndLossRate               float64   `gorm:"column:PROFIT_AND_LOSS_RATE"`
	Correlation250WithAvgClosePrice float64   `gorm:"column:CORRELATION250_WITH_AVG_C_P"`
}

type ProfitAndLossRateAndCorrelation250WithAvgClosePriceArray []*ProfitAndLossRateAndCorrelation250WithAvgClosePrice
