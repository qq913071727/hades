package domain

type LastWeekAverageClosePriceAndLastWeekAverageMA5 struct {
	LastWeekAverageClosePrice float64 `gorm:"column:LAST_WEEK_AVERAGE_CLOSE_PRICE"`
	LastWeekAverageMa5        float64 `gorm:"column:LAST_WEEK_AVERAGE_MA5"`
}

type LastWeekAverageClosePriceAndLastWeekAverageMA5Array []*LastWeekAverageClosePriceAndLastWeekAverageMA5

