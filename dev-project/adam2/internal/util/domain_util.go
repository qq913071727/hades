package util

import (
	"adam2/internal/domain"
	"adam2/internal/model"
)

// 将domain.XueQiuApiResult转换为model.StockTransactionDataAll类型，注意如果无法转换则会返回nil
func XueQiuApiResultToStockTransactionDataAll(xueQiuApiResult domain.XueQiuApiResult) *model.StockTransactionDataAll {
	if len(xueQiuApiResult.DataArray) > 0 {
		var stockTransactionDataAll model.StockTransactionDataAll = model.StockTransactionDataAll{}
		var data *domain.Data = xueQiuApiResult.DataArray[0]
		stockTransactionDataAll.ClosePrice = data.CurrentPrice
		stockTransactionDataAll.Code = data.StockCode[2:]
		stockTransactionDataAll.ChangeRange = data.Percent
		stockTransactionDataAll.ChangeAmount = data.Change
		stockTransactionDataAll.Volume = float64(data.Volume)
		stockTransactionDataAll.Turnover = data.Amount
		stockTransactionDataAll.TotalMarketValue = data.MarketCapital
		stockTransactionDataAll.TotalMarketValue = data.FloatMarketCapital
		stockTransactionDataAll.TurnoverRate = data.TurnoverRate
		//stockTransactionDataAll = data.Amplitude
		stockTransactionDataAll.OpenPrice = data.OpenPrice
		stockTransactionDataAll.LastClosePrice = data.LastClosePrice
		stockTransactionDataAll.HighestPrice = data.HighestPrice
		stockTransactionDataAll.LowestPrice = data.LowestPrice
		//stockTransactionDataAll = data.AverageClosePrice
		return &stockTransactionDataAll
	} else {
		return nil
	}
}
