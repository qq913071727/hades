package util

import "adam2/internal/model"

// 返回StockTransactionDataAllArray中最大和最小的最高价
func MaxMinHighestPriceInStockTransactionDataAllArray(stockTransactionDataArray model.StockTransactionDataAllArray) (float64, float64) {
	//var maxArrayIndex, minArrayIndex int
	maxNum := stockTransactionDataArray[0].HighestPrice   //假设最大值是第一个
	minNum := stockTransactionDataArray[0].HighestPrice   //假设最小值也是第一个
	for i := 1; i < len(stockTransactionDataArray); i++ { //循环  除第一个值外其它值
		if stockTransactionDataArray[i].HighestPrice > maxNum { //发现更大的值
			maxNum = stockTransactionDataArray[i].HighestPrice
			//maxArrayIndex = i
		} else if stockTransactionDataArray[i].HighestPrice < minNum { //发现更小值
			minNum = stockTransactionDataArray[i].HighestPrice
			//minArrayIndex = i
		}
	}
	return maxNum, minNum
}

// 返回StockTransactionDataAllArray中最大和最小的最低价
func MaxMinLowestPriceInStockTransactionDataAllArray(stockTransactionDataArray model.StockTransactionDataAllArray) (float64, float64) {
	//var maxArrayIndex, minArrayIndex int
	maxNum := stockTransactionDataArray[0].LowestPrice    //假设最大值是第一个
	minNum := stockTransactionDataArray[0].LowestPrice    //假设最小值也是第一个
	for i := 1; i < len(stockTransactionDataArray); i++ { //循环  除第一个值外其它值
		if stockTransactionDataArray[i].LowestPrice > maxNum { //发现更大的值
			maxNum = stockTransactionDataArray[i].LowestPrice
			//maxArrayIndex = i
		} else if stockTransactionDataArray[i].LowestPrice < minNum { //发现更小值
			minNum = stockTransactionDataArray[i].LowestPrice
			//minArrayIndex = i
		}
	}
	return maxNum, minNum
}

// 返回CommodityFutureDateDataArray中最大和最小的最高价
func MaxMinHighestPriceInCommodityFutureDateDataArray(commodityFutureDateDataArray model.CommodityFutureDateDataArray) (float64, float64) {
	//var maxArrayIndex, minArrayIndex int
	maxNum := commodityFutureDateDataArray[0].HighestPrice   //假设最大值是第一个
	minNum := commodityFutureDateDataArray[0].HighestPrice   //假设最小值也是第一个
	for i := 1; i < len(commodityFutureDateDataArray); i++ { //循环  除第一个值外其它值
		if commodityFutureDateDataArray[i].HighestPrice > maxNum { //发现更大的值
			maxNum = commodityFutureDateDataArray[i].HighestPrice
			//maxArrayIndex = i
		} else if commodityFutureDateDataArray[i].HighestPrice < minNum { //发现更小值
			minNum = commodityFutureDateDataArray[i].HighestPrice
			//minArrayIndex = i
		}
	}
	return maxNum, minNum
}

// 返回CommodityFutureDateDataArray中最大和最小的最低价
func MaxMinLowestPriceInCommodityFutureDateDataArray(commodityFutureDateDataArray model.CommodityFutureDateDataArray) (float64, float64) {
	//var maxArrayIndex, minArrayIndex int
	maxNum := commodityFutureDateDataArray[0].LowestPrice    //假设最大值是第一个
	minNum := commodityFutureDateDataArray[0].LowestPrice    //假设最小值也是第一个
	for i := 1; i < len(commodityFutureDateDataArray); i++ { //循环  除第一个值外其它值
		if commodityFutureDateDataArray[i].LowestPrice > maxNum { //发现更大的值
			maxNum = commodityFutureDateDataArray[i].LowestPrice
			//maxArrayIndex = i
		} else if commodityFutureDateDataArray[i].LowestPrice < minNum { //发现更小值
			minNum = commodityFutureDateDataArray[i].LowestPrice
			//minArrayIndex = i
		}
	}
	return maxNum, minNum
}

// 返回StockWeekArray中最大的最高价
func MaxHighestPriceInStockWeekArrayWithNDate(stockWeekArray model.StockWeekArray, n int) float64 {
	maxNum := stockWeekArray[len(stockWeekArray)-1].HighestPrice         //假设最大值是第一个
	for i := len(stockWeekArray) - 1; i > len(stockWeekArray)-1-n; i-- { //循环  除第一个值外其它值
		if stockWeekArray[i].HighestPrice > maxNum { //发现更大的值
			maxNum = stockWeekArray[i].HighestPrice
		}
	}
	return maxNum
}

// 返回StockWeekArray中最小的最低价
func MinLowestPriceInStockWeekArrayWithNDate(stockWeekArray model.StockWeekArray, n int) float64 {
	minNum := stockWeekArray[len(stockWeekArray)-1].LowestPrice          //假设最小值是第一个
	for i := len(stockWeekArray) - 1; i > len(stockWeekArray)-1-n; i-- { //循环  除第一个值外其它值
		if stockWeekArray[i].LowestPrice < minNum { //发现更小的值
			minNum = stockWeekArray[i].LowestPrice
		}
	}
	return minNum
}

// 返回CommodityFutureWeekDataArray中最大的最高价
func MaxHighestPriceInCommodityFutureWeekDataArrayWithNDate(commodityFutureWeekDataArray model.CommodityFutureWeekDataArray, n int) float64 {
	maxNum := commodityFutureWeekDataArray[len(commodityFutureWeekDataArray)-1].HighestPrice         //假设最大值是第一个
	for i := len(commodityFutureWeekDataArray) - 1; i > len(commodityFutureWeekDataArray)-1-n; i-- { //循环  除第一个值外其它值
		if commodityFutureWeekDataArray[i].HighestPrice > maxNum { //发现更大的值
			maxNum = commodityFutureWeekDataArray[i].HighestPrice
		}
	}
	return maxNum
}

// 返回CommodityFutureWeekDataArray中最小的最低价
func MinLowestPriceInCommodityFutureWeekDataArrayWithNDate(commodityFutureWeekDataArray model.CommodityFutureWeekDataArray, n int) float64 {
	minNum := commodityFutureWeekDataArray[len(commodityFutureWeekDataArray)-1].LowestPrice          //假设最小值是第一个
	for i := len(commodityFutureWeekDataArray) - 1; i > len(commodityFutureWeekDataArray)-1-n; i-- { //循环  除第一个值外其它值
		if commodityFutureWeekDataArray[i].HighestPrice > minNum { //发现更小的值
			minNum = commodityFutureWeekDataArray[i].LowestPrice
		}
	}
	return minNum
}
