package util

import (
	"adam2/internal/properties"
	"fmt"
	"strconv"
)

// 计算印花税，只取小数点后两位
func CalculateStampDuty(price float64, amount int) float64 {
	fee := price * float64(amount) * properties.Robot7Properties_.StampDutyRate
	formatted := fmt.Sprintf("%.2f", fee)
	result, _ := strconv.ParseFloat(formatted, 64)
	return result
}

// 计算过户费，只取小数点后两位
func CalculateRegistrationFee(stockCode string, amount int) float64 {
	// 判断这只股票是否是沪市的股票
	var firstThreeString string = stockCode[:3]
	if firstThreeString == "600" || firstThreeString == "601" || firstThreeString == "688" {
		fee := float64(amount) * properties.Robot7Properties_.RegistrationFeeRate
		if fee < 1 {
			return 1
		} else {
			formatted := fmt.Sprintf("%.2f", fee)
			result, _ := strconv.ParseFloat(formatted, 64)
			return result
		}
	} else {
		return 0
	}
}

// 计算佣金，只取小数点后两位
func CalculateCommission(price float64, amount int) float64 {
	fee := price * float64(amount) * properties.Robot7Properties_.CommissionRate
	if fee < 5 {
		return 5
	} else {
		formatted := fmt.Sprintf("%.2f", fee)
		result, _ := strconv.ParseFloat(formatted, 64)
		return result
	}
}

// 计算买股票时的全部费用，包括：买股票时的金额、过户费和佣金
func CalculateBuyStockFee(stockCode string, price float64, amount int) float64 {
	// 计算过户费
	var registrationFee float64 = CalculateRegistrationFee(stockCode, amount)

	// 计算佣金
	var commission float64 = CalculateCommission(price, amount)

	var result float64 = price*float64(amount) + registrationFee + commission
	return result
}

// 计算卖股票时的全部费用，包括：卖股票时的金额、印花税、过户费和佣金
func CalculateSellStockProfit(stockCode string, price float64, amount int) float64 {
	// 计算印花税
	var stampDuty float64 = CalculateStampDuty(price, amount)

	// 计算过户费
	var registrationFee float64 = CalculateRegistrationFee(stockCode, amount)

	// 计算佣金
	var commission float64 = CalculateCommission(price, amount)

	var result float64 = price*float64(amount) - stampDuty - registrationFee - commission
	return result
}
