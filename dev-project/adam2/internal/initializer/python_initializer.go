package initializer

import (
	iDomain "adam2/internal/properties"
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/util"
	"github.com/go-ini/ini"
)

// 初始化调用Python配置
func InitPythonProperties() {
	io.Infoln("开始读取配置文件python.ini")

	var cfg *ini.File
	var err error
	cfg, err = ini.Load("internal/config/python.ini")
	if err != nil {
		io.Fatalf("读取配置文件失败: %v", err.Error())
	}

	util.MapTo(cfg, "python", iDomain.PythonProperties_)
}
