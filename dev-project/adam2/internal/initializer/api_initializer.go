package initializer

import (
	"adam2/internal/controller/impl"
	"adam2/internal/domain"
	"github.com/gin-gonic/gin"
)

// 注册接口
func RegisterApi() {

	/************************************** TrainingInfoController ***************************************/
	adam2ApiV1TrainingInfoGroup := domain.Router_.Engine_.Group("/adam4/api/v1/trainingInfo")
	// 添加
	adam2ApiV1TrainingInfoGroup.POST("/add", func(c *gin.Context) {
		impl.GetTrainingInfoController().Add(c)
	})
	// 根据name查询
	adam2ApiV1TrainingInfoGroup.GET("/findByName/:name", func(c *gin.Context) {
		impl.GetTrainingInfoController().FindByName(c)
	})
	// 根据name，更新status
	adam2ApiV1TrainingInfoGroup.GET("/updateStatusByName/:name/:status", func(c *gin.Context) {
		impl.GetTrainingInfoController().UpdateStatusByName(c)
	})
	// 根据训练名称查询日线级别交易记录
	adam2ApiV1TrainingInfoGroup.GET("/findDateDataByTrainingName/:name", func(c *gin.Context) {
		impl.GetTrainingInfoController().FindDateDataByTrainingName(c)
	})
	// 分页显示
	adam2ApiV1TrainingInfoGroup.POST("/page", func(c *gin.Context) {
		impl.GetTrainingInfoController().Page(c)
	})
	// 根据训练名称查询周线级别交易记录
	adam2ApiV1TrainingInfoGroup.GET("/findWeekDataByTrainingName/:name", func(c *gin.Context) {
		impl.GetTrainingInfoController().FindWeekDataByTrainingName(c)
	})
	// 结束训练
	adam2ApiV1TrainingInfoGroup.GET("/stopTraining/:name", func(c *gin.Context) {
		impl.GetTrainingInfoController().StopTraining(c)
	})
	// 删除训练
	adam2ApiV1TrainingInfoGroup.GET("/deleteTraining/:name", func(c *gin.Context) {
		impl.GetTrainingInfoController().DeleteTraining(c)
	})
	// 根据trainingName。返回下一个交易日的K先数据
	adam2ApiV1TrainingInfoGroup.GET("/nextDate/:name", func(c *gin.Context) {
		impl.GetTrainingInfoController().NextDate(c)
	})

	/********************************* TrainingTransactionDataController **********************************/
	adam2ApiV1TrainingTransactionDataGroup := domain.Router_.Engine_.Group("/adam4/api/v1/trainingTransactionData")
	// 根据trg_info_id，判断是否是持仓状态
	adam2ApiV1TrainingTransactionDataGroup.GET("/isHoldingShareByTrainingInfoId/:trainingInfoId", func(c *gin.Context) {
		impl.GetTrainingTransactionDataControllerImpl().IsHoldingShareByTrainingInfoId(c)
	})
	// 开多仓
	adam2ApiV1TrainingTransactionDataGroup.GET("/openLongPosition/:name", func(c *gin.Context) {
		impl.GetTrainingTransactionDataControllerImpl().OpenLongPosition(c)
	})
	// 开空仓
	adam2ApiV1TrainingTransactionDataGroup.GET("/openShortPosition/:name", func(c *gin.Context) {
		impl.GetTrainingTransactionDataControllerImpl().OpenShortPosition(c)
	})
	// 平多仓
	adam2ApiV1TrainingTransactionDataGroup.GET("/closeLongPosition/:name", func(c *gin.Context) {
		impl.GetTrainingTransactionDataControllerImpl().CloseLongPosition(c)
	})
	// 平空仓
	adam2ApiV1TrainingTransactionDataGroup.GET("/closeShortPosition/:name", func(c *gin.Context) {
		impl.GetTrainingTransactionDataControllerImpl().CloseShortPosition(c)
	})
	// 买入
	adam2ApiV1TrainingTransactionDataGroup.GET("/buy/:name", func(c *gin.Context) {
		impl.GetTrainingTransactionDataControllerImpl().Buy(c)
	})
	// 卖出
	adam2ApiV1TrainingTransactionDataGroup.GET("/sell/:name", func(c *gin.Context) {
		impl.GetTrainingTransactionDataControllerImpl().Sell(c)
	})
	// 计算收益率
	adam2ApiV1TrainingTransactionDataGroup.GET("/calculateProfitAndLossRate/:name", func(c *gin.Context) {
		impl.GetTrainingTransactionDataControllerImpl().CalculateProfitAndLossRate(c)
	})

	/********************************* TrainingAnalysisController **********************************/
	adam2ApiV1AnalysisGroup := domain.Router_.Engine_.Group("/adam4/api/v1/trainingAnalysis")
	// 返回收益率折线图数据
	adam2ApiV1AnalysisGroup.GET("/findProfitAndLossRatePictureData/:name", func(c *gin.Context) {
		impl.GetTrainingAnalysisControllerImpl().FindProfitAndLossRatePictureData(c)
	})
}
