package initializer

import (
	"adam2/internal/domain"
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/manager"
)

// 初始化manager
func InitManager() {
	io.Infoln("初始化manager")

	domain.Manager_ = &domain.Manager{}
	domain.Manager_.AudioManager_ = manager.AudioManager{}
}
