package initializer

import (
	"adam2/internal/properties"
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/util"
	"github.com/go-ini/ini"
)

// 初始化Training
func InitTrainingProperties() {
	io.Infoln("开始读取配置文件training.ini，初始化应用程序、服务器")

	var cfg *ini.File
	var err error
	cfg, err = ini.Load("internal/config/training.ini")
	if err != nil {
		io.Fatalf("读取配置文件失败: %v", err.Error())
	} else {
		util.MapTo(cfg, "training", properties.TrainingProperties_)
	}
}
