package initializer

import (
	"adam2/internal/domain"
	"anubis-framework/pkg/io"
	"github.com/gin-gonic/gin"
)

// 初始化Router
func InitRouter() {
	io.Infoln("初始化Router")

	// 创建路由
	domain.Router_.Engine_ = gin.Default()
}
