package initializer

import (
	"adam2/internal/properties"
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/util"
	"github.com/go-ini/ini"
)

// 读取配置文件
func InitChartConfig() {
	io.Infoln("开始读取配置文件chart.ini")

	var cfg *ini.File
	var err error
	cfg, err = ini.Load("internal/config/chart.ini")
	if err != nil {
		io.Fatalf("读取配置文件失败: %v", err.Error())
	}

	util.MapTo(cfg, "chart", properties.ChartProperties_)
}
