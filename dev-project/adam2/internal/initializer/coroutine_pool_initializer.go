package initializer

import (
	iDomain "adam2/internal/domain"
	"anubis-framework/pkg/domain"
	"anubis-framework/pkg/io"
	"github.com/panjf2000/ants/v2"
	"sync"
)

// 初始化协程池
func InitCoroutinePool() {
	io.Infoln("初始化协程池")

	iDomain.CoroutinePool_.WaitGroup_ = sync.WaitGroup{}

	//申请一个协程池对象
	iDomain.CoroutinePool_.Pool_, _ = ants.NewPool(domain.Application_.CoroutinePoolCount)

}
