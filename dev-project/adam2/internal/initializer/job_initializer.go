package initializer

import (
	"adam2/internal/domain"
	"adam2/internal/service/impl"
	"anubis-framework/pkg/io"
)

// 初始化job中的service、dao、db
func InitJob() {
	io.Infoln("开始初始化service实现类")

	var err error
	domain.BaseJob_.Robot7StockTransactRecordServiceImpl_ = impl.GetRobot7StockTransactRecordServiceImpl()
	if err != nil {
		io.Fatalf("调用方法job_initializer.initJobConfig()，初始化Robot7StockTransactRecordServiceImpl时报错：%s", err)
	}
	domain.BaseJob_.ModelServiceImpl_ = impl.GetModelServiceImpl()
	if err != nil {
		io.Fatalf("调用方法job_initializer.initJobConfig()，初始化ModelServiceImpl时报错：%s", err)
	}
	domain.BaseJob_.MdlEtfBullShortLineUpServiceImpl_ = impl.GetMdlEtfBullShortLineUpServiceImpl()
	if err != nil {
		io.Fatalf("调用方法job_initializer.initJobConfig()，初始化MdlEtfBullShortLineUpServiceImpl时报错：%s", err)
	}
	domain.BaseJob_.HisRobotAccountLogServiceImpl_ = impl.GetHisRobotAccountLogServiceImpl()
	if err != nil {
		io.Fatalf("调用方法job_initializer.initJobConfig()，初始化HisRobotAccountLogServiceImpl时报错：%s", err)
	}
	domain.BaseJob_.HisRobotTransactionRecordServiceImpl_ = impl.GetHisRobotTransactionRecordServiceImpl()
	if err != nil {
		io.Fatalf("调用方法job_initializer.initJobConfig()，初始化HisRobotTransactionRecordServiceImpl时报错：%s", err)
	}
	domain.BaseJob_.StockTransactionDataAllServiceImpl_ = impl.GetStockTransactionDataAllServiceImpl()
	//if err != nil {
	//	io.Fatalf("调用方法job_initializer.initJobConfig()，初始化StockTransactionDataAllServiceImpl时报错：%s", err)
	//}
	domain.BaseJob_.Real7StockTransactionRecordServiceImpl_ = impl.GetReal7StockTransactionRecordServiceImpl()
	if err != nil {
		io.Fatalf("调用方法job_initializer.initJobConfig()，初始化Real7StockTransactionRecordServiceImpl时报错：%s", err)
	}
	domain.BaseJob_.Real7TransactionConditionServiceImpl_ = impl.GetReal7TransactConditionServiceImpl()
	if err != nil {
		io.Fatalf("调用方法job_initializer.initJobConfig()，初始化Real7TransactConditionServiceImpl时报错：%s", err)
	}
	domain.BaseJob_.ProjectServiceImpl_ = impl.GetProjectServiceImpl()
	if err != nil {
		io.Fatalf("调用方法job_initializer.initJobConfig()，初始化ProjectServiceImpl时报错：%s", err)
	}
	domain.BaseJob_.TrainingInfoServiceImpl_ = impl.GetTrainingInfoServiceImpl()
	//io.Infoln("调用方法job_initializer.initJobConfig()，初始化TrainingInfoServiceImpl")
	domain.BaseJob_.TrainingTransactionDataService_ = impl.GetTrainingTransactionDataServiceImpl()
	domain.BaseJob_.CommodityFutureMinuteDataService_ = impl.GetCommodityFutureMinuteDataServiceImpl()
	domain.BaseJob_.CommodityFutureInfoService_ = impl.GetCommodityFutureInfoServiceImpl()
}
