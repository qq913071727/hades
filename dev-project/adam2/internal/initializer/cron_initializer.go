package initializer

import (
	"adam2/internal/properties"
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/util"
	"github.com/go-ini/ini"
)

// 初始化定时任务配置
func InitCronProperties() {
	io.Infoln("开始读取配置文件cron.ini")

	var cfg *ini.File
	var err error
	cfg, err = ini.Load("internal/config/cron.ini")
	if err != nil {
		io.Fatalf("读取配置文件失败: %v", err.Error())
	}

	util.MapTo(cfg, "cron", properties.CronProperties_)
}
