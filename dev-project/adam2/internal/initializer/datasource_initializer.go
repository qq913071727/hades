package initializer

import (
	iDomain "adam2/internal/domain"
	pDomain "anubis-framework/pkg/domain"
	"anubis-framework/pkg/io"
	"fmt"
	"github.com/cengsin/oracle"
	"gorm.io/gorm"
	"sync"
	"time"
)

// 初始化数据源
func InitDataSource() {
	io.Infoln("开始连接数据库")

	var once sync.Once
	var err error
	once.Do(func() {
		dsn := fmt.Sprintf("%s/%s@%s/%s",
			pDomain.Database_.Username,
			pDomain.Database_.Password,
			pDomain.Database_.Host,
			pDomain.Database_.Schema)
		iDomain.DataSource_.GormDb, err = gorm.Open(oracle.Open(dsn), &gorm.Config{
			// 设置 NowFunc 为一个立即返回当前时间的函数，这样就不会记录慢SQL
			NowFunc: func() time.Time {
				return time.Now()
			},
		})

		sqlDB, _ := iDomain.DataSource_.GormDb.DB()
		//允许连接多少个mysql
		sqlDB.SetMaxOpenConns(pDomain.Database_.MaxOpenConnections)
		//允许最大的空闲的连接数
		sqlDB.SetMaxIdleConns(pDomain.Database_.MaxIdleConnections)
		//重用连接的最大时长
		sqlDB.SetConnMaxLifetime(pDomain.Database_.MaxConnectionLifetime)
	})

	if err != nil {
		io.Fatalf("连接数据库时报错：%s", err.Error())
	} else {
		io.Infoln("数据库连接成功")
	}
}

// 关闭数据库
func CloseDatabase() {
	sqlDB, _ := iDomain.DataSource_.GormDb.DB()
	sqlDB.Close()
}
