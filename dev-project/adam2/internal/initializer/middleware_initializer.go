package initializer

import (
	"adam2/internal/domain"
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/middleware"
)

// 初始化中间件
func InitMiddleware() {
	io.Infoln("初始化中间件")

	// 初始化中间件
	domain.Router_.Engine_.Use(middleware.Cors())
}
