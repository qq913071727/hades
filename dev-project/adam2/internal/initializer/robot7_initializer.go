package initializer

import (
	"adam2/internal/properties"
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/util"
	"github.com/go-ini/ini"
)

// 读取配置文件
func InitRobot7Properties() {
	io.Infoln("开始读取配置文件robot7.ini")

	var cfg *ini.File
	var err error
	cfg, err = ini.Load("internal/config/robot7.ini")
	if err != nil {
		io.Fatalf("读取配置文件失败: %v", err.Error())
	}

	util.MapTo(cfg, "robot7", properties.Robot7Properties_)
}

// 读取配置文件
func InitReal7Properties() {
	io.Infoln("开始读取配置文件real7.ini")

	var cfg *ini.File
	var err error
	cfg, err = ini.Load("internal/config/real7.ini")
	if err != nil {
		io.Fatalf("读取配置文件失败: %v", err.Error())
	}

	util.MapTo(cfg, "real7", properties.Real7Properties_)
}
