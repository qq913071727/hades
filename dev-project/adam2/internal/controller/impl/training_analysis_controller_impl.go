package impl

import (
	pConstants "anubis-framework/pkg/constants"
	pDomain "anubis-framework/pkg/domain"
	"anubis-framework/pkg/util"
	"github.com/gin-gonic/gin"
	"net/http"
)

type TrainingAnalysisControllerImpl struct {
	*BaseControllerImpl
}

// 获取TrainingAnalysisController
func GetTrainingAnalysisControllerImpl() *TrainingAnalysisControllerImpl {
	return &TrainingAnalysisControllerImpl{GetBaseControllerImpl()}
}

// 返回收益率折线图数据
func (_trainingAnalysisControllerImpl TrainingAnalysisControllerImpl) FindProfitAndLossRatePictureData(c *gin.Context) {
	var trainingName string = c.Param("name")

	// 参数验证：参数name不能为空
	if trainingName == "" {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.OperationFailCode
		apiResult.Message = "参数name不能为空"
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 返回收益率折线图数据
	var serviceResult *pDomain.ServiceResult = _trainingAnalysisControllerImpl._trainingTransactionDataServiceImpl.FindProfitAndLossRatePictureData(trainingName)
	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}
