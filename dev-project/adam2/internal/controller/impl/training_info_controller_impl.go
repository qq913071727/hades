package impl

import (
	"adam2/internal/constants"
	"adam2/internal/vo"
	pConstants "anubis-framework/pkg/constants"
	pDomain "anubis-framework/pkg/domain"
	"anubis-framework/pkg/util"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type TrainingInfoController struct {
	*BaseControllerImpl
}

// 获取TrainingInfoController
func GetTrainingInfoController() *TrainingInfoController {
	return &TrainingInfoController{GetBaseControllerImpl()}
}

// 添加
func (_trainingInfoController *TrainingInfoController) Add(c *gin.Context) {
	// 转换参数
	var addTrainingInfoVo *vo.AddTrainingInfoVo = &vo.AddTrainingInfoVo{}
	c.BindJSON(addTrainingInfoVo)

	// 参数验证：参数name不能为空
	if addTrainingInfoVo.Name == "" {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.OperationFailCode
		apiResult.Message = "参数name不能为空"
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}
	// 参数验证：根据name判断记录是否已经存在
	var serviceResult *pDomain.ServiceResult = _trainingInfoController._trainingInfoServiceImpl.ExistByName(addTrainingInfoVo.Name)
	if serviceResult.Success == false {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.OperationFailCode
		apiResult.Message = "参数name不能重复"
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 插入记录
	var _serviceResult *pDomain.ServiceResult = _trainingInfoController._trainingInfoServiceImpl.Add(addTrainingInfoVo)
	var code, apiResult = util.ServiceResultToApiResult(_serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 根据name查询
func (_trainingInfoController *TrainingInfoController) FindByName(c *gin.Context) {
	var name string = c.Param("name")

	// 根据name查询
	var serviceResult *pDomain.ServiceResult = _trainingInfoController._trainingInfoServiceImpl.FindByName(name)
	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 根据name，更新status
func (_trainingInfoController *TrainingInfoController) UpdateStatusByName(c *gin.Context) {
	var trainingName string = c.Param("name")
	var status, _ = strconv.Atoi(c.Param("status"))

	// 验证参数
	if trainingName == "" {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.OperationFailCode
		apiResult.Message = "参数name不能重复"
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}
	if status == 0 {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.OperationFailCode
		apiResult.Message = "参数status不能重复"
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 根据name，更新status
	var serviceResult *pDomain.ServiceResult = _trainingInfoController._trainingInfoServiceImpl.UpdateStatusByName(trainingName, status)
	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 根据训练名称查询日线级别交易记录
func (_trainingInfoController *TrainingInfoController) FindDateDataByTrainingName(c *gin.Context) {
	var trainingName string = c.Param("name")

	// 根据训练名称查询日线级别交易记录
	var serviceResult *pDomain.ServiceResult = _trainingInfoController._trainingInfoServiceImpl.FindDateDataByTrainingName(trainingName)

	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 分页显示
func (_trainingInfoController *TrainingInfoController) Page(c *gin.Context) {
	// 转换参数
	var pageTrainingInfoVo *vo.PageTrainingInfoVo = &vo.PageTrainingInfoVo{}
	c.BindJSON(pageTrainingInfoVo)

	// 参数验证：参数pageNo和pageSize不能为空
	if pageTrainingInfoVo.PageNo == 0 || pageTrainingInfoVo.PageSize == 0 {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.OperationFailCode
		apiResult.Message = "参数pageNo和pageSize不能为空"
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 分页
	var serviceResult *pDomain.ServiceResult = _trainingInfoController._trainingInfoServiceImpl.Page(pageTrainingInfoVo.Name, pageTrainingInfoVo.PageNo, pageTrainingInfoVo.PageSize)
	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 根据训练名称查询周线级别交易记录
func (_trainingInfoController *TrainingInfoController) FindWeekDataByTrainingName(c *gin.Context) {
	var trainingName string = c.Param("name")

	// 根据训练名称查询日线级别交易记录
	var serviceResult *pDomain.ServiceResult = _trainingInfoController._trainingInfoServiceImpl.FindWeekDataByTrainingName(trainingName)

	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 结束训练
func (_trainingInfoController *TrainingInfoController) StopTraining(c *gin.Context) {
	var trainingName string = c.Param("name")

	// 参数验证：判断训练是否已经存在
	var trainingNameExist bool = _trainingInfoController.ValidateTrainingNameExist(trainingName)
	if trainingNameExist == false {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.NotExistsCode
		apiResult.Message = fmt.Sprintf("name为[%s]的训练不存在", trainingName)
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 更新
	var _serviceResult *pDomain.ServiceResult = _trainingInfoController._trainingInfoServiceImpl.UpdateStatusByName(trainingName, constants.TrainingFinish)
	var code, apiResult = util.ServiceResultToApiResult(_serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 删除训练
func (_trainingInfoController *TrainingInfoController) DeleteTraining(c *gin.Context) {
	var trainingName string = c.Param("name")

	// 参数验证：判断训练是否已经存在
	var trainingNameExist bool = _trainingInfoController.ValidateTrainingNameExist(trainingName)
	if trainingNameExist == false {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.NotExistsCode
		apiResult.Message = fmt.Sprintf("name为[%s]的训练不存在", trainingName)
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 删除训练和对应的交易记录
	var _serviceResult *pDomain.ServiceResult = _trainingInfoController._trainingInfoServiceImpl.DeleteTraining(trainingName)
	var code, apiResult = util.ServiceResultToApiResult(_serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 根据trainingName。返回下一个交易日的K线数据
func (_trainingInfoController *TrainingInfoController) NextDate(c *gin.Context) {
	var name string = c.Param("name")

	// 参数验证：判断训练是否已经存在
	var trainingNameExist bool = _trainingInfoController.ValidateTrainingNameExist(name)
	if trainingNameExist == false {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.NotExistsCode
		apiResult.Message = fmt.Sprintf("name为[%s]的训练不存在", name)
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	var serviceResult *pDomain.ServiceResult = _trainingInfoController._trainingInfoServiceImpl.NextDate(name)
	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}
