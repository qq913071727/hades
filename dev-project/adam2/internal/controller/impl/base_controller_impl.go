package impl

import (
	"adam2/internal/model"
	"adam2/internal/service/impl"
	pDomain "anubis-framework/pkg/domain"
)

// controller基类的实现
type BaseControllerImpl struct {
	_trainingInfoServiceImpl            *impl.TrainingInfoServiceImpl
	_stockTransactionDataAllServiceImpl *impl.StockTransactionDataAllServiceImpl
	_commodityFutureDateDataServiceImpl *impl.CommodityFutureDateDataServiceImpl
	_stockWeekServiceImpl               *impl.StockWeekServiceImpl
	_commodityFutureWeekDataServiceImpl *impl.CommodityFutureWeekDataServiceImpl
	_trainingTransactionDataServiceImpl *impl.TrainingTransactionDataServiceImpl
}

// 获取BaseControllerImpl的实例
func GetBaseControllerImpl() *BaseControllerImpl {
	return &BaseControllerImpl{impl.GetTrainingInfoServiceImpl(),
		impl.GetStockTransactionDataAllServiceImpl(),
		impl.GetCommodityFutureDateDataServiceImpl(),
		impl.GetStockWeekServiceImpl(),
		impl.GetCommodityFutureWeekDataServiceImpl(),
		impl.GetTrainingTransactionDataServiceImpl()}
}

// 判断trainingName是否存在
func (_baseControllerImpl *BaseControllerImpl) ValidateTrainingNameExist(name string) bool {
	// 判断训练是否已经存在
	var serviceResult *pDomain.ServiceResult = _baseControllerImpl._trainingInfoServiceImpl.FindByName(name)
	if serviceResult.Success == true {
		var trainingInfo model.TrainingInfo = serviceResult.Result.(model.TrainingInfo)
		if trainingInfo.ID == 0 {
			return false
		} else {
			return true
		}
	} else {
		return false
	}
}
