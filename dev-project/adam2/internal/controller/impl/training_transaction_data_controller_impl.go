package impl

import (
	pConstants "anubis-framework/pkg/constants"
	pDomain "anubis-framework/pkg/domain"
	"anubis-framework/pkg/util"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type TrainingTransactionDataControllerImpl struct {
	*BaseControllerImpl
}

// 获取TrainingTransactionDataControllerImpl
func GetTrainingTransactionDataControllerImpl() *TrainingTransactionDataControllerImpl {
	return &TrainingTransactionDataControllerImpl{GetBaseControllerImpl()}
}

// 根据trg_info_id，判断是否是持仓状态
func (_trainingTransactionDataControllerImpl TrainingTransactionDataControllerImpl) IsHoldingShareByTrainingInfoId(c *gin.Context) {
	var trainingInfoId string = c.Param("trainingInfoId")

	// 参数验证：参数TrainingInfoId、Market、Code、Direction不能为空
	if trainingInfoId == "" {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.OperationFailCode
		apiResult.Message = "参数name不能为空"
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 根据trg_info_id，判断是否是持仓状态
	_trainingInfoId, _ := strconv.Atoi(trainingInfoId)
	var serviceResult *pDomain.ServiceResult = _trainingTransactionDataControllerImpl._trainingTransactionDataServiceImpl.IsHoldingShareByTrainingInfoId(_trainingInfoId)
	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 开多仓
func (_trainingTransactionDataControllerImpl TrainingTransactionDataControllerImpl) OpenLongPosition(c *gin.Context) {
	var trainingName string = c.Param("name")

	// 参数验证：参数TrainingInfoId、Market、Code、Direction不能为空
	if trainingName == "" {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.OperationFailCode
		apiResult.Message = "参数name不能为空"
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 参数验证：判断训练是否已经存在
	var trainingNameExist bool = _trainingTransactionDataControllerImpl.ValidateTrainingNameExist(trainingName)
	if trainingNameExist == false {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.NotExistsCode
		apiResult.Message = fmt.Sprintf("name为[%s]的训练不存在", trainingName)
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 开多仓
	var serviceResult *pDomain.ServiceResult = _trainingTransactionDataControllerImpl._trainingTransactionDataServiceImpl.OpenLongPosition(trainingName)
	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 开空仓
func (_trainingTransactionDataControllerImpl TrainingTransactionDataControllerImpl) OpenShortPosition(c *gin.Context) {
	var trainingName string = c.Param("name")

	// 参数验证：参数name不能为空
	if trainingName == "" {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.OperationFailCode
		apiResult.Message = "参数name不能为空"
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 开空仓
	var serviceResult *pDomain.ServiceResult = _trainingTransactionDataControllerImpl._trainingTransactionDataServiceImpl.OpenShortPosition(trainingName)
	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 平多仓
func (_trainingTransactionDataControllerImpl TrainingTransactionDataControllerImpl) CloseLongPosition(c *gin.Context) {
	var trainingName string = c.Param("name")

	// 参数验证：参数name不能为空
	if trainingName == "" {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.OperationFailCode
		apiResult.Message = "参数name不能为空"
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 平多仓
	var serviceResult *pDomain.ServiceResult = _trainingTransactionDataControllerImpl._trainingTransactionDataServiceImpl.CloseLongPosition(trainingName)
	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 平空仓
func (_trainingTransactionDataControllerImpl TrainingTransactionDataControllerImpl) CloseShortPosition(c *gin.Context) {
	var trainingName string = c.Param("name")

	// 参数验证：参数name不能为空
	if trainingName == "" {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.OperationFailCode
		apiResult.Message = "参数name不能为空"
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 平空仓
	var serviceResult *pDomain.ServiceResult = _trainingTransactionDataControllerImpl._trainingTransactionDataServiceImpl.CloseShortPosition(trainingName)
	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 买入
func (_trainingTransactionDataControllerImpl TrainingTransactionDataControllerImpl) Buy(c *gin.Context) {
	var trainingName string = c.Param("name")

	// 参数验证：参数name不能为空
	if trainingName == "" {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.OperationFailCode
		apiResult.Message = "参数name不能为空"
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 买入
	var serviceResult *pDomain.ServiceResult = _trainingTransactionDataControllerImpl._trainingTransactionDataServiceImpl.Buy(trainingName)
	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 卖出
func (_trainingTransactionDataControllerImpl TrainingTransactionDataControllerImpl) Sell(c *gin.Context) {
	var trainingName string = c.Param("name")

	// 参数验证：参数name不能为空
	if trainingName == "" {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.OperationFailCode
		apiResult.Message = "参数name不能为空"
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 卖出
	var serviceResult *pDomain.ServiceResult = _trainingTransactionDataControllerImpl._trainingTransactionDataServiceImpl.Sell(trainingName)
	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}

// 计算收益率
func (_trainingTransactionDataControllerImpl TrainingTransactionDataControllerImpl) CalculateProfitAndLossRate(c *gin.Context) {
	var trainingName string = c.Param("name")

	// 参数验证：参数name不能为空
	if trainingName == "" {
		var apiResult *pDomain.ApiResult = &pDomain.ApiResult{}
		apiResult.Code = pConstants.OperationFailCode
		apiResult.Message = "参数name不能为空"
		apiResult.Success = false

		c.JSON(http.StatusBadRequest, *apiResult)
		return
	}

	// 计算收益率
	var serviceResult *pDomain.ServiceResult = _trainingTransactionDataControllerImpl._trainingTransactionDataServiceImpl.CalculateProfitAndLossRate(trainingName)
	var code, apiResult = util.ServiceResultToApiResult(serviceResult)
	c.JSON(code, *apiResult)
	return
}
