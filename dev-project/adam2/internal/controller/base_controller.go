package controller

// controller基类的接口
type BaseController interface {

	// 判断trainingName是否存在
	ValidateTrainingNameExist(name string) bool
}
