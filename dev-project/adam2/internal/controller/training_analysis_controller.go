package controller

import "github.com/gin-gonic/gin"

type TrainingAnalysisController interface {
	// 返回收益率折线图数据
	FindProfitAndLossRatePictureData(c *gin.Context)
}
