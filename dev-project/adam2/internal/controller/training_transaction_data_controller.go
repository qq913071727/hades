package controller

import "github.com/gin-gonic/gin"

type TrainingTransactionDataController interface {

	// 根据trg_info_id，判断是否是持仓状态
	IsHoldingShareByTrainingInfoId(c *gin.Context)

	// 开多仓
	OpenLongPosition(c *gin.Context)

	// 开空仓
	OpenShortPosition(c *gin.Context)

	// 平多仓
	CloseLongPosition(c *gin.Context)

	// 平空仓
	CloseShortPosition(c *gin.Context)

	// 买入
	Buy(c *gin.Context)

	// 卖出
	Sell(c *gin.Context)

	// 计算收益率
	CalculateProfitAndLossRate(c *gin.Context)
}
