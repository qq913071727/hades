package controller

import "github.com/gin-gonic/gin"

type TrainingInfoController interface {
	// 添加
	Add(c *gin.Context)

	// 根据name查询
	FindByName(c *gin.Context)

	// 根据name，更新status
	UpdateStatusByName(c *gin.Context)

	// 根据训练名称查询日线级别交易记录
	FindDateDataByTrainingName(c *gin.Context)

	// 根据训练名称查询周线级别交易记录
	FindWeekDataByTrainingName(c *gin.Context)

	// 分页显示
	Page(c *gin.Context)

	// 结束训练
	StopTraining(c *gin.Context)

	// 删除训练
	DeleteTraining(c *gin.Context)

	// 根据trainingName。返回下一个交易日的K线数据
	NextDate(c *gin.Context)
}
