package vo

type PageTrainingInfoVo struct {
	Name     string
	PageNo   int
	PageSize int
}
