package vo

import "time"

type ClosePositionVo struct {
	TrainingInfoId int
	Market         int
	Code           string
	BuyDate        time.Time
	BuyPrice       float64
	BuyAmount      float64
	SellDate       time.Time
	SellPrice      float64
	SellAmount     float64
	Direction      int
}
