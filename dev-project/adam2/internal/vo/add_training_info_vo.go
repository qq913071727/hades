package vo

type AddTrainingInfoVo struct {
	// 主键
	ID int `gorm:"column:ID_"`
	// 名称
	Name string `gorm:"column:NAME"`
	// 市场。0表示随机，1表示股市，2表示期货
	Market int `gorm:"column:MARKET"`
	// 股票/期货代码
	Code string `gorm:"column:CODE"`
	// 开始时间
	BeginDate string `gorm:"column:BEGIN_DATE"`
	// 结束时间
	EndDate string `gorm:"column:END_DATE"`
}
