package constants

// 交易方向
const (
	// 做多
	Bull int = 1
	// 做空
	Short int = -1
)
