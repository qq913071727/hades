package constants

// 交易类型
const (
	// 股票
	Stock int = 1
	// ETF
	ETF int = 2
	// 期货
	Commodity_Future int = 3
)
