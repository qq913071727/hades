package constants

// 方差
const (
	// 2日方差
	Variance2 int = 2
	// 5日方差
	Variance5 int = 5
	// 10日方差
	Variance10 int = 10
	// 20日方差
	Variance20 int = 20
	// 60日方差
	Variance60 int = 60
	// 120日方差
	Variance120 int = 120
	// 250日方差
	Variance250 int = 250
)
