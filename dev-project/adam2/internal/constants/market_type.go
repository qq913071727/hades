package constants

// 市场类型
const (
	// 股票市场
	StockMarket int = 1
	// 期货市场
	CommodityFutureMarket int = 2
)
