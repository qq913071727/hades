package constants

// 训练状态
const (
	// 未开始
	TrainingWaiting int = 1
	// 已经开始
	TrainingStarting int = 2
	// 已经结束
	TrainingFinish int = 3
)
