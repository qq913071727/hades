package constants

// 各种算法
const (
	// MACD金叉
	MacdGoldCross int = 1
	// MACD死叉
	MacdDeadCross int = 2
	// 收盘价金叉ma5
	ClosePriceMa5GoldCross int = 3
	// 收盘价死叉ma5
	ClosePriceMa5DeadCross int = 4
	// hei_kin_ashi上涨趋势
	HeiKinAshiUp int = 5
	// hei_kin_ashi下跌趋势
	HeiKinAshiDown int = 6
	// kd金叉
	KdGoldCross int = 7
	// kd死叉
	KdDeadCross int = 8
	// 收盘价金叉牛熊线
	ClosePriceGoldCrossBullShortLine int = 9
	// 收盘价死叉牛熊线
	ClosePriceDeadCrossBullShortLine int = 10
)
