package constants

// 任务名称
const (
	// robot7测试
	Robot7 string = "robot7"
	// 生成图表
	Chart string = "chart"
	// 真实交易7
	Real7 string = "real7"
	// 训练
	Training string = "training"
	// 解密
	Decrypt string = "decrypt"
	// 定时任务
	Cron string = "cron"
)
