package constants

// 从这一天开始，创业板的涨跌停限制为20%
const JUDGEMENT_DATE = "2020-08-24"

// 是涨停板
const LIMIT_UP = 1

// 不是涨停板
const NOT_LIMIT_UP = 2
