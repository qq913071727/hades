package service

import (
	"adam2/internal/model"
	pDomain "anubis-framework/pkg/domain"
)

type StraightLineIncreaseDecreaseService interface {
	// 添加
	Add(straightLineIncreaseDecrease model.StraightLineIncreaseDecrease) *pDomain.ServiceResult
}
