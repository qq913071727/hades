package service

import (
	pDomain "anubis-framework/pkg/domain"
)

// service接口
type BaseService interface {

	// 根据训练名称查询日线级别交易记录
	FindDateDataByTrainingName(trainingName string) *pDomain.ServiceResult

	// 根据训练名称查询周线级别交易记录
	FindWeekDataByTrainingName(trainingName string) *pDomain.ServiceResult
}
