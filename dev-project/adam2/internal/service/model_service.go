package service

type ModelService interface {

	// 将测试数据导入到历史数据表中
	ImportIntoHistoryDataTableFromRobot7() error
}
