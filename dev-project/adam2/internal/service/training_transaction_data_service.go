package service

import (
	pDomain "anubis-framework/pkg/domain"
)

type TrainingTransactionDataService interface {

	// 根据trg_info_id，判断是否是持仓状态
	IsHoldingShareByTrainingInfoId(trainingInfoId int) *pDomain.ServiceResult

	// 开多仓
	OpenLongPosition(trainingName string) *pDomain.ServiceResult

	// 开空仓
	OpenShortPosition(trainingName string) *pDomain.ServiceResult

	// 平多仓
	CloseLongPosition(trainingName string) *pDomain.ServiceResult

	// 平空仓
	CloseShortPosition(trainingName string) *pDomain.ServiceResult

	// 买入
	Buy(trainingName string) *pDomain.ServiceResult

	// 卖出
	Sell(trainingName string) *pDomain.ServiceResult

	// 计算收益率
	CalculateProfitAndLossRate(trainingName string) *pDomain.ServiceResult

	// 返回收益率折线图数据
	FindProfitAndLossRatePictureData(trainingName string) *pDomain.ServiceResult
}
