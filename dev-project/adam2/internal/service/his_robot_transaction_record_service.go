package service

type HisRobotTransactionRecordService interface {

	// 创建收益率(his_robot_transact_record)和相关系数关系的散点图
	CreateProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterByHisRobotTransactRecord() error
}
