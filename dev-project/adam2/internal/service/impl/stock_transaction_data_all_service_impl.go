package impl

import (
	"adam2/internal/constants"
	"adam2/internal/domain"
	"adam2/internal/model"
	"adam2/internal/properties"
	pConstants "anubis-framework/pkg/constants"
	pDomain "anubis-framework/pkg/domain"
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/ui"
	"anubis-framework/pkg/util"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/go-echarts/go-echarts/v2/charts"
	"github.com/go-echarts/go-echarts/v2/opts"
	"github.com/go-echarts/go-echarts/v2/types"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
	"io/ioutil"
	"os"
	"os/exec"
	"time"
)

type StockTransactionDataAllServiceImpl struct {
	*BaseServiceImpl
}

// 获取service实现类
func GetStockTransactionDataAllServiceImpl() *StockTransactionDataAllServiceImpl {
	return &StockTransactionDataAllServiceImpl{GetBaseServiceImpl()}
}

// 获取开始日期和结束日期之间的日期（去重），并按照日期升序排列
func (_stockTransactionDataAllServiceImpl *StockTransactionDataAllServiceImpl) GetDistinctDateBetwenDateOrderByDateAsc(beginDate string, endDate string) ([]string, error) {
	var dateArray []string
	var err error
	dateArray, err = _stockTransactionDataAllServiceImpl._stockTransactionDataAllDaoImpl.GetDistinctDateBetweenDateOrderByDateAsc(beginDate, endDate)
	if err != nil {
		io.Fatalf("调用GetDistinctDateBetwenDateOrderByDateAsc方法时报错：%s", err.Error())
		return nil, err
	} else {
		return dateArray, nil
	}
}

// 创建平均收盘价和各个平均均线的折线图（经过平滑处理后的）
func (_stockTransactionDataAllServiceImpl *StockTransactionDataAllServiceImpl) CreateAverageClosePriceAndMALineChart_smooth() {
	io.Infoln("创建平均收盘价和各个平均均线的折线图，时间区间为[%s]至[%s]", properties.ChartProperties_.AverageClosePriceAndMABeginDate, properties.ChartProperties_.AverageClosePriceAndMAEndDate)

	line := charts.NewLine()
	line.SetGlobalOptions(
		charts.WithInitializationOpts(opts.Initialization{
			Theme: types.ThemeInfographic,
		}),
		charts.WithTitleOpts(opts.Title{
			Title:    "平均收盘价和各个平均均线的折线图",
			Subtitle: "",
		}),
	)

	var averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray *domain.AverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray
	var err error
	averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray, err = _stockTransactionDataAllServiceImpl._stockTransactionDataAllDaoImpl.GetAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc_smooth(properties.ChartProperties_.AverageClosePriceAndMABeginDate, properties.ChartProperties_.AverageClosePriceAndMAEndDate)
	if err != nil {
		io.Fatalf("调用GetAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc方法时报错：%s", err.Error())
		return
	} else {
		if len(*averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray) != 0 {
			dateArray := make([]opts.LineData, 0)
			averageClosePriceArray := make([]opts.LineData, 0)
			averageMa5Array := make([]opts.LineData, 0)
			averageMa10Array := make([]opts.LineData, 0)
			averageMa20Array := make([]opts.LineData, 0)
			averageMa60Array := make([]opts.LineData, 0)
			averageMa120Array := make([]opts.LineData, 0)
			averageMa250Array := make([]opts.LineData, 0)

			for _, averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc := range *averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray {
				dateArray = append(dateArray, opts.LineData{Value: averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.Date})
				averageClosePriceArray = append(averageClosePriceArray, opts.LineData{Value: averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageClosePrice})
				averageMa5Array = append(averageMa5Array, opts.LineData{Value: averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa5})
				averageMa10Array = append(averageMa10Array, opts.LineData{Value: averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa10})
				averageMa20Array = append(averageMa20Array, opts.LineData{Value: averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa20})
				averageMa60Array = append(averageMa60Array, opts.LineData{Value: averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa60})
				averageMa120Array = append(averageMa120Array, opts.LineData{Value: averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa120})
				averageMa250Array = append(averageMa250Array, opts.LineData{Value: averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa250})
			}

			line.SetXAxis(dateArray).
				AddSeries("average close price", averageClosePriceArray).
				AddSeries("average ma5", averageMa5Array).
				AddSeries("average ma10", averageMa10Array).
				AddSeries("average ma20", averageMa20Array).
				AddSeries("average ma60", averageMa60Array).
				AddSeries("average ma120", averageMa120Array).
				AddSeries("average ma250", averageMa250Array).
				SetSeriesOptions(charts.WithLineChartOpts(opts.LineChart{Smooth: true}))
			f, _ := os.Create(properties.ChartProperties_.AverageClosePriceAndMALineChartPath + properties.ChartProperties_.AverageClosePriceAndMAgLineChartSuffix)
			_ = line.Render(f)
			defer f.Close()
		}
	}
	return
}

// 创建平均收盘价和牛熊线（bias决定）的折线图
func (_stockTransactionDataAllServiceImpl *StockTransactionDataAllServiceImpl) CreateAverageClosePriceAndMABullShortLineByBiasLineChart() {
	var beginDate string = properties.ChartProperties_.AverageClosePriceAndMABullShortLineByBiasBeginDate
	var endDate string = properties.ChartProperties_.AverageClosePriceAndMABullShortLineByBiasEndDate
	io.Infoln("创建平均收盘价和牛熊线（bias决定）的折线图，时间区间为[%s]至[%s]", beginDate, endDate)

	line := charts.NewLine()
	line.SetGlobalOptions(
		charts.WithInitializationOpts(opts.Initialization{
			Theme: types.ThemeInfographic,
		}),
		charts.WithTitleOpts(opts.Title{
			Title:    "创建平均收盘价和牛熊线（bias决定）的折线图",
			Subtitle: "",
		}),
	)

	var averageClosePriceAndMaBullShortLineByBiasArray *domain.AverageClosePriceAndMaBullShortLineByBiasArray
	var err error
	averageClosePriceAndMaBullShortLineByBiasArray, err = _stockTransactionDataAllServiceImpl._stockTransactionDataAllDaoImpl.GetAverageClosePriceAndMaAndBiasBetweenDateGroupByDateOrderByDateAsc(beginDate, endDate)
	if err != nil {
		io.Fatalf("调用GetAverageClosePriceAndMaAndBiasBetweenDateGroupByDateOrderByDateAsc方法时报错：%s", err.Error())
		return
	} else {
		if len(*averageClosePriceAndMaBullShortLineByBiasArray) != 0 {
			dateArray := make([]opts.LineData, 0)
			averageClosePriceArray := make([]opts.LineData, 0)
			averageBullShortLineArray := make([]opts.LineData, 0)

			var topLimit float64 = properties.ChartProperties_.AverageClosePriceAndMABullShortLineByBiasTopLimit
			var bottomList float64 = properties.ChartProperties_.AverageClosePriceAndMABullShortLineByBiasBottomLimit

			for _, averageClosePriceAndMaBullShortLineByBias := range *averageClosePriceAndMaBullShortLineByBiasArray {
				var averageClosePrice float64 = averageClosePriceAndMaBullShortLineByBias.AverageClosePrice
				var averageBias5 float64 = averageClosePriceAndMaBullShortLineByBias.AverageBias5
				var averageBias10 float64 = averageClosePriceAndMaBullShortLineByBias.AverageBias10
				var averageBias20 float64 = averageClosePriceAndMaBullShortLineByBias.AverageBias20
				var averageBias60 float64 = averageClosePriceAndMaBullShortLineByBias.AverageBias60
				var averageBias120 float64 = averageClosePriceAndMaBullShortLineByBias.AverageBias120
				var averageBias250 float64 = averageClosePriceAndMaBullShortLineByBias.AverageBias250

				//if averageClosePrice <= averageClosePriceAndMaBullShortLineByBias.AverageMa250 ||
				//	(averageClosePrice > averageClosePriceAndMaBullShortLineByBias.AverageMa250 &&
				//		averageClosePriceAndMaBullShortLineByBias.AverageMa250 > averageClosePriceAndMaBullShortLineByBias.AverageMa120) {
				if averageBias250 <= topLimit && averageBias250 >= bottomList {
					averageBullShortLineArray = append(averageBullShortLineArray, opts.LineData{Value: averageClosePriceAndMaBullShortLineByBias.AverageMa250})
				} else if averageBias120 <= topLimit && averageBias120 >= bottomList {
					averageBullShortLineArray = append(averageBullShortLineArray, opts.LineData{Value: averageClosePriceAndMaBullShortLineByBias.AverageMa120})
				} else if averageBias60 <= topLimit && averageBias60 >= bottomList {
					averageBullShortLineArray = append(averageBullShortLineArray, opts.LineData{Value: averageClosePriceAndMaBullShortLineByBias.AverageMa60})
				} else if averageBias20 <= topLimit && averageBias20 >= bottomList {
					averageBullShortLineArray = append(averageBullShortLineArray, opts.LineData{Value: averageClosePriceAndMaBullShortLineByBias.AverageMa20})
				} else if averageBias10 <= topLimit && averageBias10 >= bottomList {
					averageBullShortLineArray = append(averageBullShortLineArray, opts.LineData{Value: averageClosePriceAndMaBullShortLineByBias.AverageMa10})
				} else if averageBias5 <= topLimit && averageBias5 >= bottomList {
					averageBullShortLineArray = append(averageBullShortLineArray, opts.LineData{Value: averageClosePriceAndMaBullShortLineByBias.AverageMa5})
				} else {
					averageBullShortLineArray = append(averageBullShortLineArray, opts.LineData{Value: averageClosePrice})
				}
				//} else {
				//	if averageBias120 <= topLimit && averageBias120 >= bottomList {
				//		averageBullShortLineArray = append(averageBullShortLineArray, opts.LineData{Value: averageClosePriceAndMaBullShortLineByBias.AverageMa120})
				//	} else if averageBias60 <= topLimit && averageBias60 >= bottomList {
				//		averageBullShortLineArray = append(averageBullShortLineArray, opts.LineData{Value: averageClosePriceAndMaBullShortLineByBias.AverageMa60})
				//	} else if averageBias20 <= topLimit && averageBias20 >= bottomList {
				//		averageBullShortLineArray = append(averageBullShortLineArray, opts.LineData{Value: averageClosePriceAndMaBullShortLineByBias.AverageMa20})
				//	} else if averageBias10 <= topLimit && averageBias10 >= bottomList {
				//		averageBullShortLineArray = append(averageBullShortLineArray, opts.LineData{Value: averageClosePriceAndMaBullShortLineByBias.AverageMa10})
				//	} else if averageBias5 <= topLimit && averageBias5 >= bottomList {
				//		averageBullShortLineArray = append(averageBullShortLineArray, opts.LineData{Value: averageClosePriceAndMaBullShortLineByBias.AverageMa5})
				//	} else {
				//		averageBullShortLineArray = append(averageBullShortLineArray, opts.LineData{Value: averageClosePrice})
				//	}
				//}

				dateArray = append(dateArray, opts.LineData{Value: averageClosePriceAndMaBullShortLineByBias.Date})
				averageClosePriceArray = append(averageClosePriceArray, opts.LineData{Value: averageClosePrice})
			}

			line.SetXAxis(dateArray).
				AddSeries("average close price", averageClosePriceArray).
				AddSeries("average bull short line", averageBullShortLineArray).
				SetSeriesOptions(charts.WithLineChartOpts(opts.LineChart{Smooth: true}))
			f, _ := os.Create(properties.ChartProperties_.AverageClosePriceAndMABullShortLineByBiasLineChartPath +
				"_" + beginDate + "_" + endDate + properties.ChartProperties_.AverageClosePriceAndMABullShortLineByBiasLineChartSuffix)
			_ = line.Render(f)
			defer f.Close()
		}
	}
	return
}

// 根据code、开始时间和结束时间查询记录
func (_stockTransactionDataAllServiceImpl *StockTransactionDataAllServiceImpl) FindByCodeBetweenDate(code string, beginDate time.Time, endDate time.Time) *pDomain.ServiceResult {
	var _beginDate string = util.DateToString(beginDate)
	var _endDate string = util.DateToString(endDate)
	io.Infoln("根据code[%s]、开始时间[%s]和结束时间[%s]查询记录", code, _beginDate, _endDate)

	var stockTransactionDataAllArray model.StockTransactionDataAllArray = _stockTransactionDataAllServiceImpl._stockTransactionDataAllDaoImpl.FindByCodeBetweenDate(code, _beginDate, _endDate)
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}
	serviceResult.Code = pConstants.OperationSuccessCode
	serviceResult.Message = pConstants.OperationSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = stockTransactionDataAllArray
	return serviceResult
}

// 查询所有code
func (_stockTransactionDataAllServiceImpl *StockTransactionDataAllServiceImpl) FindDistinctCode() *pDomain.ServiceResult {
	var codeArray []string = _stockTransactionDataAllServiceImpl._stockTransactionDataAllDaoImpl.FindDistinctCode()
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}
	serviceResult.Code = pConstants.OperationSuccessCode
	serviceResult.Message = pConstants.OperationSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = codeArray
	return serviceResult
}

// 监视直线上涨或直线下跌的行情
func (_stockTransactionDataAllServiceImpl *StockTransactionDataAllServiceImpl) WatchStraightLineIncreaseOrDecreaseInStockTransactionMinuteData(lastStockTransactionDataAllArray *model.StockTransactionDataAllArray) *pDomain.ServiceResult {
	io.Infoln("监控股票的开始时间：" + time.Now().Format("2006-01-02 15:04:05"))

	// 调用 Python 脚本
	cmd := exec.Command("python", properties.PythonProperties_.ManagePath, properties.PythonProperties_.PackageName, properties.PythonProperties_.WatchStockTransactionMinuteDataMethodName)

	// 捕获标准输出
	var out bytes.Buffer
	cmd.Stdout = &out

	// 运行命令并处理错误
	if _error := cmd.Run(); _error != nil {
		io.Infoln("调用python程序，获取股票时报错：", _error)
		return &pDomain.ServiceResult{}
	}

	// 解析返回的 JSON 数据
	var stockTransactionDataAllArray model.StockTransactionDataAllArray
	if len(out.Bytes()) != 0 {
		reader := transform.NewReader(bytes.NewReader(out.Bytes()), simplifiedchinese.GBK.NewDecoder())
		all, err := ioutil.ReadAll(reader)
		if err != nil {
			io.Infoln("调用python程序，读取json数据时报错" + err.Error())
		}
		var result string = string(all)
		err = json.Unmarshal([]byte(result), &stockTransactionDataAllArray)
		if err != nil {
			io.Infoln("调用python程序，转换json为struct数据时报错" + err.Error())
		}
	} else {
		io.Infoln("调用python程序，获取股票的数据为空")
		return &pDomain.ServiceResult{}
	}

	// 如果没有上一次的数据，则直接返回，等待下一次定时任务执行。如果有上一次的数据，则进行比较
	if lastStockTransactionDataAllArray != nil && stockTransactionDataAllArray != nil {
		for _, stockTransactionDataAll := range stockTransactionDataAllArray {
			for _, lastStockTransactionDataAll := range *lastStockTransactionDataAllArray {
				if stockTransactionDataAll.Code == lastStockTransactionDataAll.Code {

					// 时间超过了4分钟，存在异常，不再监控
					if stockTransactionDataAll.Date.Minute()-lastStockTransactionDataAll.Date.Minute() > 4 {
						var message string = fmt.Sprintf("股票[%s]的实时收盘价，最近的时间是[%s]，上一个时间是[%s]，超过了4分钟，存在异常，不再监控",
							stockTransactionDataAll.Code, stockTransactionDataAll.Date, lastStockTransactionDataAll.Date)
						io.Infoln(message)
						continue
					}

					// 监控股票直线上涨或下跌
					if properties.PythonProperties_.WatchStockTransactionMinuteDataStraightLineIncreaseDecreaseEnable {
						// 出现了快速上涨
						var risePercentage float64 = (stockTransactionDataAll.ClosePrice - lastStockTransactionDataAll.ClosePrice) / lastStockTransactionDataAll.ClosePrice * 100
						if risePercentage >= properties.PythonProperties_.WatchStockTransactionMinuteDataRiseOrFallThreshold {
							var currentTime time.Time = time.Now()
							var message string = fmt.Sprintf("当前时间[%s]，股票[%s]当前收盘价为[%f]，五分钟之前的收盘价为[%f]，上涨百分比为[%f]，超过了阈值[%f]",
								util.DateTimeToString(currentTime), stockTransactionDataAll.Code, stockTransactionDataAll.ClosePrice,
								lastStockTransactionDataAll.ClosePrice, risePercentage, properties.PythonProperties_.WatchStockTransactionMinuteDataRiseOrFallThreshold)
							io.Infoln(message)

							if properties.PythonProperties_.WatchStockTransactionMinuteDataEnableMessageBox {
								// 弹出提示对话框
								ui.MessageBox(message, "快速上涨")
							}
							if properties.PythonProperties_.WatchStockTransactionMinuteDataEnableSoundAudio {
								// 播放报警音频
								domain.Manager_.AudioManager_.Sound(properties.PythonProperties_.WatchStockTransactionMinuteDataAudioFilePath)
							}
							if properties.PythonProperties_.WatchStockTransactionMinuteDataEnableSaveInDatabase {
								// 添加记录
								var straightLineIncreaseDecrease model.StraightLineIncreaseDecrease = model.StraightLineIncreaseDecrease{}
								straightLineIncreaseDecrease.Code = stockTransactionDataAll.Code
								straightLineIncreaseDecrease.TransactionType = constants.Stock
								var stockInfo model.StockInfo = _stockTransactionDataAllServiceImpl._stockInfoDaoImpl.FindByCode(stockTransactionDataAll.Code)
								straightLineIncreaseDecrease.Name = stockInfo.Name
								straightLineIncreaseDecrease.Message = message
								straightLineIncreaseDecrease.CreateTime = currentTime
								_stockTransactionDataAllServiceImpl.BaseServiceImpl._straightLineIncreaseDecreaseDaoImpl.Add(&straightLineIncreaseDecrease)
							}
						}
					}

					// 监控股票突破或跌破某一价位
					if properties.PythonProperties_.WatchStockTransactionMinuteDataUpOrDownPriceEnable {
						var monitorCurrentPrice model.MonitorCurrentPrice
						monitorCurrentPrice.TransactionType = constants.Stock
						monitorCurrentPrice.Available = constants.AVAILABLE
						monitorCurrentPrice.Code = stockTransactionDataAll.Code
						var monitorCurrentPriceArray model.MonitorCurrentPriceArray = _stockTransactionDataAllServiceImpl.BaseServiceImpl._monitorCurrentPriceDaoImpl.Find(monitorCurrentPrice)
						if monitorCurrentPriceArray != nil && len(monitorCurrentPriceArray) > 0 {
							for _, monitorCurrentPrice := range monitorCurrentPriceArray {
								if monitorCurrentPrice.Code != stockTransactionDataAll.Code {
									continue
								}

								var currentClosePrice float64 = stockTransactionDataAll.ClosePrice
								var stockInfo model.StockInfo = _stockTransactionDataAllServiceImpl._stockInfoDaoImpl.FindByCode(stockTransactionDataAll.Code)
								if monitorCurrentPrice.Comparison == constants.GREATER_THAN_OR_EQUAL &&
									monitorCurrentPrice.MonitorPrice >= currentClosePrice {

									var currentTime time.Time = time.Now()
									var message string = fmt.Sprintf("当前时间[%s]，股票[%s %s]当前收盘价为[%f]，向上突破了设置的价格[%f]",
										util.DateTimeToString(currentTime), stockTransactionDataAll.Code, stockInfo.Name, currentClosePrice, monitorCurrentPrice.MonitorPrice)
									io.Infoln(message)

									if properties.PythonProperties_.WatchCommodityFutureMinuteDataEnableMessageBox {
										// 弹出提示对话框
										ui.MessageBox(message, "向上突破")
									}
									if properties.PythonProperties_.WatchCommodityFutureMinuteDataEnableSoundAudio {
										// 播放报警音频
										domain.Manager_.AudioManager_.Sound(properties.PythonProperties_.WatchCommodityFutureMinuteDataAudioFilePath)
									}
									if properties.PythonProperties_.WatchCommodityFutureMinuteDataEnableSaveInDatabase {
										// 添加记录
										var straightLineIncreaseDecrease model.StraightLineIncreaseDecrease = model.StraightLineIncreaseDecrease{}
										straightLineIncreaseDecrease.Code = stockTransactionDataAll.Code
										straightLineIncreaseDecrease.TransactionType = constants.Stock
										straightLineIncreaseDecrease.Name = stockInfo.Name
										straightLineIncreaseDecrease.Message = message
										straightLineIncreaseDecrease.CreateTime = currentTime
										_stockTransactionDataAllServiceImpl.BaseServiceImpl._straightLineIncreaseDecreaseDaoImpl.Add(&straightLineIncreaseDecrease)
									}
								}
								if monitorCurrentPrice.Comparison == constants.LESS_THAN_OR_EQUAL &&
									monitorCurrentPrice.MonitorPrice <= currentClosePrice {

									var currentTime time.Time = time.Now()
									var message string = fmt.Sprintf("当前时间[%s]，股票[%s %s]当前收盘价为[%f]，向下跌破了设置的价格[%f]",
										util.DateTimeToString(currentTime), stockTransactionDataAll.Code, stockInfo.Name, currentClosePrice, monitorCurrentPrice.MonitorPrice)
									io.Infoln(message)

									if properties.PythonProperties_.WatchCommodityFutureMinuteDataEnableMessageBox {
										// 弹出提示对话框
										ui.MessageBox(message, "向下跌破")
									}
									if properties.PythonProperties_.WatchCommodityFutureMinuteDataEnableSoundAudio {
										// 播放报警音频
										domain.Manager_.AudioManager_.Sound(properties.PythonProperties_.WatchCommodityFutureMinuteDataAudioFilePath)
									}
									if properties.PythonProperties_.WatchCommodityFutureMinuteDataEnableSaveInDatabase {
										// 添加记录
										var straightLineIncreaseDecrease model.StraightLineIncreaseDecrease = model.StraightLineIncreaseDecrease{}
										straightLineIncreaseDecrease.Code = stockTransactionDataAll.Code
										straightLineIncreaseDecrease.TransactionType = constants.Stock
										straightLineIncreaseDecrease.Name = stockInfo.Name
										straightLineIncreaseDecrease.Message = message
										straightLineIncreaseDecrease.CreateTime = currentTime
										_stockTransactionDataAllServiceImpl.BaseServiceImpl._straightLineIncreaseDecreaseDaoImpl.Add(&straightLineIncreaseDecrease)
									}

									if properties.PythonProperties_.WatchCommodityFutureMinuteDataEnableMessageBox {
										// 弹出提示对话框
										ui.MessageBox(message, "快速下跌")
									}
									if properties.PythonProperties_.WatchCommodityFutureMinuteDataEnableSoundAudio {
										// 播放报警音频
										domain.Manager_.AudioManager_.Sound(properties.PythonProperties_.WatchCommodityFutureMinuteDataAudioFilePath)
									}
									if properties.PythonProperties_.WatchCommodityFutureMinuteDataEnableSaveInDatabase {
										// 添加记录
										var straightLineIncreaseDecrease model.StraightLineIncreaseDecrease = model.StraightLineIncreaseDecrease{}
										straightLineIncreaseDecrease.Code = stockTransactionDataAll.Code
										straightLineIncreaseDecrease.TransactionType = constants.Stock
										straightLineIncreaseDecrease.Name = stockInfo.Name
										straightLineIncreaseDecrease.Message = message
										straightLineIncreaseDecrease.CreateTime = currentTime
										_stockTransactionDataAllServiceImpl.BaseServiceImpl._straightLineIncreaseDecreaseDaoImpl.Add(&straightLineIncreaseDecrease)
									}
								}
							}
						}
					}
				}
			}
		}
	}

	io.Infoln("监控股票的结束时间：" + time.Now().Format("2006-01-02 15:04:05"))
	var serviceResult pDomain.ServiceResult = pDomain.ServiceResult{}
	serviceResult.Code = pConstants.OperationSuccessCode
	serviceResult.Message = pConstants.OperationSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = &stockTransactionDataAllArray
	return &serviceResult
}
