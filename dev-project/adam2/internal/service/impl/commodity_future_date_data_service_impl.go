package impl

import (
	"adam2/internal/model"
	pConstants "anubis-framework/pkg/constants"
	pDomain "anubis-framework/pkg/domain"
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/util"
	"time"
)

type CommodityFutureDateDataServiceImpl struct {
	*BaseServiceImpl
}

func GetCommodityFutureDateDataServiceImpl() *CommodityFutureDateDataServiceImpl {
	return &CommodityFutureDateDataServiceImpl{GetBaseServiceImpl()}
}

// 根据code、开始时间和结束时间查询记录
func (_commodityFutureDateDataServiceImpl CommodityFutureDateDataServiceImpl) FindByCodeBetweenDate(code string, beginDate time.Time, endDate time.Time) *pDomain.ServiceResult {
	var _beginDate string = util.DateToString(beginDate)
	var _endDate string = util.DateToString(endDate)
	io.Infoln("根据code[%s]、开始时间[%s]和结束时间[%s]查询记录", code, _beginDate, _endDate)

	var commodityFutureDateDataArray model.CommodityFutureDateDataArray = _commodityFutureDateDataServiceImpl._commodityFutureDateDataDaoImpl.FindByCodeBetweenDate(code, _beginDate, _endDate)
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}
	serviceResult.Code = pConstants.OperationSuccessCode
	serviceResult.Message = pConstants.OperationSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = commodityFutureDateDataArray
	return serviceResult
}
