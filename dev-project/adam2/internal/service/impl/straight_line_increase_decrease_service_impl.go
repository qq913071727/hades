package impl

import (
	"adam2/internal/model"
	pConstants "anubis-framework/pkg/constants"
	pDomain "anubis-framework/pkg/domain"
)

type StraightLineIncreaseDecreaseServiceImpl struct {
	*BaseServiceImpl
}

// 获取service的实现类
func GetStraightLineIncreaseDecreaseServiceImpl() *StraightLineIncreaseDecreaseServiceImpl {
	return &StraightLineIncreaseDecreaseServiceImpl{GetBaseServiceImpl()}
}

// 添加
func (_straightLineIncreaseDecreaseServiceImpl StraightLineIncreaseDecreaseServiceImpl) Add(straightLineIncreaseDecrease *model.StraightLineIncreaseDecrease) *pDomain.ServiceResult {
	_straightLineIncreaseDecreaseServiceImpl.Add(straightLineIncreaseDecrease)

	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}
	serviceResult.Code = pConstants.OperationSuccessCode
	serviceResult.Message = pConstants.OperationSuccessMessage
	serviceResult.Success = true
	return serviceResult
}
