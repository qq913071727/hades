package impl

import (
	"adam2/internal/model"
	pConstants "anubis-framework/pkg/constants"
	"anubis-framework/pkg/domain"
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/util"
	"time"
)

type CommodityFutureWeekDataServiceImpl struct {
	*BaseServiceImpl
}

func GetCommodityFutureWeekDataServiceImpl() *CommodityFutureWeekDataServiceImpl {
	return &CommodityFutureWeekDataServiceImpl{GetBaseServiceImpl()}
}

// 根据code、开始时间和结束时间查询记录
func (_commodityFutureWeekDataServiceImpl CommodityFutureWeekDataServiceImpl) FindByCodeBetweenDate(code string, beginDate time.Time, endDate time.Time) *domain.ServiceResult {
	var _beginDate string = util.DateToString(beginDate)
	var _endDate string = util.DateToString(endDate)
	io.Infoln("根据code[%s]、开始时间[%s]和结束时间[%s]查询记录", code, _beginDate, _endDate)

	var commodityFutureWeekDataArray model.CommodityFutureWeekDataArray = _commodityFutureWeekDataServiceImpl._commodityFutureWeekDataDaoImpl.FindByCodeBetweenDate(code, _beginDate, _endDate)
	var serviceResult *domain.ServiceResult = &domain.ServiceResult{}
	serviceResult.Code = pConstants.OperationSuccessCode
	serviceResult.Message = pConstants.OperationSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = commodityFutureWeekDataArray
	return serviceResult
}
