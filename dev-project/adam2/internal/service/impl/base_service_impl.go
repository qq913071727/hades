package impl

import (
	"adam2/internal/constants"
	"adam2/internal/dao/impl"
	"adam2/internal/domain"
	"adam2/internal/model"
	"adam2/internal/properties"
	iUtil "adam2/internal/util"
	pConstants "anubis-framework/pkg/constants"
	pDomain "anubis-framework/pkg/domain"
	"anubis-framework/pkg/util"
)

// service实现类
type BaseServiceImpl struct {
	_commodityFutureDateDataDaoImpl      *impl.CommodityFutureDateDataDaoImpl
	_commodityFutureWeekDataDaoImpl      *impl.CommodityFutureWeekDataDaoImpl
	_commodityFutureInfoDaoImpl          *impl.CommodityFutureInfoDaoImpl
	_hisRobotAccountLogDaoImpl           *impl.HisRobotAccountLogDaoImpl
	_hisRobotTransactionRecordDaoImpl    *impl.HisRobotTransactionRecordDaoImpl
	_mdlClosePriceMa5GoldCrossDaoImpl    *impl.MdlClosePriceMa5GoldCrossDaoImpl
	_mdlEtfBullShortLineUpDaoImpl        *impl.MdlEtfBullShortLineUpDaoImpl
	_mdlHeiKinAshiUpDownDaoImpl          *impl.MdlHeiKinAshiUpDownDaoImpl
	_mdlKdGoldCrossDaoImpl               *impl.MdlKdGoldCrossDaoImpl
	_mdlMacdGoldCrossDaoImpl             *impl.MdlMacdGoldCrossDaoImpl
	_modelDaoImpl                        *impl.ModelDaoImpl
	_real7StockTransactionRecordDaoImpl  *impl.Real7StockTransactionRecordDaoImpl
	_real7TransactionConditionDaoImpl    *impl.Real7TransactionConditionDaoImpl
	_robot7AccountDaoImpl                *impl.Robot7AccountDaoImpl
	_robot7StockFilterDaoImpl            *impl.Robot7StockFilterDaoImpl
	_robot7StockTransactRecordDaoImpl    *impl.Robot7StockTransactRecordDaoImpl
	_stockInfoDaoImpl                    *impl.StockInfoDaoImpl
	_stockTransactionDataAllDaoImpl      *impl.StockTransactionDataAllDaoImpl
	_stockWeekDaoImpl                    *impl.StockWeekDaoImpl
	_trainingInfoDaoImpl                 *impl.TrainingInfoDaoImpl
	_trainingTransactionDataDaoImpl      *impl.TrainingTransactionDataDaoImpl
	_straightLineIncreaseDecreaseDaoImpl *impl.StraightLineIncreaseDecreaseDaoImpl
	_monitorCurrentPriceDaoImpl          *impl.MonitorCurrentPriceDaoImpl
}

// 获取BaseServiceImpl的实例
func GetBaseServiceImpl() *BaseServiceImpl {
	return &BaseServiceImpl{
		impl.GetCommodityFutureDateDataDaoImpl(),
		impl.GetCommodityFutureWeekDataDaoImpl(),
		impl.GetCommodityFutureInfoDaoImpl(),
		impl.GetHisRobotAccountLogDaoImpl(),
		impl.GetHisRobotTransactionRecordDaoImpl(),
		impl.GetMdlClosePriceMa5GoldCrossDaoImpl(),
		impl.GetMdlEtfBullShortLineUpDaoImpl(),
		impl.GetMdlHeiKinAshiUpDownDaoImpl(),
		impl.GetMdlKdGoldCrossDaoImpl(),
		impl.GetMdlMacdGoldCrossDaoImpl(),
		impl.GetModelDaoImpl(),
		impl.GetReal7StockTransactionRecordDaoImpl(),
		impl.GetReal7TransactionConditionDaoImpl(),
		impl.GetRobot7AccountDaoImpl(),
		impl.GetRobot7StockFilterDaoImpl(),
		impl.GetRobot7StockTransactRecordDaoImpl(),
		impl.GetStockInfoDaoImpl(),
		impl.GetStockTransactionDataAllDaoImpl(),
		impl.GetStockWeekDaoImpl(),
		impl.GetTrainingInfoDaoImpl(),
		impl.GetTrainingTransactionDataDaoImpl(),
		impl.GetStraightLineIncreaseDecreaseDaoImpl(),
		impl.GetMonitorCurrentPriceDaoImpl()}
}

// 根据训练名称查询日线级别交易记录
func (_baseServiceImpl BaseServiceImpl) FindDateDataByTrainingName(trainingName string) *pDomain.ServiceResult {
	// 根据name查询
	var trainingInfo model.TrainingInfo = _baseServiceImpl._trainingInfoDaoImpl.FindByName(trainingName)

	// 根据code、开始时间和结束时间查询
	var decryptCode, _ = util.Decrypt(trainingInfo.Code, properties.TrainingProperties_.EncryptKey, properties.TrainingProperties_.IV)

	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}
	serviceResult.Code = pConstants.OperationSuccessCode
	serviceResult.Message = pConstants.OperationSuccessMessage
	serviceResult.Success = true

	var _beginDate string = util.DateToString(trainingInfo.BeginDate)
	var _endDate string = util.DateToString(trainingInfo.EndDate)

	// 股票市场
	if trainingInfo.Market == constants.StockMarket {
		var stockTransactionDataAllArray model.StockTransactionDataAllArray = _baseServiceImpl._stockTransactionDataAllDaoImpl.FindByCodeBetweenDate(decryptCode, _beginDate, _endDate)

		// 数据转换
		var kLineData domain.KLineData = domain.KLineData{}
		kLineData.Code = stockTransactionDataAllArray[0].Code
		kLineData.Name = stockTransactionDataAllArray[0].Code
		//kLineData.Lines = [][6]interface{}
		for _, stockTransactionDataAll := range stockTransactionDataAllArray {
			var date_ string = util.DateToString(stockTransactionDataAll.Date)
			var row []interface{}
			row = append(row, date_, stockTransactionDataAll.OpenPrice, stockTransactionDataAll.ClosePrice,
				stockTransactionDataAll.LowestPrice, stockTransactionDataAll.HighestPrice, stockTransactionDataAll.Up, stockTransactionDataAll.Mb,
				stockTransactionDataAll.Dn, stockTransactionDataAll.Ema12, stockTransactionDataAll.Ema26, stockTransactionDataAll.Dif,
				stockTransactionDataAll.Dea, stockTransactionDataAll.Macd, stockTransactionDataAll.K, stockTransactionDataAll.D)
			kLineData.Lines = append(kLineData.Lines, row)
		}
		serviceResult.Result = kLineData
	}
	// 期货市场
	if trainingInfo.Market == constants.CommodityFutureMarket {
		var commodityFutureDateDataArray model.CommodityFutureDateDataArray = _baseServiceImpl._commodityFutureDateDataDaoImpl.FindByCodeBetweenDate(decryptCode, _beginDate, _endDate)

		// 数据转换
		var kLineData domain.KLineData = domain.KLineData{}
		kLineData.Code = commodityFutureDateDataArray[0].Code
		kLineData.Name = commodityFutureDateDataArray[0].Code
		//kLineData.Lines = [][6]float64{}
		for _, commodityFutureDateData := range commodityFutureDateDataArray {
			var date_ string = util.DateToString(commodityFutureDateData.TransactionDate)
			var row []interface{}
			row = append(row, date_, commodityFutureDateData.OpenPrice, commodityFutureDateData.ClosePrice,
				commodityFutureDateData.LowestPrice, commodityFutureDateData.HighestPrice, commodityFutureDateData.Up, commodityFutureDateData.Mb,
				commodityFutureDateData.Dn, commodityFutureDateData.Ema12, commodityFutureDateData.Ema26, commodityFutureDateData.Dif,
				commodityFutureDateData.Dea, commodityFutureDateData.Macd, commodityFutureDateData.K, commodityFutureDateData.D)
			kLineData.Lines = append(kLineData.Lines, row)
		}
		serviceResult.Result = kLineData
	}

	return serviceResult
}

func (_baseServiceImpl BaseServiceImpl) FindWeekDataByTrainingName(trainingName string) *pDomain.ServiceResult {
	// 根据name查询
	var trainingInfo model.TrainingInfo = _baseServiceImpl._trainingInfoDaoImpl.FindByName(trainingName)

	// 根据code、开始时间和结束时间查询
	var decryptCode, _ = util.Decrypt(trainingInfo.Code, properties.TrainingProperties_.EncryptKey, properties.TrainingProperties_.IV)

	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}
	serviceResult.Code = pConstants.OperationSuccessCode
	serviceResult.Message = pConstants.OperationSuccessMessage
	serviceResult.Success = true

	var _beginDate string = util.DateToString(trainingInfo.BeginDate)
	var _endDate string = util.DateToString(trainingInfo.EndDate)

	// 股票市场
	if trainingInfo.Market == constants.StockMarket {
		var stockWeekArray model.StockWeekArray = _baseServiceImpl._stockWeekDaoImpl.FindByCodeBetweenDate(decryptCode, _beginDate, _endDate)
		// 如果当前周还没有结束，则把当前交易日的数据最为最近一周的数据加入进去
		if trainingInfo.EndDate.After(stockWeekArray[len(stockWeekArray)-1].EndDate) {
			var newBeginDate = util.AfterNDay(util.DateToString(stockWeekArray[len(stockWeekArray)-1].EndDate), 1)
			var stockTransactionDataArray model.StockTransactionDataAllArray = _baseServiceImpl._stockTransactionDataAllDaoImpl.FindByCodeBetweenDate(decryptCode, newBeginDate, _endDate)
			// 如果当前周还没有结束
			if len(stockTransactionDataArray) != 0 {
				var stockWeek model.StockWeek = model.StockWeek{}
				stockWeekArray = append(stockWeekArray, &stockWeek)
				stockWeek.Code = stockTransactionDataArray[len(stockTransactionDataArray)-1].Code
				stockWeek.EndDate = stockTransactionDataArray[len(stockTransactionDataArray)-1].Date
				stockWeek.ClosePrice = stockTransactionDataArray[len(stockTransactionDataArray)-1].ClosePrice
				stockWeek.OpenPrice = stockTransactionDataArray[0].OpenPrice
				_, stockWeek.LowestPrice = iUtil.MaxMinLowestPriceInStockTransactionDataAllArray(stockTransactionDataArray)
				stockWeek.HighestPrice, _ = iUtil.MaxMinHighestPriceInStockTransactionDataAllArray(stockTransactionDataArray)
				var up, mb, dn float64 = iUtil.CalculateLatestBollForStockWeekArray(stockWeekArray)
				stockWeek.Up = up
				stockWeek.Mb = mb
				stockWeek.Dn = dn
				stockWeek.Ema12 = 2.0*stockTransactionDataArray[len(stockTransactionDataArray)-1].ClosePrice/(12.0+1.0) + 11.0*stockWeekArray[len(stockWeekArray)-2].Ema12/(12.0+1.0)
				stockWeek.Ema26 = 2.0*stockTransactionDataArray[len(stockTransactionDataArray)-1].ClosePrice/(26.0+1.0) + 25.0*stockWeekArray[len(stockWeekArray)-2].Ema26/(26.0+1.0)
				stockWeek.Dif = stockWeek.Ema12 - stockWeek.Ema26
				stockWeek.Dea = 2.0/(9.0+1.0)*stockWeek.Dif + 8.0/(9.0+1.0)*stockWeekArray[len(stockWeekArray)-2].Dea
				stockWeek.Macd = 2.0 * (stockWeek.Dif - stockWeek.Dea)
				var maxHighestPrice float64 = iUtil.MaxHighestPriceInStockWeekArrayWithNDate(stockWeekArray, 9)
				var minLowestPrice float64 = iUtil.MinLowestPriceInStockWeekArrayWithNDate(stockWeekArray, 9)
				stockWeek.Rsv = (stockWeek.ClosePrice - minLowestPrice) / (maxHighestPrice - minLowestPrice) * 100
				stockWeek.K = 2.0/3.0*stockWeekArray[len(stockWeekArray)-2].K + 1.0/3.0*stockWeek.Rsv
				stockWeek.D = 2.0/3.0*stockWeekArray[len(stockWeekArray)-2].D + 1.0/3.0*stockWeek.K
			}
		}

		// 数据转换
		var kLineData domain.KLineData = domain.KLineData{}
		kLineData.Code = stockWeekArray[0].Code
		kLineData.Name = stockWeekArray[0].Code
		//kLineData.Lines = [][6]float64{}
		for _, _stockWeek := range stockWeekArray {
			var date_ string = util.DateToString(_stockWeek.EndDate)
			var row []interface{}
			row = append(row, date_, _stockWeek.OpenPrice, _stockWeek.ClosePrice,
				_stockWeek.LowestPrice, _stockWeek.HighestPrice, _stockWeek.Up, _stockWeek.Mb,
				_stockWeek.Dn, _stockWeek.Ema12, _stockWeek.Ema26, _stockWeek.Dif, _stockWeek.Dea, _stockWeek.Macd,
				_stockWeek.K, _stockWeek.D)
			kLineData.Lines = append(kLineData.Lines, row)
		}
		serviceResult.Result = kLineData
	}
	// 期货市场
	if trainingInfo.Market == constants.CommodityFutureMarket {
		var commodityFutureWeekDataArray model.CommodityFutureWeekDataArray = _baseServiceImpl._commodityFutureWeekDataDaoImpl.FindByCodeBetweenDate(decryptCode, _beginDate, _endDate)
		// 如果当前周还没有结束，则把当前交易日的数据最为最近一周的数据加入进去
		if trainingInfo.EndDate.After(commodityFutureWeekDataArray[len(commodityFutureWeekDataArray)-1].EndDate) {
			var newBeginDate = util.AfterNDay(util.DateToString(commodityFutureWeekDataArray[len(commodityFutureWeekDataArray)-1].EndDate), 1)
			var commodityFutureDateDataArray model.CommodityFutureDateDataArray = _baseServiceImpl._commodityFutureDateDataDaoImpl.FindByCodeBetweenDate(decryptCode, newBeginDate, _endDate)
			// 如果当前周还没有结束
			if len(commodityFutureDateDataArray) != 0 {
				var commodityFutureWeekData model.CommodityFutureWeekData = model.CommodityFutureWeekData{}
				commodityFutureWeekDataArray = append(commodityFutureWeekDataArray, &commodityFutureWeekData)
				commodityFutureWeekData.Code = commodityFutureDateDataArray[len(commodityFutureDateDataArray)-1].Code
				commodityFutureWeekData.EndDate = commodityFutureDateDataArray[len(commodityFutureDateDataArray)-1].TransactionDate
				commodityFutureWeekData.ClosePrice = commodityFutureDateDataArray[len(commodityFutureDateDataArray)-1].ClosePrice
				commodityFutureWeekData.OpenPrice = commodityFutureDateDataArray[0].OpenPrice
				_, commodityFutureWeekData.LowestPrice = iUtil.MaxMinLowestPriceInCommodityFutureDateDataArray(commodityFutureDateDataArray)
				commodityFutureWeekData.HighestPrice, _ = iUtil.MaxMinHighestPriceInCommodityFutureDateDataArray(commodityFutureDateDataArray)
				var up, mb, dn float64 = iUtil.CalculateLatestBollForCommodityFutureWeekDataArray(commodityFutureWeekDataArray)
				commodityFutureWeekData.Up = up
				commodityFutureWeekData.Mb = mb
				commodityFutureWeekData.Dn = dn
				commodityFutureWeekData.Ema12 = 2.0*commodityFutureDateDataArray[len(commodityFutureDateDataArray)-1].ClosePrice/(12.0+1.0) + 11.0*commodityFutureWeekDataArray[len(commodityFutureWeekDataArray)-2].Ema12/(12.0+1.0)
				commodityFutureWeekData.Ema26 = 2.0*commodityFutureDateDataArray[len(commodityFutureDateDataArray)-1].ClosePrice/(26.0+1.0) + 25.0*commodityFutureWeekDataArray[len(commodityFutureWeekDataArray)-2].Ema26/(26.0+1.0)
				commodityFutureWeekData.Dif = commodityFutureWeekData.Ema12 - commodityFutureWeekData.Ema26
				commodityFutureWeekData.Dea = 2.0/(9.0+1.0)*commodityFutureWeekData.Dif + 8.0/(9.0+1.0)*commodityFutureWeekDataArray[len(commodityFutureWeekDataArray)-2].Dea
				commodityFutureWeekData.Macd = 2.0 * (commodityFutureWeekData.Dif - commodityFutureWeekData.Dea)
				var maxHighestPrice float64 = iUtil.MaxHighestPriceInCommodityFutureWeekDataArrayWithNDate(commodityFutureWeekDataArray, 9)
				var minLowestPrice float64 = iUtil.MinLowestPriceInCommodityFutureWeekDataArrayWithNDate(commodityFutureWeekDataArray, 9)
				commodityFutureWeekData.Rsv = (commodityFutureWeekData.ClosePrice - minLowestPrice) / (maxHighestPrice - minLowestPrice) * 100
				commodityFutureWeekData.K = 2.0/3.0*commodityFutureWeekDataArray[len(commodityFutureWeekDataArray)-2].K + 1.0/3.0*commodityFutureWeekData.Rsv
				commodityFutureWeekData.D = 2.0/3.0*commodityFutureWeekDataArray[len(commodityFutureWeekDataArray)-2].D + 1.0/3.0*commodityFutureWeekData.K
			}
		}

		// 数据转换
		var kLineData domain.KLineData = domain.KLineData{}
		kLineData.Code = commodityFutureWeekDataArray[0].Code
		kLineData.Name = commodityFutureWeekDataArray[0].Code
		//kLineData.Lines = [][6]float64{}
		for _, _commodityFutureWeekData := range commodityFutureWeekDataArray {
			var date_ string = util.DateToString(_commodityFutureWeekData.EndDate)
			var row []interface{}
			row = append(row, date_, _commodityFutureWeekData.OpenPrice, _commodityFutureWeekData.ClosePrice,
				_commodityFutureWeekData.LowestPrice, _commodityFutureWeekData.HighestPrice, _commodityFutureWeekData.Up, _commodityFutureWeekData.Mb,
				_commodityFutureWeekData.Dn, _commodityFutureWeekData.Ema12, _commodityFutureWeekData.Ema26, _commodityFutureWeekData.Dif,
				_commodityFutureWeekData.Dea, _commodityFutureWeekData.Macd, _commodityFutureWeekData.K, _commodityFutureWeekData.D)
			kLineData.Lines = append(kLineData.Lines, row)
		}
		serviceResult.Result = kLineData
	}

	return serviceResult
}
