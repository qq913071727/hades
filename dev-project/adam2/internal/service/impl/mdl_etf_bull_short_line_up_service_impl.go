package impl

import (
	"adam2/internal/domain"
	"adam2/internal/properties"
	"anubis-framework/pkg/io"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
)

type MdlEtfBullShortLineUpServiceImpl struct {
	*BaseServiceImpl
}

func GetMdlEtfBullShortLineUpServiceImpl() *MdlEtfBullShortLineUpServiceImpl {
	return &MdlEtfBullShortLineUpServiceImpl{GetBaseServiceImpl()}
}

// 创建收益率(mdl_etf_bull_short_line_up)和相关系数关系的散点图
func (_mdlEtfBullShortLineUpServiceImpl *MdlEtfBullShortLineUpServiceImpl) CreateProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterByMdlEtfBullShortLineUp() error {
	io.Infoln("开始创建收益率(mdl_etf_bull_short_line_up)和相关系数关系的散点图")

	var beginDate string = properties.ChartProperties_.ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterBeginDateByMdlEtfBullShortLineUp
	var endDate string = properties.ChartProperties_.ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterEndDateByMdlEtfBullShortLineUp
	var path string = properties.ChartProperties_.ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterPathByMdlEtfBullShortLineUp
	var suffix string = properties.ChartProperties_.ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterSuffixByMdlEtfBullShortLineUp

	var profitAndLossRateAndCorrelation250WithAvgClosePriceArray domain.ProfitAndLossRateAndCorrelation250WithAvgClosePriceArray = _mdlEtfBullShortLineUpServiceImpl._mdlEtfBullShortLineUpDaoImpl.FindProfitAndLossRateAndCorrelation250WithAvgClosePrice()

	pts := plotter.XYs{}
	if len(profitAndLossRateAndCorrelation250WithAvgClosePriceArray) != 0 {
		for _, profitOrLossRateAndCorrelation250WithAvgClosePrice := range profitAndLossRateAndCorrelation250WithAvgClosePriceArray {
			xy := plotter.XY{profitOrLossRateAndCorrelation250WithAvgClosePrice.ProfitAndLossRate, profitOrLossRateAndCorrelation250WithAvgClosePrice.Correlation250WithAvgClosePrice}
			pts = append(pts, xy)
		}
	}

	p := plot.New()

	p.Title.Text = "收益率和相关系数"
	p.X.Label.Text = "收益率"
	p.Y.Label.Text = "相关系数"

	scatter2, _ := plotter.NewScatter(pts)
	p.Add(scatter2)

	if err := p.Save(8*vg.Inch, 8*vg.Inch, path+"_"+beginDate+"_"+endDate+suffix); err != nil {
		io.Fatalf(err.Error())
	}

	io.Infoln("创建收益率(mdl_etf_bull_short_line_up)和相关系数关系的散点图结束")
	return nil
}
