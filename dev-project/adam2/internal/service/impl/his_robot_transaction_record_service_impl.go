package impl

import (
	"adam2/internal/domain"
	"adam2/internal/properties"
	"anubis-framework/pkg/io"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
)

type HisRobotTransactionRecordServiceImpl struct {
	*BaseServiceImpl
}

func GetHisRobotTransactionRecordServiceImpl() *HisRobotTransactionRecordServiceImpl {
	return &HisRobotTransactionRecordServiceImpl{GetBaseServiceImpl()}
}

// 创建收益率(his_robot_transact_record)和相关系数关系的散点图
func (_hisRobotTransactionRecordServiceImpl *HisRobotTransactionRecordServiceImpl) CreateProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterByHisRobotTransactRecord() error {
	io.Infoln("开始创建收益率(his_robot_transact_record)和相关系数关系的散点图")

	var beginDate string = properties.ChartProperties_.ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterBeginDateByHisRobotTransactRecord
	var endDate string = properties.ChartProperties_.ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterEndDateByHisRobotTransactRecord
	var path string = properties.ChartProperties_.ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterPathByHisRobotTransactRecord
	var suffix string = properties.ChartProperties_.ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterSuffixByHisRobotTransactRecord

	var profitAndLossRateAndCorrelation250WithAvgClosePriceArray domain.ProfitAndLossRateAndCorrelation250WithAvgClosePriceArray = _hisRobotTransactionRecordServiceImpl._hisRobotTransactionRecordDaoImpl.FindProfitAndLossRateAndCorrelation250WithAvgClosePriceByModelId(properties.ChartProperties_.ProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterModelIdByHisRobotTransactRecord)

	pts := plotter.XYs{}
	if len(profitAndLossRateAndCorrelation250WithAvgClosePriceArray) != 0 {
		for _, profitOrLossRateAndCorrelation250WithAvgClosePrice := range profitAndLossRateAndCorrelation250WithAvgClosePriceArray {
			xy := plotter.XY{profitOrLossRateAndCorrelation250WithAvgClosePrice.ProfitAndLossRate, profitOrLossRateAndCorrelation250WithAvgClosePrice.Correlation250WithAvgClosePrice}
			pts = append(pts, xy)
		}
	}

	p := plot.New()

	p.Title.Text = "收益率和相关系数(250日)"
	p.X.Label.Text = "收益率"
	p.Y.Label.Text = "相关系数(250日)"

	scatter2, _ := plotter.NewScatter(pts)
	p.Add(scatter2)

	if err := p.Save(8*vg.Inch, 8*vg.Inch, path+"_"+beginDate+"_"+endDate+suffix); err != nil {
		io.Fatalf(err.Error())
	}

	io.Infoln("创建收益率(his_robot_transact_record)和相关系数关系的散点图结束")
	return nil
}
