package impl

import (
	"adam2/internal/model"
	"adam2/internal/properties"
	"adam2/internal/service/helper"
	pIo "anubis-framework/pkg/io"
	"anubis-framework/pkg/util"
	"time"
)

type Real7StockTransactionRecordServiceImpl struct {
	*BaseServiceImpl
}

// 获取service的实现类
func GetReal7StockTransactionRecordServiceImpl() *Real7StockTransactionRecordServiceImpl {
	return &Real7StockTransactionRecordServiceImpl{GetBaseServiceImpl()}
}

// 实时地判断买卖条件，给出交易建议
func (_real7StockTransactionRecordServiceImpl *Real7StockTransactionRecordServiceImpl) RealTimeJudgeConditionAndGiveSuggestion() error {
	pIo.Infoln("开始实时地判断买卖条件，给出交易建议")

	for {
		var currentTime time.Time = time.Now()
		year, month, day := time.Now().Date()
		var beginTime time.Time = util.StringToTime(properties.Real7Properties_.ScheduleBeginTime)
		beginHour, beginMinute, beginSecond := beginTime.Clock()
		var endTime time.Time = util.StringToTime(properties.Real7Properties_.ScheduleEndTime)
		endHour, endMinute, endSecond := endTime.Clock()
		scheduleBeginTime := time.Date(year, month, day, beginHour, beginMinute, beginSecond, 0, time.Now().Location())
		scheduleEndTime := time.Date(year, month, day, endHour, endMinute, endSecond, 0, time.Now().Location())
		if currentTime.Before(scheduleBeginTime) {
			time.Sleep(5 * time.Minute)
			continue
		}
		if currentTime.After(scheduleEndTime) {
			return nil
		}

		var stockTransactionDataAllArray model.StockTransactionDataAllArray
		if properties.Real7Properties_.Debug == false {
			// 获取所有的实时数据
			stockTransactionDataAllArray = helper.GetXueQiuApiResultArray()
		} else {
			// 获取所有的历史数据
			stockTransactionDataAllArray = helper.GetStockTransactionDataForDebug()
		}

		// 判断当前交易日，所有股票的平均价收盘价是否在牛熊线之上。true表示是，false表示否
		var isUpBullShortLine, averageClosePriceAndMaBullShortLineByBias = _real7StockTransactionRecordServiceImpl._real7TransactionConditionDaoImpl.IsBullShortLineUp(stockTransactionDataAllArray)
		// 判断当前交易日，所有股票的平均年线是否单调不递减(平均年线只保留小数点后2位，并做了平滑处理，时间窗口为20)
		var isAverageMa250NotDecrementSmooth bool = _real7StockTransactionRecordServiceImpl._real7TransactionConditionDaoImpl.IsAverageMa250NotDecrement_smooth(properties.Real7Properties_.TransactionDate, properties.Real7Properties_.NotDecrementDateNumber, averageClosePriceAndMaBullShortLineByBias)
		// 返回real7_stock_transaction_record表中sell_date为空的记录数
		var count int = _real7StockTransactionRecordServiceImpl._real7StockTransactionRecordDaoImpl.CountBySellDateNull()

		if (isUpBullShortLine == true && isAverageMa250NotDecrementSmooth == true) || count > 0 {
			pIo.Infoln("当前股票市场，按照牛市处理；或者不是牛市，但还有股票为卖出")

			// 更新字段：xr、close_price_out_of_limit、filter_type、direction、accumulative_profit_loss、is_transaction_date、suspension、variance_type、t.variance为空
			_real7StockTransactionRecordServiceImpl._real7TransactionConditionDaoImpl.ResetReal7TransactionCondition()

			// 过滤条件：过滤这个价格区间以内的股票
			_real7StockTransactionRecordServiceImpl._real7TransactionConditionDaoImpl.FilterByLessThanClosePrice(stockTransactionDataAllArray, properties.Real7Properties_.ClosePriceStart, properties.Real7Properties_.ClosePriceEnd)

			// 过滤条件：判断是否是涨停板
			_real7StockTransactionRecordServiceImpl._real7TransactionConditionDaoImpl.FilterByLimitUp(properties.Real7Properties_.TransactionDate, stockTransactionDataAllArray)

			// 过滤条件：判断用哪根均线作为牛熊线，牛熊线以下的股票都过滤掉。如果在牛熊线以上则准备方差类型和方差值
			_real7StockTransactionRecordServiceImpl._real7TransactionConditionDaoImpl.FilterByBiasThreshold(stockTransactionDataAllArray, properties.Real7Properties_.BiasThresholdTop, properties.Real7Properties_.BiasThresholdBottom)

			// 过滤条件：如果这只股票在当天除权了，则过滤掉
			_real7StockTransactionRecordServiceImpl._real7TransactionConditionDaoImpl.FilterByXrByCurrentDate(stockTransactionDataAllArray, properties.Real7Properties_.TransactionDate)

			// 过滤条件：更新real7_transaction_condition表的字段：accumulative_profit_rate和filter_type。单位：元
			_real7StockTransactionRecordServiceImpl._real7TransactionConditionDaoImpl.UpdateAccumulativeProfitLossAndFilterTypeInReal7TransactionCondition(stockTransactionDataAllArray, properties.Real7Properties_.TransactionDate)

			// 过滤条件：更新real7_transaction_condition表的字段：direction
			_real7StockTransactionRecordServiceImpl._real7TransactionConditionDaoImpl.UpdateDirectionInReal7TransactionCondition(stockTransactionDataAllArray, properties.Real7Properties_.TransactionDate)

			// 过滤条件：从real7_transaction_condition表中删除那些不是交易日的记录
			_real7StockTransactionRecordServiceImpl._real7TransactionConditionDaoImpl.FilterByBuyDate(stockTransactionDataAllArray)

			// 发送邮件
			_real7StockTransactionRecordServiceImpl._real7TransactionConditionDaoImpl.GiveSuggestionAndSendEmail(stockTransactionDataAllArray,
				properties.Real7Properties_.MaxHoldStockNumber, (isUpBullShortLine == true && isAverageMa250NotDecrementSmooth == true), count > 0)
		}
	}

	pIo.Infoln("实时地判断买卖条件，给出交易建议结束")
	return nil
}
