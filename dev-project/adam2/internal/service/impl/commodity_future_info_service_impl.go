package impl

import (
	"adam2/internal/model"
	pConstants "anubis-framework/pkg/constants"
	pDomain "anubis-framework/pkg/domain"
)

type CommodityFutureInfoServiceImpl struct {
	*BaseServiceImpl
}

func GetCommodityFutureInfoServiceImpl() *CommodityFutureInfoServiceImpl {
	return &CommodityFutureInfoServiceImpl{GetBaseServiceImpl()}
}

// 查询所有记录
func (_commodityFutureInfoServiceImpl CommodityFutureInfoServiceImpl) FindAll() *pDomain.ServiceResult {
	var commodityFutureInfoArray model.CommodityFutureInfoArray = _commodityFutureInfoServiceImpl._commodityFutureInfoDaoImpl.FindAll()
	var serviceResult *pDomain.ServiceResult = &pDomain.ServiceResult{}
	serviceResult.Code = pConstants.OperationSuccessCode
	serviceResult.Message = pConstants.OperationSuccessMessage
	serviceResult.Success = true
	serviceResult.Result = commodityFutureInfoArray
	return serviceResult
}
