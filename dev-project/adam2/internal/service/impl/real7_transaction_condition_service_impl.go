package impl

import (
	"adam2/internal/properties"
	"anubis-framework/pkg/io"
)

type Real7TransactionConditionServiceImpl struct {
	*BaseServiceImpl
}

// 获取service的实现类
func GetReal7TransactConditionServiceImpl() *Real7TransactionConditionServiceImpl {
	return &Real7TransactionConditionServiceImpl{GetBaseServiceImpl()}
}

// 预先计算股票的买卖条件
func (_real7TransactionConditionServiceImpl *Real7TransactionConditionServiceImpl) PreCalculateTransactionCondition() error {
	io.Infoln("开始预先计算股票的买卖条件")

	// 清空表real7_transaction_condition
	_real7TransactionConditionServiceImpl._real7TransactionConditionDaoImpl.TruncateTableReal7TransactionCondition()

	// 向real7_transaction_condition表中导入数据
	_real7TransactionConditionServiceImpl._real7TransactionConditionDaoImpl.ImportIntoReal7TransactionCondition(properties.Real7Properties_.TransactionDate)

	// 过滤条件：过滤除过权的股票
	_real7TransactionConditionServiceImpl._real7TransactionConditionDaoImpl.FilterByXr(properties.Real7Properties_.TransactionDate, properties.Real7Properties_.XrDateNumber)

	// 判断牛熊线的预先准备，计算MA（不包括当前交易日）、MACD、close_price金叉/死叉ma5、hei_kin_ashi和kd
	_real7TransactionConditionServiceImpl._real7TransactionConditionDaoImpl.PrepareForBullShortLine(properties.Real7Properties_.TransactionDate)

	io.Infoln("预先计算股票的买卖条件结束")
	return nil
}
