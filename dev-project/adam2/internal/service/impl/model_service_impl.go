package impl

import (
	"adam2/internal/properties"
	"anubis-framework/pkg/io"
)

type ModelServiceImpl struct {
	*BaseServiceImpl
}

func GetModelServiceImpl() *ModelServiceImpl {
	return &ModelServiceImpl{GetBaseServiceImpl()}
}

// 将测试数据导入到历史数据表中
func (_modelServiceImpl *ModelServiceImpl) ImportIntoHistoryDataTableFromRobot7() error {
	io.Infoln("将测试数据导入到历史数据表中")

	// 删除TEMP_开头的四个表
	_modelServiceImpl._modelDaoImpl.DropTableBeginWithTemp()

	// 将ROBOT7_开头的表中的记录导入到TEMP_开头的表中
	_modelServiceImpl._modelDaoImpl.ImportIntoTableBeginWithTempFromTableBeginWithRobot7()

	// 将TEMP_开头的表导入到HIS_开头的表中
	_modelServiceImpl._modelDaoImpl.InsertIntoTableBeginWithHisFromTableBeginWithTemp(properties.Robot7Properties_.ModelId)

	// 删除或重置ROBOT7_开头的表中的记录
	_modelServiceImpl._modelDaoImpl.TruncateOrResetTableBeginRobot7(properties.Robot7Properties_.InitAssets)

	return nil
}
