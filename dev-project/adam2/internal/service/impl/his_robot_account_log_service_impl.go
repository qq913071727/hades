package impl

import (
	"adam2/internal/domain"
	"adam2/internal/model"
	"adam2/internal/properties"
	"anubis-framework/pkg/io"
	"fmt"
	"github.com/go-echarts/go-echarts/v2/charts"
	"github.com/go-echarts/go-echarts/v2/opts"
	"github.com/go-echarts/go-echarts/v2/types"
	"os"
	"strconv"
)

type HisRobotAccountLogServiceImpl struct {
	*BaseServiceImpl
}

func GetHisRobotAccountLogServiceImpl() *HisRobotAccountLogServiceImpl {
	return &HisRobotAccountLogServiceImpl{GetBaseServiceImpl()}
}

// 根据表his_robot_account_log中每个账号的总资金，生成折线图
func (_hisRobotAccountLogServiceImpl *HisRobotAccountLogServiceImpl) CreateHisRobotAccountLogLineChart() error {
	io.Infoln("根据表his_robot_account_log中每个账号的总资金，生成折线图")

	line := charts.NewLine()
	line.SetGlobalOptions(
		charts.WithInitializationOpts(opts.Initialization{
			Theme: types.ThemeInfographic,
		}),
		charts.WithTitleOpts(opts.Title{
			Title:    "表his_robot_account_log中\n每个账号的总资金(折线图)",
			Subtitle: "",
		}),
	)

	// 根据model_id字段，返回his_robot_account_log表中date_字段，按照date_升序排列，并去重
	var dateArray *[]string = _hisRobotAccountLogServiceImpl._hisRobotAccountLogDaoImpl.FindDistinctDateByModelIdOrderByDateAsc(properties.ChartProperties_.HisRobotAccountLogLineChartModelId)

	// 根据model_id字段，返回his_robot_account_log表中robot_name字段，并去重
	var robotNameArray *[]string = _hisRobotAccountLogServiceImpl._hisRobotAccountLogDaoImpl.FindDistinctRobotNameByModelId(properties.ChartProperties_.HisRobotAccountLogLineChartModelId)
	if len(*robotNameArray) != 0 {
		// 所有账号名称和对应的所有交易记录
		var hisRobotAccountLogLineChartArray domain.HisRobotAccountLogLineChartArray
		for _, robotName := range *robotNameArray {
			// 某一个账号的所有交易日的记录
			items := make([]opts.LineData, 0)
			// 根据robot_name、model_id字段返回记录，并按照date_字段升序排列
			var hisRobotAccountLogArray model.HisRobotAccountLogArray = _hisRobotAccountLogServiceImpl._hisRobotAccountLogDaoImpl.FindByRobotNameAndModelIdOrderByDateAsc(properties.ChartProperties_.HisRobotAccountLogLineChartModelId, robotName)
			if len(hisRobotAccountLogArray) != 0 {
				for _, hisRobotAccountLog := range hisRobotAccountLogArray {
					// 每一个交易日的记录
					items = append(items, opts.LineData{Value: hisRobotAccountLog.TotalAssets / float64(properties.ChartProperties_.HisRobotAccountLogLineChartInitAssets/100)})
				}
			}
			var hisRobotAccountLogLineChart domain.HisRobotAccountLogLineChart = domain.HisRobotAccountLogLineChart{robotName, items}
			hisRobotAccountLogLineChartArray = append(hisRobotAccountLogLineChartArray, hisRobotAccountLogLineChart)
		}

		line.SetXAxis(dateArray).
			AddSeries(hisRobotAccountLogLineChartArray[0].RobotName, hisRobotAccountLogLineChartArray[0].HisRobotAccountLogArray).
			AddSeries(hisRobotAccountLogLineChartArray[1].RobotName, hisRobotAccountLogLineChartArray[1].HisRobotAccountLogArray).
			AddSeries(hisRobotAccountLogLineChartArray[2].RobotName, hisRobotAccountLogLineChartArray[2].HisRobotAccountLogArray).
			AddSeries(hisRobotAccountLogLineChartArray[3].RobotName, hisRobotAccountLogLineChartArray[3].HisRobotAccountLogArray).
			SetSeriesOptions(charts.WithLineChartOpts(opts.LineChart{Smooth: true}))
		f, _ := os.Create(properties.ChartProperties_.HisRobotAccountLogLineChartPath + "_" + strconv.Itoa(properties.ChartProperties_.HisRobotAccountLogLineChartModelId) +
			properties.ChartProperties_.HisRobotAccountLogLineChartSuffix)
		_ = line.Render(f)
		defer f.Close()
	}
	return nil
}

// 打印最大回撤
func (_hisRobotAccountLogServiceImpl *HisRobotAccountLogServiceImpl) PrintMaximumDrawdown() {
	io.Infoln("计算最大回撤")

	var maximumDrawdownArray domain.MaximumDrawdownArray = _hisRobotAccountLogServiceImpl._hisRobotAccountLogDaoImpl.CalculateMaximumDrawdown(properties.Robot7Properties_.PrintMaximumDrawdownModelId)
	if maximumDrawdownArray != nil && len(maximumDrawdownArray) != 0 {
		for _, maximumDrawdown := range maximumDrawdownArray {
			var num, _ = strconv.ParseFloat(fmt.Sprintf("%.5f", maximumDrawdown.MaximumDrawdown), 64)
			fmt.Printf("%f\n", num)
		}
	}
}
