package impl

import (
	"anubis-framework/pkg/io"
	"os/exec"
)

type ProjectServiceImpl struct {
	*BaseServiceImpl
}

// 获取service的实现类
func GetProjectServiceImpl() *ProjectServiceImpl {
	return &ProjectServiceImpl{GetBaseServiceImpl()}
}

// 使操作系统睡眠
func (_projectServiceImpl *ProjectServiceImpl) MakeOperationSystemSleep() {
	io.Infoln("使操作系统睡眠")
	cmd := exec.Command("rundll32.exe", "powrprof.dll,SetSuspendState")
	_, err := cmd.Output()
	if err != nil {
		io.Fatalf("使操作系统睡眠出错：", err)
	}
}
