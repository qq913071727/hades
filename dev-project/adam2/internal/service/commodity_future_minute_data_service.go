package service

import "adam2/internal/model"

type CommodityFutureMinuteDataService interface {
	// 监视直线上涨或直线下跌的行情
	WatchStraightLineIncreaseOrDecreaseInCommodityFutureMinuteData(commodityFutureInfoArray model.CommodityFutureInfoArray)
}
