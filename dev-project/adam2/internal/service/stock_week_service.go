package service

import (
	"anubis-framework/pkg/domain"
	"time"
)

type StockWeekService interface {

	// 根据code、开始时间和结束时间查询记录
	FindByCodeBetweenDate(code string, beginDate time.Time, endDate time.Time) *domain.ServiceResult
}
