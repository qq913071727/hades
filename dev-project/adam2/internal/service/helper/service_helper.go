package helper

import (
	"adam2/internal/dao"
	daoHelper "adam2/internal/dao/helper"
	"adam2/internal/dao/impl"
	"adam2/internal/domain"
	"adam2/internal/model"
	"adam2/internal/properties"
	"adam2/internal/util"
	pIo "anubis-framework/pkg/io"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

// ServiceHelper service层的通用方法
type ServiceHelper struct {
	_stockInfoDaoImpl        dao.StockInfoDao
	_stockTransactionDataAll dao.StockTransactionDataAllDao
}

func getSerivceHelper() *ServiceHelper {
	return &ServiceHelper{impl.GetStockInfoDaoImpl(), impl.GetStockTransactionDataAllDaoImpl()}
}

// 获取实时数据，并将数据转换为结构体domain.Data类型
func GetDataFromXueQiuApi(url string) domain.XueQiuApiResult {
	pIo.Infoln("获取实时数据，并将数据转换为结构体domain.Data类型。接口：[%s]", url)

	response, httpError := http.Get(url)
	if httpError != nil {
		pIo.Fatalf("调用接口[%s]报错：", properties.Real7Properties_.Url, httpError)
	}
	defer func(body io.ReadCloser) {
		closeError := body.Close()
		if closeError != nil {
			pIo.Fatalf("关闭接口的输入流时报错：", closeError)
		}
	}(response.Body)
	body, ioError := ioutil.ReadAll(response.Body)
	if ioError != nil {
		pIo.Fatalf("解析接口返回值时报错：", ioError)
	}
	var bodyString string = string(body)
	var xueQiuApiResult domain.XueQiuApiResult
	jsonError := json.Unmarshal([]byte(bodyString), &xueQiuApiResult)
	if jsonError != nil {
		pIo.Fatalf("将json字符串转换为struct时报错：", jsonError)
	}

	return xueQiuApiResult
}

// 获取所有的实时数据，并以数据形式返回
func GetXueQiuApiResultArray() model.StockTransactionDataAllArray {
	pIo.Infoln("获取所有的实时数据，并以数据形式返回")

	// 获取stock_info表中的所有记录
	var stockInfoArray model.StockInfoArray
	stockInfoArray, _ = getSerivceHelper()._stockInfoDaoImpl.FindAll()

	// 这个地方可以考虑用多线程
	var stockTransactionDataAllArray model.StockTransactionDataAllArray
	for _, stockInfo := range stockInfoArray {
		// 获取实时数据，并将数据转换为结构体XueQiuApiResult类型数组
		var xueQiuApiResult domain.XueQiuApiResult = GetDataFromXueQiuApi(properties.Real7Properties_.Url + strings.Replace(stockInfo.UrlParam, ".", "", -1))
		// 将domain.XueQiuApiResult转换为model.StockTransactionDataAll类型
		var stockTransactionData *model.StockTransactionDataAll = util.XueQiuApiResultToStockTransactionDataAll(xueQiuApiResult)
		if stockTransactionData != nil {
			stockTransactionDataAllArray = append(stockTransactionDataAllArray, stockTransactionData)
		}
	}

	return stockTransactionDataAllArray
}

// 为了debug，从数据库中获取某一个交易日的数据
func GetStockTransactionDataForDebug() model.StockTransactionDataAllArray {
	pIo.Infoln("为了debug，从数据库中获取某一个交易日的数据")

	// 获取stock_info表中的所有记录
	var stockTransactionDataAllArray model.StockTransactionDataAllArray = getSerivceHelper()._stockTransactionDataAll.FindByDate(properties.Real7Properties_.TransactionDate)
	return stockTransactionDataAllArray
}

// 打印买卖建议
func PrintBuyAndSuggestion(currentDate string) error {
	var printText string
	var err error
	printText, err = daoHelper.PrintBuySellSuggestionByJson_bias(currentDate)
	if err != nil {
		pIo.Fatalf("打印买卖建议时报错：%s", err.Error())
		return err
	} else {
		pIo.Infoln("买卖建议：\n%s", printText)
		return nil
	}
}
