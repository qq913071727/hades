package service

type HisRobotAccountLogService interface {
	// 根据表his_robot_account_log中每个账号的总资金，生成折线图
	CreateHisRobotAccountLogLineChart() error

	// 打印最大回撤
	PrintMaximumDrawdown()
}
