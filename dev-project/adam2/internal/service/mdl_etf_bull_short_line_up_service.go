package service

type MdlEtfBullShortLineUpService interface {
	// 创建收益率(mdl_etf_bull_short_line_up)和相关系数关系的散点图
	CreateProfitAndLossRateAndCorrelation250WithAvgClosePriceScatterByMdlEtfBullShortLineUp() error
}
