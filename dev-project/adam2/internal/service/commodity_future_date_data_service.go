package service

import (
	pDomain "anubis-framework/pkg/domain"
	"time"
)

type CommodityFutureDateDataService interface {

	// 根据code、开始时间和结束时间查询记录
	FindByCodeBetweenDate(code string, beginDate time.Time, endDate time.Time) *pDomain.ServiceResult
}
