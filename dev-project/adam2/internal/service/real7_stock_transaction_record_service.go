package service

type Real7StockTransactionRecordService interface {
	// 实时地判断买卖条件，给出交易建议
	RealTimeJudgeConditionAndGiveSuggestion() error
}
