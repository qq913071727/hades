package service

import (
	"adam2/internal/model"
	pDomain "anubis-framework/pkg/domain"
	"time"
)

type StockTransactionDataAllService interface {
	// 获取开始日期和结束日期之间的日期（去重），并按照日期升序排列
	GetDistinctDateBetwenDateOrderByDateAsc(beginDate string, endDate string) ([]string, error)

	// 创建平均收盘价和各个平均均线的折线图（经过平滑处理后的）
	CreateAverageClosePriceAndMALineChart_smooth()

	// 创建平均收盘价和牛熊线（bias决定）的折线图
	CreateAverageClosePriceAndMABullShortLineByBiasLineChart()

	// 根据code、开始时间和结束时间查询记录
	FindByCodeBetweenDate(code string, beginDate time.Time, endDate time.Time) *pDomain.ServiceResult

	// 查询所有code
	FindDistinctCode() *pDomain.ServiceResult

	// 监视直线上涨或直线下跌的行情
	WatchStraightLineIncreaseOrDecreaseInStockTransactionMinuteData(lastStockTransactionDataAllArray *model.StockTransactionDataAllArray) *pDomain.ServiceResult
}
