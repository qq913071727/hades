package service

type Real7TransactionConditionService interface {
	// 预先计算股票的买卖条件
	PreCalculateTransactionCondition() error
}
