package service

import (
	"adam2/internal/vo"
	pDomain "anubis-framework/pkg/domain"
)

type TrainingInfoService interface {

	// 添加
	Add(addTrainingInfoVo *vo.AddTrainingInfoVo) *pDomain.ServiceResult

	// 根据name判断记录是否已经存在
	ExistByName(name string) *pDomain.ServiceResult

	// 根据name查询
	FindByName(name string) *pDomain.ServiceResult

	// 分页显示
	Page(name string, pageNo int, pageSize int) *pDomain.ServiceResult

	// 根据name更新status
	UpdateStatusByName(name string, status int) *pDomain.ServiceResult

	// 删除训练和对应的交易记录
	DeleteTraining(name string) *pDomain.ServiceResult

	// 根据trainingName。返回下一个交易日的K线数据，更新trng_info表
	NextDate(name string) *pDomain.ServiceResult
}
