package service

import (
	pDomain "anubis-framework/pkg/domain"
)

type CommodityFutureInfoService interface {

	// 查询所有记录
	FindAll() *pDomain.ServiceResult
}
