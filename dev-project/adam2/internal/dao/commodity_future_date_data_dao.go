package dao

import (
	"adam2/internal/domain"
	"adam2/internal/model"
	"time"
)

type CommodityFutureDateDataDao interface {

	// 随机选取一个期货code
	FindRandomCommodityFutureCode(beginDate string, enDate string) string

	// 根据code和transactionDate，查询前一个交易日的日期
	FindBeforeOneByCodeAndTransactionDate(code string, transactionDate string) time.Time

	// 查询某个期货的最大日期和最小日期
	FindMinDateAndMaxDate(code string) domain.MaxDataAndMinData

	// 根据code、开始时间和结束时间查询记录
	FindByCodeBetweenDate(code string, beginDate string, endDate string) model.CommodityFutureDateDataArray

	// 查询某个期货下一个交易日记录
	FindNextByCodeAndDate(code string, date string) model.CommodityFutureDateData

	// 根据code和transaction_date查询记录
	FindByCodeAndTransactionDate(code string, transactionDate string) model.CommodityFutureDateData
}
