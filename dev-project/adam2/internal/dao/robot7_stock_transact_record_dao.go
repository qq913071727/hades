package dao

type Robot7StockTransactRecordDao interface {

	// 返回表名
	FindTableName() string

	// 卖股票
	SellOrBuy_stock(currentDate string, mandatoryStopLoss bool, mandatoryStopLossRate float64)

	// 卖股票/ETF
	SellOrBuy_all(currentDate string, mandatoryStopLoss bool, mandatoryStopLossRate float64)

	// 买股票。用于做多
	BuyOrSell_stock(date string, transactionStrategy int, maxHoldStockNumberInBull int)

	// ETF加仓
	AddPosition(date string, maxHoldStockNumberInShort int)

	// 买ETF。用于做多
	BuyOrSell_etf(date string, transactionStrategy int, maxHoldStockNumberInShort int)
}
