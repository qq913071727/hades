package dao

import "adam2/internal/model"

type StraightLineIncreaseDecreaseDao interface {

	// 添加
	Add(straightLineIncreaseDecrease *model.StraightLineIncreaseDecrease) error
}
