package dao

type ModelDao interface {
	// 返回表名
	FindTableName() string

	// 删除TEMP_开头的表
	DropTableBeginWithTemp()

	// 将ROBOT7_开头的表中的记录导入到TEMP_开头的表中
	ImportIntoTableBeginWithTempFromTableBeginWithRobot7()

	// 将TEMP_开头的表导入到HIS_开头的表中
	InsertIntoTableBeginWithHisFromTableBeginWithTemp(modelId int)

	// 删除或重置ROBOT7_开头的表中的记录
	TruncateOrResetTableBeginRobot7(initAssets int)
}
