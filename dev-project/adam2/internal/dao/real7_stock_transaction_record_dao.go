package dao

type Real7StockTransactionRecordDao interface {

	// 返回real7_stock_transaction_record表中sell_date为空的记录数
	CountBySellDateNull() int
}
