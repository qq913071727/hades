package dao

import "adam2/internal/domain"

type MdlEtfBullShortLineUpDao interface {

	// 返回表名
	FindTableName() string

	// 查找收益率和相关系数
	FindProfitAndLossRateAndCorrelation250WithAvgClosePrice() domain.ProfitAndLossRateAndCorrelation250WithAvgClosePriceArray
}
