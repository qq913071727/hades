package dao

import "adam2/internal/model"

type MonitorCurrentPrice interface {
	// 根据条件，查询记录
	Find(monitorCurrentPrice model.MonitorCurrentPrice) model.MonitorCurrentPriceArray
}
