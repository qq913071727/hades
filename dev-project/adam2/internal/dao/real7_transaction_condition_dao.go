package dao

import (
	"adam2/internal/domain"
	"adam2/internal/model"
)

type Real7TransactionConditionDao interface {

	// 清空表real7_transaction_condition
	TruncateTableReal7TransactionCondition()

	// 向real7_transaction_condition表中导入数据
	ImportIntoReal7TransactionCondition(transactionDate string)

	// 判断当前交易日，更新MA和BIAS，并且半段所有股票的平均价收盘价是否在牛熊线之上。true表示是，false表示否
	IsBullShortLineUp(stockTransactionDataAllArray model.StockTransactionDataAllArray) (bool, domain.AverageClosePriceAndMaBullShortLineByBias)

	// 判断当前交易日，所有股票的平均年线是否单调不递减(平均年线只保留小数点后2位，并做了平滑处理，时间窗口为20)
	IsAverageMa250NotDecrement_smooth(date string, notDecrementDateNumber int, averageClosePriceAndMaBullShortLineByBias domain.AverageClosePriceAndMaBullShortLineByBias) bool

	// 更新字段：xr、close_price_out_of_limit、filter_type、direction、accumulative_profit_loss、is_transaction_date、suspension、variance_type、t.variance为空
	ResetReal7TransactionCondition()

	// 过滤条件：过滤这个价格区间以内的股票
	FilterByLessThanClosePrice(stockTransactionDataAllArray model.StockTransactionDataAllArray, closePriceStart float64, closePriceEnd float64)

	// 过滤条件：判断是否是涨停板
	FilterByLimitUp(currentDate string, stockTransactionDataAllArray model.StockTransactionDataAllArray)

	// 过滤条件：更新real7_transaction_condition表的字段：accumulative_profit_rate和filter_type。单位：元
	UpdateAccumulativeProfitLossAndFilterTypeInReal7TransactionCondition(currentStockTransactionDataAllArray model.StockTransactionDataAllArray, transactionDate string)

	// 过滤条件：更新real7_transaction_condition表的字段：direction
	UpdateDirectionInReal7TransactionCondition(currentStockTransactionDataAllArray model.StockTransactionDataAllArray, transactionDate string)

	// 过滤条件：在real7_transaction_condition表中更新那些不是交易日的记录
	FilterByBuyDate(currentStockTransactionDataAllArray model.StockTransactionDataAllArray)

	// 过滤条件：判断用哪根均线作为牛熊线，牛熊线以下的股票都删除。如果在牛熊线以上则准备方差类型和方差值
	FilterByBiasThreshold(stockTransactionDataAllArray model.StockTransactionDataAllArray, biasThresholdTop float64, biasThresholdBottom float64)

	// 过滤条件：过滤除过权的股票
	FilterByXr(currentDate string, xrDateNumber int)

	// 600679
	PrepareForBullShortLine(currentDate string)

	// 过滤条件：如果这只股票在当天除权了，则删除
	FilterByXrByCurrentDate(stockTransactionDataAllArray model.StockTransactionDataAllArray, currentDate string)

	// 给出买卖建议，并发送邮件
	GiveSuggestionAndSendEmail(currentStockTransactionDataAllArray model.StockTransactionDataAllArray,
		maxHoldStockNumber int, buySuggestion bool, sellSuggestion bool)
}
