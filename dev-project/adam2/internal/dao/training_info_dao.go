package dao

import (
	"adam2/internal/dto"
	"adam2/internal/model"
)

type TrainingInfoDao interface {
	// 添加
	Add(trainingInfo *model.TrainingInfo) error

	// 根据name查询
	FindByName(name string) model.TrainingInfo

	// 分页显示
	Page(name string, pageNo int, pageSize int) dto.TrainingInfoDtoArray

	// 分页显示，返回总数
	PageTotal(name string) int

	// 根据name更新status
	UpdateStatusByName(name string, status int)

	// 删除训练
	DeleteTraining(name string)

	// 根据name，更新begin_date和end_date
	UpdateBeginDateAndEndDateByName(name string, beginDate string, endDate string)
}
