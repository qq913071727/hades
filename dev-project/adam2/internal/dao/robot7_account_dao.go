package dao

type Robot7AccountDao interface {

	// 返回表名
	FindTableName() string

	// 更新robot7_account表
	UpdateRobotAccountBeforeSellOrBuy_stock(date string)

	// 更新robot7_account表
	UpdateRobotAccountBeforeSellOrBuy_all(date string)

	// 根据当日卖出股票的收盘价，在卖股票之后，更新robot7_account表
	UpdateRobotAccountAfterSellOrBuy_stock(date string)

	// 根据当日卖出股票/ETF的收盘价，在卖股票/ETF之后，更新robot7_account表
	UpdateRobotAccountAfterSellOrBuy_all(date string)

	// 更新robot7_account表的total_assets字段
	UpdateRobotAccountAfterBuyOrSell_stock(date string)

	// 更新robot7_account表的total_assets字段
	UpdateRobotAccountAfterBuyOrSell_all(date string)
}
