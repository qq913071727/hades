package dao

import (
	"adam2/internal/domain"
	"adam2/internal/model"
	"time"
)

type StockTransactionDataAllDao interface {
	// 返回表名
	FindTableName() string

	// 获取某一个交易日的数据
	FindByDate(date string) model.StockTransactionDataAllArray

	// 根据开始时间和结束时间，返回日期，并升序排列
	GetDistinctDateBetweenDateOrderByDateAsc(beginDate string, endDate string) ([]string, error)

	// 查找某只股票，在开始时间和结束时间之间的记录，并按照日期升序排列
	GetByCodeBetweenDateOrderByDateAsc(code string, beginDate string, endDate string) (*model.StockTransactionDataAllArray, error)

	// 在开始时间和结束时间之间，按照日期分组，分别计算所有收盘价、移动平均线的平均值（经过平滑处理后的）
	GetAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc_smooth(beginDate string, endDate string) (*domain.AverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray, error)

	// 在开始时间和结束时间之间，按照日期分组，分别计算所有收盘价、移动平均线的平均值和bias的平均值
	GetAverageClosePriceAndMaAndBiasBetweenDateGroupByDateOrderByDateAsc(beginDate string, endDate string) (*domain.AverageClosePriceAndMaBullShortLineByBiasArray, error)

	// 判断当前交易日，所有股票的平均年线是否单调不递减(平均年线只保留小数点后1位)
	IsAverageMa250NotDecrement(date string, notDecrementDateNumber int) bool

	// 判断当前交易日，所有股票的平均年线是否单调不递减(平均年线只保留小数点后2位，并做了平滑处理，时间窗口为20)
	IsAverageMa250NotDecrement_smooth(date string, notDecrementDateNumber int) bool

	// 根据code和date，查找最近8日内的最高收盘价和最低收盘价
	FindHighestAndLowestClosePriceByCodeAndDateInEightDay(code string, date string) domain.MaxHighestPriceAndMinLowestPrice

	// 随机选取一个股票code
	FindRandomStockCode(beginDate string, enDate string) string

	// 根据code和date，查询前一个交易日的日期
	FindBeforeOneByStockCodeAndDate(code string, date string) time.Time

	// 查询某个股票的最大日期和最小日期
	FindMinDateAndMaxDate(code string) domain.MaxDataAndMinData

	// 根据code、开始时间和结束时间查询记录
	FindByCodeBetweenDate(code string, beginDate string, endDate string) model.StockTransactionDataAllArray

	// 查询某个股票下一个交易日记录
	FindNextByCodeAndDate(code string, date string) model.StockTransactionDataAll

	// 根据code和date查询股票记录
	FindByCodeAndDate(code string, date string) model.StockTransactionDataAll

	// 查询所有code
	FindDistinctCode() []string
}
