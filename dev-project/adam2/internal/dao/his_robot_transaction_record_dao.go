package dao

import "adam2/internal/domain"

type HisRobotTransactionRecordDao interface {

	// 返回表名
	FindTableName() string

	// 根据modeId，查找收益率和相关系数
	FindProfitAndLossRateAndCorrelation250WithAvgClosePriceByModelId(modelId int) domain.ProfitAndLossRateAndCorrelation250WithAvgClosePriceArray
}
