package dao

import (
	"adam2/internal/model"
)

type TrainingTransactionDataDao interface {

	// 根据trng_info_id字段删除记录
	DeleteTrainingTransactionDataByTrainingInfoId(trainingInfoId int)

	// 保存
	Save(trainingTransactionData model.TrainingTransactionData)

	// 根据trg_info_id，判断是否是持仓状态
	IsHoldingShareByTrainingInfoId(trainingInfoId int) bool

	// 根据训练id查询记录，其中sell_date字段需要是空
	FindByTrainingInfoIdAndSellDateNull(trainingInfoId int) model.TrainingTransactionData

	// 根据训练id查询记录，其中buy_date字段需要是空
	FindByTrainingInfoIdAndBuyDateNull(trainingInfoId int) model.TrainingTransactionData

	// 根据id，更新sell_date、sell_price、profit_and_loss和profit_and_loss_rate
	UpdateSellDateAndSellPriceAndProfitAndLossRateById(id int, sellDate string, sellPrice float64, profitAndLoss float64, profitAndLossRate float64)

	// 根据id，更新buy_date、buy_price、profit_and_loss和profit_and_loss
	UpdateBuyDateAndBuyPriceAndProfitAndLossRateById(id int, buyDate string, buyPrice float64, profitAndLoss float64, profitAndLossRate float64)

	// 根据训练id查询记录，要求buy_date和sell_date都不为空，并按照buy_date升序排列
	FindByTrainingInfoIdAndBuyDateNotNullAndSellDateNotNullOderByBuyDateAsc(trainingInfoId int) model.TrainingTransactionDataArray

	// 连接trng_info表和trng_transaction_data表，然后根据trng_info表的name字段查询，最后按照begin_date升序排列
	JoinTrainingInfoByTrainingInfoNameOrderByBeginDateAsc(trainingInfoName string) model.TrainingTransactionDataArray
}
