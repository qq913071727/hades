package dao

import "adam2/internal/model"

type CommodityFutureInfoDao interface {

	// 查询所有记录
	FindAll() model.CommodityFutureInfoArray
}
