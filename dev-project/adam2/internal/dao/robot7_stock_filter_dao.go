package dao

type Robot7StockFilterDao interface {
	// 返回表名
	FindTableName() string

	// 清空ROBOT7_STOCK_FILTER表中的记录
	TruncateTableRobotStockFilter()

	// 从stock_transaction_data_all表中向robot7_stock_filter表中插入记录，不包括正在持有的股票
	InsertBullStockCodeFromStockTransactionDataAll(date string)

	// 从etf_transaction_data表中向robot7_stock_filter表中插入记录（tendency_type为2，correlation250_with_avg_c_p不能为空，不包括正在持有的ETF）
	InsertBullStockCodeFromEtfTransactionData(date string)

	// 过滤条件：只保留成交量超过VOLUME_MA250或者成交额超过TURNOVER_MA250
	FilterByVolumeOrTurnoverUp(date string)

	// 过滤条件：只保留这个价格区间以内的股票
	FilterByLessThanStockClosePrice(date string, closePriceBegin float64, closePriceEnd float64)

	// 过滤条件：只保留这个价格区间以内的ETF
	FilterByLessThanEtfClosePrice(date string, closePriceBegin float64, closePriceEnd float64)

	// 过滤条件：删除MACD死叉的股票
	FilterByMacdGoldCross(currentDate string)

	// 过滤条件：股票交易日期之前的天数中，如果MACD持续处于金叉状态，则删除
	FilterByMacdGoldCrossDateNumber(currentDate string, dateNumber int)

	// 过滤条件：删除上一周，周线级别KD死叉的股票
	FilterByLastStockWeekKDGoldCross(currentDate string)

	// 判断上个星期所有股票的平均KD是否是金叉，true表示是金叉，false表示不是金叉
	IsLastStockWeekAverageKDGoldCross(currentDate string) bool

	// 判断上一个交易日所有股票的平均MACD是否是金叉，true表示是金叉，false表示不是金叉
	IsLastStockTransactionDataAllAverageMACDGoldCross(currentDate string) bool

	// 判断当前交易日所有股票的平均MACD是否是金叉，true表示是金叉，false表示不是金叉
	IsCurrentStockTransactionDataAllAverageMACDGoldCross(currentDate string) bool

	// 判断当前交易日所有股票的平均KD是否是金叉，true表示是金叉，false表示不是金叉
	IsCurrentStockTransactionDataAllAverageKDGoldCross(currentDate string) bool

	// 判断当前交易日所有股票的平均close_price是否金叉ma5，true表示是金叉，false表示不是金叉
	IsCurrentStockTransactionDataAllAverageClosePriceGoldCrossMA5(currentDate string) bool

	// 过滤条件：删除上一周最低价没有跌破周线级别布林带下轨的记录
	FilterByLastLowestPriceDownBullDn(currentDate string)

	// 过滤条件：删除除过权的股票
	FilterByXr_stock(currentDate string, xrDateNumber int)

	// 过滤条件：删除一字涨停板
	FilterByOneWordLimitUp_stock(currentDate string)

	// 过滤条件：由于有些涨停板是早上一开盘没多久就涨停的，实际中很难买到，因此这部分股票也删除掉
	FilterByLimitUp_stock(currentDate string)

	// 过滤条件：删除除过权的ETF
	FilterByXr_etf(currentDate string, xrDateNumber int)

	// 过滤条件：删除一字涨停板
	FilterByOneWordLimitUp_etf(currentDate string)

	// 过滤条件：由于有些涨停板是早上一开盘没多久就涨停的，实际中很难买到，因此这部分ETF也删除掉
	FilterByLimitUp_etf(currentDate string)

	// 过滤条件：判断用哪根均线作为牛熊线，牛熊线以下的股票都删除。如果在牛熊线以上则准备方差类型和方差值
	FilterByBiasThreshold_stock(date string, biasThresholdTop float64, biasThresholdBottom float64)

	// 过滤条件：删除上周收盘价死叉周线级别MA5的股票
	FilterByLastWeekCLosePriceGoldCrossLastWeekMA5(date string)

	// 过滤条件：判断用哪根均线作为牛熊线，牛熊线以下的ETF都删除。如果在牛熊线以上则准备相关系数
	FilterByBiasThreshold_etf(date string, biasThresholdTop float64, biasThresholdBottom float64)

	// 确定仓位（是否使用动态仓位）。返回true表示买入股票，返回false表示不再买入股票
	SetMaxHoldStockNumber_stock(date string) bool

	// 确定仓位（使用动态仓位、熊市时交易ETF），设置参数MaxHoldStockNumberInShort并返回
	SetMaxHoldStockNumber_etf(date string) int

	// 判断当前交易日，所有股票的平均收盘价是否在牛熊线之上。true表示是，false表示否
	IsUpBullShortLine(date string) bool

	// 根据当前交易日，判断所有股票上一周的周线级别的平均K值是否比上上周的平均K值大，true表示是，false表示否
	IsLastWeekAverageKGreatThanLastTwoWeekAverageK(date string) bool

	// 根据当前交易日，判断所有股票上一周的周线级别的平均KD值是否金叉，true表示是，false表示否
	IsLastWeekAverageKDGoldCross(date string) bool

	// 根据当前交易日，判断所有股票上一周的平均收盘价是否金叉平均MA5，true表示是，false表示否
	IsLastWeekAverageClosePriceGoldCrossLastWeekAverageMA5(date string) bool

	// 计算最近n个交易日，上涨家数的百分比的均值，如果大于等于50%则返回true，否则返回false
	IsUpPercentageGTE50ByDateNumber(date string) bool

	// 过滤条件：更新robot7_stock_filter表：direction为1；filter_type为交易算法；accumulative_profit_rate累计收益。单位：元
	UpdateRobotStockFilter_stock(date string)

	// 过滤条件：更新robot7_stock_filter表：direction为1；filter_type为交易算法；accumulative_profit_rate累计收益。单位：元
	UpdateRobotStockFilter_etf(date string)

	// 将robot7_stock_filter表的direction字段改为1
	UpdateRobotStockFilterDirection_etf()

	// 过滤条件：从robot7_stock_filter表中删除那些不是交易日的记录
	FilterByBuyDate_stock(buyDate string)

	// 过滤条件：从robot7_stock_filter表中删除那些不是交易日的记录
	FilterByBuyDate_etf(buyDate string)
}
