package impl

import "adam2/internal/model"

type MonitorCurrentPriceDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetMonitorCurrentPriceDaoImpl() *MonitorCurrentPriceDaoImpl {
	return &MonitorCurrentPriceDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_monitorCurrentPriceDaoImpl *MonitorCurrentPriceDaoImpl) FindTableName() string {
	return "MONITOR_CURRENT_PRICE"
}

// 根据条件，查询记录
func (_monitorCurrentPriceDaoImpl *MonitorCurrentPriceDaoImpl) Find(monitorCurrentPrice model.MonitorCurrentPrice) model.MonitorCurrentPriceArray {
	var monitorCurrentPriceArray model.MonitorCurrentPriceArray
	_monitorCurrentPriceDaoImpl.db.Where(&monitorCurrentPrice).Find(&monitorCurrentPriceArray)
	return monitorCurrentPriceArray
}
