package impl

import (
	"adam2/internal/constants"
	"adam2/internal/dao/helper"
	"adam2/internal/domain"
	"adam2/internal/model"
	"adam2/internal/properties"
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/util"
	"encoding/json"
	"fmt"
	"github.com/jordan-wright/email"
	"math"
	"net/smtp"
	"strings"
)

type Real7TransactionConditionDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetReal7TransactionConditionDaoImpl() *Real7TransactionConditionDaoImpl {
	return &Real7TransactionConditionDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) FindTableName() string {
	return "REAL7_TRANSACTION_CONDITION"
}

// 清空表real7_transaction_condition
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) TruncateTableReal7TransactionCondition() {
	io.Infoln("清空表real7_transaction_condition")

	_real7TransactionConditionDaoImpl.db.Exec("truncate table real7_transaction_condition")
}

// 向real7_transaction_condition表中导入数据
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) ImportIntoReal7TransactionCondition(transactionDate string) {
	io.Infoln("向real7_transaction_condition表中导入数据，不包括已经买入的，包括停牌的，交易日期[%s]", transactionDate)

	_real7TransactionConditionDaoImpl.db.Exec("insert into real7_transaction_condition(stock_code, transaction_date, calculate_date) "+
		"select distinct t.code_, to_date(?,'yyyy-mm-dd'), sysdate "+
		"from stock_transaction_data_all t where t.date_<to_date(?,'yyyy-mm-dd')",
		transactionDate, transactionDate)
}

// 判断当前交易日，更新MA和BIAS，并且半段所有股票的平均价收盘价是否在牛熊线之上。true表示是，false表示否
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) IsBullShortLineUp(stockTransactionDataAllArray model.StockTransactionDataAllArray) (bool, domain.AverageClosePriceAndMaBullShortLineByBias) {
	io.Infoln("判断当前交易日，更新MA和BIAS，并且半段所有股票的平均价收盘价是否在牛熊线之上。true表示是，false表示否")

	var sumClosePirce float64 = 0
	var averageClosePrice float64 = 0
	var bullShortLine float64 = 0
	// 计算当前交易日的所有股票的收盘价、MA和BIAS
	for _, stockTransactionDataAll := range stockTransactionDataAllArray {
		// 收盘价总额
		sumClosePirce = sumClosePirce + stockTransactionDataAll.ClosePrice

		// 更新MA
		_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t "+
			"set t.ma5=round((t.prepare_ma5+?)/5, 2), t.ma10=round((t.prepare_ma10+?)/10, 2), t.ma20=round((t.prepare_ma20+?)/20, 2), "+
			"t.ma60=round((t.prepare_ma60+?)/60, 2), t.ma120=round((t.prepare_ma120+?)/120, 2), t.ma250=round((t.prepare_ma250+?)/250, 2) "+
			"where t.stock_code=?",
			stockTransactionDataAll.ClosePrice, stockTransactionDataAll.ClosePrice, stockTransactionDataAll.ClosePrice,
			stockTransactionDataAll.ClosePrice, stockTransactionDataAll.ClosePrice, stockTransactionDataAll.ClosePrice,
			stockTransactionDataAll.Code)

		// 更新BIAS
		_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t "+
			"set t.bias5=(? - t.ma5)/t.ma5 * 100, t.bias10=(? - t.ma10)/t.ma10 * 100, t.bias20=(? - t.ma20)/t.ma20 * 100, "+
			"t.bias60=(? - t.ma60)/t.ma60 * 100, t.bias120=(? - t.ma120)/t.ma120 * 100, t.bias250=(? - t.ma250)/t.ma250 * 100 "+
			"where t.stock_code=?",
			stockTransactionDataAll.ClosePrice, stockTransactionDataAll.ClosePrice, stockTransactionDataAll.ClosePrice,
			stockTransactionDataAll.ClosePrice, stockTransactionDataAll.ClosePrice, stockTransactionDataAll.ClosePrice,
			stockTransactionDataAll.Code)
	}

	// 计算平均MA和BIAS
	var averageClosePriceAndMaBullShortLineByBias domain.AverageClosePriceAndMaBullShortLineByBias
	_real7TransactionConditionDaoImpl.db.Raw("select avg(t.ma5), avg(t.ma10), avg(t.ma20), avg(t.ma60), avg(t.ma120), avg(t.ma250), " +
		"avg(t.bias5), avg(t.bias10), avg(t.bias20), avg(t.bias60), avg(t.bias120), avg(t.bias250) " +
		"from real7_transaction_condition t").Scan(&averageClosePriceAndMaBullShortLineByBias)

	// 计算平均收盘价
	averageClosePrice = sumClosePirce / float64(len(stockTransactionDataAllArray))

	// 确定牛熊线
	if averageClosePriceAndMaBullShortLineByBias.AverageBias250 <= properties.Real7Properties_.BiasThresholdTop && averageClosePriceAndMaBullShortLineByBias.AverageBias250 >= properties.Real7Properties_.BiasThresholdBottom {
		bullShortLine = averageClosePriceAndMaBullShortLineByBias.AverageMa250
	} else if averageClosePriceAndMaBullShortLineByBias.AverageBias120 <= properties.Real7Properties_.BiasThresholdTop && averageClosePriceAndMaBullShortLineByBias.AverageBias120 >= properties.Real7Properties_.BiasThresholdBottom {
		bullShortLine = averageClosePriceAndMaBullShortLineByBias.AverageMa120
	} else if averageClosePriceAndMaBullShortLineByBias.AverageBias60 <= properties.Real7Properties_.BiasThresholdTop && averageClosePriceAndMaBullShortLineByBias.AverageBias60 >= properties.Real7Properties_.BiasThresholdBottom {
		bullShortLine = averageClosePriceAndMaBullShortLineByBias.AverageMa60
	} else if averageClosePriceAndMaBullShortLineByBias.AverageBias20 <= properties.Real7Properties_.BiasThresholdTop && averageClosePriceAndMaBullShortLineByBias.AverageBias20 >= properties.Real7Properties_.BiasThresholdBottom {
		bullShortLine = averageClosePriceAndMaBullShortLineByBias.AverageMa20
	} else if averageClosePriceAndMaBullShortLineByBias.AverageBias10 <= properties.Real7Properties_.BiasThresholdTop && averageClosePriceAndMaBullShortLineByBias.AverageBias10 >= properties.Real7Properties_.BiasThresholdBottom {
		bullShortLine = averageClosePriceAndMaBullShortLineByBias.AverageMa10
	} else if averageClosePriceAndMaBullShortLineByBias.AverageBias5 <= properties.Real7Properties_.BiasThresholdTop && averageClosePriceAndMaBullShortLineByBias.AverageBias5 >= properties.Real7Properties_.BiasThresholdBottom {
		bullShortLine = averageClosePriceAndMaBullShortLineByBias.AverageMa5
	} else {
		bullShortLine = averageClosePrice
	}

	if averageClosePrice >= bullShortLine {
		io.Infoln("所有股票的平均价收盘价在牛熊线之上")
		return true, averageClosePriceAndMaBullShortLineByBias
	} else {
		io.Infoln("所有股票的平均价收盘价不在牛熊线之上")
		return false, averageClosePriceAndMaBullShortLineByBias
	}
}

// 判断当前交易日，所有股票的平均年线是否单调不递减(平均年线只保留小数点后2位，并做了平滑处理，时间窗口为20)
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) IsAverageMa250NotDecrement_smooth(date string, notDecrementDateNumber int, averageClosePriceAndMaBullShortLineByBias domain.AverageClosePriceAndMaBullShortLineByBias) bool {
	io.Infoln("判断当前交易日[%s]，所有股票的平均年线在[%d]天内，是否单调不递减(平均年线只保留小数点后2位，并做了平滑处理，时间窗口为20)", date, notDecrementDateNumber)

	var averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray *domain.AverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray
	_real7TransactionConditionDaoImpl.db.Raw("SELECT t1.date_, t1.AVERAGE_CLOSE_PRICE, "+
		"round(AVG(t1.AVERAGE_MA5) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 2) AS AVERAGE_MA5, "+
		"round(AVG(t1.AVERAGE_MA10) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 2) AS AVERAGE_MA10, "+
		"round(AVG(t1.AVERAGE_MA20) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 2) AS AVERAGE_MA20,"+
		"round(AVG(t1.AVERAGE_MA60) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 2) AS AVERAGE_MA60, "+
		"round(AVG(t1.AVERAGE_MA120) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 2) AS AVERAGE_MA120, "+
		"round(AVG(t1.AVERAGE_MA250) OVER (ORDER BY t1.date_ desc ROWS BETWEEN 20 PRECEDING AND CURRENT ROW), 2) AS AVERAGE_MA250 "+
		"FROM ("+
		"select to_char(t.date_, 'yyyy-mm-dd') DATE_, avg(t.close_price) AVERAGE_CLOSE_PRICE, avg(t.ma5) AVERAGE_MA5, "+
		"avg(t.ma10) AVERAGE_MA10, avg(t.ma20) AVERAGE_MA20, avg(t.ma60) AVERAGE_MA60, avg(t.ma120) AVERAGE_MA120, avg(t.ma250) AVERAGE_MA250 "+
		"from stock_transaction_data_all t "+
		"where t.date_ between add_months(to_date(?,'yyyy-mm-dd'), -24) and to_date(?,'yyyy-mm-dd') "+
		"group by t.date_ "+
		"order by t.date_ desc) t1", date, date).Scan(&averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray)

	var originNotDecrementDateNumber int = notDecrementDateNumber
	if len(*averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray) < notDecrementDateNumber {
		io.Infoln("当前记录数不足[%d]个，按照不是单调不递减处理", originNotDecrementDateNumber)
		return false
	} else {
		// 把当前交易日的数据加入到数组中
		//var newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray domain.AverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray = domain.AverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray{}
		//var newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc domain.AverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc
		//newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.Date = averageClosePriceAndMaBullShortLineByBias.Date
		//newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageClosePrice = averageClosePriceAndMaBullShortLineByBias.AverageClosePrice
		//newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa5 = averageClosePriceAndMaBullShortLineByBias.AverageMa5
		//newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa10 = averageClosePriceAndMaBullShortLineByBias.AverageMa10
		//newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa20 = averageClosePriceAndMaBullShortLineByBias.AverageMa20
		//newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa60 = averageClosePriceAndMaBullShortLineByBias.AverageMa60
		//newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa120 = averageClosePriceAndMaBullShortLineByBias.AverageMa120
		//newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa250 = averageClosePriceAndMaBullShortLineByBias.AverageMa250
		//newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray = append(newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray, &newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc)
		//for _, _averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc := range *averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray {
		//	newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray = append(newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray, _averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc)
		//}

		var last float64 = 0
		//for index, averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc := range newAverageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray {
		for index, averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc := range *averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAscArray {
			if notDecrementDateNumber == 0 {
				io.Infoln("在[%d]个记录中，ma250单调不递减", originNotDecrementDateNumber)
				return true
			}
			if index == 0 {
				last = averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa250
				notDecrementDateNumber--
				continue
			}
			if averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa250 > last {
				io.Infoln("在[%d]个记录中，ma250不是单调不递减", originNotDecrementDateNumber)
				return false
			} else {
				last = averageClosePriceAndMaBetweenDateGroupByDateOrderByDateAsc.AverageMa250
				notDecrementDateNumber--
			}
		}

		io.Infoln("默认按照ma250不是单调不递减处理")
		return false
	}
}

// 更新字段：xr、close_price_out_of_limit、filter_type、direction、accumulative_profit_loss、is_transaction_date、suspension、variance_type、t.variance为空
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) ResetReal7TransactionCondition() {
	io.Infoln("更新字段：xr、close_price_out_of_limit、filter_type、direction、accumulative_profit_loss、is_transaction_date、suspension、variance_type、t.variance为空")

	_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.xr=null, t.close_price_out_of_limit=null, " +
		"t.filter_type=null, t.direction=null, t.accumulative_profit_loss=null, t.is_transaction_date=null, t.suspension=null, " +
		"t.variance_type=null, t.variance=null")
}

// 过滤条件：过滤这个价格区间以内的股票
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) FilterByLessThanClosePrice(stockTransactionDataAllArray model.StockTransactionDataAllArray, closePriceStart float64, closePriceEnd float64) {
	io.Infoln("过滤条件：过滤这个价格区间以内的股票")

	for _, stockTransactionDataAll := range stockTransactionDataAllArray {
		if stockTransactionDataAll.ClosePrice < closePriceStart || stockTransactionDataAll.ClosePrice > closePriceEnd {
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.close_price_out_of_limit = 1 where t.stock_code=?", stockTransactionDataAll.Code)
		}
	}
}

// 过滤条件：判断是否是涨停板
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) FilterByLimitUp(currentDate string, stockTransactionDataAllArray model.StockTransactionDataAllArray) {
	io.Infoln("过滤条件：判断是否是涨停板")

	for _, stockTransactionDataAll := range stockTransactionDataAllArray {
		// 是否涨停
		var limitUp bool = false

		// 在2020-08-24之前，并且当上涨幅度大于等于10%，就表示有可能是涨停板
		if util.Before(currentDate, constants.JUDGEMENT_DATE) && stockTransactionDataAll.ChangeRange >= 10 {
			limitUp = true
		}

		// 在2020-08-24之后，如果股票代码以300开头，并且上涨幅度大于等于20%，就表示有可能是涨停板
		if util.AfterOrEqual(currentDate, constants.JUDGEMENT_DATE) && strings.Compare(string(stockTransactionDataAll.Code[3]), "300") == 0 && stockTransactionDataAll.ChangeRange >= 20 {
			limitUp = true
		}

		// 在2020-08-24之后，如果股票代码不是300开头，并且上涨幅度大于等于10%，就表示有可能是涨停板
		if util.AfterOrEqual(currentDate, constants.JUDGEMENT_DATE) && strings.Compare(string(stockTransactionDataAll.Code[3]), "300") != 0 && stockTransactionDataAll.ChangeRange >= 10 {
			limitUp = true
		}

		// 如果是涨停板，则标记
		if limitUp {
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.is_limit_up = ? where t.stock_code=?", constants.LIMIT_UP, stockTransactionDataAll.Code)
		} else {
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.is_limit_up = ? where t.stock_code=?", constants.NOT_LIMIT_UP, stockTransactionDataAll.Code)
		}
	}
}

// 过滤条件：更新real7_transaction_condition表的字段：accumulative_profit_rate累计收益。单位：元
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) UpdateAccumulativeProfitLossAndFilterTypeInReal7TransactionCondition(currentStockTransactionDataAllArray model.StockTransactionDataAllArray, transactionDate string) {
	io.Infoln("过滤条件：更新real7_transaction_condition表的字段：accumulative_profit_rate和filter_type。单位：元")

	var real7TransactionConditionArray *model.Real7TransactionConditionArray
	//_real7TransactionConditionDaoImpl.db.Raw("select * from real7_transaction_condition t " +
	//	"where t.close_price_out_of_limit is null " +
	//	"and t.variance_type is not null and t.variance is not null and t.xr is null").Scan(&real7TransactionConditionArray)
	_real7TransactionConditionDaoImpl.db.Raw("select * from real7_transaction_condition t").Scan(&real7TransactionConditionArray)
	for _, real7TransactionCondition := range *real7TransactionConditionArray {
		if real7TransactionCondition.StockCode == "300450" {
			io.Infoln("-------------")
		}

		// 某个股票的前一个交易日的记录
		var lastStockTransactionDataAll model.StockTransactionDataAll
		_real7TransactionConditionDaoImpl.db.Raw("select * from ("+
			"select * from stock_transaction_data_all t "+
			"where t.code_ = ? and t.date_ < to_date(?, 'yyyy-mm-dd') "+
			"order by t.date_ desc) "+
			"where rownum<=1",
			real7TransactionCondition.StockCode, transactionDate).Scan(&lastStockTransactionDataAll)

		// 判断是否停牌或者还没有开始上市交易
		var currentStockTransactionDataAll *model.StockTransactionDataAll = helper.FindStockTransactionDataAllByCode(currentStockTransactionDataAllArray, real7TransactionCondition.StockCode)
		if currentStockTransactionDataAll == nil {
			io.Infoln(fmt.Sprintf("股票[%s]在日期[%s]停牌或者还没有开始上市交易，因此跳过这只股票", real7TransactionCondition.StockCode, transactionDate))
			// 更新suspension字段
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t "+
				"set t.suspension = 1 where t.stock_code = ?", real7TransactionCondition.StockCode)
			continue
		}

		// 金叉最大收益率
		var goldCrossMaxProfitLoss float64 = -100
		// 死叉最大收益率
		//var deadCrossMaxProfitLoss float64 = -100
		// 最大收益率类型
		var maxProfitLossType int = 0
		// 操作方向
		//var direction int = 0

		// 计算macd金叉算法的近期收益率
		var mdlMacdGoldCrossArray model.MdlMacdGoldCrossArray
		_real7TransactionConditionDaoImpl.db.Raw("select * from "+
			"(select * from mdl_macd_gold_cross t "+
			"where t.stock_code = ? and t.sell_date <= to_date(?, 'yyyy-mm-dd') "+
			"order by t.sell_date desc) t1 where rownum <= 1", real7TransactionCondition.StockCode, transactionDate).Scan(&mdlMacdGoldCrossArray)
		if len(mdlMacdGoldCrossArray) != 0 && (mdlMacdGoldCrossArray[0]).AccumulativeProfitLoss > goldCrossMaxProfitLoss {
			goldCrossMaxProfitLoss = (mdlMacdGoldCrossArray[0]).AccumulativeProfitLoss
			maxProfitLossType = constants.MacdGoldCross
			//direction = constants.Bull
		}
		// 计算macd死叉算法的近期收益率
		//var mdlMacdDeadCrossArray model.MdlMacdDeadCrossArray
		//_real7TransactionConditionDaoImpl.db.Raw("select * from "+
		//	"(select * from mdl_macd_dead_cross t "+
		//	"where t.stock_code = ? and t.buy_date <= to_date(?, 'yyyy-mm-dd') "+
		//	"order by t.buy_date desc) t1 where rownum <= 1", real7TransactionCondition.StockCode, transactionDate).Scan(&mdlMacdDeadCrossArray)
		//if len(mdlMacdDeadCrossArray) != 0 && (mdlMacdDeadCrossArray[0]).AccumulativeProfitLoss > deadCrossMaxProfitLoss {
		//	deadCrossMaxProfitLoss = (mdlMacdDeadCrossArray[0]).AccumulativeProfitLoss
		//	maxProfitLossType = constants.MacdDeadCross
		//	direction = constants.Short
		//}
		// 计算close_price金叉ma5算法的近期收益率
		var mdlClosePriceMa5GoldCrossArray model.MdlClosePriceMa5GoldCrossArray
		_real7TransactionConditionDaoImpl.db.Raw("select * from "+
			"(select * from mdl_close_price_ma5_gold_cross t "+
			"where t.stock_code = ? and t.sell_date <= to_date(?, 'yyyy-mm-dd') "+
			"order by t.sell_date desc) t1 where rownum <= 1", real7TransactionCondition.StockCode, transactionDate).Scan(&mdlClosePriceMa5GoldCrossArray)
		if len(mdlClosePriceMa5GoldCrossArray) != 0 && (mdlClosePriceMa5GoldCrossArray[0]).AccumulativeProfitLoss > goldCrossMaxProfitLoss {
			goldCrossMaxProfitLoss = (mdlClosePriceMa5GoldCrossArray[0]).AccumulativeProfitLoss
			maxProfitLossType = constants.ClosePriceMa5GoldCross
			//direction = constants.Bull
		}
		// 计算close_price死叉ma5算法的近期收益率
		//var mdlClosePriceMa5DeadCrossArray model.MdlClosePriceMa5DeadCrossArray
		//_real7TransactionConditionDaoImpl.db.Raw("select * from "+
		//	"(select * from mdl_close_price_ma5_dead_cross t "+
		//	"where t.stock_code = ? and t.buy_date <= to_date(?, 'yyyy-mm-dd') "+
		//	"order by t.buy_date desc) t1 where rownum <= 1", real7TransactionCondition.StockCode, transactionDate).Scan(&mdlClosePriceMa5DeadCrossArray)
		//if len(mdlClosePriceMa5DeadCrossArray) != 0 && (mdlClosePriceMa5DeadCrossArray[0]).AccumulativeProfitLoss > deadCrossMaxProfitLoss {
		//	deadCrossMaxProfitLoss = (mdlClosePriceMa5DeadCrossArray[0]).AccumulativeProfitLoss
		//	maxProfitLossType = constants.ClosePriceMa5DeadCross
		//	direction = constants.Short
		//}
		// 计算hei_kin_ashi上升趋势算法的近期收益率
		var mdlHeiKinAshiUpDownArray model.MdlHeiKinAshiUpDownArray
		_real7TransactionConditionDaoImpl.db.Raw("select * from "+
			"(select * from mdl_hei_kin_ashi_up_down t "+
			"where t.stock_code = ? and t.sell_date <= to_date(?, 'yyyy-mm-dd') "+
			"order by t.sell_date desc) t1 where rownum <= 1", real7TransactionCondition.StockCode, transactionDate).Scan(&mdlHeiKinAshiUpDownArray)
		if len(mdlHeiKinAshiUpDownArray) != 0 && (mdlHeiKinAshiUpDownArray[0]).AccumulativeProfitLoss > goldCrossMaxProfitLoss {
			goldCrossMaxProfitLoss = (mdlHeiKinAshiUpDownArray[0]).AccumulativeProfitLoss
			maxProfitLossType = constants.HeiKinAshiUp
			//direction = constants.Bull
		}
		// 计算hei_kin_ashi下降趋势算法的近期收益率
		//var mdlHeiKinAshiDownUpArray model.MdlHeiKinAshiDownUpArray
		//_real7TransactionConditionDaoImpl.db.Raw("select * from "+
		//	"(select * from mdl_hei_kin_ashi_down_up t "+
		//	"where t.stock_code = ? and t.buy_date <= to_date(?, 'yyyy-mm-dd') "+
		//	"order by t.buy_date desc) t1 where rownum <= 1", real7TransactionCondition.StockCode, transactionDate).Scan(&mdlHeiKinAshiDownUpArray)
		//if len(mdlHeiKinAshiDownUpArray) != 0 && (mdlHeiKinAshiDownUpArray[0]).AccumulativeProfitLoss > deadCrossMaxProfitLoss {
		//	deadCrossMaxProfitLoss = (mdlHeiKinAshiDownUpArray[0]).AccumulativeProfitLoss
		//	maxProfitLossType = constants.HeiKinAshiDown
		//	direction = constants.Short
		//}
		// 计算kd金叉算法的近期收益率
		// 根据code和date，查找最近8日内的最高收盘价和最低收盘价
		//var maxHighestPriceAndMinLowestPrice domain.MaxHighestPriceAndMinLowestPrice
		//_real7TransactionConditionDaoImpl.db.Raw("select max(highest_price) MAX_HIGHEST_PRICE, min(lowest_price) MIN_LOWEST_PRICE "+
		//	"from (select * from stock_transaction_data_all t where t.code_=? and t.date_<to_date(?, 'yyyy-mm-dd') "+
		//	"order by t.date_ desc) "+
		//	"where rownum<=8", real7TransactionCondition.StockCode, transactionDate).Scan(&maxHighestPriceAndMinLowestPrice)
		//var highestClosePrice float64 = maxHighestPriceAndMinLowestPrice.MaxHighestPrice
		//var lowestClosePrice float64 = maxHighestPriceAndMinLowestPrice.MinLowestPrice
		//if highestClosePrice < currentStockTransactionDataAll.HighestPrice {
		//	highestClosePrice = currentStockTransactionDataAll.HighestPrice
		//}
		//if lowestClosePrice > currentStockTransactionDataAll.LowestPrice {
		//	lowestClosePrice = currentStockTransactionDataAll.LowestPrice
		//}
		//var closePrice float64 = real7TransactionCondition.PrepareKdDeadCross/100*(highestClosePrice-lowestClosePrice) + lowestClosePrice
		//if currentStockTransactionDataAll.ClosePrice > closePrice {
		var mdlKdGoldCrossArray model.MdlKdGoldCrossArray
		_real7TransactionConditionDaoImpl.db.Raw("select * from "+
			"(select * from mdl_kd_gold_cross t "+
			"where t.stock_code = ? and t.sell_date <= to_date(?, 'yyyy-mm-dd') "+
			"order by t.sell_date desc) t1 where rownum <= 1", real7TransactionCondition.StockCode, transactionDate).Scan(&mdlKdGoldCrossArray)
		if len(mdlKdGoldCrossArray) != 0 && (mdlKdGoldCrossArray[0]).AccumulativeProfitLoss > goldCrossMaxProfitLoss {
			goldCrossMaxProfitLoss = (mdlKdGoldCrossArray[0]).AccumulativeProfitLoss
			maxProfitLossType = constants.KdGoldCross
			//direction = constants.Bull
		}
		//}
		// 计算kd死叉算法的近期收益率
		// 根据code和date，查找最近8日内的最高收盘价和最低收盘价
		//var maxHighestPriceAndMinLowestPrice domain.MaxHighestPriceAndMinLowestPrice
		//_real7TransactionConditionDaoImpl.db.Raw("select max(highest_price) MAX_HIGHEST_PRICE, min(lowest_price) MIN_LOWEST_PRICE "+
		//	"from (select * from stock_transaction_data_all t where t.code_=? and t.date_<to_date(?, 'yyyy-mm-dd') "+
		//	"order by t.date_ desc) "+
		//	"where rownum<=8", real7TransactionCondition.StockCode, transactionDate).Scan(&maxHighestPriceAndMinLowestPrice)
		//var highestClosePrice float64 = maxHighestPriceAndMinLowestPrice.MaxHighestPrice
		//var lowestClosePrice float64 = maxHighestPriceAndMinLowestPrice.MinLowestPrice
		//if highestClosePrice < currentStockTransactionDataAll.HighestPrice {
		//	highestClosePrice = currentStockTransactionDataAll.HighestPrice
		//}
		//if lowestClosePrice > currentStockTransactionDataAll.LowestPrice {
		//	lowestClosePrice = currentStockTransactionDataAll.LowestPrice
		//}
		//var closePrice float64 = real7TransactionCondition.PrepareKdDeadCross/100*(highestClosePrice-lowestClosePrice) + lowestClosePrice
		//if currentStockTransactionDataAll.ClosePrice <= closePrice {
		//	var mdlKdDeadCrossArray model.MdlKdDeadCrossArray
		//	_real7TransactionConditionDaoImpl.db.Raw("select * from "+
		//		"(select * from mdl_kd_dead_cross t "+
		//		"where t.stock_code = ? and t.buy_date <= to_date(?, 'yyyy-mm-dd') "+
		//		"order by t.buy_date desc) t1 where rownum <= 1", real7TransactionCondition.StockCode, transactionDate).Scan(&mdlKdDeadCrossArray)
		//	if len(mdlKdDeadCrossArray) != 0 && (mdlKdDeadCrossArray[0]).AccumulativeProfitLoss > deadCrossMaxProfitLoss {
		//		deadCrossMaxProfitLoss = (mdlKdDeadCrossArray[0]).AccumulativeProfitLoss
		//		maxProfitLossType = constants.KdDeadCross
		//		direction = constants.Short
		//	}
		//}

		// 更新real7_transaction_condition表
		if goldCrossMaxProfitLoss != -100 && maxProfitLossType != 0 /*&& direction != 0*/ {
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t "+
				"set t.filter_type = ?, t.accumulative_profit_loss = ? "+
				"where t.stock_code = ?", maxProfitLossType, goldCrossMaxProfitLoss, real7TransactionCondition.StockCode)
		} else {
			// 更新filter_type、accumulative_profit_loss为空
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t "+
				"set t.filter_type = null, t.accumulative_profit_loss = null "+
				"where t.stock_code = ?", real7TransactionCondition.StockCode)
		}
	}
}

// 过滤条件：更新real7_transaction_condition表的字段：direction
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) UpdateDirectionInReal7TransactionCondition(currentStockTransactionDataAllArray model.StockTransactionDataAllArray, transactionDate string) {
	io.Infoln("过滤条件：更新real7_transaction_condition表的字段：direction")

	var real7TransactionConditionArray *model.Real7TransactionConditionArray
	//_real7TransactionConditionDaoImpl.db.Raw("select * from real7_transaction_condition t " +
	//	"where t.close_price_out_of_limit is null and t.variance_type is not null and t.variance is not null and t.xr is null " +
	//	"and t.suspension is null and t.filter_type is not null " +
	//	"and t.accumulative_profit_loss is not null").Scan(&real7TransactionConditionArray)
	_real7TransactionConditionDaoImpl.db.Raw("select * from real7_transaction_condition t").Scan(&real7TransactionConditionArray)
	for _, real7TransactionCondition := range *real7TransactionConditionArray {
		if real7TransactionCondition.StockCode == "300450" {
			io.Infoln("-------------")
		}

		// 某个股票的前一个交易日的记录
		var lastStockTransactionDataAll model.StockTransactionDataAll
		_real7TransactionConditionDaoImpl.db.Raw("select * from ("+
			"select * from stock_transaction_data_all t "+
			"where t.code_ = ? and t.date_ < to_date(?, 'yyyy-mm-dd') "+
			"order by t.date_ desc) "+
			"where rownum<=1",
			real7TransactionCondition.StockCode, transactionDate).Scan(&lastStockTransactionDataAll)

		// 当前交易日的数据
		var currentStockTransactionDataAll *model.StockTransactionDataAll = helper.FindStockTransactionDataAllByCode(currentStockTransactionDataAllArray, real7TransactionCondition.StockCode)
		if currentStockTransactionDataAll == nil {
			io.Infoln(fmt.Sprintf("股票[%s]在日期[%s]停牌或者还没有开始上市交易，因此跳过这只股票", real7TransactionCondition.StockCode, transactionDate))
			// 更新suspension字段
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t "+
				"set t.suspension = 1 where t.stock_code = ?", real7TransactionCondition.StockCode)
			continue
		}

		// 金叉最大收益率
		//var goldCrossMaxProfitLoss float64 = -100
		// 死叉最大收益率
		//var deadCrossMaxProfitLoss float64 = -100
		// 最大收益率类型
		//var maxProfitLossType int = 0
		// 操作方向
		var direction int = 0

		// macd金叉
		if real7TransactionCondition.FilterType == constants.MacdGoldCross &&
			real7TransactionCondition.PrepareMacdGoldCross != 0 &&
			lastStockTransactionDataAll.Dif < lastStockTransactionDataAll.Dea &&
			currentStockTransactionDataAll.ClosePrice > real7TransactionCondition.PrepareMacdGoldCross {
			direction = constants.Bull
		}
		// macd死叉
		//if real7TransactionCondition.PrepareMacdDeadCross != 0 &&
		//	lastStockTransactionDataAll.Dif > lastStockTransactionDataAll.Dea &&
		//	//currentStockTransactionDataAll.ClosePrice < real7TransactionCondition.PrepareMacdDeadCross {
		//	currentStockTransactionDataAll.ClosePrice < lastStockTransactionDataAll.ClosePrice {
		//	direction = constants.Short
		//}
		// close_price金叉ma5
		if real7TransactionCondition.FilterType == constants.ClosePriceMa5GoldCross &&
			real7TransactionCondition.PrepareClosePriceGoldCross != 0 &&
			lastStockTransactionDataAll.ClosePrice <= lastStockTransactionDataAll.Ma5 &&
			currentStockTransactionDataAll.ClosePrice > real7TransactionCondition.MA5 {
			direction = constants.Bull
		}
		// close_price死叉ma5
		//if real7TransactionCondition.PrepareClosePriceDeadCross != 0 &&
		//	lastStockTransactionDataAll.ClosePrice >= lastStockTransactionDataAll.Ma5 &&
		//	//currentStockTransactionDataAll.ClosePrice < real7TransactionCondition.MA5 {
		//	currentStockTransactionDataAll.ClosePrice < lastStockTransactionDataAll.ClosePrice {
		//	direction = constants.Short
		//}
		// hei_kin_ashi上涨趋势
		var haClosePrice float64 = (currentStockTransactionDataAll.ClosePrice + currentStockTransactionDataAll.OpenPrice + currentStockTransactionDataAll.HighestPrice + currentStockTransactionDataAll.LowestPrice) / 4
		if real7TransactionCondition.FilterType == constants.HeiKinAshiUp &&
			real7TransactionCondition.PrepareHeiKinAshiUp != 0 &&
			lastStockTransactionDataAll.HaClosePrice <= lastStockTransactionDataAll.HaOpenPrice &&
			haClosePrice > real7TransactionCondition.PrepareHeiKinAshiUp {
			direction = constants.Bull
		}
		// hei_kin_ashi下跌趋势
		//if real7TransactionCondition.PrepareHeiKinAshiDown != 0 &&
		//	lastStockTransactionDataAll.HaClosePrice >= lastStockTransactionDataAll.HaOpenPrice &&
		//	//currentStockTransactionDataAll.ClosePrice < real7TransactionCondition.PrepareHeiKinAshiDown {
		//	currentStockTransactionDataAll.ClosePrice < lastStockTransactionDataAll.ClosePrice {
		//	direction = constants.Short
		//}
		// 根据code和date，查找最近8日内的最高收盘价和最低收盘价
		var maxHighestPriceAndMinLowestPrice domain.MaxHighestPriceAndMinLowestPrice
		_real7TransactionConditionDaoImpl.db.Raw("select max(highest_price) MAX_HIGHEST_PRICE, min(lowest_price) MIN_LOWEST_PRICE "+
			"from (select * from stock_transaction_data_all t where t.code_=? and t.date_<to_date(?, 'yyyy-mm-dd') "+
			"order by t.date_ desc) "+
			"where rownum<=8", real7TransactionCondition.StockCode, transactionDate).Scan(&maxHighestPriceAndMinLowestPrice)
		var highestClosePrice float64 = maxHighestPriceAndMinLowestPrice.MaxHighestPrice
		var lowestClosePrice float64 = maxHighestPriceAndMinLowestPrice.MinLowestPrice
		if highestClosePrice < currentStockTransactionDataAll.HighestPrice {
			highestClosePrice = currentStockTransactionDataAll.HighestPrice
		}
		if lowestClosePrice > currentStockTransactionDataAll.LowestPrice {
			lowestClosePrice = currentStockTransactionDataAll.LowestPrice
		}

		// kd金叉
		if real7TransactionCondition.PrepareKdGoldCross != 0 && real7TransactionCondition.FilterType == constants.KdGoldCross &&
			lastStockTransactionDataAll.K < lastStockTransactionDataAll.D &&
			real7TransactionCondition.PrepareKdGoldCross != 0 {
			var closePrice float64 = real7TransactionCondition.PrepareKdGoldCross/100*(highestClosePrice-lowestClosePrice) + lowestClosePrice
			if currentStockTransactionDataAll.ClosePrice > closePrice {
				direction = constants.Bull
			}
		}
		// kd死叉
		//if real7TransactionCondition.PrepareKdDeadCross != 0 && lastStockTransactionDataAll.K > lastStockTransactionDataAll.D &&
		//	real7TransactionCondition.PrepareKdDeadCross != 0 {
		//	//var closePrice float64 = real7TransactionCondition.PrepareKdDeadCross/100*(highestClosePrice-lowestClosePrice) + lowestClosePrice
		//	//if currentStockTransactionDataAll.ClosePrice < closePrice {
		//	if currentStockTransactionDataAll.ClosePrice < lastStockTransactionDataAll.ClosePrice {
		//		direction = constants.Short
		//	}
		//}
		// 如果第二天收盘价小于第一天收盘价，则卖出
		if direction == 0 && currentStockTransactionDataAll.ClosePrice < lastStockTransactionDataAll.ClosePrice {
			direction = constants.Short
		}

		// 更新real7_transaction_condition表
		if direction != 0 {
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t "+
				"set t.direction = ? "+
				"where t.stock_code = ?", direction, real7TransactionCondition.StockCode)
		} else {
			// 更新direction、filter_type、accumulative_profit_loss为空
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t "+
				"set t.direction = null "+
				"where t.stock_code = ?", real7TransactionCondition.StockCode)
		}
	}
}

// 过滤条件：在real7_transaction_condition表中更新那些不是交易日的记录
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) FilterByBuyDate(currentStockTransactionDataAllArray model.StockTransactionDataAllArray) {
	io.Infoln("过滤条件：在real7_transaction_condition表中更新那些不是交易日的记录")

	var real7TransactionConditionArray model.Real7TransactionConditionArray
	//_real7TransactionConditionDaoImpl.db.Raw("select * from real7_transaction_condition t " +
	//	"where t.close_price_out_of_limit is null and t.variance_type is not null and t.variance is not null and t.xr is null " +
	//	"and t.suspension is null and t.filter_type is not null and t.accumulative_profit_loss is not null " +
	//	"and t.direction is not null").Scan(&real7TransactionConditionArray)
	_real7TransactionConditionDaoImpl.db.Raw("select * from real7_transaction_condition t").Scan(&real7TransactionConditionArray)
	for _, real7TransactionCondition := range real7TransactionConditionArray {
		var stockTransactionDataAll *model.StockTransactionDataAll = helper.FindStockTransactionDataAllByCode(currentStockTransactionDataAllArray, real7TransactionCondition.StockCode)
		if stockTransactionDataAll != nil {
			// 将is_transaction_date设置为1
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.is_transaction_date = 1 "+
				"where t.stock_code = ?", real7TransactionCondition.StockCode)
		}
	}
}

// 过滤条件：判断用哪根均线作为牛熊线，牛熊线以下的股票都删除。如果在牛熊线以上则准备方差类型和方差值
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) FilterByBiasThreshold(stockTransactionDataAllArray model.StockTransactionDataAllArray, biasThresholdTop float64, biasThresholdBottom float64) {
	io.Infoln("过滤条件：判断用哪根均线作为牛熊线，牛熊线以下的股票都删除。如果在牛熊线以上则准备方差类型和方差值")

	for _, stockTransactionDataAll := range stockTransactionDataAllArray {
		// 表示牛熊线
		var bullShortLine float64

		// real7_transaction_condition表的记录
		var real7TransactionCondition = new(model.Real7TransactionCondition)
		_real7TransactionConditionDaoImpl.db.Raw("select * from real7_transaction_condition where stock_code = ?", stockTransactionDataAll.Code).Scan(&real7TransactionCondition)

		if real7TransactionCondition == nil {
			continue
		}

		if real7TransactionCondition.StockCode == "600385" {
			io.Infoln("-----------------")
		}

		if real7TransactionCondition.Bias250 != 0 && real7TransactionCondition.Bias250 <= biasThresholdTop && real7TransactionCondition.Bias250 >= biasThresholdBottom {
			bullShortLine = real7TransactionCondition.MA250
			if stockTransactionDataAll.ClosePrice < bullShortLine {
				_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.variance_type = null, t.variance = null where t.stock_code = ?", real7TransactionCondition.StockCode)
			} else {
				// 计算variance250
				var closePrice249Array []float64 = make([]float64, 0)
				_real7TransactionConditionDaoImpl.db.Raw("select t1.close_price from ("+
					"select * from stock_transaction_data_all t where t.code_ = ? order by t.date_ desc) t1 "+
					"where rownum<=249", stockTransactionDataAll.Code).Scan(&closePrice249Array)
				var closePrice250Array []float64 = append(closePrice249Array, stockTransactionDataAll.ClosePrice)
				var sum float64 = 0
				for _, closePrice := range closePrice250Array {
					sum = sum + math.Pow(closePrice-real7TransactionCondition.MA250, 2)
				}
				var variance250 float64 = sum / 249
				_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.variance_type=?, t.variance=? "+
					"where t.stock_code = ?", constants.Variance250, variance250, real7TransactionCondition.StockCode)
				//io.Infoln("股票[%s]的牛熊线是MA250", stockTransactionDataAll.Code)
			}
			continue
		}
		if stockTransactionDataAll.Bias120 != 0 && stockTransactionDataAll.Bias120 <= biasThresholdTop && stockTransactionDataAll.Bias120 >= biasThresholdBottom {
			bullShortLine = stockTransactionDataAll.Ma120
			if stockTransactionDataAll.ClosePrice < bullShortLine {
				_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.variance_type = null, t.variance = null where t.stock_code = ?", real7TransactionCondition.StockCode)
			} else {
				// 计算variance120
				var closePrice119Array []float64 = make([]float64, 0)
				_real7TransactionConditionDaoImpl.db.Raw("select t1.close_price from ("+
					"select * from stock_transaction_data_all t where t.code_ = ? order by t.date_ desc) t1 "+
					"where rownum<=119", stockTransactionDataAll.Code).Scan(&closePrice119Array)
				var closePrice120Array []float64 = append(closePrice119Array, stockTransactionDataAll.ClosePrice)
				var sum float64 = 0
				for _, closePrice := range closePrice120Array {
					sum = sum + math.Pow(closePrice-real7TransactionCondition.MA120, 2)
				}
				var variance120 float64 = sum / 119
				_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.variance_type=?, t.variance=? "+
					"where t.stock_code = ?", constants.Variance120, variance120, stockTransactionDataAll.Code)
				//io.Infoln("股票[%s]的牛熊线是MA120", stockTransactionDataAll.Code)
			}
			continue
		}
		if stockTransactionDataAll.Bias60 != 0 && stockTransactionDataAll.Bias60 <= biasThresholdTop && stockTransactionDataAll.Bias60 >= biasThresholdBottom {
			bullShortLine = stockTransactionDataAll.Ma60
			if stockTransactionDataAll.ClosePrice < bullShortLine {
				_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.variance_type = null, t.variance = null where t.stock_code = ?", real7TransactionCondition.StockCode)
			} else {
				// 计算variance60
				var closePrice59Array []float64 = make([]float64, 0)
				_real7TransactionConditionDaoImpl.db.Raw("select t1.close_price from ("+
					"select * from stock_transaction_data_all t where t.code_ = ? order by t.date_ desc) t1 "+
					"where rownum<=59", stockTransactionDataAll.Code).Scan(&closePrice59Array)
				var closePrice60Array []float64 = append(closePrice59Array, stockTransactionDataAll.ClosePrice)
				var sum float64 = 0
				for _, closePrice := range closePrice60Array {
					sum = sum + math.Pow(closePrice-real7TransactionCondition.MA60, 2)
				}
				var variance60 float64 = sum / 59
				_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.variance_type=?, t.variance=? "+
					"where t.stock_code = ?", constants.Variance60, variance60, stockTransactionDataAll.Code)
				//io.Infoln("股票[%s]的牛熊线是MA60", stockTransactionDataAll.Code)
			}
			continue
		}
		if stockTransactionDataAll.Bias20 != 0 && stockTransactionDataAll.Bias20 <= biasThresholdTop && stockTransactionDataAll.Bias20 >= biasThresholdBottom {
			bullShortLine = stockTransactionDataAll.Ma20
			if stockTransactionDataAll.ClosePrice < bullShortLine {
				_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.variance_type = null, t.variance = null where t.stock_code = ?", real7TransactionCondition.StockCode)
			} else {
				// 计算variance20
				var closePrice19Array []float64 = make([]float64, 0)
				_real7TransactionConditionDaoImpl.db.Raw("select t1.close_price from ("+
					"select * from stock_transaction_data_all t where t.code_ = ? order by t.date_ desc) t1 "+
					"where rownum<=19", stockTransactionDataAll.Code).Scan(&closePrice19Array)
				var closePrice20Array []float64 = append(closePrice19Array, stockTransactionDataAll.ClosePrice)
				var sum float64 = 0
				for _, closePrice := range closePrice20Array {
					sum = sum + math.Pow(closePrice-real7TransactionCondition.MA20, 2)
				}
				var variance20 float64 = sum / 19
				_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.variance_type=?, t.variance=? "+
					"where t.stock_code = ?", constants.Variance20, variance20, stockTransactionDataAll.Code)
				//io.Infoln("股票[%s]的牛熊线是MA20", stockTransactionDataAll.Code)
			}
			continue
		}
		if stockTransactionDataAll.Bias10 != 0 && stockTransactionDataAll.Bias10 <= biasThresholdTop && stockTransactionDataAll.Bias10 >= biasThresholdBottom {
			bullShortLine = stockTransactionDataAll.Ma10
			if stockTransactionDataAll.ClosePrice < bullShortLine {
				_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.variance_type = null, t.variance = null where t.stock_code = ?", real7TransactionCondition.StockCode)
			} else {
				// 计算variance10
				var closePrice9Array []float64 = make([]float64, 0)
				_real7TransactionConditionDaoImpl.db.Raw("select t1.close_price from ("+
					"select * from stock_transaction_data_all t where t.code_ = ? order by t.date_ desc) t1 "+
					"where rownum<=9", stockTransactionDataAll.Code).Scan(&closePrice9Array)
				var closePrice10Array []float64 = append(closePrice9Array, stockTransactionDataAll.ClosePrice)
				var sum float64 = 0
				for _, closePrice := range closePrice10Array {
					sum = sum + math.Pow(closePrice-real7TransactionCondition.MA10, 2)
				}
				var variance10 float64 = sum / 9
				_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.variance_type=?, t.variance=? "+
					"where t.stock_code = ?", constants.Variance10, variance10, stockTransactionDataAll.Code)
				//io.Infoln("股票[%s]的牛熊线是MA10", stockTransactionDataAll.Code)
			}
			continue
		}
		if stockTransactionDataAll.Bias5 != 0 && stockTransactionDataAll.Bias5 <= biasThresholdTop && stockTransactionDataAll.Bias5 >= biasThresholdBottom {
			bullShortLine = stockTransactionDataAll.Ma5
			if stockTransactionDataAll.ClosePrice < bullShortLine {
				_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.variance_type = null, t.variance = null where t.stock_code = ?", real7TransactionCondition.StockCode)
			} else {
				// 计算variance5
				var closePrice4Array []float64 = make([]float64, 0)
				_real7TransactionConditionDaoImpl.db.Raw("select t1.close_price from ("+
					"select * from stock_transaction_data_all t where t.code_ = ? order by t.date_ desc) t1 "+
					"where rownum<=4", stockTransactionDataAll.Code).Scan(&closePrice4Array)
				var closePrice5Array []float64 = append(closePrice4Array, stockTransactionDataAll.ClosePrice)
				var sum float64 = 0
				for _, closePrice := range closePrice5Array {
					sum = sum + math.Pow(closePrice-real7TransactionCondition.MA5, 2)
				}
				var variance5 float64 = sum / 4
				_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.variance_type=?, t.variance=? "+
					"where t.stock_code = ?", constants.Variance5, variance5, stockTransactionDataAll.Code)
				//io.Infoln("股票[%s]的牛熊线是MA5", stockTransactionDataAll.Code)
			}
			continue
		}
		// 如果最后任何bias指标都无法确定哪一根均线可以作为牛熊线的话，则将前一个交易日的收盘价作为牛熊线。注意：现在数据库中last_close_price字段没有空值
		bullShortLine = stockTransactionDataAll.LastClosePrice
		if stockTransactionDataAll.ClosePrice < bullShortLine {
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.variance_type = null, t.variance = null where t.stock_code = ?", real7TransactionCondition.StockCode)
			continue
		} else {
			var avgClosePrice = (stockTransactionDataAll.LastClosePrice + stockTransactionDataAll.ClosePrice) / 2
			var variance2 = math.Pow(stockTransactionDataAll.LastClosePrice-avgClosePrice, 2) + math.Pow(stockTransactionDataAll.ClosePrice-avgClosePrice, 2)
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.variance_type=?, t.variance=? "+
				"where t.stock_code = ?", constants.Variance2, variance2, real7TransactionCondition.StockCode)
			//io.Infoln("股票[%s]的牛熊线是前一个交易日的收盘价", stockTransactionDataAll.Code)
		}
	}
}

// 过滤条件：过滤除过权的股票
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) FilterByXr(currentDate string, xrDateNumber int) {
	io.Infoln("过滤条件：过滤除过权的股票")

	var real7TransactionConditionArray model.Real7TransactionConditionArray
	_real7TransactionConditionDaoImpl.db.Raw("select * from real7_transaction_condition").Scan(&real7TransactionConditionArray)

	var beginDate string
	var endDate string
	var num int = 0

	for _, real7TransactionCondition := range real7TransactionConditionArray {
		// 判断日期currentDate之前的xrDateNumber个交易日中是否有除权的股票
		_real7TransactionConditionDaoImpl.db.Raw("select to_char(t4.date_, 'yyyy-mm-dd') from ("+
			"select * from ("+
			"select * from ("+
			"select * from stock_transaction_data_all t where t.code_=? and t.date_<=to_date(?,'yyyy-mm-dd') "+
			"order by t.date_ desc) "+
			"where rownum<=?) t3 "+
			"order by t3.date_ asc) t4 "+
			"where rownum<=1",
			real7TransactionCondition.StockCode, currentDate, xrDateNumber).Scan(&beginDate)
		beginDateXr, _ := helper.FilterStockXr_stock(real7TransactionCondition.StockCode, beginDate, currentDate)
		if beginDateXr == true {
			num++
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.xr = 1 where t.stock_code = ?", real7TransactionCondition.StockCode)
		}

		// 判断日期currentDate之后的xrDateNumber个交易日中是否有除权的股票
		_real7TransactionConditionDaoImpl.db.Raw("select to_char(t4.date_, 'yyyy-mm-dd') from ("+
			"select * from ("+
			"select * from ("+
			"select * from stock_transaction_data_all t where t.code_=? and t.date_>=to_date(?,'yyyy-mm-dd') "+
			"order by t.date_ asc) "+
			"where rownum<=?) t3 "+
			"order by t3.date_ desc) t4 "+
			"where rownum<=1",
			real7TransactionCondition.StockCode, currentDate, xrDateNumber).Scan(&endDate)
		endDateXr, _ := helper.FilterStockXr_stock(real7TransactionCondition.StockCode, currentDate, endDate)
		if endDateXr == true {
			num++
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.xr = 1 where t.stock_code = ?", real7TransactionCondition.StockCode)
		}
	}
}

// 600679
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) PrepareForBullShortLine(currentDate string) {
	io.Infoln("600679")

	var real7TransactionConditionArray model.Real7TransactionConditionArray
	_real7TransactionConditionDaoImpl.db.Raw("select * from real7_transaction_condition").Scan(&real7TransactionConditionArray)

	for _, real7TransactionCondition := range real7TransactionConditionArray {
		// 更新prepare_ma*字段
		_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t "+
			"set t.prepare_ma5 = CASE WHEN (select count(*) from (select * from stock_transaction_data_all t "+
			"where t.code_=? and t.date_<to_date(?, 'yyyy-mm-dd') order by t.date_ desc) t1 where rownum<=4)=4 THEN "+
			"(select sum(t1.close_price) from (select * from stock_transaction_data_all t "+
			"where t.code_=? and t.date_<to_date(?, 'yyyy-mm-dd') order by t.date_ desc)t1 where rownum<=4) ELSE null END, "+
			"t.prepare_ma10 = CASE WHEN (select count(*) from (select * from stock_transaction_data_all t "+
			"where t.code_=? and t.date_<to_date(?, 'yyyy-mm-dd') order by t.date_ desc) t1 where rownum<=9)=9 THEN "+
			"(select sum(t1.close_price) from (select * from stock_transaction_data_all t "+
			"where t.code_=? and  t.date_<to_date(?, 'yyyy-mm-dd') order by t.date_ desc) t1 where rownum<=9) ELSE null END, "+
			"t.prepare_ma20 = CASE WHEN(select count(*) from (select * from stock_transaction_data_all t "+
			"where t.code_=? and t.date_<to_date(?, 'yyyy-mm-dd') order by t.date_ desc) t1 where rownum<=19)=19 THEN "+
			"(select sum(t1.close_price) from (select * from stock_transaction_data_all t "+
			"where t.code_=? and t.date_<to_date(?, 'yyyy-mm-dd') order by t.date_ desc) t1 where rownum<=19) ELSE null END, "+
			"t.prepare_ma60 = CASE WHEN (select count(*) from (select * from stock_transaction_data_all t "+
			"where t.code_=? and t.date_<to_date(?, 'yyyy-mm-dd') order by t.date_ desc) t1 where rownum<=59)=59 THEN "+
			"(select sum(t1.close_price) from (select * from stock_transaction_data_all t "+
			"where t.code_=? and t.date_<to_date(?, 'yyyy-mm-dd') order by t.date_ desc) t1 where rownum<=59) ELSE null END, "+
			"t.prepare_ma120 = CASE WHEN (select count(*) from (select * from stock_transaction_data_all t "+
			"where t.code_=? and t.date_<to_date(?, 'yyyy-mm-dd') order by t.date_ desc) t1 where rownum<=119)=119 THEN "+
			"(select sum(t1.close_price) from (select * from stock_transaction_data_all t "+
			"where t.code_=? and t.date_<to_date(?, 'yyyy-mm-dd') order by t.date_ desc) t1 where rownum<=119) ELSE null END, "+
			"t.prepare_ma250 = CASE WHEN (select count(*) from (select * from stock_transaction_data_all t "+
			"where t.code_=? and t.date_<to_date(?, 'yyyy-mm-dd') order by t.date_ desc) t1 where rownum<=249)=249 THEN "+
			"(select sum(t1.close_price) from (select * from stock_transaction_data_all t "+
			"where t.code_=? and t.date_<to_date(?, 'yyyy-mm-dd') order by t.date_ desc) t1 where rownum<=249) ELSE null END "+
			"where t.stock_code=?",
			real7TransactionCondition.StockCode, currentDate, real7TransactionCondition.StockCode, currentDate,
			real7TransactionCondition.StockCode, currentDate, real7TransactionCondition.StockCode, currentDate,
			real7TransactionCondition.StockCode, currentDate, real7TransactionCondition.StockCode, currentDate,
			real7TransactionCondition.StockCode, currentDate, real7TransactionCondition.StockCode, currentDate,
			real7TransactionCondition.StockCode, currentDate, real7TransactionCondition.StockCode, currentDate,
			real7TransactionCondition.StockCode, currentDate, real7TransactionCondition.StockCode, currentDate,
			real7TransactionCondition.StockCode)

		var threshold float64
		var stockTransactionDataAll *model.StockTransactionDataAll
		_real7TransactionConditionDaoImpl.db.Raw("select * from ("+
			"select * from stock_transaction_data_all t2 where t2.code_ = ? and t2.date_ < to_date(?, 'yyyy-mm-dd') "+
			"order by t2.date_ desc) std "+
			"where rownum <= 1", real7TransactionCondition.StockCode, currentDate).Scan(&stockTransactionDataAll)

		// 计算prepare_macd_gold_cross、prepare_macd_dead_cross字段
		var prepareMacdGoldCross float64
		var prepareMacdDeadCross float64
		_real7TransactionConditionDaoImpl.db.Raw("select (t.ema12 * (11 / 13 * 2 / 10 - 11 / 13) + t.ema26 * (25 / 27 - 25 / 27 * 2 / 10) + "+
			"t.dea * 8 / 10) / (2 / 13 - 2 / 27 - 2 / 13 * 2 / 10 + 2 / 27 * 2 / 10) "+
			"from stock_transaction_data_all t where t.code_ = ? and t.date_ = ?",
			real7TransactionCondition.StockCode, stockTransactionDataAll.Date).Scan(&threshold)
		// 已经是金叉状态，保存出现死叉时的收盘价
		if stockTransactionDataAll.Dif >= stockTransactionDataAll.Dea {
			prepareMacdDeadCross = threshold
		}
		// 已经是死叉状态，保存出现金叉时的收盘价
		if stockTransactionDataAll.Dif < stockTransactionDataAll.Dea {
			prepareMacdGoldCross = threshold
		}

		// 计算prepare_close_price_gold_cross、prepare_close_price_dead_cross字段
		var prepareClosePriceGoldCross float64
		var prepareClosePriceDeadCross float64
		_real7TransactionConditionDaoImpl.db.Raw("select sum(t1.close_price) / 4 "+
			"from (select * from stock_transaction_data_all t where t.code_ = ? and t.date_ <= ? "+
			"order by t.date_ desc) t1 "+
			"where rownum <= 4", real7TransactionCondition.StockCode, stockTransactionDataAll.Date).Scan(&threshold)
		// 已经是金叉状态，保存出现死叉时的收盘价
		if stockTransactionDataAll.ClosePrice > stockTransactionDataAll.Ma5 {
			prepareClosePriceDeadCross = threshold
		}
		// 已经是死叉状态，保存出现金叉时的收盘价
		if stockTransactionDataAll.ClosePrice <= stockTransactionDataAll.Ma5 {
			prepareClosePriceGoldCross = threshold
		}

		if real7TransactionCondition.StockCode == "300450" {
			io.Infoln("%%%%%%%%%%%%%%")
		}
		// 计算prepare_hei_kin_ashi_up、prepare_hei_kin_ashi_down字段
		var prepareHeiKinAshiUp float64
		var prepareHeiKinAshiDown float64
		_real7TransactionConditionDaoImpl.db.Raw("select (t1.ha_open_price + t1.ha_close_price) / 2 "+
			"from (select * from stock_transaction_data_all t where t.code_ = ? and t.date_ <= ? "+
			"order by t.date_ desc) t1 "+
			"where rownum <= 1", real7TransactionCondition.StockCode, stockTransactionDataAll.Date).Scan(&threshold)
		// 已经是上涨趋势，保存出现下跌趋势时的开盘价
		if stockTransactionDataAll.HaClosePrice >= stockTransactionDataAll.HaOpenPrice {
			prepareHeiKinAshiDown = threshold
		}
		// 已经是下跌趋势，保存出现上涨趋势时的开盘价
		if stockTransactionDataAll.HaClosePrice < stockTransactionDataAll.HaOpenPrice {
			prepareHeiKinAshiUp = threshold
		}

		// 计算prepare_kd_gold_cross、prepare_ke_dead_cross字段
		var prepareKdGoldCross float64
		var prepareKdDeadCross float64
		_real7TransactionConditionDaoImpl.db.Raw("select (2 / 3 * t.d - 4 / 9 * t.k) * 9 / 2 "+
			"from stock_transaction_data_all t where t.code_ = ? and t.date_ = ?",
			real7TransactionCondition.StockCode, stockTransactionDataAll.Date).Scan(&threshold)
		// 已经是金叉状态，保存出现死叉时的RSV
		if stockTransactionDataAll.K >= stockTransactionDataAll.D {
			prepareKdDeadCross = threshold
		}
		// 已经是死叉状态，保存出现金叉时的RSV
		if stockTransactionDataAll.K < stockTransactionDataAll.D {
			prepareKdGoldCross = threshold
		}

		// 更新prepare_macd_gold_cross、prepare_close_price_gold_cross字段
		_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t1 "+
			"set t1.prepare_macd_gold_cross = ?, t1.prepare_macd_dead_cross = ?, "+
			"t1.prepare_close_price_gold_cross = ?, t1.prepare_close_price_dead_cross = ?, "+
			"t1.prepare_hei_kin_ashi_up = ?, t1.prepare_hei_kin_ashi_down = ?,"+
			"t1.prepare_kd_gold_cross = ?, t1.prepare_kd_dead_cross = ? "+
			"where t1.stock_code = ?", prepareMacdGoldCross, prepareMacdDeadCross,
			prepareClosePriceGoldCross, prepareClosePriceDeadCross,
			prepareHeiKinAshiUp, prepareHeiKinAshiDown,
			prepareKdGoldCross, prepareKdDeadCross,
			real7TransactionCondition.StockCode)
		//_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t1 "+
		//	"set t1.prepare_macd_gold_cross = ?, t1.prepare_macd_dead_cross = ?, "+
		//	"t1.prepare_close_price_gold_cross = ?, t1.prepare_close_price_dead_cross = ?, "+
		//	"t1.prepare_hei_kin_ashi_up = ?, t1.prepare_hei_kin_ashi_down = ?,"+
		//	"t1.prepare_kd_gold_cross = ?, t1.prepare_kd_dead_cross = ? "+
		//	"where t1.stock_code = ?", iUtil.If(prepareMacdGoldCross != 0, prepareMacdGoldCross, gorm.Expr("NULL")).(float64),
		//	iUtil.If(prepareMacdDeadCross != 0, prepareMacdDeadCross, gorm.Expr("NULL")).(float64),
		//	iUtil.If(prepareClosePriceGoldCross != 0, prepareClosePriceGoldCross, gorm.Expr("NULL")).(float64),
		//	iUtil.If(prepareClosePriceDeadCross != 0, prepareClosePriceDeadCross, gorm.Expr("NULL")).(float64),
		//	iUtil.If(prepareHeiKinAshiUp != 0, prepareHeiKinAshiUp, gorm.Expr("NULL")).(float64),
		//	iUtil.If(prepareHeiKinAshiDown != 0, prepareHeiKinAshiDown, gorm.Expr("NULL")).(float64),
		//	iUtil.If(prepareKdGoldCross != 0, prepareKdGoldCross, gorm.Expr("NULL")).(float64),
		//	iUtil.If(prepareKdDeadCross != 0, prepareKdDeadCross, gorm.Expr("NULL")).(float64),
		//	real7TransactionCondition.StockCode)
	}
}

// 过滤条件：如果这只股票在当天除权了，则删除
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) FilterByXrByCurrentDate(stockTransactionDataAllArray model.StockTransactionDataAllArray, currentDate string) {
	io.Infoln("过滤条件：如果这只股票在当天除权了，则删除")

	var real7TransactionConditionArray model.Real7TransactionConditionArray
	_real7TransactionConditionDaoImpl.db.Raw("select * from real7_transaction_condition").Scan(&real7TransactionConditionArray)

	var beginDate string

	for _, real7TransactionCondition := range real7TransactionConditionArray {
		_real7TransactionConditionDaoImpl.db.Raw("select to_char(t4.date_, 'yyyy-mm-dd') from ("+
			"select * from ("+
			"select * from ("+
			"select * from stock_transaction_data_all t where t.code_=? and t.date_<=to_date(?,'yyyy-mm-dd') "+
			"order by t.date_ desc) "+
			"where rownum<=?) t3 "+
			"order by t3.date_ asc) t4 "+
			"where rownum<=1",
			real7TransactionCondition.StockCode, currentDate, properties.Real7Properties_.XrDateNumber).Scan(&beginDate)
		beginDateXr, _ := helper.FilterStockXr_stock(real7TransactionCondition.StockCode, beginDate, currentDate)
		if beginDateXr == true {
			_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.xr = 1 where t.stock_code = ?", real7TransactionCondition.StockCode)
		}

		//// 表示某一段时间内某只股票最大的下跌幅度
		//var downPercentage float64 = (stockTransactionDataAll.ClosePrice - stockTransactionDataAll.LastClosePrice) / stockTransactionDataAll.LastClosePrice * 100
		//
		//// 在2020-08-24之前，并且当下跌幅度大于-11%，就表示有可能是除权
		//if util.Before(currentDate, helper.JUDGEMENT_DATE) && downPercentage < -11 {
		//	_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.xr = 1 where t.stock_code = ?", stockTransactionDataAll.Code)
		//}
		//
		//// 在2020-08-24之后，如果股票代码以300开头，并且下跌幅度大于22%，则表示有可能是除权
		//if util.AfterOrEqual(currentDate, helper.JUDGEMENT_DATE) && strings.Compare(string(stockTransactionDataAll.Code[3]), "300") == 0 && downPercentage < -22 {
		//	_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.xr = 1 where t.stock_code = ?", stockTransactionDataAll.Code)
		//}
		//
		//// 在2020-08-24之后，如果股票代码不是300开头，并且下跌幅度大于11%，则表示有可能是除权
		//if util.AfterOrEqual(currentDate, helper.JUDGEMENT_DATE) && strings.Compare(string(stockTransactionDataAll.Code[3]), "300") != 0 && downPercentage < -11 {
		//	_real7TransactionConditionDaoImpl.db.Exec("update real7_transaction_condition t set t.xr = 1 where t.stock_code = ?", stockTransactionDataAll.Code)
		//}
	}
}

// 给出买卖建议，并发送邮件
func (_real7TransactionConditionDaoImpl *Real7TransactionConditionDaoImpl) GiveSuggestionAndSendEmail(currentStockTransactionDataAllArray model.StockTransactionDataAllArray,
	maxHoldStockNumber int, buySuggestion bool, sellSuggestion bool) {
	io.Infoln("开始给出买卖建议，并发送邮件")

	var buyOrSellStockSuggestion domain.BuyOrSellStockSuggestion
	var sellStockSuggestionArray domain.SellStockSuggestionArray
	var buyStockSuggestionArray domain.BuyStockSuggestionArray
	var real7TransactionConditionArray model.Real7TransactionConditionArray

	// 卖出建议
	if sellSuggestion {
		_real7TransactionConditionDaoImpl.db.Raw("select t1.* " +
			"from real7_stock_transaction_record t1 " +
			"join (" +
			"select * from real7_transaction_condition t where t.direction = -1) rsc " +
			"on rsc.stock_code=t1.stock_code and t1.sell_date is null").Scan(&real7TransactionConditionArray)
		if len(real7TransactionConditionArray) == 0 {
			io.Infoln("没有卖出建议")
		} else {
			for _, real7TransactionCondition := range real7TransactionConditionArray {
				var stockInfo model.StockInfo
				_real7TransactionConditionDaoImpl.db.Raw("select * from stock_info t where t.code_ = ?", real7TransactionCondition.StockCode).Scan(&stockInfo)
				var sellStockSuggestion domain.SellStockSuggestion = domain.SellStockSuggestion{real7TransactionCondition.StockCode, stockInfo.Name, real7TransactionCondition.FilterType, real7TransactionCondition.Direction}
				sellStockSuggestionArray = append(sellStockSuggestionArray, &sellStockSuggestion)
			}
			buyOrSellStockSuggestion.SellStockSuggestionArray = sellStockSuggestionArray
		}
	}

	if buySuggestion {
		// 买入建议
		_real7TransactionConditionDaoImpl.db.Raw("select * from real7_transaction_condition t " +
			"where t.close_price_out_of_limit is null and t.variance_type is not null and t.variance is not null " +
			"and t.xr is null and t.suspension is null and t.filter_type is not null and t.accumulative_profit_loss is not null " +
			"and t.direction is not null and t.is_transaction_date = 1 and t.direction = 1 and t.is_limit_up = 2 " +
			"order by t.accumulative_profit_loss desc").Scan(&real7TransactionConditionArray)
		if len(real7TransactionConditionArray) == 0 {
			io.Infoln("没有买入建议")
		} else {
			for _, real7TransactionCondition := range real7TransactionConditionArray {
				// 查询股票信息
				var stockInfo model.StockInfo
				_real7TransactionConditionDaoImpl.db.Raw("select * from stock_info t where t.code_ = ?", real7TransactionCondition.StockCode).Scan(&stockInfo)
				// 查询收盘价
				var stockTransactionDataAll *model.StockTransactionDataAll = helper.FindStockTransactionDataAllByCode(currentStockTransactionDataAllArray, real7TransactionCondition.StockCode)
				// 计算买入股数
				var real7AccountArray model.Real7AccountArray
				_real7TransactionConditionDaoImpl.db.Raw("select * from real7_account").Scan(&real7AccountArray)
				var sellAmount int = int(real7AccountArray[0].TotalAssets/float64(maxHoldStockNumber)/stockTransactionDataAll.ClosePrice/100) * 100
				var buyStockSuggestion domain.BuyStockSuggestion = domain.BuyStockSuggestion{real7TransactionCondition.StockCode, stockInfo.Name, real7TransactionCondition.FilterType, real7TransactionCondition.Direction, stockTransactionDataAll.ClosePrice, sellAmount}
				buyStockSuggestionArray = append(buyStockSuggestionArray, &buyStockSuggestion)
			}
			buyOrSellStockSuggestion.BuyStockSuggestionArray = buyStockSuggestionArray
		}
	}

	// 打印买卖建议
	data, _ := json.MarshalIndent(&buyOrSellStockSuggestion, "", "\t")
	io.Infoln("买卖建议：" + string(data))

	// 发送邮件
	e := email.NewEmail()
	//设置发送方的邮箱
	e.From = properties.Real7Properties_.EmailFrom
	// 设置接收方的邮箱
	e.To = []string{properties.Real7Properties_.EmailTo}
	//设置主题
	e.Subject = properties.Real7Properties_.EmailSubject
	// 格式化买卖建议
	var formatBuyOrSellStockSuggestion string
	if buyOrSellStockSuggestion.SellStockSuggestionArray != nil && len(buyOrSellStockSuggestion.SellStockSuggestionArray) > 0 {
		formatBuyOrSellStockSuggestion = "卖出建议：\n"
		for _, sellSuggestion := range buyOrSellStockSuggestion.SellStockSuggestionArray {
			formatBuyOrSellStockSuggestion = formatBuyOrSellStockSuggestion + "股票名称：" + sellSuggestion.Name + "\t\t股票代码：" +
				sellSuggestion.StockCode + "\t\t操作方向：" + fmt.Sprintf("%d", sellSuggestion.Direction) + "\t\t过滤类型：" +
				fmt.Sprintf("%d", sellSuggestion.FilterType) + "\n"
		}
	}
	if buyOrSellStockSuggestion.BuyStockSuggestionArray != nil && len(buyOrSellStockSuggestion.BuyStockSuggestionArray) > 0 {
		formatBuyOrSellStockSuggestion = formatBuyOrSellStockSuggestion + "买入建议：\n"
		for _, buySuggestion := range buyOrSellStockSuggestion.BuyStockSuggestionArray {
			formatBuyOrSellStockSuggestion = formatBuyOrSellStockSuggestion + "股票名称：" + buySuggestion.Name + "\t\t股票代码：" +
				buySuggestion.StockCode + "\t\t操作方向：" + fmt.Sprintf("%d", buySuggestion.Direction) + "\t\t过滤类型：" +
				fmt.Sprintf("%d", buySuggestion.FilterType) + "\t\t买入价格：" + fmt.Sprintf("%v", buySuggestion.BuyPrice) +
				"\t\t买入数量：" + fmt.Sprintf("%d", buySuggestion.BuyAmount) + "\n"
		}
	}
	//设置文件发送的内容
	e.Text = []byte(string(formatBuyOrSellStockSuggestion))
	//设置服务器相关的配置
	//err := e.Send("smtp.sohu.com:25", smtp.PlainAuth("", "lishen821412", "Cetc1234", "smtp.sohu.com"))
	err := e.Send(properties.Real7Properties_.EmailAddress, smtp.PlainAuth("", properties.Real7Properties_.EmailUsername, properties.Real7Properties_.EmailPassword, properties.Real7Properties_.EmailHost))
	if err != nil {
		io.Fatalf(err.Error())
	}

	io.Infoln("发送给出买卖建议，并发送邮件")
}
