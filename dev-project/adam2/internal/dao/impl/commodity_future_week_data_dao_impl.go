package impl

import (
	"adam2/internal/model"
)

type CommodityFutureWeekDataDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetCommodityFutureWeekDataDaoImpl() *CommodityFutureWeekDataDaoImpl {
	return &CommodityFutureWeekDataDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_commodityFutureWeekDataDaoImpl *CommodityFutureWeekDataDaoImpl) FindTableName() string {
	return "COMMODITY_FUTURE_WEEK_DATA"
}

// 根据code、开始时间和结束时间查询记录
func (_commodityFutureWeekDataDaoImpl *CommodityFutureWeekDataDaoImpl) FindByCodeBetweenDate(code string, beginDate string, endDate string) model.CommodityFutureWeekDataArray {
	var sql string
	var commodityFutureWeekDataArray model.CommodityFutureWeekDataArray
	sql = "SELECT * FROM commodity_future_week_data t " +
		"where t.begin_date>=to_date(?, 'yyyy-mm-dd') and t.end_date<=to_date(?, 'yyyy-mm-dd') " +
		"and t.code=? order by t.end_date asc"
	_commodityFutureWeekDataDaoImpl.db.Raw(sql, beginDate, endDate, code).Scan(&commodityFutureWeekDataArray)
	return commodityFutureWeekDataArray
}

// 查询之前n周内的最高价
func (_commodityFutureWeekDataDaoImpl *CommodityFutureWeekDataDaoImpl) FindMaxHighestPriceWithNDate(code string, n int, endDate string) float64 {
	var maxHighestPrice float64
	var sql string = "select max(t1.highest_price) from (SELECT * FROM commodity_future_week_data t " +
		"where t.end_date<=to_date(?, 'yyyy-mm-dd') " +
		"and t.code_=? order by t.end_date desc) t1 where rownum<=?"
	_commodityFutureWeekDataDaoImpl.db.Raw(sql, endDate, code, n).Scan(&maxHighestPrice)
	return maxHighestPrice
}

// 查询之前n周内的最低价
func (_commodityFutureWeekDataDaoImpl *CommodityFutureWeekDataDaoImpl) FindMinLowestPriceWithNDate(code string, n int, endDate string) float64 {
	var minLowestPrice float64
	var sql string = "select min(t1.lowest_price) from (SELECT * FROM commodity_future_week_data t " +
		"where t.end_date<=to_date(?, 'yyyy-mm-dd') " +
		"and t.code_=? order by t.end_date desc) t1 where rownum<=?"
	_commodityFutureWeekDataDaoImpl.db.Raw(sql, endDate, code, n).Scan(&minLowestPrice)
	return minLowestPrice
}