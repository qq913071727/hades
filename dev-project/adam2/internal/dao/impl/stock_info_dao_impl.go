package impl

import (
	"adam2/internal/model"
)

type StockInfoDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetStockInfoDaoImpl() *StockInfoDaoImpl {
	return &StockInfoDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_stockInfoDaoImpl *StockInfoDaoImpl) FindTableName() string {
	return "STOCK_INFO"
}

// 返回全部记录
func (_stockInfoDaoImpl *StockInfoDaoImpl) FindAll() (model.StockInfoArray, error) {
	var stockInfoArray model.StockInfoArray
	_stockInfoDaoImpl.db.Raw("select * from stock_info t").Scan(&stockInfoArray)
	return stockInfoArray, nil
}

// 根据code查询记录
func (_stockInfoDaoImpl *StockInfoDaoImpl) FindByCode(code string) model.StockInfo {
	var stockInfo model.StockInfo
	_stockInfoDaoImpl.db.Raw("select * from stock_info t where t.code_=?", code).Scan(&stockInfo)
	return stockInfo
}
