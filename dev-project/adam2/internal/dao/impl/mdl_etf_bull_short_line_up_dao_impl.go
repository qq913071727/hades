package impl

import (
	"adam2/internal/domain"
)

type MdlEtfBullShortLineUpDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetMdlEtfBullShortLineUpDaoImpl() *MdlEtfBullShortLineUpDaoImpl {
	return &MdlEtfBullShortLineUpDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_mdlEtfBullShortLineUpDaoImpl *MdlEtfBullShortLineUpDaoImpl) FindTableName() string {
	return "MDL_ETL_BULL_SHORT_LINE_UP"
}

// 查找收益率和相关系数
func (_mdlEtfBullShortLineUpDaoImpl *MdlEtfBullShortLineUpDaoImpl) FindProfitAndLossRateAndCorrelation250WithAvgClosePrice() domain.ProfitAndLossRateAndCorrelation250WithAvgClosePriceArray {
	var profitAndLossRateAndCorrelation250WithAvgClosePriceArray domain.ProfitAndLossRateAndCorrelation250WithAvgClosePriceArray
	_mdlEtfBullShortLineUpDaoImpl.db.Raw("select mebslu.etf_code stock_code, mebslu.buy_date, mebslu.profit_loss profit_and_loss_rate, " +
		"etd.correlation250_with_avg_c_p " +
		"from mdl_etf_bull_short_line_up mebslu " +
		"join etf_transaction_data etd on etd.code_=mebslu.etf_code and etd.date_=mebslu.buy_date " +
		"where etd.correlation250_with_avg_c_p is not null and mebslu.profit_loss<=100").Scan(&profitAndLossRateAndCorrelation250WithAvgClosePriceArray)
	return profitAndLossRateAndCorrelation250WithAvgClosePriceArray
}
