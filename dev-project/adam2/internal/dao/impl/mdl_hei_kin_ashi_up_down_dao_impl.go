package impl

type MdlHeiKinAshiUpDownDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetMdlHeiKinAshiUpDownDaoImpl() *MdlHeiKinAshiUpDownDaoImpl {
	return &MdlHeiKinAshiUpDownDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_mdlHeiKinAshiUpDownDaoImpl *MdlHeiKinAshiUpDownDaoImpl) FindTableName() string {
	return "MDL_HEI_KIN_ASHI_UP_DOWN"
}
