package impl

type MdlClosePriceMa5GoldCrossDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetMdlClosePriceMa5GoldCrossDaoImpl() *MdlClosePriceMa5GoldCrossDaoImpl {
	return &MdlClosePriceMa5GoldCrossDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_mdlClosePriceMa5GoldCrossDaoImpl *MdlClosePriceMa5GoldCrossDaoImpl) FindTableName() string {
	return "MDL_CLOSE_PRICE_MA5_GOLD_CROSS"
}
