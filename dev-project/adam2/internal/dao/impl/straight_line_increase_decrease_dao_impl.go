package impl

import "adam2/internal/model"

type StraightLineIncreaseDecreaseDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetStraightLineIncreaseDecreaseDaoImpl() *StraightLineIncreaseDecreaseDaoImpl {
	return &StraightLineIncreaseDecreaseDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_straightLineIncreaseDecreaseDaoImpl *StraightLineIncreaseDecreaseDaoImpl) FindTableName() string {
	return "STRAIGHT_LINE_INCR_DECR"
}

// 添加
func (_straightLineIncreaseDecreaseDaoImpl *StraightLineIncreaseDecreaseDaoImpl) Add(straightLineIncreaseDecrease *model.StraightLineIncreaseDecrease) error {
	var result = _straightLineIncreaseDecreaseDaoImpl.db.Create(straightLineIncreaseDecrease)
	return result.Error
}
