package impl

import (
	"adam2/internal/constants"
	"adam2/internal/model"
	"anubis-framework/pkg/util"
	"time"
)

type TrainingTransactionDataDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetTrainingTransactionDataDaoImpl() *TrainingTransactionDataDaoImpl {
	return &TrainingTransactionDataDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_trainingTransactionDataDaoImpl *TrainingTransactionDataDaoImpl) FindTableName() string {
	return "TRNG_TRANSACTION_DATA"
}

// 根据trng_info_id字段删除记录
func (_trainingTransactionDataDaoImpl *TrainingTransactionDataDaoImpl) DeleteTrainingTransactionDataByTrainingInfoId(trainingInfoId int) {
	_trainingTransactionDataDaoImpl.db.Where("trng_info_id = ?", trainingInfoId).Delete(&model.TrainingTransactionData{})
}

// 保存
func (_trainingTransactionDataDaoImpl *TrainingTransactionDataDaoImpl) Save(trainingTransactionData model.TrainingTransactionData) {
	_trainingTransactionDataDaoImpl.db.Create(&trainingTransactionData)
}

// 根据trg_info_id，判断是否是持仓状态
func (_trainingTransactionDataDaoImpl *TrainingTransactionDataDaoImpl) IsHoldingShareByTrainingInfoId(trainingInfoId int) bool {
	var trainingTransactionData model.TrainingTransactionData
	_trainingTransactionDataDaoImpl.db.Raw("select * from ("+
		"select * from trng_transaction_data t where t.trng_info_id=? order by t.create_time desc) t1 where rownum<=1",
		trainingInfoId).Find(&trainingTransactionData)
	if trainingTransactionData.Market == constants.StockMarket {
		if trainingTransactionData.SellDate.IsZero() {
			return true
		} else {
			return false
		}
	}
	if trainingTransactionData.Market == constants.CommodityFutureMarket {
		if !trainingTransactionData.BuyDate.IsZero() && !trainingTransactionData.SellDate.IsZero() {
			return true
		} else {
			return false
		}
	}
	return false
}

// 根据训练id查询记录，其中sell_date字段需要是空
func (_trainingTransactionDataDaoImpl *TrainingTransactionDataDaoImpl) FindByTrainingInfoIdAndSellDateNull(trainingInfoId int) model.TrainingTransactionData {
	var trainingTransactionData model.TrainingTransactionData
	_trainingTransactionDataDaoImpl.db.Raw("select * from trng_transaction_data t where t.trng_info_id=? and t.sell_date is null",
		trainingInfoId).Find(&trainingTransactionData)
	return trainingTransactionData
}

// 根据训练id查询记录，其中buy_date字段需要是空
func (_trainingTransactionDataDaoImpl *TrainingTransactionDataDaoImpl) FindByTrainingInfoIdAndBuyDateNull(trainingInfoId int) model.TrainingTransactionData {
	var trainingTransactionData model.TrainingTransactionData
	_trainingTransactionDataDaoImpl.db.Raw("select * from trng_transaction_data t where t.trng_info_id=? and t.buy_date is null",
		trainingInfoId).Find(&trainingTransactionData)
	return trainingTransactionData
}

// 根据id，更新sell_date、sell_price、profit_and_loss和profit_and_loss_rate
func (_trainingTransactionDataDaoImpl *TrainingTransactionDataDaoImpl) UpdateSellDateAndSellPriceAndProfitAndLossRateById(id int, sellDate string, sellPrice float64, profitAndLoss float64, profitAndLossRate float64) {
	var _sellDate time.Time = util.StringToDate(sellDate)
	_trainingTransactionDataDaoImpl.db.Model(&model.TrainingTransactionData{}).
		Where("id = ?", id, "").Updates(model.TrainingTransactionData{SellDate: _sellDate, SellPrice: sellPrice, ProfitAndLoss: profitAndLoss, ProfitAndLossRate: profitAndLossRate, UpdateTime: time.Now()})
}

// 根据id，更新buy_date、buy_price、profit_and_loss和profit_and_loss
func (_trainingTransactionDataDaoImpl *TrainingTransactionDataDaoImpl) UpdateBuyDateAndBuyPriceAndProfitAndLossRateById(id int, buyDate string, buyPrice float64, profitAndLoss float64, profitAndLossRate float64) {
	var _buyDate time.Time = util.StringToDate(buyDate)
	_trainingTransactionDataDaoImpl.db.Model(&model.TrainingTransactionData{}).
		Where("id = ?", id, "").Updates(model.TrainingTransactionData{BuyDate: _buyDate, BuyPrice: buyPrice, ProfitAndLoss: profitAndLoss, ProfitAndLossRate: profitAndLossRate, UpdateTime: time.Now()})
}

// 根据训练id查询记录，要求buy_date和sell_date都不为空，并按照buy_date升序排列
func (_trainingTransactionDataDaoImpl *TrainingTransactionDataDaoImpl) FindByTrainingInfoIdAndBuyDateNotNullAndSellDateNotNullOderByBuyDateAsc(trainingInfoId int) model.TrainingTransactionDataArray {
	var trainingTransactionDataArray model.TrainingTransactionDataArray
	_trainingTransactionDataDaoImpl.db.Raw("select * from trng_transaction_data t where t.trng_info_id=? and t.buy_date is not null "+
		"and t.sell_date is not null order by t.buy_date asc",
		trainingInfoId).Find(&trainingTransactionDataArray)
	return trainingTransactionDataArray
}

// 连接trng_info表和trng_transaction_data表，然后根据trng_info_id查询，最后按照begin_date升序排列
func (_trainingTransactionDataDaoImpl *TrainingTransactionDataDaoImpl) JoinTrainingInfoByTrainingInfoNameOrderByBeginDateAsc(trainingInfoName string) model.TrainingTransactionDataArray {
	var trainingTransactionDataArray model.TrainingTransactionDataArray
	_trainingTransactionDataDaoImpl.db.Raw("select ttd.* from TRNG_INFO ti join TRNG_TRANSACTION_DATA ttd on ti.ID=ttd.TRNG_INFO_ID "+
		"where ti.NAME=? order by ti.BEGIN_DATE asc",
		trainingInfoName).Find(&trainingTransactionDataArray)
	return trainingTransactionDataArray
}
