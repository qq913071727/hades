package impl

import (
	"adam2/internal/model"
)

type StockWeekDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetStockWeekDaoImpl() *StockWeekDaoImpl {
	return &StockWeekDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_stockWeekDaoImpl *StockWeekDaoImpl) FindTableName() string {
	return "STOCK_WEEK"
}

// 根据code、开始时间和结束时间查询记录
func (_stockWeekDaoImpl *StockWeekDaoImpl) FindByCodeBetweenDate(code string, beginDate string, endDate string) model.StockWeekArray {
	var sql string
	var _stockWeekArray model.StockWeekArray
	sql = "SELECT * FROM stock_week t " +
		"where t.begin_date>=to_date(?, 'yyyy-mm-dd') and t.end_date<=to_date(?, 'yyyy-mm-dd') " +
		"and t.code_=? order by t.end_date asc"
	_stockWeekDaoImpl.db.Raw(sql, beginDate, endDate, code).Scan(&_stockWeekArray)
	return _stockWeekArray
}

// 查询之前n周内的最高价
func (_stockWeekDaoImpl *StockWeekDaoImpl) FindMaxHighestPriceWithNDate(code string, n int, endDate string) float64 {
	var maxHighestPrice float64
	var sql string = "select max(t1.highest_price) from (SELECT * FROM stock_week t " +
		"where t.end_date<=to_date(?, 'yyyy-mm-dd') " +
		"and t.code_=? order by t.end_date desc) t1 where rownum<=?"
	_stockWeekDaoImpl.db.Raw(sql, endDate, code, n).Scan(&maxHighestPrice)
	return maxHighestPrice
}

// 查询之前n周内的最低价
func (_stockWeekDaoImpl *StockWeekDaoImpl) FindMinLowestPriceWithNDate(code string, n int, endDate string) float64 {
	var minLowestPrice float64
	var sql string = "select min(t1.lowest_price) from (SELECT * FROM stock_week t " +
		"where t.end_date<=to_date(?, 'yyyy-mm-dd') " +
		"and t.code_=? order by t.end_date desc) t1 where rownum<=?"
	_stockWeekDaoImpl.db.Raw(sql, endDate, code, n).Scan(&minLowestPrice)
	return minLowestPrice
}
