package impl

import (
	"adam2/internal/dto"
	"adam2/internal/model"
	"anubis-framework/pkg/util"
	"time"
)

type TrainingInfoDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetTrainingInfoDaoImpl() *TrainingInfoDaoImpl {
	return &TrainingInfoDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_trainingInfoDaoImpl *TrainingInfoDaoImpl) FindTableName() string {
	return "TRNG_INFO"
}

// 添加
func (_trainingInfoDaoImpl *TrainingInfoDaoImpl) Add(trainingInfo *model.TrainingInfo) error {
	var result = _trainingInfoDaoImpl.db.Create(trainingInfo)
	return result.Error
}

// 根据name查询
func (_trainingInfoDaoImpl *TrainingInfoDaoImpl) FindByName(name string) model.TrainingInfo {
	var trainingInfo model.TrainingInfo
	_trainingInfoDaoImpl.db.Where("name = ?", name).First(&trainingInfo)
	return trainingInfo
}

// 分页显示
func (_trainingInfoDaoImpl *TrainingInfoDaoImpl) Page(name string, pageNo int, pageSize int) dto.TrainingInfoDtoArray {
	var trainingInfoDtoArray dto.TrainingInfoDtoArray
	var offset int = (pageNo - 1) * pageSize
	db := _trainingInfoDaoImpl.db.Model(model.TrainingInfo{}).Select("id, name, " +
		"(CASE WHEN market = 1 THEN '股票' WHEN market = 2 THEN '期货' ELSE '未知' END) AS market, " +
		"code, to_char(begin_date, 'yyyy-mm-dd') AS begin_date, to_char(end_date, 'yyyy-mm-dd') AS end_date, " +
		"(CASE WHEN status = 1 THEN '未开始' WHEN status = 2 THEN '已经开始' WHEN status = 3 THEN '已经结束' ELSE '未知' END) AS status, " +
		"to_char(create_time, 'yyyy-mm-dd HH24:MI:SS') AS create_time, to_char(update_time, 'yyyy-mm-dd HH24:MI:SS') AS update_time")
	db = db.Order("create_time desc, update_time desc").Offset(offset).Limit(pageSize)
	if name != "" {
		db = db.Where("name like ?", "%"+name+"%")
	}
	db.Find(&trainingInfoDtoArray)
	return trainingInfoDtoArray
}

// 分页显示，返回总数
func (_trainingInfoDaoImpl *TrainingInfoDaoImpl) PageTotal(name string) int {
	var trainingInfoDtoArray dto.TrainingInfoDtoArray
	db := _trainingInfoDaoImpl.db.Model(model.TrainingInfo{}).Select("id, name, " +
		"(CASE WHEN market = 1 THEN '股票' WHEN market = 2 THEN '期货' ELSE '未知' END) AS market, " +
		"code, to_char(begin_date, 'yyyy-mm-dd') AS begin_date, to_char(end_date, 'yyyy-mm-dd') AS end_date, " +
		"(CASE WHEN status = 1 THEN '未开始' WHEN status = 2 THEN '已经开始' WHEN status = 3 THEN '已经结束' ELSE '未知' END) AS status, " +
		"to_char(create_time, 'yyyy-mm-dd HH24:MI:SS') AS create_time, to_char(update_time, 'yyyy-mm-dd HH24:MI:SS') AS update_time")
	db = db.Order("create_time desc, update_time desc")
	if name != "" {
		db = db.Where("name like ?", "%"+name+"%")
	}
	db.Find(&trainingInfoDtoArray)
	return len(trainingInfoDtoArray)
}

// 根据name更新status
func (_trainingInfoDaoImpl *TrainingInfoDaoImpl) UpdateStatusByName(name string, status int) {
	_trainingInfoDaoImpl.db.Model(&model.TrainingInfo{}).Where("name = ?", name).Updates(model.TrainingInfo{Status: status, UpdateTime: time.Now()})
}

// 删除训练
func (_trainingInfoDaoImpl *TrainingInfoDaoImpl) DeleteTraining(name string) {
	_trainingInfoDaoImpl.db.Where("name = ?", name).Delete(&model.TrainingInfo{})
}

// 根据name，更新begin_date和end_date
func (_trainingInfoDaoImpl *TrainingInfoDaoImpl) UpdateBeginDateAndEndDateByName(name string, beginDate string, endDate string) {
	_beginDate := util.StringToDate(beginDate)
	_endDate := util.StringToDate(endDate)
	_trainingInfoDaoImpl.db.Model(&model.TrainingInfo{}).Where("name = ?", name).Updates(model.TrainingInfo{BeginDate: _beginDate, EndDate: _endDate, UpdateTime: time.Now()})
}
