package impl

import (
	"anubis-framework/pkg/io"
)

type ModelDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetModelDaoImpl() *ModelDaoImpl {
	return &ModelDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_modelDaoImpl *ModelDaoImpl) FindTableName() string {
	return "MODEL"
}

// 删除TEMP_开头的表
func (_modelDaoImpl *ModelDaoImpl) DropTableBeginWithTemp() {
	io.Infoln("删除TEMP_开头的四个表")

	_modelDaoImpl.db.Exec("drop table temp_robot_account")
	_modelDaoImpl.db.Exec("drop table temp_robot_stock_filter")
	_modelDaoImpl.db.Exec("drop table temp_transaction_record")
	_modelDaoImpl.db.Exec("drop table temp_robot_account_log")
}

// 将ROBOT7_开头的表中的记录导入到TEMP_开头的表中
func (_modelDaoImpl *ModelDaoImpl) ImportIntoTableBeginWithTempFromTableBeginWithRobot7() {
	io.Infoln("将ROBOT7_开头的表中的记录导入到TEMP_开头的表中")

	_modelDaoImpl.db.Exec("create table temp_robot_account as select * from robot7_account")
	_modelDaoImpl.db.Exec("create table temp_robot_stock_filter as select * from robot7_stock_filter")
	_modelDaoImpl.db.Exec("create table temp_transaction_record as select * from robot7_stock_transact_record")
	_modelDaoImpl.db.Exec("create table temp_robot_account_log as select * from robot7_account_log")
}

// 将TEMP_开头的表导入到HIS_开头的表中
func (_modelDaoImpl *ModelDaoImpl) InsertIntoTableBeginWithHisFromTableBeginWithTemp(modelId int) {
	io.Infoln("将TEMP_开头的表导入到HIS_开头的表中")

	_modelDaoImpl.db.Exec("insert into his_robot_account(robot_name, hold_stock_number, stock_assets, capital_assets, "+
		"total_assets, model_id, total_stamp_duty, total_registrate_fee_when_buy, total_commission_when_buy, "+
		"total_registrate_fee_when_sell, total_commission_when_sell) "+
		"select t.robot_name, t.hold_stock_number, t.stock_assets, t.capital_assets, t.total_assets, ?, "+
		"t.total_stamp_duty, t.total_registrate_fee_when_buy, t.total_commission_when_buy, t.total_registrate_fee_when_sell, "+
		"t.total_commission_when_sell "+
		"from temp_robot_account t", modelId)
	_modelDaoImpl.db.Exec("insert into his_robot_account_log(date_, robot_name, hold_stock_number, stock_assets, "+
		"capital_assets, total_assets, model_id, total_stamp_duty, total_registrate_fee_when_buy, "+
		"total_commission_when_buy, total_registrate_fee_when_sell, total_commission_when_sell) "+
		"select t.date_, t.robot_name, t.hold_stock_number, t.stock_assets, t.capital_assets, t.total_assets, ?, "+
		"t.total_stamp_duty, t.total_registrate_fee_when_buy, t.total_commission_when_buy, "+
		"t.total_registrate_fee_when_sell, t.total_commission_when_sell "+
		"from temp_robot_account_log t", modelId)
	_modelDaoImpl.db.Exec("insert into his_robot_stock_filter(stock_code, model_id, filter_type, direction, accumulative_profit_loss, "+
		"variance_type, variance, correlation250_with_avg_c_p) "+
		"select t.stock_code, ?, t.filter_type, t.direction, t.accumulative_profit_loss, t.variance_type, t.variance, t.correlation250_with_avg_c_p "+
		"from temp_robot_stock_filter t", modelId)
	_modelDaoImpl.db.Exec("insert into his_robot_transaction_record(robot_name, stock_code, buy_date, buy_price, "+
		"buy_amount, sell_date, sell_price, sell_amount, filter_type, direction, profit_and_loss, profit_and_loss_rate, model_id, "+
		"stamp_duty, registration_fee_when_buy, commission_when_buy, registration_fee_when_sell, commission_when_sell) "+
		"select t.robot_name, t.stock_code, t.buy_date, t.buy_price, t.buy_amount, t.sell_date, t.sell_price, "+
		"t.sell_amount, t.filter_type, t.direction, t.profit_and_loss, t.profit_and_loss_rate, ?, t.stamp_duty, "+
		"t.registration_fee_when_buy, t.commission_when_buy, t.registration_fee_when_sell, t.commission_when_sell "+
		"from temp_transaction_record t", modelId)
}

// 删除或重置ROBOT7_开头的表中的记录
func (_modelDaoImpl *ModelDaoImpl) TruncateOrResetTableBeginRobot7(initAssets int) {
	io.Infoln("将TEMP_开头的表导入到HIS_开头的表中")

	_modelDaoImpl.db.Exec("truncate table ROBOT7_STOCK_TRANSACT_RECORD")
	_modelDaoImpl.db.Exec("truncate table robot7_account_log")
	_modelDaoImpl.db.Exec("truncate table robot7_stock_filter")
	_modelDaoImpl.db.Exec("update robot7_account "+
		"set HOLD_STOCK_NUMBER=0, STOCK_ASSETS=0, CAPITAL_ASSETS=?, TOTAL_ASSETS=?, "+
		"TOTAL_STAMP_DUTY=0, TOTAL_REGISTRATE_FEE_WHEN_BUY=0, TOTAL_COMMISSION_WHEN_BUY=0,"+
		"TOTAL_REGISTRATE_FEE_WHEN_SELL=0, TOTAL_COMMISSION_WHEN_SELL=0",
		initAssets, initAssets)
}
