package impl

import (
	"adam2/internal/domain"
	"gorm.io/gorm"
)

// dao实现类
type BaseDaoImpl struct {
	db *gorm.DB
}

// 获取BaseDaoImpl的实例
func GetBaseDaoImpl() *BaseDaoImpl {
	return &BaseDaoImpl{domain.DataSource_.GormDb}
}
