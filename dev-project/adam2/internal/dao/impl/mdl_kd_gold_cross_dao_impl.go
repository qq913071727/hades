package impl

type MdlKdGoldCrossDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetMdlKdGoldCrossDaoImpl() *MdlKdGoldCrossDaoImpl {
	return &MdlKdGoldCrossDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_mdlKdGoldCrossDaoImpl *MdlKdGoldCrossDaoImpl) FindTableName() string {
	return "MDL_KD_GOLD_CROSS"
}
