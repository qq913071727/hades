package impl

type MdlMacdGoldCrossDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetMdlMacdGoldCrossDaoImpl() *MdlMacdGoldCrossDaoImpl {
	return &MdlMacdGoldCrossDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_mdlMacdGoldCrossDaoImpl *MdlMacdGoldCrossDaoImpl) FindTableName() string {
	return "MDL_MACD_GOLD_CROSS"
}
