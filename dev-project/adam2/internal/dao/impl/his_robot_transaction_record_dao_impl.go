package impl

import (
	"adam2/internal/domain"
)

type HisRobotTransactionRecordDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetHisRobotTransactionRecordDaoImpl() *HisRobotTransactionRecordDaoImpl {
	return &HisRobotTransactionRecordDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_hisRobotTransactionRecordDaoImpl *HisRobotTransactionRecordDaoImpl) FindTableName() string {
	return "HIS_TRANSACTION_RECORD_LOG"
}

// 根据modeId，查找收益率和相关系数
func (_hisRobotTransactionRecordDaoImpl *HisRobotTransactionRecordDaoImpl) FindProfitAndLossRateAndCorrelation250WithAvgClosePriceByModelId(modelId int) domain.ProfitAndLossRateAndCorrelation250WithAvgClosePriceArray {
	var profitAndLossRateAndCorrelation250WithAvgClosePriceArray domain.ProfitAndLossRateAndCorrelation250WithAvgClosePriceArray
	_hisRobotTransactionRecordDaoImpl.db.Raw("select hrtr.stock_code, hrtr.buy_date, hrtr.profit_and_loss_rate, "+
		"etd.correlation250_with_avg_c_p "+
		"from his_robot_transaction_record hrtr "+
		"join etf_transaction_data etd on etd.code_=hrtr.stock_code and etd.date_=hrtr.buy_date "+
		"where hrtr.model_id=?", modelId).Scan(&profitAndLossRateAndCorrelation250WithAvgClosePriceArray)
	return profitAndLossRateAndCorrelation250WithAvgClosePriceArray
}
