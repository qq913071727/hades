package impl

import (
	"adam2/internal/model"
	"anubis-framework/pkg/io"
	"anubis-framework/pkg/util"
)

type Robot7AccountDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetRobot7AccountDaoImpl() *Robot7AccountDaoImpl {
	return &Robot7AccountDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_robot7AccountDaoImpl *Robot7AccountDaoImpl) FindTableName() string {
	return "ROBOT7_ACCOUNT"
}

// 更新robot7_account表
func (_robot7AccountDaoImpl *Robot7AccountDaoImpl) UpdateRobotAccountBeforeSellOrBuy_stock(date string) {
	io.Infoln("更新robot7_account表")

	// 返回机器人账户
	var robot7AccountArray model.Robot7AccountArray
	_robot7AccountDaoImpl.db.Table("robot7_account").Select("*").Order("id_ asc").Find(&robot7AccountArray)

	for _, robot7Account := range robot7AccountArray {
		// 股票资产
		var stockAssets int = 0

		// 计算每一个机器人账户，在某一天卖完股票后的收益
		var robot7StockTransactRecordArray model.Robot7StockTransactRecordArray
		_robot7AccountDaoImpl.db.Raw("select * from robot7_stock_transact_record t "+
			"where t.robot_name = ? and t.sell_date is null and t.sell_price is null "+
			"and t.sell_amount is null and t.direction = 1",
			(*robot7Account).RobotName).Scan(&robot7StockTransactRecordArray)

		for _, robot7StockTransactRecord := range robot7StockTransactRecordArray {
			// 收盘价
			var currentClosePrice float64 = 0
			// 计算每一个机器人账户，在某一天卖完股票后的收益
			_robot7AccountDaoImpl.db.Raw("select t.close_price from stock_transaction_data_all t "+
				"where t.code_ = ? and t.date_ = to_date(?, 'yyyy-mm-dd')",
				(*robot7StockTransactRecord).StockCode, date).Scan(&currentClosePrice)

			if currentClosePrice == 0 {
				// 说明股票在这一天没有交易记录
				io.Infoln("股票[%s]在日期[%s]没有交易记录，开始查找前一个交易日的记录", robot7StockTransactRecord.StockCode, date)

				// 如果在某一天没有收盘价，比如停牌，则查找最近一个交易日的收盘价
				_robot7AccountDaoImpl.db.Raw("select std.close_price from "+
					"(select * from stock_transaction_data_all t "+
					"where t.code_ = ? and t.date_ < to_date(?, 'yyyy-mm-dd') "+
					"order by t.date_ desc) std where rownum <= 1",
					(*robot7StockTransactRecord).StockCode, date).Scan(&currentClosePrice)
			}

			// 计算这只股票的股票资产
			stockAssets = stockAssets + int(currentClosePrice*float64(robot7StockTransactRecord.BuyAmount))
		}

		// 更新robot7_account表的stock_assets、total_assets字段
		if stockAssets != 0 {
			_robot7AccountDaoImpl.db.Exec("update robot7_account t "+
				"set t.stock_assets = ?, t.total_assets = t.capital_assets + ? where t.robot_name = ?",
				stockAssets, stockAssets, (*robot7Account).RobotName)
		}
	}
}

// 更新robot7_account表
func (_robot7AccountDaoImpl *Robot7AccountDaoImpl) UpdateRobotAccountBeforeSellOrBuy_all(date string) {
	io.Infoln("更新robot7_account表")

	// 返回机器人账户
	var robot7AccountArray model.Robot7AccountArray
	_robot7AccountDaoImpl.db.Table("robot7_account").Select("*").Order("id_ asc").Find(&robot7AccountArray)

	for _, robot7Account := range robot7AccountArray {
		// 股票资产
		var stockAssets int = 0

		// 计算每一个机器人账户，在某一天卖完股票/ETF后的收益
		var robot7StockTransactRecordArray model.Robot7StockTransactRecordArray
		_robot7AccountDaoImpl.db.Raw("select * from robot7_stock_transact_record t "+
			"where t.robot_name = ? and t.sell_date is null and t.sell_price is null "+
			"and t.sell_amount is null and t.direction = 1",
			(*robot7Account).RobotName).Scan(&robot7StockTransactRecordArray)

		for _, robot7StockTransactRecord := range robot7StockTransactRecordArray {
			// 收盘价
			var currentClosePrice float64 = 0
			// 股票
			if robot7StockTransactRecord.TransactionType == 1 {
				_robot7AccountDaoImpl.db.Raw("select t.close_price from stock_transaction_data_all t "+
					"where t.code_ = ? and t.date_ = to_date(?, 'yyyy-mm-dd')",
					(*robot7StockTransactRecord).StockCode, date).Scan(&currentClosePrice)
			}
			// ETF
			if robot7StockTransactRecord.TransactionType == 2 {
				_robot7AccountDaoImpl.db.Raw("select t.close_price from etf_transaction_data t "+
					"where t.code_ = ? and t.date_ = to_date(?, 'yyyy-mm-dd')",
					(*robot7StockTransactRecord).StockCode, date).Scan(&currentClosePrice)
			}

			if currentClosePrice == 0 {
				// 说明股票/ETF在这一天没有交易记录
				io.Infoln("ETF[%s]在日期[%s]没有交易记录，开始查找前一个交易日的记录", robot7StockTransactRecord.StockCode, date)

				// 如果在某一天没有收盘价，比如停牌，则查找最近一个交易日的收盘价
				// 股票
				if robot7StockTransactRecord.TransactionType == 1 {
					_robot7AccountDaoImpl.db.Raw("select std.close_price from "+
						"(select * from stock_transaction_data_all t "+
						"where t.code_ = ? and t.date_ < to_date(?, 'yyyy-mm-dd') "+
						"order by t.date_ desc) std where rownum <= 1",
						(*robot7StockTransactRecord).StockCode, date).Scan(&currentClosePrice)
				}
				// ETF
				if robot7StockTransactRecord.TransactionType == 2 {
					_robot7AccountDaoImpl.db.Raw("select std.close_price from "+
						"(select * from etf_transaction_data t "+
						"where t.code_ = ? and t.date_ < to_date(?, 'yyyy-mm-dd') "+
						"order by t.date_ desc) std where rownum <= 1",
						(*robot7StockTransactRecord).StockCode, date).Scan(&currentClosePrice)
				}
			}

			// 计算这只股票/ETF的股票/ETF资产
			stockAssets = stockAssets + int(currentClosePrice*float64(robot7StockTransactRecord.BuyAmount))
		}

		// 更新robot7_account表的stock_assets、total_assets字段
		if stockAssets != 0 {
			_robot7AccountDaoImpl.db.Exec("update robot7_account t "+
				"set t.stock_assets = ?, t.total_assets = t.capital_assets + ? where t.robot_name = ?",
				stockAssets, stockAssets, (*robot7Account).RobotName)
		}
	}
}

// 根据当日卖出股票的收盘价，在卖股票之后，更新robot7_account表
func (_robot7AccountDaoImpl *Robot7AccountDaoImpl) UpdateRobotAccountAfterSellOrBuy_stock(date string) {
	io.Infoln("根据当日卖出股票的收盘价，在卖股票之后，更新robot7_account表")

	// 返回机器人账户
	var robot7AccountArray model.Robot7AccountArray
	_robot7AccountDaoImpl.db.Table("robot7_account").Select("*").Order("id_ asc").Find(&robot7AccountArray)

	for _, robot7Account := range robot7AccountArray {
		// 股票资产
		var stockAssets int = 0
		// 持股数量
		var sellOrBuyStockNumber int = 0

		// 计算每一个机器人账户，在某一天卖完股票后的收益
		var robot7StockTransactRecordArray model.Robot7StockTransactRecordArray
		_robot7AccountDaoImpl.db.Raw("select * from robot7_stock_transact_record retr "+
			"where retr.robot_name = ? and retr.direction = 1 "+
			"and retr.sell_date is null and retr.sell_price is null and retr.sell_amount is null",
			(*robot7Account).RobotName).Scan(&robot7StockTransactRecordArray)

		for _, robot7StockTransactRecord := range robot7StockTransactRecordArray {
			// 查找某只股票在某一天的收盘价
			var currentClosePrice float64 = 0
			_robot7AccountDaoImpl.db.Raw("select t.close_price from stock_transaction_data_all t "+
				"where t.code_ = ? and t.date_ = to_date(?, 'yyyy-mm-dd')",
				(*robot7StockTransactRecord).StockCode, date).Scan(&currentClosePrice)

			if currentClosePrice == 0 {
				// 如果在某一天没有收盘价，比如停牌，则查找最近一个交易日的收盘价
				_robot7AccountDaoImpl.db.Raw("select std.close_price from ("+
					"select * from stock_transaction_data_all t where t.code_ = ? "+
					"and t.date_ < to_date(?, 'yyyy-mm-dd') "+
					"order by t.date_ desc) std where rownum <= 1",
					(*robot7StockTransactRecord).StockCode, date).Scan(&currentClosePrice)
			}

			// 卖完股票后的股票资产、持股数量
			stockAssets = stockAssets + int(currentClosePrice*float64(robot7StockTransactRecord.BuyAmount))
			sellOrBuyStockNumber = sellOrBuyStockNumber + 1
		}

		// 更新robot7_account表
		_robot7AccountDaoImpl.db.Exec("update robot7_account t "+
			"set t.hold_stock_number = ?, t.stock_assets = ?, t.capital_assets = t.total_assets - ?, "+
			"t.total_stamp_duty = (select COALESCE(sum(t1.stamp_duty), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?), "+
			"t.total_registrate_fee_when_sell = (select COALESCE(sum(t1.registration_fee_when_sell), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?), "+
			"t.total_commission_when_sell = (select COALESCE(sum(t1.commission_when_sell), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?) "+
			"where t.robot_name = ?", sellOrBuyStockNumber, stockAssets, stockAssets,
			(*robot7Account).RobotName, (*robot7Account).RobotName, (*robot7Account).RobotName, (*robot7Account).RobotName)
	}
}

// 根据当日卖出股票/ETF的收盘价，在卖股票/ETF之后，更新robot7_account表
func (_robot7AccountDaoImpl *Robot7AccountDaoImpl) UpdateRobotAccountAfterSellOrBuy_all(date string) {
	io.Infoln("根据当日卖出股票/ETF的收盘价，在卖股票/ETF之后，更新robot7_account表")

	// 返回机器人账户
	var robot7AccountArray model.Robot7AccountArray
	_robot7AccountDaoImpl.db.Table("robot7_account").Select("*").Order("id_ asc").Find(&robot7AccountArray)

	for _, robot7Account := range robot7AccountArray {
		// ETF资产
		var stockAssets int = 0
		// 持ETF数量
		var sellOrBuyStockNumber int = 0

		// 计算每一个机器人账户，在某一天卖完股票/ETF后的收益
		var robot7StockTransactRecordArray model.Robot7StockTransactRecordArray
		_robot7AccountDaoImpl.db.Raw("select * from robot7_stock_transact_record retr "+
			"where retr.robot_name = ? and retr.direction = 1 "+
			"and retr.sell_date is null and retr.sell_price is null and retr.sell_amount is null",
			(*robot7Account).RobotName).Scan(&robot7StockTransactRecordArray)

		for _, robot7StockTransactRecord := range robot7StockTransactRecordArray {
			// 查找某只股票/ETF在某一天的收盘价
			var currentClosePrice float64 = 0
			if robot7StockTransactRecord.TransactionType == 1 {
				_robot7AccountDaoImpl.db.Raw("select t.close_price from stock_transaction_data_all t "+
					"where t.code_ = ? and t.date_ = to_date(?, 'yyyy-mm-dd')",
					(*robot7StockTransactRecord).StockCode, date).Scan(&currentClosePrice)
			}
			if robot7StockTransactRecord.TransactionType == 2 {
				_robot7AccountDaoImpl.db.Raw("select t.close_price from etf_transaction_data t "+
					"where t.code_ = ? and t.date_ = to_date(?, 'yyyy-mm-dd')",
					(*robot7StockTransactRecord).StockCode, date).Scan(&currentClosePrice)
			}

			if currentClosePrice == 0 {
				// 如果在某一天没有收盘价，比如停牌，则查找最近一个交易日的收盘价
				if robot7StockTransactRecord.TransactionType == 1 {
					_robot7AccountDaoImpl.db.Raw("select std.close_price from ("+
						"select * from stock_transaction_data_all t where t.code_ = ? "+
						"and t.date_ < to_date(?, 'yyyy-mm-dd') "+
						"order by t.date_ desc) std where rownum <= 1",
						(*robot7StockTransactRecord).StockCode, date).Scan(&currentClosePrice)
				}
				if robot7StockTransactRecord.TransactionType == 2 {
					_robot7AccountDaoImpl.db.Raw("select std.close_price from ("+
						"select * from etf_transaction_data t where t.code_ = ? "+
						"and t.date_ < to_date(?, 'yyyy-mm-dd') "+
						"order by t.date_ desc) std where rownum <= 1",
						(*robot7StockTransactRecord).StockCode, date).Scan(&currentClosePrice)
				}
			}

			// 卖完股票/ETF后的股票/ETF资产、持股票/ETF数量
			stockAssets = stockAssets + int(currentClosePrice*float64(robot7StockTransactRecord.BuyAmount))
			sellOrBuyStockNumber = sellOrBuyStockNumber + 1
		}

		// 更新robot7_account表
		_robot7AccountDaoImpl.db.Exec("update robot7_account t "+
			"set t.hold_stock_number = ?, t.stock_assets = ?, t.capital_assets = t.total_assets - ?, "+
			"t.total_stamp_duty = (select COALESCE(sum(t1.stamp_duty), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?), "+
			"t.total_registrate_fee_when_sell = (select COALESCE(sum(t1.registration_fee_when_sell), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?), "+
			"t.total_commission_when_sell = (select COALESCE(sum(t1.commission_when_sell), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?) "+
			"where t.robot_name = ?", sellOrBuyStockNumber, stockAssets, stockAssets,
			(*robot7Account).RobotName, (*robot7Account).RobotName, (*robot7Account).RobotName, (*robot7Account).RobotName)
	}
}

// 更新robot7_account表的total_assets字段
func (_robot7AccountDaoImpl *Robot7AccountDaoImpl) UpdateRobotAccountAfterBuyOrSell_stock(date string) {
	io.Infoln("更新robot7_account表的total_assets字段")

	// 返回机器人账户
	var robot7AccountArray model.Robot7AccountArray
	_robot7AccountDaoImpl.db.Table("robot7_account").Select("*").Order("id_ asc").Find(&robot7AccountArray)

	for _, robot7Account := range robot7AccountArray {
		// 股票资产
		var stockAssets float64 = 0
		// 当天买入的股票/ETF资产
		var currentBuyOrSellStockAssets float64 = 0
		// 买入股票的数量
		var sellOrBuyStockNumber int = 0

		// 计算每一个机器人账户，在某一天买完股票后的收益
		var robot7StockTransactRecordArray model.Robot7StockTransactRecordArray
		_robot7AccountDaoImpl.db.Raw("select * from robot7_stock_transact_record rstr "+
			"where rstr.robot_name = ? and rstr.direction = 1 "+
			"and rstr.sell_date is null and rstr.sell_price is null and rstr.sell_amount is null",
			(*robot7Account).RobotName).Scan(&robot7StockTransactRecordArray)

		// 如果没有买股票，则直接将robot7_account表中的记录插入到robot7_account_log表中
		if robot7StockTransactRecordArray == nil || len(robot7StockTransactRecordArray) == 0 {
			// 向表robot7_account_log中插入数据
			//_robot7AccountDaoImpl.db.Exec("insert into robot7_account_log(DATE_, ROBOT_NAME, HOLD_STOCK_NUMBER, STOCK_ASSETS, "+
			//	"CAPITAL_ASSETS, TOTAL_ASSETS, TOTAL_STAMP_DUTY, TOTAL_REGISTRATE_FEE_WHEN_BUY, TOTAL_COMMISSION_WHEN_BUY, "+
			//	"TOTAL_REGISTRATE_FEE_WHEN_SELL, TOTAL_COMMISSION_WHEN_SELL) "+
			//	"values(to_date(?, 'yyyy-mm-dd'), ?, ?, ?, round(?, 2), round(?, 2), ?, ?, ?, ?, ?)",
			//	date, (*robot7Account).RobotName, (*robot7Account).HoldStockNumber, (*robot7Account).StockAssets,
			//	(*robot7Account).CapitalAssets, (*robot7Account).TotalAssets, (*robot7Account).TotalStampDuty,
			//	(*robot7Account).TotalRegistrateFeeWhenBuy, (*robot7Account).TotalCommissionWhenBuy,
			//	(*robot7Account).TotalRegistrateFeeWhenSell, (*robot7Account).TotalCommissionWhenSell)
			_robot7AccountDaoImpl.db.Exec("insert into robot7_account_log(DATE_, ROBOT_NAME, HOLD_STOCK_NUMBER, STOCK_ASSETS, "+
				"CAPITAL_ASSETS, TOTAL_ASSETS, TOTAL_STAMP_DUTY, TOTAL_REGISTRATE_FEE_WHEN_BUY, TOTAL_COMMISSION_WHEN_BUY, "+
				"TOTAL_REGISTRATE_FEE_WHEN_SELL, TOTAL_COMMISSION_WHEN_SELL) "+
				"select to_date(?, 'yyyy-mm-dd'), t.ROBOT_NAME, t.HOLD_STOCK_NUMBER, t.STOCK_ASSETS, t.CAPITAL_ASSETS, t.TOTAL_ASSETS, t.TOTAL_STAMP_DUTY, "+
				"t.TOTAL_REGISTRATE_FEE_WHEN_BUY, t.TOTAL_COMMISSION_WHEN_BUY, t.TOTAL_REGISTRATE_FEE_WHEN_SELL, t.TOTAL_COMMISSION_WHEN_SELL "+
				"from robot7_account t where t.robot_name = ?",
				date, (*robot7Account).RobotName)

			continue
		}

		for _, robot7StockTransactRecord := range robot7StockTransactRecordArray {
			// 查找某只股票在某一天的收盘价
			// 股票的收盘价
			var currentClosePrice float64 = -1
			_robot7AccountDaoImpl.db.Raw("select t.close_price from stock_transaction_data_all t "+
				"where t.code_ = ? and t.date_ = to_date(?, 'yyyy-mm-dd')",
				robot7StockTransactRecord.StockCode, date).Scan(&currentClosePrice)

			if currentClosePrice == -1 {
				// 说明股票在这一天没有交易记录
				io.Infoln("股票[%s]在日期[%s]没有交易记录，开始查找前一个交易日的记录", robot7StockTransactRecord.StockCode, date)

				// 如果在某一天没有收盘价，比如停牌，则查找最近一个交易日的收盘价
				_robot7AccountDaoImpl.db.Raw("select std.close_price from (select * from stock_transaction_data_all t "+
					"where t.code_ = ? and t.date_ < to_date(?, 'yyyy-mm-dd') order by t.date_ desc) std where rownum <= 1",
					robot7StockTransactRecord.StockCode, date).Scan(&currentClosePrice)
			}

			// 卖完股票后的收益
			//stockAssets = stockAssets + util.CalculateSellStockProfit(robot7StockTransactRecord.StockCode, currentClosePrice, robot7StockTransactRecord.BuyAmount)
			stockAssets = stockAssets + currentClosePrice*float64(robot7StockTransactRecord.BuyAmount)
			sellOrBuyStockNumber = sellOrBuyStockNumber + 1

			// 计算当天买入的股票/ETF资产
			if util.DateToString(robot7StockTransactRecord.BuyDate) == date {
				currentBuyOrSellStockAssets = currentBuyOrSellStockAssets + currentClosePrice*float64(robot7StockTransactRecord.BuyAmount)
			}
		}

		// 更新robot7_account表
		var registrationFeeWhenBuy float64
		_robot7AccountDaoImpl.db.Raw("select COALESCE(sum(t1.registration_fee_when_buy), 0) from robot7_stock_transact_record t1 "+
			"where t1.robot_name = ? and t1.sell_price is null and t1.sell_amount is null and t1.sell_date is null "+
			"and t1.buy_date=to_date(?,'yyyy-mm-dd')",
			(*robot7Account).RobotName, date).Scan(&registrationFeeWhenBuy)
		var commissionWhenBuy float64
		_robot7AccountDaoImpl.db.Raw("select COALESCE(sum(t1.commission_when_buy), 0) from robot7_stock_transact_record t1 "+
			"where t1.robot_name = ? and t1.sell_price is null and t1.sell_amount is null and t1.sell_date is null "+
			"and t1.buy_date=to_date(?,'yyyy-mm-dd')",
			(*robot7Account).RobotName, date).Scan(&commissionWhenBuy)
		var totalStampDuty float64
		_robot7AccountDaoImpl.db.Raw("select COALESCE(sum(t1.stamp_duty), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?",
			(*robot7Account).RobotName).Scan(&totalStampDuty)
		var totalRegistrationFeeWhenBuy float64
		_robot7AccountDaoImpl.db.Raw("select COALESCE(sum(t1.registration_fee_when_buy), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?",
			(*robot7Account).RobotName).Scan(&totalRegistrationFeeWhenBuy)
		var totalCommissionWhenBuy float64
		_robot7AccountDaoImpl.db.Raw("select COALESCE(sum(t1.commission_when_buy), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?",
			(*robot7Account).RobotName).Scan(&totalCommissionWhenBuy)
		//var sumRegistrationFeeWhenSell float64
		//_robot7AccountDaoImpl.db.Raw("select COALESCE(sum(t1.registration_fee_when_sell), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?",
		//	(*robot7Account).RobotName).Scan(&sumRegistrationFeeWhenSell)
		//var sumCommissionWhenSell float64
		//_robot7AccountDaoImpl.db.Raw("select COALESCE(sum(t1.commission_when_sell), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?",
		//	(*robot7Account).RobotName).Scan(&sumCommissionWhenSell)
		_robot7AccountDaoImpl.db.Exec("update robot7_account t "+
			"set t.hold_stock_number = ?, t.stock_assets = ?, "+
			"t.total_assets = round(t.total_assets - ? - ?, 2), "+
			//"t.capital_assets = round(t.total_assets - ? - ? - ?, 2), "+
			"t.capital_assets = round(t.total_assets - ?, 2), "+
			//"t.total_stamp_duty = ?,"+
			"t.total_registrate_fee_when_buy = ?,"+
			"t.total_commission_when_buy = ? "+
			//"t.total_registrate_fee_when_sell = ?, "+
			//"t.total_commission_when_sell = ? "+
			"where t.robot_name = ?",
			sellOrBuyStockNumber, stockAssets,
			registrationFeeWhenBuy, commissionWhenBuy,
			/*stockAssets, registrationFeeWhenBuy, commissionWhenBuy,*/
			currentBuyOrSellStockAssets,
			//sumStampDuty,
			totalRegistrationFeeWhenBuy,
			totalCommissionWhenBuy,
			//sumRegistrationFeeWhenSell,
			//sumCommissionWhenSell,
			(*robot7Account).RobotName)

		// 向表robot7_account_log中插入数据
		_robot7AccountDaoImpl.db.Exec("insert into robot7_account_log(DATE_, ROBOT_NAME, HOLD_STOCK_NUMBER, STOCK_ASSETS, "+
			"CAPITAL_ASSETS, TOTAL_ASSETS, TOTAL_STAMP_DUTY, TOTAL_REGISTRATE_FEE_WHEN_BUY, TOTAL_COMMISSION_WHEN_BUY, "+
			"TOTAL_REGISTRATE_FEE_WHEN_SELL, TOTAL_COMMISSION_WHEN_SELL) "+
			"select to_date(?, 'yyyy-mm-dd'), t.ROBOT_NAME, t.HOLD_STOCK_NUMBER, t.STOCK_ASSETS, t.CAPITAL_ASSETS, t.TOTAL_ASSETS, t.TOTAL_STAMP_DUTY, "+
			"t.TOTAL_REGISTRATE_FEE_WHEN_BUY, t.TOTAL_COMMISSION_WHEN_BUY, t.TOTAL_REGISTRATE_FEE_WHEN_SELL, t.TOTAL_COMMISSION_WHEN_SELL "+
			"from robot7_account t where t.robot_name = ?",
			date, (*robot7Account).RobotName)
		//_robot7AccountDaoImpl.db.Exec("insert into robot7_account_log(DATE_, ROBOT_NAME, HOLD_STOCK_NUMBER, STOCK_ASSETS, "+
		//	"CAPITAL_ASSETS, TOTAL_ASSETS, TOTAL_STAMP_DUTY, TOTAL_REGISTRATE_FEE_WHEN_BUY, TOTAL_COMMISSION_WHEN_BUY, "+
		//	"TOTAL_REGISTRATE_FEE_WHEN_SELL, TOTAL_COMMISSION_WHEN_SELL) "+
		//	"values(to_date(?, 'yyyy-mm-dd'), ?, ?, ?, round(?, 2), round(?, 2), ?, ?, ?, ?, ?)",
		//	date, (*robot7Account).RobotName, sellOrBuyStockNumber, stockAssets,
		//	(*robot7Account).TotalAssets-stockAssets-registrationFeeWhenBuy-commissionWhenBuy,
		//	(*robot7Account).TotalAssets-registrationFeeWhenBuy-commissionWhenBuy,
		//	//(*robot7Account).TotalStampDuty,
		//	totalStampDuty,
		//	totalRegistrationFeeWhenBuy, totalCommissionWhenBuy,
		//	(*robot7Account).TotalRegistrateFeeWhenSell, (*robot7Account).TotalCommissionWhenSell)
	}
}

// 更新robot7_account表的total_assets字段
func (_robot7AccountDaoImpl *Robot7AccountDaoImpl) UpdateRobotAccountAfterBuyOrSell_all(date string) {
	io.Infoln("更新robot7_account表的total_assets字段")

	// 返回机器人账户
	var robot7AccountArray model.Robot7AccountArray
	_robot7AccountDaoImpl.db.Table("robot7_account").Select("*").Order("id_ asc").Find(&robot7AccountArray)

	for _, robot7Account := range robot7AccountArray {
		// 股票/ETF资产
		var stockAssets float64 = 0
		// 当天买入的股票/ETF资产
		var currentBuyOrSellStockAssets float64 = 0
		// 买入股票/ETF的数量
		var sellOrBuyStockNumber int = 0

		// 计算每一个机器人账户，在某一天买完股票/ETF的收益
		var robot7StockTransactRecordArray model.Robot7StockTransactRecordArray
		_robot7AccountDaoImpl.db.Raw("select * from robot7_stock_transact_record rstr "+
			"where rstr.robot_name = ? and rstr.direction = 1 "+
			"and rstr.sell_date is null and rstr.sell_price is null and rstr.sell_amount is null",
			(*robot7Account).RobotName).Scan(&robot7StockTransactRecordArray)

		// 如果没有买股票/ETF，则直接将robot7_account表中的记录插入到robot7_account_log表中
		if robot7StockTransactRecordArray == nil || len(robot7StockTransactRecordArray) == 0 {
			// 向表robot7_account_log中插入数据
			//_robot7AccountDaoImpl.db.Exec("insert into robot7_account_log(DATE_, ROBOT_NAME, HOLD_STOCK_NUMBER, STOCK_ASSETS, "+
			//	"CAPITAL_ASSETS, TOTAL_ASSETS, TOTAL_STAMP_DUTY, TOTAL_REGISTRATE_FEE_WHEN_BUY, TOTAL_COMMISSION_WHEN_BUY, "+
			//	"TOTAL_REGISTRATE_FEE_WHEN_SELL, TOTAL_COMMISSION_WHEN_SELL) "+
			//	"values(to_date(?, 'yyyy-mm-dd'), ?, ?, ?, round(?, 2), round(?, 2), ?, ?, ?, ?, ?)",
			//	date, (*robot7Account).RobotName, (*robot7Account).HoldStockNumber, (*robot7Account).StockAssets,
			//	(*robot7Account).CapitalAssets, (*robot7Account).TotalAssets, (*robot7Account).TotalStampDuty,
			//	(*robot7Account).TotalRegistrateFeeWhenBuy, (*robot7Account).TotalCommissionWhenBuy,
			//	(*robot7Account).TotalRegistrateFeeWhenSell, (*robot7Account).TotalCommissionWhenSell)
			_robot7AccountDaoImpl.db.Exec("insert into robot7_account_log(DATE_, ROBOT_NAME, HOLD_STOCK_NUMBER, STOCK_ASSETS, "+
				"CAPITAL_ASSETS, TOTAL_ASSETS, TOTAL_STAMP_DUTY, TOTAL_REGISTRATE_FEE_WHEN_BUY, TOTAL_COMMISSION_WHEN_BUY, "+
				"TOTAL_REGISTRATE_FEE_WHEN_SELL, TOTAL_COMMISSION_WHEN_SELL) "+
				"select to_date(?, 'yyyy-mm-dd'), t.ROBOT_NAME, t.HOLD_STOCK_NUMBER, t.STOCK_ASSETS, t.CAPITAL_ASSETS, t.TOTAL_ASSETS, t.TOTAL_STAMP_DUTY, "+
				"t.TOTAL_REGISTRATE_FEE_WHEN_BUY, t.TOTAL_COMMISSION_WHEN_BUY, t.TOTAL_REGISTRATE_FEE_WHEN_SELL, t.TOTAL_COMMISSION_WHEN_SELL "+
				"from robot7_account t where t.robot_name = ?",
				date, (*robot7Account).RobotName)

			continue
		}

		for _, robot7StockTransactRecord := range robot7StockTransactRecordArray {
			// 查找某只股票/ETF在某一天的收盘价
			// 股票/ETF的收盘价
			var currentClosePrice float64 = -1
			if robot7StockTransactRecord.TransactionType == 1 {
				_robot7AccountDaoImpl.db.Raw("select t.close_price from stock_transaction_data_all t "+
					"where t.code_ = ? and t.date_ = to_date(?, 'yyyy-mm-dd')",
					robot7StockTransactRecord.StockCode, date).Scan(&currentClosePrice)
			}
			if robot7StockTransactRecord.TransactionType == 2 {
				_robot7AccountDaoImpl.db.Raw("select t.close_price from etf_transaction_data t "+
					"where t.code_ = ? and t.date_ = to_date(?, 'yyyy-mm-dd')",
					robot7StockTransactRecord.StockCode, date).Scan(&currentClosePrice)
			}

			if currentClosePrice == -1 {
				// 说明股票/ETF在这一天没有交易记录
				io.Infoln("股票/ETF[%s]在日期[%s]没有交易记录，开始查找前一个交易日的记录", robot7StockTransactRecord.StockCode, date)

				// 如果在某一天没有收盘价，比如停牌，则查找最近一个交易日的收盘价
				if robot7StockTransactRecord.TransactionType == 1 {
					_robot7AccountDaoImpl.db.Raw("select std.close_price from (select * from stock_transaction_data_all t "+
						"where t.code_ = ? and t.date_ < to_date(?, 'yyyy-mm-dd') order by t.date_ desc) std where rownum <= 1",
						robot7StockTransactRecord.StockCode, date).Scan(&currentClosePrice)
				}
				if robot7StockTransactRecord.TransactionType == 2 {
					_robot7AccountDaoImpl.db.Raw("select std.close_price from (select * from etf_transaction_data t "+
						"where t.code_ = ? and t.date_ < to_date(?, 'yyyy-mm-dd') order by t.date_ desc) std where rownum <= 1",
						robot7StockTransactRecord.StockCode, date).Scan(&currentClosePrice)
				}
			}

			// 卖完股票/ETF后的收益
			//stockAssets = stockAssets + util.CalculateSellStockProfit(robot7StockTransactRecord.StockCode, currentClosePrice, robot7StockTransactRecord.BuyAmount)
			stockAssets = stockAssets + currentClosePrice*float64(robot7StockTransactRecord.BuyAmount)
			sellOrBuyStockNumber = sellOrBuyStockNumber + 1

			// 计算当天买入的股票/ETF资产
			if util.DateToString(robot7StockTransactRecord.BuyDate) == date {
				currentBuyOrSellStockAssets = currentBuyOrSellStockAssets + currentClosePrice*float64(robot7StockTransactRecord.BuyAmount)
			}
		}

		// 更新robot7_account表
		var registrationFeeWhenBuy float64
		_robot7AccountDaoImpl.db.Raw("select COALESCE(sum(t1.registration_fee_when_buy), 0) from robot7_stock_transact_record t1 "+
			"where t1.robot_name = ? and t1.sell_price is null and t1.sell_amount is null and t1.sell_date is null "+
			"and t1.buy_date=to_date(?,'yyyy-mm-dd')",
			(*robot7Account).RobotName, date).Scan(&registrationFeeWhenBuy)
		var commissionWhenBuy float64
		_robot7AccountDaoImpl.db.Raw("select COALESCE(sum(t1.commission_when_buy), 0) from robot7_stock_transact_record t1 "+
			"where t1.robot_name = ? and t1.sell_price is null and t1.sell_amount is null and t1.sell_date is null "+
			"and t1.buy_date=to_date(?,'yyyy-mm-dd')",
			(*robot7Account).RobotName, date).Scan(&commissionWhenBuy)
		var totalStampDuty float64
		_robot7AccountDaoImpl.db.Raw("select COALESCE(sum(t1.stamp_duty), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?",
			(*robot7Account).RobotName).Scan(&totalStampDuty)
		var totalRegistrationFeeWhenBuy float64
		_robot7AccountDaoImpl.db.Raw("select COALESCE(sum(t1.registration_fee_when_buy), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?",
			(*robot7Account).RobotName).Scan(&totalRegistrationFeeWhenBuy)
		var totalCommissionWhenBuy float64
		_robot7AccountDaoImpl.db.Raw("select COALESCE(sum(t1.commission_when_buy), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?",
			(*robot7Account).RobotName).Scan(&totalCommissionWhenBuy)
		//var sumRegistrationFeeWhenSell float64
		//_robot7AccountDaoImpl.db.Raw("select COALESCE(sum(t1.registration_fee_when_sell), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?",
		//	(*robot7Account).RobotName).Scan(&sumRegistrationFeeWhenSell)
		//var sumCommissionWhenSell float64
		//_robot7AccountDaoImpl.db.Raw("select COALESCE(sum(t1.commission_when_sell), 0) from robot7_stock_transact_record t1 where t1.robot_name = ?",
		//	(*robot7Account).RobotName).Scan(&sumCommissionWhenSell)
		_robot7AccountDaoImpl.db.Exec("update robot7_account t "+
			"set t.hold_stock_number = ?, t.stock_assets = ?, "+
			"t.total_assets = round(t.total_assets - ? - ?, 2), "+
			// 下面这行可能有错，正确的可能是：资金资产=总资产-股票资产，否则资金资产部分可能就会出现负数。
			//"t.capital_assets = round(t.total_assets - ? - ? - ?, 2), "+
			"t.capital_assets = round(t.capital_assets - ? - ? - ?, 2), "+
			//"t.total_stamp_duty = ?,"+
			"t.total_registrate_fee_when_buy = ?,"+
			"t.total_commission_when_buy = ? "+
			//"t.total_registrate_fee_when_sell = ?, "+
			//"t.total_commission_when_sell = ? "+
			"where t.robot_name = ?",
			sellOrBuyStockNumber, stockAssets,
			registrationFeeWhenBuy, commissionWhenBuy,
			//stockAssets, registrationFeeWhenBuy, commissionWhenBuy,
			/*stockAssets, registrationFeeWhenBuy, commissionWhenBuy,*/
			currentBuyOrSellStockAssets, registrationFeeWhenBuy, commissionWhenBuy,
			//sumStampDuty,
			totalRegistrationFeeWhenBuy,
			totalCommissionWhenBuy,
			//sumRegistrationFeeWhenSell,
			//sumCommissionWhenSell,
			(*robot7Account).RobotName)

		// 向表robot7_account_log中插入数据
		_robot7AccountDaoImpl.db.Exec("insert into robot7_account_log(DATE_, ROBOT_NAME, HOLD_STOCK_NUMBER, STOCK_ASSETS, "+
			"CAPITAL_ASSETS, TOTAL_ASSETS, TOTAL_STAMP_DUTY, TOTAL_REGISTRATE_FEE_WHEN_BUY, TOTAL_COMMISSION_WHEN_BUY, "+
			"TOTAL_REGISTRATE_FEE_WHEN_SELL, TOTAL_COMMISSION_WHEN_SELL) "+
			"select to_date(?, 'yyyy-mm-dd'), t.ROBOT_NAME, t.HOLD_STOCK_NUMBER, t.STOCK_ASSETS, t.CAPITAL_ASSETS, t.TOTAL_ASSETS, t.TOTAL_STAMP_DUTY, "+
			"t.TOTAL_REGISTRATE_FEE_WHEN_BUY, t.TOTAL_COMMISSION_WHEN_BUY, t.TOTAL_REGISTRATE_FEE_WHEN_SELL, t.TOTAL_COMMISSION_WHEN_SELL "+
			"from robot7_account t where t.robot_name = ?",
			date, (*robot7Account).RobotName)
		//_robot7AccountDaoImpl.db.Exec("insert into robot7_account_log(DATE_, ROBOT_NAME, HOLD_STOCK_NUMBER, STOCK_ASSETS, "+
		//	"CAPITAL_ASSETS, TOTAL_ASSETS, TOTAL_STAMP_DUTY, TOTAL_REGISTRATE_FEE_WHEN_BUY, TOTAL_COMMISSION_WHEN_BUY, "+
		//	"TOTAL_REGISTRATE_FEE_WHEN_SELL, TOTAL_COMMISSION_WHEN_SELL) "+
		//	"values(to_date(?, 'yyyy-mm-dd'), ?, ?, ?, round(?, 2), round(?, 2), ?, ?, ?, ?, ?)",
		//	date, (*robot7Account).RobotName, sellOrBuyStockNumber, stockAssets,
		//	(*robot7Account).TotalAssets-stockAssets-registrationFeeWhenBuy-commissionWhenBuy,
		//	(*robot7Account).TotalAssets-registrationFeeWhenBuy-commissionWhenBuy,
		//	//(*robot7Account).TotalStampDuty,
		//	totalStampDuty,
		//	totalRegistrationFeeWhenBuy, totalCommissionWhenBuy,
		//	(*robot7Account).TotalRegistrateFeeWhenSell, (*robot7Account).TotalCommissionWhenSell)
	}
}
