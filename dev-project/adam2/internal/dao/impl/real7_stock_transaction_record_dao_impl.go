package impl

type Real7StockTransactionRecordDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetReal7StockTransactionRecordDaoImpl() *Real7StockTransactionRecordDaoImpl {
	return &Real7StockTransactionRecordDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_real7StockTransactionRecordDaoImpl *Real7StockTransactionRecordDaoImpl) FindTableName() string {
	return "REAL7_STOCK_TRANSACTION_RECORD"
}

// 返回real7_stock_transaction_record表中sell_date为空的记录数
func (_real7StockTransactionRecordDaoImpl *Real7StockTransactionRecordDaoImpl) CountBySellDateNull() int {
	var count int
	_real7StockTransactionRecordDaoImpl.db.Raw("select count（*） from real7_stock_transaction_record t where t.sell_date is null").Scan(&count)
	return count
}
