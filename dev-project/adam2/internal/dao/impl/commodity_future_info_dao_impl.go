package impl

import "adam2/internal/model"

type CommodityFutureInfoDaoImpl struct {
	*BaseDaoImpl
}

// 返回dao实现类
func GetCommodityFutureInfoDaoImpl() *CommodityFutureInfoDaoImpl {
	return &CommodityFutureInfoDaoImpl{GetBaseDaoImpl()}
}

// 返回表名
func (_commodityFutureInfoDaoImpl *CommodityFutureInfoDaoImpl) FindTableName() string {
	return "COMMODITY_FUTURE_INFO"
}

// 查询所有记录
func (_commodityFutureInfoDaoImpl *CommodityFutureInfoDaoImpl) FindAll() model.CommodityFutureInfoArray {
	var commodityFutureInfoArray model.CommodityFutureInfoArray
	_commodityFutureInfoDaoImpl.db.Raw("select * from commodity_future_info t where t.tradable=1").Find(&commodityFutureInfoArray)
	return commodityFutureInfoArray
}
