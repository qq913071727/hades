package dao

import (
	"adam2/internal/domain"
	"adam2/internal/model"
)

type HisRobotAccountLogDao interface {

	// 返回表名
	FindTableName() string

	// 根据model_id字段，返回his_robot_account_log表中date_字段，按照date_升序排列，并去重
	FindDistinctDateByModelIdOrderByDateAsc(modelId int) *[]string

	// 根据model_id字段，返回his_robot_account_log表中robot_name字段，并去重
	FindDistinctRobotNameByModelId(modelId int) *[]string

	// 根据robot_name、model_id字段返回记录，并按照date_字段升序排列
	FindByRobotNameAndModelIdOrderByDateAsc(modelId int, robotName string) model.HisRobotAccountLogArray

	// 计算最大回撤
	CalculateMaximumDrawdown(modelId int) domain.MaximumDrawdownArray
}
