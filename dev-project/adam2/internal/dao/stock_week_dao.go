package dao

import "adam2/internal/model"

type StockWeekDao interface {

	// 根据code、开始时间和结束时间查询记录
	FindByCodeBetweenDate(code string, beginDate string, endDate string) model.StockWeekArray

	// 查询之前n周内的最高价
	FindMaxHighestPriceWithNDate(code string, n int, endDate string) float64

	// 查询之前n周内的最低价
	FindMinLowestPriceWithNDate(code string, n int, endDate string) float64
}
