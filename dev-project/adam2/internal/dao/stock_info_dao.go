package dao

import "adam2/internal/model"

type StockInfoDao interface {
	// 返回表名
	FindTableName() string

	// 返回全部记录
	FindAll() (model.StockInfoArray, error)

	// 根据code查询记录
	FindByCode(code string) model.StockInfo
}
