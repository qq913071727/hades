package dao

// dao接口
type BaseDao interface {
	// 返回表名
	FindTableName() string
}
