package model

import "time"

type MdlMacdGoldCross struct {
	ID                     int       `gorm:"column:ID"`
	StockCode              string    `gorm:"column:STOCK_CODE"`
	SellDate               time.Time `gorm:"column:SELL_DATE"`
	ModelId                int       `gorm:"column:MODEL_ID"`
	BuyDate                time.Time `gorm:"column:BUY_DATE"`
	AccumulativeProfitLoss float64   `gorm:"column:ACCUMULATIVE_PROFIT_LOSS"`
	BuyDea                 float64   `gorm:"column:BUY_DEA"`
	BuyDif                 float64   `gorm:"column:BUY_DIF"`
	BuyPrice               float64   `gorm:"column:BUY_PRICE"`
	ProfitLoss             float64   `gorm:"column:PROFIT_LOSS"`
	SellDea                float64   `gorm:"column:SELL_DEA"`
	SellDif                float64   `gorm:"column:SELL_DIF"`
	SellPrice              float64   `gorm:"column:SELL_PRICE"`
}

type MdlMacdGoldCrossArray []*MdlMacdGoldCross
