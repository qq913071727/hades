package model

import "time"

type StockWeek struct {
	ID                 int       `gorm:"column:ID_"`
	BeginDate          time.Time `gorm:"column:BEGIN_DATE"`
	Code               string    `gorm:"column:CODE_"`
	UpDown             int       `gorm:"column:UP_DOWN"`
	EndDate            time.Time `gorm:"column:END_DATE"`
	Number             int       `gorm:"column:NUMBER_"`
	OpenPrice          float64   `gorm:"column:OPEN_PRICE"`
	ClosePrice         float64   `gorm:"column:CLOSE_PRICE"`
	HighestPrice       float64   `gorm:"column:HIGHEST_PRICE"`
	LowestPrice        float64   `gorm:"column:LOWEST_PRICE"`
	Volume             float64   `gorm:"column:VOLUME"`
	Turnover           float64   `gorm:"column:TURNOVER"`
	Ma5                float64   `gorm:"column:MA5"`
	Ma10               float64   `gorm:"column:MA10"`
	Ma20               float64   `gorm:"column:MA20"`
	Ma60               float64   `gorm:"column:MA60"`
	Ma120              float64   `gorm:"column:MA120"`
	Ma250              float64   `gorm:"column:MA250"`
	Dif                float64   `gorm:"column:DIF"`
	Dea                float64   `gorm:"column:DEA"`
	Macd               float64   `gorm:"column:MACD"`
	Rsv                float64   `gorm:"column:RSV"`
	K                  float64   `gorm:"column:K"`
	D                  float64   `gorm:"column:D"`
	Mb                 float64   `gorm:"column:MB"`
	Up                 float64   `gorm:"column:UP"`
	Dn                 float64   `gorm:"column:DN_"`
	Ema12              float64   `gorm:"column:EMA12"`
	Ema26              float64   `gorm:"column:EMA26"`
	ChangeRangeExRight float64   `gorm:"column:CHANGE_RANGE_EX_RIGHT"`
	HaWeekOpenPrice    float64   `gorm:"column:HA_WEEK_OPEN_PRICE"`
	HaWeekClosePrice   float64   `gorm:"column:HA_WEEK_CLOSE_PRICE"`
	HaWeekHighestPrice float64   `gorm:"column:HA_WEEK_HIGHEST_PRICE"`
	HaWeekLowestPrice  float64   `gorm:"column:HA_WEEK_LOWEST_PRICE"`
}

type StockWeekArray []*StockWeek
