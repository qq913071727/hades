package model

import "time"

type MdlClosePriceMa5GoldCross struct {
	ID                     int       `gorm:"column:ID"`
	StockCode              string    `gorm:"column:STOCK_CODE"`
	BuyDate                time.Time `gorm:"column:BUY_DATE"`
	BuyPrice               float64   `gorm:"column:BUY_PRICE"`
	BuyMa5                 float64   `gorm:"column:BUY_MA5"`
	AccumulativeProfitLoss float64   `gorm:"column:ACCUMULATIVE_PROFIT_LOSS"`
	ProfitLoss             float64   `gorm:"column:PROFIT_LOSS"`
	SellDate               time.Time `gorm:"column:SELL_DATE"`
	SellPrice              float64   `gorm:"column:SELL_PRICE"`
	SellMa5                float64   `gorm:"column:SELL_MA5"`
}

type MdlClosePriceMa5GoldCrossArray []*MdlClosePriceMa5GoldCross
