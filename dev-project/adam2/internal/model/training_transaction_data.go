package model

import "time"

type TrainingTransactionData struct {
	ID                int       `gorm:"column:ID"`
	TrainingInfoId    int       `gorm:"column:TRNG_INFO_ID"`
	Market            int       `gorm:"column:MARKET"`
	Code              string    `gorm:"column:CODE"`
	BuyDate           time.Time `gorm:"column:BUY_DATE"`
	BuyPrice          float64   `gorm:"column:BUY_PRICE"`
	BuyAmount         float64   `gorm:"column:BUY_AMOUNT"`
	SellDate          time.Time `gorm:"column:SELL_DATE"`
	SellPrice         float64   `gorm:"column:SELL_PRICE"`
	SellAmount        float64   `gorm:"column:SELL_AMOUNT"`
	Direction         int       `gorm:"column:DIRECTION"`
	ProfitAndLoss     float64   `gorm:"column:PROFIT_AND_LOSS"`
	ProfitAndLossRate float64   `gorm:"column:PROFIT_AND_LOSS_RATE"`
	CreateTime        time.Time `gorm:"column:CREATE_TIME"`
	UpdateTime        time.Time `gorm:"column:UPDATE_TIME"`
}

type TrainingTransactionDataArray []*TrainingTransactionData

// 定义表名
func (_trainingTransactionData TrainingTransactionData) TableName() string {
	return "TRNG_TRANSACTION_DATA"
}
