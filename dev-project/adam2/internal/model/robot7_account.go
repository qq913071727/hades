package model

type Robot7Account struct {
	ID                         int     `gorm:"column:ID_"`
	RobotName                  string  `gorm:"column:ROBOT_NAME"`
	HoldStockNumber            int     `gorm:"column:HOLD_STOCK_NUMBER"`
	StockAssets                float64 `gorm:"column:STOCK_ASSETS"`
	CapitalAssets              float64 `gorm:"column:CAPITAL_ASSETS"`
	TotalAssets                float64 `gorm:"column:TOTAL_ASSETS"`
	TotalStampDuty             float64 `gorm:"column:STAMP_DUTY"`
	TotalRegistrateFeeWhenBuy  float64 `gorm:"column:TOTAL_REGISTRATE_FEE_WHEN_BUY"`
	TotalCommissionWhenBuy     float64 `gorm:"column:TOTAL_COMMISSION_WHEN_BUY"`
	TotalRegistrateFeeWhenSell float64 `gorm:"column:TOTAL_REGISTRATE_FEE_WHEN_SELL"`
	TotalCommissionWhenSell    float64 `gorm:"column:TOTAL_COMMISSION_WHEN_SELL"`
}

type Robot7AccountArray []*Robot7Account
