package model

import (
	"time"
)

type CommodityFutureWeekData struct {
	ID                     int       `gorm:"column:ID"`
	Code                   string    `gorm:"column:CODE"`
	BeginDate              time.Time `gorm:"column:BEGIN_DATE"`
	EndDate                time.Time `gorm:"column:END_DATE"`
	Name                   string    `gorm:"column:NAME"`
	OpenPrice              float64   `gorm:"column:OPEN_PRICE"`
	ClosePrice             float64   `gorm:"column:CLOSE_PRICE"`
	HighestPrice           float64   `gorm:"column:HIGHEST_PRICE"`
	LowestPrice            float64   `gorm:"column:LOWEST_PRICE"`
	LastClosePrice         float64   `gorm:"column:LAST_CLOSE_PRICE"`
	Volume                 float64   `gorm:"column:VOLUME"`
	Turnover               float64   `gorm:"column:TURNOVER"`
	OpenInterest           int       `gorm:"column:OPEN_INTEREST"`
	Buying                 float64   `gorm:"column:BUYING"`
	Selling                float64   `gorm:"column:SELLING"`
	RisingAndFallingAmount int       `gorm:"column:RISING_AND_FALLING_AMOUNT"`
	PriceChange            float64   `gorm:"column:PRICE_CHANGE"`
	Ma5                    float64   `gorm:"column:MA5"`
	Ma10                   float64   `gorm:"column:MA10"`
	Ma20                   float64   `gorm:"column:MA20"`
	Ma60                   float64   `gorm:"column:MA60"`
	Ma120                  float64   `gorm:"column:MA120"`
	Ma250                  float64   `gorm:"column:MA250"`
	Ema12                  float64   `gorm:"column:EMA12"`
	Ema26                  float64   `gorm:"column:EMA26"`
	Dif                    float64   `gorm:"column:DIF"`
	Dea                    float64   `gorm:"column:DEA"`
	Macd                   float64   `gorm:"column:MACD"`
	Rsv                    float64   `gorm:"column:RSV"`
	K                      float64   `gorm:"column:K"`
	D                      float64   `gorm:"column:D"`
	Dn                     float64   `gorm:"column:DN"`
	Mb                     float64   `gorm:"column:MB"`
	Up                     float64   `gorm:"column:UP"`
	HaWeekOpenPrice        float64   `gorm:"column:HA_WEEK_OPEN_PRICE"`
	HaWeekClosePrice       float64   `gorm:"column:HA_WEEK_CLOSE_PRICE"`
	HaWeekHighestPrice     float64   `gorm:"column:HA_WEEK_HIGHEST_PRICE"`
	HaWeekLowestPrice      float64   `gorm:"column:HA_WEEK_LOWEST_PRICE"`
	CreateTime             time.Time `gorm:"column:CREATE_TIME"`
}

type CommodityFutureWeekDataArray []*CommodityFutureWeekData
