package model

import "time"

type CommodityFutureDateData struct {
	ID                      int       `gorm:"column:ID"`
	Code                    string    `gorm:"column:CODE"`
	TransactionDate         time.Time `gorm:"column:TRANSACTION_DATE"`
	Name                    string    `gorm:"column:NAME"`
	HighestPrice            float64   `gorm:"column:HIGHEST_PRICE"`
	LowestPrice             float64   `gorm:"column:LOWEST_PRICE"`
	OpenPrice               float64   `gorm:"column:OPEN_PRICE"`
	ClosePrice              float64   `gorm:"column:CLOSE_PRICE"`
	LastClosePrice          float64   `gorm:"column:LAST_CLOSE_PRICE"`
	volume                  int       `gorm:"column:VOLUME"`
	turnover                int       `gorm:"column:TURNOVER"`
	OpenInterest            int       `gorm:"column:OPEN_INTEREST"`
	Buying                  int       `gorm:"column:BUYING"`
	Selling                 int       `gorm:"column:SELLING"`
	RisingAndFallingAmount  int       `gorm:"column:RISING_AND_FALLING_AMOUNT"`
	PriceChange             float64   `gorm:"column:PRICE_CHANGE"`
	Ma5                     float64   `gorm:"column:MA5"`
	Ma10                    float64   `gorm:"column:MA10"`
	Ma20                    float64   `gorm:"column:MA20"`
	Ma60                    float64   `gorm:"column:MA60"`
	Ma120                   float64   `gorm:"column:MA120"`
	Ma250                   float64   `gorm:"column:MA250"`
	Ema12                   float64   `gorm:"column:EMA12"`
	Ema26                   float64   `gorm:"column:EMA26"`
	Dif                     float64   `gorm:"column:DIF"`
	Dea                     float64   `gorm:"column:DEA"`
	Macd                    float64   `gorm:"column:MACD"`
	Rsv                     float64   `gorm:"column:RSV"`
	K                       float64   `gorm:"column:K"`
	D                       float64   `gorm:"column:D"`
	Up                      float64   `gorm:"column:UP"`
	Mb                      float64   `gorm:"column:MB"`
	Dn                      float64   `gorm:"column:DN"`
	HaOpenPrice             float64   `gorm:"column:HA_OPEN_PRICE"`
	HaClosePrice            float64   `gorm:"column:HA_CLOSE_PRICE"`
	HaHighestPrice          float64   `gorm:"column:HA_HIGHEST_PRICE"`
	HaLowestPrice           float64   `gorm:"column:HA_LOWEST_PRICE"`
	Bias5                   float64   `gorm:"column:BIAS5"`
	Bias10                  float64   `gorm:"column:BIAS10"`
	Bias20                  float64   `gorm:"column:BIAS20"`
	Bias60                  float64   `gorm:"column:BIAS60"`
	Bias120                 float64   `gorm:"column:BIAS120"`
	Bias250                 float64   `gorm:"column:BIAS250"`
	variance5               float64   `gorm:"column:VARIANCE5"`
	variance10              float64   `gorm:"column:VARIANCE10"`
	variance20              float64   `gorm:"column:VARIANCE20"`
	variance60              float64   `gorm:"column:VARIANCE60"`
	variance120             float64   `gorm:"column:VARIANCE120"`
	variance250             float64   `gorm:"column:VARIANCE250"`
	UniteRelativePriceIndex float64   `gorm:"column:UNITE_RELATIVE_PRICE_INDEX"`
	CreateTime              time.Time `gorm:"column:CREATE_TIME"`
}

type CommodityFutureDateDataArray []*CommodityFutureDateData
