package model

import "time"

type Model struct {
	ID                         int       `gorm:"column:ID"`
	Name                       string    `gorm:"column:NAME"`
	CreateTime                 time.Time `gorm:"column:CREATE_TIME"`
	BeginTime                  time.Time `gorm:"column:BEGIN_TIME"`
	EndTime                    time.Time `gorm:"column:END_TIME"`
	AlgorithmDescription       string    `gorm:"column:ALGORITHM_DESCRIPTION"`
	SPositionControl           int       `gorm:"column:S_POSITION_CONTROL"`
	AverageAnnualReturnRate    float64   `gorm:"column:AVERAGE_ANNUAL_RETURN_RATE"`
	BuyingLong                 int       `gorm:"column:BUYING_LONG"`
	ShortSelling               int       `gorm:"column:SHORT_SELLING"`
	SuccessRateCalculateTime   int       `gorm:"column:SUCCESS_RATE_CALCULATE_TIME"`
	SuccessRateThreshold       float64   `gorm:"column:SUCCESS_RATE_THRESHOLD"`
	PSingleOrAllMethod         int       `gorm:"column:P_SINGLE_OR_ALL_METHOD"`
	PJudgeMethod               int       `gorm:"column:P_JUDGE_METHOD"`
	PShippingSpaceControl      int       `gorm:"column:P_SHIPPING_SPACE_CONTROL"`
	PPercentageTopThreshold    float64   `gorm:"column:P_PERCENTAGE_TOP_THRESHOLD"`
	PPercentageBottomThreshold float64   `gorm:"column:P_PERCENTAGE_BOTTOM_THRESHOLD"`
	PUseMinFilterCondition     int       `gorm:"column:P_USE_MIN_FILTER_CONDITION"`
	TYPE                       int       `gorm:"column:TYPE_"`
	MandatoryStopLoss          int       `gorm:"column:MANDATORY_STOP_LOSS"`
	MandatoryStopLossRate      int       `gorm:"column:MANDATORY_STOP_LOSS_RATE"`
	VolatileRate               int       `gorm:"column:VOLATILE_RATE"`
	VolatileMonthNumber        int       `gorm:"column:VOLATILE_MONTH_NUMBER"`
	FollowOrReverseTrend       int       `gorm:"column:FOLLOW_OR_REVERSE_TREND"`
	HoldStockNumberPerAccount  int       `gorm:"column:HOLD_STOCK_NUMBER_PER_ACCOUNT"`
	AccountNumber              int       `gorm:"column:ACCOUNT_NUMBER"`
	Ma120NotIncreasing         int       `gorm:"column:MA120_NOT_INCREASING"`
	Ma120NotDecreasing         int       `gorm:"column:MA120_NOT_DECREASING"`
	Ma250NotIncreasing         int       `gorm:"column:MA250_NOT_INCREASING"`
	Ma250NotDecreasing         int       `gorm:"column:MA250_NOT_DECREASING"`
	CPPercentageFromUpDn       float64   `gorm:"column:C_P_PERCENTAGE_FROM_UP_DN"`
	MacdTransactionNumber      int       `gorm:"column:MACD_TRANSACTION_NUMBER"`
	CPMa5TransactionNumber     int       `gorm:"column:C_P_MA5_TRANSACTION_NUMBER"`
	HKATransactionNumber       int       `gorm:"column:K_H_A_TRANSACTION_NUMBER"`
	KdTransactionNumber        int       `gorm:"column:KD_TRANSACTION_NUMBER"`
	MandatoryStopProfit        int       `gorm:"column:MANDATORY_STOP_PROFIT"`
	MandatoryStopProfitRate    float64   `gorm:"column:MANDATORY_STOP_PROFIT_RATE"`
}

type ModelArray []*Model
