package model

import "time"

type TrainingInfo struct {
	ID         int       `gorm:"column:ID"`
	Name       string    `gorm:"column:NAME"`
	Market     int       `gorm:"column:MARKET"`
	Code       string    `gorm:"column:CODE"`
	BeginDate  time.Time `gorm:"column:BEGIN_DATE"`
	EndDate    time.Time `gorm:"column:END_DATE"`
	Status     int       `gorm:"column:STATUS"`
	CreateTime time.Time `gorm:"column:CREATE_TIME"`
	UpdateTime time.Time `gorm:"column:UPDATE_TIME"`
}

type TrainingInfoArray []*TrainingInfo

// 定义表名
func (_trainingInfo TrainingInfo) TableName() string {
	return "TRNG_INFO"
}
