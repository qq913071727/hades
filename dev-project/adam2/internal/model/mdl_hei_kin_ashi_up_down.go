package model

import "time"

type MdlHeiKinAshiUpDown struct {
	ID                     int       `gorm:"column:ID"`
	StockCode              string    `gorm:"column:STOCK_CODE"`
	SellDate               time.Time `gorm:"column:SELL_DATE"`
	SellPrice              float64   `gorm:"column:SELL_PRICE"`
	BuyDate                time.Time `gorm:"column:BUY_DATE"`
	BuyPrice               float64   `gorm:"column:BUY_PRICE"`
	AccumulativeProfitLoss float64   `gorm:"column:ACCUMULATIVE_PROFIT_LOSS"`
	ProfitLoss             float64   `gorm:"column:PROFIT_LOSS"`
}

type MdlHeiKinAshiUpDownArray []*MdlHeiKinAshiUpDown
