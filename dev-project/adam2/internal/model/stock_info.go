package model

type StockInfo struct {
	ID       int    `gorm:"column:ID_"`
	BoardId  int    `gorm:"column:BOARD_ID"`
	Code     string `gorm:"column:CODE_"`
	Name     string `gorm:"column:NAME_"`
	UrlParam string `gorm:"column:URL_PARAM"`
	Mark     string `gorm:"column:MARK"`
}

type StockInfoArray []*StockInfo
