package model

import "time"

type StockIndexWeek struct {
	ID                      int       `gorm:"column:ID_"`
	Code                    string    `gorm:"column:CODE_"`
	BeginDate               time.Time `gorm:"column:BEGIN_DATE"`
	EndDate                 time.Time `gorm:"column:END_DATE"`
	OpenPrice               float64   `gorm:"column:OPEN_PRICE"`
	ClosePrice              float64   `gorm:"column:CLOSE_PRICE"`
	HighestPrice            float64   `gorm:"column:HIGHEST_PRICE"`
	LowestPrice             float64   `gorm:"column:LOWEST_PRICE"`
	ChangeAmount            float64   `gorm:"column:CHANGE_AMOUNT"`
	ChangeRange             float64   `gorm:"column:CHANGE_RANGE"`
	Volume                  float64   `gorm:"column:VOLUME"`
	Turnover                float64   `gorm:"column:TURNOVER"`
	HaIndexWeekOpenPrice    float64   `gorm:"column:HA_INDEX_WEEK_OPEN_PRICE"`
	HaIndexWeekClosePrice   float64   `gorm:"column:HA_INDEX_WEEK_CLOSE_PRICE"`
	HaIndexWeekHighestPrice float64   `gorm:"column:HA_INDEX_WEEK_HIGHEST_PRICE"`
	HaIndexWeekLowestPrice  float64   `gorm:"column:HA_INDEX_WEEK_LOWEST_PRICE"`
}

type StockIndexWeekArray []*StockIndexWeek
