package model

import "time"

type StraightLineIncreaseDecrease struct {
	ID              int       `gorm:"column:ID"`
	TransactionType int       `gorm:"column:TRANSACTION_TYPE"`
	Code            string    `gorm:"column:CODE"`
	Name            string    `gorm:"column:NAME"`
	Message         string    `gorm:"column:MESSAGE"`
	CreateTime      time.Time `gorm:"column:CREATE_TIME"`
}

type StraightLineIncreaseDecreaseArray []*StraightLineIncreaseDecrease

// 定义表名
func (_straightLineIncreaseDecrease StraightLineIncreaseDecrease) TableName() string {
	return "STRAIGHT_LINE_INCR_DECR"
}
