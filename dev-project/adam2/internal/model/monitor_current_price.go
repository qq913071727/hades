package model

import (
	"time"
)

type MonitorCurrentPrice struct {
	ID              int       `gorm:"column:ID"`
	TransactionType int       `gorm:"column:TRANSACTION_TYPE"`
	Code            string    `gorm:"column:CODE"`
	Name            string    `gorm:"column:NAME"`
	Comparison      int       `gorm:"column:COMPARISON"`
	MonitorPrice    float64   `gorm:"column:MONITOR_PRICE"`
	CreateTime      time.Time `gorm:"column:CREATE_TIME"`
	Available       int       `gorm:"column:AVAILABLE"`
}

type MonitorCurrentPriceArray []*MonitorCurrentPrice

// 定义表名
func (_monitorCurrentPrice MonitorCurrentPrice) TableName() string {
	return "MONITOR_CURRENT_PRICE"
}
