package model

import "time"

type Real7TransactionCondition struct {
	ID                         int       `gorm:"column:ID_"`
	StockCode                  string    `gorm:"column:STOCK_CODE"`
	TransactionDate            time.Time `gorm:"column:TRANSACTION_DATE"`
	CalculateDate              time.Time `gorm:"column:CALCULATE_DATE"`
	Xr                         int       `gorm:"column:XR"`
	ClosePriceOutOfLimit       int       `gorm:"column:CLOSE_PRICE_OUT_OF_LIMIT"`
	FilterType                 int       `gorm:"column:FILTER_TYPE"`
	Direction                  int       `gorm:"column:DIRECTION"`
	AccumulativeProfitLoss     float64   `gorm:"column:ACCUMULATIVE_PROFIT_LOSS"`
	IsTransactionDate          int       `gorm:"column:IS_TRANSACTION_DATE"`
	Suspension                 int       `gorm:"column:SUSPENSION"`
	IsLimitUp                  int       `gorm:"column:IS_LIMIT_UP"`
	PrepareMA5                 float64   `gorm:"column:PREPARE_MA5"`
	PrepareMA10                float64   `gorm:"column:PREPARE_MA10"`
	PrepareMA20                float64   `gorm:"column:PREPARE_MA20"`
	PrepareMA60                float64   `gorm:"column:PREPARE_MA60"`
	PrepareMA120               float64   `gorm:"column:PREPARE_MA120"`
	PrepareMA250               float64   `gorm:"column:PREPARE_MA250"`
	MA5                        float64   `gorm:"column:MA5"`
	MA10                       float64   `gorm:"column:MA10"`
	MA20                       float64   `gorm:"column:MA20"`
	MA60                       float64   `gorm:"column:MA60"`
	MA120                      float64   `gorm:"column:MA120"`
	MA250                      float64   `gorm:"column:MA250"`
	Bias5                      float64   `gorm:"column:BIAS5"`
	Bias10                     float64   `gorm:"column:BIAS10"`
	Bias20                     float64   `gorm:"column:BIAS20"`
	Bias60                     float64   `gorm:"column:BIAS60"`
	Bias120                    float64   `gorm:"column:BIAS120"`
	Bias250                    float64   `gorm:"column:BIAS250"`
	VarianceType               float64   `gorm:"column:VARIANCE_TYPE"`
	Variance                   float64   `gorm:"column:VARIANCE"`
	PrepareMacdGoldCross       float64   `gorm:"column:PREPARE_MACD_GOLD_CROSS"`
	PrepareMacdDeadCross       float64   `gorm:"column:PREPARE_MACD_DEAD_CROSS"`
	PrepareClosePriceGoldCross float64   `gorm:"column:PREPARE_CLOSE_PRICE_GOLD_CROSS"`
	PrepareClosePriceDeadCross float64   `gorm:"column:PREPARE_CLOSE_PRICE_DEAD_CROSS"`
	PrepareHeiKinAshiUp        float64   `gorm:"column:PREPARE_HEI_KIN_ASHI_UP"`
	PrepareHeiKinAshiDown      float64   `gorm:"column:PREPARE_HEI_KIN_ASHI_DOWN"`
	PrepareKdGoldCross         float64   `gorm:"column:PREPARE_KD_GOLD_CROSS"`
	PrepareKdDeadCross         float64   `gorm:"column:PREPARE_KD_DEAD_CROSS"`
}

type Real7TransactionConditionArray []*Real7TransactionCondition
