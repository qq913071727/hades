package model

import "time"

type CommodityFutureInfo struct {
	ID                        int       `gorm:"column:ID"`
	Code                      string    `gorm:"column:CODE"`
	Name                      string    `gorm:"column:NAME"`
	CommodityFutureExchangeId int       `gorm:"column:COMMODITY_FUTURE_EXCHANGE_ID"`
	DailyTrading2EndHour      time.Time `gorm:"column:DAILY_TRADING_2_END_HOUR"`
	DailyTrading1BeginHour    time.Time `gorm:"column:DAILY_TRADING_1_BEGIN_HOUR"`
	DailyTrading1EndHour      time.Time `gorm:"column:DAILY_TRADING_1_END_HOUR"`
	DailyTrading2BeginHour    time.Time `gorm:"column:DAILY_TRADING_2_BEGIN_HOUR"`
	DailyTrading3BeginHour    time.Time `gorm:"column:DAILY_TRADING_3_BEGIN_HOUR"`
	DailyTrading3EndHour      time.Time `gorm:"column:DAILY_TRADING_3_END_HOUR"`
	DailyTradingBeginHour     time.Time `gorm:"column:NIGHT_TRADING_BEGIN_HOUR"`
	DailyTradingEndHour       time.Time `gorm:"column:NIGHT_TRADING_END_HOUR"`
	ContractUnit              int       `gorm:"column:CONTRACT_UNIT"`
	TransactionMultiplier     float64   `gorm:"column:TRANSACTION_MULTIPLIER"`
	PriceLimit                float64   `gorm:"column:PRICE_LIMIT"`
	ExchangeDeposit           float64   `gorm:"column:EXCHANGE_DEPOSIT"`
	CompanyDeposit            float64   `gorm:"column:COMPANY_DEPOSIT"`
	CalculationMethod         int       `gorm:"column:CALCULATION_METHOD"`
	OpenPosition              float64   `gorm:"column:OPEN_POSITION"`
	CloseTodayPosition        float64   `gorm:"column:CLOSE_TODAY_POSITION"`
	CloseOldPosition          float64   `gorm:"column:CLOSE_OLD_POSITION"`
	Tradable                  int       `gorm:"column:TRADABLE"`
}

type CommodityFutureInfoArray []*CommodityFutureInfo
