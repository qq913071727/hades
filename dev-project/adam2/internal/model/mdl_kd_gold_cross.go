package model

import "time"

type MdlKdGoldCross struct {
	ID                     int       `gorm:"column:ID"`
	StockCode              string    `gorm:"column:STOCK_CODE"`
	SellDate               time.Time `gorm:"column:SELL_DATE"`
	SellPrice              float64   `gorm:"column:SELL_PRICE"`
	SellK                  float64   `gorm:"column:SELL_K"`
	SellD                  float64   `gorm:"column:SELL_D"`
	BuyDate                time.Time `gorm:"column:BUY_DATE"`
	BuyPrice               float64   `gorm:"column:BUY_PRICE"`
	BuyK                   float64   `gorm:"column:BUY_K"`
	BuyD                   float64   `gorm:"column:BUY_D"`
	AccumulativeProfitLoss float64   `gorm:"column:ACCUMULATIVE_PROFIT_LOSS"`
	ProfitLoss             float64   `gorm:"column:PROFIT_LOSS"`
}

type MdlKdGoldCrossArray []*MdlKdGoldCross
