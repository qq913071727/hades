package model

import (
	"time"
)

type StockTransactionDataAll struct {
	ID                     int       `gorm:"column:ID_"`
	Date                   time.Time `gorm:"column:DATE_"`
	Code                   string    `gorm:"column:CODE_"`
	OpenPrice              float64   `gorm:"column:OPEN_PRICE"`
	ClosePrice             float64   `gorm:"column:CLOSE_PRICE"`
	HighestPrice           float64   `gorm:"column:HIGHEST_PRICE"`
	LowestPrice            float64   `gorm:"column:LOWEST_PRICE"`
	LastClosePrice         float64   `gorm:"column:LAST_CLOSE_PRICE"`
	ChangeAmount           float64   `gorm:"column:CHANGE_AMOUNT"`
	ChangeRange            float64   `gorm:"column:CHANGE_RANGE"`
	ChangeRangeExRight     float64   `gorm:"column:CHANGE_RANGE_EX_RIGHT"`
	UpDown                 int       `gorm:"column:UP_DOWN"`
	TurnoverRate           float64   `gorm:"column:TURNOVER_RATE"`
	Volume                 float64   `gorm:"column:VOLUME"`
	Turnover               float64   `gorm:"column:TURNOVER"`
	TotalMarketValue       float64   `gorm:"column:TOTAL_MARKET_VALUE"`
	CirculationMarketValue float64   `gorm:"column:CIRCULATION_MARKET_VALUE"`
	TransactionNumber      int       `gorm:"column:TRANSACTION_NUMBER"`
	Ma5                    float64   `gorm:"column:MA5"`
	Ma10                   float64   `gorm:"column:MA10"`
	Ma20                   float64   `gorm:"column:MA20"`
	Ma60                   float64   `gorm:"column:MA60"`
	Ma120                  float64   `gorm:"column:MA120"`
	Ma250                  float64   `gorm:"column:MA250"`
	Ema12                  float64   `gorm:"column:EMA12"`
	Ema26                  float64   `gorm:"column:EMA26"`
	Dif                    float64   `gorm:"column:DIF"`
	Dea                    float64   `gorm:"column:DEA"`
	Macd                   float64   `gorm:"column:MACD"`
	FiveDayVolatility      float64   `gorm:"column:FIVE_DAY_VOLATILITY"`
	TenDayVolatility       float64   `gorm:"column:FIVE_DAY_VOLATILITY"`
	TwentyDayVolatility    float64   `gorm:"column:TWENTY_DAY_VOLATILITY"`
	TwoHundredFifty        float64   `gorm:"column:TWO_HUNDRED_FIFTY"`
	Rsv                    float64   `gorm:"column:RSV"`
	K                      float64   `gorm:"column:K"`
	D                      float64   `gorm:"column:D"`
	HaOpenPrice            float64   `gorm:"column:HA_OPEN_PRICE"`
	HaClosePrice           float64   `gorm:"column:HA_CLOSE_PRICE"`
	HaHighestPrice         float64   `gorm:"column:HA_HIGHEST_PRICE"`
	HaLowestPrice          float64   `gorm:"column:HA_LOWEST_PRICE"`
	Up                     float64   `gorm:"column:UP"`
	Mb                     float64   `gorm:"column:MB"`
	Dn                     float64   `gorm:"column:DN_"`
	Bias5                  float64   `gorm:"column:BIAS5"`
	Bias10                 float64   `gorm:"column:BIAS10"`
	Bias20                 float64   `gorm:"column:BIAS20"`
	Bias60                 float64   `gorm:"column:BIAS60"`
	Bias120                float64   `gorm:"column:BIAS120"`
	Bias250                float64   `gorm:"column:BIAS250"`
	Variance5              float64   `gorm:"column:VARIANCE5"`
	Variance10             float64   `gorm:"column:VARIANCE10"`
	Variance20             float64   `gorm:"column:VARIANCE20"`
	Variance60             float64   `gorm:"column:VARIANCE60"`
	Variance120            float64   `gorm:"column:VARIANCE120"`
	Variance250            float64   `gorm:"column:VARIANCE250"`
}

type StockTransactionDataAllArray []*StockTransactionDataAll
