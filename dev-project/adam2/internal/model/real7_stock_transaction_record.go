package model

import "time"

type Real7StockTransactionRecord struct {
	ID                   int       `gorm:"column:ID_"`
	StockCode            string    `gorm:"column:STOCK_CODE"`
	BuyDate              time.Time `gorm:"column:BUY_DATE"`
	BuyPrice             float64   `gorm:"column:BUY_PRICE"`
	BuyAmount            int       `gorm:"column:BUY_AMOUNT"`
	SellDate             time.Time `gorm:"column:SELL_DATE"`
	SellPrice            float64   `gorm:"column:SELL_PRICE"`
	SellAmount           int       `gorm:"column:SELL_AMOUNT"`
	ProfitLossAmount     float64   `gorm:"column:PROFIT_LOSS_AMOUNT"`
	ProfitLossPercentage float64   `gorm:"column:PROFIT_LOSS_PERCENTAGE"`
	FilterType           int       `gorm:"column:FILTER_TYPE"`
	Direction            int       `gorm:"column:DIRECTION"`
}

type Real7StockTransactionRecordArray []*Real7StockTransactionRecord
