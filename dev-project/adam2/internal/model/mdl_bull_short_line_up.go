package model

import "time"

type MdlBullShortLineUp struct {
	ID                         int       `gorm:"column:ID"`
	StockCode                  string    `gorm:"column:STOCK_CODE"`
	SellDate                   time.Time `gorm:"column:SELL_DATE"`
	SellPrice                  float64   `gorm:"column:SELL_PRICE"`
	BuyDate                    time.Time `gorm:"column:BUY_DATE"`
	BuyPrice                   float64   `gorm:"column:BUY_PRICE"`
	BullShortLineWhenBuy       string    `gorm:"column:B_S_L_WHEN_BUY"`
	BullShortLinePriceWhenBuy  float64   `gorm:"column:B_S_L_PRICE_WHEN_BUY"`
	BullShortLineWhenSell      string    `gorm:"column:B_S_L_WHEN_SELL"`
	BullShortLinePriceWhenSell float64   `gorm:"column:B_S_L_PRICE_WHEN_SELL"`
	AccumulativeProfitLoss     float64   `gorm:"column:ACCUMULATIVE_PROFIT_LOSS"`
	ProfitLoss                 float64   `gorm:"column:PROFIT_LOSS"`
}

type MdlBullShortLineUpArray []*MdlBullShortLineUp
