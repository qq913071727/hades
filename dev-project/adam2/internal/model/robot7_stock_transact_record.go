package model

import "time"

type Robot7StockTransactRecord struct {
	ID                      int       `gorm:"column:ID_"`
	RobotName               string    `gorm:"column:ROBOT_NAME"`
	StockCode               string    `gorm:"column:STOCK_CODE"`
	BuyDate                 time.Time `gorm:"column:BUY_DATE"`
	BuyPrice                float64   `gorm:"column:BUY_PRICE"`
	BuyAmount               int       `gorm:"column:BUY_AMOUNT"`
	SellDate                time.Time `gorm:"column:SELL_DATE"`
	SellPrice               float64   `gorm:"column:SELL_PRICE"`
	SellAmount              int       `gorm:"column:SELL_AMOUNT"`
	FilterType              int       `gorm:"column:FILTER_TYPE"`
	Direction               int       `gorm:"column:DIRECTION"`
	TransactionType         int       `gorm:"column:TRANSACTION_TYPE"`
	ProfitAndLoss           int       `gorm:"column:PROFIT_AND_LOSS"`
	ProfitAndLossRate       float64   `gorm:"column:PROFIT_AND_LOSS_RATE"`
	StampDuty               float64   `gorm:"column:STAMP_DUTY"`
	RegistrationFeeWhenBuy  float64   `gorm:"column:REGISTRATION_FEE_WHEN_BUY"`
	CommissionWhenBuy       float64   `gorm:"column:COMMISSION_WHEN_BUY"`
	RegistrationFeeWhenSell float64   `gorm:"column:REGISTRATION_FEE_WHEN_SELL"`
	CommissionWhenSell      float64   `gorm:"column:COMMISSION_WHEN_SELL"`
}

type Robot7StockTransactRecordArray []*Robot7StockTransactRecord
