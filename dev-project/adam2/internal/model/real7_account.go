package model

type Real7Account struct {
	ID              int     `gorm:"column:ID_"`
	HoldStockNumber int     `gorm:"column:HOLD_STOCK_NUMBER"`
	StockAssets     float64 `gorm:"column:STOCK_ASSETS"`
	CapitalAssets   float64 `gorm:"column:CAPITAL_ASSETS"`
	TotalAssets     float64 `gorm:"column:TOTAL_ASSETS"`
}

type Real7AccountArray []*Real7Account
