package model

type Robot7StockFilter struct {
	ID                      int     `gorm:"column:ID_"`
	StockCode               string  `gorm:"column:STOCK_CODE"`
	FilterType              int     `gorm:"column:FILTER_TYPE"`
	Direction               int     `gorm:"column:DIRECTION"`
	AccumulativeProfitLoss  float64 `gorm:"column:ACCUMULATIVE_PROFIT_LOSS"`
	VarianceType            int     `gorm:"column:VARIANCE_TYPE"`
	Variance                float64 `gorm:"column:VARIANCE"`
	Correlation250WithAvgCP float64 `gorm:"column:CORRELATION250_WITH_AVG_C_P"`
	LastFiveDayYieldRate    float64 `gorm:"column:LAST_FIVE_DAY_YIELD_RATE"`
}

type Robot7StockFilterArray []*Robot7StockFilter
