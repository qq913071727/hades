package main

import (
	"adam2/internal/constants"
	iInitializer "adam2/internal/initializer"
	"adam2/internal/job"
	pInitializer "anubis-framework/pkg/initializer"
	"anubis-framework/pkg/io"
	"flag"
	"os"
	"os/signal"
	"syscall"
)

func init() {
	pInitializer.InitLog("C:/mywork/gitcode-repository/log/adam2.log")
	pInitializer.InitApplicationProperties()
	pInitializer.InitDatabaseProperties()
	iInitializer.InitDataSource()
	iInitializer.InitRobot7Properties()
	iInitializer.InitReal7Properties()
	iInitializer.InitChartConfig()
	iInitializer.InitJob()
	iInitializer.InitCronProperties()
	iInitializer.InitCoroutinePool()
	iInitializer.InitPythonProperties()
	iInitializer.InitManager()
	iInitializer.InitTrainingProperties()
	iInitializer.InitRouter()
	iInitializer.InitMiddleware()
	iInitializer.RegisterApi()
}

func main() {
	defer func() {
		if err := recover(); err != nil {
			io.Fatalf("error：", err)
		}
		io.Infoln("程序结束")
		exit := make(chan os.Signal, 1) // 需要buffer size是1，如果阻塞，信号会被丢弃
		signal.Notify(exit, os.Interrupt, syscall.SIGTERM)
	}()

	var jobName string
	flag.StringVar(&jobName, "jobName", constants.Robot7, "任务名称")
	flag.Parse()
	io.Infoln("任务名称：%s", jobName)

	switch jobName {
	case constants.Robot7:
		// 开始robot7测试
		job.StartRobot7()
		break
	case constants.Chart:
		// 开始创建图表
		job.CreateChart()
		break
	case constants.Real7:
		// 开始real交易试
		job.StartReal7()
		break
	case constants.Training:
		// 开始训练
		job.StartTraining()
		break
	case constants.Decrypt:
		// 解密
		job.Decrypt()
		break
	case constants.Cron:
		// 启动定时任务
		job.StartCron()
	}

}
