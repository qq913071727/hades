package org.bimon.manager;

import org.bimon.constant.Environment;
import org.bimon.constant.KNNInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnvironmentManager extends AbstractManager {

    private static final Logger logger = LoggerFactory.getLogger(EnvironmentManager.class);

    /**
     * 根据环境变量，获取数据文件路径
     * @return
     */
    public String getDataFilePathByEnvUSERDOMAIN() {
        logger.info("返回数据文件的路径");

        String env = System.getenv(Environment.USERDOMAIN);
        if (env.equals(Environment.USERDOMAIN_AT_HOME)) {
            logger.info("数据文件的路径是【{}】，在家中运行程序", KNNInfo.KNN_DATA_FILE_AT_HOME);
            return KNNInfo.KNN_DATA_FILE_AT_HOME;
        }
        if (env.equals(Environment.USERDOMAIN_AT_COMPANY)) {
            logger.info("数据文件的路径是【{}】，在公司中运行程序", KNNInfo.KNN_DATA_FILE_AT_COMPANY);
            return KNNInfo.KNN_DATA_FILE_AT_COMPANY;
        }
        return null;
    }
}
