package org.bimon.task;

import org.bimon.model.KNNDataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * k近邻算法
 */
public class KNNTask extends AbstractTask {

    private static final Logger logger = LoggerFactory.getLogger(KNNTask.class);

    /**
     * 开始k紧邻算法
     */
    public void doKnn(){
        logger.info("k近邻算法开始执行");

        KNNDataset knnDataset = knnHandler.getKNNDataset();

        logger.info("k近邻算法执行完毕");
    }
}
