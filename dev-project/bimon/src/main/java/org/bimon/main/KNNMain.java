package org.bimon.main;

import org.bimon.task.KNNTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KNNMain {

    private static final Logger logger = LoggerFactory.getLogger(KNNMain.class);

    public static void main(String[] args){
        logger.info("KNNMain函数开始执行");

        KNNTask knnTask = new KNNTask();
        knnTask.doKnn();

        logger.info("KNNMain函数执行完毕");
    }
}
