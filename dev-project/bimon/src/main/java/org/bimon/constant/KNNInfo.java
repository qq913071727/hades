package org.bimon.constant;

import org.bimon.util.PropertiesUtil;

/**
 * k近邻算法的信息的常量
 */
public class KNNInfo {

    /**
     * 数据文件的路径
     */
    public static final String KNN_DATA_FILE_AT_HOME = PropertiesUtil.getValue("bimon.KNN.DATA_FILE_AT_HOME");
    public static final String KNN_DATA_FILE_AT_COMPANY = PropertiesUtil.getValue("bimon.KNN.DATA_FILE_AT_COMPANY");

    /**
     * 训练数据和测试数据的百分比
     */
    public static final double KNN_TRAINING_DATASET_PERCENTAGE = Double.parseDouble(PropertiesUtil.getValue("bimon.KNN.TRAINING_DATASET_PERCENTAGE"));
    public static final double KNN_TESTING_DATASET_PERCENTAGE = Double.parseDouble(PropertiesUtil.getValue("bimon.KNN.TESTING_DATASET_PERCENTAGE"));
}