package org.bimon.constant;

import org.bimon.util.PropertiesUtil;

/**
 * 环境常量
 */
public class Environment {

    /**
     * 系统常量
     */
    public static final String USERDOMAIN = PropertiesUtil.getValue("bimon.env.USERDOMAIN");

    /**
     * 家中系统常量的值
     */
    public static final String USERDOMAIN_AT_HOME = PropertiesUtil.getValue("bimon.env.USERDOMAIN_AT_HOME");

    /**
     * 公司系统常量的值
     */
    public static final String USERDOMAIN_AT_COMPANY = PropertiesUtil.getValue("bimon.env.USERDOMAIN_AT_COMPANY");
}
