package org.bimon.util;

import java.io.IOException;
import java.util.Properties;

/**
 * 属性文件工具类
 */
public class PropertiesUtil {

    /**
     * 根据key，获取属性值
     *
     * @param key
     * @return
     */
    public static String getValue(String key) {
        Properties properties = new Properties();
        try {
            properties.load(PropertiesUtil.class.getClassLoader().getResourceAsStream("bimon.properties"));
            return properties.getProperty(key);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
