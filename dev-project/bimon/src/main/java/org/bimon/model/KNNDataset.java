package org.bimon.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * k近邻算法数据集对象
 */
@Data
@EqualsAndHashCode
public class KNNDataset {

    /**
     * 训练数据集
     */
    private List<List<Double>> trainingDataset;

    /**
     * 训练数据集的标签
     */
    private List<String> trainingDatasetLabel;

    /**
     * 测试数据集
     */
    private List<List<Double>> testingDataset;

    /**
     * 测试数据集的标签
     */
    private List<String> testingDatasetLabel;

}
