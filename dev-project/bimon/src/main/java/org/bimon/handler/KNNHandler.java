package org.bimon.handler;

import org.bimon.model.KNNDataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * k近邻算法handler类
 */
public class KNNHandler extends AbstractHandler {

    private static final Logger logger = LoggerFactory.getLogger(KNNHandler.class);

    public KNNDataset getKNNDataset() {
        // 返回数据文件的路径
        String filePath = environmentManager.getDataFilePathByEnvUSERDOMAIN();
        // 读取文件内容
        List<String[]> fileContentList = fileManager.readAll(filePath);

        // 去除表头，区分数据集和标签
        List<String> labelList = new ArrayList<>();
        List<List<Double>> datasetList = new ArrayList<>();
        if (null != fileContentList && fileContentList.size() > 0) {
            for (int i = 1; i < fileContentList.size(); i++) {
                String[] rowArray = fileContentList.get(i);
                List<Double> rowList = new ArrayList<>();
                for (int j = 0; j < rowArray.length; j++) {
                    if (j == rowArray.length - 1) {
                        // 标签
                        labelList.add(rowArray[j]);
                    } else {
                        // 数据集
                        rowList.add(new Double(rowArray[j]));
                    }
                }
                datasetList.add(rowList);
            }
        } else {
            logger.warn("数据文件的内容为空");
        }

        // 行列互换
        List<List<Double>> rowColumnInterchangeList = this.rowColumnInterchange(datasetList);

        // 归一化
        List<List<Double>> normalList = this.normalize(rowColumnInterchangeList);

        // 行列互换
        rowColumnInterchangeList = this.rowColumnInterchange(normalList);

        // 区分训练数据集、训练数据集标签、测试数据集、测试数据集标签
        KNNDataset knnDataset = this.differentiateTrainingDatasetAndTestingDataset(rowColumnInterchangeList, labelList);
        return null;
    }


}
