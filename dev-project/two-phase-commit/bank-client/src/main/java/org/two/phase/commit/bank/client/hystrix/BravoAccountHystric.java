package org.two.phase.commit.bank.client.hystrix;

import org.springframework.stereotype.Component;

import org.two.phase.commit.bank.client.service.IBravoAccountService;

@Component
public class BravoAccountHystric implements IBravoAccountService {

    @Override
    public double getAmountByUserId(String userId) {
        throw new RuntimeException("IAlphaAccountService unavailable");
    }

    @Override
    public void setAmountForUserId(String userId, double amount) {
        throw new RuntimeException("IAlphaAccountService unavailable");
    }

    @Override
    public void changeAmountForUserId(String userId, double amount) {
        throw new RuntimeException("IAlphaAccountService unavailable");
    }

    @Override
    public void changeAmountForUserId4TPC(String userId, double amount) {
        throw new RuntimeException("IAlphaAccountService unavailable");
    }
}