package org.sraosha.springboot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.sraosha.springboot.mapper.UserMapper;
import org.sraosha.springboot.model.User;
import org.sraosha.springboot.service.UserService;

@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
