package org.sraosha.springboot.service;

import org.sraosha.springboot.pojo.ServiceResult;

/**
 * 登录的service接口
 */
public interface LoginService {

    /**
     * 根据前端给定微信登录的code获取openid
     * @param code
     * @return
     */
    ServiceResult<String> jscode2session(String code);
}
