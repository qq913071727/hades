package org.sraosha.springboot.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 微信配置类
 */
@Data
@Component
@ConfigurationProperties(prefix = "wechat")
public class WechatValue {

    private String appId;

    private String appSecret;
}
