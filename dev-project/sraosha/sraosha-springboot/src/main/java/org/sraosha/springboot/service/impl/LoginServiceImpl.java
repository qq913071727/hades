package org.sraosha.springboot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.sraosha.springboot.enumeration.ServiceResultEnum;
import org.sraosha.springboot.pojo.ServiceResult;
import org.sraosha.springboot.service.LoginService;
import org.sraosha.springboot.value.WechatValue;
import org.sraosha.springboot.util.HttpUtils;

import javax.annotation.Resource;

/**
 * 登录接口的实现类
 */
@Slf4j
@Service
public class LoginServiceImpl implements LoginService {

    @Resource
    private WechatValue wechatValue;

    /**
     * 根据前端给定微信登录的code获取openid
     * @param code
     * @return
     */
    @Override
    public ServiceResult<String> jscode2session(String code) {
        String url = "https://api.weixin.qq.com/sns/jscode2session?appid="
                + wechatValue.getAppId() + "&secret=" + wechatValue.getAppSecret()
                + "&js_code=" + code + "&grant_type=authorization_code";
        String result = HttpUtils.doGet(url);

        ServiceResult<String> serviceResult = new ServiceResult<>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setCode(ServiceResultEnum.SUCCESS.getCode());
        serviceResult.setMessage(ServiceResultEnum.SUCCESS.getMessage());
        serviceResult.setResult(result);
        return serviceResult;
    }
}
