package org.sraosha.springboot.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

/**
 * 调用API的返回对象
 * @param <T>
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ApiResult<T>", description = "接口返回值对象")
public class ApiResult<T> implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "代码", dataType = "Integer")
    private Integer code;

    @ApiModelProperty(value = "消息", dataType = "String")
    private String message;

    @ApiModelProperty(value = "返回结果", dataType = "T")
    private T result = null;

    @ApiModelProperty(value = "是否成功", dataType = "Boolean")
    private Boolean success;

    public ApiResult(Integer code, String message, T result, Boolean success) {
        this.code = code;
        this.message = message;
        this.result = result;
        this.success = success;
    }

    public ApiResult(){}

    /**
     * @return
     * @description 成功
     * @parms
     */
    public static <T> ApiResult<T> ok(T data, String message) {
        return new ApiResult<T>(HttpStatus.OK.value(), message, data, true);
    }

    /**
     * @return
     * @description 成功
     * @parms
     */
    public static <T> ApiResult<T> ok(String message) {
        return new ApiResult<T>(HttpStatus.OK.value(), message, null, true);
    }

    /**
     * @return
     * @description 失败
     * @parms
     */
    public static <T> ApiResult<T> error(Integer code, String message) {
        return new ApiResult<T>(code, message, null, false);
    }
}