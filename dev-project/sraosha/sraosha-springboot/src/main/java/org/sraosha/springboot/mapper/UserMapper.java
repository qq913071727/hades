package org.sraosha.springboot.mapper;

import org.springframework.stereotype.Repository;
import org.sraosha.springboot.config.EasyBaseMapper;
import org.sraosha.springboot.model.User;

@Repository
public interface UserMapper extends EasyBaseMapper<User> {
}
