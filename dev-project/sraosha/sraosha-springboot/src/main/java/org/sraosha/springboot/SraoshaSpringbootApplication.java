package org.sraosha.springboot;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@MapperScan("org.sraosha.springboot.mapper")
@EnableSwagger2
@Slf4j
public class SraoshaSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SraoshaSpringbootApplication.class, args);
        log.info("sraosha-springboot服务启动成功");
    }
}