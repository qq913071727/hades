package org.sraosha.springboot.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 调用服务层的返回对象
 * @param <T>
 */
@Data
@ApiModel(value = "ServiceResult<T>", description = "service返回值对象")
public class ServiceResult<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "代码", dataType = "Integer")
    private Integer code;

    @ApiModelProperty(value = "返回结果", dataType = "T")
    private T result;

    @ApiModelProperty(value = "是否成功", dataType = "Boolean")
    private Boolean success;

    @ApiModelProperty(value = "消息", dataType = "String")
    private String message;

    public ServiceResult(Integer code, T data, boolean success, String message) {
        this.code = code;
        this.result = data;
        this.success = success;
        this.message = message;
    }

    public ServiceResult(Integer code, boolean success, String message) {
        this.code = code;
        this.success = success;
        this.message = message;
    }

    public ServiceResult() {
    }

    /**
     * @return
     * @description 成功
     * @parms
     */
    public static <T> ServiceResult<T> ok(Integer code, T data, String message) {
        return new ServiceResult<T>(code, data, true,  message);
    }

    /**
     * @return
     * @description 成功 不带返回值
     * @parms
     */
    public static ServiceResult ok(Integer code, String message) {
        return new ServiceResult(code, null, true,  message);
    }

    /**
     * @return
     * @description 失败
     * @parms
     */
    public static <T> ServiceResult<T> error(Integer code, String message) {
        return new ServiceResult<T>(code, null, false,  message);
    }
}