package org.sraosha.springboot.enumeration;

/**
 * ServiceResult类的枚举类，主要涉及和数据相关的状态
 */
public enum ServiceResultEnum {

    /**
     * 成功
     */
    SUCCESS(4001, true, "成功"),
    /**
     * 失败
     */
    FAIL(4002, false, "失败"),
    /**
     * 查询成功
     */
    SELECT_SUCCESS(4003, true, "查询成功"),
    /**
     * 查询失败
     */
    SELECT_FAIL(4004, false, "查询失败"),
    /**
     * 更新成功
     */
    UPDATE_SUCCESS(4005, true, "更新成功"),
    /**
     * 更新失败
     */
    UPDATE_FAIL(4006, false, "更新失败"),
    /**
     * 插入成功
     */
    INSERT_SUCCESS(4007, true, "插入成功"),
    /**
     * 插入失败
     */
    INSERT_FAIL(4008, false, "插入失败"),
    /**
     * 删除成功
     */
    DELETE_SUCCESS(4009, true, "删除成功"),
    /**
     * 删除失败
     */
    DELETE_FAIL(4010, false, "删除失败"),
    /**
     * 导入成功
     */
    IMPORT_SUCCESS(4011, true, "导入成功"),
    /**
     * 导入失败
     */
    IMPORT_FAIL(4012, false, "导入失败"),
    /**
     * 操作成功
     */
    OPERATE_SUCCESS(4013, true, "操作成功"),
    /**
     * 操作失败
     */
    OPERATE_FAIL(4014, false, "操作失败"),
    /**
     * 同步成功
     */
    SYNC_SUCCESS(4015, true, "同步成功"),
    /**
     * 同步失败
     */
    SYNC_FAIL(4016, false, "同步失败");

    private Integer code;

    private Boolean success;

    private String message;

    ServiceResultEnum() {
    }

    ServiceResultEnum(Integer code, Boolean success, String message) {
        this.code = code;
        this.success = success;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
