package org.sraosha.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.sraosha.springboot.service.LoginService;

/**
 * 抽象controller
 */
public abstract class AbstractController {

    @Autowired
    protected LoginService loginService;

}
    