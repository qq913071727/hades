//package org.sraosha.listener;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.curator.framework.CuratorFramework;
//import org.apache.curator.framework.recipes.locks.InterProcessMutex;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.event.ApplicationStartedEvent;
//import org.springframework.context.ApplicationListener;
//import org.springframework.stereotype.Component;
//import org.sraosha.base.value.ZookeeperParamConfig;
//import org.sraosha.framework.manager.ThreadPoolManager;
//
//import javax.annotation.Resource;
//import java.util.concurrent.TimeUnit;
//
///**
// * @desc 自动加载导入数据任务
// */
//@Slf4j
//@Component
//public class StartHandlerListener implements ApplicationListener<ApplicationStartedEvent> {
//
//    @Resource
//    private ThreadPoolManager threadPoolManager;
//
//    @Autowired
//    protected CuratorFramework curatorFramework;
//
//    @Autowired
//    protected ZookeeperParamConfig zookeeperParamConfig;
//
//    @Override
//    public void onApplicationEvent(ApplicationStartedEvent applicationStartedEvent) {
//        try {
//            InterProcessMutex lock = new InterProcessMutex(curatorFramework, zookeeperParamConfig.getImportRedisDataPath());
//            if (lock.acquire(zookeeperParamConfig.getWaitingLockTime(), TimeUnit.MINUTES)) {
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}