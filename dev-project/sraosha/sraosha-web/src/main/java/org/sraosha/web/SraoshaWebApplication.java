package org.sraosha.web;

import org.sraosha.framework.helper.LogbackHelper;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = "org.sraosha")
@MapperScan("org.sraosha.base.mapper")
@EnableSwagger2
@EnableDiscoveryClient
@Slf4j
public class SraoshaWebApplication {

    public static void main(String[] args) {
        //设置日志端口
        LogbackHelper.setLogbackPort(args);

        SpringApplication.run(SraoshaWebApplication.class, args);
        log.info("sraosha-web服务启动成功");
    }
}
