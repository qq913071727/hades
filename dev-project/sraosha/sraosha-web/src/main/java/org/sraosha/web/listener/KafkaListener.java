//package org.sraosha.listener;
//
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.stereotype.Component;
//import org.sraosha.framework.manager.ThreadPoolManager;
//
//import javax.annotation.Resource;
//
///**
// * 当出现数据库操作后，调用这个监听器的方法
// */
//@Component
//@EnableScheduling
//public class KafkaListener {
//    @Resource
//    private ServiceHelper serviceHelper;
//
//    @Resource
//    private ThreadPoolManager threadPoolManager;
//
//    /**
//     * 当出现数据库操作后，调用这个方法
//     * @param consumer 数据库变动内容
//     */
//    @org.springframework.kafka.annotation.KafkaListener(topics = "sc")
//    public void listen(ConsumerRecord<?, ?> consumer) {
//        String json = (String) consumer.value();
//        JSONObject metaDataJsonObject = JSONObject.parseObject(json);
//        // 数据库名称
//        String database = metaDataJsonObject.getString("database");
//        // 表名称
//        String table = metaDataJsonObject.getString("table");
//        // 操作类型
//        String type = metaDataJsonObject.getString("type");
//        // 操作数据
//        JSONArray dataJsonArray = metaDataJsonObject.getJSONArray("data");
//
//        if (null != dataJsonArray && dataJsonArray.size() > 0) {
//            JSONObject dataJsonObject = dataJsonArray.getJSONObject(0);
//            // 数据库
//            if (MultipleDataSource.ADAM.equals(database) || MultipleDataSource.MONITOR.equals(database)) {
//
//            }
//        }
//    }
//}