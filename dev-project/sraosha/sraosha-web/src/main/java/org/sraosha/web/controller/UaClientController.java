//package org.sraosha.controller;
//
//import com.github.pagehelper.PageInfo;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiImplicitParams;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.BeanUtils;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import org.sraosha.base.modelClient;
//import org.sraosha.framework.dto.ApiResult;
//import org.sraosha.framework.dto.ServiceResult;
//import org.sraosha.base.voClientVo;
//import org.sraosha.controller.AbstractController;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * UaClient的controller类
// */
//@Api(value = "UaClient的controller类", tags = "UaClientController")
//@RestController
//@RequestMapping("api/v1/uaClient")
//@Slf4j
//public class UaClientController extends AbstractController {
//
//    /**
//     * 新增
//     * @param uaClientVo UaClient的vo类
//     * @param request
//     * @param response
//     * @return
//     */
//    @ApiOperation("新增")
//    @ApiImplicitParam(name = "uaClientVo", value = "UaClient的vo类", required = true, dataType = "UaClientVo")
//    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<Integer>> add(@RequestBody UaClientVo uaClientVo, HttpServletRequest request, HttpServletResponse response) {
//        ApiResult apiResult = new ApiResult();
//        if (null == uaClientVo) {
//            String message = "调用接口【/api/v1/uaClient/add时，参数不合法";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        UaClient uaClient = new UaClient();
//        BeanUtils.copyProperties(uaClientVo, uaClient);
//        ServiceResult<Integer> serviceResult = uaClientService.add(uaClient);
//
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(serviceResult.getMessage());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 批量导入
//     * @param uaClientVoList UaClient的vo类列表
//     * @param request
//     * @param response
//     * @return
//     */
//    @ApiOperation("批量导入")
//    @ApiImplicitParam(name = "uaClientVoList", value = "UaClient的vo类列表", required = true, dataType = "List")
//    @RequestMapping(value = "/batchAdd", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<Integer>> batchAdd(@RequestBody List<UaClientVo> uaClientVoList, HttpServletRequest request, HttpServletResponse response) {
//        ApiResult apiResult = new ApiResult();
//        if (null == uaClientVoList) {
//            String message = "调用接口【/api/v1/uaClient/batchAdd时，参数不合法";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        List<UaClient> uaClientList = new ArrayList<>();
//        BeanUtils.copyProperties(uaClientVoList, uaClientList);
//        ServiceResult<Integer> serviceResult = uaClientService.batchAdd(uaClientList);
//
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(serviceResult.getMessage());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 根据id更新
//     * @param uaClientVo UaClient的vo类
//     * @param request
//     * @param response
//     * @return
//     */
//    @ApiOperation("根据id更新")
//    @ApiImplicitParam(name = "uaClientVo", value = "UaClient的vo类", required = true, dataType = "UaClientVo")
//    @RequestMapping(value = "/updateForId", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<Integer>> updateForId(@RequestBody UaClientVo uaClientVo, HttpServletRequest request, HttpServletResponse response) {
//        ApiResult apiResult = new ApiResult();
//        if (null == uaClientVo) {
//            String message = "调用接口【/api/v1/uaClient/updateForId时，参数不合法";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        UaClient uaClient = new UaClient();
//        BeanUtils.copyProperties(uaClientVo, uaClient);
//        ServiceResult<Integer> serviceResult = uaClientService.updateForId(uaClient);
//
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(serviceResult.getMessage());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 根据id删除
//     * @param id 主键
//     * @param request
//     * @param response
//     * @return
//     */
//    @ApiOperation("根据id删除")
//    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Integer")
//    @RequestMapping(value = "/deleteById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<Integer>> deleteById(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
//        ApiResult apiResult = new ApiResult();
//        if (null == id) {
//            String message = "调用接口【/api/v1/uaClient/deleteById时，参数不合法";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        ServiceResult<Integer> serviceResult = uaClientService.deleteById(id);
//
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(serviceResult.getMessage());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 根据id查询
//     * @param id 主键
//     * @param request
//     * @param response
//     * @return
//     */
//    @ApiOperation("根据id查询")
//    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Integer")
//    @RequestMapping(value = "/selectById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<UaClient>> selectById(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
//        ApiResult apiResult = new ApiResult();
//        if (null == id) {
//            String message = "调用接口【/api/v1/uaClient/selectById时，参数不合法";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        ServiceResult<UaClient> serviceResult = uaClientService.selectById(id);
//
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(serviceResult.getMessage());
//        apiResult.setResult(serviceResult.getResult());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 根据条件，分页查询，升序/降序排列
//     * @param uaClientVo UaClient的vo类
//     * @param request
//     * @param response
//     * @return
//     */
//    @ApiOperation("根据条件，分页查询，升序/降序排列")
//    @ApiImplicitParam(name = "uaClientVo", value = "UaClient的vo类", required = true, dataType = "UaClientVo")
//    @RequestMapping(value = "/page", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<PageInfo<UaClient>>> page(@RequestBody UaClientVo uaClientVo, HttpServletRequest request, HttpServletResponse response) {
//        ApiResult apiResult = new ApiResult();
//        if (null == uaClientVo) {
//            String message = "调用接口【/api/v1/uaClient/page时，参数不合法";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//        if (null == uaClientVo.getPageNo() || null == uaClientVo.getPageSize()) {
//            String message = "调用接口【/api/v1/uaClient/page时，参数pageNo和pageSize不能为空";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        UaClient uaClient = new UaClient();
//        BeanUtils.copyProperties(uaClientVo, uaClient);
//        ServiceResult<PageInfo<UaClient>> serviceResult = uaClientService.page(uaClient, uaClientVo.getPageNo(), uaClientVo.getPageSize());
//
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(serviceResult.getMessage());
//        apiResult.setResult(serviceResult.getResult());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//}
