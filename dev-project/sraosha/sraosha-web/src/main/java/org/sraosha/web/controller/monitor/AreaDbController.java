package org.sraosha.web.controller.monitor;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.sraosha.base.model.monitor.AreaDb;
import org.sraosha.framework.dto.ApiResult;
import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.base.vo.monitor.AreaDbVo;
import org.sraosha.web.controller.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * AreaDb的controller类
 */
@Api(value = "AreaDb的controller类", tags = "AreaDbController")
@RestController
@RequestMapping("api/v1/areaDb")
@Slf4j
public class AreaDbController extends AbstractController {

    /**
     * 新增
     * @param areaDbVo AreaDb的vo类
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("新增")
    @ApiImplicitParam(name = "areaDbVo", value = "AreaDb的vo类", required = true, dataType = "AreaDbVo")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> add(@RequestBody AreaDbVo areaDbVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == areaDbVo) {
            String message = "调用接口【/api/v1/areaDb/add时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        AreaDb areaDb = new AreaDb();
        BeanUtils.copyProperties(areaDbVo, areaDb);
        ServiceResult<Integer> serviceResult = areaDbService.add(areaDb);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 批量导入
     * @param areaDbVoList AreaDb的vo类列表
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("批量导入")
    @ApiImplicitParam(name = "areaDbVoList", value = "AreaDb的vo类列表", required = true, dataType = "List")
    @RequestMapping(value = "/batchAdd", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> batchAdd(@RequestBody List<AreaDbVo> areaDbVoList, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == areaDbVoList) {
            String message = "调用接口【/api/v1/areaDb/batchAdd时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        List<AreaDb> areaDbList = new ArrayList<>();
        BeanUtils.copyProperties(areaDbVoList, areaDbList);
        ServiceResult<Integer> serviceResult = areaDbService.batchAdd(areaDbList);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id更新
     * @param areaDbVo AreaDb的vo类
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id更新")
    @ApiImplicitParam(name = "areaDbVo", value = "AreaDb的vo类", required = true, dataType = "AreaDbVo")
    @RequestMapping(value = "/updateForId", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> updateForId(@RequestBody AreaDbVo areaDbVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == areaDbVo) {
            String message = "调用接口【/api/v1/areaDb/updateForId时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        AreaDb areaDb = new AreaDb();
        BeanUtils.copyProperties(areaDbVo, areaDb);
        ServiceResult<Integer> serviceResult = areaDbService.updateForId(areaDb);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id删除
     * @param id 主键
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id删除")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Integer")
    @RequestMapping(value = "/deleteById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> deleteById(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == id) {
            String message = "调用接口【/api/v1/areaDb/deleteById时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Integer> serviceResult = areaDbService.deleteById(id);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id查询
     * @param id 主键
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id查询")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Integer")
    @RequestMapping(value = "/selectById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<AreaDb>> selectById(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == id) {
            String message = "调用接口【/api/v1/areaDb/selectById时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<AreaDb> serviceResult = areaDbService.selectById(id);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param areaDbVo AreaDb的vo类
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据条件，分页查询，升序/降序排列")
    @ApiImplicitParam(name = "areaDbVo", value = "AreaDb的vo类", required = true, dataType = "AreaDbVo")
    @RequestMapping(value = "/page", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<PageInfo<AreaDb>>> page(@RequestBody AreaDbVo areaDbVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == areaDbVo) {
            String message = "调用接口【/api/v1/areaDb/page时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (null == areaDbVo.getPageNo() || null == areaDbVo.getPageSize()) {
            String message = "调用接口【/api/v1/areaDb/page时，参数pageNo和pageSize不能为空";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        AreaDb areaDb = new AreaDb();
        BeanUtils.copyProperties(areaDbVo, areaDb);
        ServiceResult<PageInfo<AreaDb>> serviceResult = areaDbService.page(areaDb, areaDbVo.getPageNo(), areaDbVo.getPageSize());

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
