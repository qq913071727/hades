package org.sraosha.web.listener;

import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.base.service.impl.ServiceHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 调用kmc的接口/server/cert，申请服务器证书
 * 调用kmc的接口/server/register，服务器注册，获取token
 */
@Slf4j
@Component
public class BootstrapApplicationListener implements ApplicationListener<ContextRefreshedEvent>, Ordered {

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 调用kmc的接口/server/cert，申请服务器证书
     * 调用kmc的接口/server/register，服务器注册，获取token
     * @param contextRefreshedEvent
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("监听器启动");

        // 将client表中的数据保存到redis中
        serviceHelper.saveUaClientInRedis();
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
