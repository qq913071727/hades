package org.sraosha.web.config;

import org.sraosha.framework.exception.handler.CustomBlockExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * sentinel相关配置
 */
@Configuration
public class SentinelConfig {

    @Bean
    public CustomBlockExceptionHandler customBlockExceptionHandler(){
        return new CustomBlockExceptionHandler();
    }
}
