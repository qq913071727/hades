//package org.sraosha.web.listener;
//
//import org.sraosha.base.pojo.constants.RabbitMqConstants;
//import org.sraosha.base.pojo.enumeration.RabbitMqEnum;
//import org.sraosha.base.pojo.variable.MessagePacket;
//import org.sraosha.base.service.impl.ServiceHelper;
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.google.common.base.Strings;
//import com.rabbitmq.client.AMQP;
//import com.rabbitmq.client.Channel;
//import com.rabbitmq.client.DefaultConsumer;
//import com.rabbitmq.client.Envelope;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.boot.ApplicationArguments;
//import org.springframework.boot.ApplicationRunner;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.io.IOException;
//
///**
// * mq队列收到消息后，调用这个监听器的方法
// */
//@Slf4j
//@Component
//public class RabbitMqListener implements ApplicationRunner {
//
//    @Resource
//    private ServiceHelper serviceHelper;
//
//    /**
//     * 监听队列
//     */
//    @Override
//    public void run(ApplicationArguments args) {
//        try {
//            Channel channel = serviceHelper.getRabbitMqManager().getChannel();
//            channel.queueBind(RabbitMqEnum.KMC_PSK.getQueue(), RabbitMqEnum.KMC_PSK.getExchange(), RabbitMqEnum.KMC_PSK.getRoutingKey());
//            channel.queueDeclare(RabbitMqEnum.KMC_PSK.getQueue(), true, false, false, null);
//            channel.exchangeDeclare(RabbitMqEnum.KMC_PSK.getExchange(), RabbitMqConstants.DIRECT_TYPE, true);
//            DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
//                @Override
//                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
//                    String bodyString = new String(body);
//                    log.info("交换器【{}】的队列【{}】接收到消息【{}】", RabbitMqEnum.KMC_PSK.getExchange(), RabbitMqEnum.KMC_PSK.getQueue(), bodyString);
//                    try {
//                        if (!Strings.isNullOrEmpty(bodyString)) {
//                            MessagePacket messagePacket = JSONObject.parseObject(bodyString, MessagePacket.class);
//                            if (null != messagePacket) {
//
//                            }
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            };
//            channel.basicConsume(RabbitMqEnum.KMC_PSK.getQueue(), true, defaultConsumer);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//}