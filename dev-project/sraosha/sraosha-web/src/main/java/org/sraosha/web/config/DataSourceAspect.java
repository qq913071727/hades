package org.sraosha.web.config;

import org.sraosha.base.pojo.constants.MultipleDataSource;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value = -100)
@Aspect
@Slf4j
public class DataSourceAspect {

    @Pointcut("execution(* org.sraosha.base.service.impl..*.*(..))")
    private void uaAspect() {
    }

    @Pointcut("execution(* org.sraosha.base.service.auth.impl..*.*(..))")
    private void authAspect() {
    }

    @Before("uaAspect()")
    public void ua() {
        log.debug("切换到{}数据源...", MultipleDataSource.SRAOSHA);
        DynamicDataSourceContextHolder.setDataSourceKey(MultipleDataSource.SRAOSHA);
    }

    @Before("authAspect()")
    public void auth() {
        log.debug("切换到{}数据源...", MultipleDataSource.SRAOSHA_AUTH);
        DynamicDataSourceContextHolder.setDataSourceKey(MultipleDataSource.SRAOSHA_AUTH);
    }

}
