package org.sraosha.web.controller.adam;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.sraosha.base.model.adam.StockTransactionData;
import org.sraosha.framework.dto.ApiResult;
import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.base.vo.adam.StockTransactionDataVo;
import org.sraosha.web.controller.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * StockTransactionData的controller类
 */
@Api(value = "StockTransactionData的controller类", tags = "StockTransactionDataController")
@RestController
@RequestMapping("api/v1/stockTransactionData")
@Slf4j
public class StockTransactionDataController extends AbstractController {

    /**
     * 新增
     * @param stockTransactionDataVo StockTransactionData的vo类
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("新增")
    @ApiImplicitParam(name = "stockTransactionDataVo", value = "StockTransactionData的vo类", required = true, dataType = "StockTransactionDataVo")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> add(@RequestBody StockTransactionDataVo stockTransactionDataVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == stockTransactionDataVo) {
            String message = "调用接口【/api/v1/stockTransactionData/add时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        StockTransactionData stockTransactionData = new StockTransactionData();
        BeanUtils.copyProperties(stockTransactionDataVo, stockTransactionData);
        ServiceResult<Integer> serviceResult = stockTransactionDataService.add(stockTransactionData);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 批量导入
     * @param stockTransactionDataVoList StockTransactionData的vo类列表
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("批量导入")
    @ApiImplicitParam(name = "stockTransactionDataVoList", value = "StockTransactionData的vo类列表", required = true, dataType = "List")
    @RequestMapping(value = "/batchAdd", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> batchAdd(@RequestBody List<StockTransactionDataVo> stockTransactionDataVoList, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == stockTransactionDataVoList) {
            String message = "调用接口【/api/v1/stockTransactionData/batchAdd时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        List<StockTransactionData> stockTransactionDataList = new ArrayList<>();
        BeanUtils.copyProperties(stockTransactionDataVoList, stockTransactionDataList);
        ServiceResult<Integer> serviceResult = stockTransactionDataService.batchAdd(stockTransactionDataList);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id更新
     * @param stockTransactionDataVo StockTransactionData的vo类
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id更新")
    @ApiImplicitParam(name = "stockTransactionDataVo", value = "StockTransactionData的vo类", required = true, dataType = "StockTransactionDataVo")
    @RequestMapping(value = "/updateForId", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> updateForId(@RequestBody StockTransactionDataVo stockTransactionDataVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == stockTransactionDataVo) {
            String message = "调用接口【/api/v1/stockTransactionData/updateForId时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        StockTransactionData stockTransactionData = new StockTransactionData();
        BeanUtils.copyProperties(stockTransactionDataVo, stockTransactionData);
        ServiceResult<Integer> serviceResult = stockTransactionDataService.updateForId(stockTransactionData);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id删除
     * @param id 主键
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id删除")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Integer")
    @RequestMapping(value = "/deleteById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> deleteById(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == id) {
            String message = "调用接口【/api/v1/stockTransactionData/deleteById时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Integer> serviceResult = stockTransactionDataService.deleteById(id);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id查询
     * @param id 主键
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id查询")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Integer")
    @RequestMapping(value = "/selectById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<StockTransactionData>> selectById(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == id) {
            String message = "调用接口【/api/v1/stockTransactionData/selectById时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<StockTransactionData> serviceResult = stockTransactionDataService.selectById(id);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param stockTransactionDataVo StockTransactionData的vo类
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据条件，分页查询，升序/降序排列")
    @ApiImplicitParam(name = "stockTransactionDataVo", value = "StockTransactionData的vo类", required = true, dataType = "StockTransactionDataVo")
    @RequestMapping(value = "/page", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<PageInfo<StockTransactionData>>> page(@RequestBody StockTransactionDataVo stockTransactionDataVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == stockTransactionDataVo) {
            String message = "调用接口【/api/v1/stockTransactionData/page时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (null == stockTransactionDataVo.getPageNo() || null == stockTransactionDataVo.getPageSize()) {
            String message = "调用接口【/api/v1/stockTransactionData/page时，参数pageNo和pageSize不能为空";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        StockTransactionData stockTransactionData = new StockTransactionData();
        BeanUtils.copyProperties(stockTransactionDataVo, stockTransactionData);
        ServiceResult<PageInfo<StockTransactionData>> serviceResult = stockTransactionDataService.page(stockTransactionData, stockTransactionDataVo.getPageNo(), stockTransactionDataVo.getPageSize());

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
