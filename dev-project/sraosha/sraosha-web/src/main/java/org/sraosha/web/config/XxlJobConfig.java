//package org.sraosha.config;
//
//import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.sraosha.base.value.XxljobParamConfig;
//
//import javax.annotation.Resource;
//
///**
// * xxl配置类
// */
//@Configuration
//public class XxlJobConfig {
//    @Resource
//    private XxljobParamConfig xxljobParamConfig;
//
//    @Bean
//    public XxlJobSpringExecutor xxlJobExecutor() {
//        XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
//        xxlJobSpringExecutor.setAdminAddresses(xxljobParamConfig.getAdminAddresses());
//        xxlJobSpringExecutor.setAppname(xxljobParamConfig.getAppname());
//        xxlJobSpringExecutor.setPort(xxljobParamConfig.getPort());
//        xxlJobSpringExecutor.setAccessToken(xxljobParamConfig.getAccessToken());
//        xxlJobSpringExecutor.setLogPath(xxljobParamConfig.getLogPath());
//        xxlJobSpringExecutor.setLogRetentionDays(xxljobParamConfig.getLogRetentionDays());
//        return xxlJobSpringExecutor;
//    }
//}