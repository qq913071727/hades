//package org.sraosha.controller;
//
//import org.sraosha.framework.enumeration.ApiResultEnum;
//import com.github.pagehelper.PageInfo;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.BeanUtils;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import org.sraosha.base.model.UserLoginLog;
//import org.sraosha.framework.dto.ApiResult;
//import org.sraosha.framework.dto.ServiceResult;
//import org.sraosha.base.vo.UserLoginLogVo;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * UserLoginLog的controller类
// */
//@Api(value = "UserLoginLog的controller类", tags = "UserLoginLogController")
//@RestController
//@RequestMapping("api/v1/userLoginLog")
//@Slf4j
//public class UserLoginLogController extends AbstractController {
//
//    /**
//     * 新增
//     *
//     * @param userLoginLogVo UserLoginLog的vo类
//     * @param request
//     * @param response
//     * @return
//     */
//    @ApiOperation("新增")
//    @ApiImplicitParam(name = "userLoginLogVo", value = "UserLoginLog的vo类", required = true, dataType = "UserLoginLogVo")
//    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<Integer>> add(@RequestBody UserLoginLogVo userLoginLogVo, HttpServletRequest request, HttpServletResponse response) {
//        ApiResult apiResult = new ApiResult();
//        if (null == userLoginLogVo) {
//            String message = "调用接口【/api/v1/userLoginLog/add时，参数不能为空";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            apiResult.setCode(ApiResultEnum.PARAMETER_EMPTY.getCode());
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        UserLoginLog userLoginLog = new UserLoginLog();
//        BeanUtils.copyProperties(userLoginLogVo, userLoginLog);
//        ServiceResult<Integer> serviceResult = userLoginLogService.add(userLoginLog);
//
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(serviceResult.getMessage());
//        apiResult.setCode(serviceResult.getCode());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 批量导入
//     *
//     * @param userLoginLogVoList UserLoginLog的vo类列表
//     * @param request
//     * @param response
//     * @return
//     */
//    @ApiOperation("批量导入")
//    @ApiImplicitParam(name = "userLoginLogVoList", value = "UserLoginLog的vo类列表", required = true, dataType = "List")
//    @RequestMapping(value = "/batchAdd", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<Integer>> batchAdd(@RequestBody List<UserLoginLogVo> userLoginLogVoList, HttpServletRequest request, HttpServletResponse response) {
//        ApiResult apiResult = new ApiResult();
//        if (null == userLoginLogVoList) {
//            String message = "调用接口【/api/v1/userLoginLog/batchAdd时，参数不能为空";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(ApiResultEnum.PARAMETER_EMPTY.getMessage());
//            apiResult.setCode(ApiResultEnum.PARAMETER_EMPTY.getCode());
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        List<UserLoginLog> userLoginLogList = new ArrayList<>();
//        BeanUtils.copyProperties(userLoginLogVoList, userLoginLogList);
//        ServiceResult<Integer> serviceResult = userLoginLogService.batchAdd(userLoginLogList);
//
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(serviceResult.getMessage());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 根据id更新
//     *
//     * @param userLoginLogVo UserLoginLog的vo类
//     * @param request
//     * @param response
//     * @return
//     */
//    @ApiOperation("根据id更新")
//    @ApiImplicitParam(name = "userLoginLogVo", value = "UserLoginLog的vo类", required = true, dataType = "UserLoginLogVo")
//    @RequestMapping(value = "/updateForId", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<Integer>> updateForId(@RequestBody UserLoginLogVo userLoginLogVo, HttpServletRequest request, HttpServletResponse response) {
//        ApiResult apiResult = new ApiResult();
//        if (null == userLoginLogVo) {
//            String message = "调用接口【/api/v1/userLoginLog/updateForId时，参数不合法";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        UserLoginLog userLoginLog = new UserLoginLog();
//        BeanUtils.copyProperties(userLoginLogVo, userLoginLog);
//        ServiceResult<Integer> serviceResult = userLoginLogService.updateForId(userLoginLog);
//
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(serviceResult.getMessage());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 根据id删除
//     *
//     * @param id       主键
//     * @param request
//     * @param response
//     * @return
//     */
//    @ApiOperation("根据id删除")
//    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Integer")
//    @RequestMapping(value = "/deleteById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<Integer>> deleteById(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
//        ApiResult apiResult = new ApiResult();
//        if (null == id) {
//            String message = "调用接口【/api/v1/userLoginLog/deleteById时，参数不合法";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        ServiceResult<Integer> serviceResult = userLoginLogService.deleteById(id);
//
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(serviceResult.getMessage());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 根据id查询
//     *
//     * @param id       主键
//     * @param request
//     * @param response
//     * @return
//     */
//    @ApiOperation("根据id查询")
//    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Integer")
//    @RequestMapping(value = "/selectById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<UserLoginLog>> selectById(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
//        ApiResult apiResult = new ApiResult();
//        if (null == id) {
//            String message = "调用接口【/api/v1/userLoginLog/selectById时，参数不合法";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        ServiceResult<UserLoginLog> serviceResult = userLoginLogService.selectById(id);
//
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(serviceResult.getMessage());
//        apiResult.setResult(serviceResult.getResult());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//    /**
//     * 根据条件，分页查询，升序/降序排列
//     *
//     * @param userLoginLogVo UserLoginLog的vo类
//     * @param request
//     * @param response
//     * @return
//     */
//    @ApiOperation("根据条件，分页查询，升序/降序排列")
//    @ApiImplicitParam(name = "userLoginLogVo", value = "UserLoginLog的vo类", required = true, dataType = "UserLoginLogVo")
//    @RequestMapping(value = "/page", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
//    @ResponseBody
//    public ResponseEntity<ApiResult<PageInfo<UserLoginLog>>> page(@RequestBody UserLoginLogVo userLoginLogVo, HttpServletRequest request, HttpServletResponse response) {
//        ApiResult apiResult = new ApiResult();
//        if (null == userLoginLogVo) {
//            String message = "调用接口【/api/v1/userLoginLog/page时，参数不合法";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//        if (null == userLoginLogVo.getPageNo() || null == userLoginLogVo.getPageSize()) {
//            String message = "调用接口【/api/v1/userLoginLog/page时，参数pageNo和pageSize不能为空";
//            log.warn(message);
//            apiResult.setSuccess(Boolean.FALSE);
//            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
//            apiResult.setMessage(message);
//            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
//        }
//
//        UserLoginLog userLoginLog = new UserLoginLog();
//        BeanUtils.copyProperties(userLoginLogVo, userLoginLog);
//        ServiceResult<PageInfo<UserLoginLog>> serviceResult = userLoginLogService.page(userLoginLog, userLoginLogVo.getPageNo(), userLoginLogVo.getPageSize());
//
//        apiResult.setSuccess(Boolean.TRUE);
//        apiResult.setCode(HttpStatus.OK.value());
//        apiResult.setMessage(serviceResult.getMessage());
//        apiResult.setResult(serviceResult.getResult());
//        return new ResponseEntity<>(apiResult, HttpStatus.OK);
//    }
//
//
//}
