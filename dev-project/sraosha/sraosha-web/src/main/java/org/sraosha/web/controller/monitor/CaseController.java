package org.sraosha.web.controller.monitor;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.sraosha.base.model.monitor.Case;
import org.sraosha.framework.dto.ApiResult;
import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.base.vo.monitor.CaseVo;
import org.sraosha.web.controller.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Case的controller类
 */
@Api(value = "Case的controller类", tags = "CaseController")
@RestController
@RequestMapping("api/v1/_case")
@Slf4j
public class CaseController extends AbstractController {

    /**
     * 新增
     * @param caseVo Case的vo类
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("新增")
    @ApiImplicitParam(name = "caseVo", value = "Case的vo类", required = true, dataType = "CaseVo")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> add(@RequestBody CaseVo caseVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == caseVo) {
            String message = "调用接口【/api/v1/_case/add时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        Case _case = new Case();
        BeanUtils.copyProperties(caseVo, _case);
        ServiceResult<Integer> serviceResult = caseService.add(_case);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 批量导入
     * @param caseVoList Case的vo类列表
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("批量导入")
    @ApiImplicitParam(name = "caseVoList", value = "Case的vo类列表", required = true, dataType = "List")
    @RequestMapping(value = "/batchAdd", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> batchAdd(@RequestBody List<CaseVo> caseVoList, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == caseVoList) {
            String message = "调用接口【/api/v1/_case/batchAdd时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        List<Case> _caseList = new ArrayList<>();
        BeanUtils.copyProperties(caseVoList, _caseList);
        ServiceResult<Integer> serviceResult = caseService.batchAdd(_caseList);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id更新
     * @param caseVo Case的vo类
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id更新")
    @ApiImplicitParam(name = "caseVo", value = "Case的vo类", required = true, dataType = "CaseVo")
    @RequestMapping(value = "/updateForId", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> updateForId(@RequestBody CaseVo caseVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == caseVo) {
            String message = "调用接口【/api/v1/_case/updateForId时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        Case _case = new Case();
        BeanUtils.copyProperties(caseVo, _case);
        ServiceResult<Integer> serviceResult = caseService.updateForId(_case);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id删除
     * @param id 主键
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id删除")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Integer")
    @RequestMapping(value = "/deleteById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> deleteById(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == id) {
            String message = "调用接口【/api/v1/_case/deleteById时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Integer> serviceResult = caseService.deleteById(id);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id查询
     * @param id 主键
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id查询")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Integer")
    @RequestMapping(value = "/selectById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Case>> selectById(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == id) {
            String message = "调用接口【/api/v1/_case/selectById时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Case> serviceResult = caseService.selectById(id);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param caseVo Case的vo类
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据条件，分页查询，升序/降序排列")
    @ApiImplicitParam(name = "caseVo", value = "Case的vo类", required = true, dataType = "CaseVo")
    @RequestMapping(value = "/page", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<PageInfo<Case>>> page(@RequestBody CaseVo caseVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == caseVo) {
            String message = "调用接口【/api/v1/_case/page时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (null == caseVo.getPageNo() || null == caseVo.getPageSize()) {
            String message = "调用接口【/api/v1/_case/page时，参数pageNo和pageSize不能为空";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        Case _case = new Case();
        BeanUtils.copyProperties(caseVo, _case);
        ServiceResult<PageInfo<Case>> serviceResult = caseService.page(_case, caseVo.getPageNo(), caseVo.getPageSize());

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
