package org.sraosha.web.controller;

import org.sraosha.base.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.sraosha.base.service.adam.StockTransactionDataService;
import org.sraosha.base.service.monitor.AreaDbService;
import org.sraosha.base.service.monitor.CaseService;

/**
 * 抽象controller
 */
public abstract class AbstractController {

    @Autowired
    protected CaseService caseService;

    @Autowired
    protected AreaDbService areaDbService;

    @Autowired
    protected StockTransactionDataService stockTransactionDataService;

    @Autowired
    protected UserLoginLogService userLoginLogService;

}
    