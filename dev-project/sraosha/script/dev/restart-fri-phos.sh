# ------------------------应用关闭---------------------------------------------------------------
echo 'sraosha应用关闭开始'

cd /opt
echo '进入到/opt目录'

echo 'Stop the program : sraosha.jar'
pid=`ps -ef | grep -w sraosha | grep -v "grep" | awk '{print $2}'`;
echo 'old Procedure pid:'$pid
if [ -n "$pid" ]
then
kill -9 $pid
fi
sleep 1;

echo 'sraosha应用关闭完成'

# ------------------------应用启动---------------------------------------------------------------
echo 'sraosha应用启动开始'

cd /opt
echo '进入到/opt目录'

echo 'Start the program : sraosha.jar'
chmod 777 /opt/deploy/sraosha/dev/sraosha.jar
echo '-------Starting sraosha.jar-------'
nohup java -Xms512M -Xmx1024M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/hprof/sraosha/dev/sraosha-dev.hprof -Xdebug -jar -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:49197 /opt/deploy/sraosha/dev/sraosha.jar --server.port=51417 --kmc.uniCode=ff37fcc79a2cf38ff71306d16563a883> /dev/null  2>&1 &
echo 'fsraosha:serverPort=51417 start success'
nohup java -Xms512M -Xmx1024M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/hprof/sraosha/dev/sraosha-dev.hprof -Xdebug -jar -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:37190 /opt/deploy/sraosha/dev/sraosha.jar --server.port=49149 --kmc.uniCode=6f5ea8b3008e200fe323d89d166a4303> /dev/null  2>&1 &
echo 'fsraosha:serverPort=49149 start success'

echo 'sraosha应用启动完成'
