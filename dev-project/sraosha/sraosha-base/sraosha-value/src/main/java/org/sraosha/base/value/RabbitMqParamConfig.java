package org.sraosha.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * rabbitmq交换机配置
 */
@Data
@Component
@ConfigurationProperties(prefix = "rabbitmq")
public class RabbitMqParamConfig {
    /** 地址 */
    private String host;

    /** 端口 */
    private int port;

    /** 用户名 */
    private String username;

    /** 密码 */
    private String password;

    /** 虚拟主机 */
    private String virtualHost;
}