package org.sraosha.base.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * redis配置类
 */
@Data
@Component
@ConfigurationProperties(prefix = "redis")
public class RedisParamConfig {
    /**
     * 连接超时时间（单位：毫秒）
     */
    private Integer timeout;

    /**
     * token超时时间（单位：毫秒）
     */
    private Integer tokenTimeout;

    /**
     * redis分布式锁的超时时间
     */
    private Integer lockTimeout;

    /**
     * redis分布式锁在加锁之后，解锁之前的等待时间（单位：毫秒）
     */
    private Integer lockWaitingTime;
}