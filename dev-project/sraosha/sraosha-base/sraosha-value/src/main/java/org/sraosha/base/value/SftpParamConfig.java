//package org.sraosha.base.value;
//
//import lombok.Data;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.stereotype.Component;
//
///**
// * sftp配置类
// */
//@Data
//@Component
//@ConfigurationProperties(prefix = "sftp")
//public class SftpParamConfig {
//    /** ip地址 */
//    private String host;
//
//    /** 端口号 */
//    private int port;
//
//    /** 用户名 */
//    private String username;
//
//    /** 密码 */
//    private String password;
//
//    /** 超时时间 */
//    private int timeout;
//}