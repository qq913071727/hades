package org.sraosha.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.sql.Blob;
import java.sql.Clob;

/**
 * client表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UaClient", description = "client表")
@TableName("client")
public class UaClient {

    @ApiModelProperty(value = "authorization", dataType = "String")
    @TableField(value = "authorization")
    private String authorization;

    @ApiModelProperty(value = "描述", dataType = "String")
    @TableField(value = "description")
    private String description;

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

}
