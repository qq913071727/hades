package org.sraosha.base.model.adam;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.sql.Blob;
import java.sql.Clob;

/**
 * 周线级别交易数据
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "StockWeek", description = "周线级别交易数据")
@TableName("STOCK_WEEK")
public class StockWeek {

    @ApiModelProperty(value = "带除权的本周股价的涨跌幅。单位：%", dataType = "BigDecimal")
    @TableField(value = "CHANGE_RANGE_EX_RIGHT")
    private BigDecimal changeRangeExRight;

    @ApiModelProperty(value = "HeiKinAshi本周开盘价", dataType = "BigDecimal")
    @TableField(value = "HA_WEEK_OPEN_PRICE")
    private BigDecimal haWeekOpenPrice;

    @ApiModelProperty(value = "HeiKinAshi本周收盘价", dataType = "BigDecimal")
    @TableField(value = "HA_WEEK_CLOSE_PRICE")
    private BigDecimal haWeekClosePrice;

    @ApiModelProperty(value = "HeiKinAshi本周最高价", dataType = "BigDecimal")
    @TableField(value = "HA_WEEK_HIGHEST_PRICE")
    private BigDecimal haWeekHighestPrice;

    @ApiModelProperty(value = "HeiKinAshi本周最低价", dataType = "BigDecimal")
    @TableField(value = "HA_WEEK_LOWEST_PRICE")
    private BigDecimal haWeekLowestPrice;

    @ApiModelProperty(value = "表示主键", dataType = "BigDecimal")
    @TableId(value = "ID_", type = IdType.AUTO)
    private BigDecimal id;

    @ApiModelProperty(value = "表示某一周的开始时间", dataType = "Date")
    @TableField(value = "BEGIN_DATE")
    private Date beginDate;

    @ApiModelProperty(value = "股票代码", dataType = "String")
    @TableField(value = "CODE_")
    private String code;

    @ApiModelProperty(value = "1表示和前一天比较当天是上涨；0表示和前一天比较当天是不涨不跌；-1表示和前一天比较当天是下跌", dataType = "BigDecimal")
    @TableField(value = "UP_DOWN")
    private BigDecimal upDown;

    @ApiModelProperty(value = "表示某一周的结束时间", dataType = "Date")
    @TableField(value = "END_DATE")
    private Date endDate;

    @ApiModelProperty(value = "表示这条记录是这只股票的第多少个交易周", dataType = "BigDecimal")
    @TableField(value = "NUMBER_")
    private BigDecimal number;

    @ApiModelProperty(value = "周线级别开盘价", dataType = "BigDecimal")
    @TableField(value = "OPEN_PRICE")
    private BigDecimal openPrice;

    @ApiModelProperty(value = "周线级别收盘价", dataType = "BigDecimal")
    @TableField(value = "CLOSE_PRICE")
    private BigDecimal closePrice;

    @ApiModelProperty(value = "周线级别最高价", dataType = "BigDecimal")
    @TableField(value = "HIGHEST_PRICE")
    private BigDecimal highestPrice;

    @ApiModelProperty(value = "周线级别最低价", dataType = "BigDecimal")
    @TableField(value = "LOWEST_PRICE")
    private BigDecimal lowestPrice;

    @ApiModelProperty(value = "周线级别成交量", dataType = "BigDecimal")
    @TableField(value = "VOLUME")
    private BigDecimal volume;

    @ApiModelProperty(value = "周线级别成交额", dataType = "BigDecimal")
    @TableField(value = "TURNOVER")
    private BigDecimal turnover;

    @ApiModelProperty(value = "5日均线", dataType = "BigDecimal")
    @TableField(value = "MA5")
    private BigDecimal ma5;

    @ApiModelProperty(value = "10日均线", dataType = "BigDecimal")
    @TableField(value = "MA10")
    private BigDecimal ma10;

    @ApiModelProperty(value = "20日均线", dataType = "BigDecimal")
    @TableField(value = "MA20")
    private BigDecimal ma20;

    @ApiModelProperty(value = "60日均线", dataType = "BigDecimal")
    @TableField(value = "MA60")
    private BigDecimal ma60;

    @ApiModelProperty(value = "120日均线", dataType = "BigDecimal")
    @TableField(value = "MA120")
    private BigDecimal ma120;

    @ApiModelProperty(value = "250日均线", dataType = "BigDecimal")
    @TableField(value = "MA250")
    private BigDecimal ma250;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    @TableField(value = "DIF")
    private BigDecimal dif;

    @ApiModelProperty(value = "计算KD时的指标", dataType = "BigDecimal")
    @TableField(value = "RSV")
    private BigDecimal rsv;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    @TableField(value = "DEA")
    private BigDecimal dea;

    @ApiModelProperty(value = "布林带下轨", dataType = "BigDecimal")
    @TableField(value = "DN_")
    private BigDecimal dn;

    @ApiModelProperty(value = "计算KD时的指标", dataType = "BigDecimal")
    @TableField(value = "K")
    private BigDecimal k;

    @ApiModelProperty(value = "计算KD时的指标", dataType = "BigDecimal")
    @TableField(value = "D")
    private BigDecimal d;

    @ApiModelProperty(value = "布林带中轨", dataType = "BigDecimal")
    @TableField(value = "MB")
    private BigDecimal mb;

    @ApiModelProperty(value = "布林带上轨", dataType = "BigDecimal")
    @TableField(value = "UP")
    private BigDecimal up;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    @TableField(value = "EMA12")
    private BigDecimal ema12;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    @TableField(value = "EMA26")
    private BigDecimal ema26;

}
