package org.sraosha.base.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 用户登录日志表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserLoginLog", description = "用户登录日志表")
@TableName("user_login_log")
public class UserLoginLog {

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(value = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableField(value = "id")
    private Integer id;

    @ApiModelProperty(value = "远程地址/客户端IP", dataType = "String")
    @TableField(value = "remote_address")
    private String remoteAddress;

    @ApiModelProperty(value = "用户名", dataType = "String")
    @TableField(value = "username")
    private String username;

}
