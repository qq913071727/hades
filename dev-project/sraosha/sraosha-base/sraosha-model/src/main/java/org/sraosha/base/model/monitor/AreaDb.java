package org.sraosha.base.model.monitor;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.sql.Blob;
import java.sql.Clob;

/**
 * 地区表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AreaDb", description = "地区表")
@TableName("area_db")
public class AreaDb {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableField(value = "id")
    private Integer id;

    @ApiModelProperty(value = "省", dataType = "String")
    @TableField(value = "province")
    private String province;

    @ApiModelProperty(value = "市", dataType = "String")
    @TableField(value = "city")
    private String city;

    @ApiModelProperty(value = "区", dataType = "String")
    @TableField(value = "district")
    private String district;

    @ApiModelProperty(value = "地区标签", dataType = "String")
    @TableField(value = "area_tag")
    private String areaTag;

    @ApiModelProperty(value = "地区坐标", dataType = "String")
    @TableField(value = "area_point")
    private String areaPoint;

    @ApiModelProperty(value = "插入时间", dataType = "String")
    @TableField(value = "insert_time")
    private String insertTime;

    @ApiModelProperty(value = "备用字段1", dataType = "String")
    @TableField(value = "field1")
    private String field1;

    @ApiModelProperty(value = "备用字段2", dataType = "String")
    @TableField(value = "field2")
    private String field2;

}
