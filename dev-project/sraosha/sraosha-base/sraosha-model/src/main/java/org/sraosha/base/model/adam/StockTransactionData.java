package org.sraosha.base.model.adam;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.sql.Blob;
import java.sql.Clob;

/**
 * 交易记录表（部分数据）
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "StockTransactionData", description = "交易记录表（部分数据）")
@TableName("STOCK_TRANSACTION_DATA")
public class StockTransactionData {

    @ApiModelProperty(value = "最低价", dataType = "BigDecimal")
    @TableField(value = "LOWEST_PRICE")
    private BigDecimal lowestPrice;

    @ApiModelProperty(value = "最高价", dataType = "BigDecimal")
    @TableField(value = "HIGHEST_PRICE")
    private BigDecimal highestPrice;

    @ApiModelProperty(value = "收盘价", dataType = "BigDecimal")
    @TableField(value = "CLOSE_PRICE")
    private BigDecimal closePrice;

    @ApiModelProperty(value = "开盘价", dataType = "BigDecimal")
    @TableField(value = "OPEN_PRICE")
    private BigDecimal openPrice;

    @ApiModelProperty(value = "代码", dataType = "String")
    @TableField(value = "CODE_")
    private String code;

    @ApiModelProperty(value = "日期", dataType = "Date")
    @TableField(value = "DATE_")
    private Date date;

    @ApiModelProperty(value = "主键", dataType = "BigDecimal")
    @TableId(value = "ID_", type = IdType.AUTO)
    private BigDecimal id;

    @ApiModelProperty(value = "HeiKinAshi最低价", dataType = "BigDecimal")
    @TableField(value = "HA_LOWEST_PRICE")
    private BigDecimal haLowestPrice;

    @ApiModelProperty(value = "HeiKinAshi最高价", dataType = "BigDecimal")
    @TableField(value = "HA_HIGHEST_PRICE")
    private BigDecimal haHighestPrice;

    @ApiModelProperty(value = "HeiKinAshi收盘价", dataType = "BigDecimal")
    @TableField(value = "HA_CLOSE_PRICE")
    private BigDecimal haClosePrice;

    @ApiModelProperty(value = "HeiKinAshi开盘价", dataType = "BigDecimal")
    @TableField(value = "HA_OPEN_PRICE")
    private BigDecimal haOpenPrice;

    @ApiModelProperty(value = "计算KD时的指标", dataType = "BigDecimal")
    @TableField(value = "D")
    private BigDecimal d;

    @ApiModelProperty(value = "计算KD时的指标", dataType = "BigDecimal")
    @TableField(value = "K")
    private BigDecimal k;

    @ApiModelProperty(value = "计算KD时的指标", dataType = "BigDecimal")
    @TableField(value = "RSV")
    private BigDecimal rsv;

    @ApiModelProperty(value = "股价250日内震荡幅度。计算方法为：将250日内涨跌百分比的绝对值相加，再除以250。单位：%", dataType = "BigDecimal")
    @TableField(value = "TWO_HUNDRED_FIFTY")
    private BigDecimal twoHundredFifty;

    @ApiModelProperty(value = "股价20日内震荡幅度。计算方法为：将20日内涨跌百分比的绝对值相加，再除以20。单位：%", dataType = "BigDecimal")
    @TableField(value = "TWENTY_DAY_VOLATILITY")
    private BigDecimal twentyDayVolatility;

    @ApiModelProperty(value = "股价10日内震荡幅度。计算方法为：将10日内涨跌百分比的绝对值相加，再除以10。单位：%", dataType = "BigDecimal")
    @TableField(value = "TEN_DAY_VOLATILITY")
    private BigDecimal tenDayVolatility;

    @ApiModelProperty(value = "股价5日内震荡幅度。计算方法为：将5日内涨跌百分比的绝对值相加，再除以5。单位：%", dataType = "BigDecimal")
    @TableField(value = "FIVE_DAY_VOLATILITY")
    private BigDecimal fiveDayVolatility;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    @TableField(value = "DEA")
    private BigDecimal dea;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    @TableField(value = "DIF")
    private BigDecimal dif;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    @TableField(value = "EMA26")
    private BigDecimal ema26;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    @TableField(value = "EMA12")
    private BigDecimal ema12;

    @ApiModelProperty(value = "250日均线", dataType = "BigDecimal")
    @TableField(value = "MA250")
    private BigDecimal ma250;

    @ApiModelProperty(value = "120日均线", dataType = "BigDecimal")
    @TableField(value = "MA120")
    private BigDecimal ma120;

    @ApiModelProperty(value = "60日均线", dataType = "BigDecimal")
    @TableField(value = "MA60")
    private BigDecimal ma60;

    @ApiModelProperty(value = "20日均线", dataType = "BigDecimal")
    @TableField(value = "MA20")
    private BigDecimal ma20;

    @ApiModelProperty(value = "10日均线", dataType = "BigDecimal")
    @TableField(value = "MA10")
    private BigDecimal ma10;

    @ApiModelProperty(value = "5日均线", dataType = "BigDecimal")
    @TableField(value = "MA5")
    private BigDecimal ma5;

    @ApiModelProperty(value = "成交笔数", dataType = "BigDecimal")
    @TableField(value = "TRANSACTION_NUMBER")
    private BigDecimal transactionNumber;

    @ApiModelProperty(value = "流通市值", dataType = "BigDecimal")
    @TableField(value = "CIRCULATION_MARKET_VALUE")
    private BigDecimal circulationMarketValue;

    @ApiModelProperty(value = "总市值", dataType = "BigDecimal")
    @TableField(value = "TOTAL_MARKET_VALUE")
    private BigDecimal totalMarketValue;

    @ApiModelProperty(value = "成交额", dataType = "BigDecimal")
    @TableField(value = "TURNOVER")
    private BigDecimal turnover;

    @ApiModelProperty(value = "成交量", dataType = "BigDecimal")
    @TableField(value = "VOLUME")
    private BigDecimal volume;

    @ApiModelProperty(value = "换手率", dataType = "BigDecimal")
    @TableField(value = "TURNOVER_RATE")
    private BigDecimal turnoverRate;

    @ApiModelProperty(value = "今日股价是否涨跌。1：涨，-1：跌，0：平", dataType = "BigDecimal")
    @TableField(value = "UP_DOWN")
    private BigDecimal upDown;

    @ApiModelProperty(value = "带除权的今日股价的涨跌幅。单位：%", dataType = "BigDecimal")
    @TableField(value = "CHANGE_RANGE_EX_RIGHT")
    private BigDecimal changeRangeExRight;

    @ApiModelProperty(value = "今日股价的涨跌幅。单位：%。注意：不包括除权。", dataType = "BigDecimal")
    @TableField(value = "CHANGE_RANGE")
    private BigDecimal changeRange;

    @ApiModelProperty(value = "今日股价的涨跌额", dataType = "BigDecimal")
    @TableField(value = "CHANGE_AMOUNT")
    private BigDecimal changeAmount;

    @ApiModelProperty(value = "前收盘", dataType = "BigDecimal")
    @TableField(value = "LAST_CLOSE_PRICE")
    private BigDecimal lastClosePrice;

    @ApiModelProperty(value = "布林带下轨", dataType = "BigDecimal")
    @TableField(value = "DN_")
    private BigDecimal dn;

    @ApiModelProperty(value = "布林带中轨", dataType = "BigDecimal")
    @TableField(value = "MB")
    private BigDecimal mb;

    @ApiModelProperty(value = "布林带上轨", dataType = "BigDecimal")
    @TableField(value = "UP")
    private BigDecimal up;

}
