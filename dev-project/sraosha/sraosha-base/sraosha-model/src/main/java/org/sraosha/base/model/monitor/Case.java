package org.sraosha.base.model.monitor;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.sql.Blob;
import java.sql.Clob;

/**
 * 案件信息
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Case", description = "案件信息")
@TableName("case")
public class Case {

    @ApiModelProperty(value = "案件实体码", dataType = "Integer")
    @TableField(value = "id")
    private Integer id;

    @ApiModelProperty(value = "案号", dataType = "String")
    @TableField(value = "number")
    private String number;

    @ApiModelProperty(value = "案件名称", dataType = "String")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty(value = "案件类型", dataType = "Integer")
    @TableField(value = "type")
    private Integer type;

    @ApiModelProperty(value = "经办法院", dataType = "Integer")
    @TableField(value = "court_id")
    private Integer courtId;

    @ApiModelProperty(value = "收案来源", dataType = "Integer")
    @TableField(value = "source")
    private Integer source;

    @ApiModelProperty(value = "审理方式", dataType = "Integer")
    @TableField(value = "trial_mode")
    private Integer trialMode;

    @ApiModelProperty(value = "承办人", dataType = "Integer")
    @TableField(value = "undertaker")
    private Integer undertaker;

    @ApiModelProperty(value = "案件进展阶段", dataType = "Integer")
    @TableField(value = "progress_stage")
    private Integer progressStage;

    @ApiModelProperty(value = "审判程序", dataType = "Integer")
    @TableField(value = "trial_procedure")
    private Integer trialProcedure;

    @ApiModelProperty(value = "适用程序", dataType = "Integer")
    @TableField(value = "applicable_procedure")
    private Integer applicableProcedure;

    @ApiModelProperty(value = "收案日期", dataType = "Date")
    @TableField(value = "receive_date")
    private Date receiveDate;

    @ApiModelProperty(value = "立案案由", dataType = "Integer")
    @TableField(value = "receive_cause")
    private Integer receiveCause;

    @ApiModelProperty(value = "结案日期", dataType = "Date")
    @TableField(value = "close_date")
    private Date closeDate;

    @ApiModelProperty(value = "结案案由", dataType = "Integer")
    @TableField(value = "close_cause")
    private Integer closeCause;

    @ApiModelProperty(value = "创建时间", dataType = "Timestamp")
    @TableField(value = "create_time")
    private Timestamp createTime;

    @ApiModelProperty(value = "修改时间", dataType = "Timestamp")
    @TableField(value = "update_time")
    private Timestamp updateTime;

}
