package org.sraosha.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * UaClient的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UaClientVo", description = "UaClient的vo类")
public class UaClientVo {
    @ApiModelProperty(value = "clientId", dataType = "String")
    private String clientId;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String description;

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;
}