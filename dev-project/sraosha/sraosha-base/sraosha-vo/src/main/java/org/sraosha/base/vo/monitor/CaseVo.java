package org.sraosha.base.vo.monitor;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Case的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CaseVo", description = "Case的vo类")
public class CaseVo {
    @ApiModelProperty(value = "案件实体码", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "案号", dataType = "String")
    private String number;

    @ApiModelProperty(value = "案件名称", dataType = "String")
    private String name;

    @ApiModelProperty(value = "案件类型", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "经办法院", dataType = "Integer")
    private Integer courtId;

    @ApiModelProperty(value = "收案来源", dataType = "Integer")
    private Integer source;

    @ApiModelProperty(value = "审理方式", dataType = "Integer")
    private Integer trialMode;

    @ApiModelProperty(value = "承办人", dataType = "Integer")
    private Integer undertaker;

    @ApiModelProperty(value = "案件进展阶段", dataType = "Integer")
    private Integer progressStage;

    @ApiModelProperty(value = "审判程序", dataType = "Integer")
    private Integer trialProcedure;

    @ApiModelProperty(value = "适用程序", dataType = "Integer")
    private Integer applicableProcedure;

    @ApiModelProperty(value = "收案日期", dataType = "Date")
    private Date receiveDate;

    @ApiModelProperty(value = "立案案由", dataType = "Integer")
    private Integer receiveCause;

    @ApiModelProperty(value = "结案日期", dataType = "Date")
    private Date closeDate;

    @ApiModelProperty(value = "结案案由", dataType = "Integer")
    private Integer closeCause;

    @ApiModelProperty(value = "创建时间", dataType = "Timestamp")
    private Timestamp createTime;

    @ApiModelProperty(value = "修改时间", dataType = "Timestamp")
    private Timestamp updateTime;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;
}