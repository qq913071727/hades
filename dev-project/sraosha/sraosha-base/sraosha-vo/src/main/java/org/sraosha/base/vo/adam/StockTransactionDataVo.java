package org.sraosha.base.vo.adam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * StockTransactionData的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "StockTransactionDataVo", description = "StockTransactionData的vo类")
public class StockTransactionDataVo {
    @ApiModelProperty(value = "最低价", dataType = "BigDecimal")
    private BigDecimal lowestPrice;

    @ApiModelProperty(value = "最高价", dataType = "BigDecimal")
    private BigDecimal highestPrice;

    @ApiModelProperty(value = "收盘价", dataType = "BigDecimal")
    private BigDecimal closePrice;

    @ApiModelProperty(value = "开盘价", dataType = "BigDecimal")
    private BigDecimal openPrice;

    @ApiModelProperty(value = "代码", dataType = "String")
    private String code;

    @ApiModelProperty(value = "日期", dataType = "Date")
    private Date date;

    @ApiModelProperty(value = "主键", dataType = "BigDecimal")
    private BigDecimal id;

    @ApiModelProperty(value = "HeiKinAshi最低价", dataType = "BigDecimal")
    private BigDecimal haLowestPrice;

    @ApiModelProperty(value = "HeiKinAshi最高价", dataType = "BigDecimal")
    private BigDecimal haHighestPrice;

    @ApiModelProperty(value = "HeiKinAshi收盘价", dataType = "BigDecimal")
    private BigDecimal haClosePrice;

    @ApiModelProperty(value = "HeiKinAshi开盘价", dataType = "BigDecimal")
    private BigDecimal haOpenPrice;

    @ApiModelProperty(value = "计算KD时的指标", dataType = "BigDecimal")
    private BigDecimal d;

    @ApiModelProperty(value = "计算KD时的指标", dataType = "BigDecimal")
    private BigDecimal k;

    @ApiModelProperty(value = "计算KD时的指标", dataType = "BigDecimal")
    private BigDecimal rsv;

    @ApiModelProperty(value = "股价250日内震荡幅度。计算方法为：将250日内涨跌百分比的绝对值相加，再除以250。单位：%", dataType = "BigDecimal")
    private BigDecimal twoHundredFifty;

    @ApiModelProperty(value = "股价20日内震荡幅度。计算方法为：将20日内涨跌百分比的绝对值相加，再除以20。单位：%", dataType = "BigDecimal")
    private BigDecimal twentyDayVolatility;

    @ApiModelProperty(value = "股价10日内震荡幅度。计算方法为：将10日内涨跌百分比的绝对值相加，再除以10。单位：%", dataType = "BigDecimal")
    private BigDecimal tenDayVolatility;

    @ApiModelProperty(value = "股价5日内震荡幅度。计算方法为：将5日内涨跌百分比的绝对值相加，再除以5。单位：%", dataType = "BigDecimal")
    private BigDecimal fiveDayVolatility;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    private BigDecimal dea;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    private BigDecimal dif;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    private BigDecimal ema26;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    private BigDecimal ema12;

    @ApiModelProperty(value = "250日均线", dataType = "BigDecimal")
    private BigDecimal ma250;

    @ApiModelProperty(value = "120日均线", dataType = "BigDecimal")
    private BigDecimal ma120;

    @ApiModelProperty(value = "60日均线", dataType = "BigDecimal")
    private BigDecimal ma60;

    @ApiModelProperty(value = "20日均线", dataType = "BigDecimal")
    private BigDecimal ma20;

    @ApiModelProperty(value = "10日均线", dataType = "BigDecimal")
    private BigDecimal ma10;

    @ApiModelProperty(value = "5日均线", dataType = "BigDecimal")
    private BigDecimal ma5;

    @ApiModelProperty(value = "成交笔数", dataType = "BigDecimal")
    private BigDecimal transactionNumber;

    @ApiModelProperty(value = "流通市值", dataType = "BigDecimal")
    private BigDecimal circulationMarketValue;

    @ApiModelProperty(value = "总市值", dataType = "BigDecimal")
    private BigDecimal totalMarketValue;

    @ApiModelProperty(value = "成交额", dataType = "BigDecimal")
    private BigDecimal turnover;

    @ApiModelProperty(value = "成交量", dataType = "BigDecimal")
    private BigDecimal volume;

    @ApiModelProperty(value = "换手率", dataType = "BigDecimal")
    private BigDecimal turnoverRate;

    @ApiModelProperty(value = "今日股价是否涨跌。1：涨，-1：跌，0：平", dataType = "BigDecimal")
    private BigDecimal upDown;

    @ApiModelProperty(value = "带除权的今日股价的涨跌幅。单位：%", dataType = "BigDecimal")
    private BigDecimal changeRangeExRight;

    @ApiModelProperty(value = "今日股价的涨跌幅。单位：%。注意：不包括除权。", dataType = "BigDecimal")
    private BigDecimal changeRange;

    @ApiModelProperty(value = "今日股价的涨跌额", dataType = "BigDecimal")
    private BigDecimal changeAmount;

    @ApiModelProperty(value = "前收盘", dataType = "BigDecimal")
    private BigDecimal lastClosePrice;

    @ApiModelProperty(value = "布林带下轨", dataType = "BigDecimal")
    private BigDecimal dn;

    @ApiModelProperty(value = "布林带中轨", dataType = "BigDecimal")
    private BigDecimal mb;

    @ApiModelProperty(value = "布林带上轨", dataType = "BigDecimal")
    private BigDecimal up;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;
}