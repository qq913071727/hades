package org.sraosha.base.vo.monitor;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * AreaDb的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AreaDbVo", description = "AreaDb的vo类")
public class AreaDbVo {
    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "省", dataType = "String")
    private String province;

    @ApiModelProperty(value = "市", dataType = "String")
    private String city;

    @ApiModelProperty(value = "区", dataType = "String")
    private String district;

    @ApiModelProperty(value = "地区标签", dataType = "String")
    private String areaTag;

    @ApiModelProperty(value = "地区坐标", dataType = "String")
    private String areaPoint;

    @ApiModelProperty(value = "插入时间", dataType = "String")
    private String insertTime;

    @ApiModelProperty(value = "备用字段1", dataType = "String")
    private String field1;

    @ApiModelProperty(value = "备用字段2", dataType = "String")
    private String field2;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;
}