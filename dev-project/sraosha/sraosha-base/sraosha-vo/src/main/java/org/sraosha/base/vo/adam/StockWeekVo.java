package org.sraosha.base.vo.adam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * StockWeek的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "StockWeekVo", description = "StockWeek的vo类")
public class StockWeekVo {
    @ApiModelProperty(value = "带除权的本周股价的涨跌幅。单位：%", dataType = "BigDecimal")
    private BigDecimal changeRangeExRight;

    @ApiModelProperty(value = "HeiKinAshi本周开盘价", dataType = "BigDecimal")
    private BigDecimal haWeekOpenPrice;

    @ApiModelProperty(value = "HeiKinAshi本周收盘价", dataType = "BigDecimal")
    private BigDecimal haWeekClosePrice;

    @ApiModelProperty(value = "HeiKinAshi本周最高价", dataType = "BigDecimal")
    private BigDecimal haWeekHighestPrice;

    @ApiModelProperty(value = "HeiKinAshi本周最低价", dataType = "BigDecimal")
    private BigDecimal haWeekLowestPrice;

    @ApiModelProperty(value = "表示主键", dataType = "BigDecimal")
    private BigDecimal id;

    @ApiModelProperty(value = "表示某一周的开始时间", dataType = "Date")
    private Date beginDate;

    @ApiModelProperty(value = "股票代码", dataType = "String")
    private String code;

    @ApiModelProperty(value = "1表示和前一天比较当天是上涨；0表示和前一天比较当天是不涨不跌；-1表示和前一天比较当天是下跌", dataType = "BigDecimal")
    private BigDecimal upDown;

    @ApiModelProperty(value = "表示某一周的结束时间", dataType = "Date")
    private Date endDate;

    @ApiModelProperty(value = "表示这条记录是这只股票的第多少个交易周", dataType = "BigDecimal")
    private BigDecimal number;

    @ApiModelProperty(value = "周线级别开盘价", dataType = "BigDecimal")
    private BigDecimal openPrice;

    @ApiModelProperty(value = "周线级别收盘价", dataType = "BigDecimal")
    private BigDecimal closePrice;

    @ApiModelProperty(value = "周线级别最高价", dataType = "BigDecimal")
    private BigDecimal highestPrice;

    @ApiModelProperty(value = "周线级别最低价", dataType = "BigDecimal")
    private BigDecimal lowestPrice;

    @ApiModelProperty(value = "周线级别成交量", dataType = "BigDecimal")
    private BigDecimal volume;

    @ApiModelProperty(value = "周线级别成交额", dataType = "BigDecimal")
    private BigDecimal turnover;

    @ApiModelProperty(value = "5日均线", dataType = "BigDecimal")
    private BigDecimal ma5;

    @ApiModelProperty(value = "10日均线", dataType = "BigDecimal")
    private BigDecimal ma10;

    @ApiModelProperty(value = "20日均线", dataType = "BigDecimal")
    private BigDecimal ma20;

    @ApiModelProperty(value = "60日均线", dataType = "BigDecimal")
    private BigDecimal ma60;

    @ApiModelProperty(value = "120日均线", dataType = "BigDecimal")
    private BigDecimal ma120;

    @ApiModelProperty(value = "250日均线", dataType = "BigDecimal")
    private BigDecimal ma250;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    private BigDecimal dif;

    @ApiModelProperty(value = "计算KD时的指标", dataType = "BigDecimal")
    private BigDecimal rsv;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    private BigDecimal dea;

    @ApiModelProperty(value = "布林带下轨", dataType = "BigDecimal")
    private BigDecimal dn;

    @ApiModelProperty(value = "计算KD时的指标", dataType = "BigDecimal")
    private BigDecimal k;

    @ApiModelProperty(value = "计算KD时的指标", dataType = "BigDecimal")
    private BigDecimal d;

    @ApiModelProperty(value = "布林带中轨", dataType = "BigDecimal")
    private BigDecimal mb;

    @ApiModelProperty(value = "布林带上轨", dataType = "BigDecimal")
    private BigDecimal up;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    private BigDecimal ema12;

    @ApiModelProperty(value = "计算MACD时的指标", dataType = "BigDecimal")
    private BigDecimal ema26;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;
}