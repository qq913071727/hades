package org.sraosha.base.mapper.adam;

import org.sraosha.base.mapper.EasyBaseMapper;
import org.sraosha.base.model.adam.StockWeek;
import org.springframework.stereotype.Repository;

/**
 * StockWeek的mapper类
 */
@Repository
public interface StockWeekMapper extends EasyBaseMapper<StockWeek> {

}