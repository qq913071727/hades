package org.sraosha.base.mapper.adam;

import org.sraosha.base.mapper.EasyBaseMapper;
import org.sraosha.base.model.adam.StockTransactionData;
import org.springframework.stereotype.Repository;

/**
 * StockTransactionData的mapper类
 */
@Repository
public interface StockTransactionDataMapper extends EasyBaseMapper<StockTransactionData> {

}