package org.sraosha.base.mapper.monitor;

import org.sraosha.base.mapper.EasyBaseMapper;
import org.sraosha.base.model.monitor.AreaDb;
import org.springframework.stereotype.Repository;

/**
 * AreaDb的mapper类
 */
@Repository
public interface AreaDbMapper extends EasyBaseMapper<AreaDb> {

}