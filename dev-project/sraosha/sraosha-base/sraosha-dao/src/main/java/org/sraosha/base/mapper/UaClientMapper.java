package org.sraosha.base.mapper;

import org.sraosha.base.model.UaClient;
import org.springframework.stereotype.Repository;

/**
 * UaClient的mapper类
 */
@Repository
public interface UaClientMapper extends EasyBaseMapper<UaClient> {

}