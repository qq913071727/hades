package org.sraosha.base.mapper;

import org.sraosha.base.model.UserLoginLog;
import org.springframework.stereotype.Repository;

/**
 * UserLoginLog的mapper类
 */
@Repository
public interface UserLoginLogMapper extends EasyBaseMapper<UserLoginLog> {

}