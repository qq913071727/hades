package org.sraosha.base.mapper.monitor;

import org.sraosha.base.mapper.EasyBaseMapper;
import org.sraosha.base.model.monitor.Case;
import org.springframework.stereotype.Repository;

/**
 * Case的mapper类
 */
@Repository
public interface CaseMapper extends EasyBaseMapper<Case> {

}