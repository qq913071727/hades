package org.sraosha.base.service.adam;

import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.base.model.adam.StockTransactionData;

import com.github.pagehelper.PageInfo;

import java.io.Serializable;
import java.util.List;

/**
 * StockTransactionData的service接口
 */
public interface StockTransactionDataService {

    /**
     * 新增
     * @param stockTransactionData 交易记录表（部分数据）
     * @return 添加成功返回1，添加失败返回-1
     */
    ServiceResult<Integer> add(StockTransactionData stockTransactionData);

    /**
     * 批量导入
     * @param stockTransactionDataList 交易记录表（部分数据）（列表）
     * @return 批量导入成功返回1，批量导入失败返回-1
     */
    ServiceResult<Integer> batchAdd(List<StockTransactionData> stockTransactionDataList);

    /**
     * 更新
     * @param stockTransactionData 交易记录表（部分数据）
     * @return 更新成功返回1，更新失败返回-1
     */
    ServiceResult<Integer> updateForId(StockTransactionData stockTransactionData);

    /**
     * 删除
     * @param id 主键
     * @return 删除成功返回1，删除失败返回-1
     */
    ServiceResult<Integer> deleteById(Serializable id);

    /**
     * 根据id查询
     * @param id 主键
     * @return 返回查询结果
     */
    ServiceResult<StockTransactionData> selectById(Serializable id);

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param stockTransactionData 交易记录表（部分数据）
     * @param pageNo 页号
     * @param pageSize 每页行数
     * @return 返回查询结果列表
     */
    ServiceResult<PageInfo<StockTransactionData>> page(StockTransactionData stockTransactionData, int pageNo, int pageSize);
}