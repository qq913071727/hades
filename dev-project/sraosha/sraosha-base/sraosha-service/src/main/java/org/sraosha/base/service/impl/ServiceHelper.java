package org.sraosha.base.service.impl;

import org.sraosha.base.mapper.*;
import org.sraosha.base.vo.*;
import org.sraosha.framework.constants.RedisStorage;
import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.framework.enumeration.ApiResultEnum;
import org.sraosha.framework.manager.RabbitMqManager;
import org.sraosha.framework.manager.RedisManager;
import org.sraosha.framework.manager.ThreadPoolManager;
import org.sraosha.framework.util.LocalUtils;
import org.sraosha.framework.enumeration.BooleanEnum;
import org.sraosha.base.pojo.constants.RedisKey;
import org.sraosha.base.thread.SaveUaClientInRedisThread;
import org.sraosha.base.value.RedisParamConfig;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
//import sonicom.csrpkg.CertHandler;
//import sonicom.csrpkg.CertModule;

import javax.annotation.Resource;
import java.util.*;

@Data
@Slf4j
@Service
@Configuration
public class ServiceHelper {
    @Autowired
    private RedisParamConfig redisParamConfig;

    @Resource
    private ThreadPoolManager threadPoolManager;

    @Resource
    private RedisManager redisManager;

    @Resource
    private RabbitMqManager rabbitMqManager;

    @Resource
    private UaClientMapper uaClientMapper;

    /**
     * 将client表中的数据保存到redis中
     */
    public void saveUaClientInRedis() {
        log.info("系统启动时，将client表中的记录存储到redis中");

        // 将client表中的数据保存到redis中
        threadPoolManager.submit(new SaveUaClientInRedisThread(redisManager, redisParamConfig, uaClientMapper));
    }

}
