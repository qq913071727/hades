package org.sraosha.base.service.adam.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.sraosha.base.mapper.adam.StockWeekMapper;
import org.sraosha.base.model.adam.StockWeek;
import org.sraosha.base.service.adam.StockWeekService;
import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.framework.enumeration.ServiceResultEnum;

import java.io.Serializable;
import java.util.List;

/**
 * StockWeekService接口的实现类
 */
@Slf4j
@Service
public class StockWeekServiceImpl extends ServiceImpl<StockWeekMapper, StockWeek> implements StockWeekService {

    /**
     * 新增
     * @param stockWeek 周线级别交易数据
     * @return 添加成功返回1，添加失败返回-1
     */
    @Override
    public ServiceResult<Integer> add(StockWeek stockWeek) {
        baseMapper.insert(stockWeek);
        return new ServiceResult<>(ServiceResultEnum.INSERT_SUCCESS.getCode(), Boolean.TRUE, ServiceResultEnum.INSERT_SUCCESS.getMessage());
    }

    /**
     * 批量导入
     * @param stockWeekList 周线级别交易数据（列表）
     * @return 批量导入成功返回1，批量导入失败返回-1
     */
    @Override
    public ServiceResult<Integer> batchAdd(List<StockWeek> stockWeekList) {
        baseMapper.insertBatchSomeColumn(stockWeekList);
        return new ServiceResult<>(ServiceResultEnum.INSERT_SUCCESS.getCode(), Boolean.TRUE, ServiceResultEnum.INSERT_SUCCESS.getMessage());
    }

    /**
     * 根据id更新
     * @param stockWeek 周线级别交易数据
     * @return 更新成功返回1，更新失败返回-1
     */
    @Override
    public ServiceResult<Integer> updateForId(StockWeek stockWeek) {
        baseMapper.updateById(stockWeek);
        return new ServiceResult<>(ServiceResultEnum.UPDATE_SUCCESS.getCode(), Boolean.TRUE, ServiceResultEnum.UPDATE_SUCCESS.getMessage());
    }

    /**
     * 根据id删除
     * @param id 主键
     * @return 删除成功返回1，删除失败返回-1
     */
    @Override
    public ServiceResult<Integer> deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return new ServiceResult<>(ServiceResultEnum.DELETE_SUCCESS.getCode(), Boolean.TRUE, ServiceResultEnum.DELETE_SUCCESS.getMessage());
    }

    /**
     * 根据id查询
     * @param id 主键
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<StockWeek> selectById(Serializable id) {
        StockWeek stockWeek = baseMapper.selectById(id);
        return new ServiceResult<>(ServiceResultEnum.SELECT_SUCCESS.getCode(), stockWeek, Boolean.TRUE, ServiceResultEnum.SELECT_SUCCESS.getMessage());
    }

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param stockWeek 周线级别交易数据
     * @param pageNo 页号
     * @param pageSize 每页行数
     * @return 返回查询结果列表
     */
    @Override
    public ServiceResult<PageInfo<StockWeek>> page(StockWeek stockWeek, int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        LambdaQueryWrapper<StockWeek> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        List<StockWeek> stockWeekList = baseMapper.selectList(lambdaQueryWrapper);
        return new ServiceResult<>(ServiceResultEnum.SELECT_SUCCESS.getCode(), new PageInfo<>(stockWeekList), Boolean.TRUE, ServiceResultEnum.SELECT_SUCCESS.getMessage());
    }
}
