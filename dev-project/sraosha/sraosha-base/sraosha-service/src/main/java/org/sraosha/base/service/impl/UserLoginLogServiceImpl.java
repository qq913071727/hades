package org.sraosha.base.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.sraosha.base.mapper.UserLoginLogMapper;
import org.sraosha.base.model.UserLoginLog;
import org.sraosha.base.service.UserLoginLogService;
import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.framework.enumeration.ServiceResultEnum;

import java.io.Serializable;
import java.util.List;

/**
 * UserLoginLogService接口的实现类
 */
@Slf4j
@Service
public class UserLoginLogServiceImpl extends ServiceImpl<UserLoginLogMapper, UserLoginLog> implements UserLoginLogService {

    /**
     * 新增
     * @param userLoginLog 用户登录日志表
     * @return 添加成功返回1，添加失败返回-1
     */
    @Override
    public ServiceResult<Integer> add(UserLoginLog userLoginLog) {
        baseMapper.insert(userLoginLog);
        return new ServiceResult<>(ServiceResultEnum.INSERT_SUCCESS.getCode(), Boolean.TRUE, ServiceResultEnum.INSERT_SUCCESS.getMessage());
    }

    /**
     * 批量导入
     * @param userLoginLogList 用户登录日志表（列表）
     * @return 批量导入成功返回1，批量导入失败返回-1
     */
    @Override
    public ServiceResult<Integer> batchAdd(List<UserLoginLog> userLoginLogList) {
        baseMapper.insertBatchSomeColumn(userLoginLogList);
        return new ServiceResult<>(ServiceResultEnum.INSERT_SUCCESS.getCode(), Boolean.TRUE, ServiceResultEnum.INSERT_SUCCESS.getMessage());
    }

    /**
     * 根据id更新
     * @param userLoginLog 用户登录日志表
     * @return 更新成功返回1，更新失败返回-1
     */
    @Override
    public ServiceResult<Integer> updateForId(UserLoginLog userLoginLog) {
        baseMapper.updateById(userLoginLog);
        return new ServiceResult<>(ServiceResultEnum.UPDATE_SUCCESS.getCode(), Boolean.TRUE, ServiceResultEnum.UPDATE_SUCCESS.getMessage());
    }

    /**
     * 根据id删除
     * @param id 主键
     * @return 删除成功返回1，删除失败返回-1
     */
    @Override
    public ServiceResult<Integer> deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return new ServiceResult<>(ServiceResultEnum.DELETE_SUCCESS.getCode(), Boolean.TRUE, ServiceResultEnum.DELETE_SUCCESS.getMessage());
    }

    /**
     * 根据id查询
     * @param id 主键
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<UserLoginLog> selectById(Serializable id) {
        UserLoginLog userLoginLog = baseMapper.selectById(id);
        return new ServiceResult<>(ServiceResultEnum.SELECT_SUCCESS.getCode(), userLoginLog, Boolean.TRUE, ServiceResultEnum.SELECT_SUCCESS.getMessage());
    }

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param userLoginLog 用户登录日志表
     * @param pageNo 页号
     * @param pageSize 每页行数
     * @return 返回查询结果列表
     */
    @Override
    public ServiceResult<PageInfo<UserLoginLog>> page(UserLoginLog userLoginLog, int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        LambdaQueryWrapper<UserLoginLog> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        List<UserLoginLog> userLoginLogList = baseMapper.selectList(lambdaQueryWrapper);
        return new ServiceResult<>(ServiceResultEnum.SELECT_SUCCESS.getCode(), new PageInfo<>(userLoginLogList), Boolean.TRUE, ServiceResultEnum.SELECT_SUCCESS.getMessage());
    }
}
