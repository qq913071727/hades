package org.sraosha.base.service;

import com.github.pagehelper.PageInfo;
import org.sraosha.base.model.UaClient;
import org.sraosha.framework.dto.ServiceResult;

import java.io.Serializable;
import java.util.List;

/**
 * UaClient的service接口
 */
public interface UaClientService {

    /**
     * 新增
     * @param uaClient client表
     * @return 添加成功返回1，添加失败返回-1
     */
    ServiceResult<Integer> add(UaClient uaClient);

    /**
     * 批量导入
     * @param uaClientList client表（列表）
     * @return 批量导入成功返回1，批量导入失败返回-1
     */
    ServiceResult<Integer> batchAdd(List<UaClient> uaClientList);

    /**
     * 更新
     * @param uaClient client表
     * @return 更新成功返回1，更新失败返回-1
     */
    ServiceResult<Integer> updateForId(UaClient uaClient);

    /**
     * 删除
     * @param id 主键
     * @return 删除成功返回1，删除失败返回-1
     */
    ServiceResult<Integer> deleteById(Serializable id);

    /**
     * 根据id查询
     * @param id 主键
     * @return 返回查询结果
     */
    ServiceResult<UaClient> selectById(Serializable id);

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param uaClient client表
     * @param pageNo 页号
     * @param pageSize 每页行数
     * @return 返回查询结果列表
     */
    ServiceResult<PageInfo<UaClient>> page(UaClient uaClient, int pageNo, int pageSize);
}