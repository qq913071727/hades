package org.sraosha.base.service.monitor.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.sraosha.base.mapper.monitor.CaseMapper;
import org.sraosha.base.model.monitor.Case;
import org.sraosha.base.service.monitor.CaseService;
import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.framework.enumeration.ServiceResultEnum;

import java.io.Serializable;
import java.util.List;

/**
 * CaseService接口的实现类
 */
@Slf4j
@Service
public class CaseServiceImpl extends ServiceImpl<CaseMapper, Case> implements CaseService {

    /**
     * 新增
     * @param _case 案件信息
     * @return 添加成功返回1，添加失败返回-1
     */
    @Override
    public ServiceResult<Integer> add(Case _case) {
        baseMapper.insert(_case);
        return new ServiceResult<>(ServiceResultEnum.INSERT_SUCCESS.getCode(), Boolean.TRUE, ServiceResultEnum.INSERT_SUCCESS.getMessage());
    }

    /**
     * 批量导入
     * @param _caseList 案件信息（列表）
     * @return 批量导入成功返回1，批量导入失败返回-1
     */
    @Override
    public ServiceResult<Integer> batchAdd(List<Case> _caseList) {
        baseMapper.insertBatchSomeColumn(_caseList);
        return new ServiceResult<>(ServiceResultEnum.INSERT_SUCCESS.getCode(), Boolean.TRUE, ServiceResultEnum.INSERT_SUCCESS.getMessage());
    }

    /**
     * 根据id更新
     * @param _case 案件信息
     * @return 更新成功返回1，更新失败返回-1
     */
    @Override
    public ServiceResult<Integer> updateForId(Case _case) {
        baseMapper.updateById(_case);
        return new ServiceResult<>(ServiceResultEnum.UPDATE_SUCCESS.getCode(), Boolean.TRUE, ServiceResultEnum.UPDATE_SUCCESS.getMessage());
    }

    /**
     * 根据id删除
     * @param id 主键
     * @return 删除成功返回1，删除失败返回-1
     */
    @Override
    public ServiceResult<Integer> deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return new ServiceResult<>(ServiceResultEnum.DELETE_SUCCESS.getCode(), Boolean.TRUE, ServiceResultEnum.DELETE_SUCCESS.getMessage());
    }

    /**
     * 根据id查询
     * @param id 主键
     * @return 返回查询结果
     */
    @Override
    public ServiceResult<Case> selectById(Serializable id) {
        Case _case = baseMapper.selectById(id);
        return new ServiceResult<>(ServiceResultEnum.SELECT_SUCCESS.getCode(), _case, Boolean.TRUE, ServiceResultEnum.SELECT_SUCCESS.getMessage());
    }

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param _case 案件信息
     * @param pageNo 页号
     * @param pageSize 每页行数
     * @return 返回查询结果列表
     */
    @Override
    public ServiceResult<PageInfo<Case>> page(Case _case, int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        LambdaQueryWrapper<Case> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        List<Case> _caseList = baseMapper.selectList(lambdaQueryWrapper);
        return new ServiceResult<>(ServiceResultEnum.SELECT_SUCCESS.getCode(), new PageInfo<>(_caseList), Boolean.TRUE, ServiceResultEnum.SELECT_SUCCESS.getMessage());
    }
}
