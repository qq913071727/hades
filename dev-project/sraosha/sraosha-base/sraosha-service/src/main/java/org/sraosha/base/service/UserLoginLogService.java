package org.sraosha.base.service;

import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.base.model.UserLoginLog;

import com.github.pagehelper.PageInfo;

import java.io.Serializable;
import java.util.List;

/**
 * UserLoginLog的service接口
 */
public interface UserLoginLogService {

    /**
     * 新增
     * @param userLoginLog 用户登录日志表
     * @return 添加成功返回1，添加失败返回-1
     */
    ServiceResult<Integer> add(UserLoginLog userLoginLog);

    /**
     * 批量导入
     * @param userLoginLogList 用户登录日志表（列表）
     * @return 批量导入成功返回1，批量导入失败返回-1
     */
    ServiceResult<Integer> batchAdd(List<UserLoginLog> userLoginLogList);

    /**
     * 更新
     * @param userLoginLog 用户登录日志表
     * @return 更新成功返回1，更新失败返回-1
     */
    ServiceResult<Integer> updateForId(UserLoginLog userLoginLog);

    /**
     * 删除
     * @param id 主键
     * @return 删除成功返回1，删除失败返回-1
     */
    ServiceResult<Integer> deleteById(Serializable id);

    /**
     * 根据id查询
     * @param id 主键
     * @return 返回查询结果
     */
    ServiceResult<UserLoginLog> selectById(Serializable id);

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param userLoginLog 用户登录日志表
     * @param pageNo 页号
     * @param pageSize 每页行数
     * @return 返回查询结果列表
     */
    ServiceResult<PageInfo<UserLoginLog>> page(UserLoginLog userLoginLog, int pageNo, int pageSize);
}