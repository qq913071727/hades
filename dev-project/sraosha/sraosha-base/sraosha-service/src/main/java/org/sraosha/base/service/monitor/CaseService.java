package org.sraosha.base.service.monitor;

import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.base.model.monitor.Case;

import com.github.pagehelper.PageInfo;

import java.io.Serializable;
import java.util.List;

/**
 * Case的service接口
 */
public interface CaseService {

    /**
     * 新增
     * @param _case 案件信息
     * @return 添加成功返回1，添加失败返回-1
     */
    ServiceResult<Integer> add(Case _case);

    /**
     * 批量导入
     * @param _caseList 案件信息（列表）
     * @return 批量导入成功返回1，批量导入失败返回-1
     */
    ServiceResult<Integer> batchAdd(List<Case> _caseList);

    /**
     * 更新
     * @param _case 案件信息
     * @return 更新成功返回1，更新失败返回-1
     */
    ServiceResult<Integer> updateForId(Case _case);

    /**
     * 删除
     * @param id 主键
     * @return 删除成功返回1，删除失败返回-1
     */
    ServiceResult<Integer> deleteById(Serializable id);

    /**
     * 根据id查询
     * @param id 主键
     * @return 返回查询结果
     */
    ServiceResult<Case> selectById(Serializable id);

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param _case 案件信息
     * @param pageNo 页号
     * @param pageSize 每页行数
     * @return 返回查询结果列表
     */
    ServiceResult<PageInfo<Case>> page(Case _case, int pageNo, int pageSize);
}