package org.sraosha.base.service.monitor;

import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.base.model.monitor.AreaDb;

import com.github.pagehelper.PageInfo;

import java.io.Serializable;
import java.util.List;

/**
 * AreaDb的service接口
 */
public interface AreaDbService {

    /**
     * 新增
     * @param areaDb 地区表
     * @return 添加成功返回1，添加失败返回-1
     */
    ServiceResult<Integer> add(AreaDb areaDb);

    /**
     * 批量导入
     * @param areaDbList 地区表（列表）
     * @return 批量导入成功返回1，批量导入失败返回-1
     */
    ServiceResult<Integer> batchAdd(List<AreaDb> areaDbList);

    /**
     * 更新
     * @param areaDb 地区表
     * @return 更新成功返回1，更新失败返回-1
     */
    ServiceResult<Integer> updateForId(AreaDb areaDb);

    /**
     * 删除
     * @param id 主键
     * @return 删除成功返回1，删除失败返回-1
     */
    ServiceResult<Integer> deleteById(Serializable id);

    /**
     * 根据id查询
     * @param id 主键
     * @return 返回查询结果
     */
    ServiceResult<AreaDb> selectById(Serializable id);

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param areaDb 地区表
     * @param pageNo 页号
     * @param pageSize 每页行数
     * @return 返回查询结果列表
     */
    ServiceResult<PageInfo<AreaDb>> page(AreaDb areaDb, int pageNo, int pageSize);
}