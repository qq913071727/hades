package org.sraosha.base.pojo.constants;

/**
 * 多数据源常量
 */
public class MultipleDataSource {

    /**
     * sraosha_auth数据库
     */
    public static final String SRAOSHA_AUTH = "sraosha-auth";

    /**
     * sraosha数据库
     */
    public static final String SRAOSHA = "sraosha";

}
