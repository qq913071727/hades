package org.sraosha.base.pojo.constants;

/**
 * redis的key的前缀
 */
public class RedisKey {

    /**
     * sraosha
     */
    public static final String SRAOSHA = "sraosha";

    /**
     * 服务的全局统一id
     */
    public static final String UNI_CODE = "uniCode";

    /**
     * client表
     */
    public static final String CLIENT = "client";
}
