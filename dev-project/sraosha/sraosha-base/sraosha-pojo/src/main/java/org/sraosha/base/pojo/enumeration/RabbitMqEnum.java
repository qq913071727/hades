package org.sraosha.base.pojo.enumeration;

/**
 * rabbitmq配置信息的枚举类
 */
public enum RabbitMqEnum {

    // kmc系统向sraosha系统同步psk数据
    KMC_PSK("sraosha_phos", "kmc.psk.to.sraosha", "kmc.psk.to.sraosha");

    private String exchange;
    private String queue;
    private String routingKey;

    RabbitMqEnum(String exchange, String queue, String routingKey) {
        this.exchange = exchange;
        this.queue = queue;
        this.routingKey = routingKey;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }
}
