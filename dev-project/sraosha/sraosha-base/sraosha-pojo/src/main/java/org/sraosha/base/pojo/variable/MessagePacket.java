package org.sraosha.base.pojo.variable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * rabbitmq的消息对象
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MessagePacket", description = "调用KMC的/id/sign接口的返回值类")
public class MessagePacket {

    @ApiModelProperty(value = "类型，1表示kmc系统向sraosha同步的psk", dataType = "Integer")
    private Integer type;

    @ApiModelProperty(value = "数据", dataType = "Object")
    private Object data;

    @ApiModelProperty(value = "描述", dataType = "String")
    private String description;
}
