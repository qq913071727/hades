//package org.sraosha.microservice.gateway.config;
//
//import org.sraosha.base.pojo.constants.MultipleDataSource;
//import lombok.extern.slf4j.Slf4j;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.core.annotation.Order;
//import org.springframework.stereotype.Component;
//
//@Component
//@Order(value = -100)
//@Aspect
//@Slf4j
//public class DataSourceAspect {
//
//    @Pointcut("execution(* org.sraosha.base.service.adam.impl..*.*(..))")
//    private void adamAspect() {
//    }
//
//    @Pointcut("execution(* org.sraosha.base.service.monitor.impl..*.*(..))")
//    private void monitorAspect() {
//    }
//
//    @Before("adamAspect()")
//    public void adam() {
//        log.debug("切换到{} 数据源...", MultipleDataSource.ADAM);
//        DynamicDataSourceContextHolder.setDataSourceKey(MultipleDataSource.ADAM);
//    }
//
//    @Before("monitorAspect()")
//    public void monitor() {
//        log.debug("切换到{} 数据源...", MultipleDataSource.MONITOR);
//        DynamicDataSourceContextHolder.setDataSourceKey(MultipleDataSource.MONITOR);
//    }
//
//}
