package org.sraosha.microservice.gateway.filter;

import org.sraosha.framework.constants.RedisStorage;
import org.sraosha.microservice.gateway.constants.RedisKey;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * 1.允许访问的路径，直接放行
 * 2.header中必须有token，并且在redis中存在
 * 3.header中必须有clientId和clientSecret，并且在redis中存在
 */
@Component
@Slf4j
public class Oauth2Filter extends AbstractFilter implements GlobalFilter, Ordered {

    /**
     * 判断当前路径是否需要过滤器处理
     *
     * @param exchange
     * @param chain
     * @return
     */
    @SneakyThrows
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        return super.handle("sraosha-oauth2", exchange, chain);
    }

    /**
     * 允许访问的路径，直接放行
     *
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Boolean checkAllowPathList(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        List<String> allowPathList = filterParamValue.getOauth2AllowPathList();
        if (null != allowPathList && allowPathList.size() != 0) {
            String path = request.getPath().toString();
            if (allowPathList.contains(path)) {
                log.info("接口{}是允许直接访问的接口，直接放行", path);
                return true;
            }
        }
        return false;
    }

    /**
     * header中必须有token，并且在redis中存在
     *
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Boolean checkToken(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        HttpHeaders httpHeaders = request.getHeaders();
        List<String> tokenList = httpHeaders.get("token");
        if (null != tokenList && tokenList.size() > 0) {
            Boolean hasTokenInRedis = false;
            String tokenInHeader = null;
            for (int i = 0; i < tokenList.size(); i++) {
                tokenInHeader = tokenList.get(i);
                // 从redis获取用户信息
                StringBuffer key = new StringBuffer().append(RedisKey.SRAOSHA_MICROSERVICE).append(RedisStorage.COLON).append(RedisKey.SRAOSHA_OAUTH2)
                        .append(RedisStorage.COLON).append(RedisStorage.TOKEN_PREFIX)
                        .append(tokenInHeader);
                String tokenInRedis = String.valueOf(redisManager.get(key.toString()));
                if (null != tokenInRedis) {
                    hasTokenInRedis = true;
                    break;
                }
            }
            if (!hasTokenInRedis) {
                log.warn("header中的token为【{}】，但是在redis中没有对应的token", tokenInHeader);
                return false;
            }
        } else {
            log.warn("header中没有token");
            return false;
        }
        return true;
    }

    /**
     * header中必须有clientId和clientSecret，并且在redis中存在
     *
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Boolean checkClient(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        HttpHeaders httpHeaders = request.getHeaders();

        List<String> clientIdInHeaderList = httpHeaders.get("clientId");
        if (null == clientIdInHeaderList || clientIdInHeaderList.size() == 0) {
            log.warn("header中没有clientId");
            return false;
        }
        List<String> clientSecretInHeaderList = httpHeaders.get("clientSecret");
        if (null == clientSecretInHeaderList || clientSecretInHeaderList.size() == 0) {
            log.warn("header中没有clientSecret");
            return false;
        }

        // 从redis获取sraosha-microservice:sraosha-oauth2:auth-client
        StringBuffer key = new StringBuffer().append(RedisKey.SRAOSHA_MICROSERVICE).append(RedisStorage.COLON).append(RedisKey.SRAOSHA_OAUTH2)
                .append(RedisStorage.COLON).append(RedisKey.AUTH_CLIENT);
        String authClientInRedisString = String.valueOf(redisManager.get(key.toString()));
        JSONArray authClientInRedisJsonArray = JSONObject.parseArray(authClientInRedisString);
        if (null == authClientInRedisJsonArray || authClientInRedisJsonArray.size() == 0) {
            log.warn("redis中没有{}", key);
            return false;
        }

        Boolean clientIdIsSame = false;
        String clientIdInHeader = null;
        String clientIdInRedis = null;
        for (int i = 0; i < clientIdInHeaderList.size(); i++) {
            clientIdInHeader = clientIdInHeaderList.get(i);
            for (int j = 0; j < authClientInRedisJsonArray.size(); j++) {
                JSONObject authClientInRedisJsonObject = (JSONObject) authClientInRedisJsonArray.get(j);
                clientIdInRedis = authClientInRedisJsonObject.getString("clientId");
                if (clientIdInHeader.equals(clientIdInRedis)) {
                    clientIdIsSame = true;
                    break;
                }
            }
            if (clientIdIsSame) {
                break;
            }
        }
        if (!clientIdIsSame) {
            log.warn("header中的clientId{}和redis中的clientId不同", clientIdInHeader, clientIdInRedis);
            return false;
        }

        Boolean clientSecretIsSame = false;
        String clientSecretInHeader = null;
        String clientSecretInRedis = null;
        for (int i = 0; i < clientSecretInHeaderList.size(); i++) {
            clientSecretInHeader = clientSecretInHeaderList.get(i);
            for (int j = 0; j < authClientInRedisJsonArray.size(); j++) {
                JSONObject authClientInRedisJsonObject = (JSONObject) authClientInRedisJsonArray.get(j);
                clientSecretInRedis = authClientInRedisJsonObject.getString("clientSecret");
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                if (passwordEncoder.matches(clientSecretInHeader, clientSecretInRedis)) {
                    clientSecretIsSame = true;
                    break;
                }
            }
            if (clientSecretIsSame) {
                break;
            }
        }
        if (!clientSecretIsSame) {
            log.warn("header中的clientSecret{}和redis中的clientSecret不同", clientSecretInHeader, clientSecretInRedis);
            return false;
        }
        return true;
    }

    @Override
    public int getOrder() {
        return 0;
    }
}