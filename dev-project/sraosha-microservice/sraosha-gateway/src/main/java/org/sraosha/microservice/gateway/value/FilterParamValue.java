package org.sraosha.microservice.gateway.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 过滤器参数配置类
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "filter")
public class FilterParamValue {
    /**
     * sraosha-oauth2认证接口
     */
    private List<String> oauth2AllowPathList;

    /**
     * sraosha接口
     */
    private List<String> uaAllowPathList;
}