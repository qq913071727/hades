package org.sraosha.microservice.gateway.constants;

/**
 * redis的key的前缀
 */
public class RedisKey {

    /************************************************** sraosha-microservice ****************************************************/
    /**
     * sraosha-microservice
     */
    public static final String SRAOSHA_MICROSERVICE = "sraosha-microservice";

    /**
     * sraosha-oauth2
     */
    public static final String SRAOSHA_OAUTH2 = "sraosha-oauth2";

    /**
     * auth_client表
     */
    public static final String AUTH_CLIENT = "auth-client";

    /**
     * 验证码
     */
    public static final String VERIFICATION_CODE = "verification-code";

    /************************************************** sraosha ****************************************************/
    /**
     * sraosha-web
     */
    public static final String SRAOSHA_WEB = "sraosha-web";

    /**
     * client表
     */
    public static final String CLIENT = "client";
}
