package org.sraosha.microservice.gateway;

import org.sraosha.framework.helper.LogbackHelper;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

//@SpringBootApplication(scanBasePackages = "{org.sraosha.microservice.gateway, " +
//        "org.sraosha.microservice.gateway.config, " +
//        "org.sraosha.microservice.gateway.value, " +
//        "org.sraosha.base.value}")
@SpringBootApplication(scanBasePackages = "org.sraosha.microservice")
//@MapperScan("org.sraosha.base.mapper")
@EnableDiscoveryClient
@EnableAutoConfiguration(exclude = {DruidDataSourceAutoConfigure.class})
@Slf4j
public class SraoshaGatewayApplication {

    public static void main(String[] args) {
        //设置日志端口
        LogbackHelper.setLogbackPort(args);

        SpringApplication.run(SraoshaGatewayApplication.class, args);
        log.info("sraosha-gateway服务启动成功");
    }

}
