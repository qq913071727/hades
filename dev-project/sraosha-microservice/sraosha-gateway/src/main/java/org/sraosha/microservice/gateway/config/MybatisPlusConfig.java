//package org.sraosha.microservice.gateway.config;
//
//import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
//import com.baomidou.mybatisplus.core.MybatisConfiguration;
//import com.baomidou.mybatisplus.core.config.GlobalConfig;
//import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
//import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.apache.ibatis.type.JdbcType;
//import org.mybatis.spring.annotation.MapperScan;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//import org.sraosha.base.pojo.constants.MultipleDataSource;
//
//import javax.sql.DataSource;
//import java.util.HashMap;
//import java.util.Map;
//
//@EnableTransactionManagement
//@Configuration
//@MapperScan("org.sraosha.base.mapper")
//public class MybatisPlusConfig {
//
//    //需要注入的Bean
//    @Bean
//    public EasySqlInjector easySqlInjector() {
//        return new EasySqlInjector();
//    }
//
//    @Bean
//    public PaginationInterceptor paginationInterceptor() {
//        return new PaginationInterceptor();
//    }
//
//    @Bean(name = MultipleDataSource.ADAM)
//    @ConfigurationProperties(prefix = "spring.datasource.adam")
//    public DataSource adam() {
//        return DruidDataSourceBuilder.create().build();
//    }
//
//    @Bean(name = MultipleDataSource.MONITOR)
//    @ConfigurationProperties(prefix = "spring.datasource.monitor")
//    public DataSource monitor() {
//        return DruidDataSourceBuilder.create().build();
//    }
//
//    /**
//     * 动态数据源配置
//     *
//     * @return
//     */
//    @Bean
//    @Primary
//    public DataSource multipleDataSource(@Qualifier(MultipleDataSource.ADAM) DataSource adam,
//                                         @Qualifier(MultipleDataSource.MONITOR) DataSource monitor) {
//        DynamicDataSource dynamicDataSource = new DynamicDataSource();
//        Map<Object, Object> targetDataSources = new HashMap<Object, Object>();
//        targetDataSources.put(MultipleDataSource.ADAM, adam);
//        targetDataSources.put(MultipleDataSource.MONITOR, monitor);
//        dynamicDataSource.setTargetDataSources(targetDataSources);
//        // 程序默认数据源，这个要根据程序调用数据源频次，经常把常调用的数据源作为默认
//        dynamicDataSource.setDefaultTargetDataSource(adam());
//        return dynamicDataSource;
//    }
//
//    @Bean("sqlSessionFactory")
//    public SqlSessionFactory sqlSessionFactory() throws Exception {
//        MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
//        sqlSessionFactory.setDataSource(multipleDataSource(adam(), monitor()));
//
//        MybatisConfiguration configuration = new MybatisConfiguration();
//        configuration.setJdbcTypeForNull(JdbcType.NULL);
//        configuration.setMapUnderscoreToCamelCase(true);
//        configuration.setCacheEnabled(false);
//        sqlSessionFactory.setConfiguration(configuration);
//        sqlSessionFactory.setGlobalConfig(globalConfiguration());
//        // 乐观锁插件
//        //PerformanceInterceptor(),OptimisticLockerInterceptor()
//        // 分页插件
//        sqlSessionFactory.setPlugins(paginationInterceptor());
//        return sqlSessionFactory.getObject();
//    }
//
//    @Bean
//    public GlobalConfig globalConfiguration() {
//        GlobalConfig conf = new GlobalConfig();
//        // 自定义的注入需要在这里进行配置
//        conf.setSqlInjector(easySqlInjector());
//        return conf;
//    }
//}
