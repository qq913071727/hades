package org.sraosha.microservice.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.sraosha.framework.manager.RedisManager;

import javax.annotation.Resource;

@Configuration
public class ManagerConfig {

    /****************************************** redis *********************************************/
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Bean
    public RedisTemplate<String, String> redisTemplate(RedisTemplate redisTemplate) {
        RedisSerializer stringSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringSerializer);
        redisTemplate.setStringSerializer(stringSerializer);
        redisTemplate.setValueSerializer(stringSerializer);
        redisTemplate.setHashKeySerializer(stringSerializer);
        redisTemplate.setHashValueSerializer(stringSerializer);
        return redisTemplate;
    }

    @Bean
    public RedisManager redisManager() {
        return new RedisManager(redisTemplate);
    }

    /****************************************** mongodb *********************************************/
//    @Resource
//    private MongoTemplate mongoTemplate;
//
//    @Bean
//    public MongodbManager mongodbManager() {
//        return new MongodbManager(mongoTemplate);
//    }

    /****************************************** rabbitmq *********************************************/
//    @Resource
//    private RabbitMqParamConfig rabbitMqParamConfig;
//
//    @Bean
//    public RabbitMqManager rabbitMqManager() {
//        return new RabbitMqManager(rabbitMqParamConfig.getHost(), rabbitMqParamConfig.getPort(),
//                rabbitMqParamConfig.getUsername(), rabbitMqParamConfig.getPassword(),
//                rabbitMqParamConfig.getVirtualHost());
//    }

    /****************************************** sftp *********************************************/
//    @Resource
//    private SftpParamConfig sftpParamConfig;
//
//    @Bean
//    public SftpChannelManager sftpChannelManager() {
//        return new SftpChannelManager(sftpParamConfig.getUsername(), sftpParamConfig.getPassword(),
//                sftpParamConfig.getHost(), sftpParamConfig.getPort(), sftpParamConfig.getTimeout());
//    }

    /****************************************** zookeeper *********************************************/
//    @Resource
//    private ZookeeperParamConfig zookeeperParamConfig;
//
//    @Bean
//    public ZookeeperManager zookeeperManager() {
//        return new ZookeeperManager(zookeeperParamConfig.getHost(), zookeeperParamConfig.getSessionTimeoutMs(),
//                zookeeperParamConfig.getConnectionTimeoutMs(), zookeeperParamConfig.getBaseSleepTimeMs(),
//                zookeeperParamConfig.getMaxEntries(), null);
//    }

    /****************************************** minio *********************************************/
//    @Resource
//    private MinioConfig minioConfig;
//
//    @Resource
//    private MinioClient minioClient;

    /**
     * 注入minio 客户端
     *
     * @return
     */
//    @Bean
//    public MinioClient minioClient() {
//        return MinioClient.builder()
//                .endpoint(minioConfig.getEndpoint())
//                .credentials(minioConfig.getAccessKey(), minioConfig.getSecretKey())
//                .build();
//    }

//    @Bean
//    public MinioManager minioManager() {
//        return new MinioManager(minioClient);
//    }
}
