# ------------------------应用关闭---------------------------------------------------------------
echo 'nacose-cluster应用关闭开始'

cd /opt
echo '进入到/opt目录'

echo 'Stop the program : nacos1'
pid=`ps -ef | grep -w nacos1 | grep -v "grep" | awk '{print $2}'`;
echo 'old Procedure pid:'$pid
if [ -n "$pid" ]
then
kill -9 $pid
fi
sleep 1;

echo 'Stop the program : nacos2'
pid=`ps -ef | grep -w nacos2 | grep -v "grep" | awk '{print $2}'`;
echo 'old Procedure pid:'$pid
if [ -n "$pid" ]
then
kill -9 $pid
fi
sleep 1;

echo 'Stop the program : nacos3'
pid=`ps -ef | grep -w nacos3 | grep -v "grep" | awk '{print $2}'`;
echo 'old Procedure pid:'$pid
if [ -n "$pid" ]
then
kill -9 $pid
fi
sleep 1;

echo 'nacose-cluster应用关闭完成'

# ------------------------应用启动---------------------------------------------------------------
echo 'nacose-cluster应用启动开始'

cd /opt
echo '进入到/opt目录'

echo 'Start the program : nacos1'
echo '-------Starting nacos1-------'
/opt/deploy/nacos-cluster/nacos1/bin/startup.sh &
echo 'nacos1:serverPort=34057 start success'

echo 'Start the program : nacos2'
echo '-------Starting nacos2-------'
/opt/deploy/nacos-cluster/nacos2/bin/startup.sh &
echo 'fnacos2:serverPort=28496 start success'

echo 'Start the program : nacos3'
echo '-------Starting nacos3-------'
/opt/deploy/nacos-cluster/nacos3/bin/startup.sh &
echo 'nacos3:serverPort=27240 start success'

echo 'nacose-cluster应用启动完成'
