﻿# ------------------------应用关闭---------------------------------------------------------------
echo 'sraosha-microservice应用关闭开始'

cd /opt
echo '进入到/opt目录'

echo 'Stop the program : sraosha-nacos-config.jar'
pid=`ps -ef | grep -w sraosha-nacos-config | grep -v "grep" | awk '{print $2}'`;
echo 'old Procedure pid:'$pid
if [ -n "$pid" ]
then
kill -9 $pid
fi
sleep 1;

echo 'Stop the program : sraosha-gateway.jar'
pid=`ps -ef | grep -w sraosha-gateway | grep -v "grep" | awk '{print $2}'`;
echo 'old Procedure pid:'$pid
if [ -n "$pid" ]
then
kill -9 $pid
fi
sleep 1;

echo 'Stop the program : sraosha-oauth2.jar'
pid=`ps -ef | grep -w sraosha-oauth2 | grep -v "grep" | awk '{print $2}'`;
echo 'old Procedure pid:'$pid
if [ -n "$pid" ]
then
kill -9 $pid
fi
sleep 1;

echo 'sraosha-microservice应用关闭完成'

# ------------------------应用启动---------------------------------------------------------------
echo 'sraosha-microservice应用启动开始'

cd /opt
echo '进入到/opt目录'

echo 'Start the program : sraosha-nacos-config.jar'
chmod 777 /opt/deploy/sraosha-microservice/dev/sraosha-nacos-config.jar
echo '-------Starting sraosha-nacos-config:serverPort=64120-------'
nohup java -Xms512M -Xmx1024M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/hprof/sraosha-microservice/dev/sraosha-nacos-config-dev.hprof -jar /opt/deploy/sraosha-microservice/dev/sraosha-nacos-config.jar --spring.profiles.active=dev --server.port=64120> /dev/null 2>&1 &
echo 'Starting sraosha-nacos-config:serverPort=64120 success'
sleep 5;

echo 'Start the program : sraosha-gateway.jar'
chmod 777 /opt/deploy/sraosha-microservice/dev/sraosha-gateway.jar
echo '-------Starting sraosha-gateway:serverPort=33995-------'
nohup java -Xms512M -Xmx1024M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/hprof/sraosha-microservice/dev/sraosha-gateway-dev.hprof -Xdebug -jar -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:10563 /opt/deploy/sraosha-microservice/dev/sraosha-gateway.jar --spring.profiles.active=dev --server.port=33995> /dev/null 2>&1 &
echo 'Starting sraosha-gateway:serverPort=33995 success'
sleep 5;

echo 'Start the program : sraosha-oauth2.jar'
chmod 777 /opt/deploy/sraosha-microservice/dev/sraosha-oauth2.jar
echo '-------Starting sraosha-oauth2:serverPort=37754-------'
nohup java -Xms512M -Xmx1024M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/hprof/sraosha-microservice/dev/sraosha-oauth2-dev.hprof -Xdebug -jar -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:58330 /opt/deploy/sraosha-microservice/dev/sraosha-oauth2.jar --spring.profiles.active=dev --server.port=37754>/dev/null  2>&1 &
echo 'sraosha-oauth2:serverPort=37754 start success'
sleep 5;

echo 'Start the program : sentinel-dashboard.jar'
chmod 777 /opt/deploy/sraosha-microservice/dev/sentinel-dashboard.jar
echo '-------Starting sentinel-dashboard:serverPort=13529-------'
nohup java -Xms512M -Xmx1024M -XX:PermSize=256M -XX:MaxPermSize=512M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/hprof/sraosha-microservice/dev/sentinel-dashboard.hprof -Xdebug -jar /opt/deploy/sraosha-microservice/dev/sentinel-dashboard-1.8.3.jar --spring.profiles.active=dev --server.port=13529>/dev/null  2>&1 &
echo 'sentinel-dashboard:serverPort=13529 start success'
sleep 5;

echo 'sraosha-microservice应用启动完成'
