package org.sraosha.microservice.oauth2.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * security 安全相关配置类
 */
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/oauth/authorize")
                .antMatchers("/oauth/check_token")
                .antMatchers("/oauth/verificationCode")
                .antMatchers("/oauth/register")
                .antMatchers("/oauth/logout/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 禁用 csrf, 由于使用的是JWT，我们这里不需要csrf
        http.cors().and().csrf().disable()
                .authorizeRequests()
                // 跨域预检请求
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                // swagger3
                .antMatchers("/swagger**/**").permitAll()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/v2/**").permitAll()
                // 静态资源：javascript, css, 图片等
                .antMatchers("/static/js/**").permitAll()
                .antMatchers("/static/jslib/**").permitAll()
                .antMatchers("/static/css/**").permitAll()
                // 接口路径
                .antMatchers("/api/**").permitAll()
                .antMatchers("/sraosha-oauth2/swagger-ui.html").permitAll()

                // websocket
//                .antMatchers("/ws/v1/**").permitAll()
                // 通过外网访问swagger3的路径
//                .antMatchers("/swagger-ui/**").permitAll()
                // 测试页面
//                .antMatchers("/page/test").permitAll()
//                .antMatchers("/page/talk").permitAll()
//                .antMatchers("/page/group").permitAll()
//                .antMatchers("/page/userGroup").permitAll()
                .and().userDetailsService(userDetailsService).authorizeRequests()
                // 其他所有请求需要身份认证
                .anyRequest().authenticated();
//
        // 退出登录处理器
//        http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());
        // 开启登录认证流程过滤器
//        http.addFilterBefore(new JwtLoginFilter(authenticationManager()), UsernamePasswordAuthenticationFilter.class);
        // 访问控制时登录状态检查过滤器
//        http.addFilterBefore(new JwtAuthenticationFilter(authenticationManager()), UsernamePasswordAuthenticationFilter.class);

//        http.formLogin()   //指定身份认证的方式为表单登录
//                .and()
//                .authorizeRequests() //对请求授权
//                .anyRequest()        //任何请求
//                .authenticated()    //安全认证
//                .and().userDetailsService(userDetailsService);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
