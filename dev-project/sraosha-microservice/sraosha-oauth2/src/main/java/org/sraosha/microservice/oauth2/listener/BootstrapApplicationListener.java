package org.sraosha.microservice.oauth2.listener;

import org.sraosha.microservice.oauth2.service.impl.ServiceHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 1.将client表中的数据保存到redis中
 */
@Slf4j
@Component
public class BootstrapApplicationListener implements ApplicationListener<ContextRefreshedEvent>, Ordered {

    @Resource
    private ServiceHelper serviceHelper;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("监听器BootstrapApplicationListener启动");

        // 将auth_client表中的数据保存到redis中
        serviceHelper.saveAuthClientInRedis();
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
