package org.sraosha.microservice.oauth2.service;

import org.sraosha.framework.dto.ServiceResult;

import org.sraosha.microservice.oauth2.model.AuthClient;
import com.github.pagehelper.PageInfo;

import java.io.Serializable;
import java.util.List;

/**
 * AuthClient的service接口
 */
public interface AuthClientService {

    /**
     * 新增
     * @param authClient client表
     * @return 添加成功返回1，添加失败返回-1
     */
    ServiceResult<Integer> add(AuthClient authClient);

    /**
     * 批量导入
     * @param authClientList client表（列表）
     * @return 批量导入成功返回1，批量导入失败返回-1
     */
    ServiceResult<Integer> batchAdd(List<AuthClient> authClientList);

    /**
     * 更新
     * @param authClient client表
     * @return 更新成功返回1，更新失败返回-1
     */
    ServiceResult<Integer> updateForId(AuthClient authClient);

    /**
     * 删除
     * @param id 主键
     * @return 删除成功返回1，删除失败返回-1
     */
    ServiceResult<Integer> deleteById(Serializable id);

    /**
     * 根据id查询
     * @param id 主键
     * @return 返回查询结果
     */
    ServiceResult<AuthClient> selectById(Serializable id);

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param authClient client表
     * @param pageNo 页号
     * @param pageSize 每页行数
     * @return 返回查询结果列表
     */
    ServiceResult<PageInfo<AuthClient>> page(AuthClient authClient, int pageNo, int pageSize);
}