package org.sraosha.microservice.oauth2.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * AuthClient的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AuthClientVo", description = "AuthClient的vo类")
public class AuthClientVo {
    @ApiModelProperty(value = "账号", dataType = "String")
    private String clientId;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String clientSecret;

    @ApiModelProperty(value = "主键", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "页数", dataType = "Integer")
    private Integer pageNo;

    @ApiModelProperty(value = "每页数量", dataType = "Integer")
    private Integer pageSize;
}