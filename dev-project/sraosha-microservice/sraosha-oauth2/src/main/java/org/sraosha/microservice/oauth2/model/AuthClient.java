package org.sraosha.microservice.oauth2.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.sql.Blob;
import java.sql.Clob;

/**
 * client表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AuthClient", description = "client表")
@TableName("auth_client")
public class AuthClient {

    @ApiModelProperty(value = "账号", dataType = "String")
    @TableField(value = "client_id")
    private String clientId;

    @ApiModelProperty(value = "密码", dataType = "String")
    @TableField(value = "client_secret")
    private String clientSecret;

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

}
