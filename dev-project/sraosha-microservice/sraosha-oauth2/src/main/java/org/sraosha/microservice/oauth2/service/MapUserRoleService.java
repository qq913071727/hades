package org.sraosha.microservice.oauth2.service;

import org.sraosha.framework.dto.ServiceResult;

import java.util.List;

/**
 * MapUserRole的service接口
 */
public interface MapUserRoleService {

    /**
     * 根据用户名，返回用户信息
     *
     * @param username
     * @return
     */
    ServiceResult<List<String>> findRoleNameListByUsername(String username);
}
