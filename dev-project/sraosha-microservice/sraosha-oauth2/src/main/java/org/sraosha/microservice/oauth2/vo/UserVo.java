package org.sraosha.microservice.oauth2.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * User的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserVo", description = "User的vo类")
public class UserVo {

    @ApiModelProperty(value = "用户名", dataType = "String")
    private String username;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;

    @ApiModelProperty(value = "确认密码", dataType = "String")
    private String confirmPassword;
}
