package org.sraosha.microservice.oauth2.service.impl;

import org.sraosha.framework.constants.RedisStorage;
import org.sraosha.framework.manager.RedisManager;
import org.sraosha.microservice.oauth2.constants.RedisKey;
import org.sraosha.microservice.oauth2.mapper.AuthClientMapper;
import org.sraosha.microservice.oauth2.mapper.MapUserRoleMapper;
import org.sraosha.microservice.oauth2.mapper.UserMapper;
import org.sraosha.microservice.oauth2.model.AuthClient;
import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Getter
@Component
public class ServiceHelper {

    /********************************************* mapper ********************************************/
    @Resource
    protected UserMapper userMapper;

    @Resource
    protected MapUserRoleMapper mapUserRoleMapper;

    @Resource
    protected AuthClientMapper authClientMapper;

    /********************************************* manager ********************************************/
    @Resource
    protected RedisManager redisManager;

    /**
     * 将auth_client表中的数据保存到redis中
     */
    public void saveAuthClientInRedis() {
        log.info("系统启动时，将auth_client表中的记录存储到redis中");

        // 查询auth_client表的全部记录
        List<AuthClient> authClientList = authClientMapper.selectList(null);
        if (null != authClientList && authClientList.size() > 0) {
            // 将auth_client信息存储到redis
            StringBuffer key = new StringBuffer().append(RedisKey.SYSTEM_NAME).append(RedisStorage.COLON).append(RedisKey.SERVICE_NAME)
                    .append(RedisStorage.COLON).append(RedisKey.AUTH_CLIENT);
            redisManager.set(key.toString(), JSON.toJSONString(authClientList));
        }
    }
}
