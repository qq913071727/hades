package org.sraosha.microservice.oauth2.constants;

/**
 * redis的key的前缀
 */
public class RedisKey {

    /**
     * 系统
     */
    public static final String SYSTEM_NAME = "sraosha-microservice";

    /**
     * 服务
     */
    public static final String SERVICE_NAME = "sraosha-oauth2";

    /**
     * 验证码
     */
    public static final String VERIFICATION_CODE = "verification-code";

    /**
     * auth_client表
     */
    public static final String AUTH_CLIENT = "auth-client";
}
