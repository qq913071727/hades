package org.sraosha.microservice.oauth2.controller;

import org.sraosha.framework.manager.RedisManager;
import org.sraosha.microservice.oauth2.service.AuthClientService;
import org.sraosha.microservice.oauth2.service.UserInfoService;
import org.sraosha.microservice.oauth2.service.UserService;
import org.sraosha.microservice.oauth2.value.OauthValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.endpoint.CheckTokenEndpoint;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;

import javax.annotation.Resource;

public abstract class AbstractController {

    @Autowired
    protected TokenEndpoint tokenEndpoint;

    @Autowired
    protected CheckTokenEndpoint checkTokenEndpoint;

    @Autowired
    protected RedisManager redisManager;

    @Autowired
    protected UserService userService;

    @Autowired
    protected UserInfoService userInfoService;

    @Autowired
    protected AuthClientService authClientService;

    @Resource
    protected OauthValue oauthValue;
}
