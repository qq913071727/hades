package org.sraosha.microservice.oauth2.service.impl;

import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.framework.enumeration.ServiceResultEnum;
import org.sraosha.microservice.oauth2.mapper.MapUserRoleMapper;
import org.sraosha.microservice.oauth2.model.MapUserRole;
import org.sraosha.microservice.oauth2.service.MapUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Service
@Transactional
public class MapUserRoleServiceImpl extends ServiceImpl<MapUserRoleMapper, MapUserRole> implements MapUserRoleService {

    @Resource
    private ServiceHelper serviceHelper;

    /**
     * 根据用户名，返回用户信息
     *
     * @param username
     * @return
     */
    public ServiceResult<List<String>> findRoleNameListByUsername(String username) {
        List<String> roleNameList = serviceHelper.getMapUserRoleMapper().findRoleNameListByUsername(username);

        ServiceResult<List<String>> serviceResult = new ServiceResult<List<String>>();
        serviceResult.setSuccess(Boolean.TRUE);
        serviceResult.setResult(roleNameList);
        serviceResult.setMessage(ServiceResultEnum.SELECT_SUCCESS.getMessage());
        serviceResult.setCode(ServiceResultEnum.SELECT_SUCCESS.getCode());
        return serviceResult;
    }
}
