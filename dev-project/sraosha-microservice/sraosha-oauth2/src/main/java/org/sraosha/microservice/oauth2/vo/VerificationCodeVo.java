package org.sraosha.microservice.oauth2.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * VerificationCode的vo类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VerificationCodeVo", description = "VerificationCode的vo类")
public class VerificationCodeVo implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "计算结果", dataType = "Integer")
    private Integer calculationResult;

    @ApiModelProperty(value = "图片(base64格式)对应的uuid", dataType = "String")
    private String imageUUID;

}
