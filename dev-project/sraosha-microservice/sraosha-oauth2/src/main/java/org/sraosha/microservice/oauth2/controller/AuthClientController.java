package org.sraosha.microservice.oauth2.controller;

import org.sraosha.microservice.oauth2.model.AuthClient;
import org.sraosha.microservice.oauth2.vo.AuthClientVo;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.sraosha.framework.dto.ApiResult;
import org.sraosha.framework.dto.ServiceResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * AuthClient的controller类
 */
@Api(value = "AuthClient的controller类", tags = "AuthClientController")
@RestController
@RequestMapping("api/v1/authClient")
@Slf4j
public class AuthClientController extends AbstractController {

    /**
     * 新增
     * @param authClientVo AuthClient的vo类
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("新增")
    @ApiImplicitParam(name = "authClientVo", value = "AuthClient的vo类", required = true, dataType = "AuthClientVo")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> add(@RequestBody AuthClientVo authClientVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == authClientVo) {
            String message = "调用接口【/api/v1/authClient/add时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        AuthClient authClient = new AuthClient();
        BeanUtils.copyProperties(authClientVo, authClient);
        ServiceResult<Integer> serviceResult = authClientService.add(authClient);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 批量导入
     * @param authClientVoList AuthClient的vo类列表
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("批量导入")
    @ApiImplicitParam(name = "authClientVoList", value = "AuthClient的vo类列表", required = true, dataType = "List")
    @RequestMapping(value = "/batchAdd", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> batchAdd(@RequestBody List<AuthClientVo> authClientVoList, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == authClientVoList) {
            String message = "调用接口【/api/v1/authClient/batchAdd时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        List<AuthClient> authClientList = new ArrayList<>();
        BeanUtils.copyProperties(authClientVoList, authClientList);
        ServiceResult<Integer> serviceResult = authClientService.batchAdd(authClientList);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id更新
     * @param authClientVo AuthClient的vo类
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id更新")
    @ApiImplicitParam(name = "authClientVo", value = "AuthClient的vo类", required = true, dataType = "AuthClientVo")
    @RequestMapping(value = "/updateForId", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> updateForId(@RequestBody AuthClientVo authClientVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == authClientVo) {
            String message = "调用接口【/api/v1/authClient/updateForId时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        AuthClient authClient = new AuthClient();
        BeanUtils.copyProperties(authClientVo, authClient);
        ServiceResult<Integer> serviceResult = authClientService.updateForId(authClient);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id删除
     * @param id 主键
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id删除")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Integer")
    @RequestMapping(value = "/deleteById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<Integer>> deleteById(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == id) {
            String message = "调用接口【/api/v1/authClient/deleteById时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<Integer> serviceResult = authClientService.deleteById(id);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据id查询
     * @param id 主键
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据id查询")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "Integer")
    @RequestMapping(value = "/selectById/{id}", method = RequestMethod.GET, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<AuthClient>> selectById(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == id) {
            String message = "调用接口【/api/v1/authClient/selectById时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        ServiceResult<AuthClient> serviceResult = authClientService.selectById(id);

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }

    /**
     * 根据条件，分页查询，升序/降序排列
     * @param authClientVo AuthClient的vo类
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("根据条件，分页查询，升序/降序排列")
    @ApiImplicitParam(name = "authClientVo", value = "AuthClient的vo类", required = true, dataType = "AuthClientVo")
    @RequestMapping(value = "/page", method = RequestMethod.POST, produces = {"application/json; charset=utf-8"})
    @ResponseBody
    public ResponseEntity<ApiResult<PageInfo<AuthClient>>> page(@RequestBody AuthClientVo authClientVo, HttpServletRequest request, HttpServletResponse response) {
        ApiResult apiResult = new ApiResult();
        if (null == authClientVo) {
            String message = "调用接口【/api/v1/authClient/page时，参数不合法";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }
        if (null == authClientVo.getPageNo() || null == authClientVo.getPageSize()) {
            String message = "调用接口【/api/v1/authClient/page时，参数pageNo和pageSize不能为空";
            log.warn(message);
            apiResult.setSuccess(Boolean.FALSE);
            apiResult.setCode(HttpStatus.BAD_REQUEST.value());
            apiResult.setMessage(message);
            return new ResponseEntity<>(apiResult, HttpStatus.BAD_REQUEST);
        }

        AuthClient authClient = new AuthClient();
        BeanUtils.copyProperties(authClientVo, authClient);
        ServiceResult<PageInfo<AuthClient>> serviceResult = authClientService.page(authClient, authClientVo.getPageNo(), authClientVo.getPageSize());

        apiResult.setSuccess(Boolean.TRUE);
        apiResult.setCode(HttpStatus.OK.value());
        apiResult.setMessage(serviceResult.getMessage());
        apiResult.setResult(serviceResult.getResult());
        return new ResponseEntity<>(apiResult, HttpStatus.OK);
    }
}
