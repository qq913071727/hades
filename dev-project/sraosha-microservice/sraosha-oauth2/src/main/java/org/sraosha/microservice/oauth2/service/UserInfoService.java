package org.sraosha.microservice.oauth2.service;

import org.sraosha.microservice.oauth2.pojo.UserInfo;

/**
 * UserInfo的service接口
 */
public interface UserInfoService {

    /**
     * 根据username，查找UserInfo对象
     * @param username
     * @return
     */
    UserInfo findByUsername(String username);
}
