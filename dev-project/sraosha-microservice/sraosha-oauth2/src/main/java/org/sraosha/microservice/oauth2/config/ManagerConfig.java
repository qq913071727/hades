package org.sraosha.microservice.oauth2.config;

import org.sraosha.framework.manager.RedisManager;
import org.sraosha.framework.manager.ThreadPoolManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import javax.annotation.Resource;

@Configuration
public class ManagerConfig {

    /****************************************** redis *********************************************/
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Bean
    public RedisTemplate<String, String> redisTemplate(RedisTemplate redisTemplate) {
        RedisSerializer stringSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringSerializer);
        redisTemplate.setStringSerializer(stringSerializer);
        redisTemplate.setValueSerializer(stringSerializer);
        redisTemplate.setHashKeySerializer(stringSerializer);
        redisTemplate.setHashValueSerializer(stringSerializer);
        return redisTemplate;
    }

    @Bean
    public RedisManager redisManager() {
        return new RedisManager(redisTemplate);
    }

    /****************************************** mongodb *********************************************/
//    @Resource
//    private MongoTemplate mongoTemplate;
//
//    @Bean
//    public MongodbManager mongodbManager() {
//        return new MongodbManager(mongoTemplate);
//    }

    /****************************************** rabbitmq *********************************************/
//    @Resource
//    private RabbitMqParamConfig rabbitMqParamConfig;
//
//    @Bean
//    public RabbitMqManager rabbitMqManager() {
//        return new RabbitMqManager(rabbitMqParamConfig.getHost(), rabbitMqParamConfig.getPort(),
//                rabbitMqParamConfig.getUsername(), rabbitMqParamConfig.getPassword(),
//                rabbitMqParamConfig.getVirtualHost());
//    }

    /****************************************** sftp *********************************************/
//    @Resource
//    private SftpParamConfig sftpParamConfig;
//
//    @Bean
//    public SftpChannelManager sftpChannelManager() {
//        return new SftpChannelManager(sftpParamConfig.getUsername(), sftpParamConfig.getPassword(),
//                sftpParamConfig.getHost(), sftpParamConfig.getPort(), sftpParamConfig.getTimeout());
//    }

    /**************************************** thread pool *******************************************/
//    @Resource
//    private ThreadPoolConfig threadPoolConfig;
//
//    @Bean
//    public ThreadPoolManager threadPoolManager() {
//        return new ThreadPoolManager(threadPoolConfig.getCorePoolSize(),
//                threadPoolConfig.getMaximumPoolSize(), threadPoolConfig.getKeepAliveTime(),
//                threadPoolConfig.getWorkQueue(), null);
//    }

    /****************************************** zookeeper *********************************************/
//    @Resource
//    private ZookeeperParamConfig zookeeperParamConfig;
//
//    @Bean
//    public ZookeeperManager zookeeperManager() {
//        return new ZookeeperManager(zookeeperParamConfig.getHost(), zookeeperParamConfig.getSessionTimeoutMs(),
//                zookeeperParamConfig.getConnectionTimeoutMs(), zookeeperParamConfig.getBaseSleepTimeMs(),
//                zookeeperParamConfig.getMaxEntries(), this.threadPoolManager().getThreadPoolTaskExecutor().getThreadPoolExecutor());
//    }

    /****************************************** aop *********************************************/
//    @Bean
//    public ZookeeperLockAspect zookeeperLockAspect(){
//        return new ZookeeperLockAspect();
//    }
//
//    @Bean
//    public ZookeeperUnLockAspect zookeeperUnLockAspect(){
//        return new ZookeeperUnLockAspect();
//    }
}
