package org.sraosha.microservice.oauth2.mapper;

import org.springframework.stereotype.Repository;
import org.sraosha.microservice.oauth2.model.User;

/**
 * User的mapper类
 */
@Repository
public interface UserMapper extends EasyBaseMapper<User> {

    User findUserByUserName(String userName);
}