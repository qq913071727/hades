package org.sraosha.microservice.oauth2.mapper;

import org.sraosha.microservice.oauth2.model.AuthClient;
import org.springframework.stereotype.Repository;

/**
 * AuthClient的mapper类
 */
@Repository
public interface AuthClientMapper extends EasyBaseMapper<AuthClient> {

}