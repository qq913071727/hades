package org.sraosha.microservice.oauth2.mapper;

import org.sraosha.microservice.oauth2.model.MapUserRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * MapUserRole的mapper类
 */
@Repository
public interface MapUserRoleMapper extends EasyBaseMapper<MapUserRole> {

    /**
     * 根据用户名查找角色名列表
     * @param username
     * @return
     */
    List<String> findRoleNameListByUsername(@Param("username") String username);
}
