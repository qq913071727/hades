package org.sraosha.microservice.oauth2.service;

import org.sraosha.framework.dto.ServiceResult;
import org.sraosha.microservice.oauth2.model.User;

/**
 * User的service接口
 */
public interface UserService {

    /**
     * 根据用户名，返回用户信息
     *
     * @param username
     * @return
     */
    ServiceResult<User> findUserByUserName(String username);

    /**
     * 添加用户
     * @param user
     * @return
     */
    ServiceResult<User> add(User user);

    /**
     * 添加用户，username不能重名
     * @param user
     * @return
     */
    ServiceResult<User> addWithoutDuplicateUsername(User user);

}
