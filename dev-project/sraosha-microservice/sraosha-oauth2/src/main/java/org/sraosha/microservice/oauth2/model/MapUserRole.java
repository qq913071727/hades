package org.sraosha.microservice.oauth2.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户和角色的关系表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MapUserRole", description = "用户和角色的关系表")
@TableName("map_user_role")
public class MapUserRole {

    @ApiModelProperty(value = "主键", dataType = "Integer")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id", dataType = "Integer")
    @TableField(value = "user_id")
    private Integer userId;

    @ApiModelProperty(value = "角色id", dataType = "Integer")
    @TableField(value = "role_id")
    private Integer roleId;
}
