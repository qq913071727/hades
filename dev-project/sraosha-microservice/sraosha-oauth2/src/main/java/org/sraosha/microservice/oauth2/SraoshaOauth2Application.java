package org.sraosha.microservice.oauth2;

import org.sraosha.framework.helper.LogbackHelper;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = "org.sraosha.microservice",
        exclude = {DataSourceAutoConfiguration.class})
@MapperScan("org.sraosha.microservice.oauth2.mapper")
@EnableSwagger2
@EnableDiscoveryClient
@Slf4j
public class SraoshaOauth2Application {

    public static void main(String[] args) {
        //设置日志端口
        LogbackHelper.setLogbackPort(args);

        SpringApplication.run(SraoshaOauth2Application.class, args);
        log.info("sraosha-oauth2服务启动成功");
    }
}