package org.sraosha.microservice.oauth2.value;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 验证
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "auth")
public class OauthValue {

    /**
     * 验证码有效时间（单位：秒）
     */
    private Integer verificationCodeTimeout;

    /**
     * 登录有效时间（单位：秒）
     */
    private Integer loginTimeout;

}
