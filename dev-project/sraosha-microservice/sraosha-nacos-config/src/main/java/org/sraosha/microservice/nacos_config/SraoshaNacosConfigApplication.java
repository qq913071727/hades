package org.sraosha.microservice.nacos_config;

import org.sraosha.framework.helper.LogbackHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@Slf4j
public class SraoshaNacosConfigApplication {

    public static void main(String[] args) {
        //设置日志端口
        LogbackHelper.setLogbackPort(args);

        SpringApplication.run(SraoshaNacosConfigApplication.class, args);
        log.info("sraosha-nacos-config服务启动成功");
    }
    
}
