cls

@REM chcp 65001

echo 数据库迁移

C:

@REM 执行dist目录
@REM cd C:\mywork\gitcode-repository\hades\dev-project\adam3\dist\adam3-1.0.tar.gz_files\adam3-1.0

@REM 执行web目录
cd C:\mywork\gitcode-repository\hades\dev-project\adam3

python manage.py makemigrations

python manage.py migrate

@REM addcolumncomments只支持mysql和postgresql
python manage.py addcolumncomments

cd C:\mywork\gitcode-repository\hades\dev-project\adam3