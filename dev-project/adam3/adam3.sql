﻿----------------------- 所有算法的每笔交易成功率、每笔交易收益率、每笔交易收益率 --------------------------
select 'mdl_c_f_date_macd_gold_cross' as 算法类型,
(select count(*) from mdl_c_f_date_macd_gold_cross t) 交易次数,
(select count(*) from mdl_c_f_date_macd_gold_cross t where t.profit_loss>0)/
(select count(*) from mdl_c_f_date_macd_gold_cross t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_c_f_date_macd_gold_cross t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.code) from
(select t1.code code, max(t1.sell_date) max_sell_date from mdl_c_f_date_macd_gold_cross t1
group by t1.code) t2
join mdl_c_f_date_macd_gold_cross t3 on t3.code=t2.code and t3.sell_date=max_sell_date) as 股票平均收益
from dual
union
select 'mdl_c_f_date_macd_dead_cross' as 算法类型,
(select count(*) from mdl_c_f_date_macd_dead_cross t) 交易次数,
(select count(*) from mdl_c_f_date_macd_dead_cross t where t.profit_loss>0)/
(select count(*) from mdl_c_f_date_macd_dead_cross t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_c_f_date_macd_dead_cross t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.code) from
(select t1.code code, max(t1.buy_date) max_buy_date from mdl_c_f_date_macd_dead_cross t1
group by t1.code) t2
join mdl_c_f_date_macd_dead_cross t3 on t3.code=t2.code and t3.buy_date=max_buy_date) as 股票平均收益
from dual
union
select 'mdl_c_f_date_c_p_ma5_g_c' as 算法类型,
(select count(*) from mdl_c_f_date_c_p_ma5_g_c t) 交易次数,
(select count(*) from mdl_c_f_date_c_p_ma5_g_c t where t.profit_loss>0)/
(select count(*) from mdl_c_f_date_c_p_ma5_g_c t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_c_f_date_c_p_ma5_g_c t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.code) from
(select t1.code code, max(t1.sell_date) max_sell_date from mdl_c_f_date_c_p_ma5_g_c t1
group by t1.code) t2
join mdl_c_f_date_c_p_ma5_g_c t3 on t3.code=t2.code and t3.sell_date=max_sell_date) as 股票平均收益
from dual
union
select 'mdl_c_f_date_c_p_ma5_d_c' as 算法类型,
(select count(*) from mdl_c_f_date_c_p_ma5_d_c t) 交易次数,
(select count(*) from mdl_c_f_date_c_p_ma5_d_c t where t.profit_loss>0)/
(select count(*) from mdl_c_f_date_c_p_ma5_d_c t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_c_f_date_c_p_ma5_d_c t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.code) from
(select t1.code code, max(t1.buy_date) max_buy_date from mdl_c_f_date_c_p_ma5_d_c t1
group by t1.code) t2
join mdl_c_f_date_c_p_ma5_d_c t3 on t3.code=t2.code and t3.buy_date=max_buy_date) as 股票平均收益
from dual
union
select 'mdl_c_f_date_hei_kin_ashi_up' as 算法类型,
(select count(*) from mdl_c_f_date_hei_kin_ashi_up t) 交易次数,
(select count(*) from mdl_c_f_date_hei_kin_ashi_up t where t.profit_loss>0)/
(select count(*) from mdl_c_f_date_hei_kin_ashi_up t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_c_f_date_hei_kin_ashi_up t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.code) from
(select t1.code code, max(t1.sell_date) max_sell_date from mdl_c_f_date_hei_kin_ashi_up t1
group by t1.code) t2
join mdl_c_f_date_hei_kin_ashi_up t3 on t3.code=t2.code and t3.sell_date=max_sell_date) as 股票平均收益
from dual
union
select 'mdl_c_f_date_hei_kin_ashi_down' as 算法类型,
(select count(*) from mdl_c_f_date_hei_kin_ashi_down t) 交易次数,
(select count(*) from mdl_c_f_date_hei_kin_ashi_down t where t.profit_loss>0)/
(select count(*) from mdl_c_f_date_hei_kin_ashi_down t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_c_f_date_hei_kin_ashi_down t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.code) from
(select t1.code code, max(t1.buy_date) max_buy_date from mdl_c_f_date_hei_kin_ashi_down t1
group by t1.code) t2
join mdl_c_f_date_hei_kin_ashi_down t3 on t3.code=t2.code and t3.buy_date=max_buy_date) as 股票平均收益
from dual
union
select 'mdl_c_f_date_kd_gold_cross' as 算法类型,
(select count(*) from mdl_c_f_date_kd_gold_cross t) 交易次数,
(select count(*) from mdl_c_f_date_kd_gold_cross t where t.profit_loss>0)/
(select count(*) from mdl_c_f_date_kd_gold_cross t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_c_f_date_kd_gold_cross t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.code) from
(select t1.code code, max(t1.sell_date) max_sell_date from mdl_c_f_date_kd_gold_cross t1
group by t1.code) t2
join mdl_c_f_date_kd_gold_cross t3 on t3.code=t2.code and t3.sell_date=max_sell_date) as 股票平均收益
from dual
union
select 'mdl_c_f_date_kd_dead_cross' as 算法类型,
(select count(*) from mdl_c_f_date_kd_dead_cross t) 交易次数,
(select count(*) from mdl_c_f_date_kd_dead_cross t where t.profit_loss>0)/
(select count(*) from mdl_c_f_date_kd_dead_cross t)*100 as 每笔交易成功率,
(select sum(t1.profit_loss)/count(*) from mdl_c_f_date_kd_dead_cross t1) as 每笔交易收益率,
(select sum(t3.accumulative_profit_loss)/count(t2.code) from
(select t1.code code, max(t1.buy_date) max_buy_date from mdl_c_f_date_kd_dead_cross t1
group by t1.code) t2
join mdl_c_f_date_kd_dead_cross t3 on t3.code=t2.code and t3.buy_date=max_buy_date) as 股票平均收益
from dual;

----------------------------------------- 重新生成表结构 -------------------------------
/*delete from auth_permission where codename like '%MDL_C_F_DATE_MACD%';
delete from django_migrations where app in('sessions','web');
delete from django_content_type where model like '%MDL_C_F_DATE_MACD%';

drop table django_session;
drop table commodity_future_exchange;
drop table commodity_future_info;
drop table commodity_future_date_data;
drop table commodity_future_hour_data;
drop table commodity_future_minute_data;
drop table mdl_c_f_date_macd_gold_cross;
drop table mdl_c_f_date_macd_dead_cross;*/

----------------------------------------- commodity_future_exchange -------------------------------

select * from commodity_future_exchange;

----------------------------------------- commodity_future_info -------------------------------

select * from commodity_future_info t for update;

select * from commodity_future_info t order by t.code desc;

select * from commodity_future_info t where t.code='EC' /*t.name='碳酸锂'*/ for update;

select * from commodity_future_info t where t.calculation_method is null for update;

select lower(t.code), count(*) from commodity_future_info t group by lower(t.code) having count(*)>1;

select * from commodity_future_info t where t.code not like '%C/P';

select * from commodity_future_info t where upper(t.code)='TL';


-- ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY-MM-DD HH24:MI:SSXFF';
-- alter system set nls_date_format='yyyy-mm-dd hh24:mi:ss' scope=spfile;
ALTER SESSION SET NLS_LANG='AMERICAN_AMERICA.ZHS16GBK';

/*update commodity_future_info t set t.daily_trading_2_end_hour=to_timestamp(t.daily_trading_2_end_hour);
commit;*/

/*update commodity_future_info t set t.tradable=1;
commit;*/

/*update commodity_future_info t set t.tradable=0 where t.code='RI';
commit;*/

/*update commodity_future_info t set t.contract_unit=10, t.minimum_fluctuation=1, 
t.price_limit=0.06, t.exchange_deposit=0.07, t.company_deposit=0.15
where t.code='L';
commit;*/

/*update commodity_future_info t 
set t.transaction_multiplier=10, t.exchange_deposit=0.1, t.company_deposit=0.6
where t.code='RS';
commit;*/

/*update commodity_future_info t set t.tradable=0 where t.code='rr';
commit;*/

/*update commodity_future_info t set t.tradable=0 where t.name like '%期权%';
commit;*/

----------------------------------------- commodity_future_date_data -------------------------------

select * from commodity_future_date_data t where t.transaction_date=to_date('20250307','yyyy-mm-dd');

select * from commodity_future_date_data t where t.code='EC' order by t.transaction_date desc;

select count(*) from commodity_future_date_data t where t.code='AU-C/P';

select max(t.transaction_date) from commodity_future_date_data t;

select * from commodity_future_date_data t where t.TURNOVER=0;

select * from commodity_future_date_data t where t.code='M' and t.transaction_date is null;

select t.code, count(*) from commodity_future_date_data t group by t.code order by count(*) asc;

/*delete from commodity_future_date_data t where t.transaction_date=to_date('20241201','yyyy-mm-dd');
commit;*/

/*update commodity_future_date_data set unite_relative_price_index=null;
commit;*/

/*update commodity_future_date_data t set t.bias5=null, t.bias10=null, t.bias20=null, t.bias60=null,
t.bias120=null, t.bias250=null
where t.transaction_date=to_date('20241122','yyyy-mm-dd');
commit;*/

/*update COMMODITY_FUTURE_DATE_DATA t set t.macd=2*(t.DIF-t.DEA) where t.DIF is not null and t.dea is not null;
commit;*/

/*update commodity_future_date_data t set t.variance5=null, t.variance10=null, t.variance20=null, 
t.variance60=null, t.variance120=null, t.variance250=null
where t.transaction_date=to_date('20241122','yyyy-mm-dd');
commit;*/

/*update commodity_future_date_data cfdd set cfdd.name=
(select cfi.name from commodity_future_info cfi where cfi.code=cfdd.code);
commit;*/

/*update commodity_future_date_data t set t.ma5=null, t.ma10=null, t.ma20=null, t.ma60=null, t.ma120=null, t.ma250=null;
update commodity_future_date_data t set t.ema12=null, t.ema26=null, t.dif=null, t.dea=null;
update commodity_future_date_data t set t.rsv=null, t.k=null, t.d=null;
commit;*/

/*update commodity_future_date_data t set t.ha_open_price=null, t.ha_close_price=null,
t.ha_highest_price=null, t.ha_lowest_price=null 
where t.transaction_date=to_date('20240816','yyyy-mm-dd');
commit;*/

/*update commodity_future_date_data t set t.last_close_price=null, t.rising_and_falling_amount=null, t.price_change=null
where t.transaction_date=to_date('20240911','yyyy-mm-dd');
commit;*/

-- truncate table commodity_future_date_data;

------------------------------------- commodity_future_week_data ---------------------------

select * from commodity_future_week_data;

select * from commodity_future_week_data t where t.code='CU' order by t.begin_date desc;

select max(t.begin_date), max(t.end_date) from commodity_future_week_data t;

select * from commodity_future_date_data t where t.name in('米', '玉米');

select * from commodity_future_info t where t.name like '%米%';

select t.code, t.begin_date, t.end_date, count(*) from commodity_future_week_data t where t.code='C' 
group by t.code, t.begin_date, t.end_date having count(*)>1;

/*update commodity_future_week_data t set t.macd=2*(t.DIF-t.DEA) where t.DIF is not null and t.dea is not null;
commit;*/

-- truncate table commodity_future_week_data;

----------------------------------------- c_f_date_contract_data -------------------------------

select * from c_f_date_contract_data;
select count(*) from c_f_date_contract_data;

select t.transaction_date, count(*) from c_f_date_contract_data t 
group by t.transaction_date order by t.transaction_date desc;

select * from c_f_date_contract_data t where t.code='INE.ec2504' order by t.transaction_date desc;

select * from c_f_date_contract_data t where t.transaction_date=to_date('2025-3-7','yyyy-mm-dd');

select distinct substr(t.code, 0, 8) from c_f_date_contract_data t where t.code like 'CFFEX%' group by substr(t.code, 0, 8);

select * from c_f_date_contract_data t where t.name like '%欧线%' order by t.transaction_date desc;

select upper(REGEXP_SUBSTR(substr(t.code, INSTR(t.code, '.') + 1), '[A-Za-z]+')) from c_f_date_contract_data t;

select min(t.transaction_date) from c_f_date_contract_data t;

/*update c_f_date_contract_data t set t.rising_and_falling_amount=null;
commit;*/

/*update c_f_date_contract_data t set t.rising_and_falling_amount=t.close_price-t.last_close_price;
commit;*/

/*delete from c_f_date_contract_data t where t.transaction_date=to_date('20250121','yyyy-mm-dd');
commit;*/

-- truncate table c_f_date_contract_data;

----------------------------------------- C_F_WEEK_CONTRACT_DATA -------------------------------

select * from C_F_WEEK_CONTRACT_DATA t;

select * from C_F_WEEK_CONTRACT_DATA t where t.code = 'CFFEX.T2506' order by t.begin_date desc;

select max(t.begin_date), max(t.end_date) from C_F_WEEK_CONTRACT_DATA t;

select * from c_f_date_contract_data t where t.code = 'SHFE.cu2501'
and t.transaction_date between to_date('20241216','yyyy-mm-dd') and to_date('20241220','yyyy-mm-dd');

/*update C_F_WEEK_CONTRACT_DATA t set t.rising_and_falling_amount=null;
commit;*/

/*update C_F_WEEK_CONTRACT_DATA t set t.rising_and_falling_amount=t.close_price-t.last_close_price;
commit;*/

/*delete from c_f_week_contract_data t where t.begin_date>=to_date('20250120','yyyy-mm-dd');
commit;*/

-- truncate table C_F_WEEK_CONTRACT_DATA;

----------------------------------------- mdl_c_f_date_macd_gold_cross -------------------------------

select * from mdl_c_f_date_macd_gold_cross;

select * from mdl_c_f_date_macd_gold_cross t where t.code='CU' order by t.sell_date desc;

select max(t.buy_date), max(t.sell_date) from mdl_c_f_date_macd_gold_cross t;

select t.code, t.sell_date, count(*) from mdl_c_f_date_macd_gold_cross t 
group by t.code, t.sell_date having count(*)>1;

select * from mdl_c_f_date_macd_gold_cross t order by t.sell_date desc;

/*delete from mdl_c_f_date_macd_gold_cross t where t.sell_date=to_date('20240814','yyyy-mm-dd');
commit;*/

-- truncate table mdl_c_f_date_macd_gold_cross;

----------------------------------------- mdl_c_f_date_macd_dead_cross -------------------------------

select * from mdl_c_f_date_macd_dead_cross;

select * from mdl_c_f_date_macd_dead_cross t where t.code='RB' order by t.buy_date desc;

select max(t.buy_date), max(t.sell_date) from mdl_c_f_date_macd_dead_cross t;

select t.code, t.buy_date, count(*) from mdl_c_f_date_macd_dead_cross t 
group by t.code, t.buy_date having count(*)>1;

select * from mdl_c_f_date_macd_dead_cross t order by t.buy_date desc;

/*delete from mdl_c_f_date_macd_dead_cross t where t.buy_date=to_date('20240814','yyyy-mm-dd');
commit;*/

-- truncate table mdl_c_f_date_macd_dead_cross;

----------------------------------------- mdl_c_f_date_c_p_ma5_g_c -------------------------------

select * from mdl_c_f_date_c_p_ma5_g_c;

select * from mdl_c_f_date_c_p_ma5_g_c t where t.code='RB' order by t.sell_date desc;

select max(t.buy_date), max(t.sell_date) from mdl_c_f_date_c_p_ma5_g_c t;

select t.code, t.sell_date, count(*) from mdl_c_f_date_c_p_ma5_g_c t 
group by t.code, t.sell_date having count(*)>1;

select * from mdl_c_f_date_c_p_ma5_g_c t order by t.sell_date desc;

/*delete from mdl_c_f_date_c_p_ma5_g_c t where t.sell_date=to_date('20240814','yyyy-mm-dd');
commit;*/

-- truncate table mdl_c_f_date_c_p_ma5_g_c;

----------------------------------------- mdl_c_f_date_c_p_ma5_d_c -------------------------------

select * from mdl_c_f_date_c_p_ma5_d_c;

select * from mdl_c_f_date_c_p_ma5_d_c t where t.code='LH' order by t.buy_date desc;

select * from mdl_c_f_date_c_p_ma5_d_c t where t.code='LH' and t.sell_date=to_date('20230410','yyyy-mm-dd');
select * from commodity_future_date_data t where t.code='LH' and t.transaction_date=to_date('20230410','yyyy-mm-dd');
select * from commodity_future_date_data t where t.code='LH' and t.transaction_date=to_date('20230414','yyyy-mm-dd');

select max(t.buy_date), max(t.sell_date) from mdl_c_f_date_c_p_ma5_d_c t;

select t.code, t.buy_date, count(*) from mdl_c_f_date_c_p_ma5_d_c t 
group by t.code, t.buy_date having count(*)>1;

select * from mdl_c_f_date_c_p_ma5_d_c t order by t.buy_date desc;

/*delete from mdl_c_f_date_c_p_ma5_d_c t where t.buy_date=to_date('20240814','yyyy-mm-dd');
commit;*/

-- truncate table mdl_c_f_date_c_p_ma5_d_c;

----------------------------------------- mdl_c_f_date_hei_kin_ashi_up -------------------------------

select * from mdl_c_f_date_hei_kin_ashi_up;

select * from mdl_c_f_date_hei_kin_ashi_up t where t.code='RB' order by t.sell_date desc;

select max(t.buy_date), max(t.sell_date) from mdl_c_f_date_hei_kin_ashi_up t;

select * from mdl_c_f_date_hei_kin_ashi_up t order by t.sell_date desc;

select t.code, t.buy_date, count(*) from mdl_c_f_date_hei_kin_ashi_up t 
group by t.code, t.buy_date having count(*)>1;

select * from mdl_c_f_date_hei_kin_ashi_up t order by t.sell_date desc;

-- truncate table mdl_c_f_date_hei_kin_ashi_up;

----------------------------------------- mdl_c_f_date_hei_kin_ashi_down -------------------------------

select * from mdl_c_f_date_hei_kin_ashi_down;

select * from mdl_c_f_date_hei_kin_ashi_down t where t.code='RB' order by t.buy_date desc;

select max(t.buy_date), max(t.sell_date) from mdl_c_f_date_hei_kin_ashi_down t;

select t.code, t.buy_date, count(*) from mdl_c_f_date_hei_kin_ashi_down t 
group by t.code, t.buy_date having count(*)>1;

select * from mdl_c_f_date_hei_kin_ashi_down t order by t.buy_date desc;

-- truncate table mdl_c_f_date_hei_kin_ashi_down;

----------------------------------------- mdl_c_f_date_kd_gold_cross -------------------------------

select * from mdl_c_f_date_kd_gold_cross;

select * from mdl_c_f_date_kd_gold_cross t where t.code='RB' order by t.sell_date desc;

select max(t.buy_date), max(t.sell_date) from mdl_c_f_date_kd_gold_cross t;

select t.code, t.sell_date, count(*) from mdl_c_f_date_kd_gold_cross t 
group by t.code, t.sell_date having count(*)>1;

select * from mdl_c_f_date_kd_gold_cross t order by t.sell_date desc;

/*delete from mdl_c_f_date_kd_gold_cross t where t.sell_date=to_date('20240816','yyyy-mm-dd');
commit;*/

-- truncate table mdl_c_f_date_kd_gold_cross;

----------------------------------------- mdl_c_f_date_kd_dead_cross -------------------------------

select * from mdl_c_f_date_kd_dead_cross;

select * from mdl_c_f_date_kd_dead_cross t where t.code='RB' order by t.buy_date desc;

select max(t.buy_date), max(t.sell_date) from mdl_c_f_date_kd_dead_cross t;

select t.code, t.sell_date, count(*) from mdl_c_f_date_kd_dead_cross t 
group by t.code, t.sell_date having count(*)>1;

select * from mdl_c_f_date_kd_dead_cross t order by t.buy_date desc;

/*delete from mdl_c_f_date_kd_dead_cross t where t.buy_date=to_date('20240816','yyyy-mm-dd');
commit;*/

-- truncate table mdl_c_f_date_kd_dead_cross;

------------------------------------- unite_relative_price_index_d ---------------------------

select * from unite_relative_price_index_d t order by t.transaction_date asc;
select * from unite_relative_price_index_d t order by t.transaction_date desc;

select * from unite_relative_price_index_d t where t.transaction_date=to_date('2024-12-1','yyyy-mm-dd');

select * from (select * from commodity_future_date_data t where t.code = 'AU-C/P' 
and t.transaction_date <= to_date('2024-12-26', 'yyyy-mm-dd') order by t.transaction_date desc) where rownum <= 1;

--select userenv('language') from dual;

/*delete from unite_relative_price_index_d t where t.transaction_date=to_date('20240925','yyyy-mm-dd');
commit;*/

-- truncate table unite_relative_price_index_d;

------------------------------------- mdl_hmm ---------------------------

select distinct t.code from mdl_hmm t;

select * from mdl_hmm t where t.code='SR' order by t.sell_date desc;

select t.CODE, max(t.ACCUMULATIVE_PROFIT_LOSS), MIN(t.ACCUMULATIVE_PROFIT_LOSS) from MDL_HMM t GROUP BY t.CODE;

select * from COMMODITY_FUTURE_DATE_DATA t where t.code='JM' order by t.TRANSACTION_DATE asc;

select t.code, max(t.sell_date), min(t.buy_date), count(*) from mdl_hmm t group by t.code order by max(t.sell_date) asc;

select * from commodity_future_info t where t.code not in(select distinct t1.code from mdl_hmm t1);

select * from MDL_HMM t where t.ACCUMULATIVE_PROFIT_LOSS is null;

/*delete from mdl_hmm t where t.code='JM';
commit;*/

-- truncate table MDL_HMM;

----------------------------------------- robot8 -------------------------------

select * from robot8_account for update;

select * from robot8_commodity_future_filter;

select * from robot8_commodity_future_filter t where t.code='LH';

select * from commodity_future_date_data t where t.code='UR' 
and t.transaction_date>=to_date('20230120','yyyy-mm-dd') order by t.transaction_date asc;

select * from commodity_future_info t for update;

select * from commodity_future_info t where t.code='PG';

select * from robot8_c_f_transact_record t order by t.buy_date desc;

select * from robot8_c_f_transact_record t where t.code='FB';

select * from robot8_c_f_transact_record t 
join commodity_future_info cfi on cfi.code=t.code order by t.buy_date desc;

select * from robot8_c_f_transact_record t order by t.Profit_And_Loss_Rate asc;
select * from robot8_c_f_transact_record t order by t.Profit_And_Loss_Rate desc;

select * from robot8_account_log t order by t.date_ desc;

select t.*, t.total_assets/5000 from robot8_account t order by t.id asc;
select t.filter_type, count(*) from robot8_c_f_transact_record t group by t.filter_type;
select count(*), count(*)/count(distinct t.buy_date)/4 from robot8_c_f_transact_record t;
select t.robot_name, count(*) from robot8_c_f_transact_record t group by t.robot_name;
select * from robot8_account_log t order by t.date_ desc;
select t.robot_name, max(t.total_assets)/5000, min(t.total_assets)/5000 
from robot8_account_log t group by t.robot_name;
select t.id, t.robot_name, t.total_open_commission, t.total_close_commission
from robot8_account t;

select * from commodity_future_date_data t where t.last_close_price is null;

select * from commodity_future_date_data t where t.code='CU' order by t.transaction_date asc;

select * from robot8_c_f_rollover;

/*update robot8_account set hold_commodity_future_number=0,commodity_future_assets=0,capital_assets=500000,
total_assets=500000,TOTAL_OPEN_COMMISSION=0,TOTAL_CLOSE_COMMISSION=0;
truncate table robot8_commodity_future_filter;
truncate table robot8_c_f_transact_record;
truncate table robot8_account_log;
truncate table robot8_c_f_rollover;
commit;*/

select t.transaction_date, avg(t.close_price), avg(t.up), avg(t.mb), avg(t.dn) 
from commodity_future_date_data t where t.transaction_date=to_date(\'2024-06-20\', \'yyyy-mm-dd\') 
group by t.transaction_date order by t.transaction_date asc;
