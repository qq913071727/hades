# coding=utf-8
from distutils.core import setup

setup(
    name='adam3',
    version='1.0',
    author='李珅',
    author_email='913071727@qq.com',
    maintainer='李珅',
    maintainer_email='913071727@qq.com',
    url='',
    packages=['', 'adam3', 'web', 'web/config', 'web/constants', 'web/domain', 'web/dto',
              'web/dao', 'web/service', 'web/manager', 'web/migrations', 'web/task', 'web/util']
)
