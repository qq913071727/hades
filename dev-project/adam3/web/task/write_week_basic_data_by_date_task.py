#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import connection
from web.manager.log_manager import LogManager
from web.config.commodity_future_week_data_config import CommodityFutureWeekDataConfig
from web.task.base_task import BaseTask

Logger = LogManager.get_logger(__name__)


class WriteWeekBasicDataByDateTask(BaseTask):

    def do_task(self):
        """
        根据日期，计算周线级别基础数据
        """

        # 根据日期，计算周线级别某一个交易周的基础数据
        self.commodity_future_week_data_service.write_week_by_date()

        # 根据日期，计算周线级别某一个交易周的MA数据
        self.commodity_future_week_data_service.write_week_ma_by_date()

        # 根据日期，计算周线级别某一个交易周的KD数据
        self.commodity_future_week_data_service.write_week_kd_by_date()

        # 根据日期，计算周线级别某一个交易周的MACD数据
        self.commodity_future_week_data_service.write_week_macd_by_date()

        # 根据日期，计算周线级别某一个交易周的BOLL数据
        self.commodity_future_week_data_service.write_week_boll_by_date()

        # 根据日期，计算周线级别某一个交易周的HA数据
        self.commodity_future_week_data_service.write_week_ha_by_date()
