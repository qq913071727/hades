#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import connection
from web.manager.log_manager import LogManager
from web.config.commodity_future_date_contract_data_config import CommodityFutureDateContractDataConfig
from web.task.base_task import BaseTask

Logger = LogManager.get_logger(__name__)


class WriteDateContractBasicDataByDateTask(BaseTask):

    def do_task(self):
        """
        根据日期，计算日线级别基础数据
        """

        # 根据日期，计算日线级别某一个交易日的last_close_price、rising_and_falling_amount和price_change字段
        self.commodity_future_date_contract_data_service.write_date_contract_basic_data_by_date()

        # 根据日期，计算日线级别某一个交易日的MA数据
        self.commodity_future_date_contract_data_service.write_date_contract_ma_by_date()

        # 根据日期，计算日线级别某一个交易日的MACD数据
        self.commodity_future_date_contract_data_service.write_date_contract_macd_by_date()

        # 根据日期，计算日线级别某一个交易日的KD数据
        self.commodity_future_date_contract_data_service.write_date_contract_kd_by_date()

        # 根据日期，计算日线级别某一个交易日的BOLL数据
        self.commodity_future_date_contract_data_service.write_date_contract_boll_by_date()

        # 根据日期，计算日线级别某一个交易日的Bias数据
        self.commodity_future_date_contract_data_service.write_date_contract_bias_by_date()

        # 根据日期，计算日线级别某一个交易日的hei kin ashi数据
        self.commodity_future_date_contract_data_service.write_date_contract_hei_kin_ashi_by_date()

        # 根据日期，计算日线级别某一个交易日的variance数据
        self.commodity_future_date_contract_data_service.write_date_contract_variance_by_date()

        # 计算日线级别某一日的close_price字段（从这个方法开始就会报错，还没想好要不要计算下面的数据）
        # self.unite_relative_price_index_date_service.write_date_contract_basic_data_by_date()
        #
        # 根据日期，计算日线级别某一个交易日的unite_relative_price_index数据
        # self.commodity_future_date_contract_data_service.write_date_contract_unite_relative_price_index_by_date()
        #
        # 根据日期，计算日线级别，MACD金叉算法
        # self.model_commodity_future_date_contract_data_macd_gold_cross_service.write_date_contract_all_macd_gold_cross_by_date()
        #
        # # 根据日期，计算日线级别，MACD死叉算法
        # self.model_commodity_future_date_contract_data_macd_dead_cross_service.write_date_contract_all_macd_dead_cross_by_date()
        #
        # # 根据日期，计算日线级别，close_price金叉ma5算法
        # self.model_commodity_future_date_contract_data_close_price_ma5_gold_cross_service.calculate_model_commodity_future_date_contract_data_close_price_ma5_gold_cross_increment()
        #
        # # 根据日期，计算日线级别，close_price死叉ma5算法
        # self.model_commodity_future_date_contract_data_close_price_ma5_dead_cross_service.calculate_model_commodity_future_date_contract_data_close_price_ma5_dead_cross_increment()
        #
        # # 根据日期，计算日线级别，kd金叉算法
        # self.model_commodity_future_date_contract_data_kd_gold_cross_service.calculate_model_commodity_future_date_contract_data_kd_gold_cross_increment()
        #
        # # 根据日期，计算日线级别，kd死叉算法
        # self.model_commodity_future_date_contract_data_kd_dead_cross_service.calculate_model_commodity_future_date_contract_data_kd_dead_cross_increment()
        #
        # # 根据日期，计算日线级别某一个交易日的hei_kin_ashi上升趋势算法
        # self.model_commodity_future_date_contract_data_hei_kin_ashi_up_service.calculate_model_commodity_future_date_contract_data_hei_kin_ashi_up_increment()
        #
        # # 根据日期，计算日线级别某一个交易日的hei_kin_ashi下降趋势算法
        # self.model_commodity_future_date_contract_data_hei_kin_ashi_down_service.calculate_model_commodity_future_date_contract_data_hei_kin_ashi_down_increment()
