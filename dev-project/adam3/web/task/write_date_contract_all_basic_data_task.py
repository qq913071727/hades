#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.manager.log_manager import LogManager
from web.task.base_task import BaseTask

Logger = LogManager.get_logger(__name__)

"""
计算日线级别基础数据
"""


class WriteDateContractAllBasicDataTask(BaseTask):

    def do_task(self):
        """
        计算日线级别合约的基础数据
        """

        # 计算日线级别全部last_close_price、rising_and_falling_amount和price_change字段
        self.commodity_future_date_contract_data_service.write_date_basic_data()

        # 计算日线级别全部MA数据
        self.commodity_future_date_contract_data_service.write_date_all_ma()

        # 计算日线级别全部MACD数据
        self.commodity_future_date_contract_data_service.write_date_all_macd()

        # 计算日线级别全部KD数据
        self.commodity_future_date_contract_data_service.write_date_all_kd()

        # 计算日线级别全部boll数据
        self.commodity_future_date_contract_data_service.write_date_all_boll()

        # # 计算日线级别全部hei_kin_ashi数据
        self.commodity_future_date_contract_data_service.write_date_all_hei_kin_ashi()

        # 计算日线级别全部bias数据
        self.commodity_future_date_contract_data_service.write_date_all_bias()

        # 计算日线级别全部variance数据
        self.commodity_future_date_contract_data_service.write_date_all_variance()

        # # 计算日线级别全部unite_relative_price_index数据（从这个开始的，还没想好是否要用）
        # self.commodity_future_date_contract_data_service.write_date_unite_relative_price_index()

        # # 计算日线级别，MACD金叉算法
        # self.model_commodity_future_date_contract_data_macd_gold_cross_service.write_date_all_macd_gold_cross()
        #
        # # 计算日线级别，MACD死叉算法
        # self.model_commodity_future_date_contract_data_macd_dead_cross_service.write_date_all_macd_dead_cross()
        #
        # # 计算日线级别，close_price金叉ma5算法
        # self.model_commodity_future_date_contract_data_close_price_ma5_gold_cross_service.calculate_model_commodity_future_date_contract_data_close_price_ma5_gold_cross()
        #
        # # 计算日线级别，close_price死叉ma5算法
        # self.model_commodity_future_date_contract_data_close_price_ma5_dead_cross_service.calculate_model_commodity_future_date_contract_data_close_price_ma5_dead_cross()
        #
        # # 计算日线级别，hei_kin_ashi上升趋势算法
        # self.model_commodity_future_date_contract_data_hei_kin_ashi_up_service.calculate_model_commodity_future_date_contract_data_hei_kin_ashi_up()
        #
        # # 计算日线级别，hei_kin_ashi下降趋势算法
        # self.model_commodity_future_date_contract_data_hei_kin_ashi_down_service.calculate_model_commodity_future_date_contract_data_hei_kin_ashi_down()
        #
        # # 计算日线级别，kd金叉算法
        # self.model_commodity_future_date_contract_data_kd_gold_cross_service.calculate_model_commodity_future_date_contract_data_kd_gold_cross()
        #
        # # 计算日线级别，kd死叉算法
        # self.model_commodity_future_date_contract_data_kd_dead_cross_service.calculate_model_commodity_future_date_contract_data_kd_dead_cross()
        #
        # # 计算UNITE_RELATIVE_PRICE_INDEX_D表所有的日线级别基础数据
        # self.unite_relative_price_index_date_service.write_date_basic_data()
