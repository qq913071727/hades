#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lxml import etree
import datetime
import re
from web.manager.log_manager import LogManager
from web.models import CommodityFutureExchange
from web.models import CommodityFutureInfo
from web.task.base_task import BaseTask

Logger = LogManager.get_logger(__name__)

"""
解析HTML文件，然后保存商品期货交易保证金的数据
这个任务废弃了，因为手动输入比写程序导入更快
"""


class ParseAndSaveCommodityFutureTradingBondHtmlTask(BaseTask):

    def parse_and_save(self):
        """
        解析html，保存商品期货交易保证金的数据
        """

        Logger.info("解析HTML文件，保存商品期货交易保证金的数据")

        # 解析HTML文件
        tree = etree.parse('C:/mywork/gitcode-repository/hades/dev-project/adam3/resources/commodity_future_trading_bond.html',
                           etree.XMLParser(encoding='utf-8'))
        td_list = tree.xpath('//td')

        if len(td_list) != 0:
            try:
                commodity_future_info = CommodityFutureInfo()
                for index, value in enumerate(td_list):
                    text = value.text
                    if (index + 1) % 8 == 1:
                        # 交易所
                        continue
                    if (index + 1) % 8 == 2:
                        # 名称
                        continue
                    if (index + 1) % 8 == 3:
                        # 代码
                        commodity_future_info.code = text
                        continue
                    if (index + 1) % 8 == 4:
                        # 合约单位
                        pattern = r'[吨/手, 立方米/手, 张/手, 克/手, 千克/手, 元/点, 桶/手, 元/500千克, -]'
                        commodity_future_info.contract_unit = re.sub(pattern, '', text)
                        continue
                    if (index + 1) % 8 == 5:
                        # 最小波动
                        pattern = r'[元/吨, 元/立方米, 元/张, 元/克, 元/千克, 点, 元, 元/桶, -]'
                        commodity_future_info.minimum_fluctuation = re.sub(pattern, '', text)
                        continue
                    if (index + 1) % 8 == 6:
                        # 涨跌幅限制
                        pattern = r'[%, -]'
                        commodity_future_info.price_limit = re.sub(pattern, '', text)
                        continue
                    if (index + 1) % 8 == 7:
                        # 交易所保证金
                        pattern = r'[%, -]'
                        commodity_future_info.exchange_deposit = re.sub(pattern, '', text)
                        continue
                    if (index + 1) % 8 == 0:
                        # 公司保证金
                        pattern = r'[%, -]'
                        commodity_future_info.company_deposit = re.sub(pattern, '', text)

                        CommodityFutureInfo.objects.filter(code=commodity_future_info.code).update(
                            contract_unit=commodity_future_info.contract_unit,
                            minimum_fluctuation=commodity_future_info.minimum_fluctuation,
                            price_limit=commodity_future_info.price_limit,
                            exchange_deposit=commodity_future_info.exchange_deposit,
                            company_deposit=commodity_future_info.company_deposit)
                        commodity_future_info = CommodityFutureInfo()

                        continue
            except:
                Logger.warn(commodity_future_info.code)
