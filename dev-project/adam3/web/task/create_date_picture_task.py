#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.manager.log_manager import LogManager
import matplotlib.pyplot as plt
from web.task.base_task import BaseTask

Logger = LogManager.get_logger(__name__)

"""
创建日线级别图表
"""


class CreateDatePictureTask(BaseTask):

    def create_gold_cross_dead_cross_accumulative_profit_loss_line_chart(self):
        """
        创建MACD金叉死叉算法、close_price金叉死叉ma5算法、kd金叉死叉算法收益率折线图
        """

        Logger.info("创建MACD金叉死叉算法、close_price金叉死叉ma5算法、kd金叉死叉算法收益率折线图")

        self.model_commodity_future_date_data_macd_gold_cross_service.create_and_save_macd_gold_cross_accumulative_profit_loss_line_chart()
        self.model_commodity_future_date_data_macd_dead_cross_service.create_and_save_macd_dead_cross_accumulative_profit_loss_line_chart()
        self.model_commodity_future_date_data_close_price_ma5_gold_cross_service.create_and_save_close_price_ma5_gold_cross_accumulative_profit_loss_line_chart()
        self.model_commodity_future_date_data_close_price_ma5_dead_cross_service.create_and_save_close_price_ma5_dead_cross_accumulative_profit_loss_line_chart()
        self.model_commodity_future_date_data_hei_kin_ashi_up_service.create_and_save_hei_kin_ashi_up_accumulative_profit_loss_line_chart()
        self.model_commodity_future_date_data_hei_kin_ashi_down_service.create_and_save_hei_kin_ashi_down_accumulative_profit_loss_line_chart()
        self.model_commodity_future_date_data_kd_gold_cross_service.create_and_save_kd_gold_cross_accumulative_profit_loss_line_chart()
        self.model_commodity_future_date_data_kd_dead_cross_service.create_and_save_kd_dead_cross_accumulative_profit_loss_line_chart()

    def create_four_strategy_gold_cross_rate_picture(self):
        """
        创建图片：根据四种算法，计算各自金叉百分比，生成折线图
        """

        Logger.info("创建图片：根据四种算法，计算各自金叉百分比，生成折线图")

        self.commodity_future_date_data_service.create_four_strategy_gold_cross_rate_picture()

    def create_average_four_strategy_gold_cross_rate_and_its_and_ma5_picture(self):
        """
        创建图片：根据四种算法，计算各自金叉百分比，再计算平均值，这是第一条线。计算这个数据的MA5，这是第二条线。两条线一起生成折线图
        """

        Logger.info("创建图片：根据四种算法，计算各自金叉百分比，再计算平均值，这是第一条线。计算这个数据的MA5，这是第二条线。两条线一起生成折线图")

        self.commodity_future_date_data_service.create_average_four_strategy_gold_cross_rate_and_its_and_ma5_picture()

    def create_unique_relative_price_index_picture(self):
        """
        创建图片：统一价格指数图。把第一个交易日的品种的初始价格设置为100，之后的交易日上市交易的品种，初始价格为前一个交易日平均unique_close_price_index，
        直到计算到最后一个交易日，再计算每天上市交易的品种的平均价格
        """

        Logger.info("创建图片：统一价格指数图。把第一个交易日的品种的初始价格设置为100，之后的交易日上市交易的品种，初始价格为前一个交易日平均unique_close_price_index，"
                    "直到计算到最后一个交易日，再计算每天上市交易的品种的平均价格")

        self.unite_relative_price_index_date_service.create_unique_relative_price_index_picture()

    def create_price_change_up_percentage_picture(self):
        """
        创建图片：日线级别上涨的期货的百分比
        """

        Logger.info("创建图片：日线级别上涨的期货的百分比")

        self.commodity_future_date_data_service.create_price_change_up_percentage_picture()

    def create_bull_short_line_picture(self):
        """
        创建图片：每个期货的牛熊线图
        """

        Logger.info("创建图片：每个期货的牛熊线图")

        self.commodity_future_date_data_service.create_bull_short_line_picture()

    def create_average_kd_picture(self):
        """
        创建图片：日线级别平均KD图
        """

        Logger.info("创建图片：日线级别平均KD图")

        self.commodity_future_date_data_service.create_average_kd_picture()

    def create_average_boll_picture(self):
        """
        创建图片：日线级别平均BOLL图
        """

        Logger.info("创建图片：日线级别平均BOLL图")

        self.commodity_future_date_data_service.create_average_boll_picture()
