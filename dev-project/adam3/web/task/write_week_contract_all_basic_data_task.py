#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.manager.log_manager import LogManager
from web.task.base_task import BaseTask

Logger = LogManager.get_logger(__name__)

"""
计算周线级别基础数据（具体合约）
"""


class WriteWeekContractAllBasicDataTask(BaseTask):

    def do_task(self):
        """
        计算周线级别基础数据
        """

        # 计算周线级别基础数据
        self.commodity_future_week_contract_data_service.write_week_contract_basic_data()

        # 计算周线级别全部MA数据
        self.commodity_future_week_contract_data_service.write_week_contract_all_ma()

        # 计算周线级别全部KD数据
        self.commodity_future_week_contract_data_service.write_week_contract_all_kd()

        # 计算周线级别全部MACD数据
        self.commodity_future_week_contract_data_service.write_week_contract_all_macd()

        # 计算周线级别全部BOLL数据
        self.commodity_future_week_contract_data_service.write_week_contract_all_boll()

        # 计算周线级别全部HA数据
        self.commodity_future_week_contract_data_service.write_week_contract_all_ha()

