#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.manager.log_manager import LogManager
import matplotlib.pyplot as plt
from web.task.base_task import BaseTask

Logger = LogManager.get_logger(__name__)

"""
创建周线级别图表
"""


class CreateWeekPictureTask(BaseTask):

    def create_price_change_up_percentage_picture(self):
        """
        创建图片：周线级别上涨的期货的百分比
        """

        Logger.info("创建图片：周线级别上涨的期货的百分比")

        self.commodity_future_week_data_service.create_price_change_up_percentage_picture()

    def create_week_average_kd_picture(self):
        """
        创建图片：根据开始时间和结束时间，周线级别平均KD
        """

        Logger.info("创建图片：根据开始时间和结束时间，周线级别平均KD")

        self.commodity_future_week_data_service.create_week_average_kd_picture()
