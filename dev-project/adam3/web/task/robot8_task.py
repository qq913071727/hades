#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.manager.log_manager import LogManager
from web.task.base_task import BaseTask
from web.config.robot8_config import Robot8Config

Logger = LogManager.get_logger(__name__)

"""
robot8测试
"""


class Robot8Task(BaseTask):

    def do_task(self):
        """
        robot8测试
        """

        Logger.info("开始robot8测试")

        # 测试
        self.robot8_commodity_future_transact_record_service.do_buy_and_sell_by_begin_date_and_end_date()
        # 操作系统睡眠
        # self.robot8_commodity_future_transact_record_service.make_operation_sleep()

        Logger.info('robot8测试完成')
