#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.manager.log_manager import LogManager
from web.task.base_task import BaseTask

Logger = LogManager.get_logger(__name__)

"""
添加注释
"""


class AddColumnCommentTask(BaseTask):

    def add_column_comment(self):
        """
        添加注释
        """

        Logger.info('开始更新数据库注释')

        # 为表commodity_future_exchange添加注释
        self.commodity_future_exchange_service.add_oracle_column_comment()

        # 为表commodity_future_info添加注释
        self.commodity_future_info_service.add_oracle_column_comment()

        # 为表commodity_future_date_data添加注释
        self.commodity_future_date_data_service.add_oracle_column_comment()

        # 为表commodity_future_hour_data添加注释
        self.commodity_future_hour_data_service.add_oracle_column_comment()

        # 为表commodity_future_minute_data添加注释
        self.commodity_future_minute_data_service.add_oracle_column_comment()

        # 为表mdl_c_f_date_macd_gold_cross添加注释
        self.model_commodity_future_date_data_macd_gold_cross_service.add_oracle_column_comment()

        # 为表mdl_c_f_date_macd_dead_cross添加注释
        self.model_commodity_future_date_data_macd_dead_cross_service.add_oracle_column_comment()

        Logger.info('更新数据库注释完成')
