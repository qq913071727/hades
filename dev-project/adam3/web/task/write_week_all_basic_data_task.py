#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.manager.log_manager import LogManager
from web.task.base_task import BaseTask

Logger = LogManager.get_logger(__name__)

"""
计算周线级别基础数据
"""


class WriteWeekAllBasicDataTask(BaseTask):

    def do_task(self):
        """
        计算周线级别基础数据
        """

        # 计算周线级别基础数据
        self.commodity_future_week_data_service.write_week_basic_data()

        # 计算周线级别全部MA数据
        self.commodity_future_week_data_service.write_week_all_ma()

        # 计算周线级别全部KD数据
        self.commodity_future_week_data_service.write_week_all_kd()

        # 计算周线级别全部MACD数据
        self.commodity_future_week_data_service.write_week_all_macd()

        # 计算周线级别全部BOLL数据
        self.commodity_future_week_data_service.write_week_all_boll()

        # 计算周线级别全部HA数据
        self.commodity_future_week_data_service.write_week_all_ha()

