#!/usr/bin/env python
# -*- coding: utf-8 -*-

from apscheduler.schedulers.blocking import BlockingScheduler
from web.service.commodity_future_info_service import CommodityFutureInfoService
from web.service.commodity_future_hour_data_service import CommodityFutureHourDataService
from web.service.commodity_future_date_data_service import CommodityFutureDateDataService
from web.service.commodity_future_minute_data_service import CommodityFutureMinuteDataService
from web.service.commodity_future_exchange_service import CommodityFutureExchangeService
from web.service.commodity_future_week_data_service import CommodityFutureWeekDataService
from web.service.commodity_future_date_contract_data_service import CommodityFutureDateContractDataService
from web.service.commodity_future_week_contract_data_service import CommodityFutureWeekContractDataService
from web.service.model_commodity_future_date_data_macd_gold_cross_service import \
    ModelCommodityFutureDateDataMACDGoldCrossService
from web.service.model_commodity_future_date_data_macd_dead_cross_service import \
    ModelCommodityFutureDateDataMACDDeadCrossService
from web.service.model_commodity_future_date_data_close_price_ma5_gold_cross_service import \
    ModelCommodityFutureDateDataClosePriceMA5GoldCrossService
from web.service.model_commodity_future_date_data_close_price_ma5_dead_cross_service import \
    ModelCommodityFutureDateDataClosePriceMA5DeadCrossService
from web.service.model_commodity_future_date_data_hei_kin_ashi_up_service import \
    ModelCommodityFutureDateDataHeiKinAshiUpService
from web.service.model_commodity_future_date_data_hei_kin_ashi_down_service import \
    ModelCommodityFutureDateDataHeiKinAshiDownService
from web.service.model_commodity_future_date_data_kd_gold_cross_service import \
    ModelCommodityFutureDateDataKDGoldCrossService
from web.service.model_commodity_future_date_data_kd_dead_cross_service import \
    ModelCommodityFutureDateDataKDDeadCrossService
# from web.service.model_hmm_service import ModelHmmService
from web.service.robot8_commodity_future_transact_record_service import Robot8CommodityFutureTransactRecordService
from web.service.unite_relative_price_index_date_service import UniteRelativePriceIndexDateService
from web.service.tianqin_exclude_code_service import TianqinExcludeCodeService

"""
Task基类
"""


class BaseTask:

    def __init__(self) -> None:
        super().__init__()
        self.blocking_scheduler = BlockingScheduler()

        self.commodity_future_exchange_service = CommodityFutureExchangeService()
        self.commodity_future_info_service = CommodityFutureInfoService()
        self.commodity_future_date_data_service = CommodityFutureDateDataService()
        self.commodity_future_hour_data_service = CommodityFutureHourDataService()
        self.commodity_future_minute_data_service = CommodityFutureMinuteDataService()

        self.model_commodity_future_date_data_macd_gold_cross_service = ModelCommodityFutureDateDataMACDGoldCrossService()
        self.model_commodity_future_date_data_macd_dead_cross_service = ModelCommodityFutureDateDataMACDDeadCrossService()
        self.model_commodity_future_date_data_close_price_ma5_gold_cross_service = ModelCommodityFutureDateDataClosePriceMA5GoldCrossService()
        self.model_commodity_future_date_data_close_price_ma5_dead_cross_service = ModelCommodityFutureDateDataClosePriceMA5DeadCrossService()
        self.model_commodity_future_date_data_hei_kin_ashi_up_service = ModelCommodityFutureDateDataHeiKinAshiUpService()
        self.model_commodity_future_date_data_hei_kin_ashi_down_service = ModelCommodityFutureDateDataHeiKinAshiDownService()
        self.model_commodity_future_date_data_kd_gold_cross_service = ModelCommodityFutureDateDataKDGoldCrossService()
        self.model_commodity_future_date_data_kd_dead_cross_service = ModelCommodityFutureDateDataKDDeadCrossService()

        # self.model_hmm_service = ModelHmmService()

        self.robot8_commodity_future_transact_record_service = Robot8CommodityFutureTransactRecordService()

        self.unite_relative_price_index_date_service = UniteRelativePriceIndexDateService()

        self.commodity_future_week_data_service = CommodityFutureWeekDataService()

        self.commodity_future_date_contract_data_service = CommodityFutureDateContractDataService()
        self.commodity_future_week_contract_data_service = CommodityFutureWeekContractDataService()

        self.tianqin_exclude_code_service = TianqinExcludeCodeService()
