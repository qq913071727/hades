#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.manager.log_manager import LogManager
import matplotlib.pyplot as plt
from web.task.base_task import BaseTask

Logger = LogManager.get_logger(__name__)


class CreateDateContractPictureTask(BaseTask):
    """
    创建日线级别具体合约图表
    """

    def create_different_contract_compare_picture(self):
        """
        创建图片：同一品种多个不同合约的比较图
        """

        Logger.info("创建图片：同一品种多个不同合约的比较图")

        self.commodity_future_date_contract_data_service.create_different_contract_compare_picture()
