#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lxml import etree
import datetime
from web.manager.log_manager import LogManager
from web.models import CommodityFutureExchange
from web.models import CommodityFutureInfo
from web.task.base_task import BaseTask

Logger = LogManager.get_logger(__name__)

"""
解析HTML文件，然后保存商品期货交易时间的数据
"""


class ParseAndSaveCommodityFutureTradingHourHtmlTask(BaseTask):

    def parse_and_save(self):
        """
        解析html，保存商品期货交易时间的数据
        """

        Logger.info("解析HTML文件，保存商品期货交易时间的数据")

        # 解析HTML文件
        tree = etree.parse('C:/mywork/gitcode-repository/hades/dev-project/adam3/resources/commodity_future_trading_hour.html',
                           etree.XMLParser(encoding='utf-8'))
        td_list = tree.xpath('//td')

        if len(td_list) != 0:
            commodity_future_info = CommodityFutureInfo()
            for index, value in enumerate(td_list):
                text = value.text
                if (index + 1) % 5 == 1:
                    # 查询期货交易所
                    commodity_future_exchange = CommodityFutureExchange.objects.filter(name=text).get()
                    commodity_future_info.commodity_future_exchange_id = commodity_future_exchange.id
                    continue
                if (index + 1) % 5 == 2:
                    commodity_future_info.name = text
                    continue
                if (index + 1) % 5 == 3:
                    commodity_future_info.code = text
                    continue
                if (index + 1) % 5 == 4:
                    if text == '无':
                        commodity_future_info.night_trading_begin_hour = None
                        commodity_future_info.night_trading_end_hour = None
                    else:
                        time_list = text.split('-')
                        begin_time = time_list[0]
                        end_time = time_list[1]
                        if end_time.startswith('次日'):
                            end_time = end_time[2:]
                        commodity_future_info.night_trading_begin_hour = datetime.datetime.strptime(
                            '1900-01-01 ' + begin_time + ':00', '%Y-%m-%d %H:%M:%S')
                        commodity_future_info.night_trading_end_hour = datetime.datetime.strptime(
                            '1900-01-01 ' + end_time + ':00', '%Y-%m-%d %H:%M:%S')
                    continue
                if (index + 1) % 5 == 0:
                    time_list = text.split(' ')
                    if time_list[0].startswith('上午') or time_list[0].startswith('下午'):
                        begin_time = time_list[0][3:].split('-')[0]
                        end_time = time_list[0][3:].split('-')[1]
                        commodity_future_info.daily_trading_1_begin_hour = datetime.datetime.strptime(
                            '1900-01-01 ' + begin_time + ':00', '%Y-%m-%d %H:%M:%S')
                        commodity_future_info.daily_trading_1_end_hour = datetime.datetime.strptime(
                            '1900-01-01 ' + end_time + ':00', '%Y-%m-%d %H:%M:%S')
                    else:
                        begin_time = time_list[0].split('-')[0]
                        end_time = time_list[0].split('-')[1]
                        commodity_future_info.daily_trading_1_begin_hour = datetime.datetime.strptime(
                            '1900-01-01 ' + begin_time + ':00', '%Y-%m-%d %H:%M:%S')
                        commodity_future_info.daily_trading_1_end_hour = datetime.datetime.strptime(
                            '1900-01-01 ' + end_time + ':00', '%Y-%m-%d %H:%M:%S')
                    if len(time_list) == 2:
                        if time_list[1].startswith('上午') or time_list[1].startswith('下午'):
                            begin_time = time_list[1][3:].split('-')[0]
                            end_time = time_list[1][3:].split('-')[1]
                            commodity_future_info.daily_trading_2_begin_hour = datetime.datetime.strptime(
                                '1900-01-01 ' + begin_time + ':00', '%Y-%m-%d %H:%M:%S')
                            commodity_future_info.daily_trading_2_end_hour = datetime.datetime.strptime(
                                '1900-01-01 ' + end_time + ':00', '%Y-%m-%d %H:%M:%S')
                        else:
                            begin_time = time_list[1].split('-')[0]
                            end_time = time_list[1].split('-')[1]
                            commodity_future_info.daily_trading_2_begin_hour = datetime.datetime.strptime(
                                '1900-01-01 ' + begin_time + ':00', '%Y-%m-%d %H:%M:%S')
                            commodity_future_info.daily_trading_2_end_hour = datetime.datetime.strptime(
                                '1900-01-01 ' + end_time + ':00', '%Y-%m-%d %H:%M:%S')
                    if len(time_list) == 3:
                        if time_list[1].startswith('上午') or time_list[1].startswith('下午'):
                            begin_time = time_list[1][3:].split('-')[0]
                            end_time = time_list[1][3:].split('-')[1]
                            commodity_future_info.daily_trading_2_begin_hour = datetime.datetime.strptime(
                                '1900-01-01 ' + begin_time + ':00', '%Y-%m-%d %H:%M:%S')
                            commodity_future_info.daily_trading_2_end_hour = datetime.datetime.strptime(
                                '1900-01-01 ' + end_time + ':00', '%Y-%m-%d %H:%M:%S')
                        else:
                            begin_time = time_list[1].split('-')[0]
                            end_time = time_list[1].split('-')[1]
                            commodity_future_info.daily_trading_2_begin_hour = datetime.datetime.strptime(
                                '1900-01-01 ' + begin_time + ':00', '%Y-%m-%d %H:%M:%S')
                            commodity_future_info.daily_trading_2_end_hour = datetime.datetime.strptime(
                                '1900-01-01 ' + end_time + ':00', '%Y-%m-%d %H:%M:%S')
                        if time_list[2].startswith('上午') or time_list[2].startswith('下午'):
                            begin_time = time_list[2][3:].split('-')[0]
                            end_time = time_list[2][3:].split('-')[1]
                            commodity_future_info.daily_trading_3_begin_hour = datetime.datetime.strptime(
                                '1900-01-01 ' + begin_time + ':00', '%Y-%m-%d %H:%M:%S')
                            commodity_future_info.daily_trading_3_end_hour = datetime.datetime.strptime(
                                '1900-01-01 ' + end_time + ':00', '%Y-%m-%d %H:%M:%S')
                        else:
                            begin_time = time_list[2].split('-')[0]
                            end_time = time_list[2].split('-')[1]
                            commodity_future_info.daily_trading_3_begin_hour = datetime.datetime.strptime(
                                '1900-01-01 ' + begin_time + ':00', '%Y-%m-%d %H:%M:%S')
                            commodity_future_info.daily_trading_3_end_hour = datetime.datetime.strptime(
                                '1900-01-01 ' + end_time + ':00', '%Y-%m-%d %H:%M:%S')
                    commodity_future_info.save()
                    commodity_future_info = CommodityFutureI