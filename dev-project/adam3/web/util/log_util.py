#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

"""
log工具类
"""


class LogUtil:

    @staticmethod
    def extract_commodity_future_date_contract_data_code_from_log_file(log_file):
        """
        从日志文件中提取期货代码
        """

        exclude_code_list = list()
        with open(log_file, 'r', encoding='UTF-8') as file:
            line_list = file.readlines()

            pattern = r'contains non-existent instrument: (\w.+)'  # 匹配一个或多个数字
            for line in line_list:
                result_list = re.findall(pattern, line)
                if result_list != None and len(result_list) > 0:
                    for result in result_list:
                        result = result[0:len(result) - 1]
                        if result != None:
                            exclude_code_list.append(result)

        return exclude_code_list
