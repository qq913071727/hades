#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

import numpy as np
from decimal import Decimal

"""
数学工具类
"""


class MathUtil:

    @staticmethod
    def normalize(data_array) -> list:
        """
        归一化
        """

        data = np.array([data_array])
        max_val = np.max(data)
        min_val = np.min(data)

        # 对数据进行归一化处理
        normalized_data = (data - min_val) / (max_val - min_val)
        return normalized_data.tolist()

    @staticmethod
    def deviate_grade_by_mean_and_standard(data_list, choice):
        """
        均值标准差分级
        """

        mean = np.mean(data_list, axis=0)
        standard = np.std(data_list)
        ret = list()
        for i in range(int(choice / 2)):
            ret.insert(0, mean - Decimal((i + ((choice % 2) / 2))) * standard)
            ret.append(mean + Decimal((i + ((choice % 2) / 2))) * standard)
        ret.insert(0, 0)
        ret.append(+np.inf)
        ret = list(set(ret))
        ret.sort()
        return ret

    @staticmethod
    def generate_random_color():
        """
        创建16进制随机颜色
        """

        color_list = ["#" + "".join([random.choice("0123456789ABCDEF") for j in range(6)])]
        return color_list[0]
