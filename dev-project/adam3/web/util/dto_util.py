#!/usr/bin/env python

from web.models.commodity_future_date_contract_data import CommodityFutureDateContractData
from web.models.commodity_future_hour_data import CommodityFutureHourData
from web.models.commodity_future_minute_data import CommodityFutureMinuteData

"""
dto工具类
"""


class DtoUtil:

    @staticmethod
    def commodity_future_date_contract_data_dto_to_commodity_future_date_contract_data(
            commodity_future_date_contract_data_dto):
        """
        将CommodityFutureDateContractDataDto类型对象转换为CommodityFutureDateContractData类型对象
        """
        if commodity_future_date_contract_data_dto is not None and len(commodity_future_date_contract_data_dto):
            _commodity_future_date_contract_data = CommodityFutureDateContractData()
            if commodity_future_date_contract_data_dto[0] is not None:
                _commodity_future_date_contract_data.code = commodity_future_date_contract_data_dto[0]
            if commodity_future_date_contract_data_dto[1] is not None:
                _commodity_future_date_contract_data.open_price = commodity_future_date_contract_data_dto[1]
            if commodity_future_date_contract_data_dto[2] is not None:
                _commodity_future_date_contract_data.highest_price = commodity_future_date_contract_data_dto[2]
            if commodity_future_date_contract_data_dto[3] is not None:
                _commodity_future_date_contract_data.lowest_price = commodity_future_date_contract_data_dto[3]
            if commodity_future_date_contract_data_dto[4] is not None:
                _commodity_future_date_contract_data.close_price = commodity_future_date_contract_data_dto[4]
            if commodity_future_date_contract_data_dto[5] is not None:
                _commodity_future_date_contract_data.volume = commodity_future_date_contract_data_dto[5]
            if commodity_future_date_contract_data_dto[6] is not None:
                _commodity_future_date_contract_data.turnover = commodity_future_date_contract_data_dto[6]
            if commodity_future_date_contract_data_dto[7] is not None:
                _commodity_future_date_contract_data.open_interest = commodity_future_date_contract_data_dto[7]
            if commodity_future_date_contract_data_dto[8] is not None:
                _commodity_future_date_contract_data.transaction_date = commodity_future_date_contract_data_dto[8]
            return _commodity_future_date_contract_data
        else:
            return None

    @staticmethod
    def commodity_future_data_dto_to_commodity_future_hour_data(
            commodity_future_data_dto):
        """
        将CommodityFutureDataDto类型对象转换为commodity_future_hour_data类型对象
        """
        if commodity_future_data_dto:
            _commodity_future_hour_data = CommodityFutureHourData()
            if commodity_future_data_dto.np is not None:
                _commodity_future_hour_data.selling = commodity_future_data_dto.np
            if commodity_future_data_dto.h is not None:
                _commodity_future_hour_data.highest_price = commodity_future_data_dto.h
            if commodity_future_data_dto.dm is not None:
                _commodity_future_hour_data.code = commodity_future_data_dto.dm
            if commodity_future_data_dto.l is not None:
                _commodity_future_hour_data.lowest_price = commodity_future_data_dto.l
            if commodity_future_data_dto.ccl is not None:
                _commodity_future_hour_data.open_interest = commodity_future_data_dto.ccl
            if commodity_future_data_dto.o is not None:
                _commodity_future_hour_data.open_price = commodity_future_data_dto.o
            if commodity_future_data_dto.p is not None:
                _commodity_future_hour_data.close_price = commodity_future_data_dto.p
            if commodity_future_data_dto.vol is not None:
                _commodity_future_hour_data.turnover = commodity_future_data_dto.vol
            if commodity_future_data_dto.name is not None:
                _commodity_future_hour_data.name = commodity_future_data_dto.name
            if commodity_future_data_dto.wp is not None:
                _commodity_future_hour_data.buying = commodity_future_data_dto.wp
            if commodity_future_data_dto.zde is not None:
                _commodity_future_hour_data.rising_and_falling_amount = commodity_future_data_dto.zde
            if commodity_future_data_dto.zdf is not None:
                _commodity_future_hour_data.price_change = commodity_future_data_dto.zdf
            if commodity_future_data_dto.zjsj is not None:
                _commodity_future_hour_data.last_close_price = commodity_future_data_dto.zjsj
            if commodity_future_data_dto.cje is not None:
                _commodity_future_hour_data.volume = commodity_future_data_dto.cje
            return _commodity_future_hour_data
        else:
            return None

    @staticmethod
    def commodity_future_data_dto_to_commodity_future_minute_data(
            commodity_future_data_dto):
        """
        将CommodityFutureDataDto类型对象转换为commodity_future_minute_data类型对象
        """
        if commodity_future_data_dto:
            _commodity_future_minute_data = CommodityFutureMinuteData()
            if commodity_future_data_dto.np is not None:
                _commodity_future_minute_data.selling = commodity_future_data_dto.np
            if commodity_future_data_dto.h is not None:
                _commodity_future_minute_data.highest_price = commodity_future_data_dto.h
            if commodity_future_data_dto.dm is not None:
                _commodity_future_minute_data.code = commodity_future_data_dto.dm
            if commodity_future_data_dto.l is not None:
                _commodity_future_minute_data.lowest_price = commodity_future_data_dto.l
            if commodity_future_data_dto.ccl is not None:
                _commodity_future_minute_data.open_interest = commodity_future_data_dto.ccl
            if commodity_future_data_dto.o is not None:
                _commodity_future_minute_data.open_price = commodity_future_data_dto.o
            if commodity_future_data_dto.p is not None:
                _commodity_future_minute_data.close_price = commodity_future_data_dto.p
            if commodity_future_data_dto.vol is not None:
                _commodity_future_minute_data.turnover = commodity_future_data_dto.vol
            if commodity_future_data_dto.name is not None:
                _commodity_future_minute_data.name = commodity_future_data_dto.name
            if commodity_future_data_dto.wp is not None:
                _commodity_future_minute_data.buying = commodity_future_data_dto.wp
            if commodity_future_data_dto.zde is not None:
                _commodity_future_minute_data.rising_and_falling_amount = commodity_future_data_dto.zde
            if commodity_future_data_dto.zdf is not None:
                _commodity_future_minute_data.price_change = commodity_future_data_dto.zdf
            if commodity_future_data_dto.zjsj is not None:
                _commodity_future_minute_data.last_close_price = commodity_future_data_dto.zjsj
            if commodity_future_data_dto.cje is not None:
                _commodity_future_minute_data
