#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.constants.calculation_method import CalculationMethod

"""
计算交易费用的工具类
"""


class FeeUtil:

    @staticmethod
    def calculate_open_commodity_future_cost_and_fee(close_price, commodity_future_info, lot):
        """
        计算期货开仓时的全部费用，包括：开仓价格和手续费
        """

        # 固定值计算
        if commodity_future_info.calculation_method == CalculationMethod.Fixed_Value:
            fee = close_price * commodity_future_info.transaction_multiplier * commodity_future_info.company_deposit * lot + \
                  lot * commodity_future_info.open_position
            return fee

        # 费率值计算
        if commodity_future_info.calculation_method == CalculationMethod.Rate_Value:
            fee = close_price * commodity_future_info.transaction_multiplier * commodity_future_info.company_deposit * lot + \
                  close_price * commodity_future_info.transaction_multiplier * commodity_future_info.company_deposit * lot * commodity_future_info.open_position
            return fee

    @staticmethod
    def calculate_open_commodity_future_fee(close_price, commodity_future_info, lot):
        """
        计算期货开仓时的手续费
        """

        # 固定值计算
        if commodity_future_info.calculation_method == CalculationMethod.Fixed_Value:
            return lot * commodity_future_info.open_position

        # 费率值计算
        if commodity_future_info.calculation_method == CalculationMethod.Rate_Value:
            return close_price * commodity_future_info.transaction_multiplier * commodity_future_info.company_deposit * lot * commodity_future_info.open_position

    @staticmethod
    def calculate_close_today_commodity_future_fee(close_price, commodity_future_info, lot):
        """
        计算期货平今仓时的手续费
        """

        # 固定值计算
        if commodity_future_info.calculation_method == CalculationMethod.Fixed_Value:
            fee = lot * commodity_future_info.close_today_position
            return fee

        # 费率值计算
        if commodity_future_info.calculation_method == CalculationMethod.Rate_Value:
            fee = close_price * commodity_future_info.transaction_multiplier * commodity_future_info.company_deposit * lot * commodity_future_info.close_today_position
            return fee

    @staticmethod
    def calculate_close_old_commodity_future_fee(close_price, commodity_future_info, lot):
        """
        计算期货平老仓时的手续费
        """

        # 固定值计算
        if commodity_future_info.calculation_method == CalculationMethod.Fixed_Value:
            fee = lot * commodity_future_info.close_old_position
            return fee

        # 费率值计算
        if commodity_future_info.calculation_method == CalculationMethod.Rate_Value:
            fee = close_price * commodity_future_info.transaction_multiplier * commodity_future_info.company_deposit * lot * commodity_future_info.close_old_position
            return fee
