# coding:utf-8

import os
import tarfile
import sys
import shutil

sys.path.append(r".")

from web.constants.file_and_path import FileAndPath


class FileUtil:

    @staticmethod
    def un_tar(file_name):
        """
        untar
        zip
        file
        """
        tar = tarfile.open(file_name)
        names = tar.getnames()
        if os.path.isdir(file_name + "_files"):
            pass
        else:
            os.mkdir(file_name + "_files")
        # 因为解压后是很多文件，预先建立同名目录
        for name in names:
            tar.extract(name, file_name + "_files/")
        tar.close()

    def del_file(filepath):
        """
        删除某一目录下的所有文件或文件夹
        :param filepath: 路径
        :return:
        """
        del_list = os.listdir(filepath)
        for f in del_list:
            file_path = os.path.join(filepath, f)
            if os.path.isfile(file_path):
                os.remove(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)


if __name__ == "__main__":
    FileUtil.un_tar(
        FileAndPath.System_Drive + 'C:/mywork/gitcode-repository/hades/dev-project/adam3/dist/adam3-1.0.tar.gz')
