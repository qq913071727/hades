#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
from scipy.stats import boxcox
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt


class PictureUtil:
    """
    图片工具类
    """

    @staticmethod
    def create_histogram_picture(title: str, save_path: str, data_param, box_cox=False):
        """
        生成直方图
        """

        # 对数组进行Box-Cox转换
        if box_cox:
            # 注意：这个地方如果不加一，有时会报错：ValueError: Data must be positive.
            data_param, _ = boxcox(np.array(data_param).astype('float') + 1, lmbda=None, alpha=None)

        plt.style.use('ggplot')
        # 处理中文乱码
        plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']
        # 坐标轴负号的处理
        plt.rcParams['axes.unicode_minus'] = False
        # 绘制直方图
        plt.hist(x=data_param,  # 指定绘图数据
                 bins=100,  # 指定直方图中条块的个数
                 color='steelblue',  # 指定直方图的填充色
                 edgecolor='black'  # 指定直方图的边框色
                 )
        # 添加x轴和y轴标签
        plt.xlabel('数据类型')
        plt.ylabel('分布情况')
        # 添加标题
        plt.title(title)
        # 显示图形
        plt.savefig(save_path)
        fig = plt.gcf()  # 获取当前figure
        plt.close(fig)  # 关闭传入的 figure 对象

        return
