#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
from web.constants.datetime_format import DatetimeFormat

"""
datetime工具类
"""


class DatetimeUtil:

    @staticmethod
    def str_to_datetime(datetime_str, format=DatetimeFormat.Datetime_Format):
        """
        将str对象转换为datetime对象
        """

        return datetime.datetime.strptime(datetime_str, format)

    @staticmethod
    def datetime_to_str(str_datetime, format=DatetimeFormat.Datetime_Format):
        """
        将datetime对象转换为str对象
        """
        return str_datetime.strftime(format)

    @staticmethod
    def filter_last_two_char_year(year_list):
        """
        把四位的年转换为两位的年
        """

        return str(year_list)[2:]
