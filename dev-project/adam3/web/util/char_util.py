#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
字符工具类
"""


class CharUtil:

    @staticmethod
    def extract_alpha(_str):
        """
        从一个字符串中提取字母
        """

        new_str = list()
        for s in _str:
            if s.isalpha():
                new_str.append(s)
        return ' '.join(new_str)

    @staticmethod
    def extract_numbers(_str):
        """
        从一个字符串中提取数字
        """

        return ''.join([_s for _s in _str if _s.isdigit()])

    @staticmethod
    def remove_spaces(_str):
        """
        去除字符串中的空格
        """

        return "".join(_str.split())
