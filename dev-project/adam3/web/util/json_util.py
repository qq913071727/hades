#!/usr/bin/env python

from typing import Any
import json


class JsonUtil():
    """
    json工具类
    """

    @staticmethod
    def json_to_class(json_string: str, clazz: Any) -> Any:
        """
        将json字符串转换为clazz类型对象
        """
        data_dict = json.loads(json_string)
        result = clazz(data_dict)
        return result
