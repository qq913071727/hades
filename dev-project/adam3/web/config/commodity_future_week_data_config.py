#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
计算commodity_future_week_data表的基础数据
"""


class CommodityFutureWeekDataConfig:
    ######################### 计算commodity_future_week_data表的基础数据 #######################
    # 计算周线级别，某一个交易周，所有期货的基础数据
    # 开始时间
    Commodity_Future_Week_Data_By_Begin_Date = '20250303'
    # 结束时间
    Commodity_Future_Week_Data_By_End_Date = '20250307'

    # 计算周线级别，某一个交易周，所有期货的MA
    # 开始时间
    Commodity_Future_Week_Data_Ma_By_Begin_Date = '20250303'
    # 结束时间
    Commodity_Future_Week_Data_Ma_By_End_Date = '20250307'

    # 计算周线级别，某一个交易周，所有期货的KD
    # 开始时间
    Commodity_Future_Week_Data_Kd_By_Begin_Date = '20250303'
    # 结束时间
    Commodity_Future_Week_Data_Kd_By_End_Date = '20250307'

    # 计算周线级别，某一个交易周，所有期货的MACD
    # 开始时间
    Commodity_Future_Week_Data_Macd_By_Begin_Date = '20250303'
    # 结束时间
    Commodity_Future_Week_Data_Macd_By_End_Date = '20250307'

    # 计算周线级别，某一个交易周，所有期货的BOLL
    # 开始时间
    Commodity_Future_Week_Data_Boll_By_Begin_Date = '20250303'
    # 结束时间
    Commodity_Future_Week_Data_Boll_By_End_Date = '20250307'

    # 计算周线级别，某一个交易周，所有期货的HA
    # 开始时间
    Commodity_Future_Week_Data_Ha_By_Begin_Date = '20250303'
    # 结束时间
    Commodity_Future_Week_Data_Ha_By_End_Date = '20250307'
