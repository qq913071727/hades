#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
创建图表的配置
"""


class CreateWeekPictureConfig:
    ################################# 创建图片：周线级别上涨的期货的百分比 #################################
    # 开始时间
    Price_Change_Up_Percentage_Begin_Date = '20240307'

    # 结束时间
    Price_Change_Up_Percentage_End_Date = '20250307'

    # 图片保存的位置
    Price_Change_Up_Percentage_Save_Path = 'output/picture/week/price_change_up_percentage/' + \
                                           Price_Change_Up_Percentage_Begin_Date + '_' + Price_Change_Up_Percentage_End_Date + '.png'

    ################################# 创建图片：周线级别平均KD #################################
    # 开始时间
    Average_KD_Begin_Date = '20240307'

    # 结束时间
    Average_KD_End_Date = '20250307'

    # 图片保存的位置
    Average_KD_Save_Path = 'output/picture/week/average_kd/' + \
                                           Average_KD_Begin_Date + '_' + Average_KD_End_Date + '.png'
