#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
计算mdl_c_f_date_***表的基础数据
"""


class ModelCommodityFutureDateDataConfig:
    # 根据日期，计算日线级别，MACD金叉算法
    MACD_Gold_Cross_Date = '20250307'

    # 根据日期，计算日线级别，MACD死叉算法
    MACD_Dead_Cross_Date = '20250307'

    # 根据日期，计算日线级别，close_price金叉ma5算法
    Close_Price_MA5_Gold_Cross_Date = '20250307'

    # 根据日期，计算日线级别，close_price死叉ma5算法
    Close_Price_MA5_Dead_Cross_Date = '20250307'

    # 根据日期，计算日线级别，hei_kin_ashi上升趋势算法
    Hei_Kin_Ashi_Up_Date = '20250307'

    # 根据日期，计算日线级别，hei_kin_ashi下跌趋势算法
    Hei_Kin_Ashi_Down_Date = '20250307'

    # 根据日期，计算日线级别，kd金叉算法
    KD_Gold_Cross_Date = '20250307'

    # 根据日期，计算日线级别，kd死叉算法
    KD_Dead_Cross_Date = '20250307'
