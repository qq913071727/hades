#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
robot8测试的配置文件
"""


class Robot8Config:
    # 开始时间
    Begin_Date = '20110101'

    # 结束时间
    End_Date = '20240620'

    # 总资金
    Total_Fund = 500000

    # 最大持有期货的数量
    Max_Holding_Comodity_Future_Number = 10

    # 总资金中用于交易的资金比例
    Fund_Proportion_For_Trading_In_Total_Fund = 0.8

    # 判断用哪根均线作为标准时的参数，大于bias_threshold_top，或小于bias_threshold_bottom的都删除
    Bias_Threshold_Top = 10
    Bias_Threshold_Bottom = -15

    # 交易策略：
    # 1：第二天下跌就卖出
    # 2：四种算法死叉卖出
    # 3：无论第二天是涨是跌，全部卖出
    # 4：收盘价金叉牛熊线买入，收盘价死叉牛熊线卖出
    Transaction_Strategy = 4

    # 是否有强制止损
    Mandatory_Stop_Loss = False

    # 强制止损率。当损失超过这个比率时，立刻平仓
    Mandatory_Stop_Loss_Rate = 2
