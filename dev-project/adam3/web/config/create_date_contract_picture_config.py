#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
创建图表（具体合约）的配置
"""


class CreateDateContractPictureConfig:
    ######################## 创建图片：同一品种多个不同合约的比较图 #####################
    # 具体合约的code的前缀。十债用CFFEX.T2
    Different_Contract_Compare_Prefix_Code = 'CFFEX.T2'

    # 主连合约的code
    Different_Contract_Compare_Main_Code = 'T'

    # 开始时间
    Different_Contract_Compare_Begin_Date = '20240907'

    # 结束时间
    Different_Contract_Compare_End_Date = '20250307'

    # 图片保存的位置
    Different_Contract_Compare_Picture_Save_Path = ('output/picture/date/different_contract_compare/' + \
                                                   Different_Contract_Compare_Begin_Date + '_' + \
                                                   Different_Contract_Compare_End_Date + '_' + \
                                                    Different_Contract_Compare_Main_Code + '.png')
