#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
创建图表的配置
"""


class CreateDatePictureConfig:
    ######################## 创建图片：根据四种算法，计算各自金叉百分比，生成折线图（两个月） #####################
    # 开始时间
    Four_Strategy_Gold_Cross_Rate_Picture_Begin_Date = '20250107'

    # 结束时间
    Four_Strategy_Gold_Cross_Rate_Picture_End_Date = '20250307'

    # 图片保存的位置
    Four_Strategy_Gold_Cross_Rate_Picture_Save_Path = 'output/picture/date/four_strategy_gold_cross_rate/' + \
                                                      Four_Strategy_Gold_Cross_Rate_Picture_Begin_Date + '_' + \
                                                      Four_Strategy_Gold_Cross_Rate_Picture_End_Date + '.png'

    # 创建图片：根据四种算法，计算各自金叉百分比，再计算平均值，这是第一条线。计算这个数据的MA5，这是第二条线。两条线一起生成折线图（两个月） #
    # 开始时间
    Average_Four_Strategy_Gold_Cross_Rate_Picture_And_Its_And_Ma5_Begin_Date = '20250107'

    # 结束时间
    Average_Four_Strategy_Gold_Cross_Rate_Picture_And_Its_And_Ma5_End_Date = '20250307'

    # 图片保存的位置
    Average_Four_Strategy_Gold_Cross_Rate_Picture_And_Its_And_Ma5_Save_Path = 'output/picture/date/average_four_strategy_gold_cross_rate_and_its_and_ma5/' + \
                                                                              Average_Four_Strategy_Gold_Cross_Rate_Picture_And_Its_And_Ma5_Begin_Date + '_' + \
                                                                              Average_Four_Strategy_Gold_Cross_Rate_Picture_And_Its_And_Ma5_End_Date + '.png'

    # 创建图片：统一价格指数图。把第一个交易日的品种的初始价格设置为100，之后的交易日上市交易的品种，初始价格为前一个交易日平均unique_close_price_index，直到计算到最后一个交易日，再计算每天上市交易的品种的平均价格（两年） #
    # 开始时间
    Unite_Relative_Price_Index_Begin_Date = '20240307'

    # 结束时间
    Unite_Relative_Price_Index_End_Date = '20250307'

    # 图片保存的位置
    Unite_Relative_Price_Index_Save_Path = 'output/picture/date/unite_relative_price_index/' + \
                                           Unite_Relative_Price_Index_Begin_Date + '_' + Unite_Relative_Price_Index_End_Date + '.png'

    ################################# 创建图片：日线级别上涨的期货的百分比 #################################
    # 开始时间
    Price_Change_Up_Percentage_Begin_Date = '20241207'

    # 结束时间
    Price_Change_Up_Percentage_End_Date = '20250307'

    # 图片保存的位置
    Price_Change_Up_Percentage_Save_Path = 'output/picture/date/price_change_up_percentage/' + \
                                           Price_Change_Up_Percentage_Begin_Date + '_' + Price_Change_Up_Percentage_End_Date + '.png'

    ####################################### 创建图片：每个期货的牛熊线图 #######################################
    # 开始时间
    Bull_Short_Line_Begin_Date = '20150307'

    # 结束时间
    Bull_Short_Line_End_Date = '20250307'

    # 图片保存的位置
    Bull_Short_Line_Save_Path = 'output/picture/date/bull_short_line/' + \
                                           Bull_Short_Line_Begin_Date + '_' + Bull_Short_Line_End_Date + '_' + '${code}' + '.png'

    ####################################### 创建图片：日线级别平均KD图 #######################################
    # 开始时间
    Average_KD_Begin_Date = '20240907'

    # 结束时间
    Average_KD_End_Date = '20250307'

    # 图片保存的位置
    Average_KD_Save_Path = 'output/picture/date/kd/' + Average_KD_Begin_Date + '_' + Average_KD_End_Date + '.png'

    ####################################### 创建图片：日线级别平均BOLL图 #######################################
    # 开始时间
    Average_Boll_Begin_Date = '20240907'

    # 结束时间
    Average_Boll_End_Date = '20250307'

    # 图片保存的位置
    Average_Boll_Save_Path = 'output/picture/date/boll/' + Average_Boll_Begin_Date + '_' + Average_Boll_End_Date + '.png'
