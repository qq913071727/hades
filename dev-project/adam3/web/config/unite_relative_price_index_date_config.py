#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
计算unite_relative_price_index_d表的基础数据
"""


class UniteRelativePriceIndexDateConfig:
    ######################### 计算unite_relative_price_index_d表的基础数据 #######################
    # 计算日线级别，某一个交易日，统一相对价格指数的close_pricee字段
    Unite_Relative_Price_Index_Date_By_Date = '20250307'
