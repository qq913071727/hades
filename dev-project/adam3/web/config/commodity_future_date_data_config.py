#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
计算commodity_future_date_data表的基础数据
"""


class CommodityFutureDateDataConfig:
    ################################ 调用akshare接口获取期货基础数据 #############################
    # 调用akshare接口获取期货基础数据，开始时间
    # Commodity_Future_Date_Data_Basic_Data_Begin_Date = '19900101'
    Commodity_Future_Date_Data_Basic_Data_Begin_Date = '20250307'
    # 调用akshare接口获取期货基础数据，结束时间
    Commodity_Future_Date_Data_Basic_Data_End_Date = '20250307'

    ######################### 计算commodity_future_date_data表的基础数据 #######################
    # 计算日线级别，某一个交易日，所有期货的last_close_price、rising_and_falling_amount和price_change字段
    Commodity_Future_Date_Data_Basic_Data_By_Date = '20250307'

    # 计算日线级别，某一个交易日，所有期货的unite_relative_price_index字段
    Commodity_Future_Date_Data_Unite_Relative_Price_Index_By_Date = '20250307'

    # 计算日线级别，某一个交易日，所有期货的MA
    Commodity_Future_Date_Data_Ma_By_Date = '20250307'

    # 计算日线级别，某一个交易日，所有期货的MACD
    Commodity_Future_Date_Data_Macd_By_Date = '20250307'

    # 计算日线级别，某一个交易日，所有期货的KD
    Commodity_Future_Date_Data_Kd_By_Date = '20250307'

    # 计算日线级别，某一个交易日，所有期货的hei kin ashi
    Commodity_Future_Date_Data_Hei_Kin_Ashi_By_Date = '20250307'

    # 计算日线级别，某一个交易日，所有期货的BOLL
    Commodity_Future_Date_Data_Boll_By_Date = '20250307'

    # 计算日线级别，某一个交易日，所有期货的BIAS
    Commodity_Future_Date_Data_Bias_By_Date = '20250307'

    # 计算日线级别，某一个交易日，所有期货的VARIANCE
    Commodity_Future_Date_Data_Variance_By_Date = '20250307'
