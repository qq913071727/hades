#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
东方财富网相关的配置
"""


class EastMoneyConfig:

    # 下载合约的交易日期
    Download_Contract_Transaction_Date = "20241212"

    # 从东方财富网下载数据
    Commodity_Future_Download_Url = {
        'Market_Name': '期货市场',
        'Exchange': [
            {
                'Name': '上期所',
                'Commodity_Future_List': [
                    {
                        'Name': '沪锡',
                        'Url_Pattern': 'http://futsseapi.eastmoney.com/list/variety/113/14?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '沪金',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/5?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '沪镍',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/13?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '橡胶',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/10?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '螺纹钢',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/7?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '沪银',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/6?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '线材',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/8?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '沪铅',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/4?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '沪锌',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/3?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '沥青',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/11?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '沪铝',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/2?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '燃油',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/9?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '热卷',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/12?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '纸浆',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/15?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '不锈钢',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/16?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '氧化铝',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/17?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '丁二烯胶',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/113/18?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    }
                ]
            }, {
                'Name': '上期能源',
                'Commodity_Future_List': [
                    {
                        'Name': '原油',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/142/1?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '20号胶',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/142/2?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '低硫燃油',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/142/3?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '国际铜',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/142/4?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '欧线集运',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/142/5?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    }
                ]
            }, {
                'Name': '大商所',
                'Commodity_Future_List': [
                    {
                        'Name': '棕榈油',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/6?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '塑料',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/7?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': 'PVC',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/8?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '焦炭',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/9?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '焦煤',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/10?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '纤维板',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/11?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '胶合板',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/12?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '铁矿石',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/13?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '鸡蛋',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/14?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '聚丙烯',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/15?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '淀粉',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/16?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '乙二醇',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/17?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '粳米',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/18?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '苯乙烯',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/19?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '液化石油气',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/20?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '生猪',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/21?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '原木',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/114/22?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    }
                ]
            }, {
                'Name': '郑商所',
                'Commodity_Future_List': [
                    {
                        'Name': '强麦',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/1?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '普麦',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/2?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '棉花',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/3?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '白糖',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/4?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': 'PTA',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/5?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '菜油',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/6?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '早籼稻',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/7?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '甲醇',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/8?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '玻璃',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/9?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '菜籽',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/10?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '菜粕',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/11?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '粳稻',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/13?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '晚籼稻',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/14?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '硅铁',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/15?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '锰硅',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/16?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '动力煤',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/17?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '棉纱',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/18?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '苹果',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/19?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '红枣',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/20?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '尿素',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/21?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '纯碱',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/22?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '短纤',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/23?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '花生',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/24?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '对二甲苯',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/25?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '烧碱',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/26?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '瓶片',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/115/27?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    }
                ]
            }, {
                'Name': '广期所',
                'Commodity_Future_List': [
                    {
                        'Name': '工业硅',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/225/1?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    },
                    {
                        'Name': '碳酸锂',
                        'Url_Pattern': 'https://futsseapi.eastmoney.com/list/variety/225/2?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733019522805'
                    }
                ]
            }, {
                'Name': '中金所',
                'Commodity_Future_List': [
                    {
                        'Name': '二债',
                        'Url_Pattern': 'http://futsseapi.eastmoney.com/list/variety/220/6?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733043533345'
                    },
                    {
                        'Name': '五债',
                        'Url_Pattern': 'http://futsseapi.eastmoney.com/list/variety/220/5?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733043533361'
                    },
                    {
                        'Name': '十债',
                        'Url_Pattern': 'http://futsseapi.eastmoney.com/list/variety/220/4?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733043533370'
                    },
                    {
                        'Name': '三十债',
                        'Url_Pattern': 'http://futsseapi.eastmoney.com/list/variety/220/8?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733043775454'
                    },
                    {
                        'Name': '上证50',
                        'Url_Pattern': 'http://futsseapi.eastmoney.com/list/variety/220/3?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733043860981'
                    },
                    {
                        'Name': '中证500',
                        'Url_Pattern': 'http://futsseapi.eastmoney.com/list/variety/220/1?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733043912800'
                    },
                    {
                        'Name': '沪深300',
                        'Url_Pattern': 'http://futsseapi.eastmoney.com/list/variety/220/2?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733043912812'
                    },
                    {
                        'Name': '中证1000',
                        'Url_Pattern': 'http://futsseapi.eastmoney.com/list/variety/220/7?callback=aaa_callback&orderBy=zdf&sort=desc&pageSize=20&pageIndex=#{pageIndex}&callbackName=aaa_callback&token=58b2fa8f54638b60b87d69b31969089c&field=dm%2Csc%2Cname%2Cp%2Czsjd%2Czde%2Czdf%2Cf152%2Co%2Ch%2Cl%2Czjsj%2Cvol%2Ccje%2Cwp%2Cnp%2Cccl&blockName=callback&_=1733043912819'
                    }
                ]
            }
        ]
    }
