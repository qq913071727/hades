#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.core.validators import MaxValueValidator

"""
商品期货信息表
"""


class CommodityFutureInfo(models.Model):

    def __init__(self) -> None:
        super().__init__()

    # 主键
    id = models.AutoField(primary_key=True, help_text='主键')

    # 代码
    code = models.CharField(help_text='代码', max_length=32, db_index=True, null=True, blank=True)

    # 名称
    name = models.CharField(help_text='名称', max_length=32, db_index=True, null=True, blank=True)

    def __str__(self):
        return "id=" + str(self.id) + ", code=" + self.code + ", name=" + self.name

    class Meta:
        managed = True
        db_table = 'commodity_future_info'
        verbose_name = '商品期货信息表'
        verbose_name_plural = verbose_name
        # index_together = [('code', 'date')]


"""
商品期货交易数据表
"""


class CommodityFutureTransactData(models.Model):

    def __init__(self) -> None:
        super().__init__()

    # 主键
    id = models.AutoField(primary_key=True, help_text='主键')

    # 代码
    code = models.CharField(help_text='代码', max_length=32, db_index=True, null=True, blank=True)

    # k线周期类型
    k_line_period_type = models.CharField(help_text='k线周期类型', max_length=32, db_index=True, null=True, blank=True)

    # 开始时间
    begin_time = models.DateTimeField(help_text='开始时间', db_index=True, null=True, blank=True)

    # 结束时间
    end_time = models.DateTimeField(help_text='结束时间', db_index=True, null=True, blank=True)

    # 名称
    name = models.CharField(help_text='名称', max_length=32, db_index=True, null=True, blank=True)

    # 最高价
    highest_price = models.DecimalField(help_text='最高价', max_digits=32, decimal_places=2, null=True, blank=True)

    # 最低价
    lowest_price = models.DecimalField(help_text='最低价', max_digits=32, decimal_places=2, null=True, blank=True)

    # 开盘价
    open_price = models.DecimalField(help_text='开盘价', max_digits=32, decimal_places=2, null=True, blank=True)

    # 最新价/收盘价
    close_price = models.DecimalField(help_text='最新价/收盘价', max_digits=32, decimal_places=2, null=True, blank=True)

    # 上个交易日收盘价
    last_close_price = models.DecimalField(help_text='上个交易日收盘价', max_digits=32, decimal_places=2, null=True, blank=True)

    # 成交额
    volume = models.BigIntegerField(help_text='成交额', validators=[MaxValueValidator(9223372036854775807)], null=True,
                                    blank=True)

    # 成交量
    turnover = models.BigIntegerField(help_text='成交量', validators=[MaxValueValidator(9223372036854775807)], null=True,
                                      blank=True)

    # 持仓量
    open_interest = models.BigIntegerField(help_text='持仓量', validators=[MaxValueValidator(9223372036854775807)],
                                           null=True, blank=True)

    # 买盘（外盘）
    buying = models.BigIntegerField(help_text='买盘（外盘）', validators=[MaxValueValidator(9223372036854775807)], null=True,
                                    blank=True)

    # 卖盘（内盘）
    selling = models.BigIntegerField(help_text='卖盘（内盘）', validators=[MaxValueValidator(9223372036854775807)], null=True,
                                     blank=True)

    # 涨跌额
    rising_and_falling_amount = models.IntegerField(help_text='涨跌额', validators=[MaxValueValidator(2147483647)],
                                                    null=True, blank=True)

    # 涨跌幅
    price_change = models.DecimalField(help_text='涨跌幅', max_digits=32, decimal_places=2, null=True, blank=True)

    # 5日均线
    ma5 = models.DecimalField(help_text='5日均线', max_digits=32, decimal_places=16, null=True, blank=True)

    # 10日均线
    ma10 = models.DecimalField(help_text='10日均线', max_digits=32, decimal_places=16, null=True, blank=True)

    # 20日均线
    ma20 = models.DecimalField(help_text='20日均线', max_digits=32, decimal_places=16, null=True, blank=True)

    # 60日均线
    ma60 = models.DecimalField(help_text='60日均线', max_digits=32, decimal_places=16, null=True, blank=True)

    # 120日均线
    ma120 = models.DecimalField(help_text='120日均线', max_digits=32, decimal_places=16, null=True, blank=True)

    # 250日均线
    ma250 = models.DecimalField(help_text='250日均线', max_digits=32, decimal_places=16, null=True, blank=True)

    # MACD的ema12
    ema12 = models.DecimalField(help_text='MACD的ema12', max_digits=32, decimal_places=16, null=True, blank=True)

    # MACD的ema26
    ema26 = models.DecimalField(help_text='MACD的ema26', max_digits=32, decimal_places=16, null=True, blank=True)

    # MACD的dif
    dif = models.DecimalField(help_text='MACD的dif', max_digits=32, decimal_places=16, null=True, blank=True)

    # MACD的dea
    dea = models.DecimalField(help_text='MACD的dea', max_digits=32, decimal_places=16, null=True, blank=True)

    # KD的rsv
    rsv = models.DecimalField(help_text='KD的rsv', max_digits=32, decimal_places=16, null=True, blank=True)

    # KD的k
    k = models.DecimalField(help_text='KD的k', max_digits=32, decimal_places=16, null=True, blank=True)

    # KD的d
    d = models.DecimalField(help_text='KD的d', max_digits=32, decimal_places=16, null=True, blank=True)

    # 创建时间
    create_time = models.DateTimeField(help_text='创建时间', auto_now=False, auto_now_add=True, db_index=True, null=True,
                                blank=True)

    class Meta:
        managed = True
        db_table = 'commodity_future_transact_data'
        verbose_name = '商品期货交易数据表'
        verbose_name_plural = verbose_name
        index_together = [('code', 'k_line_period_type', 'end_time')]
