#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.core.validators import MaxValueValidator
import os
import sys
import django

sys.path.append(r"web")

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'adam3.settings')
django.setup()


class Robot8CommodityFutureFilter(models.Model):
    """
    机器人8--期货过滤表
    """

    # def __init__(self) -> None:
    #     super().__init__()

    # 主键
    id = models.AutoField(primary_key=True)

    # 代码
    code = models.CharField(max_length=32, db_index=True, null=True, blank=True)

    # 过滤类型/交易策略。1表示MACD金叉；2表示MACD死叉；3表示close_price金叉MA5；4表示close_price死叉MA5；5表示hei_kin_ashi从下跌趋势转为上涨趋势；6表示hei_kin_ashi从上涨趋势转为下跌趋势；7表示KD金叉；8表示KD死叉
    filter_type = models.IntegerField(validators=[MaxValueValidator(2147483647)], null=True, blank=True)

    # 交易方向。1表示做多；-1表示做空
    direction = models.IntegerField(validators=[MaxValueValidator(2147483647)], null=True, blank=True)

    # 累计收益。单位：元
    accumulative_profit_loss = models.DecimalField(max_digits=32, decimal_places=16, null=True, blank=True)

    # 方差类型。1表示5日方差，2表示10日方差，3表示20日方差，4表示60日方差，5表示120日方差，6表示250日方差
    variance_type = models.IntegerField(validators=[MaxValueValidator(2147483647)], null=True, blank=True)

    # 方差值
    variance = models.DecimalField(max_digits=32, decimal_places=16, null=True, blank=True)

    # 成功率最高的算法的成功率
    success_rate = models.DecimalField(max_digits=32, decimal_places=16, null=True,
                                       blank=True)

    class Meta:
        managed = True
        db_table = 'robot8_commodity_future_filter'
        verbose_name = '机器人8--期货过滤表'
        verbose_name_plural = verbose_name
        # 单列索引
        # indexes = [
        #     models.Index(fields=['code']),
        #     models.Index(fields=['k_line_period_type']),
        #     models.Index(fields=['begin_time']),
        #     models.Index(fields=['end_time']),
        #     models.Index(fields=['name']),
        # ]
        # 联合索引
        # index_together = [('code', 'buy_date', 'sell_date')]
