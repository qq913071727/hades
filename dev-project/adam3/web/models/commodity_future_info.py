#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.core.validators import MaxValueValidator
import os
import sys
import django

sys.path.append(r"web")

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'adam3.settings')
django.setup()


class CommodityFutureInfo(models.Model):
    """
    商品期货信息表
    """

    # def __init__(self) -> None:
    #     super().__init__()

    # 主键
    id = models.AutoField(primary_key=True)

    # 代码
    code = models.CharField(max_length=32, db_index=True, null=True, blank=True)

    # 名称
    name = models.CharField(max_length=64, db_index=True, null=True, blank=True)

    # 商品期货交易所id
    commodity_future_exchange_id = models.BigIntegerField(validators=[MaxValueValidator(9223372036854775807)],
                                                          db_index=True, null=True, blank=True)

    # 夜盘交易开始时间
    night_trading_begin_hour = models.DateTimeField(null=True, blank=True)

    # 夜盘交易结束时间
    night_trading_end_hour = models.DateTimeField(null=True, blank=True)

    # 日盘交易第一段开始时间
    daily_trading_1_begin_hour = models.DateTimeField(null=True, blank=True)

    # 日盘交易第一段结束时间
    daily_trading_1_end_hour = models.DateTimeField(null=True, blank=True)

    # 日盘交易第二段开始时间
    daily_trading_2_begin_hour = models.DateTimeField(null=True, blank=True)

    # 日盘交易第二段结束时间
    daily_trading_2_end_hour = models.DateTimeField(null=True, blank=True)

    # 日盘交易第三段开始时间
    daily_trading_3_begin_hour = models.DateTimeField(null=True, blank=True)

    # 日盘交易第三段结束时间
    daily_trading_3_end_hour = models.DateTimeField(null=True, blank=True)

    # 合约单位。取值分别为：吨/手、立方米/手、张/手、克/手、千克/手、元/点、桶/手
    contract_unit = models.IntegerField(validators=[MaxValueValidator(2147483647)], null=True, blank=True)

    # 交易乘数
    transaction_multiplier = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 涨跌幅限制。单位%
    price_limit = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 交易所保证金。单位%
    exchange_deposit = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 公司保证金。单位%
    company_deposit = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 期货费用的计算方式。1表示固定值计算，2表示费率值计算
    calculation_method = models.IntegerField(validators=[MaxValueValidator(2147483647)], null=True, blank=True)

    # 开仓标准（包括交易所收费和公司收费）
    open_position = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 平今仓标准（包括交易所收费和公司收费）
    close_today_position = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 平老仓标准（包括交易所收费和公司收费）
    close_old_position = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 是否可交易。1表示可，0表示否
    tradable = models.IntegerField(validators=[MaxValueValidator(2147483647)], null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'commodity_future_info'
        verbose_name = '商品期货信息表'
        verbose_name_plural = verbose_name
        # 单列索引
        # indexes = [
        #     models.Index(fields=['code']),
        #     models.Index(fields=['name']),
        # ]
        # 联合索引
        index_together = [('night_trading_begin_hour', 'night_trading_end_hour'),
                          ('daily_trading_1_begin_hour', 'daily_trading_1_end_hour'),
                          ('daily_trading_2_begin_hour', 'daily_trading_2_end_hour'),
                          ('daily_trading_3_begin_hour', 'daily_trading_3_end_hour')]
