#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.core.validators import MaxValueValidator
import os
import sys
import django

sys.path.append(r"web")

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'adam3.settings')
django.setup()


class CommodityFutureExchange(models.Model):
    """
    商品期货交易所
    """

    # def __init__(self) -> None:
    #     super().__init__()

    def __init__(self, id, name, code, contract_suffix, home_url) -> None:
        super().__init__()
        self.id = id
        self.name = name
        self.code = code
        self.contract_suffix = contract_suffix
        self.home_url = home_url

    # 主键
    id = models.AutoField(primary_key=True)

    # 名称
    name = models.CharField(max_length=32, db_index=True, null=True, blank=True)

    # 代码
    code = models.CharField(max_length=32, db_index=True, null=True, blank=True)

    # 合约后缀
    contract_suffix = models.CharField(max_length=32, db_index=True, null=True, blank=True)

    # 首页地址
    home_url = models.CharField(max_length=32, db_index=True, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'commodity_future_exchange'
        verbose_name = '商品期货交易所'
        verbose_name_plural = verbose_name
