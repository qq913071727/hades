#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.core.validators import MaxValueValidator
import os
import sys
import django

sys.path.append(r"web")

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'adam3.settings')
django.setup()


class UniteRelativePriceIndexDate(models.Model):
    """
    统一相对价格指数（日线）
    """

    # def __init__(self) -> None:
    #     super().__init__()

    # 主键
    id = models.AutoField(primary_key=True)

    # 代码
    code = models.CharField(max_length=32, db_index=True, null=True, blank=True)

    # 日期
    transaction_date = models.DateField(null=True, db_index=True, blank=True)

    # 名称
    name = models.CharField(max_length=32, db_index=True, null=True, blank=True)

    # 最高价
    highest_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 最低价
    lowest_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 开盘价
    open_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 最新价/收盘价
    close_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 上个交易日收盘价
    last_close_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 成交额
    volume = models.BigIntegerField(validators=[MaxValueValidator(9223372036854775807)], null=True, blank=True)

    # 成交量
    turnover = models.BigIntegerField(validators=[MaxValueValidator(9223372036854775807)], null=True, blank=True)

    # 持仓量
    open_interest = models.BigIntegerField(validators=[MaxValueValidator(9223372036854775807)], null=True, blank=True)

    # 买盘（外盘）
    buying = models.BigIntegerField(validators=[MaxValueValidator(9223372036854775807)], null=True, blank=True)

    # 卖盘（内盘）
    selling = models.BigIntegerField(validators=[MaxValueValidator(9223372036854775807)], null=True, blank=True)

    # 涨跌额
    rising_and_falling_amount = models.IntegerField(validators=[MaxValueValidator(2147483647)], null=True, blank=True)

    # 涨跌幅
    price_change = models.DecimalField(max_digits=32, decimal_places=2, null=True, blank=True)

    # 5日均线
    ma5 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 10日均线
    ma10 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 20日均线
    ma20 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 60日均线
    ma60 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 120日均线
    ma120 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 250日均线
    ma250 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # MACD的ema12
    ema12 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # MACD的ema26
    ema26 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # MACD的dif
    dif = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # MACD的dea
    dea = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # KD的rsv
    rsv = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # KD的k
    k = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # KD的d
    d = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 布林带上轨
    up = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 布林带中轨
    mb = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 布林带下轨
    dn = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # HeiKinAshi最高价
    ha_highest_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # HeiKinAshi最低价
    ha_lowest_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # HeiKinAshi开盘价
    ha_open_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # HeiKinAshi收盘价
    ha_close_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'UNITE_RELATIVE_PRICE_INDEX_D'
        verbose_name = '统一相对价格指数（日线）'
        verbose_name_plural = verbose_name
        # 单列索引
        # indexes = [
        #     models.Index(fields=['code']),
        #     models.Index(fields=['k_line_period_type']),
        #     models.Index(fields=['begin_time']),
        #     models.Index(fields=['end_time']),
        #     models.Index(fields=['name']),
        # ]
        # 联合索引
        index_together = [('code', 'transaction_date')]
