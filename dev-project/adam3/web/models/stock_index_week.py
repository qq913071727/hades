#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.core.validators import MaxValueValidator
import os
import sys
import django

sys.path.append(r"web")

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'adam3.settings')
django.setup()


class StockIndexWeek(models.Model):
    """
    周线级别股票指数
    """

    # 主键
    id_ = models.AutoField(primary_key=True)

    # 代码
    code_ = models.CharField(max_length=32, db_index=True, null=True, blank=True)

    # 本周交易的开始日期
    begin_date = models.DateField(null=True, db_index=True, blank=True)

    # 本周交易的结束日期
    end_date = models.DateField(null=True, db_index=True, blank=True)

    # 最高价
    highest_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 最低价
    lowest_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 开盘价
    open_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 最新价/收盘价
    close_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 涨跌额
    change_amount = models.DecimalField(max_digits=32, decimal_places=2, null=True, blank=True)

    # 涨跌幅
    change_range = models.DecimalField(max_digits=32, decimal_places=2, null=True, blank=True)

    # 成交量
    volume = models.BigIntegerField(validators=[MaxValueValidator(9223372036854775807)], null=True, blank=True)

    # 成交额
    turnover = models.BigIntegerField(validators=[MaxValueValidator(9223372036854775807)], null=True, blank=True)

    # HeiKinAshi最高价
    ha_index_week_highest_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # HeiKinAshi最低价
    ha_index_week_lowest_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # HeiKinAshi开盘价
    ha_index_week_open_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # HeiKinAshi收盘价
    ha_index_week_close_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'stock_index_week'
        verbose_name = '周线级别股票指数'
        verbose_name_plural = verbose_name
        # 单列索引
        # indexes = [
        #     models.Index(fields=['code']),
        #     models.Index(fields=['k_line_period_type']),
        #     models.Index(fields=['begin_time']),
        #     models.Index(fields=['end_time']),
        #     models.Index(fields=['name']),
        # ]
        # 联合索引
        index_together = [('code_', 'begin_date', 'end_date')]