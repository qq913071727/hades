#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.core.validators import MaxValueValidator
import os
import sys
import django

sys.path.append(r"web")

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'adam3.settings')
django.setup()


class Robot8Account(models.Model):
    """
    机器人8--账户
    """

    # def __init__(self) -> None:
    #     super().__init__()

    # 主键
    id = models.AutoField(primary_key=True)

    # 机器人名字
    robot_name = models.CharField(max_length=32, db_index=True, null=True, blank=True)

    # 持有期货的数量
    hold_commodity_future_number = models.IntegerField(validators=[MaxValueValidator(2147483647)], null=True,
                                                       blank=True)

    # 期货资产
    commodity_future_assets = models.DecimalField(max_digits=32, decimal_places=16, null=True, blank=True)

    # 资金资产
    capital_assets = models.DecimalField(max_digits=32, decimal_places=16, null=True, blank=True)

    # 总资产
    total_assets = models.DecimalField(max_digits=32, decimal_places=16, null=True, blank=True)

    # 总开仓手续费
    total_open_commission = models.DecimalField(max_digits=32, decimal_places=16, null=True, blank=True)

    # 总平仓手续费
    total_close_commission = models.DecimalField(max_digits=32, decimal_places=16, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'robot8_account'
        verbose_name = '机器人8--账户'
        verbose_name_plural = verbose_name
        # 单列索引
        # indexes = [
        #     models.Index(fields=['code']),
        #     models.Index(fields=['k_line_period_type']),
        #     models.Index(fields=['begin_time']),
        #     models.Index(fields=['end_time']),
        #     models.Index(fields=['name']),
        # ]
        # 联合索引
        # index_together = [('code', 'buy_date', 'sell_date')]
