#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.core.validators import MaxValueValidator
import os
import sys
import django

sys.path.append(r"web")

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'adam3.settings')
django.setup()


class ModelCommodityFutureDateDataHeiKinAshiDown(models.Model):
    """
    日线级别，hei kin ashi下降趋势算法
    """

    # def __init__(self) -> None:
    #     super().__init__()

    # 主键
    id = models.AutoField(primary_key=True)

    # 代码
    code = models.CharField(max_length=32, db_index=True, null=True, blank=True)

    # 买入日期
    buy_date = models.DateField(null=True, db_index=True, blank=True)

    # 卖出日期
    sell_date = models.DateField(null=True, db_index=True, blank=True)

    # 买入价格
    buy_price = models.DecimalField(max_digits=32, decimal_places=2, null=True, blank=True)

    # 卖出价格
    sell_price = models.DecimalField(max_digits=32, decimal_places=2, null=True, blank=True)

    # 累计收益。单位：元
    accumulative_profit_loss = models.DecimalField(max_digits=38, decimal_places=16, null=True, blank=True)

    # 本次交易盈亏百分比
    profit_loss = models.DecimalField(max_digits=32, decimal_places=2, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'mdl_c_f_date_hei_kin_ashi_down'
        verbose_name = 'ken kin ashi下降趋势算法'
        verbose_name_plural = verbose_name
        # 单列索引
        # indexes = [
        #     models.Index(fields=['code']),
        #     models.Index(fields=['k_line_period_type']),
        #     models.Index(fields=['begin_time']),
        #     models.Index(fields=['end_time']),
        #     models.Index(fields=['name']),
        # ]
        # 联合索引
        index_together = [('code', 'buy_date', 'sell_date')]
