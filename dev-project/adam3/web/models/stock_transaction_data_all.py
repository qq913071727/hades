#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.core.validators import MaxValueValidator
import os
import sys
import django

sys.path.append(r"web")

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'adam3.settings')
django.setup()


class StockTransactionDataAll(models.Model):
    """
    日线级别股票数据
    """

    # 主键
    id_ = models.AutoField(primary_key=True)

    # 日期
    date_ = models.DateField(null=True, db_index=True, blank=True)

    # 代码
    code_ = models.CharField(max_length=32, db_index=True, null=True, blank=True)

    # 最高价
    highest_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 最低价
    lowest_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 开盘价
    open_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 最新价/收盘价
    close_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 前收盘
    last_close_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 涨跌额
    change_amount = models.DecimalField(max_digits=32, decimal_places=2, null=True, blank=True)

    # 涨跌幅
    change_range = models.DecimalField(max_digits=32, decimal_places=2, null=True, blank=True)

    # 带除权的今日股价的涨跌幅。单位：%
    change_range_ex_right = models.DecimalField(max_digits=32, decimal_places=2, null=True, blank=True)

    # 今日股价是否涨跌。1：涨，-1：跌，0：平
    up_down = models.IntegerField(validators=[MaxValueValidator(2147483647)], null=True, blank=True)

    # 换手率
    turnover_rate = models.DecimalField(max_digits=32, decimal_places=2, null=True, blank=True)

    # 成交量
    volume = models.BigIntegerField(validators=[MaxValueValidator(9223372036854775807)], null=True, blank=True)

    # 成交额
    turnover = models.BigIntegerField(validators=[MaxValueValidator(9223372036854775807)], null=True, blank=True)

    # 总市值
    total_market_value = models.DecimalField(max_digits=32, decimal_places=2, null=True, blank=True)

    # 流通市值
    circulation_market_value = models.DecimalField(max_digits=32, decimal_places=2, null=True, blank=True)

    # 成交笔数
    transaction_number = models.DecimalField(max_digits=32, decimal_places=2, null=True, blank=True)

    # 5日均线
    ma5 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 10日均线
    ma10 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 20日均线
    ma20 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 60日均线
    ma60 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 120日均线
    ma120 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 250日均线
    ma250 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # MACD的ema12
    ema12 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # MACD的ema26
    ema26 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # MACD的dif
    dif = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # MACD的dea
    dea = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 股价5日内震荡幅度。计算方法为：将5日内涨跌百分比的绝对值相加，再除以5。单位：%
    five_day_volatility = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 股价10日内震荡幅度。计算方法为：将10日内涨跌百分比的绝对值相加，再除以10。单位：%
    ten_day_volatility = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 股价20日内震荡幅度。计算方法为：将20日内涨跌百分比的绝对值相加，再除以20。单位：%
    twenty_day_volatility = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 股价250日内震荡幅度。计算方法为：将250日内涨跌百分比的绝对值相加，再除以250。单位：%
    two_hundred_fifty = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # KD的rsv
    rsv = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # KD的k
    k = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # KD的d
    d = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # HeiKinAshi最高价
    ha_highest_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # HeiKinAshi最低价
    ha_lowest_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # HeiKinAshi开盘价
    ha_open_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # HeiKinAshi收盘价
    ha_close_price = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 布林带上轨
    up = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 布林带中轨
    mb = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 布林带下轨
    dn = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 5日bias
    bias5 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 10日bias
    bias10 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 20日bias
    bias20 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 60日bias
    bias60 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 120日bias
    bias120 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 250日bias
    bias250 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 5日variance
    variance5 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 10日variance
    variance10 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 20日variance
    variance20 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 60日variance
    variance60 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 120日variance
    variance120 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 250日variance
    variance250 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 成交量5日均线
    volume_ma5 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 成交量10日均线
    volume_ma10 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 成交量20日均线
    volume_ma20 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 成交量60日均线
    volume_ma60 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 成交量120日均线
    volume_ma120 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 成交量250日均线
    volume_ma250 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 成交额5日均线
    turnover_ma5 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 成交额5日均线
    turnover_ma10 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 成交额5日均线
    turnover_ma20 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 成交额5日均线
    turnover_ma60 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 成交额5日均线
    turnover_ma120 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # 成交额5日均线
    turnover_ma250 = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    # MACD动能柱
    macd = models.DecimalField(max_digits=32, decimal_places=3, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'stock_transaction_data_all'
        verbose_name = '日线级别股票数据'
        verbose_name_plural = verbose_name
        # 单列索引
        # indexes = [
        #     models.Index(fields=['code']),
        #     models.Index(fields=['k_line_period_type']),
        #     models.Index(fields=['begin_time']),
        #     models.Index(fields=['end_time']),
        #     models.Index(fields=['name']),
        # ]
        # 联合索引
        index_together = [('code_', 'date_')]
