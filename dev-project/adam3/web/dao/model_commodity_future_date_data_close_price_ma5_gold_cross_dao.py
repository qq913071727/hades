#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import connection
from web.dao.base_dao import BaseDao
from web.models.model_commodity_future_date_data_close_price_ma5_gold_cross import ModelCommodityFutureDateDataClosePriceMA5GoldCross

"""
ModelCommodityFutureDateDataClosePriceMA5GoldCross的dao类
"""


class ModelCommodityFutureDateDataClosePriceMA5GoldCrossDao(BaseDao):
    model_class = ModelCommodityFutureDateDataClosePriceMA5GoldCross

    ########################################### 全量计算数据 ########################################

    def calculate_model_commodity_future_date_data_close_price_ma5_gold_cross(self):
        """
        计算日线级别，close_price金叉ma5算法
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_MODEL_COMMODITY_FUTURE.CAL_MDL_CF_DATE_CP_MA_GC()")

    ########################################### 增量计算数据 ########################################

    def calculate_model_commodity_future_date_data_close_price_ma5_gold_cross_increment(self, date):
        """
        根据日期，计算日线级别某一个交易日的close_price金叉ma5算法
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_MODEL_COMMODITY_FUTURE.CAL_MDL_CF_DATE_CP_MA_GC_INCR", [date])

    def find_by_code_order_by_sell_date_asc(self, code) -> list:
        """
        根据code查找记录，并按照sell_date升序排列
        """

        filter_dict = {'code': code}
        order_by_list = ['sell_date']
        return self.find_list(filter_dict, dict(), order_by_list)

    def find_by_code_and_sell_date(self, code, sell_date):
        """
        根据code和sell_date，查找记录
        """

        filter_dict = {'code': code, 'sell_date': sell_date}
        return self.find_one(filter_dict, dict(), list())
