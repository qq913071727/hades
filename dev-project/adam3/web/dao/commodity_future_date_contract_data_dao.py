#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import connection
from django.db.models import Q
from web.util.datetime_util import DatetimeUtil
from web.dao.base_dao import BaseDao
from web.models.commodity_future_date_contract_data import CommodityFutureDateContractData
from web.constants.datetime_format import DatetimeFormat

"""
CommodityFutureDataData的dao类
"""


class CommodityFutureDateContractDataDao(BaseDao):
    model_class = CommodityFutureDateContractData

    ########################################### 全量计算数据 ########################################

    def write_date_basic_data(self):
        """
        计算日线级别全部last_close_price、rising_and_falling_amount和price_change字段
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.WRITE_BASIC_DATA()")

    def write_date_all_ma(self):
        """
        计算日线级别全部MA数据
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.CALCULATE_FIVE()")
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.CALCULATE_TEN()")
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.CALCULATE_TWENTY()")
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.CALCULATE_SIXTY()")
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.CALCULATE_ONEHUNDREDTWENTY()")
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.CALCULATE_TWOHUNDREDFIFTY()")

    def write_date_all_macd(self):
        """
        计算日线级别全部MACD数据
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.WRITE_MACD_INIT()")
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.WRITE_MACD_EMA()")
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.WRITE_MACD_DIF()")
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.WRITE_MACD_DEA()")
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.WRITE_MACD()")

    def write_date_all_kd(self):
        """
        计算日线级别全部KD数据
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.WRITE_KD_INIT()")
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.WRITE_KD_RSV()")
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.WRITE_KD_K()")
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.WRITE_KD_D()")

    def write_date_all_boll(self):
        """
        计算日线级别全部boll数据
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.CAL_BOLL()")

    def write_date_all_hei_kin_ashi(self):
        """
        计算日线级别全部hei_kin_ashi数据
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.WRITE_HA()")

    def write_date_all_bias(self):
        """
        计算日线级别全部bias数据
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.WRITE_BIAS()")

    def write_date_all_variance(self):
        """
        计算日线级别全部variance数据
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.WRITE_VARIANCE()")

    def write_date_unite_relative_price_index(self):
        """
        计算日线级别全部unite_relative_price_index数据
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_C_F_DATE_CONTRACT_DATA.CAL_UNITE_RELATIVE_PRICE_INDEX()")

    ########################################### 增量计算数据 ########################################
    def write_date_contract_basic_data_by_date(self, date):
        """
        根据日期，计算日线级别某一个交易日的last_close_price、rising_and_falling_amount和price_change字段
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_C_F_DATE_CONTRACT_DATA.WRITE_BASIC_DATA_BY_DATE", [date])

    def write_date_contract_ma_by_date(self, date):
        """
        根据日期，计算日线级别某一个交易日的MA数据
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_C_F_DATE_CONTRACT_DATA.WRITE_MOVING_AVERAGE_BY_DATE", [date])

    def write_date_contract_macd_by_date(self, date):
        """
        根据日期，计算日线级别某一个交易日的MACD数据
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_C_F_DATE_CONTRACT_DATA.WRITE_MACD_EMA_BY_DATE", [date])
            cursor.callproc("PKG_C_F_DATE_CONTRACT_DATA.WRITE_MACD_DIF_BY_DATE", [date])
            cursor.callproc("PKG_C_F_DATE_CONTRACT_DATA.WRITE_MACD_DEA_BY_DATE", [date])
            cursor.callproc("PKG_C_F_DATE_CONTRACT_DATA.WRITE_MACD_BY_DATE", [date])

    def write_date_contract_kd_by_date(self, date):
        """
        根据日期，计算日线级别某一个交易日的KD数据
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_C_F_DATE_CONTRACT_DATA.WRITE_KD_BY_DATE_RSV", [date])
            cursor.callproc("PKG_C_F_DATE_CONTRACT_DATA.WRITE_KD_BY_DATE_K", [date])
            cursor.callproc("PKG_C_F_DATE_CONTRACT_DATA.WRITE_KD_BY_DATE_D", [date])

    def write_date_contract_hei_kin_ashi_by_date(self, date):
        """
        根据日期，计算日线级别某一个交易日的hei kin ashi数据
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_C_F_DATE_CONTRACT_DATA.WRITE_HA_BY_DATE", [date])

    def write_date_contract_variance_by_date(self, date):
        """
        根据日期，计算日线级别某一个交易日的variance数据
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_C_F_DATE_CONTRACT_DATA.WRITE_VARIANCE_BY_DATE", [date])

    def write_date_contract_boll_by_date(self, date):
        """
        根据日期，计算日线级别某一个交易日的BOLL数据
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_C_F_DATE_CONTRACT_DATA.CAL_BOLL_BY_DATE", [date])

    def write_date_contract_bias_by_date(self, date):
        """
        根据日期，计算日线级别某一个交易日的BIAS数据
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_C_F_DATE_CONTRACT_DATA.WRITE_BIAS_BY_DATE", [date])

    ########################################### 创建图片 ########################################
    def distinct_code_between_transaction_date_like_code(self, code, begin_date, end_date):
        """
        在一定时间范围内，所有具体合约的code
        """
        with connection.cursor() as cursor:
            cursor.execute("select distinct t.code "
                           "from c_f_date_contract_data t "
                           "where t.transaction_date between %s and %s "
                           "and t.code like %s",
                           [DatetimeUtil.str_to_datetime(begin_date, DatetimeFormat.Date_Format),
                            DatetimeUtil.str_to_datetime(end_date, DatetimeFormat.Date_Format),
                            code+"%"])
            code_tuple = cursor.fetchall()
            return code_tuple

    def find_by_code_between_transaction_date_order_by_transaction_date_asc(self, code, begin_date, end_date):
        """
        在一定时间范围内，某种具体合约的数据，并按日期升序排列
        """
        with connection.cursor() as cursor:
            cursor.execute("select * "
                           "from c_f_date_contract_data t "
                           "where t.transaction_date between %s and %s "
                           "and t.code = %s "
                           "order by t.transaction_date asc",
                           (DatetimeUtil.str_to_datetime(begin_date, DatetimeFormat.Date_Format),
                            DatetimeUtil.str_to_datetime(end_date, DatetimeFormat.Date_Format),
                            code))
            _list = cursor.fetchall()
            return _list

    ########################################### robot ########################################

    def find_distinct_transaction_date_between_transaction_date_order_by_transaction_date(self, begin_date, end_date):
        """
        查找开始时间和结束时间之间的日期，并去重
        """

        with connection.cursor() as cursor:
            cursor.execute("select distinct t.transaction_date from c_f_date_contract_data t "
                           "where t.transaction_date between %s and %s "
                           "order by t.transaction_date asc",
                           (DatetimeUtil.str_to_datetime(begin_date, DatetimeFormat.Date_Format),
                            DatetimeUtil.str_to_datetime(end_date, DatetimeFormat.Date_Format)))
            transaction_date_tuple = cursor.fetchall()
            return transaction_date_tuple

