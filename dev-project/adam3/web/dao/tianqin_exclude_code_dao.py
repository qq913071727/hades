#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.dao.base_dao import BaseDao
from web.models.tianqin_exclude_code import TianqinExcludeCode

"""
TianqinExcludeCode的dao类
"""


class TianqinExcludeCodeDao(BaseDao):
    model_class = TianqinExcludeCode
