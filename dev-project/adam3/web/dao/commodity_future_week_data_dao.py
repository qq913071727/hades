#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import connection
from django.db.models import Q
from web.util.datetime_util import DatetimeUtil
from web.dao.base_dao import BaseDao
from web.models.commodity_future_week_data import CommodityFutureWeekData
from web.constants.datetime_format import DatetimeFormat

"""
CommodityFutureWeekData的dao类
"""


class CommodityFutureWeekDataDao(BaseDao):
    model_class = CommodityFutureWeekData

    ########################################### 全量计算数据 ########################################

    def write_week_basic_data(self):
        """
        计算周线级别基础数据
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_COMMODITY_FUTURE_WEEK()")

    def write_week_all_ma(self):
        """
        计算周线级别全部MA数据
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_MA5()")
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_MA10()")
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_MA20()")
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_MA60()")
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_MA120()")
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_MA250()")

    def write_week_all_kd(self):
        """
        计算周线级别全部KD数据
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_KD_INIT()")
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_KD_RSV()")
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_KD_K()")
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_KD_D()")

    def write_week_all_macd(self):
        """
        计算周线级别全部MACD数据
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_MACD_INIT()")
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_MACD_EMA()")
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_MACD_DIF()")
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_MACD_DEA()")
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_MACD()")

    def write_week_all_boll(self):
        """
        计算周线级别全部BOLL数据
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.CAL_BOLL()")

    def write_week_all_ha(self):
        """
        计算周线级别全部HA数据
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_COMMODITY_FUTURE_WEEK_DATA.CAL_WEEK_HA()")

    ########################################### 增量计算数据 ########################################
    def write_week_by_date(self, begin_date, end_date):
        """
        根据日期，计算周线级别某一个交易周的基础数据
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_BY_DATE", [begin_date, end_date])

    def write_week_ma_by_date(self, begin_date, end_date):
        """
        根据日期，计算周线级别某一个交易周的MA数据
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_MA_BY_DATE", [begin_date, end_date])

    def write_week_kd_by_date(self, begin_date, end_date):
        """
        根据日期，计算周线级别某一个交易周的KD数据
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_KD_BY_DATE_RSV", [begin_date, end_date])
            cursor.callproc("PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_KD_BY_DATE_K", [begin_date, end_date])
            cursor.callproc("PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_KD_BY_DATE_D", [begin_date, end_date])

    def write_week_macd_by_date(self, begin_date, end_date):
        """
        根据日期，计算周线级别某一个交易周的MACD数据
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_MACD_EMA_BY_DATE", [begin_date, end_date])
            cursor.callproc("PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_MACD_DIF_BY_DATE", [begin_date, end_date])
            cursor.callproc("PKG_COMMODITY_FUTURE_WEEK_DATA.WRITE_WEEK_MACD_DEA_BY_DATE", [begin_date, end_date])

    def write_week_boll_by_date(self, begin_date, end_date):
        """
        根据日期，计算周线级别某一个交易周的BOLL数据
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_COMMODITY_FUTURE_WEEK_DATA.CAL_BOLL_BY_DATE", [begin_date, end_date])

    def write_week_ha_by_date(self, begin_date, end_date):
        """
        根据日期，计算周线级别某一个交易周的HA数据
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_COMMODITY_FUTURE_WEEK_DATA.CAL_WEEK_HA_BY_DATE", [begin_date, end_date])

    def find_week_price_change_up_percentage_group_by_transaction_date(self, begin_date, end_date):
        """
        根据开始时间和结束时间，计算每一日上涨期货的百分比，并按时间升序排列
        """

        with connection.cursor() as cursor:
            cursor.execute("select t1.transaction_date, "
                           "(select count(*) from commodity_future_date_data t "
                           "where t.transaction_date=t1.transaction_date and t.price_change>0)/"
                           "(select count(*) from commodity_future_date_data t "
                           "where t.transaction_date=t1.transaction_date and t.price_change!=0) * 100 "
                           "from commodity_future_date_data t1 "
                           "where t1.transaction_date between to_date(%s,'yyyy-mm-dd') and to_date(%s,'yyyy-mm-dd') "
                           "group by t1.transaction_date "
                           "order by t1.transaction_date asc", (begin_date, end_date))
            price_change_up_percentage_tuple = cursor.fetchall()
            return price_change_up_percentage_tuple

    def find_average_kd_by_begin_date_and_end_date(self, begin_date, end_date):
        """
        根据开始时间和结束时间，计算平均KD
        """

        with connection.cursor() as cursor:
            cursor.execute("select avg(t.k), avg(t.d) from commodity_future_week_data t "
                           "where t.begin_date>=to_date(%s,'yyyy-mm-dd') and t.end_date<=to_date(%s,'yyyy-mm-dd')",
                           (begin_date, end_date))
            average_kd_tuple = cursor.fetchall()
            return average_kd_tuple
