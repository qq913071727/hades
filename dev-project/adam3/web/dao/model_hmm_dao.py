#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import connection
from web.dao.base_dao import BaseDao
from web.models.model_hmm import ModelHmm


class ModelHmmDao(BaseDao):
    """
    ModelHmm的dao类
    """

    model_class = ModelHmm
