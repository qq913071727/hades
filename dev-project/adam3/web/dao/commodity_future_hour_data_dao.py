#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import connection
from web.dao.base_dao import BaseDao
from web.models.commodity_future_hour_data import CommodityFutureHourData

"""
CommodityFutureHourData的dao类
"""


class CommodityFutureHourDataDao(BaseDao):
    model_class = CommodityFutureHourData

    def add_oracle_column_comment(self):
        """
        为表commodity_future_hour_data添加注释
        """

        with connection.cursor() as cursor:
            cursor.execute("comment on table commodity_future_hour_data is \'商品期货交易数据表（1小时级别）\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.id IS \'主键\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.code IS \'代码\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.begin_time IS \'开始时间\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.end_time IS \'结束时间\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.name IS \'名称\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.highest_price IS \'最高价\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.lowest_price IS \'最低价\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.open_price IS \'开盘价\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.close_price IS \'最新价/收盘价\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.last_close_price IS \'上个交易日收盘价\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.volume IS \'成交额\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.turnover IS \'成交量\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.open_interest IS \'持仓量\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.buying IS \'买盘（外盘）\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.selling IS \'卖盘（内盘）\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.rising_and_falling_amount IS \'涨跌额\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.price_change IS \'涨跌幅\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.ma5 IS \'5日均线\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.ma10 IS \'10日均线\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.ma20 IS \'20日均线\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.ma60 IS \'60日均线\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.ma120 IS \'120日均线\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.ma250 IS \'250日均线\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.ema12 IS \'MACD的ema12\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.ema26 IS \'MACD的ema26\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.dif IS \'MACD的dif\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.dea IS \'MACD的dea\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.rsv IS \'KD的rsv\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.k IS \'KD的k\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.d IS \'KD的d\'")
            cursor.execute("COMMENT ON COLUMN commodity_future_hour_data.create_time IS \'创建时间\'")
