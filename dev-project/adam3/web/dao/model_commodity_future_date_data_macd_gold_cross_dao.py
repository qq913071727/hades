#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import connection
from web.dao.base_dao import BaseDao
from web.models.model_commodity_future_date_data_macd_gold_cross import ModelCommodityFutureDateDataMACDGoldCross

"""
ModelCommodityFutureDateDataMACDGoldCross的dao类
"""


class ModelCommodityFutureDateDataMACDGoldCrossDao(BaseDao):
    model_class = ModelCommodityFutureDateDataMACDGoldCross

    def add_oracle_column_comment(self):
        """
        为表mdl_c_f_date_macd_gold_cross添加注释
        """

        with connection.cursor() as cursor:
            cursor.execute("comment on table mdl_c_f_date_macd_gold_cross is \'日线级别，MACD金叉算法\'")
            cursor.execute("COMMENT ON COLUMN mdl_c_f_date_macd_gold_cross.id IS \'主键\'")
            cursor.execute("COMMENT ON COLUMN mdl_c_f_date_macd_gold_cross.code IS \'代码\'")
            cursor.execute("COMMENT ON COLUMN mdl_c_f_date_macd_gold_cross.buy_date IS \'买入日期\'")
            cursor.execute("COMMENT ON COLUMN mdl_c_f_date_macd_gold_cross.sell_date IS \'卖出日期\'")
            cursor.execute("COMMENT ON COLUMN mdl_c_f_date_macd_gold_cross.accumulative_profit_loss IS \'累计收益。单位：元\'")
            cursor.execute("COMMENT ON COLUMN mdl_c_f_date_macd_gold_cross.buy_dea IS \'买入时的dea\'")
            cursor.execute("COMMENT ON COLUMN mdl_c_f_date_macd_gold_cross.buy_dif IS \'买入时的dif\'")
            cursor.execute("COMMENT ON COLUMN mdl_c_f_date_macd_gold_cross.buy_price IS \'买入价格\'")
            cursor.execute("COMMENT ON COLUMN mdl_c_f_date_macd_gold_cross.profit_loss IS \'本次交易盈亏百分比\'")
            cursor.execute("COMMENT ON COLUMN mdl_c_f_date_macd_gold_cross.sell_dea IS \'卖出时的dea\'")
            cursor.execute("COMMENT ON COLUMN mdl_c_f_date_macd_gold_cross.sell_dif IS \'卖出时的dif\'")
            cursor.execute("COMMENT ON COLUMN mdl_c_f_date_macd_gold_cross.sell_price IS \'卖出价格\'")

    ########################################### 全量计算数据 ########################################

    def write_date_all_macd_gold_cross(self):
        """
        日线级别，MACD金叉算法
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_MODEL_COMMODITY_FUTURE.CAL_MDL_MACD_GOLD_CROSS()")

    ########################################### 增量计算数据 ########################################

    def write_date_all_macd_gold_cross_by_date(self, date):
        """
        根据日期，计算日线级别，MACD金叉算法
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_MODEL_COMMODITY_FUTURE.CAL_MDL_MACD_GOLD_CROSS_INCR", [date])

    def find_by_code_order_by_sell_date_asc(self, code) -> list:
        """
        根据code查找记录，并按照sell_date升序排列
        """

        filter_dict = {'code': code}
        order_by_list = ['sell_date']
        return self.find_list(filter_dict, dict(), order_by_list)

    def find_by_code_and_sell_date(self, code, sell_date):
        """
        根据code和sell_date，查找记录
        """

        filter_dict = {'code': code, 'sell_date': sell_date}
        return self.find_one(filter_dict, dict(), list())
