#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db.models import Q
from web.manager.log_manager import LogManager
from web.domain.robot8_commodity_future_transaction_record_buy_sell_suggestion import \
    Robot8CommodityFutureTransactionRecordBuySellSuggestion
from web.models.robot8_commodity_future_transaction_record import Robot8CommodityFutureTransactionRecord
from web.util.json_util import JsonUtil

Logger = LogManager.get_logger(__name__)


class Daoelper:
    """
    dao帮助类
    """

    @staticmethod
    def print_buy_sell_suggestion_by_json(print_date):
        """
        以json格式，返回买卖建议
        """

        Logger.info("以json方式，打印买卖建议，日期为[" + str(print_date) + "]")

        # 做多时，开仓的建议
        robot8_commodity_future_transaction_record_bull_buy_suggestion_queryset = Robot8CommodityFutureTransactionRecord.objects.filter(
            Q(buy_date=print_date) & Q(sell_date__isnull=True) & Q(sell_price__isnull=True) & Q(
                sell_lot__isnull=True) & Q(direction=1))
        # 做多时，平仓的建议
        robot8_commodity_future_transaction_record_bull_sell_suggestion_queryset = Robot8CommodityFutureTransactionRecord.objects.filter(
            Q(sell_date=print_date) & Q(sell_date__isnull=False) & Q(sell_price__isnull=False) & Q(
                sell_lot__isnull=False) & Q(direction=1))
        # 做空时，开仓的建议
        robot8_commodity_future_transaction_record_short_sell_suggestion_queryset = Robot8CommodityFutureTransactionRecord.objects.filter(
            Q(sell_date=print_date) & Q(buy_date__isnull=True) & Q(buy_price__isnull=True) & Q(
                buy_lot__isnull=True) & Q(direction=-1))
        # 做空时，平仓的建议
        robot8_commodity_future_transaction_record_short_buy_suggestion_queryset = Robot8CommodityFutureTransactionRecord.objects.filter(
            Q(buy_date=print_date) & Q(buy_date__isnull=False) & Q(buy_price__isnull=False) & Q(
                buy_lot__isnull=False) & Q(direction=-1))

        # 转换为json字符串
        robot8_commodity_future_transaction_record_buy_sell_suggestion = Robot8CommodityFutureTransactionRecordBuySellSuggestion(
            list(robot8_commodity_future_transaction_record_bull_buy_suggestion_queryset),
            list(robot8_commodity_future_transaction_record_bull_sell_suggestion_queryset),
            list(robot8_commodity_future_transaction_record_short_sell_suggestion_queryset),
            list(robot8_commodity_future_transaction_record_short_buy_suggestion_queryset))
        data_json_str = robot8_commodity_future_transaction_record_buy_sell_suggestion.to_json_str()
        Logger.info("买卖建议：" + data_json_str)
