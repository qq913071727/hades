#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import connection
from web.dao.base_dao import BaseDao
from web.models.model_commodity_future_date_data_close_price_ma5_dead_cross import ModelCommodityFutureDateDataClosePriceMA5DeadCross

"""
ModelCommodityFutureDateDataClosePriceMA5DeadCross的dao类
"""


class ModelCommodityFutureDateDataClosePriceMA5DeadCrossDao(BaseDao):
    model_class = ModelCommodityFutureDateDataClosePriceMA5DeadCross

    ########################################### 全量计算数据 ########################################

    def calculate_model_commodity_future_date_data_close_price_ma5_dead_cross(self):
        """
        计算日线级别，close_price死叉ma5算法
        """

        with connection.cursor() as cursor:
            cursor.execute("CALL PKG_MODEL_COMMODITY_FUTURE.CAL_MDL_CF_DATE_CP_MA_DC()")

    ########################################### 增量计算数据 ########################################

    def calculate_model_commodity_future_date_data_close_price_ma5_dead_cross_increment(self, date):
        """
        根据日期，计算日线级别某一个交易日的close_price死叉ma5算法
        """

        with connection.cursor() as cursor:
            cursor.callproc("PKG_MODEL_COMMODITY_FUTURE.CAL_MDL_CF_DATE_CP_MA_DC_INCR", [date])

    def find_by_code_order_by_buy_date_asc(self, code) -> list:
        """
        根据code查找记录，并按照buy_date升序排列
        """

        filter_dict = {'code': code}
        order_by_list = ['buy_date']
        return self.find_list(filter_dict, dict(), order_by_list)

    def find_by_code_and_buy_date(self, code, sell_date):
        """
        根据code和buy_date，查找记录
        """

        filter_dict = {'code': code, 'buy_date': sell_date}
        return self.find_one(filter_dict, dict(), list())
