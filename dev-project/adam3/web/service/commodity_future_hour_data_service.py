#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from django.core.exceptions import ValidationError
from django.db.utils import DataError
from web.util.dto_util import DtoUtil
from web.constants.k_line_period_type import KLinePeriodType
from web.manager.log_manager import LogManager
from web.service.base_service import BaseService

Logger = LogManager.get_logger(__name__)

"""
CommodityFutureHData的service类
"""


class CommodityFutureHourDataService(BaseService):

    def add_oracle_column_comment(self):
        """
        为表commodity_future_hour_data添加注释
        """

        Logger.info('为表commodity_future_hour_data添加注释')

        self.commodity_future_hour_data_dao.add_oracle_column_comment()

    def save_commodity_future_hour_data_list(self, commodity_future_data_list):
        """
        保存CommodityFutureHourData类型对象列表
        """

        Logger.info('保存CommodityFutureHourData类型对象列表')

        for commodity_future_data_dto in commodity_future_data_list:
            commodity_future_hour_data = DtoUtil.commodity_future_data_dto_to_commodity_future_hour_data(
                commodity_future_data_dto)
            # try:
            if isinstance(commodity_future_hour_data.close_price, str) or isinstance(
                    commodity_future_hour_data.highest_price, str) or isinstance(
                commodity_future_hour_data.open_price, str) or isinstance(
                commodity_future_hour_data.lowest_price, str) or isinstance(
                commodity_future_hour_data.price_change, str) or isinstance(
                commodity_future_hour_data.rising_and_falling_amount, str) or isinstance(
                commodity_future_hour_data.volume, str):
                Logger.warn('期货[%s]在交易日[%s]没有数据', commodity_future_hour_data.code,
                            datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
                continue
            else:
                _datetime = datetime.now()
                # 计算begin_time和end_time
                commodity_future_hour_data.begin_time = _datetime
                commodity_future_hour_data.end_time = _datetime
                minute = int(_datetime.strftime('%M'))
                second = int(_datetime.strftime('%S'))
                millisecond = int(_datetime.strftime('%f')[:-3])

                commodity_future_hour_data.begin_time = commodity_future_hour_data.begin_time.replace(
                    minute=0, second=0, microsecond=0)
                commodity_future_hour_data.begin_time = commodity_future_hour_data.begin_time - timedelta(
                    hours=1)
                commodity_future_hour_data.end_time = commodity_future_hour_data.end_time.replace(
                    minute=0,
                    second=0,
                    microsecond=0)

            commodity_future_hour_data.save()
        # except DataError as e:
        #     Logger.error(e)
        # except ValidationError as e:
        #     Logger.error(e, commodity_future_hour_data)
        # except:
        #     Logger.error('未知错误，需要处理')
