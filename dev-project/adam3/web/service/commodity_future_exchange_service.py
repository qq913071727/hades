#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.manager.log_manager import LogManager
from web.service.base_service import BaseService

Logger = LogManager.get_logger(__name__)

"""
CommodityFutureExchange的service类
"""


class CommodityFutureExchangeService(BaseService):

    def add_oracle_column_comment(self):
        """
        为表commodity_future_exchange添加注释
        """

        Logger.info('为表commodity_future_exchange添加注释')

        self.commodity_future_exchange_dao.add_oracle_column_comment()

    def find_all(self):
        """
        查询commodity_future_exchange表的所有记录
        """

        Logger.info('为表commodity_future_exchange添加注释')

        return self.commodity_future_exchange_dao.find_all()

    def concat_tianqin_contract_prefix_and_code(self):
        """
        拼接交易所前缀和期货代码
        """

        Logger.info('拼接交易所前缀和期货代码')

        return self.commodity_future_exchange_dao.concat_tianqin_contract_prefix_and_code()
