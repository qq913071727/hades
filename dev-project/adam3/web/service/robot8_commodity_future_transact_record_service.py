#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
from web.manager.log_manager import LogManager
from web.service.base_service import BaseService
from web.service.helper.service_helper import ServiceHelper
from web.util.datetime_util import DatetimeUtil
from web.constants.datetime_format import DatetimeFormat
from web.config.robot8_config import Robot8Config
import akshare as ak

Logger = LogManager.get_logger(__name__)

"""
Robot8CommodityFutureTransactRecord的service类
"""


class Robot8CommodityFutureTransactRecordService(BaseService):

    def do_buy_and_sell_by_begin_date_and_end_date(self):
        """
        robot8测试
        """

        Logger.info("执行robot8测试")

        transaction_date_tuple = self.commodity_future_date_contract_data_dao.find_distinct_transaction_date_between_transaction_date_order_by_transaction_date(
            Robot8Config.Begin_Date, Robot8Config.End_Date)
        if transaction_date_tuple is not None and len(transaction_date_tuple) > 0:
            begin_date = transaction_date_tuple[0][0]
            end_date = transaction_date_tuple[len(transaction_date_tuple) - 1][0]
            Logger.info('测试的时间区间为[%s]至[%s]', begin_date, end_date)

            for item_tuple in transaction_date_tuple:
                current_transaction_date = item_tuple[0]
                Logger.info('当前的测试时间为[%s]', current_transaction_date)

                #################################### 期货平仓 ####################################
                # 更新robot8_account表的hold_commodity_future_number、commodity_future_assets、total_assets、capital_assets和total_open_commission字段
                self.robot8_account_dao.update_robot_account_before_sell_or_buy(current_transaction_date)
                # 移仓换月：平仓
                self.robot8_commodity_future_transaction_record_dao.rollover_for_close(current_transaction_date)
                # 期货平仓
                self.robot8_commodity_future_transaction_record_dao.sell_or_buy(current_transaction_date,
                                                                                Robot8Config.Mandatory_Stop_Loss,
                                                                                Robot8Config.Mandatory_Stop_Loss_Rate)
                # 根据当日期货平仓的收盘价，在期货平仓之后，更新robot8_account表
                self.robot8_account_dao.update_robot_account_after_sell_or_buy(current_transaction_date)

                #################################### 过滤期货 ####################################
                # 清空robot8_commodity_future_filter表
                self.robot8_commodity_future_filter_dao.truncate_table_robot8_commodity_future_filter()
                # 向robot8_commodity_future_filter表中插入尚未持有的期货（不包括移仓换月的期货）
                self.robot8_commodity_future_filter_dao.insert_robot8_commodity_future_filter_by_not_holding_commodity_future(
                    current_transaction_date)
                # 过滤条件：如果交易这个期货一手所需的资金大于max(total_assets) * Fund_Proportion_For_Trading_In_Total_Fund / Max_Holding_Comodity_Future_Number，则删除
                self.robot8_commodity_future_filter_dao.filter_by_fund_per_commodity_future(current_transaction_date)

                #################################### 判断趋势 ####################################
                # 如果收盘价大于等于布林带中轨，则为牛股，否则为熊股
                # self.robot8_commodity_future_filter_dao.update_direction_by_boll(current_transaction_date)
                # 如果周线级别上一周K大于等于上上周K，则为牛股，否则为熊股
                self.robot8_commodity_future_filter_dao.update_direction_by_last_week_kd_up_down(current_transaction_date)
                # 如果周线级别上一周KD金叉，则为牛股，否则为熊股
                # self.robot8_commodity_future_filter_dao.update_direction_by_last_week_kd_gold_cross_dead_cross(current_transaction_date)
                # 如果周线级别上一周收盘价大于等于上上周MA5，则为牛股，否则为熊股
                # self.robot8_commodity_future_filter_dao.update_direction_by_last_week_close_price_gold_cross_dead_cross_ma5(current_transaction_date)
                # 如果收盘价在牛熊线之上，则为牛股，否则为熊股
                # self.robot8_commodity_future_filter_dao.filter_by_bull_short_line(current_transaction_date)

                ################################# 判断使用哪种算法 ################################
                # 判断使用哪种算法
                self.robot8_commodity_future_filter_dao.update_filter_type(current_transaction_date)

                #################################### 期货开仓 ####################################
                # 移仓换月：开仓
                self.robot8_commodity_future_transaction_record_dao.rollover_for_open(current_transaction_date)
                # 期货开仓
                self.robot8_commodity_future_transaction_record_dao.bug_or_sell_commodity_future(
                    current_transaction_date)
                # 更新robot8_account表的hold_commodity_future_number、commodity_future_assets、total_assets、capital_assets和total_open_commission字段
                self.robot8_account_dao.update_robot_account_after_buy_or_sell(current_transaction_date)

                ################################## 打印买卖建议 ##################################
                ServiceHelper.print_suggestion(current_transaction_date)
        else:
            Logger.warn('测试时间[%s]至[%s]没有数据', Robot8Config.Begin_Date, Robot8Config.End_Date)

        Logger.info("robot8测试结束")

    def make_operation_sleep(self):
        """
        操作系统睡眠
        """

        Logger.info("操作系统睡眠")

        os.system("rundll32.exe powrprof.dll,SetSuspendState")
