#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.manager.log_manager import LogManager
from web.service.base_service import BaseService

Logger = LogManager.get_logger(__name__)

"""
CommodityFutureInfo的service类
"""


class CommodityFutureInfoService(BaseService):

    def add_oracle_column_comment(self):
        """
        为表commodity_future_info添加注释
        """

        Logger.info('为表commodity_future_info添加注释')

        self.commodity_future_info_dao.add_oracle_column_comment()
