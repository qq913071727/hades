#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.manager.log_manager import LogManager
from web.dao.helper.dao_helper import Daoelper

Logger = LogManager.get_logger(__name__)


class ServiceHelper:
    """
    service帮助类
    """

    @staticmethod
    def print_suggestion(current_transaction_date):
        """
        打印买卖建议
        """

        Logger.info("打印买卖建议")

        print_text = str()
        data = Daoelper.print_buy_sell_suggestion_by_json(current_transaction_date)
        Logger.info(data)
