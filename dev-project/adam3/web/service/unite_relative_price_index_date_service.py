#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from pylab import mpl
from web.config.create_date_picture_config import CreateDatePictureConfig
from web.manager.log_manager import LogManager
from web.service.base_service import BaseService
from web.config.unite_relative_price_index_date_config import UniteRelativePriceIndexDateConfig

Logger = LogManager.get_logger(__name__)

"""
UniteRelativePriceIndexDate的service类
"""


class UniteRelativePriceIndexDateService(BaseService):

    ########################################### 全量计算数据 ########################################
    def write_date_basic_data(self):
        """
        计算所有的日线级别基础数据
        """

        Logger.info('计算所有的日线级别基础数据')

        self.unite_relative_price_index_date_dao.write_date_basic_data()

    ########################################### 增量计算数据 ########################################
    def write_date_basic_data_by_date(self):
        """
        计算日线级别，某一个交易日，统一相对价格指数的close_pricee字段
        """

        Logger.info("计算日线级别，某一个交易日[%s]，统一相对价格指数的close_pricee字段",
                    UniteRelativePriceIndexDateConfig.Unite_Relative_Price_Index_Date_By_Date)

        self.unite_relative_price_index_date_dao.write_date_basic_data_by_date(
            UniteRelativePriceIndexDateConfig.Unite_Relative_Price_Index_Date_By_Date)

    ############################################# 创建图片 ###########################################
    def create_unique_relative_price_index_picture(self):
        """
        创建图片：统一价格指数图。把第一个交易日的品种的初始价格设置为100，之后的交易日上市交易的品种，初始价格为前一个交易日平均unique_close_price_index，
        直到计算到最后一个交易日，再计算每天上市交易的品种的平均价格
        """

        Logger.info("创建图片：统一价格指数图。把第一个交易日的品种的初始价格设置为100，之后的交易日上市交易的品种，初始价格为前一个交易日平均unique_close_price_index，"
                    "直到计算到最后一个交易日，再计算每天上市交易的品种的平均价格，开始时间[%s]，结束时间[%s]",
                    CreateDatePictureConfig.Unite_Relative_Price_Index_Begin_Date,
                    CreateDatePictureConfig.Unite_Relative_Price_Index_End_Date)

        order_by_list = ['transaction_date']
        commodity_future_date_data_tuple = self.unite_relative_price_index_date_dao.average_unite_relative_price_index_group_by_transaction_date(
            CreateDatePictureConfig.Unite_Relative_Price_Index_Begin_Date,
            CreateDatePictureConfig.Unite_Relative_Price_Index_End_Date)
        if commodity_future_date_data_tuple is not None and len(commodity_future_date_data_tuple) != 0:
            transaction_date_list = list()
            unite_relative_price_index_list = list()
            for commodity_future_date_data in commodity_future_date_data_tuple:
                transaction_date_list.append(commodity_future_date_data[0])
                unite_relative_price_index_list.append(commodity_future_date_data[1])

            # 创建一个新的图表
            fig, ax = plt.subplots(figsize=(18, 8))

            # 关闭交互模式
            plt.ioff()

            mpl.rcParams["font.sans-serif"] = ["SimHei"]

            # 创建折线图
            ax.plot(transaction_date_list, unite_relative_price_index_list, label='平均统一相对价格指数',
                    linewidth=0.5,
                    marker='o',
                    color='red')
            plt.legend()
            plt.title('平均统一相对价格指数')
            plt.xlabel('时间')
            plt.ylabel('指数')

            # 分辨率
            plt.rcParams['savefig.dpi'] = 300

            # 显示图例
            # plt.legend()

            # 保存图表
            plt.savefig(CreateDatePictureConfig.Unite_Relative_Price_Index_Save_Path)

            # 显示图表
            # plt.show()

            # 关闭图表
            plt.close()