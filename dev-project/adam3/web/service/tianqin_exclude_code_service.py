#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from pylab import mpl
import numpy as np
from datetime import datetime, timedelta
from django.core.exceptions import ValidationError
from django.db.utils import DataError
from django.db.models import Q
from web.util.dto_util import DtoUtil
from web.util.datetime_util import DatetimeUtil
from web.constants.k_line_period_type import KLinePeriodType
from web.constants.datetime_format import DatetimeFormat
from web.manager.log_manager import LogManager
from web.models.commodity_future_date_contract_data import CommodityFutureDateContractData
from web.service.base_service import BaseService
from web.config.create_date_picture_config import CreateDatePictureConfig
from web.config.create_week_picture_config import CreateWeekPictureConfig
from web.config.commodity_future_date_data_config import CommodityFutureDateDataConfig
from web.config.robot8_config import Robot8Config
from web.config.east_money_config import EastMoneyConfig

Logger = LogManager.get_logger(__name__)

"""
TianqinExcludeCode的service类
"""


class TianqinExcludeCodeService(BaseService):

    def save(self, tianqin_exclude_code):
        """
        保存记录
        """

        self.tianqin_exclude_dao.save(tianqin_exclude_code)

    def find_list(self):
        """
        查询所有记录
        """

        return self.tianqin_exclude_dao.find_list(dict(), dict(), list())
