#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.dao.commodity_future_exchange_dao import CommodityFutureExchangeDao
from web.dao.commodity_future_info_dao import CommodityFutureInfoDao
from web.dao.commodity_future_date_data_dao import CommodityFutureDateDataDao
from web.dao.commodity_future_hour_data_dao import CommodityFutureHourDataDao
from web.dao.commodity_future_minute_data_dao import CommodityFutureMinuteDataDao
from web.dao.commodity_future_week_data_dao import CommodityFutureWeekDataDao
from web.dao.commodity_future_date_contract_data_dao import CommodityFutureDateContractDataDao
from web.dao.commodity_future_week_contract_data_dao import CommodityFutureWeekContractDataDao
from web.dao.model_commodity_future_date_data_macd_gold_cross_dao import ModelCommodityFutureDateDataMACDGoldCrossDao
from web.dao.model_commodity_future_date_data_macd_dead_cross_dao import ModelCommodityFutureDateDataMACDDeadCrossDao
from web.dao.model_commodity_future_date_data_close_price_ma5_gold_cross_dao import \
    ModelCommodityFutureDateDataClosePriceMA5GoldCrossDao
from web.dao.model_commodity_future_date_data_close_price_ma5_dead_cross_dao import \
    ModelCommodityFutureDateDataClosePriceMA5DeadCrossDao
from web.dao.model_commodity_future_date_data_hei_kin_ashi_up_dao import ModelCommodityFutureDateDataHeiKinAshiUpDao
from web.dao.model_commodity_future_date_data_hei_kin_ashi_down_dao import ModelCommodityFutureDateDataHeiKinAshiDownDao
from web.dao.model_commodity_future_date_data_kd_gold_cross_dao import ModelCommodityFutureDateDataKDGoldCrossDao
from web.dao.model_commodity_future_date_data_kd_dead_cross_dao import ModelCommodityFutureDateDataKDDeadCrossDao
from web.dao.model_hmm_dao import ModelHmmDao
from web.dao.robot8_account_dao import Robot8AccountDao
from web.dao.robot8_commodity_future_filter_dao import Robot8CommodityFutureFilterDao
from web.dao.robot8_commodity_future_transaction_record_dao import Robot8CommodityFutureTransactionRecordDao
from web.dao.unite_relative_price_index_date_dao import UniteRelativePriceIndexDateDao
from web.dao.stock_index_week_dao import StockIndexWeekDao
from web.dao.tianqin_exclude_code_dao import TianqinExcludeCodeDao

"""
service基类
"""


class BaseService:

    def __init__(self) -> None:
        super().__init__()

        self.commodity_future_exchange_dao = CommodityFutureExchangeDao()
        self.commodity_future_info_dao = CommodityFutureInfoDao()
        self.commodity_future_date_data_dao = CommodityFutureDateDataDao()
        self.commodity_future_hour_data_dao = CommodityFutureHourDataDao()
        self.commodity_future_minute_data_dao = CommodityFutureMinuteDataDao()
        self.commodity_future_week_data_dao = CommodityFutureWeekDataDao()
        self.commodity_future_date_contract_data_dao = CommodityFutureDateContractDataDao()
        self.commodity_future_week_contract_data_dao = CommodityFutureWeekContractDataDao()
        self.model_commodity_future_date_data_macd_gold_cross_dao = ModelCommodityFutureDateDataMACDGoldCrossDao()
        self.model_commodity_future_date_data_macd_dead_cross_dao = ModelCommodityFutureDateDataMACDDeadCrossDao()
        self.model_commodity_future_date_data_close_price_ma5_gold_cross_dao = ModelCommodityFutureDateDataClosePriceMA5GoldCrossDao()
        self.model_commodity_future_date_data_close_price_ma5_dead_cross_dao = ModelCommodityFutureDateDataClosePriceMA5DeadCrossDao()
        self.model_commodity_future_date_data_hei_kin_ashi_up_dao = ModelCommodityFutureDateDataHeiKinAshiUpDao()
        self.model_commodity_future_date_data_hei_kin_ashi_down_dao = ModelCommodityFutureDateDataHeiKinAshiDownDao()
        self.model_commodity_future_date_data_kd_gold_cross_dao = ModelCommodityFutureDateDataKDGoldCrossDao()
        self.model_commodity_future_date_data_kd_dead_cross_dao = ModelCommodityFutureDateDataKDDeadCrossDao()
        self.model_hmm_dao = ModelHmmDao()
        self.robot8_account_dao = Robot8AccountDao()
        self.robot8_commodity_future_filter_dao = Robot8CommodityFutureFilterDao()
        self.robot8_commodity_future_transaction_record_dao = Robot8CommodityFutureTransactionRecordDao()
        self.unite_relative_price_index_date_dao = UniteRelativePriceIndexDateDao()
        self.stock_index_week_dao = StockIndexWeekDao()
        self.tianqin_exclude_dao = TianqinExcludeCodeDao()
