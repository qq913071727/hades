#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import time
import akshare as ak

from web.api.base_api import BaseApi


class StockTransactionMinuteDataApi(BaseApi):
    """
    股票分钟级别数据api
    """

    def find_all(self):
        """
        获取某个股票分钟级别数据
        """

        stock_zh_a_spot_df = ak.stock_zh_a_spot_em()
        new_stock_zh_a_spot_df = stock_zh_a_spot_df[['代码', '名称', '最新价', '涨跌幅']]

        stock_transaction_data_all_dict_list = list()
        for index, row in new_stock_zh_a_spot_df.iterrows():
            stock_transaction_data_all_dict = dict()
            stock_transaction_data_all_dict["Code"] = row.values[0]
            # stock_transaction_data_all_dict["Name"] = row.values[1]
            # stock_transaction_data_all_dict["Date"] = datetime.now()
            # stock_transaction_data_all_dict["Date"] = time.time()
            stock_transaction_data_all_dict["ClosePrice"] = row.values[2]
            stock_transaction_data_all_dict["ChangeRange"] = row.values[3]
            stock_transaction_data_all_dict_list.append(stock_transaction_data_all_dict)

        _json = json.dumps(stock_transaction_data_all_dict_list, ensure_ascii=False)
        # 这行代码不能注释，否则客户端无法获取数据
        print(_json)
