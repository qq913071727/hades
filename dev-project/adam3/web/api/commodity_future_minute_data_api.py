#!/usr/bin/env python
# -*- coding: utf-8 -*-

import akshare as ak

from web.api.base_api import BaseApi


class CommodityFutureMinuteDataApi(BaseApi):
    """
    期货分钟级别数据api
    """

    def find_by_code(self, code):
        """
        获取某个期货分钟级别数据
        """

        futures_zh_minute_sina_df = ak.futures_zh_minute_sina(symbol=code, period="1")
        _json = futures_zh_minute_sina_df.to_json()
        # 这行代码不能注释，否则客户端无法获取数据
        print(_json)
