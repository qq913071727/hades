#!/usr/bin/env python
# -*- coding: utf-8 -*-

from web.manager.log_manager import LogManager

Logger = LogManager.get_logger(__name__)


class BaseApi:
    """
    api基类
    """