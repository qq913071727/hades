#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging


class LogManager:
    """
    日志处理器类，同时在控制台和日志文件中打印日志
    """

    # 日志对象
    Logger = None

    def __init__(self):
        super(LogManager, self).__init__()

    @staticmethod
    def get_logger(param_name, log_file='/mywork/gitcode-repository/log/adam3.log', level=logging.INFO):
        """
        获取日志对象
        :param param_name:
        :param log_file:
        :param level:
        :return:
        """

        if LogManager.Logger is None:
            LogManager.Logger = logging.getLogger(param_name)
            LogManager.Logger.setLevel(level=level)

            formatter = logging.Formatter('%(asctime)s [%(threadName)s-%(thread)d] [%(levelname)s] %(name)s.%(funcName)s[%(lineno)d] %(message)s')

            file_handler = logging.FileHandler(log_file, encoding="utf-8")
            file_handler.setLevel(level=level)
            file_handler.setFormatter(formatter)

            console = logging.StreamHandler()
            console.setFormatter(formatter)
            console.setLevel(level)

            LogManager.Logger.addHandler(file_handler)
            LogManager.Logger.addHandler(console)
            return LogManager.Logger
        else:
            return LogManager.Logger
