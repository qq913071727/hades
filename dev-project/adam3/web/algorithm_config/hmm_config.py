#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
隐马尔科夫模型算法的配置文件
"""


class HmmConfig:
    # 状态数量
    Status_Number = 3
    # 训练记录数量
    Training_Data_Number = 250
    # 几日对数（取最大的）
    Logarithm_Date_Number = 5
    # 是否保存直方图
    Save_Histogram_Picture = False
    # 是否保存折线图
    Save_Line_Picture = False
    # 保存直方图的路径
    Save_Hmm_Histogram_Path = 'C:/mywork/gitcode-repository/hades/dev-project/adam3/output/picture/date/hmm_histogram/'
    # 保存收益率和收盘价折线图的路径
    Save_Profit_Loss_Rate_And_Close_Price_Path = 'C:/mywork/gitcode-repository/hades/dev-project/adam3/output/picture/date/hmm_profit_loss_rate_and_close_price/'
