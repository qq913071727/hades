#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json


class Robot8CommodityFutureTransactionRecordBuySellSuggestion:
    """
    买卖建议
    """

    def __init__(self, robot8_commodity_future_transaction_record_bull_buy_suggestion_list,
                 robot8_commodity_future_transaction_record_bull_sell_suggestion_list,
                 robot8_commodity_future_transaction_record_short_sell_suggestion_list,
                 robot8_commodity_future_transaction_record_short_buy_suggestion_list) -> None:
        super().__init__()
        self.robot8_commodity_future_transaction_record_bull_buy_suggestion_list = robot8_commodity_future_transaction_record_bull_buy_suggestion_list
        self.robot8_commodity_future_transaction_record_bull_sell_suggestion_list = robot8_commodity_future_transaction_record_bull_sell_suggestion_list
        self.robot8_commodity_future_transaction_record_short_sell_suggestion_list = robot8_commodity_future_transaction_record_short_sell_suggestion_list
        self.robot8_commodity_future_transaction_record_short_buy_suggestion_list = robot8_commodity_future_transaction_record_short_buy_suggestion_list

    def to_json_str(self):
        """
        转换为json字符串类型
        """

        if self.robot8_commodity_future_transaction_record_bull_buy_suggestion_list != None and len(
                self.robot8_commodity_future_transaction_record_bull_buy_suggestion_list) != 0:
            _robot8_commodity_future_transaction_record_bull_buy_suggestion_list = list()
            for robot8_commodity_future_transaction_record_bull_buy_suggestion in self.robot8_commodity_future_transaction_record_bull_buy_suggestion_list:
                data_dict = robot8_commodity_future_transaction_record_bull_buy_suggestion.to_dict()
                _robot8_commodity_future_transaction_record_bull_buy_suggestion_list.append(data_dict)
            self.robot8_commodity_future_transaction_record_bull_buy_suggestion_list = _robot8_commodity_future_transaction_record_bull_buy_suggestion_list

        if self.robot8_commodity_future_transaction_record_bull_sell_suggestion_list != None and len(
                self.robot8_commodity_future_transaction_record_bull_sell_suggestion_list) != 0:
            _robot8_commodity_future_transaction_record_bull_sell_suggestion_list = list()
            for robot8_commodity_future_transaction_record_bull_sell_suggestion in self.robot8_commodity_future_transaction_record_bull_sell_suggestion_list:
                data_dict = robot8_commodity_future_transaction_record_bull_sell_suggestion.to_dict()
                _robot8_commodity_future_transaction_record_bull_sell_suggestion_list.append(data_dict)
            self.robot8_commodity_future_transaction_record_bull_sell_suggestion_list = _robot8_commodity_future_transaction_record_bull_sell_suggestion_list

        if self.robot8_commodity_future_transaction_record_short_sell_suggestion_list != None and len(
                self.robot8_commodity_future_transaction_record_short_sell_suggestion_list) != 0:
            _robot8_commodity_future_transaction_record_short_sell_suggestion_list = list()
            for robot8_commodity_future_transaction_record_short_sell_suggestion in self.robot8_commodity_future_transaction_record_short_sell_suggestion_list:
                data_dict = robot8_commodity_future_transaction_record_short_sell_suggestion.to_dict()
                _robot8_commodity_future_transaction_record_short_sell_suggestion_list.append(data_dict)
            self.robot8_commodity_future_transaction_record_short_sell_suggestion_list = _robot8_commodity_future_transaction_record_short_sell_suggestion_list

        if self.robot8_commodity_future_transaction_record_short_buy_suggestion_list != None and len(
                self.robot8_commodity_future_transaction_record_short_buy_suggestion_list) != 0:
            _robot8_commodity_future_transaction_record_short_buy_suggestion_list = list()
            for robot8_commodity_future_transaction_record_short_buy_suggestion in self.robot8_commodity_future_transaction_record_short_buy_suggestion_list:
                data_dict = robot8_commodity_future_transaction_record_short_buy_suggestion.to_dict()
                _robot8_commodity_future_transaction_record_short_buy_suggestion_list.append(data_dict)
            self.robot8_commodity_future_transaction_record_short_buy_suggestion_list = _robot8_commodity_future_transaction_record_short_buy_suggestion_list

        _data_dict = {
            'robot8CommodityFutureTransactionRecordBullBuySuggestionList': self.robot8_commodity_future_transaction_record_bull_buy_suggestion_list,
            'robot8CommodityFutureTransactionRecordBullSellSuggestionList': self.robot8_commodity_future_transaction_record_bull_sell_suggestion_list,
            'robot8CommodityFutureTransactionRecordShortSellSuggestionList': self.robot8_commodity_future_transaction_record_short_sell_suggestion_list,
            'robot8CommodityFutureTransactionRecordShortBuySuggestionList': self.robot8_commodity_future_transaction_record_short_buy_suggestion_list
        }
        _json = json.dumps(_data_dict, ensure_ascii=False, sort_keys=True, indent=4, separators=(',', ':'))
        return _json
