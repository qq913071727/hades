#!/usr/bin/env python

from web.dto.commodity_future_date_contract_data_dto import CommodityFutureDateContractDataDto

"""
商品期货交易数据接口返回值
"""


class CommodityFutureDateContractDataApiResult:

    def __init__(self):
        super().__init__()

    def __init__(self, _obj):
        super().__init__()
        if _obj:
            self.total = _obj["total"]
            self.list = [CommodityFutureDateContractDataDto(d) for d in _obj["list"]]

    # 总数
    total: int = None

    # 数据列表
    list: list = None
