#!/usr/bin/env python


class CommodityFutureDateContractDataDto:
    """
    商品期货交易数据DTO
    """

    def __init__(self):
        super().__init__()

    def __init__(self, _obj):
        super().__init__()
        if _obj:
            self.__dict__.update(_obj)

    # 内盘（卖盘）
    np: int = None

    # 最高价
    h: float = None

    # 代码
    dm: str = None

    #
    zsjd: int = None

    # 最低价
    l: float = None

    # 持仓量
    ccl: int = None

    # 开盘价
    o: float = None

    # 最新价/收盘价
    p: float = None

    #
    sc: int = None

    # 成交量
    vol: int = None

    # 名称
    name: str = None

    # 买盘（外盘）
    wp: int = None

    # 涨跌额
    zde: float = None

    # 涨跌幅
    zdf: float = None

    # 上个交易日收盘价
    zjsj: float = None

    # 成交额
    cje: int = None
