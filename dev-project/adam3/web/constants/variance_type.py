#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
方差类型
"""


class VarianceType:
    # 2日方差
    Variance2 = 2
    # 5日方差
    Variance5 = 5
    # 10日方差
    Variance10 = 10
    # 20日方差
    Variance20 = 20
    # 60日方差
    Variance60 = 60
    # 120日方差
    Variance120 = 120
    # 250日方差
    Variance250 = 250
