# coding:utf-8

from os import path


class FileAndPath:
    """
    文件和路径相关的常量类
    """

    ####################################### 通用的常量 #######################################
    # 当前项目所在目录的盘符
    System_Drive = path.abspath(__file__)[0:2]

