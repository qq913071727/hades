#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
启动系统时的参数
"""


class StartupParameter:
    ########################################## 常用 ###############################################
    # 每日执行（主力连续）
    Date = 'date_'

    # 每周执行（主力连续）
    Week = 'week'

    # 每日执行（具体合约）
    Date_Contract = 'date_contract'

    # 每周执行（具体合约）
    Week_Contract = 'week_contract'

    # 数据库移植
    Make_Migrations = 'makemigrations'
    Migrate = 'migrate'

    # 给表和列添加注释
    Add_Column_Comments = 'addcolumncomments'

    # 启动定时任务
    Schedule = 'schedule'

    ######################################## 日线级别基础数据 ############################################

    # 通过akshare下载期货历史数据
    AK_Share_Download = 'akshare_download'

    # 通过东方财富网下载期货历史合约数据
    East_Money_Download = 'east_money_download'

    # 调用tianqin的接口，下载商品期货交易的历史合约数据
    Tianqin_Download = 'tianqin_download'

    # 调用掘金量化的接口，下载商品期货交易的历史合约数据
    My_Quant_Download = 'my_quant_download'

    # 解析并保存商品期货交易时间的HTML文件数据
    Parse_And_Save_Commodity_Future_Trading_Hour_Html = 'parse_and_save_commodity_future_trading_hour_html'

    # 解析并保存商品期货交易保证金的HTML文件数据
    Parse_And_Save_Commodity_Future_Trading_Bond_Html = 'parse_and_save_commodity_future_trading_bond_html'

    ######################################## 日线级别基础数据 ############################################

    # 计算日线级别全部基础数据（主力连续）
    Write_Date_All_Basic_Data = 'write_date_all_basic_data'

    # 计算日线级别某一个交易日的基础数据（主力连续）
    Write_Date_Basic_Data_By_Date = 'write_date_basic_data_by_date'

    # 计算日线级别全部基础数据（具体合约）
    Write_Date_Contract_All_Basic_Data = 'write_date_contract_all_basic_data'

    # 计算日线级别某一个交易日的基础数据（具体合约）
    Write_Date_Contract_Basic_Data_By_Date = 'write_date_contract_basic_data_by_date'

    ######################################## 周线级别基础数据 ############################################

    # 计算周线级别全部基础数据（主力连续）
    Write_Week_All_Basic_Data = 'write_week_all_basic_data'

    # 计算周线级别某一个交易周的基础数据（主力连续）
    Write_Week_Basic_Data_By_Date = 'write_week_basic_data_by_date'

    # 计算周线级别全部基础数据（具体合约）
    Write_Week_Contract_All_Basic_Data = 'write_week_contract_all_basic_data'

    # 计算周线级别某一个交易周的基础数据（具体合约）
    Write_Week_Contract_Basic_Data_By_Date = 'write_week_contract_basic_data_by_date'

    ######################################### 日线级别创建图片 ###########################################

    # 日线级别创建图片
    Create_Date_Picture = 'create_date_picture'

    ######################################### 周线级别创建图片 ###########################################

    # 周线级别创建图片
    Create_Week_Picture = 'create_week_picture'

    ##################################### 日线级别具体合约创建图片 #######################################

    # 日线级别具体合约创建图片
    Create_Date_Contract_Picture = 'create_date_contract_picture'

    ########################################### robot测试 #############################################

    # robot8测试
    Robot8 = 'robot8'

    ########################################### api #############################################

    # api
    Api = 'api'
