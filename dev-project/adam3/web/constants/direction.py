#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
开仓的方向
"""


class Direction:
    # 做多
    Up = 1

    # 做空
    Down = -1
