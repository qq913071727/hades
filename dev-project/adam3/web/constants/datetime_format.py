#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
日期时间格式
"""

class DatetimeFormat:
    Datetime_Format = '%Y%m%d %H:%M:%S'
    Date_Format = '%Y%m%d'
    Date_Format_With_Line = '%Y-%m-%d'
