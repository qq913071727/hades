#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
期货费用的计算方式。1表示固定值计算，2表示费率值计算
"""


class CalculationMethod:
    # 固定值计算
    Fixed_Value = 1

    # 费率值计算
    Rate_Value = 2
