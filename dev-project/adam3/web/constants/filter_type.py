#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
算法
"""


class FilterType:
    # MACD金叉
    MACD_Gold_Cross = 1

    # MACD死叉
    MACD_Dead_Cross = 2

    # 收盘价金叉ma5
    Close_Price_Ma5_Gold_Cross = 3

    # 收盘价死叉ma5
    Close_Price_Ma5_Dead_Cross = 4

    # hei kin ashi上升趋势
    Hei_Kin_Ashi_Up = 5

    # hei kin ashi下降趋势
    Hei_Kin_Ashi_Down = 6

    # KD金叉
    KD_Gold_Cross = 7

    # KD死叉
    KD_Dead_Cross = 8

    # 收盘价金叉牛熊线
    Close_Price_Gold_Cross_Bull_Short_Line = 9

    # 收盘价死叉牛熊线
    Close_Price_Dead_Cross_Bull_Short_Line = 10
