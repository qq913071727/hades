#!/usr/bin/env python
# -*- coding: utf-8 -*-

class KLinePeriodType:
    """
    K线周期类型
    """

    # 30分钟
    Thirty_Minutes = '30m'

    # 1小时
    One_Hour = '1h'

    # 1天
    One_Day = '1d'
