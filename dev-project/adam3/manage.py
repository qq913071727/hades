#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import time
import django

from web.api.commodity_future_minute_data_api import CommodityFutureMinuteDataApi
from web.api.stock_transaction_minute_data_all_api import StockTransactionMinuteDataApi
from web.manager.log_manager import LogManager
from web.constants.startup_parameter import StartupParameter
from web.constants.k_line_period_type import KLinePeriodType
from web.config.commodity_future_date_contract_data_config import CommodityFutureDateContractDataConfig
# from web.task.east_money_download_commodity_future_date_contract_data_task import \
#     EastMoneyDownloadCollectCommodityFutureDateContractDataTask
# from web.task.tianqin_download_commodity_future_date_contract_data_task import \
#     TianqinDownloadCollectCommodityFutureDateContractDataTask
from web.task.akshare_download_commodity_future_data_task import AkshareDownloadCommodityFutureDataTask
from web.task.myquant_download_commodity_future_date_contract_data_task import \
    MyQuantDownloadCollectCommodityFutureDateContractDataTask
from web.task.parse_and_save_commodity_future_trading_hour_html_task import \
    ParseAndSaveCommodityFutureTradingHourHtmlTask
from web.task.parse_and_save_commodity_future_trading_bond_html_task import \
    ParseAndSaveCommodityFutureTradingBondHtmlTask
from web.task.add_column_comment_task import AddColumnCommentTask
from web.task.write_date_all_basic_data_task import WriteDateAllBasicDataTask
from web.task.write_date_basic_data_by_date_task import WriteDateBasicDataByDateTask
from web.task.write_week_all_basic_data_task import WriteWeekAllBasicDataTask
from web.task.write_week_basic_data_by_date_task import WriteWeekBasicDataByDateTask
from web.task.write_date_contract_all_basic_data_task import WriteDateContractAllBasicDataTask
from web.task.write_date_contract_basic_data_by_date_task import WriteDateContractBasicDataByDateTask
from web.task.write_week_contract_all_basic_data_task import WriteWeekContractAllBasicDataTask
from web.task.write_week_contract_basic_data_by_date_task import WriteWeekContractBasicDataByDateTask
from web.task.robot8_task import Robot8Task
from web.task.create_date_picture_task import CreateDatePictureTask
from web.task.create_week_picture_task import CreateWeekPictureTask
from web.task.create_date_contract_picture_task import CreateDateContractPictureTask
from web.util.file_util import FileUtil

sys.path.append(r"web")

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'adam3.settings')
django.setup()

Logger = LogManager.get_logger(__name__)


def start_database_migrate():
    """
    执行命令行命令
    """

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc

    execute_from_command_line(sys.argv)


if __name__ == '__main__':

    ################################################ 数据准备 ############################################
    if sys.argv[1] == StartupParameter.Make_Migrations:
        Logger.info('开始创建数据库移植文件')

        # 创建数据库移植文件
        start_database_migrate()

        Logger.info('创建数据库移植文件完成')

    if sys.argv[1] == StartupParameter.Migrate:
        Logger.info('开始更新数据库')

        # 更新数据库
        start_database_migrate()

        Logger.info('更新数据库完成')

    if sys.argv[1] == StartupParameter.Add_Column_Comments:
        # 更新数据库注释
        # mysql
        # start_database_migrate()
        # oracle
        Logger.info('开始更新数据库注释')

        _add_column_comments_task = AddColumnCommentTask()
        _add_column_comments_task.add_column_comment()

        Logger.info('更新数据库注释完成')

    if sys.argv[1] == StartupParameter.Schedule:
        Logger.info('启动定时任务')

        # 启动定时任务：调用东方财富网的接口，获取商品期货交易数据
        task = EastMoneyDownloadCollectCommodityFutureDateContractDataTask()
        task.start_task()

        Logger.info('定时任务完成')

    if sys.argv[1] == StartupParameter.Parse_And_Save_Commodity_Future_Trading_Hour_Html:
        Logger.info('开始解析并保存商品期货交易时间的HTML文件数据')

        parse_and_save_commodity_future_trading_hour_html_task = ParseAndSaveCommodityFutureTradingHourHtmlTask()
        parse_and_save_commodity_future_trading_hour_html_task.parse_and_save()

        Logger.info('解析并保存商品期货交易时间的HTML文件数据完成')

    if sys.argv[1] == StartupParameter.Parse_And_Save_Commodity_Future_Trading_Bond_Html:
        Logger.info('开始解析并保存商品期货交易保证金的HTML文件数据')

        parse_and_save_commodity_future_trading_bond_html_task = ParseAndSaveCommodityFutureTradingBondHtmlTask()
        parse_and_save_commodity_future_trading_bond_html_task.parse_and_save()

        Logger.info('解析并保存商品期货交易保证金的HTML文件数据完成')

    ################################################ 日常执行 ############################################
    if sys.argv[1] == StartupParameter.Date:
        # 每日执行(主力连续)

        akshare_download_commodity_future_data_task = AkshareDownloadCommodityFutureDataTask()
        # 通过akshare下载日线级别期货历史数据
        akshare_download_commodity_future_data_task.download_commodity_future_date_data()

        write_date_basic_data_by_date_task = WriteDateBasicDataByDateTask()
        # 根据日期，计算日线级别基础数据
        write_date_basic_data_by_date_task.do_task()

        create_date_picture_task = CreateDatePictureTask()
        # 创建图片：根据四种算法，计算各自金叉百分比，生成折线图
        create_date_picture_task.create_four_strategy_gold_cross_rate_picture()
        # 创建图片：根据四种算法，计算各自金叉百分比，再计算平均值，这是第一条线。计算这个数据的MA5，这是第二条线。两条线一起生成折线图
        create_date_picture_task.create_average_four_strategy_gold_cross_rate_and_its_and_ma5_picture()
        # 创建图片：统一价格指数图。把第一个交易日的品种的初始价格设置为100，之后的交易日上市交易的品种，初始价格为前一个交易日平均unique_close_price_index，
        # 直到计算到最后一个交易日，再计算每天上市交易的品种的平均价格
        create_date_picture_task.create_unique_relative_price_index_picture()
        # 创建图片：日线级别上涨的期货的百分比
        create_date_picture_task.create_price_change_up_percentage_picture()
        # 创建图片：日线级别平均KD图
        create_date_picture_task.create_average_kd_picture()
        # 创建图片：日线级别平均BOLL图
        create_date_picture_task.create_average_boll_picture()

        # task = EastMoneyDownloadCollectCommodityFutureDateContractDataTask()
        # # 调用东方财富网的接口，下载商品期货交易的合约数据（通常在每天下午收盘之后，夜盘开始之前执行）
        # task.download_data(KLinePeriodType.One_Day)

    if sys.argv[1] == StartupParameter.Week:
        # 每周执行

        write_week_basic_data_by_date_task = WriteWeekBasicDataByDateTask()
        # 根据日期，计算周线级别基础数据
        write_week_basic_data_by_date_task.do_task()

        create_week_picture_task = CreateWeekPictureTask()
        # 创建图片：周线级别上涨的期货的百分比
        create_week_picture_task.create_price_change_up_percentage_picture()
        # 创建图片：根据开始时间和结束时间，周线级别平均KD
        create_week_picture_task.create_week_average_kd_picture()

    if sys.argv[1] == StartupParameter.Date_Contract:
        # 每日执行(具体合约)

        # 删除掘金量化数据目录的文件和目录
        FileUtil.del_file(CommodityFutureDateContractDataConfig.Myquant_Data_Directory)

        # 启动掘金量化软件
        result = os.system(CommodityFutureDateContractDataConfig.Myquant_Startup_Exe_File)
        time.sleep(30)

        my_quant_download_collect_commodity_future_date_contract_data_task = MyQuantDownloadCollectCommodityFutureDateContractDataTask()
        # 开始通过掘金量化api下载期货历史合约数据
        my_quant_download_collect_commodity_future_date_contract_data_task.download_data()

        write_date_contract_basic_data_by_date_task = WriteDateContractBasicDataByDateTask()
        # 根据日期，计算日线级别基础数据
        write_date_contract_basic_data_by_date_task.do_task()

    if sys.argv[1] == StartupParameter.Week_Contract:
        # 每周执行（具体合约）

        write_week_contract_basic_data_by_date_task = WriteWeekContractBasicDataByDateTask()
        # 根据日期，计算周线级别基础数据（具体合约）
        write_week_contract_basic_data_by_date_task.do_task()

    ################################################ 数据下载 ############################################
    if sys.argv[1] == StartupParameter.AK_Share_Download:
        Logger.info('开始通过akshare下载期货历史数据')

        akshare_download_commodity_future_data_task = AkshareDownloadCommodityFutureDataTask()
        akshare_download_commodity_future_data_task.download_commodity_future_date_data()

        Logger.info('通过akshare下载期货历史数据完成')

    if sys.argv[1] == StartupParameter.East_Money_Download:
        Logger.info('开始通过东方财富网下载期货历史合约数据')

        task = EastMoneyDownloadCollectCommodityFutureDateContractDataTask()
        task.download_data(KLinePeriodType.One_Day)

    if sys.argv[1] == StartupParameter.Tianqin_Download:
        Logger.info('开始调用tianqin的接口，下载商品期货交易的历史合约数据')

        task = TianqinDownloadCollectCommodityFutureDateContractDataTask()
        task.download_data()

    if sys.argv[1] == StartupParameter.My_Quant_Download:
        Logger.info('开始通过掘金量化api下载期货历史合约数据')

        task = MyQuantDownloadCollectCommodityFutureDateContractDataTask()
        task.download_data()

    ################################################ 海量更新日线级别数据（主力连续） ############################################
    if sys.argv[1] == StartupParameter.Write_Date_All_Basic_Data:
        Logger.info('开始计算日线级别全部基础数据（主力连续）')

        write_date_all_basic_data_task = WriteDateAllBasicDataTask()
        write_date_all_basic_data_task.do_task()

        Logger.info('计算日线级别全部基础数据完成（主力连续）')

    ################################################ 海量更新周线级别数据（主力连续） ############################################
    if sys.argv[1] == StartupParameter.Write_Week_All_Basic_Data:
        Logger.info('开始计算周线级别全部基础数据（主力连续）')

        write_week_all_basic_data_task = WriteWeekAllBasicDataTask()
        write_week_all_basic_data_task.do_task()

        Logger.info('计算周线级别全部基础数据完成（主力连续）')

    ################################################ 增量更新日线级别数据（主力连续） ############################################
    if sys.argv[1] == StartupParameter.Write_Date_Basic_Data_By_Date:
        Logger.info('开始计算日线级别某一个交易日的基础数据（主力连续）')

        write_date_basic_data_by_date_task = WriteDateBasicDataByDateTask()
        write_date_basic_data_by_date_task.do_task()

        Logger.info('计算日线级别某一个交易日的基础数据完成（主力连续）')

    ################################################ 增量更新周线级别数据（主力连续） ############################################
    if sys.argv[1] == StartupParameter.Write_Week_Basic_Data_By_Date:
        Logger.info('开始计算周线级别某一个交易周的基础数据（主力连续）')

        write_week_basic_data_by_date_task = WriteWeekBasicDataByDateTask()
        write_week_basic_data_by_date_task.do_task()

        Logger.info('计算周线级别某一个交易周的基础数据完成（主力连续）')

    ################################################ 海量更新日线级别数据（具体合约） ############################################
    if sys.argv[1] == StartupParameter.Write_Date_Contract_All_Basic_Data:
        Logger.info('开始计算日线级别全部基础数据（具体合约）')

        write_date_contract_all_basic_data_task = WriteDateContractAllBasicDataTask()
        write_date_contract_all_basic_data_task.do_task()

        Logger.info('计算日线级别全部基础数据完成')

    ################################################ 海量更新周线级别数据（具体合约） ############################################
    if sys.argv[1] == StartupParameter.Write_Week_Contract_All_Basic_Data:
        Logger.info('开始计算周线级别全部基础数据（具体合约）')

        write_week_contract_all_basic_data_task = WriteWeekContractAllBasicDataTask()
        write_week_contract_all_basic_data_task.do_task()

        Logger.info('计算周线级别全部基础数据完成')

    ################################################ 增量更新日线级别数据（具体合约） ############################################
    if sys.argv[1] == StartupParameter.Write_Date_Contract_Basic_Data_By_Date:
        Logger.info('开始计算日线级别某一个交易日的基础数据（具体合约）')

        write_date_contract_basic_data_by_date_task = WriteDateContractBasicDataByDateTask()
        write_date_contract_basic_data_by_date_task.do_task()

        Logger.info('计算日线级别某一个交易日的基础数据完成（具体合约）')

    ################################################ 增量更新周线级别数据（具体合约） ############################################
    if sys.argv[1] == StartupParameter.Write_Week_Contract_Basic_Data_By_Date:
        Logger.info('开始计算周线级别某一个交易周的基础数据（具体合约）')

        write_week_contract_basic_data_by_date_task = WriteWeekContractBasicDataByDateTask()
        write_week_contract_basic_data_by_date_task.do_task()

        Logger.info('计算周线级别某一个交易周的基础数据完成（具体合约）')

    ################################################ robot测试 ############################################
    if sys.argv[1] == StartupParameter.Robot8:
        Logger.info('开始robot8测试')

        robot8_task = Robot8Task()
        robot8_task.do_task()

        Logger.info('robot8测试完成')

    ################################################ 创建日线级别图片 ############################################
    if sys.argv[1] == StartupParameter.Create_Date_Picture:
        Logger.info('开始创建日线级别图片')

        create_date_picture_task = CreateDatePictureTask()
        # 创建MACD金叉死叉算法、close_price金叉死叉ma5算法、kd金叉死叉算法收益率折线图
        # create_date_picture_task.create_gold_cross_dead_cross_accumulative_profit_loss_line_chart()

        # 创建图片：根据四种算法，计算各自金叉百分比，生成折线图
        create_date_picture_task.create_four_strategy_gold_cross_rate_picture()
        # 创建图片：根据四种算法，计算各自金叉百分比，再计算平均值，这是第一条线。计算这个数据的MA5，这是第二条线。两条线一起生成折线图
        create_date_picture_task.create_average_four_strategy_gold_cross_rate_and_its_and_ma5_picture()
        # 创建图片：统一价格指数图。把第一个交易日的品种的初始价格设置为100，之后的交易日上市交易的品种，初始价格为前一个交易日平均unique_close_price_index，
        # 直到计算到最后一个交易日，再计算每天上市交易的品种的平均价格
        create_date_picture_task.create_unique_relative_price_index_picture()
        # 创建图片：日线级别上涨的期货的百分比
        create_date_picture_task.create_price_change_up_percentage_picture()
        # 创建图片：每个期货的牛熊线图
        create_date_picture_task.create_bull_short_line_picture()
        # 创建图片：日线级别平均KD图
        create_date_picture_task.create_average_kd_picture()
        # 创建图片：日线级别平均BOLL图
        create_date_picture_task.create_average_boll_picture()

        Logger.info('创建日线级别图片完成')

    ################################################ 创建周线级别图片 ############################################
    if sys.argv[1] == StartupParameter.Create_Week_Picture:
        Logger.info('开始创建周线级别图片')

        create_week_picture_task = CreateWeekPictureTask()

        # 创建图片：周线级别上涨的期货的百分比
        create_week_picture_task.create_price_change_up_percentage_picture()
        # 创建图片：根据开始时间和结束时间，周线级别平均KD
        create_week_picture_task.create_week_average_kd_picture()

        Logger.info('创建日线级别图片完成')

    ############################################ 创建日线级别具体合约图片 ########################################
    if sys.argv[1] == StartupParameter.Create_Date_Contract_Picture:
        Logger.info('开始创建日线级别具体合约图片')

        create_date_contract_picture_task = CreateDateContractPictureTask()

        # 创建图片：同一品种多个不同合约的比较图
        create_date_contract_picture_task.create_different_contract_compare_picture()

        Logger.info('创建日线级别具体合约图片完成')

    ################################################ api ############################################
    if sys.argv[1] == StartupParameter.Api:
        method_name = sys.argv[2]
        parameter_list = sys.argv[3:]
        if method_name == "find_by_code":
            commodity_future_minute_data_api = CommodityFutureMinuteDataApi()
            commodity_future_minute_data_api.find_by_code(parameter_list[0])
        if method_name == "find_all":
            stock_transaction_minute_data_api = StockTransactionMinuteDataApi()
            stock_transaction_minute_data_api.find_all()
