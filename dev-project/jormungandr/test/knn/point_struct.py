#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ctypes import *


class PointStruct(Structure):
    _fields_ = [('testingDatasetIndex', c_int),
                ('trainingDatasetIndex', c_int),
                ('distance', c_int)]
