#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Point:
    """
    表示测试数据样本到训练数据样本的距离
    """

    def __init__(self):
        pass

    def __init__(self, testing_dataset_index: int, training_dataset_index: int, distance: float):
        # 测试数据集的索引
        self.testing_dataset_index = testing_dataset_index
        # 训练数据集的索引
        self.training_dataset_index = training_dataset_index
        # 距离
        self.distance = distance
