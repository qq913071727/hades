import tensorflow as tf

tf.compat.v1.disable_eager_execution()  # 保证sess.run()能够正常运行
a = tf.constant([1.0, 2.0], name='a')
b = tf.constant([2.0, 3.0], name='b')
result = a + b
print(result)
sess = tf.compat.v1.Session()  # 版本2.0的函数
print(sess.run(result))
