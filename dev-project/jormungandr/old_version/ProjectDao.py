# '''
# 收集数据库统计信息
# '''
# def gatherDatabaseStatistics():
#     print("gather database statistics begin")
#
# '''session= HibernateUtil.currentSession();
# session.beginTransaction();
# SQLQuery query = session.createSQLQuery("{call PKG_TOOL.GATHER_DATEBASE_STATISTICS()}");
# query.executeUpdate();
# session.getTransaction().commit();
# session.close();'''
#
#     print("gather database statistics finish")
#
# def test():
# #
#
#
# if __name__ == "__main__":
#     test()
#
# '''
#  方法4：http://finance.sina.com.cn/realstock/company/[市场][股票代码]/[复权].js?d=[日期]
#
# [复权]：qianfuquan-前复权；houfuquan-后复权。
#
# 返回结果：股票日期的股价JSON数据。
#
# 例如，http://finance.sina.com.cn/realstock/company/sz002095/qianfuquan.js?d=2015-06-16，获取深圳市场002095股票的前复权2015-06-16的数据。
#
# 注意，无法获取未复权的数据。
#
# 注意，需要对返回数据进行处理才能使用，新浪会在末尾加入注释语句，打乱日期数据，key值需要自行加入双引号，否则无法解析JSON。
#
# 注意，由于新浪的周线和月线数据，是以股票日线所有数据直接计算得到的，所以无法直接通过API获取周线和月线数据，需要自行处理。
# '''
#
#
# '''
#  方法6：http://quotes.money.163.com/service/chddata.html?code=[股票代码]&start=[开始日期]&end=[结束日期]&fields=[自定义列]
#
# 返回结果：历史股价及相关情况；CSV文件。
#
# 注意，该方法为网易公开方法，推荐使用。
#
# 其中，自定义列可定义TCLOSE收盘价 ;HIGH最高价;LOW最低价;TOPEN开盘价;LCLOSE前收盘价;CHG涨跌额;PCHG涨跌幅;TURNOVER换手率;VOTURNOVER成交量;VATURNOVER成交金额;TCAP总市值;MCAP流通市值这些值。
#
# 例如，http://quotes.money.163.com/service/chddata.html?code=0601857&start=20071105&end=20150618&fields=TCLOSE;HIGH;LOW;TOPEN;LCLOSE;CHG;PCHG;TURNOVER;VOTURNOVER;VATURNOVER;TCAP;MCAP，获取0601857从2007-11-05到2015-06-18区间的数据。
# '''