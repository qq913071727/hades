# coding:utf-8

from util.HttpUtil import HttpUtil
import json
import uuid
from util.StrUtil import StrUtil
from util.ListUtil import ListUtil
from util.HbaseUtil import HbaseUtil
from entity.StockRecord import StockRecord
from service.StockRecordService import StockRecordService
from service.StockIndexService import StockIndexService
from service.StockWeekendService import StockWeekendService
from util.TimeUtil import TimeUtil
from util.RedisUtil import RedisUtil

'''
入口类
'''


class StockMain:

    def __init__(self):
        pass


if __name__ == "__main__":
    stock_record_service = StockRecordService()
    stock_record_service.collect_stock_record(False)
    # stock_record_service.calculate_stock_average_close()
    # stock_record_service.calculate_stock_macd()
    # HbaseUtil.put_stock_record_object_list()

    # stock_index_service = StockIndexService()
    # stock_index_service.collect_stock_index()
    # stock_index_service.calculate_stock_index_ha()

    # stock_weekend_service = StockWeekendService()
    # stock_weekend_service.calculate_stock_weekend()

    print(TimeUtil.date())
