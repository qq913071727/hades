# coding:utf-8

import json

'''
股票轴线级别记录类
'''


class StockWeekend:

    '''
    主键
    '''
    id = None

    '''
    表示某一周的开始时间
    '''
    begin_date = None

    '''
    股票代码
    '''
    code = None

    '''
    1表示和前一天比较当天是上涨；0表示和前一天比较当天是不涨不跌；-1表示和前一天比较当天是下跌
    '''
    up_down = None

    '''
    表示某一周的结束时间
    '''
    end_date = None

    '''
    表示这条记录是这只股票的第多少个交易周
    '''
    number = None

    '''
    周线级别开盘价
    '''
    open = None

    '''
    周线级别收盘价
    '''
    close = None

    '''
    周线级别最高价
    '''
    high = None

    '''
    周线级别最低价
    '''
    low = None

    '''
    周线级别成交金额
    '''
    amount = None

    '''
    周线级别成交量
    '''
    volume = None

    '''
    计算KD时的指标
    '''
    rsv = None

    '''
    计算KD时的指标
    '''
    k = None

    '''
    计算KD时的指标
    '''
    d = None

    '''
    计算MACD时的指标
    '''
    ema12 = None

    '''
    计算MACD时的指标
    '''
    ema26 = None

    '''
    计算MACD时的指标
    '''
    dif = None

    '''
    计算MACD时的指标
    '''
    dea = None

    '''
    BOLL带的中轨
    '''
    mb = None

    '''
    BOLL带的上轨
    '''
    up = None

    '''
    BOLL带的下轨
    '''
    dn = None

    def __init__(self):
        pass

    '''
    将json字符串转换为StockWeekend对象
    '''
    @staticmethod
    def to_entity(obj_str):
        stock_weekend_build = json.loads(obj_str)
        stock_weekend = StockWeekend()
        stock_weekend.__dict__ = stock_weekend_build
        return stock_weekend
