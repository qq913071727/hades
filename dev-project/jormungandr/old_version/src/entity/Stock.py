# coding:utf-8

'''
股票类
'''
class Stock:
    '''
    主键
    '''
    id = None

    '''
    股票代码
    '''
    code = None

    '''
    股票名称
    '''
    name = None

    '''
    TTM市盈率
    '''
    ttm_pe_ratio = None

    '''
    净利润
    '''
    net_profits = None

    '''
    市净率
    '''
    price_book_ratio = None

    '''
    总市值
    '''
    total_market_capitalization = None

    '''
    流通市值
    '''
    circulation_market_value = None

    '''
    板块id
    '''
    board_id = None
