# coding:utf-8

'''
股票指数类
'''


class StockIndex:

    '''
    主键
    '''
    id = None

    '''
    日期
    '''
    date = None

    '''
    股票代码
    '''
    code = None

    '''
    开盘价
    '''
    open = None

    '''
    最高价
    '''
    high = None

    '''
    收盘价
    '''
    close = None

    '''
    最低价
    '''
    low = None

    '''
    前收盘
    '''
    last_close = None

    '''
    涨跌额
    '''
    up_down_amount = None

    '''
    涨跌
    '''
    up_down = None

    '''
    涨跌幅
    '''
    up_down_percentage = None

    '''
    成交量
    '''
    volume = None

    '''
    成交金额
    '''
    amount = None

    '''
    hei kin ashi开盘价
    '''
    ha_open = None

    '''
    hei kin ashi收盘价
    '''
    ha_close = None

    '''
    hei kin ashi最高价
    '''
    ha_high = None

    '''
    hei kin ashi最低价
    '''
    ha_low = None

    def __init__(self):
        pass