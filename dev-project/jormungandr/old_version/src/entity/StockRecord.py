# coding:utf-8

import json

'''
股票类
'''


class StockRecord:
    '''
    主键
    '''
    id = None
    '''
    日期
    '''
    date = None
    '''
    股票代码
    '''
    code = None
    '''
    开盘价
    '''
    open = None
    '''
    最高价
    '''
    high = None
    '''
    收盘价
    '''
    close = None
    '''
    最低价
    '''
    low = None
    '''
    前收盘
    '''
    last_close = None
    '''
    涨跌。1表示上涨，-1表示下跌，0表示持平
    '''
    up_down = None
    '''
    涨跌额
    '''
    up_down_amount = None
    '''
    涨跌幅
    '''
    up_down_percentage = None
    '''
    换手率
    '''
    turnover_rate = None
    '''
    成交量
    '''
    volume = None
    '''
    成交金额
    '''
    amount = None
    '''
    总市值
    '''
    total_market_value = None
    '''
    流通市值
    '''
    circulation_market_value = None
    '''
    五日均值
    '''
    five = None
    '''
    十日均值
    '''
    ten = None
    '''
    二十日均值
    '''
    twenty = None
    '''
    六十日均值
    '''
    sixty = None
    '''
    一百二十日均值
    '''
    one_hundred_and_twenty = None
    '''
    二百四十日均值
    '''
    two_hundred_and_forty = None
    '''
    MACD的ema12
    '''
    ema12 = None
    '''
    MACD的ema26
    '''
    ema26 = None
    '''
    MACD的dif
    '''
    dif = None
    '''
    MACD的dea
    '''
    dea = None

    '''
    构造函数
    '''
    def __init__(self):
        pass

    '''
        将json字符串转换为StockRecord对象
        '''
    @staticmethod
    def to_entity(obj_str):
        stock_record_build = json.loads(obj_str)
        stock_record = StockRecord()
        stock_record.__dict__ = stock_record_build
        return stock_record

    # def __init__(self, date, code, open, high, close, low, last_close, up_down, up_down_amount, up_down_percentage,
    #              volume, amount, total_market_value, circulation_market_value, five, ten, twenty, sixty, one_hundred_and_twenty, two_hundred_and_forty, ema12, ema26,
    #              dif, dea):
    #     self.date = date
    #     self.code = code
    #     self.open = open
    #     self.high = high
    #     self.close = close
    #     self.low = low
    #     self.last_close = last_close
    #     self.up_down
    #     self.up_down_amount = up_down_amount
    #     self.up_down_percentage = up_down_percentage
    #     self.volume = volume
    #     self.amount = amount
    #     self.total_market_value = total_market_value
    #     self.circulation_market_value = circulation_market_value
    #     self.five = five
    #     self.ten = ten
    #     self.twenty = twenty
    #     self.sixty = sixty
    #     self.one_hundred_and_twenty = one_hundred_and_twenty
    #     self.two_hundred_and_forty = two_hundred_and_forty
    #     self.ema12 = ema12
    #     self.ema26 = ema26
    #     self.dif = dif
    #     self.dea = dea