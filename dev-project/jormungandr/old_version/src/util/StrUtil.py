# coding:utf-8

import uuid
import TimeUtil

'''
字符串工具类
'''


class StrUtil:

    def __init__(self):
        pass

    '''
    将科学计数法转换为普通数字
    '''
    @staticmethod
    def to_common_number(value):
        # print(TimeUtil.TimeUtil.date(), "to_common_number method start")
        index=value.find("e")
        n=float(value[:index])
        if index==-1:
            return value
        else:
            num=int(value[index+2:])
            while num>0:
                n *= 10
                num -= 1
        return int(n)

    '''
    返回UUID
    '''
    @staticmethod
    def uuid():
        # print(TimeUtil.TimeUtil.date(), "uuid method start")
        return str(uuid.uuid1()).replace("-", "")


if __name__ == "__main__":
    StrUtil.to_common_number("1.86985779776e+11")
