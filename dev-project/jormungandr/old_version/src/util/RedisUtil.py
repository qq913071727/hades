# coding:utf-8

import json
from old_version.src.util.TimeUtil import TimeUtil
from rediscluster import StrictRedisCluster

'''
Redis工具类
'''
class RedisUtil:
    '''
    redis集群的节点对象,db=0
    '''
    redis_cluster_nodes = [{'host': '192.168.23.131', 'port': 7000},
                       {'host': '192.168.23.131', 'port': 7001},
                       {'host': '192.168.23.131', 'port': 7002},
                       {'host': '192.168.23.130', 'port': 7003},
                       {'host': '192.168.23.130', 'port': 7004},
                       {'host': '192.168.23.130', 'port': 7005}
                       ]
    '''
    redis链接对象,db=0
    '''
    redis_conn = StrictRedisCluster(startup_nodes=redis_cluster_nodes)

    def __init__(self):
        pass

    '''
    根据key，获取键值对中的value
    '''
    @staticmethod
    def get_value(key):
        print(TimeUtil.date(), "get_value method start")
        return RedisUtil.redis_conn.get(key)

    '''
    根据模式pattern，查找符合条件的key。如果pattern为None，则返回所有的key。
    '''
    @staticmethod
    def get_all_keys(param):
        print(TimeUtil.date(), "get_all_keys method start")
        if param is None:
            return RedisUtil.redis_conn.keys()
        else:
            return RedisUtil.redis_conn.keys(param)

    '''
    将stock_record_list存储到redis集群中，采用有序集合，code为key，date为score
    '''
    @staticmethod
    def put_stock_record_list(stock_record_list):
        print(TimeUtil.date(), "put_stock_record_list method start")
        if stock_record_list is not None and len(stock_record_list) > 0:
            pipeline = RedisUtil.redis_conn.pipeline()
            print(TimeUtil.date(), "for start")
            for stock_record in stock_record_list:
                pipeline.zadd("StockRecord_"+stock_record.code, stock_record.date, json.dumps(stock_record.__dict__, encoding='UTF-8', ensure_ascii=False))
                # pipeline.set(stock_record.id, json.dumps(stock_record.__dict__, encoding='UTF-8', ensure_ascii=False))
            print(TimeUtil.date(), "for end")
            pipeline.execute()
            print(TimeUtil.date(), "pipeline end")

    '''
    将stock_index_list存储到redis集群中，采用有序集合，code为key，date为score
    '''
    @staticmethod
    def put_stock_index_list(stock_index_list):
        print(TimeUtil.date(), "put_stock_index_list method start")
        if stock_index_list is not None and len(stock_index_list) > 0:
            pipeline = RedisUtil.redis_conn.pipeline()
            for stock_index in stock_index_list:
                pipeline.zadd("StockIndex_"+stock_index.code, stock_index.date,
                              json.dumps(stock_index.__dict__, encoding='UTF-8', ensure_ascii=False))
            pipeline.execute()

    '''
    将stock_weekend_list存储到redis集群中，采用有序集合，code为key，begin_date为score
    '''
    @staticmethod
    def put_stock_weekend_list(stock_weekend_list):
        print(TimeUtil.date(), "put_stock_weekend_list method start")
        if stock_weekend_list is not None and len(stock_weekend_list) > 0:
            pipeline = RedisUtil.redis_conn.pipeline()
            for stock_weekend in stock_weekend_list:
                pipeline.zadd("StockWeekend_" + stock_weekend.code, stock_weekend.end_date,
                              json.dumps(stock_weekend.__dict__, encoding='UTF-8', ensure_ascii=False))
            pipeline.execute()

    @staticmethod
    def get_all_stock_record():
        print(TimeUtil.date(), "get_all_stock_record method start")
        # pipeline = RedisUtil.redis_conn.pipeline()
        all_stock_record = RedisUtil.redis_conn.zrange(name="000001", start=0, end=-1, withscores=True)
        print(all_stock_record)


if __name__ == "__main__":
    # RedisUtil.get_value("name")

    # all_keys = RedisUtil.get_all_keys(None)
    # print(type(all_keys))

    # stock_record_list = RedisUtil.get_stock_record_list()
    # stock_record_list_by_code_sort_by_date_asc = RedisUtil.find_by_code_sort_by_date_asc(stock_record_list, "000001")
    # print(stock_record_list)

    RedisUtil.get_all_stock_record()
