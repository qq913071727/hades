# coding:utf-8

import urllib

'''
HTTP工具类
'''
class HttpUtil:

    def __init__(self):
        pass

    '''
    根据url和参数，获取stock数据
    '''
    @staticmethod
    def get_data(url, param):
        data = urllib.urlopen(url, param).read()
        data = data.decode("gbk")
        return data
        #param = bytes(urllib.parse.urlencode(param), encoding='utf-8')
        #response = urllib.request.urlopen(url, data=param)
        #result = response.read().decode('gbk')
        #return result

# test
if __name__ == "__main__":
    pass