# coding:utf-8

'''
数值类型数据的工具类
'''


class NumUtil:

    def __init__(self):
        pass

    '''
    求三个数中的最大值
    '''
    @staticmethod
    def max_value(one, two, three):
        if one < two:
            result = two
        else:
            result = one
        if result < three:
            result = three
        return result

    '''
    求三个数中的最小值
    '''
    @staticmethod
    def min_value(one, two, three):
        if one > two:
            result = two
        else:
            result = one
        if result > three:
            result = three
        return result

    '''
    查找数组中最大值
    '''
    @staticmethod
    def max_value_with_list(list_param):
        num = list_param[0]
        for element in list_param:
            if element > num:
                num = element
        return num

    '''
    查找数组中最小值
    '''
    @staticmethod
    def min_value_with_list(list_param):
        num = list_param[0]
        for element in list_param:
            if element < num:
                num = element
        return num
