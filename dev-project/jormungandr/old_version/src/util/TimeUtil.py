# coding:utf-8

from datetime import date
import time

'''
时间工具类
'''
class TimeUtil:

    '''
    打印当前系统时间
    '''
    @staticmethod
    def date():
        return time.asctime(time.localtime(time.time()))

    '''
    str类型转换为date类型
    '''
    @staticmethod
    def to_date(str_date):
        year = int(str_date[0:4])
        month = int(str_date[4:6])
        day = int(str_date[6:])
        return date(year, month, day)

    '''
    date类型转换为str类型
    '''
    @staticmethod
    def to_str(date):
        return date.strftime("%Y%m%d")


if __name__ == "__main__":
    print(TimeUtil.date())