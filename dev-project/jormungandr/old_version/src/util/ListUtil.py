# coding:utf-8

'''
数组list的工具类
'''


class ListUtil:

    def __init__(self):
        pass


    '''
    判断数组是否为空
    '''
    @staticmethod
    def is_not_empty(l):
        if l is not None and len(l)>0:
            return True
        else:
            return False

    '''
    将deque对象转换为list对象
    '''
    @staticmethod
    def deque_to_list(deque_param):
        result_list = list()
        for obj in deque_param:
            result_list.append(obj)
        return result_list

