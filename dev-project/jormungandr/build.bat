rem 生成当前项目的依赖配置文件，此文件中的内容包含了整个项目的依赖，我们只需要保留非系统的依赖即可
pip3 freeze > requirements.txt

rem 将系统外的依赖下载到packages目录
pip3 download -d packages/ -r requirements.txt

rem 打包
python setup.py sdist

rem 删除目录
rmdir /s/q dist\jormungandr-1.0.tar.gz_files

rem 解压
python src/util/file_util.py

rem 拷贝packages和requirements.txt到解压后的文件夹
xcopy packages dist\jormungandr-1.0.tar.gz_files\jormungandr-1.0\packages\  /S /E /Y
xcopy requirements.txt dist\jormungandr-1.0.tar.gz_files\jormungandr-1.0\


