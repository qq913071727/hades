#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from src.constant.file_and_path_constant import FileAndPathConstant
from src.manager.log_manager import LogManager
from src.handler.nn_handler import NnHandler

sys.path.append(
    FileAndPathConstant.System_Drive + '\github-repository\hades\dev-project\jormungandr\dist\jormungandr-1.0.tar.gz_files\jormungandr-1.0')
Logger = LogManager.get_logger(__name__)


class NnTask:
    """
    神经网络
    """

    def __init__(self):
        pass


if __name__ == "__main__":
    nn_handler = NnHandler()
    nn_handler.do()
