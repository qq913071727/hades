# coding:utf-8
from src.handler.stock_trend_prediction_handler import StockTrendPredictionHandler


class StockTrendPredictionTask:

    def __init__(self) -> None:
        super().__init__()


if __name__ == '__main__':
    stock_trend_prediction_handler = StockTrendPredictionHandler()
    stock_trend_prediction_handler.predict_stock_trend()
