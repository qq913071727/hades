#!/usr/bin/env python
# -*- coding: utf-8 -*-

from src.handler.lstm_handler import LstmHandler


class LstmTask:
    def __init__(self):
        pass


if __name__ == "__main__":
    lstm_handler = LstmHandler()
    lstm_handler.use_lstm()
