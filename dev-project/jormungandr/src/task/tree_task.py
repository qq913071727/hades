#!/usr/bin/env python
# -*- coding: utf-8 -*-

from src.handler.tree_handler import TreeHandler


class TreeTask:
    """
    决策树算法的任务类
    """

    # 特征值列表
    # Characteristic_Value_Number_List = [3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610]
    Characteristic_Value_Number_List = [3, 5, 8, 13, 21, 34]

    # 特征值
    Characteristic_Value = 3

    # 近邻数量
    Neighbor_Number = 3


if __name__ == "__main__":
    tree_handler = TreeHandler()

    # 查询特征向量和标签，并存储在文件中
    # tree_handler.collect_and_store_data_in_file(TreeTask.Characteristic_Value_Number_List)

    # 从训练数据中挑选测试数据，并将其移动到testing目录中
    # tree_handler.distinguish_training_data_and_testing_data()

    # 执行决策树算法
    for item in TreeTask.Characteristic_Value_Number_List:
        tree_handler.do_tree(item)
    # tree_handler.do_knn(TreeTask.Characteristic_Value, TreeTask.Neighbor_Number)
