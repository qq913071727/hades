#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from src.constant.file_and_path_constant import FileAndPathConstant
sys.path.append(FileAndPathConstant.System_Drive + '\github-repository\hades\dev-project\jormungandr\dist\jormungandr-1.0.tar.gz_files\jormungandr-1.0')
from src.handler.knn_handler import KnnHandler


class KnnTask:
    """
    K近邻算法的任务类
    """

    # 特征值列表
    # Characteristic_Value_Number_List = [3, 5, 8, 13, 21, 34, 55, 89, 144]
    Characteristic_Value_Number_List = [233, 377, 610]

    # 特征值
    Characteristic_Value = 3

    # 近邻数量
    Neighbor_Number = 3

    def __init__(self):
        pass


if __name__ == "__main__":
    knn_handler = KnnHandler()

    # 删除所有的特征值数据文件，包括训练数据和测试数据
    knn_handler.delete_training_data_file_and_testing_data_file()

    # 查询特征向量和标签，并存储在文件中
    knn_handler.collect_and_store_data_in_file(KnnTask.Characteristic_Value_Number_List)

    # 从训练数据中挑选测试数据，并将其移动到testing目录中
    knn_handler.distinguish_training_data_and_testing_data()

    # 执行knn算法
    for item in KnnTask.Characteristic_Value_Number_List:
        knn_handler.do_knn(item, item)
    knn_handler.do_knn(KnnTask.Characteristic_Value, KnnTask.Neighbor_Number)