# coding:utf-8


class DateConfig:
    """
    和日期相关的配置
    """

    # 按照参数code_，查找表mdl_stock_analysis时的开始时间
    Mdl_Stock_Analysis_Begin_Date = '20180423'
    # 按照参数code_，查找表mdl_stock_analysis时的结束时间
    Mdl_Stock_Analysis_End_Date = '20210423'

    ###############################################################################################

    # 按照参数code_，查找表mdl_stock_month_analysis时的开始时间
    Mdl_Stock_Month_Analysis_Begin_Date = '20180401'
    # 按照参数code_，查找表mdl_stock_analysis时的结束时间
    Mdl_Stock_Month_Analysis_End_Date = '20210430'
