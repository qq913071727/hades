# coding:utf-8

class StockTrendPredictionConfig:
    """
    预测开始时间
    """
    Prediction_Begin_Date = '2022-10-01'

    """
    预测结束时间
    """
    Prediction_End_Date = '2022-12-31'

    """
    时间跨度
    """
    Date_Number = 250
