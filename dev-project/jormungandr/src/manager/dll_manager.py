#!/usr/bin/env python
# -*- coding: utf-8 -*-

from src.manager.log_manager import LogManager

Logger = LogManager.get_logger(__name__)

from ctypes import *
from src.constant.dll import Dll


class DllManager:
    """
    dll管理类
    """

    def get_dll(self):
        """
        返回dll对象
        """
        return CDLL(Dll.Lib_Bimon_Path)
