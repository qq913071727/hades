#!/usr/bin/env python
# -*- coding: utf-8 -*-
from src.manager.log_manager import LogManager

Logger = LogManager.get_logger(__name__)


class FileManager:
    """
    文件管理类
    """

    @staticmethod
    def write(path, mode, content):
        """
        写文件
        :param path:
        :param mode:
        :param content:
        :return:
        """
        f = open(path, mode, encoding='utf-8')
        f.write(content)
        f.close()

    @staticmethod
    def read(path, mode):
        """
        读文件
        :param path:
        :param mode:
        :return:
        """
        f = open(path, mode, encoding='utf-8')
        content = f.read()
        f.close()
        return content
