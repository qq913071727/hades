# coding:utf-8

from os import path


class FileAndPathConstant:
    """
    文件和路径相关的常量类
    """

    ############################################ 通用的常量 ############################################
    # 当前项目所在目录的盘符
    System_Drive = path.abspath(__file__)[0:2]

    ############################################ k近邻算法用到的常量 ############################################
    # 向量文件的存储路径
    Knn_Vector_File_Path = System_Drive + '\github-repository\hades\dev-project\jormungandr\\resources\knn'

    # 训练数据存储路径
    Knn_Training_Data_Path = '/training'

    # 测试数据存储路径
    Knn_Testing_Data_Path = '/testing'

    # 向量文件扩展名
    Knn_Vector_File_Extension_Name = '.txt'

    ############################################ 决策树算法用到的常量 ############################################
    # 向量文件的存储路径
    Tree_Vector_File_Path = System_Drive + '\github-repository\hades\dev-project\jormungandr\\resources\\tree'

    # 训练数据存储路径
    Tree_Training_Data_Path = '/training'

    # 测试数据存储路径
    Tree_Testing_Data_Path = '/testing'

    # 向量文件扩展名
    Tree_Vector_File_Extension_Name = '.txt'
