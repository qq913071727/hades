#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from src.constant.file_and_path_constant import FileAndPathConstant


class Dll:
    """
    libbimon.dll文件的常量类
    """
    Lib_Bimon_Path = FileAndPathConstant.System_Drive + "/github-repository/hades/dev-project/bimon/output/libbimon.dll"
