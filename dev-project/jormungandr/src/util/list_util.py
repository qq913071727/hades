# coding:utf-8

class ListUtil:

    @staticmethod
    def set_to_list(_set):
        """
        将set类型对象转换为list类型对象
        """
        _list = list()
        for _s in _set:
            _list.append(_s)
        return _list


