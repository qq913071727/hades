# coding:utf-8

import os
import tarfile
from src.constant.file_and_path_constant import FileAndPathConstant


class FileUtil:

    @staticmethod
    def un_tar(file_name):
        """
        untar
        zip
        file
        """
        tar = tarfile.open(file_name)
        names = tar.getnames()
        if os.path.isdir(file_name + "_files"):
            pass
        else:
            os.mkdir(file_name + "_files")
        # 因为解压后是很多文件，预先建立同名目录
        for name in names:
            tar.extract(name, file_name + "_files/")
        tar.close()


if __name__ == "__main__":
    FileUtil.un_tar(FileAndPathConstant.System_Drive + '/github-repository/hades/dev-project/jormungandr/dist/jormungandr-1.0.tar.gz')
