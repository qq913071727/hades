# coding:utf-8

class DoubleUtil:

    @staticmethod
    def str_2dlist_to_float_2dlist(str_2dlist):
        """
        将str类型的二维数组转换为float类型的二维数组
        """
        float_2dlist = []
        for str_row in str_2dlist:
            float_list = []
            for column in str_row:
                float_list.append(float(column))
            float_2dlist.append(float_list)
        return float_2dlist

    @staticmethod
    def str_list_to_float_list(str_list):
        """
        将str类型的数组转换为float类型的数组
        """
        float_list = []
        for column in str_list:
            float_list.append(float(column))
        return float_list
