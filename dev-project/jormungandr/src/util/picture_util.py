# coding:utf-8

import matplotlib
import matplotlib.pyplot as plt


class PictureUtil:
    """
    图片工具类
    """

    def scatter(self, data_x_list, data_y_list):
        """
        生成散点图
        :param data_x_list:
        :param data_y_list:
        :return:
        """
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.scatter(data_x_list, data_y_list)
        plt.show()
