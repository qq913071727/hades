package org.tpc.airlines.eastern.service;

import net.xdevelop.tpc.annotation.TPCTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;
import org.tpc.airlines.eastern.mapper.TicketMapper;
import org.tpc.airlines.eastern.model.Ticket;

@Component
public class TicketService {

    @Autowired
    TicketMapper ticketMapper;

    @Autowired
    PlatformTransactionManager tm;

    /**
     * 保存Ticket对象
     * @param ticket
     */
    @TPCTransactional
    public void save(Ticket ticket) {
        ticketMapper.save(ticket);
    }

    /**
     * 根据id，删除Ticket对象
     * @param id
     */
    public void delete(Long id){
        ticketMapper.delete(id);
    }

    /**
     * 修改Ticket对象
     * @param ticket
     */
    public void update(Ticket ticket){
        ticketMapper.update(ticket);
    }

    /**
     *根据Ticket对象的属性，查询符合条件的Ticket对象
     * @param ticket
     * @return
     */
    public Ticket find(Ticket ticket){
        return ticketMapper.find(ticket);
    }
}
