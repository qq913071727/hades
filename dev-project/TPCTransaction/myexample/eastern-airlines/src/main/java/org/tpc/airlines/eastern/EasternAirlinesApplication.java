package org.tpc.airlines.eastern;

import net.xdevelop.tpc.EnableTPC;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableEurekaClient
@MapperScan("org.tpc.airlines.eastern.mapper")
@PropertySource(value = {"classpath:druid.properties", "classpath:mybatis.properties"})
@EnableTransactionManagement
@EnableAutoConfiguration
@EnableTPC
public class EasternAirlinesApplication {
    public static void main(String[] args) {
        SpringApplication.run(EasternAirlinesApplication.class, args);
    }
}
