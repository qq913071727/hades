package org.tpc.airlines.eastern.mapper;

import org.tpc.airlines.eastern.model.Ticket;

public interface TicketMapper {

    void save(Ticket ticket);

    void delete(Long id);

    void update(Ticket ticket);

    Ticket find(Ticket ticket);
}
