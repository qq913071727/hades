package org.tpc.airlines.eastern.model;

import java.io.Serializable;

public class Ticket implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 始发地
     */
    private String departurePlace;

    /**
     * 目的地
     */
    private String destination;

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDeparturePlace() {

        return departurePlace;
    }

    public void setDeparturePlace(String departurePlace) {
        this.departurePlace = departurePlace;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
