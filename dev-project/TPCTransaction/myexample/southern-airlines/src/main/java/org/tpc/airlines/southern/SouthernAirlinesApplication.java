package org.tpc.airlines.southern;

import net.xdevelop.tpc.EnableTPC;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableEurekaClient
@MapperScan("org.tpc.airlines.southern.mapper")
@PropertySource(value = {"classpath:druid.properties", "classpath:mybatis.properties"})
@EnableTransactionManagement
@EnableAutoConfiguration
@EnableTPC
public class SouthernAirlinesApplication {
    public static void main(String[] args) {
        SpringApplication.run(SouthernAirlinesApplication.class, args);
    }
}
