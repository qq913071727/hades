package org.tpc.airlines.southern.controller;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.tpc.airlines.southern.model.Ticket;
import org.tpc.airlines.southern.service.TicketService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(value = "机票服务")
@RestController
@RequestMapping("/ticket")
public class TicketController {

    @Autowired
    private TicketService ticketService;

    /**
     * 保存Ticket对象
     * @param request
     * @param response
     * @param ticket
     */
    @ApiOperation(value = "保存机票记录", nickname = "save", notes = "")
    @RequestMapping(value = "save", method=RequestMethod.POST, produces={"application/json;charset=utf-8"})
    @ResponseBody
    public ResponseEntity<String> save(HttpServletRequest request, HttpServletResponse response, @RequestBody Ticket ticket) throws Exception {
        ticketService.save(ticket);
        return new ResponseEntity<String>("save ticket from " + ticket.getDeparturePlace() + " to " + ticket.getDestination() + " successfully", HttpStatus.OK);
    }

    /**
     * 根据id，删除Ticket对象
     * @param request
     * @param response
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id，删除Ticket对象", nickname = "delete", notes = "")
    @RequestMapping(value = "delete/{id}", method=RequestMethod.DELETE, produces={"application/json;charset=utf-8"})
    @ResponseBody
    public ResponseEntity<String> delete(HttpServletRequest request, HttpServletResponse response,
                                         @PathVariable("id") Long id) {

        ticketService.delete(id);
        return new ResponseEntity<String>("delete ticket of id: "+id + " successfully", HttpStatus.OK);
    }

    @ApiOperation(value = "修改Ticket对象", nickname = "update", notes = "")
    @RequestMapping(value = "update", method=RequestMethod.PUT, produces={"application/json;charset=utf-8"})
    @ResponseBody
    public ResponseEntity<String> update(HttpServletRequest request, HttpServletResponse response,
                                         @RequestBody Ticket ticket) {

        ticketService.update(ticket);
        return new ResponseEntity<String>("update ticket of id: "+ticket.getId() + " successfully", HttpStatus.OK);
    }

    @ApiOperation(value = "根据Ticket对象的属性，查找符合条件的Ticket对象", nickname = "find", notes = "")
    @RequestMapping(value = "find", method=RequestMethod.POST, produces={"application/json;charset=utf-8"})
    @ResponseBody
    public ResponseEntity<String> find(HttpServletRequest request, HttpServletResponse response,
                                         @RequestBody Ticket ticket) {

        Ticket newTicket=ticketService.find(ticket);
        return new ResponseEntity<String>(JSON.toJSONString(newTicket), HttpStatus.OK);
    }
}
