package org.tpc.airlines.southern.mapper;

import org.tpc.airlines.southern.model.Ticket;

public interface TicketMapper {

    void save(Ticket ticket);

    void delete(Long id);

    void update(Ticket ticket);

    Ticket find(Ticket ticket);
}
