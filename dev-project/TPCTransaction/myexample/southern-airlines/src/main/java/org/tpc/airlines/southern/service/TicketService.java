package org.tpc.airlines.southern.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.tpc.airlines.southern.mapper.TicketMapper;
import org.tpc.airlines.southern.model.Ticket;

@Component
public class TicketService {

    @Autowired
    TicketMapper ticketMapper;

    @Autowired
    PlatformTransactionManager tm;

    public void save(Ticket ticket) throws Exception {
        throw new Exception();
//        ticketMapper.save(ticket);
    }

    public void delete(Long id) {
        ticketMapper.delete(id);
    }

    public void update(Ticket ticket){
        ticketMapper.update(ticket);
    }

    public Ticket find(Ticket ticket){
        return ticketMapper.find(ticket);
    }
}
