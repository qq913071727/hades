package org.tpc.airlines.client.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.tpc.airlines.client.model.Ticket;
import org.tpc.airlines.client.service.EasternAirlinesService;
import org.tpc.airlines.client.service.SouthernAirlinesService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

@Api(value = "机票服务")
@RestController
@RequestMapping("/ticket")
public class TicketController {

    private enum Airlines {
        ALL("ALL"),
        EASTERN_AIRLINES("EASTERN_AIRLINES"),
        SOUTHERN_AIRLINES("SOUTHERN_AIRLINES");

        private String name;

        private Airlines(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    @Autowired
    private EasternAirlinesService easternAirlinesService;

    @Autowired
    private SouthernAirlinesService southernAirlinesService;

    /**
     * 根据参数airlines，在航空公司的数据库中添加Ticket对象
     *
     * @param request
     * @param response
     * @param airlines
     * @param ticket
     * @return
     */
    @ApiOperation(value = "根据参数airlines，在航空公司的数据库中添加Ticket对象", nickname = "save", notes = "")
    @RequestMapping(value="save", method=RequestMethod.POST, produces={"application/json;charset=utf-8"})
    @ResponseBody
    public ResponseEntity<String> save(HttpServletRequest request, HttpServletResponse response,
                                       @ApiParam(value = "航空公司名称", required = true, allowableValues = "ALL, EASTERN_AIRLINES, SOUTHERN_AIRLINES") @RequestParam("airlines") String airlines,
                                       @ApiParam(value = "departurePlace是始发地，destination是目的地", required = true) @RequestBody Ticket ticket) {

        if (Airlines.EASTERN_AIRLINES.toString().equals(airlines)) {
            easternAirlinesService.save(ticket);
        } else if (Airlines.SOUTHERN_AIRLINES.toString().equals(airlines)) {
            southernAirlinesService.save(ticket);
        } else if (Airlines.ALL.toString().equals(airlines)) {
            easternAirlinesService.save(ticket);
            southernAirlinesService.save(ticket);
        }
        return new ResponseEntity<String>("save ticket from " + ticket.getDeparturePlace() + " to " + ticket.getDestination() + " successfully", HttpStatus.OK);
    }

    /**
     * 根据id和airlines，删除Ticket对象
     *
     * @param request
     * @param response
     * @param airlines
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id和airlines，删除Ticket对象", nickname = "delete", notes = "")
    @RequestMapping(value="delete/{id}", method=RequestMethod.DELETE, produces={"application/json;charset=utf-8"})
    @ResponseBody
    public ResponseEntity<String> delete(HttpServletRequest request, HttpServletResponse response,
                                         @ApiParam(value = "航空公司名称", required = true, allowableValues = "ALL, EASTERN_AIRLINES, SOUTHERN_AIRLINES") @RequestParam("airlines") String airlines,
                                         @ApiParam(value = "ticket的id", required = true) @PathVariable("id") String id) {

        if (Airlines.EASTERN_AIRLINES.toString().equals(airlines)) {
            easternAirlinesService.delete(new Long(id));
        } else if (Airlines.SOUTHERN_AIRLINES.toString().equals(airlines)) {
            southernAirlinesService.delete(new Long(id));
        } else if (Airlines.ALL.toString().equals(airlines)) {
            easternAirlinesService.delete(new Long(id));
            southernAirlinesService.delete(new Long(id));
        }
        return new ResponseEntity<String>("delete ticket of id: " + id.toString() + " successfully", HttpStatus.OK);
    }

    /**
     * 修改Ticket对象
     *
     * @param request
     * @param response
     * @param airlines
     * @param ticket
     * @return
     */
    @ApiOperation(value = "修改Ticket对象", nickname = "update", notes = "")
    @RequestMapping(value="update", method=RequestMethod.PUT, produces={"application/json;charset=utf-8"})
    @ResponseBody
    public ResponseEntity<String> update(HttpServletRequest request, HttpServletResponse response,
                                         @ApiParam(value = "航空公司名称", required = true, allowableValues = "ALL, EASTERN_AIRLINES, SOUTHERN_AIRLINES") @RequestParam("airlines") String airlines,
                                         @ApiParam(value = "id是主键，departurePlace是始发地，destination是目的地", required = true) @RequestBody Ticket ticket) {

        if (Airlines.EASTERN_AIRLINES.toString().equals(airlines)) {
            easternAirlinesService.update(ticket);
        } else if (Airlines.SOUTHERN_AIRLINES.toString().equals(airlines)) {
            southernAirlinesService.update(ticket);
        } else if (Airlines.ALL.toString().equals(airlines)) {
            easternAirlinesService.update(ticket);
            southernAirlinesService.update(ticket);
        }
        return new ResponseEntity<String>("update ticket of id: " + ticket.getId() + " successfully", HttpStatus.OK);
    }

    /**
     * 根据Ticket对象的属性，查询符合条件的Ticket对象
     *
     * @param request
     * @param response
     * @param airlines
     * @param ticket
     * @return
     */
    @ApiOperation(value = "根据Ticket对象的属性，查询符合条件的Ticket对象", nickname = "find", notes = "")
    @RequestMapping(value = "find", method=RequestMethod.POST, produces={"application/json;charset=utf-8"})
    @ResponseBody
    public ResponseEntity<String> find(HttpServletRequest request, HttpServletResponse response,
                                       @ApiParam(value = "航空公司名称", required = true, allowableValues = "ALL, EASTERN_AIRLINES, SOUTHERN_AIRLINES") @RequestParam("airlines") String airlines,
                                       @ApiParam(value = "id是主键，departurePlace是始发地，destination是目的地", required = true) @RequestBody Ticket ticket) {

        Ticket easternAirlinesTicket = null;
        Ticket southernAirlinesTicket = null;
//        ResponseEntity<String> easternAirlinesTicket = null;
//        ResponseEntity<String> southernAirlinesTicket = null;

        if (Airlines.EASTERN_AIRLINES.toString().equals(airlines)) {
            easternAirlinesTicket = JSON.parseObject(easternAirlinesService.find(ticket).getBody(), org.tpc.airlines.client.model.Ticket.class);
        } else if (Airlines.SOUTHERN_AIRLINES.toString().equals(airlines)) {
            southernAirlinesTicket = JSON.parseObject(southernAirlinesService.find(ticket).getBody(), org.tpc.airlines.client.model.Ticket.class);
        } else if (Airlines.ALL.toString().equals(airlines)) {
            easternAirlinesTicket = JSON.parseObject(easternAirlinesService.find(ticket).getBody(), org.tpc.airlines.client.model.Ticket.class);
            southernAirlinesTicket = JSON.parseObject(southernAirlinesService.find(ticket).getBody(), org.tpc.airlines.client.model.Ticket.class);
        }
        return new ResponseEntity<String>("find ticket of id: " + ticket.getId() + " successfully", HttpStatus.OK);
    }
}
