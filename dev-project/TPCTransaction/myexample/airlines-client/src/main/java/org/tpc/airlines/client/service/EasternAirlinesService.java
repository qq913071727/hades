package org.tpc.airlines.client.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.tpc.airlines.client.hystrix.EasternAirlinesHystrix;
import org.tpc.airlines.client.model.Ticket;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;

@FeignClient(value = "eastern-airlines", fallback = EasternAirlinesHystrix.class)
public interface EasternAirlinesService {

    /**
     * 添加Ticket对象
     * @param ticket
     */
    @RequestMapping(value = "/ticket/save", method=RequestMethod.POST, produces={"application/json;charset=utf-8"})
    void save(@RequestBody Ticket ticket);

    /**
     * 根据id和airlines，删除机票
     * @param id
     */
    @RequestMapping(value = "/ticket/delete/{id}", method=RequestMethod.DELETE, produces={"application/json;charset=utf-8"})
    void delete(@PathVariable("id") Long id);

    /**
     * 修改Ticket对象
     * @param ticket
     */
    @RequestMapping(value = "/ticket/update", method=RequestMethod.PUT, produces={"application/json;charset=utf-8"})
    void update(@RequestBody Ticket ticket);

    /**
     * 根据Ticket对象的属性，查询符合条件的Ticket对象
     * @param ticket
     * @return
     */
    @RequestMapping(value = "/ticket/find", method=RequestMethod.POST, produces={"application/json;charset=utf-8"})
    ResponseEntity<String> find(@RequestBody Ticket ticket);

}
