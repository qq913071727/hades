package org.tpc.airlines.client.hystrix;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.tpc.airlines.client.model.Ticket;
import org.tpc.airlines.client.service.SouthernAirlinesService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class SouthernAirlinesHystrix implements SouthernAirlinesService {

    @Override
    public void save(Ticket ticket) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void update(Ticket ticket) {

    }

    @Override
    public ResponseEntity<String> find(Ticket ticket) {
        return null;
    }
}
